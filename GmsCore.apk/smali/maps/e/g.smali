.class public Lmaps/e/g;
.super Lmaps/ak/e;


# static fields
.field private static a:Lmaps/e/g;

.field private static j:Ljava/lang/Boolean;


# instance fields
.field private b:I

.field private c:Lmaps/bl/a;

.field private d:Lmaps/bl/a;

.field private e:I

.field private f:I

.field private g:[J

.field private h:I

.field private i:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lmaps/ak/e;-><init>()V

    const/4 v0, 0x6

    new-array v0, v0, [J

    iput-object v0, p0, Lmaps/e/g;->g:[J

    iput v1, p0, Lmaps/e/g;->h:I

    iput v1, p0, Lmaps/e/g;->i:I

    invoke-virtual {p0}, Lmaps/e/g;->g()V

    return-void
.end method

.method private static a(Lmaps/bj/b;)Lmaps/bj/m;
    .locals 1

    instance-of v0, p0, Lmaps/bj/p;

    if-eqz v0, :cond_0

    check-cast p0, Lmaps/bj/p;

    const-string v0, "savedLocationShiftCoefficients_lock"

    invoke-interface {p0, v0}, Lmaps/bj/p;->d(Ljava/lang/String;)Lmaps/bj/m;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lmaps/bj/m;)V
    .locals 0

    if-eqz p0, :cond_0

    invoke-interface {p0}, Lmaps/bj/m;->a()Z

    :cond_0
    return-void
.end method

.method private a(Lmaps/bl/a;I)Z
    .locals 6

    const-wide/32 v4, 0x15752a00

    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/e/g;->c:Lmaps/bl/a;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Lmaps/bl/a;->a()I

    move-result v1

    iget-object v2, p0, Lmaps/e/g;->c:Lmaps/bl/a;

    invoke-virtual {v2}, Lmaps/bl/a;->a()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    if-ge v1, p2, :cond_0

    invoke-virtual {p1}, Lmaps/bl/a;->b()I

    move-result v1

    iget-object v2, p0, Lmaps/e/g;->c:Lmaps/bl/a;

    invoke-virtual {v2}, Lmaps/bl/a;->b()I

    move-result v2

    sub-int/2addr v1, v2

    :goto_1
    if-gez v1, :cond_2

    int-to-long v1, v1

    add-long/2addr v1, v4

    long-to-int v1, v1

    goto :goto_1

    :cond_2
    int-to-long v2, v1

    sub-long v2, v4, v2

    long-to-int v2, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    if-ge v1, p2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static d(Lmaps/bl/a;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    sget-object v2, Lmaps/e/g;->j:Ljava/lang/Boolean;

    if-eqz v2, :cond_1

    sget-object v0, Lmaps/e/g;->j:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lmaps/bl/a;->a()I

    move-result v2

    const v3, 0x2dc6c0

    if-lt v2, v3, :cond_0

    invoke-virtual {p0}, Lmaps/bl/a;->a()I

    move-result v2

    const v3, 0x337f980

    if-gt v2, v3, :cond_0

    invoke-virtual {p0}, Lmaps/bl/a;->b()I

    move-result v2

    const v3, 0x44aa200

    if-lt v2, v3, :cond_0

    invoke-virtual {p0}, Lmaps/bl/a;->b()I

    move-result v2

    const v3, 0x81b3200

    if-gt v2, v3, :cond_0

    invoke-static {}, Lmaps/ct/a;->a()I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_3

    const/16 v3, 0x1cc

    if-eq v2, v3, :cond_2

    const/16 v3, 0x460

    if-ne v2, v3, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public static e()Lmaps/e/g;
    .locals 1

    sget-object v0, Lmaps/e/g;->a:Lmaps/e/g;

    if-nez v0, :cond_0

    invoke-static {}, Lmaps/e/g;->p()V

    :cond_0
    sget-object v0, Lmaps/e/g;->a:Lmaps/e/g;

    return-object v0
.end method

.method private static declared-synchronized p()V
    .locals 2

    const-class v1, Lmaps/e/g;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lmaps/e/g;

    invoke-direct {v0}, Lmaps/e/g;-><init>()V

    sput-object v0, Lmaps/e/g;->a:Lmaps/e/g;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public a()I
    .locals 1

    const/16 v0, 0x35

    return v0
.end method

.method public a(Ljava/io/DataOutput;)V
    .locals 3

    new-instance v0, Lmaps/bb/c;

    sget-object v1, Lmaps/c/cf;->a:Lmaps/bb/d;

    invoke-direct {v0, v1}, Lmaps/bb/c;-><init>(Lmaps/bb/d;)V

    const/4 v1, 0x2

    iget-object v2, p0, Lmaps/e/g;->d:Lmaps/bl/a;

    invoke-virtual {v2}, Lmaps/bl/a;->c()Lmaps/bb/c;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/bb/c;->b(ILmaps/bb/c;)Lmaps/bb/c;

    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Lmaps/bb/c;->a(Ljava/io/OutputStream;)V

    sget-boolean v0, Lmaps/ae/h;->f:Z

    if-eqz v0, :cond_0

    const-string v0, "REQUEST"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Location Shift Request for: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lmaps/e/g;->c:Lmaps/bl/a;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    sget-object v2, Lmaps/c/cf;->b:Lmaps/bb/d;

    invoke-static {v2, p1}, Lmaps/bb/b;->a(Lmaps/bb/d;Ljava/io/DataInput;)Lmaps/bb/c;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3, v0}, Lmaps/bb/c;->d(I)I

    move-result v2

    iput v2, p0, Lmaps/e/g;->b:I

    iget v2, p0, Lmaps/e/g;->b:I

    if-nez v2, :cond_0

    move v2, v1

    :goto_1
    const/4 v4, 0x6

    if-ge v2, v4, :cond_2

    iget-object v4, p0, Lmaps/e/g;->g:[J

    const/4 v5, 0x2

    invoke-virtual {v3, v5, v2}, Lmaps/bb/c;->d(II)J

    move-result-wide v5

    aput-wide v5, v4, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    const/4 v2, 0x4

    invoke-virtual {v3, v2}, Lmaps/bb/c;->d(I)I

    move-result v2

    iput v2, p0, Lmaps/e/g;->f:I

    const/4 v2, 0x5

    invoke-virtual {v3, v2}, Lmaps/bb/c;->d(I)I

    move-result v2

    iput v2, p0, Lmaps/e/g;->e:I

    const/4 v2, 0x3

    invoke-virtual {v3, v2}, Lmaps/bb/c;->f(I)Lmaps/bb/c;

    move-result-object v2

    invoke-static {v2}, Lmaps/bl/a;->a(Lmaps/bb/c;)Lmaps/bl/a;

    move-result-object v2

    iput-object v2, p0, Lmaps/e/g;->c:Lmaps/bl/a;

    iget v2, p0, Lmaps/e/g;->b:I

    if-nez v2, :cond_3

    invoke-virtual {p0}, Lmaps/e/g;->i()V

    sget-boolean v2, Lmaps/ae/h;->f:Z

    if-eqz v2, :cond_3

    const-string v2, "REQUEST"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Location Shift Response for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lmaps/e/g;->c:Lmaps/bl/a;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget v2, p0, Lmaps/e/g;->b:I

    if-nez v2, :cond_4

    :goto_2
    move v1, v0

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public a(Lmaps/bl/a;)Z
    .locals 1

    iget v0, p0, Lmaps/e/g;->e:I

    div-int/lit8 v0, v0, 0x2

    invoke-direct {p0, p1, v0}, Lmaps/e/g;->a(Lmaps/bl/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lmaps/bl/a;)V
    .locals 1

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/e/g;->c:Lmaps/bl/a;

    invoke-virtual {p1, v0}, Lmaps/bl/a;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/e/g;->d:Lmaps/bl/a;

    invoke-virtual {p1, v0}, Lmaps/bl/a;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iput-object p1, p0, Lmaps/e/g;->d:Lmaps/bl/a;

    invoke-static {}, Lmaps/ak/n;->e()Lmaps/ak/n;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Lmaps/ak/n;->a(Lmaps/ak/j;)V

    goto :goto_0
.end method

.method public c(Lmaps/bl/a;)Lmaps/bl/a;
    .locals 10

    const-wide/32 v8, 0xf4240

    iget-object v0, p0, Lmaps/e/g;->g:[J

    const/4 v1, 0x0

    aget-wide v0, v0, v1

    iget-object v2, p0, Lmaps/e/g;->g:[J

    const/4 v3, 0x1

    aget-wide v2, v2, v3

    invoke-virtual {p1}, Lmaps/bl/a;->a()I

    move-result v4

    int-to-long v4, v4

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    iget-object v2, p0, Lmaps/e/g;->g:[J

    const/4 v3, 0x2

    aget-wide v2, v2, v3

    invoke-virtual {p1}, Lmaps/bl/a;->b()I

    move-result v4

    int-to-long v4, v4

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    div-long/2addr v0, v8

    iget-object v2, p0, Lmaps/e/g;->g:[J

    const/4 v3, 0x3

    aget-wide v2, v2, v3

    iget-object v4, p0, Lmaps/e/g;->g:[J

    const/4 v5, 0x4

    aget-wide v4, v4, v5

    invoke-virtual {p1}, Lmaps/bl/a;->a()I

    move-result v6

    int-to-long v6, v6

    mul-long/2addr v4, v6

    add-long/2addr v2, v4

    iget-object v4, p0, Lmaps/e/g;->g:[J

    const/4 v5, 0x5

    aget-wide v4, v4, v5

    invoke-virtual {p1}, Lmaps/bl/a;->b()I

    move-result v6

    int-to-long v6, v6

    mul-long/2addr v4, v6

    add-long/2addr v2, v4

    div-long/2addr v2, v8

    invoke-virtual {p0, p1}, Lmaps/e/g;->a(Lmaps/bl/a;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0, p1}, Lmaps/e/g;->b(Lmaps/bl/a;)V

    :cond_0
    long-to-int v4, v0

    invoke-virtual {p1}, Lmaps/bl/a;->a()I

    move-result v5

    sub-int/2addr v4, v5

    iput v4, p0, Lmaps/e/g;->h:I

    long-to-int v4, v2

    invoke-virtual {p1}, Lmaps/bl/a;->b()I

    move-result v5

    sub-int/2addr v4, v5

    iput v4, p0, Lmaps/e/g;->i:I

    new-instance v4, Lmaps/bl/a;

    long-to-int v0, v0

    long-to-int v1, v2

    invoke-direct {v4, v0, v1}, Lmaps/bl/a;-><init>(II)V

    return-object v4
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public f()V
    .locals 8

    const-wide/32 v6, 0xf4240

    const/4 v5, 0x1

    const-wide/16 v3, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lmaps/e/g;->g:[J

    aput-wide v3, v0, v2

    iget-object v0, p0, Lmaps/e/g;->g:[J

    aput-wide v6, v0, v5

    iget-object v0, p0, Lmaps/e/g;->g:[J

    const/4 v1, 0x2

    aput-wide v3, v0, v1

    iget-object v0, p0, Lmaps/e/g;->g:[J

    const/4 v1, 0x3

    aput-wide v3, v0, v1

    iget-object v0, p0, Lmaps/e/g;->g:[J

    const/4 v1, 0x4

    aput-wide v3, v0, v1

    iget-object v0, p0, Lmaps/e/g;->g:[J

    const/4 v1, 0x5

    aput-wide v6, v0, v1

    iput v2, p0, Lmaps/e/g;->e:I

    iput v2, p0, Lmaps/e/g;->f:I

    iput v2, p0, Lmaps/e/g;->h:I

    iput v2, p0, Lmaps/e/g;->i:I

    iput v5, p0, Lmaps/e/g;->b:I

    return-void
.end method

.method public declared-synchronized g()V
    .locals 5

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ae/c;->c()Lmaps/bj/b;

    move-result-object v1

    invoke-static {v1}, Lmaps/e/g;->a(Lmaps/bj/b;)Lmaps/bj/m;

    move-result-object v2

    const-string v0, "savedLocationShiftCoefficients"

    invoke-static {v1, v0}, Lmaps/ae/f;->a(Lmaps/bj/b;Ljava/lang/String;)Ljava/io/DataInput;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :try_start_1
    invoke-virtual {p0, v0}, Lmaps/e/g;->a(Ljava/io/DataInput;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    :try_start_2
    invoke-static {v2}, Lmaps/e/g;->a(Lmaps/bj/m;)V

    :goto_0
    if-nez v0, :cond_0

    invoke-virtual {p0}, Lmaps/e/g;->f()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    :try_start_3
    sget-boolean v3, Lmaps/ae/h;->f:Z

    if-eqz v3, :cond_1

    const-string v3, "FLASH"

    const-string v4, "Error reading coefficients for location shift."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const-string v3, "savedLocationShiftCoefficients"

    invoke-interface {v1, v3}, Lmaps/bj/b;->a(Ljava/lang/String;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    invoke-static {v2}, Lmaps/e/g;->a(Lmaps/bj/m;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_5
    invoke-static {v2}, Lmaps/e/g;->a(Lmaps/bj/m;)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method public h()Lmaps/bb/c;
    .locals 5

    new-instance v1, Lmaps/bb/c;

    sget-object v0, Lmaps/c/cf;->b:Lmaps/bb/d;

    invoke-direct {v1, v0}, Lmaps/bb/c;-><init>(Lmaps/bb/d;)V

    const/4 v0, 0x1

    iget v2, p0, Lmaps/e/g;->b:I

    invoke-virtual {v1, v0, v2}, Lmaps/bb/c;->j(II)Lmaps/bb/c;

    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x6

    if-ge v0, v2, :cond_0

    const/4 v2, 0x2

    iget-object v3, p0, Lmaps/e/g;->g:[J

    aget-wide v3, v3, v0

    invoke-virtual {v1, v2, v3, v4}, Lmaps/bb/c;->a(IJ)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x3

    iget-object v2, p0, Lmaps/e/g;->c:Lmaps/bl/a;

    invoke-virtual {v2}, Lmaps/bl/a;->c()Lmaps/bb/c;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lmaps/bb/c;->b(ILmaps/bb/c;)Lmaps/bb/c;

    const/4 v0, 0x4

    iget v2, p0, Lmaps/e/g;->f:I

    invoke-virtual {v1, v0, v2}, Lmaps/bb/c;->j(II)Lmaps/bb/c;

    const/4 v0, 0x5

    iget v2, p0, Lmaps/e/g;->e:I

    invoke-virtual {v1, v0, v2}, Lmaps/bb/c;->j(II)Lmaps/bb/c;

    return-object v1
.end method

.method public declared-synchronized i()V
    .locals 5

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ae/c;->c()Lmaps/bj/b;

    move-result-object v1

    invoke-static {v1}, Lmaps/e/g;->a(Lmaps/bj/b;)Lmaps/bj/m;

    move-result-object v2

    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v0, Ljava/io/DataOutputStream;

    invoke-direct {v0, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {p0}, Lmaps/e/g;->h()Lmaps/bb/c;

    move-result-object v4

    check-cast v0, Ljava/io/DataOutputStream;

    invoke-virtual {v4, v0}, Lmaps/bb/c;->a(Ljava/io/OutputStream;)V

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    const-string v3, "savedLocationShiftCoefficients"

    invoke-interface {v1, v0, v3}, Lmaps/bj/b;->a([BLjava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-static {v2}, Lmaps/e/g;->a(Lmaps/bj/m;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_3
    sget-boolean v0, Lmaps/ae/h;->f:Z

    if-eqz v0, :cond_0

    const-string v0, "FLASH"

    const-string v3, "Error writing coefficients for location shift."

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v0, "savedLocationShiftCoefficients"

    invoke-interface {v1, v0}, Lmaps/bj/b;->a(Ljava/lang/String;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    invoke-static {v2}, Lmaps/e/g;->a(Lmaps/bj/m;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_5
    invoke-static {v2}, Lmaps/e/g;->a(Lmaps/bj/m;)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method
