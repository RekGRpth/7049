.class public abstract Lmaps/e/l;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;
.implements Lmaps/e/aa;


# instance fields
.field private final a:Lmaps/ae/d;

.field private b:Ljava/util/concurrent/CopyOnWriteArrayList;

.field protected d:Lmaps/e/a;

.field protected e:Z

.field protected f:Z

.field protected volatile g:Z

.field protected final h:Ljava/lang/Object;

.field protected i:J


# direct methods
.method protected constructor <init>(ZLmaps/ae/d;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/e/l;->e:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/e/l;->g:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lmaps/e/l;->h:Ljava/lang/Object;

    const-wide/16 v0, 0x1388

    iput-wide v0, p0, Lmaps/e/l;->i:J

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lmaps/e/l;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    iput-boolean p1, p0, Lmaps/e/l;->f:Z

    iput-object p2, p0, Lmaps/e/l;->a:Lmaps/ae/d;

    return-void
.end method

.method private c(Lmaps/e/a;)Lmaps/e/a;
    .locals 3

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lmaps/e/a;->c()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lmaps/e/r;

    invoke-direct {v0}, Lmaps/e/r;-><init>()V

    invoke-virtual {v0, p1}, Lmaps/e/r;->a(Landroid/location/Location;)Lmaps/e/r;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/e/l;->o()Lmaps/ae/d;

    move-result-object v1

    invoke-interface {v1}, Lmaps/ae/d;->a()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lmaps/e/r;->a(J)Lmaps/e/r;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/e/r;->c()Lmaps/e/a;

    move-result-object p1

    :cond_0
    invoke-virtual {p1}, Lmaps/e/a;->getProvider()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Lmaps/e/r;

    invoke-direct {v0}, Lmaps/e/r;-><init>()V

    invoke-virtual {v0, p1}, Lmaps/e/r;->a(Landroid/location/Location;)Lmaps/e/r;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/e/l;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/e/r;->a(Ljava/lang/String;)Lmaps/e/r;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/e/r;->c()Lmaps/e/a;

    move-result-object p1

    :cond_1
    iget-object v0, p0, Lmaps/e/l;->d:Lmaps/e/a;

    if-eqz p1, :cond_2

    iput-object p1, p0, Lmaps/e/l;->d:Lmaps/e/a;

    :cond_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Lmaps/e/l;->f:Z

    return-object v0
.end method

.method private i()V
    .locals 4

    iget-boolean v0, p0, Lmaps/e/l;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/e/l;->d:Lmaps/e/a;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmaps/e/l;->o()Lmaps/ae/d;

    move-result-object v0

    invoke-interface {v0}, Lmaps/ae/d;->a()J

    move-result-wide v0

    iget-object v2, p0, Lmaps/e/l;->d:Lmaps/e/a;

    invoke-virtual {v2}, Lmaps/e/a;->getTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/32 v2, 0xafc80

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lmaps/e/l;->k()V

    :cond_0
    return-void
.end method


# virtual methods
.method public declared-synchronized a()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/e/l;->g:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Lmaps/ae/h;->f:Z

    if-eqz v0, :cond_0

    const-string v0, "LOCATION"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Starting "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lmaps/e/l;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/e/l;->g:Z

    new-instance v0, Ljava/lang/Thread;

    const-string v1, "BaseLocationProvider"

    invoke-direct {v0, p0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(I)V
    .locals 3

    iget-object v1, p0, Lmaps/e/l;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/e/l;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/e/k;

    invoke-interface {v0, p1, p0}, Lmaps/e/k;->a(ILmaps/e/aa;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final a(Lmaps/e/a;)V
    .locals 1

    iget-boolean v0, p0, Lmaps/e/l;->g:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lmaps/e/l;->b(Lmaps/e/a;)V

    :cond_0
    return-void
.end method

.method public a(Lmaps/e/k;)V
    .locals 1

    iget-object v0, p0, Lmaps/e/l;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public declared-synchronized b()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/e/l;->g:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lmaps/ae/h;->f:Z

    if-eqz v0, :cond_0

    const-string v0, "LOCATION"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Stopping "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lmaps/e/l;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/e/l;->g:Z

    iget-object v1, p0, Lmaps/e/l;->h:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, p0, Lmaps/e/l;->h:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {p0}, Lmaps/e/l;->g()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(I)V
    .locals 2

    iget-object v0, p0, Lmaps/e/l;->d:Lmaps/e/a;

    if-eqz v0, :cond_0

    new-instance v0, Lmaps/e/r;

    invoke-direct {v0}, Lmaps/e/r;-><init>()V

    iget-object v1, p0, Lmaps/e/l;->d:Lmaps/e/a;

    invoke-virtual {v0, v1}, Lmaps/e/r;->a(Landroid/location/Location;)Lmaps/e/r;

    move-result-object v0

    const v1, 0x1869f

    if-eq p1, v1, :cond_1

    const/4 v1, -0x1

    if-eq p1, v1, :cond_1

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Lmaps/e/r;->a(F)Lmaps/e/r;

    :goto_0
    invoke-virtual {v0}, Lmaps/e/r;->c()Lmaps/e/a;

    move-result-object v0

    iput-object v0, p0, Lmaps/e/l;->d:Lmaps/e/a;

    :cond_0
    return-void

    :cond_1
    invoke-virtual {v0}, Lmaps/e/r;->a()Lmaps/e/r;

    goto :goto_0
.end method

.method protected final b(Lmaps/e/a;)V
    .locals 4

    invoke-direct {p0, p1}, Lmaps/e/l;->c(Lmaps/e/a;)Lmaps/e/a;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmaps/e/a;->a()Lmaps/bl/a;

    move-result-object v0

    move-object v1, v0

    :goto_0
    iget-object v2, p0, Lmaps/e/l;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lmaps/e/l;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/e/k;

    invoke-interface {v0, v1, p0}, Lmaps/e/k;->a(Lmaps/bl/a;Lmaps/e/aa;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_0

    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public d()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public e()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public abstract f()Ljava/lang/String;
.end method

.method protected abstract g()V
.end method

.method protected abstract h()V
.end method

.method protected j()I
    .locals 1

    invoke-direct {p0}, Lmaps/e/l;->i()V

    iget-boolean v0, p0, Lmaps/e/l;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/e/l;->d:Lmaps/e/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/e/l;->d:Lmaps/e/a;

    invoke-virtual {v0}, Lmaps/e/a;->hasAccuracy()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/e/l;->d:Lmaps/e/a;

    invoke-virtual {v0}, Lmaps/e/a;->getAccuracy()F

    move-result v0

    float-to-int v0, v0

    :goto_0
    return v0

    :cond_0
    const v0, 0x1869f

    goto :goto_0
.end method

.method public k()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/e/l;->d:Lmaps/e/a;

    return-void
.end method

.method public l()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/e/l;->f:Z

    return v0
.end method

.method public m()Z
    .locals 4

    iget-object v0, p0, Lmaps/e/l;->d:Lmaps/e/a;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmaps/e/l;->o()Lmaps/ae/d;

    move-result-object v0

    invoke-interface {v0}, Lmaps/ae/d;->a()J

    move-result-wide v0

    iget-object v2, p0, Lmaps/e/l;->d:Lmaps/e/a;

    invoke-virtual {v2}, Lmaps/e/a;->getTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/32 v2, 0xafc80

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()Lmaps/e/a;
    .locals 1

    iget-object v0, p0, Lmaps/e/l;->d:Lmaps/e/a;

    return-object v0
.end method

.method public o()Lmaps/ae/d;
    .locals 1

    iget-object v0, p0, Lmaps/e/l;->a:Lmaps/ae/d;

    return-object v0
.end method

.method public run()V
    .locals 1

    :try_start_0
    invoke-virtual {p0}, Lmaps/e/l;->h()V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lmaps/e/l;->a(I)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "enabled: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lmaps/e/l;->f:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", location: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lmaps/e/l;->d:Lmaps/e/a;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
