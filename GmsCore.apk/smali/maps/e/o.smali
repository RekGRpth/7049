.class Lmaps/e/o;
.super Ljava/lang/Object;


# instance fields
.field a:Z

.field b:Z

.field c:I


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lmaps/e/o;->c:I

    return-void
.end method

.method constructor <init>(Lmaps/e/o;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lmaps/e/o;->c:I

    iget-boolean v0, p1, Lmaps/e/o;->a:Z

    iput-boolean v0, p0, Lmaps/e/o;->a:Z

    iget-boolean v0, p1, Lmaps/e/o;->b:Z

    iput-boolean v0, p0, Lmaps/e/o;->b:Z

    iget v0, p1, Lmaps/e/o;->c:I

    iput v0, p0, Lmaps/e/o;->c:I

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p1, Lmaps/e/o;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Lmaps/e/o;

    iget-boolean v1, p0, Lmaps/e/o;->a:Z

    iget-boolean v2, p1, Lmaps/e/o;->a:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lmaps/e/o;->a:Z

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lmaps/e/o;->b:Z

    iget-boolean v2, p1, Lmaps/e/o;->b:Z

    if-ne v1, v2, :cond_0

    :cond_2
    iget v1, p0, Lmaps/e/o;->c:I

    iget v2, p1, Lmaps/e/o;->c:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
