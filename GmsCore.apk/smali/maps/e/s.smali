.class public Lmaps/e/s;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/e/ab;
.implements Lmaps/e/n;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/content/BroadcastReceiver;

.field private final c:Landroid/content/Context;

.field private final d:J

.field private final e:Lmaps/e/y;

.field private f:Landroid/location/LocationListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lmaps/e/s;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lmaps/e/s;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;JLmaps/e/y;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/e/s;->c:Landroid/content/Context;

    iput-wide p2, p0, Lmaps/e/s;->d:J

    iput-object p4, p0, Lmaps/e/s;->e:Lmaps/e/y;

    return-void
.end method

.method public static a(Landroid/content/Context;J)Lmaps/e/s;
    .locals 2

    new-instance v0, Lmaps/e/s;

    new-instance v1, Lmaps/e/y;

    invoke-direct {v1, p0}, Lmaps/e/y;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, p0, p1, p2, v1}, Lmaps/e/s;-><init>(Landroid/content/Context;JLmaps/e/y;)V

    return-object v0
.end method

.method private a(Landroid/location/LocationListener;)V
    .locals 3

    new-instance v0, Lmaps/e/i;

    invoke-direct {v0, p0, p1}, Lmaps/e/i;-><init>(Lmaps/e/s;Landroid/location/LocationListener;)V

    iput-object v0, p0, Lmaps/e/s;->b:Landroid/content/BroadcastReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lmaps/e/s;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".GMM_LOCATION_BROADCAST"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lmaps/e/s;->c:Landroid/content/Context;

    iget-object v2, p0, Lmaps/e/s;->b:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    sget-object v0, Lmaps/e/s;->a:Ljava/lang/String;

    return-object v0
.end method

.method private c()V
    .locals 4

    sget-boolean v0, Lmaps/ae/h;->g:Z

    if-eqz v0, :cond_0

    sget-object v0, Lmaps/e/s;->a:Ljava/lang/String;

    const-string v1, "Connecting to GMM NLP"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lmaps/e/s;->f:Landroid/location/LocationListener;

    invoke-static {v0}, Lmaps/ap/q;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lmaps/e/s;->f:Landroid/location/LocationListener;

    invoke-direct {p0, v0}, Lmaps/e/s;->a(Landroid/location/LocationListener;)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.location.internal.GMM_NLP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.location.internal.EXTRA_PENDING_INTENT"

    invoke-direct {p0}, Lmaps/e/s;->e()Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "com.google.android.location.internal.EXTRA_PERIOD_MILLIS"

    iget-wide v2, p0, Lmaps/e/s;->d:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    iget-object v1, p0, Lmaps/e/s;->c:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_2

    sget-boolean v0, Lmaps/ae/h;->g:Z

    if-eqz v0, :cond_1

    sget-object v0, Lmaps/e/s;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GMM NLP is bound with polling interval "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lmaps/e/s;->d:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    sget-boolean v0, Lmaps/ae/h;->i:Z

    if-eqz v0, :cond_1

    sget-object v0, Lmaps/e/s;->a:Ljava/lang/String;

    const-string v1, "Unable to bind to GMM NLP"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private d()V
    .locals 2

    sget-boolean v0, Lmaps/ae/h;->g:Z

    if-eqz v0, :cond_0

    sget-object v0, Lmaps/e/s;->a:Ljava/lang/String;

    const-string v1, "Disconnecting from GMM NLP"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lmaps/e/s;->b:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lmaps/e/s;->e()Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/PendingIntent;->cancel()V

    iget-object v0, p0, Lmaps/e/s;->c:Landroid/content/Context;

    iget-object v1, p0, Lmaps/e/s;->b:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/e/s;->b:Landroid/content/BroadcastReceiver;

    :cond_1
    return-void
.end method

.method private e()Landroid/app/PendingIntent;
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Landroid/content/Intent;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lmaps/e/s;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".GMM_LOCATION_BROADCAST"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lmaps/e/s;->c:Landroid/content/Context;

    invoke-static {v1, v3, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, Lmaps/e/s;->f:Landroid/location/LocationListener;

    if-nez v0, :cond_1

    sget-boolean v0, Lmaps/ae/h;->g:Z

    if-eqz v0, :cond_0

    sget-object v0, Lmaps/e/s;->a:Ljava/lang/String;

    const-string v1, "ExternalNlpLocationProvider not started yet."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/e/s;->e:Lmaps/e/y;

    invoke-virtual {v0}, Lmaps/e/y;->a()V

    invoke-direct {p0}, Lmaps/e/s;->d()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/e/s;->f:Landroid/location/LocationListener;

    goto :goto_0
.end method

.method public a(Lmaps/e/e;)V
    .locals 2

    iget-object v0, p0, Lmaps/e/s;->f:Landroid/location/LocationListener;

    if-eqz v0, :cond_1

    sget-object v0, Lmaps/e/e;->a:Lmaps/e/e;

    if-ne v0, p1, :cond_2

    sget-boolean v0, Lmaps/ae/h;->g:Z

    if-eqz v0, :cond_0

    sget-object v0, Lmaps/e/s;->a:Ljava/lang/String;

    const-string v1, "GMM Package Started"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-direct {p0}, Lmaps/e/s;->c()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    sget-object v0, Lmaps/e/e;->b:Lmaps/e/e;

    if-ne v0, p1, :cond_1

    sget-boolean v0, Lmaps/ae/h;->g:Z

    if-eqz v0, :cond_3

    sget-object v0, Lmaps/e/s;->a:Ljava/lang/String;

    const-string v1, "GMM Package Stopped"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    invoke-direct {p0}, Lmaps/e/s;->d()V

    goto :goto_0
.end method

.method public a(ZLandroid/location/LocationListener;)V
    .locals 2

    iget-object v0, p0, Lmaps/e/s;->f:Landroid/location/LocationListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/e/s;->f:Landroid/location/LocationListener;

    if-ne v0, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "ExternalNlpLocationProvider already started with a different location listener"

    invoke-static {v0, v1}, Lmaps/ap/q;->a(ZLjava/lang/Object;)V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-object p2, p0, Lmaps/e/s;->f:Landroid/location/LocationListener;

    iget-object v0, p0, Lmaps/e/s;->e:Lmaps/e/y;

    invoke-virtual {v0, p0}, Lmaps/e/y;->a(Lmaps/e/n;)V

    invoke-direct {p0}, Lmaps/e/s;->c()V

    goto :goto_1
.end method
