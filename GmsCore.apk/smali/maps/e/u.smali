.class public Lmaps/e/u;
.super Lmaps/e/b;

# interfaces
.implements Landroid/location/LocationListener;


# instance fields
.field protected b:Lmaps/e/d;

.field private final c:Lmaps/e/ab;

.field private j:Lmaps/e/a;


# direct methods
.method public constructor <init>(Lmaps/ae/d;Landroid/location/LocationManager;Lmaps/e/ab;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, p2}, Lmaps/e/b;-><init>(ZLmaps/ae/d;Landroid/location/LocationManager;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/e/u;->j:Lmaps/e/a;

    iput-object p3, p0, Lmaps/e/u;->c:Lmaps/e/ab;

    return-void
.end method


# virtual methods
.method protected c(Landroid/location/Location;)V
    .locals 1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    new-instance v0, Lmaps/e/r;

    invoke-direct {v0}, Lmaps/e/r;-><init>()V

    invoke-virtual {v0, p1}, Lmaps/e/r;->a(Landroid/location/Location;)Lmaps/e/r;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/e/r;->c()Lmaps/e/a;

    move-result-object v0

    :cond_0
    invoke-virtual {p0, v0}, Lmaps/e/u;->c(Lmaps/e/a;)V

    return-void
.end method

.method protected c(Lmaps/e/a;)V
    .locals 1

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lmaps/e/u;->b:Lmaps/e/d;

    invoke-virtual {v0, p1}, Lmaps/e/d;->a(Landroid/location/Location;)V

    iput-object p1, p0, Lmaps/e/u;->j:Lmaps/e/a;

    invoke-virtual {p0, p1}, Lmaps/e/u;->b(Lmaps/e/a;)V

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    const-string v0, "network"

    return-object v0
.end method

.method protected g()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/e/u;->c:Lmaps/e/ab;

    invoke-interface {v0}, Lmaps/e/ab;->a()V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lmaps/e/u;->k()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected h()V
    .locals 2

    new-instance v0, Lmaps/e/d;

    const-string v1, ""

    invoke-direct {v0, v1}, Lmaps/e/d;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lmaps/e/u;->b:Lmaps/e/d;

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lmaps/e/u;->c:Lmaps/e/ab;

    iget-boolean v0, p0, Lmaps/e/u;->e:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v0, p0}, Lmaps/e/ab;->a(ZLandroid/location/LocationListener;)V

    monitor-exit p0

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 0

    invoke-virtual {p0, p1}, Lmaps/e/u;->c(Landroid/location/Location;)V

    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0

    return-void
.end method
