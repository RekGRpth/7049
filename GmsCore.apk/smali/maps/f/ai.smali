.class final Lmaps/f/ai;
.super Lmaps/f/ef;

# interfaces
.implements Lmaps/f/fk;


# instance fields
.field private final transient a:Lmaps/f/bi;

.field private final transient c:Lmaps/f/ef;


# direct methods
.method constructor <init>(Lmaps/f/bi;Lmaps/f/ef;)V
    .locals 0

    invoke-direct {p0}, Lmaps/f/ef;-><init>()V

    iput-object p1, p0, Lmaps/f/ai;->a:Lmaps/f/bi;

    iput-object p2, p0, Lmaps/f/ai;->c:Lmaps/f/ef;

    return-void
.end method


# virtual methods
.method public a(II)Lmaps/f/ef;
    .locals 3

    invoke-virtual {p0}, Lmaps/f/ai;->size()I

    move-result v0

    invoke-static {p1, p2, v0}, Lmaps/ap/q;->a(III)V

    if-ne p1, p2, :cond_0

    invoke-static {}, Lmaps/f/ef;->f()Lmaps/f/ef;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lmaps/f/ec;

    iget-object v1, p0, Lmaps/f/ai;->c:Lmaps/f/ef;

    invoke-virtual {v1, p1, p2}, Lmaps/f/ef;->a(II)Lmaps/f/ef;

    move-result-object v1

    iget-object v2, p0, Lmaps/f/ai;->a:Lmaps/f/bi;

    invoke-virtual {v2}, Lmaps/f/bi;->comparator()Ljava/util/Comparator;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lmaps/f/ec;-><init>(Lmaps/f/ef;Ljava/util/Comparator;)V

    invoke-virtual {v0}, Lmaps/f/ec;->d()Lmaps/f/ef;

    move-result-object v0

    goto :goto_0
.end method

.method public a(I)Lmaps/f/et;
    .locals 1

    iget-object v0, p0, Lmaps/f/ai;->c:Lmaps/f/ef;

    invoke-virtual {v0, p1}, Lmaps/f/ef;->a(I)Lmaps/f/et;

    move-result-object v0

    return-object v0
.end method

.method public b()Lmaps/f/et;
    .locals 1

    iget-object v0, p0, Lmaps/f/ai;->c:Lmaps/f/ef;

    invoke-virtual {v0}, Lmaps/f/ef;->b()Lmaps/f/et;

    move-result-object v0

    return-object v0
.end method

.method c()Z
    .locals 1

    iget-object v0, p0, Lmaps/f/ai;->c:Lmaps/f/ef;

    invoke-virtual {v0}, Lmaps/f/ef;->c()Z

    move-result v0

    return v0
.end method

.method public comparator()Ljava/util/Comparator;
    .locals 1

    iget-object v0, p0, Lmaps/f/ai;->a:Lmaps/f/bi;

    invoke-virtual {v0}, Lmaps/f/bi;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Lmaps/f/ai;->a:Lmaps/f/bi;

    invoke-virtual {v0, p1}, Lmaps/f/bi;->d(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Lmaps/f/ai;->c:Lmaps/f/ef;

    invoke-virtual {v0, p1}, Lmaps/f/ef;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public get(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lmaps/f/ai;->c:Lmaps/f/ef;

    invoke-virtual {v0, p1}, Lmaps/f/ef;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lmaps/f/ai;->c:Lmaps/f/ef;

    invoke-virtual {v0}, Lmaps/f/ef;->hashCode()I

    move-result v0

    return v0
.end method

.method public i_()Lmaps/f/fr;
    .locals 1

    iget-object v0, p0, Lmaps/f/ai;->c:Lmaps/f/ef;

    invoke-virtual {v0}, Lmaps/f/ef;->i_()Lmaps/f/fr;

    move-result-object v0

    return-object v0
.end method

.method public indexOf(Ljava/lang/Object;)I
    .locals 1

    iget-object v0, p0, Lmaps/f/ai;->a:Lmaps/f/bi;

    invoke-virtual {v0, p1}, Lmaps/f/bi;->d(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic iterator()Ljava/util/Iterator;
    .locals 1

    invoke-virtual {p0}, Lmaps/f/ai;->i_()Lmaps/f/fr;

    move-result-object v0

    return-object v0
.end method

.method public lastIndexOf(Ljava/lang/Object;)I
    .locals 1

    iget-object v0, p0, Lmaps/f/ai;->a:Lmaps/f/bi;

    invoke-virtual {v0, p1}, Lmaps/f/bi;->d(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic listIterator()Ljava/util/ListIterator;
    .locals 1

    invoke-virtual {p0}, Lmaps/f/ai;->b()Lmaps/f/et;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic listIterator(I)Ljava/util/ListIterator;
    .locals 1

    invoke-virtual {p0, p1}, Lmaps/f/ai;->a(I)Lmaps/f/et;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    iget-object v0, p0, Lmaps/f/ai;->c:Lmaps/f/ef;

    invoke-virtual {v0}, Lmaps/f/ef;->size()I

    move-result v0

    return v0
.end method

.method public bridge synthetic subList(II)Ljava/util/List;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lmaps/f/ai;->a(II)Lmaps/f/ef;

    move-result-object v0

    return-object v0
.end method
