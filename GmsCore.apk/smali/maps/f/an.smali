.class abstract enum Lmaps/f/an;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lmaps/f/an;

.field public static final enum b:Lmaps/f/an;

.field public static final enum c:Lmaps/f/an;

.field public static final enum d:Lmaps/f/an;

.field public static final enum e:Lmaps/f/an;

.field private static final synthetic f:[Lmaps/f/an;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lmaps/f/dw;

    const-string v1, "EXPLICIT"

    invoke-direct {v0, v1, v2}, Lmaps/f/dw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/f/an;->a:Lmaps/f/an;

    new-instance v0, Lmaps/f/dx;

    const-string v1, "REPLACED"

    invoke-direct {v0, v1, v3}, Lmaps/f/dx;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/f/an;->b:Lmaps/f/an;

    new-instance v0, Lmaps/f/dy;

    const-string v1, "COLLECTED"

    invoke-direct {v0, v1, v4}, Lmaps/f/dy;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/f/an;->c:Lmaps/f/an;

    new-instance v0, Lmaps/f/du;

    const-string v1, "EXPIRED"

    invoke-direct {v0, v1, v5}, Lmaps/f/du;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/f/an;->d:Lmaps/f/an;

    new-instance v0, Lmaps/f/dv;

    const-string v1, "SIZE"

    invoke-direct {v0, v1, v6}, Lmaps/f/dv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/f/an;->e:Lmaps/f/an;

    const/4 v0, 0x5

    new-array v0, v0, [Lmaps/f/an;

    sget-object v1, Lmaps/f/an;->a:Lmaps/f/an;

    aput-object v1, v0, v2

    sget-object v1, Lmaps/f/an;->b:Lmaps/f/an;

    aput-object v1, v0, v3

    sget-object v1, Lmaps/f/an;->c:Lmaps/f/an;

    aput-object v1, v0, v4

    sget-object v1, Lmaps/f/an;->d:Lmaps/f/an;

    aput-object v1, v0, v5

    sget-object v1, Lmaps/f/an;->e:Lmaps/f/an;

    aput-object v1, v0, v6

    sput-object v0, Lmaps/f/an;->f:[Lmaps/f/an;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILmaps/f/fc;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmaps/f/an;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/f/an;
    .locals 1

    const-class v0, Lmaps/f/an;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/f/an;

    return-object v0
.end method

.method public static values()[Lmaps/f/an;
    .locals 1

    sget-object v0, Lmaps/f/an;->f:[Lmaps/f/an;

    invoke-virtual {v0}, [Lmaps/f/an;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/f/an;

    return-object v0
.end method
