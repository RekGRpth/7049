.class Lmaps/f/b;
.super Lmaps/f/be;

# interfaces
.implements Ljava/util/ListIterator;


# instance fields
.field final synthetic a:Lmaps/f/j;


# direct methods
.method constructor <init>(Lmaps/f/j;)V
    .locals 0

    iput-object p1, p0, Lmaps/f/b;->a:Lmaps/f/j;

    invoke-direct {p0, p1}, Lmaps/f/be;-><init>(Lmaps/f/az;)V

    return-void
.end method

.method public constructor <init>(Lmaps/f/j;I)V
    .locals 1

    iput-object p1, p0, Lmaps/f/b;->a:Lmaps/f/j;

    invoke-virtual {p1}, Lmaps/f/j;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lmaps/f/be;-><init>(Lmaps/f/az;Ljava/util/Iterator;)V

    return-void
.end method

.method private c()Ljava/util/ListIterator;
    .locals 1

    invoke-virtual {p0}, Lmaps/f/b;->b()Ljava/util/Iterator;

    move-result-object v0

    check-cast v0, Ljava/util/ListIterator;

    return-object v0
.end method


# virtual methods
.method public add(Ljava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lmaps/f/b;->a:Lmaps/f/j;

    invoke-virtual {v0}, Lmaps/f/j;->isEmpty()Z

    move-result v0

    invoke-direct {p0}, Lmaps/f/b;->c()Ljava/util/ListIterator;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/ListIterator;->add(Ljava/lang/Object;)V

    iget-object v1, p0, Lmaps/f/b;->a:Lmaps/f/j;

    iget-object v1, v1, Lmaps/f/j;->a:Lmaps/f/cd;

    invoke-static {v1}, Lmaps/f/cd;->c(Lmaps/f/cd;)I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/f/b;->a:Lmaps/f/j;

    invoke-virtual {v0}, Lmaps/f/j;->e()V

    :cond_0
    return-void
.end method

.method public hasPrevious()Z
    .locals 1

    invoke-direct {p0}, Lmaps/f/b;->c()Ljava/util/ListIterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v0

    return v0
.end method

.method public nextIndex()I
    .locals 1

    invoke-direct {p0}, Lmaps/f/b;->c()Ljava/util/ListIterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/ListIterator;->nextIndex()I

    move-result v0

    return v0
.end method

.method public previous()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lmaps/f/b;->c()Ljava/util/ListIterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public previousIndex()I
    .locals 1

    invoke-direct {p0}, Lmaps/f/b;->c()Ljava/util/ListIterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/ListIterator;->previousIndex()I

    move-result v0

    return v0
.end method

.method public set(Ljava/lang/Object;)V
    .locals 1

    invoke-direct {p0}, Lmaps/f/b;->c()Ljava/util/ListIterator;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/ListIterator;->set(Ljava/lang/Object;)V

    return-void
.end method
