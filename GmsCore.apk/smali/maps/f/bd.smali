.class public abstract Lmaps/f/bd;
.super Lmaps/f/ak;

# interfaces
.implements Ljava/util/Set;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lmaps/f/ak;-><init>()V

    return-void
.end method

.method static a(I)I
    .locals 3

    const/high16 v1, 0x40000000

    const/high16 v0, 0x20000000

    if-ge p0, v0, :cond_0

    invoke-static {p0}, Ljava/lang/Integer;->highestOneBit(I)I

    move-result v0

    shl-int/lit8 v0, v0, 0x2

    :goto_0
    return v0

    :cond_0
    if-ge p0, v1, :cond_1

    const/4 v0, 0x1

    :goto_1
    const-string v2, "collection too large"

    invoke-static {v0, v2}, Lmaps/ap/q;->a(ZLjava/lang/Object;)V

    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Ljava/lang/Iterable;)Lmaps/f/bd;
    .locals 1

    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_0

    invoke-static {p0}, Lmaps/f/ct;->a(Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lmaps/f/bd;->a(Ljava/util/Collection;)Lmaps/f/bd;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lmaps/f/bd;->a(Ljava/util/Iterator;)Lmaps/f/bd;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/util/Collection;)Lmaps/f/bd;
    .locals 2

    instance-of v0, p0, Lmaps/f/bd;

    if-eqz v0, :cond_0

    instance-of v0, p0, Lmaps/f/bi;

    if-nez v0, :cond_0

    move-object v0, p0

    check-cast v0, Lmaps/f/bd;

    invoke-virtual {v0}, Lmaps/f/bd;->c()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lmaps/f/bd;->b(Ljava/util/Collection;)Lmaps/f/bd;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/util/Iterator;)Lmaps/f/bd;
    .locals 1

    invoke-static {p0}, Lmaps/f/fd;->a(Ljava/util/Iterator;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lmaps/f/bd;->b(Ljava/util/Collection;)Lmaps/f/bd;

    move-result-object v0

    return-object v0
.end method

.method private static varargs a([Ljava/lang/Object;)Lmaps/f/bd;
    .locals 12

    const/4 v3, 0x0

    array-length v0, p0

    invoke-static {v0}, Lmaps/f/bd;->a(I)I

    move-result v5

    new-array v6, v5, [Ljava/lang/Object;

    add-int/lit8 v7, v5, -0x1

    const/4 v1, 0x0

    move v2, v3

    move v0, v3

    :goto_0
    array-length v4, p0

    if-ge v2, v4, :cond_4

    aget-object v8, p0, v2

    invoke-virtual {v8}, Ljava/lang/Object;->hashCode()I

    move-result v9

    invoke-static {v9}, Lmaps/f/dk;->a(I)I

    move-result v4

    :goto_1
    and-int v10, v4, v7

    aget-object v11, v6, v10

    if-nez v11, :cond_2

    if-eqz v1, :cond_0

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    aput-object v8, v6, v10

    add-int/2addr v0, v9

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {v11, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    array-length v4, p0

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    move v4, v3

    :goto_2
    if-ge v4, v2, :cond_1

    aget-object v8, p0, v4

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_4
    if-nez v1, :cond_5

    :goto_3
    array-length v1, p0

    const/4 v2, 0x1

    if-ne v1, v2, :cond_6

    aget-object v2, p0, v3

    new-instance v1, Lmaps/f/ed;

    invoke-direct {v1, v2, v0}, Lmaps/f/ed;-><init>(Ljava/lang/Object;I)V

    move-object v0, v1

    :goto_4
    return-object v0

    :cond_5
    invoke-virtual {v1}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object p0

    goto :goto_3

    :cond_6
    array-length v1, p0

    invoke-static {v1}, Lmaps/f/bd;->a(I)I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    if-le v5, v1, :cond_7

    invoke-static {p0}, Lmaps/f/bd;->a([Ljava/lang/Object;)Lmaps/f/bd;

    move-result-object v0

    goto :goto_4

    :cond_7
    new-instance v1, Lmaps/f/e;

    invoke-direct {v1, p0, v0, v6, v7}, Lmaps/f/e;-><init>([Ljava/lang/Object;I[Ljava/lang/Object;I)V

    move-object v0, v1

    goto :goto_4
.end method

.method public static b()Lmaps/f/bd;
    .locals 1

    sget-object v0, Lmaps/f/fu;->a:Lmaps/f/fu;

    return-object v0
.end method

.method public static b(Ljava/lang/Object;)Lmaps/f/bd;
    .locals 1

    new-instance v0, Lmaps/f/ed;

    invoke-direct {v0, p0}, Lmaps/f/ed;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method private static b(Ljava/util/Collection;)Lmaps/f/bd;
    .locals 2

    invoke-interface {p0}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v0

    array-length v1, v0

    packed-switch v1, :pswitch_data_0

    invoke-static {v0}, Lmaps/f/bd;->a([Ljava/lang/Object;)Lmaps/f/bd;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    invoke-static {}, Lmaps/f/bd;->b()Lmaps/f/bd;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-static {v0}, Lmaps/f/bd;->b(Ljava/lang/Object;)Lmaps/f/bd;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method a()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p1, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    instance-of v0, p1, Lmaps/f/bd;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lmaps/f/bd;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lmaps/f/bd;

    invoke-virtual {v0}, Lmaps/f/bd;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lmaps/f/bd;->hashCode()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    if-eq v0, v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-static {p0, p1}, Lmaps/f/a;->a(Ljava/util/Set;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    invoke-static {p0}, Lmaps/f/a;->a(Ljava/util/Set;)I

    move-result v0

    return v0
.end method

.method public abstract i_()Lmaps/f/fr;
.end method

.method public bridge synthetic iterator()Ljava/util/Iterator;
    .locals 1

    invoke-virtual {p0}, Lmaps/f/bd;->i_()Lmaps/f/fr;

    move-result-object v0

    return-object v0
.end method
