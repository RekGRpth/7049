.class final Lmaps/f/bh;
.super Ljava/util/AbstractQueue;


# instance fields
.field final a:Lmaps/f/fm;


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/util/AbstractQueue;-><init>()V

    new-instance v0, Lmaps/f/dq;

    invoke-direct {v0, p0}, Lmaps/f/dq;-><init>(Lmaps/f/bh;)V

    iput-object v0, p0, Lmaps/f/bh;->a:Lmaps/f/fm;

    return-void
.end method


# virtual methods
.method public a()Lmaps/f/fm;
    .locals 2

    iget-object v0, p0, Lmaps/f/bh;->a:Lmaps/f/fm;

    invoke-interface {v0}, Lmaps/f/fm;->f()Lmaps/f/fm;

    move-result-object v0

    iget-object v1, p0, Lmaps/f/bh;->a:Lmaps/f/fm;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method

.method public a(Lmaps/f/fm;)Z
    .locals 2

    invoke-interface {p1}, Lmaps/f/fm;->g()Lmaps/f/fm;

    move-result-object v0

    invoke-interface {p1}, Lmaps/f/fm;->f()Lmaps/f/fm;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/f/ea;->a(Lmaps/f/fm;Lmaps/f/fm;)V

    iget-object v0, p0, Lmaps/f/bh;->a:Lmaps/f/fm;

    invoke-interface {v0}, Lmaps/f/fm;->g()Lmaps/f/fm;

    move-result-object v0

    invoke-static {v0, p1}, Lmaps/f/ea;->a(Lmaps/f/fm;Lmaps/f/fm;)V

    iget-object v0, p0, Lmaps/f/bh;->a:Lmaps/f/fm;

    invoke-static {p1, v0}, Lmaps/f/ea;->a(Lmaps/f/fm;Lmaps/f/fm;)V

    const/4 v0, 0x1

    return v0
.end method

.method public b()Lmaps/f/fm;
    .locals 2

    iget-object v0, p0, Lmaps/f/bh;->a:Lmaps/f/fm;

    invoke-interface {v0}, Lmaps/f/fm;->f()Lmaps/f/fm;

    move-result-object v0

    iget-object v1, p0, Lmaps/f/bh;->a:Lmaps/f/fm;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, v0}, Lmaps/f/bh;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public clear()V
    .locals 2

    iget-object v0, p0, Lmaps/f/bh;->a:Lmaps/f/fm;

    invoke-interface {v0}, Lmaps/f/fm;->f()Lmaps/f/fm;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lmaps/f/bh;->a:Lmaps/f/fm;

    if-eq v0, v1, :cond_0

    invoke-interface {v0}, Lmaps/f/fm;->f()Lmaps/f/fm;

    move-result-object v1

    invoke-static {v0}, Lmaps/f/ea;->d(Lmaps/f/fm;)V

    move-object v0, v1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmaps/f/bh;->a:Lmaps/f/fm;

    iget-object v1, p0, Lmaps/f/bh;->a:Lmaps/f/fm;

    invoke-interface {v0, v1}, Lmaps/f/fm;->a(Lmaps/f/fm;)V

    iget-object v0, p0, Lmaps/f/bh;->a:Lmaps/f/fm;

    iget-object v1, p0, Lmaps/f/bh;->a:Lmaps/f/fm;

    invoke-interface {v0, v1}, Lmaps/f/fm;->b(Lmaps/f/fm;)V

    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 2

    check-cast p1, Lmaps/f/fm;

    invoke-interface {p1}, Lmaps/f/fm;->f()Lmaps/f/fm;

    move-result-object v0

    sget-object v1, Lmaps/f/ei;->a:Lmaps/f/ei;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 2

    iget-object v0, p0, Lmaps/f/bh;->a:Lmaps/f/fm;

    invoke-interface {v0}, Lmaps/f/fm;->f()Lmaps/f/fm;

    move-result-object v0

    iget-object v1, p0, Lmaps/f/bh;->a:Lmaps/f/fm;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2

    new-instance v0, Lmaps/f/dp;

    invoke-virtual {p0}, Lmaps/f/bh;->a()Lmaps/f/fm;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lmaps/f/dp;-><init>(Lmaps/f/bh;Lmaps/f/fm;)V

    return-object v0
.end method

.method public bridge synthetic offer(Ljava/lang/Object;)Z
    .locals 1

    check-cast p1, Lmaps/f/fm;

    invoke-virtual {p0, p1}, Lmaps/f/bh;->a(Lmaps/f/fm;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic peek()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lmaps/f/bh;->a()Lmaps/f/fm;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic poll()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lmaps/f/bh;->b()Lmaps/f/fm;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 2

    check-cast p1, Lmaps/f/fm;

    invoke-interface {p1}, Lmaps/f/fm;->g()Lmaps/f/fm;

    move-result-object v0

    invoke-interface {p1}, Lmaps/f/fm;->f()Lmaps/f/fm;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/f/ea;->a(Lmaps/f/fm;Lmaps/f/fm;)V

    invoke-static {p1}, Lmaps/f/ea;->d(Lmaps/f/fm;)V

    sget-object v0, Lmaps/f/ei;->a:Lmaps/f/ei;

    if-eq v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public size()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lmaps/f/bh;->a:Lmaps/f/fm;

    invoke-interface {v0}, Lmaps/f/fm;->f()Lmaps/f/fm;

    move-result-object v0

    :goto_0
    iget-object v2, p0, Lmaps/f/bh;->a:Lmaps/f/fm;

    if-eq v0, v2, :cond_0

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0}, Lmaps/f/fm;->f()Lmaps/f/fm;

    move-result-object v0

    goto :goto_0

    :cond_0
    return v1
.end method
