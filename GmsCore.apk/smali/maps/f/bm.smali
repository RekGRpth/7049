.class Lmaps/f/bm;
.super Ljava/util/concurrent/locks/ReentrantLock;


# instance fields
.field final a:Lmaps/f/ea;

.field volatile b:I

.field c:I

.field d:I

.field volatile e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

.field final f:I

.field final g:Ljava/lang/ref/ReferenceQueue;

.field final h:Ljava/lang/ref/ReferenceQueue;

.field final i:Ljava/util/Queue;

.field final j:Ljava/util/concurrent/atomic/AtomicInteger;

.field final k:Ljava/util/Queue;

.field final l:Ljava/util/Queue;


# direct methods
.method constructor <init>(Lmaps/f/ea;II)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lmaps/f/bm;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    iput-object p1, p0, Lmaps/f/bm;->a:Lmaps/f/ea;

    iput p3, p0, Lmaps/f/bm;->f:I

    invoke-virtual {p0, p2}, Lmaps/f/bm;->a(I)Ljava/util/concurrent/atomic/AtomicReferenceArray;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmaps/f/bm;->a(Ljava/util/concurrent/atomic/AtomicReferenceArray;)V

    invoke-virtual {p1}, Lmaps/f/ea;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v0}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    :goto_0
    iput-object v0, p0, Lmaps/f/bm;->g:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {p1}, Lmaps/f/ea;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v1, Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v1}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    :cond_0
    iput-object v1, p0, Lmaps/f/bm;->h:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {p1}, Lmaps/f/ea;->a()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lmaps/f/ea;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    :goto_1
    iput-object v0, p0, Lmaps/f/bm;->i:Ljava/util/Queue;

    invoke-virtual {p1}, Lmaps/f/ea;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Lmaps/f/cb;

    invoke-direct {v0}, Lmaps/f/cb;-><init>()V

    :goto_2
    iput-object v0, p0, Lmaps/f/bm;->k:Ljava/util/Queue;

    invoke-virtual {p1}, Lmaps/f/ea;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Lmaps/f/bh;

    invoke-direct {v0}, Lmaps/f/bh;-><init>()V

    :goto_3
    iput-object v0, p0, Lmaps/f/bm;->l:Ljava/util/Queue;

    return-void

    :cond_2
    move-object v0, v1

    goto :goto_0

    :cond_3
    invoke-static {}, Lmaps/f/ea;->i()Ljava/util/Queue;

    move-result-object v0

    goto :goto_1

    :cond_4
    invoke-static {}, Lmaps/f/ea;->i()Ljava/util/Queue;

    move-result-object v0

    goto :goto_2

    :cond_5
    invoke-static {}, Lmaps/f/ea;->i()Ljava/util/Queue;

    move-result-object v0

    goto :goto_3
.end method


# virtual methods
.method a(Ljava/lang/Object;ILjava/lang/Object;)Ljava/lang/Object;
    .locals 8

    const/4 v1, 0x0

    invoke-virtual {p0}, Lmaps/f/bm;->lock()V

    :try_start_0
    invoke-virtual {p0}, Lmaps/f/bm;->o()V

    iget-object v4, p0, Lmaps/f/bm;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v5, p2, v0

    invoke-virtual {v4, v5}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/f/fm;

    move-object v3, v0

    :goto_0
    if-eqz v3, :cond_3

    invoke-interface {v3}, Lmaps/f/fm;->d()Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v3}, Lmaps/f/fm;->c()I

    move-result v2

    if-ne v2, p2, :cond_2

    if-eqz v6, :cond_2

    iget-object v2, p0, Lmaps/f/bm;->a:Lmaps/f/ea;

    iget-object v2, v2, Lmaps/f/ea;->e:Lmaps/ap/a;

    invoke-virtual {v2, p1, v6}, Lmaps/ap/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v3}, Lmaps/f/fm;->a()Lmaps/f/cm;

    move-result-object v7

    invoke-interface {v7}, Lmaps/f/cm;->get()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_1

    invoke-virtual {p0, v7}, Lmaps/f/bm;->a(Lmaps/f/cm;)Z

    move-result v7

    if-eqz v7, :cond_0

    iget v7, p0, Lmaps/f/bm;->b:I

    add-int/lit8 v7, v7, -0x1

    iget v7, p0, Lmaps/f/bm;->c:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lmaps/f/bm;->c:I

    sget-object v7, Lmaps/f/an;->c:Lmaps/f/an;

    invoke-virtual {p0, v6, p2, v2, v7}, Lmaps/f/bm;->a(Ljava/lang/Object;ILjava/lang/Object;Lmaps/f/an;)V

    invoke-virtual {p0, v0, v3}, Lmaps/f/bm;->b(Lmaps/f/fm;Lmaps/f/fm;)Lmaps/f/fm;

    move-result-object v0

    iget v2, p0, Lmaps/f/bm;->b:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v4, v5, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    iput v2, p0, Lmaps/f/bm;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-virtual {p0}, Lmaps/f/bm;->unlock()V

    invoke-virtual {p0}, Lmaps/f/bm;->p()V

    move-object v0, v1

    :goto_1
    return-object v0

    :cond_1
    :try_start_1
    iget v0, p0, Lmaps/f/bm;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/f/bm;->c:I

    sget-object v0, Lmaps/f/an;->b:Lmaps/f/an;

    invoke-virtual {p0, p1, p2, v2, v0}, Lmaps/f/bm;->a(Ljava/lang/Object;ILjava/lang/Object;Lmaps/f/an;)V

    invoke-virtual {p0, v3, p3}, Lmaps/f/bm;->a(Lmaps/f/fm;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p0}, Lmaps/f/bm;->unlock()V

    invoke-virtual {p0}, Lmaps/f/bm;->p()V

    move-object v0, v2

    goto :goto_1

    :cond_2
    :try_start_2
    invoke-interface {v3}, Lmaps/f/fm;->b()Lmaps/f/fm;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    move-object v3, v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lmaps/f/bm;->unlock()V

    invoke-virtual {p0}, Lmaps/f/bm;->p()V

    move-object v0, v1

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/f/bm;->unlock()V

    invoke-virtual {p0}, Lmaps/f/bm;->p()V

    throw v0
.end method

.method a(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;
    .locals 8

    const/4 v1, 0x0

    invoke-virtual {p0}, Lmaps/f/bm;->lock()V

    :try_start_0
    invoke-virtual {p0}, Lmaps/f/bm;->o()V

    iget v0, p0, Lmaps/f/bm;->b:I

    add-int/lit8 v2, v0, 0x1

    iget v0, p0, Lmaps/f/bm;->d:I

    if-le v2, v0, :cond_0

    invoke-virtual {p0}, Lmaps/f/bm;->l()V

    iget v0, p0, Lmaps/f/bm;->b:I

    add-int/lit8 v2, v0, 0x1

    :cond_0
    iget-object v4, p0, Lmaps/f/bm;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v5, p2, v0

    invoke-virtual {v4, v5}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/f/fm;

    move-object v3, v0

    :goto_0
    if-eqz v3, :cond_6

    invoke-interface {v3}, Lmaps/f/fm;->d()Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v3}, Lmaps/f/fm;->c()I

    move-result v7

    if-ne v7, p2, :cond_5

    if-eqz v6, :cond_5

    iget-object v7, p0, Lmaps/f/bm;->a:Lmaps/f/ea;

    iget-object v7, v7, Lmaps/f/ea;->e:Lmaps/ap/a;

    invoke-virtual {v7, p1, v6}, Lmaps/ap/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v3}, Lmaps/f/fm;->a()Lmaps/f/cm;

    move-result-object v4

    invoke-interface {v4}, Lmaps/f/cm;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_3

    iget v5, p0, Lmaps/f/bm;->c:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lmaps/f/bm;->c:I

    invoke-virtual {p0, v3, p3}, Lmaps/f/bm;->a(Lmaps/f/fm;Ljava/lang/Object;)V

    invoke-interface {v4}, Lmaps/f/cm;->b()Z

    move-result v3

    if-nez v3, :cond_2

    sget-object v2, Lmaps/f/an;->c:Lmaps/f/an;

    invoke-virtual {p0, p1, p2, v0, v2}, Lmaps/f/bm;->a(Ljava/lang/Object;ILjava/lang/Object;Lmaps/f/an;)V

    iget v2, p0, Lmaps/f/bm;->b:I

    :cond_1
    :goto_1
    iput v2, p0, Lmaps/f/bm;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lmaps/f/bm;->unlock()V

    invoke-virtual {p0}, Lmaps/f/bm;->p()V

    move-object v0, v1

    :goto_2
    return-object v0

    :cond_2
    :try_start_1
    invoke-virtual {p0}, Lmaps/f/bm;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lmaps/f/bm;->b:I

    add-int/lit8 v2, v0, 0x1

    goto :goto_1

    :cond_3
    if-eqz p4, :cond_4

    invoke-virtual {p0, v3}, Lmaps/f/bm;->b(Lmaps/f/fm;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p0}, Lmaps/f/bm;->unlock()V

    invoke-virtual {p0}, Lmaps/f/bm;->p()V

    goto :goto_2

    :cond_4
    :try_start_2
    iget v1, p0, Lmaps/f/bm;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lmaps/f/bm;->c:I

    sget-object v1, Lmaps/f/an;->b:Lmaps/f/an;

    invoke-virtual {p0, p1, p2, v0, v1}, Lmaps/f/bm;->a(Ljava/lang/Object;ILjava/lang/Object;Lmaps/f/an;)V

    invoke-virtual {p0, v3, p3}, Lmaps/f/bm;->a(Lmaps/f/fm;Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-virtual {p0}, Lmaps/f/bm;->unlock()V

    invoke-virtual {p0}, Lmaps/f/bm;->p()V

    goto :goto_2

    :cond_5
    :try_start_3
    invoke-interface {v3}, Lmaps/f/fm;->b()Lmaps/f/fm;

    move-result-object v3

    goto :goto_0

    :cond_6
    iget v3, p0, Lmaps/f/bm;->c:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lmaps/f/bm;->c:I

    invoke-virtual {p0, p1, p2, v0}, Lmaps/f/bm;->a(Ljava/lang/Object;ILmaps/f/fm;)Lmaps/f/fm;

    move-result-object v0

    invoke-virtual {p0, v0, p3}, Lmaps/f/bm;->a(Lmaps/f/fm;Ljava/lang/Object;)V

    invoke-virtual {v4, v5, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    invoke-virtual {p0}, Lmaps/f/bm;->k()Z

    move-result v0

    if-eqz v0, :cond_7

    iget v0, p0, Lmaps/f/bm;->b:I

    add-int/lit8 v0, v0, 0x1

    :goto_3
    iput v0, p0, Lmaps/f/bm;->b:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-virtual {p0}, Lmaps/f/bm;->unlock()V

    invoke-virtual {p0}, Lmaps/f/bm;->p()V

    move-object v0, v1

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/f/bm;->unlock()V

    invoke-virtual {p0}, Lmaps/f/bm;->p()V

    throw v0

    :cond_7
    move v0, v2

    goto :goto_3
.end method

.method a(I)Ljava/util/concurrent/atomic/AtomicReferenceArray;
    .locals 1

    new-instance v0, Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-direct {v0, p1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;-><init>(I)V

    return-object v0
.end method

.method a(Ljava/lang/Object;I)Lmaps/f/fm;
    .locals 3

    iget v0, p0, Lmaps/f/bm;->b:I

    if-eqz v0, :cond_3

    invoke-virtual {p0, p2}, Lmaps/f/bm;->b(I)Lmaps/f/fm;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_3

    invoke-interface {v0}, Lmaps/f/fm;->c()I

    move-result v1

    if-eq v1, p2, :cond_1

    :cond_0
    :goto_1
    invoke-interface {v0}, Lmaps/f/fm;->b()Lmaps/f/fm;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-interface {v0}, Lmaps/f/fm;->d()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lmaps/f/bm;->a()V

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lmaps/f/bm;->a:Lmaps/f/ea;

    iget-object v2, v2, Lmaps/f/ea;->e:Lmaps/ap/a;

    invoke-virtual {v2, p1, v1}, Lmaps/ap/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_2
    return-object v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method

.method a(Ljava/lang/Object;ILmaps/f/fm;)Lmaps/f/fm;
    .locals 1

    iget-object v0, p0, Lmaps/f/bm;->a:Lmaps/f/ea;

    iget-object v0, v0, Lmaps/f/ea;->n:Lmaps/f/dd;

    invoke-virtual {v0, p0, p1, p2, p3}, Lmaps/f/dd;->a(Lmaps/f/bm;Ljava/lang/Object;ILmaps/f/fm;)Lmaps/f/fm;

    move-result-object v0

    return-object v0
.end method

.method a(Lmaps/f/fm;Lmaps/f/fm;)Lmaps/f/fm;
    .locals 3

    invoke-interface {p1}, Lmaps/f/fm;->a()Lmaps/f/cm;

    move-result-object v0

    iget-object v1, p0, Lmaps/f/bm;->a:Lmaps/f/ea;

    iget-object v1, v1, Lmaps/f/ea;->n:Lmaps/f/dd;

    invoke-virtual {v1, p0, p1, p2}, Lmaps/f/dd;->a(Lmaps/f/bm;Lmaps/f/fm;Lmaps/f/fm;)Lmaps/f/fm;

    move-result-object v1

    iget-object v2, p0, Lmaps/f/bm;->h:Ljava/lang/ref/ReferenceQueue;

    invoke-interface {v0, v2, v1}, Lmaps/f/cm;->a(Ljava/lang/ref/ReferenceQueue;Lmaps/f/fm;)Lmaps/f/cm;

    move-result-object v0

    invoke-interface {v1, v0}, Lmaps/f/fm;->a(Lmaps/f/cm;)V

    return-object v1
.end method

.method a()V
    .locals 1

    invoke-virtual {p0}, Lmaps/f/bm;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {p0}, Lmaps/f/bm;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lmaps/f/bm;->unlock()V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/f/bm;->unlock()V

    throw v0
.end method

.method a(Ljava/lang/Object;ILjava/lang/Object;Lmaps/f/an;)V
    .locals 2

    iget-object v0, p0, Lmaps/f/bm;->a:Lmaps/f/ea;

    iget-object v0, v0, Lmaps/f/ea;->l:Ljava/util/Queue;

    sget-object v1, Lmaps/f/ea;->q:Ljava/util/Queue;

    if-eq v0, v1, :cond_0

    new-instance v0, Lmaps/f/ar;

    invoke-direct {v0, p1, p3, p4}, Lmaps/f/ar;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lmaps/f/an;)V

    iget-object v1, p0, Lmaps/f/bm;->a:Lmaps/f/ea;

    iget-object v1, v1, Lmaps/f/ea;->l:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method a(Ljava/util/concurrent/atomic/AtomicReferenceArray;)V
    .locals 2

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    div-int/lit8 v0, v0, 0x4

    iput v0, p0, Lmaps/f/bm;->d:I

    iget v0, p0, Lmaps/f/bm;->d:I

    iget v1, p0, Lmaps/f/bm;->f:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lmaps/f/bm;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/f/bm;->d:I

    :cond_0
    iput-object p1, p0, Lmaps/f/bm;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    return-void
.end method

.method a(Lmaps/f/fm;)V
    .locals 2

    iget-object v0, p0, Lmaps/f/bm;->a:Lmaps/f/ea;

    invoke-virtual {v0}, Lmaps/f/ea;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/f/bm;->a:Lmaps/f/ea;

    iget-wide v0, v0, Lmaps/f/ea;->j:J

    invoke-virtual {p0, p1, v0, v1}, Lmaps/f/bm;->a(Lmaps/f/fm;J)V

    :cond_0
    iget-object v0, p0, Lmaps/f/bm;->i:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method a(Lmaps/f/fm;J)V
    .locals 2

    iget-object v0, p0, Lmaps/f/bm;->a:Lmaps/f/ea;

    iget-object v0, v0, Lmaps/f/ea;->o:Lmaps/ap/c;

    invoke-virtual {v0}, Lmaps/ap/c;->a()J

    move-result-wide v0

    add-long/2addr v0, p2

    invoke-interface {p1, v0, v1}, Lmaps/f/fm;->a(J)V

    return-void
.end method

.method a(Lmaps/f/fm;Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lmaps/f/bm;->a:Lmaps/f/ea;

    iget-object v0, v0, Lmaps/f/ea;->h:Lmaps/f/cx;

    invoke-virtual {v0, p0, p1, p2}, Lmaps/f/cx;->a(Lmaps/f/bm;Lmaps/f/fm;Ljava/lang/Object;)Lmaps/f/cm;

    move-result-object v0

    invoke-interface {p1, v0}, Lmaps/f/fm;->a(Lmaps/f/cm;)V

    invoke-virtual {p0, p1}, Lmaps/f/bm;->c(Lmaps/f/fm;)V

    return-void
.end method

.method a(Lmaps/f/fm;Lmaps/f/an;)V
    .locals 3

    invoke-interface {p1}, Lmaps/f/fm;->d()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1}, Lmaps/f/fm;->c()I

    move-result v1

    invoke-interface {p1}, Lmaps/f/fm;->a()Lmaps/f/cm;

    move-result-object v2

    invoke-interface {v2}, Lmaps/f/cm;->get()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2, p2}, Lmaps/f/bm;->a(Ljava/lang/Object;ILjava/lang/Object;Lmaps/f/an;)V

    return-void
.end method

.method a(Ljava/lang/Object;ILjava/lang/Object;Ljava/lang/Object;)Z
    .locals 8

    const/4 v1, 0x0

    invoke-virtual {p0}, Lmaps/f/bm;->lock()V

    :try_start_0
    invoke-virtual {p0}, Lmaps/f/bm;->o()V

    iget-object v3, p0, Lmaps/f/bm;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v4, p2, v0

    invoke-virtual {v3, v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/f/fm;

    move-object v2, v0

    :goto_0
    if-eqz v2, :cond_4

    invoke-interface {v2}, Lmaps/f/fm;->d()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v2}, Lmaps/f/fm;->c()I

    move-result v6

    if-ne v6, p2, :cond_3

    if-eqz v5, :cond_3

    iget-object v6, p0, Lmaps/f/bm;->a:Lmaps/f/ea;

    iget-object v6, v6, Lmaps/f/ea;->e:Lmaps/ap/a;

    invoke-virtual {v6, p1, v5}, Lmaps/ap/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v2}, Lmaps/f/fm;->a()Lmaps/f/cm;

    move-result-object v6

    invoke-interface {v6}, Lmaps/f/cm;->get()Ljava/lang/Object;

    move-result-object v7

    if-nez v7, :cond_1

    invoke-virtual {p0, v6}, Lmaps/f/bm;->a(Lmaps/f/cm;)Z

    move-result v6

    if-eqz v6, :cond_0

    iget v6, p0, Lmaps/f/bm;->b:I

    add-int/lit8 v6, v6, -0x1

    iget v6, p0, Lmaps/f/bm;->c:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lmaps/f/bm;->c:I

    sget-object v6, Lmaps/f/an;->c:Lmaps/f/an;

    invoke-virtual {p0, v5, p2, v7, v6}, Lmaps/f/bm;->a(Ljava/lang/Object;ILjava/lang/Object;Lmaps/f/an;)V

    invoke-virtual {p0, v0, v2}, Lmaps/f/bm;->b(Lmaps/f/fm;Lmaps/f/fm;)Lmaps/f/fm;

    move-result-object v0

    iget v2, p0, Lmaps/f/bm;->b:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v3, v4, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    iput v2, p0, Lmaps/f/bm;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-virtual {p0}, Lmaps/f/bm;->unlock()V

    invoke-virtual {p0}, Lmaps/f/bm;->p()V

    move v0, v1

    :goto_1
    return v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lmaps/f/bm;->a:Lmaps/f/ea;

    iget-object v0, v0, Lmaps/f/ea;->f:Lmaps/ap/a;

    invoke-virtual {v0, p3, v7}, Lmaps/ap/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lmaps/f/bm;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/f/bm;->c:I

    sget-object v0, Lmaps/f/an;->b:Lmaps/f/an;

    invoke-virtual {p0, p1, p2, v7, v0}, Lmaps/f/bm;->a(Ljava/lang/Object;ILjava/lang/Object;Lmaps/f/an;)V

    invoke-virtual {p0, v2, p4}, Lmaps/f/bm;->a(Lmaps/f/fm;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lmaps/f/bm;->unlock()V

    invoke-virtual {p0}, Lmaps/f/bm;->p()V

    goto :goto_1

    :cond_2
    :try_start_2
    invoke-virtual {p0, v2}, Lmaps/f/bm;->b(Lmaps/f/fm;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-virtual {p0}, Lmaps/f/bm;->unlock()V

    invoke-virtual {p0}, Lmaps/f/bm;->p()V

    move v0, v1

    goto :goto_1

    :cond_3
    :try_start_3
    invoke-interface {v2}, Lmaps/f/fm;->b()Lmaps/f/fm;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v2

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lmaps/f/bm;->unlock()V

    invoke-virtual {p0}, Lmaps/f/bm;->p()V

    move v0, v1

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/f/bm;->unlock()V

    invoke-virtual {p0}, Lmaps/f/bm;->p()V

    throw v0
.end method

.method a(Ljava/lang/Object;ILmaps/f/cm;)Z
    .locals 7

    const/4 v1, 0x0

    invoke-virtual {p0}, Lmaps/f/bm;->lock()V

    :try_start_0
    iget v0, p0, Lmaps/f/bm;->b:I

    add-int/lit8 v0, v0, -0x1

    iget-object v3, p0, Lmaps/f/bm;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v4, p2, v0

    invoke-virtual {v3, v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/f/fm;

    move-object v2, v0

    :goto_0
    if-eqz v2, :cond_4

    invoke-interface {v2}, Lmaps/f/fm;->d()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v2}, Lmaps/f/fm;->c()I

    move-result v6

    if-ne v6, p2, :cond_3

    if-eqz v5, :cond_3

    iget-object v6, p0, Lmaps/f/bm;->a:Lmaps/f/ea;

    iget-object v6, v6, Lmaps/f/ea;->e:Lmaps/ap/a;

    invoke-virtual {v6, p1, v5}, Lmaps/ap/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v2}, Lmaps/f/fm;->a()Lmaps/f/cm;

    move-result-object v5

    if-ne v5, p3, :cond_1

    iget v1, p0, Lmaps/f/bm;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lmaps/f/bm;->c:I

    invoke-interface {p3}, Lmaps/f/cm;->get()Ljava/lang/Object;

    move-result-object v1

    sget-object v5, Lmaps/f/an;->c:Lmaps/f/an;

    invoke-virtual {p0, p1, p2, v1, v5}, Lmaps/f/bm;->a(Ljava/lang/Object;ILjava/lang/Object;Lmaps/f/an;)V

    invoke-virtual {p0, v0, v2}, Lmaps/f/bm;->b(Lmaps/f/fm;Lmaps/f/fm;)Lmaps/f/fm;

    move-result-object v0

    iget v1, p0, Lmaps/f/bm;->b:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v3, v4, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    iput v1, p0, Lmaps/f/bm;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lmaps/f/bm;->unlock()V

    invoke-virtual {p0}, Lmaps/f/bm;->isHeldByCurrentThread()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lmaps/f/bm;->p()V

    :cond_0
    :goto_1
    return v0

    :cond_1
    invoke-virtual {p0}, Lmaps/f/bm;->unlock()V

    invoke-virtual {p0}, Lmaps/f/bm;->isHeldByCurrentThread()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lmaps/f/bm;->p()V

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    :try_start_1
    invoke-interface {v2}, Lmaps/f/fm;->b()Lmaps/f/fm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lmaps/f/bm;->unlock()V

    invoke-virtual {p0}, Lmaps/f/bm;->isHeldByCurrentThread()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p0}, Lmaps/f/bm;->p()V

    :cond_5
    move v0, v1

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/f/bm;->unlock()V

    invoke-virtual {p0}, Lmaps/f/bm;->isHeldByCurrentThread()Z

    move-result v1

    if-nez v1, :cond_6

    invoke-virtual {p0}, Lmaps/f/bm;->p()V

    :cond_6
    throw v0
.end method

.method a(Lmaps/f/cm;)Z
    .locals 2

    const/4 v0, 0x0

    invoke-interface {p1}, Lmaps/f/cm;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-interface {p1}, Lmaps/f/cm;->get()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method a(Lmaps/f/fm;I)Z
    .locals 7

    invoke-virtual {p0}, Lmaps/f/bm;->lock()V

    :try_start_0
    iget v0, p0, Lmaps/f/bm;->b:I

    add-int/lit8 v0, v0, -0x1

    iget-object v2, p0, Lmaps/f/bm;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v3, p2, v0

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/f/fm;

    move-object v1, v0

    :goto_0
    if-eqz v1, :cond_1

    if-ne v1, p1, :cond_0

    iget v4, p0, Lmaps/f/bm;->c:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lmaps/f/bm;->c:I

    invoke-interface {v1}, Lmaps/f/fm;->d()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v1}, Lmaps/f/fm;->a()Lmaps/f/cm;

    move-result-object v5

    invoke-interface {v5}, Lmaps/f/cm;->get()Ljava/lang/Object;

    move-result-object v5

    sget-object v6, Lmaps/f/an;->c:Lmaps/f/an;

    invoke-virtual {p0, v4, p2, v5, v6}, Lmaps/f/bm;->a(Ljava/lang/Object;ILjava/lang/Object;Lmaps/f/an;)V

    invoke-virtual {p0, v0, v1}, Lmaps/f/bm;->b(Lmaps/f/fm;Lmaps/f/fm;)Lmaps/f/fm;

    move-result-object v0

    iget v1, p0, Lmaps/f/bm;->b:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v2, v3, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    iput v1, p0, Lmaps/f/bm;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lmaps/f/bm;->unlock()V

    invoke-virtual {p0}, Lmaps/f/bm;->p()V

    :goto_1
    return v0

    :cond_0
    :try_start_1
    invoke-interface {v1}, Lmaps/f/fm;->b()Lmaps/f/fm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0}, Lmaps/f/bm;->unlock()V

    invoke-virtual {p0}, Lmaps/f/bm;->p()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/f/bm;->unlock()V

    invoke-virtual {p0}, Lmaps/f/bm;->p()V

    throw v0
.end method

.method a(Lmaps/f/fm;ILmaps/f/an;)Z
    .locals 6

    iget v0, p0, Lmaps/f/bm;->b:I

    add-int/lit8 v0, v0, -0x1

    iget-object v2, p0, Lmaps/f/bm;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v3, p2, v0

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/f/fm;

    move-object v1, v0

    :goto_0
    if-eqz v1, :cond_1

    if-ne v1, p1, :cond_0

    iget v4, p0, Lmaps/f/bm;->c:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lmaps/f/bm;->c:I

    invoke-interface {v1}, Lmaps/f/fm;->d()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v1}, Lmaps/f/fm;->a()Lmaps/f/cm;

    move-result-object v5

    invoke-interface {v5}, Lmaps/f/cm;->get()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {p0, v4, p2, v5, p3}, Lmaps/f/bm;->a(Ljava/lang/Object;ILjava/lang/Object;Lmaps/f/an;)V

    invoke-virtual {p0, v0, v1}, Lmaps/f/bm;->b(Lmaps/f/fm;Lmaps/f/fm;)Lmaps/f/fm;

    move-result-object v0

    iget v1, p0, Lmaps/f/bm;->b:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v2, v3, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    iput v1, p0, Lmaps/f/bm;->b:I

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    invoke-interface {v1}, Lmaps/f/fm;->b()Lmaps/f/fm;

    move-result-object v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method b(I)Lmaps/f/fm;
    .locals 2

    iget-object v0, p0, Lmaps/f/bm;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    and-int/2addr v1, p1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/f/fm;

    return-object v0
.end method

.method b(Ljava/lang/Object;I)Lmaps/f/fm;
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2}, Lmaps/f/bm;->a(Ljava/lang/Object;I)Lmaps/f/fm;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v2, p0, Lmaps/f/bm;->a:Lmaps/f/ea;

    invoke-virtual {v2}, Lmaps/f/ea;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lmaps/f/bm;->a:Lmaps/f/ea;

    invoke-virtual {v2, v1}, Lmaps/f/ea;->c(Lmaps/f/fm;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lmaps/f/bm;->i()V

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method b(Lmaps/f/fm;Lmaps/f/fm;)Lmaps/f/fm;
    .locals 3

    iget-object v0, p0, Lmaps/f/bm;->k:Ljava/util/Queue;

    invoke-interface {v0, p2}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/f/bm;->l:Ljava/util/Queue;

    invoke-interface {v0, p2}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    iget v1, p0, Lmaps/f/bm;->b:I

    invoke-interface {p2}, Lmaps/f/fm;->b()Lmaps/f/fm;

    move-result-object v0

    :goto_0
    if-eq p1, p2, :cond_1

    invoke-virtual {p0, p1}, Lmaps/f/bm;->e(Lmaps/f/fm;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, p1}, Lmaps/f/bm;->d(Lmaps/f/fm;)V

    add-int/lit8 v1, v1, -0x1

    :goto_1
    invoke-interface {p1}, Lmaps/f/fm;->b()Lmaps/f/fm;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1, v0}, Lmaps/f/bm;->a(Lmaps/f/fm;Lmaps/f/fm;)Lmaps/f/fm;

    move-result-object v0

    goto :goto_1

    :cond_1
    iput v1, p0, Lmaps/f/bm;->b:I

    return-object v0
.end method

.method b()V
    .locals 1

    iget-object v0, p0, Lmaps/f/bm;->a:Lmaps/f/ea;

    invoke-virtual {v0}, Lmaps/f/ea;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmaps/f/bm;->c()V

    :cond_0
    iget-object v0, p0, Lmaps/f/bm;->a:Lmaps/f/ea;

    invoke-virtual {v0}, Lmaps/f/ea;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lmaps/f/bm;->d()V

    :cond_1
    return-void
.end method

.method b(Lmaps/f/fm;)V
    .locals 2

    iget-object v0, p0, Lmaps/f/bm;->k:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/f/bm;->a:Lmaps/f/ea;

    invoke-virtual {v0}, Lmaps/f/ea;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/f/bm;->a:Lmaps/f/ea;

    iget-wide v0, v0, Lmaps/f/ea;->j:J

    invoke-virtual {p0, p1, v0, v1}, Lmaps/f/bm;->a(Lmaps/f/fm;J)V

    iget-object v0, p0, Lmaps/f/bm;->l:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method b(Ljava/lang/Object;ILjava/lang/Object;)Z
    .locals 9

    const/4 v1, 0x0

    invoke-virtual {p0}, Lmaps/f/bm;->lock()V

    :try_start_0
    invoke-virtual {p0}, Lmaps/f/bm;->o()V

    iget v0, p0, Lmaps/f/bm;->b:I

    add-int/lit8 v0, v0, -0x1

    iget-object v4, p0, Lmaps/f/bm;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v5, p2, v0

    invoke-virtual {v4, v5}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/f/fm;

    move-object v3, v0

    :goto_0
    if-eqz v3, :cond_4

    invoke-interface {v3}, Lmaps/f/fm;->d()Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v3}, Lmaps/f/fm;->c()I

    move-result v2

    if-ne v2, p2, :cond_3

    if-eqz v6, :cond_3

    iget-object v2, p0, Lmaps/f/bm;->a:Lmaps/f/ea;

    iget-object v2, v2, Lmaps/f/ea;->e:Lmaps/ap/a;

    invoke-virtual {v2, p1, v6}, Lmaps/ap/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v3}, Lmaps/f/fm;->a()Lmaps/f/cm;

    move-result-object v2

    invoke-interface {v2}, Lmaps/f/cm;->get()Ljava/lang/Object;

    move-result-object v7

    iget-object v8, p0, Lmaps/f/bm;->a:Lmaps/f/ea;

    iget-object v8, v8, Lmaps/f/ea;->f:Lmaps/ap/a;

    invoke-virtual {v8, p3, v7}, Lmaps/ap/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    sget-object v2, Lmaps/f/an;->a:Lmaps/f/an;

    :goto_1
    iget v8, p0, Lmaps/f/bm;->c:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lmaps/f/bm;->c:I

    invoke-virtual {p0, v6, p2, v7, v2}, Lmaps/f/bm;->a(Ljava/lang/Object;ILjava/lang/Object;Lmaps/f/an;)V

    invoke-virtual {p0, v0, v3}, Lmaps/f/bm;->b(Lmaps/f/fm;Lmaps/f/fm;)Lmaps/f/fm;

    move-result-object v0

    iget v3, p0, Lmaps/f/bm;->b:I

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v4, v5, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    iput v3, p0, Lmaps/f/bm;->b:I

    sget-object v0, Lmaps/f/an;->a:Lmaps/f/an;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v2, v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {p0}, Lmaps/f/bm;->unlock()V

    invoke-virtual {p0}, Lmaps/f/bm;->p()V

    move v1, v0

    :goto_3
    return v1

    :cond_0
    :try_start_1
    invoke-virtual {p0, v2}, Lmaps/f/bm;->a(Lmaps/f/cm;)Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Lmaps/f/an;->c:Lmaps/f/an;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lmaps/f/bm;->unlock()V

    invoke-virtual {p0}, Lmaps/f/bm;->p()V

    goto :goto_3

    :cond_2
    move v0, v1

    goto :goto_2

    :cond_3
    :try_start_2
    invoke-interface {v3}, Lmaps/f/fm;->b()Lmaps/f/fm;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    move-object v3, v2

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lmaps/f/bm;->unlock()V

    invoke-virtual {p0}, Lmaps/f/bm;->p()V

    goto :goto_3

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/f/bm;->unlock()V

    invoke-virtual {p0}, Lmaps/f/bm;->p()V

    throw v0
.end method

.method c(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 2

    :try_start_0
    invoke-virtual {p0, p1, p2}, Lmaps/f/bm;->b(Ljava/lang/Object;I)Lmaps/f/fm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0}, Lmaps/f/bm;->n()V

    :goto_0
    return-object v0

    :cond_0
    :try_start_1
    invoke-interface {v1}, Lmaps/f/fm;->a()Lmaps/f/cm;

    move-result-object v0

    invoke-interface {v0}, Lmaps/f/cm;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, v1}, Lmaps/f/bm;->a(Lmaps/f/fm;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    invoke-virtual {p0}, Lmaps/f/bm;->n()V

    goto :goto_0

    :cond_1
    :try_start_2
    invoke-virtual {p0}, Lmaps/f/bm;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/f/bm;->n()V

    throw v0
.end method

.method c()V
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lmaps/f/bm;->g:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lmaps/f/fm;

    iget-object v2, p0, Lmaps/f/bm;->a:Lmaps/f/ea;

    invoke-virtual {v2, v0}, Lmaps/f/ea;->a(Lmaps/f/fm;)V

    add-int/lit8 v0, v1, 0x1

    const/16 v1, 0x10

    if-ne v0, v1, :cond_1

    :cond_0
    return-void

    :cond_1
    move v1, v0

    goto :goto_0
.end method

.method c(Lmaps/f/fm;)V
    .locals 2

    invoke-virtual {p0}, Lmaps/f/bm;->h()V

    iget-object v0, p0, Lmaps/f/bm;->k:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/f/bm;->a:Lmaps/f/ea;

    invoke-virtual {v0}, Lmaps/f/ea;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/f/bm;->a:Lmaps/f/ea;

    invoke-virtual {v0}, Lmaps/f/ea;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/f/bm;->a:Lmaps/f/ea;

    iget-wide v0, v0, Lmaps/f/ea;->j:J

    :goto_0
    invoke-virtual {p0, p1, v0, v1}, Lmaps/f/bm;->a(Lmaps/f/fm;J)V

    iget-object v0, p0, Lmaps/f/bm;->l:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/f/bm;->a:Lmaps/f/ea;

    iget-wide v0, v0, Lmaps/f/ea;->k:J

    goto :goto_0
.end method

.method d()V
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lmaps/f/bm;->h:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lmaps/f/cm;

    iget-object v2, p0, Lmaps/f/bm;->a:Lmaps/f/ea;

    invoke-virtual {v2, v0}, Lmaps/f/ea;->a(Lmaps/f/cm;)V

    add-int/lit8 v0, v1, 0x1

    const/16 v1, 0x10

    if-ne v0, v1, :cond_1

    :cond_0
    return-void

    :cond_1
    move v1, v0

    goto :goto_0
.end method

.method d(Lmaps/f/fm;)V
    .locals 1

    sget-object v0, Lmaps/f/an;->c:Lmaps/f/an;

    invoke-virtual {p0, p1, v0}, Lmaps/f/bm;->a(Lmaps/f/fm;Lmaps/f/an;)V

    iget-object v0, p0, Lmaps/f/bm;->k:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/f/bm;->l:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method d(Ljava/lang/Object;I)Z
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    iget v1, p0, Lmaps/f/bm;->b:I

    if-eqz v1, :cond_2

    invoke-virtual {p0, p1, p2}, Lmaps/f/bm;->b(Ljava/lang/Object;I)Lmaps/f/fm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lmaps/f/bm;->n()V

    :goto_0
    return v0

    :cond_0
    :try_start_1
    invoke-interface {v1}, Lmaps/f/fm;->a()Lmaps/f/cm;

    move-result-object v1

    invoke-interface {v1}, Lmaps/f/cm;->get()Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-virtual {p0}, Lmaps/f/bm;->n()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lmaps/f/bm;->n()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/f/bm;->n()V

    throw v0
.end method

.method e(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 8

    const/4 v1, 0x0

    invoke-virtual {p0}, Lmaps/f/bm;->lock()V

    :try_start_0
    invoke-virtual {p0}, Lmaps/f/bm;->o()V

    iget v0, p0, Lmaps/f/bm;->b:I

    add-int/lit8 v0, v0, -0x1

    iget-object v4, p0, Lmaps/f/bm;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v5, p2, v0

    invoke-virtual {v4, v5}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/f/fm;

    move-object v3, v0

    :goto_0
    if-eqz v3, :cond_3

    invoke-interface {v3}, Lmaps/f/fm;->d()Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v3}, Lmaps/f/fm;->c()I

    move-result v2

    if-ne v2, p2, :cond_2

    if-eqz v6, :cond_2

    iget-object v2, p0, Lmaps/f/bm;->a:Lmaps/f/ea;

    iget-object v2, v2, Lmaps/f/ea;->e:Lmaps/ap/a;

    invoke-virtual {v2, p1, v6}, Lmaps/ap/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v3}, Lmaps/f/fm;->a()Lmaps/f/cm;

    move-result-object v7

    invoke-interface {v7}, Lmaps/f/cm;->get()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    sget-object v1, Lmaps/f/an;->a:Lmaps/f/an;

    :goto_1
    iget v7, p0, Lmaps/f/bm;->c:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lmaps/f/bm;->c:I

    invoke-virtual {p0, v6, p2, v2, v1}, Lmaps/f/bm;->a(Ljava/lang/Object;ILjava/lang/Object;Lmaps/f/an;)V

    invoke-virtual {p0, v0, v3}, Lmaps/f/bm;->b(Lmaps/f/fm;Lmaps/f/fm;)Lmaps/f/fm;

    move-result-object v0

    iget v1, p0, Lmaps/f/bm;->b:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v4, v5, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    iput v1, p0, Lmaps/f/bm;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lmaps/f/bm;->unlock()V

    invoke-virtual {p0}, Lmaps/f/bm;->p()V

    move-object v0, v2

    :goto_2
    return-object v0

    :cond_0
    :try_start_1
    invoke-virtual {p0, v7}, Lmaps/f/bm;->a(Lmaps/f/cm;)Z

    move-result v7

    if-eqz v7, :cond_1

    sget-object v1, Lmaps/f/an;->c:Lmaps/f/an;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lmaps/f/bm;->unlock()V

    invoke-virtual {p0}, Lmaps/f/bm;->p()V

    move-object v0, v1

    goto :goto_2

    :cond_2
    :try_start_2
    invoke-interface {v3}, Lmaps/f/fm;->b()Lmaps/f/fm;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    move-object v3, v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lmaps/f/bm;->unlock()V

    invoke-virtual {p0}, Lmaps/f/bm;->p()V

    move-object v0, v1

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/f/bm;->unlock()V

    invoke-virtual {p0}, Lmaps/f/bm;->p()V

    throw v0
.end method

.method e()V
    .locals 1

    iget-object v0, p0, Lmaps/f/bm;->a:Lmaps/f/ea;

    invoke-virtual {v0}, Lmaps/f/ea;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmaps/f/bm;->f()V

    :cond_0
    iget-object v0, p0, Lmaps/f/bm;->a:Lmaps/f/ea;

    invoke-virtual {v0}, Lmaps/f/ea;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lmaps/f/bm;->g()V

    :cond_1
    return-void
.end method

.method e(Lmaps/f/fm;)Z
    .locals 1

    invoke-interface {p1}, Lmaps/f/fm;->d()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-interface {p1}, Lmaps/f/fm;->a()Lmaps/f/cm;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmaps/f/bm;->a(Lmaps/f/cm;)Z

    move-result v0

    goto :goto_0
.end method

.method f(Lmaps/f/fm;)Ljava/lang/Object;
    .locals 3

    const/4 v0, 0x0

    invoke-interface {p1}, Lmaps/f/fm;->d()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lmaps/f/bm;->a()V

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p1}, Lmaps/f/fm;->a()Lmaps/f/cm;

    move-result-object v1

    invoke-interface {v1}, Lmaps/f/cm;->get()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lmaps/f/bm;->a()V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lmaps/f/bm;->a:Lmaps/f/ea;

    invoke-virtual {v2}, Lmaps/f/ea;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lmaps/f/bm;->a:Lmaps/f/ea;

    invoke-virtual {v2, p1}, Lmaps/f/ea;->c(Lmaps/f/fm;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lmaps/f/bm;->i()V

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method f()V
    .locals 1

    :cond_0
    iget-object v0, p0, Lmaps/f/bm;->g:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-nez v0, :cond_0

    return-void
.end method

.method g()V
    .locals 1

    :cond_0
    iget-object v0, p0, Lmaps/f/bm;->h:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-nez v0, :cond_0

    return-void
.end method

.method h()V
    .locals 2

    :cond_0
    :goto_0
    iget-object v0, p0, Lmaps/f/bm;->i:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/f/fm;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lmaps/f/bm;->k:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lmaps/f/bm;->k:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v1, p0, Lmaps/f/bm;->a:Lmaps/f/ea;

    invoke-virtual {v1}, Lmaps/f/ea;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/f/bm;->l:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/f/bm;->l:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-void
.end method

.method i()V
    .locals 1

    invoke-virtual {p0}, Lmaps/f/bm;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {p0}, Lmaps/f/bm;->j()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lmaps/f/bm;->unlock()V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/f/bm;->unlock()V

    throw v0
.end method

.method j()V
    .locals 5

    invoke-virtual {p0}, Lmaps/f/bm;->h()V

    iget-object v0, p0, Lmaps/f/bm;->l:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/f/bm;->a:Lmaps/f/ea;

    iget-object v0, v0, Lmaps/f/ea;->o:Lmaps/ap/c;

    invoke-virtual {v0}, Lmaps/ap/c;->a()J

    move-result-wide v1

    :cond_2
    iget-object v0, p0, Lmaps/f/bm;->l:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/f/fm;

    if-eqz v0, :cond_0

    iget-object v3, p0, Lmaps/f/bm;->a:Lmaps/f/ea;

    invoke-virtual {v3, v0, v1, v2}, Lmaps/f/ea;->a(Lmaps/f/fm;J)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Lmaps/f/fm;->c()I

    move-result v3

    sget-object v4, Lmaps/f/an;->d:Lmaps/f/an;

    invoke-virtual {p0, v0, v3, v4}, Lmaps/f/bm;->a(Lmaps/f/fm;ILmaps/f/an;)Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method k()Z
    .locals 3

    iget-object v0, p0, Lmaps/f/bm;->a:Lmaps/f/ea;

    invoke-virtual {v0}, Lmaps/f/ea;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lmaps/f/bm;->b:I

    iget v1, p0, Lmaps/f/bm;->f:I

    if-lt v0, v1, :cond_1

    invoke-virtual {p0}, Lmaps/f/bm;->h()V

    iget-object v0, p0, Lmaps/f/bm;->k:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/f/fm;

    invoke-interface {v0}, Lmaps/f/fm;->c()I

    move-result v1

    sget-object v2, Lmaps/f/an;->e:Lmaps/f/an;

    invoke-virtual {p0, v0, v1, v2}, Lmaps/f/bm;->a(Lmaps/f/fm;ILmaps/f/an;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method l()V
    .locals 11

    iget-object v7, p0, Lmaps/f/bm;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v8

    const/high16 v0, 0x40000000

    if-lt v8, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v5, p0, Lmaps/f/bm;->b:I

    shl-int/lit8 v0, v8, 0x1

    invoke-virtual {p0, v0}, Lmaps/f/bm;->a(I)Ljava/util/concurrent/atomic/AtomicReferenceArray;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    div-int/lit8 v0, v0, 0x4

    iput v0, p0, Lmaps/f/bm;->d:I

    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v10, v0, -0x1

    const/4 v0, 0x0

    move v6, v0

    :goto_1
    if-ge v6, v8, :cond_5

    invoke-virtual {v7, v6}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/f/fm;

    if-eqz v0, :cond_7

    invoke-interface {v0}, Lmaps/f/fm;->b()Lmaps/f/fm;

    move-result-object v3

    invoke-interface {v0}, Lmaps/f/fm;->c()I

    move-result v1

    and-int v2, v1, v10

    if-nez v3, :cond_2

    invoke-virtual {v9, v2, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    move v1, v5

    :cond_1
    :goto_2
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    move v5, v1

    goto :goto_1

    :cond_2
    move-object v4, v0

    :goto_3
    if-eqz v3, :cond_3

    invoke-interface {v3}, Lmaps/f/fm;->c()I

    move-result v1

    and-int/2addr v1, v10

    if-eq v1, v2, :cond_6

    move-object v2, v3

    :goto_4
    invoke-interface {v3}, Lmaps/f/fm;->b()Lmaps/f/fm;

    move-result-object v3

    move-object v4, v2

    move v2, v1

    goto :goto_3

    :cond_3
    invoke-virtual {v9, v2, v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    move-object v2, v0

    move v1, v5

    :goto_5
    if-eq v2, v4, :cond_1

    invoke-virtual {p0, v2}, Lmaps/f/bm;->e(Lmaps/f/fm;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0, v2}, Lmaps/f/bm;->d(Lmaps/f/fm;)V

    add-int/lit8 v0, v1, -0x1

    :goto_6
    invoke-interface {v2}, Lmaps/f/fm;->b()Lmaps/f/fm;

    move-result-object v1

    move-object v2, v1

    move v1, v0

    goto :goto_5

    :cond_4
    invoke-interface {v2}, Lmaps/f/fm;->c()I

    move-result v0

    and-int v3, v0, v10

    invoke-virtual {v9, v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/f/fm;

    invoke-virtual {p0, v2, v0}, Lmaps/f/bm;->a(Lmaps/f/fm;Lmaps/f/fm;)Lmaps/f/fm;

    move-result-object v0

    invoke-virtual {v9, v3, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    move v0, v1

    goto :goto_6

    :cond_5
    iput-object v9, p0, Lmaps/f/bm;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iput v5, p0, Lmaps/f/bm;->b:I

    goto :goto_0

    :cond_6
    move v1, v2

    move-object v2, v4

    goto :goto_4

    :cond_7
    move v1, v5

    goto :goto_2
.end method

.method m()V
    .locals 5

    const/4 v1, 0x0

    iget v0, p0, Lmaps/f/bm;->b:I

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lmaps/f/bm;->lock()V

    :try_start_0
    iget-object v3, p0, Lmaps/f/bm;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iget-object v0, p0, Lmaps/f/bm;->a:Lmaps/f/ea;

    iget-object v0, v0, Lmaps/f/ea;->l:Ljava/util/Queue;

    sget-object v2, Lmaps/f/ea;->q:Ljava/util/Queue;

    if-eq v0, v2, :cond_2

    move v2, v1

    :goto_0
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    if-ge v2, v0, :cond_2

    invoke-virtual {v3, v2}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/f/fm;

    :goto_1
    if-eqz v0, :cond_1

    invoke-interface {v0}, Lmaps/f/fm;->a()Lmaps/f/cm;

    move-result-object v4

    invoke-interface {v4}, Lmaps/f/cm;->b()Z

    move-result v4

    if-nez v4, :cond_0

    sget-object v4, Lmaps/f/an;->a:Lmaps/f/an;

    invoke-virtual {p0, v0, v4}, Lmaps/f/bm;->a(Lmaps/f/fm;Lmaps/f/an;)V

    :cond_0
    invoke-interface {v0}, Lmaps/f/fm;->b()Lmaps/f/fm;

    move-result-object v0

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    move v0, v1

    :goto_2
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v1

    if-ge v0, v1, :cond_3

    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    invoke-virtual {p0}, Lmaps/f/bm;->e()V

    iget-object v0, p0, Lmaps/f/bm;->k:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    iget-object v0, p0, Lmaps/f/bm;->l:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    iget-object v0, p0, Lmaps/f/bm;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    iget v0, p0, Lmaps/f/bm;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/f/bm;->c:I

    const/4 v0, 0x0

    iput v0, p0, Lmaps/f/bm;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lmaps/f/bm;->unlock()V

    invoke-virtual {p0}, Lmaps/f/bm;->p()V

    :cond_4
    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/f/bm;->unlock()V

    invoke-virtual {p0}, Lmaps/f/bm;->p()V

    throw v0
.end method

.method n()V
    .locals 1

    iget-object v0, p0, Lmaps/f/bm;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    and-int/lit8 v0, v0, 0x3f

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lmaps/f/bm;->q()V

    :cond_0
    return-void
.end method

.method o()V
    .locals 0

    invoke-virtual {p0}, Lmaps/f/bm;->r()V

    return-void
.end method

.method p()V
    .locals 0

    invoke-virtual {p0}, Lmaps/f/bm;->s()V

    return-void
.end method

.method q()V
    .locals 0

    invoke-virtual {p0}, Lmaps/f/bm;->r()V

    invoke-virtual {p0}, Lmaps/f/bm;->s()V

    return-void
.end method

.method r()V
    .locals 2

    invoke-virtual {p0}, Lmaps/f/bm;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {p0}, Lmaps/f/bm;->b()V

    invoke-virtual {p0}, Lmaps/f/bm;->j()V

    iget-object v0, p0, Lmaps/f/bm;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lmaps/f/bm;->unlock()V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/f/bm;->unlock()V

    throw v0
.end method

.method s()V
    .locals 1

    invoke-virtual {p0}, Lmaps/f/bm;->isHeldByCurrentThread()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/f/bm;->a:Lmaps/f/ea;

    invoke-virtual {v0}, Lmaps/f/ea;->j()V

    :cond_0
    return-void
.end method
