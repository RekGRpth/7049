.class final Lmaps/f/cc;
.super Ljava/util/AbstractSet;


# instance fields
.field final synthetic a:Lmaps/f/ea;


# direct methods
.method constructor <init>(Lmaps/f/ea;)V
    .locals 0

    iput-object p1, p0, Lmaps/f/cc;->a:Lmaps/f/ea;

    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    iget-object v0, p0, Lmaps/f/cc;->a:Lmaps/f/ea;

    invoke-virtual {v0}, Lmaps/f/ea;->clear()V

    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x0

    instance-of v1, p1, Ljava/util/Map$Entry;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Ljava/util/Map$Entry;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lmaps/f/cc;->a:Lmaps/f/ea;

    invoke-virtual {v2, v1}, Lmaps/f/ea;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lmaps/f/cc;->a:Lmaps/f/ea;

    iget-object v2, v2, Lmaps/f/ea;->f:Lmaps/ap/a;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Lmaps/ap/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 1

    iget-object v0, p0, Lmaps/f/cc;->a:Lmaps/f/ea;

    invoke-virtual {v0}, Lmaps/f/ea;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2

    new-instance v0, Lmaps/f/dr;

    iget-object v1, p0, Lmaps/f/cc;->a:Lmaps/f/ea;

    invoke-direct {v0, v1}, Lmaps/f/dr;-><init>(Lmaps/f/ea;)V

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x0

    instance-of v1, p1, Ljava/util/Map$Entry;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Ljava/util/Map$Entry;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lmaps/f/cc;->a:Lmaps/f/ea;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lmaps/f/ea;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public size()I
    .locals 1

    iget-object v0, p0, Lmaps/f/cc;->a:Lmaps/f/ea;

    invoke-virtual {v0}, Lmaps/f/ea;->size()I

    move-result v0

    return v0
.end method
