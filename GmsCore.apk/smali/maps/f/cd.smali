.class abstract Lmaps/f/cd;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;
.implements Lmaps/f/at;


# static fields
.field private static final serialVersionUID:J = 0x21f766b1f568c81dL


# instance fields
.field private transient a:Ljava/util/Map;

.field private transient b:I

.field private transient c:Ljava/util/Set;

.field private transient d:Ljava/util/Map;


# direct methods
.method protected constructor <init>(Ljava/util/Map;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    invoke-static {v0}, Lmaps/ap/q;->a(Z)V

    iput-object p1, p0, Lmaps/f/cd;->a:Ljava/util/Map;

    return-void
.end method

.method static synthetic a(Lmaps/f/cd;I)I
    .locals 1

    iget v0, p0, Lmaps/f/cd;->b:I

    add-int/2addr v0, p1

    iput v0, p0, Lmaps/f/cd;->b:I

    return v0
.end method

.method static synthetic a(Lmaps/f/cd;Ljava/lang/Object;)I
    .locals 1

    invoke-direct {p0, p1}, Lmaps/f/cd;->d(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private a(Ljava/lang/Object;Ljava/util/Collection;)Ljava/util/Collection;
    .locals 2

    const/4 v1, 0x0

    instance-of v0, p2, Ljava/util/SortedSet;

    if-eqz v0, :cond_0

    new-instance v0, Lmaps/f/aw;

    check-cast p2, Ljava/util/SortedSet;

    invoke-direct {v0, p0, p1, p2, v1}, Lmaps/f/aw;-><init>(Lmaps/f/cd;Ljava/lang/Object;Ljava/util/SortedSet;Lmaps/f/az;)V

    :goto_0
    return-object v0

    :cond_0
    instance-of v0, p2, Ljava/util/Set;

    if-eqz v0, :cond_1

    new-instance v0, Lmaps/f/fo;

    check-cast p2, Ljava/util/Set;

    invoke-direct {v0, p0, p1, p2}, Lmaps/f/fo;-><init>(Lmaps/f/cd;Ljava/lang/Object;Ljava/util/Set;)V

    goto :goto_0

    :cond_1
    instance-of v0, p2, Ljava/util/List;

    if-eqz v0, :cond_2

    check-cast p2, Ljava/util/List;

    invoke-direct {p0, p1, p2, v1}, Lmaps/f/cd;->a(Ljava/lang/Object;Ljava/util/List;Lmaps/f/az;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_2
    new-instance v0, Lmaps/f/az;

    invoke-direct {v0, p0, p1, p2, v1}, Lmaps/f/az;-><init>(Lmaps/f/cd;Ljava/lang/Object;Ljava/util/Collection;Lmaps/f/az;)V

    goto :goto_0
.end method

.method static synthetic a(Lmaps/f/cd;Ljava/lang/Object;Ljava/util/Collection;)Ljava/util/Collection;
    .locals 1

    invoke-direct {p0, p1, p2}, Lmaps/f/cd;->a(Ljava/lang/Object;Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/util/Collection;)Ljava/util/Iterator;
    .locals 1

    instance-of v0, p1, Ljava/util/List;

    if-eqz v0, :cond_0

    check-cast p1, Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lmaps/f/cd;Ljava/util/Collection;)Ljava/util/Iterator;
    .locals 1

    invoke-direct {p0, p1}, Lmaps/f/cd;->a(Ljava/util/Collection;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/Object;Ljava/util/List;Lmaps/f/az;)Ljava/util/List;
    .locals 1

    instance-of v0, p2, Ljava/util/RandomAccess;

    if-eqz v0, :cond_0

    new-instance v0, Lmaps/f/u;

    invoke-direct {v0, p0, p1, p2, p3}, Lmaps/f/u;-><init>(Lmaps/f/cd;Ljava/lang/Object;Ljava/util/List;Lmaps/f/az;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lmaps/f/j;

    invoke-direct {v0, p0, p1, p2, p3}, Lmaps/f/j;-><init>(Lmaps/f/cd;Ljava/lang/Object;Ljava/util/List;Lmaps/f/az;)V

    goto :goto_0
.end method

.method static synthetic a(Lmaps/f/cd;Ljava/lang/Object;Ljava/util/List;Lmaps/f/az;)Ljava/util/List;
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lmaps/f/cd;->a(Ljava/lang/Object;Ljava/util/List;Lmaps/f/az;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lmaps/f/cd;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lmaps/f/cd;->a:Ljava/util/Map;

    return-object v0
.end method

.method private a()Ljava/util/Set;
    .locals 2

    iget-object v0, p0, Lmaps/f/cd;->a:Ljava/util/Map;

    instance-of v0, v0, Ljava/util/SortedMap;

    if-eqz v0, :cond_0

    new-instance v1, Lmaps/f/cr;

    iget-object v0, p0, Lmaps/f/cd;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-direct {v1, p0, v0}, Lmaps/f/cr;-><init>(Lmaps/f/cd;Ljava/util/SortedMap;)V

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lmaps/f/cq;

    iget-object v1, p0, Lmaps/f/cd;->a:Ljava/util/Map;

    invoke-direct {v0, p0, v1}, Lmaps/f/cq;-><init>(Lmaps/f/cd;Ljava/util/Map;)V

    goto :goto_0
.end method

.method static synthetic b(Lmaps/f/cd;)I
    .locals 2

    iget v0, p0, Lmaps/f/cd;->b:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lmaps/f/cd;->b:I

    return v0
.end method

.method static synthetic b(Lmaps/f/cd;I)I
    .locals 1

    iget v0, p0, Lmaps/f/cd;->b:I

    sub-int/2addr v0, p1

    iput v0, p0, Lmaps/f/cd;->b:I

    return v0
.end method

.method private b(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 2

    iget-object v0, p0, Lmaps/f/cd;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lmaps/f/cd;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    iget-object v1, p0, Lmaps/f/cd;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method

.method static synthetic c(Lmaps/f/cd;)I
    .locals 2

    iget v0, p0, Lmaps/f/cd;->b:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lmaps/f/cd;->b:I

    return v0
.end method

.method private d(Ljava/lang/Object;)I
    .locals 2

    const/4 v1, 0x0

    :try_start_0
    iget-object v0, p0, Lmaps/f/cd;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    iget v0, p0, Lmaps/f/cd;->b:I

    sub-int/2addr v0, v1

    iput v0, p0, Lmaps/f/cd;->b:I

    :cond_0
    move v0, v1

    move v1, v0

    :goto_0
    return v1

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method private h()Ljava/util/Map;
    .locals 2

    iget-object v0, p0, Lmaps/f/cd;->a:Ljava/util/Map;

    instance-of v0, v0, Ljava/util/SortedMap;

    if-eqz v0, :cond_0

    new-instance v1, Lmaps/f/de;

    iget-object v0, p0, Lmaps/f/cd;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-direct {v1, p0, v0}, Lmaps/f/de;-><init>(Lmaps/f/cd;Ljava/util/SortedMap;)V

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lmaps/f/bx;

    iget-object v1, p0, Lmaps/f/cd;->a:Ljava/util/Map;

    invoke-direct {v0, p0, v1}, Lmaps/f/bx;-><init>(Lmaps/f/cd;Ljava/util/Map;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 1

    iget-object v0, p0, Lmaps/f/cd;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lmaps/f/cd;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    :cond_0
    invoke-direct {p0, p1, v0}, Lmaps/f/cd;->a(Ljava/lang/Object;Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    invoke-direct {p0, p1}, Lmaps/f/cd;->b(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lmaps/f/cd;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/f/cd;->b:I

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method abstract b()Ljava/util/Collection;
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lmaps/f/cd;->b:I

    return v0
.end method

.method c(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 1

    invoke-virtual {p0}, Lmaps/f/cd;->b()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public d()Z
    .locals 1

    iget v0, p0, Lmaps/f/cd;->b:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lmaps/f/cd;->d:Ljava/util/Map;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lmaps/f/cd;->h()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lmaps/f/cd;->d:Ljava/util/Map;

    :cond_0
    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p1, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    instance-of v0, p1, Lmaps/f/at;

    if-eqz v0, :cond_1

    check-cast p1, Lmaps/f/at;

    iget-object v0, p0, Lmaps/f/cd;->a:Ljava/util/Map;

    invoke-interface {p1}, Lmaps/f/at;->e()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()V
    .locals 2

    iget-object v0, p0, Lmaps/f/cd;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmaps/f/cd;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    const/4 v0, 0x0

    iput v0, p0, Lmaps/f/cd;->b:I

    return-void
.end method

.method public g()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lmaps/f/cd;->c:Ljava/util/Set;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lmaps/f/cd;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lmaps/f/cd;->c:Ljava/util/Set;

    :cond_0
    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lmaps/f/cd;->a:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/f/cd;->a:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
