.class Lmaps/f/ck;
.super Ljava/lang/ref/WeakReference;

# interfaces
.implements Lmaps/f/fm;


# instance fields
.field final f:I

.field final g:Lmaps/f/fm;

.field volatile h:Lmaps/f/cm;


# direct methods
.method constructor <init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILmaps/f/fm;)V
    .locals 1

    invoke-direct {p0, p2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    invoke-static {}, Lmaps/f/ea;->g()Lmaps/f/cm;

    move-result-object v0

    iput-object v0, p0, Lmaps/f/ck;->h:Lmaps/f/cm;

    iput p3, p0, Lmaps/f/ck;->f:I

    iput-object p4, p0, Lmaps/f/ck;->g:Lmaps/f/fm;

    return-void
.end method


# virtual methods
.method public a()Lmaps/f/cm;
    .locals 1

    iget-object v0, p0, Lmaps/f/ck;->h:Lmaps/f/cm;

    return-object v0
.end method

.method public a(J)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a(Lmaps/f/cm;)V
    .locals 1

    iget-object v0, p0, Lmaps/f/ck;->h:Lmaps/f/cm;

    iput-object p1, p0, Lmaps/f/ck;->h:Lmaps/f/cm;

    invoke-interface {v0, p1}, Lmaps/f/cm;->a(Lmaps/f/cm;)V

    return-void
.end method

.method public a(Lmaps/f/fm;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b()Lmaps/f/fm;
    .locals 1

    iget-object v0, p0, Lmaps/f/ck;->g:Lmaps/f/fm;

    return-object v0
.end method

.method public b(Lmaps/f/fm;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lmaps/f/ck;->f:I

    return v0
.end method

.method public c(Lmaps/f/fm;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public d()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lmaps/f/ck;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public d(Lmaps/f/fm;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public e()J
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public f()Lmaps/f/fm;
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public g()Lmaps/f/fm;
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public h()Lmaps/f/fm;
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public i()Lmaps/f/fm;
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
