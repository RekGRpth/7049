.class abstract Lmaps/f/eg;
.super Lmaps/f/et;


# instance fields
.field private final a:I

.field private b:I


# direct methods
.method protected constructor <init>(I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmaps/f/eg;-><init>(II)V

    return-void
.end method

.method protected constructor <init>(II)V
    .locals 0

    invoke-direct {p0}, Lmaps/f/et;-><init>()V

    invoke-static {p2, p1}, Lmaps/ap/q;->b(II)I

    iput p1, p0, Lmaps/f/eg;->a:I

    iput p2, p0, Lmaps/f/eg;->b:I

    return-void
.end method


# virtual methods
.method protected abstract a(I)Ljava/lang/Object;
.end method

.method public final hasNext()Z
    .locals 2

    iget v0, p0, Lmaps/f/eg;->b:I

    iget v1, p0, Lmaps/f/eg;->a:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasPrevious()Z
    .locals 1

    iget v0, p0, Lmaps/f/eg;->b:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final next()Ljava/lang/Object;
    .locals 2

    invoke-virtual {p0}, Lmaps/f/eg;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lmaps/f/eg;->b:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lmaps/f/eg;->b:I

    invoke-virtual {p0, v0}, Lmaps/f/eg;->a(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final nextIndex()I
    .locals 1

    iget v0, p0, Lmaps/f/eg;->b:I

    return v0
.end method

.method public final previous()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lmaps/f/eg;->hasPrevious()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lmaps/f/eg;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lmaps/f/eg;->b:I

    invoke-virtual {p0, v0}, Lmaps/f/eg;->a(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final previousIndex()I
    .locals 1

    iget v0, p0, Lmaps/f/eg;->b:I

    add-int/lit8 v0, v0, -0x1

    return v0
.end method
