.class Lmaps/f/j;
.super Lmaps/f/az;

# interfaces
.implements Ljava/util/List;


# instance fields
.field final synthetic a:Lmaps/f/cd;


# direct methods
.method constructor <init>(Lmaps/f/cd;Ljava/lang/Object;Ljava/util/List;Lmaps/f/az;)V
    .locals 0

    iput-object p1, p0, Lmaps/f/j;->a:Lmaps/f/cd;

    invoke-direct {p0, p1, p2, p3, p4}, Lmaps/f/az;-><init>(Lmaps/f/cd;Ljava/lang/Object;Ljava/util/Collection;Lmaps/f/az;)V

    return-void
.end method


# virtual methods
.method a()Ljava/util/List;
    .locals 1

    invoke-virtual {p0}, Lmaps/f/j;->f()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public add(ILjava/lang/Object;)V
    .locals 2

    invoke-virtual {p0}, Lmaps/f/j;->b()V

    invoke-virtual {p0}, Lmaps/f/j;->f()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    invoke-virtual {p0}, Lmaps/f/j;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iget-object v1, p0, Lmaps/f/j;->a:Lmaps/f/cd;

    invoke-static {v1}, Lmaps/f/cd;->c(Lmaps/f/cd;)I

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmaps/f/j;->e()V

    :cond_0
    return-void
.end method

.method public addAll(ILjava/util/Collection;)Z
    .locals 4

    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lmaps/f/j;->size()I

    move-result v1

    invoke-virtual {p0}, Lmaps/f/j;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmaps/f/j;->f()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v2

    iget-object v3, p0, Lmaps/f/j;->a:Lmaps/f/cd;

    sub-int/2addr v2, v1

    invoke-static {v3, v2}, Lmaps/f/cd;->a(Lmaps/f/cd;I)I

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lmaps/f/j;->e()V

    goto :goto_0
.end method

.method public get(I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lmaps/f/j;->b()V

    invoke-virtual {p0}, Lmaps/f/j;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public indexOf(Ljava/lang/Object;)I
    .locals 1

    invoke-virtual {p0}, Lmaps/f/j;->b()V

    invoke-virtual {p0}, Lmaps/f/j;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public lastIndexOf(Ljava/lang/Object;)I
    .locals 1

    invoke-virtual {p0}, Lmaps/f/j;->b()V

    invoke-virtual {p0}, Lmaps/f/j;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->lastIndexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public listIterator()Ljava/util/ListIterator;
    .locals 1

    invoke-virtual {p0}, Lmaps/f/j;->b()V

    new-instance v0, Lmaps/f/b;

    invoke-direct {v0, p0}, Lmaps/f/b;-><init>(Lmaps/f/j;)V

    return-object v0
.end method

.method public listIterator(I)Ljava/util/ListIterator;
    .locals 1

    invoke-virtual {p0}, Lmaps/f/j;->b()V

    new-instance v0, Lmaps/f/b;

    invoke-direct {v0, p0, p1}, Lmaps/f/b;-><init>(Lmaps/f/j;I)V

    return-object v0
.end method

.method public remove(I)Ljava/lang/Object;
    .locals 2

    invoke-virtual {p0}, Lmaps/f/j;->b()V

    invoke-virtual {p0}, Lmaps/f/j;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lmaps/f/j;->a:Lmaps/f/cd;

    invoke-static {v1}, Lmaps/f/cd;->b(Lmaps/f/cd;)I

    invoke-virtual {p0}, Lmaps/f/j;->c()V

    return-object v0
.end method

.method public set(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lmaps/f/j;->b()V

    invoke-virtual {p0}, Lmaps/f/j;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public subList(II)Ljava/util/List;
    .locals 4

    invoke-virtual {p0}, Lmaps/f/j;->b()V

    iget-object v0, p0, Lmaps/f/j;->a:Lmaps/f/cd;

    invoke-virtual {p0}, Lmaps/f/j;->d()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0}, Lmaps/f/j;->a()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, p1, p2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    invoke-virtual {p0}, Lmaps/f/j;->g()Lmaps/f/az;

    move-result-object v3

    if-nez v3, :cond_0

    :goto_0
    invoke-static {v0, v1, v2, p0}, Lmaps/f/cd;->a(Lmaps/f/cd;Ljava/lang/Object;Ljava/util/List;Lmaps/f/az;)Ljava/util/List;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-virtual {p0}, Lmaps/f/j;->g()Lmaps/f/az;

    move-result-object p0

    goto :goto_0
.end method
