.class public abstract enum Lmaps/f/s;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lmaps/f/s;

.field public static final enum b:Lmaps/f/s;

.field public static final enum c:Lmaps/f/s;

.field private static final synthetic d:[Lmaps/f/s;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lmaps/f/cv;

    const-string v1, "NEXT_LOWER"

    invoke-direct {v0, v1, v2}, Lmaps/f/cv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/f/s;->a:Lmaps/f/s;

    new-instance v0, Lmaps/f/cw;

    const-string v1, "NEXT_HIGHER"

    invoke-direct {v0, v1, v3}, Lmaps/f/cw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/f/s;->b:Lmaps/f/s;

    new-instance v0, Lmaps/f/cu;

    const-string v1, "INVERTED_INSERTION_INDEX"

    invoke-direct {v0, v1, v4}, Lmaps/f/cu;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/f/s;->c:Lmaps/f/s;

    const/4 v0, 0x3

    new-array v0, v0, [Lmaps/f/s;

    sget-object v1, Lmaps/f/s;->a:Lmaps/f/s;

    aput-object v1, v0, v2

    sget-object v1, Lmaps/f/s;->b:Lmaps/f/s;

    aput-object v1, v0, v3

    sget-object v1, Lmaps/f/s;->c:Lmaps/f/s;

    aput-object v1, v0, v4

    sput-object v0, Lmaps/f/s;->d:[Lmaps/f/s;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILmaps/f/q;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmaps/f/s;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/f/s;
    .locals 1

    const-class v0, Lmaps/f/s;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/f/s;

    return-object v0
.end method

.method public static values()[Lmaps/f/s;
    .locals 1

    sget-object v0, Lmaps/f/s;->d:[Lmaps/f/s;

    invoke-virtual {v0}, [Lmaps/f/s;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/f/s;

    return-object v0
.end method


# virtual methods
.method abstract a(I)I
.end method
