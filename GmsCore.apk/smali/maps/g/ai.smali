.class public Lmaps/g/ai;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lmaps/bb/d;

.field public static final b:Lmaps/bb/d;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    const/4 v9, 0x2

    const/4 v8, 0x1

    const-wide/16 v6, -0x1

    const/16 v5, 0x215

    const/4 v1, 0x0

    new-instance v0, Lmaps/bb/d;

    invoke-direct {v0}, Lmaps/bb/d;-><init>()V

    sput-object v0, Lmaps/g/ai;->a:Lmaps/bb/d;

    new-instance v0, Lmaps/bb/d;

    invoke-direct {v0}, Lmaps/bb/d;-><init>()V

    sput-object v0, Lmaps/g/ai;->b:Lmaps/bb/d;

    sget-object v2, Lmaps/g/ai;->a:Lmaps/bb/d;

    move-object v0, v1

    check-cast v0, Lmaps/g/ba;

    invoke-static {v6, v7}, Lmaps/ah/c;->a(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v5, v8, v0}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/g/ba;

    invoke-static {v6, v7}, Lmaps/ah/c;->a(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v5, v9, v0}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/g/ba;

    const/4 v0, 0x3

    invoke-static {v6, v7}, Lmaps/ah/c;->a(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v5, v0, v3}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/g/ba;

    const/4 v0, 0x4

    invoke-static {v6, v7}, Lmaps/ah/c;->a(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v5, v0, v3}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/g/ba;

    const/4 v0, 0x5

    const-wide/16 v3, -0x270f

    invoke-static {v3, v4}, Lmaps/ah/c;->a(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v5, v0, v3}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/g/ba;

    const/4 v0, 0x6

    const-wide/16 v3, 0x0

    invoke-static {v3, v4}, Lmaps/ah/c;->a(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v5, v0, v3}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/g/ba;

    const/4 v0, 0x7

    invoke-static {v6, v7}, Lmaps/ah/c;->a(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v5, v0, v3}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/g/ba;

    const/16 v0, 0x8

    invoke-static {v6, v7}, Lmaps/ah/c;->a(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v5, v0, v3}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    const/16 v3, 0x21b

    move-object v0, v1

    check-cast v0, Lmaps/g/ba;

    const/16 v0, 0x9

    sget-object v4, Lmaps/g/bl;->a:Lmaps/bb/d;

    invoke-virtual {v2, v3, v0, v4}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    const/16 v3, 0x21e

    move-object v0, v1

    check-cast v0, Lmaps/g/ba;

    const/16 v0, 0xa

    invoke-virtual {v2, v3, v0, v1}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/g/ba;

    const/16 v0, 0xb

    invoke-virtual {v2, v5, v0, v1}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/g/ba;

    const/16 v0, 0xc

    invoke-virtual {v2, v5, v0, v1}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    sget-object v2, Lmaps/g/ai;->b:Lmaps/bb/d;

    const/16 v3, 0x11b

    move-object v0, v1

    check-cast v0, Lmaps/g/as;

    sget-object v0, Lmaps/g/ai;->a:Lmaps/bb/d;

    invoke-virtual {v2, v3, v8, v0}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    const/16 v3, 0x113

    move-object v0, v1

    check-cast v0, Lmaps/g/as;

    invoke-virtual {v2, v3, v9, v1}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    const/16 v3, 0x41b

    move-object v0, v1

    check-cast v0, Lmaps/g/as;

    const/4 v0, 0x3

    sget-object v4, Lmaps/g/ai;->a:Lmaps/bb/d;

    invoke-virtual {v2, v3, v0, v4}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    const/16 v3, 0x41b

    move-object v0, v1

    check-cast v0, Lmaps/g/as;

    const/4 v0, 0x4

    sget-object v4, Lmaps/g/ai;->a:Lmaps/bb/d;

    invoke-virtual {v2, v3, v0, v4}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v0

    check-cast v1, Lmaps/g/as;

    const/4 v1, 0x5

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Lmaps/ah/c;->a(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v5, v1, v2}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
