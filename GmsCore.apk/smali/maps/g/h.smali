.class public Lmaps/g/h;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lmaps/bb/d;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    const-wide/16 v10, 0x0

    const-wide/32 v8, 0x15180

    const/16 v7, 0x218

    const/16 v6, 0x215

    const/4 v1, 0x0

    new-instance v0, Lmaps/bb/d;

    invoke-direct {v0}, Lmaps/bb/d;-><init>()V

    sput-object v0, Lmaps/g/h;->a:Lmaps/bb/d;

    sget-object v2, Lmaps/g/h;->a:Lmaps/bb/d;

    move-object v0, v1

    check-cast v0, Lmaps/g/ag;

    const/4 v0, 0x1

    invoke-static {v10, v11}, Lmaps/ah/c;->a(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v6, v0, v3}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/g/ag;

    const/4 v0, 0x2

    sget-object v3, Lmaps/bb/c;->a:Ljava/lang/Boolean;

    invoke-virtual {v2, v7, v0, v3}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    const/16 v3, 0x214

    move-object v0, v1

    check-cast v0, Lmaps/g/ag;

    const/4 v0, 0x3

    const-wide/32 v4, 0x93a80

    invoke-static {v4, v5}, Lmaps/ah/c;->a(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v0, v4}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/g/ag;

    const/4 v0, 0x4

    invoke-static {v8, v9}, Lmaps/ah/c;->a(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v6, v0, v3}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/g/ag;

    const/4 v0, 0x5

    sget-object v3, Lmaps/bb/c;->a:Ljava/lang/Boolean;

    invoke-virtual {v2, v7, v0, v3}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/g/ag;

    const/4 v0, 0x6

    sget-object v3, Lmaps/bb/c;->a:Ljava/lang/Boolean;

    invoke-virtual {v2, v7, v0, v3}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/g/ag;

    const/4 v0, 0x7

    sget-object v3, Lmaps/bb/c;->a:Ljava/lang/Boolean;

    invoke-virtual {v2, v7, v0, v3}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/g/ag;

    const/16 v0, 0x8

    sget-object v3, Lmaps/bb/c;->a:Ljava/lang/Boolean;

    invoke-virtual {v2, v7, v0, v3}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/g/ag;

    const/16 v0, 0xb

    invoke-static {v10, v11}, Lmaps/ah/c;->a(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v6, v0, v3}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/g/ag;

    const/16 v0, 0xc

    invoke-static {v10, v11}, Lmaps/ah/c;->a(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v6, v0, v3}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/g/ag;

    const/16 v0, 0xd

    const-wide/16 v3, 0x64

    invoke-static {v3, v4}, Lmaps/ah/c;->a(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v6, v0, v3}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/g/ag;

    const/16 v0, 0xe

    const-wide/16 v3, 0x12c

    invoke-static {v3, v4}, Lmaps/ah/c;->a(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v6, v0, v3}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/g/ag;

    const/16 v0, 0xf

    const-wide/32 v3, 0x2a300

    invoke-static {v3, v4}, Lmaps/ah/c;->a(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v6, v0, v3}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/g/ag;

    const/16 v0, 0x10

    const-wide/32 v3, 0x2a300

    invoke-static {v3, v4}, Lmaps/ah/c;->a(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v6, v0, v3}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/g/ag;

    const/16 v0, 0x11

    const-wide/16 v3, 0x258

    invoke-static {v3, v4}, Lmaps/ah/c;->a(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v6, v0, v3}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/g/ag;

    const/16 v0, 0x12

    const-wide/16 v3, 0xb4

    invoke-static {v3, v4}, Lmaps/ah/c;->a(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v6, v0, v3}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/g/ag;

    const/16 v0, 0x15

    const-wide/16 v3, 0x258

    invoke-static {v3, v4}, Lmaps/ah/c;->a(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v6, v0, v3}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/g/ag;

    const/16 v0, 0x16

    const-wide/32 v3, 0x12c00

    invoke-static {v3, v4}, Lmaps/ah/c;->a(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v6, v0, v3}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/g/ag;

    const/16 v0, 0x17

    const-wide/16 v3, 0x4ec

    invoke-static {v3, v4}, Lmaps/ah/c;->a(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v6, v0, v3}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/g/ag;

    const/16 v0, 0x18

    const-wide/16 v3, 0x78

    invoke-static {v3, v4}, Lmaps/ah/c;->a(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v6, v0, v3}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/g/ag;

    const/16 v0, 0x1f

    const-wide/16 v3, 0x258

    invoke-static {v3, v4}, Lmaps/ah/c;->a(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v6, v0, v3}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/g/ag;

    const/16 v0, 0x20

    invoke-static {v8, v9}, Lmaps/ah/c;->a(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v6, v0, v3}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/g/ag;

    const/16 v0, 0x22

    const-wide/16 v3, 0x3200

    invoke-static {v3, v4}, Lmaps/ah/c;->a(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v6, v0, v3}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/g/ag;

    const/16 v0, 0x23

    const-wide/16 v3, 0x3840

    invoke-static {v3, v4}, Lmaps/ah/c;->a(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v6, v0, v3}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/g/ag;

    const/16 v0, 0x24

    const-wide/16 v3, 0x4ec

    invoke-static {v3, v4}, Lmaps/ah/c;->a(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v6, v0, v3}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/g/ag;

    const/16 v0, 0x25

    invoke-static {v8, v9}, Lmaps/ah/c;->a(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v6, v0, v3}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/g/ag;

    const/16 v0, 0x26

    const-wide/16 v3, 0x78

    invoke-static {v3, v4}, Lmaps/ah/c;->a(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v6, v0, v3}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v0

    check-cast v1, Lmaps/g/ag;

    const/16 v1, 0x27

    invoke-static {v8, v9}, Lmaps/ah/c;->a(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v6, v1, v2}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
