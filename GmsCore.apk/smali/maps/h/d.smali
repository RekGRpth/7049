.class public Lmaps/h/d;
.super Lmaps/e/u;

# interfaces
.implements Landroid/location/LocationListener;


# static fields
.field private static c:Lmaps/h/e;


# instance fields
.field private j:Z


# direct methods
.method public constructor <init>(Lmaps/ae/d;Landroid/location/LocationManager;Lmaps/e/ab;Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lmaps/e/u;-><init>(Lmaps/ae/d;Landroid/location/LocationManager;Lmaps/e/ab;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/h/d;->j:Z

    new-instance v0, Lmaps/h/e;

    invoke-direct {v0, p0, p4}, Lmaps/h/e;-><init>(Lmaps/h/d;Landroid/content/Context;)V

    sput-object v0, Lmaps/h/d;->c:Lmaps/h/e;

    return-void
.end method


# virtual methods
.method protected b(Landroid/location/Location;)V
    .locals 0

    invoke-virtual {p0, p1}, Lmaps/h/d;->c(Landroid/location/Location;)V

    return-void
.end method

.method protected declared-synchronized g()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Lmaps/e/u;->g()V

    iget-boolean v0, p0, Lmaps/h/d;->j:Z

    if-eqz v0, :cond_0

    sget-object v0, Lmaps/h/d;->c:Lmaps/h/e;

    invoke-virtual {v0}, Lmaps/h/e;->b()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/h/d;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized h()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/h/d;->e:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lmaps/h/d;->j:Z

    if-nez v0, :cond_0

    sget-object v0, Lmaps/h/d;->c:Lmaps/h/e;

    invoke-virtual {v0}, Lmaps/h/e;->a()V

    invoke-super {p0}, Lmaps/e/u;->h()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/h/d;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    invoke-super {p0}, Lmaps/e/u;->h()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 2

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lmaps/h/d;->e:Z

    if-nez v0, :cond_1

    invoke-static {p1}, Lmaps/h/d;->a(Landroid/location/Location;)Lmaps/t/bb;

    move-result-object v0

    sget-object v1, Lmaps/h/d;->c:Lmaps/h/e;

    invoke-virtual {v1, p1, v0}, Lmaps/h/e;->a(Landroid/location/Location;Lmaps/t/bb;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1}, Lmaps/h/d;->b(Landroid/location/Location;)V

    goto :goto_0
.end method
