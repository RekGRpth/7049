.class public Lmaps/h/f;
.super Ljava/lang/Object;


# instance fields
.field private a:Lmaps/ba/b;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public declared-synchronized a(J)Lmaps/bw/a;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/h/f;->a:Lmaps/ba/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/h/f;->a:Lmaps/ba/b;

    invoke-virtual {v0, p1, p2}, Lmaps/ba/b;->b(J)Lmaps/bw/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0}, Lmaps/h/f;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(JFFF)V
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/h/f;->a:Lmaps/ba/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/h/f;->a:Lmaps/ba/b;

    move-wide v1, p1

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lmaps/ba/b;->a(JFFF)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(JLmaps/bw/a;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/h/f;->a:Lmaps/ba/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/h/f;->a:Lmaps/ba/b;

    invoke-virtual {v0, p1, p2, p3}, Lmaps/ba/b;->a(JLmaps/bw/a;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Z)V
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/h/f;->a:Lmaps/ba/b;

    if-nez v0, :cond_0

    if-eqz p1, :cond_1

    new-instance v0, Lmaps/ba/b;

    const v1, 0x3dcccccd

    const/16 v2, 0x1770

    const/high16 v3, 0x3f400000

    const/16 v4, 0x12c

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lmaps/ba/b;-><init>(FIFIZ)V

    iput-object v0, p0, Lmaps/h/f;->a:Lmaps/ba/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    new-instance v0, Lmaps/ba/b;

    invoke-direct {v0}, Lmaps/ba/b;-><init>()V

    iput-object v0, p0, Lmaps/h/f;->a:Lmaps/ba/b;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lmaps/h/f;->a:Lmaps/ba/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
