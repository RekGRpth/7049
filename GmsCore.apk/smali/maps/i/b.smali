.class public final Lmaps/i/b;
.super Lmaps/y/v;

# interfaces
.implements Lmaps/i/h;


# instance fields
.field private b:Lmaps/i/l;

.field private c:Lcom/google/android/gms/maps/internal/IOnMapClickListener;

.field private d:Lcom/google/android/gms/maps/internal/IOnMapLongClickListener;

.field private e:Lmaps/y/bm;

.field private f:Lmaps/au/b;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmaps/y/v;-><init>(Landroid/content/Context;Landroid/content/res/Resources;)V

    invoke-direct {p0}, Lmaps/i/b;->s()V

    return-void
.end method

.method static synthetic a(Lmaps/i/b;)Lcom/google/android/gms/maps/internal/IOnMapLongClickListener;
    .locals 1

    iget-object v0, p0, Lmaps/i/b;->d:Lcom/google/android/gms/maps/internal/IOnMapLongClickListener;

    return-object v0
.end method

.method static synthetic a(Lmaps/i/b;ZZ)V
    .locals 0

    invoke-super {p0, p1, p2}, Lmaps/y/v;->a(ZZ)V

    return-void
.end method

.method static synthetic b(Lmaps/i/b;)Lcom/google/android/gms/maps/internal/IOnMapClickListener;
    .locals 1

    iget-object v0, p0, Lmaps/i/b;->c:Lcom/google/android/gms/maps/internal/IOnMapClickListener;

    return-object v0
.end method

.method private s()V
    .locals 2

    new-instance v0, Lmaps/i/s;

    invoke-direct {v0, p0}, Lmaps/i/s;-><init>(Lmaps/i/b;)V

    iput-object v0, p0, Lmaps/i/b;->e:Lmaps/y/bm;

    new-instance v0, Lmaps/au/b;

    new-instance v1, Lmaps/i/r;

    invoke-direct {v1, p0}, Lmaps/i/r;-><init>(Lmaps/i/b;)V

    invoke-direct {v0, v1}, Lmaps/au/b;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lmaps/i/b;->f:Lmaps/au/b;

    return-void
.end method

.method private t()V
    .locals 1

    iget-object v0, p0, Lmaps/i/b;->c:Lcom/google/android/gms/maps/internal/IOnMapClickListener;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/i/b;->d:Lcom/google/android/gms/maps/internal/IOnMapLongClickListener;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lmaps/i/b;->a(Lmaps/y/bm;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lmaps/i/b;->e:Lmaps/y/bm;

    invoke-virtual {p0, v0}, Lmaps/i/b;->a(Lmaps/y/bm;)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lmaps/i/b;->f:Lmaps/au/b;

    invoke-virtual {v0}, Lmaps/au/b;->a()V

    return-void
.end method

.method public a(Lcom/google/android/gms/maps/internal/IOnMapClickListener;)V
    .locals 0

    iput-object p1, p0, Lmaps/i/b;->c:Lcom/google/android/gms/maps/internal/IOnMapClickListener;

    invoke-direct {p0}, Lmaps/i/b;->t()V

    return-void
.end method

.method public a(Lcom/google/android/gms/maps/internal/IOnMapLongClickListener;)V
    .locals 0

    iput-object p1, p0, Lmaps/i/b;->d:Lcom/google/android/gms/maps/internal/IOnMapLongClickListener;

    invoke-direct {p0}, Lmaps/i/b;->t()V

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1

    invoke-super {p0, p1, p2, p3, p4}, Lmaps/y/v;->onSizeChanged(IIII)V

    iget-object v0, p0, Lmaps/i/b;->b:Lmaps/i/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/i/b;->b:Lmaps/i/l;

    invoke-interface {v0, p1, p2, p3, p4}, Lmaps/i/l;->a(IIII)V

    :cond_0
    return-void
.end method
