.class public final Lmaps/i/d;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/i/c;


# instance fields
.field private final a:Lmaps/y/q;


# direct methods
.method public constructor <init>(Lmaps/y/q;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/i/d;->a:Lmaps/y/q;

    return-void
.end method


# virtual methods
.method public a(FFFI)F
    .locals 1

    iget-object v0, p0, Lmaps/i/d;->a:Lmaps/y/q;

    invoke-virtual {v0, p1, p2, p3, p4}, Lmaps/y/q;->a(FFFI)F

    move-result v0

    return v0
.end method

.method public a(FI)F
    .locals 1

    iget-object v0, p0, Lmaps/i/d;->a:Lmaps/y/q;

    invoke-virtual {v0, p1, p2}, Lmaps/y/q;->b(FI)F

    move-result v0

    return v0
.end method

.method public a(Lmaps/t/bx;)F
    .locals 1

    iget-object v0, p0, Lmaps/i/d;->a:Lmaps/y/q;

    invoke-virtual {v0, p1}, Lmaps/y/q;->a(Lmaps/t/bx;)F

    move-result v0

    return v0
.end method

.method public a()Lmaps/bq/a;
    .locals 1

    iget-object v0, p0, Lmaps/i/d;->a:Lmaps/y/q;

    invoke-virtual {v0}, Lmaps/y/q;->b()Lmaps/bq/a;

    move-result-object v0

    return-object v0
.end method

.method public a(Lmaps/bq/a;Lmaps/bq/d;FF)Lmaps/bq/a;
    .locals 1

    invoke-static {p1, p2, p3, p4}, Lmaps/y/q;->a(Lmaps/bq/a;Lmaps/bq/d;FF)Lmaps/bq/a;

    move-result-object v0

    return-object v0
.end method

.method public a(F)V
    .locals 1

    iget-object v0, p0, Lmaps/i/d;->a:Lmaps/y/q;

    invoke-virtual {v0, p1}, Lmaps/y/q;->a(F)V

    return-void
.end method

.method public a(FF)V
    .locals 1

    iget-object v0, p0, Lmaps/i/d;->a:Lmaps/y/q;

    invoke-virtual {v0, p1, p2}, Lmaps/y/q;->a(FF)V

    return-void
.end method

.method public a(Lmaps/af/o;)V
    .locals 1

    iget-object v0, p0, Lmaps/i/d;->a:Lmaps/y/q;

    invoke-virtual {v0, p1}, Lmaps/y/q;->a(Lmaps/af/o;)V

    return-void
.end method

.method public a(Lmaps/bq/b;II)V
    .locals 1

    iget-object v0, p0, Lmaps/i/d;->a:Lmaps/y/q;

    invoke-virtual {v0, p1, p2, p3}, Lmaps/y/q;->a(Lmaps/bq/b;II)V

    return-void
.end method

.method public a(Lmaps/t/bx;I)V
    .locals 1

    iget-object v0, p0, Lmaps/i/d;->a:Lmaps/y/q;

    invoke-virtual {v0, p1, p2}, Lmaps/y/q;->a(Lmaps/t/bx;I)V

    return-void
.end method

.method public a(Lmaps/y/ar;)V
    .locals 1

    iget-object v0, p0, Lmaps/i/d;->a:Lmaps/y/q;

    invoke-virtual {v0, p1}, Lmaps/y/q;->a(Lmaps/y/ar;)V

    return-void
.end method

.method public b()F
    .locals 1

    iget-object v0, p0, Lmaps/i/d;->a:Lmaps/y/q;

    invoke-virtual {v0}, Lmaps/y/q;->a()F

    move-result v0

    return v0
.end method

.method public b(FI)F
    .locals 1

    iget-object v0, p0, Lmaps/i/d;->a:Lmaps/y/q;

    invoke-virtual {v0, p1, p2}, Lmaps/y/q;->a(FI)F

    move-result v0

    return v0
.end method

.method public c()F
    .locals 1

    iget-object v0, p0, Lmaps/i/d;->a:Lmaps/y/q;

    invoke-virtual {v0}, Lmaps/y/q;->c()F

    move-result v0

    return v0
.end method
