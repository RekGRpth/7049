.class public final Lmaps/i/t;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/i/m;


# static fields
.field private static a:J

.field private static final b:Ljava/util/concurrent/atomic/AtomicLong;


# instance fields
.field private final c:Lmaps/b/r;

.field private final d:J

.field private final e:Lmaps/b/y;

.field private volatile f:Lmaps/ao/c;

.field private final g:Ljava/util/concurrent/Executor;

.field private final h:Lmaps/bf/b;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-wide/16 v0, -0x1

    sput-wide v0, Lmaps/i/t;->a:J

    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v1, 0x0

    invoke-direct {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    sput-object v0, Lmaps/i/t;->b:Ljava/util/concurrent/atomic/AtomicLong;

    return-void
.end method

.method private constructor <init>(Lmaps/b/r;Ljava/util/concurrent/Executor;Lmaps/bf/b;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lmaps/i/t;->b:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->getAndIncrement()J

    move-result-wide v0

    iput-wide v0, p0, Lmaps/i/t;->d:J

    new-instance v0, Lmaps/i/i;

    invoke-direct {v0, p0}, Lmaps/i/i;-><init>(Lmaps/i/t;)V

    iput-object v0, p0, Lmaps/i/t;->e:Lmaps/b/y;

    invoke-static {p1}, Lmaps/ap/q;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/b/r;

    iput-object v0, p0, Lmaps/i/t;->c:Lmaps/b/r;

    invoke-static {p2}, Lmaps/ap/q;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lmaps/i/t;->g:Ljava/util/concurrent/Executor;

    invoke-static {p3}, Lmaps/ap/q;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/bf/b;

    iput-object v0, p0, Lmaps/i/t;->h:Lmaps/bf/b;

    return-void
.end method

.method static synthetic a(Lmaps/i/t;)Lmaps/ao/c;
    .locals 1

    iget-object v0, p0, Lmaps/i/t;->f:Lmaps/ao/c;

    return-object v0
.end method

.method public static a(Lmaps/b/r;Ljava/util/concurrent/Executor;Lmaps/bf/b;)Lmaps/i/m;
    .locals 1

    new-instance v0, Lmaps/i/t;

    invoke-direct {v0, p0, p1, p2}, Lmaps/i/t;-><init>(Lmaps/b/r;Ljava/util/concurrent/Executor;Lmaps/bf/b;)V

    invoke-direct {v0}, Lmaps/i/t;->f()V

    return-object v0
.end method

.method static synthetic b(Lmaps/i/t;)Ljava/util/concurrent/Executor;
    .locals 1

    iget-object v0, p0, Lmaps/i/t;->g:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method static synthetic c(Lmaps/i/t;)Lmaps/bf/b;
    .locals 1

    iget-object v0, p0, Lmaps/i/t;->h:Lmaps/bf/b;

    return-object v0
.end method

.method private f()V
    .locals 2

    iget-object v0, p0, Lmaps/i/t;->c:Lmaps/b/r;

    iget-object v1, p0, Lmaps/i/t;->e:Lmaps/b/y;

    invoke-virtual {v0, v1}, Lmaps/b/r;->a(Lmaps/b/y;)V

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 4

    sget-wide v0, Lmaps/i/t;->a:J

    iget-wide v2, p0, Lmaps/i/t;->d:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    sget-wide v0, Lmaps/i/t;->a:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    :cond_0
    iget-wide v0, p0, Lmaps/i/t;->d:J

    sput-wide v0, Lmaps/i/t;->a:J

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 4

    sget-wide v0, Lmaps/i/t;->a:J

    iget-wide v2, p0, Lmaps/i/t;->d:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const-wide/16 v0, -0x1

    sput-wide v0, Lmaps/i/t;->a:J

    :cond_0
    return-void
.end method

.method public c()Z
    .locals 4

    iget-wide v0, p0, Lmaps/i/t;->d:J

    sget-wide v2, Lmaps/i/t;->a:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Lmaps/b/r;
    .locals 1

    iget-object v0, p0, Lmaps/i/t;->c:Lmaps/b/r;

    return-object v0
.end method

.method public e()Z
    .locals 1

    invoke-virtual {p0}, Lmaps/i/t;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/i/t;->c:Lmaps/b/r;

    invoke-virtual {v0}, Lmaps/b/r;->d()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
