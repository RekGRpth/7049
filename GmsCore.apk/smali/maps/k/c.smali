.class public Lmaps/k/c;
.super Ljava/lang/Object;


# static fields
.field private static volatile a:Z

.field private static final b:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lmaps/k/c;->a:Z

    invoke-static {}, Lmaps/f/cs;->a()Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lmaps/k/c;->b:Ljava/util/Map;

    invoke-static {}, Lmaps/k/c;->a()V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a()V
    .locals 6

    invoke-static {}, Lmaps/k/g;->values()[Lmaps/k/g;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    sget-object v4, Lmaps/k/c;->b:Ljava/util/Map;

    new-instance v5, Ljava/util/LinkedList;

    invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static a(Lmaps/k/g;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-static {p0}, Lmaps/k/g;->a(Lmaps/k/g;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lmaps/k/c;->b:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    sget-object v0, Lmaps/k/c;->b:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(Lmaps/k/g;Lmaps/ai/d;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    sget-object v0, Lmaps/k/c;->b:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_0

    monitor-exit p0

    :goto_0
    return-void

    :cond_0
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1, v0}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    invoke-static {p0}, Lmaps/k/c;->a(Lmaps/k/g;)V

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Lmaps/ai/a;

    new-instance v2, Lmaps/k/h;

    invoke-direct {v2, v1, p0}, Lmaps/k/h;-><init>(Ljava/util/List;Lmaps/k/g;)V

    invoke-direct {v0, p1, v2}, Lmaps/ai/a;-><init>(Lmaps/ai/d;Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Lmaps/ai/a;->d()V

    :cond_1
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic a(Lmaps/k/g;Lmaps/k/i;)V
    .locals 0

    invoke-static {p0, p1}, Lmaps/k/c;->b(Lmaps/k/g;Lmaps/k/i;)V

    return-void
.end method

.method public static a(Lmaps/k/g;Lmaps/k/i;I)V
    .locals 3

    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lmaps/ae/h;->L:Z

    if-eqz v0, :cond_0

    sget-object v0, Lmaps/k/g;->f:Lmaps/k/g;

    if-eq p0, v0, :cond_0

    sget-object v0, Lmaps/k/g;->g:Lmaps/k/g;

    if-eq p0, v0, :cond_0

    invoke-virtual {p1}, Lmaps/k/i;->run()V

    monitor-exit p0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lmaps/k/c;->b:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_2

    sget-boolean v0, Lmaps/ae/h;->i:Z

    if-eqz v0, :cond_1

    const-string v0, "MAPS"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Adding DeferredTask of type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " after all tasks of this type were executed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-static {p0, p1}, Lmaps/k/c;->b(Lmaps/k/g;Lmaps/k/i;)V

    invoke-virtual {p1}, Lmaps/k/i;->run()V

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    if-nez p2, :cond_3

    const/4 v1, 0x0

    :try_start_1
    invoke-interface {v0, v1, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :goto_1
    monitor-exit p0

    goto :goto_0

    :cond_3
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public static b()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lmaps/k/c;->a:Z

    return-void
.end method

.method private static b(Lmaps/k/g;Lmaps/k/i;)V
    .locals 3

    invoke-static {}, Lmaps/ae/h;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lmaps/ae/h;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-static {p1}, Lmaps/k/i;->a(Lmaps/k/i;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ae/c;->s()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Attempting to run a DeferredTask of type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " on a non-UI thread : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-void
.end method

.method public static c()V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Lmaps/k/c;->a:Z

    return-void
.end method

.method static synthetic d()Z
    .locals 1

    sget-boolean v0, Lmaps/k/c;->a:Z

    return v0
.end method
