.class public final enum Lmaps/k/e;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lmaps/k/e;

.field public static final enum b:Lmaps/k/e;

.field private static final synthetic c:[Lmaps/k/e;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lmaps/k/e;

    const-string v1, "ORIENTATION_PROVIDER_ACTIVITY_RESUME"

    invoke-direct {v0, v1, v2}, Lmaps/k/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/k/e;->a:Lmaps/k/e;

    new-instance v0, Lmaps/k/e;

    const-string v1, "START_MOTION_RECOGNIZER"

    invoke-direct {v0, v1, v3}, Lmaps/k/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/k/e;->b:Lmaps/k/e;

    const/4 v0, 0x2

    new-array v0, v0, [Lmaps/k/e;

    sget-object v1, Lmaps/k/e;->a:Lmaps/k/e;

    aput-object v1, v0, v2

    sget-object v1, Lmaps/k/e;->b:Lmaps/k/e;

    aput-object v1, v0, v3

    sput-object v0, Lmaps/k/e;->c:[Lmaps/k/e;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/k/e;
    .locals 1

    const-class v0, Lmaps/k/e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/k/e;

    return-object v0
.end method

.method public static values()[Lmaps/k/e;
    .locals 1

    sget-object v0, Lmaps/k/e;->c:[Lmaps/k/e;

    invoke-virtual {v0}, [Lmaps/k/e;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/k/e;

    return-object v0
.end method
