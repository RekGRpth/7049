.class public abstract Lmaps/k/i;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Z

.field private final b:Z

.field private final c:Lmaps/k/e;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, v0}, Lmaps/k/i;-><init>(ZZ)V

    return-void
.end method

.method public constructor <init>(ZZ)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lmaps/k/i;-><init>(ZZLmaps/k/e;)V

    return-void
.end method

.method public constructor <init>(ZZLmaps/k/e;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lmaps/k/i;->a:Z

    iput-boolean p2, p0, Lmaps/k/i;->b:Z

    iput-object p3, p0, Lmaps/k/i;->c:Lmaps/k/e;

    return-void
.end method

.method static synthetic a(Lmaps/k/i;)Z
    .locals 1

    iget-boolean v0, p0, Lmaps/k/i;->a:Z

    return v0
.end method


# virtual methods
.method public abstract a()V
.end method

.method public final run()V
    .locals 2

    iget-boolean v0, p0, Lmaps/k/i;->b:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/k/c;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lmaps/k/i;->c:Lmaps/k/e;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lmaps/k/i;->c:Lmaps/k/e;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Lmaps/k/i;->a()V

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    invoke-virtual {p0}, Lmaps/k/i;->a()V

    goto :goto_0
.end method
