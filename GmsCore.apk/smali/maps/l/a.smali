.class public Lmaps/l/a;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/q/x;


# instance fields
.field private a:Z

.field private b:Ljava/util/List;

.field private c:Ljava/util/List;

.field private d:Z

.field private volatile e:I

.field private f:F

.field private final g:Ljava/lang/Object;

.field private final h:Lmaps/s/d;

.field private final i:Lmaps/s/d;

.field private final j:Lmaps/q/ad;

.field private k:Lmaps/q/i;


# direct methods
.method public constructor <init>(Lmaps/s/d;Lmaps/s/d;Lmaps/cr/c;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, Lmaps/l/a;->a:Z

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/a;->b:Ljava/util/List;

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/a;->c:Ljava/util/List;

    iput-boolean v1, p0, Lmaps/l/a;->d:Z

    iput v1, p0, Lmaps/l/a;->e:I

    const/high16 v0, 0x3f800000

    iput v0, p0, Lmaps/l/a;->f:F

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lmaps/l/a;->g:Ljava/lang/Object;

    iput-object p1, p0, Lmaps/l/a;->h:Lmaps/s/d;

    iput-object p2, p0, Lmaps/l/a;->i:Lmaps/s/d;

    invoke-virtual {p3}, Lmaps/cr/c;->m()Lmaps/q/ad;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/a;->j:Lmaps/q/ad;

    return-void
.end method

.method private b(IF)F
    .locals 3

    const/high16 v0, 0x3f800000

    int-to-float v1, p1

    sub-float v1, p2, v1

    const/high16 v2, 0x40800000

    cmpl-float v2, v1, v2

    if-ltz v2, :cond_1

    const v0, 0x3ed55555

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/high16 v2, 0x40400000

    cmpl-float v2, v1, v2

    if-ltz v2, :cond_2

    const/high16 v0, 0x3f000000

    goto :goto_0

    :cond_2
    const/high16 v2, 0x40100000

    cmpl-float v2, v1, v2

    if-ltz v2, :cond_3

    const v0, 0x3f155555

    goto :goto_0

    :cond_3
    cmpl-float v1, v1, v0

    if-ltz v1, :cond_0

    const/16 v1, 0x11

    if-lt p1, v1, :cond_0

    const v0, 0x3f2aaaab

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public a(IF)V
    .locals 2

    iget-object v1, p0, Lmaps/l/a;->g:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lmaps/l/a;->d:Z

    invoke-direct {p0, p1, p2}, Lmaps/l/a;->b(IF)F

    move-result v0

    iput v0, p0, Lmaps/l/a;->f:F

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/l/a;->k:Lmaps/q/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/l/a;->k:Lmaps/q/i;

    sget-object v1, Lmaps/q/ay;->b:Lmaps/q/ay;

    invoke-interface {v0, p0, v1}, Lmaps/q/i;->a(Lmaps/q/x;Lmaps/q/c;)V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Ljava/lang/Iterable;)V
    .locals 4

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lmaps/l/a;->c:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/q/aa;

    iget-object v3, p0, Lmaps/l/a;->c:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lmaps/l/a;->j:Lmaps/q/ad;

    invoke-virtual {v3, v0}, Lmaps/q/ad;->a(Lmaps/q/ba;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lmaps/l/a;->k:Lmaps/q/i;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/l/a;->k:Lmaps/q/i;

    sget-object v2, Lmaps/q/ay;->b:Lmaps/q/ay;

    invoke-interface {v0, p0, v2}, Lmaps/q/i;->a(Lmaps/q/x;Lmaps/q/c;)V

    :cond_2
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public a(Lmaps/q/i;)V
    .locals 1

    iput-object p1, p0, Lmaps/l/a;->k:Lmaps/q/i;

    sget-object v0, Lmaps/q/ay;->b:Lmaps/q/ay;

    invoke-interface {p1, p0, v0}, Lmaps/q/i;->a(Lmaps/q/x;Lmaps/q/c;)V

    return-void
.end method

.method public a(Z)V
    .locals 2

    if-eqz p1, :cond_1

    iget-object v0, p0, Lmaps/l/a;->j:Lmaps/q/ad;

    invoke-virtual {v0, p0}, Lmaps/q/ad;->a(Lmaps/q/x;)V

    const/4 v0, 0x1

    iput v0, p0, Lmaps/l/a;->e:I

    :goto_0
    iget-object v0, p0, Lmaps/l/a;->k:Lmaps/q/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/l/a;->k:Lmaps/q/i;

    sget-object v1, Lmaps/q/ay;->b:Lmaps/q/ay;

    invoke-interface {v0, p0, v1}, Lmaps/q/i;->a(Lmaps/q/x;Lmaps/q/c;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x2

    iput v0, p0, Lmaps/l/a;->e:I

    goto :goto_0
.end method

.method public b(Lmaps/q/i;)V
    .locals 7

    const/16 v2, 0xff

    const/4 v3, 0x0

    iget-object v4, p0, Lmaps/l/a;->c:Ljava/util/List;

    monitor-enter v4

    :try_start_0
    iget-object v0, p0, Lmaps/l/a;->b:Ljava/util/List;

    iget-object v1, p0, Lmaps/l/a;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v5, p0, Lmaps/l/a;->g:Ljava/lang/Object;

    monitor-enter v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-boolean v0, p0, Lmaps/l/a;->d:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/l/a;->h:Lmaps/s/d;

    iget v1, p0, Lmaps/l/a;->f:F

    invoke-virtual {v0, v1}, Lmaps/s/d;->a(F)V

    iget-object v0, p0, Lmaps/l/a;->i:Lmaps/s/d;

    iget v1, p0, Lmaps/l/a;->f:F

    invoke-virtual {v0, v1}, Lmaps/s/d;->a(F)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/l/a;->d:Z

    :cond_0
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v0, p0, Lmaps/l/a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    iget v0, p0, Lmaps/l/a;->e:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :cond_1
    :try_start_3
    iget-object v0, p0, Lmaps/l/a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/q/aa;

    iget-boolean v1, p0, Lmaps/l/a;->a:Z

    if-eqz v1, :cond_2

    move v1, v2

    :goto_2
    invoke-virtual {v0, v1}, Lmaps/q/aa;->b(I)V

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    :cond_2
    move v1, v3

    goto :goto_2

    :pswitch_1
    iget-object v0, p0, Lmaps/l/a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/q/aa;

    invoke-virtual {v0, v2}, Lmaps/q/aa;->b(I)V

    goto :goto_3

    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/l/a;->a:Z

    iput v3, p0, Lmaps/l/a;->e:I

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lmaps/l/a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/q/aa;

    invoke-virtual {v0, v3}, Lmaps/q/aa;->b(I)V

    goto :goto_4

    :cond_4
    iput-boolean v3, p0, Lmaps/l/a;->a:Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
