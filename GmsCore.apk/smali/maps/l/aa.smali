.class public Lmaps/l/aa;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/l/t;


# static fields
.field static final a:Lmaps/l/aa;

.field private static b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lmaps/l/aa;

    invoke-direct {v0}, Lmaps/l/aa;-><init>()V

    sput-object v0, Lmaps/l/aa;->a:Lmaps/l/aa;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lmaps/cr/c;Lmaps/af/s;)V
    .locals 3

    invoke-virtual {p0}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-interface {p1}, Lmaps/af/s;->c()Lmaps/p/av;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/p/av;->a()Lmaps/y/bc;

    move-result-object v0

    check-cast v0, Lmaps/y/ab;

    invoke-virtual {v0}, Lmaps/y/ab;->o()Lmaps/o/c;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/o/c;->h()Z

    move-result v2

    sput-boolean v2, Lmaps/l/aa;->b:Z

    sget-boolean v2, Lmaps/l/aa;->b:Z

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1}, Lmaps/o/c;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    const/high16 v0, 0x40a00000

    invoke-interface {v1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glLineWidth(F)V

    iget-object v0, p0, Lmaps/cr/c;->j:Lmaps/al/o;

    invoke-virtual {v0, p0}, Lmaps/al/o;->d(Lmaps/cr/c;)V

    :cond_0
    return-void
.end method

.method public static a(Lmaps/cr/c;Lmaps/bq/d;)V
    .locals 4

    const/high16 v1, 0x10000

    invoke-virtual {p0}, Lmaps/cr/c;->D()V

    invoke-virtual {p0}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0, v1, v1, v1, v1}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    iget-object v0, p0, Lmaps/cr/c;->f:Lmaps/al/o;

    invoke-virtual {v0, p0}, Lmaps/al/o;->d(Lmaps/cr/c;)V

    invoke-virtual {p0}, Lmaps/cr/c;->q()V

    invoke-virtual {p0}, Lmaps/cr/c;->s()V

    invoke-virtual {p0}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/4 v1, 0x5

    const/4 v2, 0x0

    const/4 v3, 0x4

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    invoke-virtual {p0}, Lmaps/cr/c;->F()V

    return-void
.end method


# virtual methods
.method public a(Lmaps/cr/c;)V
    .locals 0

    return-void
.end method

.method public a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 4

    sget-boolean v0, Lmaps/l/aa;->b:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x4

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    :cond_0
    return-void
.end method

.method public c(Lmaps/cr/c;)V
    .locals 0

    return-void
.end method
