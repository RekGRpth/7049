.class public Lmaps/l/ag;
.super Lmaps/l/ak;


# static fields
.field private static final c:[F

.field private static final d:F


# instance fields
.field private A:Lmaps/w/v;

.field private final B:Lmaps/t/bx;

.field private final C:Ljava/util/ArrayList;

.field private final D:Ljava/util/ArrayList;

.field private E:Z

.field private F:F

.field a:F

.field private final e:Lmaps/t/ah;

.field private final f:[F

.field private g:Lmaps/s/r;

.field private final h:Lmaps/s/t;

.field private final i:Lmaps/s/t;

.field private final j:Lmaps/al/o;

.field private final k:Lmaps/al/q;

.field private final l:Lmaps/al/a;

.field private final m:Lmaps/al/a;

.field private final n:Lmaps/al/i;

.field private final o:Lmaps/al/a;

.field private final p:Lmaps/al/o;

.field private final q:Lmaps/al/q;

.field private final r:Lmaps/al/i;

.field private final s:Lmaps/al/a;

.field private final t:Lmaps/al/o;

.field private final u:Lmaps/s/g;

.field private final v:Lmaps/al/i;

.field private final w:Lmaps/al/i;

.field private final x:Lmaps/al/o;

.field private final y:Lmaps/al/q;

.field private final z:Lmaps/al/i;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x4

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    sput-object v0, Lmaps/l/ag;->c:[F

    const-wide/high16 v0, 0x3ff0000000000000L

    const-wide/high16 v2, 0x4000000000000000L

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    sput v0, Lmaps/l/ag;->d:F

    return-void

    nop

    :array_0
    .array-data 4
        0x3f37b7b8
        0x3f37b7b8
        0x3f65e5e6
        0x3f800000
    .end array-data
.end method

.method private constructor <init>(Lmaps/t/ah;Lmaps/l/bb;Ljava/util/HashSet;Lmaps/cr/c;I)V
    .locals 7

    const/4 v2, 0x0

    const/high16 v6, -0x40800000

    const/4 v5, 0x1

    const/high16 v4, 0x437f0000

    const/4 v3, 0x0

    invoke-direct {p0, p3}, Lmaps/l/ak;-><init>(Ljava/util/Set;)V

    new-instance v0, Lmaps/t/bx;

    invoke-direct {v0}, Lmaps/t/bx;-><init>()V

    iput-object v0, p0, Lmaps/l/ag;->B:Lmaps/t/bx;

    iput-boolean v2, p0, Lmaps/l/ag;->E:Z

    iput-object p1, p0, Lmaps/l/ag;->e:Lmaps/t/ah;

    const/4 v0, 0x4

    new-array v0, v0, [F

    invoke-static {p5}, Lmaps/s/k;->d(I)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v4

    aput v1, v0, v2

    invoke-static {p5}, Lmaps/s/k;->e(I)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v4

    aput v1, v0, v5

    const/4 v1, 0x2

    invoke-static {p5}, Lmaps/s/k;->f(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v4

    aput v2, v0, v1

    const/4 v1, 0x3

    invoke-static {p5}, Lmaps/s/k;->g(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v4

    aput v2, v0, v1

    iput-object v0, p0, Lmaps/l/ag;->f:[F

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    new-instance v0, Lmaps/s/t;

    iget v1, p2, Lmaps/l/bb;->a:I

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2, v5}, Lmaps/s/t;-><init>(IIZ)V

    iput-object v0, p0, Lmaps/l/ag;->h:Lmaps/s/t;

    new-instance v0, Lmaps/s/t;

    iget v1, p2, Lmaps/l/bb;->e:I

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2, v5}, Lmaps/s/t;-><init>(IIZ)V

    iput-object v0, p0, Lmaps/l/ag;->i:Lmaps/s/t;

    iput-object v3, p0, Lmaps/l/ag;->j:Lmaps/al/o;

    iput-object v3, p0, Lmaps/l/ag;->k:Lmaps/al/q;

    iput-object v3, p0, Lmaps/l/ag;->l:Lmaps/al/a;

    iput-object v3, p0, Lmaps/l/ag;->n:Lmaps/al/i;

    iput-object v3, p0, Lmaps/l/ag;->m:Lmaps/al/a;

    iput-object v3, p0, Lmaps/l/ag;->o:Lmaps/al/a;

    :goto_0
    iget v0, p2, Lmaps/l/bb;->c:I

    if-lez v0, :cond_2

    new-instance v0, Lmaps/al/e;

    iget v1, p2, Lmaps/l/bb;->c:I

    invoke-direct {v0, v1}, Lmaps/al/e;-><init>(I)V

    iput-object v0, p0, Lmaps/l/ag;->t:Lmaps/al/o;

    new-instance v0, Lmaps/s/g;

    iget v1, p2, Lmaps/l/bb;->c:I

    invoke-virtual {p4}, Lmaps/cr/c;->K()Lmaps/s/l;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lmaps/s/g;-><init>(ILmaps/s/l;)V

    iput-object v0, p0, Lmaps/l/ag;->u:Lmaps/s/g;

    new-instance v0, Lmaps/al/j;

    iget v1, p2, Lmaps/l/bb;->d:I

    invoke-direct {v0, v1}, Lmaps/al/j;-><init>(I)V

    iput-object v0, p0, Lmaps/l/ag;->v:Lmaps/al/i;

    new-instance v0, Lmaps/al/j;

    iget v1, p2, Lmaps/l/bb;->b:I

    invoke-direct {v0, v1}, Lmaps/al/j;-><init>(I)V

    iput-object v0, p0, Lmaps/l/ag;->w:Lmaps/al/i;

    :goto_1
    iget v0, p2, Lmaps/l/bb;->e:I

    if-lez v0, :cond_3

    new-instance v0, Lmaps/al/e;

    iget v1, p2, Lmaps/l/bb;->e:I

    invoke-direct {v0, v1}, Lmaps/al/e;-><init>(I)V

    iput-object v0, p0, Lmaps/l/ag;->x:Lmaps/al/o;

    new-instance v0, Lmaps/al/k;

    iget v1, p2, Lmaps/l/bb;->e:I

    invoke-direct {v0, v1}, Lmaps/al/k;-><init>(I)V

    iput-object v0, p0, Lmaps/l/ag;->y:Lmaps/al/q;

    new-instance v0, Lmaps/al/j;

    iget v1, p2, Lmaps/l/bb;->f:I

    invoke-direct {v0, v1}, Lmaps/al/j;-><init>(I)V

    iput-object v0, p0, Lmaps/l/ag;->z:Lmaps/al/i;

    :goto_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/l/ag;->C:Ljava/util/ArrayList;

    iput v6, p0, Lmaps/l/ag;->a:F

    sget-boolean v0, Lmaps/ae/h;->O:Z

    if-eqz v0, :cond_4

    iget v0, p2, Lmaps/l/bb;->g:I

    if-lez v0, :cond_4

    new-instance v0, Lmaps/al/e;

    iget v1, p2, Lmaps/l/bb;->g:I

    invoke-direct {v0, v1}, Lmaps/al/e;-><init>(I)V

    iput-object v0, p0, Lmaps/l/ag;->p:Lmaps/al/o;

    new-instance v0, Lmaps/al/k;

    iget v1, p2, Lmaps/l/bb;->g:I

    invoke-direct {v0, v1}, Lmaps/al/k;-><init>(I)V

    iput-object v0, p0, Lmaps/l/ag;->q:Lmaps/al/q;

    new-instance v0, Lmaps/al/j;

    iget v1, p2, Lmaps/l/bb;->h:I

    invoke-direct {v0, v1}, Lmaps/al/j;-><init>(I)V

    iput-object v0, p0, Lmaps/l/ag;->r:Lmaps/al/i;

    new-instance v0, Lmaps/al/f;

    iget v1, p2, Lmaps/l/bb;->g:I

    invoke-direct {v0, v1}, Lmaps/al/f;-><init>(I)V

    iput-object v0, p0, Lmaps/l/ag;->s:Lmaps/al/a;

    :goto_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/l/ag;->D:Ljava/util/ArrayList;

    iput v6, p0, Lmaps/l/ag;->F:F

    return-void

    :cond_0
    iput-object v3, p0, Lmaps/l/ag;->h:Lmaps/s/t;

    iput-object v3, p0, Lmaps/l/ag;->i:Lmaps/s/t;

    new-instance v0, Lmaps/al/e;

    iget v1, p2, Lmaps/l/bb;->a:I

    invoke-direct {v0, v1, v5}, Lmaps/al/e;-><init>(IZ)V

    iput-object v0, p0, Lmaps/l/ag;->j:Lmaps/al/o;

    new-instance v0, Lmaps/al/k;

    iget v1, p2, Lmaps/l/bb;->a:I

    invoke-direct {v0, v1, v5}, Lmaps/al/k;-><init>(IZ)V

    iput-object v0, p0, Lmaps/l/ag;->k:Lmaps/al/q;

    new-instance v0, Lmaps/al/f;

    iget v1, p2, Lmaps/l/bb;->a:I

    invoke-direct {v0, v1, v5}, Lmaps/al/f;-><init>(IZ)V

    iput-object v0, p0, Lmaps/l/ag;->m:Lmaps/al/a;

    new-instance v0, Lmaps/al/f;

    iget v1, p2, Lmaps/l/bb;->a:I

    invoke-direct {v0, v1, v5}, Lmaps/al/f;-><init>(IZ)V

    iput-object v0, p0, Lmaps/l/ag;->l:Lmaps/al/a;

    new-instance v0, Lmaps/al/j;

    iget v1, p2, Lmaps/l/bb;->b:I

    invoke-direct {v0, v1, v5}, Lmaps/al/j;-><init>(IZ)V

    iput-object v0, p0, Lmaps/l/ag;->n:Lmaps/al/i;

    sget-boolean v0, Lmaps/ae/h;->O:Z

    if-eqz v0, :cond_1

    iget-object v0, p2, Lmaps/l/bb;->i:Ljava/lang/Boolean;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    if-ne v0, v1, :cond_1

    new-instance v0, Lmaps/al/f;

    iget v1, p2, Lmaps/l/bb;->a:I

    invoke-direct {v0, v1}, Lmaps/al/f;-><init>(I)V

    iput-object v0, p0, Lmaps/l/ag;->o:Lmaps/al/a;

    goto/16 :goto_0

    :cond_1
    iput-object v3, p0, Lmaps/l/ag;->o:Lmaps/al/a;

    goto/16 :goto_0

    :cond_2
    iput-object v3, p0, Lmaps/l/ag;->t:Lmaps/al/o;

    iput-object v3, p0, Lmaps/l/ag;->u:Lmaps/s/g;

    iput-object v3, p0, Lmaps/l/ag;->v:Lmaps/al/i;

    iput-object v3, p0, Lmaps/l/ag;->w:Lmaps/al/i;

    goto/16 :goto_1

    :cond_3
    iput-object v3, p0, Lmaps/l/ag;->x:Lmaps/al/o;

    iput-object v3, p0, Lmaps/l/ag;->y:Lmaps/al/q;

    iput-object v3, p0, Lmaps/l/ag;->z:Lmaps/al/i;

    goto/16 :goto_2

    :cond_4
    iput-object v3, p0, Lmaps/l/ag;->p:Lmaps/al/o;

    iput-object v3, p0, Lmaps/l/ag;->q:Lmaps/al/q;

    iput-object v3, p0, Lmaps/l/ag;->r:Lmaps/al/i;

    iput-object v3, p0, Lmaps/l/ag;->s:Lmaps/al/a;

    goto :goto_3
.end method

.method private static a(F)F
    .locals 3

    float-to-int v1, p0

    int-to-float v0, v1

    sub-float v0, p0, v0

    const/high16 v2, 0x3f000000

    cmpl-float v0, v0, v2

    if-lez v0, :cond_0

    sget v0, Lmaps/l/ag;->d:F

    :goto_0
    const/4 v2, 0x1

    rsub-int/lit8 v1, v1, 0x1e

    shl-int v1, v2, v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    const/high16 v1, 0x43800000

    div-float/2addr v0, v1

    return v0

    :cond_0
    const/high16 v0, 0x3f800000

    goto :goto_0
.end method

.method private a(Lmaps/bq/d;)F
    .locals 2

    const/high16 v0, 0x40c00000

    invoke-virtual {p1}, Lmaps/bq/d;->s()F

    move-result v1

    invoke-static {v1}, Lmaps/l/ag;->a(F)F

    move-result v1

    mul-float/2addr v0, v1

    return v0
.end method

.method private static a(Lmaps/t/aa;)F
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lmaps/t/aa;->c()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    sget-boolean v2, Lmaps/ae/h;->O:Z

    if-nez v2, :cond_2

    invoke-virtual {p0, v0}, Lmaps/t/aa;->b(I)Lmaps/t/ac;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/ac;->c()F

    move-result v1

    goto :goto_0

    :cond_2
    :goto_1
    invoke-virtual {p0}, Lmaps/t/aa;->c()I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-virtual {p0, v0}, Lmaps/t/aa;->b(I)Lmaps/t/ac;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/t/ac;->c()F

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private static a(FI)I
    .locals 2

    int-to-float v0, p1

    sub-float v0, p0, v0

    const/high16 v1, 0x40800000

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    const/high16 v1, 0x40400000

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_1

    const/4 v0, 0x3

    goto :goto_0

    :cond_1
    const/high16 v1, 0x40100000

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_2

    const/4 v0, 0x4

    goto :goto_0

    :cond_2
    const/high16 v1, 0x3f800000

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_3

    const/16 v0, 0x11

    if-lt p1, v0, :cond_3

    const/4 v0, 0x5

    goto :goto_0

    :cond_3
    const/16 v0, 0x16

    goto :goto_0
.end method

.method private static a(FILmaps/af/q;)I
    .locals 2

    sget-object v0, Lmaps/af/q;->b:Lmaps/af/q;

    if-ne p2, v0, :cond_1

    const/high16 v0, 0x41800000

    cmpg-float v0, p0, v0

    if-gez v0, :cond_0

    const/4 v0, 0x7

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x15

    goto :goto_0

    :cond_1
    int-to-float v0, p1

    sub-float v0, p0, v0

    const/high16 v1, 0x40800000

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/high16 v1, 0x40400000

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_3

    const/4 v0, 0x2

    goto :goto_0

    :cond_3
    const/high16 v1, 0x40100000

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_4

    const/4 v0, 0x3

    goto :goto_0

    :cond_4
    const/high16 v1, 0x3f800000

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_5

    const/16 v0, 0x11

    if-lt p1, v0, :cond_5

    const/4 v0, 0x4

    goto :goto_0

    :cond_5
    const/4 v0, 0x6

    goto :goto_0
.end method

.method public static a(Lmaps/bq/d;Lmaps/af/q;)I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lmaps/bq/d;->s()F

    move-result v1

    sget-object v2, Lmaps/af/q;->b:Lmaps/af/q;

    if-ne p1, v2, :cond_0

    const/high16 v2, 0x418c0000

    cmpg-float v2, v1, v2

    if-gtz v2, :cond_1

    :cond_0
    const/16 v0, 0x20

    :cond_1
    sget-object v2, Lmaps/af/q;->a:Lmaps/af/q;

    if-ne p1, v2, :cond_2

    or-int/lit8 v0, v0, 0x14

    :cond_2
    const/high16 v2, 0x41780000

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_4

    sget-object v1, Lmaps/af/q;->a:Lmaps/af/q;

    if-eq p1, v1, :cond_3

    sget-object v1, Lmaps/af/q;->c:Lmaps/af/q;

    if-ne p1, v1, :cond_4

    :cond_3
    or-int/lit8 v0, v0, 0x40

    :cond_4
    sget-boolean v1, Lmaps/ae/h;->O:Z

    if-eqz v1, :cond_5

    or-int/lit16 v0, v0, 0x180

    :cond_5
    return v0
.end method

.method public static a(Lmaps/t/ah;[Ljava/lang/String;Lmaps/t/h;Lmaps/q/aj;Lmaps/q/aj;Lmaps/s/j;Lmaps/cr/c;)Lmaps/l/ag;
    .locals 13

    invoke-virtual {p0}, Lmaps/t/ah;->j()Lmaps/t/ax;

    move-result-object v6

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    new-instance v7, Ljava/util/ArrayList;

    const/16 v0, 0x200

    invoke-direct {v7, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {p0}, Lmaps/t/ah;->c()I

    move-result v5

    new-instance v2, Lmaps/l/bb;

    invoke-direct {v2}, Lmaps/l/bb;-><init>()V

    new-instance v8, Lmaps/l/v;

    invoke-direct {v8}, Lmaps/l/v;-><init>()V

    const/4 v0, -0x1

    move v4, v0

    :cond_0
    invoke-interface {p2}, Lmaps/t/h;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {p2}, Lmaps/t/h;->a()Lmaps/t/ca;

    move-result-object v1

    instance-of v0, v1, Lmaps/t/bu;

    if-nez v0, :cond_1

    move v5, v4

    :goto_0
    new-instance v0, Lmaps/l/ag;

    move-object v1, p0

    move-object/from16 v4, p6

    invoke-direct/range {v0 .. v5}, Lmaps/l/ag;-><init>(Lmaps/t/ah;Lmaps/l/bb;Ljava/util/HashSet;Lmaps/cr/c;I)V

    invoke-static {}, Lmaps/s/q;->a()Lmaps/s/q;

    move-result-object v4

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmaps/t/bu;

    invoke-static {v2, v8}, Lmaps/l/ag;->a(Lmaps/t/bu;Lmaps/l/v;)V

    move-object v1, v6

    move-object v3, v8

    move-object/from16 v5, p5

    invoke-direct/range {v0 .. v5}, Lmaps/l/ag;->a(Lmaps/t/ax;Lmaps/t/bu;Lmaps/l/v;Lmaps/s/q;Lmaps/s/j;)V

    goto :goto_1

    :cond_1
    move-object v0, v1

    check-cast v0, Lmaps/t/bu;

    invoke-virtual {v0}, Lmaps/t/bu;->g()Z

    move-result v9

    if-eqz v9, :cond_2

    sget-boolean v9, Lmaps/ae/h;->H:Z

    if-eqz v9, :cond_2

    invoke-virtual {v0}, Lmaps/t/bu;->h()Lmaps/t/aa;

    move-result-object v9

    invoke-virtual {v9}, Lmaps/t/aa;->l()Lmaps/t/ac;

    move-result-object v9

    if-nez v9, :cond_3

    const v4, -0x48481b

    :cond_2
    :goto_2
    invoke-static {v0, v8}, Lmaps/l/ag;->a(Lmaps/t/bu;Lmaps/l/v;)V

    invoke-static {v5, v0, v8, v2}, Lmaps/l/ag;->a(ILmaps/t/bu;Lmaps/l/v;Lmaps/l/bb;)Z

    move-result v9

    if-nez v9, :cond_5

    move v5, v4

    goto :goto_0

    :cond_3
    const/4 v9, -0x1

    if-ne v4, v9, :cond_4

    invoke-virtual {v0}, Lmaps/t/bu;->h()Lmaps/t/aa;

    move-result-object v4

    invoke-virtual {v4}, Lmaps/t/aa;->l()Lmaps/t/ac;

    move-result-object v4

    invoke-virtual {v4}, Lmaps/t/ac;->b()I

    move-result v4

    goto :goto_2

    :cond_4
    invoke-virtual {v0}, Lmaps/t/bu;->h()Lmaps/t/aa;

    move-result-object v9

    invoke-virtual {v9}, Lmaps/t/aa;->l()Lmaps/t/ac;

    move-result-object v9

    invoke-virtual {v9}, Lmaps/t/ac;->b()I

    move-result v9

    if-eq v4, v9, :cond_2

    move v5, v4

    goto :goto_0

    :cond_5
    invoke-interface {v1}, Lmaps/t/ca;->j()[I

    move-result-object v9

    array-length v10, v9

    const/4 v1, 0x0

    :goto_3
    if-ge v1, v10, :cond_7

    aget v11, v9, v1

    if-ltz v11, :cond_6

    array-length v12, p1

    if-ge v11, v12, :cond_6

    aget-object v11, p1, v11

    invoke-virtual {v3, v11}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_7
    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {p2}, Lmaps/t/h;->next()Ljava/lang/Object;

    sget-boolean v1, Lmaps/ae/h;->G:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lmaps/t/bu;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    move v5, v4

    goto/16 :goto_0

    :cond_8
    move-object v1, p0

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    invoke-direct/range {v0 .. v5}, Lmaps/l/ag;->a(Lmaps/t/ah;Lmaps/q/aj;Lmaps/q/aj;Lmaps/s/j;Lmaps/cr/c;)V

    return-object v0

    :cond_9
    move v5, v4

    goto/16 :goto_0
.end method

.method public static a(Lmaps/cr/c;FILmaps/af/q;)V
    .locals 5

    const/16 v4, 0x2300

    const/16 v3, 0x2200

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x302

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    sget-boolean v1, Lmaps/ae/h;->t:Z

    if-eqz v1, :cond_1

    const/4 v1, -0x2

    invoke-interface {v0, v4, v3, v1}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    :goto_1
    invoke-virtual {p0}, Lmaps/cr/c;->p()V

    invoke-virtual {p0}, Lmaps/cr/c;->r()V

    invoke-static {p1, p2}, Lmaps/l/ag;->a(FI)I

    move-result v1

    invoke-virtual {p0}, Lmaps/cr/c;->a()Lmaps/s/i;

    move-result-object v2

    invoke-virtual {v2, v1}, Lmaps/s/i;->a(I)Lmaps/cr/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmaps/cr/a;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    goto :goto_0

    :cond_1
    const/16 v1, 0x1e01

    invoke-interface {v0, v4, v3, v1}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    goto :goto_1
.end method

.method public static a(Lmaps/cr/c;FLmaps/af/q;)V
    .locals 4

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x302

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x2100

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    invoke-virtual {p0}, Lmaps/cr/c;->r()V

    invoke-virtual {p0}, Lmaps/cr/c;->a()Lmaps/s/i;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lmaps/s/i;->a(I)Lmaps/cr/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmaps/cr/a;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    goto :goto_0
.end method

.method private a(Lmaps/t/ah;Lmaps/q/aj;Lmaps/q/aj;Lmaps/s/j;Lmaps/cr/c;)V
    .locals 9

    const/16 v8, 0x16

    const/16 v7, 0x303

    const/16 v6, 0x302

    const/4 v5, 0x4

    const/4 v4, 0x0

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/l/ag;->h:Lmaps/s/t;

    invoke-virtual {v0, v5}, Lmaps/s/t;->f(I)Lmaps/s/r;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/ag;->g:Lmaps/s/r;

    iget-object v0, p0, Lmaps/l/ag;->h:Lmaps/s/t;

    invoke-virtual {v0}, Lmaps/s/t;->h()V

    iget-object v0, p0, Lmaps/l/ag;->i:Lmaps/s/t;

    invoke-virtual {v0, v5}, Lmaps/s/t;->f(I)Lmaps/s/r;

    move-result-object v0

    iget-object v1, p0, Lmaps/l/ag;->i:Lmaps/s/t;

    invoke-virtual {v1}, Lmaps/s/t;->h()V

    invoke-virtual {p4}, Lmaps/s/j;->b()I

    move-result v1

    if-nez v1, :cond_2

    sget-boolean v0, Lmaps/ae/h;->i:Z

    if-eqz v0, :cond_0

    const-string v0, "GLRoadGroup"

    const-string v1, "Group has no styles"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    new-instance v1, Lmaps/q/aa;

    invoke-direct {v1, v5, p1}, Lmaps/q/aa;-><init>(ILmaps/t/ah;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Road Outline"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmaps/q/aa;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lmaps/l/ag;->g:Lmaps/s/r;

    invoke-virtual {v1, v2}, Lmaps/q/aa;->a(Lmaps/q/r;)V

    invoke-virtual {p5}, Lmaps/cr/c;->a()Lmaps/s/i;

    move-result-object v2

    invoke-virtual {v2, v8}, Lmaps/s/i;->a(I)Lmaps/cr/a;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmaps/q/aa;->a(Lmaps/q/z;)V

    invoke-virtual {v1, p2}, Lmaps/q/aa;->a(Lmaps/q/z;)V

    new-instance v2, Lmaps/q/n;

    invoke-direct {v2, v6, v7}, Lmaps/q/n;-><init>(II)V

    invoke-virtual {v1, v2}, Lmaps/q/aa;->a(Lmaps/q/z;)V

    iget-object v2, p0, Lmaps/l/ag;->b:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1, v4}, Lmaps/q/aa;->b(I)V

    new-instance v1, Lmaps/q/aa;

    invoke-direct {v1, v5, p1}, Lmaps/q/aa;-><init>(ILmaps/t/ah;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Road Fill"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmaps/q/aa;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lmaps/l/ag;->g:Lmaps/s/r;

    invoke-virtual {v1, v2}, Lmaps/q/aa;->a(Lmaps/q/r;)V

    invoke-virtual {p5}, Lmaps/cr/c;->a()Lmaps/s/i;

    move-result-object v2

    invoke-virtual {v2, v8}, Lmaps/s/i;->a(I)Lmaps/cr/a;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmaps/q/aa;->a(Lmaps/q/z;)V

    invoke-virtual {v1, p3}, Lmaps/q/aa;->a(Lmaps/q/z;)V

    new-instance v2, Lmaps/q/n;

    invoke-direct {v2, v6, v7}, Lmaps/q/n;-><init>(II)V

    invoke-virtual {v1, v2}, Lmaps/q/aa;->a(Lmaps/q/z;)V

    iget-object v2, p0, Lmaps/l/ag;->b:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1, v4}, Lmaps/q/aa;->b(I)V

    new-instance v1, Lmaps/q/aa;

    const/4 v2, 0x6

    invoke-direct {v1, v2, p1}, Lmaps/q/aa;-><init>(ILmaps/t/ah;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Road Arrow"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmaps/q/aa;->a(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lmaps/q/aa;->a(Lmaps/q/r;)V

    invoke-virtual {p5}, Lmaps/cr/c;->a()Lmaps/s/i;

    move-result-object v0

    invoke-virtual {v0, v4}, Lmaps/s/i;->a(I)Lmaps/cr/a;

    move-result-object v0

    invoke-virtual {v1, v0}, Lmaps/q/aa;->a(Lmaps/q/z;)V

    sget-boolean v0, Lmaps/ae/h;->H:Z

    if-eqz v0, :cond_3

    new-instance v0, Lmaps/q/ar;

    iget-object v2, p0, Lmaps/l/ag;->f:[F

    invoke-direct {v0, v4, v2}, Lmaps/q/ar;-><init>(I[F)V

    invoke-virtual {v1, v0}, Lmaps/q/aa;->a(Lmaps/q/z;)V

    :goto_1
    new-instance v0, Lmaps/q/n;

    invoke-direct {v0, v6, v7}, Lmaps/q/n;-><init>(II)V

    invoke-virtual {v1, v0}, Lmaps/q/aa;->a(Lmaps/q/z;)V

    iget-object v0, p0, Lmaps/l/ag;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1, v4}, Lmaps/q/aa;->b(I)V

    goto/16 :goto_0

    :cond_3
    new-instance v0, Lmaps/q/ar;

    sget-object v2, Lmaps/l/ag;->c:[F

    invoke-direct {v0, v4, v2}, Lmaps/q/ar;-><init>(I[F)V

    invoke-virtual {v1, v0}, Lmaps/q/aa;->a(Lmaps/q/z;)V

    goto :goto_1
.end method

.method private a(Lmaps/t/ax;Lmaps/t/bu;Lmaps/l/v;Lmaps/s/q;Lmaps/s/j;)V
    .locals 17

    move-object/from16 v0, p3

    iget-boolean v2, v0, Lmaps/l/v;->a:Z

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {p2 .. p2}, Lmaps/t/bu;->h()Lmaps/t/aa;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/t/aa;->c()I

    move-result v3

    if-lez v3, :cond_1

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lmaps/t/aa;->b(I)Lmaps/t/ac;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lmaps/t/ac;->f()Z

    move-result v10

    invoke-virtual {v3}, Lmaps/t/ac;->g()Z

    move-result v11

    :cond_1
    invoke-virtual/range {p1 .. p1}, Lmaps/t/ax;->d()Lmaps/t/bx;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lmaps/t/ax;->g()I

    move-result v6

    invoke-virtual/range {p2 .. p2}, Lmaps/t/bu;->c()Lmaps/t/cg;

    move-result-object v3

    move-object/from16 v0, p3

    iget v4, v0, Lmaps/l/v;->c:F

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v6}, Lmaps/l/ag;->c(FI)F

    move-result v4

    move-object/from16 v0, p3

    iget v7, v0, Lmaps/l/v;->e:I

    if-nez v7, :cond_2

    move-object/from16 v0, p3

    iget v7, v0, Lmaps/l/v;->d:I

    if-nez v7, :cond_2

    sget-boolean v7, Lmaps/ae/h;->O:Z

    if-eqz v7, :cond_3

    move-object/from16 v0, p3

    iget v7, v0, Lmaps/l/v;->f:I

    if-eqz v7, :cond_3

    :cond_2
    sget-boolean v7, Lmaps/ae/h;->F:Z

    if-eqz v7, :cond_6

    move-object/from16 v0, p5

    invoke-virtual {v0, v2}, Lmaps/s/j;->a(Lmaps/t/aa;)B

    move-result v12

    move-object/from16 v0, p0

    iget-object v7, v0, Lmaps/l/ag;->h:Lmaps/s/t;

    move-object/from16 v0, p0

    iget-object v8, v0, Lmaps/l/ag;->h:Lmaps/s/t;

    move-object/from16 v0, p0

    iget-object v9, v0, Lmaps/l/ag;->h:Lmaps/s/t;

    move-object/from16 v2, p4

    invoke-virtual/range {v2 .. v12}, Lmaps/s/q;->a(Lmaps/t/cg;FLmaps/t/bx;ILmaps/al/l;Lmaps/al/c;Lmaps/al/b;ZZB)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/l/ag;->h:Lmaps/s/t;

    invoke-virtual {v2}, Lmaps/s/t;->d()V

    :cond_3
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/l/ag;->e:Lmaps/t/ah;

    invoke-virtual {v2}, Lmaps/t/ah;->c()I

    move-result v2

    move-object/from16 v0, p2

    invoke-static {v2, v0}, Lmaps/l/ag;->a(ILmaps/t/bu;)Z

    move-result v2

    if-eqz v2, :cond_4

    sget-boolean v2, Lmaps/ae/h;->F:Z

    if-eqz v2, :cond_9

    const/4 v10, 0x0

    const/high16 v13, 0x3f800000

    move-object/from16 v0, p0

    iget-object v14, v0, Lmaps/l/ag;->i:Lmaps/s/t;

    move-object/from16 v0, p0

    iget-object v15, v0, Lmaps/l/ag;->i:Lmaps/s/t;

    const/16 v16, 0x0

    move-object/from16 v7, p4

    move-object v8, v3

    move v9, v4

    move-object v11, v5

    move v12, v6

    invoke-virtual/range {v7 .. v16}, Lmaps/s/q;->a(Lmaps/t/cg;FZLmaps/t/bx;IFLmaps/al/l;Lmaps/al/b;Lmaps/al/c;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/l/ag;->i:Lmaps/s/t;

    const/high16 v7, 0x40c00000

    move-object/from16 v0, p0

    iget-object v8, v0, Lmaps/l/ag;->e:Lmaps/t/ah;

    invoke-virtual {v8}, Lmaps/t/ah;->c()I

    move-result v8

    int-to-float v8, v8

    invoke-static {v8}, Lmaps/l/ag;->a(F)F

    move-result v8

    mul-float/2addr v7, v8

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v2, v7}, Lmaps/l/ag;->a(Lmaps/t/bu;Lmaps/al/c;F)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/l/ag;->i:Lmaps/s/t;

    invoke-virtual {v2}, Lmaps/s/t;->d()V

    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/l/ag;->C:Ljava/util/ArrayList;

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    sget-boolean v2, Lmaps/ae/h;->O:Z

    if-eqz v2, :cond_5

    move-object/from16 v0, p3

    iget v2, v0, Lmaps/l/v;->g:I

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/l/ag;->p:Lmaps/al/o;

    invoke-virtual {v2}, Lmaps/al/o;->c()I

    move-result v2

    const/4 v10, 0x0

    const/high16 v13, 0x3f800000

    move-object/from16 v0, p0

    iget-object v14, v0, Lmaps/l/ag;->p:Lmaps/al/o;

    move-object/from16 v0, p0

    iget-object v15, v0, Lmaps/l/ag;->r:Lmaps/al/i;

    const/16 v16, 0x0

    move-object/from16 v7, p4

    move-object v8, v3

    move v9, v4

    move-object v11, v5

    move v12, v6

    invoke-virtual/range {v7 .. v16}, Lmaps/s/q;->a(Lmaps/t/cg;FZLmaps/t/bx;IFLmaps/al/l;Lmaps/al/b;Lmaps/al/c;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/l/ag;->p:Lmaps/al/o;

    invoke-virtual {v3}, Lmaps/al/o;->c()I

    move-result v3

    sub-int v2, v3, v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/l/ag;->s:Lmaps/al/a;

    move-object/from16 v0, p3

    iget v4, v0, Lmaps/l/v;->g:I

    invoke-virtual {v3, v4, v2}, Lmaps/al/a;->b(II)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/l/ag;->D:Ljava/util/ArrayList;

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    invoke-virtual/range {p2 .. p2}, Lmaps/t/bu;->n()Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lmaps/l/ag;->E:Z

    goto/16 :goto_0

    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/l/ag;->j:Lmaps/al/o;

    invoke-virtual {v2}, Lmaps/al/o;->c()I

    move-result v13

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/l/ag;->n:Lmaps/al/i;

    invoke-virtual {v2}, Lmaps/al/i;->a()I

    move-result v14

    move-object/from16 v0, p0

    iget-object v7, v0, Lmaps/l/ag;->j:Lmaps/al/o;

    move-object/from16 v0, p0

    iget-object v8, v0, Lmaps/l/ag;->k:Lmaps/al/q;

    move-object/from16 v0, p0

    iget-object v9, v0, Lmaps/l/ag;->n:Lmaps/al/i;

    const/4 v12, 0x0

    move-object/from16 v2, p4

    invoke-virtual/range {v2 .. v12}, Lmaps/s/q;->a(Lmaps/t/cg;FLmaps/t/bx;ILmaps/al/l;Lmaps/al/c;Lmaps/al/b;ZZB)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/l/ag;->j:Lmaps/al/o;

    invoke-virtual {v2}, Lmaps/al/o;->c()I

    move-result v2

    sub-int/2addr v2, v13

    move-object/from16 v0, p0

    iget-object v7, v0, Lmaps/l/ag;->l:Lmaps/al/a;

    move-object/from16 v0, p3

    iget v8, v0, Lmaps/l/v;->e:I

    invoke-virtual {v7, v8, v2}, Lmaps/al/a;->b(II)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lmaps/l/ag;->m:Lmaps/al/a;

    move-object/from16 v0, p3

    iget v8, v0, Lmaps/l/v;->d:I

    invoke-virtual {v7, v8, v2}, Lmaps/al/a;->b(II)V

    sget-boolean v7, Lmaps/ae/h;->O:Z

    if-eqz v7, :cond_7

    move-object/from16 v0, p0

    iget-object v7, v0, Lmaps/l/ag;->o:Lmaps/al/a;

    if-eqz v7, :cond_7

    move-object/from16 v0, p3

    iget v7, v0, Lmaps/l/v;->f:I

    if-eqz v7, :cond_7

    move-object/from16 v0, p0

    iget-object v7, v0, Lmaps/l/ag;->o:Lmaps/al/a;

    move-object/from16 v0, p3

    iget v8, v0, Lmaps/l/v;->f:I

    invoke-virtual {v7, v8, v2}, Lmaps/al/a;->b(II)V

    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/l/ag;->t:Lmaps/al/o;

    if-eqz v2, :cond_3

    move-object/from16 v0, p3

    iget-boolean v2, v0, Lmaps/l/v;->b:Z

    if-eqz v2, :cond_8

    move-object/from16 v0, p3

    iget v2, v0, Lmaps/l/v;->d:I

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v5, v6, v2}, Lmaps/l/ag;->a(Lmaps/t/cg;Lmaps/t/bx;II)V

    goto/16 :goto_1

    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/l/ag;->w:Lmaps/al/i;

    move-object/from16 v0, p0

    iget-object v7, v0, Lmaps/l/ag;->n:Lmaps/al/i;

    move-object/from16 v0, p0

    iget-object v8, v0, Lmaps/l/ag;->n:Lmaps/al/i;

    invoke-virtual {v8}, Lmaps/al/i;->a()I

    move-result v8

    sub-int/2addr v8, v14

    invoke-virtual {v2, v7, v14, v8}, Lmaps/al/i;->a(Lmaps/al/i;II)V

    goto/16 :goto_1

    :cond_9
    const/4 v10, 0x0

    const/high16 v13, 0x3f800000

    move-object/from16 v0, p0

    iget-object v14, v0, Lmaps/l/ag;->x:Lmaps/al/o;

    move-object/from16 v0, p0

    iget-object v15, v0, Lmaps/l/ag;->z:Lmaps/al/i;

    const/16 v16, 0x0

    move-object/from16 v7, p4

    move-object v8, v3

    move v9, v4

    move-object v11, v5

    move v12, v6

    invoke-virtual/range {v7 .. v16}, Lmaps/s/q;->a(Lmaps/t/cg;FZLmaps/t/bx;IFLmaps/al/l;Lmaps/al/b;Lmaps/al/c;)V

    goto/16 :goto_2
.end method

.method private a(Lmaps/t/bu;Lmaps/al/c;F)V
    .locals 11

    const/high16 v0, 0x41800000

    mul-float/2addr v0, p3

    const/high16 v1, 0x3f800000

    div-float v4, v1, p3

    const/high16 v1, 0x3f800000

    div-float v5, v1, v0

    iget-object v0, p0, Lmaps/l/ag;->e:Lmaps/t/ah;

    invoke-virtual {v0}, Lmaps/t/ah;->j()Lmaps/t/ax;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/ax;->g()I

    move-result v0

    invoke-virtual {p1}, Lmaps/t/bu;->c()Lmaps/t/cg;

    move-result-object v6

    invoke-virtual {v6}, Lmaps/t/cg;->b()I

    move-result v1

    add-int/lit8 v7, v1, -0x1

    invoke-virtual {p1}, Lmaps/t/bu;->h()Lmaps/t/aa;

    move-result-object v1

    invoke-static {v1}, Lmaps/l/ag;->a(Lmaps/t/aa;)F

    move-result v1

    const/high16 v2, 0x40000000

    invoke-direct {p0, v1, v0}, Lmaps/l/ag;->c(FI)F

    move-result v0

    mul-float v8, v2, v0

    const/4 v0, 0x0

    move v3, v0

    :goto_0
    if-ge v3, v7, :cond_3

    invoke-virtual {v6, v3}, Lmaps/t/cg;->b(I)F

    move-result v9

    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    mul-float/2addr v9, v5

    const/high16 v10, 0x3f000000

    cmpl-float v10, v9, v10

    if-lez v10, :cond_1

    mul-float v1, v8, v4

    const/high16 v2, 0x47000000

    mul-float/2addr v1, v2

    float-to-int v2, v1

    const/high16 v1, 0x47800000

    mul-float/2addr v1, v9

    float-to-int v1, v1

    float-to-int v10, v9

    int-to-float v10, v10

    sub-float/2addr v9, v10

    const/high16 v10, 0x3e000000

    cmpl-float v10, v9, v10

    if-lez v10, :cond_0

    const/high16 v10, 0x3ec00000

    cmpg-float v9, v9, v10

    if-gez v9, :cond_0

    const v0, 0xa000

    :cond_0
    :goto_1
    const v9, 0x8000

    sub-int/2addr v9, v2

    const v10, 0x8000

    add-int/2addr v2, v10

    invoke-virtual {p1}, Lmaps/t/bu;->e()Z

    move-result v10

    if-eqz v10, :cond_2

    add-int v10, v1, v0

    invoke-interface {p2, v2, v10}, Lmaps/al/c;->a(II)V

    add-int/2addr v1, v0

    invoke-interface {p2, v9, v1}, Lmaps/al/c;->a(II)V

    invoke-interface {p2, v9, v0}, Lmaps/al/c;->a(II)V

    invoke-interface {p2, v2, v0}, Lmaps/al/c;->a(II)V

    :goto_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_1
    const v0, 0xc000

    goto :goto_1

    :cond_2
    invoke-interface {p2, v9, v0}, Lmaps/al/c;->a(II)V

    invoke-interface {p2, v2, v0}, Lmaps/al/c;->a(II)V

    add-int v10, v1, v0

    invoke-interface {p2, v2, v10}, Lmaps/al/c;->a(II)V

    add-int/2addr v0, v1

    invoke-interface {p2, v9, v0}, Lmaps/al/c;->a(II)V

    goto :goto_2

    :cond_3
    return-void
.end method

.method static a(Lmaps/t/bu;Lmaps/l/v;)V
    .locals 8

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v1, 0x2

    const/4 v5, 0x0

    invoke-virtual {p0}, Lmaps/t/bu;->h()Lmaps/t/aa;

    move-result-object v6

    invoke-static {v6}, Lmaps/l/ag;->a(Lmaps/t/aa;)F

    move-result v0

    iput v0, p1, Lmaps/l/v;->c:F

    iput v5, p1, Lmaps/l/v;->d:I

    iput v5, p1, Lmaps/l/v;->e:I

    iput v5, p1, Lmaps/l/v;->f:I

    invoke-virtual {v6}, Lmaps/t/aa;->c()I

    move-result v0

    if-lt v0, v1, :cond_3

    invoke-virtual {v6, v5}, Lmaps/t/aa;->b(I)Lmaps/t/ac;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/ac;->b()I

    move-result v0

    iput v0, p1, Lmaps/l/v;->d:I

    invoke-virtual {v6, v4}, Lmaps/t/aa;->b(I)Lmaps/t/ac;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/ac;->b()I

    move-result v0

    iput v0, p1, Lmaps/l/v;->e:I

    sget-boolean v0, Lmaps/ae/h;->O:Z

    if-eqz v0, :cond_0

    invoke-virtual {v6}, Lmaps/t/aa;->c()I

    move-result v0

    if-le v0, v1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v6}, Lmaps/t/aa;->c()I

    move-result v2

    if-ge v0, v2, :cond_e

    invoke-virtual {v6, v0}, Lmaps/t/aa;->b(I)Lmaps/t/ac;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/t/ac;->d()[I

    move-result-object v2

    array-length v2, v2

    if-nez v2, :cond_2

    :goto_1
    invoke-virtual {v6, v0}, Lmaps/t/aa;->b(I)Lmaps/t/ac;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/ac;->b()I

    move-result v0

    iput v0, p1, Lmaps/l/v;->f:I

    :cond_0
    :goto_2
    iput v5, p1, Lmaps/l/v;->g:I

    sget-boolean v0, Lmaps/ae/h;->O:Z

    if-eqz v0, :cond_5

    invoke-virtual {v6}, Lmaps/t/aa;->c()I

    move-result v0

    if-le v0, v1, :cond_5

    move v0, v1

    move v2, v3

    :goto_3
    invoke-virtual {v6}, Lmaps/t/aa;->c()I

    move-result v7

    if-ge v0, v7, :cond_5

    invoke-virtual {v6, v0}, Lmaps/t/aa;->b(I)Lmaps/t/ac;

    move-result-object v7

    invoke-virtual {v7}, Lmaps/t/ac;->d()[I

    move-result-object v7

    array-length v7, v7

    if-lez v7, :cond_4

    invoke-virtual {v6, v0}, Lmaps/t/aa;->b(I)Lmaps/t/ac;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/t/ac;->c()F

    move-result v2

    invoke-virtual {v6, v0}, Lmaps/t/aa;->b(I)Lmaps/t/ac;

    move-result-object v7

    invoke-virtual {v7}, Lmaps/t/ac;->b()I

    move-result v7

    iput v7, p1, Lmaps/l/v;->g:I

    :cond_1
    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    invoke-virtual {v6}, Lmaps/t/aa;->c()I

    move-result v0

    if-lt v0, v4, :cond_0

    invoke-virtual {v6, v5}, Lmaps/t/aa;->b(I)Lmaps/t/ac;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/ac;->b()I

    move-result v0

    iput v0, p1, Lmaps/l/v;->e:I

    goto :goto_2

    :cond_4
    iget v7, p1, Lmaps/l/v;->g:I

    if-eqz v7, :cond_1

    invoke-virtual {v6, v0}, Lmaps/t/aa;->b(I)Lmaps/t/ac;

    move-result-object v7

    invoke-virtual {v7}, Lmaps/t/ac;->c()F

    move-result v7

    cmpl-float v7, v7, v2

    if-lez v7, :cond_1

    iput v5, p1, Lmaps/l/v;->g:I

    goto :goto_4

    :cond_5
    iget v0, p1, Lmaps/l/v;->g:I

    if-eqz v0, :cond_6

    iput v5, p1, Lmaps/l/v;->f:I

    :cond_6
    sget-boolean v0, Lmaps/ae/h;->O:Z

    if-eqz v0, :cond_8

    iget v0, p1, Lmaps/l/v;->f:I

    if-eqz v0, :cond_8

    iget v0, p1, Lmaps/l/v;->e:I

    invoke-static {v0}, Lmaps/s/k;->a(I)I

    move-result v0

    iget v2, p1, Lmaps/l/v;->d:I

    invoke-static {v2}, Lmaps/s/k;->a(I)I

    move-result v2

    if-lt v0, v2, :cond_7

    iget v0, p1, Lmaps/l/v;->c:F

    const/high16 v2, 0x41100000

    cmpg-float v0, v0, v2

    if-gez v0, :cond_8

    :cond_7
    iget v0, p1, Lmaps/l/v;->f:I

    iput v0, p1, Lmaps/l/v;->e:I

    :cond_8
    invoke-virtual {p0}, Lmaps/t/bu;->c()Lmaps/t/cg;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/cg;->b()I

    move-result v0

    if-lt v0, v1, :cond_c

    iget v0, p1, Lmaps/l/v;->c:F

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_c

    iget v0, p1, Lmaps/l/v;->e:I

    if-nez v0, :cond_a

    iget v0, p1, Lmaps/l/v;->d:I

    if-nez v0, :cond_a

    sget-boolean v0, Lmaps/ae/h;->O:Z

    if-eqz v0, :cond_9

    iget v0, p1, Lmaps/l/v;->f:I

    if-nez v0, :cond_a

    iget v0, p1, Lmaps/l/v;->g:I

    if-nez v0, :cond_a

    :cond_9
    invoke-virtual {p0}, Lmaps/t/bu;->l()Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_a
    move v0, v4

    :goto_5
    iput-boolean v0, p1, Lmaps/l/v;->a:Z

    invoke-virtual {p0}, Lmaps/t/bu;->m()Z

    move-result v0

    if-eqz v0, :cond_d

    sget-boolean v0, Lmaps/ae/h;->O:Z

    if-eqz v0, :cond_b

    iget v0, p1, Lmaps/l/v;->g:I

    if-nez v0, :cond_d

    iget v0, p1, Lmaps/l/v;->f:I

    if-nez v0, :cond_d

    iget v0, p1, Lmaps/l/v;->e:I

    invoke-static {v0}, Lmaps/s/k;->c(I)Z

    move-result v0

    if-nez v0, :cond_d

    :cond_b
    :goto_6
    iput-boolean v4, p1, Lmaps/l/v;->b:Z

    return-void

    :cond_c
    move v0, v5

    goto :goto_5

    :cond_d
    move v4, v5

    goto :goto_6

    :cond_e
    move v0, v1

    goto/16 :goto_1
.end method

.method private a(Lmaps/t/cg;Lmaps/t/bx;II)V
    .locals 6

    iget-object v0, p0, Lmaps/l/ag;->t:Lmaps/al/o;

    invoke-virtual {v0}, Lmaps/al/o;->c()I

    move-result v1

    invoke-virtual {p1}, Lmaps/t/cg;->b()I

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    iget-object v3, p0, Lmaps/l/ag;->B:Lmaps/t/bx;

    invoke-virtual {p1, v0, v3}, Lmaps/t/cg;->a(ILmaps/t/bx;)V

    iget-object v3, p0, Lmaps/l/ag;->B:Lmaps/t/bx;

    iget-object v4, p0, Lmaps/l/ag;->B:Lmaps/t/bx;

    invoke-static {v3, p2, v4}, Lmaps/t/bx;->b(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    iget-object v3, p0, Lmaps/l/ag;->t:Lmaps/al/o;

    iget-object v4, p0, Lmaps/l/ag;->B:Lmaps/t/bx;

    invoke-virtual {v3, v4, p3}, Lmaps/al/o;->a(Lmaps/t/bx;I)V

    if-lez v0, :cond_0

    iget-object v3, p0, Lmaps/l/ag;->v:Lmaps/al/i;

    add-int v4, v1, v0

    add-int/lit8 v4, v4, -0x1

    int-to-short v4, v4

    add-int v5, v1, v0

    int-to-short v5, v5

    invoke-virtual {v3, v4, v5}, Lmaps/al/i;->a(SS)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lmaps/l/ag;->u:Lmaps/s/g;

    invoke-virtual {v0, p4, v2}, Lmaps/s/g;->a(II)V

    return-void
.end method

.method private static a(ILmaps/t/bu;)Z
    .locals 1

    const/16 v0, 0xe

    if-lt p0, v0, :cond_0

    invoke-virtual {p1}, Lmaps/t/bu;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static a(ILmaps/t/bu;Lmaps/l/v;Lmaps/l/bb;)Z
    .locals 10

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Lmaps/t/bu;->c()Lmaps/t/cg;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/t/cg;->b()I

    move-result v4

    add-int/lit8 v5, v4, -0x1

    iget-boolean v0, p2, Lmaps/l/v;->a:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-static {v3}, Lmaps/s/q;->a(Lmaps/t/cg;)I

    move-result v6

    iget v0, p3, Lmaps/l/bb;->a:I

    if-lez v0, :cond_2

    iget v0, p3, Lmaps/l/bb;->a:I

    add-int/2addr v0, v6

    const/16 v7, 0x4000

    if-le v0, v7, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lmaps/t/bu;->h()Lmaps/t/aa;

    move-result-object v7

    sget-boolean v0, Lmaps/ae/h;->O:Z

    if-eqz v0, :cond_5

    iget v0, p2, Lmaps/l/v;->f:I

    if-eqz v0, :cond_5

    move v0, v1

    :goto_1
    iget-object v8, p3, Lmaps/l/bb;->i:Ljava/lang/Boolean;

    if-nez v8, :cond_6

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p3, Lmaps/l/bb;->i:Ljava/lang/Boolean;

    :cond_3
    sget-boolean v0, Lmaps/ae/h;->O:Z

    if-eqz v0, :cond_7

    iget v0, p2, Lmaps/l/v;->g:I

    if-eqz v0, :cond_7

    :goto_2
    invoke-virtual {v7}, Lmaps/t/aa;->c()I

    move-result v0

    if-ge v2, v0, :cond_7

    invoke-virtual {v7, v2}, Lmaps/t/aa;->b(I)Lmaps/t/ac;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/ac;->d()[I

    move-result-object v0

    array-length v0, v0

    if-eqz v0, :cond_4

    mul-int/lit8 v0, v5, 0x4

    mul-int/lit8 v8, v5, 0x2

    iget v9, p3, Lmaps/l/bb;->g:I

    add-int/2addr v0, v9

    iput v0, p3, Lmaps/l/bb;->g:I

    iget v0, p3, Lmaps/l/bb;->h:I

    mul-int/lit8 v8, v8, 0x3

    add-int/2addr v0, v8

    iput v0, p3, Lmaps/l/bb;->h:I

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_1

    :cond_6
    iget-object v8, p3, Lmaps/l/bb;->i:Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-eq v8, v0, :cond_3

    move v1, v2

    goto :goto_0

    :cond_7
    iget v0, p3, Lmaps/l/bb;->a:I

    add-int/2addr v0, v6

    iput v0, p3, Lmaps/l/bb;->a:I

    iget v0, p3, Lmaps/l/bb;->b:I

    invoke-static {v3}, Lmaps/s/q;->b(Lmaps/t/cg;)I

    move-result v2

    add-int/2addr v0, v2

    iput v0, p3, Lmaps/l/bb;->b:I

    iget-boolean v0, p2, Lmaps/l/v;->b:Z

    if-eqz v0, :cond_8

    iget v0, p3, Lmaps/l/bb;->c:I

    add-int/2addr v0, v4

    iput v0, p3, Lmaps/l/bb;->c:I

    iget v0, p3, Lmaps/l/bb;->d:I

    mul-int/lit8 v2, v5, 0x2

    add-int/2addr v0, v2

    iput v0, p3, Lmaps/l/bb;->d:I

    :cond_8
    invoke-static {p0, p1}, Lmaps/l/ag;->a(ILmaps/t/bu;)Z

    move-result v0

    if-eqz v0, :cond_0

    mul-int/lit8 v0, v5, 0x4

    mul-int/lit8 v2, v5, 0x2

    iget v3, p3, Lmaps/l/bb;->e:I

    add-int/2addr v0, v3

    iput v0, p3, Lmaps/l/bb;->e:I

    iget v0, p3, Lmaps/l/bb;->f:I

    mul-int/lit8 v2, v2, 0x3

    add-int/2addr v0, v2

    iput v0, p3, Lmaps/l/bb;->f:I

    goto/16 :goto_0
.end method

.method private b(Lmaps/bq/d;)F
    .locals 2

    const/high16 v0, 0x40c00000

    invoke-virtual {p1}, Lmaps/bq/d;->s()F

    move-result v1

    invoke-static {v1}, Lmaps/l/ag;->a(F)F

    move-result v1

    mul-float/2addr v0, v1

    return v0
.end method

.method private static b(FI)I
    .locals 2

    int-to-float v0, p1

    sub-float v0, p0, v0

    const/high16 v1, 0x40800000

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_0

    const/16 v0, 0x19

    :goto_0
    return v0

    :cond_0
    const/high16 v1, 0x40400000

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_1

    const/16 v0, 0x1a

    goto :goto_0

    :cond_1
    const/high16 v1, 0x40100000

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_2

    const/16 v0, 0x1b

    goto :goto_0

    :cond_2
    const/high16 v1, 0x3f800000

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_3

    const/16 v0, 0x11

    if-lt p1, v0, :cond_3

    const/16 v0, 0x1c

    goto :goto_0

    :cond_3
    const/16 v0, 0x1d

    goto :goto_0
.end method

.method public static b(Lmaps/cr/c;FILmaps/af/q;)V
    .locals 5

    const/16 v4, 0x2300

    const/16 v3, 0x2200

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x302

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    sget-boolean v1, Lmaps/ae/h;->t:Z

    if-eqz v1, :cond_1

    const/4 v1, -0x2

    invoke-interface {v0, v4, v3, v1}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    :goto_1
    invoke-virtual {p0}, Lmaps/cr/c;->p()V

    invoke-virtual {p0}, Lmaps/cr/c;->r()V

    invoke-static {p1, p2, p3}, Lmaps/l/ag;->a(FILmaps/af/q;)I

    move-result v1

    invoke-virtual {p0}, Lmaps/cr/c;->a()Lmaps/s/i;

    move-result-object v2

    invoke-virtual {v2, v1}, Lmaps/s/i;->a(I)Lmaps/cr/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmaps/cr/a;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    goto :goto_0

    :cond_1
    const/16 v1, 0x1e01

    invoke-interface {v0, v4, v3, v1}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    goto :goto_1
.end method

.method public static b(Lmaps/cr/c;FLmaps/af/q;)V
    .locals 4

    invoke-virtual {p0}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x302

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x1e01

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    invoke-virtual {p0}, Lmaps/cr/c;->p()V

    invoke-virtual {p0}, Lmaps/cr/c;->r()V

    invoke-virtual {p0}, Lmaps/cr/c;->a()Lmaps/s/i;

    move-result-object v1

    const/16 v2, 0x1e

    invoke-virtual {v1, v2}, Lmaps/s/i;->a(I)Lmaps/cr/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmaps/cr/a;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    return-void
.end method

.method private c(FI)F
    .locals 3

    iget-object v0, p0, Lmaps/l/ag;->e:Lmaps/t/ah;

    invoke-virtual {v0}, Lmaps/t/ah;->c()I

    move-result v0

    const/16 v1, 0xe

    if-le v0, v1, :cond_0

    const/high16 v0, 0x3f000000

    :goto_0
    int-to-float v1, p2

    mul-float/2addr v1, p1

    const/high16 v2, 0x43800000

    div-float/2addr v1, v2

    mul-float/2addr v0, v1

    return v0

    :cond_0
    const v0, 0x3e99999a

    goto :goto_0
.end method

.method public static c(Lmaps/cr/c;FILmaps/af/q;)V
    .locals 4

    invoke-virtual {p0}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x302

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x1e01

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    invoke-virtual {p0}, Lmaps/cr/c;->p()V

    invoke-virtual {p0}, Lmaps/cr/c;->r()V

    invoke-static {p1, p2}, Lmaps/l/ag;->b(FI)I

    move-result v1

    invoke-virtual {p0}, Lmaps/cr/c;->a()Lmaps/s/i;

    move-result-object v2

    invoke-virtual {v2, v1}, Lmaps/s/i;->a(I)Lmaps/cr/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmaps/cr/a;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 2

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/l/ag;->g:Lmaps/s/r;

    invoke-virtual {v0}, Lmaps/s/r;->d()I

    move-result v0

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lmaps/l/ag;->j:Lmaps/al/o;

    invoke-virtual {v0}, Lmaps/al/o;->d()I

    move-result v0

    iget-object v1, p0, Lmaps/l/ag;->k:Lmaps/al/q;

    invoke-virtual {v1}, Lmaps/al/q;->d()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/l/ag;->l:Lmaps/al/a;

    invoke-virtual {v1}, Lmaps/al/a;->b()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/l/ag;->m:Lmaps/al/a;

    invoke-virtual {v1}, Lmaps/al/a;->b()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/l/ag;->n:Lmaps/al/i;

    invoke-virtual {v1}, Lmaps/al/i;->c()I

    move-result v1

    add-int/2addr v0, v1

    sget-boolean v1, Lmaps/ae/h;->O:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lmaps/l/ag;->o:Lmaps/al/a;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lmaps/l/ag;->o:Lmaps/al/a;

    invoke-virtual {v1}, Lmaps/al/a;->b()I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Lmaps/l/ag;->t:Lmaps/al/o;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lmaps/l/ag;->t:Lmaps/al/o;

    invoke-virtual {v1}, Lmaps/al/o;->d()I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Lmaps/l/ag;->u:Lmaps/s/g;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lmaps/l/ag;->u:Lmaps/s/g;

    invoke-virtual {v1}, Lmaps/s/g;->a()I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-object v1, p0, Lmaps/l/ag;->v:Lmaps/al/i;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lmaps/l/ag;->v:Lmaps/al/i;

    invoke-virtual {v1}, Lmaps/al/i;->c()I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-object v1, p0, Lmaps/l/ag;->w:Lmaps/al/i;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lmaps/l/ag;->w:Lmaps/al/i;

    invoke-virtual {v1}, Lmaps/al/i;->c()I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget-object v1, p0, Lmaps/l/ag;->x:Lmaps/al/o;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lmaps/l/ag;->x:Lmaps/al/o;

    invoke-virtual {v1}, Lmaps/al/o;->d()I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget-object v1, p0, Lmaps/l/ag;->y:Lmaps/al/q;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lmaps/l/ag;->y:Lmaps/al/q;

    invoke-virtual {v1}, Lmaps/al/q;->d()I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget-object v1, p0, Lmaps/l/ag;->z:Lmaps/al/i;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lmaps/l/ag;->z:Lmaps/al/i;

    invoke-virtual {v1}, Lmaps/al/i;->c()I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    sget-boolean v1, Lmaps/ae/h;->O:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/l/ag;->p:Lmaps/al/o;

    if-eqz v1, :cond_a

    iget-object v1, p0, Lmaps/l/ag;->p:Lmaps/al/o;

    invoke-virtual {v1}, Lmaps/al/o;->d()I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iget-object v1, p0, Lmaps/l/ag;->q:Lmaps/al/q;

    if-eqz v1, :cond_b

    iget-object v1, p0, Lmaps/l/ag;->q:Lmaps/al/q;

    invoke-virtual {v1}, Lmaps/al/q;->d()I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    iget-object v1, p0, Lmaps/l/ag;->r:Lmaps/al/i;

    if-eqz v1, :cond_c

    iget-object v1, p0, Lmaps/l/ag;->r:Lmaps/al/i;

    invoke-virtual {v1}, Lmaps/al/i;->c()I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    iget-object v1, p0, Lmaps/l/ag;->s:Lmaps/al/a;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/l/ag;->s:Lmaps/al/a;

    invoke-virtual {v1}, Lmaps/al/a;->b()I

    move-result v1

    add-int/2addr v0, v1

    goto/16 :goto_0
.end method

.method public a(Lmaps/cr/c;)V
    .locals 1

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0, p1}, Lmaps/l/ag;->c(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ag;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :cond_0
    :goto_0
    sget-boolean v0, Lmaps/ae/h;->O:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/l/ag;->p:Lmaps/al/o;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/l/ag;->p:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->e(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ag;->q:Lmaps/al/q;

    invoke-virtual {v0, p1}, Lmaps/al/q;->e(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ag;->r:Lmaps/al/i;

    invoke-virtual {v0, p1}, Lmaps/al/i;->c(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ag;->s:Lmaps/al/a;

    invoke-virtual {v0, p1}, Lmaps/al/a;->b(Lmaps/cr/c;)V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lmaps/l/ag;->j:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->e(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ag;->k:Lmaps/al/q;

    invoke-virtual {v0, p1}, Lmaps/al/q;->e(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ag;->m:Lmaps/al/a;

    invoke-virtual {v0, p1}, Lmaps/al/a;->b(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ag;->l:Lmaps/al/a;

    invoke-virtual {v0, p1}, Lmaps/al/a;->b(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ag;->n:Lmaps/al/i;

    invoke-virtual {v0, p1}, Lmaps/al/i;->c(Lmaps/cr/c;)V

    sget-boolean v0, Lmaps/ae/h;->O:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/l/ag;->o:Lmaps/al/a;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/l/ag;->o:Lmaps/al/a;

    invoke-virtual {v0, p1}, Lmaps/al/a;->b(Lmaps/cr/c;)V

    :cond_3
    iget-object v0, p0, Lmaps/l/ag;->t:Lmaps/al/o;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lmaps/l/ag;->t:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->e(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ag;->u:Lmaps/s/g;

    invoke-virtual {v0, p1}, Lmaps/s/g;->c(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ag;->v:Lmaps/al/i;

    invoke-virtual {v0, p1}, Lmaps/al/i;->c(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ag;->w:Lmaps/al/i;

    invoke-virtual {v0, p1}, Lmaps/al/i;->c(Lmaps/cr/c;)V

    :cond_4
    iget-object v0, p0, Lmaps/l/ag;->x:Lmaps/al/o;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/l/ag;->x:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->e(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ag;->y:Lmaps/al/q;

    invoke-virtual {v0, p1}, Lmaps/al/q;->e(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ag;->z:Lmaps/al/i;

    invoke-virtual {v0, p1}, Lmaps/al/i;->c(Lmaps/cr/c;)V

    goto :goto_0
.end method

.method a(Lmaps/cr/c;F)V
    .locals 8

    const-wide/16 v3, 0x1f4

    iget v0, p0, Lmaps/l/ag;->a:F

    cmpl-float v0, p2, v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput p2, p0, Lmaps/l/ag;->a:F

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lmaps/l/ag;->y:Lmaps/al/q;

    invoke-virtual {v0, p1}, Lmaps/al/q;->a(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ag;->y:Lmaps/al/q;

    move-object v1, v0

    :goto_1
    iget-object v0, p0, Lmaps/l/ag;->C:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/bu;

    invoke-direct {p0, v0, v1, p2}, Lmaps/l/ag;->a(Lmaps/t/bu;Lmaps/al/c;F)V

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lmaps/l/ag;->i:Lmaps/s/t;

    move-object v1, v0

    goto :goto_1

    :cond_3
    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lmaps/ae/h;->H:Z

    if-eqz v0, :cond_4

    new-instance v0, Lmaps/w/v;

    const-wide/16 v1, 0x0

    sget-object v5, Lmaps/w/j;->c:Lmaps/w/j;

    const/4 v6, 0x0

    const/16 v7, 0x64

    invoke-direct/range {v0 .. v7}, Lmaps/w/v;-><init>(JJLmaps/w/j;II)V

    iput-object v0, p0, Lmaps/l/ag;->A:Lmaps/w/v;

    goto :goto_0

    :cond_4
    new-instance v0, Lmaps/w/v;

    sget-object v1, Lmaps/w/j;->a:Lmaps/w/j;

    invoke-direct {v0, v3, v4, v1}, Lmaps/w/v;-><init>(JLmaps/w/j;)V

    iput-object v0, p0, Lmaps/l/ag;->A:Lmaps/w/v;

    goto :goto_0
.end method

.method public a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 8

    const/4 v6, 0x1

    const v5, 0x4737b700

    const-wide/16 v3, 0x0

    const/high16 v2, 0x3e800000

    const/4 v7, 0x4

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Lmaps/bq/d;->s()F

    move-result v0

    iget-object v1, p0, Lmaps/l/ag;->e:Lmaps/t/ah;

    invoke-virtual {v1}, Lmaps/t/ah;->c()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    invoke-interface {p3}, Lmaps/af/s;->b()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lmaps/l/ag;->t:Lmaps/al/o;

    if-eqz v1, :cond_0

    cmpl-float v0, v0, v2

    if-gtz v0, :cond_0

    iget-object v0, p0, Lmaps/l/ag;->t:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->d(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ag;->u:Lmaps/s/g;

    invoke-virtual {v0, p1}, Lmaps/s/g;->a(Lmaps/cr/c;)V

    invoke-static {p1}, Lmaps/s/l;->c(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ag;->v:Lmaps/al/i;

    invoke-virtual {v0, p1, v6}, Lmaps/al/i;->a(Lmaps/cr/c;I)V

    invoke-static {p1}, Lmaps/s/l;->d(Lmaps/cr/c;)V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lmaps/l/ag;->k:Lmaps/al/q;

    invoke-virtual {v1, p1}, Lmaps/al/q;->d(Lmaps/cr/c;)V

    iget-object v1, p0, Lmaps/l/ag;->j:Lmaps/al/o;

    invoke-virtual {v1, p1}, Lmaps/al/o;->d(Lmaps/cr/c;)V

    iget-object v1, p0, Lmaps/l/ag;->m:Lmaps/al/a;

    invoke-virtual {v1, p1}, Lmaps/al/a;->c(Lmaps/cr/c;)V

    iget-object v1, p0, Lmaps/l/ag;->w:Lmaps/al/i;

    if-eqz v1, :cond_2

    cmpl-float v0, v0, v2

    if-lez v0, :cond_3

    :cond_2
    iget-object v0, p0, Lmaps/l/ag;->n:Lmaps/al/i;

    invoke-virtual {v0, p1, v7}, Lmaps/al/i;->a(Lmaps/cr/c;I)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lmaps/l/ag;->w:Lmaps/al/i;

    invoke-virtual {v0, p1, v7}, Lmaps/al/i;->a(Lmaps/cr/c;I)V

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lmaps/l/ag;->k:Lmaps/al/q;

    invoke-virtual {v1, p1}, Lmaps/al/q;->d(Lmaps/cr/c;)V

    iget-object v1, p0, Lmaps/l/ag;->j:Lmaps/al/o;

    invoke-virtual {v1, p1}, Lmaps/al/o;->d(Lmaps/cr/c;)V

    iget-object v1, p0, Lmaps/l/ag;->l:Lmaps/al/a;

    invoke-virtual {v1, p1}, Lmaps/al/a;->c(Lmaps/cr/c;)V

    iget-object v1, p0, Lmaps/l/ag;->w:Lmaps/al/i;

    if-eqz v1, :cond_4

    cmpl-float v0, v0, v2

    if-lez v0, :cond_5

    :cond_4
    iget-object v0, p0, Lmaps/l/ag;->n:Lmaps/al/i;

    invoke-virtual {v0, p1, v7}, Lmaps/al/i;->a(Lmaps/cr/c;I)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lmaps/l/ag;->w:Lmaps/al/i;

    invoke-virtual {v0, p1, v7}, Lmaps/al/i;->a(Lmaps/cr/c;I)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lmaps/l/ag;->x:Lmaps/al/o;

    if-eqz v0, :cond_0

    invoke-direct {p0, p2}, Lmaps/l/ag;->a(Lmaps/bq/d;)F

    move-result v0

    float-to-double v1, v0

    cmpg-double v1, v1, v3

    if-lez v1, :cond_0

    invoke-virtual {p0, p1, v0}, Lmaps/l/ag;->a(Lmaps/cr/c;F)V

    const/high16 v1, 0x10000

    const/high16 v0, 0x3f800000

    iget-object v2, p0, Lmaps/l/ag;->A:Lmaps/w/v;

    if-eqz v2, :cond_6

    sget-boolean v2, Lmaps/ae/h;->H:Z

    if-eqz v2, :cond_7

    iget-object v0, p0, Lmaps/l/ag;->A:Lmaps/w/v;

    invoke-virtual {v0, p1}, Lmaps/w/v;->a(Lmaps/cr/c;)I

    move-result v0

    int-to-float v0, v0

    const/high16 v2, 0x42c80000

    div-float/2addr v0, v2

    const v2, 0x3f7d70a4

    cmpl-float v2, v0, v2

    if-lez v2, :cond_6

    const/4 v2, 0x0

    iput-object v2, p0, Lmaps/l/ag;->A:Lmaps/w/v;

    :cond_6
    :goto_1
    sget-boolean v2, Lmaps/ae/h;->H:Z

    if-eqz v2, :cond_8

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    iget-object v2, p0, Lmaps/l/ag;->f:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    iget-object v3, p0, Lmaps/l/ag;->f:[F

    aget v3, v3, v6

    iget-object v4, p0, Lmaps/l/ag;->f:[F

    const/4 v5, 0x2

    aget v4, v4, v5

    iget-object v5, p0, Lmaps/l/ag;->f:[F

    const/4 v6, 0x3

    aget v5, v5, v6

    mul-float/2addr v0, v5

    invoke-interface {v1, v2, v3, v4, v0}, Ljavax/microedition/khronos/opengles/GL10;->glColor4f(FFFF)V

    :goto_2
    iget-object v0, p0, Lmaps/l/ag;->y:Lmaps/al/q;

    invoke-virtual {v0, p1}, Lmaps/al/q;->d(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ag;->x:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->d(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ag;->z:Lmaps/al/i;

    invoke-virtual {v0, p1, v7}, Lmaps/al/i;->a(Lmaps/cr/c;I)V

    goto/16 :goto_0

    :cond_7
    iget-object v1, p0, Lmaps/l/ag;->A:Lmaps/w/v;

    invoke-virtual {v1, p1}, Lmaps/w/v;->a(Lmaps/cr/c;)I

    move-result v1

    const/high16 v2, 0x10000

    if-ne v1, v2, :cond_6

    const/4 v2, 0x0

    iput-object v2, p0, Lmaps/l/ag;->A:Lmaps/w/v;

    goto :goto_1

    :cond_8
    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const v2, 0x4765e500

    int-to-float v1, v1

    invoke-interface {v0, v5, v5, v2, v1}, Ljavax/microedition/khronos/opengles/GL10;->glColor4f(FFFF)V

    goto :goto_2

    :pswitch_5
    sget-boolean v1, Lmaps/ae/h;->O:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/l/ag;->o:Lmaps/al/a;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/l/ag;->k:Lmaps/al/q;

    invoke-virtual {v1, p1}, Lmaps/al/q;->d(Lmaps/cr/c;)V

    iget-object v1, p0, Lmaps/l/ag;->j:Lmaps/al/o;

    invoke-virtual {v1, p1}, Lmaps/al/o;->d(Lmaps/cr/c;)V

    iget-object v1, p0, Lmaps/l/ag;->o:Lmaps/al/a;

    invoke-virtual {v1, p1}, Lmaps/al/a;->c(Lmaps/cr/c;)V

    iget-object v1, p0, Lmaps/l/ag;->w:Lmaps/al/i;

    if-eqz v1, :cond_9

    cmpl-float v0, v0, v2

    if-lez v0, :cond_a

    :cond_9
    iget-object v0, p0, Lmaps/l/ag;->n:Lmaps/al/i;

    invoke-virtual {v0, p1, v7}, Lmaps/al/i;->a(Lmaps/cr/c;I)V

    goto/16 :goto_0

    :cond_a
    iget-object v0, p0, Lmaps/l/ag;->w:Lmaps/al/i;

    invoke-virtual {v0, p1, v7}, Lmaps/al/i;->a(Lmaps/cr/c;I)V

    goto/16 :goto_0

    :pswitch_6
    sget-boolean v0, Lmaps/ae/h;->O:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/l/ag;->p:Lmaps/al/o;

    if-eqz v0, :cond_0

    invoke-direct {p0, p2}, Lmaps/l/ag;->b(Lmaps/bq/d;)F

    move-result v0

    float-to-double v1, v0

    cmpg-double v1, v1, v3

    if-lez v1, :cond_0

    invoke-virtual {p0, p1, v0}, Lmaps/l/ag;->b(Lmaps/cr/c;F)V

    iget-object v0, p0, Lmaps/l/ag;->q:Lmaps/al/q;

    invoke-virtual {v0, p1}, Lmaps/al/q;->d(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ag;->p:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->d(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ag;->s:Lmaps/al/a;

    invoke-virtual {v0, p1}, Lmaps/al/a;->c(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ag;->r:Lmaps/al/i;

    invoke-virtual {v0, p1, v7}, Lmaps/al/i;->a(Lmaps/cr/c;I)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public b()I
    .locals 3

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/l/ag;->g:Lmaps/s/r;

    invoke-virtual {v0}, Lmaps/s/r;->c()I

    move-result v1

    :cond_0
    return v1

    :cond_1
    iget-object v0, p0, Lmaps/l/ag;->j:Lmaps/al/o;

    invoke-virtual {v0}, Lmaps/al/o;->a()I

    move-result v0

    add-int/lit16 v0, v0, 0x1d0

    iget-object v1, p0, Lmaps/l/ag;->k:Lmaps/al/q;

    invoke-virtual {v1}, Lmaps/al/q;->a()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/l/ag;->l:Lmaps/al/a;

    invoke-virtual {v1}, Lmaps/al/a;->c()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/l/ag;->m:Lmaps/al/a;

    invoke-virtual {v1}, Lmaps/al/a;->c()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/l/ag;->n:Lmaps/al/i;

    invoke-virtual {v1}, Lmaps/al/i;->d()I

    move-result v1

    add-int/2addr v0, v1

    sget-boolean v1, Lmaps/ae/h;->O:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lmaps/l/ag;->o:Lmaps/al/a;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lmaps/l/ag;->o:Lmaps/al/a;

    invoke-virtual {v1}, Lmaps/al/a;->c()I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Lmaps/l/ag;->t:Lmaps/al/o;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lmaps/l/ag;->t:Lmaps/al/o;

    invoke-virtual {v1}, Lmaps/al/o;->a()I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Lmaps/l/ag;->u:Lmaps/s/g;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lmaps/l/ag;->u:Lmaps/s/g;

    invoke-virtual {v1}, Lmaps/s/g;->b()I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-object v1, p0, Lmaps/l/ag;->v:Lmaps/al/i;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lmaps/l/ag;->v:Lmaps/al/i;

    invoke-virtual {v1}, Lmaps/al/i;->d()I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-object v1, p0, Lmaps/l/ag;->w:Lmaps/al/i;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lmaps/l/ag;->w:Lmaps/al/i;

    invoke-virtual {v1}, Lmaps/al/i;->d()I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget-object v1, p0, Lmaps/l/ag;->x:Lmaps/al/o;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lmaps/l/ag;->x:Lmaps/al/o;

    invoke-virtual {v1}, Lmaps/al/o;->a()I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget-object v1, p0, Lmaps/l/ag;->y:Lmaps/al/q;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lmaps/l/ag;->y:Lmaps/al/q;

    invoke-virtual {v1}, Lmaps/al/q;->a()I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget-object v1, p0, Lmaps/l/ag;->z:Lmaps/al/i;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lmaps/l/ag;->z:Lmaps/al/i;

    invoke-virtual {v1}, Lmaps/al/i;->d()I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    sget-boolean v1, Lmaps/ae/h;->O:Z

    if-eqz v1, :cond_d

    iget-object v1, p0, Lmaps/l/ag;->p:Lmaps/al/o;

    if-eqz v1, :cond_a

    iget-object v1, p0, Lmaps/l/ag;->p:Lmaps/al/o;

    invoke-virtual {v1}, Lmaps/al/o;->a()I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iget-object v1, p0, Lmaps/l/ag;->q:Lmaps/al/q;

    if-eqz v1, :cond_b

    iget-object v1, p0, Lmaps/l/ag;->q:Lmaps/al/q;

    invoke-virtual {v1}, Lmaps/al/q;->a()I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    iget-object v1, p0, Lmaps/l/ag;->r:Lmaps/al/i;

    if-eqz v1, :cond_c

    iget-object v1, p0, Lmaps/l/ag;->r:Lmaps/al/i;

    invoke-virtual {v1}, Lmaps/al/i;->d()I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    iget-object v1, p0, Lmaps/l/ag;->s:Lmaps/al/a;

    if-eqz v1, :cond_d

    iget-object v1, p0, Lmaps/l/ag;->s:Lmaps/al/a;

    invoke-virtual {v1}, Lmaps/al/a;->c()I

    move-result v1

    add-int/2addr v0, v1

    :cond_d
    add-int/lit8 v0, v0, 0x18

    iget-object v1, p0, Lmaps/l/ag;->C:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/bu;

    invoke-virtual {v0}, Lmaps/t/bu;->k()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_e
    sget-boolean v0, Lmaps/ae/h;->O:Z

    if-eqz v0, :cond_0

    add-int/lit8 v0, v1, 0x18

    iget-object v1, p0, Lmaps/l/ag;->D:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/bu;

    invoke-virtual {v0}, Lmaps/t/bu;->k()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_1
.end method

.method b(Lmaps/cr/c;F)V
    .locals 12

    iget v0, p0, Lmaps/l/ag;->F:F

    cmpl-float v0, p2, v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iput p2, p0, Lmaps/l/ag;->F:F

    const/high16 v0, 0x41800000

    mul-float/2addr v0, p2

    const/high16 v1, 0x3f800000

    div-float/2addr v1, p2

    const/high16 v2, 0x3f800000

    div-float/2addr v2, v0

    iget-object v0, p0, Lmaps/l/ag;->q:Lmaps/al/q;

    invoke-virtual {v0, p1}, Lmaps/al/q;->a(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ag;->D:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    iget-object v0, p0, Lmaps/l/ag;->e:Lmaps/t/ah;

    invoke-virtual {v0}, Lmaps/t/ah;->j()Lmaps/t/ax;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/ax;->g()I

    move-result v4

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/bu;

    invoke-virtual {v0}, Lmaps/t/bu;->c()Lmaps/t/cg;

    move-result-object v5

    invoke-virtual {v5}, Lmaps/t/cg;->b()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v0}, Lmaps/t/bu;->h()Lmaps/t/aa;

    move-result-object v0

    invoke-static {v0}, Lmaps/l/ag;->a(Lmaps/t/aa;)F

    move-result v0

    const/high16 v7, 0x40000000

    invoke-direct {p0, v0, v4}, Lmaps/l/ag;->c(FI)F

    move-result v0

    mul-float/2addr v7, v0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v6, :cond_2

    invoke-virtual {v5, v0}, Lmaps/t/cg;->b(I)F

    move-result v8

    mul-float/2addr v8, v2

    mul-float v9, v7, v1

    const/high16 v10, 0x47000000

    mul-float/2addr v9, v10

    float-to-int v9, v9

    const/high16 v10, 0x47800000

    mul-float/2addr v8, v10

    float-to-int v8, v8

    const v10, 0x8000

    sub-int/2addr v10, v9

    const v11, 0x8000

    add-int/2addr v9, v11

    iget-object v11, p0, Lmaps/l/ag;->q:Lmaps/al/q;

    invoke-virtual {v11, v9, v8}, Lmaps/al/q;->a(II)V

    iget-object v11, p0, Lmaps/l/ag;->q:Lmaps/al/q;

    invoke-virtual {v11, v10, v8}, Lmaps/al/q;->a(II)V

    iget-object v8, p0, Lmaps/l/ag;->q:Lmaps/al/q;

    const/4 v11, 0x0

    invoke-virtual {v8, v10, v11}, Lmaps/al/q;->a(II)V

    iget-object v8, p0, Lmaps/l/ag;->q:Lmaps/al/q;

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Lmaps/al/q;->a(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public c(Lmaps/cr/c;)V
    .locals 3

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/l/ag;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/q/aa;

    invoke-virtual {p1}, Lmaps/cr/c;->m()Lmaps/q/ad;

    move-result-object v2

    invoke-virtual {v2, v0}, Lmaps/q/ad;->b(Lmaps/q/ba;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmaps/l/ag;->j:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->b(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ag;->k:Lmaps/al/q;

    invoke-virtual {v0, p1}, Lmaps/al/q;->b(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ag;->m:Lmaps/al/a;

    invoke-virtual {v0, p1}, Lmaps/al/a;->a(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ag;->l:Lmaps/al/a;

    invoke-virtual {v0, p1}, Lmaps/al/a;->a(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ag;->n:Lmaps/al/i;

    invoke-virtual {v0, p1}, Lmaps/al/i;->b(Lmaps/cr/c;)V

    sget-boolean v0, Lmaps/ae/h;->O:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/l/ag;->o:Lmaps/al/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/l/ag;->o:Lmaps/al/a;

    invoke-virtual {v0, p1}, Lmaps/al/a;->a(Lmaps/cr/c;)V

    :cond_1
    iget-object v0, p0, Lmaps/l/ag;->t:Lmaps/al/o;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/l/ag;->t:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->b(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ag;->u:Lmaps/s/g;

    invoke-virtual {v0, p1}, Lmaps/s/g;->b(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ag;->v:Lmaps/al/i;

    invoke-virtual {v0, p1}, Lmaps/al/i;->b(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ag;->w:Lmaps/al/i;

    invoke-virtual {v0, p1}, Lmaps/al/i;->b(Lmaps/cr/c;)V

    :cond_2
    iget-object v0, p0, Lmaps/l/ag;->x:Lmaps/al/o;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/l/ag;->x:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->b(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ag;->y:Lmaps/al/q;

    invoke-virtual {v0, p1}, Lmaps/al/q;->b(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ag;->z:Lmaps/al/i;

    invoke-virtual {v0, p1}, Lmaps/al/i;->b(Lmaps/cr/c;)V

    :cond_3
    sget-boolean v0, Lmaps/ae/h;->O:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lmaps/l/ag;->p:Lmaps/al/o;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lmaps/l/ag;->p:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->b(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ag;->q:Lmaps/al/q;

    invoke-virtual {v0, p1}, Lmaps/al/q;->b(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ag;->r:Lmaps/al/i;

    invoke-virtual {v0, p1}, Lmaps/al/i;->b(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ag;->s:Lmaps/al/a;

    invoke-virtual {v0, p1}, Lmaps/al/a;->a(Lmaps/cr/c;)V

    :cond_4
    return-void
.end method

.method public c()Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    sget-boolean v2, Lmaps/ae/h;->F:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lmaps/l/ag;->g:Lmaps/s/r;

    invoke-virtual {v2}, Lmaps/s/r;->b()I

    move-result v2

    if-lez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lmaps/l/ag;->j:Lmaps/al/o;

    invoke-virtual {v2}, Lmaps/al/o;->c()I

    move-result v2

    if-gtz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method d()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/l/ag;->E:Z

    return v0
.end method
