.class Lmaps/l/ai;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lmaps/al/o;

.field private final b:Lmaps/al/i;

.field private final c:Lmaps/al/q;

.field private final d:Lmaps/al/o;

.field private final e:Lmaps/al/q;

.field private final f:I


# direct methods
.method public constructor <init>(Lmaps/t/ax;Ljava/util/List;Ljava/util/List;Lmaps/l/ad;)V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lmaps/s/q;->a(Ljava/util/List;)I

    move-result v0

    new-instance v1, Lmaps/al/e;

    invoke-direct {v1, v0}, Lmaps/al/e;-><init>(I)V

    iput-object v1, p0, Lmaps/l/ai;->a:Lmaps/al/o;

    new-instance v1, Lmaps/al/k;

    invoke-direct {v1, v0}, Lmaps/al/k;-><init>(I)V

    iput-object v1, p0, Lmaps/l/ai;->c:Lmaps/al/q;

    new-instance v0, Lmaps/al/j;

    invoke-static {p2}, Lmaps/s/q;->b(Ljava/util/List;)I

    move-result v1

    invoke-direct {v0, v1}, Lmaps/al/j;-><init>(I)V

    iput-object v0, p0, Lmaps/l/ai;->b:Lmaps/al/i;

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/cg;

    invoke-direct {p0, p1, v0, p4}, Lmaps/l/ai;->a(Lmaps/t/ax;Lmaps/t/cg;Lmaps/l/ad;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/co;

    invoke-static {v0}, Lmaps/l/av;->a(Lmaps/t/co;)I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_1

    :cond_1
    invoke-virtual {p4}, Lmaps/l/ad;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    if-lez v1, :cond_2

    new-instance v0, Lmaps/al/e;

    invoke-direct {v0, v1}, Lmaps/al/e;-><init>(I)V

    iput-object v0, p0, Lmaps/l/ai;->d:Lmaps/al/o;

    new-instance v0, Lmaps/al/k;

    invoke-direct {v0, v1}, Lmaps/al/k;-><init>(I)V

    iput-object v0, p0, Lmaps/l/ai;->e:Lmaps/al/q;

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/co;

    invoke-direct {p0, p1, v0, p4}, Lmaps/l/ai;->a(Lmaps/t/ax;Lmaps/t/co;Lmaps/l/ad;)V

    goto :goto_2

    :cond_2
    iput-object v3, p0, Lmaps/l/ai;->d:Lmaps/al/o;

    iput-object v3, p0, Lmaps/l/ai;->e:Lmaps/al/q;

    :cond_3
    invoke-virtual {p4}, Lmaps/l/ad;->h()I

    move-result v0

    iput v0, p0, Lmaps/l/ai;->f:I

    return-void
.end method

.method private a(Lmaps/t/ax;Lmaps/t/cg;Lmaps/l/ad;)V
    .locals 11

    const/high16 v0, 0x10000

    const/4 v1, 0x0

    invoke-virtual {p1}, Lmaps/t/ax;->d()Lmaps/t/bx;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/t/bx;->f()I

    move-result v2

    invoke-virtual {p3}, Lmaps/l/ad;->d()F

    move-result v3

    float-to-int v3, v3

    add-int/2addr v2, v3

    invoke-virtual {p1}, Lmaps/t/ax;->d()Lmaps/t/bx;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/t/bx;->g()I

    move-result v3

    invoke-virtual {p3}, Lmaps/l/ad;->e()F

    move-result v4

    float-to-int v4, v4

    add-int/2addr v3, v4

    new-instance v4, Lmaps/t/bx;

    invoke-direct {v4, v2, v3}, Lmaps/t/bx;-><init>(II)V

    invoke-virtual {p1}, Lmaps/t/ax;->g()I

    move-result v5

    invoke-virtual {p3}, Lmaps/l/ad;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    move v6, v0

    :goto_0
    invoke-virtual {p3}, Lmaps/l/ad;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    move v7, v1

    :goto_1
    invoke-virtual {p3}, Lmaps/l/ad;->g()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_4

    invoke-virtual {p3}, Lmaps/l/ad;->g()F

    move-result v0

    invoke-virtual {p2, v0}, Lmaps/t/cg;->c(F)Lmaps/t/cg;

    move-result-object v1

    :goto_2
    invoke-static {}, Lmaps/s/q;->a()Lmaps/s/q;

    move-result-object v0

    invoke-virtual {p3}, Lmaps/l/ad;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p3}, Lmaps/l/ad;->b()F

    move-result v2

    :goto_3
    invoke-virtual {p3}, Lmaps/l/ad;->c()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p3}, Lmaps/l/ad;->a()F

    move-result v3

    :goto_4
    iget-object v8, p0, Lmaps/l/ai;->a:Lmaps/al/o;

    iget-object v9, p0, Lmaps/l/ai;->b:Lmaps/al/i;

    iget-object v10, p0, Lmaps/l/ai;->c:Lmaps/al/q;

    invoke-virtual/range {v0 .. v10}, Lmaps/s/q;->a(Lmaps/t/cg;FFLmaps/t/bx;IIILmaps/al/l;Lmaps/al/b;Lmaps/al/c;)V

    return-void

    :cond_0
    move v6, v1

    goto :goto_0

    :cond_1
    move v7, v0

    goto :goto_1

    :cond_2
    invoke-virtual {p3}, Lmaps/l/ad;->a()F

    move-result v2

    goto :goto_3

    :cond_3
    invoke-virtual {p3}, Lmaps/l/ad;->b()F

    move-result v3

    goto :goto_4

    :cond_4
    move-object v1, p2

    goto :goto_2
.end method

.method private a(Lmaps/t/ax;Lmaps/t/co;Lmaps/l/ad;)V
    .locals 11

    const/4 v6, 0x0

    invoke-virtual {p2}, Lmaps/t/co;->h()Lmaps/t/aa;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/aa;->d()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Lmaps/t/co;->c()Lmaps/t/ad;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/ad;->a()I

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {p1}, Lmaps/t/ax;->d()Lmaps/t/bx;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/t/bx;->f()I

    move-result v1

    invoke-virtual {p3}, Lmaps/l/ad;->d()F

    move-result v2

    float-to-int v2, v2

    add-int/2addr v1, v2

    invoke-virtual {p1}, Lmaps/t/ax;->d()Lmaps/t/bx;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/t/bx;->g()I

    move-result v2

    invoke-virtual {p3}, Lmaps/l/ad;->e()F

    move-result v3

    float-to-int v3, v3

    add-int/2addr v3, v2

    new-instance v2, Lmaps/t/bx;

    invoke-direct {v2, v1, v3}, Lmaps/t/bx;-><init>(II)V

    invoke-virtual {p1}, Lmaps/t/ax;->g()I

    move-result v9

    invoke-static {}, Lmaps/l/av;->c()Ljava/lang/ThreadLocal;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lmaps/t/bx;

    aget-object v3, v1, v6

    invoke-static {}, Lmaps/l/av;->c()Ljava/lang/ThreadLocal;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lmaps/t/bx;

    const/4 v4, 0x1

    aget-object v4, v1, v4

    invoke-static {}, Lmaps/l/av;->c()Ljava/lang/ThreadLocal;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lmaps/t/bx;

    const/4 v5, 0x2

    aget-object v5, v1, v5

    move v1, v6

    move v7, v6

    :goto_1
    if-ge v1, v8, :cond_2

    invoke-virtual/range {v0 .. v5}, Lmaps/t/ad;->a(ILmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    iget-object v10, p0, Lmaps/l/ai;->d:Lmaps/al/o;

    invoke-virtual {v10, v3, v9}, Lmaps/al/o;->a(Lmaps/t/bx;I)V

    iget-object v10, p0, Lmaps/l/ai;->d:Lmaps/al/o;

    invoke-virtual {v10, v4, v9}, Lmaps/al/o;->a(Lmaps/t/bx;I)V

    iget-object v10, p0, Lmaps/l/ai;->d:Lmaps/al/o;

    invoke-virtual {v10, v5, v9}, Lmaps/al/o;->a(Lmaps/t/bx;I)V

    add-int/lit8 v7, v7, 0x3

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {p3}, Lmaps/l/ad;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    const/high16 v0, 0x10000

    :goto_2
    iget-object v1, p0, Lmaps/l/ai;->e:Lmaps/al/q;

    invoke-virtual {v1, v0, v6, v7}, Lmaps/al/q;->a(III)V

    goto :goto_0

    :cond_3
    move v0, v6

    goto :goto_2
.end method


# virtual methods
.method public a()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lmaps/l/ai;->a:Lmaps/al/o;

    invoke-virtual {v0}, Lmaps/al/o;->d()I

    move-result v0

    iget-object v2, p0, Lmaps/l/ai;->b:Lmaps/al/i;

    invoke-virtual {v2}, Lmaps/al/i;->c()I

    move-result v2

    add-int/2addr v0, v2

    iget-object v2, p0, Lmaps/l/ai;->c:Lmaps/al/q;

    invoke-virtual {v2}, Lmaps/al/q;->d()I

    move-result v2

    add-int/2addr v2, v0

    iget-object v0, p0, Lmaps/l/ai;->d:Lmaps/al/o;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/l/ai;->d:Lmaps/al/o;

    invoke-virtual {v0}, Lmaps/al/o;->d()I

    move-result v0

    :goto_0
    add-int/2addr v0, v2

    iget-object v2, p0, Lmaps/l/ai;->e:Lmaps/al/q;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lmaps/l/ai;->e:Lmaps/al/q;

    invoke-virtual {v1}, Lmaps/al/q;->d()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public a(Lmaps/cr/c;)V
    .locals 1

    iget-object v0, p0, Lmaps/l/ai;->a:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->b(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ai;->b:Lmaps/al/i;

    invoke-virtual {v0, p1}, Lmaps/al/i;->b(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ai;->c:Lmaps/al/q;

    invoke-virtual {v0, p1}, Lmaps/al/q;->b(Lmaps/cr/c;)V

    return-void
.end method

.method public a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 5

    const/4 v4, 0x4

    const/high16 v3, 0x10000

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    iget v1, p0, Lmaps/l/ai;->f:I

    invoke-static {v0, v1}, Lmaps/s/k;->a(Ljavax/microedition/khronos/opengles/GL10;I)V

    invoke-virtual {p1}, Lmaps/cr/c;->a()Lmaps/s/i;

    move-result-object v0

    const/16 v1, 0x17

    invoke-virtual {v0, v1}, Lmaps/s/i;->a(I)Lmaps/cr/a;

    move-result-object v0

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/cr/a;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    iget-object v0, p0, Lmaps/l/ai;->a:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->d(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ai;->c:Lmaps/al/q;

    invoke-virtual {v0, p1}, Lmaps/al/q;->d(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ai;->b:Lmaps/al/i;

    invoke-virtual {v0, p1, v4}, Lmaps/al/i;->a(Lmaps/cr/c;I)V

    iget-object v0, p0, Lmaps/l/ai;->d:Lmaps/al/o;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/l/ai;->e:Lmaps/al/q;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/l/ai;->d:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->d(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ai;->e:Lmaps/al/q;

    invoke-virtual {v0, p1}, Lmaps/al/q;->d(Lmaps/cr/c;)V

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lmaps/l/ai;->d:Lmaps/al/o;

    invoke-virtual {v2}, Lmaps/al/o;->c()I

    move-result v2

    invoke-interface {v0, v4, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    :cond_0
    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0, v3, v3, v3, v3}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    return-void
.end method

.method public b()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lmaps/l/ai;->a:Lmaps/al/o;

    invoke-virtual {v0}, Lmaps/al/o;->a()I

    move-result v0

    add-int/lit8 v0, v0, 0x24

    iget-object v2, p0, Lmaps/l/ai;->b:Lmaps/al/i;

    invoke-virtual {v2}, Lmaps/al/i;->d()I

    move-result v2

    add-int/2addr v0, v2

    iget-object v2, p0, Lmaps/l/ai;->c:Lmaps/al/q;

    invoke-virtual {v2}, Lmaps/al/q;->a()I

    move-result v2

    add-int/2addr v2, v0

    iget-object v0, p0, Lmaps/l/ai;->d:Lmaps/al/o;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/l/ai;->d:Lmaps/al/o;

    invoke-virtual {v0}, Lmaps/al/o;->a()I

    move-result v0

    :goto_0
    add-int/2addr v0, v2

    iget-object v2, p0, Lmaps/l/ai;->e:Lmaps/al/q;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lmaps/l/ai;->e:Lmaps/al/q;

    invoke-virtual {v1}, Lmaps/al/q;->a()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public b(Lmaps/cr/c;)V
    .locals 1

    iget-object v0, p0, Lmaps/l/ai;->a:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->e(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ai;->b:Lmaps/al/i;

    invoke-virtual {v0, p1}, Lmaps/al/i;->c(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ai;->c:Lmaps/al/q;

    invoke-virtual {v0, p1}, Lmaps/al/q;->e(Lmaps/cr/c;)V

    return-void
.end method
