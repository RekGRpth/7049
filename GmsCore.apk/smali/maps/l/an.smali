.class public Lmaps/l/an;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/l/al;


# instance fields
.field private final a:Lmaps/t/ah;

.field private final b:Lmaps/t/ax;

.field private final c:Lmaps/al/q;


# direct methods
.method public constructor <init>(Lmaps/t/ah;I)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lmaps/al/q;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lmaps/al/q;-><init>(I)V

    iput-object v0, p0, Lmaps/l/an;->c:Lmaps/al/q;

    iput-object p1, p0, Lmaps/l/an;->a:Lmaps/t/ah;

    iget-object v0, p0, Lmaps/l/an;->a:Lmaps/t/ah;

    invoke-virtual {v0}, Lmaps/t/ah;->j()Lmaps/t/ax;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/an;->b:Lmaps/t/ax;

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-nez v0, :cond_0

    const/high16 v0, 0x10000

    mul-int/2addr v0, p2

    div-int/lit8 v0, v0, 0x20

    iget-object v1, p0, Lmaps/l/an;->c:Lmaps/al/q;

    invoke-virtual {v1, v2, v2}, Lmaps/al/q;->a(II)V

    iget-object v1, p0, Lmaps/l/an;->c:Lmaps/al/q;

    invoke-virtual {v1, v2, v0}, Lmaps/al/q;->a(II)V

    iget-object v1, p0, Lmaps/l/an;->c:Lmaps/al/q;

    invoke-virtual {v1, v0, v2}, Lmaps/al/q;->a(II)V

    iget-object v1, p0, Lmaps/l/an;->c:Lmaps/al/q;

    invoke-virtual {v1, v0, v0}, Lmaps/al/q;->a(II)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lmaps/bq/d;Lmaps/af/q;)I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public a(J)V
    .locals 0

    return-void
.end method

.method public a(Lmaps/ac/g;)V
    .locals 0

    return-void
.end method

.method public a(Lmaps/bq/d;ILjava/util/Collection;)V
    .locals 0

    return-void
.end method

.method public a(Lmaps/bq/d;Ljava/util/Collection;)V
    .locals 0

    return-void
.end method

.method public a(Lmaps/bq/d;Lmaps/cr/c;)V
    .locals 0

    return-void
.end method

.method public a(Lmaps/cr/c;)V
    .locals 0

    invoke-virtual {p0, p1}, Lmaps/l/an;->c(Lmaps/cr/c;)V

    return-void
.end method

.method public a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 4

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    iget-object v1, p0, Lmaps/l/an;->b:Lmaps/t/ax;

    invoke-virtual {v1}, Lmaps/t/ax;->d()Lmaps/t/bx;

    move-result-object v1

    iget-object v2, p0, Lmaps/l/an;->b:Lmaps/t/ax;

    invoke-virtual {v2}, Lmaps/t/ax;->g()I

    move-result v2

    int-to-float v2, v2

    invoke-static {p1, p2, v1, v2}, Lmaps/af/d;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/t/bx;F)V

    iget-object v1, p0, Lmaps/l/an;->c:Lmaps/al/q;

    invoke-virtual {v1, p1}, Lmaps/al/q;->d(Lmaps/cr/c;)V

    const/4 v1, 0x5

    const/4 v2, 0x0

    const/4 v3, 0x4

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto :goto_0
.end method

.method public a(Lmaps/l/u;)V
    .locals 0

    return-void
.end method

.method public a(Z)V
    .locals 0

    return-void
.end method

.method public a()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public a(Lmaps/ae/d;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public a(Lmaps/p/ac;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b()Lmaps/t/ah;
    .locals 1

    iget-object v0, p0, Lmaps/l/an;->a:Lmaps/t/ah;

    return-object v0
.end method

.method public b(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 4

    const/4 v2, 0x1

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {p3}, Lmaps/af/s;->b()I

    move-result v0

    if-gt v0, v2, :cond_0

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x303

    invoke-interface {v0, v2, v1}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x1e01

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    invoke-virtual {p1}, Lmaps/cr/c;->r()V

    iget-object v1, p1, Lmaps/cr/c;->f:Lmaps/al/o;

    invoke-virtual {v1, p1}, Lmaps/al/o;->d(Lmaps/cr/c;)V

    invoke-virtual {p1}, Lmaps/cr/c;->a()Lmaps/s/i;

    move-result-object v1

    const/16 v2, 0x14

    invoke-virtual {v1, v2}, Lmaps/s/i;->a(I)Lmaps/cr/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmaps/cr/a;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    goto :goto_0
.end method

.method public b(Lmaps/ae/d;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public c(Lmaps/cr/c;)V
    .locals 0

    return-void
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public d()V
    .locals 0

    return-void
.end method

.method public e()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public f()I
    .locals 1

    iget-object v0, p0, Lmaps/l/an;->c:Lmaps/al/q;

    invoke-virtual {v0}, Lmaps/al/q;->a()I

    move-result v0

    return v0
.end method

.method public g()I
    .locals 1

    iget-object v0, p0, Lmaps/l/an;->c:Lmaps/al/q;

    invoke-virtual {v0}, Lmaps/al/q;->d()I

    move-result v0

    return v0
.end method

.method public h()Lmaps/o/c;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public i()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public j()Lmaps/l/u;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method
