.class public Lmaps/l/ao;
.super Lmaps/l/u;


# instance fields
.field private final A:F

.field private final B:F

.field private final C:F

.field private D:I

.field private E:I

.field private F:F

.field private final G:I

.field private H:Z

.field private I:I

.field private J:Z

.field private final K:Ljava/lang/String;

.field private final L:F

.field private final M:[F

.field private final l:Ljava/lang/String;

.field private final m:Lmaps/p/l;

.field private final n:Lmaps/t/cg;

.field private o:Lmaps/t/cg;

.field private final p:F

.field private q:Lmaps/t/j;

.field private r:[Lmaps/l/aq;

.field private final s:Lmaps/p/y;

.field private t:Lmaps/cr/a;

.field private u:Lmaps/al/q;

.field private v:Lmaps/al/q;

.field private w:Z

.field private x:Lmaps/al/q;

.field private y:Lmaps/al/o;

.field private z:Lmaps/w/v;


# direct methods
.method constructor <init>(Lmaps/t/ca;Lmaps/ac/g;Ljava/lang/String;Lmaps/t/aa;IIZFFFILmaps/t/cg;FFLmaps/p/l;FLmaps/p/y;Z)V
    .locals 10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p4

    move/from16 v5, p8

    move/from16 v6, p9

    move v7, p5

    move/from16 v8, p7

    move/from16 v9, p18

    invoke-direct/range {v1 .. v9}, Lmaps/l/u;-><init>(Lmaps/t/ca;Lmaps/ac/g;Lmaps/t/aa;FFIZZ)V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lmaps/l/ao;->w:Z

    const/16 v1, 0x8

    new-array v1, v1, [F

    iput-object v1, p0, Lmaps/l/ao;->M:[F

    iput-object p3, p0, Lmaps/l/ao;->l:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "L"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lmaps/l/ao;->l:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lmaps/l/ao;->K:Ljava/lang/String;

    move-object/from16 v0, p12

    iput-object v0, p0, Lmaps/l/ao;->n:Lmaps/t/cg;

    mul-float v1, p16, p10

    iput v1, p0, Lmaps/l/ao;->p:F

    move/from16 v0, p6

    iput v0, p0, Lmaps/l/ao;->G:I

    move-object/from16 v0, p17

    iput-object v0, p0, Lmaps/l/ao;->s:Lmaps/p/y;

    move/from16 v0, p10

    iput v0, p0, Lmaps/l/ao;->A:F

    move/from16 v0, p13

    iput v0, p0, Lmaps/l/ao;->B:F

    move/from16 v0, p14

    iput v0, p0, Lmaps/l/ao;->C:F

    move-object/from16 v0, p15

    iput-object v0, p0, Lmaps/l/ao;->m:Lmaps/p/l;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lmaps/l/ao;->J:Z

    move/from16 v0, p11

    iput v0, p0, Lmaps/l/ao;->E:I

    const/4 v1, 0x0

    iput v1, p0, Lmaps/l/ao;->I:I

    mul-float v1, p16, p13

    iput v1, p0, Lmaps/l/ao;->L:F

    return-void
.end method

.method static a(F[FI)I
    .locals 4

    const/4 v3, 0x0

    aget v0, p1, p2

    sub-float v0, p0, v0

    cmpg-float v1, v0, v3

    if-gtz v1, :cond_2

    :cond_0
    :goto_0
    return p2

    :cond_1
    add-int/lit8 p2, p2, 0x1

    move v0, v1

    :cond_2
    cmpl-float v1, v0, v3

    if-lez v1, :cond_3

    array-length v1, p1

    add-int/lit8 v1, v1, -0x1

    if-ge p2, v1, :cond_3

    add-int/lit8 v1, p2, 0x1

    aget v1, p1, v1

    sub-float v1, p0, v1

    cmpg-float v2, v1, v3

    if-gtz v2, :cond_1

    neg-float v1, v1

    cmpg-float v0, v1, v0

    if-gez v0, :cond_0

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_3
    array-length v0, p1

    add-int/lit8 p2, v0, -0x1

    goto :goto_0
.end method

.method public static a(Lmaps/t/aa;ZLmaps/af/q;)I
    .locals 3

    if-eqz p1, :cond_1

    sget-object v0, Lmaps/af/q;->a:Lmaps/af/q;

    if-ne p2, v0, :cond_1

    invoke-virtual {p0}, Lmaps/t/aa;->c()I

    move-result v0

    if-lez v0, :cond_1

    sget-boolean v0, Lmaps/ae/h;->O:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmaps/t/aa;->c()I

    move-result v0

    const/4 v1, 0x2

    if-gt v0, v1, :cond_1

    :cond_0
    invoke-virtual {p0}, Lmaps/t/aa;->c()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lmaps/t/aa;->b(I)Lmaps/t/ac;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/ac;->b()I

    move-result v0

    invoke-static {v0}, Lmaps/s/k;->a(I)I

    move-result v1

    const/16 v2, 0x80

    if-lt v1, v2, :cond_1

    :goto_0
    return v0

    :cond_1
    invoke-static {p0, p2}, Lmaps/l/u;->b(Lmaps/t/aa;Lmaps/af/q;)I

    move-result v0

    goto :goto_0
.end method

.method static a(Lmaps/t/cg;)I
    .locals 3

    sget-object v0, Lmaps/af/v;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/s/p;

    iget-object v1, v0, Lmaps/s/p;->a:Lmaps/t/bx;

    iget-object v0, v0, Lmaps/s/p;->b:Lmaps/t/bx;

    const/4 v2, 0x0

    invoke-virtual {p0, v2, v1}, Lmaps/t/cg;->a(ILmaps/t/bx;)V

    invoke-virtual {p0, v0}, Lmaps/t/cg;->a(Lmaps/t/bx;)V

    invoke-static {v1, v0}, Lmaps/t/cb;->b(Lmaps/t/bx;Lmaps/t/bx;)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public static a(Lmaps/t/az;Lmaps/ac/g;Lmaps/t/bh;Lmaps/t/cg;ZFLmaps/p/l;FLmaps/bq/d;Lmaps/p/y;Z)Lmaps/l/ao;
    .locals 12

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p7

    move-object/from16 v8, p6

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move/from16 v11, p10

    invoke-static/range {v0 .. v11}, Lmaps/l/ao;->a(Lmaps/t/ca;Lmaps/ac/g;Lmaps/t/bh;Lmaps/t/cg;IZFFLmaps/p/l;Lmaps/bq/d;Lmaps/p/y;Z)Lmaps/l/ao;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lmaps/t/bu;Lmaps/ac/g;Lmaps/t/bh;Lmaps/t/cg;IZFFLmaps/p/l;Lmaps/bq/d;Lmaps/p/y;Z)Lmaps/l/ao;
    .locals 1

    invoke-static/range {p0 .. p11}, Lmaps/l/ao;->a(Lmaps/t/ca;Lmaps/ac/g;Lmaps/t/bh;Lmaps/t/cg;IZFFLmaps/p/l;Lmaps/bq/d;Lmaps/p/y;Z)Lmaps/l/ao;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lmaps/t/ca;Lmaps/ac/g;Lmaps/t/bh;Lmaps/t/cg;IZFFLmaps/p/l;Lmaps/bq/d;Lmaps/p/y;Z)Lmaps/l/ao;
    .locals 22

    invoke-virtual/range {p9 .. p9}, Lmaps/bq/d;->z()F

    move-result v13

    invoke-virtual/range {p3 .. p3}, Lmaps/t/cg;->b()I

    move-result v3

    const/4 v4, 0x2

    if-le v3, v4, :cond_6

    mul-float v3, p6, v13

    const v4, 0x3e4ccccd

    mul-float/2addr v3, v4

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Lmaps/t/cg;->b(F)Lmaps/t/cg;

    move-result-object v15

    :goto_0
    invoke-interface/range {p0 .. p0}, Lmaps/t/ca;->h()Lmaps/t/aa;

    move-result-object v7

    const/4 v3, 0x0

    :goto_1
    invoke-virtual/range {p2 .. p2}, Lmaps/t/bh;->b()I

    move-result v4

    if-ge v3, v4, :cond_0

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lmaps/t/bh;->a(I)Lmaps/t/a;

    move-result-object v4

    invoke-virtual {v4}, Lmaps/t/a;->a()Z

    move-result v4

    if-eqz v4, :cond_2

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lmaps/t/bh;->a(I)Lmaps/t/a;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/t/a;->j()Lmaps/t/aa;

    move-result-object v7

    :cond_0
    invoke-virtual/range {p2 .. p2}, Lmaps/t/bh;->a()Ljava/lang/String;

    move-result-object v6

    if-eqz v7, :cond_3

    invoke-virtual {v7}, Lmaps/t/aa;->j()Lmaps/t/cv;

    move-result-object v3

    :goto_2
    move-object/from16 v0, p10

    move-object/from16 v1, p8

    move/from16 v2, p6

    invoke-virtual {v0, v6, v1, v3, v2}, Lmaps/p/y;->a(Ljava/lang/String;Lmaps/p/l;Lmaps/t/cv;F)F

    move-result v19

    const/high16 v3, 0x3f800000

    add-float v3, v3, v19

    invoke-virtual/range {p9 .. p9}, Lmaps/bq/d;->p()F

    move-result v4

    move-object/from16 v0, p9

    invoke-virtual {v0, v3, v4}, Lmaps/bq/d;->a(FF)F

    move-result v3

    const/4 v4, 0x0

    cmpl-float v4, v19, v4

    if-nez v4, :cond_4

    const/4 v3, 0x0

    :cond_1
    :goto_3
    return-object v3

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    goto :goto_2

    :cond_4
    invoke-virtual {v15}, Lmaps/t/cg;->d()F

    move-result v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_5

    const/4 v3, 0x0

    goto :goto_3

    :cond_5
    new-instance v3, Lmaps/l/ao;

    invoke-interface/range {p0 .. p0}, Lmaps/t/ca;->i()I

    move-result v8

    const/high16 v11, -0x40800000

    const/high16 v12, -0x40800000

    invoke-virtual/range {p9 .. p9}, Lmaps/bq/d;->q()F

    move-result v4

    float-to-int v14, v4

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move/from16 v9, p4

    move/from16 v10, p5

    move/from16 v16, p6

    move/from16 v17, p7

    move-object/from16 v18, p8

    move-object/from16 v20, p10

    move/from16 v21, p11

    invoke-direct/range {v3 .. v21}, Lmaps/l/ao;-><init>(Lmaps/t/ca;Lmaps/ac/g;Ljava/lang/String;Lmaps/t/aa;IIZFFFILmaps/t/cg;FFLmaps/p/l;FLmaps/p/y;Z)V

    invoke-virtual {v3}, Lmaps/l/ao;->e()Z

    move-result v4

    if-nez v4, :cond_1

    const/4 v3, 0x0

    goto :goto_3

    :cond_6
    move-object/from16 v15, p3

    goto/16 :goto_0
.end method

.method static a(Lmaps/t/cg;FF)Lmaps/t/cg;
    .locals 12

    sget-object v0, Lmaps/af/v;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/s/p;

    iget-object v6, v0, Lmaps/s/p;->i:Lmaps/t/bx;

    iget-object v7, v0, Lmaps/s/p;->j:Lmaps/t/bx;

    iget-object v8, v0, Lmaps/s/p;->k:Lmaps/t/bx;

    iget-object v9, v0, Lmaps/s/p;->l:Lmaps/t/bx;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lmaps/t/cg;->b()I

    move-result v2

    add-int/lit8 v10, v2, -0x1

    const/4 v2, 0x0

    move v3, p2

    :goto_0
    if-ge v2, v10, :cond_0

    invoke-virtual {p0, v2}, Lmaps/t/cg;->b(I)F

    move-result v4

    sub-float/2addr p1, v4

    const v5, 0x38d1b717

    cmpg-float v5, p1, v5

    if-gtz v5, :cond_1

    const v5, -0x472e48e9

    cmpg-float v5, p1, v5

    if-gez v5, :cond_0

    const/4 v0, 0x1

    const/high16 v5, 0x3f800000

    div-float v4, p1, v4

    add-float/2addr v4, v5

    invoke-virtual {p0, v2, v9}, Lmaps/t/cg;->a(ILmaps/t/bx;)V

    add-int/lit8 v5, v2, 0x1

    invoke-virtual {p0, v5, v8}, Lmaps/t/cg;->a(ILmaps/t/bx;)V

    invoke-static {v9, v8, v4, v6}, Lmaps/t/bx;->a(Lmaps/t/bx;Lmaps/t/bx;FLmaps/t/bx;)V

    :cond_0
    move v4, v2

    :goto_1
    if-ge v4, v10, :cond_8

    invoke-virtual {p0, v4}, Lmaps/t/cg;->b(I)F

    move-result v11

    sub-float v5, v3, v11

    const v3, 0x38d1b717

    cmpg-float v3, v5, v3

    if-gtz v3, :cond_2

    const v3, 0x38d1b717

    cmpg-float v3, v5, v3

    if-gez v3, :cond_8

    const/4 v1, 0x1

    const/high16 v3, 0x3f800000

    div-float/2addr v5, v11

    add-float/2addr v3, v5

    invoke-virtual {p0, v4, v9}, Lmaps/t/cg;->a(ILmaps/t/bx;)V

    add-int/lit8 v5, v4, 0x1

    invoke-virtual {p0, v5, v8}, Lmaps/t/cg;->a(ILmaps/t/bx;)V

    invoke-static {v9, v8, v3, v7}, Lmaps/t/bx;->a(Lmaps/t/bx;Lmaps/t/bx;FLmaps/t/bx;)V

    move v3, v1

    :goto_2
    add-int/lit8 v2, v2, 0x1

    sub-int v1, v4, v2

    add-int/lit8 v5, v1, 0x1

    if-eqz v0, :cond_3

    const/4 v1, 0x1

    :goto_3
    add-int/2addr v5, v1

    if-eqz v3, :cond_4

    const/4 v1, 0x1

    :goto_4
    add-int/2addr v1, v5

    mul-int/lit8 v1, v1, 0x3

    new-array v5, v1, [I

    const/4 v1, 0x0

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    invoke-virtual {v6, v5, v1}, Lmaps/t/bx;->a([II)V

    :goto_5
    move v1, v0

    move v0, v2

    :goto_6
    if-gt v0, v4, :cond_5

    invoke-virtual {p0, v0, v8}, Lmaps/t/cg;->a(ILmaps/t/bx;)V

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {v8, v5, v1}, Lmaps/t/bx;->a([II)V

    add-int/lit8 v0, v0, 0x1

    move v1, v2

    goto :goto_6

    :cond_1
    sub-float/2addr v3, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v5

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_3

    :cond_4
    const/4 v1, 0x0

    goto :goto_4

    :cond_5
    if-eqz v3, :cond_6

    add-int/lit8 v0, v1, 0x1

    invoke-virtual {v7, v5, v1}, Lmaps/t/bx;->a([II)V

    :cond_6
    invoke-static {v5}, Lmaps/t/cg;->b([I)Lmaps/t/cg;

    move-result-object v0

    return-object v0

    :cond_7
    move v0, v1

    goto :goto_5

    :cond_8
    move v3, v1

    goto :goto_2
.end method

.method private a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/l/aq;F)V
    .locals 7

    const/high16 v6, 0x3f800000

    const/4 v5, 0x0

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    sget-object v0, Lmaps/af/v;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/s/p;

    iget-object v2, v0, Lmaps/s/p;->a:Lmaps/t/bx;

    iget-object v0, v0, Lmaps/s/p;->b:Lmaps/t/bx;

    invoke-virtual {p2, v0}, Lmaps/bq/d;->a(Lmaps/t/bx;)V

    iget-object v3, p3, Lmaps/l/aq;->a:Lmaps/t/bx;

    invoke-static {v3, v0, v2}, Lmaps/t/bx;->b(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    invoke-virtual {p2}, Lmaps/bq/d;->x()F

    move-result v0

    invoke-virtual {v2}, Lmaps/t/bx;->f()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v0

    invoke-virtual {v2}, Lmaps/t/bx;->g()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v0

    invoke-virtual {v2}, Lmaps/t/bx;->h()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v0

    invoke-interface {v1, v3, v4, v2}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    const/high16 v2, 0x42b40000

    iget v3, p3, Lmaps/l/aq;->d:F

    sub-float/2addr v2, v3

    invoke-interface {v1, v2, v5, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    cmpl-float v2, p4, v5

    if-eqz v2, :cond_0

    invoke-interface {v1, p4, v6, v5, v5}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    :cond_0
    iget v2, p3, Lmaps/l/aq;->b:F

    mul-float/2addr v2, v0

    iget v3, p3, Lmaps/l/aq;->c:F

    mul-float/2addr v0, v3

    invoke-interface {v1, v2, v0, v6}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    const/4 v0, 0x5

    const/4 v2, 0x0

    const/4 v3, 0x4

    invoke-interface {v1, v0, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    return-void
.end method

.method private a(Lmaps/cr/c;Lmaps/t/cg;F)V
    .locals 11

    sget-object v0, Lmaps/af/v;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/s/p;

    iget-object v2, v0, Lmaps/s/p;->a:Lmaps/t/bx;

    iget-object v1, v0, Lmaps/s/p;->b:Lmaps/t/bx;

    invoke-virtual {p2}, Lmaps/t/cg;->b()I

    move-result v6

    add-int/lit8 v3, v6, -0x1

    mul-int/lit8 v0, v3, 0x4

    const/4 v4, 0x1

    new-instance v5, Lmaps/al/k;

    invoke-direct {v5, v0, v4}, Lmaps/al/k;-><init>(IZ)V

    iput-object v5, p0, Lmaps/l/ao;->u:Lmaps/al/q;

    new-instance v5, Lmaps/al/k;

    invoke-direct {v5, v0, v4}, Lmaps/al/k;-><init>(IZ)V

    iput-object v5, p0, Lmaps/l/ao;->v:Lmaps/al/q;

    new-array v0, v3, [Lmaps/l/aq;

    iput-object v0, p0, Lmaps/l/ao;->r:[Lmaps/l/aq;

    new-array v7, v6, [F

    const/4 v0, 0x0

    const/4 v4, 0x0

    aput v4, v7, v0

    const/4 v0, 0x0

    invoke-virtual {p2, v0, v2}, Lmaps/t/cg;->a(ILmaps/t/bx;)V

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    add-int/lit8 v4, v0, 0x1

    invoke-virtual {p2, v4, v1}, Lmaps/t/cg;->a(ILmaps/t/bx;)V

    iget-object v4, p0, Lmaps/l/ao;->r:[Lmaps/l/aq;

    new-instance v5, Lmaps/l/aq;

    const/4 v8, 0x0

    invoke-direct {v5, v2, v1, p3, v8}, Lmaps/l/aq;-><init>(Lmaps/t/bx;Lmaps/t/bx;FLmaps/l/i;)V

    aput-object v5, v4, v0

    invoke-virtual {v2, v1}, Lmaps/t/bx;->c(Lmaps/t/bx;)F

    move-result v4

    add-int/lit8 v5, v0, 0x1

    aget v8, v7, v0

    add-float/2addr v4, v8

    aput v4, v7, v5

    add-int/lit8 v0, v0, 0x1

    move-object v10, v2

    move-object v2, v1

    move-object v1, v10

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmaps/l/ao;->s:Lmaps/p/y;

    iget-object v1, p0, Lmaps/l/ao;->l:Ljava/lang/String;

    iget-object v2, p0, Lmaps/l/ao;->m:Lmaps/p/l;

    iget-object v3, p0, Lmaps/l/ao;->b:Lmaps/t/aa;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lmaps/l/ao;->b:Lmaps/t/aa;

    invoke-virtual {v3}, Lmaps/t/aa;->j()Lmaps/t/cv;

    move-result-object v3

    :goto_1
    iget v4, p0, Lmaps/l/ao;->B:F

    const/4 v5, 0x1

    invoke-virtual/range {v0 .. v5}, Lmaps/p/y;->b(Ljava/lang/String;Lmaps/p/l;Lmaps/t/cv;FZ)[F

    move-result-object v3

    const/high16 v0, 0x3f800000

    array-length v1, v3

    add-int/lit8 v1, v1, -0x1

    aget v1, v3, v1

    div-float v1, v0, v1

    const/4 v0, 0x0

    :goto_2
    array-length v2, v3

    if-ge v0, v2, :cond_2

    aget v2, v3, v0

    mul-float/2addr v2, v1

    aput v2, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    :cond_2
    const/high16 v0, 0x3f800000

    array-length v1, v7

    add-int/lit8 v1, v1, -0x1

    aget v1, v7, v1

    div-float v1, v0, v1

    new-array v4, v6, [F

    const/4 v0, 0x0

    :goto_3
    if-ge v0, v6, :cond_3

    aget v2, v7, v0

    mul-float/2addr v2, v1

    aput v2, v7, v0

    add-int/lit8 v2, v6, -0x1

    sub-int/2addr v2, v0

    const/high16 v5, 0x3f800000

    aget v8, v7, v0

    sub-float/2addr v5, v8

    aput v5, v4, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v6, :cond_4

    aget v5, v7, v0

    invoke-static {v5, v3, v2}, Lmaps/l/ao;->a(F[FI)I

    move-result v2

    aget v5, v3, v2

    aput v5, v7, v0

    aget v5, v4, v0

    invoke-static {v5, v3, v1}, Lmaps/l/ao;->a(F[FI)I

    move-result v1

    aget v5, v3, v1

    aput v5, v4, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_4
    iget-object v0, p0, Lmaps/l/ao;->t:Lmaps/cr/a;

    invoke-virtual {v0}, Lmaps/cr/a;->b()F

    move-result v1

    iget-object v0, p0, Lmaps/l/ao;->t:Lmaps/cr/a;

    invoke-virtual {v0}, Lmaps/cr/a;->c()F

    move-result v2

    const/4 v0, 0x0

    :goto_5
    if-ge v0, v6, :cond_6

    aget v3, v7, v0

    mul-float/2addr v3, v1

    sub-int v5, v6, v0

    add-int/lit8 v5, v5, -0x1

    aget v5, v4, v5

    mul-float/2addr v5, v1

    iget-object v8, p0, Lmaps/l/ao;->u:Lmaps/al/q;

    const/4 v9, 0x0

    invoke-virtual {v8, v3, v9}, Lmaps/al/q;->a(FF)V

    iget-object v8, p0, Lmaps/l/ao;->u:Lmaps/al/q;

    invoke-virtual {v8, v3, v2}, Lmaps/al/q;->a(FF)V

    iget-object v8, p0, Lmaps/l/ao;->v:Lmaps/al/q;

    invoke-virtual {v8, v5, v2}, Lmaps/al/q;->a(FF)V

    iget-object v8, p0, Lmaps/l/ao;->v:Lmaps/al/q;

    const/4 v9, 0x0

    invoke-virtual {v8, v5, v9}, Lmaps/al/q;->a(FF)V

    if-lez v0, :cond_5

    iget-object v5, p0, Lmaps/l/ao;->r:[Lmaps/l/aq;

    add-int/lit8 v8, v0, -0x1

    aget-object v5, v5, v8

    iput v3, v5, Lmaps/l/aq;->e:F

    iget-object v3, p0, Lmaps/l/ao;->r:[Lmaps/l/aq;

    add-int/lit8 v5, v0, -0x1

    aget-object v3, v3, v5

    iput v2, v3, Lmaps/l/aq;->f:F

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_6
    return-void
.end method

.method private b(F)V
    .locals 12

    const/4 v1, 0x1

    const/4 v11, 0x0

    sget-object v0, Lmaps/af/v;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/s/p;

    iget-object v2, v0, Lmaps/s/p;->a:Lmaps/t/bx;

    iget-object v3, v0, Lmaps/s/p;->b:Lmaps/t/bx;

    iget-object v4, v0, Lmaps/s/p;->c:Lmaps/t/bx;

    iget-object v5, v0, Lmaps/s/p;->d:Lmaps/t/bx;

    iget-object v0, p0, Lmaps/l/ao;->o:Lmaps/t/cg;

    invoke-virtual {v0}, Lmaps/t/cg;->b()I

    move-result v6

    iget v0, p0, Lmaps/l/ao;->A:F

    iget v7, p0, Lmaps/l/ao;->B:F

    mul-float/2addr v0, v7

    mul-float/2addr v0, p1

    const/high16 v7, 0x40000000

    div-float v7, v0, v7

    mul-int/lit8 v0, v6, 0x2

    new-array v8, v0, [Lmaps/t/bx;

    iget-object v0, p0, Lmaps/l/ao;->o:Lmaps/t/cg;

    invoke-virtual {v0, v11, v5}, Lmaps/t/cg;->a(ILmaps/t/bx;)V

    move v0, v1

    :goto_0
    if-ge v0, v6, :cond_1

    iget-object v9, p0, Lmaps/l/ao;->o:Lmaps/t/cg;

    invoke-virtual {v9, v0, v4}, Lmaps/t/cg;->a(ILmaps/t/bx;)V

    invoke-static {v4, v5, v2}, Lmaps/t/bx;->b(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    invoke-static {v2, v7, v3}, Lmaps/t/cb;->a(Lmaps/t/bx;FLmaps/t/bx;)V

    invoke-virtual {v4, v3}, Lmaps/t/bx;->f(Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v9

    aput-object v9, v8, v0

    mul-int/lit8 v9, v6, 0x2

    sub-int/2addr v9, v0

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v4, v3}, Lmaps/t/bx;->e(Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v10

    aput-object v10, v8, v9

    if-ne v0, v1, :cond_0

    invoke-virtual {v5, v3}, Lmaps/t/bx;->f(Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v9

    aput-object v9, v8, v11

    mul-int/lit8 v9, v6, 0x2

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v5, v3}, Lmaps/t/bx;->e(Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v10

    aput-object v10, v8, v9

    :cond_0
    invoke-virtual {v5, v4}, Lmaps/t/bx;->b(Lmaps/t/bx;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    new-instance v0, Lmaps/t/j;

    invoke-direct {v0, v8}, Lmaps/t/j;-><init>([Lmaps/t/bx;)V

    iput-object v0, p0, Lmaps/l/ao;->q:Lmaps/t/j;

    return-void
.end method

.method static b(Lmaps/t/cg;)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0}, Lmaps/t/cg;->b()I

    move-result v2

    add-int/lit8 v3, v2, -0x1

    const/4 v2, 0x2

    if-ge v3, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, v0}, Lmaps/t/cg;->d(I)F

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_0

    invoke-virtual {p0, v2}, Lmaps/t/cg;->d(I)F

    move-result v5

    sub-float/2addr v5, v4

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const/high16 v6, 0x42700000

    cmpl-float v6, v5, v6

    if-lez v6, :cond_2

    const/high16 v6, 0x43960000

    cmpg-float v5, v5, v6

    if-gez v5, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private d(Lmaps/cr/c;)V
    .locals 7

    const/4 v4, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lmaps/l/ao;->t:Lmaps/cr/a;

    invoke-virtual {v0}, Lmaps/cr/a;->e()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lmaps/l/ao;->A:F

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f000000

    mul-float/2addr v0, v1

    const/high16 v1, 0x3fc00000

    div-float v1, v0, v1

    iget-object v0, p0, Lmaps/l/ao;->o:Lmaps/t/cg;

    invoke-virtual {v0}, Lmaps/t/cg;->b()I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    sget-object v0, Lmaps/af/v;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/s/p;

    iget-object v2, v0, Lmaps/s/p;->a:Lmaps/t/bx;

    iget-object v0, v0, Lmaps/s/p;->b:Lmaps/t/bx;

    iget-object v3, p0, Lmaps/l/ao;->o:Lmaps/t/cg;

    invoke-virtual {v3, v6, v2}, Lmaps/t/cg;->a(ILmaps/t/bx;)V

    iget-object v3, p0, Lmaps/l/ao;->o:Lmaps/t/cg;

    invoke-virtual {v3, v4, v0}, Lmaps/t/cg;->a(ILmaps/t/bx;)V

    new-array v3, v4, [Lmaps/l/aq;

    iput-object v3, p0, Lmaps/l/ao;->r:[Lmaps/l/aq;

    iget-object v3, p0, Lmaps/l/ao;->r:[Lmaps/l/aq;

    new-instance v4, Lmaps/l/aq;

    const/4 v5, 0x0

    invoke-direct {v4, v2, v0, v1, v5}, Lmaps/l/aq;-><init>(Lmaps/t/bx;Lmaps/t/bx;FLmaps/l/i;)V

    aput-object v4, v3, v6

    iget-object v0, p0, Lmaps/l/ao;->t:Lmaps/cr/a;

    invoke-virtual {v0}, Lmaps/cr/a;->b()F

    move-result v0

    iget-object v1, p0, Lmaps/l/ao;->t:Lmaps/cr/a;

    invoke-virtual {v1}, Lmaps/cr/a;->c()F

    move-result v1

    iget-object v2, p0, Lmaps/l/ao;->r:[Lmaps/l/aq;

    aget-object v2, v2, v6

    iput v0, v2, Lmaps/l/aq;->e:F

    iget-object v0, p0, Lmaps/l/ao;->r:[Lmaps/l/aq;

    aget-object v0, v0, v6

    iput v1, v0, Lmaps/l/aq;->f:F

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lmaps/l/ao;->o:Lmaps/t/cg;

    invoke-direct {p0, p1, v0, v1}, Lmaps/l/ao;->a(Lmaps/cr/c;Lmaps/t/cg;F)V

    goto :goto_0
.end method

.method private i()V
    .locals 5

    const/4 v1, 0x0

    new-instance v0, Lmaps/al/o;

    iget-object v2, p0, Lmaps/l/ao;->q:Lmaps/t/j;

    invoke-virtual {v2}, Lmaps/t/j;->b()I

    move-result v2

    invoke-direct {v0, v2}, Lmaps/al/o;-><init>(I)V

    iput-object v0, p0, Lmaps/l/ao;->y:Lmaps/al/o;

    new-instance v2, Lmaps/t/bx;

    invoke-direct {v2}, Lmaps/t/bx;-><init>()V

    move v0, v1

    :goto_0
    iget-object v3, p0, Lmaps/l/ao;->q:Lmaps/t/j;

    invoke-virtual {v3}, Lmaps/t/j;->b()I

    move-result v3

    if-ge v0, v3, :cond_0

    iget-object v3, p0, Lmaps/l/ao;->q:Lmaps/t/j;

    invoke-virtual {v3, v0}, Lmaps/t/j;->a(I)Lmaps/t/bx;

    move-result-object v3

    iget-object v4, p0, Lmaps/l/ao;->q:Lmaps/t/j;

    invoke-virtual {v4, v1}, Lmaps/t/j;->a(I)Lmaps/t/bx;

    move-result-object v4

    invoke-static {v3, v4, v2}, Lmaps/t/cb;->d(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    iget-object v3, p0, Lmaps/l/ao;->y:Lmaps/al/o;

    iget v4, p0, Lmaps/l/ao;->A:F

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-virtual {v3, v2, v4}, Lmaps/al/o;->a(Lmaps/t/bx;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private j()Lmaps/t/cg;
    .locals 4

    const/4 v0, 0x0

    invoke-direct {p0}, Lmaps/l/ao;->v()Lmaps/t/cg;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/t/cg;->b()I

    move-result v2

    const/4 v3, 0x2

    if-le v2, v3, :cond_2

    add-int/lit8 v2, v2, -0x1

    mul-int/lit8 v2, v2, 0x3

    iget-object v3, p0, Lmaps/l/ao;->l:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-le v2, v3, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-static {v1}, Lmaps/l/ao;->b(Lmaps/t/cg;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method private v()Lmaps/t/cg;
    .locals 8

    const/high16 v4, 0x40000000

    const/high16 v7, 0x3e800000

    iget-object v0, p0, Lmaps/l/ao;->n:Lmaps/t/cg;

    invoke-virtual {v0}, Lmaps/t/cg;->b()I

    move-result v0

    add-int/lit8 v2, v0, -0x1

    iget v0, p0, Lmaps/l/ao;->I:I

    if-nez v0, :cond_2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Lmaps/l/ao;->n:Lmaps/t/cg;

    invoke-virtual {v0, v1}, Lmaps/t/cg;->b(I)F

    move-result v3

    iget v0, p0, Lmaps/l/ao;->p:F

    cmpl-float v0, v3, v0

    if-lez v0, :cond_0

    sget-object v0, Lmaps/af/v;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/s/p;

    iget-object v2, v0, Lmaps/s/p;->a:Lmaps/t/bx;

    iget-object v4, v0, Lmaps/s/p;->b:Lmaps/t/bx;

    iget-object v5, v0, Lmaps/s/p;->c:Lmaps/t/bx;

    iget-object v0, v0, Lmaps/s/p;->d:Lmaps/t/bx;

    iget-object v6, p0, Lmaps/l/ao;->n:Lmaps/t/cg;

    invoke-virtual {v6, v1, v5}, Lmaps/t/cg;->a(ILmaps/t/bx;)V

    iget-object v6, p0, Lmaps/l/ao;->n:Lmaps/t/cg;

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v6, v1, v0}, Lmaps/t/cg;->a(ILmaps/t/bx;)V

    iget v1, p0, Lmaps/l/ao;->p:F

    sub-float v1, v3, v1

    div-float/2addr v1, v3

    mul-float v3, v1, v7

    invoke-static {v5, v0, v3, v2}, Lmaps/t/bx;->a(Lmaps/t/bx;Lmaps/t/bx;FLmaps/t/bx;)V

    const/high16 v3, 0x3f400000

    mul-float/2addr v1, v3

    invoke-static {v0, v5, v1, v4}, Lmaps/t/bx;->a(Lmaps/t/bx;Lmaps/t/bx;FLmaps/t/bx;)V

    invoke-static {v2, v4}, Lmaps/t/cg;->a(Lmaps/t/bx;Lmaps/t/bx;)Lmaps/t/cg;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    iput v0, p0, Lmaps/l/ao;->I:I

    :cond_2
    iget-object v0, p0, Lmaps/l/ao;->n:Lmaps/t/cg;

    invoke-virtual {v0}, Lmaps/t/cg;->d()F

    move-result v0

    iget v1, p0, Lmaps/l/ao;->I:I

    packed-switch v1, :pswitch_data_0

    const/4 v0, 0x0

    goto :goto_1

    :pswitch_0
    iget v1, p0, Lmaps/l/ao;->p:F

    sub-float/2addr v0, v1

    mul-float/2addr v0, v7

    iget v1, p0, Lmaps/l/ao;->p:F

    add-float/2addr v1, v0

    iget-object v2, p0, Lmaps/l/ao;->n:Lmaps/t/cg;

    invoke-static {v2, v0, v1}, Lmaps/l/ao;->a(Lmaps/t/cg;FF)Lmaps/t/cg;

    move-result-object v0

    goto :goto_1

    :pswitch_1
    iget v1, p0, Lmaps/l/ao;->A:F

    mul-float/2addr v1, v4

    iget v2, p0, Lmaps/l/ao;->B:F

    mul-float/2addr v1, v2

    iget v2, p0, Lmaps/l/ao;->p:F

    sub-float/2addr v0, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iget v1, p0, Lmaps/l/ao;->p:F

    add-float/2addr v1, v0

    iget-object v2, p0, Lmaps/l/ao;->n:Lmaps/t/cg;

    invoke-static {v2, v0, v1}, Lmaps/l/ao;->a(Lmaps/t/cg;FF)Lmaps/t/cg;

    move-result-object v0

    goto :goto_1

    :pswitch_2
    const/4 v1, 0x0

    iget v2, p0, Lmaps/l/ao;->p:F

    sub-float/2addr v0, v2

    iget v2, p0, Lmaps/l/ao;->A:F

    mul-float/2addr v2, v4

    iget v3, p0, Lmaps/l/ao;->B:F

    mul-float/2addr v2, v3

    sub-float/2addr v0, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iget v1, p0, Lmaps/l/ao;->p:F

    add-float/2addr v1, v0

    iget-object v2, p0, Lmaps/l/ao;->n:Lmaps/t/cg;

    invoke-static {v2, v0, v1}, Lmaps/l/ao;->a(Lmaps/t/cg;FF)Lmaps/t/cg;

    move-result-object v0

    goto :goto_1

    :pswitch_3
    iget v1, p0, Lmaps/l/ao;->p:F

    sub-float/2addr v0, v1

    const v1, 0x3ea8f5c3

    mul-float/2addr v0, v1

    iget v1, p0, Lmaps/l/ao;->p:F

    add-float/2addr v1, v0

    iget-object v2, p0, Lmaps/l/ao;->n:Lmaps/t/cg;

    invoke-static {v2, v0, v1}, Lmaps/l/ao;->a(Lmaps/t/cg;FF)Lmaps/t/cg;

    move-result-object v0

    goto :goto_1

    :pswitch_4
    iget v1, p0, Lmaps/l/ao;->p:F

    sub-float/2addr v0, v1

    const v1, 0x3f2b851f

    mul-float/2addr v0, v1

    iget v1, p0, Lmaps/l/ao;->p:F

    add-float/2addr v1, v0

    iget-object v2, p0, Lmaps/l/ao;->n:Lmaps/t/cg;

    invoke-static {v2, v0, v1}, Lmaps/l/ao;->a(Lmaps/t/cg;FF)Lmaps/t/cg;

    move-result-object v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method a(Lmaps/bq/d;)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    sget-object v0, Lmaps/af/v;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/s/p;

    iget-object v1, v0, Lmaps/s/p;->a:Lmaps/t/bx;

    iget-object v0, v0, Lmaps/s/p;->b:Lmaps/t/bx;

    iget-object v2, p0, Lmaps/l/ao;->o:Lmaps/t/cg;

    invoke-virtual {v2, v4, v1}, Lmaps/t/cg;->a(ILmaps/t/bx;)V

    iget-object v2, p0, Lmaps/l/ao;->o:Lmaps/t/cg;

    invoke-virtual {v2, v0}, Lmaps/t/cg;->a(Lmaps/t/bx;)V

    iget-object v2, p0, Lmaps/l/ao;->M:[F

    invoke-virtual {p1, v1, v2}, Lmaps/bq/d;->a(Lmaps/t/bx;[F)V

    iget-object v1, p0, Lmaps/l/ao;->M:[F

    aget v1, v1, v4

    iget-object v2, p0, Lmaps/l/ao;->M:[F

    aget v2, v2, v5

    iget-object v3, p0, Lmaps/l/ao;->M:[F

    invoke-virtual {p1, v0, v3}, Lmaps/bq/d;->a(Lmaps/t/bx;[F)V

    iget-object v0, p0, Lmaps/l/ao;->M:[F

    aget v3, v0, v4

    sub-float v1, v3, v1

    aput v1, v0, v4

    iget-object v0, p0, Lmaps/l/ao;->M:[F

    aget v1, v0, v5

    sub-float/2addr v1, v2

    aput v1, v0, v5

    return-void
.end method

.method public a(Lmaps/cr/c;)V
    .locals 1

    invoke-super {p0, p1}, Lmaps/l/u;->a(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ao;->u:Lmaps/al/q;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/l/ao;->u:Lmaps/al/q;

    invoke-virtual {v0, p1}, Lmaps/al/q;->e(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ao;->v:Lmaps/al/q;

    invoke-virtual {v0, p1}, Lmaps/al/q;->e(Lmaps/cr/c;)V

    :cond_0
    iget-object v0, p0, Lmaps/l/ao;->y:Lmaps/al/o;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/l/ao;->y:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->e(Lmaps/cr/c;)V

    :cond_1
    return-void
.end method

.method public a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 10

    const/high16 v9, 0x40000000

    const/high16 v1, 0x3f800000

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget-boolean v0, p0, Lmaps/l/ao;->J:Z

    if-nez v0, :cond_1

    invoke-interface {p3}, Lmaps/af/s;->a()Lmaps/af/q;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lmaps/l/ao;->a(Lmaps/cr/c;Lmaps/af/q;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {p3}, Lmaps/af/s;->b()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/l/ao;->r:[Lmaps/l/aq;

    array-length v0, v0

    if-lez v0, :cond_0

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v6

    invoke-virtual {p1}, Lmaps/cr/c;->r()V

    iget-object v0, p0, Lmaps/l/ao;->r:[Lmaps/l/aq;

    array-length v0, v0

    if-ne v0, v3, :cond_7

    iget-object v0, p1, Lmaps/cr/c;->e:Lmaps/al/g;

    invoke-virtual {v0, p1}, Lmaps/al/g;->b(Lmaps/cr/c;)V

    :goto_1
    iget-object v0, p0, Lmaps/l/ao;->t:Lmaps/cr/a;

    invoke-virtual {v0, v6}, Lmaps/cr/a;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    iget-object v0, p0, Lmaps/l/ao;->z:Lmaps/w/v;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lmaps/l/ao;->z:Lmaps/w/v;

    invoke-virtual {v0, p1}, Lmaps/w/v;->a(Lmaps/cr/c;)I

    move-result v0

    const/high16 v2, 0x10000

    if-ne v0, v2, :cond_2

    const/4 v2, 0x0

    iput-object v2, p0, Lmaps/l/ao;->z:Lmaps/w/v;

    :cond_2
    :goto_2
    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    invoke-interface {v2, v0, v0, v0, v0}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    iget-object v0, p0, Lmaps/l/ao;->x:Lmaps/al/q;

    if-eqz v0, :cond_3

    invoke-virtual {p2}, Lmaps/bq/d;->r()F

    move-result v0

    cmpl-float v0, v0, v5

    if-gtz v0, :cond_3

    invoke-virtual {p2}, Lmaps/bq/d;->q()F

    move-result v0

    cmpl-float v0, v0, v5

    if-lez v0, :cond_15

    :cond_3
    invoke-virtual {p0, p2}, Lmaps/l/ao;->a(Lmaps/bq/d;)V

    iget-object v0, p0, Lmaps/l/ao;->M:[F

    aget v0, v0, v4

    iget-object v2, p0, Lmaps/l/ao;->M:[F

    aget v7, v2, v3

    mul-float v2, v0, v0

    mul-float v8, v7, v7

    add-float/2addr v2, v8

    invoke-static {v2}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v8

    cmpl-float v0, v0, v5

    if-ltz v0, :cond_9

    move v2, v1

    :goto_3
    cmpl-float v0, v7, v5

    if-ltz v0, :cond_a

    move v0, v1

    :goto_4
    mul-float/2addr v7, v0

    div-float/2addr v7, v8

    sub-float v7, v1, v7

    mul-float/2addr v7, v2

    iget-object v2, p0, Lmaps/l/ao;->x:Lmaps/al/q;

    if-nez v2, :cond_4

    cmpl-float v2, v0, v5

    if-lez v2, :cond_b

    iget-object v2, p0, Lmaps/l/ao;->v:Lmaps/al/q;

    :goto_5
    iput-object v2, p0, Lmaps/l/ao;->x:Lmaps/al/q;

    cmpl-float v0, v0, v5

    if-lez v0, :cond_c

    move v0, v3

    :goto_6
    iput-boolean v0, p0, Lmaps/l/ao;->w:Z

    :cond_4
    iget-object v0, p0, Lmaps/l/ao;->x:Lmaps/al/q;

    iget-object v2, p0, Lmaps/l/ao;->v:Lmaps/al/q;

    if-ne v0, v2, :cond_f

    const v0, 0x3a83126f

    cmpg-float v0, v7, v0

    if-gez v0, :cond_d

    iget-object v0, p0, Lmaps/l/ao;->v:Lmaps/al/q;

    :goto_7
    iput-object v0, p0, Lmaps/l/ao;->x:Lmaps/al/q;

    const v0, 0x3a83126f

    cmpg-float v0, v7, v0

    if-gez v0, :cond_e

    move v0, v3

    :goto_8
    iput-boolean v0, p0, Lmaps/l/ao;->w:Z

    :goto_9
    iget-boolean v0, p0, Lmaps/l/ao;->H:Z

    if-nez v0, :cond_15

    const/high16 v0, 0x3f400000

    cmpl-float v0, v7, v0

    if-gtz v0, :cond_5

    const/high16 v0, -0x40c00000

    cmpg-float v0, v7, v0

    if-gez v0, :cond_15

    :cond_5
    invoke-virtual {p2}, Lmaps/bq/d;->r()F

    move-result v0

    mul-float/2addr v0, v7

    :goto_a
    move v2, v4

    :goto_b
    iget-object v7, p0, Lmaps/l/ao;->r:[Lmaps/l/aq;

    array-length v7, v7

    if-ge v2, v7, :cond_13

    iget-object v7, p0, Lmaps/l/ao;->r:[Lmaps/l/aq;

    array-length v7, v7

    if-ne v7, v3, :cond_12

    const/16 v7, 0x1702

    invoke-interface {v6, v7}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-interface {v6}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    iget-boolean v7, p0, Lmaps/l/ao;->w:Z

    if-eqz v7, :cond_6

    iget-object v7, p0, Lmaps/l/ao;->r:[Lmaps/l/aq;

    aget-object v7, v7, v2

    iget v7, v7, Lmaps/l/aq;->e:F

    div-float/2addr v7, v9

    iget-object v8, p0, Lmaps/l/ao;->r:[Lmaps/l/aq;

    aget-object v8, v8, v2

    iget v8, v8, Lmaps/l/aq;->f:F

    div-float/2addr v8, v9

    invoke-interface {v6, v7, v8, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    const/high16 v7, 0x43340000

    invoke-interface {v6, v7, v5, v5, v1}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    iget-object v7, p0, Lmaps/l/ao;->r:[Lmaps/l/aq;

    aget-object v7, v7, v2

    iget v7, v7, Lmaps/l/aq;->e:F

    neg-float v7, v7

    div-float/2addr v7, v9

    iget-object v8, p0, Lmaps/l/ao;->r:[Lmaps/l/aq;

    aget-object v8, v8, v2

    iget v8, v8, Lmaps/l/aq;->f:F

    neg-float v8, v8

    div-float/2addr v8, v9

    invoke-interface {v6, v7, v8, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    :cond_6
    iget-object v7, p0, Lmaps/l/ao;->r:[Lmaps/l/aq;

    aget-object v7, v7, v2

    iget v7, v7, Lmaps/l/aq;->e:F

    iget-object v8, p0, Lmaps/l/ao;->r:[Lmaps/l/aq;

    aget-object v8, v8, v2

    iget v8, v8, Lmaps/l/aq;->f:F

    invoke-interface {v6, v7, v8, v1}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    const/16 v7, 0x1700

    invoke-interface {v6, v7}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    :goto_c
    invoke-interface {v6}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    iget-object v7, p0, Lmaps/l/ao;->r:[Lmaps/l/aq;

    aget-object v7, v7, v2

    invoke-direct {p0, p1, p2, v7, v0}, Lmaps/l/ao;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/l/aq;F)V

    invoke-interface {v6}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    add-int/lit8 v2, v2, 0x1

    goto :goto_b

    :cond_7
    iget-object v0, p1, Lmaps/cr/c;->h:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->d(Lmaps/cr/c;)V

    goto/16 :goto_1

    :cond_8
    iget v0, p0, Lmaps/l/ao;->k:I

    goto/16 :goto_2

    :cond_9
    const/high16 v0, -0x40800000

    move v2, v0

    goto/16 :goto_3

    :cond_a
    const/high16 v0, -0x40800000

    goto/16 :goto_4

    :cond_b
    iget-object v2, p0, Lmaps/l/ao;->u:Lmaps/al/q;

    goto/16 :goto_5

    :cond_c
    move v0, v4

    goto/16 :goto_6

    :cond_d
    iget-object v0, p0, Lmaps/l/ao;->u:Lmaps/al/q;

    goto/16 :goto_7

    :cond_e
    move v0, v4

    goto/16 :goto_8

    :cond_f
    const v0, -0x457ced91

    cmpg-float v0, v7, v0

    if-gez v0, :cond_10

    iget-object v0, p0, Lmaps/l/ao;->v:Lmaps/al/q;

    :goto_d
    iput-object v0, p0, Lmaps/l/ao;->x:Lmaps/al/q;

    const v0, -0x457ced91

    cmpg-float v0, v7, v0

    if-gez v0, :cond_11

    move v0, v3

    :goto_e
    iput-boolean v0, p0, Lmaps/l/ao;->w:Z

    goto/16 :goto_9

    :cond_10
    iget-object v0, p0, Lmaps/l/ao;->u:Lmaps/al/q;

    goto :goto_d

    :cond_11
    move v0, v4

    goto :goto_e

    :cond_12
    iget-object v7, p0, Lmaps/l/ao;->x:Lmaps/al/q;

    mul-int/lit8 v8, v2, 0x2

    invoke-virtual {v7, p1, v8}, Lmaps/al/q;->a(Lmaps/cr/c;I)V

    goto :goto_c

    :cond_13
    iget-object v0, p0, Lmaps/l/ao;->r:[Lmaps/l/aq;

    array-length v0, v0

    if-ne v0, v3, :cond_14

    const/16 v0, 0x1702

    invoke-interface {v6, v0}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-interface {v6}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    const/16 v0, 0x1700

    invoke-interface {v6, v0}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    :cond_14
    sget-boolean v0, Lmaps/ae/h;->P:Z

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    iget-object v0, p0, Lmaps/l/ao;->q:Lmaps/t/j;

    invoke-virtual {v0, v4}, Lmaps/t/j;->a(I)Lmaps/t/bx;

    move-result-object v0

    iget v2, p0, Lmaps/l/ao;->A:F

    invoke-static {p1, p2, v0, v2}, Lmaps/af/d;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/t/bx;F)V

    invoke-virtual {p1}, Lmaps/cr/c;->s()V

    iget-object v0, p0, Lmaps/l/ao;->y:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->d(Lmaps/cr/c;)V

    const/high16 v0, 0x3f000000

    invoke-interface {v6, v0, v5, v5, v1}, Ljavax/microedition/khronos/opengles/GL10;->glColor4f(FFFF)V

    const/4 v0, 0x2

    iget-object v1, p0, Lmaps/l/ao;->y:Lmaps/al/o;

    invoke-virtual {v1}, Lmaps/al/o;->c()I

    move-result v1

    invoke-interface {v6, v0, v4, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    invoke-virtual {p1}, Lmaps/cr/c;->r()V

    invoke-interface {v6}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto/16 :goto_0

    :cond_15
    move v0, v5

    goto/16 :goto_a
.end method

.method public a(Lmaps/bq/d;Lmaps/cr/c;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Lmaps/bq/d;->z()F

    move-result v0

    iget v3, p0, Lmaps/l/ao;->A:F

    div-float v3, v0, v3

    invoke-virtual {p0}, Lmaps/l/ao;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lmaps/l/ao;->i:Z

    if-eqz v0, :cond_1

    const/high16 v0, 0x3e800000

    cmpl-float v0, v3, v0

    if-ltz v0, :cond_0

    const/high16 v0, 0x40000000

    cmpg-float v0, v3, v0

    if-gtz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v3}, Lmaps/l/ao;->a(F)I

    move-result v3

    iput v3, p0, Lmaps/l/ao;->k:I

    :goto_1
    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lmaps/bq/d;->q()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lmaps/l/ao;->E:I

    invoke-virtual {p1}, Lmaps/bq/d;->r()F

    move-result v0

    iput v0, p0, Lmaps/l/ao;->F:F

    :goto_2
    return v1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    const v0, 0x3f666666

    cmpl-float v0, v3, v0

    if-ltz v0, :cond_2

    const/high16 v0, 0x3fa00000

    cmpg-float v0, v3, v0

    if-gtz v0, :cond_2

    move v0, v1

    :goto_3
    const/high16 v3, 0x10000

    iput v3, p0, Lmaps/l/ao;->k:I

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_3

    :cond_3
    move v1, v2

    goto :goto_2
.end method

.method a(Lmaps/cr/c;Lmaps/af/q;)Z
    .locals 17

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/l/ao;->b:Lmaps/t/aa;

    move-object/from16 v0, p2

    invoke-static {v1, v0}, Lmaps/l/ao;->a(Lmaps/t/aa;Lmaps/af/q;)I

    move-result v6

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/l/ao;->b:Lmaps/t/aa;

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/l/ao;->a:Lmaps/t/ca;

    invoke-interface {v1}, Lmaps/t/ca;->a()I

    move-result v1

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    const/4 v1, 0x1

    :goto_0
    move-object/from16 v0, p2

    invoke-static {v2, v1, v0}, Lmaps/l/ao;->a(Lmaps/t/aa;ZLmaps/af/q;)I

    move-result v7

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/l/ao;->b:Lmaps/t/aa;

    if-eqz v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/l/ao;->b:Lmaps/t/aa;

    invoke-virtual {v1}, Lmaps/t/aa;->j()Lmaps/t/cv;

    move-result-object v4

    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/l/ao;->s:Lmaps/p/y;

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/l/ao;->l:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/l/ao;->m:Lmaps/p/l;

    move-object/from16 v0, p0

    iget v5, v0, Lmaps/l/ao;->B:F

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Lmaps/p/y;->a(Ljava/lang/String;Lmaps/p/l;Lmaps/t/cv;FIII)Lmaps/cr/a;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lmaps/l/ao;->t:Lmaps/cr/a;

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/l/ao;->t:Lmaps/cr/a;

    if-nez v1, :cond_3

    sget-boolean v1, Lmaps/ae/h;->F:Z

    if-nez v1, :cond_2

    const/16 v1, 0x2710

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lmaps/cr/c;->a(I)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x0

    :goto_2
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    goto :goto_1

    :cond_2
    move-object/from16 v0, p0

    iget-object v8, v0, Lmaps/l/ao;->s:Lmaps/p/y;

    move-object/from16 v0, p0

    iget-object v10, v0, Lmaps/l/ao;->l:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lmaps/l/ao;->m:Lmaps/p/l;

    move-object/from16 v0, p0

    iget v13, v0, Lmaps/l/ao;->B:F

    const/16 v16, 0x0

    move-object/from16 v9, p1

    move-object v12, v4

    move v14, v6

    move v15, v7

    invoke-virtual/range {v8 .. v16}, Lmaps/p/y;->a(Lmaps/cr/c;Ljava/lang/String;Lmaps/p/l;Lmaps/t/cv;FIII)Lmaps/cr/a;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lmaps/l/ao;->t:Lmaps/cr/a;

    :cond_3
    invoke-direct/range {p0 .. p1}, Lmaps/l/ao;->d(Lmaps/cr/c;)V

    sget-boolean v1, Lmaps/ae/h;->P:Z

    if-eqz v1, :cond_4

    invoke-direct/range {p0 .. p0}, Lmaps/l/ao;->i()V

    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/l/ao;->r:[Lmaps/l/aq;

    array-length v1, v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_5

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/l/ao;->r:[Lmaps/l/aq;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    iget v2, v1, Lmaps/l/aq;->d:F

    const/4 v1, 0x1

    :goto_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/l/ao;->r:[Lmaps/l/aq;

    array-length v3, v3

    if-ge v1, v3, :cond_5

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/l/ao;->r:[Lmaps/l/aq;

    aget-object v3, v3, v1

    iget v3, v3, Lmaps/l/aq;->d:F

    sub-float/2addr v3, v2

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x41f00000

    cmpl-float v4, v3, v4

    if-lez v4, :cond_7

    const/high16 v4, 0x43a50000

    cmpg-float v3, v3, v4

    if-gez v3, :cond_7

    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lmaps/l/ao;->H:Z

    :cond_5
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lmaps/l/ao;->h:Z

    if-eqz v1, :cond_6

    new-instance v1, Lmaps/w/v;

    const-wide/16 v2, 0x1f4

    sget-object v4, Lmaps/w/j;->a:Lmaps/w/j;

    invoke-direct {v1, v2, v3, v4}, Lmaps/w/v;-><init>(JLmaps/w/j;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lmaps/l/ao;->z:Lmaps/w/v;

    :cond_6
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lmaps/l/ao;->J:Z

    const/4 v1, 0x1

    goto :goto_2

    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_3
.end method

.method public a(Lmaps/t/bf;)Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/l/ao;->o:Lmaps/t/cg;

    invoke-virtual {v1, v0}, Lmaps/t/cg;->a(I)Lmaps/t/bx;

    move-result-object v1

    invoke-virtual {p1, v1}, Lmaps/t/bf;->a(Lmaps/t/bx;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/l/ao;->o:Lmaps/t/cg;

    invoke-virtual {v1}, Lmaps/t/cg;->c()Lmaps/t/bx;

    move-result-object v1

    invoke-virtual {p1, v1}, Lmaps/t/bf;->a(Lmaps/t/bx;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public b(Lmaps/bq/d;Lmaps/cr/c;)Z
    .locals 1

    iget v0, p0, Lmaps/l/ao;->I:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/l/ao;->I:I

    invoke-virtual {p0}, Lmaps/l/ao;->e()Z

    move-result v0

    return v0
.end method

.method public c(Lmaps/cr/c;)V
    .locals 1

    invoke-super {p0, p1}, Lmaps/l/u;->c(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ao;->t:Lmaps/cr/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/l/ao;->t:Lmaps/cr/a;

    invoke-virtual {v0}, Lmaps/cr/a;->g()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/l/ao;->t:Lmaps/cr/a;

    :cond_0
    iget-object v0, p0, Lmaps/l/ao;->u:Lmaps/al/q;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/l/ao;->u:Lmaps/al/q;

    invoke-virtual {v0, p1}, Lmaps/al/q;->b(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ao;->v:Lmaps/al/q;

    invoke-virtual {v0, p1}, Lmaps/al/q;->b(Lmaps/cr/c;)V

    :cond_1
    iget-object v0, p0, Lmaps/l/ao;->y:Lmaps/al/o;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/l/ao;->y:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->b(Lmaps/cr/c;)V

    :cond_2
    return-void
.end method

.method e()Z
    .locals 5

    const/4 v1, 0x1

    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lmaps/l/ao;->I:I

    const/4 v3, 0x6

    if-ge v2, v3, :cond_0

    iget v2, p0, Lmaps/l/ao;->I:I

    if-le v2, v1, :cond_1

    iget-object v2, p0, Lmaps/l/ao;->n:Lmaps/t/cg;

    invoke-virtual {v2}, Lmaps/t/cg;->d()F

    move-result v2

    iget v3, p0, Lmaps/l/ao;->p:F

    const/high16 v4, 0x40000000

    mul-float/2addr v3, v4

    cmpg-float v2, v2, v3

    if-gez v2, :cond_1

    :cond_0
    :goto_1
    return v0

    :cond_1
    iget v2, p0, Lmaps/l/ao;->I:I

    const/4 v3, 0x3

    if-le v2, v3, :cond_2

    iget-object v2, p0, Lmaps/l/ao;->n:Lmaps/t/cg;

    invoke-virtual {v2}, Lmaps/t/cg;->d()F

    move-result v2

    iget v3, p0, Lmaps/l/ao;->p:F

    const/high16 v4, 0x40400000

    mul-float/2addr v3, v4

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_0

    :cond_2
    invoke-direct {p0}, Lmaps/l/ao;->j()Lmaps/t/cg;

    move-result-object v2

    iput-object v2, p0, Lmaps/l/ao;->o:Lmaps/t/cg;

    iget-object v2, p0, Lmaps/l/ao;->o:Lmaps/t/cg;

    if-eqz v2, :cond_3

    iget v0, p0, Lmaps/l/ao;->C:F

    invoke-direct {p0, v0}, Lmaps/l/ao;->b(F)V

    iget-object v0, p0, Lmaps/l/ao;->o:Lmaps/t/cg;

    invoke-static {v0}, Lmaps/l/ao;->a(Lmaps/t/cg;)I

    move-result v0

    iput v0, p0, Lmaps/l/ao;->D:I

    move v0, v1

    goto :goto_1

    :cond_3
    iget v2, p0, Lmaps/l/ao;->I:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lmaps/l/ao;->I:I

    goto :goto_0
.end method

.method h()I
    .locals 3

    iget v0, p0, Lmaps/l/ao;->E:I

    iget v1, p0, Lmaps/l/ao;->D:I

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v1, p0, Lmaps/l/ao;->G:I

    int-to-float v1, v1

    int-to-float v0, v0

    const v2, 0x3c8efa35

    mul-float/2addr v0, v2

    invoke-static {v0}, Landroid/util/FloatMath;->sin(F)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public l()F
    .locals 1

    iget v0, p0, Lmaps/l/ao;->L:F

    return v0
.end method

.method public m()Lmaps/t/an;
    .locals 1

    iget-object v0, p0, Lmaps/l/ao;->q:Lmaps/t/j;

    return-object v0
.end method

.method public r()I
    .locals 4

    const/4 v1, 0x0

    iget v2, p0, Lmaps/l/ao;->f:I

    iget v0, p0, Lmaps/l/ao;->I:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v2

    iget v2, p0, Lmaps/l/ao;->F:F

    const/high16 v3, 0x41f00000

    cmpg-float v2, v2, v3

    if-gez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lmaps/l/ao;->h()I

    move-result v1

    goto :goto_1
.end method

.method public s()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/l/ao;->K:Ljava/lang/String;

    return-object v0
.end method
