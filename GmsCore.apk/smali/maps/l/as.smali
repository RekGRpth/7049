.class public Lmaps/l/as;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lmaps/t/cg;

.field private final b:Ljava/util/ArrayList;

.field private final c:Ljava/util/ArrayList;

.field private final d:Ljava/util/HashSet;

.field private final e:[Ljava/lang/String;

.field private f:Lmaps/t/v;


# direct methods
.method private constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lmaps/l/as;->b:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lmaps/l/as;->c:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lmaps/l/as;->d:Ljava/util/HashSet;

    iput-object v2, p0, Lmaps/l/as;->a:Lmaps/t/cg;

    iput-object v2, p0, Lmaps/l/as;->e:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lmaps/t/az;[Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lmaps/l/as;->b:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lmaps/l/as;->c:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lmaps/l/as;->d:Ljava/util/HashSet;

    invoke-virtual {p1}, Lmaps/t/az;->c()Lmaps/t/cg;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/as;->a:Lmaps/t/cg;

    iput-object p2, p0, Lmaps/l/as;->e:[Ljava/lang/String;

    invoke-virtual {p0, p1}, Lmaps/l/as;->a(Lmaps/t/az;)V

    return-void
.end method

.method static a(II)I
    .locals 2

    mul-int v0, p0, p1

    invoke-static {p0, p1}, Lmaps/l/as;->b(II)I

    move-result v1

    div-int/2addr v0, v1

    return v0
.end method

.method static synthetic a(Lmaps/l/as;)Lmaps/t/cg;
    .locals 1

    iget-object v0, p0, Lmaps/l/as;->a:Lmaps/t/cg;

    return-object v0
.end method

.method static b(II)I
    .locals 2

    if-lez p0, :cond_0

    if-gtz p1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Greatest common divisor should be computed on numbers greater than zero."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    if-eqz p1, :cond_2

    rem-int v0, p0, p1

    move p0, p1

    move p1, v0

    goto :goto_0

    :cond_2
    return p0
.end method


# virtual methods
.method public a()Lmaps/t/cg;
    .locals 1

    iget-object v0, p0, Lmaps/l/as;->a:Lmaps/t/cg;

    return-object v0
.end method

.method public a(Lmaps/t/az;)V
    .locals 8

    const/4 v7, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1}, Lmaps/t/az;->j()[I

    move-result-object v2

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_1

    aget v4, v2, v0

    if-ltz v4, :cond_0

    iget-object v5, p0, Lmaps/l/as;->e:[Ljava/lang/String;

    array-length v5, v5

    if-ge v4, v5, :cond_0

    iget-object v5, p0, Lmaps/l/as;->d:Ljava/util/HashSet;

    iget-object v6, p0, Lmaps/l/as;->e:[Ljava/lang/String;

    aget-object v4, v6, v4

    invoke-virtual {v5, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lmaps/t/az;->h()Lmaps/t/aa;

    move-result-object v0

    invoke-virtual {p1}, Lmaps/t/az;->e()F

    move-result v2

    invoke-virtual {v0}, Lmaps/t/aa;->c()I

    move-result v3

    if-ne v3, v7, :cond_4

    iget-object v3, p0, Lmaps/l/as;->c:Ljava/util/ArrayList;

    new-instance v4, Lmaps/l/au;

    invoke-direct {v4, v2, v0, v1}, Lmaps/l/au;-><init>(FLmaps/t/aa;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    :goto_1
    invoke-virtual {p1}, Lmaps/t/az;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lmaps/t/az;->b()Lmaps/t/v;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/as;->f:Lmaps/t/v;

    :cond_3
    return-void

    :cond_4
    invoke-virtual {v0}, Lmaps/t/aa;->c()I

    move-result v3

    if-le v3, v7, :cond_2

    iget-object v3, p0, Lmaps/l/as;->b:Ljava/util/ArrayList;

    new-instance v4, Lmaps/l/au;

    invoke-direct {v4, v2, v0, v1}, Lmaps/l/au;-><init>(FLmaps/t/aa;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lmaps/l/as;->c:Ljava/util/ArrayList;

    new-instance v3, Lmaps/l/au;

    invoke-direct {v3, v2, v0, v7}, Lmaps/l/au;-><init>(FLmaps/t/aa;I)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public b()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lmaps/l/as;->d:Ljava/util/HashSet;

    return-object v0
.end method

.method public c()Lmaps/t/v;
    .locals 1

    iget-object v0, p0, Lmaps/l/as;->f:Lmaps/t/v;

    return-object v0
.end method

.method public d()F
    .locals 5

    const/high16 v4, 0x40000000

    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/l/as;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/au;

    iget v3, v0, Lmaps/l/au;->a:F

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget v0, v0, Lmaps/l/au;->b:F

    div-float/2addr v0, v4

    add-float/2addr v0, v3

    cmpl-float v3, v0, v1

    if-lez v3, :cond_3

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmaps/l/as;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/au;

    iget v3, v0, Lmaps/l/au;->a:F

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget v0, v0, Lmaps/l/au;->b:F

    div-float/2addr v0, v4

    add-float/2addr v0, v3

    cmpl-float v3, v0, v1

    if-lez v3, :cond_1

    move v1, v0

    goto :goto_2

    :cond_2
    mul-float v0, v1, v4

    return v0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public e()I
    .locals 12

    const/4 v1, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lmaps/l/as;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v2, p0, Lmaps/l/as;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int v7, v0, v2

    if-nez v7, :cond_1

    :cond_0
    return v1

    :cond_1
    new-array v8, v7, [I

    move v6, v4

    :goto_0
    if-ge v6, v7, :cond_5

    iget-object v0, p0, Lmaps/l/as;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v6, v0, :cond_2

    iget-object v0, p0, Lmaps/l/as;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/au;

    :goto_1
    iget-object v2, v0, Lmaps/l/au;->d:[I

    if-nez v2, :cond_3

    move v0, v1

    :goto_2
    aput v0, v8, v6

    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lmaps/l/as;->c:Ljava/util/ArrayList;

    iget-object v2, p0, Lmaps/l/as;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    sub-int v2, v6, v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/au;

    goto :goto_1

    :cond_3
    iget-object v9, v0, Lmaps/l/au;->d:[I

    array-length v10, v9

    move v3, v4

    move v2, v4

    :goto_3
    if-ge v3, v10, :cond_4

    aget v5, v9, v3

    add-int/2addr v5, v2

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v5

    goto :goto_3

    :cond_4
    iget-object v0, v0, Lmaps/l/au;->d:[I

    array-length v0, v0

    rem-int/lit8 v0, v0, 0x2

    if-ne v0, v1, :cond_6

    mul-int/lit8 v0, v2, 0x2

    goto :goto_2

    :cond_5
    aget v0, v8, v4

    move v11, v1

    move v1, v0

    move v0, v11

    :goto_4
    if-ge v0, v7, :cond_0

    aget v2, v8, v0

    invoke-static {v1, v2}, Lmaps/l/as;->a(II)I

    move-result v2

    add-int/lit8 v1, v0, 0x1

    move v0, v1

    move v1, v2

    goto :goto_4

    :cond_6
    move v0, v2

    goto :goto_2
.end method

.method public f()Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lmaps/l/as;->b:Ljava/util/ArrayList;

    return-object v0
.end method

.method public g()Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lmaps/l/as;->c:Ljava/util/ArrayList;

    return-object v0
.end method

.method public h()I
    .locals 3

    const/16 v0, 0xd0

    iget-object v1, p0, Lmaps/l/as;->a:Lmaps/t/cg;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/l/as;->a:Lmaps/t/cg;

    invoke-virtual {v1}, Lmaps/t/cg;->i()I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Lmaps/l/as;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget-object v2, p0, Lmaps/l/as;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x18

    add-int/2addr v0, v1

    return v0
.end method
