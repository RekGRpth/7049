.class public Lmaps/l/at;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/l/al;


# static fields
.field private static L:[F

.field public static final a:Lmaps/be/n;

.field private static final b:Lmaps/l/ad;

.field private static final c:Lmaps/l/ad;

.field private static final d:Lmaps/l/ad;

.field private static final e:Lmaps/l/ad;

.field private static final f:Lmaps/l/ad;

.field private static final g:Lmaps/l/ad;


# instance fields
.field private A:Lmaps/o/c;

.field private B:I

.field private C:[F

.field private D:J

.field private volatile E:I

.field private F:Lmaps/ac/g;

.field private G:Ljava/lang/Boolean;

.field private H:J

.field private I:J

.field private J:I

.field private K:J

.field private M:Z

.field private N:Lmaps/l/a;

.field private O:Lmaps/l/o;

.field private h:Lmaps/s/d;

.field private i:Lmaps/s/d;

.field private final j:[F

.field private k:[Lmaps/l/m;

.field private l:[Lmaps/l/ak;

.field private m:[Lmaps/l/q;

.field private n:[[Lmaps/l/ag;

.field private o:[Lmaps/l/ak;

.field private p:[Lmaps/l/q;

.field private q:[Lmaps/l/c;

.field private r:[Lmaps/l/s;

.field private s:Lmaps/l/u;

.field private t:Ljava/util/ArrayList;

.field private u:Ljava/util/Set;

.field private final v:Lmaps/t/ah;

.field private final w:Lmaps/t/ax;

.field private final x:Ljava/util/HashSet;

.field private y:I

.field private z:Lmaps/p/ap;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const v15, -0x33000001

    const/high16 v11, -0x80000000

    const/4 v6, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    new-instance v0, Lmaps/l/ad;

    const/high16 v2, 0x42f00000

    const/high16 v4, -0x3dc00000

    const/high16 v5, 0x42400000

    const v8, 0x6fffffff

    move v7, v1

    invoke-direct/range {v0 .. v8}, Lmaps/l/ad;-><init>(FFZFFZFI)V

    sput-object v0, Lmaps/l/at;->b:Lmaps/l/ad;

    new-instance v0, Lmaps/l/ad;

    const/high16 v2, 0x42700000

    const/high16 v4, -0x3e400000

    const/high16 v5, 0x41c00000

    const v8, 0x6fffffff

    move v7, v1

    invoke-direct/range {v0 .. v8}, Lmaps/l/ad;-><init>(FFZFFZFI)V

    sput-object v0, Lmaps/l/at;->d:Lmaps/l/ad;

    new-instance v0, Lmaps/l/ad;

    const/high16 v2, 0x41f00000

    const/high16 v4, -0x3f400000

    const/high16 v5, 0x40c00000

    const v8, 0x6fffffff

    move v7, v1

    invoke-direct/range {v0 .. v8}, Lmaps/l/ad;-><init>(FFZFFZFI)V

    sput-object v0, Lmaps/l/at;->f:Lmaps/l/ad;

    new-instance v7, Lmaps/l/ad;

    const/high16 v8, 0x42400000

    const/high16 v9, 0x42f00000

    move v10, v6

    move v12, v1

    move v13, v3

    move v14, v1

    invoke-direct/range {v7 .. v15}, Lmaps/l/ad;-><init>(FFZFFZFI)V

    sput-object v7, Lmaps/l/at;->c:Lmaps/l/ad;

    new-instance v7, Lmaps/l/ad;

    const/high16 v8, 0x41c00000

    const/high16 v9, 0x42700000

    move v10, v6

    move v12, v1

    move v13, v3

    move v14, v1

    invoke-direct/range {v7 .. v15}, Lmaps/l/ad;-><init>(FFZFFZFI)V

    sput-object v7, Lmaps/l/at;->e:Lmaps/l/ad;

    new-instance v7, Lmaps/l/ad;

    const/high16 v8, 0x41800000

    const/high16 v9, 0x42200000

    move v10, v6

    move v12, v1

    move v13, v3

    move v14, v1

    invoke-direct/range {v7 .. v15}, Lmaps/l/ad;-><init>(FFZFFZFI)V

    sput-object v7, Lmaps/l/at;->g:Lmaps/l/ad;

    sget-object v0, Lmaps/be/n;->a:Lmaps/be/n;

    sput-object v0, Lmaps/l/at;->a:Lmaps/be/n;

    const/16 v0, 0x8

    new-array v0, v0, [F

    sput-object v0, Lmaps/l/at;->L:[F

    return-void
.end method

.method private constructor <init>(Lmaps/t/ah;Lmaps/cr/c;)V
    .locals 8

    const-wide/16 v6, 0x0

    const-wide/16 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x4

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lmaps/l/at;->h:Lmaps/s/d;

    iput-object v1, p0, Lmaps/l/at;->i:Lmaps/s/d;

    new-array v0, v2, [F

    iput-object v0, p0, Lmaps/l/at;->j:[F

    new-array v0, v2, [F

    iput-object v0, p0, Lmaps/l/at;->C:[F

    iput-wide v6, p0, Lmaps/l/at;->D:J

    const/4 v0, -0x1

    iput v0, p0, Lmaps/l/at;->E:I

    iput-object v1, p0, Lmaps/l/at;->F:Lmaps/ac/g;

    iput-object v1, p0, Lmaps/l/at;->G:Ljava/lang/Boolean;

    iput-wide v4, p0, Lmaps/l/at;->H:J

    iput-wide v4, p0, Lmaps/l/at;->I:J

    iput v3, p0, Lmaps/l/at;->J:I

    iput-wide v6, p0, Lmaps/l/at;->K:J

    iput-boolean v3, p0, Lmaps/l/at;->M:Z

    iput-object v1, p0, Lmaps/l/at;->O:Lmaps/l/o;

    iput-object p1, p0, Lmaps/l/at;->v:Lmaps/t/ah;

    iget-object v0, p0, Lmaps/l/at;->v:Lmaps/t/ah;

    invoke-virtual {v0}, Lmaps/t/ah;->j()Lmaps/t/ax;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/at;->w:Lmaps/t/ax;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lmaps/l/at;->x:Ljava/util/HashSet;

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    new-instance v0, Lmaps/s/d;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Lmaps/s/d;-><init>(I)V

    iput-object v0, p0, Lmaps/l/at;->h:Lmaps/s/d;

    new-instance v0, Lmaps/s/d;

    invoke-direct {v0, v2}, Lmaps/s/d;-><init>(I)V

    iput-object v0, p0, Lmaps/l/at;->i:Lmaps/s/d;

    new-instance v0, Lmaps/l/a;

    iget-object v1, p0, Lmaps/l/at;->i:Lmaps/s/d;

    iget-object v2, p0, Lmaps/l/at;->h:Lmaps/s/d;

    invoke-direct {v0, v1, v2, p2}, Lmaps/l/a;-><init>(Lmaps/s/d;Lmaps/s/d;Lmaps/cr/c;)V

    iput-object v0, p0, Lmaps/l/at;->N:Lmaps/l/a;

    :cond_0
    return-void
.end method

.method private a(Lmaps/t/ar;)Lmaps/l/ad;
    .locals 5

    const/high16 v4, 0x41940000

    const/high16 v3, 0x41840000

    const/4 v1, 0x0

    invoke-virtual {p1}, Lmaps/t/ar;->g()Lmaps/t/ah;

    move-result-object v0

    sget-object v2, Lmaps/t/cm;->d:Lmaps/t/cm;

    invoke-virtual {v0, v2}, Lmaps/t/ah;->a(Lmaps/t/cm;)Lmaps/t/ao;

    move-result-object v0

    check-cast v0, Lmaps/t/cu;

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lmaps/t/cu;->b()Lmaps/t/bg;

    move-result-object v0

    invoke-static {}, Lmaps/an/i;->b()Lmaps/an/i;

    move-result-object v2

    if-nez v2, :cond_1

    move-object v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {v2, v0}, Lmaps/an/i;->a(Lmaps/t/bg;)Lmaps/t/e;

    move-result-object v2

    if-nez v2, :cond_2

    move-object v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {v2, v0}, Lmaps/t/e;->a(Lmaps/t/bg;)Lmaps/t/bi;

    move-result-object v0

    if-nez v0, :cond_3

    move-object v0, v1

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Lmaps/t/bi;->f()I

    move-result v2

    if-lez v2, :cond_6

    invoke-virtual {p1}, Lmaps/t/ar;->g()Lmaps/t/ah;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/ah;->c()I

    move-result v0

    int-to-float v0, v0

    cmpl-float v0, v0, v4

    if-lez v0, :cond_4

    sget-object v0, Lmaps/l/at;->f:Lmaps/l/ad;

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Lmaps/t/ar;->g()Lmaps/t/ah;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/ah;->c()I

    move-result v0

    int-to-float v0, v0

    cmpl-float v0, v0, v3

    if-lez v0, :cond_5

    sget-object v0, Lmaps/l/at;->d:Lmaps/l/ad;

    goto :goto_0

    :cond_5
    sget-object v0, Lmaps/l/at;->b:Lmaps/l/ad;

    goto :goto_0

    :cond_6
    invoke-virtual {v0}, Lmaps/t/bi;->f()I

    move-result v0

    if-gez v0, :cond_9

    invoke-virtual {p1}, Lmaps/t/ar;->g()Lmaps/t/ah;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/ah;->c()I

    move-result v0

    int-to-float v0, v0

    cmpl-float v0, v0, v4

    if-lez v0, :cond_7

    sget-object v0, Lmaps/l/at;->g:Lmaps/l/ad;

    goto :goto_0

    :cond_7
    invoke-virtual {p1}, Lmaps/t/ar;->g()Lmaps/t/ah;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/ah;->c()I

    move-result v0

    int-to-float v0, v0

    cmpl-float v0, v0, v3

    if-lez v0, :cond_8

    sget-object v0, Lmaps/l/at;->e:Lmaps/l/ad;

    goto :goto_0

    :cond_8
    sget-object v0, Lmaps/l/at;->c:Lmaps/l/ad;

    goto :goto_0

    :cond_9
    move-object v0, v1

    goto :goto_0
.end method

.method public static a(Lmaps/t/bz;Lmaps/cr/c;)Lmaps/l/at;
    .locals 4

    invoke-virtual {p0}, Lmaps/t/bz;->a()Lmaps/t/ar;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    const/4 v3, 0x6

    aput v3, v1, v2

    invoke-static {v1}, Lmaps/be/n;->a([I)Lmaps/be/n;

    move-result-object v1

    invoke-static {v0, v1, p1}, Lmaps/l/at;->a(Lmaps/t/o;Lmaps/be/n;Lmaps/cr/c;)Lmaps/l/at;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lmaps/t/o;Lmaps/be/n;Lmaps/cr/c;)Lmaps/l/at;
    .locals 4

    invoke-interface {p0}, Lmaps/t/o;->g()Lmaps/t/ah;

    move-result-object v0

    new-instance v1, Lmaps/l/at;

    invoke-direct {v1, v0, p2}, Lmaps/l/at;-><init>(Lmaps/t/ah;Lmaps/cr/c;)V

    instance-of v0, p0, Lmaps/t/ar;

    if-eqz v0, :cond_0

    check-cast p0, Lmaps/t/ar;

    invoke-direct {v1, p0, p1, p2}, Lmaps/l/at;->a(Lmaps/t/ar;Lmaps/be/n;Lmaps/cr/c;)V

    invoke-virtual {p0}, Lmaps/t/ar;->f()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lmaps/l/at;->b(J)V

    :cond_0
    return-object v1
.end method

.method private a(Lmaps/cr/c;F)V
    .locals 5

    const/high16 v4, 0x3f800000

    const/4 v3, 0x0

    invoke-virtual {p1}, Lmaps/cr/c;->t()V

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x302

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    sub-float v0, v4, p2

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    mul-float v2, v4, v0

    invoke-interface {v1, v3, v3, v2, v0}, Ljavax/microedition/khronos/opengles/GL10;->glColor4f(FFFF)V

    iget-object v0, p1, Lmaps/cr/c;->f:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->d(Lmaps/cr/c;)V

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/4 v1, 0x5

    const/4 v2, 0x0

    const/4 v3, 0x4

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    invoke-virtual {p1}, Lmaps/cr/c;->u()V

    return-void
.end method

.method private a(Lmaps/t/ar;Lmaps/be/n;Lmaps/cr/c;)V
    .locals 30

    invoke-virtual/range {p1 .. p1}, Lmaps/t/ar;->o()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lmaps/l/at;->y:I

    invoke-virtual/range {p1 .. p1}, Lmaps/t/ar;->a()[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v5, v3, v2

    move-object/from16 v0, p0

    iget-object v6, v0, Lmaps/l/at;->x:Ljava/util/HashSet;

    invoke-virtual {v6, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual/range {p1 .. p1}, Lmaps/t/ar;->h()Lmaps/o/c;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lmaps/l/at;->A:Lmaps/o/c;

    invoke-virtual/range {p1 .. p1}, Lmaps/t/ar;->j()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lmaps/l/at;->B:I

    invoke-virtual/range {p1 .. p1}, Lmaps/t/ar;->b()[Ljava/lang/String;

    move-result-object v3

    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    new-instance v25, Ljava/util/ArrayList;

    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V

    new-instance v26, Ljava/util/ArrayList;

    invoke-direct/range {v26 .. v26}, Ljava/util/ArrayList;-><init>()V

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    new-instance v27, Ljava/util/ArrayList;

    invoke-direct/range {v27 .. v27}, Ljava/util/ArrayList;-><init>()V

    new-instance v28, Ljava/util/ArrayList;

    invoke-direct/range {v28 .. v28}, Ljava/util/ArrayList;-><init>()V

    new-instance v29, Ljava/util/ArrayList;

    invoke-direct/range {v29 .. v29}, Ljava/util/ArrayList;-><init>()V

    new-instance v11, Lmaps/l/k;

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/l/at;->v:Lmaps/t/ah;

    invoke-direct {v11, v2, v3}, Lmaps/l/k;-><init>(Lmaps/t/ah;[Ljava/lang/String;)V

    new-instance v16, Lmaps/l/k;

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/l/at;->v:Lmaps/t/ah;

    move-object/from16 v0, v16

    invoke-direct {v0, v2, v3}, Lmaps/l/k;-><init>(Lmaps/t/ah;[Ljava/lang/String;)V

    move-object/from16 v0, v26

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct/range {p0 .. p1}, Lmaps/l/at;->a(Lmaps/t/ar;)Lmaps/l/ad;

    move-result-object v12

    const/4 v7, 0x0

    sget-boolean v2, Lmaps/ae/h;->F:Z

    if-eqz v2, :cond_1

    new-instance v7, Lmaps/s/j;

    invoke-direct {v7}, Lmaps/s/j;-><init>()V

    :cond_1
    invoke-virtual/range {p1 .. p1}, Lmaps/t/ar;->d()Lmaps/t/h;

    move-result-object v4

    const/4 v2, 0x1

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v5

    move-wide/from16 v20, v5

    move/from16 v22, v2

    move-object/from16 v23, v8

    :goto_1
    invoke-interface {v4}, Lmaps/t/h;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v4}, Lmaps/t/h;->a()Lmaps/t/ca;

    move-result-object v2

    invoke-interface {v2}, Lmaps/t/ca;->a()I

    move-result v5

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Lmaps/be/n;->a(I)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-interface {v4}, Lmaps/t/h;->next()Ljava/lang/Object;

    goto :goto_1

    :cond_2
    invoke-interface {v2}, Lmaps/t/ca;->a()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    :pswitch_0
    invoke-interface {v4}, Lmaps/t/h;->next()Ljava/lang/Object;

    :cond_3
    move/from16 v2, v22

    move-object/from16 v8, v23

    :goto_2
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v5

    sub-long v9, v5, v20

    const-wide/16 v13, 0xa

    cmp-long v9, v9, v13

    if-lez v9, :cond_1b

    invoke-static {}, Ljava/lang/Thread;->yield()V

    :goto_3
    move-wide/from16 v20, v5

    move/from16 v22, v2

    move-object/from16 v23, v8

    goto :goto_1

    :pswitch_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/l/at;->v:Lmaps/t/ah;

    move-object/from16 v0, p0

    iget-object v5, v0, Lmaps/l/at;->i:Lmaps/s/d;

    move-object/from16 v0, p0

    iget-object v6, v0, Lmaps/l/at;->h:Lmaps/s/d;

    move-object/from16 v8, p3

    invoke-static/range {v2 .. v8}, Lmaps/l/ag;->a(Lmaps/t/ah;[Ljava/lang/String;Lmaps/t/h;Lmaps/q/aj;Lmaps/q/aj;Lmaps/s/j;Lmaps/cr/c;)Lmaps/l/ag;

    move-result-object v2

    if-eqz v22, :cond_4

    invoke-virtual {v2}, Lmaps/l/ag;->c()Z

    move-result v5

    if-eqz v5, :cond_4

    const/16 v22, 0x0

    :cond_4
    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-boolean v5, Lmaps/ae/h;->G:Z

    if-eqz v5, :cond_3

    invoke-virtual {v2}, Lmaps/l/ag;->d()Z

    move-result v2

    if-eqz v2, :cond_3

    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v26

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v2, v22

    move-object/from16 v8, v23

    goto :goto_2

    :pswitch_2
    if-eqz v22, :cond_5

    move-object/from16 v0, p0

    iget-object v8, v0, Lmaps/l/at;->v:Lmaps/t/ah;

    const/4 v13, 0x2

    move-object v9, v3

    move-object v10, v4

    move-object/from16 v14, p3

    invoke-static/range {v8 .. v14}, Lmaps/l/av;->a(Lmaps/t/ah;[Ljava/lang/String;Lmaps/t/h;Lmaps/l/k;Lmaps/l/ad;ILmaps/cr/c;)Lmaps/l/av;

    move-result-object v2

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v2, v22

    move-object/from16 v8, v23

    goto :goto_2

    :cond_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lmaps/l/at;->v:Lmaps/t/ah;

    const/16 v18, 0x9

    move-object v14, v3

    move-object v15, v4

    move-object/from16 v17, v12

    move-object/from16 v19, p3

    invoke-static/range {v13 .. v19}, Lmaps/l/av;->a(Lmaps/t/ah;[Ljava/lang/String;Lmaps/t/h;Lmaps/l/k;Lmaps/l/ad;ILmaps/cr/c;)Lmaps/l/av;

    move-result-object v2

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v2, v22

    move-object/from16 v8, v23

    goto/16 :goto_2

    :pswitch_3
    if-eqz v22, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/l/at;->v:Lmaps/t/ah;

    const/4 v5, 0x3

    move-object/from16 v0, p3

    invoke-static {v2, v3, v4, v5, v0}, Lmaps/l/ar;->a(Lmaps/t/ah;[Ljava/lang/String;Lmaps/t/h;ILmaps/cr/c;)Lmaps/l/ar;

    move-result-object v2

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v2, v22

    move-object/from16 v8, v23

    goto/16 :goto_2

    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/l/at;->v:Lmaps/t/ah;

    const/16 v5, 0xa

    move-object/from16 v0, p3

    invoke-static {v2, v3, v4, v5, v0}, Lmaps/l/ar;->a(Lmaps/t/ah;[Ljava/lang/String;Lmaps/t/h;ILmaps/cr/c;)Lmaps/l/ar;

    move-result-object v2

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v2, v22

    move-object/from16 v8, v23

    goto/16 :goto_2

    :pswitch_4
    check-cast v2, Lmaps/t/az;

    invoke-static {v2}, Lmaps/l/ar;->a(Lmaps/t/az;)Z

    move-result v2

    if-eqz v2, :cond_8

    if-eqz v22, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/l/at;->v:Lmaps/t/ah;

    const/4 v5, 0x3

    move-object/from16 v0, p3

    invoke-static {v2, v3, v4, v5, v0}, Lmaps/l/ar;->a(Lmaps/t/ah;[Ljava/lang/String;Lmaps/t/h;ILmaps/cr/c;)Lmaps/l/ar;

    move-result-object v2

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v2, v22

    move-object/from16 v8, v23

    goto/16 :goto_2

    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/l/at;->v:Lmaps/t/ah;

    const/16 v5, 0xa

    move-object/from16 v0, p3

    invoke-static {v2, v3, v4, v5, v0}, Lmaps/l/ar;->a(Lmaps/t/ah;[Ljava/lang/String;Lmaps/t/h;ILmaps/cr/c;)Lmaps/l/ar;

    move-result-object v2

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v2, v22

    move-object/from16 v8, v23

    goto/16 :goto_2

    :cond_8
    if-eqz v22, :cond_9

    invoke-virtual {v11, v4}, Lmaps/l/k;->a(Lmaps/t/h;)V

    move/from16 v2, v22

    move-object/from16 v8, v23

    goto/16 :goto_2

    :cond_9
    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Lmaps/l/k;->a(Lmaps/t/h;)V

    move/from16 v2, v22

    move-object/from16 v8, v23

    goto/16 :goto_2

    :pswitch_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/l/at;->v:Lmaps/t/ah;

    invoke-static {v2, v3, v4}, Lmaps/l/c;->a(Lmaps/t/ah;[Ljava/lang/String;Lmaps/t/h;)Lmaps/l/c;

    move-result-object v2

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v2, v22

    move-object/from16 v8, v23

    goto/16 :goto_2

    :pswitch_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/l/at;->v:Lmaps/t/ah;

    move-object/from16 v0, p3

    invoke-static {v2, v3, v4, v0}, Lmaps/l/m;->a(Lmaps/t/ah;[Ljava/lang/String;Lmaps/t/h;Lmaps/cr/c;)Lmaps/l/m;

    move-result-object v2

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v2, v22

    move-object/from16 v8, v23

    goto/16 :goto_2

    :pswitch_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/l/at;->v:Lmaps/t/ah;

    invoke-static {v2, v3, v4}, Lmaps/l/s;->a(Lmaps/t/ah;[Ljava/lang/String;Lmaps/t/h;)Lmaps/l/s;

    move-result-object v2

    move-object/from16 v0, v29

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v2, v22

    move-object/from16 v8, v23

    goto/16 :goto_2

    :cond_a
    const/4 v2, 0x0

    sget-boolean v4, Lmaps/ae/h;->F:Z

    if-eqz v4, :cond_1a

    invoke-virtual {v7}, Lmaps/s/j;->c()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/l/at;->h:Lmaps/s/d;

    invoke-virtual {v4, v2}, Lmaps/s/d;->a(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/l/at;->i:Lmaps/s/d;

    invoke-virtual {v4, v2}, Lmaps/s/d;->a(I)V

    const/4 v4, 0x1

    new-instance v5, Lmaps/q/ab;

    invoke-virtual {v7}, Lmaps/s/j;->a()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-direct {v5, v2}, Lmaps/q/ab;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v2, Lmaps/q/au;

    invoke-direct {v2, v5, v4}, Lmaps/q/au;-><init>(Lmaps/q/ab;I)V

    const/16 v4, 0x2600

    const/16 v5, 0x2600

    invoke-virtual {v2, v4, v5}, Lmaps/q/au;->c(II)V

    move-object v5, v2

    :goto_4
    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_b

    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Lmaps/l/m;

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lmaps/l/m;

    move-object/from16 v0, p0

    iput-object v2, v0, Lmaps/l/at;->k:[Lmaps/l/m;

    sget-boolean v2, Lmaps/ae/h;->F:Z

    if-eqz v2, :cond_b

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/l/at;->k:[Lmaps/l/m;

    array-length v6, v4

    const/4 v2, 0x0

    :goto_5
    if-ge v2, v6, :cond_b

    aget-object v7, v4, v2

    move-object/from16 v0, p0

    iget-object v8, v0, Lmaps/l/at;->N:Lmaps/l/a;

    invoke-virtual {v7}, Lmaps/l/ak;->f()Ljava/lang/Iterable;

    move-result-object v7

    invoke-virtual {v8, v7}, Lmaps/l/a;->a(Ljava/lang/Iterable;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_b
    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_c

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Lmaps/l/ak;

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lmaps/l/ak;

    move-object/from16 v0, p0

    iput-object v2, v0, Lmaps/l/at;->l:[Lmaps/l/ak;

    sget-boolean v2, Lmaps/ae/h;->F:Z

    if-eqz v2, :cond_c

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/l/at;->l:[Lmaps/l/ak;

    array-length v6, v4

    const/4 v2, 0x0

    :goto_6
    if-ge v2, v6, :cond_c

    aget-object v7, v4, v2

    move-object/from16 v0, p0

    iget-object v8, v0, Lmaps/l/at;->N:Lmaps/l/a;

    invoke-virtual {v7}, Lmaps/l/ak;->f()Ljava/lang/Iterable;

    move-result-object v7

    invoke-virtual {v8, v7}, Lmaps/l/a;->a(Ljava/lang/Iterable;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    :cond_c
    const/4 v2, 0x0

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_f

    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [[Lmaps/l/ag;

    move-object/from16 v0, p0

    iput-object v2, v0, Lmaps/l/at;->n:[[Lmaps/l/ag;

    const/4 v2, 0x0

    move v6, v2

    :goto_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/l/at;->n:[[Lmaps/l/ag;

    array-length v2, v2

    if-ge v6, v2, :cond_f

    move-object/from16 v0, v26

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v7, v0, Lmaps/l/at;->n:[[Lmaps/l/ag;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [Lmaps/l/ag;

    invoke-interface {v2, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lmaps/l/ag;

    aput-object v4, v7, v6

    sget-boolean v4, Lmaps/ae/h;->F:Z

    if-eqz v4, :cond_e

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_d
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmaps/l/ag;

    move-object/from16 v0, p0

    iget-object v7, v0, Lmaps/l/at;->N:Lmaps/l/a;

    invoke-virtual {v2}, Lmaps/l/ak;->f()Ljava/lang/Iterable;

    move-result-object v8

    invoke-virtual {v7, v8}, Lmaps/l/a;->a(Ljava/lang/Iterable;)V

    invoke-virtual {v2}, Lmaps/l/ak;->f()Ljava/lang/Iterable;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_8
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmaps/q/aa;

    invoke-virtual {v2, v5}, Lmaps/q/aa;->a(Lmaps/q/z;)V

    goto :goto_8

    :cond_e
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto :goto_7

    :cond_f
    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_10

    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Lmaps/l/ak;

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lmaps/l/ak;

    move-object/from16 v0, p0

    iput-object v2, v0, Lmaps/l/at;->o:[Lmaps/l/ak;

    sget-boolean v2, Lmaps/ae/h;->F:Z

    if-eqz v2, :cond_10

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/l/at;->o:[Lmaps/l/ak;

    array-length v5, v4

    const/4 v2, 0x0

    :goto_9
    if-ge v2, v5, :cond_10

    aget-object v6, v4, v2

    move-object/from16 v0, p0

    iget-object v7, v0, Lmaps/l/at;->N:Lmaps/l/a;

    invoke-virtual {v6}, Lmaps/l/ak;->f()Ljava/lang/Iterable;

    move-result-object v6

    invoke-virtual {v7, v6}, Lmaps/l/a;->a(Ljava/lang/Iterable;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_9

    :cond_10
    invoke-virtual/range {v28 .. v28}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_11

    invoke-virtual/range {v28 .. v28}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Lmaps/l/c;

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lmaps/l/c;

    move-object/from16 v0, p0

    iput-object v2, v0, Lmaps/l/at;->q:[Lmaps/l/c;

    sget-boolean v2, Lmaps/ae/h;->F:Z

    if-eqz v2, :cond_11

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/l/at;->q:[Lmaps/l/c;

    array-length v5, v4

    const/4 v2, 0x0

    :goto_a
    if-ge v2, v5, :cond_11

    aget-object v6, v4, v2

    move-object/from16 v0, p0

    iget-object v7, v0, Lmaps/l/at;->N:Lmaps/l/a;

    invoke-virtual {v6}, Lmaps/l/ak;->f()Ljava/lang/Iterable;

    move-result-object v6

    invoke-virtual {v7, v6}, Lmaps/l/a;->a(Ljava/lang/Iterable;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_a

    :cond_11
    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_12

    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Lmaps/l/s;

    move-object/from16 v0, v29

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lmaps/l/s;

    move-object/from16 v0, p0

    iput-object v2, v0, Lmaps/l/at;->r:[Lmaps/l/s;

    sget-boolean v2, Lmaps/ae/h;->F:Z

    if-eqz v2, :cond_12

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/l/at;->r:[Lmaps/l/s;

    array-length v5, v4

    const/4 v2, 0x0

    :goto_b
    if-ge v2, v5, :cond_12

    aget-object v6, v4, v2

    move-object/from16 v0, p0

    iget-object v7, v0, Lmaps/l/at;->N:Lmaps/l/a;

    invoke-virtual {v6}, Lmaps/l/ak;->f()Ljava/lang/Iterable;

    move-result-object v6

    invoke-virtual {v7, v6}, Lmaps/l/a;->a(Ljava/lang/Iterable;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_b

    :cond_12
    const/4 v2, 0x3

    invoke-virtual {v11, v2}, Lmaps/l/k;->a(I)[Lmaps/l/q;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lmaps/l/at;->m:[Lmaps/l/q;

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/l/at;->m:[Lmaps/l/q;

    if-eqz v2, :cond_13

    sget-boolean v2, Lmaps/ae/h;->F:Z

    if-eqz v2, :cond_13

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/l/at;->m:[Lmaps/l/q;

    array-length v5, v4

    const/4 v2, 0x0

    :goto_c
    if-ge v2, v5, :cond_13

    aget-object v6, v4, v2

    move-object/from16 v0, p0

    iget-object v7, v0, Lmaps/l/at;->N:Lmaps/l/a;

    invoke-virtual {v6}, Lmaps/l/ak;->f()Ljava/lang/Iterable;

    move-result-object v6

    invoke-virtual {v7, v6}, Lmaps/l/a;->a(Ljava/lang/Iterable;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_c

    :cond_13
    const/16 v2, 0xa

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Lmaps/l/k;->a(I)[Lmaps/l/q;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lmaps/l/at;->p:[Lmaps/l/q;

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/l/at;->p:[Lmaps/l/q;

    if-eqz v2, :cond_14

    sget-boolean v2, Lmaps/ae/h;->F:Z

    if-eqz v2, :cond_14

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/l/at;->p:[Lmaps/l/q;

    array-length v5, v4

    const/4 v2, 0x0

    :goto_d
    if-ge v2, v5, :cond_14

    aget-object v6, v4, v2

    move-object/from16 v0, p0

    iget-object v7, v0, Lmaps/l/at;->N:Lmaps/l/a;

    invoke-virtual {v6}, Lmaps/l/ak;->f()Ljava/lang/Iterable;

    move-result-object v6

    invoke-virtual {v7, v6}, Lmaps/l/a;->a(Ljava/lang/Iterable;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_d

    :cond_14
    invoke-virtual/range {p1 .. p1}, Lmaps/t/ar;->c()I

    move-result v6

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v6}, Ljava/util/ArrayList;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lmaps/l/at;->t:Ljava/util/ArrayList;

    const/4 v2, 0x0

    move v5, v2

    :goto_e
    if-ge v5, v6, :cond_17

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lmaps/t/ar;->a(I)Lmaps/t/ca;

    move-result-object v4

    invoke-interface {v4}, Lmaps/t/ca;->a()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_15
    :goto_f
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_e

    :sswitch_0
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lmaps/l/at;->a(Lmaps/t/ca;)V

    move-object v2, v4

    check-cast v2, Lmaps/t/n;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lmaps/l/at;->a(Lmaps/t/n;)V

    invoke-interface {v4}, Lmaps/t/ca;->j()[I

    move-result-object v4

    array-length v7, v4

    const/4 v2, 0x0

    :goto_10
    if-ge v2, v7, :cond_15

    aget v8, v4, v2

    if-ltz v8, :cond_16

    array-length v9, v3

    if-ge v8, v9, :cond_16

    move-object/from16 v0, p0

    iget-object v9, v0, Lmaps/l/at;->x:Ljava/util/HashSet;

    aget-object v8, v3, v8

    invoke-virtual {v9, v8}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_16
    add-int/lit8 v2, v2, 0x1

    goto :goto_10

    :sswitch_1
    move-object v2, v4

    check-cast v2, Lmaps/t/az;

    invoke-virtual {v2}, Lmaps/t/az;->d()I

    move-result v2

    if-lez v2, :cond_15

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lmaps/l/at;->a(Lmaps/t/ca;)V

    goto :goto_f

    :sswitch_2
    move-object v2, v4

    check-cast v2, Lmaps/t/bu;

    invoke-virtual {v2}, Lmaps/t/bu;->d()I

    move-result v2

    if-lez v2, :cond_15

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lmaps/l/at;->a(Lmaps/t/ca;)V

    goto :goto_f

    :cond_17
    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/l/at;->t:Ljava/util/ArrayList;

    new-instance v3, Lmaps/l/f;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lmaps/l/f;-><init>(Lmaps/l/at;)V

    invoke-static {v2, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    move-object/from16 v0, p1

    instance-of v2, v0, Lmaps/t/f;

    if-eqz v2, :cond_18

    check-cast p1, Lmaps/t/f;

    invoke-virtual/range {p1 .. p1}, Lmaps/t/f;->e()Ljava/util/Set;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lmaps/l/at;->u:Ljava/util/Set;

    :cond_18
    sget-boolean v2, Lmaps/ae/h;->F:Z

    if-eqz v2, :cond_19

    sget-boolean v2, Lmaps/af/v;->a:Z

    if-eqz v2, :cond_19

    :cond_19
    return-void

    :cond_1a
    move-object v5, v2

    goto/16 :goto_4

    :cond_1b
    move-wide/from16 v5, v20

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_3
        :pswitch_6
        :pswitch_0
        :pswitch_4
        :pswitch_7
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_2
        0x7 -> :sswitch_0
        0x8 -> :sswitch_1
        0xb -> :sswitch_1
    .end sparse-switch
.end method

.method private a(Lmaps/t/ca;)V
    .locals 3

    iget-object v0, p0, Lmaps/l/at;->t:Ljava/util/ArrayList;

    new-instance v1, Lmaps/ac/h;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lmaps/ac/h;-><init>(Lmaps/t/ca;Lmaps/ac/g;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private k()I
    .locals 3

    const/16 v0, 0x18

    iget-object v1, p0, Lmaps/l/at;->t:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/h;

    invoke-virtual {v0}, Lmaps/ac/h;->d()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_0
    return v1
.end method


# virtual methods
.method public a(Lmaps/bq/d;Lmaps/af/q;)I
    .locals 3

    const/4 v0, 0x2

    const/4 v1, 0x0

    sget-boolean v2, Lmaps/ae/h;->F:Z

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lmaps/l/at;->k:[Lmaps/l/m;

    if-eqz v2, :cond_b

    :goto_1
    iget-object v1, p0, Lmaps/l/at;->l:[Lmaps/l/ak;

    if-eqz v1, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget-object v1, p0, Lmaps/l/at;->m:[Lmaps/l/q;

    if-eqz v1, :cond_3

    or-int/lit8 v0, v0, 0x8

    :cond_3
    iget-object v1, p0, Lmaps/l/at;->n:[[Lmaps/l/ag;

    if-eqz v1, :cond_4

    invoke-static {p1, p2}, Lmaps/l/ag;->a(Lmaps/bq/d;Lmaps/af/q;)I

    move-result v1

    or-int/2addr v0, v1

    :cond_4
    iget-object v1, p0, Lmaps/l/at;->o:[Lmaps/l/ak;

    if-eqz v1, :cond_5

    or-int/lit16 v0, v0, 0x200

    :cond_5
    iget-object v1, p0, Lmaps/l/at;->p:[Lmaps/l/q;

    if-eqz v1, :cond_6

    or-int/lit16 v0, v0, 0x400

    :cond_6
    iget-object v1, p0, Lmaps/l/at;->q:[Lmaps/l/c;

    if-eqz v1, :cond_8

    invoke-virtual {p1}, Lmaps/bq/d;->r()F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_7

    or-int/lit16 v0, v0, 0x800

    :cond_7
    or-int/lit16 v0, v0, 0x1000

    :cond_8
    iget-object v1, p0, Lmaps/l/at;->r:[Lmaps/l/s;

    if-eqz v1, :cond_9

    or-int/lit16 v0, v0, 0x2000

    :cond_9
    sget-boolean v1, Lmaps/ae/h;->J:Z

    if-eqz v1, :cond_a

    or-int/lit16 v0, v0, 0x4000

    :cond_a
    sget-boolean v1, Lmaps/af/v;->a:Z

    if-eqz v1, :cond_0

    const v1, 0x8000

    or-int/2addr v0, v1

    goto :goto_0

    :cond_b
    move v0, v1

    goto :goto_1
.end method

.method public a(J)V
    .locals 4

    iget-object v0, p0, Lmaps/l/at;->k:[Lmaps/l/m;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lmaps/l/at;->k:[Lmaps/l/m;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3, p1, p2}, Lmaps/l/m;->a(J)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(Lmaps/ac/g;)V
    .locals 4

    iget-object v0, p0, Lmaps/l/at;->F:Lmaps/ac/g;

    invoke-static {p1, v0}, Lmaps/ac/g;->a(Lmaps/ac/g;Lmaps/ac/g;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lmaps/l/at;->F:Lmaps/ac/g;

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lmaps/l/at;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lmaps/l/at;->t:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/h;

    invoke-virtual {v0}, Lmaps/ac/h;->a()Lmaps/t/ca;

    move-result-object v0

    iget-object v2, p0, Lmaps/l/at;->t:Ljava/util/ArrayList;

    new-instance v3, Lmaps/ac/h;

    invoke-direct {v3, v0, p1}, Lmaps/ac/h;-><init>(Lmaps/t/ca;Lmaps/ac/g;)V

    invoke-virtual {v2, v1, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Lmaps/l/at;->E:I

    goto :goto_0
.end method

.method public a(Lmaps/bq/d;ILjava/util/Collection;)V
    .locals 8

    const/4 v1, 0x0

    iget-object v0, p0, Lmaps/l/at;->x:Ljava/util/HashSet;

    invoke-interface {p3, v0}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lmaps/l/at;->k:[Lmaps/l/m;

    if-eqz v0, :cond_0

    and-int/lit8 v0, p2, 0x2

    if-eqz v0, :cond_0

    iget-object v2, p0, Lmaps/l/at;->k:[Lmaps/l/m;

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    invoke-virtual {v4}, Lmaps/l/m;->e()Ljava/util/Set;

    move-result-object v4

    invoke-interface {p3, v4}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmaps/l/at;->l:[Lmaps/l/ak;

    if-eqz v0, :cond_1

    and-int/lit8 v0, p2, 0x4

    if-eqz v0, :cond_1

    iget-object v2, p0, Lmaps/l/at;->l:[Lmaps/l/ak;

    array-length v3, v2

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    invoke-virtual {v4}, Lmaps/l/ak;->e()Ljava/util/Set;

    move-result-object v4

    invoke-interface {p3, v4}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lmaps/l/at;->m:[Lmaps/l/q;

    if-eqz v0, :cond_2

    and-int/lit8 v0, p2, 0x8

    if-eqz v0, :cond_2

    iget-object v2, p0, Lmaps/l/at;->m:[Lmaps/l/q;

    array-length v3, v2

    move v0, v1

    :goto_2
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    invoke-virtual {v4}, Lmaps/l/q;->e()Ljava/util/Set;

    move-result-object v4

    invoke-interface {p3, v4}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    const/16 v0, 0x70

    sget-boolean v2, Lmaps/ae/h;->O:Z

    if-eqz v2, :cond_3

    const/16 v0, 0x1f0

    :cond_3
    iget-object v2, p0, Lmaps/l/at;->n:[[Lmaps/l/ag;

    if-eqz v2, :cond_5

    and-int/2addr v0, p2

    if-eqz v0, :cond_5

    iget-object v3, p0, Lmaps/l/at;->n:[[Lmaps/l/ag;

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_5

    aget-object v5, v3, v2

    array-length v6, v5

    move v0, v1

    :goto_4
    if-ge v0, v6, :cond_4

    aget-object v7, v5, v0

    invoke-virtual {v7}, Lmaps/l/ag;->e()Ljava/util/Set;

    move-result-object v7

    invoke-interface {p3, v7}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_5
    iget-object v0, p0, Lmaps/l/at;->o:[Lmaps/l/ak;

    if-eqz v0, :cond_6

    and-int/lit16 v0, p2, 0x200

    if-eqz v0, :cond_6

    iget-object v2, p0, Lmaps/l/at;->o:[Lmaps/l/ak;

    array-length v3, v2

    move v0, v1

    :goto_5
    if-ge v0, v3, :cond_6

    aget-object v4, v2, v0

    invoke-virtual {v4}, Lmaps/l/ak;->e()Ljava/util/Set;

    move-result-object v4

    invoke-interface {p3, v4}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_6
    iget-object v0, p0, Lmaps/l/at;->p:[Lmaps/l/q;

    if-eqz v0, :cond_7

    and-int/lit16 v0, p2, 0x400

    if-eqz v0, :cond_7

    iget-object v2, p0, Lmaps/l/at;->p:[Lmaps/l/q;

    array-length v3, v2

    move v0, v1

    :goto_6
    if-ge v0, v3, :cond_7

    aget-object v4, v2, v0

    invoke-virtual {v4}, Lmaps/l/q;->e()Ljava/util/Set;

    move-result-object v4

    invoke-interface {p3, v4}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_7
    iget-object v0, p0, Lmaps/l/at;->q:[Lmaps/l/c;

    if-eqz v0, :cond_8

    and-int/lit16 v0, p2, 0x1800

    if-eqz v0, :cond_8

    iget-object v2, p0, Lmaps/l/at;->q:[Lmaps/l/c;

    array-length v3, v2

    move v0, v1

    :goto_7
    if-ge v0, v3, :cond_8

    aget-object v4, v2, v0

    invoke-virtual {v4}, Lmaps/l/c;->e()Ljava/util/Set;

    move-result-object v4

    invoke-interface {p3, v4}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_8
    iget-object v0, p0, Lmaps/l/at;->r:[Lmaps/l/s;

    if-eqz v0, :cond_9

    and-int/lit16 v0, p2, 0x2000

    if-eqz v0, :cond_9

    iget-object v2, p0, Lmaps/l/at;->r:[Lmaps/l/s;

    array-length v3, v2

    move v0, v1

    :goto_8
    if-ge v0, v3, :cond_9

    aget-object v1, v2, v0

    invoke-virtual {v1}, Lmaps/l/s;->e()Ljava/util/Set;

    move-result-object v1

    invoke-interface {p3, v1}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_9
    return-void
.end method

.method public a(Lmaps/bq/d;Ljava/util/Collection;)V
    .locals 0

    return-void
.end method

.method public a(Lmaps/bq/d;Lmaps/cr/c;)V
    .locals 3

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/l/at;->w:Lmaps/t/ax;

    invoke-virtual {v0}, Lmaps/t/ax;->d()Lmaps/t/bx;

    move-result-object v0

    iget-object v1, p0, Lmaps/l/at;->w:Lmaps/t/ax;

    invoke-virtual {v1}, Lmaps/t/ax;->g()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lmaps/l/at;->j:[F

    invoke-static {p2, p1, v0, v1, v2}, Lmaps/af/d;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/t/bx;F[F)V

    iget-object v0, p0, Lmaps/l/at;->N:Lmaps/l/a;

    iget-object v1, p0, Lmaps/l/at;->v:Lmaps/t/ah;

    invoke-virtual {v1}, Lmaps/t/ah;->c()I

    move-result v1

    invoke-virtual {p1}, Lmaps/bq/d;->s()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lmaps/l/a;->a(IF)V

    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iput-object v0, p0, Lmaps/l/at;->G:Ljava/lang/Boolean;

    :cond_0
    return-void
.end method

.method public a(Lmaps/cr/c;)V
    .locals 8

    const/4 v0, 0x0

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iput-object v1, p0, Lmaps/l/at;->G:Ljava/lang/Boolean;

    iget-object v1, p0, Lmaps/l/at;->k:[Lmaps/l/m;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lmaps/l/at;->k:[Lmaps/l/m;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    invoke-virtual {v4, p1}, Lmaps/l/m;->a(Lmaps/cr/c;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lmaps/l/at;->l:[Lmaps/l/ak;

    if-eqz v1, :cond_1

    iget-object v2, p0, Lmaps/l/at;->l:[Lmaps/l/ak;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    invoke-interface {v4, p1}, Lmaps/l/t;->a(Lmaps/cr/c;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lmaps/l/at;->m:[Lmaps/l/q;

    if-eqz v1, :cond_2

    iget-object v2, p0, Lmaps/l/at;->m:[Lmaps/l/q;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    invoke-virtual {v4, p1}, Lmaps/l/q;->a(Lmaps/cr/c;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    iget-object v1, p0, Lmaps/l/at;->n:[[Lmaps/l/ag;

    if-eqz v1, :cond_4

    iget-object v3, p0, Lmaps/l/at;->n:[[Lmaps/l/ag;

    array-length v4, v3

    move v2, v0

    :goto_3
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    array-length v6, v5

    move v1, v0

    :goto_4
    if-ge v1, v6, :cond_3

    aget-object v7, v5, v1

    invoke-virtual {v7, p1}, Lmaps/l/ag;->a(Lmaps/cr/c;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    :cond_4
    iget-object v1, p0, Lmaps/l/at;->o:[Lmaps/l/ak;

    if-eqz v1, :cond_5

    iget-object v2, p0, Lmaps/l/at;->o:[Lmaps/l/ak;

    array-length v3, v2

    move v1, v0

    :goto_5
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    invoke-interface {v4, p1}, Lmaps/l/t;->a(Lmaps/cr/c;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_5
    iget-object v1, p0, Lmaps/l/at;->p:[Lmaps/l/q;

    if-eqz v1, :cond_6

    iget-object v2, p0, Lmaps/l/at;->p:[Lmaps/l/q;

    array-length v3, v2

    move v1, v0

    :goto_6
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    invoke-virtual {v4, p1}, Lmaps/l/q;->a(Lmaps/cr/c;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_6
    iget-object v1, p0, Lmaps/l/at;->q:[Lmaps/l/c;

    if-eqz v1, :cond_7

    iget-object v2, p0, Lmaps/l/at;->q:[Lmaps/l/c;

    array-length v3, v2

    move v1, v0

    :goto_7
    if-ge v1, v3, :cond_7

    aget-object v4, v2, v1

    invoke-virtual {v4, p1}, Lmaps/l/c;->a(Lmaps/cr/c;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    :cond_7
    iget-object v1, p0, Lmaps/l/at;->s:Lmaps/l/u;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lmaps/l/at;->s:Lmaps/l/u;

    invoke-virtual {v1, p1}, Lmaps/l/u;->a(Lmaps/cr/c;)V

    const/4 v1, 0x0

    iput-object v1, p0, Lmaps/l/at;->s:Lmaps/l/u;

    :cond_8
    iget-object v1, p0, Lmaps/l/at;->r:[Lmaps/l/s;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lmaps/l/at;->r:[Lmaps/l/s;

    array-length v2, v1

    :goto_8
    if-ge v0, v2, :cond_9

    aget-object v3, v1, v0

    invoke-virtual {v3, p1}, Lmaps/l/s;->a(Lmaps/cr/c;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_9
    return-void
.end method

.method public a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 11

    const/high16 v0, 0x3f800000

    sget-boolean v1, Lmaps/ae/h;->J:Z

    if-eqz v1, :cond_1

    iget-wide v1, p0, Lmaps/l/at;->K:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lmaps/cr/c;->e()J

    move-result-wide v1

    iput-wide v1, p0, Lmaps/l/at;->K:J

    invoke-virtual {p1}, Lmaps/cr/c;->b()V

    :cond_0
    invoke-virtual {p1}, Lmaps/cr/c;->e()J

    move-result-wide v1

    iget-wide v3, p0, Lmaps/l/at;->K:J

    sub-long/2addr v1, v3

    long-to-int v1, v1

    const/16 v2, 0x3e8

    if-ge v1, v2, :cond_1

    int-to-float v0, v1

    const/high16 v1, 0x447a0000

    div-float/2addr v0, v1

    invoke-virtual {p1}, Lmaps/cr/c;->b()V

    :cond_1
    sget-boolean v1, Lmaps/ae/h;->F:Z

    if-eqz v1, :cond_3

    :cond_2
    :goto_0
    return-void

    :cond_3
    invoke-virtual {p1}, Lmaps/cr/c;->M()I

    move-result v1

    if-lez v1, :cond_7

    invoke-virtual {p1}, Lmaps/cr/c;->N()Z

    move-result v1

    if-eqz v1, :cond_7

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v3

    invoke-interface {v3}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    invoke-virtual {p2}, Lmaps/bq/d;->g()J

    move-result-wide v4

    iget-wide v6, p0, Lmaps/l/at;->D:J

    cmp-long v2, v4, v6

    if-eqz v2, :cond_5

    invoke-virtual {p2}, Lmaps/bq/d;->g()J

    move-result-wide v4

    iput-wide v4, p0, Lmaps/l/at;->D:J

    iget-object v2, p0, Lmaps/l/at;->w:Lmaps/t/ax;

    invoke-virtual {v2}, Lmaps/t/ax;->d()Lmaps/t/bx;

    move-result-object v2

    invoke-virtual {p2}, Lmaps/bq/d;->k()Z

    move-result v4

    if-nez v4, :cond_4

    invoke-virtual {p2}, Lmaps/bq/d;->r()F

    move-result v4

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-nez v4, :cond_4

    invoke-virtual {p2}, Lmaps/bq/d;->q()F

    move-result v4

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-nez v4, :cond_4

    invoke-virtual {p2}, Lmaps/bq/d;->s()F

    move-result v4

    invoke-virtual {p2}, Lmaps/bq/d;->s()F

    move-result v5

    float-to-int v5, v5

    int-to-float v5, v5

    cmpl-float v4, v4, v5

    if-nez v4, :cond_4

    sget-object v4, Lmaps/l/at;->L:[F

    invoke-virtual {p2, v2, v4}, Lmaps/bq/d;->a(Lmaps/t/bx;[F)V

    sget-object v2, Lmaps/l/at;->L:[F

    const/4 v4, 0x0

    aget v2, v2, v4

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    int-to-float v2, v2

    sget-object v4, Lmaps/l/at;->L:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {p2, v2, v4}, Lmaps/bq/d;->d(FF)Lmaps/t/bx;

    move-result-object v2

    :cond_4
    iget-object v4, p0, Lmaps/l/at;->w:Lmaps/t/ax;

    invoke-virtual {v4}, Lmaps/t/ax;->g()I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, Lmaps/l/at;->C:[F

    invoke-static {p1, p2, v2, v4, v5}, Lmaps/af/d;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/t/bx;F[F)V

    :cond_5
    iget-object v2, p0, Lmaps/l/at;->C:[F

    invoke-static {v3, v2}, Lmaps/af/d;->a(Ljavax/microedition/khronos/opengles/GL10;[F)V

    invoke-interface {p3}, Lmaps/af/s;->b()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_6
    :goto_2
    invoke-interface {v3}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    iget-object v0, p0, Lmaps/l/at;->G:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lmaps/cr/c;->L()Z

    move-result v0

    if-eqz v0, :cond_14

    sget-boolean v0, Lmaps/bm/b;->a:Z

    if-nez v0, :cond_14

    iget-object v0, p0, Lmaps/l/at;->l:[Lmaps/l/ak;

    if-nez v0, :cond_13

    iget-object v0, p0, Lmaps/l/at;->n:[[Lmaps/l/ag;

    if-nez v0, :cond_13

    iget-object v0, p0, Lmaps/l/at;->m:[Lmaps/l/q;

    if-nez v0, :cond_13

    iget-object v0, p0, Lmaps/l/at;->o:[Lmaps/l/ak;

    if-nez v0, :cond_13

    iget-object v0, p0, Lmaps/l/at;->p:[Lmaps/l/q;

    if-nez v0, :cond_13

    iget-object v0, p0, Lmaps/l/at;->q:[Lmaps/l/c;

    if-nez v0, :cond_13

    iget-object v0, p0, Lmaps/l/at;->r:[Lmaps/l/s;

    if-nez v0, :cond_13

    const/4 v0, 0x1

    :goto_3
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/at;->G:Ljava/lang/Boolean;

    goto/16 :goto_0

    :cond_7
    const/4 v1, 0x0

    goto/16 :goto_1

    :pswitch_0
    iget-object v0, p0, Lmaps/l/at;->k:[Lmaps/l/m;

    if-eqz v0, :cond_6

    iget-object v1, p0, Lmaps/l/at;->k:[Lmaps/l/m;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v2, :cond_6

    aget-object v4, v1, v0

    invoke-virtual {v4, p1, p2, p3}, Lmaps/l/m;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :pswitch_1
    iget-object v0, p0, Lmaps/l/at;->l:[Lmaps/l/ak;

    if-eqz v0, :cond_8

    iget-object v1, p0, Lmaps/l/at;->l:[Lmaps/l/ak;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_5
    if-ge v0, v2, :cond_8

    aget-object v4, v1, v0

    invoke-interface {v4, p1, p2, p3}, Lmaps/l/t;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_8
    iget-object v0, p0, Lmaps/l/at;->n:[[Lmaps/l/ag;

    if-eqz v0, :cond_6

    iget-object v2, p0, Lmaps/l/at;->n:[[Lmaps/l/ag;

    array-length v4, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_6
    if-ge v1, v4, :cond_6

    aget-object v5, v2, v1

    array-length v6, v5

    const/4 v0, 0x0

    :goto_7
    if-ge v0, v6, :cond_9

    aget-object v7, v5, v0

    invoke-virtual {v7, p1, p2, p3}, Lmaps/l/ag;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_9
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    :pswitch_2
    iget-object v0, p0, Lmaps/l/at;->m:[Lmaps/l/q;

    if-eqz v0, :cond_6

    iget-object v1, p0, Lmaps/l/at;->m:[Lmaps/l/q;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_8
    if-ge v0, v2, :cond_6

    aget-object v4, v1, v0

    invoke-virtual {v4, p1, p2, p3}, Lmaps/l/q;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :pswitch_3
    if-nez v1, :cond_6

    iget-object v0, p0, Lmaps/l/at;->n:[[Lmaps/l/ag;

    if-eqz v0, :cond_6

    iget-object v2, p0, Lmaps/l/at;->n:[[Lmaps/l/ag;

    array-length v4, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_9
    if-ge v1, v4, :cond_6

    aget-object v5, v2, v1

    array-length v6, v5

    const/4 v0, 0x0

    :goto_a
    if-ge v0, v6, :cond_a

    aget-object v7, v5, v0

    invoke-virtual {v7, p1, p2, p3}, Lmaps/l/ag;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_a
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_9

    :pswitch_4
    if-eqz v1, :cond_f

    iget-object v0, p0, Lmaps/l/at;->n:[[Lmaps/l/ag;

    if-eqz v0, :cond_f

    invoke-interface {p3}, Lmaps/af/s;->a()Lmaps/af/q;

    move-result-object v0

    invoke-static {p2, v0}, Lmaps/l/ag;->a(Lmaps/bq/d;Lmaps/af/q;)I

    move-result v2

    new-instance v4, Lmaps/af/t;

    const/4 v1, 0x4

    invoke-direct {v4, v0, v1}, Lmaps/af/t;-><init>(Lmaps/af/q;I)V

    new-instance v5, Lmaps/af/t;

    const/4 v1, 0x6

    invoke-direct {v5, v0, v1}, Lmaps/af/t;-><init>(Lmaps/af/q;I)V

    iget-object v6, p0, Lmaps/l/at;->n:[[Lmaps/l/ag;

    array-length v7, v6

    const/4 v0, 0x0

    move v1, v0

    :goto_b
    if-ge v1, v7, :cond_6

    aget-object v8, v6, v1

    and-int/lit8 v0, v2, 0x10

    if-eqz v0, :cond_b

    invoke-virtual {p0, p1, p2, v4}, Lmaps/l/at;->b(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    array-length v9, v8

    const/4 v0, 0x0

    :goto_c
    if-ge v0, v9, :cond_b

    aget-object v10, v8, v0

    invoke-virtual {v10, p1, p2, v4}, Lmaps/l/ag;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    :cond_b
    and-int/lit8 v0, v2, 0x20

    if-eqz v0, :cond_c

    invoke-virtual {p0, p1, p2, p3}, Lmaps/l/at;->b(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    array-length v9, v8

    const/4 v0, 0x0

    :goto_d
    if-ge v0, v9, :cond_c

    aget-object v10, v8, v0

    invoke-virtual {v10, p1, p2, p3}, Lmaps/l/ag;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    :cond_c
    and-int/lit8 v0, v2, 0x40

    if-eqz v0, :cond_d

    invoke-virtual {p1}, Lmaps/cr/c;->q()V

    invoke-virtual {p0, p1, p2, v5}, Lmaps/l/at;->b(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    array-length v9, v8

    const/4 v0, 0x0

    :goto_e
    if-ge v0, v9, :cond_d

    aget-object v10, v8, v0

    invoke-virtual {v10, p1, p2, v5}, Lmaps/l/ag;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_e

    :cond_d
    sget-boolean v0, Lmaps/ae/h;->O:Z

    if-eqz v0, :cond_e

    and-int/lit16 v0, v2, 0x180

    if-eqz v0, :cond_e

    invoke-virtual {p1}, Lmaps/cr/c;->p()V

    invoke-virtual {p0, p1, p2, p3}, Lmaps/l/at;->b(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    array-length v9, v8

    const/4 v0, 0x0

    :goto_f
    if-ge v0, v9, :cond_e

    aget-object v10, v8, v0

    invoke-virtual {v10, p1, p2, p3}, Lmaps/l/ag;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_f

    :cond_e
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_b

    :cond_f
    iget-object v0, p0, Lmaps/l/at;->n:[[Lmaps/l/ag;

    if-eqz v0, :cond_6

    iget-object v2, p0, Lmaps/l/at;->n:[[Lmaps/l/ag;

    array-length v4, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_10
    if-ge v1, v4, :cond_6

    aget-object v5, v2, v1

    array-length v6, v5

    const/4 v0, 0x0

    :goto_11
    if-ge v0, v6, :cond_10

    aget-object v7, v5, v0

    invoke-virtual {v7, p1, p2, p3}, Lmaps/l/ag;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_11

    :cond_10
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_10

    :pswitch_5
    if-nez v1, :cond_6

    iget-object v0, p0, Lmaps/l/at;->n:[[Lmaps/l/ag;

    if-eqz v0, :cond_6

    iget-object v2, p0, Lmaps/l/at;->n:[[Lmaps/l/ag;

    array-length v4, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_12
    if-ge v1, v4, :cond_6

    aget-object v5, v2, v1

    array-length v6, v5

    const/4 v0, 0x0

    :goto_13
    if-ge v0, v6, :cond_11

    aget-object v7, v5, v0

    invoke-virtual {v7, p1, p2, p3}, Lmaps/l/ag;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_13

    :cond_11
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_12

    :pswitch_6
    sget-boolean v0, Lmaps/ae/h;->O:Z

    if-eqz v0, :cond_6

    if-nez v1, :cond_6

    iget-object v0, p0, Lmaps/l/at;->n:[[Lmaps/l/ag;

    if-eqz v0, :cond_6

    iget-object v2, p0, Lmaps/l/at;->n:[[Lmaps/l/ag;

    array-length v4, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_14
    if-ge v1, v4, :cond_6

    aget-object v5, v2, v1

    array-length v6, v5

    const/4 v0, 0x0

    :goto_15
    if-ge v0, v6, :cond_12

    aget-object v7, v5, v0

    invoke-virtual {v7, p1, p2, p3}, Lmaps/l/ag;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_15

    :cond_12
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_14

    :pswitch_7
    iget-object v0, p0, Lmaps/l/at;->o:[Lmaps/l/ak;

    if-eqz v0, :cond_6

    iget-object v1, p0, Lmaps/l/at;->o:[Lmaps/l/ak;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_16
    if-ge v0, v2, :cond_6

    aget-object v4, v1, v0

    invoke-interface {v4, p1, p2, p3}, Lmaps/l/t;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_16

    :pswitch_8
    iget-object v0, p0, Lmaps/l/at;->p:[Lmaps/l/q;

    if-eqz v0, :cond_6

    iget-object v1, p0, Lmaps/l/at;->p:[Lmaps/l/q;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_17
    if-ge v0, v2, :cond_6

    aget-object v4, v1, v0

    invoke-virtual {v4, p1, p2, p3}, Lmaps/l/q;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_17

    :pswitch_9
    iget-object v0, p0, Lmaps/l/at;->q:[Lmaps/l/c;

    if-eqz v0, :cond_6

    iget-object v1, p0, Lmaps/l/at;->q:[Lmaps/l/c;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_18
    if-ge v0, v2, :cond_6

    aget-object v4, v1, v0

    invoke-virtual {v4, p1, p2, p3}, Lmaps/l/c;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_18

    :pswitch_a
    iget-object v0, p0, Lmaps/l/at;->r:[Lmaps/l/s;

    if-eqz v0, :cond_6

    iget-object v1, p0, Lmaps/l/at;->r:[Lmaps/l/s;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_19
    if-ge v0, v2, :cond_6

    aget-object v4, v1, v0

    invoke-virtual {v4, p1, p2, p3}, Lmaps/l/s;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_19

    :pswitch_b
    const/high16 v1, 0x3f800000

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_6

    invoke-direct {p0, p1, v0}, Lmaps/l/at;->a(Lmaps/cr/c;F)V

    goto/16 :goto_2

    :pswitch_c
    sget-object v0, Lmaps/l/aa;->a:Lmaps/l/aa;

    invoke-virtual {v0, p1, p2, p3}, Lmaps/l/aa;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    goto/16 :goto_2

    :cond_13
    const/4 v0, 0x0

    goto/16 :goto_3

    :cond_14
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v0, p0, Lmaps/l/at;->G:Ljava/lang/Boolean;

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method public a(Lmaps/l/u;)V
    .locals 0

    iput-object p1, p0, Lmaps/l/at;->s:Lmaps/l/u;

    return-void
.end method

.method public a(Lmaps/t/n;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lmaps/l/at;->z:Lmaps/p/ap;

    if-nez v0, :cond_0

    invoke-static {}, Lmaps/be/b;->c()Lmaps/p/k;

    move-result-object v0

    iget-object v1, p0, Lmaps/l/at;->w:Lmaps/t/ax;

    invoke-virtual {v1}, Lmaps/t/ax;->f()Lmaps/t/bx;

    move-result-object v1

    iget-object v2, p0, Lmaps/l/at;->A:Lmaps/o/c;

    invoke-virtual {v0, v1, v2}, Lmaps/p/k;->a(Lmaps/t/bx;Lmaps/o/c;)Lmaps/p/ap;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/at;->z:Lmaps/p/ap;

    :cond_0
    iget-object v0, p0, Lmaps/l/at;->z:Lmaps/p/ap;

    invoke-virtual {p1}, Lmaps/t/n;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lmaps/p/ap;->d(I)F

    move-result v0

    cmpg-float v1, v0, v3

    if-gez v1, :cond_1

    invoke-virtual {p1}, Lmaps/t/n;->e()I

    move-result v0

    int-to-float v0, v0

    :cond_1
    invoke-virtual {p1, v0}, Lmaps/t/n;->a(F)V

    iget-object v0, p0, Lmaps/l/at;->z:Lmaps/p/ap;

    invoke-virtual {p1}, Lmaps/t/n;->f()I

    move-result v1

    invoke-virtual {v0, v1}, Lmaps/p/ap;->e(I)F

    move-result v0

    cmpg-float v1, v0, v3

    if-gez v1, :cond_2

    const/16 v0, 0x16

    invoke-virtual {p1}, Lmaps/t/n;->f()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-float v0, v0

    :cond_2
    invoke-virtual {p1, v0}, Lmaps/t/n;->b(F)V

    return-void
.end method

.method public a(Z)V
    .locals 1

    iget-boolean v0, p0, Lmaps/l/at;->M:Z

    if-ne v0, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-boolean p1, p0, Lmaps/l/at;->M:Z

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/l/at;->N:Lmaps/l/a;

    invoke-virtual {v0, p1}, Lmaps/l/a;->a(Z)V

    goto :goto_0
.end method

.method public a()Z
    .locals 2

    const/4 v0, 0x0

    sget-boolean v1, Lmaps/ae/h;->F:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lmaps/l/at;->G:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lmaps/l/at;->G:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Ljava/util/Set;)Z
    .locals 1

    iget-object v0, p0, Lmaps/l/at;->u:Ljava/util/Set;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/l/at;->u:Ljava/util/Set;

    invoke-interface {p1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/l/at;->u:Ljava/util/Set;

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lmaps/ae/d;)Z
    .locals 4

    iget-wide v0, p0, Lmaps/l/at;->H:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    invoke-interface {p1}, Lmaps/ae/d;->b()J

    move-result-wide v0

    iget-wide v2, p0, Lmaps/l/at;->H:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lmaps/p/ac;)Z
    .locals 1

    iget-object v0, p0, Lmaps/l/at;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/l/at;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmaps/p/ac;->a(Ljava/util/Iterator;)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b()Lmaps/t/ah;
    .locals 1

    iget-object v0, p0, Lmaps/l/at;->v:Lmaps/t/ah;

    return-object v0
.end method

.method public b(J)V
    .locals 4

    const-wide/16 v2, 0x0

    cmp-long v0, p1, v2

    if-gez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-wide v0, p0, Lmaps/l/at;->H:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_2

    iget-wide v0, p0, Lmaps/l/at;->H:J

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    :cond_2
    iput-wide p1, p0, Lmaps/l/at;->H:J

    const-wide/32 v0, 0xea60

    add-long/2addr v0, p1

    iput-wide v0, p0, Lmaps/l/at;->I:J

    goto :goto_0
.end method

.method public b(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 3

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {p3}, Lmaps/af/s;->a()Lmaps/af/q;

    move-result-object v0

    invoke-interface {p3}, Lmaps/af/s;->b()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    invoke-static {p1, v0}, Lmaps/l/m;->a(Lmaps/cr/c;Lmaps/af/q;)V

    goto :goto_0

    :pswitch_2
    invoke-static {p1, p3}, Lmaps/l/av;->a(Lmaps/cr/c;Lmaps/af/s;)V

    goto :goto_0

    :pswitch_3
    invoke-static {p1, v0}, Lmaps/l/q;->a(Lmaps/cr/c;Lmaps/af/q;)V

    goto :goto_0

    :pswitch_4
    invoke-virtual {p2}, Lmaps/bq/d;->s()F

    move-result v1

    iget-object v2, p0, Lmaps/l/at;->v:Lmaps/t/ah;

    invoke-virtual {v2}, Lmaps/t/ah;->c()I

    move-result v2

    invoke-static {p1, v1, v2, v0}, Lmaps/l/ag;->a(Lmaps/cr/c;FILmaps/af/q;)V

    goto :goto_0

    :pswitch_5
    invoke-virtual {p2}, Lmaps/bq/d;->s()F

    move-result v1

    iget-object v2, p0, Lmaps/l/at;->v:Lmaps/t/ah;

    invoke-virtual {v2}, Lmaps/t/ah;->c()I

    move-result v2

    invoke-static {p1, v1, v2, v0}, Lmaps/l/ag;->b(Lmaps/cr/c;FILmaps/af/q;)V

    goto :goto_0

    :pswitch_6
    invoke-virtual {p2}, Lmaps/bq/d;->s()F

    move-result v1

    invoke-static {p1, v1, v0}, Lmaps/l/ag;->a(Lmaps/cr/c;FLmaps/af/q;)V

    goto :goto_0

    :pswitch_7
    sget-boolean v1, Lmaps/ae/h;->O:Z

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Lmaps/bq/d;->s()F

    move-result v1

    iget-object v2, p0, Lmaps/l/at;->v:Lmaps/t/ah;

    invoke-virtual {v2}, Lmaps/t/ah;->c()I

    move-result v2

    invoke-static {p1, v1, v2, v0}, Lmaps/l/ag;->c(Lmaps/cr/c;FILmaps/af/q;)V

    goto :goto_0

    :pswitch_8
    sget-boolean v1, Lmaps/ae/h;->O:Z

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Lmaps/bq/d;->s()F

    move-result v1

    invoke-static {p1, v1, v0}, Lmaps/l/ag;->b(Lmaps/cr/c;FLmaps/af/q;)V

    goto :goto_0

    :pswitch_9
    invoke-static {p1, p3}, Lmaps/l/av;->a(Lmaps/cr/c;Lmaps/af/s;)V

    goto :goto_0

    :pswitch_a
    invoke-static {p1, v0}, Lmaps/l/q;->a(Lmaps/cr/c;Lmaps/af/q;)V

    goto :goto_0

    :pswitch_b
    invoke-static {p1, v1}, Lmaps/l/c;->a(Lmaps/cr/c;I)V

    goto :goto_0

    :pswitch_c
    invoke-static {p1, v1}, Lmaps/l/c;->a(Lmaps/cr/c;I)V

    goto :goto_0

    :pswitch_d
    invoke-static {p1, v0}, Lmaps/l/s;->a(Lmaps/cr/c;Lmaps/af/q;)V

    goto :goto_0

    :pswitch_e
    invoke-static {p1, p3}, Lmaps/l/aa;->a(Lmaps/cr/c;Lmaps/af/s;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_c
        :pswitch_b
        :pswitch_d
        :pswitch_0
        :pswitch_e
    .end packed-switch
.end method

.method public b(Lmaps/ae/d;)Z
    .locals 4

    iget-wide v0, p0, Lmaps/l/at;->I:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    invoke-interface {p1}, Lmaps/ae/d;->b()J

    move-result-wide v0

    iget-wide v2, p0, Lmaps/l/at;->I:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Lmaps/cr/c;)V
    .locals 8

    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/l/at;->k:[Lmaps/l/m;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lmaps/l/at;->k:[Lmaps/l/m;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    invoke-virtual {v4, p1}, Lmaps/l/m;->c(Lmaps/cr/c;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lmaps/l/at;->l:[Lmaps/l/ak;

    if-eqz v1, :cond_1

    iget-object v2, p0, Lmaps/l/at;->l:[Lmaps/l/ak;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    invoke-interface {v4, p1}, Lmaps/l/t;->c(Lmaps/cr/c;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lmaps/l/at;->m:[Lmaps/l/q;

    if-eqz v1, :cond_2

    iget-object v2, p0, Lmaps/l/at;->m:[Lmaps/l/q;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    invoke-virtual {v4, p1}, Lmaps/l/q;->c(Lmaps/cr/c;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    iget-object v1, p0, Lmaps/l/at;->n:[[Lmaps/l/ag;

    if-eqz v1, :cond_4

    iget-object v3, p0, Lmaps/l/at;->n:[[Lmaps/l/ag;

    array-length v4, v3

    move v2, v0

    :goto_3
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    array-length v6, v5

    move v1, v0

    :goto_4
    if-ge v1, v6, :cond_3

    aget-object v7, v5, v1

    invoke-virtual {v7, p1}, Lmaps/l/ag;->c(Lmaps/cr/c;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    :cond_4
    iget-object v1, p0, Lmaps/l/at;->o:[Lmaps/l/ak;

    if-eqz v1, :cond_5

    iget-object v2, p0, Lmaps/l/at;->o:[Lmaps/l/ak;

    array-length v3, v2

    move v1, v0

    :goto_5
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    invoke-interface {v4, p1}, Lmaps/l/t;->c(Lmaps/cr/c;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_5
    iget-object v1, p0, Lmaps/l/at;->p:[Lmaps/l/q;

    if-eqz v1, :cond_6

    iget-object v2, p0, Lmaps/l/at;->p:[Lmaps/l/q;

    array-length v3, v2

    move v1, v0

    :goto_6
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    invoke-virtual {v4, p1}, Lmaps/l/q;->c(Lmaps/cr/c;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_6
    iget-object v1, p0, Lmaps/l/at;->q:[Lmaps/l/c;

    if-eqz v1, :cond_7

    iget-object v2, p0, Lmaps/l/at;->q:[Lmaps/l/c;

    array-length v3, v2

    move v1, v0

    :goto_7
    if-ge v1, v3, :cond_7

    aget-object v4, v2, v1

    invoke-virtual {v4, p1}, Lmaps/l/c;->c(Lmaps/cr/c;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    :cond_7
    iget-object v1, p0, Lmaps/l/at;->s:Lmaps/l/u;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lmaps/l/at;->s:Lmaps/l/u;

    invoke-virtual {v1, p1}, Lmaps/l/u;->c(Lmaps/cr/c;)V

    const/4 v1, 0x0

    iput-object v1, p0, Lmaps/l/at;->s:Lmaps/l/u;

    :cond_8
    iget-object v1, p0, Lmaps/l/at;->r:[Lmaps/l/s;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lmaps/l/at;->r:[Lmaps/l/s;

    array-length v2, v1

    :goto_8
    if-ge v0, v2, :cond_9

    aget-object v3, v1, v0

    invoke-virtual {v3, p1}, Lmaps/l/s;->c(Lmaps/cr/c;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_9
    return-void
.end method

.method public c()Z
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/l/at;->k:[Lmaps/l/m;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lmaps/l/at;->k:[Lmaps/l/m;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    invoke-virtual {v4}, Lmaps/l/m;->c()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v0, 0x1

    :cond_0
    return v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public d()V
    .locals 4

    iget-object v0, p0, Lmaps/l/at;->k:[Lmaps/l/m;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lmaps/l/at;->k:[Lmaps/l/m;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3}, Lmaps/l/m;->d()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public e()I
    .locals 1

    iget v0, p0, Lmaps/l/at;->y:I

    return v0
.end method

.method public f()I
    .locals 9

    const/4 v1, 0x0

    const/16 v0, 0x100

    iget-object v2, p0, Lmaps/l/at;->k:[Lmaps/l/m;

    if-eqz v2, :cond_0

    const/16 v0, 0x110

    iget-object v4, p0, Lmaps/l/at;->k:[Lmaps/l/m;

    array-length v5, v4

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_0

    aget-object v3, v4, v2

    invoke-virtual {v3}, Lmaps/l/m;->b()I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lmaps/l/at;->m:[Lmaps/l/q;

    if-eqz v2, :cond_1

    add-int/lit8 v0, v0, 0x10

    iget-object v4, p0, Lmaps/l/at;->m:[Lmaps/l/q;

    array-length v5, v4

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_1

    aget-object v3, v4, v2

    invoke-virtual {v3}, Lmaps/l/q;->b()I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_1

    :cond_1
    iget-object v2, p0, Lmaps/l/at;->n:[[Lmaps/l/ag;

    if-eqz v2, :cond_3

    add-int/lit8 v0, v0, 0x10

    iget-object v4, p0, Lmaps/l/at;->n:[[Lmaps/l/ag;

    array-length v5, v4

    move v3, v1

    :goto_2
    if-ge v3, v5, :cond_3

    aget-object v6, v4, v3

    array-length v7, v6

    move v2, v0

    move v0, v1

    :goto_3
    if-ge v0, v7, :cond_2

    aget-object v8, v6, v0

    invoke-virtual {v8}, Lmaps/l/ag;->b()I

    move-result v8

    add-int/2addr v2, v8

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v0, v2

    goto :goto_2

    :cond_3
    iget-object v2, p0, Lmaps/l/at;->p:[Lmaps/l/q;

    if-eqz v2, :cond_4

    add-int/lit8 v0, v0, 0x10

    iget-object v4, p0, Lmaps/l/at;->p:[Lmaps/l/q;

    array-length v5, v4

    move v2, v1

    :goto_4
    if-ge v2, v5, :cond_4

    aget-object v3, v4, v2

    invoke-virtual {v3}, Lmaps/l/q;->b()I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_4

    :cond_4
    iget-object v2, p0, Lmaps/l/at;->q:[Lmaps/l/c;

    if-eqz v2, :cond_5

    add-int/lit8 v0, v0, 0x10

    iget-object v4, p0, Lmaps/l/at;->q:[Lmaps/l/c;

    array-length v5, v4

    move v2, v1

    :goto_5
    if-ge v2, v5, :cond_5

    aget-object v3, v4, v2

    invoke-virtual {v3}, Lmaps/l/c;->b()I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_5

    :cond_5
    iget-object v2, p0, Lmaps/l/at;->r:[Lmaps/l/s;

    if-eqz v2, :cond_6

    add-int/lit8 v0, v0, 0x10

    iget-object v4, p0, Lmaps/l/at;->r:[Lmaps/l/s;

    array-length v5, v4

    move v2, v1

    :goto_6
    if-ge v2, v5, :cond_6

    aget-object v3, v4, v2

    invoke-virtual {v3}, Lmaps/l/s;->b()I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_6

    :cond_6
    iget-object v2, p0, Lmaps/l/at;->l:[Lmaps/l/ak;

    if-eqz v2, :cond_7

    add-int/lit8 v0, v0, 0x10

    iget-object v4, p0, Lmaps/l/at;->l:[Lmaps/l/ak;

    array-length v5, v4

    move v2, v1

    :goto_7
    if-ge v2, v5, :cond_7

    aget-object v3, v4, v2

    invoke-virtual {v3}, Lmaps/l/ak;->b()I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_7

    :cond_7
    iget-object v2, p0, Lmaps/l/at;->o:[Lmaps/l/ak;

    if-eqz v2, :cond_8

    add-int/lit8 v0, v0, 0x10

    iget-object v3, p0, Lmaps/l/at;->o:[Lmaps/l/ak;

    array-length v4, v3

    :goto_8
    if-ge v1, v4, :cond_8

    aget-object v2, v3, v1

    invoke-virtual {v2}, Lmaps/l/ak;->b()I

    move-result v2

    add-int/2addr v2, v0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_8

    :cond_8
    iget v1, p0, Lmaps/l/at;->E:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_9

    invoke-direct {p0}, Lmaps/l/at;->k()I

    move-result v1

    iput v1, p0, Lmaps/l/at;->E:I

    :cond_9
    add-int/2addr v0, v1

    return v0
.end method

.method public g()I
    .locals 9

    const/4 v1, 0x0

    iget-object v0, p0, Lmaps/l/at;->k:[Lmaps/l/m;

    if-eqz v0, :cond_0

    iget-object v4, p0, Lmaps/l/at;->k:[Lmaps/l/m;

    array-length v5, v4

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v5, :cond_1

    aget-object v3, v4, v2

    invoke-virtual {v3}, Lmaps/l/m;->a()I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_0

    :cond_0
    move v0, v1

    :cond_1
    iget-object v2, p0, Lmaps/l/at;->m:[Lmaps/l/q;

    if-eqz v2, :cond_2

    iget-object v4, p0, Lmaps/l/at;->m:[Lmaps/l/q;

    array-length v5, v4

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_2

    aget-object v3, v4, v2

    invoke-virtual {v3}, Lmaps/l/q;->a()I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lmaps/l/at;->n:[[Lmaps/l/ag;

    if-eqz v2, :cond_4

    iget-object v4, p0, Lmaps/l/at;->n:[[Lmaps/l/ag;

    array-length v5, v4

    move v3, v1

    :goto_2
    if-ge v3, v5, :cond_4

    aget-object v6, v4, v3

    array-length v7, v6

    move v2, v0

    move v0, v1

    :goto_3
    if-ge v0, v7, :cond_3

    aget-object v8, v6, v0

    invoke-virtual {v8}, Lmaps/l/ag;->a()I

    move-result v8

    add-int/2addr v2, v8

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v0, v2

    goto :goto_2

    :cond_4
    iget-object v2, p0, Lmaps/l/at;->p:[Lmaps/l/q;

    if-eqz v2, :cond_5

    iget-object v4, p0, Lmaps/l/at;->p:[Lmaps/l/q;

    array-length v5, v4

    move v2, v1

    :goto_4
    if-ge v2, v5, :cond_5

    aget-object v3, v4, v2

    invoke-virtual {v3}, Lmaps/l/q;->a()I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_4

    :cond_5
    iget-object v2, p0, Lmaps/l/at;->q:[Lmaps/l/c;

    if-eqz v2, :cond_6

    iget-object v4, p0, Lmaps/l/at;->q:[Lmaps/l/c;

    array-length v5, v4

    move v2, v1

    :goto_5
    if-ge v2, v5, :cond_6

    aget-object v3, v4, v2

    invoke-virtual {v3}, Lmaps/l/c;->a()I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_5

    :cond_6
    iget-object v2, p0, Lmaps/l/at;->r:[Lmaps/l/s;

    if-eqz v2, :cond_7

    iget-object v4, p0, Lmaps/l/at;->r:[Lmaps/l/s;

    array-length v5, v4

    move v2, v1

    :goto_6
    if-ge v2, v5, :cond_7

    aget-object v3, v4, v2

    invoke-virtual {v3}, Lmaps/l/s;->a()I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_6

    :cond_7
    iget-object v2, p0, Lmaps/l/at;->l:[Lmaps/l/ak;

    if-eqz v2, :cond_8

    iget-object v4, p0, Lmaps/l/at;->l:[Lmaps/l/ak;

    array-length v5, v4

    move v2, v1

    :goto_7
    if-ge v2, v5, :cond_8

    aget-object v3, v4, v2

    invoke-virtual {v3}, Lmaps/l/ak;->a()I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_7

    :cond_8
    iget-object v2, p0, Lmaps/l/at;->o:[Lmaps/l/ak;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lmaps/l/at;->o:[Lmaps/l/ak;

    array-length v3, v2

    :goto_8
    if-ge v1, v3, :cond_9

    aget-object v4, v2, v1

    invoke-virtual {v4}, Lmaps/l/ak;->a()I

    move-result v4

    add-int/2addr v0, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    :cond_9
    return v0
.end method

.method public h()Lmaps/o/c;
    .locals 1

    iget-object v0, p0, Lmaps/l/at;->A:Lmaps/o/c;

    return-object v0
.end method

.method public i()I
    .locals 1

    iget v0, p0, Lmaps/l/at;->B:I

    return v0
.end method

.method public j()Lmaps/l/u;
    .locals 1

    iget-object v0, p0, Lmaps/l/at;->s:Lmaps/l/u;

    return-object v0
.end method
