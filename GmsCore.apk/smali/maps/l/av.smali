.class public Lmaps/l/av;
.super Lmaps/l/ak;


# static fields
.field private static final a:[I

.field private static c:I

.field private static final n:Lmaps/t/aa;

.field private static final o:Lmaps/t/aa;

.field private static final p:Ljava/lang/ThreadLocal;


# instance fields
.field private final d:Lmaps/al/o;

.field private final e:Lmaps/al/o;

.field private final f:Lmaps/s/g;

.field private final g:Lmaps/s/g;

.field private h:Lmaps/q/ao;

.field private i:Lmaps/q/ao;

.field private j:I

.field private k:Lmaps/s/t;

.field private l:Lmaps/s/t;

.field private final m:Lmaps/l/ai;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    const/4 v11, 0x1

    const/4 v1, -0x1

    const v10, -0x45749f

    const/4 v5, 0x0

    const/4 v9, 0x0

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lmaps/l/av;->a:[I

    const/16 v0, 0x4000

    sput v0, Lmaps/l/av;->c:I

    new-instance v0, Lmaps/t/aa;

    const/4 v2, 0x2

    new-array v3, v9, [I

    new-array v4, v11, [Lmaps/t/ac;

    new-instance v6, Lmaps/t/ac;

    const/high16 v7, 0x40000000

    new-array v8, v9, [I

    invoke-direct {v6, v10, v7, v8, v9}, Lmaps/t/ac;-><init>(IF[II)V

    aput-object v6, v4, v9

    move-object v6, v5

    move-object v7, v5

    move-object v8, v5

    invoke-direct/range {v0 .. v8}, Lmaps/t/aa;-><init>(II[I[Lmaps/t/ac;Lmaps/t/cv;Lmaps/t/s;Lmaps/t/ac;Lmaps/t/c;)V

    sput-object v0, Lmaps/l/av;->n:Lmaps/t/aa;

    new-instance v0, Lmaps/t/aa;

    const/4 v2, 0x2

    new-array v3, v9, [I

    new-array v4, v11, [Lmaps/t/ac;

    new-instance v6, Lmaps/t/ac;

    const/high16 v7, 0x3fc00000

    new-array v8, v9, [I

    invoke-direct {v6, v10, v7, v8, v9}, Lmaps/t/ac;-><init>(IF[II)V

    aput-object v6, v4, v9

    move-object v6, v5

    move-object v7, v5

    move-object v8, v5

    invoke-direct/range {v0 .. v8}, Lmaps/t/aa;-><init>(II[I[Lmaps/t/ac;Lmaps/t/cv;Lmaps/t/s;Lmaps/t/ac;Lmaps/t/c;)V

    sput-object v0, Lmaps/l/av;->o:Lmaps/t/aa;

    new-instance v0, Lmaps/l/d;

    invoke-direct {v0}, Lmaps/l/d;-><init>()V

    sput-object v0, Lmaps/l/av;->p:Ljava/lang/ThreadLocal;

    return-void

    :array_0
    .array-data 4
        0x0
        0x2
        0x2
        0x4
        0x2
        0x4
        0x4
        0x6
    .end array-data
.end method

.method private constructor <init>(IILjava/util/Set;Lmaps/l/ai;Lmaps/cr/c;)V
    .locals 5

    const/4 v4, 0x4

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p3}, Lmaps/l/ak;-><init>(Ljava/util/Set;)V

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    iput-object v1, p0, Lmaps/l/av;->e:Lmaps/al/o;

    iput-object v1, p0, Lmaps/l/av;->d:Lmaps/al/o;

    iput-object v1, p0, Lmaps/l/av;->g:Lmaps/s/g;

    iput-object v1, p0, Lmaps/l/av;->f:Lmaps/s/g;

    new-instance v0, Lmaps/s/t;

    invoke-direct {v0, p1, v4, v3}, Lmaps/s/t;-><init>(IIZ)V

    iput-object v0, p0, Lmaps/l/av;->k:Lmaps/s/t;

    new-instance v0, Lmaps/s/t;

    invoke-direct {v0, p2, v4, v3}, Lmaps/s/t;-><init>(IIZ)V

    iput-object v0, p0, Lmaps/l/av;->l:Lmaps/s/t;

    :goto_0
    iput-object p4, p0, Lmaps/l/av;->m:Lmaps/l/ai;

    return-void

    :cond_0
    iput-object v1, p0, Lmaps/l/av;->i:Lmaps/q/ao;

    iput-object v1, p0, Lmaps/l/av;->h:Lmaps/q/ao;

    new-instance v0, Lmaps/al/e;

    invoke-direct {v0, p1, v2}, Lmaps/al/e;-><init>(IZ)V

    iput-object v0, p0, Lmaps/l/av;->e:Lmaps/al/o;

    new-instance v0, Lmaps/al/e;

    invoke-direct {v0, p2, v2}, Lmaps/al/e;-><init>(IZ)V

    iput-object v0, p0, Lmaps/l/av;->d:Lmaps/al/o;

    new-instance v0, Lmaps/s/g;

    invoke-virtual {p5}, Lmaps/cr/c;->K()Lmaps/s/l;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lmaps/s/g;-><init>(ILmaps/s/l;)V

    iput-object v0, p0, Lmaps/l/av;->g:Lmaps/s/g;

    new-instance v0, Lmaps/s/g;

    invoke-virtual {p5}, Lmaps/cr/c;->K()Lmaps/s/l;

    move-result-object v1

    invoke-direct {v0, p2, v1}, Lmaps/s/g;-><init>(ILmaps/s/l;)V

    iput-object v0, p0, Lmaps/l/av;->f:Lmaps/s/g;

    goto :goto_0
.end method

.method static a(Lmaps/t/co;)I
    .locals 1

    invoke-virtual {p0}, Lmaps/t/co;->h()Lmaps/t/aa;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/aa;->d()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lmaps/t/co;->c()Lmaps/t/ad;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/ad;->a()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    goto :goto_0
.end method

.method public static a(Lmaps/t/ah;[Ljava/lang/String;Lmaps/t/h;Lmaps/l/k;Lmaps/l/ad;ILmaps/cr/c;)Lmaps/l/av;
    .locals 13

    invoke-virtual {p0}, Lmaps/t/ah;->j()Lmaps/t/ax;

    move-result-object v7

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    const/4 v1, 0x0

    if-eqz p4, :cond_10

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v1

    move-object v6, v1

    :goto_0
    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_1
    invoke-interface {p2}, Lmaps/t/h;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p2}, Lmaps/t/h;->a()Lmaps/t/ca;

    move-result-object v5

    instance-of v1, v5, Lmaps/t/co;

    if-eqz v1, :cond_3

    move-object v1, v5

    check-cast v1, Lmaps/t/co;

    invoke-static {v1}, Lmaps/l/av;->a(Lmaps/t/co;)I

    move-result v9

    invoke-static {v1}, Lmaps/l/av;->b(Lmaps/t/co;)I

    move-result v10

    sget v11, Lmaps/l/av;->c:I

    if-gt v9, v11, :cond_0

    sget v11, Lmaps/l/av;->c:I

    if-le v10, v11, :cond_2

    :cond_0
    sget-boolean v1, Lmaps/ae/h;->j:Z

    if-eqz v1, :cond_1

    const-string v1, "GLAreaGroup"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "unable to handle the Area with fillVertexCount = "

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, " and strokeVertexCount = "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, " for "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, " since the limit is "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget v9, Lmaps/l/av;->c:I

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Lmaps/bt/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-interface {p2}, Lmaps/t/h;->next()Ljava/lang/Object;

    goto :goto_1

    :cond_2
    add-int v11, v2, v9

    sget v12, Lmaps/l/av;->c:I

    if-gt v11, v12, :cond_3

    add-int v11, v3, v10

    sget v12, Lmaps/l/av;->c:I

    if-le v11, v12, :cond_5

    :cond_3
    const/4 v5, 0x0

    if-eqz v6, :cond_e

    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_e

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_4
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_d

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/t/co;

    invoke-virtual {v1}, Lmaps/t/co;->l()Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_5
    add-int/2addr v2, v9

    add-int/2addr v3, v10

    invoke-interface {v5}, Lmaps/t/ca;->j()[I

    move-result-object v9

    array-length v10, v9

    const/4 v5, 0x0

    :goto_3
    if-ge v5, v10, :cond_7

    aget v11, v9, v5

    if-ltz v11, :cond_6

    array-length v12, p1

    if-ge v11, v12, :cond_6

    aget-object v11, p1, v11

    invoke-virtual {v4, v11}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_6
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    :cond_7
    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz p4, :cond_c

    invoke-virtual {v1}, Lmaps/t/co;->l()Z

    move-result v5

    if-eqz v5, :cond_c

    const/4 v5, 0x1

    :goto_4
    invoke-static {v1}, Lmaps/l/av;->c(Lmaps/t/co;)Z

    move-result v9

    if-nez v5, :cond_8

    if-nez v9, :cond_b

    :cond_8
    invoke-virtual {v1}, Lmaps/t/co;->d()Z

    move-result v10

    if-eqz v10, :cond_b

    invoke-virtual {v1}, Lmaps/t/co;->c()Lmaps/t/ad;

    move-result-object v10

    invoke-virtual {v1}, Lmaps/t/co;->e()[B

    move-result-object v11

    invoke-static {v10, v11}, Lmaps/s/m;->a(Lmaps/t/ad;[B)Ljava/util/List;

    move-result-object v10

    if-eqz p3, :cond_9

    if-nez v9, :cond_9

    const/4 v9, 0x0

    move-object/from16 v0, p3

    invoke-static {v0, v1, v10, v9}, Lmaps/l/av;->a(Lmaps/l/k;Lmaps/t/co;Ljava/util/List;Lmaps/t/ah;)V

    :cond_9
    if-eqz p3, :cond_a

    invoke-virtual {v1}, Lmaps/t/co;->l()Z

    move-result v9

    if-eqz v9, :cond_a

    move-object/from16 v0, p3

    invoke-static {v0, v1, v10, p0}, Lmaps/l/av;->a(Lmaps/l/k;Lmaps/t/co;Ljava/util/List;Lmaps/t/ah;)V

    :cond_a
    if-eqz v5, :cond_b

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_b
    invoke-interface {p2}, Lmaps/t/h;->next()Ljava/lang/Object;

    goto/16 :goto_1

    :cond_c
    const/4 v5, 0x0

    goto :goto_4

    :cond_d
    new-instance v5, Lmaps/l/ai;

    move-object/from16 v0, p4

    invoke-direct {v5, v7, v6, v9, v0}, Lmaps/l/ai;-><init>(Lmaps/t/ax;Ljava/util/List;Ljava/util/List;Lmaps/l/ad;)V

    :cond_e
    new-instance v1, Lmaps/l/av;

    move-object/from16 v6, p6

    invoke-direct/range {v1 .. v6}, Lmaps/l/av;-><init>(IILjava/util/Set;Lmaps/l/ai;Lmaps/cr/c;)V

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_f

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmaps/t/co;

    invoke-direct {v1, v7, v2}, Lmaps/l/av;->a(Lmaps/t/ax;Lmaps/t/co;)V

    goto :goto_5

    :cond_f
    move/from16 v0, p5

    invoke-direct {v1, p0, v0}, Lmaps/l/av;->a(Lmaps/t/ah;I)V

    return-object v1

    :cond_10
    move-object v6, v1

    goto/16 :goto_0
.end method

.method private static a(F)Lmaps/t/aa;
    .locals 1

    const/high16 v0, 0x41900000

    cmpl-float v0, p0, v0

    if-lez v0, :cond_0

    sget-object v0, Lmaps/l/av;->n:Lmaps/t/aa;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lmaps/l/av;->o:Lmaps/t/aa;

    goto :goto_0
.end method

.method public static a(Lmaps/cr/c;Lmaps/af/s;)V
    .locals 5

    const/high16 v4, 0x10000

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-interface {p1}, Lmaps/af/s;->c()Lmaps/p/av;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/p/av;->e()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lmaps/cr/c;->r()V

    :cond_1
    invoke-virtual {p0}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0, v4}, Ljavax/microedition/khronos/opengles/GL10;->glLineWidthx(I)V

    invoke-virtual {p0}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/4 v1, 0x1

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    invoke-virtual {p0}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x2100

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    invoke-virtual {p0}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0, v4, v4, v4, v4}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    goto :goto_0
.end method

.method private static a(Lmaps/l/k;Lmaps/t/co;Ljava/util/List;Lmaps/t/ah;)V
    .locals 15

    const/4 v2, -0x1

    if-eqz p3, :cond_1

    sget-object v1, Lmaps/t/cm;->d:Lmaps/t/cm;

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Lmaps/t/ah;->a(Lmaps/t/cm;)Lmaps/t/ao;

    move-result-object v1

    check-cast v1, Lmaps/t/cu;

    invoke-virtual {v1}, Lmaps/t/cu;->b()Lmaps/t/bg;

    move-result-object v3

    invoke-virtual/range {p3 .. p3}, Lmaps/t/ah;->c()I

    move-result v1

    int-to-float v1, v1

    invoke-static {v1}, Lmaps/l/av;->a(F)Lmaps/t/aa;

    move-result-object v6

    :goto_0
    invoke-virtual/range {p1 .. p1}, Lmaps/t/co;->c()Lmaps/t/ad;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/t/ad;->a()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v6}, Lmaps/t/aa;->c()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual/range {p1 .. p1}, Lmaps/t/co;->d()Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    return-void

    :cond_1
    invoke-virtual/range {p1 .. p1}, Lmaps/t/co;->b()Lmaps/t/v;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lmaps/t/co;->h()Lmaps/t/aa;

    move-result-object v6

    goto :goto_0

    :cond_2
    invoke-virtual/range {p1 .. p1}, Lmaps/t/co;->i()I

    move-result v9

    invoke-virtual/range {p1 .. p1}, Lmaps/t/co;->f()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lmaps/t/co;->g()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {p1 .. p1}, Lmaps/t/co;->j()[I

    move-result-object v12

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lmaps/t/cg;

    new-instance v1, Lmaps/t/az;

    const/4 v5, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    if-eqz v3, :cond_3

    const/4 v13, 0x1

    :goto_2
    invoke-direct/range {v1 .. v13}, Lmaps/t/az;-><init>(ILmaps/t/v;Lmaps/t/cg;[Lmaps/t/bh;Lmaps/t/aa;ILjava/lang/String;IFI[IZ)V

    invoke-virtual {p0, v1}, Lmaps/l/k;->a(Lmaps/t/ca;)V

    goto :goto_1

    :cond_3
    const/4 v13, 0x0

    goto :goto_2
.end method

.method private a(Lmaps/t/ah;I)V
    .locals 7

    const/4 v3, 0x0

    const/4 v4, 0x5

    const/4 v6, 0x0

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/l/av;->k:Lmaps/s/t;

    invoke-virtual {v0}, Lmaps/s/t;->f()Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    iput v0, p0, Lmaps/l/av;->j:I

    new-instance v0, Lmaps/q/ao;

    iget-object v2, p0, Lmaps/l/av;->k:Lmaps/s/t;

    invoke-virtual {v2}, Lmaps/s/t;->c()I

    move-result v2

    const/4 v5, 0x4

    invoke-direct/range {v0 .. v5}, Lmaps/q/ao;-><init>(Ljava/nio/ByteBuffer;I[SII)V

    iput-object v0, p0, Lmaps/l/av;->i:Lmaps/q/ao;

    new-instance v0, Lmaps/q/aa;

    invoke-direct {v0, p2, p1}, Lmaps/q/aa;-><init>(ILmaps/t/ah;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AreaFill "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/q/aa;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lmaps/l/av;->i:Lmaps/q/ao;

    invoke-virtual {v0, v1}, Lmaps/q/aa;->a(Lmaps/q/r;)V

    new-instance v1, Lmaps/q/bb;

    invoke-direct {v1}, Lmaps/q/bb;-><init>()V

    invoke-virtual {v0, v1}, Lmaps/q/aa;->a(Lmaps/q/z;)V

    iget-object v1, p0, Lmaps/l/av;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0, v6}, Lmaps/q/aa;->b(I)V

    iget-object v0, p0, Lmaps/l/av;->l:Lmaps/s/t;

    invoke-virtual {v0}, Lmaps/s/t;->i()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lmaps/l/av;->l:Lmaps/s/t;

    invoke-virtual {v0}, Lmaps/s/t;->f()Ljava/nio/ByteBuffer;

    move-result-object v1

    iget v0, p0, Lmaps/l/av;->j:I

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v2

    add-int/2addr v0, v2

    iput v0, p0, Lmaps/l/av;->j:I

    new-instance v0, Lmaps/q/ao;

    iget-object v2, p0, Lmaps/l/av;->l:Lmaps/s/t;

    invoke-virtual {v2}, Lmaps/s/t;->c()I

    move-result v2

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lmaps/q/ao;-><init>(Ljava/nio/ByteBuffer;I[SII)V

    iput-object v0, p0, Lmaps/l/av;->h:Lmaps/q/ao;

    new-instance v0, Lmaps/q/aa;

    invoke-direct {v0, p2, p1}, Lmaps/q/aa;-><init>(ILmaps/t/ah;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AreaStroke "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/q/aa;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lmaps/l/av;->h:Lmaps/q/ao;

    invoke-virtual {v0, v1}, Lmaps/q/aa;->a(Lmaps/q/r;)V

    new-instance v1, Lmaps/q/bb;

    invoke-direct {v1}, Lmaps/q/bb;-><init>()V

    invoke-virtual {v0, v1}, Lmaps/q/aa;->a(Lmaps/q/z;)V

    iget-object v1, p0, Lmaps/l/av;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0, v6}, Lmaps/q/aa;->b(I)V

    goto/16 :goto_0
.end method

.method private a(Lmaps/t/ax;Lmaps/t/co;)V
    .locals 17

    invoke-virtual/range {p2 .. p2}, Lmaps/t/co;->h()Lmaps/t/aa;

    move-result-object v4

    invoke-virtual/range {p2 .. p2}, Lmaps/t/co;->c()Lmaps/t/ad;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/t/ad;->a()I

    move-result v12

    if-nez v12, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v4}, Lmaps/t/aa;->d()I

    move-result v2

    if-lez v2, :cond_7

    const/4 v2, 0x1

    move v7, v2

    :goto_1
    invoke-static/range {p2 .. p2}, Lmaps/l/av;->c(Lmaps/t/co;)Z

    move-result v13

    if-nez v7, :cond_2

    if-eqz v13, :cond_0

    :cond_2
    invoke-virtual/range {p1 .. p1}, Lmaps/t/ax;->d()Lmaps/t/bx;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lmaps/t/ax;->g()I

    move-result v14

    if-eqz v7, :cond_8

    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Lmaps/t/aa;->a(I)I

    move-result v2

    move v11, v2

    :goto_2
    if-eqz v13, :cond_9

    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Lmaps/t/aa;->b(I)Lmaps/t/ac;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/t/ac;->b()I

    move-result v2

    move v8, v2

    :goto_3
    invoke-virtual/range {p2 .. p2}, Lmaps/t/co;->e()[B

    move-result-object v15

    const/4 v10, 0x0

    const/4 v9, 0x0

    sget-object v2, Lmaps/l/av;->p:Ljava/lang/ThreadLocal;

    invoke-virtual {v2}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lmaps/t/bx;

    const/4 v4, 0x0

    aget-object v4, v2, v4

    sget-object v2, Lmaps/l/av;->p:Ljava/lang/ThreadLocal;

    invoke-virtual {v2}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lmaps/t/bx;

    const/4 v5, 0x1

    aget-object v5, v2, v5

    sget-object v2, Lmaps/l/av;->p:Ljava/lang/ThreadLocal;

    invoke-virtual {v2}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lmaps/t/bx;

    const/4 v6, 0x2

    aget-object v6, v2, v6

    const/4 v2, 0x0

    :goto_4
    if-ge v2, v12, :cond_e

    invoke-virtual/range {v1 .. v6}, Lmaps/t/ad;->a(ILmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    if-eqz v7, :cond_3

    sget-boolean v16, Lmaps/ae/h;->F:Z

    if-eqz v16, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/l/av;->k:Lmaps/s/t;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v14}, Lmaps/s/t;->a(Lmaps/t/bx;I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/l/av;->k:Lmaps/s/t;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v14}, Lmaps/s/t;->a(Lmaps/t/bx;I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/l/av;->k:Lmaps/s/t;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v6, v14}, Lmaps/s/t;->a(Lmaps/t/bx;I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/l/av;->k:Lmaps/s/t;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Lmaps/s/t;->e(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/l/av;->k:Lmaps/s/t;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Lmaps/s/t;->e(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/l/av;->k:Lmaps/s/t;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Lmaps/s/t;->e(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/l/av;->k:Lmaps/s/t;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lmaps/s/t;->d()V

    :goto_5
    add-int/lit8 v10, v10, 0x3

    :cond_3
    if-eqz v13, :cond_6

    aget-byte v16, v15, v2

    and-int/lit8 v16, v16, 0x1

    if-eqz v16, :cond_4

    sget-boolean v16, Lmaps/ae/h;->F:Z

    if-eqz v16, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/l/av;->l:Lmaps/s/t;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v14}, Lmaps/s/t;->a(Lmaps/t/bx;I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/l/av;->l:Lmaps/s/t;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v14}, Lmaps/s/t;->a(Lmaps/t/bx;I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/l/av;->l:Lmaps/s/t;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Lmaps/s/t;->e(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/l/av;->l:Lmaps/s/t;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Lmaps/s/t;->e(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/l/av;->l:Lmaps/s/t;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lmaps/s/t;->d()V

    :goto_6
    add-int/lit8 v9, v9, 0x2

    :cond_4
    aget-byte v16, v15, v2

    and-int/lit8 v16, v16, 0x2

    if-eqz v16, :cond_5

    sget-boolean v16, Lmaps/ae/h;->F:Z

    if-eqz v16, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/l/av;->l:Lmaps/s/t;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v14}, Lmaps/s/t;->a(Lmaps/t/bx;I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/l/av;->l:Lmaps/s/t;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v6, v14}, Lmaps/s/t;->a(Lmaps/t/bx;I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/l/av;->l:Lmaps/s/t;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Lmaps/s/t;->e(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/l/av;->l:Lmaps/s/t;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Lmaps/s/t;->e(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/l/av;->l:Lmaps/s/t;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lmaps/s/t;->d()V

    :goto_7
    add-int/lit8 v9, v9, 0x2

    :cond_5
    aget-byte v16, v15, v2

    and-int/lit8 v16, v16, 0x4

    if-eqz v16, :cond_6

    sget-boolean v16, Lmaps/ae/h;->F:Z

    if-eqz v16, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/l/av;->l:Lmaps/s/t;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v6, v14}, Lmaps/s/t;->a(Lmaps/t/bx;I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/l/av;->l:Lmaps/s/t;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v14}, Lmaps/s/t;->a(Lmaps/t/bx;I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/l/av;->l:Lmaps/s/t;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Lmaps/s/t;->e(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/l/av;->l:Lmaps/s/t;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Lmaps/s/t;->e(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/l/av;->l:Lmaps/s/t;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lmaps/s/t;->d()V

    :goto_8
    add-int/lit8 v9, v9, 0x2

    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_4

    :cond_7
    const/4 v2, 0x0

    move v7, v2

    goto/16 :goto_1

    :cond_8
    const/4 v2, 0x0

    move v11, v2

    goto/16 :goto_2

    :cond_9
    const/4 v2, 0x0

    move v8, v2

    goto/16 :goto_3

    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/l/av;->e:Lmaps/al/o;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v14}, Lmaps/al/o;->a(Lmaps/t/bx;I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/l/av;->e:Lmaps/al/o;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v14}, Lmaps/al/o;->a(Lmaps/t/bx;I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/l/av;->e:Lmaps/al/o;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v6, v14}, Lmaps/al/o;->a(Lmaps/t/bx;I)V

    goto/16 :goto_5

    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/l/av;->d:Lmaps/al/o;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v14}, Lmaps/al/o;->a(Lmaps/t/bx;I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/l/av;->d:Lmaps/al/o;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v14}, Lmaps/al/o;->a(Lmaps/t/bx;I)V

    goto/16 :goto_6

    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/l/av;->d:Lmaps/al/o;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v14}, Lmaps/al/o;->a(Lmaps/t/bx;I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/l/av;->d:Lmaps/al/o;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v6, v14}, Lmaps/al/o;->a(Lmaps/t/bx;I)V

    goto/16 :goto_7

    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/l/av;->d:Lmaps/al/o;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v6, v14}, Lmaps/al/o;->a(Lmaps/t/bx;I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/l/av;->d:Lmaps/al/o;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v14}, Lmaps/al/o;->a(Lmaps/t/bx;I)V

    goto :goto_8

    :cond_e
    sget-boolean v1, Lmaps/ae/h;->F:Z

    if-nez v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/l/av;->g:Lmaps/s/g;

    invoke-virtual {v1, v11, v10}, Lmaps/s/g;->a(II)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/l/av;->f:Lmaps/s/g;

    invoke-virtual {v1, v8, v9}, Lmaps/s/g;->a(II)V

    goto/16 :goto_0
.end method

.method static b(Lmaps/t/co;)I
    .locals 5

    const/4 v0, 0x0

    invoke-static {p0}, Lmaps/l/av;->c(Lmaps/t/co;)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lmaps/t/co;->e()[B

    move-result-object v2

    move v1, v0

    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_1

    aget-byte v3, v2, v0

    and-int/lit8 v3, v3, 0x7

    sget-object v4, Lmaps/l/av;->a:[I

    aget v3, v4, v3

    add-int/2addr v1, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method static synthetic c()Ljava/lang/ThreadLocal;
    .locals 1

    sget-object v0, Lmaps/l/av;->p:Ljava/lang/ThreadLocal;

    return-object v0
.end method

.method private static c(Lmaps/t/co;)Z
    .locals 5

    const/4 v1, 0x0

    invoke-virtual {p0}, Lmaps/t/co;->h()Lmaps/t/aa;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/t/aa;->c()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmaps/t/co;->d()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    move v0, v1

    :goto_1
    invoke-virtual {v2}, Lmaps/t/aa;->c()I

    move-result v3

    if-ge v0, v3, :cond_2

    invoke-virtual {v2, v0}, Lmaps/t/aa;->b(I)Lmaps/t/ac;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/t/ac;->c()F

    move-result v3

    const/high16 v4, 0x3f800000

    cmpl-float v3, v3, v4

    if-gtz v3, :cond_0

    invoke-virtual {v2, v0}, Lmaps/t/aa;->b(I)Lmaps/t/ac;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/t/ac;->e()Z

    move-result v3

    if-nez v3, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 3

    const/4 v0, 0x0

    sget-boolean v1, Lmaps/ae/h;->F:Z

    if-eqz v1, :cond_1

    iget v1, p0, Lmaps/l/av;->j:I

    iget-object v2, p0, Lmaps/l/av;->m:Lmaps/l/ai;

    if-nez v2, :cond_0

    :goto_0
    add-int/2addr v0, v1

    :goto_1
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/l/av;->m:Lmaps/l/ai;

    invoke-virtual {v0}, Lmaps/l/ai;->a()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lmaps/l/av;->d:Lmaps/al/o;

    invoke-virtual {v1}, Lmaps/al/o;->d()I

    move-result v1

    iget-object v2, p0, Lmaps/l/av;->e:Lmaps/al/o;

    invoke-virtual {v2}, Lmaps/al/o;->d()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lmaps/l/av;->f:Lmaps/s/g;

    invoke-virtual {v2}, Lmaps/s/g;->a()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lmaps/l/av;->g:Lmaps/s/g;

    invoke-virtual {v2}, Lmaps/s/g;->a()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lmaps/l/av;->m:Lmaps/l/ai;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lmaps/l/av;->m:Lmaps/l/ai;

    invoke-virtual {v0}, Lmaps/l/ai;->a()I

    move-result v0

    goto :goto_2
.end method

.method public a(Lmaps/cr/c;)V
    .locals 1

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0, p1}, Lmaps/l/av;->c(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/av;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :goto_0
    iget-object v0, p0, Lmaps/l/av;->m:Lmaps/l/ai;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/l/av;->m:Lmaps/l/ai;

    invoke-virtual {v0, p1}, Lmaps/l/ai;->b(Lmaps/cr/c;)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/l/av;->e:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->e(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/av;->d:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->e(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/av;->g:Lmaps/s/g;

    invoke-virtual {v0, p1}, Lmaps/s/g;->c(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/av;->f:Lmaps/s/g;

    invoke-virtual {v0, p1}, Lmaps/s/g;->c(Lmaps/cr/c;)V

    goto :goto_0
.end method

.method public a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 6

    const/4 v5, 0x4

    const/4 v4, 0x1

    const/4 v3, 0x0

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {p3}, Lmaps/af/s;->c()Lmaps/p/av;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/p/av;->c()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-static {p1}, Lmaps/s/l;->c(Lmaps/cr/c;)V

    iget-object v1, p0, Lmaps/l/av;->e:Lmaps/al/o;

    invoke-virtual {v1}, Lmaps/al/o;->c()I

    move-result v1

    if-lez v1, :cond_2

    iget-object v1, p0, Lmaps/l/av;->e:Lmaps/al/o;

    invoke-virtual {v1, p1}, Lmaps/al/o;->d(Lmaps/cr/c;)V

    iget-object v1, p0, Lmaps/l/av;->g:Lmaps/s/g;

    invoke-virtual {v1, p1}, Lmaps/s/g;->a(Lmaps/cr/c;)V

    iget-object v1, p0, Lmaps/l/av;->e:Lmaps/al/o;

    invoke-virtual {v1}, Lmaps/al/o;->c()I

    move-result v1

    invoke-interface {v0, v5, v3, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    :cond_2
    iget-object v1, p0, Lmaps/l/av;->d:Lmaps/al/o;

    invoke-virtual {v1}, Lmaps/al/o;->c()I

    move-result v1

    if-lez v1, :cond_3

    iget-object v1, p0, Lmaps/l/av;->d:Lmaps/al/o;

    invoke-virtual {v1, p1}, Lmaps/al/o;->d(Lmaps/cr/c;)V

    iget-object v1, p0, Lmaps/l/av;->f:Lmaps/s/g;

    invoke-virtual {v1, p1}, Lmaps/s/g;->a(Lmaps/cr/c;)V

    iget-object v1, p0, Lmaps/l/av;->d:Lmaps/al/o;

    invoke-virtual {v1}, Lmaps/al/o;->c()I

    move-result v1

    invoke-interface {v0, v4, v3, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    :cond_3
    invoke-static {p1}, Lmaps/s/l;->d(Lmaps/cr/c;)V

    goto :goto_0

    :cond_4
    invoke-virtual {v1}, Lmaps/p/av;->e()Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v1, p0, Lmaps/l/av;->e:Lmaps/al/o;

    invoke-virtual {v1}, Lmaps/al/o;->c()I

    move-result v1

    if-lez v1, :cond_5

    iget-object v1, p0, Lmaps/l/av;->e:Lmaps/al/o;

    invoke-virtual {v1, p1}, Lmaps/al/o;->d(Lmaps/cr/c;)V

    iget-object v1, p0, Lmaps/l/av;->e:Lmaps/al/o;

    invoke-virtual {v1}, Lmaps/al/o;->c()I

    move-result v1

    invoke-interface {v0, v5, v3, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    :cond_5
    iget-object v1, p0, Lmaps/l/av;->d:Lmaps/al/o;

    invoke-virtual {v1}, Lmaps/al/o;->c()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lmaps/l/av;->d:Lmaps/al/o;

    invoke-virtual {v1, p1}, Lmaps/al/o;->d(Lmaps/cr/c;)V

    iget-object v1, p0, Lmaps/l/av;->d:Lmaps/al/o;

    invoke-virtual {v1}, Lmaps/al/o;->c()I

    move-result v1

    invoke-interface {v0, v4, v3, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    goto :goto_0

    :cond_6
    invoke-virtual {v1}, Lmaps/p/av;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/l/av;->m:Lmaps/l/ai;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/l/av;->m:Lmaps/l/ai;

    invoke-virtual {v0, p1, p2, p3}, Lmaps/l/ai;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    goto/16 :goto_0
.end method

.method public b()I
    .locals 3

    const/4 v0, 0x0

    sget-boolean v1, Lmaps/ae/h;->F:Z

    if-eqz v1, :cond_1

    iget v1, p0, Lmaps/l/av;->j:I

    add-int/lit16 v1, v1, 0x9c

    iget-object v2, p0, Lmaps/l/av;->m:Lmaps/l/ai;

    if-nez v2, :cond_0

    :goto_0
    add-int/2addr v0, v1

    :goto_1
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/l/av;->m:Lmaps/l/ai;

    invoke-virtual {v0}, Lmaps/l/ai;->b()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lmaps/l/av;->d:Lmaps/al/o;

    invoke-virtual {v1}, Lmaps/al/o;->a()I

    move-result v1

    add-int/lit16 v1, v1, 0x9c

    iget-object v2, p0, Lmaps/l/av;->e:Lmaps/al/o;

    invoke-virtual {v2}, Lmaps/al/o;->a()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lmaps/l/av;->f:Lmaps/s/g;

    invoke-virtual {v2}, Lmaps/s/g;->b()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lmaps/l/av;->g:Lmaps/s/g;

    invoke-virtual {v2}, Lmaps/s/g;->b()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lmaps/l/av;->m:Lmaps/l/ai;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lmaps/l/av;->m:Lmaps/l/ai;

    invoke-virtual {v0}, Lmaps/l/ai;->b()I

    move-result v0

    goto :goto_2
.end method

.method public c(Lmaps/cr/c;)V
    .locals 3

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lmaps/cr/c;->m()Lmaps/q/ad;

    move-result-object v1

    iget-object v0, p0, Lmaps/l/av;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/q/aa;

    invoke-virtual {v1, v0}, Lmaps/q/ad;->b(Lmaps/q/ba;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmaps/l/av;->e:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->b(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/av;->d:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->b(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/av;->g:Lmaps/s/g;

    invoke-virtual {v0, p1}, Lmaps/s/g;->b(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/av;->f:Lmaps/s/g;

    invoke-virtual {v0, p1}, Lmaps/s/g;->b(Lmaps/cr/c;)V

    :cond_1
    iget-object v0, p0, Lmaps/l/av;->m:Lmaps/l/ai;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/l/av;->m:Lmaps/l/ai;

    invoke-virtual {v0, p1}, Lmaps/l/ai;->a(Lmaps/cr/c;)V

    :cond_2
    return-void
.end method
