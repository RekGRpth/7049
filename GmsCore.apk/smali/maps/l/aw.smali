.class public Lmaps/l/aw;
.super Lmaps/l/u;


# static fields
.field private static final M:[Lmaps/l/h;

.field private static final N:[Lmaps/l/h;


# instance fields
.field private A:F

.field private B:Z

.field private final C:[Lmaps/l/h;

.field private D:I

.field private E:Lmaps/w/v;

.field private F:Z

.field private G:F

.field private final H:F

.field private final I:[F

.field private J:Ljava/lang/String;

.field private final K:Z

.field private final L:Lmaps/cm/f;

.field protected l:Lmaps/t/y;

.field protected m:Lmaps/l/g;

.field protected n:Lmaps/l/g;

.field protected o:Lmaps/l/h;

.field protected p:F

.field protected q:F

.field protected r:F

.field protected s:F

.field private t:Lmaps/t/y;

.field private u:Lmaps/t/ak;

.field private v:Lmaps/t/ak;

.field private w:Lmaps/al/o;

.field private final x:Ljava/lang/String;

.field private final y:Z

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x0

    new-array v0, v4, [Lmaps/l/h;

    sput-object v0, Lmaps/l/aw;->M:[Lmaps/l/h;

    const/4 v0, 0x4

    new-array v0, v0, [Lmaps/l/h;

    new-instance v1, Lmaps/l/h;

    sget-object v2, Lmaps/l/af;->d:Lmaps/l/af;

    sget-object v3, Lmaps/l/w;->a:Lmaps/l/w;

    invoke-direct {v1, v2, v3}, Lmaps/l/h;-><init>(Lmaps/l/af;Lmaps/l/w;)V

    aput-object v1, v0, v4

    const/4 v1, 0x1

    new-instance v2, Lmaps/l/h;

    sget-object v3, Lmaps/l/af;->b:Lmaps/l/af;

    sget-object v4, Lmaps/l/w;->a:Lmaps/l/w;

    invoke-direct {v2, v3, v4}, Lmaps/l/h;-><init>(Lmaps/l/af;Lmaps/l/w;)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Lmaps/l/h;

    sget-object v3, Lmaps/l/af;->e:Lmaps/l/af;

    sget-object v4, Lmaps/l/w;->c:Lmaps/l/w;

    invoke-direct {v2, v3, v4}, Lmaps/l/h;-><init>(Lmaps/l/af;Lmaps/l/w;)V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Lmaps/l/h;

    sget-object v3, Lmaps/l/af;->c:Lmaps/l/af;

    sget-object v4, Lmaps/l/w;->b:Lmaps/l/w;

    invoke-direct {v2, v3, v4}, Lmaps/l/h;-><init>(Lmaps/l/af;Lmaps/l/w;)V

    aput-object v2, v0, v1

    sput-object v0, Lmaps/l/aw;->N:[Lmaps/l/h;

    return-void
.end method

.method constructor <init>(Lmaps/t/ca;Lmaps/ac/g;Ljava/lang/String;Lmaps/t/y;Lmaps/t/y;FFZZLmaps/l/g;Lmaps/l/g;[Lmaps/l/h;ZZLmaps/cm/f;)V
    .locals 10

    invoke-interface {p1}, Lmaps/t/ca;->h()Lmaps/t/aa;

    move-result-object v4

    invoke-interface {p1}, Lmaps/t/ca;->i()I

    move-result v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move/from16 v5, p6

    move/from16 v6, p7

    move/from16 v8, p8

    move/from16 v9, p13

    invoke-direct/range {v1 .. v9}, Lmaps/l/u;-><init>(Lmaps/t/ca;Lmaps/ac/g;Lmaps/t/aa;FFIZZ)V

    const/high16 v1, -0x40800000

    iput v1, p0, Lmaps/l/aw;->A:F

    const/16 v1, 0x8

    new-array v1, v1, [F

    iput-object v1, p0, Lmaps/l/aw;->I:[F

    iput-object p3, p0, Lmaps/l/aw;->x:Ljava/lang/String;

    iput-object p4, p0, Lmaps/l/aw;->l:Lmaps/t/y;

    iput-object p5, p0, Lmaps/l/aw;->t:Lmaps/t/y;

    move-object/from16 v0, p10

    iput-object v0, p0, Lmaps/l/aw;->m:Lmaps/l/g;

    move-object/from16 v0, p11

    iput-object v0, p0, Lmaps/l/aw;->n:Lmaps/l/g;

    move/from16 v0, p9

    iput-boolean v0, p0, Lmaps/l/aw;->y:Z

    iget-object v1, p0, Lmaps/l/aw;->n:Lmaps/l/g;

    if-nez v1, :cond_0

    sget-object p12, Lmaps/l/aw;->M:[Lmaps/l/h;

    :cond_0
    move-object/from16 v0, p12

    iput-object v0, p0, Lmaps/l/aw;->C:[Lmaps/l/h;

    move/from16 v0, p14

    iput-boolean v0, p0, Lmaps/l/aw;->K:Z

    move-object/from16 v0, p15

    iput-object v0, p0, Lmaps/l/aw;->L:Lmaps/cm/f;

    const/4 v1, 0x0

    iput v1, p0, Lmaps/l/aw;->D:I

    iget-object v1, p0, Lmaps/l/aw;->n:Lmaps/l/g;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lmaps/l/aw;->C:[Lmaps/l/h;

    iget v2, p0, Lmaps/l/aw;->D:I

    aget-object v1, v1, v2

    iput-object v1, p0, Lmaps/l/aw;->o:Lmaps/l/h;

    iget-object v1, p0, Lmaps/l/aw;->n:Lmaps/l/g;

    iget-object v2, p0, Lmaps/l/aw;->o:Lmaps/l/h;

    iget-object v2, v2, Lmaps/l/h;->b:Lmaps/l/w;

    invoke-virtual {v1, v2}, Lmaps/l/g;->a(Lmaps/l/w;)V

    :cond_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lmaps/l/aw;->B:Z

    const/4 v1, 0x0

    if-eqz p10, :cond_2

    invoke-virtual/range {p10 .. p10}, Lmaps/l/g;->a()F

    move-result v2

    invoke-virtual/range {p10 .. p10}, Lmaps/l/g;->b()F

    move-result v3

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    :cond_2
    if-eqz p11, :cond_3

    invoke-virtual/range {p11 .. p11}, Lmaps/l/g;->a()F

    move-result v2

    invoke-virtual/range {p11 .. p11}, Lmaps/l/g;->b()F

    move-result v3

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    :cond_3
    iput v1, p0, Lmaps/l/aw;->H:F

    return-void
.end method

.method private a(Lmaps/bq/d;)F
    .locals 3

    const/high16 v2, 0x3f800000

    const/4 v0, 0x1

    iget-boolean v1, p0, Lmaps/l/aw;->y:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/l/aw;->l:Lmaps/t/y;

    invoke-virtual {v1}, Lmaps/t/y;->b()Lmaps/t/bx;

    move-result-object v1

    invoke-virtual {p1, v1, v0}, Lmaps/bq/d;->a(Lmaps/t/bx;Z)F

    move-result v0

    invoke-virtual {p1, v2, v0}, Lmaps/bq/d;->a(FF)F

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lmaps/bq/d;->p()F

    move-result v0

    invoke-virtual {p1, v2, v0}, Lmaps/bq/d;->a(FF)F

    move-result v0

    goto :goto_0
.end method

.method static a(Lmaps/t/aa;Lmaps/cf/a;F)I
    .locals 3

    iget v0, p1, Lmaps/cf/a;->e:F

    iget v1, p1, Lmaps/cf/a;->f:I

    iget v2, p1, Lmaps/cf/a;->g:I

    invoke-static {p0, v0, v1, v2, p2}, Lmaps/l/u;->a(Lmaps/t/aa;FIIF)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method private static a(Lmaps/t/n;Lmaps/t/bx;)Ljava/lang/String;
    .locals 5

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move v0, v1

    :goto_0
    invoke-virtual {p0}, Lmaps/t/n;->n()Lmaps/t/bh;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/t/bh;->b()I

    move-result v3

    if-ge v0, v3, :cond_1

    invoke-virtual {p0}, Lmaps/t/n;->n()Lmaps/t/bh;

    move-result-object v3

    invoke-virtual {v3, v0}, Lmaps/t/bh;->a(I)Lmaps/t/a;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/t/a;->b()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Lmaps/t/a;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    invoke-virtual {p0}, Lmaps/t/n;->o()Lmaps/t/bh;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/bh;->b()I

    move-result v0

    if-ge v1, v0, :cond_3

    invoke-virtual {p0}, Lmaps/t/n;->o()Lmaps/t/bh;

    move-result-object v0

    invoke-virtual {v0, v1}, Lmaps/t/bh;->a(I)Lmaps/t/a;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/a;->b()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Lmaps/t/a;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lmaps/t/n;->b()Lmaps/t/v;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lmaps/t/n;->b()Lmaps/t/v;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/v;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    invoke-virtual {p0}, Lmaps/t/n;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_6

    invoke-virtual {p0}, Lmaps/t/n;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    :goto_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_6
    invoke-virtual {p0}, Lmaps/t/n;->c()Lmaps/t/ah;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/ah;->c()I

    move-result v0

    const/16 v1, 0xd

    if-le v0, v1, :cond_5

    invoke-virtual {p1}, Lmaps/t/bx;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2
.end method

.method public static a(Lmaps/l/al;Lmaps/p/y;Lmaps/bq/d;)Lmaps/l/aw;
    .locals 22

    invoke-interface/range {p0 .. p0}, Lmaps/l/al;->h()Lmaps/o/c;

    move-result-object v7

    invoke-virtual {v7}, Lmaps/o/c;->h()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-interface/range {p0 .. p0}, Lmaps/l/al;->b()Lmaps/t/ah;

    move-result-object v8

    invoke-virtual {v8}, Lmaps/t/ah;->j()Lmaps/t/ax;

    move-result-object v1

    invoke-virtual {v7, v1}, Lmaps/o/c;->a(Lmaps/t/ax;)Lmaps/t/bx;

    move-result-object v9

    invoke-virtual {v7}, Lmaps/o/c;->i()Lmaps/l/h;

    move-result-object v10

    invoke-virtual {v7}, Lmaps/o/c;->j()Lmaps/t/aa;

    move-result-object v5

    const/4 v1, 0x1

    new-array v14, v1, [Lmaps/l/h;

    const/4 v1, 0x0

    aput-object v10, v14, v1

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v15, Lmaps/cf/a;->t:Lmaps/cf/a;

    new-instance v13, Lmaps/l/ap;

    const/high16 v1, 0x40800000

    invoke-direct {v13, v1}, Lmaps/l/ap;-><init>(F)V

    invoke-virtual/range {p2 .. p2}, Lmaps/bq/d;->n()F

    move-result v1

    invoke-static {v5, v15, v1}, Lmaps/l/aw;->a(Lmaps/t/aa;Lmaps/cf/a;F)I

    move-result v4

    new-instance v1, Lmaps/l/aj;

    invoke-virtual {v8}, Lmaps/t/ah;->d()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v6, v15, Lmaps/cf/a;->d:Lmaps/p/l;

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v6}, Lmaps/l/aj;-><init>(Lmaps/p/y;Ljava/lang/String;ILmaps/t/aa;Lmaps/p/l;)V

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lmaps/l/aj;

    invoke-virtual {v8}, Lmaps/t/ah;->e()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v6, v15, Lmaps/cf/a;->d:Lmaps/p/l;

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v6}, Lmaps/l/aj;-><init>(Lmaps/p/y;Ljava/lang/String;ILmaps/t/aa;Lmaps/p/l;)V

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lmaps/l/aj;

    invoke-virtual {v8}, Lmaps/t/ah;->c()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v6, v15, Lmaps/cf/a;->d:Lmaps/p/l;

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v6}, Lmaps/l/aj;-><init>(Lmaps/p/y;Ljava/lang/String;ILmaps/t/aa;Lmaps/p/l;)V

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v11, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lmaps/l/aj;

    invoke-virtual {v7}, Lmaps/o/c;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v6, v15, Lmaps/cf/a;->d:Lmaps/p/l;

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v6}, Lmaps/l/aj;-><init>(Lmaps/p/y;Ljava/lang/String;ILmaps/t/aa;Lmaps/p/l;)V

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface/range {p0 .. p0}, Lmaps/l/al;->i()I

    move-result v1

    if-lez v1, :cond_1

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v11, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lmaps/l/aj;

    invoke-interface/range {p0 .. p0}, Lmaps/l/al;->i()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v6, v15, Lmaps/cf/a;->d:Lmaps/p/l;

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v6}, Lmaps/l/aj;-><init>(Lmaps/p/y;Ljava/lang/String;ILmaps/t/aa;Lmaps/p/l;)V

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    new-instance v13, Lmaps/l/g;

    iget-object v1, v10, Lmaps/l/h;->b:Lmaps/l/w;

    sget-object v2, Lmaps/l/j;->c:Lmaps/l/j;

    invoke-direct {v13, v11, v1, v2}, Lmaps/l/g;-><init>(Ljava/util/ArrayList;Lmaps/l/w;Lmaps/l/j;)V

    const/4 v3, 0x1

    const/4 v4, 0x0

    new-instance v18, Lmaps/l/aw;

    new-instance v19, Lmaps/t/r;

    const/4 v1, -0x1

    const/4 v2, 0x0

    move-object/from16 v0, v19

    invoke-direct {v0, v1, v2}, Lmaps/t/r;-><init>(ILmaps/t/aa;)V

    const/16 v20, 0x0

    const/16 v21, 0x0

    new-instance v1, Lmaps/t/y;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v2, v9

    invoke-direct/range {v1 .. v8}, Lmaps/t/y;-><init>(Lmaps/t/bx;IFLmaps/t/bx;FFF)V

    const/4 v7, 0x0

    const/high16 v8, -0x40800000

    const/high16 v9, -0x40800000

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    iget-boolean v15, v15, Lmaps/cf/a;->q:Z

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object/from16 v2, v18

    move-object/from16 v3, v19

    move-object/from16 v4, v20

    move-object/from16 v5, v21

    move-object v6, v1

    invoke-direct/range {v2 .. v17}, Lmaps/l/aw;-><init>(Lmaps/t/ca;Lmaps/ac/g;Ljava/lang/String;Lmaps/t/y;Lmaps/t/y;FFZZLmaps/l/g;Lmaps/l/g;[Lmaps/l/h;ZZLmaps/cm/f;)V

    const/4 v1, 0x1

    move-object/from16 v0, v18

    invoke-direct {v0, v1}, Lmaps/l/aw;->b(Z)V

    move-object/from16 v1, v18

    goto/16 :goto_0
.end method

.method public static a(Lmaps/t/bu;ILmaps/ac/g;Lmaps/t/bx;Lmaps/t/bx;ZLmaps/cf/a;Lmaps/bq/d;Lmaps/cr/b;)Lmaps/l/aw;
    .locals 19

    invoke-virtual/range {p0 .. p1}, Lmaps/t/bu;->a(I)Lmaps/t/bh;

    move-result-object v1

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v1}, Lmaps/t/bh;->b()I

    move-result v3

    if-ge v2, v3, :cond_2

    invoke-virtual {v1, v2}, Lmaps/t/bh;->a(I)Lmaps/t/a;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/t/a;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v1, 0x0

    :goto_1
    return-object v1

    :cond_0
    invoke-virtual {v3}, Lmaps/t/a;->b()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3}, Lmaps/t/a;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-nez v2, :cond_3

    const/4 v1, 0x0

    goto :goto_1

    :cond_3
    const/4 v5, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p7

    move-object/from16 v4, p8

    move-object/from16 v6, p6

    invoke-static/range {v1 .. v6}, Lmaps/l/g;->a(Lmaps/t/bh;Lmaps/t/ca;Lmaps/bq/d;Lmaps/cr/b;Lmaps/p/y;Lmaps/cf/a;)Lmaps/l/g;

    move-result-object v12

    if-nez v12, :cond_4

    const/4 v1, 0x0

    goto :goto_1

    :cond_4
    new-instance v18, Lmaps/l/aw;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    new-instance v1, Lmaps/t/y;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v2, p3

    invoke-direct/range {v1 .. v8}, Lmaps/t/y;-><init>(Lmaps/t/bx;IFLmaps/t/bx;FFF)V

    if-nez p4, :cond_5

    const/4 v7, 0x0

    :goto_2
    const/high16 v8, -0x40800000

    const/high16 v9, -0x40800000

    const/4 v11, 0x0

    const/4 v13, 0x0

    sget-object v14, Lmaps/l/aw;->M:[Lmaps/l/h;

    move-object/from16 v0, p6

    iget-boolean v15, v0, Lmaps/cf/a;->q:Z

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object/from16 v2, v18

    move-object/from16 v3, p0

    move-object/from16 v4, p2

    move-object v5, v10

    move-object v6, v1

    move/from16 v10, p5

    invoke-direct/range {v2 .. v17}, Lmaps/l/aw;-><init>(Lmaps/t/ca;Lmaps/ac/g;Ljava/lang/String;Lmaps/t/y;Lmaps/t/y;FFZZLmaps/l/g;Lmaps/l/g;[Lmaps/l/h;ZZLmaps/cm/f;)V

    move-object/from16 v1, v18

    goto :goto_1

    :cond_5
    new-instance v2, Lmaps/t/y;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v3, p4

    invoke-direct/range {v2 .. v9}, Lmaps/t/y;-><init>(Lmaps/t/bx;IFLmaps/t/bx;FFF)V

    move-object v7, v2

    goto :goto_2
.end method

.method public static a(Lmaps/t/n;Lmaps/ac/g;ZLmaps/bq/d;Lmaps/cr/b;Lmaps/p/y;Lmaps/cf/a;Lmaps/cm/f;)Lmaps/l/aw;
    .locals 17

    invoke-virtual/range {p0 .. p0}, Lmaps/t/n;->n()Lmaps/t/bh;

    move-result-object v1

    move-object/from16 v2, p0

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    invoke-static/range {v1 .. v6}, Lmaps/l/g;->a(Lmaps/t/bh;Lmaps/t/ca;Lmaps/bq/d;Lmaps/cr/b;Lmaps/p/y;Lmaps/cf/a;)Lmaps/l/g;

    move-result-object v11

    if-eqz v11, :cond_0

    invoke-virtual {v11}, Lmaps/l/g;->c()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return-object v1

    :cond_2
    const/4 v12, 0x0

    invoke-virtual/range {p0 .. p0}, Lmaps/t/n;->o()Lmaps/t/bh;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual/range {p0 .. p0}, Lmaps/t/n;->o()Lmaps/t/bh;

    move-result-object v1

    move-object/from16 v2, p0

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    invoke-static/range {v1 .. v6}, Lmaps/l/g;->a(Lmaps/t/bh;Lmaps/t/ca;Lmaps/bq/d;Lmaps/cr/b;Lmaps/p/y;Lmaps/cf/a;)Lmaps/l/g;

    move-result-object v12

    if-nez v12, :cond_3

    const/4 v1, 0x0

    goto :goto_0

    :cond_3
    invoke-virtual {v12}, Lmaps/l/g;->c()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v12, 0x0

    :cond_4
    invoke-virtual/range {p0 .. p0}, Lmaps/t/n;->p()[Lmaps/t/g;

    move-result-object v1

    array-length v1, v1

    if-nez v1, :cond_8

    if-eqz v12, :cond_8

    sget-object v13, Lmaps/l/aw;->N:[Lmaps/l/h;

    :cond_5
    if-eqz v12, :cond_6

    array-length v1, v13

    const/4 v2, 0x1

    if-ne v1, v2, :cond_9

    :cond_6
    const/4 v10, 0x1

    :goto_1
    invoke-virtual/range {p0 .. p0}, Lmaps/t/n;->m()[Lmaps/t/y;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v5, v1, v2

    invoke-virtual {v5}, Lmaps/t/y;->b()Lmaps/t/bx;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-static {v0, v1}, Lmaps/l/aw;->a(Lmaps/t/n;Lmaps/t/bx;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lmaps/t/n;->q()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-virtual/range {p0 .. p0}, Lmaps/t/n;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_a

    move-object/from16 v0, p6

    iget-boolean v1, v0, Lmaps/cf/a;->p:Z

    if-eqz v1, :cond_a

    const/4 v15, 0x1

    :goto_2
    sget-boolean v1, Lmaps/ae/h;->F:Z

    if-nez v1, :cond_b

    if-eqz v15, :cond_b

    new-instance v1, Lmaps/l/b;

    const/4 v6, 0x0

    invoke-virtual/range {p0 .. p0}, Lmaps/t/n;->g()F

    move-result v7

    invoke-virtual/range {p0 .. p0}, Lmaps/t/n;->l()F

    move-result v8

    move-object/from16 v0, p6

    iget-boolean v14, v0, Lmaps/cf/a;->q:Z

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move/from16 v9, p2

    invoke-direct/range {v1 .. v14}, Lmaps/l/b;-><init>(Lmaps/t/n;Lmaps/ac/g;Ljava/lang/String;Lmaps/t/y;Lmaps/t/y;FFZZLmaps/l/g;Lmaps/l/g;[Lmaps/l/h;Z)V

    :goto_3
    invoke-static {}, Lmaps/ae/h;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual/range {p0 .. p0}, Lmaps/t/n;->n()Lmaps/t/bh;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/t/bh;->a()Ljava/lang/String;

    move-result-object v3

    if-eqz v12, :cond_d

    invoke-virtual/range {p0 .. p0}, Lmaps/t/n;->o()Lmaps/t/bh;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/t/bh;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_c

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_c

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_7
    :goto_4
    invoke-direct {v1, v2}, Lmaps/l/aw;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    invoke-virtual/range {p0 .. p0}, Lmaps/t/n;->p()[Lmaps/t/g;

    move-result-object v1

    array-length v1, v1

    new-array v13, v1, [Lmaps/l/h;

    const/4 v1, 0x0

    :goto_5
    array-length v2, v13

    if-ge v1, v2, :cond_5

    new-instance v2, Lmaps/l/h;

    invoke-virtual/range {p0 .. p0}, Lmaps/t/n;->p()[Lmaps/t/g;

    move-result-object v3

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lmaps/t/g;->a()I

    move-result v3

    invoke-static {v3}, Lmaps/l/af;->a(I)Lmaps/l/af;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lmaps/t/n;->p()[Lmaps/t/g;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Lmaps/t/g;->b()I

    move-result v4

    invoke-static {v4}, Lmaps/l/w;->a(I)Lmaps/l/w;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lmaps/l/h;-><init>(Lmaps/l/af;Lmaps/l/w;)V

    aput-object v2, v13, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_9
    const/4 v10, 0x0

    goto/16 :goto_1

    :cond_a
    const/4 v15, 0x0

    goto/16 :goto_2

    :cond_b
    new-instance v1, Lmaps/l/aw;

    const/4 v6, 0x0

    invoke-virtual/range {p0 .. p0}, Lmaps/t/n;->g()F

    move-result v7

    invoke-virtual/range {p0 .. p0}, Lmaps/t/n;->l()F

    move-result v8

    move-object/from16 v0, p6

    iget-boolean v14, v0, Lmaps/cf/a;->q:Z

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move/from16 v9, p2

    move-object/from16 v16, p7

    invoke-direct/range {v1 .. v16}, Lmaps/l/aw;-><init>(Lmaps/t/ca;Lmaps/ac/g;Ljava/lang/String;Lmaps/t/y;Lmaps/t/y;FFZZLmaps/l/g;Lmaps/l/g;[Lmaps/l/h;ZZLmaps/cm/f;)V

    goto/16 :goto_3

    :cond_c
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_7

    :cond_d
    move-object v2, v3

    goto :goto_4
.end method

.method private a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lmaps/l/aw;->J:Ljava/lang/String;

    return-void
.end method

.method private b(Lmaps/bq/d;)Lmaps/t/ak;
    .locals 8

    const/high16 v4, 0x42b40000

    const/high16 v3, 0x43870000

    iget-object v0, p0, Lmaps/l/aw;->l:Lmaps/t/y;

    invoke-virtual {v0}, Lmaps/t/y;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lmaps/l/aw;->l:Lmaps/t/y;

    invoke-virtual {v0}, Lmaps/t/y;->c()F

    move-result v0

    cmpg-float v1, v0, v3

    if-gez v1, :cond_1

    add-float/2addr v0, v4

    move v2, v0

    :goto_1
    cmpg-float v0, v2, v3

    if-gez v0, :cond_2

    add-float v0, v2, v4

    move v1, v0

    :goto_2
    iget-object v0, p0, Lmaps/l/aw;->l:Lmaps/t/y;

    invoke-virtual {v0}, Lmaps/t/y;->b()Lmaps/t/bx;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {p1, v0, v3}, Lmaps/bq/d;->a(Lmaps/t/bx;Z)F

    move-result v3

    sget-object v0, Lmaps/af/v;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/s/p;

    iget-object v4, v0, Lmaps/s/p;->a:Lmaps/t/bx;

    iget v5, p0, Lmaps/l/aw;->p:F

    invoke-virtual {p1, v5, v3}, Lmaps/bq/d;->a(FF)F

    move-result v5

    invoke-virtual {v4, v2, v5}, Lmaps/t/bx;->a(FF)V

    iget-object v5, v0, Lmaps/s/p;->b:Lmaps/t/bx;

    iget v6, p0, Lmaps/l/aw;->q:F

    invoke-virtual {p1, v6, v3}, Lmaps/bq/d;->a(FF)F

    move-result v6

    invoke-virtual {v5, v2, v6}, Lmaps/t/bx;->a(FF)V

    iget-object v2, v0, Lmaps/s/p;->c:Lmaps/t/bx;

    iget v6, p0, Lmaps/l/aw;->r:F

    invoke-virtual {p1, v6, v3}, Lmaps/bq/d;->a(FF)F

    move-result v6

    invoke-virtual {v2, v1, v6}, Lmaps/t/bx;->a(FF)V

    iget-object v6, v0, Lmaps/s/p;->d:Lmaps/t/bx;

    iget v7, p0, Lmaps/l/aw;->s:F

    invoke-virtual {p1, v7, v3}, Lmaps/bq/d;->a(FF)F

    move-result v3

    invoke-virtual {v6, v1, v3}, Lmaps/t/bx;->a(FF)V

    iget-object v1, v0, Lmaps/s/p;->e:Lmaps/t/bx;

    iget-object v3, p0, Lmaps/l/aw;->l:Lmaps/t/y;

    invoke-virtual {v3}, Lmaps/t/y;->b()Lmaps/t/bx;

    move-result-object v3

    invoke-static {v3, v2, v1}, Lmaps/t/bx;->a(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    iget-object v0, v0, Lmaps/s/p;->f:Lmaps/t/bx;

    iget-object v2, p0, Lmaps/l/aw;->l:Lmaps/t/y;

    invoke-virtual {v2}, Lmaps/t/y;->b()Lmaps/t/bx;

    move-result-object v2

    invoke-static {v2, v6, v0}, Lmaps/t/bx;->a(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    invoke-virtual {v1, v4}, Lmaps/t/bx;->e(Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v2

    invoke-virtual {v1, v5}, Lmaps/t/bx;->e(Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v1

    invoke-virtual {v0, v4}, Lmaps/t/bx;->e(Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v3

    invoke-virtual {v0, v5}, Lmaps/t/bx;->e(Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v0

    invoke-static {v3, v0, v2, v1}, Lmaps/t/ak;->a(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)Lmaps/t/ak;

    move-result-object v0

    goto :goto_0

    :cond_1
    sub-float/2addr v0, v3

    move v2, v0

    goto :goto_1

    :cond_2
    sub-float v0, v2, v3

    move v1, v0

    goto :goto_2
.end method

.method private b(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 9

    const/4 v7, 0x0

    const/high16 v5, 0x3f800000

    const/high16 v4, 0x43b40000

    const/high16 v8, 0x40000000

    const/4 v1, 0x0

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v6

    iget-object v0, p0, Lmaps/l/aw;->l:Lmaps/t/y;

    invoke-virtual {v0}, Lmaps/t/y;->b()Lmaps/t/bx;

    move-result-object v0

    invoke-virtual {p2}, Lmaps/bq/d;->k()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lmaps/l/aw;->l:Lmaps/t/y;

    invoke-virtual {v2}, Lmaps/t/y;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lmaps/l/aw;->l:Lmaps/t/y;

    invoke-virtual {v2}, Lmaps/t/y;->c()F

    move-result v2

    invoke-virtual {p2}, Lmaps/bq/d;->q()F

    move-result v3

    sub-float/2addr v2, v3

    cmpl-float v2, v2, v1

    if-nez v2, :cond_1

    :cond_0
    invoke-virtual {p2}, Lmaps/bq/d;->r()F

    move-result v2

    cmpl-float v2, v2, v1

    if-nez v2, :cond_1

    iget-object v0, p0, Lmaps/l/aw;->l:Lmaps/t/y;

    invoke-virtual {v0}, Lmaps/t/y;->b()Lmaps/t/bx;

    move-result-object v0

    iget-object v2, p0, Lmaps/l/aw;->I:[F

    invoke-virtual {p2, v0, v2}, Lmaps/bq/d;->a(Lmaps/t/bx;[F)V

    iget-object v0, p0, Lmaps/l/aw;->I:[F

    aget v0, v0, v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    iget-object v2, p0, Lmaps/l/aw;->I:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2, v0, v2}, Lmaps/bq/d;->d(FF)Lmaps/t/bx;

    move-result-object v0

    :cond_1
    iget v2, p0, Lmaps/l/aw;->A:F

    invoke-static {p1, p2, v0, v2}, Lmaps/af/d;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/t/bx;F)V

    sget-boolean v0, Lmaps/ae/h;->P:Z

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lmaps/cr/c;->s()V

    iget-object v0, p0, Lmaps/l/aw;->w:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->d(Lmaps/cr/c;)V

    const/high16 v0, 0x3f000000

    invoke-interface {v6, v0, v1, v1, v5}, Ljavax/microedition/khronos/opengles/GL10;->glColor4f(FFFF)V

    const/4 v0, 0x2

    iget-object v2, p0, Lmaps/l/aw;->w:Lmaps/al/o;

    invoke-virtual {v2}, Lmaps/al/o;->c()I

    move-result v2

    invoke-interface {v6, v0, v7, v2}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    invoke-virtual {p1}, Lmaps/cr/c;->r()V

    :cond_2
    iget-object v0, p0, Lmaps/l/aw;->E:Lmaps/w/v;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmaps/l/aw;->E:Lmaps/w/v;

    invoke-virtual {v0, p1}, Lmaps/w/v;->a(Lmaps/cr/c;)I

    move-result v0

    const/high16 v2, 0x10000

    if-ne v0, v2, :cond_3

    const/4 v2, 0x0

    iput-object v2, p0, Lmaps/l/aw;->E:Lmaps/w/v;

    :cond_3
    :goto_0
    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    invoke-interface {v2, v0, v0, v0, v0}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    iget-object v0, p0, Lmaps/l/aw;->l:Lmaps/t/y;

    invoke-virtual {v0}, Lmaps/t/y;->a()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lmaps/l/aw;->l:Lmaps/t/y;

    invoke-virtual {v0}, Lmaps/t/y;->c()F

    move-result v0

    invoke-virtual {p2}, Lmaps/bq/d;->q()F

    move-result v2

    sub-float/2addr v0, v2

    cmpg-float v2, v0, v1

    if-gez v2, :cond_4

    add-float/2addr v0, v4

    :cond_4
    iget-object v2, p0, Lmaps/l/aw;->l:Lmaps/t/y;

    invoke-virtual {v2}, Lmaps/t/y;->c()F

    move-result v2

    iget-boolean v3, p0, Lmaps/l/aw;->z:Z

    if-nez v3, :cond_c

    const/high16 v3, 0x42b40000

    cmpl-float v3, v0, v3

    if-lez v3, :cond_c

    const/high16 v3, 0x43870000

    cmpg-float v0, v0, v3

    if-gez v0, :cond_c

    const/high16 v0, 0x43340000

    add-float/2addr v0, v2

    :goto_1
    cmpl-float v2, v0, v4

    if-ltz v2, :cond_5

    sub-float/2addr v0, v4

    :cond_5
    const/high16 v2, -0x40800000

    invoke-interface {v6, v0, v1, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    const/high16 v0, -0x3d4c0000

    invoke-interface {v6, v0, v5, v1, v1}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    :goto_2
    iget-object v0, p0, Lmaps/l/aw;->m:Lmaps/l/g;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lmaps/l/aw;->m:Lmaps/l/g;

    invoke-virtual {v0}, Lmaps/l/g;->a()F

    move-result v4

    iget-object v0, p0, Lmaps/l/aw;->m:Lmaps/l/g;

    invoke-virtual {v0}, Lmaps/l/g;->b()F

    move-result v3

    iget-object v0, p0, Lmaps/l/aw;->m:Lmaps/l/g;

    invoke-virtual {v0}, Lmaps/l/g;->a()F

    move-result v0

    div-float v2, v0, v8

    iget-object v0, p0, Lmaps/l/aw;->m:Lmaps/l/g;

    invoke-virtual {v0}, Lmaps/l/g;->b()F

    move-result v0

    div-float/2addr v0, v8

    neg-float v5, v2

    neg-float v7, v0

    invoke-interface {v6, v5, v1, v7}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    iget-object v5, p0, Lmaps/l/aw;->m:Lmaps/l/g;

    invoke-virtual {v5, p1, p2, p3}, Lmaps/l/g;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    :goto_3
    iget-object v5, p0, Lmaps/l/aw;->n:Lmaps/l/g;

    if-eqz v5, :cond_8

    sget-object v5, Lmaps/l/e;->a:[I

    iget-object v7, p0, Lmaps/l/aw;->o:Lmaps/l/h;

    iget-object v7, v7, Lmaps/l/h;->a:Lmaps/l/af;

    invoke-virtual {v7}, Lmaps/l/af;->ordinal()I

    move-result v7

    aget v5, v5, v7

    packed-switch v5, :pswitch_data_0

    move v3, v1

    move v0, v1

    :goto_4
    iget-object v5, p0, Lmaps/l/aw;->o:Lmaps/l/h;

    iget-object v5, v5, Lmaps/l/h;->a:Lmaps/l/af;

    sget-object v7, Lmaps/l/af;->d:Lmaps/l/af;

    if-eq v5, v7, :cond_6

    iget-object v5, p0, Lmaps/l/aw;->o:Lmaps/l/h;

    iget-object v5, v5, Lmaps/l/h;->a:Lmaps/l/af;

    sget-object v7, Lmaps/l/af;->b:Lmaps/l/af;

    if-ne v5, v7, :cond_7

    :cond_6
    sget-object v5, Lmaps/l/e;->b:[I

    iget-object v7, p0, Lmaps/l/aw;->o:Lmaps/l/h;

    iget-object v7, v7, Lmaps/l/h;->b:Lmaps/l/w;

    invoke-virtual {v7}, Lmaps/l/w;->ordinal()I

    move-result v7

    aget v5, v5, v7

    packed-switch v5, :pswitch_data_1

    :cond_7
    :goto_5
    invoke-interface {v6, v0, v1, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    iget-object v0, p0, Lmaps/l/aw;->n:Lmaps/l/g;

    invoke-virtual {v0, p1, p2, p3}, Lmaps/l/g;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    :cond_8
    return-void

    :cond_9
    iget v0, p0, Lmaps/l/aw;->k:I

    goto/16 :goto_0

    :cond_a
    invoke-static {v6, p2}, Lmaps/af/d;->a(Ljavax/microedition/khronos/opengles/GL10;Lmaps/bq/d;)V

    goto :goto_2

    :pswitch_0
    iget-object v3, p0, Lmaps/l/aw;->n:Lmaps/l/g;

    invoke-virtual {v3}, Lmaps/l/g;->a()F

    move-result v3

    div-float/2addr v3, v8

    sub-float v5, v2, v3

    iget-object v3, p0, Lmaps/l/aw;->n:Lmaps/l/g;

    invoke-virtual {v3}, Lmaps/l/g;->b()F

    move-result v3

    div-float/2addr v3, v8

    sub-float v3, v0, v3

    move v0, v5

    goto :goto_4

    :pswitch_1
    iget-object v3, p0, Lmaps/l/aw;->n:Lmaps/l/g;

    invoke-virtual {v3}, Lmaps/l/g;->b()F

    move-result v3

    div-float/2addr v3, v8

    sub-float v3, v0, v3

    move v0, v4

    goto :goto_4

    :pswitch_2
    iget-object v3, p0, Lmaps/l/aw;->n:Lmaps/l/g;

    invoke-virtual {v3}, Lmaps/l/g;->a()F

    move-result v3

    neg-float v5, v3

    iget-object v3, p0, Lmaps/l/aw;->n:Lmaps/l/g;

    invoke-virtual {v3}, Lmaps/l/g;->b()F

    move-result v3

    div-float/2addr v3, v8

    sub-float v3, v0, v3

    move v0, v5

    goto :goto_4

    :pswitch_3
    iget-object v0, p0, Lmaps/l/aw;->n:Lmaps/l/g;

    invoke-virtual {v0}, Lmaps/l/g;->b()F

    move-result v0

    neg-float v3, v0

    move v0, v1

    goto :goto_4

    :pswitch_4
    move v0, v1

    goto :goto_4

    :pswitch_5
    iget-object v0, p0, Lmaps/l/aw;->n:Lmaps/l/g;

    invoke-virtual {v0}, Lmaps/l/g;->b()F

    move-result v0

    neg-float v3, v0

    move v0, v4

    goto :goto_4

    :pswitch_6
    iget-object v0, p0, Lmaps/l/aw;->n:Lmaps/l/g;

    invoke-virtual {v0}, Lmaps/l/g;->a()F

    move-result v0

    neg-float v0, v0

    iget-object v3, p0, Lmaps/l/aw;->n:Lmaps/l/g;

    invoke-virtual {v3}, Lmaps/l/g;->b()F

    move-result v3

    neg-float v3, v3

    goto/16 :goto_4

    :pswitch_7
    move v0, v4

    goto/16 :goto_4

    :pswitch_8
    iget-object v0, p0, Lmaps/l/aw;->n:Lmaps/l/g;

    invoke-virtual {v0}, Lmaps/l/g;->a()F

    move-result v0

    neg-float v0, v0

    goto/16 :goto_4

    :pswitch_9
    const/high16 v0, -0x3ee00000

    goto :goto_5

    :pswitch_a
    const/high16 v0, 0x41200000

    add-float/2addr v0, v4

    iget-object v2, p0, Lmaps/l/aw;->n:Lmaps/l/g;

    invoke-virtual {v2}, Lmaps/l/g;->a()F

    move-result v2

    sub-float/2addr v0, v2

    goto :goto_5

    :pswitch_b
    iget-object v0, p0, Lmaps/l/aw;->n:Lmaps/l/g;

    invoke-virtual {v0}, Lmaps/l/g;->a()F

    move-result v0

    div-float/2addr v0, v8

    sub-float v0, v2, v0

    goto/16 :goto_5

    :cond_b
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto/16 :goto_3

    :cond_c
    move v0, v2

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method private b(Z)V
    .locals 0

    iput-boolean p1, p0, Lmaps/l/aw;->z:Z

    return-void
.end method

.method private d(Lmaps/cr/c;)V
    .locals 4

    iget-object v0, p0, Lmaps/l/aw;->w:Lmaps/al/o;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/al/o;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lmaps/al/o;-><init>(I)V

    iput-object v0, p0, Lmaps/l/aw;->w:Lmaps/al/o;

    :goto_0
    iget-object v0, p0, Lmaps/l/aw;->u:Lmaps/t/ak;

    if-eqz v0, :cond_1

    new-instance v1, Lmaps/t/bx;

    invoke-direct {v1}, Lmaps/t/bx;-><init>()V

    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lmaps/l/aw;->u:Lmaps/t/ak;

    invoke-virtual {v2}, Lmaps/t/ak;->b()I

    move-result v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lmaps/l/aw;->u:Lmaps/t/ak;

    invoke-virtual {v2, v0}, Lmaps/t/ak;->a(I)Lmaps/t/bx;

    move-result-object v2

    iget-object v3, p0, Lmaps/l/aw;->l:Lmaps/t/y;

    invoke-virtual {v3}, Lmaps/t/y;->b()Lmaps/t/bx;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lmaps/t/cb;->d(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    iget-object v2, p0, Lmaps/l/aw;->w:Lmaps/al/o;

    iget v3, p0, Lmaps/l/aw;->A:F

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-virtual {v2, v1, v3}, Lmaps/al/o;->a(Lmaps/t/bx;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lmaps/l/aw;->w:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->a(Lmaps/cr/c;)V

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public a(Lmaps/cr/c;)V
    .locals 1

    invoke-super {p0, p1}, Lmaps/l/u;->a(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/aw;->m:Lmaps/l/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/l/aw;->m:Lmaps/l/g;

    invoke-virtual {v0, p1}, Lmaps/l/g;->a(Lmaps/cr/c;)V

    :cond_0
    iget-object v0, p0, Lmaps/l/aw;->n:Lmaps/l/g;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/l/aw;->n:Lmaps/l/g;

    invoke-virtual {v0, p1}, Lmaps/l/g;->a(Lmaps/cr/c;)V

    :cond_1
    iget-object v0, p0, Lmaps/l/aw;->w:Lmaps/al/o;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/l/aw;->w:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->e(Lmaps/cr/c;)V

    :cond_2
    return-void
.end method

.method public a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 2

    iget-boolean v0, p0, Lmaps/l/aw;->B:Z

    if-nez v0, :cond_0

    invoke-interface {p3}, Lmaps/af/s;->a()Lmaps/af/q;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lmaps/l/aw;->a(Lmaps/cr/c;Lmaps/af/q;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-boolean v0, Lmaps/ae/h;->P:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/l/aw;->u:Lmaps/t/ak;

    iget-object v1, p0, Lmaps/l/aw;->v:Lmaps/t/ak;

    if-eq v0, v1, :cond_1

    invoke-direct {p0, p1}, Lmaps/l/aw;->d(Lmaps/cr/c;)V

    :cond_1
    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    invoke-direct {p0, p1, p2, p3}, Lmaps/l/aw;->b(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto :goto_0
.end method

.method public a(Lmaps/bq/d;Lmaps/cr/c;)Z
    .locals 10

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lmaps/l/aw;->k()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lmaps/l/aw;->i:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lmaps/l/aw;->A:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_2

    invoke-direct {p0, p1}, Lmaps/l/aw;->a(Lmaps/bq/d;)F

    move-result v0

    iget v3, p0, Lmaps/l/aw;->A:F

    div-float/2addr v0, v3

    invoke-static {v0}, Lmaps/l/aw;->a(F)I

    move-result v3

    iput v3, p0, Lmaps/l/aw;->k:I

    const/high16 v3, 0x3e800000

    cmpl-float v3, v0, v3

    if-ltz v3, :cond_1

    const/high16 v3, 0x40000000

    cmpg-float v0, v0, v3

    if-gtz v0, :cond_1

    move v0, v1

    :goto_0
    move v1, v0

    :cond_0
    :goto_1
    return v1

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    const/high16 v0, 0x10000

    iput v0, p0, Lmaps/l/aw;->k:I

    invoke-virtual {p1}, Lmaps/bq/d;->r()F

    move-result v0

    cmpl-float v0, v0, v3

    if-nez v0, :cond_4

    invoke-virtual {p1}, Lmaps/bq/d;->q()F

    move-result v0

    cmpl-float v0, v0, v3

    if-nez v0, :cond_4

    move v0, v1

    :goto_2
    invoke-virtual {p1}, Lmaps/bq/d;->s()F

    move-result v3

    iget-object v4, p0, Lmaps/l/aw;->u:Lmaps/t/ak;

    if-eqz v4, :cond_3

    iget-boolean v4, p0, Lmaps/l/aw;->F:Z

    if-eqz v4, :cond_3

    if-eqz v0, :cond_3

    iget v4, p0, Lmaps/l/aw;->G:F

    cmpl-float v4, v3, v4

    if-eqz v4, :cond_0

    :cond_3
    iget-object v4, p0, Lmaps/l/aw;->u:Lmaps/t/ak;

    if-eqz v4, :cond_5

    iget-object v4, p0, Lmaps/l/aw;->l:Lmaps/t/y;

    invoke-virtual {v4}, Lmaps/t/y;->a()Z

    move-result v4

    if-eqz v4, :cond_5

    iget v4, p0, Lmaps/l/aw;->G:F

    cmpl-float v4, v3, v4

    if-nez v4, :cond_5

    iget-object v0, p0, Lmaps/l/aw;->u:Lmaps/t/ak;

    iput-object v0, p0, Lmaps/l/aw;->v:Lmaps/t/ak;

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_2

    :cond_5
    iput-boolean v0, p0, Lmaps/l/aw;->F:Z

    iput v3, p0, Lmaps/l/aw;->G:F

    iget-object v0, p0, Lmaps/l/aw;->m:Lmaps/l/g;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lmaps/l/aw;->m:Lmaps/l/g;

    invoke-virtual {v0}, Lmaps/l/g;->a()F

    move-result v0

    float-to-int v0, v0

    shr-int/lit8 v3, v0, 0x1

    iget-object v0, p0, Lmaps/l/aw;->m:Lmaps/l/g;

    invoke-virtual {v0}, Lmaps/l/g;->b()F

    move-result v0

    float-to-int v0, v0

    shr-int/lit8 v0, v0, 0x1

    :goto_3
    iget-object v4, p0, Lmaps/l/aw;->n:Lmaps/l/g;

    if-nez v4, :cond_9

    neg-int v4, v3

    int-to-float v4, v4

    iput v4, p0, Lmaps/l/aw;->p:F

    int-to-float v3, v3

    iput v3, p0, Lmaps/l/aw;->q:F

    neg-int v3, v0

    int-to-float v3, v3

    iput v3, p0, Lmaps/l/aw;->r:F

    int-to-float v0, v0

    iput v0, p0, Lmaps/l/aw;->s:F

    :cond_6
    :goto_4
    sget-object v0, Lmaps/af/v;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/s/p;

    iget-object v0, v0, Lmaps/s/p;->a:Lmaps/t/bx;

    invoke-virtual {p1}, Lmaps/bq/d;->u()Lmaps/t/bx;

    move-result-object v3

    iget-object v4, p0, Lmaps/l/aw;->l:Lmaps/t/y;

    invoke-virtual {v4}, Lmaps/t/y;->b()Lmaps/t/bx;

    move-result-object v4

    invoke-static {v3, v4, v0}, Lmaps/t/bx;->b(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    invoke-direct {p0, p1}, Lmaps/l/aw;->a(Lmaps/bq/d;)F

    move-result v0

    iput v0, p0, Lmaps/l/aw;->A:F

    iget-boolean v0, p0, Lmaps/l/aw;->y:Z

    if-nez v0, :cond_7

    invoke-virtual {p1}, Lmaps/bq/d;->p()F

    move-result v0

    iget-object v3, p0, Lmaps/l/aw;->l:Lmaps/t/y;

    invoke-virtual {v3}, Lmaps/t/y;->b()Lmaps/t/bx;

    move-result-object v3

    invoke-virtual {p1, v3, v1}, Lmaps/bq/d;->a(Lmaps/t/bx;Z)F

    move-result v3

    div-float/2addr v0, v3

    iget v3, p0, Lmaps/l/aw;->p:F

    mul-float/2addr v3, v0

    iput v3, p0, Lmaps/l/aw;->p:F

    iget v3, p0, Lmaps/l/aw;->q:F

    mul-float/2addr v3, v0

    iput v3, p0, Lmaps/l/aw;->q:F

    iget v3, p0, Lmaps/l/aw;->r:F

    mul-float/2addr v3, v0

    iput v3, p0, Lmaps/l/aw;->r:F

    iget v3, p0, Lmaps/l/aw;->s:F

    mul-float/2addr v0, v3

    iput v0, p0, Lmaps/l/aw;->s:F

    :cond_7
    iget-object v0, p0, Lmaps/l/aw;->c:Lmaps/ac/g;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lmaps/l/aw;->c:Lmaps/ac/g;

    instance-of v0, v0, Lmaps/ac/b;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lmaps/l/aw;->c:Lmaps/ac/g;

    check-cast v0, Lmaps/ac/b;

    invoke-virtual {v0}, Lmaps/ac/b;->a()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_8

    instance-of v3, v0, Lmaps/t/bg;

    if-eqz v3, :cond_8

    invoke-static {}, Lmaps/b/r;->a()Lmaps/b/r;

    move-result-object v3

    check-cast v0, Lmaps/t/bg;

    invoke-virtual {v3, v0}, Lmaps/b/r;->e(Lmaps/t/bg;)Lmaps/b/z;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v3, p0, Lmaps/l/aw;->l:Lmaps/t/y;

    invoke-virtual {v3}, Lmaps/t/y;->b()Lmaps/t/bx;

    move-result-object v3

    iget-object v4, p0, Lmaps/l/aw;->l:Lmaps/t/y;

    invoke-virtual {v4}, Lmaps/t/y;->b()Lmaps/t/bx;

    move-result-object v4

    invoke-virtual {v0, p1, v4}, Lmaps/b/z;->a(Lmaps/bq/d;Lmaps/t/bx;)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {v3, v0}, Lmaps/t/bx;->b(I)V

    :cond_8
    iget-object v0, p0, Lmaps/l/aw;->u:Lmaps/t/ak;

    iput-object v0, p0, Lmaps/l/aw;->v:Lmaps/t/ak;

    iget-object v0, p0, Lmaps/l/aw;->l:Lmaps/t/y;

    invoke-virtual {v0}, Lmaps/t/y;->a()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-direct {p0, p1}, Lmaps/l/aw;->b(Lmaps/bq/d;)Lmaps/t/ak;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/aw;->u:Lmaps/t/ak;

    :goto_5
    iget-object v0, p0, Lmaps/l/aw;->u:Lmaps/t/ak;

    if-nez v0, :cond_0

    move v1, v2

    goto/16 :goto_1

    :cond_9
    iget-object v4, p0, Lmaps/l/aw;->n:Lmaps/l/g;

    invoke-virtual {v4}, Lmaps/l/g;->a()F

    move-result v4

    float-to-int v4, v4

    shr-int/lit8 v7, v4, 0x1

    iget-object v4, p0, Lmaps/l/aw;->n:Lmaps/l/g;

    invoke-virtual {v4}, Lmaps/l/g;->b()F

    move-result v4

    float-to-int v4, v4

    shr-int/lit8 v5, v4, 0x1

    if-le v3, v7, :cond_b

    move v6, v3

    :goto_6
    if-le v0, v5, :cond_c

    move v4, v0

    :goto_7
    sget-object v8, Lmaps/l/e;->a:[I

    iget-object v9, p0, Lmaps/l/aw;->o:Lmaps/l/h;

    iget-object v9, v9, Lmaps/l/h;->a:Lmaps/l/af;

    invoke-virtual {v9}, Lmaps/l/af;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    neg-int v0, v6

    int-to-float v0, v0

    iput v0, p0, Lmaps/l/aw;->p:F

    int-to-float v0, v6

    iput v0, p0, Lmaps/l/aw;->q:F

    neg-int v0, v4

    int-to-float v0, v0

    iput v0, p0, Lmaps/l/aw;->r:F

    int-to-float v0, v4

    iput v0, p0, Lmaps/l/aw;->s:F

    :goto_8
    iget-object v0, p0, Lmaps/l/aw;->o:Lmaps/l/h;

    iget-object v0, v0, Lmaps/l/h;->a:Lmaps/l/af;

    sget-object v4, Lmaps/l/af;->d:Lmaps/l/af;

    if-eq v0, v4, :cond_a

    iget-object v0, p0, Lmaps/l/aw;->o:Lmaps/l/h;

    iget-object v0, v0, Lmaps/l/h;->a:Lmaps/l/af;

    sget-object v4, Lmaps/l/af;->b:Lmaps/l/af;

    if-ne v0, v4, :cond_6

    :cond_a
    sget-object v0, Lmaps/l/e;->b:[I

    iget-object v4, p0, Lmaps/l/aw;->o:Lmaps/l/h;

    iget-object v4, v4, Lmaps/l/h;->b:Lmaps/l/w;

    invoke-virtual {v4}, Lmaps/l/w;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_1

    goto/16 :goto_4

    :pswitch_0
    neg-int v0, v3

    add-int/lit8 v0, v0, -0xa

    int-to-float v0, v0

    iput v0, p0, Lmaps/l/aw;->p:F

    iget v0, p0, Lmaps/l/aw;->p:F

    mul-int/lit8 v4, v7, 0x2

    int-to-float v4, v4

    add-float/2addr v0, v4

    int-to-float v3, v3

    invoke-static {v0, v3}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lmaps/l/aw;->q:F

    goto/16 :goto_4

    :cond_b
    move v6, v7

    goto :goto_6

    :cond_c
    move v4, v5

    goto :goto_7

    :pswitch_1
    neg-int v4, v0

    mul-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    int-to-float v4, v4

    iput v4, p0, Lmaps/l/aw;->r:F

    int-to-float v0, v0

    iput v0, p0, Lmaps/l/aw;->s:F

    goto :goto_8

    :pswitch_2
    neg-int v0, v3

    int-to-float v0, v0

    iput v0, p0, Lmaps/l/aw;->p:F

    mul-int/lit8 v0, v7, 0x2

    add-int/2addr v0, v3

    int-to-float v0, v0

    iput v0, p0, Lmaps/l/aw;->q:F

    neg-int v0, v4

    int-to-float v0, v0

    iput v0, p0, Lmaps/l/aw;->r:F

    int-to-float v0, v4

    iput v0, p0, Lmaps/l/aw;->s:F

    goto :goto_8

    :pswitch_3
    neg-int v4, v0

    int-to-float v4, v4

    iput v4, p0, Lmaps/l/aw;->r:F

    mul-int/lit8 v4, v5, 0x2

    add-int/2addr v0, v4

    int-to-float v0, v0

    iput v0, p0, Lmaps/l/aw;->s:F

    goto :goto_8

    :pswitch_4
    neg-int v0, v3

    mul-int/lit8 v5, v7, 0x2

    sub-int/2addr v0, v5

    int-to-float v0, v0

    iput v0, p0, Lmaps/l/aw;->p:F

    int-to-float v0, v3

    iput v0, p0, Lmaps/l/aw;->q:F

    neg-int v0, v4

    int-to-float v0, v0

    iput v0, p0, Lmaps/l/aw;->r:F

    int-to-float v0, v4

    iput v0, p0, Lmaps/l/aw;->s:F

    goto :goto_8

    :pswitch_5
    neg-int v4, v3

    int-to-float v4, v4

    iput v4, p0, Lmaps/l/aw;->p:F

    mul-int/lit8 v4, v7, 0x2

    add-int/2addr v4, v3

    int-to-float v4, v4

    iput v4, p0, Lmaps/l/aw;->q:F

    neg-int v4, v0

    int-to-float v4, v4

    iput v4, p0, Lmaps/l/aw;->r:F

    mul-int/lit8 v4, v5, 0x2

    add-int/2addr v0, v4

    int-to-float v0, v0

    iput v0, p0, Lmaps/l/aw;->s:F

    goto/16 :goto_8

    :pswitch_6
    neg-int v4, v3

    mul-int/lit8 v8, v7, 0x2

    sub-int/2addr v4, v8

    int-to-float v4, v4

    iput v4, p0, Lmaps/l/aw;->p:F

    int-to-float v4, v3

    iput v4, p0, Lmaps/l/aw;->q:F

    neg-int v4, v0

    int-to-float v4, v4

    iput v4, p0, Lmaps/l/aw;->r:F

    mul-int/lit8 v4, v5, 0x2

    add-int/2addr v0, v4

    int-to-float v0, v0

    iput v0, p0, Lmaps/l/aw;->s:F

    goto/16 :goto_8

    :pswitch_7
    neg-int v4, v3

    int-to-float v4, v4

    iput v4, p0, Lmaps/l/aw;->p:F

    mul-int/lit8 v4, v7, 0x2

    add-int/2addr v4, v3

    int-to-float v4, v4

    iput v4, p0, Lmaps/l/aw;->q:F

    neg-int v4, v0

    mul-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    int-to-float v4, v4

    iput v4, p0, Lmaps/l/aw;->r:F

    int-to-float v0, v0

    iput v0, p0, Lmaps/l/aw;->s:F

    goto/16 :goto_8

    :pswitch_8
    neg-int v4, v3

    mul-int/lit8 v8, v7, 0x2

    sub-int/2addr v4, v8

    int-to-float v4, v4

    iput v4, p0, Lmaps/l/aw;->p:F

    int-to-float v4, v3

    iput v4, p0, Lmaps/l/aw;->q:F

    neg-int v4, v0

    mul-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    int-to-float v4, v4

    iput v4, p0, Lmaps/l/aw;->r:F

    int-to-float v0, v0

    iput v0, p0, Lmaps/l/aw;->s:F

    goto/16 :goto_8

    :pswitch_9
    add-int/lit8 v0, v3, 0xa

    int-to-float v0, v0

    iput v0, p0, Lmaps/l/aw;->q:F

    iget v0, p0, Lmaps/l/aw;->q:F

    mul-int/lit8 v4, v7, 0x2

    int-to-float v4, v4

    sub-float/2addr v0, v4

    neg-int v3, v3

    int-to-float v3, v3

    invoke-static {v0, v3}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, Lmaps/l/aw;->p:F

    goto/16 :goto_4

    :pswitch_a
    neg-int v0, v6

    int-to-float v0, v0

    iput v0, p0, Lmaps/l/aw;->p:F

    int-to-float v0, v6

    iput v0, p0, Lmaps/l/aw;->q:F

    goto/16 :goto_4

    :cond_d
    iget-object v0, p0, Lmaps/l/aw;->l:Lmaps/t/y;

    invoke-virtual {v0}, Lmaps/t/y;->b()Lmaps/t/bx;

    move-result-object v0

    iget-object v3, p0, Lmaps/l/aw;->I:[F

    invoke-virtual {p1, v0, v3}, Lmaps/bq/d;->a(Lmaps/t/bx;[F)V

    iget-object v0, p0, Lmaps/l/aw;->I:[F

    aget v0, v0, v2

    iget-object v3, p0, Lmaps/l/aw;->I:[F

    aget v3, v3, v1

    iget v4, p0, Lmaps/l/aw;->p:F

    add-float/2addr v4, v0

    iget v5, p0, Lmaps/l/aw;->q:F

    add-float/2addr v0, v5

    iget v5, p0, Lmaps/l/aw;->r:F

    add-float/2addr v5, v3

    iget v6, p0, Lmaps/l/aw;->s:F

    add-float/2addr v3, v6

    invoke-virtual {p1, v4, v0, v5, v3}, Lmaps/bq/d;->a(FFFF)Lmaps/t/ak;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/aw;->u:Lmaps/t/ak;

    goto/16 :goto_5

    :cond_e
    move v0, v2

    move v3, v2

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_1
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_0
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method a(Lmaps/cr/c;Lmaps/af/q;)Z
    .locals 5

    const/4 v4, 0x1

    iget-boolean v0, p0, Lmaps/l/aw;->h:Z

    if-eqz v0, :cond_0

    new-instance v0, Lmaps/w/v;

    const-wide/16 v1, 0x1f4

    sget-object v3, Lmaps/w/j;->a:Lmaps/w/j;

    invoke-direct {v0, v1, v2, v3}, Lmaps/w/v;-><init>(JLmaps/w/j;)V

    iput-object v0, p0, Lmaps/l/aw;->E:Lmaps/w/v;

    :cond_0
    iput-boolean v4, p0, Lmaps/l/aw;->B:Z

    return v4
.end method

.method public a(Lmaps/t/bf;)Z
    .locals 1

    iget-object v0, p0, Lmaps/l/aw;->l:Lmaps/t/y;

    invoke-virtual {v0}, Lmaps/t/y;->b()Lmaps/t/bx;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmaps/t/bf;->a(Lmaps/t/bx;)Z

    move-result v0

    return v0
.end method

.method public b(Lmaps/bq/d;Lmaps/cr/c;)Z
    .locals 5

    const/4 v4, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget v2, p0, Lmaps/l/aw;->D:I

    add-int/lit8 v2, v2, 0x1

    iget-object v3, p0, Lmaps/l/aw;->C:[Lmaps/l/h;

    array-length v3, v3

    if-ge v2, v3, :cond_0

    iget-object v1, p0, Lmaps/l/aw;->C:[Lmaps/l/h;

    iget v2, p0, Lmaps/l/aw;->D:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lmaps/l/aw;->D:I

    aget-object v1, v1, v2

    iput-object v1, p0, Lmaps/l/aw;->o:Lmaps/l/h;

    iget-object v1, p0, Lmaps/l/aw;->n:Lmaps/l/g;

    iget-object v2, p0, Lmaps/l/aw;->o:Lmaps/l/h;

    iget-object v2, v2, Lmaps/l/h;->b:Lmaps/l/w;

    invoke-virtual {v1, v2}, Lmaps/l/g;->a(Lmaps/l/w;)V

    iput-object v4, p0, Lmaps/l/aw;->u:Lmaps/t/ak;

    invoke-virtual {p0, p1, p2}, Lmaps/l/aw;->a(Lmaps/bq/d;Lmaps/cr/c;)Z

    :goto_0
    return v0

    :cond_0
    iget-object v2, p0, Lmaps/l/aw;->t:Lmaps/t/y;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lmaps/l/aw;->t:Lmaps/t/y;

    iput-object v2, p0, Lmaps/l/aw;->l:Lmaps/t/y;

    iput-object v4, p0, Lmaps/l/aw;->t:Lmaps/t/y;

    iget-object v2, p0, Lmaps/l/aw;->C:[Lmaps/l/h;

    array-length v2, v2

    if-le v2, v0, :cond_1

    iput v1, p0, Lmaps/l/aw;->D:I

    iget-object v2, p0, Lmaps/l/aw;->C:[Lmaps/l/h;

    aget-object v1, v2, v1

    iput-object v1, p0, Lmaps/l/aw;->o:Lmaps/l/h;

    iget-object v1, p0, Lmaps/l/aw;->n:Lmaps/l/g;

    iget-object v2, p0, Lmaps/l/aw;->o:Lmaps/l/h;

    iget-object v2, v2, Lmaps/l/h;->b:Lmaps/l/w;

    invoke-virtual {v1, v2}, Lmaps/l/g;->a(Lmaps/l/w;)V

    :cond_1
    iput-object v4, p0, Lmaps/l/aw;->u:Lmaps/t/ak;

    invoke-virtual {p0, p1, p2}, Lmaps/l/aw;->a(Lmaps/bq/d;Lmaps/cr/c;)Z

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public c(Lmaps/cr/c;)V
    .locals 1

    invoke-super {p0, p1}, Lmaps/l/u;->c(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/aw;->m:Lmaps/l/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/l/aw;->m:Lmaps/l/g;

    invoke-virtual {v0, p1}, Lmaps/l/g;->c(Lmaps/cr/c;)V

    :cond_0
    iget-object v0, p0, Lmaps/l/aw;->n:Lmaps/l/g;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/l/aw;->n:Lmaps/l/g;

    invoke-virtual {v0, p1}, Lmaps/l/g;->c(Lmaps/cr/c;)V

    :cond_1
    iget-object v0, p0, Lmaps/l/aw;->w:Lmaps/al/o;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/l/aw;->w:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->b(Lmaps/cr/c;)V

    :cond_2
    return-void
.end method

.method public l()F
    .locals 1

    iget v0, p0, Lmaps/l/aw;->H:F

    return v0
.end method

.method public m()Lmaps/t/an;
    .locals 1

    iget-object v0, p0, Lmaps/l/aw;->u:Lmaps/t/ak;

    return-object v0
.end method

.method public s()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/l/aw;->x:Ljava/lang/String;

    return-object v0
.end method

.method public v()Z
    .locals 1

    iget-object v0, p0, Lmaps/l/aw;->l:Lmaps/t/y;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/l/aw;->l:Lmaps/t/y;

    invoke-virtual {v0}, Lmaps/t/y;->a()Z

    move-result v0

    goto :goto_0
.end method
