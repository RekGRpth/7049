.class public Lmaps/l/c;
.super Lmaps/l/ak;


# static fields
.field private static final a:Lmaps/t/bx;

.field private static final c:Ljava/util/Comparator;


# instance fields
.field private d:Lmaps/s/t;

.field private e:Lmaps/s/r;

.field private final f:Lmaps/al/o;

.field private final g:Lmaps/al/a;

.field private h:Lmaps/w/u;

.field private final i:Lmaps/al/i;

.field private final j:Lmaps/al/i;

.field private final k:Lmaps/al/b;

.field private final l:Lmaps/t/bx;

.field private final m:Lmaps/t/bx;

.field private final n:Lmaps/t/bx;

.field private final o:Lmaps/t/bx;

.field private final p:Lmaps/t/bx;

.field private final q:Lmaps/t/bx;

.field private final r:Lmaps/t/bx;

.field private final s:Lmaps/t/bx;

.field private final t:Lmaps/t/bx;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const v2, 0xb504

    new-instance v0, Lmaps/t/bx;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v2, v1}, Lmaps/t/bx;-><init>(III)V

    sput-object v0, Lmaps/l/c;->a:Lmaps/t/bx;

    new-instance v0, Lmaps/l/am;

    invoke-direct {v0}, Lmaps/l/am;-><init>()V

    sput-object v0, Lmaps/l/c;->c:Ljava/util/Comparator;

    return-void
.end method

.method private constructor <init>(Lmaps/l/ax;Ljava/util/Set;)V
    .locals 5

    const/4 v4, 0x0

    invoke-direct {p0, p2}, Lmaps/l/ak;-><init>(Ljava/util/Set;)V

    new-instance v0, Lmaps/w/u;

    invoke-direct {v0}, Lmaps/w/u;-><init>()V

    iput-object v0, p0, Lmaps/l/c;->h:Lmaps/w/u;

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    new-instance v1, Lmaps/s/t;

    iget v2, p1, Lmaps/l/ax;->a:I

    const/4 v3, 0x5

    invoke-direct {v1, v2, v3, v0}, Lmaps/s/t;-><init>(IIZ)V

    iput-object v1, p0, Lmaps/l/c;->d:Lmaps/s/t;

    iput-object v4, p0, Lmaps/l/c;->f:Lmaps/al/o;

    iput-object v4, p0, Lmaps/l/c;->g:Lmaps/al/a;

    iput-object v4, p0, Lmaps/l/c;->i:Lmaps/al/i;

    new-instance v0, Lmaps/al/j;

    iget v1, p1, Lmaps/l/ax;->d:I

    invoke-direct {v0, v1}, Lmaps/al/j;-><init>(I)V

    iput-object v0, p0, Lmaps/l/c;->j:Lmaps/al/i;

    new-instance v0, Lmaps/l/y;

    iget v1, p1, Lmaps/l/ax;->c:I

    invoke-direct {v0, p0, v1}, Lmaps/l/y;-><init>(Lmaps/l/c;I)V

    iput-object v0, p0, Lmaps/l/c;->k:Lmaps/al/b;

    :goto_0
    new-instance v0, Lmaps/t/bx;

    invoke-direct {v0}, Lmaps/t/bx;-><init>()V

    iput-object v0, p0, Lmaps/l/c;->l:Lmaps/t/bx;

    new-instance v0, Lmaps/t/bx;

    invoke-direct {v0}, Lmaps/t/bx;-><init>()V

    iput-object v0, p0, Lmaps/l/c;->m:Lmaps/t/bx;

    new-instance v0, Lmaps/t/bx;

    invoke-direct {v0}, Lmaps/t/bx;-><init>()V

    iput-object v0, p0, Lmaps/l/c;->n:Lmaps/t/bx;

    new-instance v0, Lmaps/t/bx;

    invoke-direct {v0}, Lmaps/t/bx;-><init>()V

    iput-object v0, p0, Lmaps/l/c;->o:Lmaps/t/bx;

    new-instance v0, Lmaps/t/bx;

    invoke-direct {v0}, Lmaps/t/bx;-><init>()V

    iput-object v0, p0, Lmaps/l/c;->p:Lmaps/t/bx;

    new-instance v0, Lmaps/t/bx;

    invoke-direct {v0}, Lmaps/t/bx;-><init>()V

    iput-object v0, p0, Lmaps/l/c;->q:Lmaps/t/bx;

    new-instance v0, Lmaps/t/bx;

    invoke-direct {v0}, Lmaps/t/bx;-><init>()V

    iput-object v0, p0, Lmaps/l/c;->r:Lmaps/t/bx;

    new-instance v0, Lmaps/t/bx;

    invoke-direct {v0}, Lmaps/t/bx;-><init>()V

    iput-object v0, p0, Lmaps/l/c;->s:Lmaps/t/bx;

    new-instance v0, Lmaps/t/bx;

    invoke-direct {v0}, Lmaps/t/bx;-><init>()V

    iput-object v0, p0, Lmaps/l/c;->t:Lmaps/t/bx;

    return-void

    :cond_0
    new-instance v0, Lmaps/al/e;

    iget v1, p1, Lmaps/l/ax;->a:I

    invoke-direct {v0, v1}, Lmaps/al/e;-><init>(I)V

    iput-object v0, p0, Lmaps/l/c;->f:Lmaps/al/o;

    new-instance v0, Lmaps/al/f;

    iget v1, p1, Lmaps/l/ax;->a:I

    invoke-direct {v0, v1}, Lmaps/al/f;-><init>(I)V

    iput-object v0, p0, Lmaps/l/c;->g:Lmaps/al/a;

    new-instance v0, Lmaps/al/j;

    iget v1, p1, Lmaps/l/ax;->c:I

    iget v2, p1, Lmaps/l/ax;->b:I

    add-int/2addr v1, v2

    invoke-direct {v0, v1}, Lmaps/al/j;-><init>(I)V

    iput-object v0, p0, Lmaps/l/c;->i:Lmaps/al/i;

    new-instance v0, Lmaps/al/j;

    iget v1, p1, Lmaps/l/ax;->d:I

    invoke-direct {v0, v1}, Lmaps/al/j;-><init>(I)V

    iput-object v0, p0, Lmaps/l/c;->j:Lmaps/al/i;

    new-instance v0, Lmaps/al/i;

    iget v1, p1, Lmaps/l/ax;->c:I

    invoke-direct {v0, v1}, Lmaps/al/i;-><init>(I)V

    iput-object v0, p0, Lmaps/l/c;->k:Lmaps/al/b;

    goto :goto_0
.end method

.method private static final a(I)I
    .locals 4

    shr-int/lit8 v0, p0, 0x10

    and-int/lit16 v0, v0, 0xff

    shr-int/lit8 v1, p0, 0x8

    and-int/lit16 v1, v1, 0xff

    and-int/lit16 v2, p0, 0xff

    add-int/lit16 v0, v0, 0x2fd

    shr-int/lit8 v0, v0, 0x2

    add-int/lit16 v1, v1, 0x2fd

    shr-int/lit8 v1, v1, 0x2

    add-int/lit16 v2, v2, 0x2fd

    shr-int/lit8 v2, v2, 0x2

    const/high16 v3, -0x1000000

    and-int/2addr v3, p0

    shl-int/lit8 v0, v0, 0x10

    or-int/2addr v0, v3

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    or-int/2addr v0, v2

    return v0
.end method

.method private static a(II)I
    .locals 2

    shr-int/lit8 v0, p0, 0x18

    and-int/lit16 v0, v0, 0xff

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    const v1, 0xffffff

    and-int/2addr v1, p0

    shl-int/lit8 v0, v0, 0x18

    or-int/2addr v0, v1

    return v0
.end method

.method private static a(ILmaps/t/bx;)I
    .locals 6

    const/high16 v0, -0x1000000

    and-int v1, p0, v0

    shr-int/lit8 v0, p0, 0x10

    and-int/lit16 v2, v0, 0xff

    shr-int/lit8 v0, p0, 0x8

    and-int/lit16 v3, v0, 0xff

    and-int/lit16 v4, p0, 0xff

    sget-object v0, Lmaps/l/c;->a:Lmaps/t/bx;

    invoke-static {p1, v0}, Lmaps/t/bx;->a(Lmaps/t/bx;Lmaps/t/bx;)F

    move-result v0

    invoke-virtual {p1}, Lmaps/t/bx;->i()F

    move-result v5

    div-float/2addr v0, v5

    float-to-int v0, v0

    if-gez v0, :cond_0

    neg-int v0, v0

    :cond_0
    mul-int/lit16 v0, v0, 0x4ccc

    shr-int/lit8 v0, v0, 0x10

    const v5, 0xb333

    add-int/2addr v0, v5

    mul-int/2addr v2, v0

    shr-int/lit8 v2, v2, 0x10

    mul-int/2addr v3, v0

    shr-int/lit8 v3, v3, 0x10

    mul-int/2addr v0, v4

    shr-int/lit8 v0, v0, 0x10

    shl-int/lit8 v2, v2, 0x10

    or-int/2addr v1, v2

    shl-int/lit8 v2, v3, 0x8

    or-int/2addr v1, v2

    or-int/2addr v0, v1

    return v0
.end method

.method public static a(Lmaps/t/ah;[Ljava/lang/String;Lmaps/t/h;)Lmaps/l/c;
    .locals 11

    const/4 v3, 0x0

    const/4 v0, 0x0

    invoke-static {}, Lmaps/af/w;->b()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-static {}, Lmaps/b/r;->a()Lmaps/b/r;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/b/r;->j()Lmaps/b/u;

    move-result-object v0

    move-object v2, v0

    :goto_0
    new-instance v4, Ljava/util/ArrayList;

    const/16 v0, 0x80

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v5, Lmaps/l/ax;

    invoke-direct {v5}, Lmaps/l/ax;-><init>()V

    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    :goto_1
    invoke-interface {p2}, Lmaps/t/h;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Lmaps/t/h;->a()Lmaps/t/ca;

    move-result-object v1

    instance-of v0, v1, Lmaps/t/l;

    if-eqz v0, :cond_0

    move-object v0, v1

    check-cast v0, Lmaps/t/l;

    invoke-static {v0, v5}, Lmaps/l/c;->a(Lmaps/t/l;Lmaps/l/ax;)Z

    move-result v7

    if-nez v7, :cond_1

    :cond_0
    sget-object v0, Lmaps/l/c;->c:Ljava/util/Comparator;

    invoke-static {v4, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-virtual {p0}, Lmaps/t/ah;->j()Lmaps/t/ax;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/t/ax;->d()Lmaps/t/bx;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/bx;->b()D

    move-result-wide v2

    invoke-static {v2, v3}, Lmaps/t/bx;->a(D)D

    move-result-wide v2

    double-to-float v2, v2

    new-instance v3, Lmaps/l/c;

    invoke-direct {v3, v5, v6}, Lmaps/l/c;-><init>(Lmaps/l/ax;Ljava/util/Set;)V

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/l;

    invoke-direct {v3, p0, v1, v0, v2}, Lmaps/l/c;->a(Lmaps/t/ah;Lmaps/t/ax;Lmaps/t/l;F)V

    goto :goto_2

    :cond_1
    invoke-interface {v1}, Lmaps/t/ca;->j()[I

    move-result-object v7

    array-length v8, v7

    move v1, v3

    :goto_3
    if-ge v1, v8, :cond_3

    aget v9, v7, v1

    if-ltz v9, :cond_2

    array-length v10, p1

    if-ge v9, v10, :cond_2

    aget-object v9, p1, v9

    invoke-virtual {v6, v9}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_3
    invoke-virtual {v0}, Lmaps/t/l;->e()Z

    move-result v1

    if-nez v1, :cond_4

    if-eqz v2, :cond_6

    invoke-virtual {v0}, Lmaps/t/l;->b()Lmaps/t/v;

    move-result-object v1

    invoke-interface {v2, v1}, Lmaps/b/u;->a(Lmaps/t/v;)Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_4
    const/4 v1, 0x1

    :goto_4
    if-nez v1, :cond_5

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    invoke-interface {p2}, Lmaps/t/h;->next()Ljava/lang/Object;

    goto :goto_1

    :cond_6
    move v1, v3

    goto :goto_4

    :cond_7
    invoke-direct {v3}, Lmaps/l/c;->c()V

    invoke-virtual {v3, p0}, Lmaps/l/c;->a(Lmaps/t/ah;)V

    return-object v3

    :cond_8
    move-object v2, v0

    goto/16 :goto_0
.end method

.method public static a(Lmaps/cr/c;I)V
    .locals 4

    const/high16 v3, 0x10000

    invoke-virtual {p0}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/cr/c;->w()V

    invoke-virtual {p0}, Lmaps/cr/c;->y()V

    invoke-interface {v0, v3, v3}, Ljavax/microedition/khronos/opengles/GL10;->glPolygonOffsetx(II)V

    const/16 v1, 0xb

    if-ne p1, v1, :cond_1

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v1, 0x201

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDepthFunc(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v1, 0xc

    if-ne p1, v1, :cond_0

    invoke-virtual {p0}, Lmaps/cr/c;->p()V

    const/16 v1, 0x203

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDepthFunc(I)V

    const/16 v1, 0x302

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    invoke-interface {v0, v3}, Ljavax/microedition/khronos/opengles/GL10;->glLineWidthx(I)V

    goto :goto_0
.end method

.method private a(Lmaps/t/ah;Lmaps/t/ax;Lmaps/t/l;F)V
    .locals 14

    invoke-virtual/range {p3 .. p3}, Lmaps/t/l;->h()Lmaps/t/aa;

    move-result-object v1

    invoke-virtual/range {p3 .. p3}, Lmaps/t/l;->c()Lmaps/t/ad;

    move-result-object v10

    invoke-virtual {v10}, Lmaps/t/ad;->a()I

    move-result v11

    invoke-virtual {v1}, Lmaps/t/aa;->d()I

    move-result v2

    if-eqz v11, :cond_0

    if-nez v2, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual/range {p2 .. p2}, Lmaps/t/ax;->d()Lmaps/t/bx;

    move-result-object v12

    invoke-virtual/range {p2 .. p2}, Lmaps/t/ax;->g()I

    move-result v6

    invoke-virtual/range {p3 .. p3}, Lmaps/t/l;->f()I

    move-result v3

    invoke-virtual/range {p3 .. p3}, Lmaps/t/l;->g()I

    move-result v13

    iget-object v4, p0, Lmaps/l/c;->r:Lmaps/t/bx;

    const/4 v5, 0x0

    const/4 v7, 0x0

    int-to-float v3, v3

    mul-float v3, v3, p4

    float-to-int v3, v3

    invoke-virtual {v4, v5, v7, v3}, Lmaps/t/bx;->a(III)V

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lmaps/t/aa;->a(I)I

    move-result v3

    const/16 v4, 0xa0

    invoke-static {v3, v4}, Lmaps/l/c;->a(II)I

    move-result v7

    const/4 v3, 0x1

    if-le v2, v3, :cond_6

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lmaps/t/aa;->a(I)I

    move-result v1

    const/16 v2, 0xa0

    invoke-static {v1, v2}, Lmaps/l/c;->a(II)I

    move-result v1

    move v8, v1

    :goto_0
    const/4 v1, 0x0

    move v9, v1

    :goto_1
    if-ge v9, v11, :cond_0

    sget-boolean v1, Lmaps/ae/h;->F:Z

    if-eqz v1, :cond_7

    iget-object v1, p0, Lmaps/l/c;->d:Lmaps/s/t;

    invoke-virtual {v1}, Lmaps/s/t;->c()I

    move-result v1

    :goto_2
    iget-object v2, p0, Lmaps/l/c;->l:Lmaps/t/bx;

    iget-object v3, p0, Lmaps/l/c;->m:Lmaps/t/bx;

    iget-object v4, p0, Lmaps/l/c;->n:Lmaps/t/bx;

    invoke-virtual {v10, v9, v2, v3, v4}, Lmaps/t/ad;->a(ILmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    iget-object v2, p0, Lmaps/l/c;->l:Lmaps/t/bx;

    iget-object v3, p0, Lmaps/l/c;->l:Lmaps/t/bx;

    invoke-static {v2, v12, v3}, Lmaps/t/bx;->b(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    iget-object v2, p0, Lmaps/l/c;->m:Lmaps/t/bx;

    iget-object v3, p0, Lmaps/l/c;->m:Lmaps/t/bx;

    invoke-static {v2, v12, v3}, Lmaps/t/bx;->b(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    iget-object v2, p0, Lmaps/l/c;->n:Lmaps/t/bx;

    iget-object v3, p0, Lmaps/l/c;->n:Lmaps/t/bx;

    invoke-static {v2, v12, v3}, Lmaps/t/bx;->b(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    if-eqz v13, :cond_2

    iget-object v2, p0, Lmaps/l/c;->s:Lmaps/t/bx;

    const/4 v3, 0x0

    const/4 v4, 0x0

    int-to-float v5, v13

    mul-float v5, v5, p4

    float-to-int v5, v5

    invoke-virtual {v2, v3, v4, v5}, Lmaps/t/bx;->a(III)V

    iget-object v2, p0, Lmaps/l/c;->l:Lmaps/t/bx;

    iget-object v3, p0, Lmaps/l/c;->s:Lmaps/t/bx;

    iget-object v4, p0, Lmaps/l/c;->l:Lmaps/t/bx;

    invoke-static {v2, v3, v4}, Lmaps/t/bx;->a(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    iget-object v2, p0, Lmaps/l/c;->m:Lmaps/t/bx;

    iget-object v3, p0, Lmaps/l/c;->s:Lmaps/t/bx;

    iget-object v4, p0, Lmaps/l/c;->m:Lmaps/t/bx;

    invoke-static {v2, v3, v4}, Lmaps/t/bx;->a(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    iget-object v2, p0, Lmaps/l/c;->n:Lmaps/t/bx;

    iget-object v3, p0, Lmaps/l/c;->s:Lmaps/t/bx;

    iget-object v4, p0, Lmaps/l/c;->n:Lmaps/t/bx;

    invoke-static {v2, v3, v4}, Lmaps/t/bx;->a(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    :cond_2
    iget-object v2, p0, Lmaps/l/c;->l:Lmaps/t/bx;

    iget-object v3, p0, Lmaps/l/c;->r:Lmaps/t/bx;

    iget-object v4, p0, Lmaps/l/c;->o:Lmaps/t/bx;

    invoke-static {v2, v3, v4}, Lmaps/t/bx;->a(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    iget-object v2, p0, Lmaps/l/c;->m:Lmaps/t/bx;

    iget-object v3, p0, Lmaps/l/c;->r:Lmaps/t/bx;

    iget-object v4, p0, Lmaps/l/c;->p:Lmaps/t/bx;

    invoke-static {v2, v3, v4}, Lmaps/t/bx;->a(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    iget-object v2, p0, Lmaps/l/c;->n:Lmaps/t/bx;

    iget-object v3, p0, Lmaps/l/c;->r:Lmaps/t/bx;

    iget-object v4, p0, Lmaps/l/c;->q:Lmaps/t/bx;

    invoke-static {v2, v3, v4}, Lmaps/t/bx;->a(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    sget-boolean v2, Lmaps/ae/h;->F:Z

    if-eqz v2, :cond_8

    iget-object v2, p0, Lmaps/l/c;->d:Lmaps/s/t;

    iget-object v3, p0, Lmaps/l/c;->o:Lmaps/t/bx;

    invoke-virtual {v2, v3, v6}, Lmaps/s/t;->a(Lmaps/t/bx;I)V

    iget-object v2, p0, Lmaps/l/c;->d:Lmaps/s/t;

    iget-object v3, p0, Lmaps/l/c;->p:Lmaps/t/bx;

    invoke-virtual {v2, v3, v6}, Lmaps/s/t;->a(Lmaps/t/bx;I)V

    iget-object v2, p0, Lmaps/l/c;->d:Lmaps/s/t;

    iget-object v3, p0, Lmaps/l/c;->q:Lmaps/t/bx;

    invoke-virtual {v2, v3, v6}, Lmaps/s/t;->a(Lmaps/t/bx;I)V

    iget-object v2, p0, Lmaps/l/c;->d:Lmaps/s/t;

    const/4 v3, 0x3

    invoke-virtual {v2, v8, v3}, Lmaps/s/t;->b(II)V

    iget-object v2, p0, Lmaps/l/c;->d:Lmaps/s/t;

    int-to-short v3, v1

    add-int/lit8 v4, v1, 0x1

    int-to-short v4, v4

    add-int/lit8 v1, v1, 0x2

    int-to-short v1, v1

    invoke-virtual {v2, v3, v4, v1}, Lmaps/s/t;->a(III)V

    :goto_3
    const/4 v1, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v9, v1}, Lmaps/t/l;->a(II)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v2, p0, Lmaps/l/c;->l:Lmaps/t/bx;

    iget-object v3, p0, Lmaps/l/c;->m:Lmaps/t/bx;

    iget-object v4, p0, Lmaps/l/c;->o:Lmaps/t/bx;

    iget-object v5, p0, Lmaps/l/c;->p:Lmaps/t/bx;

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lmaps/l/c;->a(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;II)V

    :cond_3
    const/4 v1, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v9, v1}, Lmaps/t/l;->a(II)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v2, p0, Lmaps/l/c;->m:Lmaps/t/bx;

    iget-object v3, p0, Lmaps/l/c;->n:Lmaps/t/bx;

    iget-object v4, p0, Lmaps/l/c;->p:Lmaps/t/bx;

    iget-object v5, p0, Lmaps/l/c;->q:Lmaps/t/bx;

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lmaps/l/c;->a(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;II)V

    :cond_4
    const/4 v1, 0x2

    move-object/from16 v0, p3

    invoke-virtual {v0, v9, v1}, Lmaps/t/l;->a(II)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v2, p0, Lmaps/l/c;->n:Lmaps/t/bx;

    iget-object v3, p0, Lmaps/l/c;->l:Lmaps/t/bx;

    iget-object v4, p0, Lmaps/l/c;->q:Lmaps/t/bx;

    iget-object v5, p0, Lmaps/l/c;->o:Lmaps/t/bx;

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lmaps/l/c;->a(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;II)V

    :cond_5
    add-int/lit8 v1, v9, 0x1

    move v9, v1

    goto/16 :goto_1

    :cond_6
    invoke-static {v7}, Lmaps/l/c;->a(I)I

    move-result v1

    move v8, v1

    goto/16 :goto_0

    :cond_7
    iget-object v1, p0, Lmaps/l/c;->f:Lmaps/al/o;

    invoke-virtual {v1}, Lmaps/al/o;->c()I

    move-result v1

    goto/16 :goto_2

    :cond_8
    iget-object v2, p0, Lmaps/l/c;->f:Lmaps/al/o;

    iget-object v3, p0, Lmaps/l/c;->o:Lmaps/t/bx;

    invoke-virtual {v2, v3, v6}, Lmaps/al/o;->a(Lmaps/t/bx;I)V

    iget-object v2, p0, Lmaps/l/c;->f:Lmaps/al/o;

    iget-object v3, p0, Lmaps/l/c;->p:Lmaps/t/bx;

    invoke-virtual {v2, v3, v6}, Lmaps/al/o;->a(Lmaps/t/bx;I)V

    iget-object v2, p0, Lmaps/l/c;->f:Lmaps/al/o;

    iget-object v3, p0, Lmaps/l/c;->q:Lmaps/t/bx;

    invoke-virtual {v2, v3, v6}, Lmaps/al/o;->a(Lmaps/t/bx;I)V

    iget-object v2, p0, Lmaps/l/c;->g:Lmaps/al/a;

    const/4 v3, 0x3

    invoke-virtual {v2, v8, v3}, Lmaps/al/a;->b(II)V

    iget-object v2, p0, Lmaps/l/c;->i:Lmaps/al/i;

    int-to-short v3, v1

    add-int/lit8 v4, v1, 0x1

    int-to-short v4, v4

    add-int/lit8 v1, v1, 0x2

    int-to-short v1, v1

    invoke-virtual {v2, v3, v4, v1}, Lmaps/al/i;->a(SSS)V

    goto :goto_3
.end method

.method private a(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;II)V
    .locals 6

    const/4 v5, 0x4

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/l/c;->d:Lmaps/s/t;

    invoke-virtual {v0}, Lmaps/s/t;->c()I

    move-result v0

    :goto_0
    sget-boolean v1, Lmaps/ae/h;->F:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lmaps/l/c;->d:Lmaps/s/t;

    invoke-virtual {v1, p1, p5}, Lmaps/s/t;->a(Lmaps/t/bx;I)V

    iget-object v1, p0, Lmaps/l/c;->d:Lmaps/s/t;

    invoke-virtual {v1, p3, p5}, Lmaps/s/t;->a(Lmaps/t/bx;I)V

    iget-object v1, p0, Lmaps/l/c;->d:Lmaps/s/t;

    invoke-virtual {v1, p2, p5}, Lmaps/s/t;->a(Lmaps/t/bx;I)V

    iget-object v1, p0, Lmaps/l/c;->d:Lmaps/s/t;

    invoke-virtual {v1, p4, p5}, Lmaps/s/t;->a(Lmaps/t/bx;I)V

    iget-object v1, p0, Lmaps/l/c;->k:Lmaps/al/b;

    add-int/lit8 v2, v0, 0x1

    add-int/lit8 v3, v0, 0x3

    add-int/lit8 v4, v0, 0x2

    invoke-interface {v1, v2, v0, v3, v4}, Lmaps/al/b;->a(IIII)V

    iget-object v1, p0, Lmaps/l/c;->j:Lmaps/al/i;

    add-int/lit8 v2, v0, 0x1

    int-to-short v2, v2

    add-int/lit8 v0, v0, 0x3

    int-to-short v0, v0

    invoke-virtual {v1, v2, v0}, Lmaps/al/i;->a(SS)V

    iget-object v0, p0, Lmaps/l/c;->t:Lmaps/t/bx;

    invoke-static {p2, p1, v0}, Lmaps/t/bx;->b(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    iget-object v0, p0, Lmaps/l/c;->d:Lmaps/s/t;

    iget-object v1, p0, Lmaps/l/c;->t:Lmaps/t/bx;

    invoke-static {p6, v1}, Lmaps/l/c;->a(ILmaps/t/bx;)I

    move-result v1

    invoke-virtual {v0, v1, v5}, Lmaps/s/t;->b(II)V

    :goto_1
    return-void

    :cond_0
    iget-object v0, p0, Lmaps/l/c;->f:Lmaps/al/o;

    invoke-virtual {v0}, Lmaps/al/o;->c()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lmaps/l/c;->f:Lmaps/al/o;

    invoke-virtual {v1, p1, p5}, Lmaps/al/o;->a(Lmaps/t/bx;I)V

    iget-object v1, p0, Lmaps/l/c;->f:Lmaps/al/o;

    invoke-virtual {v1, p3, p5}, Lmaps/al/o;->a(Lmaps/t/bx;I)V

    iget-object v1, p0, Lmaps/l/c;->f:Lmaps/al/o;

    invoke-virtual {v1, p2, p5}, Lmaps/al/o;->a(Lmaps/t/bx;I)V

    iget-object v1, p0, Lmaps/l/c;->f:Lmaps/al/o;

    invoke-virtual {v1, p4, p5}, Lmaps/al/o;->a(Lmaps/t/bx;I)V

    iget-object v1, p0, Lmaps/l/c;->k:Lmaps/al/b;

    add-int/lit8 v2, v0, 0x1

    add-int/lit8 v3, v0, 0x3

    add-int/lit8 v4, v0, 0x2

    invoke-interface {v1, v2, v0, v3, v4}, Lmaps/al/b;->a(IIII)V

    iget-object v1, p0, Lmaps/l/c;->j:Lmaps/al/i;

    add-int/lit8 v2, v0, 0x1

    int-to-short v2, v2

    add-int/lit8 v0, v0, 0x3

    int-to-short v0, v0

    invoke-virtual {v1, v2, v0}, Lmaps/al/i;->a(SS)V

    iget-object v0, p0, Lmaps/l/c;->t:Lmaps/t/bx;

    invoke-static {p2, p1, v0}, Lmaps/t/bx;->b(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    iget-object v0, p0, Lmaps/l/c;->g:Lmaps/al/a;

    iget-object v1, p0, Lmaps/l/c;->t:Lmaps/t/bx;

    invoke-static {p6, v1}, Lmaps/l/c;->a(ILmaps/t/bx;)I

    move-result v1

    invoke-virtual {v0, v1, v5}, Lmaps/al/a;->b(II)V

    goto :goto_1
.end method

.method static a(Lmaps/t/l;Lmaps/l/ax;)Z
    .locals 4

    invoke-virtual {p0}, Lmaps/t/l;->c()Lmaps/t/ad;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/ad;->a()I

    move-result v0

    invoke-virtual {p0}, Lmaps/t/l;->d()I

    move-result v1

    mul-int/lit8 v0, v0, 0x3

    mul-int/lit8 v2, v1, 0x4

    iget v3, p1, Lmaps/l/ax;->a:I

    add-int/2addr v3, v0

    add-int/2addr v2, v3

    const/16 v3, 0x4000

    if-le v2, v3, :cond_0

    iget v3, p1, Lmaps/l/ax;->a:I

    if-lez v3, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iput v2, p1, Lmaps/l/ax;->a:I

    iget v2, p1, Lmaps/l/ax;->b:I

    add-int/2addr v0, v2

    iput v0, p1, Lmaps/l/ax;->b:I

    iget v0, p1, Lmaps/l/ax;->c:I

    mul-int/lit8 v2, v1, 0x6

    add-int/2addr v0, v2

    iput v0, p1, Lmaps/l/ax;->c:I

    iget v0, p1, Lmaps/l/ax;->d:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iput v0, p1, Lmaps/l/ax;->d:I

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private c()V
    .locals 4

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    iget-object v1, p0, Lmaps/l/c;->d:Lmaps/s/t;

    iget-object v0, p0, Lmaps/l/c;->k:Lmaps/al/b;

    check-cast v0, Lmaps/l/y;

    iget-object v0, v0, Lmaps/l/y;->a:Ljava/util/List;

    invoke-virtual {v1, v0}, Lmaps/s/t;->a(Ljava/util/List;)V

    iget-object v0, p0, Lmaps/l/c;->k:Lmaps/al/b;

    check-cast v0, Lmaps/l/y;

    iget-object v0, v0, Lmaps/l/y;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lmaps/l/c;->i:Lmaps/al/i;

    iget-object v0, p0, Lmaps/l/c;->k:Lmaps/al/b;

    check-cast v0, Lmaps/al/i;

    const/4 v2, 0x0

    iget-object v3, p0, Lmaps/l/c;->k:Lmaps/al/b;

    invoke-interface {v3}, Lmaps/al/b;->a()I

    move-result v3

    invoke-virtual {v1, v0, v2, v3}, Lmaps/al/i;->a(Lmaps/al/i;II)V

    const/4 v1, 0x0

    iget-object v0, p0, Lmaps/l/c;->k:Lmaps/al/b;

    check-cast v0, Lmaps/al/i;

    invoke-virtual {v0, v1}, Lmaps/al/i;->a(Lmaps/cr/c;)V

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 2

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/l/c;->e:Lmaps/s/r;

    invoke-virtual {v0}, Lmaps/s/r;->d()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/l/c;->f:Lmaps/al/o;

    invoke-virtual {v0}, Lmaps/al/o;->d()I

    move-result v0

    iget-object v1, p0, Lmaps/l/c;->g:Lmaps/al/a;

    invoke-virtual {v1}, Lmaps/al/a;->b()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/l/c;->i:Lmaps/al/i;

    invoke-virtual {v1}, Lmaps/al/i;->c()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/l/c;->j:Lmaps/al/i;

    invoke-virtual {v1}, Lmaps/al/i;->c()I

    move-result v1

    add-int/2addr v1, v0

    iget-object v0, p0, Lmaps/l/c;->k:Lmaps/al/b;

    check-cast v0, Lmaps/al/i;

    invoke-virtual {v0}, Lmaps/al/i;->c()I

    move-result v0

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public a(Lmaps/cr/c;)V
    .locals 1

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lmaps/l/c;->c(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/c;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lmaps/l/c;->f:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->e(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/c;->g:Lmaps/al/a;

    invoke-virtual {v0, p1}, Lmaps/al/a;->b(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/c;->i:Lmaps/al/i;

    invoke-virtual {v0, p1}, Lmaps/al/i;->c(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/c;->j:Lmaps/al/i;

    invoke-virtual {v0, p1}, Lmaps/al/i;->c(Lmaps/cr/c;)V

    goto :goto_0
.end method

.method public a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 5

    const/4 v4, 0x4

    const/high16 v1, 0x10000

    iget-object v0, p0, Lmaps/l/c;->f:Lmaps/al/o;

    invoke-virtual {v0}, Lmaps/al/o;->c()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/l/c;->f:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->d(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/c;->g:Lmaps/al/a;

    invoke-virtual {v0, p1}, Lmaps/al/a;->c(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/c;->h:Lmaps/w/u;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/l/c;->h:Lmaps/w/u;

    invoke-virtual {v0, p1}, Lmaps/w/u;->a(Lmaps/cr/c;)I

    move-result v0

    if-ne v0, v1, :cond_2

    const/4 v2, 0x0

    iput-object v2, p0, Lmaps/l/c;->h:Lmaps/w/u;

    :goto_1
    invoke-interface {p3}, Lmaps/af/s;->b()I

    move-result v2

    const/16 v3, 0xb

    if-ne v2, v3, :cond_4

    iget-object v0, p0, Lmaps/l/c;->i:Lmaps/al/i;

    invoke-virtual {v0, p1, v4}, Lmaps/al/i;->a(Lmaps/cr/c;I)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    invoke-interface {v2, v1, v1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glScalex(III)V

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    invoke-interface {p3}, Lmaps/af/s;->b()I

    move-result v2

    const/16 v3, 0xc

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lmaps/l/c;->i:Lmaps/al/i;

    invoke-virtual {v2, p1, v4}, Lmaps/al/i;->a(Lmaps/cr/c;I)V

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lmaps/l/c;->j:Lmaps/al/i;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lmaps/al/i;->a(Lmaps/cr/c;I)V

    goto :goto_0
.end method

.method a(Lmaps/t/ah;)V
    .locals 4

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lmaps/l/c;->d:Lmaps/s/t;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lmaps/s/t;->f(I)Lmaps/s/r;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/c;->e:Lmaps/s/r;

    iget-object v0, p0, Lmaps/l/c;->d:Lmaps/s/t;

    invoke-virtual {v0}, Lmaps/s/t;->h()V

    new-instance v0, Lmaps/q/aa;

    const/16 v1, 0xc

    invoke-direct {v0, v1, p1}, Lmaps/q/aa;-><init>(ILmaps/t/ah;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Building "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/q/aa;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lmaps/l/c;->e:Lmaps/s/r;

    invoke-virtual {v0, v1}, Lmaps/q/aa;->a(Lmaps/q/r;)V

    new-instance v1, Lmaps/q/bb;

    invoke-direct {v1}, Lmaps/q/bb;-><init>()V

    invoke-virtual {v0, v1}, Lmaps/q/aa;->a(Lmaps/q/z;)V

    new-instance v1, Lmaps/q/n;

    const/16 v2, 0x302

    const/16 v3, 0x303

    invoke-direct {v1, v2, v3}, Lmaps/q/n;-><init>(II)V

    invoke-virtual {v0, v1}, Lmaps/q/aa;->a(Lmaps/q/z;)V

    iget-object v1, p0, Lmaps/l/c;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmaps/q/aa;->b(I)V

    goto :goto_0
.end method

.method public b()I
    .locals 2

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/l/c;->e:Lmaps/s/r;

    invoke-virtual {v0}, Lmaps/s/r;->c()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/l/c;->f:Lmaps/al/o;

    invoke-virtual {v0}, Lmaps/al/o;->a()I

    move-result v0

    add-int/lit16 v0, v0, 0x160

    iget-object v1, p0, Lmaps/l/c;->g:Lmaps/al/a;

    invoke-virtual {v1}, Lmaps/al/a;->c()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/l/c;->i:Lmaps/al/i;

    invoke-virtual {v1}, Lmaps/al/i;->d()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/l/c;->j:Lmaps/al/i;

    invoke-virtual {v1}, Lmaps/al/i;->d()I

    move-result v1

    add-int/2addr v1, v0

    iget-object v0, p0, Lmaps/l/c;->k:Lmaps/al/b;

    check-cast v0, Lmaps/al/i;

    invoke-virtual {v0}, Lmaps/al/i;->d()I

    move-result v0

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public c(Lmaps/cr/c;)V
    .locals 3

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/l/c;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/q/aa;

    invoke-virtual {p1}, Lmaps/cr/c;->m()Lmaps/q/ad;

    move-result-object v2

    invoke-virtual {v2, v0}, Lmaps/q/ad;->b(Lmaps/q/ba;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmaps/l/c;->f:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->b(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/c;->g:Lmaps/al/a;

    invoke-virtual {v0, p1}, Lmaps/al/a;->a(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/c;->i:Lmaps/al/i;

    invoke-virtual {v0, p1}, Lmaps/al/i;->b(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/c;->j:Lmaps/al/i;

    invoke-virtual {v0, p1}, Lmaps/al/i;->b(Lmaps/cr/c;)V

    :cond_1
    return-void
.end method
