.class public Lmaps/l/m;
.super Lmaps/l/ak;


# static fields
.field private static final h:Ljava/util/Map;


# instance fields
.field private volatile a:Lmaps/cr/a;

.field private c:Lmaps/al/q;

.field private final d:[B

.field private e:Lmaps/w/v;

.field private f:Z

.field private g:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lmaps/l/m;->h:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>([BLjava/util/Set;Lmaps/t/ah;Lmaps/cr/c;)V
    .locals 4

    const/4 v3, 0x1

    invoke-direct {p0, p2}, Lmaps/l/ak;-><init>(Ljava/util/Set;)V

    iput-object p1, p0, Lmaps/l/m;->d:[B

    iput-boolean v3, p0, Lmaps/l/m;->f:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lmaps/l/m;->g:J

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    new-instance v0, Lmaps/q/aa;

    invoke-direct {v0, v3, p3}, Lmaps/q/aa;-><init>(ILmaps/t/ah;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Raster "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/q/aa;->a(Ljava/lang/String;)V

    invoke-virtual {p4}, Lmaps/cr/c;->n()Lmaps/s/s;

    move-result-object v1

    invoke-virtual {v1, v3}, Lmaps/s/s;->a(I)Lmaps/q/r;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/q/aa;->a(Lmaps/q/r;)V

    new-instance v1, Lmaps/q/au;

    new-instance v2, Lmaps/q/ab;

    invoke-virtual {p0, p4}, Lmaps/l/m;->b(Lmaps/cr/c;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-direct {v2, v3}, Lmaps/q/ab;-><init>(Landroid/graphics/Bitmap;)V

    invoke-direct {v1, v2}, Lmaps/q/au;-><init>(Lmaps/q/ab;)V

    invoke-virtual {v0, v1}, Lmaps/q/aa;->a(Lmaps/q/z;)V

    new-instance v1, Lmaps/q/ar;

    invoke-direct {v1}, Lmaps/q/ar;-><init>()V

    invoke-virtual {v0, v1}, Lmaps/q/aa;->a(Lmaps/q/z;)V

    iget-object v1, p0, Lmaps/l/m;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmaps/q/aa;->b(I)V

    :cond_0
    return-void
.end method

.method private static a(F)Lmaps/al/q;
    .locals 3

    const/4 v2, 0x0

    sget-object v0, Lmaps/l/m;->h:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lmaps/l/m;->h:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/al/q;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lmaps/al/q;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lmaps/al/q;-><init>(I)V

    const/high16 v1, 0x47800000

    mul-float/2addr v1, p0

    float-to-int v1, v1

    invoke-virtual {v0, v2, v2}, Lmaps/al/q;->a(II)V

    invoke-virtual {v0, v2, v1}, Lmaps/al/q;->a(II)V

    invoke-virtual {v0, v1, v2}, Lmaps/al/q;->a(II)V

    invoke-virtual {v0, v1, v1}, Lmaps/al/q;->a(II)V

    sget-object v1, Lmaps/l/m;->h:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static a(Lmaps/t/ah;[Ljava/lang/String;Lmaps/t/h;Lmaps/cr/c;)Lmaps/l/m;
    .locals 7

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-interface {p2}, Lmaps/t/h;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/ae;

    invoke-virtual {v0}, Lmaps/t/ae;->j()[I

    move-result-object v3

    array-length v4, v3

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v4, :cond_1

    aget v5, v3, v1

    if-ltz v5, :cond_0

    array-length v6, p1

    if-ge v5, v6, :cond_0

    aget-object v5, p1, v5

    invoke-virtual {v2, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    new-instance v1, Lmaps/l/m;

    invoke-virtual {v0}, Lmaps/t/ae;->c()[B

    move-result-object v0

    invoke-direct {v1, v0, v2, p0, p3}, Lmaps/l/m;-><init>([BLjava/util/Set;Lmaps/t/ah;Lmaps/cr/c;)V

    return-object v1
.end method

.method public static a([BLmaps/t/ah;Lmaps/cr/c;)Lmaps/l/m;
    .locals 2

    new-instance v0, Lmaps/l/m;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-direct {v0, p0, v1, p1, p2}, Lmaps/l/m;-><init>([BLjava/util/Set;Lmaps/t/ah;Lmaps/cr/c;)V

    return-object v0
.end method

.method public static a(Lmaps/cr/c;Lmaps/af/q;)V
    .locals 4

    invoke-virtual {p0}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/4 v1, 0x1

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x2100

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    invoke-virtual {p0}, Lmaps/cr/c;->r()V

    iget-object v0, p0, Lmaps/cr/c;->f:Lmaps/al/o;

    invoke-virtual {v0, p0}, Lmaps/al/o;->d(Lmaps/cr/c;)V

    return-void
.end method

.method private a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/cr/a;)V
    .locals 4

    const/high16 v1, 0x10000

    const/4 v3, 0x0

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    iget-object v0, p0, Lmaps/l/m;->c:Lmaps/al/q;

    invoke-virtual {v0, p1}, Lmaps/al/q;->d(Lmaps/cr/c;)V

    invoke-virtual {p3, v2}, Lmaps/cr/a;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    iget-object v0, p0, Lmaps/l/m;->e:Lmaps/w/v;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/l/m;->e:Lmaps/w/v;

    invoke-virtual {v0, p1}, Lmaps/w/v;->a(Lmaps/cr/c;)I

    move-result v0

    if-ne v0, v1, :cond_0

    const/4 v1, 0x0

    iput-object v1, p0, Lmaps/l/m;->e:Lmaps/w/v;

    iput-boolean v3, p0, Lmaps/l/m;->f:Z

    :cond_0
    :goto_0
    invoke-interface {v2, v0, v0, v0, v0}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    const/4 v0, 0x5

    const/4 v1, 0x4

    invoke-interface {v2, v0, v3, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/l/m;->a:Lmaps/cr/a;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lmaps/cr/a;->i()I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    return v0
.end method

.method public a(J)V
    .locals 0

    iput-wide p1, p0, Lmaps/l/m;->g:J

    return-void
.end method

.method public a(Lmaps/cr/c;)V
    .locals 1

    invoke-virtual {p0, p1}, Lmaps/l/m;->c(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/m;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method public a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 8

    const/16 v2, 0x4e20

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v6, p0, Lmaps/l/m;->a:Lmaps/cr/a;

    if-nez v6, :cond_3

    const/4 v0, 0x0

    iget-boolean v1, p0, Lmaps/l/m;->f:Z

    if-nez v1, :cond_2

    invoke-virtual {p1, v2}, Lmaps/cr/c;->b(I)V

    invoke-virtual {p0, p1}, Lmaps/l/m;->b(Lmaps/cr/c;)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v1, v0

    :goto_1
    if-eqz v1, :cond_3

    new-instance v0, Lmaps/cr/a;

    invoke-direct {v0, p1}, Lmaps/cr/a;-><init>(Lmaps/cr/c;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lmaps/cr/a;->c(Z)V

    invoke-virtual {v0, v1}, Lmaps/cr/a;->b(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lmaps/l/m;->a:Lmaps/cr/a;

    invoke-virtual {v0}, Lmaps/cr/a;->b()F

    move-result v2

    invoke-static {v2}, Lmaps/l/m;->a(F)Lmaps/al/q;

    move-result-object v2

    iput-object v2, p0, Lmaps/l/m;->c:Lmaps/al/q;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    :goto_2
    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2, v0}, Lmaps/l/m;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/cr/a;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1, v2}, Lmaps/cr/c;->a(I)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p0, p1}, Lmaps/l/m;->b(Lmaps/cr/c;)Landroid/graphics/Bitmap;

    move-result-object v7

    new-instance v0, Lmaps/w/v;

    iget-wide v1, p0, Lmaps/l/m;->g:J

    const-wide/16 v3, 0xfa

    sget-object v5, Lmaps/w/j;->a:Lmaps/w/j;

    invoke-direct/range {v0 .. v5}, Lmaps/w/v;-><init>(JJLmaps/w/j;)V

    iput-object v0, p0, Lmaps/l/m;->e:Lmaps/w/v;

    move-object v1, v7

    goto :goto_1

    :cond_3
    move-object v0, v6

    goto :goto_2

    :cond_4
    move-object v1, v0

    goto :goto_1
.end method

.method public b()I
    .locals 2

    const/16 v0, 0x60

    iget-object v1, p0, Lmaps/l/m;->d:[B

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/l/m;->d:[B

    array-length v1, v1

    add-int/lit8 v1, v1, 0x10

    add-int/2addr v0, v1

    :cond_0
    return v0
.end method

.method b(Lmaps/cr/c;)Landroid/graphics/Bitmap;
    .locals 3

    const/4 v1, 0x1

    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inInputShareable:Z

    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    :try_start_0
    invoke-virtual {p1}, Lmaps/cr/c;->l()Lmaps/s/h;

    move-result-object v1

    iget-object v2, p0, Lmaps/l/m;->d:[B

    invoke-virtual {v1, v2, v0}, Lmaps/s/h;->a([BLandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Lmaps/cr/c;)V
    .locals 3

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lmaps/cr/c;->m()Lmaps/q/ad;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lmaps/l/m;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/q/aa;

    invoke-virtual {v1, v0}, Lmaps/q/ad;->b(Lmaps/q/ba;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmaps/l/m;->a:Lmaps/cr/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/l/m;->a:Lmaps/cr/a;

    invoke-virtual {v0}, Lmaps/cr/a;->g()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/l/m;->a:Lmaps/cr/a;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/l/m;->f:Z

    :cond_1
    return-void
.end method

.method public c()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/l/m;->f:Z

    return v0
.end method

.method public d()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/l/m;->f:Z

    return-void
.end method
