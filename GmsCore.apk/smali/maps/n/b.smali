.class public Lmaps/n/b;
.super Landroid/widget/ListView;

# interfaces
.implements Lmaps/b/y;
.implements Lmaps/e/z;


# instance fields
.field private a:I

.field private b:Lmaps/t/e;

.field private c:Lmaps/b/r;

.field private d:I

.field private volatile e:Lmaps/t/bb;

.field private f:Lmaps/n/j;

.field private final g:Ljava/util/Set;

.field private final h:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lmaps/n/b;-><init>(Landroid/content/Context;Landroid/content/res/Resources;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lmaps/n/b;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/content/res/Resources;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lmaps/n/b;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/content/res/Resources;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/content/res/Resources;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput v0, p0, Lmaps/n/b;->a:I

    iput v0, p0, Lmaps/n/b;->d:I

    invoke-static {}, Lmaps/f/a;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lmaps/n/b;->g:Ljava/util/Set;

    iput-object p3, p0, Lmaps/n/b;->h:Landroid/content/res/Resources;

    return-void
.end method

.method static synthetic a(Lmaps/n/b;)Landroid/content/res/Resources;
    .locals 1

    iget-object v0, p0, Lmaps/n/b;->h:Landroid/content/res/Resources;

    return-object v0
.end method

.method private a()V
    .locals 2

    iget v0, p0, Lmaps/n/b;->a:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lmaps/n/b;->a:I

    invoke-virtual {p0, v0}, Lmaps/n/b;->smoothScrollToPosition(I)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lmaps/n/b;Lmaps/t/e;Lmaps/t/bb;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmaps/n/b;->a(Lmaps/t/e;Lmaps/t/bb;)V

    return-void
.end method

.method private static a(Lmaps/t/bi;Lmaps/t/e;)V
    .locals 9

    if-nez p0, :cond_0

    const-string v0, "none"

    move-object v1, v0

    :goto_0
    if-nez p1, :cond_1

    const-string v0, "none"

    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "?sa=T&oi=m_map:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lmaps/bh/e;->c:Lmaps/bh/e;

    invoke-virtual {v3}, Lmaps/bh/e;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x68

    const-string v4, "s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "l="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v6

    const/4 v1, 0x1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "b="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v1

    const/4 v1, 0x2

    if-nez v2, :cond_2

    const/4 v0, 0x0

    :goto_2
    aput-object v0, v5, v1

    invoke-static {v5}, Lmaps/bh/k;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v4, v0}, Lmaps/bh/k;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-virtual {p0}, Lmaps/t/bi;->b()Lmaps/t/bg;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/bg;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lmaps/t/e;->a()Lmaps/t/bg;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/bg;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "u="

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method private a(Lmaps/t/e;Lmaps/t/bb;)V
    .locals 1

    sget-boolean v0, Lmaps/ae/h;->k:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ae/c;->q()V

    :cond_0
    invoke-virtual {p0, p1}, Lmaps/n/b;->a(Lmaps/t/e;)V

    iget-object v0, p0, Lmaps/n/b;->b:Lmaps/t/e;

    if-nez v0, :cond_1

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/n/b;->b:Lmaps/t/e;

    invoke-static {v0, p2}, Lmaps/n/b;->b(Lmaps/t/e;Lmaps/t/bb;)I

    move-result v0

    invoke-virtual {p0, v0}, Lmaps/n/b;->a(I)V

    invoke-direct {p0}, Lmaps/n/b;->a()V

    goto :goto_0
.end method

.method static synthetic a(Lmaps/t/e;Lmaps/t/e;)Z
    .locals 1

    invoke-static {p0, p1}, Lmaps/n/b;->b(Lmaps/t/e;Lmaps/t/e;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lmaps/n/b;)I
    .locals 1

    iget v0, p0, Lmaps/n/b;->a:I

    return v0
.end method

.method private static b(Lmaps/t/e;Lmaps/t/bb;)I
    .locals 3

    const/4 v0, -0x1

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-nez p1, :cond_3

    invoke-virtual {p0}, Lmaps/t/e;->d()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x0

    :cond_2
    :goto_1
    if-ltz v1, :cond_0

    move v0, v1

    goto :goto_0

    :cond_3
    invoke-virtual {p0, p1}, Lmaps/t/e;->b(Lmaps/t/bb;)I

    move-result v1

    if-ltz v1, :cond_2

    invoke-virtual {p0}, Lmaps/t/e;->d()Z

    move-result v2

    if-eqz v2, :cond_2

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    move v1, v0

    goto :goto_1
.end method

.method private b()V
    .locals 2

    sget-boolean v0, Lmaps/ae/h;->k:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ae/c;->q()V

    :cond_0
    iget-object v0, p0, Lmaps/n/b;->b:Lmaps/t/e;

    if-nez v0, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/4 v0, -0x1

    iget-object v1, p0, Lmaps/n/b;->e:Lmaps/t/bb;

    if-eqz v1, :cond_3

    iget-object v0, p0, Lmaps/n/b;->b:Lmaps/t/e;

    iget-object v1, p0, Lmaps/n/b;->e:Lmaps/t/bb;

    invoke-static {v0, v1}, Lmaps/n/b;->b(Lmaps/t/e;Lmaps/t/bb;)I

    move-result v0

    :cond_3
    iget v1, p0, Lmaps/n/b;->d:I

    if-eq v0, v1, :cond_1

    iput v0, p0, Lmaps/n/b;->d:I

    iget-object v0, p0, Lmaps/n/b;->f:Lmaps/n/j;

    invoke-virtual {v0}, Lmaps/n/j;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method private static b(Lmaps/t/e;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez p0, :cond_0

    :goto_0
    return v2

    :cond_0
    invoke-virtual {p0}, Lmaps/t/e;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p0}, Lmaps/t/e;->b()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lt v3, v0, :cond_2

    :goto_2
    move v2, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method

.method private static b(Lmaps/t/e;Lmaps/t/e;)Z
    .locals 2

    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    if-eqz p0, :cond_1

    if-nez p1, :cond_2

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lmaps/t/e;->a()Lmaps/t/bg;

    move-result-object v0

    invoke-virtual {p1}, Lmaps/t/e;->a()Lmaps/t/bg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/t/bg;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method static synthetic c(Lmaps/n/b;)I
    .locals 1

    iget v0, p0, Lmaps/n/b;->d:I

    return v0
.end method

.method private c(Lmaps/t/e;Lmaps/t/bb;)V
    .locals 9

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "?sa=T&oi=m_map:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lmaps/bh/e;->c:Lmaps/bh/e;

    invoke-virtual {v1}, Lmaps/bh/e;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    if-nez p2, :cond_0

    const-string v0, "0"

    :goto_0
    invoke-static {p1}, Lmaps/n/b;->b(Lmaps/t/e;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "1"

    :goto_1
    const/16 v3, 0x68

    const-string v4, "f"

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "b="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Lmaps/t/e;->a()Lmaps/t/bg;

    move-result-object v8

    invoke-virtual {v8}, Lmaps/t/bg;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "p="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v6

    const/4 v1, 0x2

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "v="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v1

    const/4 v1, 0x3

    if-nez v2, :cond_2

    const/4 v0, 0x0

    :goto_2
    aput-object v0, v5, v1

    invoke-static {v5}, Lmaps/bh/k;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v4, v0}, Lmaps/bh/k;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    const-string v0, "1"

    goto :goto_0

    :cond_1
    const-string v1, "0"

    goto :goto_1

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "u="

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method static synthetic d(Lmaps/n/b;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lmaps/n/b;->g:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic e(Lmaps/n/b;)V
    .locals 0

    invoke-direct {p0}, Lmaps/n/b;->a()V

    return-void
.end method

.method static synthetic f(Lmaps/n/b;)Lmaps/t/e;
    .locals 1

    iget-object v0, p0, Lmaps/n/b;->b:Lmaps/t/e;

    return-object v0
.end method


# virtual methods
.method public a(I)V
    .locals 3

    iget v0, p0, Lmaps/n/b;->a:I

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput p1, p0, Lmaps/n/b;->a:I

    iget-object v0, p0, Lmaps/n/b;->f:Lmaps/n/j;

    invoke-virtual {v0}, Lmaps/n/j;->notifyDataSetChanged()V

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    invoke-virtual {p0, p1}, Lmaps/n/b;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/n/c;

    if-nez v0, :cond_2

    sget-boolean v0, Lmaps/ae/h;->f:Z

    if-eqz v0, :cond_0

    const-string v0, "INDOOR"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getItemAtPosition("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") returned null"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lmaps/n/c;->a()Lmaps/t/bi;

    move-result-object v0

    iget-object v1, p0, Lmaps/n/b;->b:Lmaps/t/e;

    invoke-static {v0, v1}, Lmaps/n/b;->a(Lmaps/t/bi;Lmaps/t/e;)V

    iget-object v1, p0, Lmaps/n/b;->c:Lmaps/b/r;

    if-eqz v1, :cond_0

    if-nez v0, :cond_3

    iget-object v0, p0, Lmaps/n/b;->c:Lmaps/b/r;

    iget-object v1, p0, Lmaps/n/b;->b:Lmaps/t/e;

    invoke-virtual {v0, v1}, Lmaps/b/r;->a(Lmaps/t/e;)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lmaps/n/b;->c:Lmaps/b/r;

    invoke-virtual {v0}, Lmaps/t/bi;->a()Lmaps/t/bb;

    move-result-object v0

    invoke-virtual {v1, v0}, Lmaps/b/r;->a(Lmaps/t/bb;)V

    goto :goto_0
.end method

.method public a(ILmaps/e/v;)V
    .locals 0

    return-void
.end method

.method public a(Lmaps/b/r;)V
    .locals 1

    new-instance v0, Lmaps/n/g;

    invoke-direct {v0, p0, p1}, Lmaps/n/g;-><init>(Lmaps/n/b;Lmaps/b/r;)V

    invoke-virtual {p0, v0}, Lmaps/n/b;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public a(Lmaps/b/r;Lmaps/t/e;)V
    .locals 1

    new-instance v0, Lmaps/n/f;

    invoke-direct {v0, p0, p1, p2}, Lmaps/n/f;-><init>(Lmaps/n/b;Lmaps/b/r;Lmaps/t/e;)V

    invoke-virtual {p0, v0}, Lmaps/n/b;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public a(Lmaps/bl/a;Lmaps/e/v;)V
    .locals 2

    invoke-interface {p2}, Lmaps/e/v;->g()Lmaps/e/a;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Lmaps/e/v;->g()Lmaps/e/a;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/e/a;->b()Lmaps/t/bb;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lmaps/n/b;->e:Lmaps/t/bb;

    invoke-static {v1, v0}, Lmaps/ap/e;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lmaps/n/i;

    invoke-direct {v1, p0, v0}, Lmaps/n/i;-><init>(Lmaps/n/b;Lmaps/t/bb;)V

    invoke-virtual {p0, v1}, Lmaps/n/b;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method a(Lmaps/t/bb;)V
    .locals 0

    iput-object p1, p0, Lmaps/n/b;->e:Lmaps/t/bb;

    invoke-direct {p0}, Lmaps/n/b;->b()V

    return-void
.end method

.method a(Lmaps/t/e;)V
    .locals 7

    const-wide/16 v5, 0x1f4

    const/4 v1, -0x1

    const/high16 v4, 0x3f800000

    const/4 v3, 0x0

    iget-object v0, p0, Lmaps/n/b;->b:Lmaps/t/e;

    invoke-static {p1, v0}, Lmaps/n/b;->b(Lmaps/t/e;Lmaps/t/e;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lmaps/n/b;->clearAnimation()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/n/b;->b:Lmaps/t/e;

    iput v1, p0, Lmaps/n/b;->d:I

    iput v1, p0, Lmaps/n/b;->a:I

    if-eqz p1, :cond_2

    invoke-static {p1}, Lmaps/n/b;->b(Lmaps/t/e;)Z

    move-result v0

    if-eqz v0, :cond_2

    iput-object p1, p0, Lmaps/n/b;->b:Lmaps/t/e;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lmaps/n/b;->setVisibility(I)V

    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v3, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    invoke-virtual {v0, v5, v6}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    new-instance v1, Lmaps/n/e;

    invoke-direct {v1, p0}, Lmaps/n/e;-><init>(Lmaps/n/b;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-virtual {p0, v0}, Lmaps/n/b;->startAnimation(Landroid/view/animation/Animation;)V

    new-instance v0, Lmaps/n/j;

    invoke-virtual {p0}, Lmaps/n/b;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lmaps/n/b;->b:Lmaps/t/e;

    invoke-direct {v0, p0, v1, v2}, Lmaps/n/j;-><init>(Lmaps/n/b;Landroid/content/Context;Lmaps/t/e;)V

    iput-object v0, p0, Lmaps/n/b;->f:Lmaps/n/j;

    iget-object v0, p0, Lmaps/n/b;->f:Lmaps/n/j;

    invoke-virtual {p0, v0}, Lmaps/n/b;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-direct {p0}, Lmaps/n/b;->b()V

    :cond_2
    iget-object v0, p0, Lmaps/n/b;->b:Lmaps/t/e;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lmaps/n/b;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lmaps/n/b;->setVisibility(I)V

    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v4, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    invoke-virtual {v0, v5, v6}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    new-instance v1, Lmaps/n/d;

    invoke-direct {v1, p0}, Lmaps/n/d;-><init>(Lmaps/n/b;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-virtual {p0, v0}, Lmaps/n/b;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public b(Lmaps/b/r;)V
    .locals 0

    return-void
.end method

.method public c(Lmaps/b/r;)V
    .locals 1

    iget-object v0, p0, Lmaps/n/b;->c:Lmaps/b/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/n/b;->c:Lmaps/b/r;

    invoke-virtual {v0, p0}, Lmaps/b/r;->b(Lmaps/b/y;)V

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0, p1}, Lmaps/n/b;->a(Lmaps/b/r;)V

    invoke-virtual {p1, p0}, Lmaps/b/r;->a(Lmaps/b/y;)V

    :cond_1
    iput-object p1, p0, Lmaps/n/b;->c:Lmaps/b/r;

    return-void
.end method

.method d(Lmaps/b/r;)V
    .locals 5

    invoke-virtual {p1}, Lmaps/b/r;->c()Lmaps/t/e;

    move-result-object v1

    const/4 v0, 0x0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lmaps/t/e;->a()Lmaps/t/bg;

    move-result-object v2

    invoke-virtual {p1, v2}, Lmaps/b/r;->b(Lmaps/t/bg;)Lmaps/t/cu;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lmaps/t/cu;->c()Lmaps/t/bb;

    move-result-object v0

    :cond_0
    invoke-direct {p0, v1, v0}, Lmaps/n/b;->c(Lmaps/t/e;Lmaps/t/bb;)V

    :cond_1
    sget-boolean v2, Lmaps/ae/h;->f:Z

    if-eqz v2, :cond_2

    const-string v2, "INDOOR"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onIndoorBuildingFocused: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-direct {p0, v1, v0}, Lmaps/n/b;->a(Lmaps/t/e;Lmaps/t/bb;)V

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ListView;->onSizeChanged(IIII)V

    new-instance v0, Lmaps/n/h;

    invoke-direct {v0, p0}, Lmaps/n/h;-><init>(Lmaps/n/b;)V

    invoke-virtual {p0, v0}, Lmaps/n/b;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
