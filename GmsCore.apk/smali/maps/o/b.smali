.class abstract Lmaps/o/b;
.super Ljava/lang/Object;


# instance fields
.field private final a:I

.field private b:I

.field private c:Ljava/lang/String;

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Z


# direct methods
.method private constructor <init>(I)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lmaps/o/b;->b:I

    const-string v0, ""

    iput-object v0, p0, Lmaps/o/b;->c:Ljava/lang/String;

    iput-boolean v1, p0, Lmaps/o/b;->d:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/o/b;->e:Z

    iput-boolean v1, p0, Lmaps/o/b;->f:Z

    iput-boolean v1, p0, Lmaps/o/b;->g:Z

    iput p1, p0, Lmaps/o/b;->a:I

    return-void
.end method

.method synthetic constructor <init>(ILmaps/o/r;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/o/b;-><init>(I)V

    return-void
.end method

.method static synthetic a(Lmaps/o/b;)I
    .locals 1

    iget v0, p0, Lmaps/o/b;->a:I

    return v0
.end method

.method static synthetic b(Lmaps/o/b;)I
    .locals 1

    iget v0, p0, Lmaps/o/b;->b:I

    return v0
.end method

.method static synthetic c(Lmaps/o/b;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/o/b;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lmaps/o/b;)Z
    .locals 1

    iget-boolean v0, p0, Lmaps/o/b;->d:Z

    return v0
.end method

.method static synthetic e(Lmaps/o/b;)Z
    .locals 1

    iget-boolean v0, p0, Lmaps/o/b;->e:Z

    return v0
.end method

.method static synthetic f(Lmaps/o/b;)Z
    .locals 1

    iget-boolean v0, p0, Lmaps/o/b;->f:Z

    return v0
.end method

.method static synthetic g(Lmaps/o/b;)Z
    .locals 1

    iget-boolean v0, p0, Lmaps/o/b;->g:Z

    return v0
.end method


# virtual methods
.method a(I)Lmaps/o/b;
    .locals 0

    iput p1, p0, Lmaps/o/b;->b:I

    return-object p0
.end method

.method a(Ljava/lang/String;)Lmaps/o/b;
    .locals 0

    iput-object p1, p0, Lmaps/o/b;->c:Ljava/lang/String;

    return-object p0
.end method

.method a(Z)Lmaps/o/b;
    .locals 0

    iput-boolean p1, p0, Lmaps/o/b;->d:Z

    return-object p0
.end method

.method abstract a()Lmaps/o/c;
.end method

.method b(Z)Lmaps/o/b;
    .locals 0

    iput-boolean p1, p0, Lmaps/o/b;->e:Z

    return-object p0
.end method

.method c(Z)Lmaps/o/b;
    .locals 0

    iput-boolean p1, p0, Lmaps/o/b;->f:Z

    return-object p0
.end method

.method d(Z)Lmaps/o/b;
    .locals 0

    iput-boolean p1, p0, Lmaps/o/b;->g:Z

    return-object p0
.end method
