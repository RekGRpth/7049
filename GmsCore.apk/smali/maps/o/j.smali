.class Lmaps/o/j;
.super Lmaps/o/c;


# direct methods
.method private constructor <init>(Lmaps/o/f;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmaps/o/c;-><init>(Lmaps/o/b;Lmaps/o/r;)V

    return-void
.end method

.method synthetic constructor <init>(Lmaps/o/f;Lmaps/o/r;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/o/j;-><init>(Lmaps/o/f;)V

    return-void
.end method


# virtual methods
.method a()I
    .locals 1

    const/16 v0, 0x800

    return v0
.end method

.method public a(Lmaps/ak/a;Landroid/content/Context;Landroid/content/res/Resources;Ljava/util/Locale;Ljava/io/File;ZZ)Lmaps/an/y;
    .locals 9

    invoke-virtual {p3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-static {v0}, Lmaps/o/c;->b(I)I

    move-result v3

    sget-object v0, Lmaps/o/c;->d:Lmaps/o/c;

    if-ne p0, v0, :cond_0

    const/16 v0, 0x100

    invoke-static {p3, v0}, Lmaps/y/ab;->b(Landroid/content/res/Resources;I)I

    move-result v4

    :goto_0
    invoke-static {p0}, Lmaps/o/c;->b(Lmaps/o/c;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v5, v0, Landroid/util/DisplayMetrics;->density:F

    :goto_1
    new-instance v0, Lmaps/an/n;

    const/4 v8, 0x0

    move-object v1, p1

    move-object v2, p0

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v0 .. v8}, Lmaps/an/n;-><init>(Lmaps/ak/a;Lmaps/o/c;IIFLjava/util/Locale;Ljava/io/File;Lmaps/ag/g;)V

    return-object v0

    :cond_0
    const/16 v0, 0x100

    invoke-static {p3, v0}, Lmaps/y/ab;->a(Landroid/content/res/Resources;I)I

    move-result v4

    goto :goto_0

    :cond_1
    const/high16 v5, 0x3f800000

    goto :goto_1
.end method

.method public a(Lmaps/t/ax;)Lmaps/t/bx;
    .locals 1

    invoke-virtual {p1}, Lmaps/t/ax;->d()Lmaps/t/bx;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 3

    const/4 v2, 0x0

    const/high16 v0, 0x3f800000

    const v1, 0x3e99999a

    invoke-interface {p1, v2, v2, v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glColor4f(FFFF)V

    return-void
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lmaps/o/c;

    invoke-super {p0, p1}, Lmaps/o/c;->a(Lmaps/o/c;)I

    move-result v0

    return v0
.end method

.method public h()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public i()Lmaps/l/h;
    .locals 3

    new-instance v0, Lmaps/l/h;

    sget-object v1, Lmaps/l/af;->h:Lmaps/l/af;

    sget-object v2, Lmaps/l/w;->b:Lmaps/l/w;

    invoke-direct {v0, v1, v2}, Lmaps/l/h;-><init>(Lmaps/l/af;Lmaps/l/w;)V

    return-object v0
.end method

.method public j()Lmaps/t/aa;
    .locals 1

    invoke-static {}, Lmaps/o/c;->o()Lmaps/t/aa;

    move-result-object v0

    return-object v0
.end method

.method public k()Lmaps/ag/e;
    .locals 1

    new-instance v0, Lmaps/o/n;

    invoke-direct {v0, p0}, Lmaps/o/n;-><init>(Lmaps/o/c;)V

    return-object v0
.end method
