.class Lmaps/p/ai;
.super Lmaps/be/l;


# instance fields
.field final synthetic a:Lmaps/p/au;

.field private volatile b:Z

.field private volatile c:I

.field private volatile d:Z

.field private e:Z

.field private f:J


# direct methods
.method public constructor <init>(Lmaps/p/au;)V
    .locals 2

    const/4 v1, 0x0

    iput-object p1, p0, Lmaps/p/ai;->a:Lmaps/p/au;

    const-string v0, "RenderDrive"

    invoke-direct {p0, v0}, Lmaps/be/l;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/p/ai;->b:Z

    sget v0, Lmaps/p/au;->a:I

    iput v0, p0, Lmaps/p/ai;->c:I

    iput-boolean v1, p0, Lmaps/p/ai;->d:Z

    iput-boolean v1, p0, Lmaps/p/ai;->e:Z

    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lmaps/p/ai;->f:J

    return-void
.end method

.method static synthetic a(Lmaps/p/ai;)Z
    .locals 1

    iget-boolean v0, p0, Lmaps/p/ai;->e:Z

    return v0
.end method


# virtual methods
.method public a(I)V
    .locals 1

    iget v0, p0, Lmaps/p/ai;->c:I

    if-eq v0, p1, :cond_0

    const/16 v0, 0xf

    if-le p1, v0, :cond_0

    iput p1, p0, Lmaps/p/ai;->c:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/p/ai;->d:Z

    invoke-virtual {p0}, Lmaps/p/ai;->interrupt()V

    :cond_0
    return-void
.end method

.method public declared-synchronized a(J)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-wide p1, p0, Lmaps/p/ai;->f:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/p/ai;->a:Lmaps/p/au;

    iget-object v0, v0, Lmaps/p/au;->b:Lmaps/p/am;

    invoke-interface {v0}, Lmaps/p/am;->j_()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/p/ai;->b:Z

    invoke-virtual {p0}, Lmaps/p/ai;->interrupt()V

    return-void
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lmaps/p/ai;->c:I

    return v0
.end method

.method public e()V
    .locals 0

    invoke-virtual {p0}, Lmaps/p/ai;->f()V

    invoke-virtual {p0}, Lmaps/p/ai;->interrupt()V

    return-void
.end method

.method public declared-synchronized f()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lmaps/p/ai;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized g()J
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lmaps/p/ai;->f:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public n_()V
    .locals 4

    :goto_0
    iget-boolean v0, p0, Lmaps/p/ai;->b:Z

    if-eqz v0, :cond_3

    :cond_0
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lmaps/p/ai;->d:Z

    iget v0, p0, Lmaps/p/ai;->c:I

    int-to-long v0, v0

    invoke-static {v0, v1}, Lmaps/p/ai;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-boolean v0, p0, Lmaps/p/ai;->d:Z

    if-nez v0, :cond_0

    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lmaps/p/ai;->e:Z

    if-nez v0, :cond_1

    iget-wide v0, p0, Lmaps/p/ai;->f:J

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gtz v0, :cond_2

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/p/ai;->e:Z

    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lmaps/p/ai;->f:J

    iget-object v0, p0, Lmaps/p/ai;->a:Lmaps/p/au;

    iget-object v0, v0, Lmaps/p/au;->b:Lmaps/p/am;

    invoke-interface {v0}, Lmaps/p/am;->j_()V

    :cond_2
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_3
    return-void

    :catch_0
    move-exception v0

    goto :goto_1
.end method
