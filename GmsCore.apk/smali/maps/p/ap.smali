.class public final Lmaps/p/ap;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lmaps/p/ap;


# instance fields
.field private final b:I

.field private final c:[I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:[I

.field private final i:[I

.field private final j:[F

.field private k:Ljava/util/TreeSet;

.field private final l:[F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lmaps/p/ap;

    const/16 v1, 0x15

    invoke-direct {v0, v1}, Lmaps/p/ap;-><init>(I)V

    sput-object v0, Lmaps/p/ap;->a:Lmaps/p/ap;

    return-void
.end method

.method private constructor <init>(I)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v0, v1, [I

    iput-object v0, p0, Lmaps/p/ap;->c:[I

    const/4 v0, 0x1

    iput v0, p0, Lmaps/p/ap;->b:I

    iput p1, p0, Lmaps/p/ap;->g:I

    const/4 v0, -0x1

    iput v0, p0, Lmaps/p/ap;->e:I

    iput p1, p0, Lmaps/p/ap;->f:I

    add-int/lit8 v0, p1, 0x1

    iput v0, p0, Lmaps/p/ap;->d:I

    new-array v0, v1, [F

    iput-object v0, p0, Lmaps/p/ap;->j:[F

    new-array v0, v1, [F

    iput-object v0, p0, Lmaps/p/ap;->l:[F

    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    iput-object v0, p0, Lmaps/p/ap;->k:Ljava/util/TreeSet;

    new-array v0, v1, [I

    iput-object v0, p0, Lmaps/p/ap;->h:[I

    new-array v0, v1, [I

    iput-object v0, p0, Lmaps/p/ap;->i:[I

    return-void
.end method

.method public constructor <init>([IIII)V
    .locals 9

    const/4 v0, 0x0

    const/high16 v4, -0x40800000

    const/4 v3, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/p/ap;->c:[I

    iput p3, p0, Lmaps/p/ap;->b:I

    const/high16 v1, 0x3f800000

    int-to-float v2, p3

    div-float v5, v1, v2

    iput p4, p0, Lmaps/p/ap;->g:I

    iget-object v1, p0, Lmaps/p/ap;->c:[I

    array-length v6, v1

    iget-object v1, p0, Lmaps/p/ap;->c:[I

    aget v1, v1, v0

    iput v1, p0, Lmaps/p/ap;->e:I

    iget-object v1, p0, Lmaps/p/ap;->c:[I

    add-int/lit8 v2, v6, -0x1

    aget v1, v1, v2

    iput v1, p0, Lmaps/p/ap;->f:I

    iput p2, p0, Lmaps/p/ap;->d:I

    iget v1, p0, Lmaps/p/ap;->f:I

    add-int/lit8 v1, v1, 0x1

    new-array v1, v1, [F

    iput-object v1, p0, Lmaps/p/ap;->j:[F

    iget v1, p0, Lmaps/p/ap;->f:I

    add-int/lit8 v1, v1, 0x1

    new-array v1, v1, [F

    iput-object v1, p0, Lmaps/p/ap;->l:[F

    iget-object v1, p0, Lmaps/p/ap;->j:[F

    invoke-static {v1, v4}, Ljava/util/Arrays;->fill([FF)V

    iget-object v1, p0, Lmaps/p/ap;->l:[F

    invoke-static {v1, v4}, Ljava/util/Arrays;->fill([FF)V

    new-instance v1, Ljava/util/TreeSet;

    invoke-direct {v1}, Ljava/util/TreeSet;-><init>()V

    iput-object v1, p0, Lmaps/p/ap;->k:Ljava/util/TreeSet;

    iget v1, p0, Lmaps/p/ap;->d:I

    iget v2, p0, Lmaps/p/ap;->d:I

    int-to-float v2, v2

    move v4, v0

    move v0, v1

    :goto_0
    if-ge v4, v6, :cond_1

    iget-object v1, p0, Lmaps/p/ap;->k:Ljava/util/TreeSet;

    iget-object v7, p0, Lmaps/p/ap;->c:[I

    aget v7, v7, v4

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lmaps/p/ap;->j:[F

    iget-object v7, p0, Lmaps/p/ap;->c:[I

    aget v7, v7, v4

    aget v1, v1, v7

    const/4 v7, 0x0

    cmpg-float v1, v1, v7

    if-gez v1, :cond_4

    int-to-float v1, v4

    mul-float/2addr v1, v5

    iget v7, p0, Lmaps/p/ap;->d:I

    int-to-float v7, v7

    add-float/2addr v1, v7

    :goto_1
    iget-object v7, p0, Lmaps/p/ap;->c:[I

    aget v7, v7, v4

    if-ge v0, v7, :cond_0

    iget-object v7, p0, Lmaps/p/ap;->j:[F

    aput v2, v7, v0

    iget-object v7, p0, Lmaps/p/ap;->l:[F

    aput v1, v7, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    iget-object v2, p0, Lmaps/p/ap;->j:[F

    aput v1, v2, v0

    move v8, v1

    move v1, v0

    move v0, v8

    :goto_2
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v0

    move v0, v1

    goto :goto_0

    :cond_1
    iget v0, p0, Lmaps/p/ap;->f:I

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lmaps/p/ap;->h:[I

    iget v0, p0, Lmaps/p/ap;->f:I

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lmaps/p/ap;->i:[I

    iget-object v0, p0, Lmaps/p/ap;->h:[I

    invoke-static {v0, v3}, Ljava/util/Arrays;->fill([II)V

    iget-object v0, p0, Lmaps/p/ap;->i:[I

    invoke-static {v0, v3}, Ljava/util/Arrays;->fill([II)V

    iget-object v0, p0, Lmaps/p/ap;->k:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v3

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v4, p0, Lmaps/p/ap;->h:[I

    aput v1, v4, v0

    if-le v1, v3, :cond_2

    iget-object v4, p0, Lmaps/p/ap;->i:[I

    aput v0, v4, v1

    :cond_2
    move v1, v0

    goto :goto_3

    :cond_3
    return-void

    :cond_4
    move v1, v0

    move v0, v2

    goto :goto_2
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lmaps/p/ap;->g:I

    return v0
.end method

.method public a(F)I
    .locals 2

    iget v0, p0, Lmaps/p/ap;->d:I

    int-to-float v0, v0

    sub-float v0, p1, v0

    iget v1, p0, Lmaps/p/ap;->b:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iget-object v1, p0, Lmaps/p/ap;->c:[I

    array-length v1, v1

    if-lt v0, v1, :cond_0

    iget v0, p0, Lmaps/p/ap;->f:I

    :goto_0
    return v0

    :cond_0
    if-gez v0, :cond_1

    const/4 v0, -0x1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lmaps/p/ap;->c:[I

    aget v0, v1, v0

    goto :goto_0
.end method

.method public a(I)I
    .locals 1

    if-ltz p1, :cond_0

    iget-object v0, p0, Lmaps/p/ap;->h:[I

    array-length v0, v0

    if-lt p1, v0, :cond_1

    :cond_0
    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lmaps/p/ap;->h:[I

    aget v0, v0, p1

    goto :goto_0
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lmaps/p/ap;->f:I

    return v0
.end method

.method public b(I)I
    .locals 1

    if-ltz p1, :cond_0

    iget-object v0, p0, Lmaps/p/ap;->i:[I

    array-length v0, v0

    if-lt p1, v0, :cond_1

    :cond_0
    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lmaps/p/ap;->i:[I

    aget v0, v0, p1

    goto :goto_0
.end method

.method public c(I)Z
    .locals 2

    iget-object v0, p0, Lmaps/p/ap;->k:Ljava/util/TreeSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public d(I)F
    .locals 1

    if-ltz p1, :cond_0

    iget-object v0, p0, Lmaps/p/ap;->j:[F

    array-length v0, v0

    if-lt p1, v0, :cond_1

    :cond_0
    const/high16 v0, -0x40800000

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lmaps/p/ap;->j:[F

    aget v0, v0, p1

    goto :goto_0
.end method

.method public e(I)F
    .locals 1

    if-ltz p1, :cond_0

    iget-object v0, p0, Lmaps/p/ap;->j:[F

    array-length v0, v0

    if-lt p1, v0, :cond_1

    :cond_0
    const/high16 v0, -0x40800000

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lmaps/p/ap;->l:[F

    aget v0, v0, p1

    goto :goto_0
.end method
