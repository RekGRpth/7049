.class public Lmaps/p/av;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field private final a:Lmaps/y/bc;

.field private final b:Lmaps/p/j;

.field private final c:Ljava/util/Map;

.field private final d:Ljava/util/Map;

.field private e:Z

.field private f:Z


# direct methods
.method public constructor <init>(Lmaps/y/bc;Lmaps/p/j;Ljava/util/Collection;Ljava/util/Collection;)V
    .locals 5

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lmaps/f/cs;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/p/av;->c:Ljava/util/Map;

    invoke-static {}, Lmaps/f/cs;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/p/av;->d:Ljava/util/Map;

    iput-boolean v1, p0, Lmaps/p/av;->e:Z

    iput-boolean v1, p0, Lmaps/p/av;->f:Z

    iput-object p1, p0, Lmaps/p/av;->a:Lmaps/y/bc;

    iput-object p2, p0, Lmaps/p/av;->b:Lmaps/p/j;

    invoke-interface {p3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/p/z;

    invoke-interface {v0}, Lmaps/p/z;->a()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/t/v;

    iget-object v4, p0, Lmaps/p/av;->c:Ljava/util/Map;

    invoke-interface {v4, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    invoke-interface {p4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/p/z;

    invoke-interface {v0}, Lmaps/p/z;->a()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/t/v;

    iget-object v4, p0, Lmaps/p/av;->d:Ljava/util/Map;

    invoke-interface {v4, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_3
    return-void
.end method

.method public varargs constructor <init>(Lmaps/y/bc;Lmaps/p/j;[Lmaps/p/z;)V
    .locals 6

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lmaps/f/cs;->a()Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, Lmaps/p/av;->c:Ljava/util/Map;

    invoke-static {}, Lmaps/f/cs;->a()Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, Lmaps/p/av;->d:Ljava/util/Map;

    iput-boolean v0, p0, Lmaps/p/av;->e:Z

    iput-boolean v0, p0, Lmaps/p/av;->f:Z

    iput-object p1, p0, Lmaps/p/av;->a:Lmaps/y/bc;

    iput-object p2, p0, Lmaps/p/av;->b:Lmaps/p/j;

    array-length v2, p3

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, p3, v1

    invoke-interface {v3}, Lmaps/p/z;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/v;

    iget-object v5, p0, Lmaps/p/av;->c:Ljava/util/Map;

    invoke-interface {v5, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public a(Lmaps/p/av;)I
    .locals 3

    iget-object v0, p0, Lmaps/p/av;->b:Lmaps/p/j;

    invoke-virtual {p1}, Lmaps/p/av;->b()Lmaps/p/j;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/p/j;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-nez v0, :cond_1

    iget-object v1, p0, Lmaps/p/av;->a:Lmaps/y/bc;

    invoke-virtual {p1}, Lmaps/p/av;->a()Lmaps/y/bc;

    move-result-object v2

    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lmaps/y/bc;->b()Lmaps/y/am;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/y/am;->a()I

    move-result v0

    invoke-virtual {v2}, Lmaps/y/bc;->b()Lmaps/y/am;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/y/am;->a()I

    move-result v1

    sub-int/2addr v0, v1

    :cond_0
    if-nez v0, :cond_1

    iget-object v1, p0, Lmaps/p/av;->c:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p1, Lmaps/p/av;->c:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v0, p0, Lmaps/p/av;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->max(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/p/z;

    iget-object v1, p1, Lmaps/p/av;->c:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->max(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/p/z;

    invoke-interface {v0, v1}, Lmaps/p/z;->compareTo(Ljava/lang/Object;)I

    move-result v0

    :cond_1
    return v0
.end method

.method public a(Lmaps/t/v;)Lmaps/p/z;
    .locals 1

    iget-object v0, p0, Lmaps/p/av;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/p/z;

    return-object v0
.end method

.method public a()Lmaps/y/bc;
    .locals 1

    iget-object v0, p0, Lmaps/p/av;->a:Lmaps/y/bc;

    return-object v0
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lmaps/p/av;->e:Z

    return-void
.end method

.method public b()Lmaps/p/j;
    .locals 1

    iget-object v0, p0, Lmaps/p/av;->b:Lmaps/p/j;

    return-object v0
.end method

.method public b(Lmaps/t/v;)Lmaps/p/z;
    .locals 1

    iget-object v0, p0, Lmaps/p/av;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/p/z;

    return-object v0
.end method

.method public b(Z)V
    .locals 0

    iput-boolean p1, p0, Lmaps/p/av;->f:Z

    return-void
.end method

.method public c()Z
    .locals 2

    iget-object v0, p0, Lmaps/p/av;->b:Lmaps/p/j;

    sget-object v1, Lmaps/p/j;->a:Lmaps/p/j;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lmaps/p/av;->b:Lmaps/p/j;

    sget-object v1, Lmaps/p/j;->c:Lmaps/p/j;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lmaps/p/av;->b:Lmaps/p/j;

    sget-object v1, Lmaps/p/j;->h:Lmaps/p/j;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lmaps/p/av;->b:Lmaps/p/j;

    sget-object v1, Lmaps/p/j;->f:Lmaps/p/j;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lmaps/p/av;->b:Lmaps/p/j;

    sget-object v1, Lmaps/p/j;->i:Lmaps/p/j;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lmaps/p/av;

    invoke-virtual {p0, p1}, Lmaps/p/av;->a(Lmaps/p/av;)I

    move-result v0

    return v0
.end method

.method public d()Z
    .locals 2

    iget-object v0, p0, Lmaps/p/av;->b:Lmaps/p/j;

    sget-object v1, Lmaps/p/j;->g:Lmaps/p/j;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lmaps/p/av;->b:Lmaps/p/j;

    sget-object v1, Lmaps/p/j;->b:Lmaps/p/j;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Z
    .locals 2

    iget-object v0, p0, Lmaps/p/av;->b:Lmaps/p/j;

    sget-object v1, Lmaps/p/j;->e:Lmaps/p/j;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/p/av;->e:Z

    return v0
.end method

.method public g()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/p/av;->f:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Lmaps/ap/e;->a(Ljava/lang/Object;)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "overlay"

    iget-object v2, p0, Lmaps/p/av;->a:Lmaps/y/bc;

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "order"

    iget-object v2, p0, Lmaps/p/av;->b:Lmaps/p/j;

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "isFirstPassForOverlay"

    iget-boolean v2, p0, Lmaps/p/av;->e:Z

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;Z)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "isLastPassForOverlay"

    iget-boolean v2, p0, Lmaps/p/av;->f:Z

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;Z)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "overlayRenderTweaks"

    iget-object v2, p0, Lmaps/p/av;->c:Ljava/util/Map;

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "featureRenderTweaks"

    iget-object v2, p0, Lmaps/p/av;->d:Ljava/util/Map;

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/ap/n;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ap/n;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
