.class public Lmaps/p/w;
.super Landroid/view/SurfaceView;

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# static fields
.field private static final a:Lmaps/p/aa;


# instance fields
.field private final b:Ljava/lang/ref/WeakReference;

.field private c:Lmaps/p/q;

.field private d:Lmaps/p/b;

.field private e:Z

.field private f:Lmaps/p/e;

.field private g:Lmaps/p/ak;

.field private h:Lmaps/p/p;

.field private i:Lmaps/p/as;

.field private j:I

.field private k:I

.field private l:Z

.field private m:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lmaps/p/aa;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lmaps/p/aa;-><init>(Lmaps/p/ag;)V

    sput-object v0, Lmaps/p/w;->a:Lmaps/p/aa;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lmaps/p/w;->b:Ljava/lang/ref/WeakReference;

    invoke-direct {p0}, Lmaps/p/w;->a()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lmaps/p/w;->b:Ljava/lang/ref/WeakReference;

    invoke-direct {p0}, Lmaps/p/w;->a()V

    return-void
.end method

.method static synthetic a(Lmaps/p/w;)I
    .locals 1

    iget v0, p0, Lmaps/p/w;->k:I

    return v0
.end method

.method private a()V
    .locals 3

    invoke-virtual {p0}, Lmaps/p/w;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x9

    if-ge v1, v2, :cond_0

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setFormat(I)V

    :cond_0
    return-void
.end method

.method static synthetic b(Lmaps/p/w;)Lmaps/p/e;
    .locals 1

    iget-object v0, p0, Lmaps/p/w;->f:Lmaps/p/e;

    return-object v0
.end method

.method static synthetic c(Lmaps/p/w;)Lmaps/p/ak;
    .locals 1

    iget-object v0, p0, Lmaps/p/w;->g:Lmaps/p/ak;

    return-object v0
.end method

.method static synthetic d(Lmaps/p/w;)Lmaps/p/p;
    .locals 1

    iget-object v0, p0, Lmaps/p/w;->h:Lmaps/p/p;

    return-object v0
.end method

.method private d()V
    .locals 2

    iget-object v0, p0, Lmaps/p/w;->c:Lmaps/p/q;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setRenderer has already been called for this instance."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method static synthetic e(Lmaps/p/w;)Lmaps/p/as;
    .locals 1

    iget-object v0, p0, Lmaps/p/w;->i:Lmaps/p/as;

    return-object v0
.end method

.method static synthetic f(Lmaps/p/w;)I
    .locals 1

    iget v0, p0, Lmaps/p/w;->j:I

    return v0
.end method

.method static synthetic g(Lmaps/p/w;)Z
    .locals 1

    iget-boolean v0, p0, Lmaps/p/w;->l:Z

    return v0
.end method

.method static synthetic h(Lmaps/p/w;)Lmaps/p/b;
    .locals 1

    iget-object v0, p0, Lmaps/p/w;->d:Lmaps/p/b;

    return-object v0
.end method

.method static synthetic r()Lmaps/p/aa;
    .locals 1

    sget-object v0, Lmaps/p/w;->a:Lmaps/p/aa;

    return-object v0
.end method


# virtual methods
.method public a(I)V
    .locals 0

    invoke-direct {p0}, Lmaps/p/w;->d()V

    iput p1, p0, Lmaps/p/w;->k:I

    return-void
.end method

.method public a(Lmaps/p/as;)V
    .locals 0

    iput-object p1, p0, Lmaps/p/w;->i:Lmaps/p/as;

    return-void
.end method

.method public a(Lmaps/p/b;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lmaps/p/w;->d()V

    iget-object v0, p0, Lmaps/p/w;->f:Lmaps/p/e;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/p/o;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lmaps/p/o;-><init>(Lmaps/p/w;Z)V

    iput-object v0, p0, Lmaps/p/w;->f:Lmaps/p/e;

    :cond_0
    iget-object v0, p0, Lmaps/p/w;->g:Lmaps/p/ak;

    if-nez v0, :cond_1

    new-instance v0, Lmaps/p/d;

    invoke-direct {v0, p0, v2}, Lmaps/p/d;-><init>(Lmaps/p/w;Lmaps/p/ag;)V

    iput-object v0, p0, Lmaps/p/w;->g:Lmaps/p/ak;

    :cond_1
    iget-object v0, p0, Lmaps/p/w;->h:Lmaps/p/p;

    if-nez v0, :cond_2

    new-instance v0, Lmaps/p/ad;

    invoke-direct {v0, v2}, Lmaps/p/ad;-><init>(Lmaps/p/ag;)V

    iput-object v0, p0, Lmaps/p/w;->h:Lmaps/p/p;

    :cond_2
    iput-object p1, p0, Lmaps/p/w;->d:Lmaps/p/b;

    new-instance v0, Lmaps/p/q;

    iget-object v1, p0, Lmaps/p/w;->b:Ljava/lang/ref/WeakReference;

    invoke-direct {v0, v1}, Lmaps/p/q;-><init>(Ljava/lang/ref/WeakReference;)V

    iput-object v0, p0, Lmaps/p/w;->c:Lmaps/p/q;

    iget-object v0, p0, Lmaps/p/w;->c:Lmaps/p/q;

    invoke-virtual {v0}, Lmaps/p/q;->start()V

    return-void
.end method

.method public a(Lmaps/p/e;)V
    .locals 0

    invoke-direct {p0}, Lmaps/p/w;->d()V

    iput-object p1, p0, Lmaps/p/w;->f:Lmaps/p/e;

    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lmaps/p/w;->c:Lmaps/p/q;

    invoke-virtual {v0}, Lmaps/p/q;->f()V

    return-void
.end method

.method public b(I)V
    .locals 1

    iget-object v0, p0, Lmaps/p/w;->c:Lmaps/p/q;

    invoke-virtual {v0, p1}, Lmaps/p/q;->a(I)V

    return-void
.end method

.method public c()V
    .locals 1

    iget-object v0, p0, Lmaps/p/w;->c:Lmaps/p/q;

    invoke-virtual {v0}, Lmaps/p/q;->g()V

    return-void
.end method

.method protected finalize()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lmaps/p/w;->c:Lmaps/p/q;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/p/w;->c:Lmaps/p/q;

    invoke-virtual {v0}, Lmaps/p/q;->h()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    return-void

    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method public h(Z)V
    .locals 1

    iput-boolean p1, p0, Lmaps/p/w;->m:Z

    if-nez p1, :cond_0

    iget-boolean v0, p0, Lmaps/p/w;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/p/w;->c:Lmaps/p/q;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/p/w;->c:Lmaps/p/q;

    invoke-virtual {v0}, Lmaps/p/q;->i()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/p/w;->c:Lmaps/p/q;

    invoke-virtual {v0}, Lmaps/p/q;->h()V

    :cond_0
    return-void
.end method

.method public i(Z)V
    .locals 0

    iput-boolean p1, p0, Lmaps/p/w;->l:Z

    return-void
.end method

.method public j_()V
    .locals 1

    iget-object v0, p0, Lmaps/p/w;->c:Lmaps/p/q;

    invoke-virtual {v0}, Lmaps/p/q;->c()V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 4

    const/4 v1, 0x1

    invoke-super {p0}, Landroid/view/SurfaceView;->onAttachedToWindow()V

    iget-boolean v0, p0, Lmaps/p/w;->e:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/p/w;->d:Lmaps/p/b;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/p/w;->c:Lmaps/p/q;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/p/w;->c:Lmaps/p/q;

    invoke-virtual {v0}, Lmaps/p/q;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Lmaps/p/w;->c:Lmaps/p/q;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/p/w;->c:Lmaps/p/q;

    invoke-virtual {v0}, Lmaps/p/q;->b()I

    move-result v0

    :goto_0
    new-instance v2, Lmaps/p/q;

    iget-object v3, p0, Lmaps/p/w;->b:Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v3}, Lmaps/p/q;-><init>(Ljava/lang/ref/WeakReference;)V

    iput-object v2, p0, Lmaps/p/w;->c:Lmaps/p/q;

    if-eq v0, v1, :cond_1

    iget-object v1, p0, Lmaps/p/w;->c:Lmaps/p/q;

    invoke-virtual {v1, v0}, Lmaps/p/q;->a(I)V

    :cond_1
    iget-object v0, p0, Lmaps/p/w;->c:Lmaps/p/q;

    invoke-virtual {v0}, Lmaps/p/q;->start()V

    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/p/w;->e:Z

    return-void

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    iget-boolean v0, p0, Lmaps/p/w;->m:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/p/w;->c:Lmaps/p/q;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/p/w;->c:Lmaps/p/q;

    invoke-virtual {v0}, Lmaps/p/q;->h()V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/p/w;->e:Z

    invoke-super {p0}, Landroid/view/SurfaceView;->onDetachedFromWindow()V

    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 1

    iget-object v0, p0, Lmaps/p/w;->c:Lmaps/p/q;

    invoke-virtual {v0, p3, p4}, Lmaps/p/q;->a(II)V

    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 1

    iget-object v0, p0, Lmaps/p/w;->c:Lmaps/p/q;

    invoke-virtual {v0}, Lmaps/p/q;->d()V

    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 1

    iget-object v0, p0, Lmaps/p/w;->c:Lmaps/p/q;

    invoke-virtual {v0}, Lmaps/p/q;->e()V

    return-void
.end method
