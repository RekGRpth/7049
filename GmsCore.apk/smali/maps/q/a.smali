.class public Lmaps/q/a;
.super Ljava/lang/Object;


# instance fields
.field private a:Lmaps/q/am;

.field private b:Lmaps/q/am;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private declared-synchronized a()Lmaps/q/am;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/q/a;->a:Lmaps/q/am;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lmaps/q/am;->a(Lmaps/q/am;)Lmaps/q/am;

    move-result-object v1

    iput-object v1, p0, Lmaps/q/a;->a:Lmaps/q/am;

    iget-object v1, p0, Lmaps/q/a;->a:Lmaps/q/am;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    iput-object v1, p0, Lmaps/q/a;->b:Lmaps/q/am;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Lmaps/q/a;)Lmaps/q/am;
    .locals 1

    invoke-direct {p0}, Lmaps/q/a;->a()Lmaps/q/am;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lmaps/q/a;Lmaps/q/am;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/q/a;->a(Lmaps/q/am;)V

    return-void
.end method

.method private declared-synchronized a(Lmaps/q/am;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/q/a;->b:Lmaps/q/am;

    iput-object p1, p0, Lmaps/q/a;->b:Lmaps/q/am;

    if-nez v0, :cond_0

    iput-object p1, p0, Lmaps/q/a;->a:Lmaps/q/am;

    :goto_0
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lmaps/q/am;->a(Lmaps/q/am;Lmaps/q/am;)Lmaps/q/am;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    invoke-static {v0, p1}, Lmaps/q/am;->a(Lmaps/q/am;Lmaps/q/am;)Lmaps/q/am;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
