.class public Lmaps/q/ab;
.super Ljava/lang/Object;


# instance fields
.field private a:Z

.field private b:Landroid/graphics/Bitmap;

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, Lmaps/q/ab;->a:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/q/ab;->b:Landroid/graphics/Bitmap;

    iput v1, p0, Lmaps/q/ab;->c:I

    iput v1, p0, Lmaps/q/ab;->d:I

    iput-object p1, p0, Lmaps/q/ab;->b:Landroid/graphics/Bitmap;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lmaps/q/ab;->c:I

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lmaps/q/ab;->d:I

    return-void
.end method


# virtual methods
.method a(Lmaps/q/ad;Lmaps/q/f;)Z
    .locals 9

    const/16 v2, 0x1907

    const/16 v0, 0xde1

    const/4 v1, 0x0

    iget-boolean v3, p2, Lmaps/q/f;->e:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lmaps/q/ab;->b:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_1

    iget-object v2, p0, Lmaps/q/ab;->b:Landroid/graphics/Bitmap;

    invoke-static {v0, v1, v2, v1}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    :cond_0
    :goto_0
    iget-boolean v0, p2, Lmaps/q/f;->e:Z

    iput-boolean v0, p0, Lmaps/q/ab;->a:Z

    const/4 v0, 0x1

    return v0

    :cond_1
    iget v3, p0, Lmaps/q/ab;->c:I

    iget v4, p0, Lmaps/q/ab;->d:I

    const v7, 0x8363

    const/4 v8, 0x0

    move v5, v1

    move v6, v2

    invoke-static/range {v0 .. v8}, Landroid/opengl/GLES20;->glTexImage2D(IIIIIIIILjava/nio/Buffer;)V

    const-string v0, "ImageData"

    const-string v1, "glTexImage2D failed"

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
