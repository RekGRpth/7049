.class public Lmaps/q/ao;
.super Lmaps/q/r;


# instance fields
.field protected final c:Ljava/nio/ByteBuffer;

.field protected final d:I

.field protected e:I

.field protected f:I

.field protected g:I

.field protected h:I

.field protected i:I

.field protected final j:I

.field protected final k:I

.field private final l:Ljava/nio/ShortBuffer;

.field private final m:I

.field private n:[I

.field private o:[I

.field private final p:Z

.field private final q:Z

.field private final r:Z

.field private final s:Z

.field private final t:Z


# direct methods
.method public constructor <init>(Ljava/nio/ByteBuffer;I[SII)V
    .locals 4

    const/4 v3, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Lmaps/q/r;-><init>()V

    iput v2, p0, Lmaps/q/ao;->e:I

    iput v2, p0, Lmaps/q/ao;->f:I

    new-array v0, v1, [I

    iput-object v0, p0, Lmaps/q/ao;->n:[I

    new-array v0, v1, [I

    iput-object v0, p0, Lmaps/q/ao;->o:[I

    const/4 v0, 0x4

    if-eq p5, v0, :cond_0

    const/4 v0, 0x5

    if-eq p5, v0, :cond_0

    const/4 v0, 0x6

    if-eq p5, v0, :cond_0

    if-eq p5, v1, :cond_0

    const/4 v0, 0x3

    if-eq p5, v0, :cond_0

    const/4 v0, 0x2

    if-eq p5, v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Illegal dataType "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lmaps/q/ao;->n:[I

    aput v3, v0, v2

    iget-object v0, p0, Lmaps/q/ao;->o:[I

    aput v3, v0, v2

    and-int/lit8 v0, p4, 0x1

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lmaps/q/ao;->p:Z

    and-int/lit8 v0, p4, 0x2

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lmaps/q/ao;->q:Z

    and-int/lit8 v0, p4, 0x4

    if-eqz v0, :cond_3

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lmaps/q/ao;->s:Z

    and-int/lit8 v0, p4, 0x10

    if-eqz v0, :cond_4

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lmaps/q/ao;->t:Z

    and-int/lit8 v0, p4, 0x8

    if-eqz v0, :cond_5

    :goto_4
    iput-boolean v1, p0, Lmaps/q/ao;->r:Z

    iput p4, p0, Lmaps/q/ao;->k:I

    iput p5, p0, Lmaps/q/ao;->j:I

    invoke-virtual {p0}, Lmaps/q/ao;->a()V

    iput-object p1, p0, Lmaps/q/ao;->c:Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lmaps/q/ao;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    iput p2, p0, Lmaps/q/ao;->d:I

    if-eqz p3, :cond_6

    array-length v0, p3

    if-lez v0, :cond_6

    array-length v0, p3

    mul-int/lit8 v0, v0, 0x2

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asShortBuffer()Ljava/nio/ShortBuffer;

    move-result-object v0

    iput-object v0, p0, Lmaps/q/ao;->l:Ljava/nio/ShortBuffer;

    iget-object v0, p0, Lmaps/q/ao;->l:Ljava/nio/ShortBuffer;

    invoke-virtual {v0, p3}, Ljava/nio/ShortBuffer;->put([S)Ljava/nio/ShortBuffer;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/nio/ShortBuffer;->position(I)Ljava/nio/Buffer;

    array-length v0, p3

    iput v0, p0, Lmaps/q/ao;->m:I

    :goto_5
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_3

    :cond_5
    move v1, v2

    goto :goto_4

    :cond_6
    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/q/ao;->l:Ljava/nio/ShortBuffer;

    iput v2, p0, Lmaps/q/ao;->m:I

    goto :goto_5
.end method

.method public constructor <init>([FII)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2, p3}, Lmaps/q/ao;-><init>([F[SII)V

    return-void
.end method

.method public constructor <init>([F[SII)V
    .locals 4

    const/4 v3, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Lmaps/q/r;-><init>()V

    iput v2, p0, Lmaps/q/ao;->e:I

    iput v2, p0, Lmaps/q/ao;->f:I

    new-array v0, v1, [I

    iput-object v0, p0, Lmaps/q/ao;->n:[I

    new-array v0, v1, [I

    iput-object v0, p0, Lmaps/q/ao;->o:[I

    const/4 v0, 0x4

    if-eq p4, v0, :cond_0

    const/4 v0, 0x5

    if-eq p4, v0, :cond_0

    const/4 v0, 0x6

    if-eq p4, v0, :cond_0

    if-eq p4, v1, :cond_0

    const/4 v0, 0x3

    if-eq p4, v0, :cond_0

    const/4 v0, 0x2

    if-eq p4, v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Illegal dataType "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lmaps/q/ao;->n:[I

    aput v3, v0, v2

    iget-object v0, p0, Lmaps/q/ao;->o:[I

    aput v3, v0, v2

    and-int/lit8 v0, p3, 0x1

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lmaps/q/ao;->p:Z

    and-int/lit8 v0, p3, 0x2

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lmaps/q/ao;->q:Z

    and-int/lit8 v0, p3, 0x4

    if-eqz v0, :cond_3

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lmaps/q/ao;->s:Z

    and-int/lit8 v0, p3, 0x10

    if-eqz v0, :cond_4

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lmaps/q/ao;->t:Z

    and-int/lit8 v0, p3, 0x8

    if-eqz v0, :cond_5

    :goto_4
    iput-boolean v1, p0, Lmaps/q/ao;->r:Z

    iput p3, p0, Lmaps/q/ao;->k:I

    iput p4, p0, Lmaps/q/ao;->j:I

    iget-boolean v0, p0, Lmaps/q/ao;->t:Z

    if-eqz v0, :cond_6

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "COLORS_BYTE_MASK can not be used with this constructor "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lmaps/q/ao;->k:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_3

    :cond_5
    move v1, v2

    goto :goto_4

    :cond_6
    invoke-virtual {p0}, Lmaps/q/ao;->a()V

    array-length v0, p1

    iget v1, p0, Lmaps/q/ao;->e:I

    div-int/lit8 v1, v1, 0x4

    div-int/2addr v0, v1

    iput v0, p0, Lmaps/q/ao;->d:I

    iget v0, p0, Lmaps/q/ao;->d:I

    iget v1, p0, Lmaps/q/ao;->e:I

    mul-int/2addr v0, v1

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lmaps/q/ao;->c:Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lmaps/q/ao;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    if-eqz p2, :cond_7

    array-length v0, p2

    if-lez v0, :cond_7

    array-length v0, p2

    mul-int/lit8 v0, v0, 0x2

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asShortBuffer()Ljava/nio/ShortBuffer;

    move-result-object v0

    iput-object v0, p0, Lmaps/q/ao;->l:Ljava/nio/ShortBuffer;

    iget-object v0, p0, Lmaps/q/ao;->l:Ljava/nio/ShortBuffer;

    invoke-virtual {v0, p2}, Ljava/nio/ShortBuffer;->put([S)Ljava/nio/ShortBuffer;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/nio/ShortBuffer;->position(I)Ljava/nio/Buffer;

    array-length v0, p2

    iput v0, p0, Lmaps/q/ao;->m:I

    :goto_5
    return-void

    :cond_7
    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/q/ao;->l:Ljava/nio/ShortBuffer;

    iput v2, p0, Lmaps/q/ao;->m:I

    goto :goto_5
.end method


# virtual methods
.method protected a()V
    .locals 1

    iget-boolean v0, p0, Lmaps/q/ao;->p:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lmaps/q/ao;->e:I

    add-int/lit8 v0, v0, 0xc

    iput v0, p0, Lmaps/q/ao;->e:I

    :cond_0
    iget-boolean v0, p0, Lmaps/q/ao;->q:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lmaps/q/ao;->e:I

    iput v0, p0, Lmaps/q/ao;->h:I

    iget v0, p0, Lmaps/q/ao;->e:I

    add-int/lit8 v0, v0, 0xc

    iput v0, p0, Lmaps/q/ao;->e:I

    :cond_1
    iget-boolean v0, p0, Lmaps/q/ao;->s:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lmaps/q/ao;->e:I

    iput v0, p0, Lmaps/q/ao;->i:I

    iget v0, p0, Lmaps/q/ao;->e:I

    add-int/lit8 v0, v0, 0x10

    iput v0, p0, Lmaps/q/ao;->e:I

    :cond_2
    iget-boolean v0, p0, Lmaps/q/ao;->t:Z

    if-eqz v0, :cond_3

    iget v0, p0, Lmaps/q/ao;->e:I

    iput v0, p0, Lmaps/q/ao;->i:I

    iget v0, p0, Lmaps/q/ao;->e:I

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Lmaps/q/ao;->e:I

    :cond_3
    iget-boolean v0, p0, Lmaps/q/ao;->r:Z

    if-eqz v0, :cond_4

    iget v0, p0, Lmaps/q/ao;->e:I

    iput v0, p0, Lmaps/q/ao;->g:I

    iget v0, p0, Lmaps/q/ao;->e:I

    add-int/lit8 v0, v0, 0x8

    iput v0, p0, Lmaps/q/ao;->e:I

    :cond_4
    return-void
.end method

.method public a(Lmaps/q/ad;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lmaps/q/ao;->l:Ljava/nio/ShortBuffer;

    if-eqz v0, :cond_0

    iget v0, p0, Lmaps/q/ao;->j:I

    iget v1, p0, Lmaps/q/ao;->m:I

    const/16 v2, 0x1403

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/GLES20;->glDrawElements(IIII)V

    const-string v0, "InterleavedVertexData"

    const-string v1, "glDrawElements"

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lmaps/q/ao;->j:I

    iget v1, p0, Lmaps/q/ao;->d:I

    invoke-static {v0, v3, v1}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    const-string v0, "InterleavedVertexData"

    const-string v1, "glDrawArrays"

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lmaps/q/ad;Lmaps/q/r;)V
    .locals 12

    const/16 v2, 0x1406

    const/4 v11, 0x4

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v0, 0x0

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    iget-boolean v1, p0, Lmaps/q/ao;->r:Z

    if-eqz v1, :cond_0

    invoke-static {v11}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    :cond_0
    iget-boolean v1, p0, Lmaps/q/ao;->q:Z

    if-eqz v1, :cond_1

    invoke-static {v9}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    :cond_1
    iget-boolean v1, p0, Lmaps/q/ao;->s:Z

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lmaps/q/ao;->t:Z

    if-eqz v1, :cond_3

    :cond_2
    invoke-static {v10}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    :cond_3
    const-string v1, "InterleavedVertexData"

    const-string v3, "glEnableVertexAttribArrays"

    invoke-static {v1, v3}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    const v1, 0x8892

    iget-object v3, p0, Lmaps/q/ao;->n:[I

    aget v3, v3, v0

    invoke-static {v1, v3}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    const-string v1, "InterleavedVertexData"

    const-string v3, "glVertexAttribPointer BindBuffer"

    invoke-static {v1, v3}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x3

    iget v4, p0, Lmaps/q/ao;->e:I

    move v3, v0

    move v5, v0

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZII)V

    const-string v1, "InterleavedVertexData"

    const-string v3, "glVertexAttribPointer position "

    invoke-static {v1, v3}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v1, p0, Lmaps/q/ao;->q:Z

    if-eqz v1, :cond_4

    const/4 v4, 0x3

    iget v7, p0, Lmaps/q/ao;->e:I

    iget v8, p0, Lmaps/q/ao;->h:I

    move v3, v9

    move v5, v2

    move v6, v9

    invoke-static/range {v3 .. v8}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZII)V

    const-string v1, "InterleavedVertexData"

    const-string v3, "glVertexAttribPointer Normals"

    invoke-static {v1, v3}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget-boolean v1, p0, Lmaps/q/ao;->s:Z

    if-eqz v1, :cond_5

    iget v7, p0, Lmaps/q/ao;->e:I

    iget v8, p0, Lmaps/q/ao;->i:I

    move v3, v10

    move v4, v11

    move v5, v2

    move v6, v0

    invoke-static/range {v3 .. v8}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZII)V

    const-string v1, "InterleavedVertexData"

    const-string v3, "glVertexAttribPointer Colors"

    invoke-static {v1, v3}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    iget-boolean v1, p0, Lmaps/q/ao;->t:Z

    if-eqz v1, :cond_6

    const/16 v5, 0x1401

    iget v7, p0, Lmaps/q/ao;->e:I

    iget v8, p0, Lmaps/q/ao;->i:I

    move v3, v10

    move v4, v11

    move v6, v9

    invoke-static/range {v3 .. v8}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZII)V

    const-string v1, "InterleavedVertexData"

    const-string v3, "glVertexAttribPointer byte Colors"

    invoke-static {v1, v3}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    iget-boolean v1, p0, Lmaps/q/ao;->r:Z

    if-eqz v1, :cond_7

    iget v7, p0, Lmaps/q/ao;->e:I

    iget v8, p0, Lmaps/q/ao;->g:I

    move v3, v11

    move v4, v10

    move v5, v2

    move v6, v9

    invoke-static/range {v3 .. v8}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZII)V

    const-string v1, "InterleavedVertexData"

    const-string v2, "glVertexAttribPointer VBO"

    invoke-static {v1, v2}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    iget-object v1, p0, Lmaps/q/ao;->l:Ljava/nio/ShortBuffer;

    if-eqz v1, :cond_8

    const v1, 0x8893

    iget-object v2, p0, Lmaps/q/ao;->o:[I

    aget v0, v2, v0

    invoke-static {v1, v0}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    :cond_8
    return-void
.end method

.method public a(Lmaps/q/ad;Lmaps/q/f;)Z
    .locals 8

    const v7, 0x88e4

    const v6, 0x8893

    const v5, 0x8892

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-super {p0, p1, p2}, Lmaps/q/r;->a(Lmaps/q/ad;Lmaps/q/f;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v1, p2, Lmaps/q/f;->e:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lmaps/q/ao;->n:[I

    invoke-static {v4, v1, v3}, Landroid/opengl/GLES20;->glGenBuffers(I[II)V

    iget-object v1, p0, Lmaps/q/ao;->n:[I

    aget v1, v1, v3

    invoke-static {v5, v1}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    iget-object v1, p0, Lmaps/q/ao;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    iget v1, p0, Lmaps/q/ao;->d:I

    iget v2, p0, Lmaps/q/ao;->e:I

    mul-int/2addr v1, v2

    iget-object v2, p0, Lmaps/q/ao;->c:Ljava/nio/ByteBuffer;

    invoke-static {v5, v1, v2, v7}, Landroid/opengl/GLES20;->glBufferData(IILjava/nio/Buffer;I)V

    invoke-static {v5, v3}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    const-string v1, "InterleavedVertexData"

    const-string v2, "glBindBuffers"

    invoke-static {v1, v2}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lmaps/q/ao;->l:Ljava/nio/ShortBuffer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/q/ao;->o:[I

    invoke-static {v4, v1, v3}, Landroid/opengl/GLES20;->glGenBuffers(I[II)V

    iget-object v1, p0, Lmaps/q/ao;->o:[I

    aget v1, v1, v3

    invoke-static {v6, v1}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    iget-object v1, p0, Lmaps/q/ao;->l:Ljava/nio/ShortBuffer;

    invoke-virtual {v1, v3}, Ljava/nio/ShortBuffer;->position(I)Ljava/nio/Buffer;

    iget v1, p0, Lmaps/q/ao;->m:I

    mul-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lmaps/q/ao;->l:Ljava/nio/ShortBuffer;

    invoke-static {v6, v1, v2, v7}, Landroid/opengl/GLES20;->glBufferData(IILjava/nio/Buffer;I)V

    invoke-static {v6, v3}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    const-string v1, "InterleavedVertexData"

    const-string v2, "glBindBuffers"

    invoke-static {v1, v2}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lmaps/q/ao;->n:[I

    invoke-static {v4, v1, v3}, Landroid/opengl/GLES20;->glDeleteBuffers(I[II)V

    iget-object v1, p0, Lmaps/q/ao;->n:[I

    aput v3, v1, v3

    iget-object v1, p0, Lmaps/q/ao;->l:Ljava/nio/ShortBuffer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/q/ao;->o:[I

    invoke-static {v4, v1, v3}, Landroid/opengl/GLES20;->glDeleteBuffers(I[II)V

    iget-object v1, p0, Lmaps/q/ao;->o:[I

    aput v3, v1, v3

    goto :goto_0
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lmaps/q/ao;->d:I

    return v0
.end method

.method public b(Lmaps/q/ad;Lmaps/q/r;)V
    .locals 1

    iget-boolean v0, p0, Lmaps/q/ao;->r:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisableVertexAttribArray(I)V

    :cond_0
    iget-boolean v0, p0, Lmaps/q/ao;->q:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisableVertexAttribArray(I)V

    :cond_1
    iget-boolean v0, p0, Lmaps/q/ao;->s:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lmaps/q/ao;->t:Z

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x2

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisableVertexAttribArray(I)V

    :cond_3
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p1, Lmaps/q/ao;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Lmaps/q/ao;

    iget-object v1, p0, Lmaps/q/ao;->n:[I

    aget v1, v1, v0

    iget-object v2, p1, Lmaps/q/ao;->n:[I

    aget v2, v2, v0

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lmaps/q/ao;->o:[I

    aget v1, v1, v0

    iget-object v2, p1, Lmaps/q/ao;->o:[I

    aget v2, v2, v0

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
