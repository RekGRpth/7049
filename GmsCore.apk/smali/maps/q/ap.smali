.class public final enum Lmaps/q/ap;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lmaps/q/ap;

.field public static final enum b:Lmaps/q/ap;

.field public static final enum c:Lmaps/q/ap;

.field public static final enum d:Lmaps/q/ap;

.field public static final enum e:Lmaps/q/ap;

.field public static final enum f:Lmaps/q/ap;

.field public static final enum g:Lmaps/q/ap;

.field private static final synthetic i:[Lmaps/q/ap;


# instance fields
.field private final h:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lmaps/q/ap;

    const-string v1, "PICK"

    invoke-direct {v0, v1, v4, v4}, Lmaps/q/ap;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lmaps/q/ap;->a:Lmaps/q/ap;

    new-instance v0, Lmaps/q/ap;

    const-string v1, "TEXTURE0"

    invoke-direct {v0, v1, v5, v5}, Lmaps/q/ap;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lmaps/q/ap;->b:Lmaps/q/ap;

    new-instance v0, Lmaps/q/ap;

    const-string v1, "TEXTURE1"

    invoke-direct {v0, v1, v6, v6}, Lmaps/q/ap;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lmaps/q/ap;->c:Lmaps/q/ap;

    new-instance v0, Lmaps/q/ap;

    const-string v1, "SHADER"

    invoke-direct {v0, v1, v7, v7}, Lmaps/q/ap;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lmaps/q/ap;->d:Lmaps/q/ap;

    new-instance v0, Lmaps/q/ap;

    const-string v1, "STENCIL"

    invoke-direct {v0, v1, v8, v8}, Lmaps/q/ap;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lmaps/q/ap;->e:Lmaps/q/ap;

    new-instance v0, Lmaps/q/ap;

    const-string v1, "POLYGON"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lmaps/q/ap;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lmaps/q/ap;->f:Lmaps/q/ap;

    new-instance v0, Lmaps/q/ap;

    const-string v1, "BLEND"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lmaps/q/ap;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lmaps/q/ap;->g:Lmaps/q/ap;

    const/4 v0, 0x7

    new-array v0, v0, [Lmaps/q/ap;

    sget-object v1, Lmaps/q/ap;->a:Lmaps/q/ap;

    aput-object v1, v0, v4

    sget-object v1, Lmaps/q/ap;->b:Lmaps/q/ap;

    aput-object v1, v0, v5

    sget-object v1, Lmaps/q/ap;->c:Lmaps/q/ap;

    aput-object v1, v0, v6

    sget-object v1, Lmaps/q/ap;->d:Lmaps/q/ap;

    aput-object v1, v0, v7

    sget-object v1, Lmaps/q/ap;->e:Lmaps/q/ap;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lmaps/q/ap;->f:Lmaps/q/ap;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lmaps/q/ap;->g:Lmaps/q/ap;

    aput-object v2, v0, v1

    sput-object v0, Lmaps/q/ap;->i:[Lmaps/q/ap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lmaps/q/ap;->h:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/q/ap;
    .locals 1

    const-class v0, Lmaps/q/ap;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/q/ap;

    return-object v0
.end method

.method public static values()[Lmaps/q/ap;
    .locals 1

    sget-object v0, Lmaps/q/ap;->i:[Lmaps/q/ap;

    invoke-virtual {v0}, [Lmaps/q/ap;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/q/ap;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lmaps/q/ap;->h:I

    return v0
.end method
