.class public Lmaps/q/ax;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/q/b;


# instance fields
.field private final a:Ljava/util/HashSet;

.field private final b:Lmaps/q/h;

.field private final c:Ljava/util/concurrent/Semaphore;

.field private final d:Lmaps/q/m;


# direct methods
.method public constructor <init>(Lmaps/q/h;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lmaps/f/a;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lmaps/q/ax;->a:Ljava/util/HashSet;

    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lmaps/q/ax;->c:Ljava/util/concurrent/Semaphore;

    iput-object p1, p0, Lmaps/q/ax;->b:Lmaps/q/h;

    new-instance v0, Lmaps/q/m;

    invoke-direct {v0, p0}, Lmaps/q/m;-><init>(Lmaps/q/ax;)V

    iput-object v0, p0, Lmaps/q/ax;->d:Lmaps/q/m;

    iget-object v0, p0, Lmaps/q/ax;->d:Lmaps/q/m;

    invoke-virtual {v0}, Lmaps/q/m;->start()V

    return-void
.end method

.method static synthetic a(Lmaps/q/ax;)Ljava/util/concurrent/Semaphore;
    .locals 1

    iget-object v0, p0, Lmaps/q/ax;->c:Ljava/util/concurrent/Semaphore;

    return-object v0
.end method

.method static synthetic b(Lmaps/q/ax;)Lmaps/q/h;
    .locals 1

    iget-object v0, p0, Lmaps/q/ax;->b:Lmaps/q/h;

    return-object v0
.end method

.method static synthetic c(Lmaps/q/ax;)Ljava/util/HashSet;
    .locals 1

    iget-object v0, p0, Lmaps/q/ax;->a:Ljava/util/HashSet;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lmaps/q/ax;->c:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquire()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method public a(Lmaps/q/ba;)V
    .locals 2

    instance-of v0, p1, Lmaps/q/ac;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Current implementation only supports GmmGeoEntity"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lmaps/q/ax;->a:Ljava/util/HashSet;

    check-cast p1, Lmaps/q/ac;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lmaps/q/ax;->c:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    return-void
.end method

.method public b(Lmaps/q/ba;)V
    .locals 1

    iget-object v0, p0, Lmaps/q/ax;->a:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    return-void
.end method
