.class public Lmaps/q/bb;
.super Lmaps/q/aj;

# interfaces
.implements Lmaps/q/bg;


# instance fields
.field private volatile i:[F


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lmaps/q/bb;-><init>(I)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    const-class v0, Lmaps/q/l;

    invoke-direct {p0, v0}, Lmaps/q/aj;-><init>(Ljava/lang/Class;)V

    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lmaps/q/bb;->i:[F

    invoke-virtual {p0, p1}, Lmaps/q/bb;->a(I)V

    return-void
.end method


# virtual methods
.method public a(FFFF)V
    .locals 2

    iget-boolean v0, p0, Lmaps/q/bb;->c:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/q/bd;->b()V

    :cond_0
    iget-object v0, p0, Lmaps/q/bb;->i:[F

    const/4 v1, 0x0

    aput p1, v0, v1

    iget-object v0, p0, Lmaps/q/bb;->i:[F

    const/4 v1, 0x1

    aput p2, v0, v1

    iget-object v0, p0, Lmaps/q/bb;->i:[F

    const/4 v1, 0x2

    aput p3, v0, v1

    iget-object v0, p0, Lmaps/q/bb;->i:[F

    const/4 v1, 0x3

    aput p4, v0, v1

    return-void
.end method

.method public a(I)V
    .locals 4

    const/high16 v3, 0x437f0000

    iget-boolean v0, p0, Lmaps/q/bb;->c:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/q/bd;->b()V

    :cond_0
    iget-object v0, p0, Lmaps/q/bb;->i:[F

    const/4 v1, 0x0

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/q/bb;->i:[F

    const/4 v1, 0x1

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/q/bb;->i:[F

    const/4 v1, 0x2

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/q/bb;->i:[F

    const/4 v1, 0x3

    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    return-void
.end method

.method protected a(Lmaps/q/ba;Lmaps/q/h;Lmaps/q/k;I)V
    .locals 3

    invoke-super {p0, p1, p2, p3, p4}, Lmaps/q/aj;->a(Lmaps/q/ba;Lmaps/q/h;Lmaps/q/k;I)V

    const/4 v0, 0x2

    iget-object v1, p0, Lmaps/q/bb;->i:[F

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glVertexAttrib4fv(I[FI)V

    const-string v0, "ShaderState"

    const-string v1, "glUniform"

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
