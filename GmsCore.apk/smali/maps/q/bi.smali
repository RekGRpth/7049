.class public Lmaps/q/bi;
.super Lmaps/q/al;


# instance fields
.field private d:Ljava/util/LinkedHashSet;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    invoke-direct {p0, p1}, Lmaps/q/al;-><init>(I)V

    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lmaps/q/bi;->d:Ljava/util/LinkedHashSet;

    return-void
.end method


# virtual methods
.method a(Lmaps/q/ad;)V
    .locals 3

    iget-object v0, p0, Lmaps/q/bi;->a:Lmaps/q/h;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lmaps/q/bi;->a:Lmaps/q/h;

    invoke-virtual {v0, p1, p0}, Lmaps/q/h;->a(Lmaps/q/ad;Lmaps/q/al;)V

    iget-object v0, p0, Lmaps/q/bi;->d:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/q/ba;

    iget v2, p0, Lmaps/q/bi;->c:I

    invoke-virtual {v0, v2}, Lmaps/q/ba;->a(I)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0, p1, p0}, Lmaps/q/ba;->a(Lmaps/q/ad;Lmaps/q/al;)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lmaps/q/bi;->a:Lmaps/q/h;

    invoke-virtual {v0, p1, p0}, Lmaps/q/h;->b(Lmaps/q/ad;Lmaps/q/al;)V

    goto :goto_0
.end method

.method a(Lmaps/q/ba;)V
    .locals 1

    iget-object v0, p0, Lmaps/q/bi;->d:Ljava/util/LinkedHashSet;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method b(Lmaps/q/ad;)V
    .locals 3

    iget-object v0, p0, Lmaps/q/bi;->d:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/q/ba;

    sget-object v2, Lmaps/q/f;->d:Lmaps/q/f;

    invoke-virtual {v0, p1, v2}, Lmaps/q/ba;->a(Lmaps/q/ad;Lmaps/q/f;)Z

    goto :goto_0

    :cond_0
    return-void
.end method

.method b(Lmaps/q/ba;)V
    .locals 1

    iget-object v0, p0, Lmaps/q/bi;->d:Ljava/util/LinkedHashSet;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashSet;->remove(Ljava/lang/Object;)Z

    return-void
.end method
