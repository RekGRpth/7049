.class public Lmaps/q/l;
.super Lmaps/q/bh;


# direct methods
.method public constructor <init>()V
    .locals 2

    const-string v0, "#version 100\nprecision highp float;\nuniform mat4 uMVPMatrix;\nattribute vec4 aPosition;\nattribute vec4 aColor;\nvarying vec4 color;\nvoid main() {\n  gl_Position = uMVPMatrix * aPosition;\n  color = aColor;\n}\n"

    const-string v1, "precision highp float;\nvarying vec4 color;\nvoid main() {\n  gl_FragColor = color;\n}\n"

    invoke-direct {p0, v0, v1}, Lmaps/q/bh;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
