.class Lmaps/q/m;
.super Ljava/lang/Thread;


# instance fields
.field final synthetic a:Lmaps/q/ax;

.field private final b:Ljava/util/List;

.field private final c:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lmaps/q/ax;)V
    .locals 1

    iput-object p1, p0, Lmaps/q/m;->a:Lmaps/q/ax;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmaps/q/m;->b:Ljava/util/List;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lmaps/q/m;->c:Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lmaps/q/m;->setDaemon(Z)V

    return-void
.end method

.method private a()V
    .locals 9

    iget-object v3, p0, Lmaps/q/m;->b:Ljava/util/List;

    monitor-enter v3

    :try_start_0
    iget-object v0, p0, Lmaps/q/m;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/bx;

    iget-object v1, p0, Lmaps/q/m;->a:Lmaps/q/ax;

    invoke-static {v1}, Lmaps/q/ax;->b(Lmaps/q/ax;)Lmaps/q/h;

    move-result-object v1

    check-cast v1, Lmaps/bq/d;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lmaps/bq/d;->a(Lmaps/t/bx;Z)F

    move-result v5

    iget-object v1, p0, Lmaps/q/m;->a:Lmaps/q/ax;

    invoke-static {v1}, Lmaps/q/ax;->c(Lmaps/q/ax;)Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/q/ac;

    invoke-virtual {v1}, Lmaps/q/ac;->a()Lmaps/q/aw;

    move-result-object v7

    iget-object v2, p0, Lmaps/q/m;->a:Lmaps/q/ax;

    invoke-static {v2}, Lmaps/q/ax;->b(Lmaps/q/ax;)Lmaps/q/h;

    move-result-object v2

    check-cast v2, Lmaps/bq/d;

    iget v8, v7, Lmaps/q/aw;->g:F

    invoke-virtual {v2, v8, v5}, Lmaps/bq/d;->a(FF)F

    move-result v2

    iget-object v8, v1, Lmaps/q/ac;->c:Lmaps/t/bx;

    invoke-virtual {v8, v0}, Lmaps/t/bx;->c(Lmaps/t/bx;)F

    move-result v8

    cmpg-float v2, v8, v2

    if-gez v2, :cond_1

    invoke-virtual {v7, v1}, Lmaps/q/aw;->c(Lmaps/q/ba;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    iget-object v0, p0, Lmaps/q/m;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    :goto_0
    :try_start_0
    iget-object v1, p0, Lmaps/q/m;->c:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, p0, Lmaps/q/m;->c:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v0, p0, Lmaps/q/m;->a:Lmaps/q/ax;

    invoke-static {v0}, Lmaps/q/ax;->a(Lmaps/q/ax;)Ljava/util/concurrent/Semaphore;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquire()V

    invoke-direct {p0}, Lmaps/q/m;->a()V

    iget-object v0, p0, Lmaps/q/m;->a:Lmaps/q/ax;

    invoke-static {v0}, Lmaps/q/ax;->a(Lmaps/q/ax;)Ljava/util/concurrent/Semaphore;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
.end method
