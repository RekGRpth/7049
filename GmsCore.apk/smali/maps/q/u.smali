.class public Lmaps/q/u;
.super Lmaps/q/al;


# static fields
.field private static final d:I


# instance fields
.field private final e:[Ljava/util/LinkedHashSet;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lmaps/y/am;->values()[Lmaps/y/am;

    move-result-object v0

    array-length v0, v0

    sput v0, Lmaps/q/u;->d:I

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    invoke-direct {p0, p1}, Lmaps/q/al;-><init>(I)V

    sget v0, Lmaps/q/u;->d:I

    new-array v0, v0, [Ljava/util/LinkedHashSet;

    iput-object v0, p0, Lmaps/q/u;->e:[Ljava/util/LinkedHashSet;

    return-void
.end method


# virtual methods
.method a(Lmaps/q/ad;)V
    .locals 4

    iget-object v0, p0, Lmaps/q/u;->a:Lmaps/q/h;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lmaps/q/u;->a:Lmaps/q/h;

    invoke-virtual {v0, p1, p0}, Lmaps/q/h;->a(Lmaps/q/ad;Lmaps/q/al;)V

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    sget v0, Lmaps/q/u;->d:I

    if-ge v1, v0, :cond_3

    iget-object v0, p0, Lmaps/q/u;->e:[Ljava/util/LinkedHashSet;

    aget-object v0, v0, v1

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/q/aq;

    iget v3, p0, Lmaps/q/u;->c:I

    invoke-virtual {v0, v3}, Lmaps/q/aq;->a(I)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v0, p1, p0}, Lmaps/q/aq;->a(Lmaps/q/ad;Lmaps/q/al;)V

    goto :goto_2

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lmaps/q/u;->a:Lmaps/q/h;

    invoke-virtual {v0, p1, p0}, Lmaps/q/h;->b(Lmaps/q/ad;Lmaps/q/al;)V

    goto :goto_0
.end method

.method a(Lmaps/q/ba;)V
    .locals 3

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    instance-of v0, p1, Lmaps/q/aq;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Attempt to add non GmmLayerEntity to GmmLayerRenderBin"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    check-cast p1, Lmaps/q/aq;

    iget-object v0, p0, Lmaps/q/u;->e:[Ljava/util/LinkedHashSet;

    iget v1, p1, Lmaps/q/aq;->d:I

    aget-object v0, v0, v1

    if-nez v0, :cond_2

    iget-object v0, p0, Lmaps/q/u;->e:[Ljava/util/LinkedHashSet;

    iget v1, p1, Lmaps/q/aq;->d:I

    new-instance v2, Ljava/util/LinkedHashSet;

    invoke-direct {v2}, Ljava/util/LinkedHashSet;-><init>()V

    aput-object v2, v0, v1

    :cond_2
    iget-object v0, p0, Lmaps/q/u;->e:[Ljava/util/LinkedHashSet;

    iget v1, p1, Lmaps/q/aq;->d:I

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method b(Lmaps/q/ad;)V
    .locals 6

    iget-object v2, p0, Lmaps/q/u;->e:[Ljava/util/LinkedHashSet;

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v0, v2, v1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/q/aq;

    iget v5, p0, Lmaps/q/u;->c:I

    invoke-virtual {v0, v5}, Lmaps/q/aq;->a(I)Z

    move-result v5

    if-eqz v5, :cond_0

    sget-object v5, Lmaps/q/f;->d:Lmaps/q/f;

    invoke-virtual {v0, p1, v5}, Lmaps/q/aq;->a(Lmaps/q/ad;Lmaps/q/f;)Z

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    return-void
.end method

.method b(Lmaps/q/ba;)V
    .locals 3

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    instance-of v0, p1, Lmaps/q/aq;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Attempt to remove non GmmLayerEntity to GmmLayerRenderBin"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    check-cast p1, Lmaps/q/aq;

    iget-object v0, p0, Lmaps/q/u;->e:[Ljava/util/LinkedHashSet;

    iget v1, p1, Lmaps/q/aq;->d:I

    aget-object v0, v0, v1

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/q/u;->e:[Ljava/util/LinkedHashSet;

    iget v1, p1, Lmaps/q/aq;->d:I

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashSet;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/q/u;->e:[Ljava/util/LinkedHashSet;

    iget v1, p1, Lmaps/q/aq;->d:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->size()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/q/u;->e:[Ljava/util/LinkedHashSet;

    iget v1, p1, Lmaps/q/aq;->d:I

    const/4 v2, 0x0

    aput-object v2, v0, v1

    goto :goto_0

    :cond_3
    sget-boolean v0, Lmaps/ae/h;->h:Z

    if-eqz v0, :cond_0

    const-string v0, "GmmLayerRenderBin"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Attempt to remove non existing Entity from layer "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lmaps/q/aq;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
