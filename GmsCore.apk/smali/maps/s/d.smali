.class public Lmaps/s/d;
.super Lmaps/q/aj;


# instance fields
.field private i:[F


# direct methods
.method public constructor <init>(I)V
    .locals 3

    const/4 v2, 0x0

    const-class v0, Lmaps/s/e;

    invoke-direct {p0, v0}, Lmaps/q/aj;-><init>(Ljava/lang/Class;)V

    const/4 v0, 0x3

    new-array v0, v0, [F

    iput-object v0, p0, Lmaps/s/d;->i:[F

    const/4 v0, 0x5

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lmaps/s/d;->i:[F

    const/high16 v1, 0x3f400000

    aput v1, v0, v2

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lmaps/s/d;->i:[F

    const/high16 v1, 0x3e800000

    aput v1, v0, v2

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unexpected value for pass"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public a(F)V
    .locals 2

    iget-object v0, p0, Lmaps/s/d;->i:[F

    const/4 v1, 0x2

    aput p1, v0, v1

    return-void
.end method

.method public a(I)V
    .locals 3

    iget-object v0, p0, Lmaps/s/d;->i:[F

    const/4 v1, 0x1

    int-to-float v2, p1

    aput v2, v0, v1

    return-void
.end method

.method protected a(Lmaps/q/ba;Lmaps/q/h;Lmaps/q/k;I)V
    .locals 4

    invoke-super {p0, p1, p2, p3, p4}, Lmaps/q/aj;->a(Lmaps/q/ba;Lmaps/q/h;Lmaps/q/k;I)V

    iget-object v0, p0, Lmaps/s/d;->g:Lmaps/q/bh;

    check-cast v0, Lmaps/s/e;

    iget v0, v0, Lmaps/s/e;->c:I

    const/4 v1, 0x1

    iget-object v2, p0, Lmaps/s/d;->i:[F

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/GLES20;->glUniform3fv(II[FI)V

    const-string v0, "RoadShaderState"

    const-string v1, "glUniform"

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
