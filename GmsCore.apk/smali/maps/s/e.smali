.class public Lmaps/s/e;
.super Lmaps/q/bh;


# instance fields
.field protected a:I

.field protected b:I

.field protected c:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const-string v0, "uniform mat4 uMVPMatrix;\nattribute vec4 aPosition;\nattribute vec4 aColor;\nattribute vec2 aTextureCoord;\nattribute float aStyle;\nvarying vec2 vTextureCoord;\nvarying vec4 fillColor;\nvarying vec4 outlineColor;\nuniform sampler2D sStyleTexture;\nuniform vec3 uTextureInfo;\nvoid main() {\n  float styleCount = uTextureInfo[1];\n  float roadPass = uTextureInfo[0];\n  float textureScaleX = uTextureInfo[2];\n  float z = 0.0;\n  if(roadPass > 0.5) z = 0.001;\n  vec4 pos = vec4(aPosition.xy, z, 1.0);\n  gl_Position = uMVPMatrix * pos;\n  vTextureCoord = aTextureCoord;\n  vTextureCoord.x = ((vTextureCoord.x - 0.5) / textureScaleX) + 0.5;\n  vTextureCoord.y = ((vTextureCoord.y - 0.5) / textureScaleX) + 0.5;\n  fillColor = texture2D(sStyleTexture,       vec2(0.75, aStyle / styleCount + 0.5 / styleCount));\n  outlineColor = texture2D(sStyleTexture,       vec2(0.25, aStyle / styleCount + 0.5 / styleCount));\n  if(roadPass > 0.5) outlineColor.a = 0.0;\n}\n"

    const-string v1, "precision mediump float;\nvarying vec2 vTextureCoord;\nvarying vec4 fillColor;\nvarying vec4 outlineColor;\nuniform sampler2D sTexture0;\nvoid main() {\n  vec4 roadMask = texture2D(sTexture0, vTextureCoord);\n  if(roadMask.a == 0.0) discard;\n  float t = 2.0 * roadMask.r;\n  gl_FragColor = outlineColor * (1.0 - t) + fillColor * t;\n  if(outlineColor.a == 0.0 && gl_FragColor.a < 1.0) discard;\n  gl_FragColor.a = roadMask.a;\n}\n"

    invoke-direct {p0, v0, v1}, Lmaps/q/bh;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected a(I)V
    .locals 3

    const/4 v2, -0x1

    invoke-super {p0, p1}, Lmaps/q/bh;->a(I)V

    const-string v0, "sTexture0"

    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lmaps/s/e;->a:I

    const-string v0, "RoadShaderState"

    const-string v1, "glGetUniformLocation"

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p0, Lmaps/s/e;->a:I

    if-ne v0, v2, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to get sTexture0 handle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget v0, p0, Lmaps/s/e;->a:I

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1i(II)V

    const-string v0, "RoadShaderState"

    const-string v1, "glUniform"

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "sStyleTexture"

    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lmaps/s/e;->b:I

    const-string v0, "RoadShaderState"

    const-string v1, "glGetUniformLocation"

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p0, Lmaps/s/e;->b:I

    if-ne v0, v2, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to get sStyleTexture handle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget v0, p0, Lmaps/s/e;->b:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1i(II)V

    const-string v0, "RoadShaderState"

    const-string v1, "glUniform"

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "uTextureInfo"

    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lmaps/s/e;->c:I

    const-string v0, "RoadShaderState"

    const-string v1, "glGetUniformLocation"

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p0, Lmaps/s/e;->c:I

    if-ne v0, v2, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to get uTextureInfo handle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-void
.end method

.method protected b(I)V
    .locals 2

    invoke-super {p0, p1}, Lmaps/q/bh;->b(I)V

    const/4 v0, 0x5

    const-string v1, "aStyle"

    invoke-static {p1, v0, v1}, Landroid/opengl/GLES20;->glBindAttribLocation(IILjava/lang/String;)V

    const-string v0, "RoadShaderState"

    const-string v1, "bindAttribute aPosition"

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
