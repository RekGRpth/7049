.class public Lmaps/s/i;
.super Ljava/lang/Object;


# instance fields
.field private a:[Lmaps/cr/a;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lmaps/cr/c;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "TexturePool.initialize"

    invoke-static {v0}, Lmaps/ah/a;->a(Ljava/lang/String;)V

    const/16 v0, 0x1f

    new-array v0, v0, [Lmaps/cr/a;

    iput-object v0, p0, Lmaps/s/i;->a:[Lmaps/cr/a;

    invoke-direct {p0, p1, p2}, Lmaps/s/i;->a(Landroid/content/res/Resources;Lmaps/cr/c;)V

    const-string v0, "TexturePool.initialize"

    invoke-static {v0}, Lmaps/ah/a;->b(Ljava/lang/String;)V

    return-void
.end method

.method private a(Landroid/content/res/Resources;Lmaps/cr/c;)V
    .locals 9

    const/4 v3, 0x0

    sget v4, Lmaps/ad/e;->t:I

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lmaps/s/i;->a(Lmaps/cr/c;Landroid/content/res/Resources;IIZZZZ)V

    sget-boolean v0, Lmaps/ae/h;->O:Z

    if-eqz v0, :cond_0

    const/16 v3, 0x19

    sget v4, Lmaps/ad/e;->F:I

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lmaps/s/i;->a(Lmaps/cr/c;Landroid/content/res/Resources;IIZZZZ)V

    const/16 v3, 0x1a

    sget v4, Lmaps/ad/e;->u:I

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lmaps/s/i;->a(Lmaps/cr/c;Landroid/content/res/Resources;IIZZZZ)V

    const/16 v3, 0x1b

    sget v4, Lmaps/ad/e;->v:I

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lmaps/s/i;->a(Lmaps/cr/c;Landroid/content/res/Resources;IIZZZZ)V

    const/16 v3, 0x1c

    sget v4, Lmaps/ad/e;->w:I

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lmaps/s/i;->a(Lmaps/cr/c;Landroid/content/res/Resources;IIZZZZ)V

    const/16 v3, 0x1d

    sget v4, Lmaps/ad/e;->x:I

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lmaps/s/i;->a(Lmaps/cr/c;Landroid/content/res/Resources;IIZZZZ)V

    const/16 v3, 0x1e

    sget v4, Lmaps/ad/e;->r:I

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lmaps/s/i;->a(Lmaps/cr/c;Landroid/content/res/Resources;IIZZZZ)V

    :cond_0
    const/4 v3, 0x1

    sget v4, Lmaps/ad/e;->y:I

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lmaps/s/i;->a(Lmaps/cr/c;Landroid/content/res/Resources;IIZZZZ)V

    const/4 v3, 0x2

    sget v4, Lmaps/ad/e;->A:I

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lmaps/s/i;->a(Lmaps/cr/c;Landroid/content/res/Resources;IIZZZZ)V

    const/4 v3, 0x3

    sget v4, Lmaps/ad/e;->C:I

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lmaps/s/i;->a(Lmaps/cr/c;Landroid/content/res/Resources;IIZZZZ)V

    const/4 v3, 0x4

    sget v4, Lmaps/ad/e;->E:I

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lmaps/s/i;->a(Lmaps/cr/c;Landroid/content/res/Resources;IIZZZZ)V

    const/4 v3, 0x5

    sget v4, Lmaps/ad/e;->z:I

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lmaps/s/i;->a(Lmaps/cr/c;Landroid/content/res/Resources;IIZZZZ)V

    const/4 v3, 0x6

    sget v4, Lmaps/ad/e;->B:I

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lmaps/s/i;->a(Lmaps/cr/c;Landroid/content/res/Resources;IIZZZZ)V

    const/16 v3, 0x16

    sget v4, Lmaps/ad/e;->D:I

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-nez v0, :cond_1

    const/4 v5, 0x1

    :goto_0
    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lmaps/s/i;->a(Lmaps/cr/c;Landroid/content/res/Resources;IIZZZZ)V

    const/4 v3, 0x7

    sget v4, Lmaps/ad/e;->H:I

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lmaps/s/i;->a(Lmaps/cr/c;Landroid/content/res/Resources;IIZZZZ)V

    const/16 v3, 0x15

    sget v4, Lmaps/ad/e;->G:I

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lmaps/s/i;->a(Lmaps/cr/c;Landroid/content/res/Resources;IIZZZZ)V

    const/16 v3, 0x8

    sget v4, Lmaps/ad/e;->I:I

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lmaps/s/i;->a(Lmaps/cr/c;Landroid/content/res/Resources;IIZZZZ)V

    const/16 v3, 0x9

    sget v4, Lmaps/ad/e;->J:I

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lmaps/s/i;->a(Lmaps/cr/c;Landroid/content/res/Resources;IIZZZZ)V

    const/16 v3, 0xa

    sget v4, Lmaps/ad/e;->K:I

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lmaps/s/i;->a(Lmaps/cr/c;Landroid/content/res/Resources;IIZZZZ)V

    const/16 v3, 0x14

    sget v4, Lmaps/ad/e;->l:I

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x1

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lmaps/s/i;->a(Lmaps/cr/c;Landroid/content/res/Resources;IIZZZZ)V

    const/16 v3, 0x17

    sget v4, Lmaps/ad/e;->s:I

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lmaps/s/i;->a(Lmaps/cr/c;Landroid/content/res/Resources;IIZZZZ)V

    const/16 v3, 0x18

    sget v4, Lmaps/ad/e;->o:I

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lmaps/s/i;->a(Lmaps/cr/c;Landroid/content/res/Resources;IIZZZZ)V

    sget-boolean v0, Lmaps/ae/h;->K:Z

    if-nez v0, :cond_2

    const/16 v3, 0xb

    sget v4, Lmaps/ad/e;->L:I

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lmaps/s/i;->a(Lmaps/cr/c;Landroid/content/res/Resources;IIZZZZ)V

    :goto_1
    return-void

    :cond_1
    const/4 v5, 0x0

    goto/16 :goto_0

    :cond_2
    const/16 v3, 0xc

    sget v4, Lmaps/ad/e;->M:I

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lmaps/s/i;->a(Lmaps/cr/c;Landroid/content/res/Resources;IIZZZZ)V

    const/16 v3, 0xd

    sget v4, Lmaps/ad/e;->N:I

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lmaps/s/i;->a(Lmaps/cr/c;Landroid/content/res/Resources;IIZZZZ)V

    const/16 v3, 0xe

    sget v4, Lmaps/ad/e;->O:I

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lmaps/s/i;->a(Lmaps/cr/c;Landroid/content/res/Resources;IIZZZZ)V

    const/16 v3, 0xf

    sget v4, Lmaps/ad/e;->P:I

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lmaps/s/i;->a(Lmaps/cr/c;Landroid/content/res/Resources;IIZZZZ)V

    const/16 v3, 0x10

    sget v4, Lmaps/ad/e;->Q:I

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lmaps/s/i;->a(Lmaps/cr/c;Landroid/content/res/Resources;IIZZZZ)V

    const/16 v3, 0x11

    sget v4, Lmaps/ad/e;->R:I

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lmaps/s/i;->a(Lmaps/cr/c;Landroid/content/res/Resources;IIZZZZ)V

    const/16 v3, 0x12

    sget v4, Lmaps/ad/e;->S:I

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lmaps/s/i;->a(Lmaps/cr/c;Landroid/content/res/Resources;IIZZZZ)V

    const/16 v3, 0x13

    sget v4, Lmaps/ad/e;->T:I

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lmaps/s/i;->a(Lmaps/cr/c;Landroid/content/res/Resources;IIZZZZ)V

    goto :goto_1
.end method

.method private a(Lmaps/cr/c;Landroid/content/res/Resources;IIZZZZ)V
    .locals 2

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    new-instance v0, Lmaps/cr/a;

    sget-boolean v1, Lmaps/ae/h;->F:Z

    invoke-direct {v0, p1, v1}, Lmaps/cr/a;-><init>(Lmaps/cr/c;Z)V

    :goto_0
    invoke-virtual {v0, p6}, Lmaps/cr/a;->a(Z)V

    invoke-virtual {v0, p7}, Lmaps/cr/a;->b(Z)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmaps/cr/a;->c(Z)V

    if-eqz p5, :cond_2

    if-eqz p8, :cond_1

    invoke-virtual {v0, p2, p4}, Lmaps/cr/a;->d(Landroid/content/res/Resources;I)V

    :goto_1
    iget-object v1, p0, Lmaps/s/i;->a:[Lmaps/cr/a;

    aput-object v0, v1, p3

    return-void

    :cond_0
    new-instance v0, Lmaps/cr/a;

    invoke-direct {v0, p1}, Lmaps/cr/a;-><init>(Lmaps/cr/c;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v0, p2, p4}, Lmaps/cr/a;->c(Landroid/content/res/Resources;I)V

    goto :goto_1

    :cond_2
    if-eqz p8, :cond_3

    invoke-virtual {v0, p2, p4}, Lmaps/cr/a;->b(Landroid/content/res/Resources;I)V

    goto :goto_1

    :cond_3
    invoke-virtual {v0, p2, p4}, Lmaps/cr/a;->a(Landroid/content/res/Resources;I)V

    goto :goto_1
.end method


# virtual methods
.method public a(I)Lmaps/cr/a;
    .locals 1

    iget-object v0, p0, Lmaps/s/i;->a:[Lmaps/cr/a;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public a()V
    .locals 4

    iget-object v1, p0, Lmaps/s/i;->a:[Lmaps/cr/a;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lmaps/cr/a;->g()V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/s/i;->a:[Lmaps/cr/a;

    return-void
.end method
