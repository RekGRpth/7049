.class public Lmaps/s/l;
.super Ljava/lang/Object;


# static fields
.field private static final a:I


# instance fields
.field private b:Lmaps/cr/a;

.field private c:Z

.field private final d:Landroid/graphics/Bitmap;

.field private final e:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-boolean v0, Lmaps/ae/h;->t:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x100

    :goto_0
    sput v0, Lmaps/s/l;->a:I

    return-void

    :cond_0
    const/high16 v0, 0x10000

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/s/l;->b:Lmaps/cr/a;

    iput-boolean v2, p0, Lmaps/s/l;->c:Z

    const/16 v0, 0x100

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v2, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lmaps/s/l;->d:Landroid/graphics/Bitmap;

    invoke-static {}, Lmaps/f/cs;->b()Ljava/util/LinkedHashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/s/l;->e:Ljava/util/Map;

    return-void
.end method

.method public static c(Lmaps/cr/c;)V
    .locals 3

    sget-boolean v0, Lmaps/ae/h;->t:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x1702

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-virtual {p0}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    const/high16 v0, 0x10000

    sget v1, Lmaps/s/l;->a:I

    div-int/2addr v0, v1

    invoke-virtual {p0}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v0, v0, v2}, Ljavax/microedition/khronos/opengles/GL10;->glScalex(III)V

    invoke-virtual {p0}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x1700

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    :cond_0
    return-void
.end method

.method public static d(Lmaps/cr/c;)V
    .locals 2

    sget-boolean v0, Lmaps/ae/h;->t:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x1702

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-virtual {p0}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    invoke-virtual {p0}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x1700

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    :cond_0
    return-void
.end method

.method private e(Lmaps/cr/c;)V
    .locals 2

    invoke-virtual {p0}, Lmaps/s/l;->b()V

    iget-object v0, p0, Lmaps/s/l;->b:Lmaps/cr/a;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    iget-object v1, p0, Lmaps/s/l;->b:Lmaps/cr/a;

    invoke-virtual {v1}, Lmaps/cr/a;->a()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lmaps/s/l;->a()V

    :cond_0
    iget-object v0, p0, Lmaps/s/l;->b:Lmaps/cr/a;

    if-nez v0, :cond_1

    new-instance v0, Lmaps/cr/a;

    invoke-direct {v0, p1}, Lmaps/cr/a;-><init>(Lmaps/cr/c;)V

    iput-object v0, p0, Lmaps/s/l;->b:Lmaps/cr/a;

    iget-object v0, p0, Lmaps/s/l;->b:Lmaps/cr/a;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmaps/cr/a;->c(Z)V

    :cond_1
    iget-object v0, p0, Lmaps/s/l;->b:Lmaps/cr/a;

    iget-object v1, p0, Lmaps/s/l;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lmaps/cr/a;->b(Landroid/graphics/Bitmap;)V

    return-void
.end method


# virtual methods
.method public declared-synchronized a(Lmaps/cr/c;)Lmaps/cr/a;
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lmaps/s/l;->b(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/s/l;->b:Lmaps/cr/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/s/l;->b:Lmaps/cr/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/s/l;->b:Lmaps/cr/a;

    invoke-virtual {v0}, Lmaps/cr/a;->g()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/s/l;->b:Lmaps/cr/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized a(ILmaps/s/n;)V
    .locals 5

    const/high16 v4, 0x10000

    const v3, 0x8000

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/s/l;->e:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-nez v0, :cond_1

    iget-object v0, p0, Lmaps/s/l;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/16 v2, 0x100

    if-lt v1, v2, :cond_0

    const-string v0, "ColorPalette"

    const-string v1, "Color texture is full"

    invoke-static {v0, v1}, Lmaps/bt/a;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v1, p0, Lmaps/s/l;->e:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lmaps/s/l;->c:Z

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    rem-int/lit16 v1, v1, 0x100

    mul-int/2addr v1, v4

    add-int/2addr v1, v3

    div-int/lit16 v1, v1, 0x100

    iput v1, p2, Lmaps/s/n;->a:I

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    div-int/lit16 v0, v0, 0x100

    mul-int/2addr v0, v4

    add-int/2addr v0, v3

    div-int/lit8 v0, v0, 0x1

    iput v0, p2, Lmaps/s/n;->b:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized b()V
    .locals 5

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lmaps/s/l;->c:Z

    iget-object v0, p0, Lmaps/s/l;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    iget-object v3, p0, Lmaps/s/l;->d:Landroid/graphics/Bitmap;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    rem-int/lit16 v4, v1, 0x100

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    div-int/lit16 v1, v1, 0x100

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, v4, v1, v0}, Landroid/graphics/Bitmap;->setPixel(III)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized b(Lmaps/cr/c;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/s/l;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/s/l;->b:Lmaps/cr/a;

    if-nez v0, :cond_1

    :cond_0
    invoke-direct {p0, p1}, Lmaps/s/l;->e(Lmaps/cr/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
