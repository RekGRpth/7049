.class public Lmaps/s/r;
.super Lmaps/q/ao;


# instance fields
.field private l:I


# direct methods
.method public constructor <init>(Ljava/nio/ByteBuffer;I[SII)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lmaps/q/ao;-><init>(Ljava/nio/ByteBuffer;I[SII)V

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 1

    invoke-super {p0}, Lmaps/q/ao;->a()V

    iget v0, p0, Lmaps/s/r;->k:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    iget v0, p0, Lmaps/s/r;->e:I

    iput v0, p0, Lmaps/s/r;->l:I

    iget v0, p0, Lmaps/s/r;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/s/r;->e:I

    :cond_0
    return-void
.end method

.method public a(Lmaps/q/ad;Lmaps/q/r;)V
    .locals 6

    const/4 v0, 0x5

    const/4 v1, 0x1

    invoke-super {p0, p1, p2}, Lmaps/q/ao;->a(Lmaps/q/ad;Lmaps/q/r;)V

    iget v2, p0, Lmaps/s/r;->k:I

    and-int/lit8 v2, v2, 0x20

    if-eqz v2, :cond_0

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    const/16 v2, 0x1401

    const/4 v3, 0x0

    iget v4, p0, Lmaps/s/r;->e:I

    iget v5, p0, Lmaps/s/r;->l:I

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZII)V

    const-string v0, "GmmVertexData"

    const-string v1, "glVertexAttribPointer VERTEX_USER_0_ATTR_INDEX"

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public b(Lmaps/q/ad;Lmaps/q/r;)V
    .locals 1

    invoke-super {p0, p1, p2}, Lmaps/q/ao;->b(Lmaps/q/ad;Lmaps/q/r;)V

    iget v0, p0, Lmaps/s/r;->k:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x5

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisableVertexAttribArray(I)V

    :cond_0
    return-void
.end method

.method public c()I
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lmaps/s/r;->d()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public d()I
    .locals 1

    iget-object v0, p0, Lmaps/s/r;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    return v0
.end method
