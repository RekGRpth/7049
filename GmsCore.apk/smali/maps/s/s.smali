.class public Lmaps/s/s;
.super Ljava/lang/Object;


# static fields
.field private static final b:[F

.field private static final c:[F

.field private static final d:[F


# instance fields
.field private a:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/16 v1, 0x14

    const/16 v0, 0xc

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    sput-object v0, Lmaps/s/s;->b:[F

    new-array v0, v1, [F

    fill-array-data v0, :array_1

    sput-object v0, Lmaps/s/s;->c:[F

    new-array v0, v1, [F

    fill-array-data v0, :array_2

    sput-object v0, Lmaps/s/s;->d:[F

    return-void

    :array_0
    .array-data 4
        0x0
        0x3f800000
        0x0
        0x0
        0x0
        0x0
        0x3f800000
        0x3f800000
        0x0
        0x3f800000
        0x0
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x3f800000
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x3f800000
        0x3f800000
        0x3f800000
        0x0
        0x3f800000
        0x0
        0x3f800000
        0x0
        0x0
        0x3f800000
        0x3f800000
    .end array-data

    :array_2
    .array-data 4
        -0x40800000
        0x3f800000
        0x0
        0x0
        0x0
        -0x40800000
        -0x40800000
        0x0
        0x0
        0x3f800000
        0x3f800000
        0x3f800000
        0x0
        0x3f800000
        0x0
        0x3f800000
        -0x40800000
        0x0
        0x3f800000
        0x3f800000
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmaps/s/s;->a:Ljava/util/HashMap;

    return-void
.end method

.method public static a(Lmaps/al/l;Lmaps/al/b;)V
    .locals 8

    const/4 v2, 0x0

    const v0, 0x3d80adfd

    float-to-double v3, v0

    invoke-static {v3, v4}, Ljava/lang/Math;->tan(D)D

    move-result-wide v3

    double-to-float v4, v3

    invoke-static {v0}, Landroid/util/FloatMath;->cos(F)F

    move-result v5

    const/high16 v1, 0x3f800000

    const/4 v0, 0x0

    move v3, v1

    move v1, v2

    :goto_0
    const/16 v6, 0x64

    if-ge v0, v6, :cond_1

    add-float v6, v3, v2

    add-float v7, v1, v2

    invoke-interface {p0, v6, v7, v2}, Lmaps/al/l;->a(FFF)V

    if-eqz p1, :cond_0

    int-to-short v6, v0

    invoke-interface {p1, v6}, Lmaps/al/b;->a(I)V

    :cond_0
    neg-float v6, v1

    mul-float/2addr v6, v4

    add-float/2addr v6, v3

    mul-float/2addr v3, v4

    add-float/2addr v1, v3

    mul-float v3, v6, v5

    mul-float/2addr v1, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static b(Lmaps/al/l;Lmaps/al/b;)V
    .locals 9

    const/4 v0, 0x0

    const/4 v2, 0x0

    const v1, 0x3d80adfd

    float-to-double v3, v1

    invoke-static {v3, v4}, Ljava/lang/Math;->tan(D)D

    move-result-wide v3

    double-to-float v5, v3

    invoke-static {v1}, Landroid/util/FloatMath;->cos(F)F

    move-result v6

    const/high16 v4, 0x3f800000

    invoke-interface {p0, v2, v2, v2}, Lmaps/al/l;->a(FFF)V

    if-eqz p1, :cond_0

    invoke-interface {p1, v0}, Lmaps/al/b;->a(I)V

    :cond_0
    move v1, v2

    move v3, v4

    :goto_0
    const/16 v7, 0x64

    if-ge v0, v7, :cond_2

    add-float v7, v3, v2

    add-float v8, v1, v2

    invoke-interface {p0, v7, v8, v2}, Lmaps/al/l;->a(FFF)V

    if-eqz p1, :cond_1

    add-int/lit8 v7, v0, 0x1

    int-to-short v7, v7

    invoke-interface {p1, v7}, Lmaps/al/b;->a(I)V

    :cond_1
    neg-float v7, v1

    mul-float/2addr v7, v5

    add-float/2addr v7, v3

    mul-float/2addr v3, v5

    add-float/2addr v1, v3

    mul-float v3, v7, v6

    mul-float/2addr v1, v6

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_3

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Lmaps/al/b;->a(I)V

    :goto_1
    return-void

    :cond_3
    add-float v0, v4, v2

    invoke-interface {p0, v0, v2, v2}, Lmaps/al/l;->a(FFF)V

    goto :goto_1
.end method


# virtual methods
.method public a(I)Lmaps/q/r;
    .locals 7

    const/4 v6, 0x0

    const/16 v5, 0x9

    const/4 v4, 0x5

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lmaps/s/s;->a:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/q/r;

    if-nez v0, :cond_0

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Lmaps/q/ao;

    sget-object v1, Lmaps/s/s;->c:[F

    invoke-direct {v0, v1, v5, v4}, Lmaps/q/ao;-><init>([FII)V

    iget-object v1, p0, Lmaps/s/s;->a:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Lmaps/q/ad;->a(Lmaps/q/r;)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Lmaps/q/ao;

    sget-object v1, Lmaps/s/s;->d:[F

    invoke-direct {v0, v1, v5, v4}, Lmaps/q/ao;-><init>([FII)V

    iget-object v1, p0, Lmaps/s/s;->a:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Lmaps/q/ad;->a(Lmaps/q/r;)V

    goto :goto_0

    :pswitch_2
    new-instance v1, Lmaps/s/t;

    const/16 v0, 0x64

    invoke-direct {v1, v0, v3, v2}, Lmaps/s/t;-><init>(IIZ)V

    invoke-static {v1, v6}, Lmaps/s/s;->a(Lmaps/al/l;Lmaps/al/b;)V

    invoke-virtual {v1}, Lmaps/s/t;->d()V

    const/4 v0, 0x2

    invoke-virtual {v1, v0}, Lmaps/s/t;->f(I)Lmaps/s/r;

    move-result-object v0

    invoke-virtual {v1}, Lmaps/s/t;->h()V

    iget-object v1, p0, Lmaps/s/s;->a:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Lmaps/q/ad;->a(Lmaps/q/r;)V

    goto :goto_0

    :pswitch_3
    new-instance v1, Lmaps/s/t;

    const/16 v0, 0x66

    invoke-direct {v1, v0, v3, v2}, Lmaps/s/t;-><init>(IIZ)V

    invoke-static {v1, v6}, Lmaps/s/s;->b(Lmaps/al/l;Lmaps/al/b;)V

    invoke-virtual {v1}, Lmaps/s/t;->d()V

    const/4 v0, 0x6

    invoke-virtual {v1, v0}, Lmaps/s/t;->f(I)Lmaps/s/r;

    move-result-object v0

    invoke-virtual {v1}, Lmaps/s/t;->h()V

    iget-object v1, p0, Lmaps/s/s;->a:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Lmaps/q/ad;->a(Lmaps/q/r;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
