.class public Lmaps/s/t;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/al/b;
.implements Lmaps/al/c;
.implements Lmaps/al/l;


# instance fields
.field private a:[F

.field private final b:Ljava/util/List;

.field private c:Ljava/util/List;

.field private d:Ljava/util/List;

.field private e:Ljava/util/List;

.field private f:Ljava/util/List;

.field private final g:Z

.field private final h:Z

.field private final i:Z

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private final o:I

.field private p:Ljava/nio/ByteBuffer;


# direct methods
.method public constructor <init>(IIZ)V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/s/t;->a:[F

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmaps/s/t;->b:Ljava/util/List;

    iput v2, p0, Lmaps/s/t;->k:I

    iput v2, p0, Lmaps/s/t;->l:I

    iput v2, p0, Lmaps/s/t;->m:I

    iput v2, p0, Lmaps/s/t;->n:I

    iput p2, p0, Lmaps/s/t;->o:I

    and-int/lit8 v0, p2, 0x8

    if-eqz v0, :cond_4

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lmaps/s/t;->g:Z

    and-int/lit8 v0, p2, 0x4

    if-eqz v0, :cond_5

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lmaps/s/t;->h:Z

    and-int/lit8 v0, p2, 0x20

    if-eqz v0, :cond_6

    :goto_2
    iput-boolean v1, p0, Lmaps/s/t;->i:Z

    const/16 v0, 0xc

    iput v0, p0, Lmaps/s/t;->j:I

    iget-boolean v0, p0, Lmaps/s/t;->h:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lmaps/s/t;->j:I

    add-int/lit8 v0, v0, 0x10

    iput v0, p0, Lmaps/s/t;->j:I

    invoke-static {p1}, Lmaps/f/fd;->c(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmaps/s/t;->d:Ljava/util/List;

    :cond_0
    iget-boolean v0, p0, Lmaps/s/t;->g:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lmaps/s/t;->j:I

    add-int/lit8 v0, v0, 0x8

    iput v0, p0, Lmaps/s/t;->j:I

    invoke-static {p1}, Lmaps/f/fd;->c(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmaps/s/t;->c:Ljava/util/List;

    :cond_1
    iget-boolean v0, p0, Lmaps/s/t;->i:Z

    if-eqz v0, :cond_2

    invoke-static {p1}, Lmaps/f/fd;->c(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmaps/s/t;->f:Ljava/util/List;

    iget v0, p0, Lmaps/s/t;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/s/t;->j:I

    :cond_2
    if-eqz p3, :cond_3

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmaps/s/t;->e:Ljava/util/List;

    :cond_3
    iget v0, p0, Lmaps/s/t;->j:I

    mul-int/2addr v0, p1

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lmaps/s/t;->p:Ljava/nio/ByteBuffer;

    return-void

    :cond_4
    move v0, v2

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_1

    :cond_6
    move v1, v2

    goto :goto_2
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lmaps/s/t;->m:I

    return v0
.end method

.method public a(FF)V
    .locals 2

    iget v0, p0, Lmaps/s/t;->n:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/s/t;->n:I

    iget-boolean v0, p0, Lmaps/s/t;->g:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Texture coordinate 0 not enabled in this VBO"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lmaps/s/t;->c:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/s/t;->c:Ljava/util/List;

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(FFF)V
    .locals 2

    iget-object v0, p0, Lmaps/s/t;->b:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/s/t;->b:Ljava/util/List;

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/s/t;->b:Ljava/util/List;

    invoke-static {p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget v0, p0, Lmaps/s/t;->l:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/s/t;->l:I

    return-void
.end method

.method public a(I)V
    .locals 2

    iget-object v0, p0, Lmaps/s/t;->e:Ljava/util/List;

    int-to-short v1, p1

    invoke-static {v1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget v0, p0, Lmaps/s/t;->m:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/s/t;->m:I

    return-void
.end method

.method public a(II)V
    .locals 3

    const/high16 v2, 0x47800000

    iget v0, p0, Lmaps/s/t;->n:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/s/t;->n:I

    iget-object v0, p0, Lmaps/s/t;->c:Ljava/util/List;

    int-to-float v1, p1

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/s/t;->c:Ljava/util/List;

    int-to-float v1, p2

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(III)V
    .locals 2

    iget-object v0, p0, Lmaps/s/t;->e:Ljava/util/List;

    int-to-short v1, p1

    invoke-static {v1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/s/t;->e:Ljava/util/List;

    int-to-short v1, p2

    invoke-static {v1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/s/t;->e:Ljava/util/List;

    int-to-short v1, p3

    invoke-static {v1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget v0, p0, Lmaps/s/t;->m:I

    add-int/lit8 v0, v0, 0x3

    iput v0, p0, Lmaps/s/t;->m:I

    return-void
.end method

.method public a(IIII)V
    .locals 2

    iget-object v0, p0, Lmaps/s/t;->e:Ljava/util/List;

    int-to-short v1, p1

    invoke-static {v1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/s/t;->e:Ljava/util/List;

    int-to-short v1, p2

    invoke-static {v1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/s/t;->e:Ljava/util/List;

    int-to-short v1, p3

    invoke-static {v1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/s/t;->e:Ljava/util/List;

    int-to-short v1, p3

    invoke-static {v1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/s/t;->e:Ljava/util/List;

    int-to-short v1, p2

    invoke-static {v1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/s/t;->e:Ljava/util/List;

    int-to-short v1, p4

    invoke-static {v1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget v0, p0, Lmaps/s/t;->m:I

    add-int/lit8 v0, v0, 0x6

    iput v0, p0, Lmaps/s/t;->m:I

    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 2

    iget-object v0, p0, Lmaps/s/t;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget v0, p0, Lmaps/s/t;->m:I

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lmaps/s/t;->m:I

    return-void
.end method

.method public a(Lmaps/t/bx;I)V
    .locals 3

    iget-object v0, p0, Lmaps/s/t;->b:Ljava/util/List;

    invoke-virtual {p1}, Lmaps/t/bx;->f()I

    move-result v1

    int-to-float v1, v1

    int-to-float v2, p2

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/s/t;->b:Ljava/util/List;

    invoke-virtual {p1}, Lmaps/t/bx;->g()I

    move-result v1

    int-to-float v1, v1

    int-to-float v2, p2

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/s/t;->b:Ljava/util/List;

    invoke-virtual {p1}, Lmaps/t/bx;->h()I

    move-result v1

    int-to-float v1, v1

    int-to-float v2, p2

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/s/t;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Expecting styleIndex"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget v0, p0, Lmaps/s/t;->l:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/s/t;->l:I

    return-void
.end method

.method public a(Lmaps/t/bx;IB)V
    .locals 3

    iget-object v0, p0, Lmaps/s/t;->b:Ljava/util/List;

    invoke-virtual {p1}, Lmaps/t/bx;->f()I

    move-result v1

    int-to-float v1, v1

    int-to-float v2, p2

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/s/t;->b:Ljava/util/List;

    invoke-virtual {p1}, Lmaps/t/bx;->g()I

    move-result v1

    int-to-float v1, v1

    int-to-float v2, p2

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/s/t;->b:Ljava/util/List;

    invoke-virtual {p1}, Lmaps/t/bx;->h()I

    move-result v1

    int-to-float v1, v1

    int-to-float v2, p2

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/s/t;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/s/t;->f:Ljava/util/List;

    invoke-static {p3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    iget v0, p0, Lmaps/s/t;->l:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/s/t;->l:I

    return-void
.end method

.method public a([I)V
    .locals 2

    iget v0, p0, Lmaps/s/t;->n:I

    array-length v1, p1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iput v0, p0, Lmaps/s/t;->n:I

    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lmaps/s/t;->a([III)V

    return-void
.end method

.method public a([III)V
    .locals 4

    iget v0, p0, Lmaps/s/t;->n:I

    div-int/lit8 v1, p3, 0x2

    add-int/2addr v0, v1

    iput v0, p0, Lmaps/s/t;->n:I

    move v0, p2

    :goto_0
    add-int v1, p2, p3

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lmaps/s/t;->c:Ljava/util/List;

    aget v2, p1, v0

    int-to-float v2, v2

    const/high16 v3, 0x47800000

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lmaps/s/t;->n:I

    return v0
.end method

.method public b(I)V
    .locals 0

    return-void
.end method

.method public b(II)V
    .locals 7

    const/high16 v4, 0x437f0000

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v0

    int-to-float v0, v0

    div-float v1, v0, v4

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v0

    int-to-float v0, v0

    div-float v2, v0, v4

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v0

    int-to-float v0, v0

    div-float v3, v0, v4

    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    int-to-float v0, v0

    div-float v4, v0, v4

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_0

    iget-object v5, p0, Lmaps/s/t;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v5, p0, Lmaps/s/t;->d:Ljava/util/List;

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v5, p0, Lmaps/s/t;->d:Ljava/util/List;

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v5, p0, Lmaps/s/t;->d:Ljava/util/List;

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public c()I
    .locals 1

    invoke-virtual {p0}, Lmaps/s/t;->d()V

    iget v0, p0, Lmaps/s/t;->l:I

    return v0
.end method

.method public c(I)V
    .locals 0

    return-void
.end method

.method public d()V
    .locals 5

    iget-object v0, p0, Lmaps/s/t;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/s/t;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    div-int/lit8 v2, v0, 0x3

    iget-boolean v0, p0, Lmaps/s/t;->g:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/s/t;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    if-eq v2, v0, :cond_2

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Buffer mismatch verts = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  tex coords = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lmaps/s/t;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-boolean v0, p0, Lmaps/s/t;->h:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/s/t;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    div-int/lit8 v0, v0, 0x4

    if-eq v2, v0, :cond_3

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Buffer mismatch"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget-boolean v0, p0, Lmaps/s/t;->i:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lmaps/s/t;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eq v2, v0, :cond_4

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Buffer mismatch"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_8

    :try_start_0
    iget-object v3, p0, Lmaps/s/t;->p:Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lmaps/s/t;->b:Ljava/util/List;

    mul-int/lit8 v4, v1, 0x3

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    iget-object v3, p0, Lmaps/s/t;->p:Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lmaps/s/t;->b:Ljava/util/List;

    mul-int/lit8 v4, v1, 0x3

    add-int/lit8 v4, v4, 0x1

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    iget-object v3, p0, Lmaps/s/t;->p:Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lmaps/s/t;->b:Ljava/util/List;

    mul-int/lit8 v4, v1, 0x3

    add-int/lit8 v4, v4, 0x2

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    iget-boolean v0, p0, Lmaps/s/t;->h:Z

    if-eqz v0, :cond_5

    iget-object v3, p0, Lmaps/s/t;->p:Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lmaps/s/t;->d:Ljava/util/List;

    mul-int/lit8 v4, v1, 0x4

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    iget-object v3, p0, Lmaps/s/t;->p:Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lmaps/s/t;->d:Ljava/util/List;

    mul-int/lit8 v4, v1, 0x4

    add-int/lit8 v4, v4, 0x1

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    iget-object v3, p0, Lmaps/s/t;->p:Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lmaps/s/t;->d:Ljava/util/List;

    mul-int/lit8 v4, v1, 0x4

    add-int/lit8 v4, v4, 0x2

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    iget-object v3, p0, Lmaps/s/t;->p:Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lmaps/s/t;->d:Ljava/util/List;

    mul-int/lit8 v4, v1, 0x4

    add-int/lit8 v4, v4, 0x3

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    :cond_5
    iget-boolean v0, p0, Lmaps/s/t;->g:Z

    if-eqz v0, :cond_6

    iget-object v3, p0, Lmaps/s/t;->p:Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lmaps/s/t;->c:Ljava/util/List;

    mul-int/lit8 v4, v1, 0x2

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    iget-object v3, p0, Lmaps/s/t;->p:Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lmaps/s/t;->c:Ljava/util/List;

    mul-int/lit8 v4, v1, 0x2

    add-int/lit8 v4, v4, 0x1

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    :cond_6
    iget-boolean v0, p0, Lmaps/s/t;->i:Z

    if-eqz v0, :cond_7

    iget-object v3, p0, Lmaps/s/t;->p:Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lmaps/s/t;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_7
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    :cond_8
    iget-object v0, p0, Lmaps/s/t;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lmaps/s/t;->c:Ljava/util/List;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmaps/s/t;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :cond_9
    iget-object v0, p0, Lmaps/s/t;->d:Ljava/util/List;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lmaps/s/t;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :cond_a
    iget-object v0, p0, Lmaps/s/t;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/s/t;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto/16 :goto_0
.end method

.method public d(I)V
    .locals 4

    iget-object v0, p0, Lmaps/s/t;->p:Ljava/nio/ByteBuffer;

    if-nez v0, :cond_1

    iget v0, p0, Lmaps/s/t;->j:I

    mul-int/2addr v0, p1

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lmaps/s/t;->p:Ljava/nio/ByteBuffer;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lmaps/s/t;->j:I

    mul-int/2addr v0, p1

    iget-object v1, p0, Lmaps/s/t;->p:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v1

    if-le v0, v1, :cond_0

    iget v0, p0, Lmaps/s/t;->j:I

    mul-int/2addr v0, p1

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    iget-object v1, p0, Lmaps/s/t;->p:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {}, Lmaps/ae/h;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    sget-boolean v1, Lmaps/ae/h;->i:Z

    if-eqz v1, :cond_2

    const-string v1, "GmmVertexDataBuild"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "growing native buffer from "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lmaps/s/t;->p:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v1, p0, Lmaps/s/t;->p:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    iget-object v1, p0, Lmaps/s/t;->p:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    :cond_3
    iput-object v0, p0, Lmaps/s/t;->p:Ljava/nio/ByteBuffer;

    goto :goto_0
.end method

.method public e()I
    .locals 1

    iget v0, p0, Lmaps/s/t;->o:I

    return v0
.end method

.method public e(I)V
    .locals 5

    const/high16 v4, 0x437f0000

    iget-boolean v0, p0, Lmaps/s/t;->h:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Vertex Colors not enabled in this VBO"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v4

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v4

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v4

    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v4

    iget-object v4, p0, Lmaps/s/t;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/s/t;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/s/t;->d:Ljava/util/List;

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/s/t;->d:Ljava/util/List;

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public f()Ljava/nio/ByteBuffer;
    .locals 2

    invoke-virtual {p0}, Lmaps/s/t;->d()V

    iget-object v0, p0, Lmaps/s/t;->p:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    const/4 v1, 0x0

    iput-object v1, p0, Lmaps/s/t;->p:Ljava/nio/ByteBuffer;

    return-object v0
.end method

.method public f(I)Lmaps/s/r;
    .locals 6

    new-instance v0, Lmaps/s/r;

    invoke-virtual {p0}, Lmaps/s/t;->f()Ljava/nio/ByteBuffer;

    move-result-object v1

    iget v2, p0, Lmaps/s/t;->l:I

    invoke-virtual {p0}, Lmaps/s/t;->g()[S

    move-result-object v3

    iget v4, p0, Lmaps/s/t;->o:I

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lmaps/s/r;-><init>(Ljava/nio/ByteBuffer;I[SII)V

    return-object v0
.end method

.method public g()[S
    .locals 4

    iget-object v0, p0, Lmaps/s/t;->e:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lmaps/s/t;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Lmaps/s/t;->m:I

    if-eq v0, v1, :cond_1

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Inconsistent index buffer size"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget v0, p0, Lmaps/s/t;->m:I

    new-array v2, v0, [S

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    array-length v0, v2

    if-ge v1, v0, :cond_3

    iget-object v0, p0, Lmaps/s/t;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Short;

    invoke-virtual {v0}, Ljava/lang/Short;->shortValue()S

    move-result v0

    aput-short v0, v2, v1

    aget-short v0, v2, v1

    iget v3, p0, Lmaps/s/t;->l:I

    if-le v0, v3, :cond_2

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Index larger than buffer"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    move-object v0, v2

    goto :goto_0
.end method

.method public h()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/s/t;->a:[F

    iget-object v0, p0, Lmaps/s/t;->e:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/s/t;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :cond_0
    iput v1, p0, Lmaps/s/t;->m:I

    iput v1, p0, Lmaps/s/t;->l:I

    iput v1, p0, Lmaps/s/t;->n:I

    iget-object v0, p0, Lmaps/s/t;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lmaps/s/t;->d:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/s/t;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :cond_1
    iget-object v0, p0, Lmaps/s/t;->c:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/s/t;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :cond_2
    iput v1, p0, Lmaps/s/t;->k:I

    iget-object v0, p0, Lmaps/s/t;->p:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/s/t;->p:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    :cond_3
    return-void
.end method

.method public i()I
    .locals 1

    iget v0, p0, Lmaps/s/t;->l:I

    return v0
.end method
