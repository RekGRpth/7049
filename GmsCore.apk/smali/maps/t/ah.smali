.class public Lmaps/t/ah;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field final a:I

.field final b:I

.field final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:Lmaps/t/al;

.field private h:Lmaps/t/ah;


# direct methods
.method public constructor <init>(III)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lmaps/t/ah;-><init>(IIILmaps/t/al;)V

    return-void
.end method

.method public constructor <init>(IIILmaps/t/al;)V
    .locals 3

    const/high16 v2, 0x20000000

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/t/ah;->h:Lmaps/t/ah;

    iput p1, p0, Lmaps/t/ah;->d:I

    iput p2, p0, Lmaps/t/ah;->e:I

    iput p3, p0, Lmaps/t/ah;->f:I

    if-nez p4, :cond_0

    new-instance p4, Lmaps/t/al;

    invoke-direct {p4}, Lmaps/t/al;-><init>()V

    :cond_0
    iput-object p4, p0, Lmaps/t/ah;->g:Lmaps/t/al;

    rsub-int/lit8 v0, p1, 0x12

    iput v0, p0, Lmaps/t/ah;->c:I

    const/high16 v0, 0x40000000

    shr-int/2addr v0, p1

    iget v1, p0, Lmaps/t/ah;->e:I

    mul-int/2addr v1, v0

    sub-int/2addr v1, v2

    iput v1, p0, Lmaps/t/ah;->a:I

    iget v1, p0, Lmaps/t/ah;->f:I

    add-int/lit8 v1, v1, 0x1

    mul-int/2addr v0, v1

    sub-int/2addr v0, v2

    neg-int v0, v0

    iput v0, p0, Lmaps/t/ah;->b:I

    return-void
.end method

.method public static a(Lmaps/t/bw;I)Ljava/util/ArrayList;
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lmaps/t/ah;->a(Lmaps/t/bw;ILmaps/t/al;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lmaps/t/bw;ILmaps/t/al;)Ljava/util/ArrayList;
    .locals 11

    const/4 v10, 0x2

    const/4 v3, 0x0

    if-gez p1, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lmaps/t/bw;->f()Lmaps/t/bx;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/bx;->f()I

    move-result v0

    invoke-virtual {p0}, Lmaps/t/bw;->g()Lmaps/t/bx;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/t/bx;->g()I

    move-result v1

    invoke-static {p1, v0, v1, p2}, Lmaps/t/ah;->a(IIILmaps/t/al;)Lmaps/t/ah;

    move-result-object v5

    invoke-virtual {p0}, Lmaps/t/bw;->g()Lmaps/t/bx;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/bx;->f()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0}, Lmaps/t/bw;->f()Lmaps/t/bx;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/t/bx;->g()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {p1, v0, v1, p2}, Lmaps/t/ah;->a(IIILmaps/t/al;)Lmaps/t/ah;

    move-result-object v6

    invoke-virtual {v5}, Lmaps/t/ah;->d()I

    move-result v1

    invoke-virtual {v5}, Lmaps/t/ah;->e()I

    move-result v2

    invoke-virtual {v6}, Lmaps/t/ah;->d()I

    move-result v7

    invoke-virtual {v6}, Lmaps/t/ah;->e()I

    move-result v8

    const/4 v0, 0x1

    shl-int v9, v0, p1

    if-le v1, v7, :cond_2

    sub-int v0, v9, v1

    add-int/2addr v0, v7

    add-int/lit8 v0, v0, 0x1

    sub-int v4, v8, v2

    add-int/lit8 v4, v4, 0x1

    mul-int/2addr v0, v4

    move v4, v0

    :goto_1
    if-gez v4, :cond_3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0

    :cond_2
    sub-int v0, v7, v1

    add-int/lit8 v0, v0, 0x1

    sub-int v4, v8, v2

    add-int/lit8 v4, v4, 0x1

    mul-int/2addr v0, v4

    move v4, v0

    goto :goto_1

    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(I)V

    if-gt v4, v10, :cond_4

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-ne v4, v10, :cond_0

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    if-le v1, v7, :cond_8

    move v4, v1

    :goto_2
    if-ge v4, v9, :cond_7

    move v1, v2

    :goto_3
    if-gt v1, v8, :cond_5

    new-instance v5, Lmaps/t/ah;

    invoke-direct {v5, p1, v4, v1, p2}, Lmaps/t/ah;-><init>(IIILmaps/t/al;)V

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_5
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_2

    :cond_6
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    :cond_7
    if-gt v3, v7, :cond_0

    move v1, v2

    :goto_4
    if-gt v1, v8, :cond_6

    new-instance v4, Lmaps/t/ah;

    invoke-direct {v4, p1, v3, v1, p2}, Lmaps/t/ah;-><init>(IIILmaps/t/al;)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_8
    move v3, v1

    :goto_5
    if-gt v3, v7, :cond_0

    move v1, v2

    :goto_6
    if-gt v1, v8, :cond_9

    new-instance v4, Lmaps/t/ah;

    invoke-direct {v4, p1, v3, v1, p2}, Lmaps/t/ah;-><init>(IIILmaps/t/al;)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_9
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_5
.end method

.method public static a(IIILmaps/t/al;)Lmaps/t/ah;
    .locals 5

    const/high16 v4, 0x20000000

    const/16 v1, 0x1e

    const/4 v0, 0x0

    if-gtz p0, :cond_0

    new-instance v1, Lmaps/t/ah;

    invoke-direct {v1, v0, v0, v0}, Lmaps/t/ah;-><init>(III)V

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    if-le p0, v1, :cond_1

    move p0, v1

    :cond_1
    rsub-int/lit8 v1, p0, 0x1e

    add-int v2, p1, v4

    shr-int/2addr v2, v1

    neg-int v3, p2

    add-int/2addr v3, v4

    shr-int v1, v3, v1

    const/4 v3, 0x1

    shl-int/2addr v3, p0

    if-gez v2, :cond_3

    add-int/2addr v2, v3

    :cond_2
    :goto_1
    if-gez v1, :cond_4

    :goto_2
    new-instance v1, Lmaps/t/ah;

    invoke-direct {v1, p0, v2, v0, p3}, Lmaps/t/ah;-><init>(IIILmaps/t/al;)V

    move-object v0, v1

    goto :goto_0

    :cond_3
    if-lt v2, v3, :cond_2

    sub-int/2addr v2, v3

    goto :goto_1

    :cond_4
    if-lt v1, v3, :cond_5

    add-int/lit8 v0, v3, -0x1

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_2
.end method

.method public static a(ILmaps/t/bx;)Lmaps/t/ah;
    .locals 2

    invoke-virtual {p1}, Lmaps/t/bx;->f()I

    move-result v0

    invoke-virtual {p1}, Lmaps/t/bx;->g()I

    move-result v1

    invoke-static {p0, v0, v1}, Lmaps/t/ah;->b(III)Lmaps/t/ah;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/io/DataInput;)Lmaps/t/ah;
    .locals 4

    invoke-static {p0}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v0

    invoke-static {p0}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v1

    invoke-static {p0}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v2

    new-instance v3, Lmaps/t/ah;

    invoke-direct {v3, v0, v1, v2}, Lmaps/t/ah;-><init>(III)V

    return-object v3
.end method

.method public static b(Lmaps/t/bw;I)Ljava/util/ArrayList;
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lmaps/t/ah;->b(Lmaps/t/bw;ILmaps/t/al;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lmaps/t/bw;ILmaps/t/al;)Ljava/util/ArrayList;
    .locals 8

    invoke-virtual {p0}, Lmaps/t/bw;->f()Lmaps/t/bx;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/bx;->f()I

    move-result v0

    invoke-virtual {p0}, Lmaps/t/bw;->g()Lmaps/t/bx;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/t/bx;->g()I

    move-result v1

    invoke-static {p1, v0, v1, p2}, Lmaps/t/ah;->b(IIILmaps/t/al;)Lmaps/t/ah;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/t/bw;->g()Lmaps/t/bx;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/t/bx;->f()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0}, Lmaps/t/bw;->f()Lmaps/t/bx;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/t/bx;->g()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {p1, v1, v2, p2}, Lmaps/t/ah;->b(IIILmaps/t/al;)Lmaps/t/ah;

    move-result-object v2

    invoke-virtual {v0}, Lmaps/t/ah;->d()I

    move-result v1

    invoke-virtual {v0}, Lmaps/t/ah;->e()I

    move-result v3

    invoke-virtual {v2}, Lmaps/t/ah;->d()I

    move-result v5

    invoke-virtual {v2}, Lmaps/t/ah;->e()I

    move-result v6

    invoke-virtual {v0}, Lmaps/t/ah;->f()I

    move-result v4

    invoke-virtual {v0}, Lmaps/t/ah;->g()I

    move-result v0

    invoke-static {v4, v0}, Lmaps/t/bx;->e(II)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v2}, Lmaps/t/ah;->f()I

    move-result v0

    invoke-virtual {v2}, Lmaps/t/ah;->g()I

    move-result v2

    invoke-static {v0, v2}, Lmaps/t/bx;->e(II)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :cond_0
    return-object v0

    :cond_1
    const/4 v0, 0x1

    shl-int v2, v0, p1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-le v1, v5, :cond_7

    move v4, v1

    :goto_0
    if-ge v4, v2, :cond_4

    move v1, v3

    :goto_1
    if-gez v1, :cond_2

    new-instance v7, Lmaps/t/ah;

    invoke-direct {v7, p1, v4, v1, p2}, Lmaps/t/ah;-><init>(IIILmaps/t/al;)V

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    move v1, v2

    :goto_2
    if-gt v1, v6, :cond_3

    new-instance v7, Lmaps/t/ah;

    invoke-direct {v7, p1, v4, v1, p2}, Lmaps/t/ah;-><init>(IIILmaps/t/al;)V

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_3
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    move v4, v1

    :goto_3
    if-gt v4, v5, :cond_0

    move v1, v3

    :goto_4
    if-gez v1, :cond_5

    new-instance v7, Lmaps/t/ah;

    invoke-direct {v7, p1, v4, v1, p2}, Lmaps/t/ah;-><init>(IIILmaps/t/al;)V

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_5
    move v1, v2

    :goto_5
    if-gt v1, v6, :cond_6

    new-instance v7, Lmaps/t/ah;

    invoke-direct {v7, p1, v4, v1, p2}, Lmaps/t/ah;-><init>(IIILmaps/t/al;)V

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_6
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_3

    :cond_7
    move v4, v1

    :goto_6
    if-gt v4, v5, :cond_0

    move v1, v3

    :goto_7
    if-gez v1, :cond_8

    new-instance v7, Lmaps/t/ah;

    invoke-direct {v7, p1, v4, v1, p2}, Lmaps/t/ah;-><init>(IIILmaps/t/al;)V

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    :cond_8
    move v1, v2

    :goto_8
    if-gt v1, v6, :cond_9

    new-instance v7, Lmaps/t/ah;

    invoke-direct {v7, p1, v4, v1, p2}, Lmaps/t/ah;-><init>(IIILmaps/t/al;)V

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    :cond_9
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_6
.end method

.method public static b(III)Lmaps/t/ah;
    .locals 4

    const/high16 v3, 0x20000000

    if-ltz p0, :cond_0

    const/16 v0, 0x1e

    if-gt p0, v0, :cond_0

    const/high16 v0, -0x20000000

    if-le p2, v0, :cond_0

    if-le p2, v3, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    rsub-int/lit8 v1, p0, 0x1e

    add-int v0, p1, v3

    shr-int/2addr v0, v1

    neg-int v2, p2

    add-int/2addr v2, v3

    shr-int/2addr v2, v1

    const/4 v1, 0x1

    shl-int/2addr v1, p0

    if-gez v0, :cond_3

    add-int/2addr v0, v1

    :cond_2
    :goto_1
    new-instance v1, Lmaps/t/ah;

    invoke-direct {v1, p0, v0, v2}, Lmaps/t/ah;-><init>(III)V

    move-object v0, v1

    goto :goto_0

    :cond_3
    if-lt v0, v1, :cond_2

    sub-int/2addr v0, v1

    goto :goto_1
.end method

.method private static b(IIILmaps/t/al;)Lmaps/t/ah;
    .locals 4

    const/high16 v3, 0x20000000

    const/16 v0, 0x1e

    const/4 v1, 0x0

    if-gtz p0, :cond_0

    new-instance v0, Lmaps/t/ah;

    invoke-direct {v0, v1, v1, v1}, Lmaps/t/ah;-><init>(III)V

    :goto_0
    return-object v0

    :cond_0
    if-le p0, v0, :cond_1

    move p0, v0

    :cond_1
    rsub-int/lit8 v0, p0, 0x1e

    add-int v1, p1, v3

    shr-int/2addr v1, v0

    neg-int v2, p2

    add-int/2addr v2, v3

    shr-int/2addr v2, v0

    new-instance v0, Lmaps/t/ah;

    invoke-direct {v0, p0, v1, v2, p3}, Lmaps/t/ah;-><init>(IIILmaps/t/al;)V

    goto :goto_0
.end method

.method public static b(ILmaps/t/bx;)Lmaps/t/ah;
    .locals 3

    invoke-virtual {p1}, Lmaps/t/bx;->f()I

    move-result v0

    invoke-virtual {p1}, Lmaps/t/bx;->g()I

    move-result v1

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Lmaps/t/ah;->a(IIILmaps/t/al;)Lmaps/t/ah;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lmaps/t/ah;)I
    .locals 2

    iget v0, p0, Lmaps/t/ah;->d:I

    iget v1, p1, Lmaps/t/ah;->d:I

    if-ne v0, v1, :cond_2

    iget v0, p0, Lmaps/t/ah;->e:I

    iget v1, p1, Lmaps/t/ah;->e:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lmaps/t/ah;->f:I

    iget v1, p1, Lmaps/t/ah;->f:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lmaps/t/ah;->g:Lmaps/t/al;

    iget-object v1, p1, Lmaps/t/ah;->g:Lmaps/t/al;

    invoke-virtual {v0, v1}, Lmaps/t/al;->a(Lmaps/t/al;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lmaps/t/ah;->f:I

    iget v1, p1, Lmaps/t/ah;->f:I

    sub-int/2addr v0, v1

    goto :goto_0

    :cond_1
    iget v0, p0, Lmaps/t/ah;->e:I

    iget v1, p1, Lmaps/t/ah;->e:I

    sub-int/2addr v0, v1

    goto :goto_0

    :cond_2
    iget v0, p0, Lmaps/t/ah;->d:I

    iget v1, p1, Lmaps/t/ah;->d:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public a(I)Lmaps/t/ah;
    .locals 3

    iget v0, p0, Lmaps/t/ah;->d:I

    sub-int/2addr v0, p1

    if-gtz v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    iget v1, p0, Lmaps/t/ah;->e:I

    shr-int/2addr v1, v0

    iget v2, p0, Lmaps/t/ah;->f:I

    shr-int v0, v2, v0

    invoke-virtual {p0, p1, v1, v0}, Lmaps/t/ah;->a(III)Lmaps/t/ah;

    move-result-object p0

    goto :goto_0
.end method

.method public a(III)Lmaps/t/ah;
    .locals 2

    new-instance v0, Lmaps/t/ah;

    iget-object v1, p0, Lmaps/t/ah;->g:Lmaps/t/al;

    invoke-direct {v0, p1, p2, p3, v1}, Lmaps/t/ah;-><init>(IIILmaps/t/al;)V

    return-object v0
.end method

.method public a(Lmaps/o/c;)Lmaps/t/ah;
    .locals 1

    invoke-virtual {p0}, Lmaps/t/ah;->l()Lmaps/t/al;

    move-result-object v0

    invoke-virtual {v0, p1}, Lmaps/t/al;->a(Lmaps/o/c;)Lmaps/t/al;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmaps/t/ah;->a(Lmaps/t/al;)Lmaps/t/ah;

    move-result-object v0

    return-object v0
.end method

.method public a(Lmaps/t/al;)Lmaps/t/ah;
    .locals 4

    new-instance v0, Lmaps/t/ah;

    iget v1, p0, Lmaps/t/ah;->d:I

    iget v2, p0, Lmaps/t/ah;->e:I

    iget v3, p0, Lmaps/t/ah;->f:I

    invoke-direct {v0, v1, v2, v3, p1}, Lmaps/t/ah;-><init>(IIILmaps/t/al;)V

    return-object v0
.end method

.method public a(Lmaps/t/cm;)Lmaps/t/ao;
    .locals 1

    iget-object v0, p0, Lmaps/t/ah;->g:Lmaps/t/al;

    invoke-virtual {v0, p1}, Lmaps/t/al;->a(Lmaps/t/cm;)Lmaps/t/ao;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/io/DataOutput;)V
    .locals 1

    iget v0, p0, Lmaps/t/ah;->d:I

    invoke-static {p1, v0}, Lmaps/t/bq;->a(Ljava/io/DataOutput;I)V

    iget v0, p0, Lmaps/t/ah;->e:I

    invoke-static {p1, v0}, Lmaps/t/bq;->a(Ljava/io/DataOutput;I)V

    iget v0, p0, Lmaps/t/ah;->f:I

    invoke-static {p1, v0}, Lmaps/t/bq;->a(Ljava/io/DataOutput;I)V

    return-void
.end method

.method public a(Lmaps/o/c;Lmaps/bb/c;)V
    .locals 1

    iget-object v0, p0, Lmaps/t/ah;->g:Lmaps/t/al;

    invoke-virtual {v0, p1, p2}, Lmaps/t/al;->a(Lmaps/o/c;Lmaps/bb/c;)V

    return-void
.end method

.method public b()Lmaps/t/ah;
    .locals 5

    iget-object v0, p0, Lmaps/t/ah;->h:Lmaps/t/ah;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/t/ah;

    iget v1, p0, Lmaps/t/ah;->d:I

    iget v2, p0, Lmaps/t/ah;->e:I

    iget v3, p0, Lmaps/t/ah;->f:I

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lmaps/t/ah;-><init>(IIILmaps/t/al;)V

    iput-object v0, p0, Lmaps/t/ah;->h:Lmaps/t/ah;

    :cond_0
    iget-object v0, p0, Lmaps/t/ah;->h:Lmaps/t/ah;

    return-object v0
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lmaps/t/ah;->d:I

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lmaps/t/ah;

    invoke-virtual {p0, p1}, Lmaps/t/ah;->a(Lmaps/t/ah;)I

    move-result v0

    return v0
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lmaps/t/ah;->e:I

    return v0
.end method

.method public e()I
    .locals 1

    iget v0, p0, Lmaps/t/ah;->f:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    if-ne p0, p1, :cond_1

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v1, p1, Lmaps/t/ah;

    if-eqz v1, :cond_0

    check-cast p1, Lmaps/t/ah;

    iget v1, p0, Lmaps/t/ah;->e:I

    iget v2, p1, Lmaps/t/ah;->e:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lmaps/t/ah;->f:I

    iget v2, p1, Lmaps/t/ah;->f:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lmaps/t/ah;->d:I

    iget v2, p1, Lmaps/t/ah;->d:I

    if-ne v1, v2, :cond_0

    iget-object v0, p0, Lmaps/t/ah;->g:Lmaps/t/al;

    iget-object v1, p1, Lmaps/t/ah;->g:Lmaps/t/al;

    invoke-virtual {v0, v1}, Lmaps/t/al;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public f()I
    .locals 1

    iget v0, p0, Lmaps/t/ah;->a:I

    return v0
.end method

.method public g()I
    .locals 1

    iget v0, p0, Lmaps/t/ah;->b:I

    return v0
.end method

.method public h()Lmaps/t/bx;
    .locals 3

    new-instance v0, Lmaps/t/bx;

    iget v1, p0, Lmaps/t/ah;->a:I

    iget v2, p0, Lmaps/t/ah;->b:I

    invoke-direct {v0, v1, v2}, Lmaps/t/bx;-><init>(II)V

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lmaps/t/ah;->d:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lmaps/t/ah;->e:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lmaps/t/ah;->f:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/t/ah;->g:Lmaps/t/al;

    invoke-virtual {v1}, Lmaps/t/al;->b()Z

    move-result v1

    if-nez v1, :cond_0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lmaps/t/ah;->g:Lmaps/t/al;

    invoke-virtual {v1}, Lmaps/t/al;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    return v0
.end method

.method public i()Lmaps/t/bx;
    .locals 4

    const/high16 v0, 0x40000000

    iget v1, p0, Lmaps/t/ah;->d:I

    shr-int/2addr v0, v1

    new-instance v1, Lmaps/t/bx;

    iget v2, p0, Lmaps/t/ah;->a:I

    add-int/2addr v2, v0

    iget v3, p0, Lmaps/t/ah;->b:I

    add-int/2addr v0, v3

    invoke-direct {v1, v2, v0}, Lmaps/t/bx;-><init>(II)V

    return-object v1
.end method

.method public j()Lmaps/t/ax;
    .locals 6

    const/high16 v0, 0x40000000

    iget v1, p0, Lmaps/t/ah;->d:I

    shr-int/2addr v0, v1

    new-instance v1, Lmaps/t/ax;

    new-instance v2, Lmaps/t/bx;

    iget v3, p0, Lmaps/t/ah;->a:I

    iget v4, p0, Lmaps/t/ah;->b:I

    invoke-direct {v2, v3, v4}, Lmaps/t/bx;-><init>(II)V

    new-instance v3, Lmaps/t/bx;

    iget v4, p0, Lmaps/t/ah;->a:I

    add-int/2addr v4, v0

    iget v5, p0, Lmaps/t/ah;->b:I

    add-int/2addr v0, v5

    invoke-direct {v3, v4, v0}, Lmaps/t/bx;-><init>(II)V

    invoke-direct {v1, v2, v3}, Lmaps/t/ax;-><init>(Lmaps/t/bx;Lmaps/t/bx;)V

    return-object v1
.end method

.method public l()Lmaps/t/al;
    .locals 1

    iget-object v0, p0, Lmaps/t/ah;->g:Lmaps/t/al;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lmaps/t/ah;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lmaps/t/ah;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lmaps/t/ah;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lmaps/t/ah;->g:Lmaps/t/al;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
