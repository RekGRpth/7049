.class public Lmaps/t/ai;
.super Ljava/lang/Object;


# instance fields
.field private a:Lmaps/t/an;

.field private b:I

.field private c:[Lmaps/t/bx;

.field private d:Z

.field private final e:Ljava/util/ArrayList;

.field private final f:Ljava/util/ArrayList;

.field private g:I


# direct methods
.method public constructor <init>(Lmaps/t/an;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/t/ai;->e:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/t/ai;->f:Ljava/util/ArrayList;

    invoke-virtual {p0, p1}, Lmaps/t/ai;->a(Lmaps/t/an;)V

    return-void
.end method

.method private a(ILmaps/t/bx;IZ)V
    .locals 5

    const/4 v4, 0x0

    iget v0, p0, Lmaps/t/ai;->b:I

    if-ne p1, v0, :cond_0

    invoke-direct {p0, p2, p3, p4}, Lmaps/t/ai;->a(Lmaps/t/bx;IZ)V

    :goto_0
    return-void

    :cond_0
    if-nez p1, :cond_3

    iget-object v0, p0, Lmaps/t/ai;->a:Lmaps/t/an;

    invoke-virtual {v0}, Lmaps/t/an;->c()Lmaps/t/bx;

    move-result-object v1

    iget-object v0, p0, Lmaps/t/ai;->a:Lmaps/t/an;

    invoke-virtual {v0, v4}, Lmaps/t/an;->a(I)Lmaps/t/bx;

    move-result-object v0

    :goto_1
    invoke-static {v1, v0, p2}, Lmaps/t/cb;->a(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)I

    move-result v2

    if-ltz v2, :cond_4

    if-nez p4, :cond_1

    iget-object v2, p0, Lmaps/t/ai;->c:[Lmaps/t/bx;

    aget-object v2, v2, p1

    invoke-static {v1, v0, v2}, Lmaps/t/cb;->a(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)I

    move-result v2

    if-gez v2, :cond_1

    new-instance v2, Lmaps/t/bx;

    invoke-direct {v2}, Lmaps/t/bx;-><init>()V

    iget-object v3, p0, Lmaps/t/ai;->c:[Lmaps/t/bx;

    aget-object v3, v3, p1

    invoke-static {v1, v0, p2, v3, v2}, Lmaps/t/cb;->a(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)Z

    add-int/lit8 v0, p1, 0x1

    const/4 v1, 0x1

    invoke-direct {p0, v0, v2, p3, v1}, Lmaps/t/ai;->a(ILmaps/t/bx;IZ)V

    :cond_1
    add-int/lit8 v0, p1, 0x1

    invoke-direct {p0, v0, p2, p3, p4}, Lmaps/t/ai;->a(ILmaps/t/bx;IZ)V

    :cond_2
    :goto_2
    iget-object v0, p0, Lmaps/t/ai;->c:[Lmaps/t/bx;

    aget-object v0, v0, p1

    invoke-virtual {v0, p2}, Lmaps/t/bx;->b(Lmaps/t/bx;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lmaps/t/ai;->a:Lmaps/t/an;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, Lmaps/t/an;->a(I)Lmaps/t/bx;

    move-result-object v1

    iget-object v0, p0, Lmaps/t/ai;->a:Lmaps/t/an;

    invoke-virtual {v0, p1}, Lmaps/t/an;->a(I)Lmaps/t/bx;

    move-result-object v0

    goto :goto_1

    :cond_4
    if-nez p4, :cond_2

    iget-object v2, p0, Lmaps/t/ai;->c:[Lmaps/t/bx;

    aget-object v2, v2, p1

    invoke-static {v1, v0, v2}, Lmaps/t/cb;->a(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)I

    move-result v2

    if-ltz v2, :cond_2

    new-instance v2, Lmaps/t/bx;

    invoke-direct {v2}, Lmaps/t/bx;-><init>()V

    iget-object v3, p0, Lmaps/t/ai;->c:[Lmaps/t/bx;

    aget-object v3, v3, p1

    invoke-static {v1, v0, v3, p2, v2}, Lmaps/t/cb;->a(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)Z

    add-int/lit8 v0, p1, 0x1

    invoke-direct {p0, v0, v2, p3, v4}, Lmaps/t/ai;->a(ILmaps/t/bx;IZ)V

    goto :goto_2
.end method

.method private a(Lmaps/t/bx;IZ)V
    .locals 2

    if-eqz p3, :cond_1

    iget v0, p0, Lmaps/t/ai;->g:I

    iget-object v1, p0, Lmaps/t/ai;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lmaps/t/ai;->e:Ljava/util/ArrayList;

    new-instance v1, Lmaps/t/by;

    invoke-direct {v1}, Lmaps/t/by;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/t/ai;->f:Ljava/util/ArrayList;

    new-instance v1, Lmaps/t/ce;

    invoke-direct {v1}, Lmaps/t/ce;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    iget v0, p0, Lmaps/t/ai;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/t/ai;->g:I

    :cond_1
    iget-object v0, p0, Lmaps/t/ai;->e:Ljava/util/ArrayList;

    iget v1, p0, Lmaps/t/ai;->g:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/by;

    invoke-virtual {v0, p1}, Lmaps/t/by;->a(Lmaps/t/bx;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lmaps/t/ai;->d:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/t/ai;->f:Ljava/util/ArrayList;

    iget v1, p0, Lmaps/t/ai;->g:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/ce;

    invoke-virtual {v0, p2}, Lmaps/t/ce;->a(I)V

    :cond_2
    return-void
.end method


# virtual methods
.method public a(Lmaps/t/an;)V
    .locals 3

    iput-object p1, p0, Lmaps/t/ai;->a:Lmaps/t/an;

    invoke-virtual {p1}, Lmaps/t/an;->b()I

    move-result v0

    iput v0, p0, Lmaps/t/ai;->b:I

    iget v0, p0, Lmaps/t/ai;->b:I

    new-array v0, v0, [Lmaps/t/bx;

    iput-object v0, p0, Lmaps/t/ai;->c:[Lmaps/t/bx;

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lmaps/t/ai;->c:[Lmaps/t/bx;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lmaps/t/ai;->c:[Lmaps/t/bx;

    new-instance v2, Lmaps/t/bx;

    invoke-direct {v2}, Lmaps/t/bx;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(Lmaps/t/cg;Ljava/util/List;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2, v0}, Lmaps/t/ai;->a(Lmaps/t/cg;[ILjava/util/List;Ljava/util/List;)V

    return-void
.end method

.method public a(Lmaps/t/cg;[ILjava/util/List;Ljava/util/List;)V
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    iput v2, p0, Lmaps/t/ai;->g:I

    if-eqz p2, :cond_1

    if-eqz p4, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lmaps/t/ai;->d:Z

    invoke-virtual {p1}, Lmaps/t/cg;->a()Lmaps/t/ax;

    move-result-object v0

    iget-object v3, p0, Lmaps/t/ai;->a:Lmaps/t/an;

    invoke-virtual {v3, v0}, Lmaps/t/an;->a(Lmaps/t/an;)Z

    move-result v3

    if-nez v3, :cond_2

    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lmaps/t/ai;->a:Lmaps/t/an;

    invoke-virtual {v3, v0}, Lmaps/t/an;->b(Lmaps/t/an;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-boolean v0, p0, Lmaps/t/ai;->d:Z

    if-eqz v0, :cond_0

    invoke-interface {p4, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    new-instance v4, Lmaps/t/bx;

    invoke-direct {v4}, Lmaps/t/bx;-><init>()V

    invoke-virtual {p1}, Lmaps/t/cg;->b()I

    move-result v5

    invoke-virtual {p1, v2, v4}, Lmaps/t/cg;->a(ILmaps/t/bx;)V

    iget-boolean v0, p0, Lmaps/t/ai;->d:Z

    if-eqz v0, :cond_4

    aget v0, p2, v2

    :goto_2
    invoke-direct {p0, v2, v4, v0, v1}, Lmaps/t/ai;->a(ILmaps/t/bx;IZ)V

    move v3, v1

    :goto_3
    if-ge v3, v5, :cond_6

    invoke-virtual {p1, v3, v4}, Lmaps/t/cg;->a(ILmaps/t/bx;)V

    iget-boolean v0, p0, Lmaps/t/ai;->d:Z

    if-eqz v0, :cond_5

    aget v0, p2, v3

    :goto_4
    invoke-direct {p0, v2, v4, v0, v2}, Lmaps/t/ai;->a(ILmaps/t/bx;IZ)V

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_4

    :cond_6
    :goto_5
    iget v0, p0, Lmaps/t/ai;->g:I

    if-ge v2, v0, :cond_0

    iget-object v0, p0, Lmaps/t/ai;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/by;

    invoke-virtual {v0}, Lmaps/t/by;->a()I

    move-result v3

    if-le v3, v1, :cond_7

    invoke-virtual {v0}, Lmaps/t/by;->c()Lmaps/t/cg;

    move-result-object v3

    invoke-interface {p3, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_7
    invoke-virtual {v0}, Lmaps/t/by;->b()V

    iget-boolean v0, p0, Lmaps/t/ai;->d:Z

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmaps/t/ai;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/ce;

    invoke-virtual {v0}, Lmaps/t/ce;->c()I

    move-result v3

    if-le v3, v1, :cond_8

    invoke-virtual {v0}, Lmaps/t/ce;->a()[I

    move-result-object v3

    invoke-interface {p4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_8
    invoke-virtual {v0}, Lmaps/t/ce;->b()V

    :cond_9
    add-int/lit8 v2, v2, 0x1

    goto :goto_5
.end method
