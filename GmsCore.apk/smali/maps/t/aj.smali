.class public Lmaps/t/aj;
.super Ljava/lang/Object;


# instance fields
.field protected a:Lmaps/t/bx;

.field protected b:F

.field protected c:I

.field protected d:Lmaps/t/bx;

.field protected e:F

.field protected f:Z

.field protected g:Lmaps/t/bb;

.field protected h:Z

.field protected i:F

.field protected j:Z

.field protected k:F


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0}, Lmaps/t/aj;->k()V

    return-void
.end method

.method public constructor <init>(Lmaps/t/bx;FI)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0, p1, p2, p3}, Lmaps/t/aj;->a(Lmaps/t/bx;FI)V

    return-void
.end method


# virtual methods
.method public a()Lmaps/t/bx;
    .locals 1

    iget-object v0, p0, Lmaps/t/aj;->a:Lmaps/t/bx;

    return-object v0
.end method

.method public a(F)V
    .locals 0

    iput p1, p0, Lmaps/t/aj;->b:F

    return-void
.end method

.method public a(I)V
    .locals 0

    iput p1, p0, Lmaps/t/aj;->c:I

    return-void
.end method

.method public final a(Lmaps/t/aj;)V
    .locals 3

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lmaps/t/aj;->k()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p1, Lmaps/t/aj;->a:Lmaps/t/bx;

    iget v1, p1, Lmaps/t/aj;->b:F

    iget v2, p1, Lmaps/t/aj;->c:I

    invoke-virtual {p0, v0, v1, v2}, Lmaps/t/aj;->a(Lmaps/t/bx;FI)V

    iget-object v0, p1, Lmaps/t/aj;->d:Lmaps/t/bx;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    iput-object v0, p0, Lmaps/t/aj;->d:Lmaps/t/bx;

    iget v0, p1, Lmaps/t/aj;->e:F

    iput v0, p0, Lmaps/t/aj;->e:F

    iget-boolean v0, p1, Lmaps/t/aj;->f:Z

    iput-boolean v0, p0, Lmaps/t/aj;->f:Z

    iget-object v0, p1, Lmaps/t/aj;->g:Lmaps/t/bb;

    iput-object v0, p0, Lmaps/t/aj;->g:Lmaps/t/bb;

    iget-boolean v0, p1, Lmaps/t/aj;->h:Z

    iput-boolean v0, p0, Lmaps/t/aj;->h:Z

    iget v0, p1, Lmaps/t/aj;->i:F

    iput v0, p0, Lmaps/t/aj;->i:F

    iget-boolean v0, p1, Lmaps/t/aj;->j:Z

    iput-boolean v0, p0, Lmaps/t/aj;->j:Z

    iget v0, p1, Lmaps/t/aj;->k:F

    iput v0, p0, Lmaps/t/aj;->k:F

    goto :goto_0

    :cond_1
    new-instance v0, Lmaps/t/bx;

    iget-object v1, p1, Lmaps/t/aj;->d:Lmaps/t/bx;

    invoke-direct {v0, v1}, Lmaps/t/bx;-><init>(Lmaps/t/bx;)V

    goto :goto_1
.end method

.method public a(Lmaps/t/bx;)V
    .locals 0

    iput-object p1, p0, Lmaps/t/aj;->a:Lmaps/t/bx;

    return-void
.end method

.method public final a(Lmaps/t/bx;FI)V
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lmaps/t/aj;->a:Lmaps/t/bx;

    iput p2, p0, Lmaps/t/aj;->b:F

    iput p3, p0, Lmaps/t/aj;->c:I

    return-void

    :cond_0
    new-instance v0, Lmaps/t/bx;

    invoke-direct {v0, p1}, Lmaps/t/bx;-><init>(Lmaps/t/bx;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lmaps/t/aj;->f:Z

    return-void
.end method

.method public b()F
    .locals 1

    iget v0, p0, Lmaps/t/aj;->b:F

    return v0
.end method

.method public b(F)V
    .locals 0

    iput p1, p0, Lmaps/t/aj;->i:F

    return-void
.end method

.method public b(Lmaps/t/bx;)V
    .locals 0

    iput-object p1, p0, Lmaps/t/aj;->d:Lmaps/t/bx;

    return-void
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lmaps/t/aj;->c:I

    return v0
.end method

.method public c(F)V
    .locals 2

    const/high16 v0, 0x3f800000

    const/4 v1, 0x0

    invoke-static {v1, p1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, Lmaps/t/aj;->k:F

    return-void
.end method

.method public d()Lmaps/t/bx;
    .locals 2

    iget-object v0, p0, Lmaps/t/aj;->d:Lmaps/t/bx;

    iget-object v1, p0, Lmaps/t/aj;->a:Lmaps/t/bx;

    invoke-static {v0, v1}, Lmaps/ap/e;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/bx;

    return-object v0
.end method

.method public e()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/t/aj;->f:Z

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lmaps/t/aj;

    iget-object v2, p0, Lmaps/t/aj;->a:Lmaps/t/bx;

    iget-object v3, p1, Lmaps/t/aj;->a:Lmaps/t/bx;

    invoke-static {v2, v3}, Lmaps/ap/e;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget v2, p0, Lmaps/t/aj;->b:F

    iget v3, p1, Lmaps/t/aj;->b:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_4

    iget v2, p0, Lmaps/t/aj;->c:I

    iget v3, p1, Lmaps/t/aj;->c:I

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lmaps/t/aj;->d:Lmaps/t/bx;

    iget-object v3, p1, Lmaps/t/aj;->d:Lmaps/t/bx;

    invoke-static {v2, v3}, Lmaps/ap/e;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget v2, p0, Lmaps/t/aj;->e:F

    iget v3, p1, Lmaps/t/aj;->e:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_4

    iget-boolean v2, p0, Lmaps/t/aj;->f:Z

    iget-boolean v3, p1, Lmaps/t/aj;->f:Z

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lmaps/t/aj;->g:Lmaps/t/bb;

    iget-object v3, p1, Lmaps/t/aj;->g:Lmaps/t/bb;

    invoke-static {v2, v3}, Lmaps/ap/e;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-boolean v2, p0, Lmaps/t/aj;->h:Z

    iget-boolean v3, p1, Lmaps/t/aj;->h:Z

    if-ne v2, v3, :cond_4

    iget v2, p0, Lmaps/t/aj;->i:F

    iget v3, p1, Lmaps/t/aj;->i:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_4

    iget-boolean v2, p0, Lmaps/t/aj;->j:Z

    iget-boolean v3, p1, Lmaps/t/aj;->j:Z

    if-ne v2, v3, :cond_4

    iget v2, p0, Lmaps/t/aj;->k:F

    iget v3, p1, Lmaps/t/aj;->k:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public f()Lmaps/t/bb;
    .locals 1

    iget-object v0, p0, Lmaps/t/aj;->g:Lmaps/t/bb;

    return-object v0
.end method

.method public g()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/t/aj;->h:Z

    return v0
.end method

.method public h()F
    .locals 1

    iget v0, p0, Lmaps/t/aj;->i:F

    return v0
.end method

.method public hashCode()I
    .locals 3

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lmaps/t/aj;->a:Lmaps/t/bx;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lmaps/t/aj;->b:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lmaps/t/aj;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lmaps/t/aj;->e:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-boolean v2, p0, Lmaps/t/aj;->f:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lmaps/t/aj;->g:Lmaps/t/bb;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-boolean v2, p0, Lmaps/t/aj;->h:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget v2, p0, Lmaps/t/aj;->i:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-boolean v2, p0, Lmaps/t/aj;->j:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget v2, p0, Lmaps/t/aj;->k:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lmaps/ap/e;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public i()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/t/aj;->j:Z

    return v0
.end method

.method public j()F
    .locals 1

    iget v0, p0, Lmaps/t/aj;->k:F

    return v0
.end method

.method public final k()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    iput-object v3, p0, Lmaps/t/aj;->a:Lmaps/t/bx;

    iput v1, p0, Lmaps/t/aj;->b:F

    const/4 v0, -0x1

    iput v0, p0, Lmaps/t/aj;->c:I

    iput-object v3, p0, Lmaps/t/aj;->d:Lmaps/t/bx;

    iput v1, p0, Lmaps/t/aj;->e:F

    iput-boolean v2, p0, Lmaps/t/aj;->f:Z

    iput-object v3, p0, Lmaps/t/aj;->g:Lmaps/t/bb;

    iput-boolean v2, p0, Lmaps/t/aj;->h:Z

    iput v1, p0, Lmaps/t/aj;->i:F

    iput-boolean v2, p0, Lmaps/t/aj;->j:Z

    const/high16 v0, 0x3f800000

    iput v0, p0, Lmaps/t/aj;->k:F

    return-void
.end method

.method public l()Z
    .locals 1

    iget-object v0, p0, Lmaps/t/aj;->a:Lmaps/t/bx;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Lmaps/ap/e;->a(Ljava/lang/Object;)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "@"

    iget-object v2, p0, Lmaps/t/aj;->a:Lmaps/t/bx;

    invoke-virtual {v2}, Lmaps/t/bx;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/ap/n;

    const-string v1, "Accuracy"

    iget v2, p0, Lmaps/t/aj;->c:I

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;I)Lmaps/ap/n;

    iget-object v1, p0, Lmaps/t/aj;->d:Lmaps/t/bx;

    if-eqz v1, :cond_0

    const-string v1, "Accuracy point"

    iget-object v2, p0, Lmaps/t/aj;->d:Lmaps/t/bx;

    invoke-virtual {v2}, Lmaps/t/bx;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/ap/n;

    :cond_0
    const-string v1, "Accuracy emphasis"

    iget v2, p0, Lmaps/t/aj;->e:F

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;F)Lmaps/ap/n;

    const-string v1, "Use bearing"

    iget-boolean v2, p0, Lmaps/t/aj;->f:Z

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;Z)Lmaps/ap/n;

    iget-boolean v1, p0, Lmaps/t/aj;->f:Z

    if-eqz v1, :cond_1

    const-string v1, "Bearing"

    iget v2, p0, Lmaps/t/aj;->b:F

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;F)Lmaps/ap/n;

    :cond_1
    const-string v1, "Brightness"

    iget v2, p0, Lmaps/t/aj;->k:F

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;F)Lmaps/ap/n;

    const-string v1, "Height"

    iget v2, p0, Lmaps/t/aj;->i:F

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;F)Lmaps/ap/n;

    const-string v1, "Level"

    iget-object v2, p0, Lmaps/t/aj;->g:Lmaps/t/bb;

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/ap/n;

    const-string v1, "Stale"

    iget-boolean v2, p0, Lmaps/t/aj;->j:Z

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;Z)Lmaps/ap/n;

    invoke-virtual {v0}, Lmaps/ap/n;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
