.class public final Lmaps/t/ak;
.super Lmaps/t/an;


# instance fields
.field private a:[Lmaps/t/bx;

.field private b:Lmaps/t/ax;


# direct methods
.method protected constructor <init>([Lmaps/t/bx;)V
    .locals 1

    invoke-direct {p0}, Lmaps/t/an;-><init>()V

    iput-object p1, p0, Lmaps/t/ak;->a:[Lmaps/t/bx;

    invoke-static {p1}, Lmaps/t/ax;->a([Lmaps/t/bx;)Lmaps/t/ax;

    move-result-object v0

    iput-object v0, p0, Lmaps/t/ak;->b:Lmaps/t/ax;

    return-void
.end method

.method public static a(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)Lmaps/t/ak;
    .locals 2

    const/4 v0, 0x4

    new-array v0, v0, [Lmaps/t/bx;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p3, v0, v1

    const/4 v1, 0x3

    aput-object p2, v0, v1

    new-instance v1, Lmaps/t/ak;

    invoke-direct {v1, v0}, Lmaps/t/ak;-><init>([Lmaps/t/bx;)V

    return-object v1
.end method


# virtual methods
.method public a()Lmaps/t/ax;
    .locals 1

    iget-object v0, p0, Lmaps/t/ak;->b:Lmaps/t/ax;

    return-object v0
.end method

.method public a(I)Lmaps/t/bx;
    .locals 1

    iget-object v0, p0, Lmaps/t/ak;->a:[Lmaps/t/bx;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public a(Lmaps/t/bx;)Z
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lmaps/t/ak;->a:[Lmaps/t/bx;

    aget-object v0, v0, v2

    iget-object v3, p0, Lmaps/t/ak;->a:[Lmaps/t/bx;

    aget-object v3, v3, v1

    invoke-static {v0, v3, p1}, Lmaps/t/cb;->b(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_0
    iget-object v3, p0, Lmaps/t/ak;->a:[Lmaps/t/bx;

    aget-object v3, v3, v1

    iget-object v4, p0, Lmaps/t/ak;->a:[Lmaps/t/bx;

    aget-object v4, v4, v5

    invoke-static {v3, v4, p1}, Lmaps/t/cb;->b(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)Z

    move-result v3

    if-eqz v3, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    iget-object v3, p0, Lmaps/t/ak;->a:[Lmaps/t/bx;

    aget-object v3, v3, v5

    iget-object v4, p0, Lmaps/t/ak;->a:[Lmaps/t/bx;

    aget-object v4, v4, v6

    invoke-static {v3, v4, p1}, Lmaps/t/cb;->b(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)Z

    move-result v3

    if-eqz v3, :cond_1

    add-int/lit8 v0, v0, 0x1

    :cond_1
    iget-object v3, p0, Lmaps/t/ak;->a:[Lmaps/t/bx;

    aget-object v3, v3, v6

    iget-object v4, p0, Lmaps/t/ak;->a:[Lmaps/t/bx;

    aget-object v4, v4, v2

    invoke-static {v3, v4, p1}, Lmaps/t/cb;->b(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)Z

    move-result v3

    if-eqz v3, :cond_2

    add-int/lit8 v0, v0, 0x1

    :cond_2
    if-ne v0, v1, :cond_3

    :goto_1
    return v1

    :cond_3
    move v1, v2

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_0
.end method

.method public b()I
    .locals 1

    const/4 v0, 0x4

    return v0
.end method

.method public b(Lmaps/t/an;)Z
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Lmaps/t/ak;->a()Lmaps/t/ax;

    move-result-object v0

    invoke-virtual {p1}, Lmaps/t/an;->a()Lmaps/t/ax;

    move-result-object v2

    invoke-virtual {v0, v2}, Lmaps/t/ax;->a(Lmaps/t/an;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    move v0, v1

    :goto_1
    invoke-virtual {p1}, Lmaps/t/an;->b()I

    move-result v2

    if-ge v0, v2, :cond_2

    invoke-virtual {p1, v0}, Lmaps/t/an;->a(I)Lmaps/t/bx;

    move-result-object v2

    invoke-virtual {p0, v2}, Lmaps/t/ak;->a(Lmaps/t/bx;)Z

    move-result v2

    if-eqz v2, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public c()Lmaps/t/bx;
    .locals 2

    iget-object v0, p0, Lmaps/t/ak;->a:[Lmaps/t/bx;

    const/4 v1, 0x3

    aget-object v0, v0, v1

    return-object v0
.end method

.method public d()Lmaps/t/bx;
    .locals 2

    iget-object v0, p0, Lmaps/t/ak;->a:[Lmaps/t/bx;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    return-object v0
.end method

.method public e()Lmaps/t/bx;
    .locals 2

    iget-object v0, p0, Lmaps/t/ak;->a:[Lmaps/t/bx;

    const/4 v1, 0x3

    aget-object v0, v0, v1

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    instance-of v0, p1, Lmaps/t/ak;

    if-eqz v0, :cond_1

    check-cast p1, Lmaps/t/ak;

    iget-object v0, p0, Lmaps/t/ak;->a:[Lmaps/t/bx;

    iget-object v1, p1, Lmaps/t/ak;->a:[Lmaps/t/bx;

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lmaps/t/ak;->a:[Lmaps/t/bx;

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmaps/t/ak;->a:[Lmaps/t/bx;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmaps/t/ak;->a:[Lmaps/t/bx;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmaps/t/ak;->a:[Lmaps/t/bx;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmaps/t/ak;->a:[Lmaps/t/bx;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
