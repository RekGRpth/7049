.class public Lmaps/t/ar;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/t/bv;


# instance fields
.field private final a:Lmaps/t/ah;

.field private final b:I

.field private final c:B

.field private final d:[Lmaps/t/ca;

.field private final e:Lmaps/t/cl;

.field private final f:[Ljava/lang/String;

.field private final g:J

.field private final h:[Ljava/lang/String;

.field private final i:[Ljava/lang/String;

.field private final j:[Ljava/lang/String;

.field private final k:I

.field private final l:Lmaps/o/c;

.field private final m:[Lmaps/t/cr;

.field private final n:I

.field private o:J


# direct methods
.method protected constructor <init>(Lmaps/t/cl;[Ljava/lang/String;Lmaps/t/ah;IBI[Ljava/lang/String;[Ljava/lang/String;I[Lmaps/t/ca;Lmaps/o/c;[Lmaps/t/cr;JJ)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    iput-object v2, p0, Lmaps/t/ar;->i:[Ljava/lang/String;

    iput-object p1, p0, Lmaps/t/ar;->e:Lmaps/t/cl;

    iput-object p2, p0, Lmaps/t/ar;->f:[Ljava/lang/String;

    iput-object p3, p0, Lmaps/t/ar;->a:Lmaps/t/ah;

    iput p4, p0, Lmaps/t/ar;->b:I

    iput-byte p5, p0, Lmaps/t/ar;->c:B

    iput-object p7, p0, Lmaps/t/ar;->h:[Ljava/lang/String;

    iput-object p8, p0, Lmaps/t/ar;->j:[Ljava/lang/String;

    iput p9, p0, Lmaps/t/ar;->k:I

    iput-object p10, p0, Lmaps/t/ar;->d:[Lmaps/t/ca;

    iput-object p11, p0, Lmaps/t/ar;->l:Lmaps/o/c;

    iput-object p12, p0, Lmaps/t/ar;->m:[Lmaps/t/cr;

    iput p6, p0, Lmaps/t/ar;->n:I

    move-wide/from16 v0, p13

    iput-wide v0, p0, Lmaps/t/ar;->g:J

    move-wide/from16 v0, p15

    iput-wide v0, p0, Lmaps/t/ar;->o:J

    return-void
.end method

.method public static a(Lmaps/t/ah;Ljava/io/DataInput;IBIILmaps/o/c;JJ)Lmaps/t/ar;
    .locals 20

    invoke-static/range {p1 .. p1}, Lmaps/t/ar;->a(Ljava/io/DataInput;)V

    invoke-static/range {p1 .. p1}, Lmaps/t/ah;->a(Ljava/io/DataInput;)Lmaps/t/ah;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/t/ah;->d()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lmaps/t/ah;->d()I

    move-result v3

    if-ne v2, v3, :cond_0

    invoke-virtual {v1}, Lmaps/t/ah;->e()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lmaps/t/ah;->e()I

    move-result v3

    if-ne v2, v3, :cond_0

    invoke-virtual {v1}, Lmaps/t/ah;->c()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lmaps/t/ah;->c()I

    move-result v3

    if-eq v2, v3, :cond_1

    :cond_0
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Expected tile coords: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " but received "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    invoke-interface/range {p1 .. p1}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v12

    if-lez v12, :cond_2

    add-int/lit16 v12, v12, 0x7d0

    :cond_2
    invoke-static/range {p1 .. p1}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v2

    new-array v10, v2, [Ljava/lang/String;

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_3

    invoke-interface/range {p1 .. p1}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v10, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    invoke-static/range {p1 .. p1}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v2

    new-array v11, v2, [Ljava/lang/String;

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_4

    invoke-interface/range {p1 .. p1}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v11, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    const/16 v1, 0xb

    move/from16 v0, p2

    if-ne v0, v1, :cond_5

    invoke-static/range {p1 .. p1}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v2

    const/4 v1, 0x0

    :goto_2
    if-ge v1, v2, :cond_5

    invoke-interface/range {p1 .. p1}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_5
    invoke-static/range {p1 .. p2}, Lmaps/t/cl;->a(Ljava/io/DataInput;I)Lmaps/t/cl;

    move-result-object v4

    const/16 v1, 0xb

    move/from16 v0, p2

    if-ne v0, v1, :cond_7

    const/4 v1, 0x0

    new-array v5, v1, [Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lmaps/t/ci;->a(Ljava/io/DataInput;Lmaps/t/cl;)Lmaps/t/ci;

    move-result-object v6

    :cond_6
    new-instance v1, Lmaps/t/cd;

    move/from16 v2, p2

    move-object/from16 v3, p0

    invoke-direct/range {v1 .. v6}, Lmaps/t/cd;-><init>(ILmaps/t/ah;Lmaps/t/cl;[Ljava/lang/String;Lmaps/t/ci;)V

    invoke-static/range {p1 .. p1}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v3

    new-array v13, v3, [Lmaps/t/ca;

    const/4 v2, 0x0

    :goto_3
    if-ge v2, v3, :cond_8

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lmaps/t/ar;->a(Ljava/io/DataInput;Lmaps/t/cd;)Lmaps/t/ca;

    move-result-object v6

    aput-object v6, v13, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_7
    const/4 v6, 0x0

    invoke-static/range {p1 .. p1}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v2

    new-array v5, v2, [Ljava/lang/String;

    const/4 v1, 0x0

    :goto_4
    if-ge v1, v2, :cond_6

    invoke-interface/range {p1 .. p1}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_8
    invoke-static/range {p1 .. p1}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v3

    new-array v15, v3, [Lmaps/t/cr;

    const/4 v2, 0x0

    :goto_5
    if-ge v2, v3, :cond_9

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lmaps/t/cr;->a(Ljava/io/DataInput;Lmaps/t/cd;)Lmaps/t/cr;

    move-result-object v6

    aput-object v6, v15, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_9
    new-instance v3, Lmaps/t/ar;

    move-object/from16 v6, p0

    move/from16 v7, p4

    move/from16 v8, p3

    move/from16 v9, p5

    move-object/from16 v14, p6

    move-wide/from16 v16, p7

    move-wide/from16 v18, p9

    invoke-direct/range {v3 .. v19}, Lmaps/t/ar;-><init>(Lmaps/t/cl;[Ljava/lang/String;Lmaps/t/ah;IBI[Ljava/lang/String;[Ljava/lang/String;I[Lmaps/t/ca;Lmaps/o/c;[Lmaps/t/cr;JJ)V

    return-object v3
.end method

.method public static a(Lmaps/t/ah;[BILmaps/o/c;JJ)Lmaps/t/ar;
    .locals 14

    invoke-static/range {p1 .. p2}, Lmaps/t/ar;->a([BI)[J

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    aget-wide v3, v0, v1

    long-to-int v8, v3

    const/4 v1, 0x2

    aget-wide v2, v0, v2

    long-to-int v9, v2

    const/4 v2, 0x3

    aget-wide v3, v0, v1

    long-to-int v1, v3

    const/4 v3, 0x4

    aget-wide v4, v0, v2

    long-to-int v2, v4

    const/4 v5, 0x5

    aget-wide v3, v0, v3

    aget-wide v5, v0, v5

    long-to-int v0, v5

    int-to-byte v10, v0

    add-int/lit8 v6, p2, 0x1b

    array-length v0, p1

    sub-int v7, v0, v6

    move-object v0, p0

    move-object v5, p1

    invoke-static/range {v0 .. v7}, Lmaps/t/ar;->a(Lmaps/t/ah;IIJ[BII)V

    :try_start_0
    invoke-static {p1, v6, v7}, Lmaps/be/d;->a([BII)Lmaps/be/a;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/be/a;->a()[B

    move-result-object v13

    invoke-virtual {v0}, Lmaps/be/a;->b()I

    move-result v0

    new-instance v3, Lmaps/cs/a;

    invoke-direct {v3, v13}, Lmaps/cs/a;-><init>([B)V

    move-object v2, p0

    move v4, v1

    move v5, v10

    move v6, v8

    move v7, v9

    move-object/from16 v8, p3

    move-wide/from16 v9, p4

    move-wide/from16 v11, p6

    invoke-static/range {v2 .. v12}, Lmaps/t/ar;->a(Lmaps/t/ah;Ljava/io/DataInput;IBIILmaps/o/c;JJ)Lmaps/t/ar;

    move-result-object v1

    invoke-virtual {v3}, Lmaps/cs/a;->a()I

    move-result v2

    if-eq v2, v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Byte stream not fully read for: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lmaps/t/ar;->g()Lmaps/t/ah;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/util/zip/DataFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    invoke-virtual {v0}, Ljava/util/zip/DataFormatException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    :try_start_1
    invoke-static {v13}, Lmaps/bh/c;->a([B)V
    :try_end_1
    .catch Ljava/util/zip/DataFormatException; {:try_start_1 .. :try_end_1} :catch_0

    return-object v1
.end method

.method static a(Ljava/io/DataInput;Lmaps/t/cd;)Lmaps/t/ca;
    .locals 4

    invoke-static {p0}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v0

    invoke-static {p0, p1}, Lmaps/t/bn;->a(Ljava/io/DataInput;Lmaps/t/cd;)Lmaps/t/bn;

    move-result-object v1

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown feature type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_1
    invoke-static {p0, p1, v1}, Lmaps/t/r;->a(Ljava/io/DataInput;Lmaps/t/cd;Lmaps/t/bn;)Lmaps/t/r;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_2
    invoke-static {p0, p1, v1}, Lmaps/t/bu;->a(Ljava/io/DataInput;Lmaps/t/cd;Lmaps/t/bn;)Lmaps/t/bu;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    invoke-static {p0, p1, v1}, Lmaps/t/co;->a(Ljava/io/DataInput;Lmaps/t/cd;Lmaps/t/bn;)Lmaps/t/co;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    invoke-static {p0, p1, v1}, Lmaps/t/l;->a(Ljava/io/DataInput;Lmaps/t/cd;Lmaps/t/bn;)Lmaps/t/l;

    move-result-object v0

    goto :goto_0

    :pswitch_5
    invoke-static {p0, p1, v1}, Lmaps/t/ay;->a(Ljava/io/DataInput;Lmaps/t/cd;Lmaps/t/bn;)Lmaps/t/ay;

    move-result-object v0

    goto :goto_0

    :pswitch_6
    invoke-static {p0, p1, v1}, Lmaps/t/ae;->a(Ljava/io/DataInput;Lmaps/t/cd;Lmaps/t/bn;)Lmaps/t/ae;

    move-result-object v0

    goto :goto_0

    :pswitch_7
    invoke-static {p0, p1, v1}, Lmaps/t/n;->a(Ljava/io/DataInput;Lmaps/t/cd;Lmaps/t/bn;)Lmaps/t/n;

    move-result-object v0

    goto :goto_0

    :pswitch_8
    invoke-static {p0, p1, v1}, Lmaps/t/az;->a(Ljava/io/DataInput;Lmaps/t/cd;Lmaps/t/bn;)Lmaps/t/az;

    move-result-object v0

    goto :goto_0

    :pswitch_9
    invoke-static {p0, p1, v1}, Lmaps/t/p;->a(Ljava/io/DataInput;Lmaps/t/cd;Lmaps/t/bn;)Lmaps/t/ca;

    move-result-object v0

    goto :goto_0

    :pswitch_a
    invoke-static {p0, p1, v1}, Lmaps/t/i;->a(Ljava/io/DataInput;Lmaps/t/cd;Lmaps/t/bn;)Lmaps/t/az;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_a
    .end packed-switch
.end method

.method public static a(II[B)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p2, v0}, Lmaps/t/ab;->a(I[BI)V

    const/4 v0, 0x4

    invoke-static {p1, p2, v0}, Lmaps/t/ab;->a(I[BI)V

    return-void
.end method

.method private static a(Ljava/io/DataInput;)V
    .locals 4

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v0

    const v1, 0x44524154

    if-eq v0, v1, :cond_0

    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TILE_MAGIC expected. Found: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    return-void
.end method

.method private static a(Lmaps/t/ah;IIJ[BII)V
    .locals 12

    new-instance v11, Lmaps/an/a;

    invoke-direct {v11}, Lmaps/an/a;-><init>()V

    const/16 v3, 0x28

    new-array v10, v3, [B

    invoke-virtual {p0}, Lmaps/t/ah;->d()I

    move-result v3

    invoke-virtual {p0}, Lmaps/t/ah;->e()I

    move-result v4

    invoke-virtual {p0}, Lmaps/t/ah;->c()I

    move-result v5

    move v6, p1

    move v7, p2

    move-wide v8, p3

    invoke-static/range {v3 .. v10}, Lmaps/an/a;->a(IIIIIJ[B)V

    const/16 v3, 0x100

    invoke-virtual {v11, v10, v3}, Lmaps/an/a;->a([BI)V

    move-object/from16 v0, p5

    move/from16 v1, p6

    move/from16 v2, p7

    invoke-virtual {v11, v0, v1, v2}, Lmaps/an/a;->a([BII)V

    return-void
.end method

.method public static a(Lmaps/t/o;)Z
    .locals 2

    invoke-interface {p0}, Lmaps/t/o;->h()Lmaps/o/c;

    move-result-object v0

    sget-object v1, Lmaps/o/c;->q:Lmaps/o/c;

    if-ne v0, v1, :cond_0

    move-object v0, p0

    check-cast v0, Lmaps/t/ar;

    invoke-virtual {v0}, Lmaps/t/ar;->q()I

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    instance-of v0, p0, Lmaps/t/ck;

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a([BI)[J
    .locals 11

    new-instance v0, Lmaps/cs/a;

    invoke-direct {v0, p0}, Lmaps/cs/a;-><init>([B)V

    invoke-virtual {v0, p1}, Lmaps/cs/a;->skipBytes(I)I

    invoke-virtual {v0}, Lmaps/cs/a;->readInt()I

    move-result v1

    invoke-virtual {v0}, Lmaps/cs/a;->readInt()I

    move-result v2

    invoke-static {v0}, Lmaps/t/ar;->a(Ljava/io/DataInput;)V

    invoke-virtual {v0}, Lmaps/cs/a;->readUnsignedShort()I

    move-result v3

    const/16 v4, 0xa

    if-eq v3, v4, :cond_0

    const/16 v4, 0xb

    if-eq v3, v4, :cond_0

    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Version mismatch: 10 or 11 expected, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " found"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {v0}, Lmaps/cs/a;->readInt()I

    move-result v4

    invoke-virtual {v0}, Lmaps/cs/a;->readLong()J

    move-result-wide v5

    invoke-virtual {v0}, Lmaps/cs/a;->readUnsignedByte()I

    move-result v0

    const/4 v7, 0x6

    new-array v7, v7, [J

    const/4 v8, 0x0

    int-to-long v9, v1

    aput-wide v9, v7, v8

    const/4 v1, 0x1

    int-to-long v8, v2

    aput-wide v8, v7, v1

    const/4 v1, 0x2

    int-to-long v2, v3

    aput-wide v2, v7, v1

    const/4 v1, 0x3

    int-to-long v2, v4

    aput-wide v2, v7, v1

    const/4 v1, 0x4

    aput-wide v5, v7, v1

    const/4 v1, 0x5

    int-to-long v2, v0

    aput-wide v2, v7, v1

    return-object v7
.end method

.method static synthetic b(Lmaps/t/ar;)[Lmaps/t/ca;
    .locals 1

    iget-object v0, p0, Lmaps/t/ar;->d:[Lmaps/t/ca;

    return-object v0
.end method

.method public static s()J
    .locals 2

    invoke-static {}, Lmaps/k/b;->a()Lmaps/k/b;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/k/b;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/az/c;->c()Lmaps/az/a;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/az/c;->c()Lmaps/az/a;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/az/a;->b()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public static t()I
    .locals 4

    invoke-static {}, Lmaps/t/ar;->s()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-static {}, Lmaps/t/ar;->s()J

    move-result-wide v0

    const-wide/32 v2, 0x36ee80

    div-long/2addr v0, v2

    long-to-int v0, v0

    goto :goto_0
.end method

.method public static u()I
    .locals 1

    const/16 v0, 0x8

    return v0
.end method


# virtual methods
.method public a(I)Lmaps/t/ca;
    .locals 1

    iget-object v0, p0, Lmaps/t/ar;->d:[Lmaps/t/ca;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public a(Lmaps/ae/d;)Z
    .locals 4

    iget-wide v0, p0, Lmaps/t/ar;->g:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    invoke-interface {p1}, Lmaps/ae/d;->b()J

    move-result-wide v0

    iget-wide v2, p0, Lmaps/t/ar;->g:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/t/ar;->h:[Ljava/lang/String;

    return-object v0
.end method

.method public b(I)Lmaps/t/cr;
    .locals 1

    iget-object v0, p0, Lmaps/t/ar;->m:[Lmaps/t/cr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/t/ar;->m:[Lmaps/t/cr;

    aget-object v0, v0, p1

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lmaps/ae/d;)Z
    .locals 4

    iget-wide v0, p0, Lmaps/t/ar;->o:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    invoke-interface {p1}, Lmaps/ae/d;->b()J

    move-result-wide v0

    iget-wide v2, p0, Lmaps/t/ar;->o:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/t/ar;->j:[Ljava/lang/String;

    return-object v0
.end method

.method public c()I
    .locals 1

    iget-object v0, p0, Lmaps/t/ar;->d:[Lmaps/t/ca;

    array-length v0, v0

    return v0
.end method

.method public c(Lmaps/ae/d;)V
    .locals 4

    invoke-static {}, Lmaps/t/ar;->s()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    invoke-interface {p1}, Lmaps/ae/d;->b()J

    move-result-wide v0

    invoke-static {}, Lmaps/t/ar;->s()J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lmaps/t/ar;->o:J

    :goto_0
    return-void

    :cond_0
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lmaps/t/ar;->o:J

    goto :goto_0
.end method

.method public d()Lmaps/t/h;
    .locals 2

    new-instance v0, Lmaps/t/at;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lmaps/t/at;-><init>(Lmaps/t/ar;Lmaps/t/aw;)V

    return-object v0
.end method

.method public f()J
    .locals 2

    iget-wide v0, p0, Lmaps/t/ar;->g:J

    return-wide v0
.end method

.method public g()Lmaps/t/ah;
    .locals 1

    iget-object v0, p0, Lmaps/t/ar;->a:Lmaps/t/ah;

    return-object v0
.end method

.method public h()Lmaps/o/c;
    .locals 1

    iget-object v0, p0, Lmaps/t/ar;->l:Lmaps/o/c;

    return-object v0
.end method

.method public i()I
    .locals 1

    iget v0, p0, Lmaps/t/ar;->b:I

    return v0
.end method

.method public j()I
    .locals 1

    iget v0, p0, Lmaps/t/ar;->n:I

    return v0
.end method

.method public k()Z
    .locals 3

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-static {}, Lmaps/az/c;->c()Lmaps/az/a;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lmaps/az/c;->c()Lmaps/az/a;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/az/a;->d()Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_0
    invoke-virtual {p0}, Lmaps/t/ar;->m()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lmaps/t/ar;->n()Z

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    move v0, v1

    :cond_2
    move v1, v0

    :cond_3
    :goto_0
    return v1

    :cond_4
    invoke-virtual {p0}, Lmaps/t/ar;->n()Z

    move-result v2

    if-eqz v2, :cond_3

    move v1, v0

    goto :goto_0
.end method

.method public l()B
    .locals 1

    iget-byte v0, p0, Lmaps/t/ar;->c:B

    return v0
.end method

.method public m()Z
    .locals 1

    iget-byte v0, p0, Lmaps/t/ar;->c:B

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()Z
    .locals 1

    iget-byte v0, p0, Lmaps/t/ar;->c:B

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o()I
    .locals 1

    iget v0, p0, Lmaps/t/ar;->k:I

    return v0
.end method

.method protected p()[Lmaps/t/ca;
    .locals 1

    iget-object v0, p0, Lmaps/t/ar;->d:[Lmaps/t/ca;

    return-object v0
.end method

.method public q()I
    .locals 1

    iget-object v0, p0, Lmaps/t/ar;->m:[Lmaps/t/cr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/t/ar;->m:[Lmaps/t/cr;

    array-length v0, v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public r()J
    .locals 2

    iget-wide v0, p0, Lmaps/t/ar;->o:J

    return-wide v0
.end method
