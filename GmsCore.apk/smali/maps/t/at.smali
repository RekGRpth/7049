.class Lmaps/t/at;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/t/h;


# instance fields
.field final synthetic a:Lmaps/t/ar;

.field private b:I

.field private c:I


# direct methods
.method private constructor <init>(Lmaps/t/ar;)V
    .locals 1

    const/4 v0, 0x0

    iput-object p1, p0, Lmaps/t/at;->a:Lmaps/t/ar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lmaps/t/at;->b:I

    iput v0, p0, Lmaps/t/at;->c:I

    return-void
.end method

.method synthetic constructor <init>(Lmaps/t/ar;Lmaps/t/aw;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/t/at;-><init>(Lmaps/t/ar;)V

    return-void
.end method


# virtual methods
.method public a()Lmaps/t/ca;
    .locals 2

    iget-object v0, p0, Lmaps/t/at;->a:Lmaps/t/ar;

    invoke-static {v0}, Lmaps/t/ar;->b(Lmaps/t/ar;)[Lmaps/t/ca;

    move-result-object v0

    iget v1, p0, Lmaps/t/at;->b:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public b()V
    .locals 1

    iget v0, p0, Lmaps/t/at;->b:I

    iput v0, p0, Lmaps/t/at;->c:I

    return-void
.end method

.method public c()V
    .locals 1

    iget v0, p0, Lmaps/t/at;->c:I

    iput v0, p0, Lmaps/t/at;->b:I

    return-void
.end method

.method public d()Lmaps/t/ca;
    .locals 3

    iget-object v0, p0, Lmaps/t/at;->a:Lmaps/t/ar;

    invoke-static {v0}, Lmaps/t/ar;->b(Lmaps/t/ar;)[Lmaps/t/ca;

    move-result-object v0

    iget v1, p0, Lmaps/t/at;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lmaps/t/at;->b:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public hasNext()Z
    .locals 2

    iget v0, p0, Lmaps/t/at;->b:I

    iget-object v1, p0, Lmaps/t/at;->a:Lmaps/t/ar;

    invoke-static {v1}, Lmaps/t/ar;->b(Lmaps/t/ar;)[Lmaps/t/ca;

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lmaps/t/at;->d()Lmaps/t/ca;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "remove() not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
