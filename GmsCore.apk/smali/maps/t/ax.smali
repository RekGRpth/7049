.class public final Lmaps/t/ax;
.super Lmaps/t/an;


# instance fields
.field protected a:Lmaps/t/bx;

.field protected b:Lmaps/t/bx;

.field private volatile c:Lmaps/t/bx;

.field private volatile d:Lmaps/t/bx;


# direct methods
.method public constructor <init>(Lmaps/t/bx;Lmaps/t/bx;)V
    .locals 0

    invoke-direct {p0}, Lmaps/t/an;-><init>()V

    iput-object p1, p0, Lmaps/t/ax;->a:Lmaps/t/bx;

    iput-object p2, p0, Lmaps/t/ax;->b:Lmaps/t/bx;

    return-void
.end method

.method public static a(Lmaps/t/bx;I)Lmaps/t/ax;
    .locals 4

    new-instance v0, Lmaps/t/bx;

    iget v1, p0, Lmaps/t/bx;->a:I

    sub-int/2addr v1, p1

    iget v2, p0, Lmaps/t/bx;->b:I

    sub-int/2addr v2, p1

    invoke-direct {v0, v1, v2}, Lmaps/t/bx;-><init>(II)V

    new-instance v1, Lmaps/t/bx;

    iget v2, p0, Lmaps/t/bx;->a:I

    add-int/2addr v2, p1

    iget v3, p0, Lmaps/t/bx;->b:I

    add-int/2addr v3, p1

    invoke-direct {v1, v2, v3}, Lmaps/t/bx;-><init>(II)V

    new-instance v2, Lmaps/t/ax;

    invoke-direct {v2, v0, v1}, Lmaps/t/ax;-><init>(Lmaps/t/bx;Lmaps/t/bx;)V

    return-object v2
.end method

.method public static a(Lmaps/t/bx;Lmaps/t/bx;)Lmaps/t/ax;
    .locals 6

    iget v0, p0, Lmaps/t/bx;->a:I

    iget v1, p1, Lmaps/t/bx;->a:I

    if-ge v0, v1, :cond_0

    iget v1, p0, Lmaps/t/bx;->a:I

    iget v0, p1, Lmaps/t/bx;->a:I

    :goto_0
    iget v2, p0, Lmaps/t/bx;->b:I

    iget v3, p1, Lmaps/t/bx;->b:I

    if-ge v2, v3, :cond_1

    iget v3, p0, Lmaps/t/bx;->b:I

    iget v2, p1, Lmaps/t/bx;->b:I

    :goto_1
    new-instance v4, Lmaps/t/ax;

    new-instance v5, Lmaps/t/bx;

    invoke-direct {v5, v1, v3}, Lmaps/t/bx;-><init>(II)V

    new-instance v1, Lmaps/t/bx;

    invoke-direct {v1, v0, v2}, Lmaps/t/bx;-><init>(II)V

    invoke-direct {v4, v5, v1}, Lmaps/t/ax;-><init>(Lmaps/t/bx;Lmaps/t/bx;)V

    return-object v4

    :cond_0
    iget v1, p1, Lmaps/t/bx;->a:I

    iget v0, p0, Lmaps/t/bx;->a:I

    goto :goto_0

    :cond_1
    iget v3, p1, Lmaps/t/bx;->b:I

    iget v2, p0, Lmaps/t/bx;->b:I

    goto :goto_1
.end method

.method public static a(Lmaps/t/cg;)Lmaps/t/ax;
    .locals 7

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lmaps/t/cg;->a(I)Lmaps/t/bx;

    move-result-object v5

    iget v2, v5, Lmaps/t/bx;->a:I

    iget v1, v5, Lmaps/t/bx;->b:I

    const/4 v0, 0x1

    move v3, v2

    move v4, v2

    move v2, v1

    :goto_0
    invoke-virtual {p0}, Lmaps/t/cg;->b()I

    move-result v6

    if-ge v0, v6, :cond_4

    invoke-virtual {p0, v0, v5}, Lmaps/t/cg;->a(ILmaps/t/bx;)V

    iget v6, v5, Lmaps/t/bx;->a:I

    if-ge v6, v4, :cond_0

    iget v4, v5, Lmaps/t/bx;->a:I

    :cond_0
    iget v6, v5, Lmaps/t/bx;->a:I

    if-le v6, v3, :cond_1

    iget v3, v5, Lmaps/t/bx;->a:I

    :cond_1
    iget v6, v5, Lmaps/t/bx;->b:I

    if-ge v6, v2, :cond_2

    iget v2, v5, Lmaps/t/bx;->b:I

    :cond_2
    iget v6, v5, Lmaps/t/bx;->b:I

    if-le v6, v1, :cond_3

    iget v1, v5, Lmaps/t/bx;->b:I

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    invoke-virtual {v5, v4, v2}, Lmaps/t/bx;->d(II)V

    new-instance v0, Lmaps/t/bx;

    invoke-direct {v0, v3, v1}, Lmaps/t/bx;-><init>(II)V

    new-instance v1, Lmaps/t/ax;

    invoke-direct {v1, v5, v0}, Lmaps/t/ax;-><init>(Lmaps/t/bx;Lmaps/t/bx;)V

    return-object v1
.end method

.method public static a([Lmaps/t/bx;)Lmaps/t/ax;
    .locals 7

    const/4 v0, 0x0

    aget-object v0, p0, v0

    iget v2, v0, Lmaps/t/bx;->a:I

    iget v1, v0, Lmaps/t/bx;->b:I

    const/4 v0, 0x1

    move v3, v2

    move v4, v2

    move v2, v1

    :goto_0
    array-length v5, p0

    if-ge v0, v5, :cond_4

    aget-object v5, p0, v0

    iget v6, v5, Lmaps/t/bx;->a:I

    if-ge v6, v4, :cond_0

    iget v4, v5, Lmaps/t/bx;->a:I

    :cond_0
    iget v6, v5, Lmaps/t/bx;->a:I

    if-le v6, v3, :cond_1

    iget v3, v5, Lmaps/t/bx;->a:I

    :cond_1
    iget v6, v5, Lmaps/t/bx;->b:I

    if-ge v6, v2, :cond_2

    iget v2, v5, Lmaps/t/bx;->b:I

    :cond_2
    iget v6, v5, Lmaps/t/bx;->b:I

    if-le v6, v1, :cond_3

    iget v1, v5, Lmaps/t/bx;->b:I

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    new-instance v0, Lmaps/t/ax;

    new-instance v5, Lmaps/t/bx;

    invoke-direct {v5, v4, v2}, Lmaps/t/bx;-><init>(II)V

    new-instance v2, Lmaps/t/bx;

    invoke-direct {v2, v3, v1}, Lmaps/t/bx;-><init>(II)V

    invoke-direct {v0, v5, v2}, Lmaps/t/ax;-><init>(Lmaps/t/bx;Lmaps/t/bx;)V

    return-object v0
.end method


# virtual methods
.method public a()Lmaps/t/ax;
    .locals 0

    return-object p0
.end method

.method public a(I)Lmaps/t/bx;
    .locals 3

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lmaps/t/ax;->c:Lmaps/t/bx;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/t/bx;

    iget-object v1, p0, Lmaps/t/ax;->b:Lmaps/t/bx;

    iget v1, v1, Lmaps/t/bx;->a:I

    iget-object v2, p0, Lmaps/t/ax;->a:Lmaps/t/bx;

    iget v2, v2, Lmaps/t/bx;->b:I

    invoke-direct {v0, v1, v2}, Lmaps/t/bx;-><init>(II)V

    iput-object v0, p0, Lmaps/t/ax;->c:Lmaps/t/bx;

    :cond_0
    iget-object v0, p0, Lmaps/t/ax;->c:Lmaps/t/bx;

    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, Lmaps/t/ax;->b:Lmaps/t/bx;

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lmaps/t/ax;->d:Lmaps/t/bx;

    if-nez v0, :cond_1

    new-instance v0, Lmaps/t/bx;

    iget-object v1, p0, Lmaps/t/ax;->a:Lmaps/t/bx;

    iget v1, v1, Lmaps/t/bx;->a:I

    iget-object v2, p0, Lmaps/t/ax;->b:Lmaps/t/bx;

    iget v2, v2, Lmaps/t/bx;->b:I

    invoke-direct {v0, v1, v2}, Lmaps/t/bx;-><init>(II)V

    iput-object v0, p0, Lmaps/t/ax;->d:Lmaps/t/bx;

    :cond_1
    iget-object v0, p0, Lmaps/t/ax;->d:Lmaps/t/bx;

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lmaps/t/ax;->a:Lmaps/t/bx;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method a(Lmaps/t/ax;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lmaps/t/ax;->a:Lmaps/t/bx;

    iget-object v1, p0, Lmaps/t/ax;->a:Lmaps/t/bx;

    iget v1, v1, Lmaps/t/bx;->a:I

    iget-object v2, p1, Lmaps/t/ax;->a:Lmaps/t/bx;

    iget v2, v2, Lmaps/t/bx;->a:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, v0, Lmaps/t/bx;->a:I

    iget-object v0, p0, Lmaps/t/ax;->a:Lmaps/t/bx;

    iget-object v1, p0, Lmaps/t/ax;->a:Lmaps/t/bx;

    iget v1, v1, Lmaps/t/bx;->b:I

    iget-object v2, p1, Lmaps/t/ax;->a:Lmaps/t/bx;

    iget v2, v2, Lmaps/t/bx;->b:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, v0, Lmaps/t/bx;->b:I

    iget-object v0, p0, Lmaps/t/ax;->b:Lmaps/t/bx;

    iget-object v1, p0, Lmaps/t/ax;->b:Lmaps/t/bx;

    iget v1, v1, Lmaps/t/bx;->a:I

    iget-object v2, p1, Lmaps/t/ax;->b:Lmaps/t/bx;

    iget v2, v2, Lmaps/t/bx;->a:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, v0, Lmaps/t/bx;->a:I

    iget-object v0, p0, Lmaps/t/ax;->b:Lmaps/t/bx;

    iget-object v1, p0, Lmaps/t/ax;->b:Lmaps/t/bx;

    iget v1, v1, Lmaps/t/bx;->b:I

    iget-object v2, p1, Lmaps/t/ax;->b:Lmaps/t/bx;

    iget v2, v2, Lmaps/t/bx;->b:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, v0, Lmaps/t/bx;->b:I

    iput-object v3, p0, Lmaps/t/ax;->c:Lmaps/t/bx;

    iput-object v3, p0, Lmaps/t/ax;->d:Lmaps/t/bx;

    return-void
.end method

.method public a(Lmaps/t/an;)Z
    .locals 2

    instance-of v0, p1, Lmaps/t/ax;

    if-eqz v0, :cond_1

    check-cast p1, Lmaps/t/ax;

    iget-object v0, p0, Lmaps/t/ax;->a:Lmaps/t/bx;

    iget v0, v0, Lmaps/t/bx;->a:I

    iget-object v1, p1, Lmaps/t/ax;->b:Lmaps/t/bx;

    iget v1, v1, Lmaps/t/bx;->a:I

    if-gt v0, v1, :cond_0

    iget-object v0, p0, Lmaps/t/ax;->a:Lmaps/t/bx;

    iget v0, v0, Lmaps/t/bx;->b:I

    iget-object v1, p1, Lmaps/t/ax;->b:Lmaps/t/bx;

    iget v1, v1, Lmaps/t/bx;->b:I

    if-gt v0, v1, :cond_0

    iget-object v0, p0, Lmaps/t/ax;->b:Lmaps/t/bx;

    iget v0, v0, Lmaps/t/bx;->a:I

    iget-object v1, p1, Lmaps/t/ax;->a:Lmaps/t/bx;

    iget v1, v1, Lmaps/t/bx;->a:I

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lmaps/t/ax;->b:Lmaps/t/bx;

    iget v0, v0, Lmaps/t/bx;->b:I

    iget-object v1, p1, Lmaps/t/ax;->a:Lmaps/t/bx;

    iget v1, v1, Lmaps/t/bx;->b:I

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-super {p0, p1}, Lmaps/t/an;->a(Lmaps/t/an;)Z

    move-result v0

    goto :goto_0
.end method

.method public a(Lmaps/t/bx;)Z
    .locals 2

    iget v0, p1, Lmaps/t/bx;->a:I

    iget-object v1, p0, Lmaps/t/ax;->a:Lmaps/t/bx;

    iget v1, v1, Lmaps/t/bx;->a:I

    if-lt v0, v1, :cond_0

    iget v0, p1, Lmaps/t/bx;->a:I

    iget-object v1, p0, Lmaps/t/ax;->b:Lmaps/t/bx;

    iget v1, v1, Lmaps/t/bx;->a:I

    if-gt v0, v1, :cond_0

    iget v0, p1, Lmaps/t/bx;->b:I

    iget-object v1, p0, Lmaps/t/ax;->a:Lmaps/t/bx;

    iget v1, v1, Lmaps/t/bx;->b:I

    if-lt v0, v1, :cond_0

    iget v0, p1, Lmaps/t/bx;->b:I

    iget-object v1, p0, Lmaps/t/ax;->b:Lmaps/t/bx;

    iget v1, v1, Lmaps/t/bx;->b:I

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    const/4 v0, 0x4

    return v0
.end method

.method public b(Lmaps/t/an;)Z
    .locals 3

    invoke-virtual {p1}, Lmaps/t/an;->a()Lmaps/t/ax;

    move-result-object v0

    iget-object v1, p0, Lmaps/t/ax;->a:Lmaps/t/bx;

    iget v1, v1, Lmaps/t/bx;->a:I

    iget-object v2, v0, Lmaps/t/ax;->a:Lmaps/t/bx;

    iget v2, v2, Lmaps/t/bx;->a:I

    if-gt v1, v2, :cond_0

    iget-object v1, p0, Lmaps/t/ax;->a:Lmaps/t/bx;

    iget v1, v1, Lmaps/t/bx;->b:I

    iget-object v2, v0, Lmaps/t/ax;->a:Lmaps/t/bx;

    iget v2, v2, Lmaps/t/bx;->b:I

    if-gt v1, v2, :cond_0

    iget-object v1, p0, Lmaps/t/ax;->b:Lmaps/t/bx;

    iget v1, v1, Lmaps/t/bx;->a:I

    iget-object v2, v0, Lmaps/t/ax;->b:Lmaps/t/bx;

    iget v2, v2, Lmaps/t/bx;->a:I

    if-lt v1, v2, :cond_0

    iget-object v1, p0, Lmaps/t/ax;->b:Lmaps/t/bx;

    iget v1, v1, Lmaps/t/bx;->b:I

    iget-object v0, v0, Lmaps/t/ax;->b:Lmaps/t/bx;

    iget v0, v0, Lmaps/t/bx;->b:I

    if-lt v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Lmaps/t/bx;
    .locals 1

    iget-object v0, p0, Lmaps/t/ax;->a:Lmaps/t/bx;

    return-object v0
.end method

.method public d()Lmaps/t/bx;
    .locals 1

    iget-object v0, p0, Lmaps/t/ax;->a:Lmaps/t/bx;

    return-object v0
.end method

.method public e()Lmaps/t/bx;
    .locals 1

    iget-object v0, p0, Lmaps/t/ax;->b:Lmaps/t/bx;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lmaps/t/ax;

    if-eqz v2, :cond_3

    check-cast p1, Lmaps/t/ax;

    iget-object v2, p1, Lmaps/t/ax;->b:Lmaps/t/bx;

    iget-object v3, p0, Lmaps/t/ax;->b:Lmaps/t/bx;

    invoke-virtual {v2, v3}, Lmaps/t/bx;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p1, Lmaps/t/ax;->a:Lmaps/t/bx;

    iget-object v3, p0, Lmaps/t/ax;->a:Lmaps/t/bx;

    invoke-virtual {v2, v3}, Lmaps/t/bx;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public f()Lmaps/t/bx;
    .locals 4

    new-instance v0, Lmaps/t/bx;

    iget-object v1, p0, Lmaps/t/ax;->a:Lmaps/t/bx;

    iget v1, v1, Lmaps/t/bx;->a:I

    iget-object v2, p0, Lmaps/t/ax;->b:Lmaps/t/bx;

    iget v2, v2, Lmaps/t/bx;->a:I

    add-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lmaps/t/ax;->a:Lmaps/t/bx;

    iget v2, v2, Lmaps/t/bx;->b:I

    iget-object v3, p0, Lmaps/t/ax;->b:Lmaps/t/bx;

    iget v3, v3, Lmaps/t/bx;->b:I

    add-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    invoke-direct {v0, v1, v2}, Lmaps/t/bx;-><init>(II)V

    return-object v0
.end method

.method public g()I
    .locals 2

    iget-object v0, p0, Lmaps/t/ax;->b:Lmaps/t/bx;

    iget v0, v0, Lmaps/t/bx;->a:I

    iget-object v1, p0, Lmaps/t/ax;->a:Lmaps/t/bx;

    iget v1, v1, Lmaps/t/bx;->a:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public h()I
    .locals 2

    iget-object v0, p0, Lmaps/t/ax;->b:Lmaps/t/bx;

    iget v0, v0, Lmaps/t/bx;->b:I

    iget-object v1, p0, Lmaps/t/ax;->a:Lmaps/t/bx;

    iget v1, v1, Lmaps/t/bx;->b:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lmaps/t/ax;->b:Lmaps/t/bx;

    invoke-virtual {v0}, Lmaps/t/bx;->hashCode()I

    move-result v0

    add-int/lit8 v0, v0, 0x1f

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lmaps/t/ax;->a:Lmaps/t/bx;

    invoke-virtual {v1}, Lmaps/t/bx;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmaps/t/ax;->a:Lmaps/t/bx;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmaps/t/ax;->b:Lmaps/t/bx;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
