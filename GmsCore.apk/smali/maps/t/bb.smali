.class public final Lmaps/t/bb;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lmaps/t/bg;

.field private final b:I


# direct methods
.method public constructor <init>(Lmaps/t/bg;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/t/bb;->a:Lmaps/t/bg;

    iput p2, p0, Lmaps/t/bb;->b:I

    return-void
.end method


# virtual methods
.method public a()Lmaps/t/bg;
    .locals 1

    iget-object v0, p0, Lmaps/t/bb;->a:Lmaps/t/bg;

    return-object v0
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lmaps/t/bb;->b:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    instance-of v0, p1, Lmaps/t/bb;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/t/bb;->a:Lmaps/t/bg;

    check-cast p1, Lmaps/t/bb;

    iget-object v1, p1, Lmaps/t/bb;->a:Lmaps/t/bg;

    invoke-virtual {v0, v1}, Lmaps/t/bg;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_1
    instance-of v0, p1, Lmaps/t/bg;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/t/bb;->a:Lmaps/t/bg;

    invoke-virtual {v0, p1}, Lmaps/t/bg;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lmaps/t/bb;->a:Lmaps/t/bg;

    invoke-virtual {v0}, Lmaps/t/bg;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":mId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmaps/t/bb;->a:Lmaps/t/bg;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mLevelNumberE3="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmaps/t/bb;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
