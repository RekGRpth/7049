.class public final Lmaps/t/bw;
.super Lmaps/t/bf;


# instance fields
.field private b:Lmaps/t/ax;

.field private c:Lmaps/t/bx;

.field private d:Lmaps/t/bx;

.field private e:I

.field private f:I

.field private g:I

.field private volatile h:Lmaps/t/bx;

.field private volatile i:Lmaps/t/bx;

.field private volatile j:Lmaps/t/bx;

.field private volatile k:Lmaps/t/bx;

.field private volatile l:Lmaps/t/bx;

.field private volatile m:Lmaps/t/bx;


# direct methods
.method private constructor <init>(Lmaps/t/ax;)V
    .locals 4

    const/high16 v3, 0x40000000

    invoke-direct {p0}, Lmaps/t/bf;-><init>()V

    iput-object p1, p0, Lmaps/t/bw;->b:Lmaps/t/ax;

    invoke-virtual {p1}, Lmaps/t/ax;->d()Lmaps/t/bx;

    move-result-object v1

    invoke-virtual {p1}, Lmaps/t/ax;->e()Lmaps/t/bx;

    move-result-object v2

    iget v0, v1, Lmaps/t/bx;->a:I

    if-gez v0, :cond_1

    iget v0, v1, Lmaps/t/bx;->a:I

    neg-int v0, v0

    iput v0, p0, Lmaps/t/bw;->e:I

    :cond_0
    :goto_0
    new-instance v0, Lmaps/t/bx;

    invoke-direct {v0}, Lmaps/t/bx;-><init>()V

    iput-object v0, p0, Lmaps/t/bw;->c:Lmaps/t/bx;

    iget-object v0, p0, Lmaps/t/bw;->c:Lmaps/t/bx;

    invoke-virtual {v1, v0}, Lmaps/t/bx;->i(Lmaps/t/bx;)V

    new-instance v0, Lmaps/t/bx;

    invoke-direct {v0}, Lmaps/t/bx;-><init>()V

    iput-object v0, p0, Lmaps/t/bw;->d:Lmaps/t/bx;

    iget-object v0, p0, Lmaps/t/bw;->d:Lmaps/t/bx;

    invoke-virtual {v2, v0}, Lmaps/t/bx;->i(Lmaps/t/bx;)V

    iget-object v0, p0, Lmaps/t/bw;->c:Lmaps/t/bx;

    iget v0, v0, Lmaps/t/bx;->a:I

    iget-object v3, p0, Lmaps/t/bw;->d:Lmaps/t/bx;

    iget v3, v3, Lmaps/t/bx;->a:I

    if-le v0, v3, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lmaps/t/bw;->a:Z

    iget v0, v1, Lmaps/t/bx;->a:I

    iget v1, p0, Lmaps/t/bw;->e:I

    add-int/2addr v0, v1

    iput v0, p0, Lmaps/t/bw;->f:I

    iget v0, v2, Lmaps/t/bx;->a:I

    iget v1, p0, Lmaps/t/bw;->e:I

    add-int/2addr v0, v1

    iput v0, p0, Lmaps/t/bw;->g:I

    return-void

    :cond_1
    iget v0, v2, Lmaps/t/bx;->a:I

    if-le v0, v3, :cond_0

    iget v0, v2, Lmaps/t/bx;->a:I

    sub-int v0, v3, v0

    iput v0, p0, Lmaps/t/bw;->e:I

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Lmaps/t/ax;)Lmaps/t/bw;
    .locals 1

    new-instance v0, Lmaps/t/bw;

    invoke-direct {v0, p0}, Lmaps/t/bw;-><init>(Lmaps/t/ax;)V

    return-object v0
.end method


# virtual methods
.method public a()Lmaps/t/bw;
    .locals 0

    return-object p0
.end method

.method public a(I)Lmaps/t/bx;
    .locals 3

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lmaps/t/bw;->h:Lmaps/t/bx;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/t/bx;

    iget-object v1, p0, Lmaps/t/bw;->d:Lmaps/t/bx;

    iget v1, v1, Lmaps/t/bx;->a:I

    iget-object v2, p0, Lmaps/t/bw;->c:Lmaps/t/bx;

    iget v2, v2, Lmaps/t/bx;->b:I

    invoke-direct {v0, v1, v2}, Lmaps/t/bx;-><init>(II)V

    iput-object v0, p0, Lmaps/t/bw;->h:Lmaps/t/bx;

    :cond_0
    iget-object v0, p0, Lmaps/t/bw;->h:Lmaps/t/bx;

    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, Lmaps/t/bw;->d:Lmaps/t/bx;

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lmaps/t/bw;->i:Lmaps/t/bx;

    if-nez v0, :cond_1

    new-instance v0, Lmaps/t/bx;

    iget-object v1, p0, Lmaps/t/bw;->c:Lmaps/t/bx;

    iget v1, v1, Lmaps/t/bx;->a:I

    iget-object v2, p0, Lmaps/t/bw;->d:Lmaps/t/bx;

    iget v2, v2, Lmaps/t/bx;->b:I

    invoke-direct {v0, v1, v2}, Lmaps/t/bx;-><init>(II)V

    iput-object v0, p0, Lmaps/t/bw;->i:Lmaps/t/bx;

    :cond_1
    iget-object v0, p0, Lmaps/t/bw;->i:Lmaps/t/bx;

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lmaps/t/bw;->c:Lmaps/t/bx;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a(I[Lmaps/t/bx;)V
    .locals 6

    const/4 v1, 0x3

    const/4 v5, 0x2

    const v2, -0x20000001

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-boolean v0, p0, Lmaps/t/bw;->a:Z

    if-eqz v0, :cond_4

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0, v3}, Lmaps/t/bw;->a(I)Lmaps/t/bx;

    move-result-object v0

    aput-object v0, p2, v3

    invoke-virtual {p0, v4}, Lmaps/t/bw;->a(I)Lmaps/t/bx;

    move-result-object v0

    aput-object v0, p2, v4

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0, v4}, Lmaps/t/bw;->a(I)Lmaps/t/bx;

    move-result-object v0

    aput-object v0, p2, v3

    iget-object v0, p0, Lmaps/t/bw;->j:Lmaps/t/bx;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/t/bx;

    iget-object v1, p0, Lmaps/t/bw;->d:Lmaps/t/bx;

    iget v1, v1, Lmaps/t/bx;->b:I

    invoke-direct {v0, v2, v1}, Lmaps/t/bx;-><init>(II)V

    iput-object v0, p0, Lmaps/t/bw;->j:Lmaps/t/bx;

    :cond_0
    iget-object v0, p0, Lmaps/t/bw;->j:Lmaps/t/bx;

    aput-object v0, p2, v4

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lmaps/t/bw;->k:Lmaps/t/bx;

    if-nez v0, :cond_1

    new-instance v0, Lmaps/t/bx;

    const/high16 v1, 0x20000000

    iget-object v2, p0, Lmaps/t/bw;->d:Lmaps/t/bx;

    iget v2, v2, Lmaps/t/bx;->b:I

    invoke-direct {v0, v1, v2}, Lmaps/t/bx;-><init>(II)V

    iput-object v0, p0, Lmaps/t/bw;->k:Lmaps/t/bx;

    :cond_1
    iget-object v0, p0, Lmaps/t/bw;->k:Lmaps/t/bx;

    aput-object v0, p2, v3

    invoke-virtual {p0, v5}, Lmaps/t/bw;->a(I)Lmaps/t/bx;

    move-result-object v0

    aput-object v0, p2, v4

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0, v5}, Lmaps/t/bw;->a(I)Lmaps/t/bx;

    move-result-object v0

    aput-object v0, p2, v3

    invoke-virtual {p0, v1}, Lmaps/t/bw;->a(I)Lmaps/t/bx;

    move-result-object v0

    aput-object v0, p2, v4

    goto :goto_0

    :pswitch_4
    invoke-virtual {p0, v1}, Lmaps/t/bw;->a(I)Lmaps/t/bx;

    move-result-object v0

    aput-object v0, p2, v3

    iget-object v0, p0, Lmaps/t/bw;->l:Lmaps/t/bx;

    if-nez v0, :cond_2

    new-instance v0, Lmaps/t/bx;

    const/high16 v1, 0x20000000

    iget-object v2, p0, Lmaps/t/bw;->c:Lmaps/t/bx;

    iget v2, v2, Lmaps/t/bx;->b:I

    invoke-direct {v0, v1, v2}, Lmaps/t/bx;-><init>(II)V

    iput-object v0, p0, Lmaps/t/bw;->l:Lmaps/t/bx;

    :cond_2
    iget-object v0, p0, Lmaps/t/bw;->l:Lmaps/t/bx;

    aput-object v0, p2, v4

    goto :goto_0

    :pswitch_5
    iget-object v0, p0, Lmaps/t/bw;->m:Lmaps/t/bx;

    if-nez v0, :cond_3

    new-instance v0, Lmaps/t/bx;

    iget-object v1, p0, Lmaps/t/bw;->c:Lmaps/t/bx;

    iget v1, v1, Lmaps/t/bx;->b:I

    invoke-direct {v0, v2, v1}, Lmaps/t/bx;-><init>(II)V

    iput-object v0, p0, Lmaps/t/bw;->m:Lmaps/t/bx;

    :cond_3
    iget-object v0, p0, Lmaps/t/bw;->m:Lmaps/t/bx;

    aput-object v0, p2, v3

    invoke-virtual {p0, v3}, Lmaps/t/bw;->a(I)Lmaps/t/bx;

    move-result-object v0

    aput-object v0, p2, v4

    goto/16 :goto_0

    :cond_4
    invoke-virtual {p0, p1}, Lmaps/t/bw;->a(I)Lmaps/t/bx;

    move-result-object v0

    aput-object v0, p2, v3

    add-int/lit8 v0, p1, 0x1

    rem-int/lit8 v0, v0, 0x4

    invoke-virtual {p0, v0}, Lmaps/t/bw;->a(I)Lmaps/t/bx;

    move-result-object v0

    aput-object v0, p2, v4

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public a(Lmaps/t/an;)Z
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/high16 v7, 0x20000000

    const/high16 v6, -0x20000000

    const/high16 v5, 0x40000000

    iget-boolean v0, p0, Lmaps/t/bw;->a:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lmaps/t/bw;->b:Lmaps/t/ax;

    invoke-virtual {v0, p1}, Lmaps/t/ax;->b(Lmaps/t/an;)Z

    move-result v1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {p1}, Lmaps/t/an;->a()Lmaps/t/ax;

    move-result-object v0

    iget-object v3, p0, Lmaps/t/bw;->c:Lmaps/t/bx;

    iget v3, v3, Lmaps/t/bx;->b:I

    iget-object v4, v0, Lmaps/t/ax;->a:Lmaps/t/bx;

    iget v4, v4, Lmaps/t/bx;->b:I

    if-gt v3, v4, :cond_2

    iget-object v3, p0, Lmaps/t/bw;->d:Lmaps/t/bx;

    iget v3, v3, Lmaps/t/bx;->b:I

    iget-object v4, v0, Lmaps/t/ax;->b:Lmaps/t/bx;

    iget v4, v4, Lmaps/t/bx;->b:I

    if-ge v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    iget-object v3, v0, Lmaps/t/ax;->a:Lmaps/t/bx;

    iget v3, v3, Lmaps/t/bx;->a:I

    iget-object v0, v0, Lmaps/t/ax;->b:Lmaps/t/bx;

    iget v0, v0, Lmaps/t/bx;->a:I

    iget-object v4, p0, Lmaps/t/bw;->c:Lmaps/t/bx;

    iget v4, v4, Lmaps/t/bx;->a:I

    if-gt v4, v3, :cond_4

    if-gt v7, v0, :cond_0

    :cond_4
    if-gt v6, v3, :cond_5

    iget-object v4, p0, Lmaps/t/bw;->d:Lmaps/t/bx;

    iget v4, v4, Lmaps/t/bx;->a:I

    if-ge v4, v0, :cond_0

    :cond_5
    if-ge v3, v6, :cond_8

    add-int/2addr v3, v5

    :cond_6
    :goto_1
    if-ge v0, v6, :cond_9

    add-int/2addr v0, v5

    :cond_7
    :goto_2
    iget-object v4, p0, Lmaps/t/bw;->c:Lmaps/t/bx;

    iget v4, v4, Lmaps/t/bx;->a:I

    if-gt v4, v3, :cond_a

    iget-object v3, p0, Lmaps/t/bw;->d:Lmaps/t/bx;

    iget v3, v3, Lmaps/t/bx;->a:I

    if-lt v3, v0, :cond_a

    move v0, v1

    :goto_3
    move v1, v0

    goto :goto_0

    :cond_8
    if-lt v3, v7, :cond_6

    sub-int/2addr v3, v5

    goto :goto_1

    :cond_9
    if-lt v0, v7, :cond_7

    sub-int/2addr v0, v5

    goto :goto_2

    :cond_a
    move v0, v2

    goto :goto_3
.end method

.method public a(Lmaps/t/bx;)Z
    .locals 2

    iget v0, p1, Lmaps/t/bx;->a:I

    iget v1, p0, Lmaps/t/bw;->e:I

    add-int/2addr v0, v1

    const v1, 0x3fffffff

    and-int/2addr v0, v1

    iget v1, p0, Lmaps/t/bw;->f:I

    if-lt v0, v1, :cond_0

    iget v1, p0, Lmaps/t/bw;->g:I

    if-gt v0, v1, :cond_0

    iget v0, p1, Lmaps/t/bx;->b:I

    iget-object v1, p0, Lmaps/t/bw;->c:Lmaps/t/bx;

    iget v1, v1, Lmaps/t/bx;->b:I

    if-lt v0, v1, :cond_0

    iget v0, p1, Lmaps/t/bx;->b:I

    iget-object v1, p0, Lmaps/t/bw;->d:Lmaps/t/bx;

    iget v1, v1, Lmaps/t/bx;->b:I

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Lmaps/t/ax;
    .locals 1

    iget-object v0, p0, Lmaps/t/bw;->b:Lmaps/t/ax;

    return-object v0
.end method

.method public b(Lmaps/t/an;)Z
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Lmaps/t/bw;->a:Z

    if-nez v1, :cond_1

    iget-object v0, p0, Lmaps/t/bw;->b:Lmaps/t/ax;

    invoke-virtual {v0, p1}, Lmaps/t/ax;->a(Lmaps/t/an;)Z

    move-result v0

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v1, p1, Lmaps/t/ax;

    if-eqz v1, :cond_4

    check-cast p1, Lmaps/t/ax;

    iget-object v1, p0, Lmaps/t/bw;->c:Lmaps/t/bx;

    iget v1, v1, Lmaps/t/bx;->b:I

    iget-object v2, p1, Lmaps/t/ax;->b:Lmaps/t/bx;

    iget v2, v2, Lmaps/t/bx;->b:I

    if-gt v1, v2, :cond_0

    iget-object v1, p0, Lmaps/t/bw;->d:Lmaps/t/bx;

    iget v1, v1, Lmaps/t/bx;->b:I

    iget-object v2, p1, Lmaps/t/ax;->a:Lmaps/t/bx;

    iget v2, v2, Lmaps/t/bx;->b:I

    if-lt v1, v2, :cond_0

    iget-object v1, p0, Lmaps/t/bw;->c:Lmaps/t/bx;

    iget v1, v1, Lmaps/t/bx;->a:I

    iget-object v2, p1, Lmaps/t/ax;->b:Lmaps/t/bx;

    iget v2, v2, Lmaps/t/bx;->a:I

    if-gt v1, v2, :cond_2

    const/high16 v1, 0x20000000

    iget-object v2, p1, Lmaps/t/ax;->a:Lmaps/t/bx;

    iget v2, v2, Lmaps/t/bx;->a:I

    if-gt v1, v2, :cond_3

    :cond_2
    const/high16 v1, -0x20000000

    iget-object v2, p1, Lmaps/t/ax;->b:Lmaps/t/bx;

    iget v2, v2, Lmaps/t/bx;->a:I

    if-gt v1, v2, :cond_0

    iget-object v1, p0, Lmaps/t/bw;->d:Lmaps/t/bx;

    iget v1, v1, Lmaps/t/bx;->a:I

    iget-object v2, p1, Lmaps/t/ax;->a:Lmaps/t/bx;

    iget v2, v2, Lmaps/t/bx;->a:I

    if-lt v1, v2, :cond_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    invoke-super {p0, p1}, Lmaps/t/bf;->b(Lmaps/t/an;)Z

    move-result v0

    goto :goto_0
.end method

.method public bridge synthetic c()Lmaps/t/an;
    .locals 1

    invoke-virtual {p0}, Lmaps/t/bw;->b()Lmaps/t/ax;

    move-result-object v0

    return-object v0
.end method

.method public d()I
    .locals 1

    iget-object v0, p0, Lmaps/t/bw;->b:Lmaps/t/ax;

    invoke-virtual {v0}, Lmaps/t/ax;->g()I

    move-result v0

    return v0
.end method

.method public e()I
    .locals 2

    iget-object v0, p0, Lmaps/t/bw;->d:Lmaps/t/bx;

    iget v0, v0, Lmaps/t/bx;->b:I

    iget-object v1, p0, Lmaps/t/bw;->c:Lmaps/t/bx;

    iget v1, v1, Lmaps/t/bx;->b:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public f()Lmaps/t/bx;
    .locals 1

    iget-object v0, p0, Lmaps/t/bw;->c:Lmaps/t/bx;

    return-object v0
.end method

.method public g()Lmaps/t/bx;
    .locals 1

    iget-object v0, p0, Lmaps/t/bw;->d:Lmaps/t/bx;

    return-object v0
.end method

.method public h()I
    .locals 1

    iget-boolean v0, p0, Lmaps/t/bw;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method
