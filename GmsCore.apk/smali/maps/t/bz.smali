.class public Lmaps/t/bz;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/t/o;


# instance fields
.field private final a:Lmaps/t/ah;

.field private final b:Lmaps/o/c;

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:[B

.field private g:[Ljava/lang/String;

.field private h:[Ljava/lang/String;

.field private i:I


# direct methods
.method public constructor <init>(Lmaps/t/ah;III[BLmaps/o/c;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lmaps/t/bz;->i:I

    iput-object p1, p0, Lmaps/t/bz;->a:Lmaps/t/ah;

    iput-object p6, p0, Lmaps/t/bz;->b:Lmaps/o/c;

    iput p2, p0, Lmaps/t/bz;->c:I

    iput p3, p0, Lmaps/t/bz;->d:I

    iput p4, p0, Lmaps/t/bz;->e:I

    if-eqz p5, :cond_1

    array-length v0, p5

    if-eqz v0, :cond_1

    new-instance v0, Lmaps/bl/b;

    invoke-direct {v0}, Lmaps/bl/b;-><init>()V

    invoke-virtual {v0, p5}, Lmaps/bl/b;->a([B)[B

    move-result-object p5

    invoke-static {}, Lmaps/af/w;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lmaps/bl/b;->a()[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lmaps/t/bz;->g:[Ljava/lang/String;

    invoke-virtual {v0}, Lmaps/bl/b;->b()[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lmaps/t/bz;->h:[Ljava/lang/String;

    invoke-virtual {v0}, Lmaps/bl/b;->c()I

    move-result v0

    iput v0, p0, Lmaps/t/bz;->i:I

    :cond_0
    aget-byte v0, p5, v2

    const/16 v1, 0x43

    if-ne v0, v1, :cond_1

    :try_start_0
    invoke-static {p5}, Lmaps/bi/b;->a([B)[B
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p5

    :cond_1
    iget-object v0, p0, Lmaps/t/bz;->g:[Ljava/lang/String;

    if-nez v0, :cond_2

    new-array v0, v2, [Ljava/lang/String;

    iput-object v0, p0, Lmaps/t/bz;->g:[Ljava/lang/String;

    :cond_2
    iget-object v0, p0, Lmaps/t/bz;->h:[Ljava/lang/String;

    if-nez v0, :cond_3

    new-array v0, v2, [Ljava/lang/String;

    iput-object v0, p0, Lmaps/t/bz;->h:[Ljava/lang/String;

    :cond_3
    iput-object p5, p0, Lmaps/t/bz;->f:[B

    return-void

    :catch_0
    move-exception v0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Input image is not Compact JPEG"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(Lmaps/t/ah;Ljava/io/DataInput;Lmaps/o/c;)Lmaps/t/bz;
    .locals 7

    invoke-interface {p1}, Ljava/io/DataInput;->readInt()I

    move-result v0

    const v1, 0x44524154

    if-eq v0, v1, :cond_0

    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TILE_MAGIC expected: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    invoke-static {p1}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v0

    const/4 v1, 0x7

    if-eq v0, v1, :cond_1

    const/16 v1, 0x8

    if-eq v0, v1, :cond_1

    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Version mismatch: 7 or 8 expected, "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " found"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {p1}, Lmaps/t/ah;->a(Ljava/io/DataInput;)Lmaps/t/ah;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/ah;->d()I

    move-result v1

    invoke-virtual {p0}, Lmaps/t/ah;->d()I

    move-result v2

    if-ne v1, v2, :cond_2

    invoke-virtual {v0}, Lmaps/t/ah;->e()I

    move-result v1

    invoke-virtual {p0}, Lmaps/t/ah;->e()I

    move-result v2

    if-ne v1, v2, :cond_2

    invoke-virtual {v0}, Lmaps/t/ah;->c()I

    move-result v1

    invoke-virtual {p0}, Lmaps/t/ah;->c()I

    move-result v2

    if-eq v1, v2, :cond_3

    :cond_2
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Expected tile coords: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " but received "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    invoke-static {p1}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v2

    invoke-static {p1}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v3

    invoke-static {p1}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v4

    invoke-static {p1}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v0

    new-array v5, v0, [B

    invoke-interface {p1, v5}, Ljava/io/DataInput;->readFully([B)V

    new-instance v0, Lmaps/t/bz;

    move-object v1, p0

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lmaps/t/bz;-><init>(Lmaps/t/ah;III[BLmaps/o/c;)V

    return-object v0
.end method

.method public static a(Lmaps/t/ah;III[BLjava/io/DataOutput;)V
    .locals 1

    const v0, 0x44524154

    invoke-interface {p5, v0}, Ljava/io/DataOutput;->writeInt(I)V

    const/16 v0, 0x8

    invoke-static {p5, v0}, Lmaps/t/bq;->a(Ljava/io/DataOutput;I)V

    invoke-virtual {p0, p5}, Lmaps/t/ah;->a(Ljava/io/DataOutput;)V

    invoke-static {p5, p1}, Lmaps/t/bq;->a(Ljava/io/DataOutput;I)V

    invoke-static {p5, p2}, Lmaps/t/bq;->a(Ljava/io/DataOutput;I)V

    invoke-static {p5, p3}, Lmaps/t/bq;->a(Ljava/io/DataOutput;I)V

    array-length v0, p4

    invoke-static {p5, v0}, Lmaps/t/bq;->a(Ljava/io/DataOutput;I)V

    invoke-interface {p5, p4}, Ljava/io/DataOutput;->write([B)V

    return-void
.end method


# virtual methods
.method public a()Lmaps/t/ar;
    .locals 7

    const/4 v1, 0x0

    new-instance v0, Lmaps/t/ae;

    invoke-virtual {p0}, Lmaps/t/bz;->e()[B

    move-result-object v3

    const/4 v5, 0x0

    new-array v6, v1, [I

    move v2, v1

    move v4, v1

    invoke-direct/range {v0 .. v6}, Lmaps/t/ae;-><init>(II[BILmaps/t/aa;[I)V

    new-instance v2, Lmaps/t/cj;

    invoke-direct {v2}, Lmaps/t/cj;-><init>()V

    new-instance v3, Lmaps/t/cl;

    invoke-direct {v3}, Lmaps/t/cl;-><init>()V

    invoke-virtual {v2, v3}, Lmaps/t/cj;->a(Lmaps/t/cl;)Lmaps/t/cj;

    move-result-object v2

    invoke-virtual {p0}, Lmaps/t/bz;->g()Lmaps/t/ah;

    move-result-object v3

    invoke-virtual {v2, v3}, Lmaps/t/cj;->a(Lmaps/t/ah;)Lmaps/t/cj;

    move-result-object v2

    invoke-virtual {p0}, Lmaps/t/bz;->h()Lmaps/o/c;

    move-result-object v3

    invoke-virtual {v2, v3}, Lmaps/t/cj;->a(Lmaps/o/c;)Lmaps/t/cj;

    move-result-object v2

    invoke-virtual {p0}, Lmaps/t/bz;->b()[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lmaps/t/cj;->a([Ljava/lang/String;)Lmaps/t/cj;

    move-result-object v2

    invoke-virtual {p0}, Lmaps/t/bz;->d()I

    move-result v3

    invoke-virtual {v2, v3}, Lmaps/t/cj;->c(I)Lmaps/t/cj;

    move-result-object v2

    const-wide/16 v3, -0x1

    invoke-virtual {v2, v3, v4}, Lmaps/t/cj;->a(J)Lmaps/t/cj;

    move-result-object v2

    invoke-virtual {p0}, Lmaps/t/bz;->i()I

    move-result v3

    invoke-virtual {v2, v3}, Lmaps/t/cj;->b(I)Lmaps/t/cj;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Lmaps/t/ca;

    aput-object v0, v3, v1

    invoke-virtual {v2, v3}, Lmaps/t/cj;->a([Lmaps/t/ca;)Lmaps/t/cj;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/cj;->a()Lmaps/t/ar;

    move-result-object v0

    return-object v0
.end method

.method public a(Lmaps/ae/d;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b(Lmaps/ae/d;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/t/bz;->g:[Ljava/lang/String;

    return-object v0
.end method

.method public c(Lmaps/ae/d;)V
    .locals 0

    return-void
.end method

.method public c()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/t/bz;->h:[Ljava/lang/String;

    return-object v0
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lmaps/t/bz;->i:I

    return v0
.end method

.method public e()[B
    .locals 1

    iget-object v0, p0, Lmaps/t/bz;->f:[B

    return-object v0
.end method

.method public g()Lmaps/t/ah;
    .locals 1

    iget-object v0, p0, Lmaps/t/bz;->a:Lmaps/t/ah;

    return-object v0
.end method

.method public h()Lmaps/o/c;
    .locals 1

    iget-object v0, p0, Lmaps/t/bz;->b:Lmaps/o/c;

    return-object v0
.end method

.method public i()I
    .locals 1

    iget v0, p0, Lmaps/t/bz;->c:I

    return v0
.end method

.method public j()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public k()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
