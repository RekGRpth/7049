.class public Lmaps/t/cd;
.super Ljava/lang/Object;


# instance fields
.field private final a:I

.field private final b:Lmaps/t/ah;

.field private final c:Lmaps/t/cl;

.field private final d:[Ljava/lang/String;

.field private final e:Lmaps/t/ci;


# direct methods
.method public constructor <init>(ILmaps/t/ah;Lmaps/t/cl;[Ljava/lang/String;Lmaps/t/ci;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lmaps/t/cd;->a:I

    iput-object p2, p0, Lmaps/t/cd;->b:Lmaps/t/ah;

    iput-object p3, p0, Lmaps/t/cd;->c:Lmaps/t/cl;

    iput-object p4, p0, Lmaps/t/cd;->d:[Ljava/lang/String;

    iput-object p5, p0, Lmaps/t/cd;->e:Lmaps/t/ci;

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lmaps/t/cd;->a:I

    return v0
.end method

.method public a(I)Lmaps/t/aa;
    .locals 1

    iget-object v0, p0, Lmaps/t/cd;->c:Lmaps/t/cl;

    invoke-virtual {v0, p1}, Lmaps/t/cl;->a(I)Lmaps/t/aa;

    move-result-object v0

    return-object v0
.end method

.method public b()Lmaps/t/ah;
    .locals 1

    iget-object v0, p0, Lmaps/t/cd;->b:Lmaps/t/ah;

    return-object v0
.end method

.method public b(I)Lmaps/t/br;
    .locals 1

    iget-object v0, p0, Lmaps/t/cd;->e:Lmaps/t/ci;

    invoke-virtual {v0, p1}, Lmaps/t/ci;->a(I)Lmaps/t/br;

    move-result-object v0

    return-object v0
.end method

.method public c(I)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/t/cd;->d:[Ljava/lang/String;

    invoke-static {p1, v0}, Lmaps/t/ab;->a(I[Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/t/cd;->d:[Ljava/lang/String;

    aget-object v0, v0, p1

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
