.class public final Lmaps/t/cg;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/ThreadLocal;


# instance fields
.field private final b:[I

.field private volatile c:Lmaps/t/ax;

.field private volatile d:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lmaps/t/cn;

    invoke-direct {v0}, Lmaps/t/cn;-><init>()V

    sput-object v0, Lmaps/t/cg;->a:Ljava/lang/ThreadLocal;

    return-void
.end method

.method private constructor <init>([I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/t/cg;->b:[I

    const/high16 v0, -0x40800000

    iput v0, p0, Lmaps/t/cg;->d:F

    return-void
.end method

.method synthetic constructor <init>([ILmaps/t/cn;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/t/cg;-><init>([I)V

    return-void
.end method

.method private a(FIIILmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;[Z)I
    .locals 15

    move/from16 v0, p3

    move-object/from16 v1, p5

    invoke-virtual {p0, v0, v1}, Lmaps/t/cg;->a(ILmaps/t/bx;)V

    move/from16 v0, p4

    move-object/from16 v1, p6

    invoke-virtual {p0, v0, v1}, Lmaps/t/cg;->a(ILmaps/t/bx;)V

    const/4 v8, -0x1

    add-int v6, p3, p2

    move/from16 v5, p1

    :goto_0
    add-int/lit8 v4, p4, -0x1

    if-gt v6, v4, :cond_0

    move-object/from16 v0, p8

    invoke-virtual {p0, v6, v0}, Lmaps/t/cg;->a(ILmaps/t/bx;)V

    move-object/from16 v0, p5

    move-object/from16 v1, p6

    move-object/from16 v2, p8

    move-object/from16 v3, p7

    invoke-static {v0, v1, v2, v3}, Lmaps/t/bx;->a(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)F

    move-result v4

    cmpl-float v7, v4, v5

    if-lez v7, :cond_3

    move v8, v6

    :goto_1
    add-int v6, v6, p2

    move v5, v4

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    if-ltz v8, :cond_2

    const/4 v4, 0x1

    const/4 v5, 0x1

    aput-boolean v5, p9, v8

    add-int/lit8 v5, p3, 0x1

    if-le v8, v5, :cond_1

    move-object v4, p0

    move/from16 v5, p1

    move/from16 v6, p2

    move/from16 v7, p3

    move-object/from16 v9, p5

    move-object/from16 v10, p6

    move-object/from16 v11, p7

    move-object/from16 v12, p8

    move-object/from16 v13, p9

    invoke-direct/range {v4 .. v13}, Lmaps/t/cg;->a(FIIILmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;[Z)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    :cond_1
    add-int/lit8 v5, p4, -0x1

    if-ge v8, v5, :cond_2

    move-object v5, p0

    move/from16 v6, p1

    move/from16 v7, p2

    move/from16 v9, p4

    move-object/from16 v10, p5

    move-object/from16 v11, p6

    move-object/from16 v12, p7

    move-object/from16 v13, p8

    move-object/from16 v14, p9

    invoke-direct/range {v5 .. v14}, Lmaps/t/cg;->a(FIIILmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;[Z)I

    move-result v5

    add-int/2addr v4, v5

    :cond_2
    return v4

    :cond_3
    move v4, v5

    goto :goto_1
.end method

.method public static a(Ljava/io/DataInput;Lmaps/t/ah;)Lmaps/t/cg;
    .locals 3

    invoke-static {p0}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v1

    mul-int/lit8 v0, v1, 0x3

    new-array v2, v0, [I

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-static {p0, p1, v2, v0}, Lmaps/t/bx;->a(Ljava/io/DataInput;Lmaps/t/ah;[II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v0, Lmaps/t/cg;

    invoke-direct {v0, v2}, Lmaps/t/cg;-><init>([I)V

    return-object v0
.end method

.method public static a(Lmaps/t/bx;Lmaps/t/bx;)Lmaps/t/cg;
    .locals 2

    const/4 v0, 0x6

    new-array v0, v0, [I

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lmaps/t/bx;->a([II)V

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Lmaps/t/bx;->a([II)V

    new-instance v1, Lmaps/t/cg;

    invoke-direct {v1, v0}, Lmaps/t/cg;-><init>([I)V

    return-object v1
.end method

.method public static a([I)Lmaps/t/cg;
    .locals 7

    const/4 v1, 0x0

    array-length v0, p0

    div-int/lit8 v0, v0, 0x2

    mul-int/lit8 v0, v0, 0x3

    new-array v3, v0, [I

    move v0, v1

    move v2, v1

    :goto_0
    array-length v4, v3

    if-ge v2, v4, :cond_0

    add-int/lit8 v4, v2, 0x1

    add-int/lit8 v5, v0, 0x1

    aget v0, p0, v0

    aput v0, v3, v2

    add-int/lit8 v6, v4, 0x1

    add-int/lit8 v0, v5, 0x1

    aget v2, p0, v5

    aput v2, v3, v4

    add-int/lit8 v2, v6, 0x1

    aput v1, v3, v6

    goto :goto_0

    :cond_0
    new-instance v0, Lmaps/t/cg;

    invoke-direct {v0, v3}, Lmaps/t/cg;-><init>([I)V

    return-object v0
.end method

.method public static b([I)Lmaps/t/cg;
    .locals 1

    new-instance v0, Lmaps/t/cg;

    invoke-direct {v0, p0}, Lmaps/t/cg;-><init>([I)V

    return-object v0
.end method


# virtual methods
.method public a()Lmaps/t/ax;
    .locals 3

    iget-object v0, p0, Lmaps/t/cg;->c:Lmaps/t/ax;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lmaps/t/cg;->b()I

    move-result v0

    const/4 v1, 0x1

    if-lt v0, v1, :cond_1

    invoke-static {p0}, Lmaps/t/ax;->a(Lmaps/t/cg;)Lmaps/t/ax;

    move-result-object v0

    iput-object v0, p0, Lmaps/t/cg;->c:Lmaps/t/ax;

    :cond_0
    :goto_0
    iget-object v0, p0, Lmaps/t/cg;->c:Lmaps/t/ax;

    return-object v0

    :cond_1
    new-instance v0, Lmaps/t/ax;

    new-instance v1, Lmaps/t/bx;

    invoke-direct {v1}, Lmaps/t/bx;-><init>()V

    new-instance v2, Lmaps/t/bx;

    invoke-direct {v2}, Lmaps/t/bx;-><init>()V

    invoke-direct {v0, v1, v2}, Lmaps/t/ax;-><init>(Lmaps/t/bx;Lmaps/t/bx;)V

    iput-object v0, p0, Lmaps/t/cg;->c:Lmaps/t/ax;

    goto :goto_0
.end method

.method public a(F)Lmaps/t/bx;
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x0

    cmpg-float v1, p1, v1

    if-gtz v1, :cond_0

    invoke-virtual {p0, v0}, Lmaps/t/cg;->a(I)Lmaps/t/bx;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/high16 v1, 0x3f800000

    cmpl-float v1, p1, v1

    if-ltz v1, :cond_1

    invoke-virtual {p0}, Lmaps/t/cg;->c()Lmaps/t/bx;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lmaps/t/cg;->d()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {p0}, Lmaps/t/cg;->b()I

    move-result v2

    add-int/lit8 v3, v2, -0x1

    move v5, v0

    move v0, v1

    move v1, v5

    :goto_1
    if-ge v1, v3, :cond_3

    invoke-virtual {p0, v1}, Lmaps/t/cg;->b(I)F

    move-result v2

    cmpl-float v4, v2, v0

    if-ltz v4, :cond_2

    div-float v2, v0, v2

    new-instance v3, Lmaps/t/bx;

    invoke-direct {v3}, Lmaps/t/bx;-><init>()V

    new-instance v0, Lmaps/t/bx;

    invoke-direct {v0}, Lmaps/t/bx;-><init>()V

    invoke-virtual {p0, v1, v3}, Lmaps/t/cg;->a(ILmaps/t/bx;)V

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v1, v0}, Lmaps/t/cg;->a(ILmaps/t/bx;)V

    invoke-static {v3, v0, v2, v0}, Lmaps/t/bx;->a(Lmaps/t/bx;Lmaps/t/bx;FLmaps/t/bx;)V

    goto :goto_0

    :cond_2
    sub-float v2, v0, v2

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lmaps/t/cg;->c()Lmaps/t/bx;

    move-result-object v0

    goto :goto_0
.end method

.method public a(I)Lmaps/t/bx;
    .locals 5

    mul-int/lit8 v0, p1, 0x3

    new-instance v1, Lmaps/t/bx;

    iget-object v2, p0, Lmaps/t/cg;->b:[I

    aget v2, v2, v0

    iget-object v3, p0, Lmaps/t/cg;->b:[I

    add-int/lit8 v4, v0, 0x1

    aget v3, v3, v4

    iget-object v4, p0, Lmaps/t/cg;->b:[I

    add-int/lit8 v0, v0, 0x2

    aget v0, v4, v0

    invoke-direct {v1, v2, v3, v0}, Lmaps/t/bx;-><init>(III)V

    return-object v1
.end method

.method public a(ILmaps/t/bx;)V
    .locals 3

    mul-int/lit8 v0, p1, 0x3

    iget-object v1, p0, Lmaps/t/cg;->b:[I

    aget v1, v1, v0

    iput v1, p2, Lmaps/t/bx;->a:I

    iget-object v1, p0, Lmaps/t/cg;->b:[I

    add-int/lit8 v2, v0, 0x1

    aget v1, v1, v2

    iput v1, p2, Lmaps/t/bx;->b:I

    iget-object v1, p0, Lmaps/t/cg;->b:[I

    add-int/lit8 v0, v0, 0x2

    aget v0, v1, v0

    iput v0, p2, Lmaps/t/bx;->c:I

    return-void
.end method

.method public a(ILmaps/t/bx;Lmaps/t/bx;)V
    .locals 3

    mul-int/lit8 v0, p1, 0x3

    iget-object v1, p0, Lmaps/t/cg;->b:[I

    aget v1, v1, v0

    iget v2, p2, Lmaps/t/bx;->a:I

    sub-int/2addr v1, v2

    iput v1, p3, Lmaps/t/bx;->a:I

    iget-object v1, p0, Lmaps/t/cg;->b:[I

    add-int/lit8 v2, v0, 0x1

    aget v1, v1, v2

    iget v2, p2, Lmaps/t/bx;->b:I

    sub-int/2addr v1, v2

    iput v1, p3, Lmaps/t/bx;->b:I

    iget-object v1, p0, Lmaps/t/cg;->b:[I

    add-int/lit8 v0, v0, 0x2

    aget v0, v1, v0

    iget v1, p2, Lmaps/t/bx;->c:I

    sub-int/2addr v0, v1

    iput v0, p3, Lmaps/t/bx;->c:I

    return-void
.end method

.method public a(Lmaps/t/bx;)V
    .locals 3

    iget-object v0, p0, Lmaps/t/cg;->b:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x3

    iget-object v1, p0, Lmaps/t/cg;->b:[I

    aget v1, v1, v0

    iput v1, p1, Lmaps/t/bx;->a:I

    iget-object v1, p0, Lmaps/t/cg;->b:[I

    add-int/lit8 v2, v0, 0x1

    aget v1, v1, v2

    iput v1, p1, Lmaps/t/bx;->b:I

    iget-object v1, p0, Lmaps/t/cg;->b:[I

    add-int/lit8 v0, v0, 0x2

    aget v0, v1, v0

    iput v0, p1, Lmaps/t/bx;->c:I

    return-void
.end method

.method public b(I)F
    .locals 6

    mul-int/lit8 v0, p1, 0x3

    add-int/lit8 v1, v0, 0x3

    iget-object v2, p0, Lmaps/t/cg;->b:[I

    add-int/lit8 v3, v0, 0x1

    aget v0, v2, v0

    iget-object v2, p0, Lmaps/t/cg;->b:[I

    add-int/lit8 v4, v1, 0x1

    aget v1, v2, v1

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iget-object v1, p0, Lmaps/t/cg;->b:[I

    add-int/lit8 v2, v3, 0x1

    aget v1, v1, v3

    iget-object v3, p0, Lmaps/t/cg;->b:[I

    add-int/lit8 v5, v4, 0x1

    aget v3, v3, v4

    sub-int/2addr v1, v3

    int-to-float v1, v1

    iget-object v3, p0, Lmaps/t/cg;->b:[I

    add-int/lit8 v4, v2, 0x1

    aget v2, v3, v2

    iget-object v3, p0, Lmaps/t/cg;->b:[I

    add-int/lit8 v4, v5, 0x1

    aget v3, v3, v5

    sub-int/2addr v2, v3

    int-to-float v2, v2

    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    mul-float v1, v2, v2

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public b()I
    .locals 1

    iget-object v0, p0, Lmaps/t/cg;->b:[I

    array-length v0, v0

    div-int/lit8 v0, v0, 0x3

    return v0
.end method

.method public b(F)Lmaps/t/cg;
    .locals 11

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lmaps/t/cg;->b:[I

    array-length v0, v0

    const/4 v1, 0x6

    if-gt v0, v1, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    iget-object v0, p0, Lmaps/t/cg;->b:[I

    array-length v0, v0

    div-int/lit8 v10, v0, 0x3

    new-array v9, v10, [Z

    aput-boolean v2, v9, v3

    add-int/lit8 v0, v10, -0x1

    aput-boolean v2, v9, v0

    mul-float v1, p1, p1

    add-int/lit8 v4, v10, -0x1

    new-instance v5, Lmaps/t/bx;

    invoke-direct {v5}, Lmaps/t/bx;-><init>()V

    new-instance v6, Lmaps/t/bx;

    invoke-direct {v6}, Lmaps/t/bx;-><init>()V

    new-instance v7, Lmaps/t/bx;

    invoke-direct {v7}, Lmaps/t/bx;-><init>()V

    new-instance v8, Lmaps/t/bx;

    invoke-direct {v8}, Lmaps/t/bx;-><init>()V

    move-object v0, p0

    invoke-direct/range {v0 .. v9}, Lmaps/t/cg;->a(FIIILmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;[Z)I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    if-eq v0, v10, :cond_0

    mul-int/lit8 v0, v0, 0x3

    new-array v1, v0, [I

    move v0, v3

    :goto_1
    if-ge v3, v10, :cond_3

    aget-boolean v2, v9, v3

    if-eqz v2, :cond_2

    mul-int/lit8 v2, v3, 0x3

    add-int/lit8 v4, v0, 0x1

    iget-object v5, p0, Lmaps/t/cg;->b:[I

    add-int/lit8 v6, v2, 0x1

    aget v2, v5, v2

    aput v2, v1, v0

    add-int/lit8 v2, v4, 0x1

    iget-object v0, p0, Lmaps/t/cg;->b:[I

    add-int/lit8 v5, v6, 0x1

    aget v0, v0, v6

    aput v0, v1, v4

    add-int/lit8 v0, v2, 0x1

    iget-object v4, p0, Lmaps/t/cg;->b:[I

    add-int/lit8 v6, v5, 0x1

    aget v4, v4, v5

    aput v4, v1, v2

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    new-instance p0, Lmaps/t/cg;

    invoke-direct {p0, v1}, Lmaps/t/cg;-><init>([I)V

    goto :goto_0
.end method

.method public b(Lmaps/t/bx;)Lmaps/t/cg;
    .locals 5

    iget-object v0, p0, Lmaps/t/cg;->b:[I

    array-length v0, v0

    new-array v1, v0, [I

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lmaps/t/cg;->b:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lmaps/t/cg;->b:[I

    aget v2, v2, v0

    iget v3, p1, Lmaps/t/bx;->a:I

    add-int/2addr v2, v3

    aput v2, v1, v0

    add-int/lit8 v2, v0, 0x1

    iget-object v3, p0, Lmaps/t/cg;->b:[I

    add-int/lit8 v4, v0, 0x1

    aget v3, v3, v4

    iget v4, p1, Lmaps/t/bx;->b:I

    add-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v0, 0x2

    iget-object v3, p0, Lmaps/t/cg;->b:[I

    add-int/lit8 v4, v0, 0x2

    aget v3, v3, v4

    iget v4, p1, Lmaps/t/bx;->c:I

    add-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v0, v0, 0x3

    goto :goto_0

    :cond_0
    new-instance v0, Lmaps/t/cg;

    invoke-direct {v0, v1}, Lmaps/t/cg;-><init>([I)V

    return-object v0
.end method

.method public c()Lmaps/t/bx;
    .locals 5

    iget-object v0, p0, Lmaps/t/cg;->b:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x3

    new-instance v1, Lmaps/t/bx;

    iget-object v2, p0, Lmaps/t/cg;->b:[I

    aget v2, v2, v0

    iget-object v3, p0, Lmaps/t/cg;->b:[I

    add-int/lit8 v4, v0, 0x1

    aget v3, v3, v4

    iget-object v4, p0, Lmaps/t/cg;->b:[I

    add-int/lit8 v0, v0, 0x2

    aget v0, v4, v0

    invoke-direct {v1, v2, v3, v0}, Lmaps/t/bx;-><init>(III)V

    return-object v1
.end method

.method public c(F)Lmaps/t/cg;
    .locals 25

    invoke-virtual/range {p0 .. p0}, Lmaps/t/cg;->e()Z

    move-result v5

    invoke-virtual/range {p0 .. p0}, Lmaps/t/cg;->b()I

    move-result v6

    add-int/lit8 v7, v6, -0x1

    const/4 v2, 0x2

    if-le v6, v2, :cond_0

    const/4 v2, 0x0

    cmpg-float v2, p1, v2

    if-lez v2, :cond_0

    const/4 v2, 0x3

    if-gt v6, v2, :cond_1

    if-eqz v5, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    new-instance v8, Lmaps/t/by;

    invoke-direct {v8, v6}, Lmaps/t/by;-><init>(I)V

    if-eqz v5, :cond_3

    add-int/lit8 v2, v7, -0x1

    :goto_1
    add-int/lit8 v3, v2, -0x1

    mul-int/lit8 v3, v3, 0x3

    mul-int/lit8 v4, v2, 0x3

    add-int/lit8 v9, v2, 0x1

    rem-int/2addr v9, v6

    mul-int/lit8 v9, v9, 0x3

    add-int/lit8 v2, v2, 0x2

    rem-int/2addr v2, v6

    mul-int/lit8 v2, v2, 0x3

    new-instance v10, Lmaps/t/bx;

    move-object/from16 v0, p0

    iget-object v11, v0, Lmaps/t/cg;->b:[I

    aget v11, v11, v3

    move-object/from16 v0, p0

    iget-object v12, v0, Lmaps/t/cg;->b:[I

    add-int/lit8 v3, v3, 0x1

    aget v3, v12, v3

    const/4 v12, 0x0

    invoke-direct {v10, v11, v3, v12}, Lmaps/t/bx;-><init>(III)V

    new-instance v11, Lmaps/t/bx;

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/t/cg;->b:[I

    aget v3, v3, v4

    move-object/from16 v0, p0

    iget-object v12, v0, Lmaps/t/cg;->b:[I

    add-int/lit8 v4, v4, 0x1

    aget v4, v12, v4

    const/4 v12, 0x0

    invoke-direct {v11, v3, v4, v12}, Lmaps/t/bx;-><init>(III)V

    new-instance v12, Lmaps/t/bx;

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/t/cg;->b:[I

    aget v3, v3, v9

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/t/cg;->b:[I

    add-int/lit8 v9, v9, 0x1

    aget v4, v4, v9

    const/4 v9, 0x0

    invoke-direct {v12, v3, v4, v9}, Lmaps/t/bx;-><init>(III)V

    new-instance v9, Lmaps/t/bx;

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/t/cg;->b:[I

    aget v3, v3, v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/t/cg;->b:[I

    add-int/lit8 v13, v2, 0x1

    aget v4, v4, v13

    const/4 v13, 0x0

    invoke-direct {v9, v3, v4, v13}, Lmaps/t/bx;-><init>(III)V

    new-instance v13, Lmaps/t/bx;

    invoke-direct {v13}, Lmaps/t/bx;-><init>()V

    if-eqz v5, :cond_4

    invoke-virtual {v11, v12}, Lmaps/t/bx;->c(Lmaps/t/bx;)F

    move-result v3

    cmpg-float v3, v3, p1

    if-gtz v3, :cond_4

    const/4 v3, 0x1

    move v4, v3

    :goto_2
    if-nez v5, :cond_8

    invoke-virtual {v8, v10}, Lmaps/t/by;->a(Lmaps/t/bx;)Z

    if-nez v2, :cond_6

    invoke-virtual {v10, v11}, Lmaps/t/bx;->c(Lmaps/t/bx;)F

    move-result v2

    cmpl-float v2, v2, p1

    if-lez v2, :cond_2

    invoke-virtual {v11, v12}, Lmaps/t/bx;->c(Lmaps/t/bx;)F

    move-result v2

    cmpl-float v2, v2, p1

    if-lez v2, :cond_2

    invoke-virtual {v8, v11}, Lmaps/t/by;->a(Lmaps/t/bx;)Z

    :cond_2
    invoke-virtual {v8, v12}, Lmaps/t/by;->a(Lmaps/t/bx;)Z

    invoke-virtual {v8}, Lmaps/t/by;->c()Lmaps/t/cg;

    move-result-object p0

    goto/16 :goto_0

    :cond_3
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_4
    const/4 v3, 0x0

    move v4, v3

    goto :goto_2

    :cond_5
    invoke-virtual {v11, v12}, Lmaps/t/bx;->b(Lmaps/t/bx;)V

    invoke-virtual {v12, v9}, Lmaps/t/bx;->b(Lmaps/t/bx;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/t/cg;->b:[I

    aget v3, v3, v2

    move-object/from16 v0, p0

    iget-object v14, v0, Lmaps/t/cg;->b:[I

    add-int/lit8 v15, v2, 0x1

    aget v14, v14, v15

    const/4 v15, 0x0

    invoke-virtual {v9, v3, v14, v15}, Lmaps/t/bx;->a(III)V

    :cond_6
    invoke-virtual {v10, v11}, Lmaps/t/bx;->c(Lmaps/t/bx;)F

    move-result v3

    cmpg-float v3, v3, p1

    if-gtz v3, :cond_8

    add-int/lit8 v2, v2, 0x3

    mul-int/lit8 v3, v6, 0x3

    if-ne v2, v3, :cond_5

    invoke-virtual {v10, v12}, Lmaps/t/bx;->c(Lmaps/t/bx;)F

    move-result v2

    cmpl-float v2, v2, p1

    if-lez v2, :cond_7

    invoke-virtual {v12, v9}, Lmaps/t/bx;->c(Lmaps/t/bx;)F

    move-result v2

    cmpl-float v2, v2, p1

    if-lez v2, :cond_7

    invoke-virtual {v8, v12}, Lmaps/t/by;->a(Lmaps/t/bx;)Z

    :cond_7
    invoke-virtual {v8, v9}, Lmaps/t/by;->a(Lmaps/t/bx;)Z

    invoke-virtual {v8}, Lmaps/t/by;->c()Lmaps/t/cg;

    move-result-object p0

    goto/16 :goto_0

    :cond_8
    move v3, v2

    :goto_3
    mul-int/lit8 v14, v6, 0x3

    if-ge v3, v14, :cond_d

    move-object/from16 v0, p0

    iget-object v14, v0, Lmaps/t/cg;->b:[I

    aget v14, v14, v3

    move-object/from16 v0, p0

    iget-object v15, v0, Lmaps/t/cg;->b:[I

    add-int/lit8 v16, v3, 0x1

    aget v15, v15, v16

    invoke-virtual {v9, v14, v15}, Lmaps/t/bx;->d(II)V

    if-eqz v4, :cond_9

    add-int/lit8 v14, v7, -0x1

    mul-int/lit8 v14, v14, 0x3

    if-ne v3, v14, :cond_a

    invoke-virtual {v9, v13}, Lmaps/t/bx;->b(Lmaps/t/bx;)V

    :cond_9
    invoke-virtual {v11, v12}, Lmaps/t/bx;->c(Lmaps/t/bx;)F

    move-result v14

    cmpl-float v15, v14, p1

    if-lez v15, :cond_c

    if-eqz v5, :cond_b

    if-ne v3, v2, :cond_b

    invoke-virtual {v13, v11}, Lmaps/t/bx;->b(Lmaps/t/bx;)V

    :goto_4
    invoke-virtual {v10, v11}, Lmaps/t/bx;->b(Lmaps/t/bx;)V

    invoke-virtual {v11, v12}, Lmaps/t/bx;->b(Lmaps/t/bx;)V

    invoke-virtual {v12, v9}, Lmaps/t/bx;->b(Lmaps/t/bx;)V

    :goto_5
    add-int/lit8 v3, v3, 0x3

    goto :goto_3

    :cond_a
    mul-int/lit8 v14, v7, 0x3

    if-ne v3, v14, :cond_9

    goto :goto_5

    :cond_b
    invoke-virtual {v8, v11}, Lmaps/t/by;->a(Lmaps/t/bx;)Z

    goto :goto_4

    :cond_c
    invoke-virtual {v10, v11}, Lmaps/t/bx;->c(Lmaps/t/bx;)F

    move-result v15

    invoke-virtual {v12, v9}, Lmaps/t/bx;->c(Lmaps/t/bx;)F

    move-result v16

    add-float/2addr v15, v14

    float-to-double v0, v15

    move-wide/from16 v17, v0

    add-float v14, v14, v16

    float-to-double v14, v14

    iget v0, v11, Lmaps/t/bx;->a:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-double v0, v0

    move-wide/from16 v19, v0

    mul-double v19, v19, v17

    iget v0, v12, Lmaps/t/bx;->a:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-double v0, v0

    move-wide/from16 v21, v0

    mul-double v21, v21, v14

    add-double v19, v19, v21

    add-double v21, v17, v14

    div-double v19, v19, v21

    iget v0, v11, Lmaps/t/bx;->b:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-double v0, v0

    move-wide/from16 v21, v0

    mul-double v21, v21, v17

    iget v0, v12, Lmaps/t/bx;->b:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-double v0, v0

    move-wide/from16 v23, v0

    mul-double v23, v23, v14

    add-double v21, v21, v23

    add-double v14, v14, v17

    div-double v14, v21, v14

    invoke-static/range {v19 .. v20}, Ljava/lang/Math;->round(D)J

    move-result-wide v16

    move-wide/from16 v0, v16

    long-to-int v0, v0

    move/from16 v16, v0

    invoke-static {v14, v15}, Ljava/lang/Math;->round(D)J

    move-result-wide v14

    long-to-int v14, v14

    move/from16 v0, v16

    invoke-virtual {v11, v0, v14}, Lmaps/t/bx;->d(II)V

    invoke-virtual {v12, v9}, Lmaps/t/bx;->b(Lmaps/t/bx;)V

    goto :goto_5

    :cond_d
    if-nez v5, :cond_e

    invoke-virtual {v11, v12}, Lmaps/t/bx;->c(Lmaps/t/bx;)F

    move-result v2

    cmpl-float v2, v2, p1

    if-lez v2, :cond_f

    :cond_e
    invoke-virtual {v8, v11}, Lmaps/t/by;->a(Lmaps/t/bx;)Z

    :cond_f
    if-eqz v5, :cond_10

    const/4 v2, 0x0

    invoke-virtual {v8, v2}, Lmaps/t/by;->a(I)Lmaps/t/bx;

    move-result-object v2

    invoke-virtual {v8, v2}, Lmaps/t/by;->a(Lmaps/t/bx;)Z

    :goto_6
    invoke-virtual {v8}, Lmaps/t/by;->a()I

    move-result v2

    if-eq v2, v6, :cond_0

    invoke-virtual {v8}, Lmaps/t/by;->c()Lmaps/t/cg;

    move-result-object p0

    goto/16 :goto_0

    :cond_10
    invoke-virtual {v8, v12}, Lmaps/t/by;->a(Lmaps/t/bx;)Z

    goto :goto_6
.end method

.method public c(I)Lmaps/t/cg;
    .locals 9

    const/high16 v8, 0x40000000

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/high16 v0, -0x20000000

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    if-gez p1, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {p0}, Lmaps/t/cg;->b()I

    move-result v4

    new-instance v5, Lmaps/t/by;

    invoke-direct {v5, v4}, Lmaps/t/by;-><init>(I)V

    new-instance v6, Lmaps/t/bx;

    invoke-direct {v6}, Lmaps/t/bx;-><init>()V

    move v3, v2

    :goto_2
    if-ge v3, v4, :cond_5

    invoke-virtual {p0, v3, v6}, Lmaps/t/cg;->a(ILmaps/t/bx;)V

    if-eqz v0, :cond_4

    iget v7, v6, Lmaps/t/bx;->a:I

    if-ge v7, p1, :cond_2

    iget v2, v6, Lmaps/t/bx;->a:I

    add-int/2addr v2, v8

    iput v2, v6, Lmaps/t/bx;->a:I

    move v2, v1

    :cond_2
    :goto_3
    invoke-virtual {v5, v6}, Lmaps/t/by;->a(Lmaps/t/bx;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    iget v7, v6, Lmaps/t/bx;->a:I

    if-le v7, p1, :cond_2

    iget v2, v6, Lmaps/t/bx;->a:I

    sub-int/2addr v2, v8

    iput v2, v6, Lmaps/t/bx;->a:I

    move v2, v1

    goto :goto_3

    :cond_5
    if-eqz v2, :cond_0

    invoke-virtual {v5}, Lmaps/t/by;->c()Lmaps/t/cg;

    move-result-object p0

    goto :goto_0
.end method

.method public d()F
    .locals 4

    const/4 v1, 0x0

    iget v0, p0, Lmaps/t/cg;->d:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    invoke-virtual {p0}, Lmaps/t/cg;->b()I

    move-result v0

    add-int/lit8 v2, v0, -0x1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    invoke-virtual {p0, v0}, Lmaps/t/cg;->b(I)F

    move-result v3

    add-float/2addr v1, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iput v1, p0, Lmaps/t/cg;->d:F

    :cond_1
    iget v0, p0, Lmaps/t/cg;->d:F

    return v0
.end method

.method public d(I)F
    .locals 4

    mul-int/lit8 v0, p1, 0x3

    iget-object v1, p0, Lmaps/t/cg;->b:[I

    add-int/lit8 v2, v0, 0x3

    aget v1, v1, v2

    iget-object v2, p0, Lmaps/t/cg;->b:[I

    aget v2, v2, v0

    sub-int/2addr v1, v2

    iget-object v2, p0, Lmaps/t/cg;->b:[I

    add-int/lit8 v3, v0, 0x3

    add-int/lit8 v3, v3, 0x1

    aget v2, v2, v3

    iget-object v3, p0, Lmaps/t/cg;->b:[I

    add-int/lit8 v0, v0, 0x1

    aget v0, v3, v0

    sub-int v0, v2, v0

    invoke-static {v1, v0}, Lmaps/t/cb;->a(II)F

    move-result v0

    return v0
.end method

.method public e()Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lmaps/t/cg;->b:[I

    array-length v2, v2

    if-lez v2, :cond_1

    iget-object v2, p0, Lmaps/t/cg;->b:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x3

    iget-object v3, p0, Lmaps/t/cg;->b:[I

    aget v3, v3, v1

    iget-object v4, p0, Lmaps/t/cg;->b:[I

    aget v4, v4, v2

    if-ne v3, v4, :cond_0

    iget-object v3, p0, Lmaps/t/cg;->b:[I

    aget v3, v3, v0

    iget-object v4, p0, Lmaps/t/cg;->b:[I

    add-int/lit8 v5, v2, 0x1

    aget v4, v4, v5

    if-ne v3, v4, :cond_0

    iget-object v3, p0, Lmaps/t/cg;->b:[I

    const/4 v4, 0x2

    aget v3, v3, v4

    iget-object v4, p0, Lmaps/t/cg;->b:[I

    add-int/lit8 v2, v2, 0x2

    aget v2, v4, v2

    if-ne v3, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p1, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    instance-of v0, p1, Lmaps/t/cg;

    if-eqz v0, :cond_1

    check-cast p1, Lmaps/t/cg;

    iget-object v0, p0, Lmaps/t/cg;->b:[I

    iget-object v1, p1, Lmaps/t/cg;->b:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Z
    .locals 13

    const-wide/16 v4, 0x0

    const/4 v0, 0x0

    invoke-virtual {p0}, Lmaps/t/cg;->b()I

    move-result v8

    move v3, v0

    move-wide v1, v4

    :goto_0
    add-int/lit8 v6, v8, -0x1

    if-ge v3, v6, :cond_0

    invoke-virtual {p0, v3}, Lmaps/t/cg;->a(I)Lmaps/t/bx;

    move-result-object v6

    add-int/lit8 v7, v3, 0x1

    invoke-virtual {p0, v7}, Lmaps/t/cg;->a(I)Lmaps/t/bx;

    move-result-object v7

    iget v9, v6, Lmaps/t/bx;->a:I

    int-to-long v9, v9

    iget v11, v7, Lmaps/t/bx;->b:I

    int-to-long v11, v11

    mul-long/2addr v9, v11

    iget v7, v7, Lmaps/t/bx;->a:I

    int-to-long v11, v7

    iget v6, v6, Lmaps/t/bx;->b:I

    int-to-long v6, v6

    mul-long/2addr v6, v11

    sub-long v6, v9, v6

    add-long/2addr v6, v1

    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move-wide v1, v6

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lmaps/t/cg;->e()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {p0}, Lmaps/t/cg;->c()Lmaps/t/bx;

    move-result-object v3

    invoke-virtual {p0, v0}, Lmaps/t/cg;->a(I)Lmaps/t/bx;

    move-result-object v6

    iget v7, v3, Lmaps/t/bx;->a:I

    int-to-long v7, v7

    iget v9, v6, Lmaps/t/bx;->b:I

    int-to-long v9, v9

    mul-long/2addr v7, v9

    iget v6, v6, Lmaps/t/bx;->a:I

    int-to-long v9, v6

    iget v3, v3, Lmaps/t/bx;->b:I

    int-to-long v11, v3

    mul-long/2addr v9, v11

    sub-long v6, v7, v9

    add-long/2addr v1, v6

    :cond_1
    cmp-long v1, v1, v4

    if-lez v1, :cond_2

    const/4 v0, 0x1

    :cond_2
    return v0
.end method

.method public g()I
    .locals 9

    const/high16 v0, -0x20000000

    invoke-virtual {p0}, Lmaps/t/cg;->b()I

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {}, Lmaps/ay/j;->a()Lmaps/ay/j;

    move-result-object v2

    new-instance v4, Lmaps/t/bx;

    invoke-direct {v4}, Lmaps/t/bx;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lmaps/t/cg;->a(I)Lmaps/t/bx;

    move-result-object v1

    invoke-virtual {v1, v4}, Lmaps/t/bx;->i(Lmaps/t/bx;)V

    const/4 v1, 0x1

    :goto_1
    if-ge v1, v3, :cond_2

    new-instance v5, Lmaps/t/bx;

    invoke-direct {v5, v4}, Lmaps/t/bx;-><init>(Lmaps/t/bx;)V

    invoke-virtual {p0, v1}, Lmaps/t/cg;->a(I)Lmaps/t/bx;

    move-result-object v6

    invoke-virtual {v6, v4}, Lmaps/t/bx;->i(Lmaps/t/bx;)V

    invoke-virtual {v5}, Lmaps/t/bx;->f()I

    move-result v5

    invoke-static {v5}, Lmaps/t/bx;->c(I)I

    move-result v5

    invoke-static {v5}, Lmaps/ay/j;->a(I)D

    move-result-wide v5

    invoke-virtual {v4}, Lmaps/t/bx;->f()I

    move-result v7

    invoke-static {v7}, Lmaps/t/bx;->c(I)I

    move-result v7

    invoke-static {v7}, Lmaps/ay/j;->a(I)D

    move-result-wide v7

    invoke-static {v5, v6, v7, v8}, Lmaps/ay/j;->a(DD)Lmaps/ay/j;

    move-result-object v5

    invoke-virtual {v2, v5}, Lmaps/ay/j;->b(Lmaps/ay/j;)Lmaps/ay/j;

    move-result-object v2

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, Lmaps/ay/j;->e()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lmaps/ay/j;->a(I)D

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lmaps/ay/j;->a(D)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v2}, Lmaps/ay/j;->i()Lmaps/ay/j;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ay/j;->h()D

    move-result-wide v0

    invoke-static {v0, v1}, Lmaps/ay/j;->c(D)I

    move-result v0

    invoke-static {v0}, Lmaps/t/bx;->c(I)I

    move-result v0

    goto :goto_0
.end method

.method public h()Lmaps/t/cg;
    .locals 6

    iget-object v0, p0, Lmaps/t/cg;->b:[I

    array-length v1, v0

    new-array v2, v1, [I

    iget-object v3, p0, Lmaps/t/cg;->b:[I

    const/4 v0, 0x0

    :goto_0
    iget-object v4, p0, Lmaps/t/cg;->b:[I

    array-length v4, v4

    if-ge v0, v4, :cond_0

    sub-int v4, v1, v0

    add-int/lit8 v4, v4, -0x3

    aget v4, v3, v4

    aput v4, v2, v0

    add-int/lit8 v4, v0, 0x1

    sub-int v5, v1, v0

    add-int/lit8 v5, v5, -0x2

    aget v5, v3, v5

    aput v5, v2, v4

    add-int/lit8 v4, v0, 0x2

    sub-int v5, v1, v0

    add-int/lit8 v5, v5, -0x1

    aget v5, v3, v5

    aput v5, v2, v4

    add-int/lit8 v0, v0, 0x3

    goto :goto_0

    :cond_0
    new-instance v0, Lmaps/t/cg;

    invoke-direct {v0, v2}, Lmaps/t/cg;-><init>([I)V

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lmaps/t/cg;->b:[I

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([I)I

    move-result v0

    return v0
.end method

.method public i()I
    .locals 1

    iget-object v0, p0, Lmaps/t/cg;->b:[I

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x4

    add-int/lit16 v0, v0, 0xa0

    return v0
.end method
