.class public Lmaps/t/cj;
.super Ljava/lang/Object;


# instance fields
.field private a:Lmaps/t/ah;

.field private b:I

.field private c:B

.field private d:I

.field private e:[Lmaps/t/ca;

.field private f:Lmaps/t/cl;

.field private g:[Ljava/lang/String;

.field private h:J

.field private i:[Ljava/lang/String;

.field private j:[Ljava/lang/String;

.field private k:I

.field private l:Lmaps/o/c;

.field private m:[Lmaps/t/cr;

.field private n:J


# direct methods
.method public constructor <init>()V
    .locals 3

    const-wide/16 v1, -0x1

    const/4 v0, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lmaps/t/cj;->d:I

    iput-wide v1, p0, Lmaps/t/cj;->h:J

    iput v0, p0, Lmaps/t/cj;->k:I

    sget-object v0, Lmaps/o/c;->a:Lmaps/o/c;

    iput-object v0, p0, Lmaps/t/cj;->l:Lmaps/o/c;

    iput-wide v1, p0, Lmaps/t/cj;->n:J

    return-void
.end method


# virtual methods
.method public a()Lmaps/t/ar;
    .locals 19

    new-instance v2, Lmaps/t/ar;

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/t/cj;->f:Lmaps/t/cl;

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/t/cj;->g:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lmaps/t/cj;->a:Lmaps/t/ah;

    move-object/from16 v0, p0

    iget v6, v0, Lmaps/t/cj;->b:I

    move-object/from16 v0, p0

    iget-byte v7, v0, Lmaps/t/cj;->c:B

    move-object/from16 v0, p0

    iget v8, v0, Lmaps/t/cj;->d:I

    move-object/from16 v0, p0

    iget-object v9, v0, Lmaps/t/cj;->i:[Ljava/lang/String;

    if-nez v9, :cond_0

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/String;

    :goto_0
    move-object/from16 v0, p0

    iget-object v10, v0, Lmaps/t/cj;->j:[Ljava/lang/String;

    if-nez v10, :cond_1

    const/4 v10, 0x0

    new-array v10, v10, [Ljava/lang/String;

    :goto_1
    move-object/from16 v0, p0

    iget v11, v0, Lmaps/t/cj;->k:I

    move-object/from16 v0, p0

    iget-object v12, v0, Lmaps/t/cj;->e:[Lmaps/t/ca;

    if-nez v12, :cond_2

    const/4 v12, 0x0

    new-array v12, v12, [Lmaps/t/ca;

    :goto_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lmaps/t/cj;->l:Lmaps/o/c;

    move-object/from16 v0, p0

    iget-object v14, v0, Lmaps/t/cj;->m:[Lmaps/t/cr;

    move-object/from16 v0, p0

    iget-wide v15, v0, Lmaps/t/cj;->h:J

    move-object/from16 v0, p0

    iget-wide v0, v0, Lmaps/t/cj;->n:J

    move-wide/from16 v17, v0

    invoke-direct/range {v2 .. v18}, Lmaps/t/ar;-><init>(Lmaps/t/cl;[Ljava/lang/String;Lmaps/t/ah;IBI[Ljava/lang/String;[Ljava/lang/String;I[Lmaps/t/ca;Lmaps/o/c;[Lmaps/t/cr;JJ)V

    return-object v2

    :cond_0
    move-object/from16 v0, p0

    iget-object v9, v0, Lmaps/t/cj;->i:[Ljava/lang/String;

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    iget-object v10, v0, Lmaps/t/cj;->j:[Ljava/lang/String;

    goto :goto_1

    :cond_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lmaps/t/cj;->e:[Lmaps/t/ca;

    goto :goto_2
.end method

.method public a(I)Lmaps/t/cj;
    .locals 0

    iput p1, p0, Lmaps/t/cj;->d:I

    return-object p0
.end method

.method public a(J)Lmaps/t/cj;
    .locals 0

    iput-wide p1, p0, Lmaps/t/cj;->h:J

    return-object p0
.end method

.method public a(Lmaps/o/c;)Lmaps/t/cj;
    .locals 0

    iput-object p1, p0, Lmaps/t/cj;->l:Lmaps/o/c;

    return-object p0
.end method

.method public a(Lmaps/t/ah;)Lmaps/t/cj;
    .locals 0

    iput-object p1, p0, Lmaps/t/cj;->a:Lmaps/t/ah;

    return-object p0
.end method

.method public a(Lmaps/t/cl;)Lmaps/t/cj;
    .locals 0

    iput-object p1, p0, Lmaps/t/cj;->f:Lmaps/t/cl;

    return-object p0
.end method

.method public a([Ljava/lang/String;)Lmaps/t/cj;
    .locals 0

    iput-object p1, p0, Lmaps/t/cj;->i:[Ljava/lang/String;

    return-object p0
.end method

.method public a([Lmaps/t/ca;)Lmaps/t/cj;
    .locals 0

    iput-object p1, p0, Lmaps/t/cj;->e:[Lmaps/t/ca;

    return-object p0
.end method

.method public b(I)Lmaps/t/cj;
    .locals 0

    iput p1, p0, Lmaps/t/cj;->b:I

    return-object p0
.end method

.method public b(J)Lmaps/t/cj;
    .locals 0

    iput-wide p1, p0, Lmaps/t/cj;->n:J

    return-object p0
.end method

.method public b([Ljava/lang/String;)Lmaps/t/cj;
    .locals 0

    iput-object p1, p0, Lmaps/t/cj;->j:[Ljava/lang/String;

    return-object p0
.end method

.method public c(I)Lmaps/t/cj;
    .locals 0

    iput p1, p0, Lmaps/t/cj;->k:I

    return-object p0
.end method
