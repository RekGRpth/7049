.class public Lmaps/t/cl;
.super Ljava/lang/Object;


# instance fields
.field private final a:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/t/cl;->a:Ljava/util/ArrayList;

    return-void
.end method

.method public static a(Ljava/io/DataInput;I)Lmaps/t/cl;
    .locals 5

    new-instance v1, Lmaps/t/cl;

    invoke-direct {v1}, Lmaps/t/cl;-><init>()V

    invoke-static {p0}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    iget-object v3, v1, Lmaps/t/cl;->a:Ljava/util/ArrayList;

    invoke-static {v0, p0, p1}, Lmaps/t/aa;->a(ILjava/io/DataInput;I)Lmaps/t/aa;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method


# virtual methods
.method public a()Lmaps/t/aa;
    .locals 1

    invoke-static {}, Lmaps/t/aa;->a()Lmaps/t/aa;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Lmaps/t/aa;
    .locals 1

    iget-object v0, p0, Lmaps/t/cl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    if-gez p1, :cond_1

    :cond_0
    invoke-static {}, Lmaps/t/aa;->a()Lmaps/t/aa;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lmaps/t/cl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/aa;

    goto :goto_0
.end method
