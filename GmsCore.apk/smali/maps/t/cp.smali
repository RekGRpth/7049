.class Lmaps/t/cp;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/t/h;


# instance fields
.field final synthetic a:Lmaps/t/f;

.field private b:I

.field private c:I


# direct methods
.method private constructor <init>(Lmaps/t/f;)V
    .locals 1

    const/4 v0, 0x0

    iput-object p1, p0, Lmaps/t/cp;->a:Lmaps/t/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lmaps/t/cp;->b:I

    iput v0, p0, Lmaps/t/cp;->c:I

    return-void
.end method

.method synthetic constructor <init>(Lmaps/t/f;Lmaps/t/ag;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/t/cp;-><init>(Lmaps/t/f;)V

    return-void
.end method


# virtual methods
.method public a()Lmaps/t/ca;
    .locals 2

    iget-object v0, p0, Lmaps/t/cp;->a:Lmaps/t/f;

    invoke-static {v0}, Lmaps/t/f;->a(Lmaps/t/f;)Ljava/util/List;

    move-result-object v0

    iget v1, p0, Lmaps/t/cp;->b:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/ca;

    return-object v0
.end method

.method public b()V
    .locals 1

    iget v0, p0, Lmaps/t/cp;->b:I

    iput v0, p0, Lmaps/t/cp;->c:I

    return-void
.end method

.method public c()V
    .locals 1

    iget v0, p0, Lmaps/t/cp;->c:I

    iput v0, p0, Lmaps/t/cp;->b:I

    return-void
.end method

.method public d()Lmaps/t/ca;
    .locals 3

    iget-object v0, p0, Lmaps/t/cp;->a:Lmaps/t/f;

    invoke-static {v0}, Lmaps/t/f;->a(Lmaps/t/f;)Ljava/util/List;

    move-result-object v0

    iget v1, p0, Lmaps/t/cp;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lmaps/t/cp;->b:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/ca;

    return-object v0
.end method

.method public hasNext()Z
    .locals 2

    iget v0, p0, Lmaps/t/cp;->b:I

    iget-object v1, p0, Lmaps/t/cp;->a:Lmaps/t/f;

    invoke-static {v1}, Lmaps/t/f;->a(Lmaps/t/f;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lmaps/t/cp;->d()Lmaps/t/ca;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "remove() not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
