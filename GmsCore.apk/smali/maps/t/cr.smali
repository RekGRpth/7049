.class public Lmaps/t/cr;
.super Ljava/lang/Object;


# instance fields
.field protected final b:Lmaps/t/v;

.field protected final c:Lmaps/t/ca;

.field protected final d:I

.field protected final e:I


# direct methods
.method protected constructor <init>(Lmaps/t/v;Lmaps/t/ca;II)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/t/cr;->b:Lmaps/t/v;

    iput-object p2, p0, Lmaps/t/cr;->c:Lmaps/t/ca;

    iput p3, p0, Lmaps/t/cr;->d:I

    iput p4, p0, Lmaps/t/cr;->e:I

    return-void
.end method

.method public static a(Ljava/io/DataInput;Lmaps/t/cd;)Lmaps/t/cr;
    .locals 4

    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v2

    invoke-virtual {p1}, Lmaps/t/cd;->a()I

    move-result v0

    const/16 v1, 0xb

    if-ne v0, v1, :cond_1

    invoke-static {p0}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v1

    invoke-static {v2}, Lmaps/t/cr;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0, p1}, Lmaps/t/ar;->a(Ljava/io/DataInput;Lmaps/t/cd;)Lmaps/t/ca;

    move-result-object v2

    new-instance v0, Lmaps/t/x;

    invoke-direct {v0, v1, v2}, Lmaps/t/x;-><init>(ILmaps/t/ca;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lmaps/t/ba;

    invoke-direct {v0, v1}, Lmaps/t/ba;-><init>(I)V

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    invoke-static {v2}, Lmaps/t/cr;->c(I)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {p0, p1}, Lmaps/t/ar;->a(Ljava/io/DataInput;Lmaps/t/cd;)Lmaps/t/ca;

    move-result-object v3

    invoke-static {v2}, Lmaps/t/cr;->b(I)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {p0}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v0

    :cond_2
    new-instance v1, Lmaps/t/q;

    invoke-direct {v1, v3, v2, v0}, Lmaps/t/q;-><init>(Lmaps/t/ca;II)V

    move-object v0, v1

    goto :goto_0

    :cond_3
    const/4 v0, 0x2

    invoke-static {v2, v0}, Lmaps/t/ab;->a(II)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p0}, Lmaps/t/v;->b(Ljava/io/DataInput;)Lmaps/t/bt;

    move-result-object v0

    :goto_1
    new-instance v1, Lmaps/t/ch;

    invoke-direct {v1, v0}, Lmaps/t/ch;-><init>(Lmaps/t/v;)V

    move-object v0, v1

    goto :goto_0

    :cond_4
    invoke-static {p0}, Lmaps/t/v;->a(Ljava/io/DataInput;)Lmaps/t/bs;

    move-result-object v0

    goto :goto_1
.end method

.method static synthetic a(I)Z
    .locals 1

    invoke-static {p0}, Lmaps/t/cr;->b(I)Z

    move-result v0

    return v0
.end method

.method private static b(I)Z
    .locals 1

    const/4 v0, 0x2

    invoke-static {p0, v0}, Lmaps/t/ab;->a(II)Z

    move-result v0

    return v0
.end method

.method private static c(I)Z
    .locals 1

    const/4 v0, 0x1

    invoke-static {p0, v0}, Lmaps/t/ab;->a(II)Z

    move-result v0

    return v0
.end method
