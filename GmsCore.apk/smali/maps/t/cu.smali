.class public Lmaps/t/cu;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/t/ao;


# instance fields
.field private final a:Lmaps/t/bb;


# direct methods
.method protected constructor <init>(Lmaps/t/bb;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/t/cu;->a:Lmaps/t/bb;

    return-void
.end method

.method public static a(Lmaps/t/bb;)Lmaps/t/cu;
    .locals 1

    new-instance v0, Lmaps/t/be;

    invoke-direct {v0}, Lmaps/t/be;-><init>()V

    invoke-virtual {v0, p0}, Lmaps/t/be;->a(Lmaps/t/bb;)Lmaps/t/be;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/be;->a()Lmaps/t/cu;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Lmaps/t/cm;
    .locals 1

    sget-object v0, Lmaps/t/cm;->d:Lmaps/t/cm;

    return-object v0
.end method

.method public a(Lmaps/bb/c;)V
    .locals 2

    const/4 v0, 0x6

    invoke-virtual {p0}, Lmaps/t/cu;->b()Lmaps/t/bg;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/t/bg;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lmaps/bb/c;->b(ILjava/lang/String;)Lmaps/bb/c;

    return-void
.end method

.method public a(Lmaps/o/c;)Z
    .locals 1

    sget-object v0, Lmaps/o/c;->n:Lmaps/o/c;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lmaps/t/cu;->a:Lmaps/t/bb;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lmaps/t/ao;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public b(Lmaps/t/ao;)I
    .locals 2

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lmaps/t/cu;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public b()Lmaps/t/bg;
    .locals 1

    iget-object v0, p0, Lmaps/t/cu;->a:Lmaps/t/bb;

    invoke-virtual {v0}, Lmaps/t/bb;->a()Lmaps/t/bg;

    move-result-object v0

    return-object v0
.end method

.method public c()Lmaps/t/bb;
    .locals 1

    iget-object v0, p0, Lmaps/t/cu;->a:Lmaps/t/bb;

    return-object v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lmaps/t/ao;

    invoke-virtual {p0, p1}, Lmaps/t/cu;->b(Lmaps/t/ao;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-nez p1, :cond_2

    iget-object v2, p0, Lmaps/t/cu;->a:Lmaps/t/bb;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_2
    instance-of v0, p1, Lmaps/t/cu;

    if-nez v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lmaps/t/cu;->a:Lmaps/t/bb;

    invoke-virtual {v0}, Lmaps/t/bb;->a()Lmaps/t/bg;

    move-result-object v0

    check-cast p1, Lmaps/t/cu;

    iget-object v1, p1, Lmaps/t/cu;->a:Lmaps/t/bb;

    invoke-virtual {v1}, Lmaps/t/bb;->a()Lmaps/t/bg;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/ap/e;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lmaps/t/cu;->a:Lmaps/t/bb;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    return v0

    :cond_0
    iget-object v0, p0, Lmaps/t/cu;->a:Lmaps/t/bb;

    invoke-virtual {v0}, Lmaps/t/bb;->a()Lmaps/t/bg;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/bg;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lmaps/t/cu;->b()Lmaps/t/bg;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/bg;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
