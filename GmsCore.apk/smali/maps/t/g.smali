.class public Lmaps/t/g;
.super Lmaps/t/au;


# static fields
.field public static final a:Lmaps/t/g;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lmaps/t/g;

    const/4 v1, 0x5

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lmaps/t/g;-><init>(II)V

    sput-object v0, Lmaps/t/g;->a:Lmaps/t/g;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/t/au;-><init>(I)V

    return-void
.end method

.method public constructor <init>(II)V
    .locals 1

    add-int/lit8 v0, p2, 0x4

    shl-int v0, p1, v0

    invoke-direct {p0, v0}, Lmaps/t/au;-><init>(I)V

    return-void
.end method

.method public static a(Ljava/io/DataInput;I)Lmaps/t/g;
    .locals 2

    new-instance v0, Lmaps/t/g;

    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v1

    invoke-direct {v0, v1}, Lmaps/t/g;-><init>(I)V

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lmaps/t/g;->b:I

    shr-int/lit8 v0, v0, 0x4

    and-int/lit8 v0, v0, 0xf

    return v0
.end method
