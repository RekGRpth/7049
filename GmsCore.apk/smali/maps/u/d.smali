.class public Lmaps/u/d;
.super Lmaps/u/c;


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private b:Lmaps/cr/a;

.field private c:Lmaps/al/q;

.field private d:I

.field private e:I

.field private f:Lmaps/u/e;

.field private g:Lmaps/q/aq;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 3

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Lmaps/u/c;-><init>()V

    iput-object v1, p0, Lmaps/u/d;->b:Lmaps/cr/a;

    iput-object v1, p0, Lmaps/u/d;->c:Lmaps/al/q;

    iput v0, p0, Lmaps/u/d;->d:I

    iput v0, p0, Lmaps/u/d;->e:I

    iput-object p1, p0, Lmaps/u/d;->a:Landroid/content/res/Resources;

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    new-instance v0, Lmaps/u/e;

    iget v1, p0, Lmaps/u/d;->d:I

    int-to-float v1, v1

    iget v2, p0, Lmaps/u/d;->e:I

    int-to-float v2, v2

    invoke-direct {v0, v1, v2}, Lmaps/u/e;-><init>(FF)V

    iput-object v0, p0, Lmaps/u/d;->f:Lmaps/u/e;

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 9

    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x1

    const/high16 v5, 0x47800000

    const/4 v4, 0x0

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lmaps/k/b;->a()Lmaps/k/b;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/k/b;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/u/d;->b:Lmaps/cr/a;

    if-nez v0, :cond_2

    new-instance v0, Lmaps/cr/a;

    invoke-direct {v0, p1}, Lmaps/cr/a;-><init>(Lmaps/cr/c;)V

    iput-object v0, p0, Lmaps/u/d;->b:Lmaps/cr/a;

    iget-object v0, p0, Lmaps/u/d;->b:Lmaps/cr/a;

    invoke-virtual {v0, v6}, Lmaps/cr/a;->c(Z)V

    iget-object v0, p0, Lmaps/u/d;->b:Lmaps/cr/a;

    iget-object v1, p0, Lmaps/u/d;->a:Landroid/content/res/Resources;

    sget v2, Lmaps/ad/e;->Y:I

    invoke-virtual {v0, v1, v2}, Lmaps/cr/a;->a(Landroid/content/res/Resources;I)V

    new-instance v0, Lmaps/al/r;

    const/16 v1, 0x8

    new-array v1, v1, [I

    aput v4, v1, v4

    aput v4, v1, v6

    const/4 v2, 0x2

    aput v4, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lmaps/u/d;->b:Lmaps/cr/a;

    invoke-virtual {v3}, Lmaps/cr/a;->c()F

    move-result v3

    mul-float/2addr v3, v5

    float-to-int v3, v3

    aput v3, v1, v2

    iget-object v2, p0, Lmaps/u/d;->b:Lmaps/cr/a;

    invoke-virtual {v2}, Lmaps/cr/a;->b()F

    move-result v2

    mul-float/2addr v2, v5

    float-to-int v2, v2

    aput v2, v1, v7

    aput v4, v1, v8

    const/4 v2, 0x6

    iget-object v3, p0, Lmaps/u/d;->b:Lmaps/cr/a;

    invoke-virtual {v3}, Lmaps/cr/a;->b()F

    move-result v3

    mul-float/2addr v3, v5

    float-to-int v3, v3

    aput v3, v1, v2

    const/4 v2, 0x7

    iget-object v3, p0, Lmaps/u/d;->b:Lmaps/cr/a;

    invoke-virtual {v3}, Lmaps/cr/a;->c()F

    move-result v3

    mul-float/2addr v3, v5

    float-to-int v3, v3

    aput v3, v1, v2

    invoke-direct {v0, v1}, Lmaps/al/r;-><init>([I)V

    iput-object v0, p0, Lmaps/u/d;->c:Lmaps/al/q;

    :cond_2
    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-virtual {p1}, Lmaps/cr/c;->D()V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    iget v1, p0, Lmaps/u/d;->d:I

    int-to-float v1, v1

    iget v2, p0, Lmaps/u/d;->e:I

    int-to-float v2, v2

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    iget-object v1, p0, Lmaps/u/d;->b:Lmaps/cr/a;

    invoke-virtual {v1}, Lmaps/cr/a;->d()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lmaps/u/d;->b:Lmaps/cr/a;

    invoke-virtual {v2}, Lmaps/cr/a;->e()I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x3f800000

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    invoke-virtual {p1}, Lmaps/cr/c;->r()V

    invoke-virtual {p1}, Lmaps/cr/c;->t()V

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x1e01

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    const/16 v1, 0x303

    invoke-interface {v0, v6, v1}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    iget-object v1, p0, Lmaps/u/d;->c:Lmaps/al/q;

    invoke-virtual {v1, p1}, Lmaps/al/q;->d(Lmaps/cr/c;)V

    iget-object v1, p0, Lmaps/u/d;->b:Lmaps/cr/a;

    invoke-virtual {v1, v0}, Lmaps/cr/a;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    iget-object v1, p1, Lmaps/cr/c;->f:Lmaps/al/o;

    invoke-virtual {v1, p1}, Lmaps/al/o;->d(Lmaps/cr/c;)V

    invoke-interface {v0, v8, v4, v7}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    invoke-virtual {p1}, Lmaps/cr/c;->F()V

    goto/16 :goto_0
.end method

.method public a_(Lmaps/cr/c;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lmaps/u/d;->b:Lmaps/cr/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/u/d;->b:Lmaps/cr/a;

    invoke-virtual {v0}, Lmaps/cr/a;->g()V

    iput-object v2, p0, Lmaps/u/d;->b:Lmaps/cr/a;

    :cond_0
    iget-object v0, p0, Lmaps/u/d;->c:Lmaps/al/q;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/u/d;->c:Lmaps/al/q;

    invoke-virtual {v0, p1}, Lmaps/al/q;->e(Lmaps/cr/c;)V

    iput-object v2, p0, Lmaps/u/d;->c:Lmaps/al/q;

    :cond_1
    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/u/d;->g:Lmaps/q/aq;

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lmaps/cr/c;->m()Lmaps/q/ad;

    move-result-object v0

    iget-object v1, p0, Lmaps/u/d;->g:Lmaps/q/aq;

    invoke-virtual {v0, v1}, Lmaps/q/ad;->b(Lmaps/q/ba;)V

    invoke-virtual {p1}, Lmaps/cr/c;->m()Lmaps/q/ad;

    move-result-object v0

    iget-object v1, p0, Lmaps/u/d;->f:Lmaps/u/e;

    invoke-virtual {v0, v1}, Lmaps/q/ad;->b(Lmaps/q/x;)V

    iput-object v2, p0, Lmaps/u/d;->f:Lmaps/u/e;

    iput-object v2, p0, Lmaps/u/d;->g:Lmaps/q/aq;

    :cond_2
    return-void
.end method

.method public b(Lmaps/cr/c;)V
    .locals 8

    const/16 v7, 0x9

    const/4 v6, 0x5

    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v5, 0x0

    new-instance v0, Lmaps/q/aq;

    sget-object v3, Lmaps/y/am;->B:Lmaps/y/am;

    invoke-direct {v0, v3}, Lmaps/q/aq;-><init>(Lmaps/y/am;)V

    iput-object v0, p0, Lmaps/u/d;->g:Lmaps/q/aq;

    iget-object v0, p0, Lmaps/u/d;->g:Lmaps/q/aq;

    const-string v3, "HudWatermark"

    invoke-virtual {v0, v3}, Lmaps/q/aq;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lmaps/u/d;->b:Lmaps/cr/a;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lmaps/ap/q;->b(Z)V

    new-instance v0, Lmaps/cr/a;

    sget-boolean v3, Lmaps/ae/h;->F:Z

    invoke-direct {v0, p1, v3}, Lmaps/cr/a;-><init>(Lmaps/cr/c;Z)V

    iput-object v0, p0, Lmaps/u/d;->b:Lmaps/cr/a;

    iget-object v0, p0, Lmaps/u/d;->b:Lmaps/cr/a;

    invoke-virtual {v0, v1}, Lmaps/cr/a;->c(Z)V

    iget-object v0, p0, Lmaps/u/d;->b:Lmaps/cr/a;

    iget-object v3, p0, Lmaps/u/d;->a:Landroid/content/res/Resources;

    sget v4, Lmaps/ad/e;->Y:I

    invoke-virtual {v0, v3, v4}, Lmaps/cr/a;->a(Landroid/content/res/Resources;I)V

    new-instance v0, Lmaps/q/ao;

    const/16 v3, 0x14

    new-array v3, v3, [F

    aput v5, v3, v2

    iget-object v2, p0, Lmaps/u/d;->b:Lmaps/cr/a;

    invoke-virtual {v2}, Lmaps/cr/a;->e()I

    move-result v2

    int-to-float v2, v2

    aput v2, v3, v1

    const/4 v2, 0x2

    aput v5, v3, v2

    const/4 v2, 0x3

    aput v5, v3, v2

    const/4 v2, 0x4

    aput v5, v3, v2

    aput v5, v3, v6

    const/4 v2, 0x6

    aput v5, v3, v2

    const/4 v2, 0x7

    aput v5, v3, v2

    const/16 v2, 0x8

    aput v5, v3, v2

    iget-object v2, p0, Lmaps/u/d;->b:Lmaps/cr/a;

    invoke-virtual {v2}, Lmaps/cr/a;->c()F

    move-result v2

    aput v2, v3, v7

    const/16 v2, 0xa

    iget-object v4, p0, Lmaps/u/d;->b:Lmaps/cr/a;

    invoke-virtual {v4}, Lmaps/cr/a;->d()I

    move-result v4

    int-to-float v4, v4

    aput v4, v3, v2

    const/16 v2, 0xb

    iget-object v4, p0, Lmaps/u/d;->b:Lmaps/cr/a;

    invoke-virtual {v4}, Lmaps/cr/a;->e()I

    move-result v4

    int-to-float v4, v4

    aput v4, v3, v2

    const/16 v2, 0xc

    aput v5, v3, v2

    const/16 v2, 0xd

    iget-object v4, p0, Lmaps/u/d;->b:Lmaps/cr/a;

    invoke-virtual {v4}, Lmaps/cr/a;->b()F

    move-result v4

    aput v4, v3, v2

    const/16 v2, 0xe

    aput v5, v3, v2

    const/16 v2, 0xf

    iget-object v4, p0, Lmaps/u/d;->b:Lmaps/cr/a;

    invoke-virtual {v4}, Lmaps/cr/a;->d()I

    move-result v4

    int-to-float v4, v4

    aput v4, v3, v2

    const/16 v2, 0x10

    aput v5, v3, v2

    const/16 v2, 0x11

    aput v5, v3, v2

    const/16 v2, 0x12

    iget-object v4, p0, Lmaps/u/d;->b:Lmaps/cr/a;

    invoke-virtual {v4}, Lmaps/cr/a;->b()F

    move-result v4

    aput v4, v3, v2

    const/16 v2, 0x13

    iget-object v4, p0, Lmaps/u/d;->b:Lmaps/cr/a;

    invoke-virtual {v4}, Lmaps/cr/a;->c()F

    move-result v4

    aput v4, v3, v2

    invoke-direct {v0, v3, v7, v6}, Lmaps/q/ao;-><init>([FII)V

    iget-object v2, p0, Lmaps/u/d;->g:Lmaps/q/aq;

    invoke-virtual {v2, v0}, Lmaps/q/aq;->a(Lmaps/q/r;)V

    iget-object v0, p0, Lmaps/u/d;->g:Lmaps/q/aq;

    iget-object v2, p0, Lmaps/u/d;->b:Lmaps/cr/a;

    invoke-virtual {v0, v2}, Lmaps/q/aq;->a(Lmaps/q/z;)V

    iget-object v0, p0, Lmaps/u/d;->g:Lmaps/q/aq;

    new-instance v2, Lmaps/q/ar;

    invoke-direct {v2}, Lmaps/q/ar;-><init>()V

    invoke-virtual {v0, v2}, Lmaps/q/aq;->a(Lmaps/q/z;)V

    iget-object v0, p0, Lmaps/u/d;->g:Lmaps/q/aq;

    new-instance v2, Lmaps/q/n;

    const/16 v3, 0x303

    invoke-direct {v2, v1, v3}, Lmaps/q/n;-><init>(II)V

    invoke-virtual {v0, v2}, Lmaps/q/aq;->a(Lmaps/q/z;)V

    iget-object v0, p0, Lmaps/u/d;->f:Lmaps/u/e;

    iget-object v1, p0, Lmaps/u/d;->g:Lmaps/q/aq;

    invoke-virtual {v0, v1}, Lmaps/u/e;->a(Lmaps/q/aq;)V

    invoke-virtual {p1}, Lmaps/cr/c;->m()Lmaps/q/ad;

    move-result-object v0

    iget-object v1, p0, Lmaps/u/d;->f:Lmaps/u/e;

    invoke-virtual {v0, v1}, Lmaps/q/ad;->a(Lmaps/q/x;)V

    invoke-virtual {p1}, Lmaps/cr/c;->m()Lmaps/q/ad;

    move-result-object v0

    iget-object v1, p0, Lmaps/u/d;->g:Lmaps/q/aq;

    invoke-virtual {v0, v1}, Lmaps/q/ad;->a(Lmaps/q/ba;)V

    return-void

    :cond_0
    move v0, v2

    goto/16 :goto_0
.end method

.method public h()V
    .locals 3

    const/4 v0, 0x0

    iput v0, p0, Lmaps/u/d;->e:I

    invoke-static {}, Lmaps/k/b;->a()Lmaps/k/b;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/k/b;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x60

    iput v0, p0, Lmaps/u/d;->e:I

    :cond_0
    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/u/d;->f:Lmaps/u/e;

    iget v1, p0, Lmaps/u/d;->d:I

    int-to-float v1, v1

    iget v2, p0, Lmaps/u/d;->e:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Lmaps/u/e;->a(FF)V

    :cond_1
    return-void
.end method
