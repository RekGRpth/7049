.class final Lmaps/u/e;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/q/x;


# instance fields
.field private a:Lmaps/q/aq;

.field private b:F

.field private c:F

.field private d:Lmaps/q/k;

.field private e:Lmaps/q/i;


# direct methods
.method public constructor <init>(FF)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lmaps/q/k;

    invoke-direct {v0}, Lmaps/q/k;-><init>()V

    iput-object v0, p0, Lmaps/u/e;->d:Lmaps/q/k;

    iput p1, p0, Lmaps/u/e;->b:F

    iput p2, p0, Lmaps/u/e;->c:F

    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/u/e;->d:Lmaps/q/k;

    iget v1, p0, Lmaps/u/e;->b:F

    iget v2, p0, Lmaps/u/e;->c:F

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lmaps/q/k;->a(FFF)Lmaps/q/k;

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(FF)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/u/e;->b:F

    cmpl-float v0, v0, p1

    if-nez v0, :cond_1

    iget v0, p0, Lmaps/u/e;->c:F

    cmpl-float v0, v0, p2

    if-nez v0, :cond_1

    monitor-exit p0

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput p1, p0, Lmaps/u/e;->b:F

    iput p2, p0, Lmaps/u/e;->c:F

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/u/e;->e:Lmaps/q/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/u/e;->e:Lmaps/q/i;

    sget-object v1, Lmaps/q/ay;->b:Lmaps/q/ay;

    invoke-interface {v0, p0, v1}, Lmaps/q/i;->a(Lmaps/q/x;Lmaps/q/c;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Lmaps/q/aq;)V
    .locals 0

    iput-object p1, p0, Lmaps/u/e;->a:Lmaps/q/aq;

    return-void
.end method

.method public a(Lmaps/q/i;)V
    .locals 1

    iput-object p1, p0, Lmaps/u/e;->e:Lmaps/q/i;

    sget-object v0, Lmaps/q/ay;->b:Lmaps/q/ay;

    invoke-interface {p1, p0, v0}, Lmaps/q/i;->a(Lmaps/q/x;Lmaps/q/c;)V

    return-void
.end method

.method public b(Lmaps/q/i;)V
    .locals 2

    iget-object v0, p0, Lmaps/u/e;->a:Lmaps/q/aq;

    iget-object v1, p0, Lmaps/u/e;->d:Lmaps/q/k;

    invoke-virtual {v0, v1}, Lmaps/q/aq;->a(Lmaps/q/k;)V

    return-void
.end method
