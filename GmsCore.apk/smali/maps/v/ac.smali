.class abstract enum Lmaps/v/ac;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lmaps/v/ac;

.field public static final enum b:Lmaps/v/ac;

.field public static final enum c:Lmaps/v/ac;

.field private static final synthetic d:[Lmaps/v/ac;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lmaps/v/am;

    const-string v1, "STRONG"

    invoke-direct {v0, v1, v2}, Lmaps/v/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/v/ac;->a:Lmaps/v/ac;

    new-instance v0, Lmaps/v/an;

    const-string v1, "SOFT"

    invoke-direct {v0, v1, v3}, Lmaps/v/an;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/v/ac;->b:Lmaps/v/ac;

    new-instance v0, Lmaps/v/ao;

    const-string v1, "WEAK"

    invoke-direct {v0, v1, v4}, Lmaps/v/ao;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/v/ac;->c:Lmaps/v/ac;

    const/4 v0, 0x3

    new-array v0, v0, [Lmaps/v/ac;

    sget-object v1, Lmaps/v/ac;->a:Lmaps/v/ac;

    aput-object v1, v0, v2

    sget-object v1, Lmaps/v/ac;->b:Lmaps/v/ac;

    aput-object v1, v0, v3

    sget-object v1, Lmaps/v/ac;->c:Lmaps/v/ac;

    aput-object v1, v0, v4

    sput-object v0, Lmaps/v/ac;->d:[Lmaps/v/ac;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILmaps/v/p;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmaps/v/ac;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/v/ac;
    .locals 1

    const-class v0, Lmaps/v/ac;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/v/ac;

    return-object v0
.end method

.method public static values()[Lmaps/v/ac;
    .locals 1

    sget-object v0, Lmaps/v/ac;->d:[Lmaps/v/ac;

    invoke-virtual {v0}, [Lmaps/v/ac;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/v/ac;

    return-object v0
.end method


# virtual methods
.method abstract a()Lmaps/ap/a;
.end method

.method abstract a(Lmaps/v/ak;Lmaps/v/t;Ljava/lang/Object;I)Lmaps/v/av;
.end method
