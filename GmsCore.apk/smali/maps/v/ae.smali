.class abstract Lmaps/v/ae;
.super Ljava/lang/Object;


# instance fields
.field b:I

.field c:I

.field d:Lmaps/v/ak;

.field e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

.field f:Lmaps/v/t;

.field g:Lmaps/v/aa;

.field h:Lmaps/v/aa;

.field final synthetic i:Lmaps/v/r;


# direct methods
.method constructor <init>(Lmaps/v/r;)V
    .locals 1

    iput-object p1, p0, Lmaps/v/ae;->i:Lmaps/v/r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p1, Lmaps/v/r;->e:[Lmaps/v/ak;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lmaps/v/ae;->b:I

    const/4 v0, -0x1

    iput v0, p0, Lmaps/v/ae;->c:I

    invoke-virtual {p0}, Lmaps/v/ae;->a()V

    return-void
.end method


# virtual methods
.method final a()V
    .locals 3

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/v/ae;->g:Lmaps/v/aa;

    invoke-virtual {p0}, Lmaps/v/ae;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lmaps/v/ae;->c()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    iget v0, p0, Lmaps/v/ae;->b:I

    if-ltz v0, :cond_0

    iget-object v0, p0, Lmaps/v/ae;->i:Lmaps/v/r;

    iget-object v0, v0, Lmaps/v/r;->e:[Lmaps/v/ak;

    iget v1, p0, Lmaps/v/ae;->b:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lmaps/v/ae;->b:I

    aget-object v0, v0, v1

    iput-object v0, p0, Lmaps/v/ae;->d:Lmaps/v/ak;

    iget-object v0, p0, Lmaps/v/ae;->d:Lmaps/v/ak;

    iget v0, v0, Lmaps/v/ak;->b:I

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/v/ae;->d:Lmaps/v/ak;

    iget-object v0, v0, Lmaps/v/ak;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iput-object v0, p0, Lmaps/v/ae;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iget-object v0, p0, Lmaps/v/ae;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lmaps/v/ae;->c:I

    invoke-virtual {p0}, Lmaps/v/ae;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0
.end method

.method a(Lmaps/v/t;)Z
    .locals 4

    :try_start_0
    iget-object v0, p0, Lmaps/v/ae;->i:Lmaps/v/r;

    iget-object v0, v0, Lmaps/v/r;->r:Lmaps/ap/c;

    invoke-virtual {v0}, Lmaps/ap/c;->a()J

    move-result-wide v0

    invoke-interface {p1}, Lmaps/v/t;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, Lmaps/v/ae;->i:Lmaps/v/r;

    invoke-virtual {v3, p1, v0, v1}, Lmaps/v/r;->a(Lmaps/v/t;J)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lmaps/v/aa;

    iget-object v3, p0, Lmaps/v/ae;->i:Lmaps/v/r;

    invoke-direct {v1, v3, v2, v0}, Lmaps/v/aa;-><init>(Lmaps/v/r;Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lmaps/v/ae;->g:Lmaps/v/aa;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    iget-object v1, p0, Lmaps/v/ae;->d:Lmaps/v/ak;

    invoke-virtual {v1}, Lmaps/v/ak;->m()V

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/v/ae;->d:Lmaps/v/ak;

    invoke-virtual {v1}, Lmaps/v/ak;->m()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lmaps/v/ae;->d:Lmaps/v/ak;

    invoke-virtual {v1}, Lmaps/v/ak;->m()V

    throw v0
.end method

.method b()Z
    .locals 1

    iget-object v0, p0, Lmaps/v/ae;->f:Lmaps/v/t;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/v/ae;->f:Lmaps/v/t;

    invoke-interface {v0}, Lmaps/v/t;->j()Lmaps/v/t;

    move-result-object v0

    iput-object v0, p0, Lmaps/v/ae;->f:Lmaps/v/t;

    :goto_0
    iget-object v0, p0, Lmaps/v/ae;->f:Lmaps/v/t;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/v/ae;->f:Lmaps/v/t;

    invoke-virtual {p0, v0}, Lmaps/v/ae;->a(Lmaps/v/t;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/v/ae;->f:Lmaps/v/t;

    invoke-interface {v0}, Lmaps/v/t;->j()Lmaps/v/t;

    move-result-object v0

    iput-object v0, p0, Lmaps/v/ae;->f:Lmaps/v/t;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method c()Z
    .locals 3

    :cond_0
    iget v0, p0, Lmaps/v/ae;->c:I

    if-ltz v0, :cond_2

    iget-object v0, p0, Lmaps/v/ae;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iget v1, p0, Lmaps/v/ae;->c:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lmaps/v/ae;->c:I

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/v/t;

    iput-object v0, p0, Lmaps/v/ae;->f:Lmaps/v/t;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/v/ae;->f:Lmaps/v/t;

    invoke-virtual {p0, v0}, Lmaps/v/ae;->a(Lmaps/v/t;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lmaps/v/ae;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method d()Lmaps/v/aa;
    .locals 1

    iget-object v0, p0, Lmaps/v/ae;->g:Lmaps/v/aa;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lmaps/v/ae;->g:Lmaps/v/aa;

    iput-object v0, p0, Lmaps/v/ae;->h:Lmaps/v/aa;

    invoke-virtual {p0}, Lmaps/v/ae;->a()V

    iget-object v0, p0, Lmaps/v/ae;->h:Lmaps/v/aa;

    return-object v0
.end method

.method public hasNext()Z
    .locals 1

    iget-object v0, p0, Lmaps/v/ae;->g:Lmaps/v/aa;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public remove()V
    .locals 2

    iget-object v0, p0, Lmaps/v/ae;->h:Lmaps/v/aa;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lmaps/ap/q;->b(Z)V

    iget-object v0, p0, Lmaps/v/ae;->i:Lmaps/v/r;

    iget-object v1, p0, Lmaps/v/ae;->h:Lmaps/v/aa;

    invoke-virtual {v1}, Lmaps/v/aa;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/v/r;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/v/ae;->h:Lmaps/v/aa;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
