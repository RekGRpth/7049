.class abstract enum Lmaps/v/bb;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lmaps/v/bb;

.field public static final enum b:Lmaps/v/bb;

.field public static final enum c:Lmaps/v/bb;

.field public static final enum d:Lmaps/v/bb;

.field public static final enum e:Lmaps/v/bb;

.field public static final enum f:Lmaps/v/bb;

.field public static final enum g:Lmaps/v/bb;

.field public static final enum h:Lmaps/v/bb;

.field static final i:[Lmaps/v/bb;

.field private static final synthetic j:[Lmaps/v/bb;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lmaps/v/bf;

    const-string v1, "STRONG"

    invoke-direct {v0, v1, v3}, Lmaps/v/bf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/v/bb;->a:Lmaps/v/bb;

    new-instance v0, Lmaps/v/bi;

    const-string v1, "STRONG_ACCESS"

    invoke-direct {v0, v1, v4}, Lmaps/v/bi;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/v/bb;->b:Lmaps/v/bb;

    new-instance v0, Lmaps/v/bh;

    const-string v1, "STRONG_WRITE"

    invoke-direct {v0, v1, v5}, Lmaps/v/bh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/v/bb;->c:Lmaps/v/bb;

    new-instance v0, Lmaps/v/bk;

    const-string v1, "STRONG_ACCESS_WRITE"

    invoke-direct {v0, v1, v6}, Lmaps/v/bk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/v/bb;->d:Lmaps/v/bb;

    new-instance v0, Lmaps/v/bj;

    const-string v1, "WEAK"

    invoke-direct {v0, v1, v7}, Lmaps/v/bj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/v/bb;->e:Lmaps/v/bb;

    new-instance v0, Lmaps/v/bm;

    const-string v1, "WEAK_ACCESS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lmaps/v/bm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/v/bb;->f:Lmaps/v/bb;

    new-instance v0, Lmaps/v/bl;

    const-string v1, "WEAK_WRITE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lmaps/v/bl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/v/bb;->g:Lmaps/v/bb;

    new-instance v0, Lmaps/v/be;

    const-string v1, "WEAK_ACCESS_WRITE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lmaps/v/be;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/v/bb;->h:Lmaps/v/bb;

    const/16 v0, 0x8

    new-array v0, v0, [Lmaps/v/bb;

    sget-object v1, Lmaps/v/bb;->a:Lmaps/v/bb;

    aput-object v1, v0, v3

    sget-object v1, Lmaps/v/bb;->b:Lmaps/v/bb;

    aput-object v1, v0, v4

    sget-object v1, Lmaps/v/bb;->c:Lmaps/v/bb;

    aput-object v1, v0, v5

    sget-object v1, Lmaps/v/bb;->d:Lmaps/v/bb;

    aput-object v1, v0, v6

    sget-object v1, Lmaps/v/bb;->e:Lmaps/v/bb;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lmaps/v/bb;->f:Lmaps/v/bb;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lmaps/v/bb;->g:Lmaps/v/bb;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lmaps/v/bb;->h:Lmaps/v/bb;

    aput-object v2, v0, v1

    sput-object v0, Lmaps/v/bb;->j:[Lmaps/v/bb;

    const/16 v0, 0x8

    new-array v0, v0, [Lmaps/v/bb;

    sget-object v1, Lmaps/v/bb;->a:Lmaps/v/bb;

    aput-object v1, v0, v3

    sget-object v1, Lmaps/v/bb;->b:Lmaps/v/bb;

    aput-object v1, v0, v4

    sget-object v1, Lmaps/v/bb;->c:Lmaps/v/bb;

    aput-object v1, v0, v5

    sget-object v1, Lmaps/v/bb;->d:Lmaps/v/bb;

    aput-object v1, v0, v6

    sget-object v1, Lmaps/v/bb;->e:Lmaps/v/bb;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lmaps/v/bb;->f:Lmaps/v/bb;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lmaps/v/bb;->g:Lmaps/v/bb;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lmaps/v/bb;->h:Lmaps/v/bb;

    aput-object v2, v0, v1

    sput-object v0, Lmaps/v/bb;->i:[Lmaps/v/bb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILmaps/v/p;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmaps/v/bb;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static a(Lmaps/v/ac;ZZ)Lmaps/v/bb;
    .locals 3

    const/4 v0, 0x0

    sget-object v1, Lmaps/v/ac;->c:Lmaps/v/ac;

    if-ne p0, v1, :cond_1

    const/4 v1, 0x4

    move v2, v1

    :goto_0
    if-eqz p1, :cond_2

    const/4 v1, 0x1

    :goto_1
    or-int/2addr v1, v2

    if-eqz p2, :cond_0

    const/4 v0, 0x2

    :cond_0
    or-int/2addr v0, v1

    sget-object v1, Lmaps/v/bb;->i:[Lmaps/v/bb;

    aget-object v0, v1, v0

    return-object v0

    :cond_1
    move v2, v0

    goto :goto_0

    :cond_2
    move v1, v0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/v/bb;
    .locals 1

    const-class v0, Lmaps/v/bb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/v/bb;

    return-object v0
.end method

.method public static values()[Lmaps/v/bb;
    .locals 1

    sget-object v0, Lmaps/v/bb;->j:[Lmaps/v/bb;

    invoke-virtual {v0}, [Lmaps/v/bb;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/v/bb;

    return-object v0
.end method


# virtual methods
.method abstract a(Lmaps/v/ak;Ljava/lang/Object;ILmaps/v/t;)Lmaps/v/t;
.end method

.method a(Lmaps/v/ak;Lmaps/v/t;Lmaps/v/t;)Lmaps/v/t;
    .locals 2

    invoke-interface {p2}, Lmaps/v/t;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p2}, Lmaps/v/t;->i()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1, p3}, Lmaps/v/bb;->a(Lmaps/v/ak;Ljava/lang/Object;ILmaps/v/t;)Lmaps/v/t;

    move-result-object v0

    return-object v0
.end method

.method a(Lmaps/v/t;Lmaps/v/t;)V
    .locals 2

    invoke-interface {p1}, Lmaps/v/t;->b()J

    move-result-wide v0

    invoke-interface {p2, v0, v1}, Lmaps/v/t;->a(J)V

    invoke-interface {p1}, Lmaps/v/t;->d()Lmaps/v/t;

    move-result-object v0

    invoke-static {v0, p2}, Lmaps/v/r;->a(Lmaps/v/t;Lmaps/v/t;)V

    invoke-interface {p1}, Lmaps/v/t;->c()Lmaps/v/t;

    move-result-object v0

    invoke-static {p2, v0}, Lmaps/v/r;->a(Lmaps/v/t;Lmaps/v/t;)V

    invoke-static {p1}, Lmaps/v/r;->b(Lmaps/v/t;)V

    return-void
.end method

.method b(Lmaps/v/t;Lmaps/v/t;)V
    .locals 2

    invoke-interface {p1}, Lmaps/v/t;->e()J

    move-result-wide v0

    invoke-interface {p2, v0, v1}, Lmaps/v/t;->b(J)V

    invoke-interface {p1}, Lmaps/v/t;->g()Lmaps/v/t;

    move-result-object v0

    invoke-static {v0, p2}, Lmaps/v/r;->b(Lmaps/v/t;Lmaps/v/t;)V

    invoke-interface {p1}, Lmaps/v/t;->f()Lmaps/v/t;

    move-result-object v0

    invoke-static {p2, v0}, Lmaps/v/r;->b(Lmaps/v/t;Lmaps/v/t;)V

    invoke-static {p1}, Lmaps/v/r;->c(Lmaps/v/t;)V

    return-void
.end method
