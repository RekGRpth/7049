.class Lmaps/v/br;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;
.implements Lmaps/v/bo;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field final a:Lmaps/v/r;


# direct methods
.method protected constructor <init>(Lmaps/v/l;Lmaps/v/af;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lmaps/v/r;

    invoke-direct {v0, p1, p2}, Lmaps/v/r;-><init>(Lmaps/v/l;Lmaps/v/af;)V

    iput-object v0, p0, Lmaps/v/br;->a:Lmaps/v/r;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lmaps/v/br;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lmaps/v/br;->a:Lmaps/v/r;

    invoke-virtual {v0, p1}, Lmaps/v/r;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    :try_start_0
    invoke-virtual {p0, p1}, Lmaps/v/br;->b(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lmaps/ab/g;

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-direct {v1, v0}, Lmaps/ab/g;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
