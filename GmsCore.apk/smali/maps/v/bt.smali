.class Lmaps/v/bt;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/v/t;


# instance fields
.field final g:Ljava/lang/Object;

.field final h:I

.field final i:Lmaps/v/t;

.field volatile j:Lmaps/v/av;


# direct methods
.method constructor <init>(Ljava/lang/Object;ILmaps/v/t;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lmaps/v/r;->o()Lmaps/v/av;

    move-result-object v0

    iput-object v0, p0, Lmaps/v/bt;->j:Lmaps/v/av;

    iput-object p1, p0, Lmaps/v/bt;->g:Ljava/lang/Object;

    iput p2, p0, Lmaps/v/bt;->h:I

    iput-object p3, p0, Lmaps/v/bt;->i:Lmaps/v/t;

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lmaps/v/bt;->g:Ljava/lang/Object;

    return-object v0
.end method

.method public a(J)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a(Lmaps/v/av;)V
    .locals 0

    iput-object p1, p0, Lmaps/v/bt;->j:Lmaps/v/av;

    return-void
.end method

.method public a(Lmaps/v/t;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b()J
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b(J)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b(Lmaps/v/t;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public c()Lmaps/v/t;
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public c(Lmaps/v/t;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public d()Lmaps/v/t;
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public d(Lmaps/v/t;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public e()J
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public f()Lmaps/v/t;
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public g()Lmaps/v/t;
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public h()Lmaps/v/av;
    .locals 1

    iget-object v0, p0, Lmaps/v/bt;->j:Lmaps/v/av;

    return-object v0
.end method

.method public i()I
    .locals 1

    iget v0, p0, Lmaps/v/bt;->h:I

    return v0
.end method

.method public j()Lmaps/v/t;
    .locals 1

    iget-object v0, p0, Lmaps/v/bt;->i:Lmaps/v/t;

    return-object v0
.end method
