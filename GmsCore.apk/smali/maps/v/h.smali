.class Lmaps/v/h;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/v/av;


# instance fields
.field volatile a:Lmaps/v/av;

.field final b:Lmaps/ab/q;

.field final c:Lmaps/ap/w;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-static {}, Lmaps/v/r;->o()Lmaps/v/av;

    move-result-object v0

    invoke-direct {p0, v0}, Lmaps/v/h;-><init>(Lmaps/v/av;)V

    return-void
.end method

.method public constructor <init>(Lmaps/v/av;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lmaps/ab/q;->b()Lmaps/ab/q;

    move-result-object v0

    iput-object v0, p0, Lmaps/v/h;->b:Lmaps/ab/q;

    new-instance v0, Lmaps/ap/w;

    invoke-direct {v0}, Lmaps/ap/w;-><init>()V

    iput-object v0, p0, Lmaps/v/h;->c:Lmaps/ap/w;

    iput-object p1, p0, Lmaps/v/h;->a:Lmaps/v/av;

    return-void
.end method

.method private static a(Lmaps/ab/q;Ljava/lang/Throwable;)Z
    .locals 1

    :try_start_0
    invoke-virtual {p0, p1}, Lmaps/ab/q;->a(Ljava/lang/Throwable;)Z
    :try_end_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ljava/lang/Throwable;)Lmaps/ab/c;
    .locals 1

    invoke-static {}, Lmaps/ab/q;->b()Lmaps/ab/q;

    move-result-object v0

    invoke-static {v0, p1}, Lmaps/v/h;->a(Lmaps/ab/q;Ljava/lang/Throwable;)Z

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Object;Lmaps/v/af;)Lmaps/ab/c;
    .locals 2

    iget-object v0, p0, Lmaps/v/h;->c:Lmaps/ap/w;

    invoke-virtual {v0}, Lmaps/ap/w;->a()Lmaps/ap/w;

    iget-object v0, p0, Lmaps/v/h;->a:Lmaps/v/av;

    invoke-interface {v0}, Lmaps/v/av;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    :try_start_0
    invoke-virtual {p2, p1}, Lmaps/v/af;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmaps/v/h;->a(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lmaps/v/h;->b:Lmaps/ab/q;

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-static {v0}, Lmaps/ab/k;->a(Ljava/lang/Object;)Lmaps/ab/c;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-virtual {p2, p1, v0}, Lmaps/v/af;->a(Ljava/lang/Object;Ljava/lang/Object;)Lmaps/ab/c;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-static {v0}, Lmaps/ab/k;->a(Ljava/lang/Object;)Lmaps/ab/c;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {p0, v0}, Lmaps/v/h;->a(Ljava/lang/Throwable;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v0, p0, Lmaps/v/h;->b:Lmaps/ab/q;

    goto :goto_0

    :cond_3
    invoke-direct {p0, v0}, Lmaps/v/h;->b(Ljava/lang/Throwable;)Lmaps/ab/c;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljava/lang/ref/ReferenceQueue;Lmaps/v/t;)Lmaps/v/av;
    .locals 0

    return-object p0
.end method

.method public a()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public a(Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Lmaps/v/h;->b:Lmaps/ab/q;

    invoke-virtual {v0, p1}, Lmaps/ab/q;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/Throwable;)Z
    .locals 1

    iget-object v0, p0, Lmaps/v/h;->b:Lmaps/ab/q;

    invoke-static {v0, p1}, Lmaps/v/h;->a(Lmaps/ab/q;Ljava/lang/Throwable;)Z

    move-result v0

    return v0
.end method

.method public b(Ljava/lang/Object;)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Lmaps/v/h;->a(Ljava/lang/Object;)Z

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lmaps/v/r;->o()Lmaps/v/av;

    move-result-object v0

    iput-object v0, p0, Lmaps/v/h;->a:Lmaps/v/av;

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    iget-object v0, p0, Lmaps/v/h;->a:Lmaps/v/av;

    invoke-interface {v0}, Lmaps/v/av;->b()Z

    move-result v0

    return v0
.end method

.method public c()I
    .locals 1

    iget-object v0, p0, Lmaps/v/h;->a:Lmaps/v/av;

    invoke-interface {v0}, Lmaps/v/av;->c()I

    move-result v0

    return v0
.end method

.method public d()J
    .locals 2

    iget-object v0, p0, Lmaps/v/h;->c:Lmaps/ap/w;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1}, Lmaps/ap/w;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    return-wide v0
.end method

.method public e()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lmaps/v/h;->b:Lmaps/ab/q;

    invoke-static {v0}, Lmaps/ab/d;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public f()Lmaps/v/av;
    .locals 1

    iget-object v0, p0, Lmaps/v/h;->a:Lmaps/v/av;

    return-object v0
.end method

.method public g()Lmaps/v/t;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public get()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lmaps/v/h;->a:Lmaps/v/av;

    invoke-interface {v0}, Lmaps/v/av;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
