.class public Lmaps/w/ab;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/animation/Interpolator;


# instance fields
.field private a:Lmaps/w/e;


# direct methods
.method public constructor <init>(Lmaps/w/e;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/w/ab;->a:Lmaps/w/e;

    return-void
.end method


# virtual methods
.method protected final a(F)F
    .locals 2

    const/high16 v1, 0x3f800000

    sub-float v0, v1, p1

    invoke-virtual {p0, v0}, Lmaps/w/ab;->b(F)F

    move-result v0

    sub-float v0, v1, v0

    return v0
.end method

.method protected final b(F)F
    .locals 2

    const/high16 v1, 0x40f20000

    const v0, 0x3eba2e8c

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    mul-float v0, p1, p1

    mul-float/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    const v0, 0x3f3a2e8c

    cmpg-float v0, p1, v0

    if-gez v0, :cond_1

    const v0, 0x3f0ba2e9

    sub-float v0, p1, v0

    mul-float/2addr v0, v0

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f400000

    add-float/2addr v0, v1

    goto :goto_0

    :cond_1
    const v0, 0x3f68ba2f

    cmpg-float v0, p1, v0

    if-gez v0, :cond_2

    const v0, 0x3f51745d

    sub-float v0, p1, v0

    mul-float/2addr v0, v0

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f700000

    add-float/2addr v0, v1

    goto :goto_0

    :cond_2
    const v0, 0x3f745d17

    sub-float v0, p1, v0

    mul-float/2addr v0, v0

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f7c0000

    add-float/2addr v0, v1

    goto :goto_0
.end method

.method public getInterpolation(F)F
    .locals 4

    const/high16 v3, 0x3f800000

    const/high16 v2, 0x3f000000

    sget-object v0, Lmaps/w/aa;->a:[I

    iget-object v1, p0, Lmaps/w/ab;->a:Lmaps/w/e;

    invoke-virtual {v1}, Lmaps/w/e;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    invoke-virtual {p0, p1}, Lmaps/w/ab;->b(F)F

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    invoke-virtual {p0, p1}, Lmaps/w/ab;->a(F)F

    move-result v0

    goto :goto_0

    :pswitch_1
    const/high16 v0, 0x40000000

    mul-float/2addr v0, p1

    cmpg-float v1, v0, v3

    if-gez v1, :cond_0

    invoke-virtual {p0, v0}, Lmaps/w/ab;->a(F)F

    move-result v0

    mul-float/2addr v0, v2

    goto :goto_0

    :cond_0
    sub-float/2addr v0, v3

    invoke-virtual {p0, v0}, Lmaps/w/ab;->b(F)F

    move-result v0

    mul-float/2addr v0, v2

    add-float/2addr v0, v2

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
