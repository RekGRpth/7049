.class public abstract Lmaps/w/f;
.super Lmaps/w/ad;


# instance fields
.field protected a:Ljava/lang/Object;

.field protected b:Ljava/lang/Object;

.field protected c:Ljava/lang/Object;

.field protected d:Z


# direct methods
.method public constructor <init>(Landroid/view/animation/Interpolator;)V
    .locals 1

    invoke-direct {p0}, Lmaps/w/ad;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/w/f;->d:Z

    invoke-virtual {p0, p1}, Lmaps/w/f;->setInterpolator(Landroid/view/animation/Interpolator;)V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lmaps/w/f;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public a(J)V
    .locals 0

    invoke-virtual {p0, p1, p2}, Lmaps/w/f;->b(J)V

    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 1

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lmaps/w/f;->d:Z

    if-nez v0, :cond_1

    invoke-virtual {p0, p1, p1}, Lmaps/w/f;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lmaps/w/f;->c:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lmaps/w/f;->b(Ljava/lang/Object;)V

    invoke-virtual {p0, p1}, Lmaps/w/f;->c(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, p1}, Lmaps/w/f;->b(Ljava/lang/Object;)V

    invoke-virtual {p0, p2}, Lmaps/w/f;->c(Ljava/lang/Object;)V

    invoke-virtual {p0, p1}, Lmaps/w/f;->d(Ljava/lang/Object;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/w/f;->d:Z

    goto :goto_0
.end method

.method public b()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lmaps/w/f;->b:Ljava/lang/Object;

    return-object v0
.end method

.method protected abstract b(J)V
.end method

.method protected abstract b(Ljava/lang/Object;)V
.end method

.method public c()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lmaps/w/f;->c:Ljava/lang/Object;

    return-object v0
.end method

.method protected abstract c(Ljava/lang/Object;)V
.end method

.method protected abstract d(Ljava/lang/Object;)V
.end method

.method public isInitialized()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/w/f;->d:Z

    return v0
.end method
