.class final enum Lmaps/w/g;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lmaps/w/g;

.field public static final enum b:Lmaps/w/g;

.field public static final enum c:Lmaps/w/g;

.field public static final enum d:Lmaps/w/g;

.field private static final synthetic e:[Lmaps/w/g;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lmaps/w/g;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v2}, Lmaps/w/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/w/g;->a:Lmaps/w/g;

    new-instance v0, Lmaps/w/g;

    const-string v1, "SHORT_MOVE"

    invoke-direct {v0, v1, v3}, Lmaps/w/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/w/g;->b:Lmaps/w/g;

    new-instance v0, Lmaps/w/g;

    const-string v1, "LONG_MOVE"

    invoke-direct {v0, v1, v4}, Lmaps/w/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/w/g;->c:Lmaps/w/g;

    new-instance v0, Lmaps/w/g;

    const-string v1, "TELEPORT"

    invoke-direct {v0, v1, v5}, Lmaps/w/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/w/g;->d:Lmaps/w/g;

    const/4 v0, 0x4

    new-array v0, v0, [Lmaps/w/g;

    sget-object v1, Lmaps/w/g;->a:Lmaps/w/g;

    aput-object v1, v0, v2

    sget-object v1, Lmaps/w/g;->b:Lmaps/w/g;

    aput-object v1, v0, v3

    sget-object v1, Lmaps/w/g;->c:Lmaps/w/g;

    aput-object v1, v0, v4

    sget-object v1, Lmaps/w/g;->d:Lmaps/w/g;

    aput-object v1, v0, v5

    sput-object v0, Lmaps/w/g;->e:[Lmaps/w/g;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/w/g;
    .locals 1

    const-class v0, Lmaps/w/g;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/w/g;

    return-object v0
.end method

.method public static values()[Lmaps/w/g;
    .locals 1

    sget-object v0, Lmaps/w/g;->e:[Lmaps/w/g;

    invoke-virtual {v0}, [Lmaps/w/g;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/w/g;

    return-object v0
.end method
