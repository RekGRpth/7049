.class public Lmaps/w/h;
.super Lmaps/w/ad;


# instance fields
.field private a:F

.field private b:F

.field private c:F

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/view/animation/Interpolator;)V
    .locals 1

    invoke-direct {p0}, Lmaps/w/ad;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/w/h;->d:Z

    invoke-virtual {p0, p1}, Lmaps/w/h;->setInterpolator(Landroid/view/animation/Interpolator;)V

    return-void
.end method


# virtual methods
.method public a()F
    .locals 1

    iget v0, p0, Lmaps/w/h;->b:F

    return v0
.end method

.method public a(F)V
    .locals 1

    iget-boolean v0, p0, Lmaps/w/h;->d:Z

    if-nez v0, :cond_0

    invoke-virtual {p0, p1, p1}, Lmaps/w/h;->a(FF)V

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lmaps/w/h;->c:F

    iput v0, p0, Lmaps/w/h;->a:F

    iput p1, p0, Lmaps/w/h;->b:F

    goto :goto_0
.end method

.method public a(FF)V
    .locals 1

    iput p1, p0, Lmaps/w/h;->a:F

    iput p2, p0, Lmaps/w/h;->b:F

    iput p1, p0, Lmaps/w/h;->c:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/w/h;->d:Z

    return-void
.end method

.method public a(J)V
    .locals 4

    invoke-virtual {p0, p1, p2}, Lmaps/w/h;->c(J)F

    move-result v0

    iget v1, p0, Lmaps/w/h;->a:F

    iget v2, p0, Lmaps/w/h;->b:F

    iget v3, p0, Lmaps/w/h;->a:F

    sub-float/2addr v2, v3

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    iput v0, p0, Lmaps/w/h;->c:F

    return-void
.end method

.method public b()F
    .locals 1

    iget v0, p0, Lmaps/w/h;->c:F

    return v0
.end method

.method public isInitialized()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/w/h;->d:Z

    return v0
.end method
