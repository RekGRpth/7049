.class public Lmaps/w/i;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/w/o;


# instance fields
.field private final a:Lmaps/w/l;

.field private final b:Lmaps/w/h;

.field private final c:Lmaps/w/a;

.field private final d:Lmaps/w/a;


# direct methods
.method public constructor <init>()V
    .locals 5

    const v2, 0x3f7d70a4

    const-wide/16 v3, 0x1388

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lmaps/w/l;

    new-instance v1, Lmaps/w/ac;

    invoke-direct {v1, v2}, Lmaps/w/ac;-><init>(F)V

    invoke-direct {v0, v1}, Lmaps/w/l;-><init>(Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Lmaps/w/i;->a:Lmaps/w/l;

    new-instance v0, Lmaps/w/a;

    new-instance v1, Lmaps/w/ac;

    invoke-direct {v1, v2}, Lmaps/w/ac;-><init>(F)V

    invoke-direct {v0, v1}, Lmaps/w/a;-><init>(Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Lmaps/w/i;->c:Lmaps/w/a;

    new-instance v0, Lmaps/w/h;

    new-instance v1, Lmaps/w/af;

    const/high16 v2, 0x3f800000

    invoke-direct {v1, v2}, Lmaps/w/af;-><init>(F)V

    invoke-direct {v0, v1}, Lmaps/w/h;-><init>(Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Lmaps/w/i;->b:Lmaps/w/h;

    iget-object v0, p0, Lmaps/w/i;->a:Lmaps/w/l;

    invoke-virtual {v0, v3, v4}, Lmaps/w/l;->setDuration(J)V

    iget-object v0, p0, Lmaps/w/i;->b:Lmaps/w/h;

    invoke-virtual {v0, v3, v4}, Lmaps/w/h;->setDuration(J)V

    iget-object v0, p0, Lmaps/w/i;->c:Lmaps/w/a;

    invoke-virtual {v0, v3, v4}, Lmaps/w/a;->setDuration(J)V

    new-instance v0, Lmaps/w/a;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-direct {v0, v1}, Lmaps/w/a;-><init>(Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Lmaps/w/i;->d:Lmaps/w/a;

    iget-object v0, p0, Lmaps/w/i;->d:Lmaps/w/a;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmaps/w/a;->a(I)V

    iget-object v0, p0, Lmaps/w/i;->d:Lmaps/w/a;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lmaps/w/a;->a(I)V

    iget-object v0, p0, Lmaps/w/i;->d:Lmaps/w/a;

    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, v1, v2}, Lmaps/w/a;->setDuration(J)V

    iget-object v0, p0, Lmaps/w/i;->d:Lmaps/w/a;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lmaps/w/a;->setRepeatCount(I)V

    iget-object v0, p0, Lmaps/w/i;->d:Lmaps/w/a;

    invoke-virtual {v0}, Lmaps/w/a;->start()V

    return-void
.end method


# virtual methods
.method public declared-synchronized a(J)I
    .locals 5

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lmaps/w/i;->a:Lmaps/w/l;

    invoke-virtual {v1}, Lmaps/w/l;->isInitialized()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    :try_start_1
    iget-object v1, p0, Lmaps/w/i;->d:Lmaps/w/a;

    invoke-virtual {v1, p1, p2}, Lmaps/w/a;->a(J)V

    iget-object v1, p0, Lmaps/w/i;->a:Lmaps/w/l;

    invoke-virtual {v1}, Lmaps/w/l;->hasEnded()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lmaps/w/i;->b:Lmaps/w/h;

    invoke-virtual {v1}, Lmaps/w/h;->hasEnded()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lmaps/w/i;->c:Lmaps/w/a;

    invoke-virtual {v1}, Lmaps/w/a;->hasEnded()Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    iget-object v0, p0, Lmaps/w/i;->a:Lmaps/w/l;

    invoke-virtual {v0}, Lmaps/w/l;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/bx;

    iget-object v1, p0, Lmaps/w/i;->a:Lmaps/w/l;

    invoke-virtual {v1}, Lmaps/w/l;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/t/bx;

    invoke-virtual {v0, v1}, Lmaps/t/bx;->c(Lmaps/t/bx;)F

    move-result v0

    float-to-double v1, v0

    iget-object v0, p0, Lmaps/w/i;->a:Lmaps/w/l;

    invoke-virtual {v0}, Lmaps/w/l;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/bx;

    invoke-virtual {v0}, Lmaps/t/bx;->e()D

    move-result-wide v3

    div-double v0, v1, v3

    const-wide/high16 v2, 0x4059000000000000L

    cmpl-double v0, v0, v2

    if-lez v0, :cond_3

    iget-object v0, p0, Lmaps/w/i;->a:Lmaps/w/l;

    iget-object v1, p0, Lmaps/w/i;->a:Lmaps/w/l;

    invoke-virtual {v1}, Lmaps/w/l;->b()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lmaps/w/i;->a:Lmaps/w/l;

    invoke-virtual {v2}, Lmaps/w/l;->b()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/w/l;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_3
    iget-object v0, p0, Lmaps/w/i;->a:Lmaps/w/l;

    invoke-virtual {v0, p1, p2}, Lmaps/w/l;->a(J)V

    iget-object v0, p0, Lmaps/w/i;->b:Lmaps/w/h;

    invoke-virtual {v0, p1, p2}, Lmaps/w/h;->a(J)V

    iget-object v0, p0, Lmaps/w/i;->c:Lmaps/w/a;

    invoke-virtual {v0, p1, p2}, Lmaps/w/a;->a(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lmaps/bq/d;)V
    .locals 0

    return-void
.end method

.method public declared-synchronized a(Lmaps/t/aj;)Z
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/w/i;->a:Lmaps/w/l;

    invoke-virtual {v0}, Lmaps/w/l;->isInitialized()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    invoke-static {p1}, Lmaps/ap/q;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lmaps/w/i;->a:Lmaps/w/l;

    invoke-virtual {v0}, Lmaps/w/l;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/bx;

    iget-object v1, p0, Lmaps/w/i;->b:Lmaps/w/h;

    invoke-virtual {v1}, Lmaps/w/h;->b()F

    move-result v1

    iget-object v2, p0, Lmaps/w/i;->c:Lmaps/w/a;

    invoke-virtual {v2}, Lmaps/w/a;->b()I

    move-result v2

    invoke-virtual {p1, v0, v1, v2}, Lmaps/t/aj;->a(Lmaps/t/bx;FI)V

    iget-object v0, p0, Lmaps/w/i;->d:Lmaps/w/a;

    invoke-virtual {v0}, Lmaps/w/a;->b()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Lmaps/t/aj;->c(F)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(Lmaps/t/aj;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/w/i;->a:Lmaps/w/l;

    invoke-virtual {v0}, Lmaps/w/l;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lmaps/t/aj;->a()Lmaps/t/bx;

    move-result-object v0

    iget-object v1, p0, Lmaps/w/i;->a:Lmaps/w/l;

    invoke-virtual {v1}, Lmaps/w/l;->b()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/ap/e;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lmaps/w/i;->a:Lmaps/w/l;

    invoke-virtual {p1}, Lmaps/t/aj;->a()Lmaps/t/bx;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/w/l;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lmaps/w/i;->a:Lmaps/w/l;

    invoke-virtual {v0}, Lmaps/w/l;->start()V

    :cond_1
    iget-object v0, p0, Lmaps/w/i;->b:Lmaps/w/h;

    invoke-virtual {v0}, Lmaps/w/h;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lmaps/t/aj;->b()F

    move-result v0

    iget-object v1, p0, Lmaps/w/i;->b:Lmaps/w/h;

    invoke-virtual {v1}, Lmaps/w/h;->a()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lmaps/w/i;->b:Lmaps/w/h;

    invoke-virtual {p1}, Lmaps/t/aj;->b()F

    move-result v1

    invoke-virtual {v0, v1}, Lmaps/w/h;->a(F)V

    iget-object v0, p0, Lmaps/w/i;->b:Lmaps/w/h;

    invoke-virtual {v0}, Lmaps/w/h;->start()V

    :cond_3
    iget-object v0, p0, Lmaps/w/i;->c:Lmaps/w/a;

    invoke-virtual {v0}, Lmaps/w/a;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lmaps/t/aj;->c()I

    move-result v0

    iget-object v1, p0, Lmaps/w/i;->c:Lmaps/w/a;

    invoke-virtual {v1}, Lmaps/w/a;->a()I

    move-result v1

    if-eq v0, v1, :cond_5

    :cond_4
    iget-object v0, p0, Lmaps/w/i;->c:Lmaps/w/a;

    invoke-virtual {p1}, Lmaps/t/aj;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Lmaps/w/a;->a(I)V

    iget-object v0, p0, Lmaps/w/i;->c:Lmaps/w/a;

    invoke-virtual {v0}, Lmaps/w/a;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_5
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
