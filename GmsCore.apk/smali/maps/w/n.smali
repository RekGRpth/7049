.class public Lmaps/w/n;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/w/o;


# static fields
.field private static final A:Landroid/view/animation/Interpolator;

.field private static final B:Landroid/view/animation/Interpolator;

.field private static final C:Landroid/view/animation/Interpolator;

.field private static final D:Landroid/view/animation/Interpolator;

.field private static final E:Landroid/view/animation/Interpolator;

.field private static final F:Landroid/view/animation/Interpolator;

.field private static final G:Landroid/view/animation/Interpolator;

.field private static final H:Landroid/view/animation/Interpolator;

.field private static final I:Landroid/view/animation/Interpolator;

.field private static final J:Landroid/view/animation/Interpolator;

.field private static final K:Landroid/view/animation/Interpolator;

.field private static final v:Landroid/view/animation/Interpolator;

.field private static final w:Landroid/view/animation/Interpolator;

.field private static final x:Landroid/view/animation/Interpolator;

.field private static final y:Landroid/view/animation/Interpolator;

.field private static final z:Landroid/view/animation/Interpolator;


# instance fields
.field private final L:Landroid/view/animation/Interpolator;

.field public a:F

.field public b:F

.field private final c:Lmaps/w/w;

.field private final d:Lmaps/w/w;

.field private final e:Lmaps/w/w;

.field private f:Lmaps/w/w;

.field private final g:Lmaps/w/h;

.field private h:J

.field private i:Lmaps/t/aj;

.field private j:Lmaps/t/aj;

.field private k:Lmaps/t/aj;

.field private l:F

.field private m:D

.field private n:F

.field private o:F

.field private p:F

.field private q:F

.field private r:Z

.field private s:Z

.field private t:Lmaps/w/g;

.field private u:Lmaps/w/g;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/high16 v3, 0x40400000

    const/high16 v2, 0x40000000

    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    sput-object v0, Lmaps/w/n;->v:Landroid/view/animation/Interpolator;

    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    sput-object v0, Lmaps/w/n;->w:Landroid/view/animation/Interpolator;

    new-instance v0, Lmaps/w/af;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lmaps/w/af;-><init>(F)V

    sput-object v0, Lmaps/w/n;->x:Landroid/view/animation/Interpolator;

    new-instance v0, Lmaps/w/af;

    const/high16 v1, 0x3f800000

    invoke-direct {v0, v1}, Lmaps/w/af;-><init>(F)V

    sput-object v0, Lmaps/w/n;->y:Landroid/view/animation/Interpolator;

    new-instance v0, Lmaps/w/m;

    invoke-direct {v0}, Lmaps/w/m;-><init>()V

    sput-object v0, Lmaps/w/n;->z:Landroid/view/animation/Interpolator;

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    sput-object v0, Lmaps/w/n;->A:Landroid/view/animation/Interpolator;

    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    sput-object v0, Lmaps/w/n;->B:Landroid/view/animation/Interpolator;

    new-instance v0, Lmaps/w/ab;

    sget-object v1, Lmaps/w/e;->a:Lmaps/w/e;

    invoke-direct {v0, v1}, Lmaps/w/ab;-><init>(Lmaps/w/e;)V

    sput-object v0, Lmaps/w/n;->C:Landroid/view/animation/Interpolator;

    new-instance v0, Lmaps/w/s;

    invoke-direct {v0}, Lmaps/w/s;-><init>()V

    sget-object v1, Lmaps/w/n;->A:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Lmaps/w/s;->a(Landroid/view/animation/Interpolator;)Lmaps/w/s;

    move-result-object v0

    sget-object v1, Lmaps/w/n;->A:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Lmaps/w/s;->a(Landroid/view/animation/Interpolator;)Lmaps/w/s;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/w/s;->a()Lmaps/w/s;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/w/s;->b()Lmaps/w/s;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/w/s;->c()Lmaps/w/z;

    move-result-object v0

    sput-object v0, Lmaps/w/n;->D:Landroid/view/animation/Interpolator;

    new-instance v0, Lmaps/w/s;

    invoke-direct {v0}, Lmaps/w/s;-><init>()V

    sget-object v1, Lmaps/w/n;->x:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Lmaps/w/s;->a(Landroid/view/animation/Interpolator;)Lmaps/w/s;

    move-result-object v0

    invoke-virtual {v0, v3}, Lmaps/w/s;->a(F)Lmaps/w/s;

    move-result-object v0

    sget-object v1, Lmaps/w/n;->B:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Lmaps/w/s;->a(Landroid/view/animation/Interpolator;)Lmaps/w/s;

    move-result-object v0

    invoke-virtual {v0, v2}, Lmaps/w/s;->a(F)Lmaps/w/s;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/w/s;->b()Lmaps/w/s;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/w/s;->c()Lmaps/w/z;

    move-result-object v0

    sput-object v0, Lmaps/w/n;->E:Landroid/view/animation/Interpolator;

    new-instance v0, Lmaps/w/s;

    invoke-direct {v0}, Lmaps/w/s;-><init>()V

    sget-object v1, Lmaps/w/n;->v:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Lmaps/w/s;->a(Landroid/view/animation/Interpolator;)Lmaps/w/s;

    move-result-object v0

    sget-object v1, Lmaps/w/n;->y:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Lmaps/w/s;->a(Landroid/view/animation/Interpolator;)Lmaps/w/s;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lmaps/w/s;->a(I)Lmaps/w/s;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/w/s;->b()Lmaps/w/s;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/w/s;->c()Lmaps/w/z;

    move-result-object v0

    sput-object v0, Lmaps/w/n;->F:Landroid/view/animation/Interpolator;

    new-instance v0, Lmaps/w/s;

    invoke-direct {v0}, Lmaps/w/s;-><init>()V

    sget-object v1, Lmaps/w/n;->x:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Lmaps/w/s;->a(Landroid/view/animation/Interpolator;)Lmaps/w/s;

    move-result-object v0

    invoke-virtual {v0, v3}, Lmaps/w/s;->a(F)Lmaps/w/s;

    move-result-object v0

    sget-object v1, Lmaps/w/n;->D:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Lmaps/w/s;->a(Landroid/view/animation/Interpolator;)Lmaps/w/s;

    move-result-object v0

    invoke-virtual {v0, v2}, Lmaps/w/s;->a(F)Lmaps/w/s;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/w/s;->b()Lmaps/w/s;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/w/s;->c()Lmaps/w/z;

    move-result-object v0

    sput-object v0, Lmaps/w/n;->G:Landroid/view/animation/Interpolator;

    new-instance v0, Lmaps/w/s;

    invoke-direct {v0}, Lmaps/w/s;-><init>()V

    sget-object v1, Lmaps/w/n;->x:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Lmaps/w/s;->a(Landroid/view/animation/Interpolator;)Lmaps/w/s;

    move-result-object v0

    sget-object v1, Lmaps/w/n;->y:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Lmaps/w/s;->a(Landroid/view/animation/Interpolator;)Lmaps/w/s;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/w/s;->b()Lmaps/w/s;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/w/s;->c()Lmaps/w/z;

    move-result-object v0

    sput-object v0, Lmaps/w/n;->H:Landroid/view/animation/Interpolator;

    new-instance v0, Lmaps/w/s;

    invoke-direct {v0}, Lmaps/w/s;-><init>()V

    sget-object v1, Lmaps/w/n;->x:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Lmaps/w/s;->a(Landroid/view/animation/Interpolator;)Lmaps/w/s;

    move-result-object v0

    sget-object v1, Lmaps/w/n;->y:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Lmaps/w/s;->a(Landroid/view/animation/Interpolator;)Lmaps/w/s;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/w/s;->b()Lmaps/w/s;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/w/s;->c()Lmaps/w/z;

    move-result-object v0

    sput-object v0, Lmaps/w/n;->I:Landroid/view/animation/Interpolator;

    new-instance v0, Lmaps/w/s;

    invoke-direct {v0}, Lmaps/w/s;-><init>()V

    sget-object v1, Lmaps/w/n;->B:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Lmaps/w/s;->a(Landroid/view/animation/Interpolator;)Lmaps/w/s;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/w/s;->a()Lmaps/w/s;

    move-result-object v0

    sget-object v1, Lmaps/w/n;->x:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Lmaps/w/s;->a(Landroid/view/animation/Interpolator;)Lmaps/w/s;

    move-result-object v0

    invoke-virtual {v0, v2}, Lmaps/w/s;->a(F)Lmaps/w/s;

    move-result-object v0

    sget-object v1, Lmaps/w/n;->B:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Lmaps/w/s;->a(Landroid/view/animation/Interpolator;)Lmaps/w/s;

    move-result-object v0

    sget-object v1, Lmaps/w/n;->y:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Lmaps/w/s;->a(Landroid/view/animation/Interpolator;)Lmaps/w/s;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/w/s;->b()Lmaps/w/s;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/w/s;->c()Lmaps/w/z;

    move-result-object v0

    sput-object v0, Lmaps/w/n;->J:Landroid/view/animation/Interpolator;

    new-instance v0, Lmaps/w/s;

    invoke-direct {v0}, Lmaps/w/s;-><init>()V

    sget-object v1, Lmaps/w/n;->x:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Lmaps/w/s;->a(Landroid/view/animation/Interpolator;)Lmaps/w/s;

    move-result-object v0

    sget-object v1, Lmaps/w/n;->w:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Lmaps/w/s;->a(Landroid/view/animation/Interpolator;)Lmaps/w/s;

    move-result-object v0

    sget-object v1, Lmaps/w/n;->y:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Lmaps/w/s;->a(Landroid/view/animation/Interpolator;)Lmaps/w/s;

    move-result-object v0

    sget-object v1, Lmaps/w/n;->C:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Lmaps/w/s;->a(Landroid/view/animation/Interpolator;)Lmaps/w/s;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/w/s;->a()Lmaps/w/s;

    move-result-object v0

    invoke-virtual {v0, v2}, Lmaps/w/s;->a(F)Lmaps/w/s;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/w/s;->b()Lmaps/w/s;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/w/s;->c()Lmaps/w/z;

    move-result-object v0

    sput-object v0, Lmaps/w/n;->K:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>()V
    .locals 9

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v2, 0x0

    const/4 v6, 0x0

    const/high16 v5, 0x3f800000

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lmaps/w/w;

    invoke-direct {v0}, Lmaps/w/w;-><init>()V

    iput-object v0, p0, Lmaps/w/n;->c:Lmaps/w/w;

    new-instance v0, Lmaps/w/w;

    invoke-direct {v0}, Lmaps/w/w;-><init>()V

    iput-object v0, p0, Lmaps/w/n;->d:Lmaps/w/w;

    new-instance v0, Lmaps/w/w;

    invoke-direct {v0}, Lmaps/w/w;-><init>()V

    iput-object v0, p0, Lmaps/w/n;->e:Lmaps/w/w;

    new-instance v0, Lmaps/t/aj;

    invoke-direct {v0}, Lmaps/t/aj;-><init>()V

    iput-object v0, p0, Lmaps/w/n;->i:Lmaps/t/aj;

    new-instance v0, Lmaps/t/aj;

    invoke-direct {v0}, Lmaps/t/aj;-><init>()V

    iput-object v0, p0, Lmaps/w/n;->j:Lmaps/t/aj;

    new-instance v0, Lmaps/t/aj;

    invoke-direct {v0}, Lmaps/t/aj;-><init>()V

    iput-object v0, p0, Lmaps/w/n;->k:Lmaps/t/aj;

    iput v5, p0, Lmaps/w/n;->l:F

    const-wide/high16 v0, 0x3ff0000000000000L

    iput-wide v0, p0, Lmaps/w/n;->m:D

    iput v5, p0, Lmaps/w/n;->n:F

    iput v2, p0, Lmaps/w/n;->o:F

    iput v2, p0, Lmaps/w/n;->p:F

    iput v2, p0, Lmaps/w/n;->q:F

    iput-boolean v6, p0, Lmaps/w/n;->r:Z

    iput-boolean v6, p0, Lmaps/w/n;->s:Z

    sget-object v0, Lmaps/w/g;->a:Lmaps/w/g;

    iput-object v0, p0, Lmaps/w/n;->t:Lmaps/w/g;

    sget-object v0, Lmaps/w/g;->a:Lmaps/w/g;

    iput-object v0, p0, Lmaps/w/n;->u:Lmaps/w/g;

    iput v5, p0, Lmaps/w/n;->a:F

    iput v5, p0, Lmaps/w/n;->b:F

    new-instance v0, Lmaps/w/c;

    invoke-direct {v0, p0}, Lmaps/w/c;-><init>(Lmaps/w/n;)V

    iput-object v0, p0, Lmaps/w/n;->L:Landroid/view/animation/Interpolator;

    iget-object v0, p0, Lmaps/w/n;->i:Lmaps/t/aj;

    new-instance v1, Lmaps/t/bx;

    invoke-direct {v1}, Lmaps/t/bx;-><init>()V

    invoke-virtual {v0, v1}, Lmaps/t/aj;->a(Lmaps/t/bx;)V

    iget-object v0, p0, Lmaps/w/n;->j:Lmaps/t/aj;

    new-instance v1, Lmaps/t/bx;

    invoke-direct {v1}, Lmaps/t/bx;-><init>()V

    invoke-virtual {v0, v1}, Lmaps/t/aj;->a(Lmaps/t/bx;)V

    iget-object v0, p0, Lmaps/w/n;->k:Lmaps/t/aj;

    new-instance v1, Lmaps/t/bx;

    invoke-direct {v1}, Lmaps/t/bx;-><init>()V

    invoke-virtual {v0, v1}, Lmaps/t/aj;->a(Lmaps/t/bx;)V

    new-instance v0, Lmaps/w/l;

    sget-object v1, Lmaps/w/n;->v:Landroid/view/animation/Interpolator;

    invoke-direct {v0, v1}, Lmaps/w/l;-><init>(Landroid/view/animation/Interpolator;)V

    iget-object v1, p0, Lmaps/w/n;->c:Lmaps/w/w;

    new-instance v2, Lmaps/w/a;

    sget-object v3, Lmaps/w/n;->v:Landroid/view/animation/Interpolator;

    invoke-direct {v2, v3}, Lmaps/w/a;-><init>(Landroid/view/animation/Interpolator;)V

    invoke-virtual {v1, v7, v2}, Lmaps/w/w;->a(ILmaps/w/ad;)V

    iget-object v1, p0, Lmaps/w/n;->c:Lmaps/w/w;

    invoke-virtual {v1, v8, v0}, Lmaps/w/w;->a(ILmaps/w/ad;)V

    iget-object v1, p0, Lmaps/w/n;->c:Lmaps/w/w;

    const/4 v2, 0x3

    new-instance v3, Lmaps/w/h;

    sget-object v4, Lmaps/w/n;->x:Landroid/view/animation/Interpolator;

    invoke-direct {v3, v4}, Lmaps/w/h;-><init>(Landroid/view/animation/Interpolator;)V

    invoke-virtual {v1, v2, v3}, Lmaps/w/w;->a(ILmaps/w/ad;)V

    iget-object v1, p0, Lmaps/w/n;->c:Lmaps/w/w;

    invoke-virtual {v1, v6, v0}, Lmaps/w/w;->a(ILmaps/w/ad;)V

    iget-object v0, p0, Lmaps/w/n;->d:Lmaps/w/w;

    new-instance v1, Lmaps/w/a;

    sget-object v2, Lmaps/w/n;->v:Landroid/view/animation/Interpolator;

    invoke-direct {v1, v2}, Lmaps/w/a;-><init>(Landroid/view/animation/Interpolator;)V

    invoke-virtual {v0, v7, v1}, Lmaps/w/w;->a(ILmaps/w/ad;)V

    iget-object v0, p0, Lmaps/w/n;->d:Lmaps/w/w;

    new-instance v1, Lmaps/w/l;

    sget-object v2, Lmaps/w/n;->F:Landroid/view/animation/Interpolator;

    invoke-direct {v1, v2}, Lmaps/w/l;-><init>(Landroid/view/animation/Interpolator;)V

    invoke-virtual {v0, v8, v1}, Lmaps/w/w;->a(ILmaps/w/ad;)V

    iget-object v0, p0, Lmaps/w/n;->d:Lmaps/w/w;

    const/4 v1, 0x3

    new-instance v2, Lmaps/w/h;

    sget-object v3, Lmaps/w/n;->G:Landroid/view/animation/Interpolator;

    invoke-direct {v2, v3}, Lmaps/w/h;-><init>(Landroid/view/animation/Interpolator;)V

    invoke-virtual {v0, v1, v2}, Lmaps/w/w;->a(ILmaps/w/ad;)V

    iget-object v0, p0, Lmaps/w/n;->d:Lmaps/w/w;

    new-instance v1, Lmaps/w/l;

    sget-object v2, Lmaps/w/n;->E:Landroid/view/animation/Interpolator;

    invoke-direct {v1, v2}, Lmaps/w/l;-><init>(Landroid/view/animation/Interpolator;)V

    invoke-virtual {v0, v6, v1}, Lmaps/w/w;->a(ILmaps/w/ad;)V

    iget-object v0, p0, Lmaps/w/n;->e:Lmaps/w/w;

    new-instance v1, Lmaps/w/a;

    iget-object v2, p0, Lmaps/w/n;->L:Landroid/view/animation/Interpolator;

    invoke-direct {v1, v2}, Lmaps/w/a;-><init>(Landroid/view/animation/Interpolator;)V

    invoke-virtual {v0, v7, v1}, Lmaps/w/w;->a(ILmaps/w/ad;)V

    iget-object v0, p0, Lmaps/w/n;->e:Lmaps/w/w;

    new-instance v1, Lmaps/w/l;

    sget-object v2, Lmaps/w/n;->I:Landroid/view/animation/Interpolator;

    invoke-direct {v1, v2}, Lmaps/w/l;-><init>(Landroid/view/animation/Interpolator;)V

    invoke-virtual {v0, v8, v1}, Lmaps/w/w;->a(ILmaps/w/ad;)V

    iget-object v0, p0, Lmaps/w/n;->e:Lmaps/w/w;

    const/4 v1, 0x3

    new-instance v2, Lmaps/w/h;

    sget-object v3, Lmaps/w/n;->K:Landroid/view/animation/Interpolator;

    invoke-direct {v2, v3}, Lmaps/w/h;-><init>(Landroid/view/animation/Interpolator;)V

    invoke-virtual {v0, v1, v2}, Lmaps/w/w;->a(ILmaps/w/ad;)V

    iget-object v0, p0, Lmaps/w/n;->e:Lmaps/w/w;

    new-instance v1, Lmaps/w/l;

    sget-object v2, Lmaps/w/n;->H:Landroid/view/animation/Interpolator;

    invoke-direct {v1, v2}, Lmaps/w/l;-><init>(Landroid/view/animation/Interpolator;)V

    invoke-virtual {v0, v6, v1}, Lmaps/w/w;->a(ILmaps/w/ad;)V

    iget-object v0, p0, Lmaps/w/n;->c:Lmaps/w/w;

    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, v1, v2}, Lmaps/w/w;->a(J)V

    iget-object v0, p0, Lmaps/w/n;->d:Lmaps/w/w;

    const-wide/16 v1, 0x5dc

    invoke-virtual {v0, v1, v2}, Lmaps/w/w;->a(J)V

    iget-object v0, p0, Lmaps/w/n;->e:Lmaps/w/w;

    const-wide/16 v1, 0xbb8

    invoke-virtual {v0, v1, v2}, Lmaps/w/w;->a(J)V

    new-instance v0, Lmaps/w/h;

    new-instance v1, Lmaps/w/af;

    invoke-direct {v1, v5}, Lmaps/w/af;-><init>(F)V

    invoke-direct {v0, v1}, Lmaps/w/h;-><init>(Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Lmaps/w/n;->g:Lmaps/w/h;

    iget-object v0, p0, Lmaps/w/n;->g:Lmaps/w/h;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lmaps/w/h;->setDuration(J)V

    iget-object v0, p0, Lmaps/w/n;->c:Lmaps/w/w;

    iput-object v0, p0, Lmaps/w/n;->f:Lmaps/w/w;

    return-void
.end method

.method private a(F)F
    .locals 2

    iget v0, p0, Lmaps/w/n;->l:F

    div-float v0, p1, v0

    iget v1, p0, Lmaps/w/n;->n:F

    div-float/2addr v0, v1

    return v0
.end method

.method private b(F)F
    .locals 4

    float-to-double v0, p1

    iget-wide v2, p0, Lmaps/w/n;->m:D

    mul-double/2addr v0, v2

    double-to-float v0, v0

    invoke-direct {p0, v0}, Lmaps/w/n;->a(F)F

    move-result v0

    return v0
.end method

.method private b(J)V
    .locals 9

    const-wide/16 v7, 0x1388

    const/4 v1, 0x0

    const/4 v2, 0x1

    iget-boolean v0, p0, Lmaps/w/n;->s:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/w/n;->j:Lmaps/t/aj;

    invoke-virtual {v0}, Lmaps/t/aj;->a()Lmaps/t/bx;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-boolean v1, p0, Lmaps/w/n;->s:Z

    iget-boolean v0, p0, Lmaps/w/n;->r:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lmaps/w/n;->i:Lmaps/t/aj;

    invoke-virtual {v0}, Lmaps/t/aj;->a()Lmaps/t/bx;

    move-result-object v0

    iget-object v1, p0, Lmaps/w/n;->j:Lmaps/t/aj;

    invoke-virtual {v1}, Lmaps/t/aj;->a()Lmaps/t/bx;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/t/bx;->b(Lmaps/t/bx;)V

    iget-object v0, p0, Lmaps/w/n;->i:Lmaps/t/aj;

    iget-object v1, p0, Lmaps/w/n;->j:Lmaps/t/aj;

    invoke-virtual {v1}, Lmaps/t/aj;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Lmaps/t/aj;->a(I)V

    iput-boolean v2, p0, Lmaps/w/n;->r:Z

    goto :goto_0

    :cond_2
    sget-object v0, Lmaps/w/g;->a:Lmaps/w/g;

    iput-object v0, p0, Lmaps/w/n;->u:Lmaps/w/g;

    iget-wide v3, p0, Lmaps/w/n;->h:J

    sub-long v3, p1, v3

    const-wide/16 v5, 0x3e8

    cmp-long v0, v3, v5

    if-ltz v0, :cond_0

    const/4 v0, 0x0

    iget-object v5, p0, Lmaps/w/n;->i:Lmaps/t/aj;

    invoke-virtual {v5}, Lmaps/t/aj;->a()Lmaps/t/bx;

    move-result-object v5

    if-eqz v5, :cond_5

    iget-object v5, p0, Lmaps/w/n;->j:Lmaps/t/aj;

    invoke-virtual {v5}, Lmaps/t/aj;->a()Lmaps/t/bx;

    move-result-object v5

    if-eqz v5, :cond_5

    iget-object v5, p0, Lmaps/w/n;->j:Lmaps/t/aj;

    invoke-virtual {v5}, Lmaps/t/aj;->a()Lmaps/t/bx;

    move-result-object v5

    iget-object v6, p0, Lmaps/w/n;->i:Lmaps/t/aj;

    invoke-virtual {v6}, Lmaps/t/aj;->a()Lmaps/t/bx;

    move-result-object v6

    invoke-virtual {v5, v6}, Lmaps/t/bx;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    iget-object v0, p0, Lmaps/w/n;->i:Lmaps/t/aj;

    invoke-virtual {v0}, Lmaps/t/aj;->a()Lmaps/t/bx;

    move-result-object v0

    iget-object v5, p0, Lmaps/w/n;->j:Lmaps/t/aj;

    invoke-virtual {v5}, Lmaps/t/aj;->a()Lmaps/t/bx;

    move-result-object v5

    invoke-virtual {v0, v5}, Lmaps/t/bx;->c(Lmaps/t/bx;)F

    move-result v0

    invoke-direct {p0, v0}, Lmaps/w/n;->a(F)F

    move-result v0

    :cond_3
    const/high16 v5, 0x41a00000

    cmpl-float v5, v0, v5

    if-gtz v5, :cond_4

    cmp-long v5, v3, v7

    if-lez v5, :cond_5

    :cond_4
    iget-object v1, p0, Lmaps/w/n;->i:Lmaps/t/aj;

    invoke-virtual {v1}, Lmaps/t/aj;->a()Lmaps/t/bx;

    move-result-object v1

    iget-object v5, p0, Lmaps/w/n;->j:Lmaps/t/aj;

    invoke-virtual {v5}, Lmaps/t/aj;->a()Lmaps/t/bx;

    move-result-object v5

    invoke-virtual {v1, v5}, Lmaps/t/bx;->b(Lmaps/t/bx;)V

    move v1, v2

    :cond_5
    iget-object v5, p0, Lmaps/w/n;->j:Lmaps/t/aj;

    invoke-direct {p0, v5}, Lmaps/w/n;->c(Lmaps/t/aj;)V

    iget-object v5, p0, Lmaps/w/n;->i:Lmaps/t/aj;

    invoke-virtual {v5}, Lmaps/t/aj;->c()I

    move-result v5

    iget-object v6, p0, Lmaps/w/n;->j:Lmaps/t/aj;

    invoke-virtual {v6}, Lmaps/t/aj;->c()I

    move-result v6

    if-eq v5, v6, :cond_7

    iget-object v5, p0, Lmaps/w/n;->j:Lmaps/t/aj;

    invoke-virtual {v5}, Lmaps/t/aj;->c()I

    move-result v5

    iget-object v6, p0, Lmaps/w/n;->i:Lmaps/t/aj;

    invoke-virtual {v6}, Lmaps/t/aj;->c()I

    move-result v6

    sub-int/2addr v5, v6

    int-to-float v5, v5

    invoke-direct {p0, v5}, Lmaps/w/n;->b(F)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const/high16 v6, 0x42480000

    cmpl-float v5, v5, v6

    if-gtz v5, :cond_6

    cmp-long v3, v3, v7

    if-gtz v3, :cond_6

    iget-object v3, p0, Lmaps/w/n;->j:Lmaps/t/aj;

    invoke-virtual {v3}, Lmaps/t/aj;->c()I

    move-result v3

    if-nez v3, :cond_7

    :cond_6
    iget-object v1, p0, Lmaps/w/n;->i:Lmaps/t/aj;

    iget-object v3, p0, Lmaps/w/n;->j:Lmaps/t/aj;

    invoke-virtual {v3}, Lmaps/t/aj;->c()I

    move-result v3

    invoke-virtual {v1, v3}, Lmaps/t/aj;->a(I)V

    move v1, v2

    :cond_7
    if-eqz v1, :cond_0

    const/high16 v1, 0x42c80000

    cmpg-float v1, v0, v1

    if-gez v1, :cond_8

    sget-object v0, Lmaps/w/g;->b:Lmaps/w/g;

    iput-object v0, p0, Lmaps/w/n;->u:Lmaps/w/g;

    goto/16 :goto_0

    :cond_8
    iget v1, p0, Lmaps/w/n;->o:F

    iget v2, p0, Lmaps/w/n;->p:F

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_9

    sget-object v0, Lmaps/w/g;->c:Lmaps/w/g;

    iput-object v0, p0, Lmaps/w/n;->u:Lmaps/w/g;

    goto/16 :goto_0

    :cond_9
    sget-object v0, Lmaps/w/g;->d:Lmaps/w/g;

    iput-object v0, p0, Lmaps/w/n;->u:Lmaps/w/g;

    goto/16 :goto_0
.end method

.method private c(F)F
    .locals 4

    iget v0, p0, Lmaps/w/n;->n:F

    mul-float/2addr v0, p1

    iget v1, p0, Lmaps/w/n;->l:F

    mul-float/2addr v0, v1

    float-to-double v0, v0

    iget-wide v2, p0, Lmaps/w/n;->m:D

    div-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method

.method private c(J)V
    .locals 8

    const/4 v7, 0x2

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    iget-object v0, p0, Lmaps/w/n;->u:Lmaps/w/g;

    iput-object v0, p0, Lmaps/w/n;->t:Lmaps/w/g;

    sget-object v0, Lmaps/w/g;->a:Lmaps/w/g;

    iput-object v0, p0, Lmaps/w/n;->u:Lmaps/w/g;

    iget-object v0, p0, Lmaps/w/n;->t:Lmaps/w/g;

    sget-object v1, Lmaps/w/g;->a:Lmaps/w/g;

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lmaps/w/n;->k:Lmaps/t/aj;

    invoke-virtual {p0, v0}, Lmaps/w/n;->a(Lmaps/t/aj;)Z

    sget-object v0, Lmaps/w/d;->a:[I

    iget-object v1, p0, Lmaps/w/n;->t:Lmaps/w/g;

    invoke-virtual {v1}, Lmaps/w/g;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_1
    iget-object v0, p0, Lmaps/w/n;->f:Lmaps/w/w;

    invoke-virtual {v0}, Lmaps/w/w;->a()V

    iput-wide p1, p0, Lmaps/w/n;->h:J

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lmaps/w/n;->c:Lmaps/w/w;

    invoke-virtual {v0, v4}, Lmaps/w/w;->c(I)Lmaps/w/l;

    move-result-object v0

    iget-object v1, p0, Lmaps/w/n;->k:Lmaps/t/aj;

    invoke-virtual {v1}, Lmaps/t/aj;->a()Lmaps/t/bx;

    move-result-object v1

    iget-object v2, p0, Lmaps/w/n;->i:Lmaps/t/aj;

    invoke-virtual {v2}, Lmaps/t/aj;->a()Lmaps/t/bx;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/w/l;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v0, p0, Lmaps/w/n;->c:Lmaps/w/w;

    invoke-virtual {v0, v5}, Lmaps/w/w;->a(I)Lmaps/w/a;

    move-result-object v0

    iget-object v1, p0, Lmaps/w/n;->k:Lmaps/t/aj;

    invoke-virtual {v1}, Lmaps/t/aj;->c()I

    move-result v1

    iget-object v2, p0, Lmaps/w/n;->i:Lmaps/t/aj;

    invoke-virtual {v2}, Lmaps/t/aj;->c()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lmaps/w/a;->a(II)V

    iget-object v0, p0, Lmaps/w/n;->c:Lmaps/w/w;

    invoke-virtual {v0, v6}, Lmaps/w/w;->b(I)Lmaps/w/h;

    move-result-object v0

    invoke-virtual {v0, v3, v3}, Lmaps/w/h;->a(FF)V

    iget-object v0, p0, Lmaps/w/n;->c:Lmaps/w/w;

    iput-object v0, p0, Lmaps/w/n;->f:Lmaps/w/w;

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lmaps/w/n;->d:Lmaps/w/w;

    invoke-virtual {v0, v4}, Lmaps/w/w;->c(I)Lmaps/w/l;

    move-result-object v0

    iget-object v1, p0, Lmaps/w/n;->k:Lmaps/t/aj;

    invoke-virtual {v1}, Lmaps/t/aj;->a()Lmaps/t/bx;

    move-result-object v1

    iget-object v2, p0, Lmaps/w/n;->i:Lmaps/t/aj;

    invoke-virtual {v2}, Lmaps/t/aj;->a()Lmaps/t/bx;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/w/l;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v0, p0, Lmaps/w/n;->d:Lmaps/w/w;

    invoke-virtual {v0, v5}, Lmaps/w/w;->a(I)Lmaps/w/a;

    move-result-object v0

    iget-object v1, p0, Lmaps/w/n;->k:Lmaps/t/aj;

    invoke-virtual {v1}, Lmaps/t/aj;->c()I

    move-result v1

    iget-object v2, p0, Lmaps/w/n;->i:Lmaps/t/aj;

    invoke-virtual {v2}, Lmaps/t/aj;->c()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lmaps/w/a;->a(II)V

    iget-object v0, p0, Lmaps/w/n;->d:Lmaps/w/w;

    invoke-virtual {v0, v7}, Lmaps/w/w;->c(I)Lmaps/w/l;

    move-result-object v0

    iget-object v1, p0, Lmaps/w/n;->k:Lmaps/t/aj;

    invoke-virtual {v1}, Lmaps/t/aj;->a()Lmaps/t/bx;

    move-result-object v1

    iget-object v2, p0, Lmaps/w/n;->i:Lmaps/t/aj;

    invoke-virtual {v2}, Lmaps/t/aj;->a()Lmaps/t/bx;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/w/l;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v0, p0, Lmaps/w/n;->d:Lmaps/w/w;

    invoke-virtual {v0, v6}, Lmaps/w/w;->b(I)Lmaps/w/h;

    move-result-object v0

    const v1, 0x3e4ccccd

    invoke-virtual {v0, v3, v1}, Lmaps/w/h;->a(FF)V

    iget-object v0, p0, Lmaps/w/n;->d:Lmaps/w/w;

    iput-object v0, p0, Lmaps/w/n;->f:Lmaps/w/w;

    goto/16 :goto_1

    :pswitch_2
    iget-object v0, p0, Lmaps/w/n;->e:Lmaps/w/w;

    invoke-virtual {v0, v4}, Lmaps/w/w;->c(I)Lmaps/w/l;

    move-result-object v0

    iget-object v1, p0, Lmaps/w/n;->k:Lmaps/t/aj;

    invoke-virtual {v1}, Lmaps/t/aj;->a()Lmaps/t/bx;

    move-result-object v1

    iget-object v2, p0, Lmaps/w/n;->i:Lmaps/t/aj;

    invoke-virtual {v2}, Lmaps/t/aj;->a()Lmaps/t/bx;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/w/l;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v0, p0, Lmaps/w/n;->k:Lmaps/t/aj;

    invoke-virtual {v0}, Lmaps/t/aj;->c()I

    move-result v0

    iget-object v1, p0, Lmaps/w/n;->i:Lmaps/t/aj;

    invoke-virtual {v1}, Lmaps/t/aj;->c()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget-object v1, p0, Lmaps/w/n;->k:Lmaps/t/aj;

    invoke-virtual {v1}, Lmaps/t/aj;->c()I

    move-result v1

    int-to-float v1, v1

    int-to-float v2, v0

    div-float/2addr v1, v2

    iput v1, p0, Lmaps/w/n;->a:F

    iget-object v1, p0, Lmaps/w/n;->i:Lmaps/t/aj;

    invoke-virtual {v1}, Lmaps/t/aj;->c()I

    move-result v1

    int-to-float v1, v1

    int-to-float v2, v0

    div-float/2addr v1, v2

    iput v1, p0, Lmaps/w/n;->b:F

    iget-object v1, p0, Lmaps/w/n;->e:Lmaps/w/w;

    invoke-virtual {v1, v5}, Lmaps/w/w;->a(I)Lmaps/w/a;

    move-result-object v1

    invoke-virtual {v1, v4, v0}, Lmaps/w/a;->a(II)V

    iget-object v0, p0, Lmaps/w/n;->e:Lmaps/w/w;

    invoke-virtual {v0, v7}, Lmaps/w/w;->c(I)Lmaps/w/l;

    move-result-object v0

    iget-object v1, p0, Lmaps/w/n;->k:Lmaps/t/aj;

    invoke-virtual {v1}, Lmaps/t/aj;->a()Lmaps/t/bx;

    move-result-object v1

    iget-object v2, p0, Lmaps/w/n;->i:Lmaps/t/aj;

    invoke-virtual {v2}, Lmaps/t/aj;->a()Lmaps/t/bx;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/w/l;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v0, p0, Lmaps/w/n;->e:Lmaps/w/w;

    invoke-virtual {v0, v6}, Lmaps/w/w;->b(I)Lmaps/w/h;

    move-result-object v0

    const v1, 0x3ecccccd

    invoke-virtual {v0, v3, v1}, Lmaps/w/h;->a(FF)V

    iget-object v0, p0, Lmaps/w/n;->e:Lmaps/w/w;

    iput-object v0, p0, Lmaps/w/n;->f:Lmaps/w/w;

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private c(Lmaps/t/aj;)V
    .locals 3

    const/high16 v2, 0x41700000

    invoke-virtual {p1}, Lmaps/t/aj;->c()I

    move-result v0

    int-to-float v0, v0

    invoke-direct {p0, v0}, Lmaps/w/n;->b(F)F

    move-result v0

    cmpg-float v0, v0, v2

    if-gez v0, :cond_0

    iget v0, p0, Lmaps/w/n;->q:F

    const/high16 v1, 0x41400000

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lmaps/t/aj;->a(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, v2}, Lmaps/w/n;->c(F)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1, v0}, Lmaps/t/aj;->a(I)V

    goto :goto_0
.end method


# virtual methods
.method public a(J)I
    .locals 2

    invoke-direct {p0, p1, p2}, Lmaps/w/n;->b(J)V

    iget-object v0, p0, Lmaps/w/n;->f:Lmaps/w/w;

    invoke-virtual {v0}, Lmaps/w/w;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/w/n;->f:Lmaps/w/w;

    invoke-virtual {v0}, Lmaps/w/w;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lmaps/w/g;->a:Lmaps/w/g;

    iput-object v0, p0, Lmaps/w/n;->t:Lmaps/w/g;

    invoke-direct {p0, p1, p2}, Lmaps/w/n;->c(J)V

    :cond_1
    iget-object v0, p0, Lmaps/w/n;->t:Lmaps/w/g;

    sget-object v1, Lmaps/w/g;->a:Lmaps/w/g;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lmaps/w/n;->f:Lmaps/w/w;

    invoke-virtual {v0, p1, p2}, Lmaps/w/w;->b(J)V

    :cond_2
    iget-object v0, p0, Lmaps/w/n;->g:Lmaps/w/h;

    invoke-virtual {v0, p1, p2}, Lmaps/w/h;->a(J)V

    iget-object v0, p0, Lmaps/w/n;->f:Lmaps/w/w;

    invoke-virtual {v0}, Lmaps/w/w;->c()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Lmaps/bq/d;)V
    .locals 2

    invoke-virtual {p1}, Lmaps/bq/d;->z()F

    move-result v0

    iput v0, p0, Lmaps/w/n;->l:F

    invoke-virtual {p1}, Lmaps/bq/d;->n()F

    move-result v0

    iput v0, p0, Lmaps/w/n;->n:F

    invoke-virtual {p1}, Lmaps/bq/d;->l()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lmaps/w/n;->n:F

    div-float/2addr v0, v1

    iput v0, p0, Lmaps/w/n;->o:F

    invoke-virtual {p1}, Lmaps/bq/d;->m()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lmaps/w/n;->n:F

    div-float/2addr v0, v1

    iput v0, p0, Lmaps/w/n;->p:F

    invoke-virtual {p1}, Lmaps/bq/d;->h()Lmaps/t/bx;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/bx;->e()D

    move-result-wide v0

    iput-wide v0, p0, Lmaps/w/n;->m:D

    invoke-virtual {p1}, Lmaps/bq/d;->s()F

    move-result v0

    iput v0, p0, Lmaps/w/n;->q:F

    return-void
.end method

.method public a(Lmaps/t/aj;)Z
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lmaps/w/n;->f:Lmaps/w/w;

    invoke-virtual {v0}, Lmaps/w/w;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/w/n;->j:Lmaps/t/aj;

    invoke-virtual {p1, v0}, Lmaps/t/aj;->a(Lmaps/t/aj;)V

    :goto_0
    invoke-virtual {p1}, Lmaps/t/aj;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lmaps/w/n;->q:F

    const/high16 v3, 0x41400000

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Lmaps/t/aj;->a(Z)V

    invoke-direct {p0, p1}, Lmaps/w/n;->c(Lmaps/t/aj;)V

    return v1

    :cond_0
    iget-object v0, p0, Lmaps/w/n;->f:Lmaps/w/w;

    invoke-virtual {v0, v2}, Lmaps/w/w;->c(I)Lmaps/w/l;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/w/l;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/bx;

    iget-object v3, p0, Lmaps/w/n;->g:Lmaps/w/h;

    invoke-virtual {v3}, Lmaps/w/h;->b()F

    move-result v3

    iget-object v4, p0, Lmaps/w/n;->f:Lmaps/w/w;

    invoke-virtual {v4, v1}, Lmaps/w/w;->a(I)Lmaps/w/a;

    move-result-object v4

    invoke-virtual {v4}, Lmaps/w/a;->b()I

    move-result v4

    invoke-virtual {p1, v0, v3, v4}, Lmaps/t/aj;->a(Lmaps/t/bx;FI)V

    iget-object v0, p0, Lmaps/w/n;->f:Lmaps/w/w;

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Lmaps/w/w;->c(I)Lmaps/w/l;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/w/l;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/bx;

    invoke-virtual {p1, v0}, Lmaps/t/aj;->b(Lmaps/t/bx;)V

    iget-object v0, p0, Lmaps/w/n;->f:Lmaps/w/w;

    const/4 v3, 0x3

    invoke-virtual {v0, v3}, Lmaps/w/w;->b(I)Lmaps/w/h;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/w/h;->b()F

    move-result v0

    invoke-virtual {p1, v0}, Lmaps/t/aj;->b(F)V

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1
.end method

.method public b(Lmaps/t/aj;)V
    .locals 2

    iget-object v0, p0, Lmaps/w/n;->j:Lmaps/t/aj;

    invoke-virtual {v0, p1}, Lmaps/t/aj;->a(Lmaps/t/aj;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/w/n;->s:Z

    iget-object v0, p0, Lmaps/w/n;->j:Lmaps/t/aj;

    invoke-virtual {v0}, Lmaps/t/aj;->b()F

    move-result v0

    iget-object v1, p0, Lmaps/w/n;->g:Lmaps/w/h;

    invoke-virtual {v1}, Lmaps/w/h;->a()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/w/n;->i:Lmaps/t/aj;

    iget-object v1, p0, Lmaps/w/n;->j:Lmaps/t/aj;

    invoke-virtual {v1}, Lmaps/t/aj;->b()F

    move-result v1

    invoke-virtual {v0, v1}, Lmaps/t/aj;->a(F)V

    iget-object v0, p0, Lmaps/w/n;->g:Lmaps/w/h;

    iget-object v1, p0, Lmaps/w/n;->i:Lmaps/t/aj;

    invoke-virtual {v1}, Lmaps/t/aj;->b()F

    move-result v1

    invoke-virtual {v0, v1}, Lmaps/w/h;->a(F)V

    iget-object v0, p0, Lmaps/w/n;->g:Lmaps/w/h;

    invoke-virtual {v0}, Lmaps/w/h;->start()V

    :cond_0
    return-void
.end method
