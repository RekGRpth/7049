.class public Lmaps/w/p;
.super Lmaps/w/h;


# direct methods
.method public constructor <init>(JJLmaps/w/q;)V
    .locals 8

    const/4 v6, 0x0

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    move-object v5, p5

    move v7, v6

    invoke-direct/range {v0 .. v7}, Lmaps/w/p;-><init>(JJLmaps/w/q;FF)V

    return-void
.end method

.method public constructor <init>(JJLmaps/w/q;FF)V
    .locals 4

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    new-instance v0, Lmaps/w/r;

    long-to-float v1, p1

    add-long v2, p1, p3

    long-to-float v2, v2

    div-float/2addr v1, v2

    invoke-direct {v0, v1}, Lmaps/w/r;-><init>(F)V

    :goto_0
    invoke-direct {p0, v0}, Lmaps/w/h;-><init>(Landroid/view/animation/Interpolator;)V

    add-long v0, p1, p3

    invoke-virtual {p0, v0, v1}, Lmaps/w/p;->setDuration(J)V

    invoke-virtual {p0, p5, p6, p7}, Lmaps/w/p;->a(Lmaps/w/q;FF)V

    return-void

    :cond_0
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method public a(Lmaps/w/q;FF)V
    .locals 4

    const/high16 v3, 0x3f800000

    const/4 v2, 0x0

    sget-object v0, Lmaps/w/b;->a:[I

    invoke-virtual {p1}, Lmaps/w/q;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0, v2}, Lmaps/w/p;->a(F)V

    invoke-virtual {p0, v3}, Lmaps/w/p;->a(F)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0, v3}, Lmaps/w/p;->a(F)V

    invoke-virtual {p0, v2}, Lmaps/w/p;->a(F)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0, p2}, Lmaps/w/p;->a(F)V

    invoke-virtual {p0, p3}, Lmaps/w/p;->a(F)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public b(J)F
    .locals 1

    invoke-virtual {p0, p1, p2}, Lmaps/w/p;->a(J)V

    invoke-virtual {p0}, Lmaps/w/p;->b()F

    move-result v0

    return v0
.end method
