.class public final enum Lmaps/w/q;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lmaps/w/q;

.field public static final enum b:Lmaps/w/q;

.field public static final enum c:Lmaps/w/q;

.field private static final synthetic d:[Lmaps/w/q;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lmaps/w/q;

    const-string v1, "FADE_IN"

    invoke-direct {v0, v1, v2}, Lmaps/w/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/w/q;->a:Lmaps/w/q;

    new-instance v0, Lmaps/w/q;

    const-string v1, "FADE_OUT"

    invoke-direct {v0, v1, v3}, Lmaps/w/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/w/q;->b:Lmaps/w/q;

    new-instance v0, Lmaps/w/q;

    const-string v1, "FADE_BETWEEN"

    invoke-direct {v0, v1, v4}, Lmaps/w/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/w/q;->c:Lmaps/w/q;

    const/4 v0, 0x3

    new-array v0, v0, [Lmaps/w/q;

    sget-object v1, Lmaps/w/q;->a:Lmaps/w/q;

    aput-object v1, v0, v2

    sget-object v1, Lmaps/w/q;->b:Lmaps/w/q;

    aput-object v1, v0, v3

    sget-object v1, Lmaps/w/q;->c:Lmaps/w/q;

    aput-object v1, v0, v4

    sput-object v0, Lmaps/w/q;->d:[Lmaps/w/q;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/w/q;
    .locals 1

    const-class v0, Lmaps/w/q;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/w/q;

    return-object v0
.end method

.method public static values()[Lmaps/w/q;
    .locals 1

    sget-object v0, Lmaps/w/q;->d:[Lmaps/w/q;

    invoke-virtual {v0}, [Lmaps/w/q;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/w/q;

    return-object v0
.end method
