.class public Lmaps/w/s;
.super Ljava/lang/Object;


# instance fields
.field private a:Ljava/util/List;

.field private b:Lmaps/w/k;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmaps/w/s;->a:Ljava/util/List;

    sget-object v0, Lmaps/w/k;->a:Lmaps/w/k;

    iput-object v0, p0, Lmaps/w/s;->b:Lmaps/w/k;

    return-void
.end method


# virtual methods
.method public a()Lmaps/w/s;
    .locals 2

    iget-object v0, p0, Lmaps/w/s;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "reverse must be called after add"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lmaps/w/s;->a:Ljava/util/List;

    iget-object v1, p0, Lmaps/w/s;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/w/ae;

    sget-object v1, Lmaps/w/x;->b:Lmaps/w/x;

    iput-object v1, v0, Lmaps/w/ae;->c:Lmaps/w/x;

    return-object p0
.end method

.method public a(F)Lmaps/w/s;
    .locals 2

    iget-object v0, p0, Lmaps/w/s;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "withPeriod must be called after add"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lmaps/w/s;->a:Ljava/util/List;

    iget-object v1, p0, Lmaps/w/s;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/w/ae;

    iput p1, v0, Lmaps/w/ae;->b:F

    return-object p0
.end method

.method public a(I)Lmaps/w/s;
    .locals 5

    iget-object v0, p0, Lmaps/w/s;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "repeat must be called after add"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, p1, :cond_1

    iget-object v2, p0, Lmaps/w/s;->a:Ljava/util/List;

    new-instance v3, Lmaps/w/ae;

    iget-object v0, p0, Lmaps/w/s;->a:Ljava/util/List;

    iget-object v4, p0, Lmaps/w/s;->a:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/w/ae;

    invoke-direct {v3, v0}, Lmaps/w/ae;-><init>(Lmaps/w/ae;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-object p0
.end method

.method public a(Landroid/view/animation/Interpolator;)Lmaps/w/s;
    .locals 2

    new-instance v0, Lmaps/w/ae;

    invoke-direct {v0}, Lmaps/w/ae;-><init>()V

    iget-object v1, p0, Lmaps/w/s;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iput-object p1, v0, Lmaps/w/ae;->a:Landroid/view/animation/Interpolator;

    const/high16 v1, 0x3f800000

    iput v1, v0, Lmaps/w/ae;->b:F

    sget-object v1, Lmaps/w/x;->a:Lmaps/w/x;

    iput-object v1, v0, Lmaps/w/ae;->c:Lmaps/w/x;

    return-object p0
.end method

.method public b()Lmaps/w/s;
    .locals 1

    sget-object v0, Lmaps/w/k;->b:Lmaps/w/k;

    iput-object v0, p0, Lmaps/w/s;->b:Lmaps/w/k;

    return-object p0
.end method

.method public c()Lmaps/w/z;
    .locals 3

    new-instance v1, Lmaps/w/z;

    iget-object v0, p0, Lmaps/w/s;->a:Ljava/util/List;

    const/4 v2, 0x0

    new-array v2, v2, [Lmaps/w/ae;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/w/ae;

    iget-object v2, p0, Lmaps/w/s;->b:Lmaps/w/k;

    invoke-direct {v1, v0, v2}, Lmaps/w/z;-><init>([Lmaps/w/ae;Lmaps/w/k;)V

    return-object v1
.end method
