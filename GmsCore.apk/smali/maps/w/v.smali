.class public Lmaps/w/v;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lmaps/w/a;


# direct methods
.method public constructor <init>(JJLmaps/w/j;)V
    .locals 8

    const/4 v6, 0x0

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    move-object v5, p5

    move v7, v6

    invoke-direct/range {v0 .. v7}, Lmaps/w/v;-><init>(JJLmaps/w/j;II)V

    return-void
.end method

.method public constructor <init>(JJLmaps/w/j;II)V
    .locals 7

    const/high16 v6, 0x10000

    const/4 v5, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lmaps/w/a;

    new-instance v1, Lmaps/w/r;

    long-to-float v2, p1

    add-long v3, p1, p3

    long-to-float v3, v3

    div-float/2addr v2, v3

    invoke-direct {v1, v2}, Lmaps/w/r;-><init>(F)V

    invoke-direct {v0, v1}, Lmaps/w/a;-><init>(Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Lmaps/w/v;->a:Lmaps/w/a;

    iget-object v0, p0, Lmaps/w/v;->a:Lmaps/w/a;

    add-long v1, p1, p3

    invoke-virtual {v0, v1, v2}, Lmaps/w/a;->setDuration(J)V

    sget-object v0, Lmaps/w/y;->a:[I

    invoke-virtual {p5}, Lmaps/w/j;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lmaps/w/v;->a:Lmaps/w/a;

    invoke-virtual {v0, v5}, Lmaps/w/a;->a(I)V

    iget-object v0, p0, Lmaps/w/v;->a:Lmaps/w/a;

    invoke-virtual {v0, v6}, Lmaps/w/a;->a(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lmaps/w/v;->a:Lmaps/w/a;

    invoke-virtual {v0, v6}, Lmaps/w/a;->a(I)V

    iget-object v0, p0, Lmaps/w/v;->a:Lmaps/w/a;

    invoke-virtual {v0, v5}, Lmaps/w/a;->a(I)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lmaps/w/v;->a:Lmaps/w/a;

    invoke-virtual {v0, p6}, Lmaps/w/a;->a(I)V

    iget-object v0, p0, Lmaps/w/v;->a:Lmaps/w/a;

    invoke-virtual {v0, p7}, Lmaps/w/a;->a(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public constructor <init>(JLmaps/w/j;)V
    .locals 6

    const-wide/16 v1, 0x0

    move-object v0, p0

    move-wide v3, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lmaps/w/v;-><init>(JJLmaps/w/j;)V

    return-void
.end method


# virtual methods
.method public a(Lmaps/cr/c;)I
    .locals 3

    invoke-virtual {p1}, Lmaps/cr/c;->e()J

    move-result-wide v0

    iget-object v2, p0, Lmaps/w/v;->a:Lmaps/w/a;

    invoke-virtual {v2}, Lmaps/w/a;->hasStarted()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lmaps/w/v;->a:Lmaps/w/a;

    invoke-virtual {v2}, Lmaps/w/a;->start()V

    :cond_0
    iget-object v2, p0, Lmaps/w/v;->a:Lmaps/w/a;

    invoke-virtual {v2, v0, v1}, Lmaps/w/a;->a(J)V

    iget-object v0, p0, Lmaps/w/v;->a:Lmaps/w/a;

    invoke-virtual {v0}, Lmaps/w/a;->b()I

    move-result v0

    iget-object v1, p0, Lmaps/w/v;->a:Lmaps/w/a;

    invoke-virtual {v1}, Lmaps/w/a;->hasEnded()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p1}, Lmaps/cr/c;->b()V

    :cond_1
    return v0
.end method

.method public a()V
    .locals 1

    iget-object v0, p0, Lmaps/w/v;->a:Lmaps/w/a;

    invoke-virtual {v0}, Lmaps/w/a;->start()V

    return-void
.end method
