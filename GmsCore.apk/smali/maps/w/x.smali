.class public final enum Lmaps/w/x;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lmaps/w/x;

.field public static final enum b:Lmaps/w/x;

.field private static final synthetic c:[Lmaps/w/x;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lmaps/w/x;

    const-string v1, "FORWARDS"

    invoke-direct {v0, v1, v2}, Lmaps/w/x;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/w/x;->a:Lmaps/w/x;

    new-instance v0, Lmaps/w/x;

    const-string v1, "BACKWARDS"

    invoke-direct {v0, v1, v3}, Lmaps/w/x;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/w/x;->b:Lmaps/w/x;

    const/4 v0, 0x2

    new-array v0, v0, [Lmaps/w/x;

    sget-object v1, Lmaps/w/x;->a:Lmaps/w/x;

    aput-object v1, v0, v2

    sget-object v1, Lmaps/w/x;->b:Lmaps/w/x;

    aput-object v1, v0, v3

    sput-object v0, Lmaps/w/x;->c:[Lmaps/w/x;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/w/x;
    .locals 1

    const-class v0, Lmaps/w/x;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/w/x;

    return-object v0
.end method

.method public static values()[Lmaps/w/x;
    .locals 1

    sget-object v0, Lmaps/w/x;->c:[Lmaps/w/x;

    invoke-virtual {v0}, [Lmaps/w/x;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/w/x;

    return-object v0
.end method
