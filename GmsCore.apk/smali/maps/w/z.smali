.class public Lmaps/w/z;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/animation/Interpolator;


# instance fields
.field protected final a:[Lmaps/w/ae;

.field protected final b:Lmaps/w/k;


# direct methods
.method protected constructor <init>([Lmaps/w/ae;Lmaps/w/k;)V
    .locals 6

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/w/z;->a:[Lmaps/w/ae;

    iput-object p2, p0, Lmaps/w/z;->b:Lmaps/w/k;

    const/4 v1, 0x0

    iget-object v3, p0, Lmaps/w/z;->a:[Lmaps/w/ae;

    array-length v4, v3

    move v2, v1

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    iget v5, v5, Lmaps/w/ae;->b:F

    add-float/2addr v2, v5

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lmaps/w/z;->a:[Lmaps/w/ae;

    array-length v3, v1

    :goto_1
    if-ge v0, v3, :cond_1

    aget-object v4, v1, v0

    iget v5, v4, Lmaps/w/ae;->b:F

    div-float/2addr v5, v2

    iput v5, v4, Lmaps/w/ae;->b:F

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 8

    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/w/z;->a:[Lmaps/w/ae;

    array-length v1, v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v3, 0x0

    iget-object v5, p0, Lmaps/w/z;->a:[Lmaps/w/ae;

    array-length v6, v5

    const/4 v1, 0x0

    move v4, v1

    move v1, p1

    :goto_1
    if-ge v4, v6, :cond_5

    aget-object v2, v5, v4

    iget v7, v2, Lmaps/w/ae;->b:F

    sub-float v7, v1, v7

    cmpg-float v7, v7, v0

    if-gtz v7, :cond_2

    move-object v0, v2

    :goto_2
    if-nez v0, :cond_1

    iget-object v0, p0, Lmaps/w/z;->a:[Lmaps/w/ae;

    iget-object v1, p0, Lmaps/w/z;->a:[Lmaps/w/ae;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    iget v1, v0, Lmaps/w/ae;->b:F

    :cond_1
    iget-object v2, v0, Lmaps/w/ae;->c:Lmaps/w/x;

    sget-object v3, Lmaps/w/x;->b:Lmaps/w/x;

    if-ne v2, v3, :cond_4

    iget v2, v0, Lmaps/w/ae;->b:F

    sub-float/2addr v2, v1

    :goto_3
    iget-object v3, v0, Lmaps/w/ae;->a:Landroid/view/animation/Interpolator;

    iget v4, v0, Lmaps/w/ae;->b:F

    div-float/2addr v2, v4

    invoke-interface {v3, v2}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v2

    iget-object v3, p0, Lmaps/w/z;->b:Lmaps/w/k;

    sget-object v4, Lmaps/w/k;->b:Lmaps/w/k;

    if-ne v3, v4, :cond_3

    move v0, v2

    goto :goto_0

    :cond_2
    iget v2, v2, Lmaps/w/ae;->b:F

    sub-float v2, v1, v2

    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move v1, v2

    goto :goto_1

    :cond_3
    iget v0, v0, Lmaps/w/ae;->b:F

    mul-float/2addr v0, v2

    sub-float v1, p1, v1

    add-float/2addr v0, v1

    goto :goto_0

    :cond_4
    move v2, v1

    goto :goto_3

    :cond_5
    move-object v0, v3

    goto :goto_2
.end method
