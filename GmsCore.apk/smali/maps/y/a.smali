.class public Lmaps/y/a;
.super Lmaps/y/bc;


# static fields
.field private static final a:Lmaps/t/bx;

.field private static final b:Lmaps/t/bx;


# instance fields
.field private final c:Lmaps/t/cg;

.field private d:Lmaps/t/cg;

.field private e:Lmaps/t/cg;

.field private final f:Ljava/util/List;

.field private final g:Lmaps/t/bg;

.field private h:Lmaps/t/ax;

.field private i:F

.field private j:F

.field private final k:Lmaps/al/o;

.field private final l:Lmaps/al/i;

.field private final m:Lmaps/al/q;

.field private n:Lmaps/q/ac;

.field private o:Lmaps/y/y;

.field private p:F

.field private q:I

.field private r:Z

.field private final s:I

.field private t:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lmaps/t/bx;

    const/high16 v1, 0x40000000

    invoke-direct {v0, v1, v2}, Lmaps/t/bx;-><init>(II)V

    sput-object v0, Lmaps/y/a;->a:Lmaps/t/bx;

    new-instance v0, Lmaps/t/bx;

    const/high16 v1, -0x40000000

    invoke-direct {v0, v1, v2}, Lmaps/t/bx;-><init>(II)V

    sput-object v0, Lmaps/y/a;->b:Lmaps/t/bx;

    return-void
.end method

.method public constructor <init>(Lmaps/t/cg;FILmaps/t/bg;)V
    .locals 6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lmaps/y/a;-><init>(Lmaps/t/cg;FILmaps/t/bg;Z)V

    return-void
.end method

.method public constructor <init>(Lmaps/t/cg;FILmaps/t/bg;Z)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lmaps/y/bc;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/y/a;->t:Z

    iput-object p1, p0, Lmaps/y/a;->c:Lmaps/t/cg;

    iput p2, p0, Lmaps/y/a;->p:F

    iput p3, p0, Lmaps/y/a;->q:I

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/a;->f:Ljava/util/List;

    iput-object p4, p0, Lmaps/y/a;->g:Lmaps/t/bg;

    iput-boolean p5, p0, Lmaps/y/a;->t:Z

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {v0}, Lmaps/s/q;->a(Ljava/util/List;)I

    move-result v1

    iput v1, p0, Lmaps/y/a;->s:I

    sget-boolean v1, Lmaps/ae/h;->F:Z

    if-eqz v1, :cond_0

    iput-object v2, p0, Lmaps/y/a;->k:Lmaps/al/o;

    iput-object v2, p0, Lmaps/y/a;->m:Lmaps/al/q;

    iput-object v2, p0, Lmaps/y/a;->l:Lmaps/al/i;

    :goto_0
    return-void

    :cond_0
    new-instance v1, Lmaps/al/o;

    iget v2, p0, Lmaps/y/a;->s:I

    invoke-direct {v1, v2}, Lmaps/al/o;-><init>(I)V

    iput-object v1, p0, Lmaps/y/a;->k:Lmaps/al/o;

    new-instance v1, Lmaps/al/q;

    iget v2, p0, Lmaps/y/a;->s:I

    invoke-direct {v1, v2}, Lmaps/al/q;-><init>(I)V

    iput-object v1, p0, Lmaps/y/a;->m:Lmaps/al/q;

    new-instance v1, Lmaps/al/i;

    invoke-static {v0}, Lmaps/s/q;->b(Ljava/util/List;)I

    move-result v0

    invoke-direct {v1, v0}, Lmaps/al/i;-><init>(I)V

    iput-object v1, p0, Lmaps/y/a;->l:Lmaps/al/i;

    goto :goto_0
.end method

.method static a(Lmaps/t/cg;)Ljava/util/List;
    .locals 13

    invoke-virtual {p0}, Lmaps/t/cg;->b()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Lmaps/t/cg;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    invoke-static {v0}, Lmaps/f/fd;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v1

    new-instance v2, Lmaps/t/by;

    invoke-virtual {p0}, Lmaps/t/cg;->b()I

    move-result v0

    invoke-direct {v2, v0}, Lmaps/t/by;-><init>(I)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lmaps/t/cg;->a(I)Lmaps/t/bx;

    move-result-object v3

    invoke-virtual {v2, v3}, Lmaps/t/by;->a(Lmaps/t/bx;)Z

    new-instance v4, Lmaps/t/bx;

    invoke-direct {v4}, Lmaps/t/bx;-><init>()V

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p0}, Lmaps/t/cg;->b()I

    move-result v5

    if-ge v0, v5, :cond_3

    invoke-virtual {p0, v0, v4}, Lmaps/t/cg;->a(ILmaps/t/bx;)V

    invoke-virtual {v4}, Lmaps/t/bx;->f()I

    move-result v5

    invoke-virtual {v3}, Lmaps/t/bx;->f()I

    move-result v6

    if-ge v5, v6, :cond_2

    invoke-virtual {v3}, Lmaps/t/bx;->f()I

    move-result v5

    invoke-virtual {v4}, Lmaps/t/bx;->f()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {v4}, Lmaps/t/bx;->f()I

    move-result v6

    invoke-virtual {v3}, Lmaps/t/bx;->f()I

    move-result v7

    sub-int/2addr v6, v7

    const/high16 v7, 0x40000000

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_1

    const/high16 v5, 0x20000000

    invoke-virtual {v3}, Lmaps/t/bx;->f()I

    move-result v7

    sub-int/2addr v5, v7

    invoke-virtual {v4}, Lmaps/t/bx;->g()I

    move-result v7

    invoke-virtual {v3}, Lmaps/t/bx;->g()I

    move-result v8

    sub-int/2addr v7, v8

    invoke-virtual {v3}, Lmaps/t/bx;->g()I

    move-result v8

    int-to-double v9, v7

    int-to-double v11, v5

    mul-double/2addr v9, v11

    int-to-double v5, v6

    div-double v5, v9, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->round(D)J

    move-result-wide v5

    long-to-int v5, v5

    add-int/2addr v5, v8

    new-instance v6, Lmaps/t/bx;

    const v7, 0x1fffffff

    invoke-direct {v6, v7, v5}, Lmaps/t/bx;-><init>(II)V

    invoke-virtual {v2, v6}, Lmaps/t/by;->a(Lmaps/t/bx;)Z

    invoke-virtual {v2}, Lmaps/t/by;->c()Lmaps/t/cg;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v2}, Lmaps/t/by;->b()V

    new-instance v6, Lmaps/t/bx;

    const/high16 v7, -0x20000000

    invoke-direct {v6, v7, v5}, Lmaps/t/bx;-><init>(II)V

    invoke-virtual {v2, v6}, Lmaps/t/by;->a(Lmaps/t/bx;)Z

    :cond_1
    :goto_2
    invoke-virtual {v2, v4}, Lmaps/t/by;->a(Lmaps/t/bx;)Z

    invoke-virtual {v3, v4}, Lmaps/t/bx;->b(Lmaps/t/bx;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {v4}, Lmaps/t/bx;->f()I

    move-result v5

    invoke-virtual {v3}, Lmaps/t/bx;->f()I

    move-result v6

    if-le v5, v6, :cond_1

    invoke-virtual {v4}, Lmaps/t/bx;->f()I

    move-result v5

    invoke-virtual {v3}, Lmaps/t/bx;->f()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {v3}, Lmaps/t/bx;->f()I

    move-result v6

    invoke-virtual {v4}, Lmaps/t/bx;->f()I

    move-result v7

    sub-int/2addr v6, v7

    const/high16 v7, 0x40000000

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_1

    invoke-virtual {v3}, Lmaps/t/bx;->f()I

    move-result v5

    const/high16 v7, 0x20000000

    add-int/2addr v5, v7

    invoke-virtual {v4}, Lmaps/t/bx;->g()I

    move-result v7

    invoke-virtual {v3}, Lmaps/t/bx;->g()I

    move-result v8

    sub-int/2addr v7, v8

    invoke-virtual {v3}, Lmaps/t/bx;->g()I

    move-result v8

    int-to-double v9, v7

    int-to-double v11, v5

    mul-double/2addr v9, v11

    int-to-double v5, v6

    div-double v5, v9, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->round(D)J

    move-result-wide v5

    long-to-int v5, v5

    add-int/2addr v5, v8

    new-instance v6, Lmaps/t/bx;

    const/high16 v7, -0x20000000

    invoke-direct {v6, v7, v5}, Lmaps/t/bx;-><init>(II)V

    invoke-virtual {v2, v6}, Lmaps/t/by;->a(Lmaps/t/bx;)Z

    invoke-virtual {v2}, Lmaps/t/by;->c()Lmaps/t/cg;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v2}, Lmaps/t/by;->b()V

    new-instance v6, Lmaps/t/bx;

    const v7, 0x1fffffff

    invoke-direct {v6, v7, v5}, Lmaps/t/bx;-><init>(II)V

    invoke-virtual {v2, v6}, Lmaps/t/by;->a(Lmaps/t/bx;)Z

    goto :goto_2

    :cond_3
    invoke-virtual {v2}, Lmaps/t/by;->c()Lmaps/t/cg;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/cg;->b()I

    move-result v2

    const/4 v3, 0x2

    if-lt v2, v3, :cond_4

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    move-object v0, v1

    goto/16 :goto_0
.end method

.method private a(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 5

    const v4, 0xff00

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/y/a;->q:I

    shr-int/lit8 v0, v0, 0x10

    and-int/2addr v0, v4

    iget v1, p0, Lmaps/y/a;->q:I

    shr-int/lit8 v1, v1, 0x8

    and-int/2addr v1, v4

    iget v2, p0, Lmaps/y/a;->q:I

    and-int/2addr v2, v4

    iget v3, p0, Lmaps/y/a;->q:I

    shl-int/lit8 v3, v3, 0x8

    and-int/2addr v3, v4

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {p1, v1, v2, v3, v0}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private a(Lmaps/cr/c;Lmaps/af/s;Lmaps/bq/d;)V
    .locals 6

    const/4 v3, 0x0

    const/high16 v5, 0x10000

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-direct {p0}, Lmaps/y/a;->e()Lmaps/p/z;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v0, p0, Lmaps/y/a;->f:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/cg;

    invoke-virtual {v0, v3}, Lmaps/t/cg;->a(I)Lmaps/t/bx;

    move-result-object v0

    invoke-interface {v2, p1, p3, p2, v0}, Lmaps/p/z;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;Lmaps/t/bx;)V

    :cond_0
    iget-object v0, p0, Lmaps/y/a;->h:Lmaps/t/ax;

    invoke-virtual {v0}, Lmaps/t/ax;->d()Lmaps/t/bx;

    move-result-object v0

    iget-object v3, p0, Lmaps/y/a;->h:Lmaps/t/ax;

    invoke-virtual {v3}, Lmaps/t/ax;->g()I

    move-result v3

    int-to-float v3, v3

    invoke-static {p1, p3, v0, v3}, Lmaps/af/d;->b(Lmaps/cr/c;Lmaps/bq/d;Lmaps/t/bx;F)V

    invoke-virtual {p1}, Lmaps/cr/c;->r()V

    const/4 v0, 0x1

    const/16 v3, 0x303

    invoke-interface {v1, v0, v3}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v0, 0x2300

    const/16 v3, 0x2200

    const/16 v4, 0x2100

    invoke-interface {v1, v0, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    invoke-direct {p0, v1}, Lmaps/y/a;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    invoke-virtual {p1}, Lmaps/cr/c;->a()Lmaps/s/i;

    move-result-object v0

    const/16 v1, 0x18

    invoke-virtual {v0, v1}, Lmaps/s/i;->a(I)Lmaps/cr/a;

    move-result-object v0

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/cr/a;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    iget-object v0, p0, Lmaps/y/a;->k:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->d(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/y/a;->m:Lmaps/al/q;

    invoke-virtual {v0, p1}, Lmaps/al/q;->d(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/y/a;->l:Lmaps/al/i;

    const/4 v1, 0x4

    invoke-virtual {v0, p1, v1}, Lmaps/al/i;->a(Lmaps/cr/c;I)V

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0, v5, v5, v5, v5}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    if-eqz v2, :cond_1

    invoke-interface {v2, p1, p2}, Lmaps/p/z;->a(Lmaps/cr/c;Lmaps/af/s;)V

    :cond_1
    return-void
.end method

.method private a(Lmaps/cr/c;Lmaps/bq/d;)V
    .locals 2

    iget-object v0, p0, Lmaps/y/a;->k:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->a(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/y/a;->l:Lmaps/al/i;

    invoke-virtual {v0, p1}, Lmaps/al/i;->a(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/y/a;->m:Lmaps/al/q;

    invoke-virtual {v0, p1}, Lmaps/al/q;->a(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/y/a;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/cg;

    invoke-direct {p0, v0, p2}, Lmaps/y/a;->a(Lmaps/t/cg;Lmaps/bq/d;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p2}, Lmaps/bq/d;->p()F

    move-result v0

    iput v0, p0, Lmaps/y/a;->j:F

    return-void
.end method

.method private a(Lmaps/t/cg;Lmaps/bq/d;)V
    .locals 11

    iget-object v0, p0, Lmaps/y/a;->h:Lmaps/t/ax;

    invoke-virtual {v0}, Lmaps/t/ax;->d()Lmaps/t/bx;

    move-result-object v4

    iget-object v0, p0, Lmaps/y/a;->h:Lmaps/t/ax;

    invoke-virtual {v0}, Lmaps/t/ax;->g()I

    move-result v5

    monitor-enter p0

    :try_start_0
    invoke-virtual {p2}, Lmaps/bq/d;->z()F

    move-result v0

    iget v1, p0, Lmaps/y/a;->p:F

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f000000

    mul-float/2addr v0, v1

    const/high16 v1, 0x40e00000

    div-float/2addr v0, v1

    const/high16 v1, 0x41000000

    mul-float v2, v0, v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {}, Lmaps/s/q;->a()Lmaps/s/q;

    move-result-object v0

    const/4 v6, 0x0

    const/high16 v7, 0x10000

    iget-object v8, p0, Lmaps/y/a;->k:Lmaps/al/o;

    iget-object v9, p0, Lmaps/y/a;->l:Lmaps/al/i;

    iget-object v10, p0, Lmaps/y/a;->m:Lmaps/al/q;

    move-object v1, p1

    move v3, v2

    invoke-virtual/range {v0 .. v10}, Lmaps/s/q;->a(Lmaps/t/cg;FFLmaps/t/bx;IIILmaps/al/l;Lmaps/al/b;Lmaps/al/c;)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private a(Lmaps/bq/d;)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v0, 0x0

    const/high16 v4, 0x3fa00000

    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lmaps/y/a;->r:Z

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/y/a;->r:Z

    monitor-exit p0

    :goto_0
    return v1

    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p1}, Lmaps/bq/d;->p()F

    move-result v2

    iget v3, p0, Lmaps/y/a;->j:F

    mul-float/2addr v3, v4

    cmpl-float v3, v2, v3

    if-gtz v3, :cond_1

    iget v3, p0, Lmaps/y/a;->j:F

    div-float/2addr v3, v4

    cmpg-float v2, v2, v3

    if-gez v2, :cond_2

    :cond_1
    move v0, v1

    :cond_2
    move v1, v0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private b(F)Lmaps/t/cg;
    .locals 1

    const/high16 v0, 0x41200000

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lmaps/y/a;->c:Lmaps/t/cg;

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lmaps/y/a;->h()V

    const/high16 v0, 0x40c00000

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lmaps/y/a;->d:Lmaps/t/cg;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lmaps/y/a;->e:Lmaps/t/cg;

    goto :goto_0
.end method

.method private b(Lmaps/bq/d;)Z
    .locals 4

    const/high16 v3, 0x40000000

    const/4 v0, 0x1

    iget-object v1, p0, Lmaps/y/a;->h:Lmaps/t/ax;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Lmaps/bq/d;->p()F

    move-result v1

    iget v2, p0, Lmaps/y/a;->i:F

    mul-float/2addr v2, v3

    cmpl-float v2, v1, v2

    if-gtz v2, :cond_0

    iget v2, p0, Lmaps/y/a;->i:F

    div-float/2addr v2, v3

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_0

    iget-object v1, p0, Lmaps/y/a;->h:Lmaps/t/ax;

    invoke-virtual {p1}, Lmaps/bq/d;->C()Lmaps/t/u;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/t/u;->c()Lmaps/t/an;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmaps/t/ax;->b(Lmaps/t/an;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Lmaps/bq/d;)V
    .locals 9

    const/high16 v5, 0x20000000

    iget-object v0, p0, Lmaps/y/a;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    invoke-virtual {p1}, Lmaps/bq/d;->s()F

    move-result v0

    invoke-direct {p0, v0}, Lmaps/y/a;->b(F)Lmaps/t/cg;

    move-result-object v4

    invoke-virtual {p1}, Lmaps/bq/d;->C()Lmaps/t/u;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/u;->b()Lmaps/t/ax;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/t/ax;->g()I

    move-result v0

    invoke-virtual {v2}, Lmaps/t/ax;->h()I

    move-result v1

    const v3, 0x71c71c7

    if-gt v0, v3, :cond_0

    if-le v1, v3, :cond_1

    :cond_0
    new-instance v1, Lmaps/t/bx;

    invoke-virtual {v2}, Lmaps/t/ax;->f()Lmaps/t/bx;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/bx;->f()I

    move-result v0

    sub-int/2addr v0, v5

    const/high16 v3, -0x40000000

    invoke-direct {v1, v0, v3}, Lmaps/t/bx;-><init>(II)V

    new-instance v0, Lmaps/t/bx;

    invoke-virtual {v2}, Lmaps/t/ax;->f()Lmaps/t/bx;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/t/bx;->f()I

    move-result v2

    add-int/2addr v2, v5

    add-int/lit8 v2, v2, -0x1

    const v3, 0x3fffffff

    invoke-direct {v0, v2, v3}, Lmaps/t/bx;-><init>(II)V

    move-object v2, v0

    move-object v3, v1

    :goto_0
    new-instance v0, Lmaps/t/ax;

    invoke-direct {v0, v3, v2}, Lmaps/t/ax;-><init>(Lmaps/t/bx;Lmaps/t/bx;)V

    iput-object v0, p0, Lmaps/y/a;->h:Lmaps/t/ax;

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {p1}, Lmaps/bq/d;->s()F

    move-result v0

    float-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    iget-boolean v1, p0, Lmaps/y/a;->t:Z

    invoke-static {v0, v1}, Lmaps/y/bk;->a(IZ)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v4, v0}, Lmaps/t/cg;->b(F)Lmaps/t/cg;

    move-result-object v0

    invoke-static {v0}, Lmaps/y/a;->a(Lmaps/t/cg;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/cg;

    new-instance v1, Lmaps/t/ai;

    iget-object v6, p0, Lmaps/y/a;->h:Lmaps/t/ax;

    invoke-direct {v1, v6}, Lmaps/t/ai;-><init>(Lmaps/t/an;)V

    invoke-virtual {v1, v0, v5}, Lmaps/t/ai;->a(Lmaps/t/cg;Ljava/util/List;)V

    iget-object v1, p0, Lmaps/y/a;->f:Ljava/util/List;

    invoke-interface {v1, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-interface {v5}, Ljava/util/List;->clear()V

    new-instance v1, Lmaps/t/ax;

    sget-object v6, Lmaps/y/a;->a:Lmaps/t/bx;

    invoke-virtual {v3, v6}, Lmaps/t/bx;->e(Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v6

    sget-object v7, Lmaps/y/a;->a:Lmaps/t/bx;

    invoke-virtual {v2, v7}, Lmaps/t/bx;->e(Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v7

    invoke-direct {v1, v6, v7}, Lmaps/t/ax;-><init>(Lmaps/t/bx;Lmaps/t/bx;)V

    new-instance v6, Lmaps/t/ai;

    invoke-direct {v6, v1}, Lmaps/t/ai;-><init>(Lmaps/t/an;)V

    invoke-virtual {v6, v0, v5}, Lmaps/t/ai;->a(Lmaps/t/cg;Ljava/util/List;)V

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/t/cg;

    iget-object v7, p0, Lmaps/y/a;->f:Ljava/util/List;

    sget-object v8, Lmaps/y/a;->b:Lmaps/t/bx;

    invoke-virtual {v1, v8}, Lmaps/t/cg;->b(Lmaps/t/bx;)Lmaps/t/cg;

    move-result-object v1

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_1
    new-instance v3, Lmaps/t/bx;

    mul-int/lit8 v0, v0, 0x4

    mul-int/lit8 v1, v1, 0x4

    invoke-direct {v3, v0, v1}, Lmaps/t/bx;-><init>(II)V

    invoke-virtual {v2}, Lmaps/t/ax;->d()Lmaps/t/bx;

    move-result-object v0

    invoke-virtual {v0, v3}, Lmaps/t/bx;->f(Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v1

    invoke-virtual {v2}, Lmaps/t/ax;->e()Lmaps/t/bx;

    move-result-object v0

    invoke-virtual {v0, v3}, Lmaps/t/bx;->e(Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v0

    move-object v2, v0

    move-object v3, v1

    goto/16 :goto_0

    :cond_2
    invoke-interface {v5}, Ljava/util/List;->clear()V

    new-instance v1, Lmaps/t/ax;

    sget-object v6, Lmaps/y/a;->b:Lmaps/t/bx;

    invoke-virtual {v3, v6}, Lmaps/t/bx;->e(Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v6

    sget-object v7, Lmaps/y/a;->b:Lmaps/t/bx;

    invoke-virtual {v2, v7}, Lmaps/t/bx;->e(Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v7

    invoke-direct {v1, v6, v7}, Lmaps/t/ax;-><init>(Lmaps/t/bx;Lmaps/t/bx;)V

    new-instance v6, Lmaps/t/ai;

    invoke-direct {v6, v1}, Lmaps/t/ai;-><init>(Lmaps/t/an;)V

    invoke-virtual {v6, v0, v5}, Lmaps/t/ai;->a(Lmaps/t/cg;Ljava/util/List;)V

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/cg;

    iget-object v6, p0, Lmaps/y/a;->f:Ljava/util/List;

    sget-object v7, Lmaps/y/a;->a:Lmaps/t/bx;

    invoke-virtual {v0, v7}, Lmaps/t/cg;->b(Lmaps/t/bx;)Lmaps/t/cg;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_3
    invoke-interface {v5}, Ljava/util/List;->clear()V

    goto/16 :goto_1

    :cond_4
    invoke-virtual {p1}, Lmaps/bq/d;->p()F

    move-result v0

    iput v0, p0, Lmaps/y/a;->i:F

    return-void
.end method

.method private e()Lmaps/p/z;
    .locals 2

    iget-object v0, p0, Lmaps/y/a;->g:Lmaps/t/bg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/a;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/a;->f:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/cg;

    invoke-virtual {v0}, Lmaps/t/cg;->b()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    invoke-static {}, Lmaps/b/r;->a()Lmaps/b/r;

    move-result-object v0

    iget-object v1, p0, Lmaps/y/a;->g:Lmaps/t/bg;

    invoke-virtual {v0, v1}, Lmaps/b/r;->e(Lmaps/t/bg;)Lmaps/b/z;

    move-result-object v0

    goto :goto_0
.end method

.method private h()V
    .locals 3

    iget-object v0, p0, Lmaps/y/a;->d:Lmaps/t/cg;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/y/a;->c:Lmaps/t/cg;

    const/16 v1, 0xa

    iget-boolean v2, p0, Lmaps/y/a;->t:Z

    invoke-static {v1, v2}, Lmaps/y/bk;->a(IZ)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lmaps/t/cg;->b(F)Lmaps/t/cg;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/a;->d:Lmaps/t/cg;

    iget-object v0, p0, Lmaps/y/a;->d:Lmaps/t/cg;

    const/4 v1, 0x6

    iget-boolean v2, p0, Lmaps/y/a;->t:Z

    invoke-static {v1, v2}, Lmaps/y/bk;->a(IZ)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lmaps/t/cg;->b(F)Lmaps/t/cg;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/a;->e:Lmaps/t/cg;

    :cond_0
    return-void
.end method


# virtual methods
.method public declared-synchronized a(F)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lmaps/y/a;->p:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/y/a;->r:Z

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/a;->o:Lmaps/y/y;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/a;->o:Lmaps/y/y;

    invoke-virtual {v0, p1}, Lmaps/y/y;->a(F)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 1

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {p3}, Lmaps/af/s;->b()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/y/a;->h:Lmaps/t/ax;

    if-nez v0, :cond_2

    invoke-direct {p0, p2}, Lmaps/y/a;->c(Lmaps/bq/d;)V

    :cond_2
    invoke-direct {p0, p2}, Lmaps/y/a;->a(Lmaps/bq/d;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0, p1, p2}, Lmaps/y/a;->a(Lmaps/cr/c;Lmaps/bq/d;)V

    :cond_3
    iget-object v0, p0, Lmaps/y/a;->k:Lmaps/al/o;

    invoke-virtual {v0}, Lmaps/al/o;->c()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    invoke-direct {p0, p1, p3, p2}, Lmaps/y/a;->a(Lmaps/cr/c;Lmaps/af/s;Lmaps/bq/d;)V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto :goto_0
.end method

.method public a(Lmaps/bq/d;Lmaps/cr/c;)Z
    .locals 5

    const/4 v4, 0x1

    invoke-direct {p0, p1}, Lmaps/y/a;->b(Lmaps/bq/d;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lmaps/y/a;->c(Lmaps/bq/d;)V

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lmaps/y/a;->r:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/y/a;->o:Lmaps/y/y;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/y/a;->o:Lmaps/y/y;

    iget-object v1, p0, Lmaps/y/a;->f:Ljava/util/List;

    iget-object v2, p0, Lmaps/y/a;->h:Lmaps/t/ax;

    iget-boolean v3, p0, Lmaps/y/a;->r:Z

    invoke-virtual {v0, p1, v1, v2, v3}, Lmaps/y/y;->a(Lmaps/bq/d;Ljava/util/List;Lmaps/t/ax;Z)V

    :cond_1
    return v4

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public declared-synchronized a_(I)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lmaps/y/a;->q:I

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/a;->o:Lmaps/y/y;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/a;->o:Lmaps/y/y;

    invoke-virtual {v0, p1}, Lmaps/y/y;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a_(Lmaps/cr/c;)V
    .locals 0

    invoke-virtual {p0, p1}, Lmaps/y/a;->c(Lmaps/cr/c;)V

    return-void
.end method

.method public b()Lmaps/y/am;
    .locals 1

    sget-object v0, Lmaps/y/am;->a:Lmaps/y/am;

    return-object v0
.end method

.method public b(Lmaps/cr/c;)V
    .locals 4

    new-instance v0, Lmaps/q/ac;

    sget-object v1, Lmaps/y/am;->a:Lmaps/y/am;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lmaps/q/ac;-><init>(Lmaps/y/am;Z)V

    iput-object v0, p0, Lmaps/y/a;->n:Lmaps/q/ac;

    iget-object v0, p0, Lmaps/y/a;->n:Lmaps/q/ac;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Colored Polyline"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/q/ac;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lmaps/y/a;->n:Lmaps/q/ac;

    invoke-virtual {p1}, Lmaps/cr/c;->a()Lmaps/s/i;

    move-result-object v1

    const/16 v2, 0x18

    invoke-virtual {v1, v2}, Lmaps/s/i;->a(I)Lmaps/cr/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/q/ac;->a(Lmaps/q/z;)V

    iget-object v0, p0, Lmaps/y/a;->n:Lmaps/q/ac;

    new-instance v1, Lmaps/q/n;

    const/4 v2, 0x1

    const/16 v3, 0x303

    invoke-direct {v1, v2, v3}, Lmaps/q/n;-><init>(II)V

    invoke-virtual {v0, v1}, Lmaps/q/ac;->a(Lmaps/q/z;)V

    iget-object v0, p0, Lmaps/y/a;->n:Lmaps/q/ac;

    new-instance v1, Lmaps/q/ar;

    const/4 v2, 0x2

    iget v3, p0, Lmaps/y/a;->q:I

    invoke-direct {v1, v2, v3}, Lmaps/q/ar;-><init>(II)V

    invoke-virtual {v0, v1}, Lmaps/q/ac;->a(Lmaps/q/z;)V

    new-instance v0, Lmaps/y/y;

    iget-object v1, p0, Lmaps/y/a;->n:Lmaps/q/ac;

    invoke-virtual {p1}, Lmaps/cr/c;->m()Lmaps/q/ad;

    move-result-object v2

    iget v3, p0, Lmaps/y/a;->s:I

    invoke-direct {v0, v1, v2, v3}, Lmaps/y/y;-><init>(Lmaps/q/ac;Lmaps/q/ad;I)V

    iput-object v0, p0, Lmaps/y/a;->o:Lmaps/y/y;

    iget-object v0, p0, Lmaps/y/a;->o:Lmaps/y/y;

    iget v1, p0, Lmaps/y/a;->q:I

    invoke-virtual {v0, v1}, Lmaps/y/y;->a(I)V

    iget-object v0, p0, Lmaps/y/a;->o:Lmaps/y/y;

    iget v1, p0, Lmaps/y/a;->p:F

    invoke-virtual {v0, v1}, Lmaps/y/y;->a(F)V

    invoke-virtual {p1}, Lmaps/cr/c;->m()Lmaps/q/ad;

    move-result-object v0

    iget-object v1, p0, Lmaps/y/a;->o:Lmaps/y/y;

    invoke-virtual {v0, v1}, Lmaps/q/ad;->a(Lmaps/q/x;)V

    return-void
.end method

.method public c(Lmaps/cr/c;)V
    .locals 3

    const/4 v2, 0x0

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/y/a;->n:Lmaps/q/ac;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lmaps/cr/c;->m()Lmaps/q/ad;

    move-result-object v0

    iget-object v1, p0, Lmaps/y/a;->n:Lmaps/q/ac;

    invoke-virtual {v0, v1}, Lmaps/q/ad;->b(Lmaps/q/ba;)V

    iget-object v1, p0, Lmaps/y/a;->o:Lmaps/y/y;

    invoke-virtual {v0, v1}, Lmaps/q/ad;->b(Lmaps/q/x;)V

    iput-object v2, p0, Lmaps/y/a;->n:Lmaps/q/ac;

    iput-object v2, p0, Lmaps/y/a;->o:Lmaps/y/y;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/y/a;->k:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->e(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/y/a;->l:Lmaps/al/i;

    invoke-virtual {v0, p1}, Lmaps/al/i;->c(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/y/a;->m:Lmaps/al/q;

    invoke-virtual {v0, p1}, Lmaps/al/q;->e(Lmaps/cr/c;)V

    goto :goto_0
.end method
