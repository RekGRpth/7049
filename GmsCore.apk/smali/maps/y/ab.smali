.class public Lmaps/y/ab;
.super Lmaps/y/bc;


# static fields
.field private static final M:Lmaps/cq/b;

.field private static final d:Lmaps/be/n;

.field private static final e:Lmaps/be/n;


# instance fields
.field private volatile A:Z

.field private final B:Ljava/util/Set;

.field private final C:Lmaps/be/i;

.field private final D:Lmaps/t/bx;

.field private E:Lmaps/bq/a;

.field private F:J

.field private G:J

.field private H:J

.field private I:Z

.field private final J:Z

.field private final K:Z

.field private final L:Lmaps/cq/b;

.field private N:I

.field private O:J

.field private final P:Ljava/util/Set;

.field private final Q:Lmaps/o/o;

.field private R:Ljava/lang/ref/WeakReference;

.field protected volatile a:Lmaps/af/f;

.field protected volatile b:Z

.field protected final c:Lmaps/ac/g;

.field private final f:I

.field private final g:I

.field private final h:Lmaps/y/am;

.field private i:Z

.field private final j:I

.field private final k:I

.field private final l:Z

.field private final m:Z

.field private final n:Z

.field private final o:Z

.field private final p:Lmaps/o/c;

.field private final q:Lmaps/bk/b;

.field private final r:Ljava/util/ArrayList;

.field private final s:Ljava/util/ArrayList;

.field private final t:[I

.field private final u:Ljava/util/ArrayList;

.field private final v:[I

.field private final w:Lmaps/y/aj;

.field private x:Lmaps/cq/a;

.field private y:Lmaps/bu/c;

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/4 v2, 0x4

    aput v2, v0, v1

    invoke-static {v0}, Lmaps/be/n;->a([I)Lmaps/be/n;

    move-result-object v0

    sput-object v0, Lmaps/y/ab;->d:Lmaps/be/n;

    sget-object v0, Lmaps/l/at;->a:Lmaps/be/n;

    sget-object v1, Lmaps/y/ab;->d:Lmaps/be/n;

    invoke-static {v0, v1}, Lmaps/be/n;->a(Lmaps/be/n;Lmaps/be/n;)Lmaps/be/n;

    move-result-object v0

    sput-object v0, Lmaps/y/ab;->e:Lmaps/be/n;

    new-instance v0, Lmaps/cq/c;

    invoke-direct {v0}, Lmaps/cq/c;-><init>()V

    sput-object v0, Lmaps/y/ab;->M:Lmaps/cq/b;

    return-void
.end method

.method constructor <init>(Lmaps/o/c;Lmaps/bk/b;Lmaps/cq/b;IIILmaps/y/am;IIZZZZZZ)V
    .locals 5

    invoke-direct {p0}, Lmaps/y/bc;-><init>()V

    new-instance v1, Lmaps/y/aj;

    invoke-direct {v1}, Lmaps/y/aj;-><init>()V

    iput-object v1, p0, Lmaps/y/ab;->w:Lmaps/y/aj;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-static {v1}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, Lmaps/y/ab;->B:Ljava/util/Set;

    new-instance v1, Lmaps/be/i;

    const/16 v2, 0x50

    invoke-direct {v1, v2}, Lmaps/be/i;-><init>(I)V

    iput-object v1, p0, Lmaps/y/ab;->C:Lmaps/be/i;

    new-instance v1, Lmaps/t/bx;

    invoke-direct {v1}, Lmaps/t/bx;-><init>()V

    iput-object v1, p0, Lmaps/y/ab;->D:Lmaps/t/bx;

    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lmaps/y/ab;->F:J

    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lmaps/y/ab;->G:J

    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lmaps/y/ab;->H:J

    const/4 v1, 0x0

    iput-boolean v1, p0, Lmaps/y/ab;->b:Z

    invoke-static {}, Lmaps/f/a;->c()Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, Lmaps/y/ab;->P:Ljava/util/Set;

    iput-object p1, p0, Lmaps/y/ab;->p:Lmaps/o/c;

    iput-object p2, p0, Lmaps/y/ab;->q:Lmaps/bk/b;

    iput-object p3, p0, Lmaps/y/ab;->L:Lmaps/cq/b;

    iput p4, p0, Lmaps/y/ab;->f:I

    iput p5, p0, Lmaps/y/ab;->g:I

    iput-object p7, p0, Lmaps/y/ab;->h:Lmaps/y/am;

    iput p8, p0, Lmaps/y/ab;->j:I

    iput p9, p0, Lmaps/y/ab;->k:I

    iput-boolean p10, p0, Lmaps/y/ab;->o:Z

    move/from16 v0, p11

    iput-boolean v0, p0, Lmaps/y/ab;->l:Z

    move/from16 v0, p12

    iput-boolean v0, p0, Lmaps/y/ab;->m:Z

    move/from16 v0, p13

    iput-boolean v0, p0, Lmaps/y/ab;->n:Z

    move/from16 v0, p14

    iput-boolean v0, p0, Lmaps/y/ab;->J:Z

    move/from16 v0, p15

    iput-boolean v0, p0, Lmaps/y/ab;->K:Z

    new-instance v1, Lmaps/o/o;

    invoke-direct {v1}, Lmaps/o/o;-><init>()V

    iput-object v1, p0, Lmaps/y/ab;->Q:Lmaps/o/o;

    iget-object v1, p0, Lmaps/y/ab;->q:Lmaps/bk/b;

    iget-object v2, p0, Lmaps/y/ab;->Q:Lmaps/o/o;

    invoke-virtual {v1, v2}, Lmaps/bk/b;->a(Lmaps/o/l;)V

    iget-boolean v1, p0, Lmaps/y/ab;->J:Z

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lmaps/o/c;->g()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lmaps/ac/g;

    invoke-virtual {p0}, Lmaps/y/ab;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lmaps/ac/g;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lmaps/y/ab;->c:Lmaps/ac/g;

    :goto_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmaps/y/ab;->s:Ljava/util/ArrayList;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmaps/y/ab;->r:Ljava/util/ArrayList;

    new-array v1, p4, [I

    iput-object v1, p0, Lmaps/y/ab;->t:[I

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p6}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lmaps/y/ab;->u:Ljava/util/ArrayList;

    const/4 v1, 0x0

    :goto_1
    if-ge v1, p6, :cond_1

    iget-object v2, p0, Lmaps/y/ab;->u:Ljava/util/ArrayList;

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lmaps/y/ab;->c:Lmaps/ac/g;

    goto :goto_0

    :cond_1
    add-int/lit8 v1, p6, 0x1

    new-array v1, v1, [I

    iput-object v1, p0, Lmaps/y/ab;->v:[I

    const/4 v1, 0x0

    iput v1, p0, Lmaps/y/ab;->N:I

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    const-wide/16 v3, 0xfa0

    add-long/2addr v1, v3

    iput-wide v1, p0, Lmaps/y/ab;->O:J

    iget-object v1, p0, Lmaps/y/ab;->q:Lmaps/bk/b;

    new-instance v2, Lmaps/y/o;

    invoke-direct {v2, p0}, Lmaps/y/o;-><init>(Lmaps/y/ab;)V

    invoke-virtual {v1, v2}, Lmaps/bk/b;->a(Lmaps/bk/g;)V

    return-void
.end method

.method public static a(Landroid/content/res/Resources;I)I
    .locals 3

    const v1, 0x64140

    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    mul-int/2addr v0, v2

    if-ge v0, v1, :cond_0

    move v0, v1

    :cond_0
    div-int/lit16 v2, p1, 0x100

    int-to-float v2, v2

    mul-float/2addr v2, v2

    mul-int/lit8 v0, v0, 0x18

    div-int/2addr v0, v1

    int-to-float v0, v0

    div-float/2addr v0, v2

    float-to-int v0, v0

    return v0
.end method

.method private a(Lmaps/bq/d;Lmaps/af/q;II)I
    .locals 3

    const/4 v0, 0x0

    sget-object v1, Lmaps/af/q;->f:Lmaps/af/q;

    if-eq p2, v1, :cond_1

    move v1, v0

    :goto_0
    if-ge p3, p4, :cond_0

    iget-object v2, p0, Lmaps/y/ab;->t:[I

    iget-object v0, p0, Lmaps/y/ab;->s:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/al;

    invoke-interface {v0, p1, p2}, Lmaps/l/al;->a(Lmaps/bq/d;Lmaps/af/q;)I

    move-result v0

    aput v0, v2, p3

    iget-object v0, p0, Lmaps/y/ab;->t:[I

    aget v0, v0, p3

    or-int/2addr v0, v1

    add-int/lit8 p3, p3, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmaps/y/ab;->p:Lmaps/o/c;

    invoke-virtual {v0, v1, p2}, Lmaps/o/c;->a(ILmaps/af/q;)I

    move-result v0

    :cond_1
    return v0
.end method

.method static synthetic a(Lmaps/y/ab;)Lmaps/bk/b;
    .locals 1

    iget-object v0, p0, Lmaps/y/ab;->q:Lmaps/bk/b;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/content/res/Resources;Lmaps/b/r;)Lmaps/y/ab;
    .locals 10

    const/16 v7, 0x100

    const/4 v5, 0x4

    invoke-static {p1, v7}, Lmaps/y/ab;->a(Landroid/content/res/Resources;I)I

    move-result v0

    mul-int/lit8 v3, v0, 0x2

    mul-int/lit8 v4, v3, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v2, Lmaps/bk/f;

    invoke-direct {v2, v5, v4, v1, v0}, Lmaps/bk/f;-><init>(IIZZ)V

    new-instance v1, Lmaps/bk/b;

    sget-object v0, Lmaps/o/c;->n:Lmaps/o/c;

    invoke-direct {v1, v0, v2}, Lmaps/bk/b;-><init>(Lmaps/o/c;Lmaps/bk/f;)V

    new-instance v0, Lmaps/y/i;

    sget-object v2, Lmaps/y/ab;->M:Lmaps/cq/b;

    sget-object v6, Lmaps/y/am;->g:Lmaps/y/am;

    move-object v8, p0

    move-object v9, p2

    invoke-direct/range {v0 .. v9}, Lmaps/y/i;-><init>(Lmaps/bk/b;Lmaps/cq/b;IIILmaps/y/am;ILandroid/content/Context;Lmaps/b/r;)V

    invoke-virtual {v0}, Lmaps/y/i;->d()V

    return-object v0
.end method

.method public static a(Lmaps/cq/b;Lmaps/o/c;Landroid/content/res/Resources;)Lmaps/y/ab;
    .locals 17

    const/16 v1, 0x100

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lmaps/y/ab;->a(Landroid/content/res/Resources;I)I

    move-result v5

    mul-int/lit8 v6, v5, 0x2

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    new-instance v4, Lmaps/bk/f;

    const/4 v3, 0x0

    invoke-direct {v4, v3, v6, v1, v12}, Lmaps/bk/f;-><init>(IIZZ)V

    new-instance v3, Lmaps/bk/b;

    new-instance v1, Lmaps/ca/e;

    sget-object v7, Lmaps/y/ab;->d:Lmaps/be/n;

    move-object/from16 v0, p1

    invoke-direct {v1, v0, v2, v7}, Lmaps/ca/e;-><init>(Lmaps/o/c;Ljava/util/Set;Lmaps/be/n;)V

    sget-object v2, Lmaps/y/ab;->d:Lmaps/be/n;

    move-object/from16 v0, p1

    invoke-direct {v3, v0, v1, v4, v2}, Lmaps/bk/b;-><init>(Lmaps/o/c;Lmaps/ca/e;Lmaps/bk/f;Lmaps/be/n;)V

    new-instance v1, Lmaps/y/m;

    const/4 v7, 0x0

    sget-object v8, Lmaps/y/am;->j:Lmaps/y/am;

    const/16 v9, 0x100

    const/16 v10, 0x100

    move-object/from16 v2, p1

    move-object/from16 v4, p0

    invoke-direct/range {v1 .. v16}, Lmaps/y/m;-><init>(Lmaps/o/c;Lmaps/bk/b;Lmaps/cq/b;IIILmaps/y/am;IIZZZZZZ)V

    return-object v1
.end method

.method public static a(Lmaps/o/c;Landroid/content/res/Resources;)Lmaps/y/ab;
    .locals 2

    const/4 v1, 0x1

    sget-object v0, Lmaps/o/c;->a:Lmaps/o/c;

    if-eq p0, v0, :cond_0

    sget-object v0, Lmaps/o/c;->b:Lmaps/o/c;

    if-eq p0, v0, :cond_0

    sget-object v0, Lmaps/o/c;->c:Lmaps/o/c;

    if-ne p0, v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    invoke-static {p0, v0, v1, p1}, Lmaps/y/ab;->a(Lmaps/o/c;ZZLandroid/content/res/Resources;)Lmaps/y/ab;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lmaps/o/c;ZZLandroid/content/res/Resources;)Lmaps/y/ab;
    .locals 17

    const/16 v1, 0x100

    move-object/from16 v0, p3

    invoke-static {v0, v1}, Lmaps/y/ab;->a(Landroid/content/res/Resources;I)I

    move-result v5

    mul-int/lit8 v6, v5, 0x2

    if-nez p1, :cond_0

    sget-boolean v1, Lmaps/ae/h;->O:Z

    if-eqz v1, :cond_2

    sget-object v1, Lmaps/o/c;->j:Lmaps/o/c;

    move-object/from16 v0, p0

    if-ne v0, v1, :cond_2

    :cond_0
    const/4 v13, 0x1

    :goto_0
    const/4 v11, 0x0

    const/4 v12, 0x0

    if-nez v13, :cond_1

    sget-object v1, Lmaps/o/c;->o:Lmaps/o/c;

    move-object/from16 v0, p0

    if-eq v0, v1, :cond_1

    sget-object v1, Lmaps/o/c;->p:Lmaps/o/c;

    move-object/from16 v0, p0

    if-ne v0, v1, :cond_3

    :cond_1
    const/16 v16, 0x1

    :goto_1
    const/4 v4, 0x0

    if-eqz p1, :cond_4

    sget-object v1, Lmaps/y/ab;->e:Lmaps/be/n;

    move-object v2, v1

    :goto_2
    if-eqz v13, :cond_5

    if-eqz v16, :cond_5

    const/4 v1, 0x1

    :goto_3
    new-instance v7, Lmaps/bk/f;

    const/16 v3, 0x8

    invoke-direct {v7, v3, v6, v1, v12}, Lmaps/bk/f;-><init>(IIZZ)V

    new-instance v3, Lmaps/bk/b;

    new-instance v1, Lmaps/ca/e;

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v4, v2}, Lmaps/ca/e;-><init>(Lmaps/o/c;Ljava/util/Set;Lmaps/be/n;)V

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v1, v7, v2}, Lmaps/bk/b;-><init>(Lmaps/o/c;Lmaps/ca/e;Lmaps/bk/f;Lmaps/be/n;)V

    new-instance v1, Lmaps/y/ab;

    new-instance v4, Lmaps/bu/e;

    invoke-direct {v4}, Lmaps/bu/e;-><init>()V

    const/16 v7, 0x8

    sget-object v8, Lmaps/y/am;->c:Lmaps/y/am;

    const/16 v9, 0x100

    const/16 v10, 0x100

    move-object/from16 v2, p0

    move v14, v13

    move/from16 v15, p2

    invoke-direct/range {v1 .. v16}, Lmaps/y/ab;-><init>(Lmaps/o/c;Lmaps/bk/b;Lmaps/cq/b;IIILmaps/y/am;IIZZZZZZ)V

    return-object v1

    :cond_2
    const/4 v13, 0x0

    goto :goto_0

    :cond_3
    const/16 v16, 0x0

    goto :goto_1

    :cond_4
    sget-object v1, Lmaps/be/n;->a:Lmaps/be/n;

    move-object v2, v1

    goto :goto_2

    :cond_5
    const/4 v1, 0x0

    goto :goto_3
.end method

.method public static a(Landroid/content/res/Resources;Lmaps/o/c;)Lmaps/y/az;
    .locals 13

    const/16 v7, 0x100

    const/4 v5, 0x0

    invoke-static {p0, v7}, Lmaps/y/ab;->a(Landroid/content/res/Resources;I)I

    move-result v3

    mul-int/lit8 v4, v3, 0x2

    const/4 v8, 0x1

    new-instance v0, Lmaps/bk/f;

    invoke-direct {v0, v5, v4, v5, v8}, Lmaps/bk/f;-><init>(IIZZ)V

    new-instance v1, Lmaps/bk/b;

    invoke-direct {v1, p1, v0}, Lmaps/bk/b;-><init>(Lmaps/o/c;Lmaps/bk/f;)V

    new-instance v0, Lmaps/y/az;

    sget-object v2, Lmaps/y/ab;->M:Lmaps/cq/b;

    sget-object v6, Lmaps/y/am;->f:Lmaps/y/am;

    move v9, v5

    move v10, v5

    move v11, v5

    move v12, v5

    invoke-direct/range {v0 .. v12}, Lmaps/y/az;-><init>(Lmaps/bk/b;Lmaps/cq/b;IIILmaps/y/am;IZZZZZ)V

    return-object v0
.end method

.method private a(Lmaps/bq/d;Ljava/util/Collection;ILjava/util/Set;)V
    .locals 8

    invoke-static {}, Lmaps/ca/a;->a()Lmaps/ca/a;

    move-result-object v4

    monitor-enter v4

    :try_start_0
    iget-object v0, p0, Lmaps/y/ab;->q:Lmaps/bk/b;

    invoke-virtual {v0}, Lmaps/bk/b;->c()V

    const/4 v0, 0x0

    move v3, v0

    :goto_0
    if-gt v3, p3, :cond_3

    if-ne v3, p3, :cond_4

    const/4 v0, 0x0

    move-object v2, v0

    :goto_1
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/ah;

    iget-object v1, p0, Lmaps/y/ab;->q:Lmaps/bk/b;

    invoke-virtual {v1, v0}, Lmaps/bk/b;->a(Lmaps/t/ah;)Lmaps/l/al;

    move-result-object v6

    if-eqz v6, :cond_5

    iget-object v1, p0, Lmaps/y/ab;->s:Ljava/util/ArrayList;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lmaps/y/ab;->R:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/cr/c;

    invoke-interface {v6, p1, v1}, Lmaps/l/al;->a(Lmaps/bq/d;Lmaps/cr/c;)V

    iget-object v1, p0, Lmaps/y/ab;->P:Ljava/util/Set;

    invoke-interface {v1, v6}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    invoke-interface {v6, v1}, Lmaps/l/al;->a(Z)V

    :cond_1
    iget-object v1, p0, Lmaps/y/ab;->v:[I

    aget v7, v1, v3

    add-int/lit8 v7, v7, 0x1

    aput v7, v1, v3

    iget-object v1, p0, Lmaps/y/ab;->s:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v7, p0, Lmaps/y/ab;->f:I

    if-ne v1, v7, :cond_5

    :cond_2
    iget-object v0, p0, Lmaps/y/ab;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lmaps/y/ab;->f:I

    if-eq v0, v1, :cond_3

    if-eqz v2, :cond_3

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v0

    if-nez v0, :cond_8

    :cond_3
    iget-object v0, p0, Lmaps/y/ab;->q:Lmaps/bk/b;

    invoke-virtual {v0}, Lmaps/bk/b;->d()V

    monitor-exit v4

    return-void

    :cond_4
    iget-object v0, p0, Lmaps/y/ab;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    move-object v2, v0

    goto :goto_1

    :cond_5
    if-eqz v6, :cond_6

    invoke-interface {v6}, Lmaps/l/al;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_6
    if-eqz v2, :cond_7

    iget-object v1, p0, Lmaps/y/ab;->x:Lmaps/cq/a;

    iget-object v6, p0, Lmaps/y/ab;->D:Lmaps/t/bx;

    invoke-interface {v1, v0, v6}, Lmaps/cq/a;->a(Lmaps/t/ah;Lmaps/t/bx;)Lmaps/t/ah;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_7
    if-nez v3, :cond_0

    invoke-interface {p4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_8
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move-object p2, v2

    goto/16 :goto_0
.end method

.method private a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/q;Lmaps/t/ah;)V
    .locals 7

    const/4 v6, 0x0

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/high16 v1, 0x40000000

    invoke-virtual {p4}, Lmaps/t/ah;->c()I

    move-result v2

    shr-int/2addr v1, v2

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    invoke-virtual {p4}, Lmaps/t/ah;->h()Lmaps/t/bx;

    move-result-object v2

    int-to-float v1, v1

    invoke-static {p1, p2, v2, v1}, Lmaps/af/d;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/t/bx;F)V

    iget-object v1, p1, Lmaps/cr/c;->f:Lmaps/al/o;

    invoke-virtual {v1, p1}, Lmaps/al/o;->d(Lmaps/cr/c;)V

    invoke-static {p3}, Lmaps/af/v;->a(Lmaps/af/q;)[I

    move-result-object v1

    aget v2, v1, v6

    const/4 v3, 0x1

    aget v3, v1, v3

    const/4 v4, 0x2

    aget v4, v1, v4

    const/4 v5, 0x3

    aget v1, v1, v5

    invoke-interface {v0, v2, v3, v4, v1}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    const/4 v1, 0x5

    const/4 v2, 0x4

    invoke-interface {v0, v1, v6, v2}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    return-void
.end method

.method public static b(Landroid/content/res/Resources;I)I
    .locals 4

    div-int/lit8 v0, p1, 0x2

    int-to-float v0, v0

    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v1, v1

    div-float/2addr v1, v0

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->floor(D)D

    move-result-wide v1

    double-to-int v1, v1

    add-int/lit8 v1, v1, 0x2

    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v2, v2

    div-float v0, v2, v0

    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v0, v2

    add-int/lit8 v0, v0, 0x2

    mul-int/2addr v0, v1

    return v0
.end method

.method private b(I)Ljava/lang/String;
    .locals 4

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "t="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lmaps/y/ab;->p:Lmaps/o/c;

    iget v3, v3, Lmaps/o/c;->z:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "n="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lmaps/bh/k;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lmaps/y/ab;)Lmaps/be/i;
    .locals 1

    iget-object v0, p0, Lmaps/y/ab;->C:Lmaps/be/i;

    return-object v0
.end method

.method public static b(Lmaps/o/c;Landroid/content/res/Resources;)Lmaps/y/ab;
    .locals 17

    const/16 v1, 0x100

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lmaps/y/ab;->b(Landroid/content/res/Resources;I)I

    move-result v5

    mul-int/lit8 v6, v5, 0x2

    const/16 v10, 0x180

    const/4 v11, 0x1

    const/4 v12, 0x1

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/4 v1, 0x0

    new-instance v2, Lmaps/bk/f;

    const/4 v3, 0x4

    invoke-direct {v2, v3, v6, v1, v12}, Lmaps/bk/f;-><init>(IIZZ)V

    new-instance v3, Lmaps/bk/b;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v2}, Lmaps/bk/b;-><init>(Lmaps/o/c;Lmaps/bk/f;)V

    new-instance v1, Lmaps/y/ab;

    sget-object v4, Lmaps/y/ab;->M:Lmaps/cq/b;

    const/4 v7, 0x4

    sget-object v8, Lmaps/y/am;->b:Lmaps/y/am;

    const/16 v9, 0x100

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v16}, Lmaps/y/ab;-><init>(Lmaps/o/c;Lmaps/bk/b;Lmaps/cq/b;IIILmaps/y/am;IIZZZZZZ)V

    return-object v1
.end method

.method static synthetic c(Lmaps/y/ab;)V
    .locals 0

    invoke-direct {p0}, Lmaps/y/ab;->e()V

    return-void
.end method

.method private e()V
    .locals 4

    iget v0, p0, Lmaps/y/ab;->N:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/y/ab;->N:I

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lmaps/y/ab;->O:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/16 v0, 0x6e

    const-string v1, "l"

    iget v2, p0, Lmaps/y/ab;->N:I

    invoke-direct {p0, v2}, Lmaps/y/ab;->b(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lmaps/bh/k;->a(ILjava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    iput v0, p0, Lmaps/y/ab;->N:I

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    const-wide/16 v2, 0xfa0

    add-long/2addr v0, v2

    iput-wide v0, p0, Lmaps/y/ab;->O:J

    :cond_0
    return-void
.end method

.method private h()Z
    .locals 3

    iget-object v0, p0, Lmaps/y/ab;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/al;

    iget-object v2, p0, Lmaps/y/ab;->q:Lmaps/bk/b;

    invoke-virtual {v2}, Lmaps/bk/b;->k()Lmaps/ae/d;

    move-result-object v2

    invoke-interface {v0, v2}, Lmaps/l/al;->a(Lmaps/ae/d;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lmaps/t/bx;)F
    .locals 1

    iget-object v0, p0, Lmaps/y/ab;->x:Lmaps/cq/a;

    if-nez v0, :cond_0

    const/high16 v0, 0x41a80000

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/y/ab;->x:Lmaps/cq/a;

    invoke-interface {v0, p1}, Lmaps/cq/a;->a(Lmaps/t/bx;)F

    move-result v0

    goto :goto_0
.end method

.method public a(Lmaps/t/u;Lmaps/p/ac;Ljava/util/Set;)I
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/y/ab;->s:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/al;

    if-eqz v0, :cond_2

    if-eqz p1, :cond_1

    invoke-interface {v0}, Lmaps/l/al;->b()Lmaps/t/ah;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/t/ah;->j()Lmaps/t/ax;

    move-result-object v3

    invoke-virtual {p1, v3}, Lmaps/t/u;->b(Lmaps/t/an;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    iget-object v3, p0, Lmaps/y/ab;->c:Lmaps/ac/g;

    invoke-interface {v0, v3}, Lmaps/l/al;->a(Lmaps/ac/g;)V

    invoke-interface {v0, p2}, Lmaps/l/al;->a(Lmaps/p/ac;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Lmaps/l/al;->b()Lmaps/t/ah;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/t/ah;->c()I

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    :cond_2
    instance-of v3, v0, Lmaps/l/at;

    if-eqz v3, :cond_0

    check-cast v0, Lmaps/l/at;

    invoke-virtual {v0, p3}, Lmaps/l/at;->a(Ljava/util/Set;)Z

    goto :goto_0

    :cond_3
    return v1
.end method

.method protected a(Lmaps/bq/d;)Ljava/util/Set;
    .locals 1

    invoke-static {}, Lmaps/f/bd;->b()Lmaps/f/bd;

    move-result-object v0

    return-object v0
.end method

.method public a(I)V
    .locals 1

    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lmaps/y/ab;->z:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/util/Set;Ljava/util/Map;)V
    .locals 1

    iget-object v0, p0, Lmaps/y/ab;->c:Lmaps/ac/g;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(Lmaps/bq/a;)V
    .locals 0

    iput-object p1, p0, Lmaps/y/ab;->E:Lmaps/bq/a;

    return-void
.end method

.method public a(Lmaps/bq/d;Lmaps/af/q;Ljava/util/HashSet;Ljava/util/HashSet;[I)V
    .locals 5

    const/4 v4, 0x0

    const/4 v2, -0x1

    iget-object v0, p0, Lmaps/y/ab;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {p0, p1, p2, v0, v4}, Lmaps/y/ab;->a(Lmaps/bq/d;Lmaps/af/q;II)I

    iget-object v0, p0, Lmaps/y/ab;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/al;

    invoke-interface {v0, p1, v2, p3}, Lmaps/l/al;->a(Lmaps/bq/d;ILjava/util/Collection;)V

    invoke-interface {v0, p1, p4}, Lmaps/l/al;->a(Lmaps/bq/d;Ljava/util/Collection;)V

    invoke-interface {v0}, Lmaps/l/al;->e()I

    move-result v0

    if-le v0, v1, :cond_2

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_0
    if-eqz p5, :cond_1

    array-length v0, p5

    if-lez v0, :cond_1

    aput v1, p5, v4

    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method protected a(Lmaps/cq/a;)V
    .locals 2

    iput-object p1, p0, Lmaps/y/ab;->x:Lmaps/cq/a;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lmaps/y/ab;->G:J

    return-void
.end method

.method public a(Lmaps/cr/c;Lmaps/af/f;)V
    .locals 5

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lmaps/y/ab;->R:Ljava/lang/ref/WeakReference;

    iget-object v0, p0, Lmaps/y/ab;->q:Lmaps/bk/b;

    invoke-virtual {v0, p1}, Lmaps/bk/b;->a(Lmaps/cr/c;)V

    iput-object p2, p0, Lmaps/y/ab;->a:Lmaps/af/f;

    iget-object v0, p0, Lmaps/y/ab;->x:Lmaps/cq/a;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/y/ab;->L:Lmaps/cq/b;

    iget-object v1, p0, Lmaps/y/ab;->p:Lmaps/o/c;

    iget v2, p0, Lmaps/y/ab;->k:I

    iget-boolean v3, p0, Lmaps/y/ab;->o:Z

    iget-object v4, p0, Lmaps/y/ab;->Q:Lmaps/o/o;

    invoke-interface {v0, v1, v2, v3, v4}, Lmaps/cq/b;->a(Lmaps/o/c;IZLmaps/o/l;)Lmaps/cq/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmaps/y/ab;->a(Lmaps/cq/a;)V

    iget-object v0, p0, Lmaps/y/ab;->L:Lmaps/cq/b;

    iget-object v1, p0, Lmaps/y/ab;->p:Lmaps/o/c;

    iget-boolean v2, p0, Lmaps/y/ab;->o:Z

    iget-object v3, p0, Lmaps/y/ab;->Q:Lmaps/o/o;

    invoke-interface {v0, v1, v2, v3}, Lmaps/cq/b;->a(Lmaps/o/c;ZLmaps/o/l;)Lmaps/bu/c;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/ab;->y:Lmaps/bu/c;

    iget-object v0, p0, Lmaps/y/ab;->y:Lmaps/bu/c;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/y/ab;->x:Lmaps/cq/a;

    instance-of v0, v0, Lmaps/bu/c;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/y/ab;->x:Lmaps/cq/a;

    check-cast v0, Lmaps/bu/c;

    iput-object v0, p0, Lmaps/y/ab;->y:Lmaps/bu/c;

    :cond_0
    return-void

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bad out-of-bounds coord generator"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 22

    invoke-interface/range {p3 .. p3}, Lmaps/af/s;->b()I

    move-result v3

    if-lez v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface/range {p3 .. p3}, Lmaps/af/s;->a()Lmaps/af/q;

    move-result-object v15

    invoke-interface/range {p3 .. p3}, Lmaps/af/s;->c()Lmaps/p/av;

    move-result-object v16

    new-instance v17, Lmaps/af/t;

    move-object/from16 v0, v17

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Lmaps/af/t;-><init>(Lmaps/af/s;)V

    invoke-virtual/range {v16 .. v16}, Lmaps/p/av;->f()Z

    move-result v3

    if-eqz v3, :cond_6

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lmaps/y/ab;->A:Z

    if-nez v3, :cond_2

    invoke-direct/range {p0 .. p0}, Lmaps/y/ab;->h()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lmaps/y/ab;->A:Z

    :cond_2
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lmaps/y/ab;->A:Z

    if-eqz v3, :cond_3

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lmaps/y/ab;->z:Z

    if-nez v3, :cond_3

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    invoke-virtual {v0, v1, v2}, Lmaps/y/ab;->a(Lmaps/bq/d;Lmaps/cr/c;)Z

    :cond_3
    sget-boolean v3, Lmaps/ae/h;->F:Z

    if-nez v3, :cond_5

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/y/ab;->r:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_5

    invoke-virtual/range {p1 .. p1}, Lmaps/cr/c;->D()V

    const/4 v3, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Lmaps/af/t;->a(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/y/ab;->r:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmaps/l/al;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v17

    invoke-interface {v3, v0, v1, v2}, Lmaps/l/al;->b(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/y/ab;->r:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmaps/l/al;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-interface {v3, v0, v1, v2}, Lmaps/l/al;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    goto :goto_1

    :cond_4
    invoke-virtual/range {p1 .. p1}, Lmaps/cr/c;->F()V

    :cond_5
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lmaps/y/ab;->I:Z

    if-eqz v3, :cond_6

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/y/ab;->q:Lmaps/bk/b;

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/y/ab;->s:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Lmaps/bk/b;->a(Ljava/util/List;)V

    :cond_6
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lmaps/y/ab;->m:Z

    if-eqz v3, :cond_b

    sget-object v3, Lmaps/af/q;->a:Lmaps/af/q;

    if-eq v15, v3, :cond_7

    sget-object v3, Lmaps/af/q;->c:Lmaps/af/q;

    if-ne v15, v3, :cond_b

    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/y/ab;->r:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_8

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/y/ab;->v:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/y/ab;->s:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v3, v4, :cond_b

    :cond_8
    invoke-virtual/range {v16 .. v16}, Lmaps/p/av;->c()Z

    move-result v3

    if-eqz v3, :cond_b

    const/4 v3, 0x1

    move v5, v3

    :goto_2
    invoke-virtual/range {p1 .. p1}, Lmaps/cr/c;->M()I

    move-result v3

    if-lez v3, :cond_c

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/y/ab;->p:Lmaps/o/c;

    invoke-virtual {v3}, Lmaps/o/c;->g()Z

    move-result v3

    if-eqz v3, :cond_c

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lmaps/y/ab;->K:Z

    if-eqz v3, :cond_c

    invoke-virtual/range {p1 .. p1}, Lmaps/cr/c;->N()Z

    move-result v3

    if-eqz v3, :cond_c

    sget-boolean v3, Lmaps/ae/h;->F:Z

    if-nez v3, :cond_c

    const/4 v3, 0x1

    move v14, v3

    :goto_3
    if-eqz v14, :cond_9

    invoke-virtual/range {p1 .. p1}, Lmaps/cr/c;->z()V

    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/y/ab;->s:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v9

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/y/ab;->u:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v7, 0x0

    invoke-virtual/range {v16 .. v16}, Lmaps/p/av;->d()Z

    move-result v18

    move v13, v3

    :goto_4
    if-ltz v13, :cond_19

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/y/ab;->v:[I

    aget v3, v3, v13

    if-lez v3, :cond_1e

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/y/ab;->v:[I

    aget v3, v3, v13

    sub-int v8, v9, v3

    if-eqz v5, :cond_d

    sget-boolean v3, Lmaps/ae/h;->F:Z

    if-nez v3, :cond_d

    move v4, v8

    :goto_5
    if-ge v4, v9, :cond_d

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/y/ab;->s:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmaps/l/al;

    invoke-interface {v3}, Lmaps/l/al;->c()Z

    move-result v6

    if-nez v6, :cond_a

    invoke-interface {v3}, Lmaps/l/al;->b()Lmaps/t/ah;

    move-result-object v6

    invoke-virtual {v6}, Lmaps/t/ah;->c()I

    move-result v6

    const/16 v10, 0xe

    if-lt v6, v10, :cond_a

    invoke-interface {v3}, Lmaps/l/al;->b()Lmaps/t/ah;

    move-result-object v3

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v15, v3}, Lmaps/y/ab;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/q;Lmaps/t/ah;)V

    :cond_a
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_5

    :cond_b
    const/4 v3, 0x0

    move v5, v3

    goto/16 :goto_2

    :cond_c
    const/4 v3, 0x0

    move v14, v3

    goto :goto_3

    :cond_d
    if-eqz v14, :cond_f

    sget-boolean v3, Lmaps/ae/h;->F:Z

    if-nez v3, :cond_f

    invoke-virtual/range {p1 .. p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v3

    const/16 v4, 0x1e01

    const/16 v6, 0x1e01

    const/16 v10, 0x1e01

    invoke-interface {v3, v4, v6, v10}, Ljavax/microedition/khronos/opengles/GL10;->glStencilOp(III)V

    invoke-virtual/range {p1 .. p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v3

    const/16 v4, 0x7f

    invoke-interface {v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glStencilMask(I)V

    move v4, v8

    :goto_6
    if-ge v4, v9, :cond_e

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/y/ab;->s:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmaps/l/al;

    invoke-virtual/range {p1 .. p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v6

    const/16 v10, 0x200

    add-int/lit8 v11, v4, 0x1

    const/16 v12, 0x7f

    invoke-interface {v6, v10, v11, v12}, Ljavax/microedition/khronos/opengles/GL10;->glStencilFunc(III)V

    invoke-virtual/range {p1 .. p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v6

    invoke-interface {v6}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    invoke-interface {v3}, Lmaps/l/al;->b()Lmaps/t/ah;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/t/ah;->j()Lmaps/t/ax;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/t/ax;->d()Lmaps/t/bx;

    move-result-object v6

    invoke-virtual {v3}, Lmaps/t/ax;->g()I

    move-result v3

    int-to-float v3, v3

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v0, v1, v6, v3}, Lmaps/af/d;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/t/bx;F)V

    invoke-static/range {p1 .. p2}, Lmaps/l/aa;->a(Lmaps/cr/c;Lmaps/bq/d;)V

    invoke-virtual/range {p1 .. p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v3

    invoke-interface {v3}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_6

    :cond_e
    invoke-virtual/range {p1 .. p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v3

    const/16 v4, 0x1e00

    const/16 v6, 0x1e00

    const/16 v10, 0x1e00

    invoke-interface {v3, v4, v6, v10}, Ljavax/microedition/khronos/opengles/GL10;->glStencilOp(III)V

    :cond_f
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v15, v8, v9}, Lmaps/y/ab;->a(Lmaps/bq/d;Lmaps/af/q;II)I

    move-result v4

    const/4 v3, 0x0

    move v11, v3

    move v12, v4

    :goto_7
    if-eqz v12, :cond_18

    and-int/lit8 v3, v12, 0x1

    if-eqz v3, :cond_17

    invoke-virtual/range {p1 .. p1}, Lmaps/cr/c;->D()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Lmaps/af/t;->a(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/y/ab;->s:Ljava/util/ArrayList;

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmaps/l/al;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v17

    invoke-interface {v3, v0, v1, v2}, Lmaps/l/al;->b(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    const/4 v3, 0x1

    shl-int v19, v3, v11

    move v10, v8

    :goto_8
    if-ge v10, v9, :cond_16

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/y/ab;->s:Ljava/util/ArrayList;

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmaps/l/al;

    invoke-interface {v3}, Lmaps/l/al;->b()Lmaps/t/ah;

    move-result-object v4

    invoke-virtual {v4}, Lmaps/t/ah;->l()Lmaps/t/al;

    move-result-object v6

    const/4 v4, 0x0

    sget-boolean v20, Lmaps/ae/h;->F:Z

    if-nez v20, :cond_1d

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/y/ab;->p:Lmaps/o/c;

    move-object/from16 v20, v0

    sget-object v21, Lmaps/o/c;->n:Lmaps/o/c;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-ne v0, v1, :cond_1d

    if-eqz v6, :cond_1d

    sget-object v4, Lmaps/t/cm;->d:Lmaps/t/cm;

    invoke-virtual {v6, v4}, Lmaps/t/al;->a(Lmaps/t/cm;)Lmaps/t/ao;

    move-result-object v4

    check-cast v4, Lmaps/t/cu;

    if-nez v4, :cond_10

    const/4 v4, 0x0

    :goto_9
    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Lmaps/p/av;->a(Lmaps/t/v;)Lmaps/p/z;

    move-result-object v4

    if-nez v4, :cond_11

    move v3, v7

    :goto_a
    add-int/lit8 v4, v10, 0x1

    move v10, v4

    move v7, v3

    goto :goto_8

    :cond_10
    invoke-virtual {v4}, Lmaps/t/cu;->b()Lmaps/t/bg;

    move-result-object v4

    goto :goto_9

    :cond_11
    move-object v6, v4

    :goto_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/y/ab;->t:[I

    aget v4, v4, v10

    and-int v4, v4, v19

    if-eqz v4, :cond_1c

    if-eqz v14, :cond_12

    sget-boolean v4, Lmaps/ae/h;->F:Z

    if-nez v4, :cond_12

    invoke-virtual/range {p1 .. p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v4

    const/16 v7, 0x202

    add-int/lit8 v20, v10, 0x1

    const/16 v21, 0x7f

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-interface {v4, v7, v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glStencilFunc(III)V

    :cond_12
    invoke-virtual/range {p1 .. p1}, Lmaps/cr/c;->D()V

    invoke-interface {v3}, Lmaps/l/al;->b()Lmaps/t/ah;

    move-result-object v4

    invoke-virtual {v4}, Lmaps/t/ah;->j()Lmaps/t/ax;

    move-result-object v4

    invoke-virtual {v4}, Lmaps/t/ax;->d()Lmaps/t/bx;

    move-result-object v4

    if-eqz v6, :cond_13

    sget-boolean v7, Lmaps/ae/h;->F:Z

    if-nez v7, :cond_13

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v17

    invoke-interface {v6, v0, v1, v2, v4}, Lmaps/p/z;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;Lmaps/t/bx;)V

    :cond_13
    sget-boolean v4, Lmaps/ae/h;->Q:Z

    if-eqz v4, :cond_14

    invoke-virtual/range {p1 .. p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v4

    instance-of v4, v4, Lmaps/j/d;

    if-eqz v4, :cond_14

    invoke-virtual/range {p1 .. p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v4

    check-cast v4, Lmaps/j/d;

    invoke-interface {v3}, Lmaps/l/al;->b()Lmaps/t/ah;

    move-result-object v7

    invoke-virtual {v4, v7}, Lmaps/j/d;->a(Lmaps/t/ah;)V

    :cond_14
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v17

    invoke-interface {v3, v0, v1, v2}, Lmaps/l/al;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    if-eqz v6, :cond_15

    sget-boolean v3, Lmaps/ae/h;->F:Z

    if-nez v3, :cond_15

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-interface {v6, v0, v1}, Lmaps/p/z;->a(Lmaps/cr/c;Lmaps/af/s;)V

    :cond_15
    const/4 v3, 0x1

    invoke-virtual/range {p1 .. p1}, Lmaps/cr/c;->F()V

    goto :goto_a

    :cond_16
    invoke-virtual/range {p1 .. p1}, Lmaps/cr/c;->F()V

    sget-boolean v3, Lmaps/ae/h;->Q:Z

    if-eqz v3, :cond_17

    invoke-virtual/range {p1 .. p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v3

    instance-of v3, v3, Lmaps/j/d;

    if-eqz v3, :cond_17

    invoke-virtual/range {p1 .. p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v3

    check-cast v3, Lmaps/j/d;

    sget-object v4, Lmaps/j/d;->f:Lmaps/t/ah;

    invoke-virtual {v3, v4}, Lmaps/j/d;->a(Lmaps/t/ah;)V

    :cond_17
    add-int/lit8 v3, v11, 0x1

    ushr-int/lit8 v4, v12, 0x1

    move v11, v3

    move v12, v4

    goto/16 :goto_7

    :cond_18
    if-eqz v18, :cond_1b

    if-eqz v7, :cond_1b

    :cond_19
    if-eqz v14, :cond_1a

    sget-boolean v3, Lmaps/ae/h;->F:Z

    if-nez v3, :cond_1a

    invoke-virtual/range {p1 .. p1}, Lmaps/cr/c;->A()V

    :cond_1a
    invoke-virtual/range {v16 .. v16}, Lmaps/p/av;->g()Z

    move-result v3

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lmaps/y/ab;->I:Z

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/y/ab;->q:Lmaps/bk/b;

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/y/ab;->s:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Lmaps/bk/b;->b(Ljava/util/List;)V

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lmaps/y/ab;->I:Z

    goto/16 :goto_0

    :cond_1b
    move v3, v8

    :goto_c
    add-int/lit8 v4, v13, -0x1

    move v13, v4

    move v9, v3

    goto/16 :goto_4

    :cond_1c
    move v3, v7

    goto/16 :goto_a

    :cond_1d
    move-object v6, v4

    goto/16 :goto_b

    :cond_1e
    move v3, v9

    goto :goto_c
.end method

.method public a(Lmaps/o/c;)V
    .locals 1

    iget-object v0, p0, Lmaps/y/ab;->q:Lmaps/bk/b;

    invoke-virtual {v0, p1}, Lmaps/bk/b;->a(Lmaps/o/c;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/y/ab;->A:Z

    return-void
.end method

.method public a(Lmaps/y/h;)V
    .locals 1

    iget-object v0, p0, Lmaps/y/ab;->B:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/y/ab;->q:Lmaps/bk/b;

    invoke-virtual {v0, p1}, Lmaps/bk/b;->a(Z)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/y/ab;->A:Z

    return-void
.end method

.method public a(Lmaps/bq/d;Lmaps/cr/c;)Z
    .locals 11

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/y/ab;->b:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/y/ab;->I:Z

    iget-object v0, p0, Lmaps/y/ab;->D:Lmaps/t/bx;

    invoke-virtual {p1, v0}, Lmaps/bq/d;->a(Lmaps/t/bx;)V

    iget-object v0, p0, Lmaps/y/ab;->x:Lmaps/cq/a;

    invoke-interface {v0, p1}, Lmaps/cq/a;->a(Lmaps/bq/d;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lmaps/y/ab;->w:Lmaps/y/aj;

    invoke-virtual {p1}, Lmaps/bq/d;->i()Lmaps/t/bx;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/y/aj;->a(Lmaps/t/bx;)V

    iget-object v0, p0, Lmaps/y/ab;->w:Lmaps/y/aj;

    invoke-static {v7, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_0
    iget v0, p0, Lmaps/y/ab;->j:I

    int-to-float v0, v0

    invoke-virtual {p1}, Lmaps/bq/d;->n()F

    move-result v1

    mul-float/2addr v0, v1

    float-to-int v8, v0

    iget-object v0, p0, Lmaps/y/ab;->P:Ljava/util/Set;

    iget-object v1, p0, Lmaps/y/ab;->s:Ljava/util/ArrayList;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lmaps/y/ab;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lmaps/y/ab;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lmaps/y/ab;->v:[I

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    iget-object v0, p0, Lmaps/y/ab;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v9

    invoke-static {}, Lmaps/f/a;->a()Ljava/util/HashSet;

    move-result-object v10

    iget-object v0, p0, Lmaps/y/ab;->q:Lmaps/bk/b;

    invoke-virtual {v0}, Lmaps/bk/b;->h()Z

    move-result v0

    iget-boolean v1, p0, Lmaps/y/ab;->z:Z

    if-eqz v1, :cond_3

    iget-object v0, p0, Lmaps/y/ab;->E:Lmaps/bq/a;

    const/4 v1, 0x0

    iput-object v1, p0, Lmaps/y/ab;->E:Lmaps/bq/a;

    if-eqz v0, :cond_2

    new-instance v3, Lmaps/bq/d;

    invoke-virtual {p1}, Lmaps/bq/d;->l()I

    move-result v1

    invoke-virtual {p1}, Lmaps/bq/d;->l()I

    move-result v2

    invoke-virtual {p1}, Lmaps/bq/d;->n()F

    move-result v4

    invoke-direct {v3, v0, v1, v2, v4}, Lmaps/bq/d;-><init>(Lmaps/bq/a;IIF)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget-object v0, p0, Lmaps/y/ab;->q:Lmaps/bk/b;

    iget-object v1, p0, Lmaps/y/ab;->x:Lmaps/cq/a;

    new-instance v2, Lmaps/t/bx;

    invoke-virtual {v3}, Lmaps/bq/d;->h()Lmaps/t/bx;

    move-result-object v6

    invoke-direct {v2, v6}, Lmaps/t/bx;-><init>(Lmaps/t/bx;)V

    iget-object v6, p0, Lmaps/y/ab;->x:Lmaps/cq/a;

    invoke-interface {v6, v3}, Lmaps/cq/a;->a(Lmaps/bq/d;)Ljava/util/List;

    move-result-object v3

    iget-boolean v6, p0, Lmaps/y/ab;->z:Z

    invoke-virtual/range {v0 .. v6}, Lmaps/bk/b;->a(Lmaps/cq/a;Lmaps/t/bx;Ljava/util/List;Ljava/util/Set;Ljava/util/Set;Z)V

    :cond_1
    :goto_0
    iget-object v0, p0, Lmaps/y/ab;->q:Lmaps/bk/b;

    invoke-virtual {v0}, Lmaps/bk/b;->b()I

    move-result v1

    invoke-direct {p0, p1, v7, v9, v10}, Lmaps/y/ab;->a(Lmaps/bq/d;Ljava/util/Collection;ILjava/util/Set;)V

    iget-object v0, p0, Lmaps/y/ab;->v:[I

    const/4 v2, 0x0

    aget v0, v0, v2

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v2

    if-ne v0, v2, :cond_5

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lmaps/y/ab;->b:Z

    iget-boolean v0, p0, Lmaps/y/ab;->i:Z

    if-eqz v0, :cond_7

    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/ah;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lmaps/bq/d;->s()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-virtual {v0}, Lmaps/t/ah;->c()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    iget-object v4, p0, Lmaps/y/ab;->r:Ljava/util/ArrayList;

    new-instance v5, Lmaps/l/an;

    shl-int v3, v8, v3

    invoke-direct {v5, v0, v3}, Lmaps/l/an;-><init>(Lmaps/t/ah;I)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lmaps/y/ab;->q:Lmaps/bk/b;

    invoke-virtual {v0}, Lmaps/bk/b;->e()V

    goto :goto_0

    :cond_3
    iget-wide v1, p0, Lmaps/y/ab;->F:J

    invoke-virtual {p1}, Lmaps/bq/d;->g()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_4

    iget-wide v1, p0, Lmaps/y/ab;->G:J

    iget-object v3, p0, Lmaps/y/ab;->x:Lmaps/cq/a;

    invoke-interface {v3}, Lmaps/cq/a;->a()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_4

    iget-wide v1, p0, Lmaps/y/ab;->H:J

    iget-object v3, p0, Lmaps/y/ab;->Q:Lmaps/o/o;

    invoke-virtual {v3}, Lmaps/o/o;->b()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_4

    iget-boolean v1, p0, Lmaps/y/ab;->A:Z

    if-nez v1, :cond_4

    if-eqz v0, :cond_1

    :cond_4
    invoke-virtual {p0, p1}, Lmaps/y/ab;->a(Lmaps/bq/d;)Ljava/util/Set;

    move-result-object v4

    const/4 v5, 0x0

    iget-object v0, p0, Lmaps/y/ab;->q:Lmaps/bk/b;

    iget-object v1, p0, Lmaps/y/ab;->x:Lmaps/cq/a;

    new-instance v2, Lmaps/t/bx;

    invoke-virtual {p1}, Lmaps/bq/d;->h()Lmaps/t/bx;

    move-result-object v3

    invoke-direct {v2, v3}, Lmaps/t/bx;-><init>(Lmaps/t/bx;)V

    iget-boolean v6, p0, Lmaps/y/ab;->z:Z

    move-object v3, v7

    invoke-virtual/range {v0 .. v6}, Lmaps/bk/b;->a(Lmaps/cq/a;Lmaps/t/bx;Ljava/util/List;Ljava/util/Set;Ljava/util/Set;Z)V

    invoke-virtual {p1}, Lmaps/bq/d;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lmaps/y/ab;->F:J

    iget-object v0, p0, Lmaps/y/ab;->x:Lmaps/cq/a;

    invoke-interface {v0}, Lmaps/cq/a;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lmaps/y/ab;->G:J

    iget-object v0, p0, Lmaps/y/ab;->Q:Lmaps/o/o;

    invoke-virtual {v0}, Lmaps/o/o;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lmaps/y/ab;->H:J

    goto/16 :goto_0

    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_6
    iget-object v0, p0, Lmaps/y/ab;->y:Lmaps/bu/c;

    invoke-virtual {v0, p1}, Lmaps/bu/c;->b(Lmaps/bq/d;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/ah;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lmaps/bq/d;->s()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-virtual {v0}, Lmaps/t/ah;->c()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    iget-object v4, p0, Lmaps/y/ab;->r:Ljava/util/ArrayList;

    new-instance v5, Lmaps/l/an;

    shl-int v3, v8, v3

    invoke-direct {v5, v0, v3}, Lmaps/l/an;-><init>(Lmaps/t/ah;I)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_7
    iget-boolean v0, p0, Lmaps/y/ab;->z:Z

    iput-boolean v0, p0, Lmaps/y/ab;->A:Z

    iget-object v0, p0, Lmaps/y/ab;->q:Lmaps/bk/b;

    invoke-virtual {v0}, Lmaps/bk/b;->b()I

    move-result v0

    sub-int/2addr v0, v1

    iget-boolean v1, p0, Lmaps/y/ab;->A:Z

    if-nez v1, :cond_a

    if-nez v0, :cond_a

    iget-object v1, p0, Lmaps/y/ab;->B:Ljava/util/Set;

    monitor-enter v1

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lmaps/y/ab;->B:Ljava/util/Set;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_8
    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/y/h;

    iget-object v1, p0, Lmaps/y/ab;->s:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_9

    const/4 v1, 0x1

    :goto_5
    invoke-interface {v0, v1}, Lmaps/y/h;->a(Z)Z

    move-result v1

    if-nez v1, :cond_8

    iget-object v1, p0, Lmaps/y/ab;->B:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_4

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_9
    const/4 v1, 0x0

    goto :goto_5

    :cond_a
    iget-object v0, p0, Lmaps/y/ab;->P:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/al;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Lmaps/l/al;->a(Z)V

    goto :goto_6

    :cond_b
    iget-object v0, p0, Lmaps/y/ab;->P:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    const/4 v0, 0x1

    return v0
.end method

.method public a(Lmaps/t/ao;)Z
    .locals 1

    iget-object v0, p0, Lmaps/y/ab;->Q:Lmaps/o/o;

    invoke-virtual {v0, p1}, Lmaps/o/o;->a(Lmaps/t/ao;)Z

    move-result v0

    return v0
.end method

.method public a_(Lmaps/cr/c;)V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/y/ab;->R:Ljava/lang/ref/WeakReference;

    iput-object v0, p0, Lmaps/y/ab;->a:Lmaps/af/f;

    iget-object v0, p0, Lmaps/y/ab;->q:Lmaps/bk/b;

    invoke-virtual {v0}, Lmaps/bk/b;->i()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/y/ab;->A:Z

    return-void
.end method

.method public b()Lmaps/y/am;
    .locals 1

    iget-object v0, p0, Lmaps/y/ab;->h:Lmaps/y/am;

    return-object v0
.end method

.method public b(Z)V
    .locals 0

    iput-boolean p1, p0, Lmaps/y/ab;->i:Z

    return-void
.end method

.method protected b_()Lmaps/p/j;
    .locals 1

    sget-object v0, Lmaps/p/j;->a:Lmaps/p/j;

    return-object v0
.end method

.method public e_()V
    .locals 1

    iget-object v0, p0, Lmaps/y/ab;->q:Lmaps/bk/b;

    invoke-virtual {v0}, Lmaps/bk/b;->g()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/y/ab;->A:Z

    return-void
.end method

.method public k()V
    .locals 3

    iget-object v0, p0, Lmaps/y/ab;->q:Lmaps/bk/b;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lmaps/bk/b;->a(J)V

    return-void
.end method

.method public m()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/y/ab;->J:Z

    return v0
.end method

.method public n()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/y/ab;->K:Z

    return v0
.end method

.method public o()Lmaps/o/c;
    .locals 1

    iget-object v0, p0, Lmaps/y/ab;->p:Lmaps/o/c;

    return-object v0
.end method

.method public p()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lmaps/y/ab;->s:Ljava/util/ArrayList;

    return-object v0
.end method

.method public q()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/y/ab;->b:Z

    return v0
.end method

.method public r()Lmaps/cq/b;
    .locals 1

    iget-object v0, p0, Lmaps/y/ab;->L:Lmaps/cq/b;

    return-object v0
.end method

.method public s()Lmaps/ca/e;
    .locals 1

    iget-object v0, p0, Lmaps/y/ab;->q:Lmaps/bk/b;

    invoke-virtual {v0}, Lmaps/bk/b;->f()Lmaps/ca/e;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Lmaps/ap/e;->a(Ljava/lang/Object;)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "tileType"

    iget-object v2, p0, Lmaps/y/ab;->p:Lmaps/o/c;

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "isBase"

    iget-boolean v2, p0, Lmaps/y/ab;->K:Z

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;Z)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "maxTilesPerView"

    iget v2, p0, Lmaps/y/ab;->f:I

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;I)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "maxTilesToFetch"

    iget v2, p0, Lmaps/y/ab;->g:I

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;I)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "drawOrder"

    iget-object v2, p0, Lmaps/y/ab;->h:Lmaps/y/am;

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "fetchMissingAncestorTiles"

    iget-boolean v2, p0, Lmaps/y/ab;->l:Z

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;Z)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "allowMultiZoom"

    iget-boolean v2, p0, Lmaps/y/ab;->o:Z

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;Z)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "prefetchAncestors"

    iget-boolean v2, p0, Lmaps/y/ab;->n:Z

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;Z)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "tileSize"

    iget v2, p0, Lmaps/y/ab;->j:I

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;I)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "allowMultiZoom"

    iget-boolean v2, p0, Lmaps/y/ab;->o:Z

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;Z)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "isContributingLabels"

    iget-boolean v2, p0, Lmaps/y/ab;->J:Z

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;Z)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "maxTileSize"

    iget v2, p0, Lmaps/y/ab;->k:I

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;I)Lmaps/ap/n;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ap/n;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
