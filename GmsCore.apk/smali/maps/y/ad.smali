.class public Lmaps/y/ad;
.super Lmaps/ax/a;


# instance fields
.field private final b:Lmaps/bq/a;

.field private c:Lmaps/bq/b;

.field private final d:I

.field private final e:J

.field private f:I

.field private final g:Lmaps/ae/d;

.field private h:F


# direct methods
.method protected constructor <init>(Lmaps/bq/a;Lmaps/bq/b;IZF)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Lmaps/ax/a;-><init>(Lmaps/bq/a;)V

    iput v2, p0, Lmaps/y/ad;->f:I

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ae/c;->b()Lmaps/ae/d;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/ad;->g:Lmaps/ae/d;

    iput-object p1, p0, Lmaps/y/ad;->b:Lmaps/bq/a;

    iput-object p2, p0, Lmaps/y/ad;->c:Lmaps/bq/b;

    iget-object v0, p0, Lmaps/y/ad;->g:Lmaps/ae/d;

    invoke-interface {v0}, Lmaps/ae/d;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lmaps/y/ad;->e:J

    iget-object v0, p0, Lmaps/y/ad;->b:Lmaps/bq/a;

    iget-object v1, p0, Lmaps/y/ad;->c:Lmaps/bq/b;

    invoke-interface {v1}, Lmaps/bq/b;->b()Lmaps/bq/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/bq/a;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput v2, p0, Lmaps/y/ad;->d:I

    :goto_0
    if-eqz p4, :cond_1

    const/4 v0, 0x0

    iput v0, p0, Lmaps/y/ad;->h:F

    :goto_1
    return-void

    :cond_0
    invoke-static {v2, p3}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lmaps/y/ad;->d:I

    goto :goto_0

    :cond_1
    iput p5, p0, Lmaps/y/ad;->h:F

    goto :goto_1
.end method

.method private a(F)F
    .locals 5

    const/high16 v4, 0x3f800000

    float-to-double v0, p1

    const-wide v2, 0x400921fb54442d18L

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    double-to-float v0, v0

    add-float/2addr v0, v4

    const/high16 v1, 0x3f000000

    mul-float/2addr v0, v1

    sub-float v0, v4, v0

    return v0
.end method


# virtual methods
.method public a(Lmaps/bq/d;)Lmaps/bq/b;
    .locals 6

    const/high16 v2, 0x3f800000

    const/4 v3, 0x0

    iget-object v0, p0, Lmaps/y/ad;->g:Lmaps/ae/d;

    invoke-interface {v0}, Lmaps/ae/d;->b()J

    move-result-wide v0

    iget v4, p0, Lmaps/y/ad;->d:I

    if-nez v4, :cond_1

    move v1, v2

    :goto_0
    iget-object v0, p0, Lmaps/y/ad;->c:Lmaps/bq/b;

    instance-of v0, v0, Lmaps/ax/d;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/y/ad;->c:Lmaps/bq/b;

    check-cast v0, Lmaps/ax/d;

    cmpl-float v3, v1, v2

    if-ltz v3, :cond_0

    invoke-interface {v0}, Lmaps/ax/d;->a()V

    :cond_0
    invoke-interface {v0, p1}, Lmaps/ax/d;->a(Lmaps/bq/d;)Lmaps/bq/b;

    move-result-object v3

    iput-object v3, p0, Lmaps/y/ad;->c:Lmaps/bq/b;

    invoke-interface {v0}, Lmaps/ax/d;->c()I

    move-result v0

    :goto_1
    cmpl-float v2, v1, v2

    if-ltz v2, :cond_2

    iput v0, p0, Lmaps/y/ad;->f:I

    iget-object p0, p0, Lmaps/y/ad;->c:Lmaps/bq/b;

    :goto_2
    return-object p0

    :cond_1
    iget-wide v4, p0, Lmaps/y/ad;->e:J

    sub-long/2addr v0, v4

    long-to-float v0, v0

    iget v1, p0, Lmaps/y/ad;->d:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_2
    invoke-direct {p0, v1}, Lmaps/y/ad;->a(F)F

    move-result v1

    iget-object v2, p0, Lmaps/y/ad;->b:Lmaps/bq/a;

    iget-object v3, p0, Lmaps/y/ad;->c:Lmaps/bq/b;

    invoke-interface {v3}, Lmaps/bq/b;->b()Lmaps/bq/a;

    move-result-object v3

    iget-object v4, p0, Lmaps/y/ad;->b:Lmaps/bq/a;

    invoke-virtual {v3, v4}, Lmaps/bq/a;->a(Lmaps/bq/a;)Lmaps/bq/a;

    move-result-object v3

    iget v4, p0, Lmaps/y/ad;->h:F

    invoke-static {v2, v3, v1, v4}, Lmaps/bq/a;->a(Lmaps/bq/a;Lmaps/bq/a;FF)Lmaps/bq/a;

    move-result-object v1

    iput-object v1, p0, Lmaps/y/ad;->a:Lmaps/bq/a;

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lmaps/y/ad;->f:I

    goto :goto_2

    :cond_3
    move v0, v3

    goto :goto_1
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lmaps/y/ad;->f:I

    return v0
.end method
