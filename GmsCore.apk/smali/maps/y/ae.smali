.class final enum Lmaps/y/ae;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lmaps/y/ae;

.field public static final enum b:Lmaps/y/ae;

.field public static final enum c:Lmaps/y/ae;

.field public static final enum d:Lmaps/y/ae;

.field private static final synthetic e:[Lmaps/y/ae;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lmaps/y/ae;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lmaps/y/ae;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/y/ae;->a:Lmaps/y/ae;

    new-instance v0, Lmaps/y/ae;

    const-string v1, "IN_PROGRESS"

    invoke-direct {v0, v1, v3}, Lmaps/y/ae;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/y/ae;->b:Lmaps/y/ae;

    new-instance v0, Lmaps/y/ae;

    const-string v1, "ZOOM"

    invoke-direct {v0, v1, v4}, Lmaps/y/ae;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/y/ae;->c:Lmaps/y/ae;

    new-instance v0, Lmaps/y/ae;

    const-string v1, "ROTATE"

    invoke-direct {v0, v1, v5}, Lmaps/y/ae;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/y/ae;->d:Lmaps/y/ae;

    const/4 v0, 0x4

    new-array v0, v0, [Lmaps/y/ae;

    sget-object v1, Lmaps/y/ae;->a:Lmaps/y/ae;

    aput-object v1, v0, v2

    sget-object v1, Lmaps/y/ae;->b:Lmaps/y/ae;

    aput-object v1, v0, v3

    sget-object v1, Lmaps/y/ae;->c:Lmaps/y/ae;

    aput-object v1, v0, v4

    sget-object v1, Lmaps/y/ae;->d:Lmaps/y/ae;

    aput-object v1, v0, v5

    sput-object v0, Lmaps/y/ae;->e:[Lmaps/y/ae;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/y/ae;
    .locals 1

    const-class v0, Lmaps/y/ae;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/y/ae;

    return-object v0
.end method

.method public static values()[Lmaps/y/ae;
    .locals 1

    sget-object v0, Lmaps/y/ae;->e:[Lmaps/y/ae;

    invoke-virtual {v0}, [Lmaps/y/ae;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/y/ae;

    return-object v0
.end method
