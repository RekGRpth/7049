.class public Lmaps/y/ah;
.super Ljava/lang/Object;


# instance fields
.field private final a:Landroid/view/View;

.field private volatile b:Z

.field private volatile c:Lmaps/y/bb;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/y/ah;->a:Landroid/view/View;

    return-void
.end method


# virtual methods
.method public a()Lmaps/y/af;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public a(FF)V
    .locals 0

    return-void
.end method

.method public b()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/y/ah;->b:Z

    return-void
.end method

.method public b(FF)V
    .locals 0

    return-void
.end method

.method public c(FF)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/y/ah;->b:Z

    return-void
.end method

.method public c()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/y/ah;->b:Z

    return v0
.end method

.method public d()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lmaps/y/ah;->a:Landroid/view/View;

    return-object v0
.end method

.method public d(FF)V
    .locals 0

    return-void
.end method

.method public e()V
    .locals 1

    iget-object v0, p0, Lmaps/y/ah;->c:Lmaps/y/bb;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lmaps/y/bb;->a()V

    :cond_0
    return-void
.end method
