.class public final enum Lmaps/y/am;
.super Ljava/lang/Enum;


# static fields
.field public static final enum A:Lmaps/y/am;

.field public static final enum B:Lmaps/y/am;

.field private static final synthetic C:[Lmaps/y/am;

.field public static final enum a:Lmaps/y/am;

.field public static final enum b:Lmaps/y/am;

.field public static final enum c:Lmaps/y/am;

.field public static final enum d:Lmaps/y/am;

.field public static final enum e:Lmaps/y/am;

.field public static final enum f:Lmaps/y/am;

.field public static final enum g:Lmaps/y/am;

.field public static final enum h:Lmaps/y/am;

.field public static final enum i:Lmaps/y/am;

.field public static final enum j:Lmaps/y/am;

.field public static final enum k:Lmaps/y/am;

.field public static final enum l:Lmaps/y/am;

.field public static final enum m:Lmaps/y/am;

.field public static final enum n:Lmaps/y/am;

.field public static final enum o:Lmaps/y/am;

.field public static final enum p:Lmaps/y/am;

.field public static final enum q:Lmaps/y/am;

.field public static final enum r:Lmaps/y/am;

.field public static final enum s:Lmaps/y/am;

.field public static final enum t:Lmaps/y/am;

.field public static final enum u:Lmaps/y/am;

.field public static final enum v:Lmaps/y/am;

.field public static final enum w:Lmaps/y/am;

.field public static final enum x:Lmaps/y/am;

.field public static final enum y:Lmaps/y/am;

.field public static final enum z:Lmaps/y/am;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lmaps/y/am;

    const-string v1, "UNUSED"

    invoke-direct {v0, v1, v3}, Lmaps/y/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/y/am;->a:Lmaps/y/am;

    new-instance v0, Lmaps/y/am;

    const-string v1, "BASE_IMAGERY"

    invoke-direct {v0, v1, v4}, Lmaps/y/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/y/am;->b:Lmaps/y/am;

    new-instance v0, Lmaps/y/am;

    const-string v1, "VECTORS"

    invoke-direct {v0, v1, v5}, Lmaps/y/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/y/am;->c:Lmaps/y/am;

    new-instance v0, Lmaps/y/am;

    const-string v1, "NIGHT_DIMMER"

    invoke-direct {v0, v1, v6}, Lmaps/y/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/y/am;->d:Lmaps/y/am;

    new-instance v0, Lmaps/y/am;

    const-string v1, "DESATURATE"

    invoke-direct {v0, v1, v7}, Lmaps/y/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/y/am;->e:Lmaps/y/am;

    new-instance v0, Lmaps/y/am;

    const-string v1, "TRAFFIC"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lmaps/y/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/y/am;->f:Lmaps/y/am;

    new-instance v0, Lmaps/y/am;

    const-string v1, "INDOOR"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lmaps/y/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/y/am;->g:Lmaps/y/am;

    new-instance v0, Lmaps/y/am;

    const-string v1, "LAYER_SHAPES"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lmaps/y/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/y/am;->h:Lmaps/y/am;

    new-instance v0, Lmaps/y/am;

    const-string v1, "TRANSIT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lmaps/y/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/y/am;->i:Lmaps/y/am;

    new-instance v0, Lmaps/y/am;

    const-string v1, "BUILDINGS"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lmaps/y/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/y/am;->j:Lmaps/y/am;

    new-instance v0, Lmaps/y/am;

    const-string v1, "POLYLINE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lmaps/y/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/y/am;->k:Lmaps/y/am;

    new-instance v0, Lmaps/y/am;

    const-string v1, "LABELS"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lmaps/y/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/y/am;->l:Lmaps/y/am;

    new-instance v0, Lmaps/y/am;

    const-string v1, "FADE_OUT_OVERLAY"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lmaps/y/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/y/am;->m:Lmaps/y/am;

    new-instance v0, Lmaps/y/am;

    const-string v1, "ROUTE_OVERVIEW_POLYLINE"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lmaps/y/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/y/am;->n:Lmaps/y/am;

    new-instance v0, Lmaps/y/am;

    const-string v1, "TURN_ARROW_OVERLAY"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lmaps/y/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/y/am;->o:Lmaps/y/am;

    new-instance v0, Lmaps/y/am;

    const-string v1, "IMPORTANT_LABELS"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lmaps/y/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/y/am;->p:Lmaps/y/am;

    new-instance v0, Lmaps/y/am;

    const-string v1, "DEBUG_LABELS"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lmaps/y/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/y/am;->q:Lmaps/y/am;

    new-instance v0, Lmaps/y/am;

    const-string v1, "INTERSECTION"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lmaps/y/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/y/am;->r:Lmaps/y/am;

    new-instance v0, Lmaps/y/am;

    const-string v1, "SKY"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lmaps/y/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/y/am;->s:Lmaps/y/am;

    new-instance v0, Lmaps/y/am;

    const-string v1, "MY_LOCATION_OVERLAY_DA"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lmaps/y/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/y/am;->t:Lmaps/y/am;

    new-instance v0, Lmaps/y/am;

    const-string v1, "LAYERS_RAW_GPS"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lmaps/y/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/y/am;->u:Lmaps/y/am;

    new-instance v0, Lmaps/y/am;

    const-string v1, "LAYER_MARKERS_SHADOW"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lmaps/y/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/y/am;->v:Lmaps/y/am;

    new-instance v0, Lmaps/y/am;

    const-string v1, "LAYER_MARKERS"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lmaps/y/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/y/am;->w:Lmaps/y/am;

    new-instance v0, Lmaps/y/am;

    const-string v1, "MY_LOCATION_OVERLAY_VECTORMAPS"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lmaps/y/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/y/am;->x:Lmaps/y/am;

    new-instance v0, Lmaps/y/am;

    const-string v1, "COMPASS_OVERLAY"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lmaps/y/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/y/am;->y:Lmaps/y/am;

    new-instance v0, Lmaps/y/am;

    const-string v1, "LOADING_SPINNY"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lmaps/y/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/y/am;->z:Lmaps/y/am;

    new-instance v0, Lmaps/y/am;

    const-string v1, "BUBBLE"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lmaps/y/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/y/am;->A:Lmaps/y/am;

    new-instance v0, Lmaps/y/am;

    const-string v1, "HEADS_UP_DISPLAY"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lmaps/y/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/y/am;->B:Lmaps/y/am;

    const/16 v0, 0x1c

    new-array v0, v0, [Lmaps/y/am;

    sget-object v1, Lmaps/y/am;->a:Lmaps/y/am;

    aput-object v1, v0, v3

    sget-object v1, Lmaps/y/am;->b:Lmaps/y/am;

    aput-object v1, v0, v4

    sget-object v1, Lmaps/y/am;->c:Lmaps/y/am;

    aput-object v1, v0, v5

    sget-object v1, Lmaps/y/am;->d:Lmaps/y/am;

    aput-object v1, v0, v6

    sget-object v1, Lmaps/y/am;->e:Lmaps/y/am;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lmaps/y/am;->f:Lmaps/y/am;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lmaps/y/am;->g:Lmaps/y/am;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lmaps/y/am;->h:Lmaps/y/am;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lmaps/y/am;->i:Lmaps/y/am;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lmaps/y/am;->j:Lmaps/y/am;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lmaps/y/am;->k:Lmaps/y/am;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lmaps/y/am;->l:Lmaps/y/am;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lmaps/y/am;->m:Lmaps/y/am;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lmaps/y/am;->n:Lmaps/y/am;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lmaps/y/am;->o:Lmaps/y/am;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lmaps/y/am;->p:Lmaps/y/am;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lmaps/y/am;->q:Lmaps/y/am;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lmaps/y/am;->r:Lmaps/y/am;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lmaps/y/am;->s:Lmaps/y/am;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lmaps/y/am;->t:Lmaps/y/am;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lmaps/y/am;->u:Lmaps/y/am;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lmaps/y/am;->v:Lmaps/y/am;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lmaps/y/am;->w:Lmaps/y/am;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lmaps/y/am;->x:Lmaps/y/am;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lmaps/y/am;->y:Lmaps/y/am;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lmaps/y/am;->z:Lmaps/y/am;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lmaps/y/am;->A:Lmaps/y/am;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lmaps/y/am;->B:Lmaps/y/am;

    aput-object v2, v0, v1

    sput-object v0, Lmaps/y/am;->C:[Lmaps/y/am;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/y/am;
    .locals 1

    const-class v0, Lmaps/y/am;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/y/am;

    return-object v0
.end method

.method public static values()[Lmaps/y/am;
    .locals 1

    sget-object v0, Lmaps/y/am;->C:[Lmaps/y/am;

    invoke-virtual {v0}, [Lmaps/y/am;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/y/am;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    invoke-virtual {p0}, Lmaps/y/am;->ordinal()I

    move-result v0

    return v0
.end method
