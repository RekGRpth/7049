.class Lmaps/y/aq;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/p/z;


# instance fields
.field private final a:Ljava/util/Set;


# direct methods
.method public constructor <init>(Ljava/util/Set;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lmaps/f/bd;->a(Ljava/util/Collection;)Lmaps/f/bd;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/aq;->a:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public a(Lmaps/p/z;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public a()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lmaps/y/aq;->a:Ljava/util/Set;

    return-object v0
.end method

.method public a(Lmaps/cr/c;Lmaps/af/s;)V
    .locals 2

    invoke-interface {p2}, Lmaps/af/s;->c()Lmaps/p/av;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/p/av;->b()Lmaps/p/j;

    move-result-object v0

    sget-object v1, Lmaps/p/j;->e:Lmaps/p/j;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lmaps/cr/c;->A()V

    :cond_0
    return-void
.end method

.method public a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;Lmaps/t/bx;)V
    .locals 5

    const/16 v4, 0x80

    const/16 v3, 0x1e01

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {p3}, Lmaps/af/s;->c()Lmaps/p/av;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/p/av;->b()Lmaps/p/j;

    move-result-object v1

    sget-object v2, Lmaps/p/j;->e:Lmaps/p/j;

    if-ne v1, v2, :cond_0

    invoke-virtual {p1}, Lmaps/cr/c;->z()V

    invoke-interface {v0, v3, v3, v3}, Ljavax/microedition/khronos/opengles/GL10;->glStencilOp(III)V

    const/16 v1, 0x207

    const/16 v2, 0xff

    invoke-interface {v0, v1, v2, v4}, Ljavax/microedition/khronos/opengles/GL10;->glStencilFunc(III)V

    invoke-interface {v0, v4}, Ljavax/microedition/khronos/opengles/GL10;->glStencilMask(I)V

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    const v1, -0x9f9fa0

    invoke-static {v0, v1}, Lmaps/s/k;->a(Ljavax/microedition/khronos/opengles/GL10;I)V

    :cond_0
    return-void
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lmaps/p/z;

    invoke-virtual {p0, p1}, Lmaps/y/aq;->a(Lmaps/p/z;)I

    move-result v0

    return v0
.end method
