.class Lmaps/y/av;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lmaps/y/bk;


# direct methods
.method private constructor <init>(Lmaps/y/bk;)V
    .locals 0

    iput-object p1, p0, Lmaps/y/av;->a:Lmaps/y/bk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lmaps/y/bk;Lmaps/y/e;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/y/av;-><init>(Lmaps/y/bk;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, Lmaps/y/av;->a:Lmaps/y/bk;

    invoke-static {v0}, Lmaps/y/bk;->a(Lmaps/y/bk;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/y/av;->a:Lmaps/y/bk;

    invoke-static {v0}, Lmaps/y/bk;->b(Lmaps/y/bk;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lmaps/f/fd;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    sget-object v1, Lmaps/ay/v;->c:Lmaps/ay/v;

    invoke-static {v0, v1}, Lmaps/ay/f;->a(Ljava/util/List;Lmaps/ay/v;)Lmaps/ay/e;
    :try_end_1
    .catch Lmaps/ay/o; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lmaps/y/av;->a:Lmaps/y/bk;

    invoke-static {v1}, Lmaps/y/bk;->a(Lmaps/y/bk;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_2
    iget-object v2, p0, Lmaps/y/av;->a:Lmaps/y/bk;

    invoke-static {v2, v0}, Lmaps/y/bk;->a(Lmaps/y/bk;Lmaps/ay/e;)Lmaps/ay/e;

    iget-object v2, p0, Lmaps/y/av;->a:Lmaps/y/bk;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lmaps/y/bk;->a(Lmaps/y/bk;Z)Z

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    sget-boolean v1, Lmaps/ae/h;->F:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/y/av;->a:Lmaps/y/bk;

    invoke-static {v1}, Lmaps/y/bk;->d(Lmaps/y/bk;)Lmaps/y/b;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/y/av;->a:Lmaps/y/bk;

    invoke-static {v1}, Lmaps/y/bk;->d(Lmaps/y/bk;)Lmaps/y/b;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmaps/y/b;->a(Lmaps/ay/e;)V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    :catch_0
    move-exception v0

    :try_start_4
    iget-object v0, p0, Lmaps/y/av;->a:Lmaps/y/bk;

    invoke-static {v0}, Lmaps/y/bk;->c(Lmaps/y/bk;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lmaps/f/fd;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    const/4 v0, 0x0

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/cg;

    invoke-virtual {v0}, Lmaps/t/cg;->b()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_1

    :cond_1
    const/16 v0, 0x7d0

    if-ge v1, v0, :cond_2

    sget-object v0, Lmaps/ay/v;->d:Lmaps/ay/v;

    invoke-static {v2, v0}, Lmaps/ay/f;->a(Ljava/util/List;Lmaps/ay/v;)Lmaps/ay/e;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-static {}, Lmaps/ay/e;->d()Lmaps/ay/e;
    :try_end_4
    .catch Lmaps/ay/o; {:try_start_4 .. :try_end_4} :catch_1

    move-result-object v0

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {}, Lmaps/ay/e;->d()Lmaps/ay/e;

    move-result-object v0

    goto :goto_0

    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0
.end method
