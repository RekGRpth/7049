.class Lmaps/y/ax;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/q/x;


# instance fields
.field a:Lmaps/cr/c;

.field b:Lmaps/q/i;

.field c:Lmaps/bq/d;

.field d:Ljava/util/List;

.field final synthetic e:Lmaps/y/k;


# direct methods
.method public constructor <init>(Lmaps/y/k;Lmaps/cr/c;)V
    .locals 1

    iput-object p1, p0, Lmaps/y/ax;->e:Lmaps/y/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/ax;->d:Ljava/util/List;

    iput-object p2, p0, Lmaps/y/ax;->a:Lmaps/cr/c;

    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 3

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/ay;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lmaps/l/ay;->b(I)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private declared-synchronized a(Lmaps/bq/d;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lmaps/y/ax;->c:Lmaps/bq/d;

    iget-object v0, p0, Lmaps/y/ax;->b:Lmaps/q/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/ax;->b:Lmaps/q/i;

    sget-object v1, Lmaps/q/ay;->b:Lmaps/q/ay;

    invoke-interface {v0, p0, v1}, Lmaps/q/i;->a(Lmaps/q/x;Lmaps/q/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Lmaps/y/ax;Lmaps/bq/d;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/y/ax;->a(Lmaps/bq/d;)V

    return-void
.end method

.method static synthetic a(Lmaps/y/ax;Lmaps/l/ay;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/y/ax;->b(Lmaps/l/ay;)V

    return-void
.end method

.method private declared-synchronized b(Lmaps/l/ay;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/y/ax;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/y/ax;->b:Lmaps/q/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/ax;->b:Lmaps/q/i;

    sget-object v1, Lmaps/q/ay;->b:Lmaps/q/ay;

    invoke-interface {v0, p0, v1}, Lmaps/q/i;->a(Lmaps/q/x;Lmaps/q/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public declared-synchronized a(Lmaps/l/ay;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/y/ax;->a:Lmaps/cr/c;

    invoke-virtual {p1, v0}, Lmaps/l/ay;->c(Lmaps/cr/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lmaps/q/i;)V
    .locals 0

    iput-object p1, p0, Lmaps/y/ax;->b:Lmaps/q/i;

    return-void
.end method

.method public declared-synchronized b(Lmaps/q/i;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lmaps/y/ax;->e:Lmaps/y/k;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, p0, Lmaps/y/ax;->d:Ljava/util/List;

    invoke-direct {p0, v0}, Lmaps/y/ax;->a(Ljava/util/List;)V

    iget-object v0, p0, Lmaps/y/ax;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lmaps/y/ax;->e:Lmaps/y/k;

    invoke-static {v0}, Lmaps/y/k;->a(Lmaps/y/k;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lmaps/y/ax;->a(Ljava/util/List;)V

    iget-object v0, p0, Lmaps/y/ax;->e:Lmaps/y/k;

    iget-object v2, p0, Lmaps/y/ax;->c:Lmaps/bq/d;

    invoke-virtual {v0, v2}, Lmaps/y/k;->a(Lmaps/bq/d;)V

    iget-object v0, p0, Lmaps/y/ax;->e:Lmaps/y/k;

    iget-object v2, p0, Lmaps/y/ax;->c:Lmaps/bq/d;

    invoke-static {v0, v2}, Lmaps/y/k;->a(Lmaps/y/k;Lmaps/bq/d;)V

    iget-object v0, p0, Lmaps/y/ax;->e:Lmaps/y/k;

    iget-object v2, p0, Lmaps/y/ax;->c:Lmaps/bq/d;

    invoke-static {v0, v2}, Lmaps/y/k;->b(Lmaps/y/k;Lmaps/bq/d;)V

    iget-object v0, p0, Lmaps/y/ax;->e:Lmaps/y/k;

    invoke-static {v0}, Lmaps/y/k;->a(Lmaps/y/k;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/ay;

    iget-object v3, p0, Lmaps/y/ax;->a:Lmaps/cr/c;

    invoke-virtual {v0, v3}, Lmaps/l/ay;->a(Lmaps/cr/c;)V

    iget-object v3, p0, Lmaps/y/ax;->a:Lmaps/cr/c;

    invoke-virtual {v0, v3}, Lmaps/l/ay;->b(Lmaps/cr/c;)V

    const/16 v3, 0xff

    invoke-virtual {v0, v3}, Lmaps/l/ay;->b(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return-void
.end method
