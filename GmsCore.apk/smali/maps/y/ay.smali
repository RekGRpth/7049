.class public Lmaps/y/ay;
.super Lmaps/y/bc;


# instance fields
.field private a:Lmaps/y/am;

.field private b:[I


# direct methods
.method public constructor <init>(Lmaps/y/am;)V
    .locals 1

    invoke-direct {p0}, Lmaps/y/bc;-><init>()V

    sget v0, Lmaps/af/q;->g:I

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lmaps/y/ay;->b:[I

    iput-object p1, p0, Lmaps/y/ay;->a:Lmaps/y/am;

    return-void
.end method


# virtual methods
.method public a(Lmaps/af/q;I)V
    .locals 2

    iget-object v0, p0, Lmaps/y/ay;->b:[I

    invoke-virtual {p1}, Lmaps/af/q;->a()I

    move-result v1

    aput p2, v0, v1

    return-void
.end method

.method public a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lmaps/y/ay;->b:[I

    invoke-interface {p3}, Lmaps/af/s;->a()Lmaps/af/q;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/af/q;->a()I

    move-result v1

    aget v0, v0, v1

    invoke-interface {p3}, Lmaps/af/s;->b()I

    move-result v1

    if-gtz v1, :cond_0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    const/high16 v2, -0x40800000

    invoke-interface {v1, v3, v3, v2}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    const/16 v2, 0x302

    const/16 v3, 0x303

    invoke-interface {v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    invoke-static {v1, v0}, Lmaps/s/k;->a(Ljavax/microedition/khronos/opengles/GL10;I)V

    iget-object v0, p1, Lmaps/cr/c;->h:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->d(Lmaps/cr/c;)V

    const/4 v0, 0x5

    const/4 v2, 0x0

    const/4 v3, 0x4

    invoke-interface {v1, v0, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto :goto_0
.end method

.method public b()Lmaps/y/am;
    .locals 1

    iget-object v0, p0, Lmaps/y/ay;->a:Lmaps/y/am;

    return-object v0
.end method

.method public b(I)V
    .locals 1

    iget-object v0, p0, Lmaps/y/ay;->b:[I

    invoke-static {v0, p1}, Ljava/util/Arrays;->fill([II)V

    return-void
.end method
