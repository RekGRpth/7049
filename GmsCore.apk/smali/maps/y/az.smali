.class public Lmaps/y/az;
.super Lmaps/y/ab;


# instance fields
.field private d:F

.field private e:Z


# direct methods
.method constructor <init>(Lmaps/bk/b;Lmaps/cq/b;IIILmaps/y/am;IZZZZZ)V
    .locals 17

    invoke-virtual/range {p1 .. p1}, Lmaps/bk/b;->a()Lmaps/o/c;

    move-result-object v2

    const/4 v11, 0x0

    move-object/from16 v1, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move/from16 v5, p3

    move/from16 v6, p4

    move/from16 v7, p5

    move-object/from16 v8, p6

    move/from16 v9, p7

    move/from16 v10, p7

    move/from16 v12, p8

    move/from16 v13, p9

    move/from16 v14, p10

    move/from16 v15, p11

    move/from16 v16, p12

    invoke-direct/range {v1 .. v16}, Lmaps/y/ab;-><init>(Lmaps/o/c;Lmaps/bk/b;Lmaps/cq/b;IIILmaps/y/am;IIZZZZZZ)V

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lmaps/y/az;->e:Z

    const/high16 v1, 0x41f00000

    move-object/from16 v0, p0

    iput v1, v0, Lmaps/y/az;->d:F

    return-void
.end method


# virtual methods
.method public a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 2

    invoke-virtual {p2}, Lmaps/bq/d;->s()F

    move-result v0

    iget v1, p0, Lmaps/y/az;->d:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    iget-boolean v0, p0, Lmaps/y/az;->e:Z

    if-eqz v0, :cond_0

    invoke-super {p0, p2, p1}, Lmaps/y/ab;->a(Lmaps/bq/d;Lmaps/cr/c;)Z

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lmaps/y/ab;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/y/az;->b:Z

    goto :goto_0
.end method

.method public a(Lmaps/bq/d;Lmaps/cr/c;)Z
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p1}, Lmaps/bq/d;->s()F

    move-result v1

    iget v2, p0, Lmaps/y/az;->d:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/y/az;->e:Z

    invoke-super {p0, p1, p2}, Lmaps/y/ab;->a(Lmaps/bq/d;Lmaps/cr/c;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iput-boolean v0, p0, Lmaps/y/az;->e:Z

    goto :goto_0
.end method

.method protected b_()Lmaps/p/j;
    .locals 1

    sget-object v0, Lmaps/p/j;->i:Lmaps/p/j;

    return-object v0
.end method
