.class Lmaps/y/bd;
.super Lmaps/ax/a;


# instance fields
.field private b:F

.field private volatile c:Z

.field private final d:Lmaps/ax/b;


# direct methods
.method public constructor <init>(Lmaps/bq/a;Lmaps/ax/b;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/ax/a;-><init>(Lmaps/bq/a;)V

    iput-object p2, p0, Lmaps/y/bd;->d:Lmaps/ax/b;

    return-void
.end method

.method private a(Lmaps/t/bx;)[F
    .locals 4

    invoke-direct {p0, p1}, Lmaps/y/bd;->b(Lmaps/t/bx;)F

    move-result v0

    const/4 v1, 0x3

    new-array v1, v1, [F

    const/4 v2, 0x0

    invoke-virtual {p1}, Lmaps/t/bx;->f()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v0

    aput v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p1}, Lmaps/t/bx;->g()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v0

    aput v3, v1, v2

    const/4 v2, 0x2

    invoke-virtual {p1}, Lmaps/t/bx;->h()I

    move-result v3

    int-to-float v3, v3

    div-float v0, v3, v0

    aput v0, v1, v2

    return-object v1
.end method

.method private b(F)D
    .locals 2

    const/high16 v0, 0x3f800000

    const/high16 v1, -0x40800000

    invoke-static {v1, p1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->acos(D)D

    move-result-wide v0

    return-wide v0
.end method

.method private b(Lmaps/t/bx;)F
    .locals 3

    invoke-virtual {p1}, Lmaps/t/bx;->f()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1}, Lmaps/t/bx;->g()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Lmaps/t/bx;->h()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    mul-float v1, v2, v2

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method


# virtual methods
.method public a(Lmaps/bq/d;)Lmaps/bq/b;
    .locals 16

    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget v1, v0, Lmaps/y/bd;->b:F

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lmaps/y/bd;->b:F

    const/4 v2, 0x0

    cmpl-float v2, v1, v2

    if-nez v2, :cond_0

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lmaps/y/bd;->c:Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/y/bd;->a:Lmaps/bq/a;

    monitor-exit p0

    move-object/from16 p0, v1

    :goto_0
    return-object p0

    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual/range {p1 .. p1}, Lmaps/bq/d;->u()Lmaps/t/bx;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/y/bd;->a:Lmaps/bq/a;

    invoke-virtual {v2}, Lmaps/bq/a;->d()F

    move-result v2

    invoke-virtual/range {p1 .. p1}, Lmaps/bq/d;->l()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    invoke-virtual/range {p1 .. p1}, Lmaps/bq/d;->m()I

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x3f400000

    mul-float/2addr v5, v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Lmaps/bq/d;->d(FF)Lmaps/t/bx;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/y/bd;->d:Lmaps/ax/b;

    invoke-virtual {v4}, Lmaps/ax/b;->b()F

    move-result v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lmaps/y/bd;->a:Lmaps/bq/a;

    invoke-virtual {v6}, Lmaps/bq/a;->a()F

    move-result v6

    invoke-static {v6}, Lmaps/ax/b;->b(F)F

    move-result v6

    invoke-static {v4, v6}, Ljava/lang/Math;->min(FF)F

    move-result v4

    add-float/2addr v1, v2

    const/4 v2, 0x0

    cmpg-float v2, v1, v2

    if-gez v2, :cond_2

    const/4 v4, 0x0

    :cond_1
    :goto_1
    const/high16 v1, 0x42b40000

    sub-float/2addr v1, v4

    float-to-double v1, v1

    const-wide v6, 0x3f91df46a2529d39L

    mul-double/2addr v6, v1

    invoke-static {v5}, Lmaps/t/bx;->a(Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/t/bx;->f()I

    move-result v2

    invoke-virtual {v3}, Lmaps/t/bx;->f()I

    move-result v8

    sub-int/2addr v2, v8

    invoke-virtual {v1}, Lmaps/t/bx;->g()I

    move-result v8

    invoke-virtual {v3}, Lmaps/t/bx;->g()I

    move-result v9

    sub-int/2addr v8, v9

    invoke-virtual {v1}, Lmaps/t/bx;->h()I

    move-result v9

    invoke-virtual {v3}, Lmaps/t/bx;->h()I

    move-result v10

    sub-int/2addr v9, v10

    invoke-virtual {v1, v2, v8, v9}, Lmaps/t/bx;->a(III)V

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lmaps/y/bd;->a(Lmaps/t/bx;)[F

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/y/bd;->a:Lmaps/bq/a;

    invoke-virtual {v2}, Lmaps/bq/a;->c()Lmaps/t/bx;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/t/bx;->f()I

    move-result v8

    invoke-virtual {v3}, Lmaps/t/bx;->f()I

    move-result v9

    sub-int/2addr v8, v9

    invoke-virtual {v2}, Lmaps/t/bx;->g()I

    move-result v9

    invoke-virtual {v3}, Lmaps/t/bx;->g()I

    move-result v10

    sub-int/2addr v9, v10

    invoke-virtual {v2}, Lmaps/t/bx;->h()I

    move-result v10

    invoke-virtual {v3}, Lmaps/t/bx;->h()I

    move-result v11

    sub-int/2addr v10, v11

    invoke-virtual {v2, v8, v9, v10}, Lmaps/t/bx;->a(III)V

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lmaps/y/bd;->a(Lmaps/t/bx;)[F

    move-result-object v2

    const/4 v8, 0x0

    aget v8, v1, v8

    const/4 v9, 0x0

    aget v9, v2, v9

    mul-float/2addr v8, v9

    const/4 v9, 0x1

    aget v9, v1, v9

    const/4 v10, 0x1

    aget v10, v2, v10

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    const/4 v9, 0x2

    aget v1, v1, v9

    const/4 v9, 0x2

    aget v2, v2, v9

    mul-float/2addr v1, v2

    add-float/2addr v1, v8

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lmaps/y/bd;->b(F)D

    move-result-wide v8

    invoke-virtual {v5, v3}, Lmaps/t/bx;->c(Lmaps/t/bx;)F

    move-result v1

    float-to-double v1, v1

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    mul-double/2addr v10, v1

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v1

    div-double v1, v10, v1

    neg-double v1, v1

    move-object/from16 v0, p0

    iget-object v12, v0, Lmaps/y/bd;->a:Lmaps/bq/a;

    invoke-virtual {v12}, Lmaps/bq/a;->e()F

    move-result v12

    float-to-double v12, v12

    const-wide v14, 0x3f91df46a2529d39L

    mul-double/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    mul-double/2addr v14, v1

    neg-double v14, v14

    double-to-int v14, v14

    invoke-static {v12, v13}, Ljava/lang/Math;->cos(D)D

    move-result-wide v12

    mul-double/2addr v1, v12

    neg-double v1, v1

    double-to-int v1, v1

    new-instance v2, Lmaps/t/bx;

    invoke-virtual {v5}, Lmaps/t/bx;->f()I

    move-result v12

    add-int/2addr v12, v14

    invoke-virtual {v5}, Lmaps/t/bx;->g()I

    move-result v5

    add-int/2addr v1, v5

    invoke-direct {v2, v12, v1}, Lmaps/t/bx;-><init>(II)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/y/bd;->a:Lmaps/bq/a;

    invoke-virtual {v1}, Lmaps/bq/a;->c()Lmaps/t/bx;

    move-result-object v1

    invoke-virtual {v1, v3}, Lmaps/t/bx;->c(Lmaps/t/bx;)F

    move-result v1

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/y/bd;->a:Lmaps/bq/a;

    invoke-virtual {v3}, Lmaps/bq/a;->a()F

    move-result v3

    invoke-static {v3}, Lmaps/bq/a;->b(F)F

    move-result v3

    div-float v1, v3, v1

    invoke-static {v8, v9}, Ljava/lang/Math;->tan(D)D

    move-result-wide v8

    div-double v8, v10, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->tan(D)D

    move-result-wide v5

    div-double v5, v10, v5

    add-double/2addr v5, v8

    double-to-float v3, v5

    mul-float/2addr v1, v3

    invoke-static {v1}, Lmaps/bq/a;->a(F)F

    move-result v3

    move-object/from16 v0, p0

    iget-object v7, v0, Lmaps/y/bd;->d:Lmaps/ax/b;

    new-instance v1, Lmaps/bq/a;

    move-object/from16 v0, p0

    iget-object v5, v0, Lmaps/y/bd;->a:Lmaps/bq/a;

    invoke-virtual {v5}, Lmaps/bq/a;->e()F

    move-result v5

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, Lmaps/bq/a;-><init>(Lmaps/t/bx;FFFF)V

    invoke-virtual {v7, v1}, Lmaps/ax/b;->a(Lmaps/bq/a;)Lmaps/bq/a;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lmaps/y/bd;->a:Lmaps/bq/a;

    goto/16 :goto_0

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    :cond_2
    cmpl-float v2, v1, v4

    if-gtz v2, :cond_1

    move v4, v1

    goto/16 :goto_1
.end method

.method declared-synchronized a(F)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/y/bd;->b:F

    add-float/2addr v0, p1

    iput v0, p0, Lmaps/y/bd;->b:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/y/bd;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()I
    .locals 1

    iget-boolean v0, p0, Lmaps/y/bd;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
