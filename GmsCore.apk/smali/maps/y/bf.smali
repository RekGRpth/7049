.class public Lmaps/y/bf;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;

# interfaces
.implements Lmaps/d/m;


# static fields
.field private static final c:F

.field private static final d:F

.field private static final e:D


# instance fields
.field private final a:F

.field private final b:F

.field private final f:Lmaps/y/aa;

.field private g:Lmaps/y/s;

.field private h:Landroid/view/MotionEvent;

.field private i:F

.field private j:F

.field private k:Lmaps/y/ae;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget-boolean v0, Lmaps/bm/b;->g:Z

    if-eqz v0, :cond_0

    const v0, 0x3f7f3b64

    :goto_0
    sput v0, Lmaps/y/bf;->c:F

    const/high16 v0, 0x3f800000

    sget v1, Lmaps/y/bf;->c:F

    div-float/2addr v0, v1

    sput v0, Lmaps/y/bf;->d:F

    const-wide/high16 v0, 0x4000000000000000L

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    sput-wide v0, Lmaps/y/bf;->e:D

    return-void

    :cond_0
    const v0, 0x3f7fbe77

    goto :goto_0
.end method

.method public constructor <init>(Lmaps/y/aa;)V
    .locals 2

    const/16 v1, 0x14

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    sget-object v0, Lmaps/y/ae;->a:Lmaps/y/ae;

    iput-object v0, p0, Lmaps/y/bf;->k:Lmaps/y/ae;

    iput-object p1, p0, Lmaps/y/bf;->f:Lmaps/y/aa;

    new-instance v0, Lmaps/y/s;

    invoke-direct {v0}, Lmaps/y/s;-><init>()V

    iput-object v0, p0, Lmaps/y/bf;->g:Lmaps/y/s;

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v0

    invoke-virtual {v0, v1}, Lmaps/ae/c;->a(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lmaps/y/bf;->b:F

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v0

    invoke-virtual {v0, v1}, Lmaps/ae/c;->a(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lmaps/y/bf;->a:F

    return-void
.end method

.method private static b(Lmaps/d/b;)Z
    .locals 2

    invoke-virtual {p0}, Lmaps/d/b;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmaps/d/b;->c()F

    move-result v0

    sget v1, Lmaps/y/bf;->c:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lmaps/d/b;->c()F

    move-result v0

    sget v1, Lmaps/y/bf;->d:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected a(Ljava/lang/String;)V
    .locals 1

    const/16 v0, 0x63

    invoke-static {v0, p1}, Lmaps/bh/k;->a(ILjava/lang/String;)V

    return-void
.end method

.method public a(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/y/bf;->g:Lmaps/y/s;

    iput-boolean p1, v0, Lmaps/y/s;->a:Z

    return-void
.end method

.method public a()Z
    .locals 1

    iget-object v0, p0, Lmaps/y/bf;->g:Lmaps/y/s;

    iget-boolean v0, v0, Lmaps/y/s;->a:Z

    return v0
.end method

.method public a(Lmaps/d/b;)Z
    .locals 5

    const/high16 v4, 0x40000000

    iget-object v0, p0, Lmaps/y/bf;->g:Lmaps/y/s;

    iget-boolean v0, v0, Lmaps/y/s;->b:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lmaps/d/b;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/y/bf;->f:Lmaps/y/aa;

    invoke-interface {v0}, Lmaps/y/aa;->f()Lmaps/y/q;

    move-result-object v0

    const/high16 v1, -0x40800000

    const/16 v2, 0x14a

    invoke-virtual {v0, v1, v2}, Lmaps/y/q;->b(FI)F

    move-result v0

    iget-object v1, p0, Lmaps/y/bf;->f:Lmaps/y/aa;

    iget-object v2, p0, Lmaps/y/bf;->f:Lmaps/y/aa;

    invoke-interface {v2}, Lmaps/y/aa;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v4

    iget-object v3, p0, Lmaps/y/bf;->f:Lmaps/y/aa;

    invoke-interface {v3}, Lmaps/y/aa;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v4

    invoke-interface {v1, v0, v2, v3}, Lmaps/y/aa;->a(FFF)V

    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    :cond_1
    invoke-virtual {p1}, Lmaps/d/b;->c()F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    sget-wide v2, Lmaps/y/bf;->e:D

    div-double/2addr v0, v2

    double-to-float v0, v0

    invoke-virtual {p1}, Lmaps/d/b;->a()F

    move-result v1

    invoke-virtual {p1}, Lmaps/d/b;->b()F

    move-result v2

    invoke-static {p1}, Lmaps/y/bf;->b(Lmaps/d/b;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v0, 0x0

    :cond_2
    iget-object v3, p0, Lmaps/y/bf;->f:Lmaps/y/aa;

    invoke-interface {v3}, Lmaps/y/aa;->f()Lmaps/y/q;

    move-result-object v3

    invoke-virtual {v3, v0, v1, v2}, Lmaps/y/q;->a(FFF)F

    move-result v0

    iget-object v3, p0, Lmaps/y/bf;->f:Lmaps/y/aa;

    invoke-interface {v3, v0, v1, v2}, Lmaps/y/aa;->a(FFF)V

    goto :goto_0
.end method

.method public a(Lmaps/d/d;)Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/y/bf;->g:Lmaps/y/s;

    iget-boolean v1, v1, Lmaps/y/s;->d:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/y/bf;->f:Lmaps/y/aa;

    invoke-interface {v1}, Lmaps/y/aa;->f()Lmaps/y/q;

    move-result-object v1

    invoke-virtual {p1}, Lmaps/d/d;->a()F

    move-result v2

    invoke-virtual {v1, v2, v0}, Lmaps/y/q;->c(FI)V

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public a(Lmaps/d/r;)Z
    .locals 4

    iget-object v0, p0, Lmaps/y/bf;->g:Lmaps/y/s;

    iget-boolean v0, v0, Lmaps/y/s;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/bf;->f:Lmaps/y/aa;

    invoke-interface {v0}, Lmaps/y/aa;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lmaps/y/bf;->f:Lmaps/y/aa;

    invoke-interface {v1}, Lmaps/y/aa;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Lmaps/d/r;->a(FF)V

    invoke-virtual {p1}, Lmaps/d/r;->c()F

    move-result v0

    const v1, 0x42652ee0

    mul-float/2addr v0, v1

    invoke-virtual {p1}, Lmaps/d/r;->a()F

    move-result v1

    invoke-virtual {p1}, Lmaps/d/r;->b()F

    move-result v2

    iget-object v3, p0, Lmaps/y/bf;->f:Lmaps/y/aa;

    invoke-interface {v3}, Lmaps/y/aa;->f()Lmaps/y/q;

    move-result-object v3

    invoke-virtual {v3, v1, v2, v0}, Lmaps/y/q;->b(FFF)F

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/y/bf;->g:Lmaps/y/s;

    iput-boolean p1, v0, Lmaps/y/s;->b:Z

    return-void
.end method

.method public b()Z
    .locals 1

    iget-object v0, p0, Lmaps/y/bf;->g:Lmaps/y/s;

    iget-boolean v0, v0, Lmaps/y/s;->b:Z

    return v0
.end method

.method public c(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/y/bf;->g:Lmaps/y/s;

    iput-boolean p1, v0, Lmaps/y/s;->d:Z

    return-void
.end method

.method public c()Z
    .locals 1

    iget-object v0, p0, Lmaps/y/bf;->g:Lmaps/y/s;

    iget-boolean v0, v0, Lmaps/y/s;->d:Z

    return v0
.end method

.method public d(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/y/bf;->g:Lmaps/y/s;

    iput-boolean p1, v0, Lmaps/y/s;->e:Z

    return-void
.end method

.method public d()Z
    .locals 1

    iget-object v0, p0, Lmaps/y/bf;->g:Lmaps/y/s;

    iget-boolean v0, v0, Lmaps/y/s;->e:Z

    return v0
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 3

    iget-object v0, p0, Lmaps/y/bf;->f:Lmaps/y/aa;

    invoke-interface {v0}, Lmaps/y/aa;->i()V

    iget-object v0, p0, Lmaps/y/bf;->f:Lmaps/y/aa;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-interface {v0, v1, v2}, Lmaps/y/aa;->b(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    sget-object v0, Lmaps/y/ae;->b:Lmaps/y/ae;

    iput-object v0, p0, Lmaps/y/bf;->k:Lmaps/y/ae;

    iput-object p1, p0, Lmaps/y/bf;->h:Landroid/view/MotionEvent;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lmaps/y/bf;->i:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lmaps/y/bf;->j:F

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 10

    const/16 v7, 0x14a

    const/4 v0, 0x0

    const/high16 v4, 0x40000000

    const/high16 v6, 0x3f000000

    const/4 v3, 0x1

    iget-object v1, p0, Lmaps/y/bf;->f:Lmaps/y/aa;

    invoke-interface {v1}, Lmaps/y/aa;->i()V

    iget-object v1, p0, Lmaps/y/bf;->h:Landroid/view/MotionEvent;

    if-eqz v1, :cond_5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v3, :cond_5

    iget-object v1, p0, Lmaps/y/bf;->g:Lmaps/y/s;

    iget-boolean v1, v1, Lmaps/y/s;->b:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lmaps/y/bf;->k:Lmaps/y/ae;

    sget-object v2, Lmaps/y/ae;->b:Lmaps/y/ae;

    if-ne v1, v2, :cond_4

    iget-object v0, p0, Lmaps/y/bf;->g:Lmaps/y/s;

    iget-boolean v0, v0, Lmaps/y/s;->c:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/y/bf;->f:Lmaps/y/aa;

    invoke-interface {v0}, Lmaps/y/aa;->getWidth()I

    move-result v0

    int-to-float v0, v0

    div-float v1, v0, v4

    iget-object v0, p0, Lmaps/y/bf;->f:Lmaps/y/aa;

    invoke-interface {v0}, Lmaps/y/aa;->getHeight()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v4

    :goto_0
    const/high16 v2, 0x3f800000

    sget-boolean v4, Lmaps/ae/h;->p:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lmaps/y/bf;->h:Landroid/view/MotionEvent;

    invoke-virtual {v4}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    sub-float/2addr v4, v5

    iget v5, p0, Lmaps/y/bf;->b:F

    cmpl-float v4, v4, v5

    if-lez v4, :cond_0

    neg-float v2, v2

    const-string v4, "h"

    invoke-virtual {p0, v4}, Lmaps/y/bf;->a(Ljava/lang/String;)V

    :cond_0
    iget-object v4, p0, Lmaps/y/bf;->g:Lmaps/y/s;

    iget-boolean v4, v4, Lmaps/y/s;->c:Z

    if-eqz v4, :cond_3

    iget-object v4, p0, Lmaps/y/bf;->f:Lmaps/y/aa;

    invoke-interface {v4}, Lmaps/y/aa;->f()Lmaps/y/q;

    move-result-object v4

    invoke-virtual {v4, v2, v7}, Lmaps/y/q;->b(FI)F

    move-result v2

    :goto_1
    iget-object v4, p0, Lmaps/y/bf;->f:Lmaps/y/aa;

    invoke-interface {v4, v2, v1, v0}, Lmaps/y/aa;->a(FFF)V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/y/bf;->h:Landroid/view/MotionEvent;

    sget-object v0, Lmaps/y/ae;->a:Lmaps/y/ae;

    iput-object v0, p0, Lmaps/y/bf;->k:Lmaps/y/ae;

    move v0, v3

    :cond_1
    :goto_2
    return v0

    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    goto :goto_0

    :cond_3
    iget-object v4, p0, Lmaps/y/bf;->f:Lmaps/y/aa;

    invoke-interface {v4}, Lmaps/y/aa;->f()Lmaps/y/q;

    move-result-object v4

    invoke-virtual {v4, v2, v1, v0, v7}, Lmaps/y/q;->a(FFFI)F

    move-result v2

    goto :goto_1

    :cond_4
    const/4 v1, 0x0

    iput-object v1, p0, Lmaps/y/bf;->h:Landroid/view/MotionEvent;

    sget-object v1, Lmaps/y/ae;->a:Lmaps/y/ae;

    iput-object v1, p0, Lmaps/y/bf;->k:Lmaps/y/ae;

    :cond_5
    iget-object v1, p0, Lmaps/y/bf;->h:Landroid/view/MotionEvent;

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iget v2, p0, Lmaps/y/bf;->j:F

    sub-float/2addr v1, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iget v4, p0, Lmaps/y/bf;->i:F

    sub-float/2addr v2, v4

    iget-object v4, p0, Lmaps/y/bf;->k:Lmaps/y/ae;

    sget-object v5, Lmaps/y/ae;->b:Lmaps/y/ae;

    if-ne v4, v5, :cond_6

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget v5, p0, Lmaps/y/bf;->b:F

    cmpg-float v4, v4, v5

    if-gez v4, :cond_6

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v4, p0, Lmaps/y/bf;->b:F

    cmpg-float v2, v2, v4

    if-gez v2, :cond_6

    iget-object v2, p0, Lmaps/y/bf;->h:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    sub-float/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iget-object v4, p0, Lmaps/y/bf;->h:Landroid/view/MotionEvent;

    invoke-virtual {v4}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    iget v5, p0, Lmaps/y/bf;->a:F

    cmpl-float v4, v4, v5

    if-lez v4, :cond_8

    iget-object v4, p0, Lmaps/y/bf;->g:Lmaps/y/s;

    iget-boolean v4, v4, Lmaps/y/s;->b:Z

    if-eqz v4, :cond_8

    sget-object v2, Lmaps/y/ae;->c:Lmaps/y/ae;

    iput-object v2, p0, Lmaps/y/bf;->k:Lmaps/y/ae;

    const-string v2, "d"

    invoke-virtual {p0, v2}, Lmaps/y/bf;->a(Ljava/lang/String;)V

    :cond_6
    :goto_3
    iget-object v2, p0, Lmaps/y/bf;->k:Lmaps/y/ae;

    sget-object v4, Lmaps/y/ae;->c:Lmaps/y/ae;

    if-ne v2, v4, :cond_b

    iget-object v2, p0, Lmaps/y/bf;->g:Lmaps/y/s;

    iget-boolean v2, v2, Lmaps/y/s;->b:Z

    if-eqz v2, :cond_b

    iget-object v2, p0, Lmaps/y/bf;->f:Lmaps/y/aa;

    invoke-interface {v2}, Lmaps/y/aa;->getHeight()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    const/high16 v2, 0x40c00000

    mul-float/2addr v1, v2

    iget-object v2, p0, Lmaps/y/bf;->f:Lmaps/y/aa;

    invoke-interface {v2}, Lmaps/y/aa;->f()Lmaps/y/q;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Lmaps/y/q;->b(FI)F

    move-result v0

    iget-object v1, p0, Lmaps/y/bf;->f:Lmaps/y/aa;

    iget-object v2, p0, Lmaps/y/bf;->h:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iget-object v4, p0, Lmaps/y/bf;->h:Landroid/view/MotionEvent;

    invoke-virtual {v4}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-interface {v1, v0, v2, v4}, Lmaps/y/aa;->a(FFF)V

    :cond_7
    :goto_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lmaps/y/bf;->i:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lmaps/y/bf;->j:F

    move v0, v3

    goto/16 :goto_2

    :cond_8
    sget-boolean v4, Lmaps/ae/h;->p:Z

    if-eqz v4, :cond_a

    iget-object v4, p0, Lmaps/y/bf;->g:Lmaps/y/s;

    iget-boolean v4, v4, Lmaps/y/s;->e:Z

    if-eqz v4, :cond_a

    int-to-float v2, v2

    iget v4, p0, Lmaps/y/bf;->a:F

    cmpl-float v2, v2, v4

    if-lez v2, :cond_a

    iget-object v2, p0, Lmaps/y/bf;->h:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget-object v4, p0, Lmaps/y/bf;->f:Lmaps/y/aa;

    invoke-interface {v4}, Lmaps/y/aa;->getHeight()I

    move-result v4

    int-to-float v4, v4

    const v5, 0x3f2b851f

    mul-float/2addr v4, v5

    cmpl-float v2, v2, v4

    if-gtz v2, :cond_9

    iget-object v2, p0, Lmaps/y/bf;->h:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget-object v4, p0, Lmaps/y/bf;->f:Lmaps/y/aa;

    invoke-interface {v4}, Lmaps/y/aa;->getHeight()I

    move-result v4

    int-to-float v4, v4

    const v5, 0x3ea8f5c3

    mul-float/2addr v4, v5

    cmpg-float v2, v2, v4

    if-gez v2, :cond_a

    :cond_9
    sget-object v2, Lmaps/y/ae;->d:Lmaps/y/ae;

    iput-object v2, p0, Lmaps/y/bf;->k:Lmaps/y/ae;

    const-string v2, "o"

    invoke-virtual {p0, v2}, Lmaps/y/bf;->a(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_a
    move v0, v3

    goto/16 :goto_2

    :cond_b
    iget-object v0, p0, Lmaps/y/bf;->k:Lmaps/y/ae;

    sget-object v1, Lmaps/y/ae;->d:Lmaps/y/ae;

    if-ne v0, v1, :cond_7

    iget-object v0, p0, Lmaps/y/bf;->g:Lmaps/y/s;

    iget-boolean v0, v0, Lmaps/y/s;->e:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lmaps/y/bf;->f:Lmaps/y/aa;

    invoke-interface {v0}, Lmaps/y/aa;->getWidth()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v6

    iget-object v1, p0, Lmaps/y/bf;->f:Lmaps/y/aa;

    invoke-interface {v1}, Lmaps/y/aa;->getHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v6

    iget v2, p0, Lmaps/y/bf;->i:F

    iget v4, p0, Lmaps/y/bf;->j:F

    invoke-static {v0, v1, v2, v4}, Lmaps/d/s;->a(FFFF)F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    invoke-static {v0, v1, v4, v5}, Lmaps/d/s;->a(FFFF)F

    move-result v4

    iget-object v5, p0, Lmaps/y/bf;->f:Lmaps/y/aa;

    invoke-interface {v5}, Lmaps/y/aa;->f()Lmaps/y/q;

    move-result-object v5

    sub-float v2, v4, v2

    const/high16 v4, 0x43340000

    mul-float/2addr v2, v4

    float-to-double v6, v2

    const-wide v8, 0x400921fb54442d18L

    div-double/2addr v6, v8

    double-to-float v2, v6

    invoke-virtual {v5, v0, v1, v2}, Lmaps/y/q;->b(FFF)F

    goto/16 :goto_4
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 3

    iget-object v0, p0, Lmaps/y/bf;->k:Lmaps/y/ae;

    sget-object v1, Lmaps/y/ae;->a:Lmaps/y/ae;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lmaps/y/bf;->f:Lmaps/y/aa;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-interface {v0, v1, v2}, Lmaps/y/aa;->e(FF)V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1

    iget-object v0, p0, Lmaps/y/bf;->g:Lmaps/y/s;

    iget-boolean v0, v0, Lmaps/y/s;->a:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p2}, Lmaps/y/bf;->onSingleTapUp(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/y/bf;->f:Lmaps/y/aa;

    invoke-interface {v0}, Lmaps/y/aa;->f()Lmaps/y/q;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lmaps/y/q;->b(FF)V

    iget-object v0, p0, Lmaps/y/bf;->f:Lmaps/y/aa;

    invoke-interface {v0}, Lmaps/y/aa;->i()V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 3

    iget-object v0, p0, Lmaps/y/bf;->h:Landroid/view/MotionEvent;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/y/bf;->g:Lmaps/y/s;

    iget-boolean v0, v0, Lmaps/y/s;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/bf;->f:Lmaps/y/aa;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-interface {v0, v1, v2}, Lmaps/y/aa;->d(FF)V

    :cond_0
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lmaps/y/bf;->f:Lmaps/y/aa;

    invoke-interface {v0, p2, p3, p4}, Lmaps/y/aa;->a(Landroid/view/MotionEvent;FF)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v0, p0, Lmaps/y/bf;->g:Lmaps/y/s;

    iget-boolean v0, v0, Lmaps/y/s;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/bf;->f:Lmaps/y/aa;

    invoke-interface {v0}, Lmaps/y/aa;->f()Lmaps/y/q;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lmaps/y/q;->a(FF)V

    iget-object v0, p0, Lmaps/y/bf;->f:Lmaps/y/aa;

    invoke-interface {v0, p3, p4}, Lmaps/y/aa;->a(FF)V

    iget-object v0, p0, Lmaps/y/bf;->f:Lmaps/y/aa;

    invoke-interface {v0}, Lmaps/y/aa;->i()V

    goto :goto_0
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 3

    iget-object v0, p0, Lmaps/y/bf;->g:Lmaps/y/s;

    iget-boolean v0, v0, Lmaps/y/s;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/bf;->f:Lmaps/y/aa;

    invoke-interface {v0}, Lmaps/y/aa;->i()V

    iget-object v0, p0, Lmaps/y/bf;->f:Lmaps/y/aa;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-interface {v0, v1, v2}, Lmaps/y/aa;->c(FF)V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 3

    iget-object v0, p0, Lmaps/y/bf;->f:Lmaps/y/aa;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-interface {v0, v1, v2}, Lmaps/y/aa;->f(FF)Z

    move-result v0

    return v0
.end method
