.class Lmaps/y/bi;
.super Lmaps/ax/a;


# instance fields
.field private b:F

.field private c:F

.field private d:F

.field private e:F

.field private f:F

.field private g:F

.field private volatile h:Z

.field private final i:Lmaps/ax/b;

.field private final j:[F


# direct methods
.method public constructor <init>(Lmaps/bq/a;Lmaps/ax/b;)V
    .locals 1

    invoke-direct {p0, p1}, Lmaps/ax/a;-><init>(Lmaps/bq/a;)V

    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, Lmaps/y/bi;->j:[F

    iput-object p2, p0, Lmaps/y/bi;->i:Lmaps/ax/b;

    return-void
.end method


# virtual methods
.method public a(Lmaps/bq/d;)Lmaps/bq/b;
    .locals 11

    monitor-enter p0

    :try_start_0
    iget v4, p0, Lmaps/y/bi;->f:F

    iget v5, p0, Lmaps/y/bi;->g:F

    iget v7, p0, Lmaps/y/bi;->d:F

    iget v8, p0, Lmaps/y/bi;->e:F

    iget v0, p0, Lmaps/y/bi;->b:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    iget v0, p0, Lmaps/y/bi;->b:F

    iget v1, p0, Lmaps/y/bi;->b:F

    iget v2, p0, Lmaps/y/bi;->b:F

    mul-float/2addr v1, v2

    iget v2, p0, Lmaps/y/bi;->b:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x42c80000

    mul-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v3

    :goto_0
    iget v0, p0, Lmaps/y/bi;->c:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    iget v0, p0, Lmaps/y/bi;->c:F

    iget v1, p0, Lmaps/y/bi;->c:F

    iget v2, p0, Lmaps/y/bi;->c:F

    mul-float/2addr v1, v2

    const v2, -0x42333333

    mul-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    move v6, v0

    :goto_1
    iget v0, p0, Lmaps/y/bi;->b:F

    sub-float/2addr v0, v3

    iput v0, p0, Lmaps/y/bi;->b:F

    iget v0, p0, Lmaps/y/bi;->c:F

    sub-float/2addr v0, v6

    iput v0, p0, Lmaps/y/bi;->c:F

    const/4 v0, 0x0

    iput v0, p0, Lmaps/y/bi;->d:F

    const/4 v0, 0x0

    iput v0, p0, Lmaps/y/bi;->e:F

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    const-wide v9, 0x3f50624dd2f1a9fcL

    cmpg-double v0, v0, v9

    if-gez v0, :cond_3

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    const-wide v9, 0x3f50624dd2f1a9fcL

    cmpg-double v0, v0, v9

    if-gez v0, :cond_3

    const/4 v0, 0x0

    cmpl-float v0, v7, v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    cmpl-float v0, v8, v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/y/bi;->h:Z

    iget-object v0, p0, Lmaps/y/bi;->a:Lmaps/bq/a;

    monitor-exit p0

    move-object p0, v0

    :cond_0
    :goto_2
    return-object p0

    :cond_1
    iget v0, p0, Lmaps/y/bi;->b:F

    iget v1, p0, Lmaps/y/bi;->b:F

    iget v2, p0, Lmaps/y/bi;->b:F

    mul-float/2addr v1, v2

    iget v2, p0, Lmaps/y/bi;->b:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x42c80000

    mul-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v3

    goto :goto_0

    :cond_2
    iget v0, p0, Lmaps/y/bi;->c:F

    iget v1, p0, Lmaps/y/bi;->c:F

    iget v2, p0, Lmaps/y/bi;->c:F

    mul-float/2addr v1, v2

    const v2, 0x3dcccccd

    mul-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    move v6, v0

    goto :goto_1

    :cond_3
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x0

    cmpl-float v0, v7, v0

    if-nez v0, :cond_4

    const/4 v0, 0x0

    cmpl-float v0, v8, v0

    if-eqz v0, :cond_8

    :cond_4
    const/4 v0, 0x1

    move v2, v0

    :goto_3
    const/4 v0, 0x0

    cmpl-float v0, v6, v0

    if-eqz v0, :cond_9

    const/4 v0, 0x1

    move v1, v0

    :goto_4
    const/4 v0, 0x0

    cmpl-float v0, v3, v0

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_5
    if-eqz v2, :cond_6

    iget-object v2, p0, Lmaps/y/bi;->a:Lmaps/bq/a;

    invoke-static {v2, p1, v7, v8}, Lmaps/y/q;->a(Lmaps/bq/a;Lmaps/bq/d;FF)Lmaps/bq/a;

    move-result-object v2

    iput-object v2, p0, Lmaps/y/bi;->a:Lmaps/bq/a;

    if-nez v1, :cond_5

    if-eqz v0, :cond_6

    :cond_5
    iget-object v2, p0, Lmaps/y/bi;->a:Lmaps/bq/a;

    invoke-virtual {p1, v2}, Lmaps/bq/d;->a(Lmaps/bq/a;)V

    :cond_6
    if-eqz v1, :cond_7

    invoke-virtual {p1, v4, v5}, Lmaps/bq/d;->d(FF)Lmaps/t/bx;

    move-result-object v1

    iget-object v2, p0, Lmaps/y/bi;->i:Lmaps/ax/b;

    invoke-static {p1, v2, v1, v6}, Lmaps/y/q;->a(Lmaps/bq/d;Lmaps/ax/b;Lmaps/t/bx;F)Lmaps/bq/a;

    move-result-object v1

    iput-object v1, p0, Lmaps/y/bi;->a:Lmaps/bq/a;

    if-eqz v0, :cond_7

    iget-object v1, p0, Lmaps/y/bi;->a:Lmaps/bq/a;

    invoke-virtual {p1, v1}, Lmaps/bq/d;->a(Lmaps/bq/a;)V

    :cond_7
    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/bi;->a:Lmaps/bq/a;

    iget-object v2, p0, Lmaps/y/bi;->i:Lmaps/ax/b;

    move-object v1, p1

    invoke-static/range {v0 .. v5}, Lmaps/y/q;->a(Lmaps/bq/a;Lmaps/bq/d;Lmaps/ax/b;FFF)Lmaps/bq/a;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/bi;->a:Lmaps/bq/a;

    goto :goto_2

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_8
    const/4 v0, 0x0

    move v2, v0

    goto :goto_3

    :cond_9
    const/4 v0, 0x0

    move v1, v0

    goto :goto_4

    :cond_a
    const/4 v0, 0x0

    goto :goto_5
.end method

.method declared-synchronized a(FFFFFF)[F
    .locals 4

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/y/bi;->b:F

    add-float/2addr v0, p1

    iput v0, p0, Lmaps/y/bi;->b:F

    iget v0, p0, Lmaps/y/bi;->c:F

    add-float/2addr v0, p2

    iput v0, p0, Lmaps/y/bi;->c:F

    iget v0, p0, Lmaps/y/bi;->d:F

    add-float/2addr v0, p5

    iput v0, p0, Lmaps/y/bi;->d:F

    iget v0, p0, Lmaps/y/bi;->e:F

    add-float/2addr v0, p6

    iput v0, p0, Lmaps/y/bi;->e:F

    cmpl-float v0, p1, v1

    if-nez v0, :cond_0

    cmpl-float v0, p2, v1

    if-eqz v0, :cond_1

    :cond_0
    iput p3, p0, Lmaps/y/bi;->f:F

    iput p4, p0, Lmaps/y/bi;->g:F

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/y/bi;->h:Z

    iget-object v0, p0, Lmaps/y/bi;->j:[F

    const/4 v1, 0x0

    iget-object v2, p0, Lmaps/y/bi;->a:Lmaps/bq/a;

    invoke-virtual {v2}, Lmaps/bq/a;->a()F

    move-result v2

    iget v3, p0, Lmaps/y/bi;->b:F

    add-float/2addr v2, v3

    invoke-static {}, Lmaps/y/q;->d()F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    const/high16 v3, 0x40000000

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/y/bi;->j:[F

    const/4 v1, 0x1

    iget-object v2, p0, Lmaps/y/bi;->a:Lmaps/bq/a;

    invoke-virtual {v2}, Lmaps/bq/a;->e()F

    move-result v2

    iget v3, p0, Lmaps/y/bi;->c:F

    add-float/2addr v2, v3

    invoke-static {v2}, Lmaps/y/q;->b(F)F

    move-result v2

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/y/bi;->j:[F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()I
    .locals 1

    iget-boolean v0, p0, Lmaps/y/bi;->h:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
