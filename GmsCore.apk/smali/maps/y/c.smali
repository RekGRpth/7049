.class Lmaps/y/c;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/p/as;


# instance fields
.field final synthetic a:Lmaps/y/bo;

.field private b:Ljavax/microedition/khronos/opengles/GL;

.field private c:Ljavax/microedition/khronos/opengles/GL;


# direct methods
.method constructor <init>(Lmaps/y/bo;)V
    .locals 0

    iput-object p1, p0, Lmaps/y/c;->a:Lmaps/y/bo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljavax/microedition/khronos/opengles/GL;)Ljavax/microedition/khronos/opengles/GL;
    .locals 5

    iget-object v0, p0, Lmaps/y/c;->b:Ljavax/microedition/khronos/opengles/GL;

    if-ne p1, v0, :cond_0

    iget-object p1, p0, Lmaps/y/c;->c:Ljavax/microedition/khronos/opengles/GL;

    :goto_0
    return-object p1

    :cond_0
    iput-object p1, p0, Lmaps/y/c;->b:Ljavax/microedition/khronos/opengles/GL;

    const/4 v1, 0x0

    sget-boolean v0, Lmaps/ae/h;->S:Z

    if-eqz v0, :cond_2

    new-instance v2, Ljava/io/File;

    iget-object v0, p0, Lmaps/y/c;->a:Lmaps/y/bo;

    invoke-virtual {v0}, Lmaps/y/bo;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lmaps/bt/a;->c(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    const-string v3, "ogl_log.txt"

    invoke-direct {v2, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    :try_start_0
    new-instance v0, Ljava/io/FileWriter;

    invoke-direct {v0, v2}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    sget-boolean v1, Lmaps/ae/h;->T:Z

    if-nez v1, :cond_3

    sget-boolean v1, Lmaps/ae/h;->S:Z

    if-nez v1, :cond_3

    :goto_2
    sget-boolean v0, Lmaps/ae/h;->Q:Z

    if-eqz v0, :cond_1

    new-instance v0, Lmaps/j/d;

    invoke-direct {v0, p1}, Lmaps/j/d;-><init>(Ljavax/microedition/khronos/opengles/GL;)V

    move-object p1, v0

    :cond_1
    iput-object p1, p0, Lmaps/y/c;->c:Ljavax/microedition/khronos/opengles/GL;

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "GLState"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to open "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lmaps/bt/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    move-object v0, v1

    goto :goto_1

    :cond_3
    const/16 v1, 0x7003

    invoke-static {p1, v1, v0}, Landroid/opengl/GLDebugHelper;->wrap(Ljavax/microedition/khronos/opengles/GL;ILjava/io/Writer;)Ljavax/microedition/khronos/opengles/GL;

    move-result-object p1

    goto :goto_2
.end method
