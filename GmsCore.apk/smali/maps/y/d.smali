.class public Lmaps/y/d;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lmaps/y/bh;

.field private final b:Lmaps/y/ba;

.field private c:I

.field private d:Z


# direct methods
.method public constructor <init>(Lmaps/y/bh;Lmaps/y/ba;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/y/d;->a:Lmaps/y/bh;

    iput-object p2, p0, Lmaps/y/d;->b:Lmaps/y/ba;

    iput p3, p0, Lmaps/y/d;->c:I

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lmaps/y/d;->c:I

    return v0
.end method

.method public a(FFLmaps/t/bx;Lmaps/bq/d;)V
    .locals 3

    iget-object v0, p0, Lmaps/y/d;->a:Lmaps/y/bh;

    invoke-interface {v0, p1, p2, p3, p4}, Lmaps/y/bh;->c(FFLmaps/t/bx;Lmaps/bq/d;)I

    move-result v0

    iput v0, p0, Lmaps/y/d;->c:I

    iget v0, p0, Lmaps/y/d;->c:I

    const v1, 0x7fffffff

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lmaps/y/d;->b:Lmaps/y/ba;

    invoke-virtual {v0}, Lmaps/y/ba;->n()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p4}, Lmaps/bq/d;->n()F

    move-result v0

    const/high16 v1, 0x40a00000

    mul-float/2addr v0, v1

    mul-float/2addr v0, v0

    iget v1, p0, Lmaps/y/d;->c:I

    iget-object v2, p0, Lmaps/y/d;->b:Lmaps/y/ba;

    invoke-virtual {v2}, Lmaps/y/ba;->n()I

    move-result v2

    float-to-int v0, v0

    mul-int/2addr v0, v2

    add-int/2addr v0, v1

    iput v0, p0, Lmaps/y/d;->c:I

    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lmaps/y/d;->d:Z

    return-void
.end method

.method public b()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/y/d;->d:Z

    return v0
.end method

.method public c()V
    .locals 2

    iget-object v0, p0, Lmaps/y/d;->b:Lmaps/y/ba;

    iget-object v1, p0, Lmaps/y/d;->a:Lmaps/y/bh;

    invoke-virtual {v0, v1}, Lmaps/y/ba;->a(Lmaps/y/bh;)V

    return-void
.end method
