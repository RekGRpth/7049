.class public Lmaps/y/f;
.super Lmaps/y/bc;


# instance fields
.field private final a:Lmaps/y/am;

.field private final b:[F

.field private c:Lmaps/y/bh;

.field private d:Lmaps/t/bx;

.field private e:Landroid/view/View;

.field private f:Lmaps/y/ah;

.field private g:Landroid/graphics/Bitmap;

.field private h:Lmaps/cr/a;

.field private i:Lmaps/y/t;

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:F

.field private final o:I

.field private final p:Lmaps/w/v;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 1

    sget-object v0, Lmaps/y/am;->A:Lmaps/y/am;

    invoke-direct {p0, v0, p1}, Lmaps/y/f;-><init>(Lmaps/y/am;Landroid/content/res/Resources;)V

    return-void
.end method

.method private constructor <init>(Lmaps/y/am;Landroid/content/res/Resources;)V
    .locals 8

    invoke-direct {p0}, Lmaps/y/bc;-><init>()V

    const/16 v0, 0x8

    new-array v0, v0, [F

    iput-object v0, p0, Lmaps/y/f;->b:[F

    new-instance v0, Lmaps/t/bx;

    invoke-direct {v0}, Lmaps/t/bx;-><init>()V

    iput-object v0, p0, Lmaps/y/f;->d:Lmaps/t/bx;

    new-instance v0, Lmaps/w/v;

    const-wide/16 v1, 0xbb8

    const-wide/16 v3, 0x2710

    sget-object v5, Lmaps/w/j;->c:Lmaps/w/j;

    const/high16 v6, 0x10000

    const v7, 0x8000

    invoke-direct/range {v0 .. v7}, Lmaps/w/v;-><init>(JJLmaps/w/j;II)V

    iput-object v0, p0, Lmaps/y/f;->p:Lmaps/w/v;

    iput-object p1, p0, Lmaps/y/f;->a:Lmaps/y/am;

    if-nez p2, :cond_0

    const v0, 0xffff00

    :goto_0
    iput v0, p0, Lmaps/y/f;->o:I

    return-void

    :cond_0
    sget v0, Lmaps/ad/a;->H:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_0
.end method

.method private a(Ljavax/microedition/khronos/opengles/GL10;Lmaps/cr/c;Lmaps/al/o;Lmaps/al/q;)V
    .locals 3

    invoke-virtual {p3, p2}, Lmaps/al/o;->d(Lmaps/cr/c;)V

    invoke-virtual {p4, p2}, Lmaps/al/q;->d(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/y/f;->h:Lmaps/cr/a;

    invoke-virtual {v0, p1}, Lmaps/cr/a;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    const/4 v0, 0x5

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-interface {p1, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    return-void
.end method

.method private a(Lmaps/bq/d;)Z
    .locals 8

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lmaps/y/f;->g:Landroid/graphics/Bitmap;

    if-nez v2, :cond_0

    :goto_0
    return v1

    :cond_0
    iget-object v2, p0, Lmaps/y/f;->c:Lmaps/y/bh;

    invoke-interface {v2}, Lmaps/y/bh;->c_()Lmaps/t/bx;

    move-result-object v2

    invoke-virtual {p1}, Lmaps/bq/d;->u()Lmaps/t/bx;

    move-result-object v3

    invoke-virtual {p1}, Lmaps/bq/d;->h()Lmaps/t/bx;

    move-result-object v4

    invoke-static {v3, v4, v2}, Lmaps/t/bx;->d(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)F

    move-result v3

    invoke-virtual {p1, v2}, Lmaps/bq/d;->b(Lmaps/t/bx;)[I

    move-result-object v2

    aget v4, v2, v1

    iget-object v5, p0, Lmaps/y/f;->c:Lmaps/y/bh;

    invoke-interface {v5}, Lmaps/y/bh;->o_()I

    move-result v5

    add-int/2addr v4, v5

    aput v4, v2, v1

    aget v4, v2, v0

    int-to-float v4, v4

    iget-object v5, p0, Lmaps/y/f;->c:Lmaps/y/bh;

    invoke-interface {v5}, Lmaps/y/bh;->h()I

    move-result v5

    int-to-float v5, v5

    div-float v3, v5, v3

    sub-float v3, v4, v3

    float-to-int v3, v3

    aput v3, v2, v0

    iget-object v3, p0, Lmaps/y/f;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    iget-object v4, p0, Lmaps/y/f;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    aget v5, v2, v1

    sub-int/2addr v5, v3

    aget v6, v2, v1

    add-int/2addr v3, v6

    aget v6, v2, v0

    sub-int/2addr v6, v4

    aget v2, v2, v0

    add-int/2addr v2, v4

    invoke-virtual {p1}, Lmaps/bq/d;->l()I

    move-result v4

    invoke-virtual {p1}, Lmaps/bq/d;->m()I

    move-result v7

    if-ltz v3, :cond_1

    if-ge v5, v4, :cond_1

    if-ltz v2, :cond_1

    if-ge v6, v7, :cond_1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private c(FFLmaps/bq/d;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lmaps/y/f;->c:Lmaps/y/bh;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lmaps/y/f;->c:Lmaps/y/bh;

    invoke-interface {v2}, Lmaps/y/bh;->c_()Lmaps/t/bx;

    move-result-object v2

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    :cond_1
    :goto_0
    return v0

    :cond_2
    iget-object v2, p0, Lmaps/y/f;->c:Lmaps/y/bh;

    invoke-interface {v2}, Lmaps/y/bh;->c_()Lmaps/t/bx;

    move-result-object v2

    invoke-virtual {p3, v2}, Lmaps/bq/d;->b(Lmaps/t/bx;)[I

    move-result-object v2

    iget-object v3, p0, Lmaps/y/f;->f:Lmaps/y/ah;

    aget v4, v2, v1

    int-to-float v4, v4

    aget v5, v2, v0

    int-to-float v5, v5

    invoke-virtual {v3, v4, v5}, Lmaps/y/ah;->a(FF)V

    aget v3, v2, v1

    iget v4, p0, Lmaps/y/f;->l:I

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    iget v4, p0, Lmaps/y/f;->l:I

    add-int/2addr v4, v3

    int-to-float v3, v3

    cmpg-float v3, p1, v3

    if-ltz v3, :cond_3

    int-to-float v3, v4

    cmpl-float v3, p1, v3

    if-lez v3, :cond_4

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    aget v2, v2, v0

    iget-object v3, p0, Lmaps/y/f;->c:Lmaps/y/bh;

    invoke-interface {v3}, Lmaps/y/bh;->h()I

    move-result v3

    sub-int/2addr v2, v3

    iget v3, p0, Lmaps/y/f;->m:I

    sub-int v3, v2, v3

    int-to-float v3, v3

    cmpg-float v3, p2, v3

    if-ltz v3, :cond_5

    int-to-float v2, v2

    cmpl-float v2, p2, v2

    if-lez v2, :cond_1

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method private h()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lmaps/y/f;->h:Lmaps/cr/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/f;->h:Lmaps/cr/a;

    invoke-virtual {v0}, Lmaps/cr/a;->g()V

    iput-object v1, p0, Lmaps/y/f;->h:Lmaps/cr/a;

    :cond_0
    iget-object v0, p0, Lmaps/y/f;->g:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/y/f;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    iput-object v1, p0, Lmaps/y/f;->g:Landroid/graphics/Bitmap;

    :cond_1
    iget-object v0, p0, Lmaps/y/f;->f:Lmaps/y/ah;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/y/f;->f:Lmaps/y/ah;

    invoke-virtual {v0}, Lmaps/y/ah;->b()V

    :cond_2
    return-void
.end method

.method private i()Landroid/graphics/Bitmap;
    .locals 6

    const/high16 v2, -0x80000000

    const/4 v3, 0x0

    iget v0, p0, Lmaps/y/f;->j:I

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iget v1, p0, Lmaps/y/f;->k:I

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    iget-object v2, p0, Lmaps/y/f;->e:Landroid/view/View;

    invoke-virtual {v2, v0, v1}, Landroid/view/View;->measure(II)V

    iget-object v0, p0, Lmaps/y/f;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    iput v0, p0, Lmaps/y/f;->l:I

    iget-object v0, p0, Lmaps/y/f;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lmaps/y/f;->m:I

    iget-object v0, p0, Lmaps/y/f;->e:Landroid/view/View;

    iget v1, p0, Lmaps/y/f;->l:I

    iget v2, p0, Lmaps/y/f;->m:I

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/view/View;->layout(IIII)V

    iget v0, p0, Lmaps/y/f;->l:I

    const/16 v1, 0x40

    invoke-static {v0, v1}, Lmaps/cr/a;->a(II)I

    move-result v0

    iget v1, p0, Lmaps/y/f;->m:I

    const/16 v2, 0x20

    invoke-static {v1, v2}, Lmaps/cr/a;->a(II)I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/graphics/Bitmap;->eraseColor(I)V

    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iget v4, p0, Lmaps/y/f;->l:I

    sub-int v4, v0, v4

    div-int/lit8 v4, v4, 0x2

    iget v5, p0, Lmaps/y/f;->m:I

    sub-int v5, v1, v5

    int-to-float v4, v4

    int-to-float v5, v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v4, p0, Lmaps/y/f;->e:Landroid/view/View;

    invoke-virtual {v4, v3}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    iget-object v3, p0, Lmaps/y/f;->f:Lmaps/y/ah;

    int-to-float v0, v0

    int-to-float v1, v1

    invoke-virtual {v3, v0, v1}, Lmaps/y/ah;->d(FF)V

    return-object v2
.end method


# virtual methods
.method public declared-synchronized a(Lmaps/cr/c;Lmaps/af/f;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/y/f;->c:Lmaps/y/bh;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lmaps/y/f;->i()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/f;->g:Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 8

    const/high16 v7, 0x10000

    invoke-interface {p3}, Lmaps/af/s;->b()I

    move-result v0

    if-nez v0, :cond_1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/y/f;->c:Lmaps/y/bh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/f;->g:Landroid/graphics/Bitmap;

    if-nez v0, :cond_2

    :cond_0
    monitor-exit p0

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lmaps/y/f;->c:Lmaps/y/bh;

    invoke-interface {v0}, Lmaps/y/bh;->c_()Lmaps/t/bx;

    move-result-object v0

    iget-object v1, p0, Lmaps/y/f;->c:Lmaps/y/bh;

    invoke-interface {v1}, Lmaps/y/bh;->d_()Lmaps/t/bb;

    move-result-object v1

    if-eqz v1, :cond_3

    if-eqz v0, :cond_3

    invoke-static {}, Lmaps/b/r;->a()Lmaps/b/r;

    move-result-object v2

    invoke-virtual {v1}, Lmaps/t/bb;->a()Lmaps/t/bg;

    move-result-object v1

    invoke-virtual {v2, v1}, Lmaps/b/r;->e(Lmaps/t/bg;)Lmaps/b/z;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v2, p0, Lmaps/y/f;->d:Lmaps/t/bx;

    invoke-virtual {v2, v0}, Lmaps/t/bx;->b(Lmaps/t/bx;)V

    iget-object v2, p0, Lmaps/y/f;->d:Lmaps/t/bx;

    invoke-virtual {v1, p2, v0}, Lmaps/b/z;->a(Lmaps/bq/d;Lmaps/t/bx;)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {v2, v0}, Lmaps/t/bx;->b(I)V

    iget-object v0, p0, Lmaps/y/f;->d:Lmaps/t/bx;

    :cond_3
    invoke-virtual {p2}, Lmaps/bq/d;->C()Lmaps/t/u;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/t/bf;->a()Lmaps/t/bw;

    move-result-object v2

    if-nez v0, :cond_4

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_4
    :try_start_1
    invoke-virtual {v2, v0}, Lmaps/t/bw;->a(Lmaps/t/bx;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v1, v0}, Lmaps/t/bf;->a(Lmaps/t/bx;)Z

    move-result v1

    if-nez v1, :cond_6

    :cond_5
    iget-object v1, p0, Lmaps/y/f;->c:Lmaps/y/bh;

    invoke-interface {v1, p2}, Lmaps/y/bh;->a(Lmaps/bq/d;)Z

    move-result v1

    if-nez v1, :cond_6

    invoke-direct {p0, p2}, Lmaps/y/f;->a(Lmaps/bq/d;)Z

    move-result v1

    if-nez v1, :cond_6

    monitor-exit p0

    goto :goto_0

    :cond_6
    invoke-virtual {p2}, Lmaps/bq/d;->k()Z

    move-result v1

    if-nez v1, :cond_7

    iget-object v1, p0, Lmaps/y/f;->b:[F

    invoke-virtual {p2, v0, v1}, Lmaps/bq/d;->a(Lmaps/t/bx;[F)V

    iget-object v0, p0, Lmaps/y/f;->b:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lmaps/y/f;->b:[F

    const/4 v2, 0x1

    aget v1, v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p2, v0, v1}, Lmaps/bq/d;->d(FF)Lmaps/t/bx;

    move-result-object v0

    :cond_7
    if-nez v0, :cond_8

    const-string v0, "UI"

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Null point for ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lmaps/y/f;->b:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lmaps/y/f;->b:[F

    const/4 v4, 0x1

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "); "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "camera="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lmaps/bh/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    monitor-exit p0

    goto/16 :goto_0

    :cond_8
    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-virtual {p1}, Lmaps/cr/c;->r()V

    const/4 v2, 0x1

    const/16 v3, 0x303

    invoke-interface {v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v2, 0x2300

    const/16 v3, 0x2200

    const/16 v4, 0x1e01

    invoke-interface {v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    iget-object v2, p0, Lmaps/y/f;->h:Lmaps/cr/a;

    if-nez v2, :cond_9

    new-instance v2, Lmaps/cr/a;

    invoke-direct {v2, p1}, Lmaps/cr/a;-><init>(Lmaps/cr/c;)V

    iput-object v2, p0, Lmaps/y/f;->h:Lmaps/cr/a;

    iget-object v2, p0, Lmaps/y/f;->h:Lmaps/cr/a;

    iget-object v3, p0, Lmaps/y/f;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v3}, Lmaps/cr/a;->b(Landroid/graphics/Bitmap;)V

    :cond_9
    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    iget v2, p0, Lmaps/y/f;->n:F

    invoke-static {p1, p2, v0, v2}, Lmaps/af/d;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/t/bx;F)V

    invoke-static {v1, p2}, Lmaps/af/d;->a(Ljavax/microedition/khronos/opengles/GL10;Lmaps/bq/d;)V

    invoke-virtual {p2}, Lmaps/bq/d;->u()Lmaps/t/bx;

    move-result-object v2

    invoke-virtual {p2}, Lmaps/bq/d;->h()Lmaps/t/bx;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lmaps/t/bx;->d(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)F

    move-result v0

    iget-object v2, p0, Lmaps/y/f;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v0

    iget-object v3, p0, Lmaps/y/f;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v0

    iget-object v4, p0, Lmaps/y/f;->c:Lmaps/y/bh;

    invoke-interface {v4}, Lmaps/y/bh;->h()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v4, v0

    invoke-virtual {p2}, Lmaps/bq/d;->k()Z

    move-result v5

    if-eqz v5, :cond_b

    const/4 v0, 0x0

    :goto_1
    neg-float v4, v2

    const/high16 v5, 0x3f000000

    mul-float/2addr v4, v5

    iget-object v5, p0, Lmaps/y/f;->c:Lmaps/y/bh;

    invoke-interface {v5}, Lmaps/y/bh;->o_()I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v4, v5

    const/4 v5, 0x0

    iget-object v6, p0, Lmaps/y/f;->c:Lmaps/y/bh;

    invoke-interface {v6}, Lmaps/y/bh;->h()I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v0, v6

    invoke-interface {v1, v4, v5, v0}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    const/high16 v0, 0x3f800000

    invoke-interface {v1, v2, v0, v3}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    iget-object v0, p0, Lmaps/y/f;->f:Lmaps/y/ah;

    invoke-virtual {v0}, Lmaps/y/ah;->c()Z

    move-result v0

    if-eqz v0, :cond_c

    const/16 v0, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x2100

    invoke-interface {v1, v0, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    iget v0, p0, Lmaps/y/f;->o:I

    invoke-static {v1, v0}, Lmaps/y/f;->a(Ljavax/microedition/khronos/opengles/GL10;I)V

    iget-object v0, p1, Lmaps/cr/c;->g:Lmaps/al/o;

    iget-object v2, p1, Lmaps/cr/c;->d:Lmaps/al/q;

    invoke-direct {p0, v1, p1, v0, v2}, Lmaps/y/f;->a(Ljavax/microedition/khronos/opengles/GL10;Lmaps/cr/c;Lmaps/al/o;Lmaps/al/q;)V

    :cond_a
    :goto_2
    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lmaps/y/f;->f:Lmaps/y/ah;

    invoke-virtual {v0}, Lmaps/y/ah;->e()V

    goto/16 :goto_0

    :cond_b
    :try_start_2
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v5

    int-to-float v5, v5

    sub-float v4, v5, v4

    mul-float/2addr v0, v4

    goto :goto_1

    :cond_c
    iget-object v0, p0, Lmaps/y/f;->f:Lmaps/y/ah;

    invoke-virtual {v0}, Lmaps/y/ah;->a()Lmaps/y/af;

    move-result-object v0

    if-nez v0, :cond_d

    invoke-static {}, Lmaps/k/b;->a()Lmaps/k/b;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/k/b;->n()Z

    move-result v2

    if-eqz v2, :cond_d

    iget-object v2, p0, Lmaps/y/f;->p:Lmaps/w/v;

    invoke-virtual {v2, p1}, Lmaps/w/v;->a(Lmaps/cr/c;)I

    move-result v2

    if-ge v2, v7, :cond_d

    const/16 v3, 0x2300

    const/16 v4, 0x2200

    const/16 v5, 0x2100

    invoke-interface {v1, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    const/16 v3, 0x302

    const/16 v4, 0x303

    invoke-interface {v1, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/high16 v3, 0x10000

    const/high16 v4, 0x10000

    const/high16 v5, 0x10000

    invoke-interface {v1, v3, v4, v5, v2}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    :cond_d
    iget-object v2, p1, Lmaps/cr/c;->g:Lmaps/al/o;

    iget-object v3, p1, Lmaps/cr/c;->d:Lmaps/al/q;

    invoke-direct {p0, v1, p1, v2, v3}, Lmaps/y/f;->a(Ljavax/microedition/khronos/opengles/GL10;Lmaps/cr/c;Lmaps/al/o;Lmaps/al/q;)V

    if-eqz v0, :cond_a

    const/16 v2, 0x2300

    const/16 v3, 0x2200

    const/16 v4, 0x2100

    invoke-interface {v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    iget v2, p0, Lmaps/y/f;->o:I

    invoke-static {v1, v2}, Lmaps/y/f;->a(Ljavax/microedition/khronos/opengles/GL10;I)V

    iget-object v2, v0, Lmaps/y/af;->a:Lmaps/al/o;

    iget-object v0, v0, Lmaps/y/af;->b:Lmaps/al/q;

    invoke-direct {p0, v1, p1, v2, v0}, Lmaps/y/f;->a(Ljavax/microedition/khronos/opengles/GL10;Lmaps/cr/c;Lmaps/al/o;Lmaps/al/q;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method public declared-synchronized a(Lmaps/y/bh;Lmaps/y/ah;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iput-object p2, p0, Lmaps/y/f;->f:Lmaps/y/ah;

    iget-object v0, p0, Lmaps/y/f;->p:Lmaps/w/v;

    invoke-virtual {v0}, Lmaps/w/v;->a()V

    iget-object v0, p0, Lmaps/y/f;->c:Lmaps/y/bh;

    if-ne v0, p1, :cond_2

    iget-object v0, p0, Lmaps/y/f;->e:Landroid/view/View;

    invoke-virtual {p2}, Lmaps/y/ah;->d()Landroid/view/View;

    move-result-object v1

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lmaps/y/f;->c:Lmaps/y/bh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/f;->c:Lmaps/y/bh;

    invoke-interface {v0}, Lmaps/y/bh;->d()V

    :cond_0
    iget-object v0, p0, Lmaps/y/f;->e:Landroid/view/View;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lmaps/y/f;->h()V

    invoke-direct {p0}, Lmaps/y/f;->i()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/f;->g:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lmaps/y/f;->e:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    :cond_2
    :try_start_1
    iget-object v0, p0, Lmaps/y/f;->c:Lmaps/y/bh;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/y/f;->c:Lmaps/y/bh;

    invoke-interface {v0}, Lmaps/y/bh;->e()V

    :cond_3
    iput-object p1, p0, Lmaps/y/f;->c:Lmaps/y/bh;

    iget-object v0, p0, Lmaps/y/f;->c:Lmaps/y/bh;

    invoke-interface {v0}, Lmaps/y/bh;->d()V

    if-eqz p2, :cond_4

    invoke-virtual {p2}, Lmaps/y/ah;->d()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/f;->e:Landroid/view/View;

    :cond_4
    iget-object v0, p0, Lmaps/y/f;->e:Landroid/view/View;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lmaps/y/f;->h()V

    invoke-direct {p0}, Lmaps/y/f;->i()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/f;->g:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lmaps/y/f;->e:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lmaps/y/t;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lmaps/y/f;->i:Lmaps/y/t;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(FFLmaps/bq/d;)Z
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2, p3}, Lmaps/y/f;->c(FFLmaps/bq/d;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/f;->p:Lmaps/w/v;

    invoke-virtual {v0}, Lmaps/w/v;->a()V

    iget-object v0, p0, Lmaps/y/f;->f:Lmaps/y/ah;

    invoke-virtual {v0, p1, p2}, Lmaps/y/ah;->c(FF)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(FFLmaps/t/bx;Lmaps/bq/d;)Z
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2, p4}, Lmaps/y/f;->c(FFLmaps/bq/d;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/f;->f:Lmaps/y/ah;

    invoke-virtual {v0, p1, p2}, Lmaps/y/ah;->b(FF)V

    invoke-virtual {p0}, Lmaps/y/f;->f()V

    const/4 v0, 0x1

    monitor-exit p0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/y/f;->c:Lmaps/y/bh;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lmaps/y/f;->e()V

    invoke-virtual {p0}, Lmaps/y/f;->f()V

    :cond_1
    monitor-exit p0

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public declared-synchronized a(Lmaps/bq/d;Lmaps/cr/c;)Z
    .locals 2

    monitor-enter p0

    const/high16 v0, 0x3f800000

    :try_start_0
    invoke-virtual {p1}, Lmaps/bq/d;->p()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lmaps/bq/d;->a(FF)F

    move-result v0

    iput v0, p0, Lmaps/y/f;->n:F

    invoke-virtual {p1}, Lmaps/bq/d;->l()I

    move-result v0

    iget v1, p0, Lmaps/y/f;->j:I

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lmaps/bq/d;->m()I

    move-result v0

    iget v1, p0, Lmaps/y/f;->k:I

    if-eq v0, v1, :cond_1

    :cond_0
    invoke-virtual {p1}, Lmaps/bq/d;->l()I

    move-result v0

    iput v0, p0, Lmaps/y/f;->j:I

    invoke-virtual {p1}, Lmaps/bq/d;->m()I

    move-result v0

    iput v0, p0, Lmaps/y/f;->k:I

    iget-object v0, p0, Lmaps/y/f;->g:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lmaps/y/f;->h()V

    invoke-direct {p0}, Lmaps/y/f;->i()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/f;->g:Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    const/4 v0, 0x1

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a_()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/y/f;->f:Lmaps/y/ah;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/f;->f:Lmaps/y/ah;

    invoke-virtual {v0}, Lmaps/y/ah;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a_(Lmaps/cr/c;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lmaps/y/f;->h()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a_(FFLmaps/t/bx;Lmaps/bq/d;)Z
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2, p4}, Lmaps/y/f;->c(FFLmaps/bq/d;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/y/f;->i:Lmaps/y/t;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/f;->i:Lmaps/y/t;

    iget-object v1, p0, Lmaps/y/f;->c:Lmaps/y/bh;

    invoke-interface {v0, v1}, Lmaps/y/t;->b(Lmaps/y/bh;)V

    :cond_0
    const/4 v0, 0x1

    monitor-exit p0

    :goto_0
    return v0

    :cond_1
    monitor-exit p0

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b()Lmaps/y/am;
    .locals 1

    iget-object v0, p0, Lmaps/y/f;->a:Lmaps/y/am;

    return-object v0
.end method

.method public declared-synchronized d()Lmaps/y/bh;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/y/f;->c:Lmaps/y/bh;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public d(FFLmaps/t/bx;Lmaps/bq/d;)Z
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2, p4}, Lmaps/y/f;->c(FFLmaps/bq/d;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmaps/y/f;->a_()V

    const/4 v0, 0x1

    monitor-exit p0

    :goto_0
    return v0

    :cond_0
    monitor-exit p0

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public declared-synchronized e()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/y/f;->c:Lmaps/y/bh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/f;->c:Lmaps/y/bh;

    invoke-interface {v0}, Lmaps/y/bh;->e()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/y/f;->c:Lmaps/y/bh;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected f()V
    .locals 2

    invoke-super {p0}, Lmaps/y/bc;->f()V

    iget-object v0, p0, Lmaps/y/f;->i:Lmaps/y/t;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/f;->i:Lmaps/y/t;

    iget-object v1, p0, Lmaps/y/f;->c:Lmaps/y/bh;

    invoke-interface {v0, v1}, Lmaps/y/t;->a(Lmaps/y/bh;)V

    :cond_0
    return-void
.end method
