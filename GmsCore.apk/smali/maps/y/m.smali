.class public Lmaps/y/m;
.super Lmaps/y/ab;


# static fields
.field private static final d:Lmaps/t/ax;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lmaps/bl/a;

    const v1, 0x243d580

    const v2, 0x81b3200

    invoke-direct {v0, v1, v2}, Lmaps/bl/a;-><init>(II)V

    invoke-static {v0}, Lmaps/be/c;->a(Lmaps/bl/a;)Lmaps/t/bx;

    move-result-object v0

    new-instance v1, Lmaps/bl/a;

    const v2, 0x1f78a40

    const v3, 0x88601c0

    invoke-direct {v1, v2, v3}, Lmaps/bl/a;-><init>(II)V

    invoke-static {v1}, Lmaps/be/c;->a(Lmaps/bl/a;)Lmaps/t/bx;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/t/ax;->a(Lmaps/t/bx;Lmaps/t/bx;)Lmaps/t/ax;

    move-result-object v0

    sput-object v0, Lmaps/y/m;->d:Lmaps/t/ax;

    return-void
.end method

.method constructor <init>(Lmaps/o/c;Lmaps/bk/b;Lmaps/cq/b;IIILmaps/y/am;IIZZZZZZ)V
    .locals 0

    invoke-direct/range {p0 .. p15}, Lmaps/y/ab;-><init>(Lmaps/o/c;Lmaps/bk/b;Lmaps/cq/b;IIILmaps/y/am;IIZZZZZZ)V

    return-void
.end method

.method static a(Lmaps/b/r;FLmaps/t/bx;)Z
    .locals 3

    const/4 v1, 0x0

    const/4 v0, 0x1

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lmaps/b/r;->h()Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lmaps/b/r;->e()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    const/high16 v2, 0x41900000

    cmpg-float v2, p1, v2

    if-lez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 3

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lmaps/b/r;->a()Lmaps/b/r;

    move-result-object v0

    invoke-virtual {p2}, Lmaps/bq/d;->t()F

    move-result v1

    invoke-virtual {p2}, Lmaps/bq/d;->h()Lmaps/t/bx;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lmaps/y/m;->a(Lmaps/b/r;FLmaps/t/bx;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-super {p0, p1, p2, p3}, Lmaps/y/ab;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/y/m;->b:Z

    goto :goto_0
.end method

.method protected b_()Lmaps/p/j;
    .locals 1

    sget-object v0, Lmaps/p/j;->c:Lmaps/p/j;

    return-object v0
.end method
