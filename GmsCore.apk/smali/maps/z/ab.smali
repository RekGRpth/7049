.class public final Lmaps/z/ab;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lmaps/z/ab;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/pm/ApplicationInfo;)Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v1, "com.google.android.maps.v2.API_KEY"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "API key not found.  Check that <meta-data android:name=\"com.google.android.maps.v2.API_KEY\" android:value=\"your API key\"/> is in the <application> element of AndroidManifest.xml"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private static a(Landroid/content/Context;Landroid/content/res/Resources;Ljava/lang/String;)Lmaps/ak/n;
    .locals 4

    invoke-static {p0}, Lmaps/ae/c;->a(Landroid/content/Context;)Lmaps/ae/c;

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    const/16 v3, 0x80

    invoke-virtual {v0, v1, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    const-string v1, "2.1.0"

    invoke-static {p0, p1, p2, v1}, Lmaps/af/w;->a(Landroid/content/Context;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;)Lmaps/ak/n;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lmaps/ak/n;->a(Z)V

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ae/c;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lmaps/ak/n;->b(Ljava/lang/String;)V

    iget v2, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmaps/ak/n;->c(Ljava/lang/String;)V

    const-string v2, "SYSTEM"

    invoke-virtual {v1, v2}, Lmaps/ak/n;->a(Ljava/lang/String;)V

    sget-object v2, Lmaps/z/ab;->a:Ljava/lang/String;

    if-eqz v2, :cond_0

    sget-object v0, Lmaps/z/ab;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lmaps/ak/n;->e(Ljava/lang/String;)V

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    :cond_0
    invoke-static {v0}, Lmaps/z/ab;->a(Landroid/content/pm/ApplicationInfo;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v1}, Lmaps/z/ax;->a(Landroid/content/Context;Ljava/lang/String;Lmaps/ak/n;)Lmaps/z/ax;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/z/ax;->b()V

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;)V
    .locals 2

    invoke-static {}, Lmaps/az/c;->d()Lmaps/az/g;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmaps/az/g;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lmaps/cc/e;->b:I

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;Landroid/content/res/Resources;)V
    .locals 8

    const-class v6, Lmaps/z/ab;

    monitor-enter v6

    :try_start_0
    sget-boolean v0, Lmaps/z/ab;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :goto_0
    monitor-exit v6

    return-void

    :cond_0
    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lmaps/ap/q;->a(Z)V

    new-instance v5, Lmaps/bm/g;

    invoke-direct {v5, p0}, Lmaps/bm/g;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x6

    new-array v2, v0, [Lmaps/o/c;

    const/4 v0, 0x0

    sget-object v1, Lmaps/o/c;->a:Lmaps/o/c;

    aput-object v1, v2, v0

    const/4 v0, 0x1

    sget-object v1, Lmaps/o/c;->d:Lmaps/o/c;

    aput-object v1, v2, v0

    const/4 v0, 0x2

    sget-object v1, Lmaps/o/c;->f:Lmaps/o/c;

    aput-object v1, v2, v0

    const/4 v0, 0x3

    sget-object v1, Lmaps/o/c;->e:Lmaps/o/c;

    aput-object v1, v2, v0

    const/4 v0, 0x4

    sget-object v1, Lmaps/o/c;->o:Lmaps/o/c;

    aput-object v1, v2, v0

    const/4 v0, 0x5

    sget-object v1, Lmaps/o/c;->n:Lmaps/o/c;

    aput-object v1, v2, v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, p1, v3}, Lmaps/z/ab;->a(Landroid/content/Context;Landroid/content/res/Resources;Ljava/lang/String;)Lmaps/ak/n;

    move-result-object v7

    invoke-static {v7}, Lmaps/az/c;->a(Lmaps/ak/n;)V

    sget v4, Lmaps/ad/g;->a:I

    move-object v0, p0

    move-object v1, p1

    invoke-static/range {v0 .. v5}, Lmaps/af/w;->a(Landroid/content/Context;Landroid/content/res/Resources;[Lmaps/o/c;Ljava/lang/String;ILmaps/bh/g;)V

    invoke-static {p0}, Lmaps/au/a;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p0}, Lmaps/au/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lmaps/bm/f;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    invoke-static {p0, v7}, Lmaps/z/ab;->b(Landroid/content/Context;Lmaps/ak/n;)V

    :goto_1
    invoke-static {}, Lmaps/k/j;->a()Lmaps/ai/d;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ai/d;->d()V

    new-instance v0, Lmaps/bm/h;

    invoke-direct {v0}, Lmaps/bm/h;-><init>()V

    invoke-virtual {v0}, Lmaps/bm/h;->a()V

    invoke-static {p0}, Lmaps/z/ab;->a(Landroid/content/Context;)V

    const/4 v0, 0x1

    sput-boolean v0, Lmaps/z/ab;->b:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    :cond_2
    :try_start_2
    invoke-virtual {v7}, Lmaps/ak/n;->v()V

    const-string v0, "Google Maps application is missing."

    invoke-static {v0}, Lmaps/au/a;->a(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    invoke-virtual {v7}, Lmaps/ak/n;->v()V

    const-string v0, "Google Maps Android API only supports OpenGL ES 2.0 andabove. Please add <uses-feature android:glEsVersion=\"0x00020000\" android:required=\"true\" /> into AndroidManifest.xml"

    invoke-static {v0}, Lmaps/au/a;->a(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method static synthetic a(Landroid/content/Context;Lmaps/ak/n;)V
    .locals 0

    invoke-static {p0, p1}, Lmaps/z/ab;->b(Landroid/content/Context;Lmaps/ak/n;)V

    return-void
.end method

.method private static b(Landroid/content/Context;Lmaps/ak/n;)V
    .locals 2

    invoke-static {}, Lmaps/be/b;->e()V

    new-instance v0, Lmaps/z/bq;

    invoke-direct {v0, p0}, Lmaps/z/bq;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, Lmaps/be/b;->a(Landroid/content/Context;Lmaps/ak/n;Ljava/lang/Runnable;Z)V

    return-void
.end method
