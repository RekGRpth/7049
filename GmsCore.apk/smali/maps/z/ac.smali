.class final Lmaps/z/ac;
.super Lcom/google/android/gms/maps/model/internal/IPolylineDelegate$Stub;

# interfaces
.implements Lmaps/z/a;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lmaps/z/bi;

.field private c:F

.field private d:I

.field private final e:Ljava/util/List;

.field private final f:Ljava/util/List;

.field private final g:Ljava/util/List;

.field private h:F

.field private i:Z

.field private j:Z

.field private final k:Lmaps/bf/b;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/google/android/gms/maps/model/PolylineOptions;Lmaps/z/bi;Lmaps/bf/b;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/gms/maps/model/internal/IPolylineDelegate$Stub;-><init>()V

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmaps/z/ac;->f:Ljava/util/List;

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmaps/z/ac;->g:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolylineOptions;->getWidth()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "line width is negative"

    invoke-static {v0, v1}, Lmaps/ap/q;->a(ZLjava/lang/Object;)V

    iput-object p1, p0, Lmaps/z/ac;->a:Ljava/lang/String;

    iput-object p3, p0, Lmaps/z/ac;->b:Lmaps/z/bi;

    iput-object p4, p0, Lmaps/z/ac;->k:Lmaps/bf/b;

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolylineOptions;->getWidth()F

    move-result v0

    iput v0, p0, Lmaps/z/ac;->c:F

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolylineOptions;->getColor()I

    move-result v0

    iput v0, p0, Lmaps/z/ac;->d:I

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolylineOptions;->getZIndex()F

    move-result v0

    iput v0, p0, Lmaps/z/ac;->h:F

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolylineOptions;->isVisible()Z

    move-result v0

    iput-boolean v0, p0, Lmaps/z/ac;->i:Z

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolylineOptions;->isGeodesic()Z

    move-result v0

    iput-boolean v0, p0, Lmaps/z/ac;->j:Z

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolylineOptions;->getPoints()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lmaps/f/fd;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmaps/z/ac;->e:Ljava/util/List;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/util/List;FILjava/util/List;)V
    .locals 5

    invoke-interface {p3}, Ljava/util/List;->clear()V

    invoke-static {p2}, Lmaps/au/c;->a(I)I

    move-result v1

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/cg;

    invoke-virtual {v0}, Lmaps/t/cg;->b()I

    move-result v3

    if-lez v3, :cond_0

    new-instance v3, Lmaps/y/a;

    const/4 v4, 0x0

    invoke-direct {v3, v0, p1, v1, v4}, Lmaps/y/a;-><init>(Lmaps/t/cg;FILmaps/t/bg;)V

    invoke-interface {p3, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-void
.end method

.method static a(Ljava/util/List;Ljava/util/List;)V
    .locals 12

    const/high16 v11, 0x40000000

    const/high16 v10, 0x20000000

    const v9, 0x1fffffff

    const/high16 v8, -0x20000000

    invoke-interface {p1}, Ljava/util/List;->clear()V

    new-instance v1, Lmaps/t/by;

    invoke-direct {v1}, Lmaps/t/by;-><init>()V

    const/4 v0, 0x0

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v2, v1

    move-object v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v0}, Lmaps/au/c;->b(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/t/bx;

    move-result-object v0

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lmaps/t/bx;->f()I

    move-result v4

    invoke-virtual {v1}, Lmaps/t/bx;->f()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    if-gt v4, v10, :cond_1

    :cond_0
    invoke-virtual {v2, v0}, Lmaps/t/by;->a(Lmaps/t/bx;)Z

    move-object v1, v2

    :goto_1
    move-object v2, v1

    move-object v1, v0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lmaps/t/bx;->f()I

    move-result v4

    invoke-virtual {v1}, Lmaps/t/bx;->f()I

    move-result v5

    sub-int/2addr v4, v5

    if-le v4, v10, :cond_2

    invoke-virtual {v0}, Lmaps/t/bx;->g()I

    move-result v4

    invoke-virtual {v1}, Lmaps/t/bx;->g()I

    move-result v5

    sub-int/2addr v4, v5

    int-to-float v4, v4

    invoke-virtual {v0}, Lmaps/t/bx;->f()I

    move-result v5

    invoke-virtual {v1}, Lmaps/t/bx;->f()I

    move-result v6

    sub-int/2addr v5, v6

    sub-int/2addr v5, v11

    int-to-float v5, v5

    div-float/2addr v4, v5

    new-instance v5, Lmaps/t/bx;

    invoke-virtual {v1}, Lmaps/t/bx;->g()I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {v1}, Lmaps/t/bx;->f()I

    move-result v1

    sub-int v1, v8, v1

    int-to-float v1, v1

    mul-float/2addr v1, v4

    add-float/2addr v1, v6

    float-to-int v1, v1

    invoke-direct {v5, v8, v1}, Lmaps/t/bx;-><init>(II)V

    new-instance v6, Lmaps/t/bx;

    invoke-virtual {v0}, Lmaps/t/bx;->g()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0}, Lmaps/t/bx;->f()I

    move-result v7

    sub-int v7, v10, v7

    int-to-float v7, v7

    mul-float/2addr v4, v7

    add-float/2addr v1, v4

    float-to-int v1, v1

    invoke-direct {v6, v9, v1}, Lmaps/t/bx;-><init>(II)V

    invoke-virtual {v2, v5}, Lmaps/t/by;->a(Lmaps/t/bx;)Z

    invoke-virtual {v2}, Lmaps/t/by;->c()Lmaps/t/cg;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lmaps/t/by;

    invoke-direct {v1}, Lmaps/t/by;-><init>()V

    invoke-virtual {v1, v6}, Lmaps/t/by;->a(Lmaps/t/bx;)Z

    invoke-virtual {v1, v0}, Lmaps/t/by;->a(Lmaps/t/bx;)Z

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, Lmaps/t/bx;->f()I

    move-result v4

    invoke-virtual {v1}, Lmaps/t/bx;->f()I

    move-result v5

    sub-int/2addr v4, v5

    if-ge v4, v8, :cond_3

    invoke-virtual {v1}, Lmaps/t/bx;->g()I

    move-result v4

    invoke-virtual {v0}, Lmaps/t/bx;->g()I

    move-result v5

    sub-int/2addr v4, v5

    int-to-float v4, v4

    invoke-virtual {v1}, Lmaps/t/bx;->f()I

    move-result v5

    invoke-virtual {v0}, Lmaps/t/bx;->f()I

    move-result v6

    sub-int/2addr v5, v6

    sub-int/2addr v5, v11

    int-to-float v5, v5

    div-float/2addr v4, v5

    new-instance v5, Lmaps/t/bx;

    invoke-virtual {v1}, Lmaps/t/bx;->g()I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {v1}, Lmaps/t/bx;->f()I

    move-result v1

    sub-int v1, v9, v1

    int-to-float v1, v1

    mul-float/2addr v1, v4

    add-float/2addr v1, v6

    float-to-int v1, v1

    invoke-direct {v5, v9, v1}, Lmaps/t/bx;-><init>(II)V

    new-instance v6, Lmaps/t/bx;

    invoke-virtual {v0}, Lmaps/t/bx;->g()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0}, Lmaps/t/bx;->f()I

    move-result v7

    sub-int v7, v8, v7

    int-to-float v7, v7

    mul-float/2addr v4, v7

    add-float/2addr v1, v4

    float-to-int v1, v1

    invoke-direct {v6, v8, v1}, Lmaps/t/bx;-><init>(II)V

    invoke-virtual {v2, v5}, Lmaps/t/by;->a(Lmaps/t/bx;)Z

    invoke-virtual {v2}, Lmaps/t/by;->c()Lmaps/t/cg;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lmaps/t/by;

    invoke-direct {v1}, Lmaps/t/by;-><init>()V

    invoke-virtual {v1, v6}, Lmaps/t/by;->a(Lmaps/t/bx;)Z

    invoke-virtual {v1, v0}, Lmaps/t/by;->a(Lmaps/t/bx;)Z

    goto/16 :goto_1

    :cond_3
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_4
    invoke-virtual {v2}, Lmaps/t/by;->c()Lmaps/t/cg;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public a(I)V
    .locals 0

    return-void
.end method

.method public declared-synchronized a(Lmaps/bq/d;Lmaps/cr/c;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/z/ac;->i:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    :cond_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-object v0, p0, Lmaps/z/ac;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/y/a;

    invoke-virtual {v0, p1, p2}, Lmaps/y/a;->a(Lmaps/bq/d;Lmaps/cr/c;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lmaps/cr/c;)V
    .locals 0

    return-void
.end method

.method public a(Lmaps/cr/c;Lmaps/af/f;)V
    .locals 0

    return-void
.end method

.method public declared-synchronized a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/z/ac;->i:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    :cond_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-object v0, p0, Lmaps/z/ac;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/y/a;

    invoke-virtual {v0, p1, p2, p3}, Lmaps/y/a;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Z)V
    .locals 0

    return-void
.end method

.method public b()V
    .locals 0

    return-void
.end method

.method c()V
    .locals 4

    iget-boolean v0, p0, Lmaps/z/ac;->j:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/z/ac;->e:Ljava/util/List;

    invoke-static {v0}, Lmaps/au/d;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lmaps/z/ac;->f:Ljava/util/List;

    invoke-static {v0, v1}, Lmaps/z/ac;->a(Ljava/util/List;Ljava/util/List;)V

    iget-object v0, p0, Lmaps/z/ac;->f:Ljava/util/List;

    iget v1, p0, Lmaps/z/ac;->c:F

    iget v2, p0, Lmaps/z/ac;->d:I

    iget-object v3, p0, Lmaps/z/ac;->g:Ljava/util/List;

    invoke-static {v0, v1, v2, v3}, Lmaps/z/ac;->a(Ljava/util/List;FILjava/util/List;)V

    return-void

    :cond_0
    iget-object v0, p0, Lmaps/z/ac;->e:Ljava/util/List;

    goto :goto_0
.end method

.method public equalsRemote(Lcom/google/android/gms/maps/model/internal/IPolylineDelegate;)Z
    .locals 1

    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getColor()I
    .locals 1

    iget-object v0, p0, Lmaps/z/ac;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/z/ac;->d:I

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/z/ac;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getPoints()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lmaps/z/ac;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/z/ac;->e:Ljava/util/List;

    invoke-static {v0}, Lmaps/f/fd;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getWidth()F
    .locals 1

    iget-object v0, p0, Lmaps/z/ac;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/z/ac;->c:F

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getZIndex()F
    .locals 1

    iget-object v0, p0, Lmaps/z/ac;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    iget v0, p0, Lmaps/z/ac;->h:F

    return v0
.end method

.method public hashCodeRemote()I
    .locals 1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public isGeodesic()Z
    .locals 1

    iget-object v0, p0, Lmaps/z/ac;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/z/ac;->j:Z

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public isVisible()Z
    .locals 1

    iget-object v0, p0, Lmaps/z/ac;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/z/ac;->i:Z

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public remove()V
    .locals 2

    iget-object v0, p0, Lmaps/z/ac;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    iget-object v0, p0, Lmaps/z/ac;->k:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->n:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iget-object v1, p0, Lmaps/z/ac;->b:Lmaps/z/bi;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/z/ac;->b:Lmaps/z/bi;

    invoke-virtual {v0, p0}, Lmaps/z/bi;->a(Lmaps/z/a;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/z/ac;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->h()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public setColor(I)V
    .locals 3

    iget-object v0, p0, Lmaps/z/ac;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    iget-object v0, p0, Lmaps/z/ac;->k:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->q:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lmaps/z/ac;->d:I

    iget-object v0, p0, Lmaps/z/ac;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/y/a;

    invoke-static {p1}, Lmaps/au/c;->a(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lmaps/y/a;->a_(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lmaps/z/ac;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->h()V

    return-void
.end method

.method public setGeodesic(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/z/ac;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    iget-object v0, p0, Lmaps/z/ac;->k:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->t:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/z/ac;->j:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lmaps/z/ac;->j:Z

    invoke-virtual {p0}, Lmaps/z/ac;->c()V

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setPoints(Ljava/util/List;)V
    .locals 2

    iget-object v0, p0, Lmaps/z/ac;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    iget-object v0, p0, Lmaps/z/ac;->k:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->o:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/z/ac;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lmaps/z/ac;->e:Ljava/util/List;

    invoke-static {v0, p1}, Lmaps/f/fa;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    invoke-virtual {p0}, Lmaps/z/ac;->c()V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/z/ac;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->h()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public setVisible(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/z/ac;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    iget-object v0, p0, Lmaps/z/ac;->k:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->s:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lmaps/z/ac;->i:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/z/ac;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->h()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public setWidth(F)V
    .locals 2

    iget-object v0, p0, Lmaps/z/ac;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    iget-object v0, p0, Lmaps/z/ac;->k:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->p:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "line width is negative"

    invoke-static {v0, v1}, Lmaps/ap/q;->a(ZLjava/lang/Object;)V

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lmaps/z/ac;->c:F

    iget-object v0, p0, Lmaps/z/ac;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/y/a;

    invoke-virtual {v0, p1}, Lmaps/y/a;->a(F)V

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lmaps/z/ac;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->h()V

    return-void
.end method

.method public setZIndex(F)V
    .locals 2

    iget-object v0, p0, Lmaps/z/ac;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    iget-object v0, p0, Lmaps/z/ac;->k:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->r:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iget-object v1, p0, Lmaps/z/ac;->b:Lmaps/z/bi;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/z/ac;->b:Lmaps/z/bi;

    invoke-virtual {v0, p0}, Lmaps/z/bi;->b(Lmaps/z/a;)V

    iput p1, p0, Lmaps/z/ac;->h:F

    iget-object v0, p0, Lmaps/z/ac;->b:Lmaps/z/bi;

    invoke-virtual {v0, p0}, Lmaps/z/bi;->c(Lmaps/z/a;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/z/ac;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->h()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lmaps/z/ac;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    monitor-enter p0

    :try_start_0
    invoke-static {p0}, Lmaps/ap/e;->a(Ljava/lang/Object;)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "points"

    iget-object v2, p0, Lmaps/z/ac;->e:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "width"

    iget v2, p0, Lmaps/z/ac;->c:F

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;F)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "color"

    iget v2, p0, Lmaps/z/ac;->d:I

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;I)Lmaps/ap/n;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ap/n;->toString()Ljava/lang/String;

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
