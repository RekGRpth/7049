.class final Lmaps/z/ae;
.super Lcom/google/android/gms/maps/internal/IOnCameraChangeListener$Stub;


# instance fields
.field private final a:Lmaps/aa/h;

.field private final b:Lmaps/i/c;


# direct methods
.method constructor <init>(Lmaps/i/c;Lmaps/aa/h;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/maps/internal/IOnCameraChangeListener$Stub;-><init>()V

    iput-object p2, p0, Lmaps/z/ae;->a:Lmaps/aa/h;

    iput-object p1, p0, Lmaps/z/ae;->b:Lmaps/i/c;

    return-void
.end method


# virtual methods
.method public onCameraChange(Lcom/google/android/gms/maps/model/CameraPosition;)V
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lmaps/z/ae;->a:Lmaps/aa/h;

    iget v0, p1, Lcom/google/android/gms/maps/model/CameraPosition;->zoom:F

    iget-object v4, p0, Lmaps/z/ae;->b:Lmaps/i/c;

    iget-object v5, p1, Lcom/google/android/gms/maps/model/CameraPosition;->target:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v5}, Lmaps/au/c;->b(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/t/bx;

    move-result-object v5

    invoke-interface {v4, v5}, Lmaps/i/c;->a(Lmaps/t/bx;)F

    move-result v4

    cmpg-float v0, v0, v4

    if-gez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-interface {v3, v0}, Lmaps/aa/h;->b(Z)V

    iget-object v0, p0, Lmaps/z/ae;->a:Lmaps/aa/h;

    iget v3, p1, Lcom/google/android/gms/maps/model/CameraPosition;->zoom:F

    iget-object v4, p0, Lmaps/z/ae;->b:Lmaps/i/c;

    invoke-interface {v4}, Lmaps/i/c;->b()F

    move-result v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_1

    :goto_1
    invoke-interface {v0, v1}, Lmaps/aa/h;->c(Z)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method
