.class Lmaps/z/ak;
.super Lcom/google/android/gms/maps/internal/IUiSettingsDelegate$Stub;


# instance fields
.field final synthetic a:Lmaps/z/ag;


# direct methods
.method constructor <init>(Lmaps/z/ag;)V
    .locals 0

    iput-object p1, p0, Lmaps/z/ak;->a:Lmaps/z/ag;

    invoke-direct {p0}, Lcom/google/android/gms/maps/internal/IUiSettingsDelegate$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public isCompassEnabled()Z
    .locals 1

    iget-object v0, p0, Lmaps/z/ak;->a:Lmaps/z/ag;

    invoke-virtual {v0}, Lmaps/z/ag;->isCompassEnabled()Z

    move-result v0

    return v0
.end method

.method public isMyLocationButtonEnabled()Z
    .locals 1

    iget-object v0, p0, Lmaps/z/ak;->a:Lmaps/z/ag;

    invoke-virtual {v0}, Lmaps/z/ag;->isMyLocationButtonEnabled()Z

    move-result v0

    return v0
.end method

.method public isRotateGesturesEnabled()Z
    .locals 1

    iget-object v0, p0, Lmaps/z/ak;->a:Lmaps/z/ag;

    invoke-virtual {v0}, Lmaps/z/ag;->isRotateGesturesEnabled()Z

    move-result v0

    return v0
.end method

.method public isScrollGesturesEnabled()Z
    .locals 1

    iget-object v0, p0, Lmaps/z/ak;->a:Lmaps/z/ag;

    invoke-virtual {v0}, Lmaps/z/ag;->isScrollGesturesEnabled()Z

    move-result v0

    return v0
.end method

.method public isTiltGesturesEnabled()Z
    .locals 1

    iget-object v0, p0, Lmaps/z/ak;->a:Lmaps/z/ag;

    invoke-virtual {v0}, Lmaps/z/ag;->isTiltGesturesEnabled()Z

    move-result v0

    return v0
.end method

.method public isZoomControlsEnabled()Z
    .locals 1

    iget-object v0, p0, Lmaps/z/ak;->a:Lmaps/z/ag;

    invoke-virtual {v0}, Lmaps/z/ag;->isZoomControlsEnabled()Z

    move-result v0

    return v0
.end method

.method public isZoomGesturesEnabled()Z
    .locals 1

    iget-object v0, p0, Lmaps/z/ak;->a:Lmaps/z/ag;

    invoke-virtual {v0}, Lmaps/z/ag;->isZoomControlsEnabled()Z

    move-result v0

    return v0
.end method

.method public setAllGesturesEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/z/ak;->a:Lmaps/z/ag;

    invoke-virtual {v0, p1}, Lmaps/z/ag;->setAllGesturesEnabled(Z)V

    return-void
.end method

.method public setCompassEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/z/ak;->a:Lmaps/z/ag;

    invoke-virtual {v0, p1}, Lmaps/z/ag;->setCompassEnabled(Z)V

    return-void
.end method

.method public setMyLocationButtonEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/z/ak;->a:Lmaps/z/ag;

    invoke-virtual {v0, p1}, Lmaps/z/ag;->setMyLocationButtonEnabled(Z)V

    return-void
.end method

.method public setRotateGesturesEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/z/ak;->a:Lmaps/z/ag;

    invoke-virtual {v0, p1}, Lmaps/z/ag;->setRotateGesturesEnabled(Z)V

    return-void
.end method

.method public setScrollGesturesEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/z/ak;->a:Lmaps/z/ag;

    invoke-virtual {v0, p1}, Lmaps/z/ag;->setScrollGesturesEnabled(Z)V

    return-void
.end method

.method public setTiltGesturesEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/z/ak;->a:Lmaps/z/ag;

    invoke-virtual {v0, p1}, Lmaps/z/ag;->setTiltGesturesEnabled(Z)V

    return-void
.end method

.method public setZoomControlsEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/z/ak;->a:Lmaps/z/ag;

    invoke-virtual {v0, p1}, Lmaps/z/ag;->setZoomControlsEnabled(Z)V

    return-void
.end method

.method public setZoomGesturesEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/z/ak;->a:Lmaps/z/ag;

    invoke-virtual {v0, p1}, Lmaps/z/ag;->setZoomGesturesEnabled(Z)V

    return-void
.end method
