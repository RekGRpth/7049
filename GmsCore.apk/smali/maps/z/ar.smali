.class public final Lmaps/z/ar;
.super Lcom/google/android/gms/maps/model/internal/ITileOverlayDelegate$Stub;

# interfaces
.implements Lmaps/z/a;
.implements Lmaps/z/au;


# static fields
.field private static final m:Lmaps/cq/b;


# instance fields
.field protected volatile a:Lmaps/af/f;

.field private final b:I

.field private final c:I

.field private final d:Z

.field private final e:Lmaps/z/az;

.field private final f:Ljava/util/ArrayList;

.field private final g:Lmaps/z/o;

.field private h:Lmaps/cq/a;

.field private i:Z

.field private volatile j:Z

.field private k:Z

.field private final l:Lmaps/cq/b;

.field private final n:Ljava/util/Set;

.field private final o:Lmaps/o/o;

.field private p:Ljava/lang/ref/WeakReference;

.field private q:Z

.field private r:F

.field private final s:Ljava/lang/String;

.field private final t:Lmaps/z/bi;

.field private final u:Lmaps/bf/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lmaps/cq/c;

    invoke-direct {v0}, Lmaps/cq/c;-><init>()V

    sput-object v0, Lmaps/z/ar;->m:Lmaps/cq/b;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Lmaps/z/bi;Lmaps/bf/b;Lmaps/z/az;Lmaps/cq/b;IIZZF)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/maps/model/internal/ITileOverlayDelegate$Stub;-><init>()V

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmaps/z/ar;->f:Ljava/util/ArrayList;

    new-instance v0, Lmaps/z/o;

    invoke-direct {v0}, Lmaps/z/o;-><init>()V

    iput-object v0, p0, Lmaps/z/ar;->g:Lmaps/z/o;

    invoke-static {}, Lmaps/f/a;->c()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lmaps/z/ar;->n:Ljava/util/Set;

    iput-object p1, p0, Lmaps/z/ar;->s:Ljava/lang/String;

    iput-object p2, p0, Lmaps/z/ar;->t:Lmaps/z/bi;

    iput-object p3, p0, Lmaps/z/ar;->u:Lmaps/bf/b;

    iput-object p4, p0, Lmaps/z/ar;->e:Lmaps/z/az;

    iput-object p5, p0, Lmaps/z/ar;->l:Lmaps/cq/b;

    iput p6, p0, Lmaps/z/ar;->b:I

    iput p7, p0, Lmaps/z/ar;->c:I

    iput-boolean p8, p0, Lmaps/z/ar;->d:Z

    iput-boolean p9, p0, Lmaps/z/ar;->q:Z

    iput p10, p0, Lmaps/z/ar;->r:F

    new-instance v0, Lmaps/o/o;

    invoke-direct {v0}, Lmaps/o/o;-><init>()V

    iput-object v0, p0, Lmaps/z/ar;->o:Lmaps/o/o;

    return-void
.end method

.method public static a(Ljava/lang/String;Lcom/google/android/gms/maps/model/TileOverlayOptions;Landroid/content/res/Resources;Lmaps/z/bi;Ljava/util/concurrent/ScheduledExecutorService;Lmaps/bf/b;)Lmaps/z/ar;
    .locals 11

    const/16 v7, 0x14c

    invoke-static {p2, v7}, Lmaps/y/ab;->a(Landroid/content/res/Resources;I)I

    move-result v6

    const/4 v8, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/TileOverlayOptions;->isVisible()Z

    move-result v9

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/TileOverlayOptions;->getZIndex()F

    move-result v10

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/TileOverlayOptions;->getTileProvider()Lcom/google/android/gms/maps/model/TileProvider;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "TileOverlay.Options must specify a TileProvider"

    invoke-static {v0, v1}, Lmaps/ap/q;->a(ZLjava/lang/Object;)V

    new-instance v4, Lmaps/z/az;

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/TileOverlayOptions;->getTileProvider()Lcom/google/android/gms/maps/model/TileProvider;

    move-result-object v0

    invoke-direct {v4, v0, p0, p4}, Lmaps/z/az;-><init>(Lcom/google/android/gms/maps/model/TileProvider;Ljava/lang/String;Ljava/util/concurrent/ScheduledExecutorService;)V

    new-instance v0, Lmaps/z/ar;

    sget-object v5, Lmaps/z/ar;->m:Lmaps/cq/b;

    move-object v1, p0

    move-object v2, p3

    move-object/from16 v3, p5

    invoke-direct/range {v0 .. v10}, Lmaps/z/ar;-><init>(Ljava/lang/String;Lmaps/z/bi;Lmaps/bf/b;Lmaps/z/az;Lmaps/cq/b;IIZZF)V

    invoke-virtual {v4, v0}, Lmaps/z/az;->a(Lmaps/z/au;)V

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lmaps/z/ar;->e:Lmaps/z/az;

    invoke-virtual {v0}, Lmaps/z/az;->e()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/z/ar;->j:Z

    return-void
.end method

.method public a(I)V
    .locals 1

    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lmaps/z/ar;->i:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lmaps/bq/d;Lmaps/cr/c;)V
    .locals 5

    const/4 v4, 0x1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/z/ar;->q:Z

    if-nez v0, :cond_0

    monitor-exit p0

    :goto_0
    return-void

    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-boolean v4, p0, Lmaps/z/ar;->k:Z

    iget-object v0, p0, Lmaps/z/ar;->h:Lmaps/cq/a;

    invoke-interface {v0, p1}, Lmaps/cq/a;->a(Lmaps/bq/d;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v4, :cond_1

    iget-object v1, p0, Lmaps/z/ar;->g:Lmaps/z/o;

    invoke-virtual {p1}, Lmaps/bq/d;->i()Lmaps/t/bx;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmaps/z/o;->a(Lmaps/t/bx;)V

    iget-object v1, p0, Lmaps/z/ar;->g:Lmaps/z/o;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_1
    iget-object v1, p0, Lmaps/z/ar;->n:Ljava/util/Set;

    iget-object v2, p0, Lmaps/z/ar;->f:Ljava/util/ArrayList;

    invoke-interface {v1, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    iget-object v1, p0, Lmaps/z/ar;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    iget-object v1, p0, Lmaps/z/ar;->e:Lmaps/z/az;

    invoke-virtual {v1}, Lmaps/z/az;->a()V

    iget-boolean v2, p0, Lmaps/z/ar;->i:Z

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/ah;

    if-eqz v2, :cond_4

    iget-object v1, p0, Lmaps/z/ar;->e:Lmaps/z/az;

    invoke-virtual {v1, v0}, Lmaps/z/az;->a(Lmaps/t/ah;)Lmaps/l/al;

    move-result-object v0

    move-object v1, v0

    :goto_2
    if-eqz v1, :cond_5

    iget-object v0, p0, Lmaps/z/ar;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/z/ar;->p:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/cr/c;

    invoke-interface {v1, p1, v0}, Lmaps/l/al;->a(Lmaps/bq/d;Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/z/ar;->n:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-interface {v1, v4}, Lmaps/l/al;->a(Z)V

    :cond_2
    iget-object v0, p0, Lmaps/z/ar;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    iget-object v0, p0, Lmaps/z/ar;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lmaps/z/ar;->b:I

    if-ne v0, v1, :cond_5

    :cond_3
    iget-boolean v0, p0, Lmaps/z/ar;->i:Z

    iput-boolean v0, p0, Lmaps/z/ar;->j:Z

    iget-object v0, p0, Lmaps/z/ar;->n:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/al;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Lmaps/l/al;->a(Z)V

    goto :goto_3

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_4
    iget-object v1, p0, Lmaps/z/ar;->e:Lmaps/z/az;

    invoke-virtual {v1, v0}, Lmaps/z/az;->b(Lmaps/t/ah;)Lmaps/l/al;

    move-result-object v0

    move-object v1, v0

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lmaps/z/ar;->e:Lmaps/z/az;

    invoke-virtual {v0}, Lmaps/z/az;->b()V

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lmaps/z/ar;->n:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    goto/16 :goto_0
.end method

.method public a(Lmaps/cr/c;)V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/z/ar;->p:Ljava/lang/ref/WeakReference;

    iput-object v0, p0, Lmaps/z/ar;->a:Lmaps/af/f;

    iget-object v0, p0, Lmaps/z/ar;->e:Lmaps/z/az;

    invoke-virtual {v0}, Lmaps/z/az;->c()V

    iget-object v0, p0, Lmaps/z/ar;->e:Lmaps/z/az;

    invoke-virtual {v0}, Lmaps/z/az;->d()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/z/ar;->j:Z

    return-void
.end method

.method public a(Lmaps/cr/c;Lmaps/af/f;)V
    .locals 5

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lmaps/z/ar;->p:Ljava/lang/ref/WeakReference;

    iget-object v0, p0, Lmaps/z/ar;->e:Lmaps/z/az;

    invoke-virtual {v0, p1}, Lmaps/z/az;->a(Lmaps/cr/c;)V

    iput-object p2, p0, Lmaps/z/ar;->a:Lmaps/af/f;

    iget-object v0, p0, Lmaps/z/ar;->h:Lmaps/cq/a;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/z/ar;->l:Lmaps/cq/b;

    sget-object v1, Lmaps/o/c;->d:Lmaps/o/c;

    iget v2, p0, Lmaps/z/ar;->c:I

    iget-boolean v3, p0, Lmaps/z/ar;->d:Z

    iget-object v4, p0, Lmaps/z/ar;->o:Lmaps/o/o;

    invoke-interface {v0, v1, v2, v3, v4}, Lmaps/cq/b;->a(Lmaps/o/c;IZLmaps/o/l;)Lmaps/cq/a;

    move-result-object v0

    iput-object v0, p0, Lmaps/z/ar;->h:Lmaps/cq/a;

    :cond_0
    return-void
.end method

.method public a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 6

    const/4 v5, 0x0

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/z/ar;->q:Z

    if-nez v0, :cond_1

    monitor-exit p0

    :cond_0
    :goto_0
    return-void

    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {p3}, Lmaps/af/s;->b()I

    move-result v0

    if-gtz v0, :cond_0

    new-instance v2, Lmaps/af/t;

    invoke-direct {v2, p3}, Lmaps/af/t;-><init>(Lmaps/af/s;)V

    iget-boolean v0, p0, Lmaps/z/ar;->j:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lmaps/z/ar;->i:Z

    if-nez v0, :cond_2

    invoke-virtual {p0, p2, p1}, Lmaps/z/ar;->a(Lmaps/bq/d;Lmaps/cr/c;)V

    :cond_2
    iget-boolean v0, p0, Lmaps/z/ar;->k:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/z/ar;->e:Lmaps/z/az;

    iget-object v1, p0, Lmaps/z/ar;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lmaps/z/az;->a(Ljava/util/List;)V

    :cond_3
    iget-object v0, p0, Lmaps/z/ar;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_6

    invoke-virtual {p1}, Lmaps/cr/c;->D()V

    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Lmaps/af/t;->a(I)V

    iget-object v0, p0, Lmaps/z/ar;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/al;

    invoke-interface {v0, p1, p2, v2}, Lmaps/l/al;->b(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    iget-object v0, p0, Lmaps/z/ar;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/al;

    sget-boolean v1, Lmaps/ae/h;->Q:Z

    if-eqz v1, :cond_4

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    instance-of v1, v1, Lmaps/j/d;

    if-eqz v1, :cond_4

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    check-cast v1, Lmaps/j/d;

    invoke-interface {v0}, Lmaps/l/al;->b()Lmaps/t/ah;

    move-result-object v4

    invoke-virtual {v1, v4}, Lmaps/j/d;->a(Lmaps/t/ah;)V

    :cond_4
    invoke-interface {v0, p1, p2, v2}, Lmaps/l/al;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_5
    invoke-virtual {p1}, Lmaps/cr/c;->F()V

    sget-boolean v0, Lmaps/ae/h;->Q:Z

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    instance-of v0, v0, Lmaps/j/d;

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    check-cast v0, Lmaps/j/d;

    sget-object v1, Lmaps/j/d;->f:Lmaps/t/ah;

    invoke-virtual {v0, v1}, Lmaps/j/d;->a(Lmaps/t/ah;)V

    :cond_6
    iget-boolean v0, p0, Lmaps/z/ar;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/z/ar;->e:Lmaps/z/az;

    iget-object v1, p0, Lmaps/z/ar;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lmaps/z/az;->b(Ljava/util/List;)V

    iput-boolean v5, p0, Lmaps/z/ar;->k:Z

    goto/16 :goto_0
.end method

.method public a(Lmaps/t/ah;Z)V
    .locals 3

    iget-object v0, p0, Lmaps/z/ar;->a:Lmaps/af/f;

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lmaps/af/f;->a(ZZ)V

    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/z/ar;->e:Lmaps/z/az;

    invoke-virtual {v0, p1}, Lmaps/z/az;->b(Z)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/z/ar;->j:Z

    return-void
.end method

.method public declared-synchronized b()V
    .locals 2

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lmaps/z/ar;->a:Lmaps/af/f;

    iget-object v0, p0, Lmaps/z/ar;->e:Lmaps/z/az;

    invoke-virtual {v0}, Lmaps/z/az;->c()V

    iget-object v0, p0, Lmaps/z/ar;->e:Lmaps/z/az;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmaps/z/az;->a(Lmaps/z/au;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public clearTileCache()V
    .locals 2

    iget-object v0, p0, Lmaps/z/ar;->t:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    iget-object v0, p0, Lmaps/z/ar;->u:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->W:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iget-object v0, p0, Lmaps/z/ar;->e:Lmaps/z/az;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmaps/z/az;->a(Z)V

    iget-object v0, p0, Lmaps/z/ar;->t:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->h()V

    return-void
.end method

.method public equalsRemote(Lcom/google/android/gms/maps/model/internal/ITileOverlayDelegate;)Z
    .locals 1

    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/z/ar;->s:Ljava/lang/String;

    return-object v0
.end method

.method public getZIndex()F
    .locals 1

    iget-object v0, p0, Lmaps/z/ar;->t:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    iget v0, p0, Lmaps/z/ar;->r:F

    return v0
.end method

.method public hashCodeRemote()I
    .locals 1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public isVisible()Z
    .locals 1

    iget-object v0, p0, Lmaps/z/ar;->t:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/z/ar;->q:Z

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public remove()V
    .locals 2

    iget-object v0, p0, Lmaps/z/ar;->t:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    iget-object v0, p0, Lmaps/z/ar;->u:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->X:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iget-object v1, p0, Lmaps/z/ar;->t:Lmaps/z/bi;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/z/ar;->t:Lmaps/z/bi;

    invoke-virtual {v0, p0}, Lmaps/z/bi;->a(Lmaps/z/a;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/z/ar;->t:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->h()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public setVisible(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/z/ar;->t:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    iget-object v0, p0, Lmaps/z/ar;->u:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->Z:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lmaps/z/ar;->q:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/z/ar;->t:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->h()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public setZIndex(F)V
    .locals 2

    iget-object v0, p0, Lmaps/z/ar;->t:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    iget-object v0, p0, Lmaps/z/ar;->u:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->Y:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iget-object v1, p0, Lmaps/z/ar;->t:Lmaps/z/bi;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/z/ar;->t:Lmaps/z/bi;

    invoke-virtual {v0, p0}, Lmaps/z/bi;->b(Lmaps/z/a;)V

    iput p1, p0, Lmaps/z/ar;->r:F

    iget-object v0, p0, Lmaps/z/ar;->t:Lmaps/z/bi;

    invoke-virtual {v0, p0}, Lmaps/z/bi;->c(Lmaps/z/a;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/z/ar;->t:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->h()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Lmaps/ap/e;->a(Ljava/lang/Object;)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "id"

    iget-object v2, p0, Lmaps/z/ar;->s:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/ap/n;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ap/n;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
