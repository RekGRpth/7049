.class public Lmaps/z/at;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lmaps/i/m;

.field private final b:Lmaps/t/bi;

.field private final c:Lmaps/bf/b;


# direct methods
.method public constructor <init>(Lmaps/i/m;Lmaps/t/bi;Lmaps/bf/b;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p2}, Lmaps/t/bi;->b()Lmaps/t/bg;

    move-result-object v0

    const-string v1, "Level must have an id"

    invoke-static {v0, v1}, Lmaps/ap/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lmaps/z/at;->a:Lmaps/i/m;

    iput-object p2, p0, Lmaps/z/at;->b:Lmaps/t/bi;

    iput-object p3, p0, Lmaps/z/at;->c:Lmaps/bf/b;

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/z/at;->b:Lmaps/t/bi;

    invoke-virtual {v0}, Lmaps/t/bi;->b()Lmaps/t/bg;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/bg;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/z/at;->b:Lmaps/t/bi;

    invoke-virtual {v0}, Lmaps/t/bi;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/z/at;->b:Lmaps/t/bi;

    invoke-virtual {v0}, Lmaps/t/bi;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    instance-of v0, p1, Lmaps/z/at;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    check-cast p1, Lmaps/z/at;

    iget-object v0, p0, Lmaps/z/at;->b:Lmaps/t/bi;

    invoke-virtual {v0}, Lmaps/t/bi;->b()Lmaps/t/bg;

    move-result-object v0

    iget-object v1, p1, Lmaps/z/at;->b:Lmaps/t/bi;

    invoke-virtual {v1}, Lmaps/t/bi;->b()Lmaps/t/bg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/t/bg;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lmaps/z/at;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lmaps/ap/e;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Lmaps/ap/e;->a(Ljava/lang/Object;)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {p0}, Lmaps/z/at;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "name"

    invoke-virtual {p0}, Lmaps/z/at;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "shortName"

    invoke-virtual {p0}, Lmaps/z/at;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/ap/n;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ap/n;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
