.class final Lmaps/z/ax;
.super Landroid/content/BroadcastReceiver;

# interfaces
.implements Ljava/lang/Runnable;
.implements Lmaps/ak/i;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Ljava/lang/String;

.field private final d:Lmaps/bb/c;

.field private final e:Lmaps/z/ad;

.field private final f:Lmaps/ae/d;

.field private final g:Lmaps/i/v;

.field private final h:Ljava/util/Random;

.field private i:Ljava/lang/String;

.field private j:J

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lmaps/z/ax;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lmaps/z/ax;->a:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Lmaps/bb/c;Lmaps/z/ad;Lmaps/ae/d;Ljava/util/Random;Lmaps/i/v;)V
    .locals 2

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lmaps/z/ax;->j:J

    iput-object p1, p0, Lmaps/z/ax;->b:Landroid/content/Context;

    iput-object p2, p0, Lmaps/z/ax;->c:Ljava/lang/String;

    iput-object p3, p0, Lmaps/z/ax;->d:Lmaps/bb/c;

    iput-object p4, p0, Lmaps/z/ax;->e:Lmaps/z/ad;

    iput-object p5, p0, Lmaps/z/ax;->f:Lmaps/ae/d;

    iput-object p6, p0, Lmaps/z/ax;->h:Ljava/util/Random;

    iput-object p7, p0, Lmaps/z/ax;->g:Lmaps/i/v;

    return-void
.end method

.method static a(Ljava/io/FileInputStream;)Landroid/util/Pair;
    .locals 7

    new-instance v3, Ljava/io/DataInputStream;

    invoke-direct {v3, p0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    :try_start_0
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v1

    invoke-virtual {v3}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :try_start_1
    invoke-virtual {v3}, Ljava/io/DataInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_0
    :goto_0
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v3

    sget-boolean v4, Lmaps/ae/h;->i:Z

    if-eqz v4, :cond_0

    sget-object v4, Lmaps/z/ax;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error closing token file: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_2
    sget-boolean v1, Lmaps/ae/h;->i:Z

    if-eqz v1, :cond_1

    sget-object v1, Lmaps/z/ax;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error reading token file: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    const-wide/16 v1, -0x1

    const/4 v0, 0x0

    :try_start_3
    invoke-virtual {v3}, Ljava/io/DataInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    :catch_2
    move-exception v3

    sget-boolean v4, Lmaps/ae/h;->i:Z

    if-eqz v4, :cond_0

    sget-object v4, Lmaps/z/ax;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error closing token file: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_4
    invoke-virtual {v3}, Ljava/io/DataInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :cond_2
    :goto_1
    throw v0

    :catch_3
    move-exception v1

    sget-boolean v2, Lmaps/ae/h;->i:Z

    if-eqz v2, :cond_2

    sget-object v2, Lmaps/z/ax;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error closing token file: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)Lmaps/bb/c;
    .locals 4

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-static {v1, v0}, Lmaps/cu/a;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lmaps/bb/c;

    sget-object v3, Lmaps/c/w;->d:Lmaps/bb/d;

    invoke-direct {v2, v3}, Lmaps/bb/c;-><init>(Lmaps/bb/d;)V

    const/4 v3, 0x1

    invoke-virtual {v2, v3, p1}, Lmaps/bb/c;->b(ILjava/lang/String;)Lmaps/bb/c;

    const/4 v3, 0x2

    invoke-virtual {v2, v3, v0}, Lmaps/bb/c;->b(ILjava/lang/String;)Lmaps/bb/c;

    const/4 v0, 0x3

    invoke-virtual {v2, v0, v1}, Lmaps/bb/c;->b(ILjava/lang/String;)Lmaps/bb/c;

    return-object v2
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Lmaps/ak/n;)Lmaps/z/ax;
    .locals 8

    new-instance v5, Lmaps/co/a;

    invoke-direct {v5}, Lmaps/co/a;-><init>()V

    invoke-static {}, Lmaps/i/o;->a()Lmaps/i/o;

    move-result-object v7

    invoke-static {p0, p1}, Lmaps/z/ax;->a(Landroid/content/Context;Ljava/lang/String;)Lmaps/bb/c;

    move-result-object v3

    new-instance v4, Lmaps/z/bs;

    invoke-direct {v4, p2}, Lmaps/z/bs;-><init>(Lmaps/ak/n;)V

    new-instance v6, Ljava/util/Random;

    invoke-direct {v6}, Ljava/util/Random;-><init>()V

    new-instance v0, Lmaps/z/ax;

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v7}, Lmaps/z/ax;-><init>(Landroid/content/Context;Ljava/lang/String;Lmaps/bb/c;Lmaps/z/ad;Lmaps/ae/d;Ljava/util/Random;Lmaps/i/v;)V

    return-object v0
.end method

.method private a(J)V
    .locals 1

    iget-object v0, p0, Lmaps/z/ax;->g:Lmaps/i/v;

    invoke-interface {v0, p0}, Lmaps/i/v;->b(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lmaps/z/ax;->g:Lmaps/i/v;

    invoke-interface {v0, p0, p1, p2}, Lmaps/i/v;->a(Ljava/lang/Runnable;J)V

    return-void
.end method

.method static a(Ljava/io/FileOutputStream;JLjava/lang/String;)V
    .locals 5

    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, p0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    :try_start_0
    invoke-virtual {v1, p1, p2}, Ljava/io/DataOutputStream;->writeLong(J)V

    invoke-virtual {v1, p3}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-boolean v1, Lmaps/ae/h;->i:Z

    if-eqz v1, :cond_0

    sget-object v1, Lmaps/z/ax;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error closing token file: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_2
    sget-boolean v2, Lmaps/ae/h;->i:Z

    if-eqz v2, :cond_1

    sget-object v2, Lmaps/z/ax;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error writing token file: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    :try_start_3
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    sget-boolean v1, Lmaps/ae/h;->i:Z

    if-eqz v1, :cond_0

    sget-object v1, Lmaps/z/ax;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error closing token file: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_4
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :cond_2
    :goto_1
    throw v0

    :catch_3
    move-exception v1

    sget-boolean v2, Lmaps/ae/h;->i:Z

    if-eqz v2, :cond_2

    sget-object v2, Lmaps/z/ax;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error closing token file: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private a(Ljava/lang/String;J)V
    .locals 4

    iput-object p1, p0, Lmaps/z/ax;->i:Ljava/lang/String;

    iput-wide p2, p0, Lmaps/z/ax;->j:J

    iget-object v0, p0, Lmaps/z/ax;->e:Lmaps/z/ad;

    iget-object v1, p0, Lmaps/z/ax;->i:Ljava/lang/String;

    invoke-interface {v0, v1}, Lmaps/z/ad;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lmaps/z/ax;->e:Lmaps/z/ad;

    iget-object v0, p0, Lmaps/z/ax;->i:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, Lmaps/z/ax;->d:Lmaps/bb/c;

    :goto_0
    invoke-interface {v1, v0}, Lmaps/z/ad;->a(Lmaps/bb/c;)V

    invoke-direct {p0}, Lmaps/z/ax;->c()V

    iget-wide v0, p0, Lmaps/z/ax;->j:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    iget-wide v0, p0, Lmaps/z/ax;->j:J

    const-wide/32 v2, 0x927c0

    sub-long/2addr v0, v2

    iget-object v2, p0, Lmaps/z/ax;->f:Lmaps/ae/d;

    invoke-interface {v2}, Lmaps/ae/d;->a()J

    move-result-wide v2

    sub-long/2addr v0, v2

    invoke-direct {p0, v0, v1}, Lmaps/z/ax;->a(J)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()V
    .locals 4

    iget-object v0, p0, Lmaps/z/ax;->i:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, Lmaps/z/ax;->b:Landroid/content/Context;

    const-string v1, "_m_t"

    invoke-virtual {v0, v1}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-boolean v0, Lmaps/ae/h;->g:Z

    if-eqz v0, :cond_0

    sget-object v0, Lmaps/z/ax;->a:Ljava/lang/String;

    const-string v1, "Deleted saved auth token"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    iget-object v0, p0, Lmaps/z/ax;->b:Landroid/content/Context;

    const-string v1, "_m_t"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    iget-wide v1, p0, Lmaps/z/ax;->j:J

    iget-object v3, p0, Lmaps/z/ax;->i:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lmaps/z/ax;->a(Ljava/io/FileOutputStream;JLjava/lang/String;)V

    sget-boolean v0, Lmaps/ae/h;->g:Z

    if-eqz v0, :cond_0

    sget-object v0, Lmaps/z/ax;->a:Ljava/lang/String;

    const-string v1, "Saved auth token"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_0
    move-exception v0

    sget-boolean v1, Lmaps/ae/h;->i:Z

    if-eqz v1, :cond_0

    sget-object v1, Lmaps/z/ax;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error opening token file: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private d()V
    .locals 4

    :try_start_0
    iget-object v0, p0, Lmaps/z/ax;->b:Landroid/content/Context;

    const-string v1, "_m_t"

    invoke-virtual {v0, v1}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    invoke-static {v0}, Lmaps/z/ax;->a(Ljava/io/FileInputStream;)Landroid/util/Pair;

    move-result-object v1

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, p0, Lmaps/z/ax;->j:J

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lmaps/z/ax;->i:Ljava/lang/String;

    sget-boolean v0, Lmaps/ae/h;->g:Z

    if-eqz v0, :cond_0

    sget-object v0, Lmaps/z/ax;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Loaded auth token.  Expires at: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lmaps/z/ax;->j:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private e()V
    .locals 3

    sget-boolean v0, Lmaps/ae/h;->g:Z

    if-eqz v0, :cond_0

    sget-object v0, Lmaps/z/ax;->a:Ljava/lang/String;

    const-string v1, "Token rejected"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    const-wide/16 v1, -0x1

    invoke-direct {p0, v0, v1, v2}, Lmaps/z/ax;->a(Ljava/lang/String;J)V

    iget v0, p0, Lmaps/z/ax;->k:I

    if-nez v0, :cond_1

    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lmaps/z/ax;->a(J)V

    :cond_1
    return-void
.end method

.method private f()V
    .locals 10

    iget-object v0, p0, Lmaps/z/ax;->h:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextDouble()D

    move-result-wide v0

    const-wide v2, 0x40b3880000000000L

    mul-double/2addr v0, v2

    const-wide/32 v2, 0x36ee80

    const-wide v4, 0x40c3880000000000L

    const-wide v6, 0x3ff999999999999aL

    iget v8, p0, Lmaps/z/ax;->k:I

    int-to-double v8, v8

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    mul-double/2addr v4, v6

    add-double/2addr v0, v4

    double-to-long v0, v0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iget-object v2, p0, Lmaps/z/ax;->g:Lmaps/i/v;

    invoke-interface {v2, p0}, Lmaps/i/v;->b(Ljava/lang/Runnable;)V

    iget-object v2, p0, Lmaps/z/ax;->g:Lmaps/i/v;

    invoke-interface {v2, p0, v0, v1}, Lmaps/i/v;->a(Ljava/lang/Runnable;J)V

    return-void
.end method

.method private g()V
    .locals 5

    const/4 v4, 0x0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.google.android.maps.api.action.TOKEN_REQUEST"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.maps.api.extra.SENDER"

    iget-object v2, p0, Lmaps/z/ax;->b:Landroid/content/Context;

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    invoke-static {v2, v4, v3, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "com.google.android.maps.api.extra.API_KEY"

    iget-object v2, p0, Lmaps/z/ax;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lmaps/z/ax;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, v0, v4}, Landroid/content/pm/PackageManager;->resolveService(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    if-eqz v1, :cond_1

    sget-boolean v1, Lmaps/ae/h;->g:Z

    if-eqz v1, :cond_0

    sget-object v1, Lmaps/z/ax;->a:Ljava/lang/String;

    const-string v2, "Requesting token"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v1, p0, Lmaps/z/ax;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_1
    return-void
.end method

.method private h()V
    .locals 1

    const-string v0, "Authorization failure."

    invoke-static {v0}, Lmaps/au/a;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lmaps/z/ax;->g:Lmaps/i/v;

    invoke-interface {v0, p0}, Lmaps/i/v;->b(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public declared-synchronized a()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lmaps/z/ax;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(IZLjava/lang/String;)V
    .locals 0

    return-void
.end method

.method public a(Lmaps/ak/j;)V
    .locals 0

    return-void
.end method

.method public declared-synchronized b()V
    .locals 5

    const-wide/16 v3, 0x0

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lmaps/z/ax;->d()V

    iget-object v0, p0, Lmaps/z/ax;->e:Lmaps/z/ad;

    iget-object v1, p0, Lmaps/z/ax;->i:Ljava/lang/String;

    invoke-interface {v0, v1}, Lmaps/z/ad;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lmaps/z/ax;->e:Lmaps/z/ad;

    iget-object v0, p0, Lmaps/z/ax;->i:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, Lmaps/z/ax;->d:Lmaps/bb/c;

    :goto_0
    invoke-interface {v1, v0}, Lmaps/z/ad;->a(Lmaps/bb/c;)V

    iget-object v0, p0, Lmaps/z/ax;->e:Lmaps/z/ad;

    invoke-interface {v0, p0}, Lmaps/z/ad;->a(Lmaps/ak/i;)V

    iget-object v0, p0, Lmaps/z/ax;->b:Landroid/content/Context;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.google.android.maps.api.action.TOKEN_RESPONSE"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v0, p0, Lmaps/z/ax;->i:Ljava/lang/String;

    if-nez v0, :cond_2

    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lmaps/z/ax;->a(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_1
    monitor-exit p0

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    :try_start_1
    iget-wide v0, p0, Lmaps/z/ax;->j:J

    cmp-long v0, v0, v3

    if-ltz v0, :cond_0

    iget-wide v0, p0, Lmaps/z/ax;->j:J

    const-wide/32 v2, 0x927c0

    sub-long/2addr v0, v2

    iget-object v2, p0, Lmaps/z/ax;->f:Lmaps/ae/d;

    invoke-interface {v2}, Lmaps/ae/d;->a()J

    move-result-wide v2

    sub-long/2addr v0, v2

    invoke-direct {p0, v0, v1}, Lmaps/z/ax;->a(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(Lmaps/ak/j;)V
    .locals 0

    return-void
.end method

.method public declared-synchronized onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    const-wide/16 v6, -0x1

    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lmaps/ae/h;->g:Z

    if-eqz v0, :cond_0

    sget-object v0, Lmaps/z/ax;->a:Ljava/lang/String;

    const-string v1, "Response received"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v0, "com.google.android.maps.api.action.TOKEN_RESPONSE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    :cond_2
    :try_start_1
    const-string v0, "com.google.android.maps.api.extra.ERROR"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "com.google.android.maps.api.extra.ERROR"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "TIMEOUT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "INVALID_PARAMETERS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-direct {p0}, Lmaps/z/ax;->h()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    :try_start_2
    const-string v1, "CERTIFICATE_ERROR"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-direct {p0}, Lmaps/z/ax;->h()V

    goto :goto_0

    :cond_4
    const-string v1, "REJECTED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-direct {p0}, Lmaps/z/ax;->h()V

    goto :goto_0

    :cond_5
    invoke-direct {p0}, Lmaps/z/ax;->h()V

    goto :goto_0

    :cond_6
    const-string v0, "com.google.android.maps.api.extra.API_TOKEN"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    invoke-direct {p0}, Lmaps/z/ax;->h()V

    goto :goto_0

    :cond_7
    const/4 v0, 0x0

    iput v0, p0, Lmaps/z/ax;->k:I

    const-string v0, "com.google.android.maps.api.extra.API_TOKEN"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.maps.api.extra.API_TOKEN_EXPIRY"

    const-wide/16 v2, -0x1

    invoke-virtual {p2, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    sget-boolean v3, Lmaps/ae/h;->g:Z

    if-eqz v3, :cond_8

    sget-object v3, Lmaps/z/ax;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Token received: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", expires at "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    cmp-long v3, v1, v6

    if-eqz v3, :cond_9

    iget-wide v3, p0, Lmaps/z/ax;->j:J

    cmp-long v3, v3, v6

    if-eqz v3, :cond_9

    iget-wide v3, p0, Lmaps/z/ax;->j:J

    cmp-long v3, v1, v3

    if-lez v3, :cond_1

    :cond_9
    invoke-direct {p0, v0, v1, v2}, Lmaps/z/ax;->a(Ljava/lang/String;J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method public declared-synchronized run()V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lmaps/z/ax;->f()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-direct {p0}, Lmaps/z/ax;->g()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget v0, p0, Lmaps/z/ax;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/z/ax;->k:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    :try_start_3
    iget v1, p0, Lmaps/z/ax;->k:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lmaps/z/ax;->k:I

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method
