.class Lmaps/z/bb;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Comparator;


# instance fields
.field final synthetic a:Lmaps/z/bi;


# direct methods
.method constructor <init>(Lmaps/z/bi;)V
    .locals 0

    iput-object p1, p0, Lmaps/z/bb;->a:Lmaps/z/bi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lmaps/z/a;Lmaps/z/a;)I
    .locals 2

    invoke-interface {p1}, Lmaps/z/a;->getZIndex()F

    move-result v0

    invoke-interface {p2}, Lmaps/z/a;->getZIndex()F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-interface {p1}, Lmaps/z/a;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2}, Lmaps/z/a;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lmaps/z/a;

    check-cast p2, Lmaps/z/a;

    invoke-virtual {p0, p1, p2}, Lmaps/z/bb;->a(Lmaps/z/a;Lmaps/z/a;)I

    move-result v0

    return v0
.end method
