.class Lmaps/z/bf;
.super Lcom/google/android/gms/maps/internal/IProjectionDelegate$Stub;


# instance fields
.field private final a:Lmaps/bq/d;

.field private final b:Lmaps/bf/b;


# direct methods
.method public constructor <init>(Lmaps/bq/d;Lmaps/bf/b;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/maps/internal/IProjectionDelegate$Stub;-><init>()V

    iput-object p1, p0, Lmaps/z/bf;->a:Lmaps/bq/d;

    iput-object p2, p0, Lmaps/z/bf;->b:Lmaps/bf/b;

    return-void
.end method


# virtual methods
.method public fromScreenLocation(Lcom/google/android/gms/dynamic/IObjectWrapper;)Lcom/google/android/gms/maps/model/LatLng;
    .locals 3

    invoke-static {p1}, Lcom/google/android/gms/dynamic/ObjectWrapper;->unwrap(Lcom/google/android/gms/dynamic/IObjectWrapper;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Point;

    iget-object v1, p0, Lmaps/z/bf;->b:Lmaps/bf/b;

    sget-object v2, Lmaps/bf/c;->aX:Lmaps/bf/c;

    invoke-interface {v1, v2}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iget-object v1, p0, Lmaps/z/bf;->a:Lmaps/bq/d;

    iget v2, v0, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    invoke-virtual {v1, v2, v0}, Lmaps/bq/d;->d(FF)Lmaps/t/bx;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lmaps/au/c;->a(Lmaps/t/bx;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    goto :goto_0
.end method

.method public getVisibleRegion()Lcom/google/android/gms/maps/model/VisibleRegion;
    .locals 2

    iget-object v0, p0, Lmaps/z/bf;->b:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->aY:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iget-object v0, p0, Lmaps/z/bf;->a:Lmaps/bq/d;

    invoke-virtual {v0}, Lmaps/bq/d;->C()Lmaps/t/u;

    move-result-object v0

    invoke-static {v0}, Lmaps/au/c;->a(Lmaps/t/u;)Lcom/google/android/gms/maps/model/VisibleRegion;

    move-result-object v0

    return-object v0
.end method

.method public toScreenLocation(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/dynamic/IObjectWrapper;
    .locals 4

    iget-object v0, p0, Lmaps/z/bf;->b:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->aZ:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iget-object v0, p0, Lmaps/z/bf;->a:Lmaps/bq/d;

    invoke-static {p1}, Lmaps/au/c;->b(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/t/bx;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/bq/d;->b(Lmaps/t/bx;)[I

    move-result-object v0

    new-instance v1, Landroid/graphics/Point;

    const/4 v2, 0x0

    aget v2, v0, v2

    const/4 v3, 0x1

    aget v0, v0, v3

    invoke-direct {v1, v2, v0}, Landroid/graphics/Point;-><init>(II)V

    invoke-static {v1}, Lcom/google/android/gms/dynamic/ObjectWrapper;->wrap(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Lmaps/ap/e;->a(Ljava/lang/Object;)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "camera"

    iget-object v2, p0, Lmaps/z/bf;->a:Lmaps/bq/d;

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/ap/n;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ap/n;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
