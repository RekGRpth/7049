.class final Lmaps/z/bh;
.super Lcom/google/android/gms/maps/internal/IOnLocationChangeListener$Stub;

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lmaps/z/z;


# instance fields
.field private final a:Lmaps/i/c;

.field private final b:Lmaps/i/h;

.field private final c:Lcom/google/android/gms/maps/internal/ILocationSourceDelegate;

.field private final d:Lmaps/aa/c;

.field private final e:Landroid/content/res/Resources;

.field private f:Lmaps/y/x;

.field private g:Landroid/location/Location;

.field private h:Lcom/google/android/gms/maps/internal/ILocationSourceDelegate;

.field private final i:Lmaps/bf/b;

.field private j:Z

.field private k:Z

.field private l:Lcom/google/android/gms/maps/internal/IOnMyLocationChangeListener;


# direct methods
.method constructor <init>(Landroid/content/res/Resources;Lmaps/i/c;Lmaps/i/h;Lmaps/aa/c;Lcom/google/android/gms/maps/internal/ILocationSourceDelegate;Lmaps/bf/b;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/maps/internal/IOnLocationChangeListener$Stub;-><init>()V

    iput-object p2, p0, Lmaps/z/bh;->a:Lmaps/i/c;

    iput-object p3, p0, Lmaps/z/bh;->b:Lmaps/i/h;

    iput-object p5, p0, Lmaps/z/bh;->c:Lcom/google/android/gms/maps/internal/ILocationSourceDelegate;

    iput-object p4, p0, Lmaps/z/bh;->d:Lmaps/aa/c;

    iput-object p5, p0, Lmaps/z/bh;->h:Lcom/google/android/gms/maps/internal/ILocationSourceDelegate;

    iput-object p6, p0, Lmaps/z/bh;->i:Lmaps/bf/b;

    iput-object p1, p0, Lmaps/z/bh;->e:Landroid/content/res/Resources;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/z/bh;->k:Z

    return-void
.end method

.method private a(Lcom/google/android/gms/maps/model/LatLng;F)F
    .locals 3

    iget-object v0, p0, Lmaps/z/bh;->a:Lmaps/i/c;

    invoke-interface {v0}, Lmaps/i/c;->a()Lmaps/bq/a;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/bq/a;->a()F

    move-result v0

    const/high16 v1, 0x41200000

    cmpg-float v1, v0, v1

    if-gtz v1, :cond_0

    invoke-direct {p0, p1}, Lmaps/z/bh;->a(Lcom/google/android/gms/maps/model/LatLng;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/high16 v0, 0x41980000

    :cond_0
    :goto_0
    invoke-direct {p0, p1, p2}, Lmaps/z/bh;->b(Lcom/google/android/gms/maps/model/LatLng;F)F

    move-result v1

    const/high16 v2, -0x40800000

    cmpl-float v2, v1, v2

    if-nez v2, :cond_2

    :goto_1
    return v0

    :cond_1
    const/high16 v0, 0x41700000

    goto :goto_0

    :cond_2
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    goto :goto_1
.end method

.method private a(Lmaps/bl/c;)F
    .locals 1

    invoke-virtual {p1}, Lmaps/bl/c;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    int-to-float v0, v0

    return v0
.end method

.method public static a(Landroid/content/res/Resources;Lmaps/i/c;Lmaps/i/h;Lmaps/aa/c;Lcom/google/android/gms/maps/internal/ILocationSourceDelegate;Lmaps/bf/b;)Lmaps/z/bh;
    .locals 7

    new-instance v0, Lmaps/z/bh;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lmaps/z/bh;-><init>(Landroid/content/res/Resources;Lmaps/i/c;Lmaps/i/h;Lmaps/aa/c;Lcom/google/android/gms/maps/internal/ILocationSourceDelegate;Lmaps/bf/b;)V

    return-object v0
.end method

.method private a(Lcom/google/android/gms/maps/model/LatLng;)Z
    .locals 6

    iget-wide v0, p1, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    iget-wide v2, p1, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    const-wide v4, 0x4037e2220bc382a1L

    cmpl-double v4, v0, v4

    if-lez v4, :cond_0

    const-wide v4, 0x4047094067cf1c32L

    cmpg-double v0, v0, v4

    if-gez v0, :cond_0

    const-wide v0, 0x405eefe9813879c4L

    cmpl-double v0, v2, v0

    if-lez v0, :cond_0

    const-wide v0, 0x4061f940010c6f7aL

    cmpg-double v0, v2, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lcom/google/android/gms/maps/model/LatLng;F)F
    .locals 8

    const/high16 v0, -0x40800000

    iget-object v1, p0, Lmaps/z/bh;->b:Lmaps/i/h;

    invoke-interface {v1}, Lmaps/i/h;->getWidth()I

    move-result v1

    iget-object v2, p0, Lmaps/z/bh;->b:Lmaps/i/h;

    invoke-interface {v2}, Lmaps/i/h;->getHeight()I

    move-result v2

    if-eqz v1, :cond_0

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {p1}, Lmaps/au/c;->c(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/bl/a;

    move-result-object v3

    mul-float v4, p2, p2

    float-to-long v4, v4

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    div-int/lit8 v2, v1, 0x2

    const/16 v1, 0x15

    invoke-static {v1}, Lmaps/bl/c;->a(I)Lmaps/bl/c;

    move-result-object v1

    :goto_1
    if-eqz v1, :cond_2

    const/4 v6, 0x0

    invoke-virtual {v3, v2, v6, v1}, Lmaps/bl/a;->a(IILmaps/bl/c;)Lmaps/bl/a;

    move-result-object v6

    invoke-virtual {v3, v6}, Lmaps/bl/a;->a(Lmaps/bl/a;)J

    move-result-wide v6

    cmp-long v6, v4, v6

    if-lez v6, :cond_2

    invoke-virtual {v1}, Lmaps/bl/c;->c()Lmaps/bl/c;

    move-result-object v1

    goto :goto_1

    :cond_2
    if-eqz v1, :cond_0

    invoke-direct {p0, v1}, Lmaps/z/bh;->a(Lmaps/bl/c;)F

    move-result v0

    goto :goto_0
.end method

.method private f()V
    .locals 2

    iget-boolean v0, p0, Lmaps/z/bh;->k:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lmaps/z/bh;->j:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lmaps/z/bh;->d:Lmaps/aa/c;

    invoke-virtual {v1, v0}, Lmaps/aa/c;->a(Z)V

    iget-object v1, p0, Lmaps/z/bh;->d:Lmaps/aa/c;

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {v1, p0}, Lmaps/aa/c;->a(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    goto :goto_1
.end method

.method private g()V
    .locals 10

    const v9, 0x73217bce

    const v8, 0x338cc6ef

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lmaps/z/bh;->f:Lmaps/y/x;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lmaps/z/bh;->b:Lmaps/i/h;

    invoke-interface {v0, v7}, Lmaps/i/h;->a(Z)Lmaps/y/x;

    move-result-object v0

    iput-object v0, p0, Lmaps/z/bh;->f:Lmaps/y/x;

    iget-object v0, p0, Lmaps/z/bh;->f:Lmaps/y/x;

    const/4 v1, 0x5

    new-array v1, v1, [Lmaps/y/bl;

    invoke-static {}, Lmaps/y/bl;->a()Lmaps/y/as;

    move-result-object v2

    invoke-virtual {v2, v7}, Lmaps/y/as;->a(Z)Lmaps/y/as;

    move-result-object v2

    invoke-virtual {v2, v6}, Lmaps/y/as;->b(Z)Lmaps/y/as;

    move-result-object v2

    invoke-virtual {v2, v7}, Lmaps/y/as;->c(Z)Lmaps/y/as;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/y/as;->a()Lmaps/y/as;

    move-result-object v2

    sget v3, Lmaps/cc/d;->am:I

    sget v4, Lmaps/cc/d;->an:I

    invoke-virtual {v2, v3, v4}, Lmaps/y/as;->a(II)Lmaps/y/as;

    move-result-object v2

    invoke-virtual {v2, v6, v8}, Lmaps/y/as;->b(II)Lmaps/y/as;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/y/as;->c()Lmaps/y/bl;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {}, Lmaps/y/bl;->a()Lmaps/y/as;

    move-result-object v2

    invoke-virtual {v2, v6}, Lmaps/y/as;->a(Z)Lmaps/y/as;

    move-result-object v2

    invoke-virtual {v2, v6}, Lmaps/y/as;->b(Z)Lmaps/y/as;

    move-result-object v2

    invoke-virtual {v2, v7}, Lmaps/y/as;->c(Z)Lmaps/y/as;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/y/as;->b()Lmaps/y/as;

    move-result-object v2

    sget v3, Lmaps/cc/d;->ai:I

    sget v4, Lmaps/cc/d;->aj:I

    invoke-virtual {v2, v3, v4}, Lmaps/y/as;->a(II)Lmaps/y/as;

    move-result-object v2

    invoke-virtual {v2, v6, v8}, Lmaps/y/as;->b(II)Lmaps/y/as;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/y/as;->c()Lmaps/y/bl;

    move-result-object v2

    aput-object v2, v1, v7

    const/4 v2, 0x2

    invoke-static {}, Lmaps/y/bl;->a()Lmaps/y/as;

    move-result-object v3

    invoke-virtual {v3, v7}, Lmaps/y/as;->a(Z)Lmaps/y/as;

    move-result-object v3

    invoke-virtual {v3, v6}, Lmaps/y/as;->b(Z)Lmaps/y/as;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/y/as;->a()Lmaps/y/as;

    move-result-object v3

    sget v4, Lmaps/cc/d;->ao:I

    sget v5, Lmaps/cc/d;->ap:I

    invoke-virtual {v3, v4, v5}, Lmaps/y/as;->a(II)Lmaps/y/as;

    move-result-object v3

    invoke-virtual {v3, v9, v8}, Lmaps/y/as;->b(II)Lmaps/y/as;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/y/as;->c()Lmaps/y/bl;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-static {}, Lmaps/y/bl;->a()Lmaps/y/as;

    move-result-object v3

    invoke-virtual {v3, v6}, Lmaps/y/as;->a(Z)Lmaps/y/as;

    move-result-object v3

    invoke-virtual {v3, v6}, Lmaps/y/as;->b(Z)Lmaps/y/as;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/y/as;->b()Lmaps/y/as;

    move-result-object v3

    sget v4, Lmaps/cc/d;->ak:I

    sget v5, Lmaps/cc/d;->al:I

    invoke-virtual {v3, v4, v5}, Lmaps/y/as;->a(II)Lmaps/y/as;

    move-result-object v3

    invoke-virtual {v3, v9, v8}, Lmaps/y/as;->b(II)Lmaps/y/as;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/y/as;->c()Lmaps/y/bl;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    invoke-static {}, Lmaps/y/bl;->a()Lmaps/y/as;

    move-result-object v3

    invoke-virtual {v3, v7}, Lmaps/y/as;->b(Z)Lmaps/y/as;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/y/as;->b()Lmaps/y/as;

    move-result-object v3

    sget v4, Lmaps/cc/d;->aq:I

    sget v5, Lmaps/cc/d;->ar:I

    invoke-virtual {v3, v4, v5}, Lmaps/y/as;->a(II)Lmaps/y/as;

    move-result-object v3

    const v4, 0x73aaaaaa

    const v5, 0x33cccccc

    invoke-virtual {v3, v4, v5}, Lmaps/y/as;->b(II)Lmaps/y/as;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/y/as;->c()Lmaps/y/bl;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lmaps/y/x;->a([Lmaps/y/bl;)V

    iget-object v0, p0, Lmaps/z/bh;->f:Lmaps/y/x;

    iget-object v1, p0, Lmaps/z/bh;->e:Landroid/content/res/Resources;

    sget v2, Lmaps/cc/h;->q:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iget-object v2, p0, Lmaps/z/bh;->e:Landroid/content/res/Resources;

    sget v3, Lmaps/cc/f;->b:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iget-object v3, p0, Lmaps/z/bh;->e:Landroid/content/res/Resources;

    sget v4, Lmaps/cc/f;->a:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lmaps/y/x;->a(FII)V

    iget-object v0, p0, Lmaps/z/bh;->f:Lmaps/y/x;

    sget-object v1, Lmaps/y/am;->x:Lmaps/y/am;

    invoke-virtual {v0, v1}, Lmaps/y/x;->a(Lmaps/y/am;)V

    iget-object v0, p0, Lmaps/z/bh;->f:Lmaps/y/x;

    const/high16 v1, 0x41600000

    const/high16 v2, 0x41200000

    const v3, 0x3f4ccccd

    invoke-virtual {v0, v1, v2, v3}, Lmaps/y/x;->a(FFF)V

    iget-object v0, p0, Lmaps/z/bh;->f:Lmaps/y/x;

    const v1, 0x8000

    invoke-virtual {v0, v1}, Lmaps/y/x;->b(I)V

    goto/16 :goto_0
.end method

.method private h()V
    .locals 7

    const/4 v6, -0x1

    invoke-virtual {p0}, Lmaps/z/bh;->c()Z

    move-result v0

    const-string v1, "MyLocation layer not enabled"

    invoke-static {v0, v1}, Lmaps/ap/q;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Lmaps/z/bh;->g:Landroid/location/Location;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    iget-object v0, p0, Lmaps/z/bh;->g:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    iget-object v0, p0, Lmaps/z/bh;->g:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    iget-object v0, p0, Lmaps/z/bh;->g:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    invoke-direct {p0, v1, v0}, Lmaps/z/bh;->a(Lcom/google/android/gms/maps/model/LatLng;F)F

    move-result v2

    iget-object v0, p0, Lmaps/z/bh;->a:Lmaps/i/c;

    invoke-interface {v0}, Lmaps/i/c;->a()Lmaps/bq/a;

    move-result-object v5

    new-instance v0, Lmaps/bq/a;

    invoke-static {v1}, Lmaps/au/c;->a(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/t/af;

    move-result-object v1

    invoke-virtual {v5}, Lmaps/bq/a;->d()F

    move-result v3

    invoke-virtual {v5}, Lmaps/bq/a;->e()F

    move-result v4

    invoke-virtual {v5}, Lmaps/bq/a;->f()F

    move-result v5

    invoke-direct/range {v0 .. v5}, Lmaps/bq/a;-><init>(Lmaps/t/af;FFFF)V

    iget-object v1, p0, Lmaps/z/bh;->a:Lmaps/i/c;

    invoke-interface {v1, v0, v6, v6}, Lmaps/i/c;->a(Lmaps/bq/b;II)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    invoke-virtual {p0}, Lmaps/z/bh;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/z/bh;->j:Z

    invoke-direct {p0}, Lmaps/z/bh;->g()V

    iget-object v0, p0, Lmaps/z/bh;->b:Lmaps/i/h;

    iget-object v1, p0, Lmaps/z/bh;->f:Lmaps/y/x;

    invoke-interface {v0, v1}, Lmaps/i/h;->a(Lmaps/y/bc;)V

    :try_start_0
    iget-object v0, p0, Lmaps/z/bh;->h:Lcom/google/android/gms/maps/internal/ILocationSourceDelegate;

    invoke-interface {v0, p0}, Lcom/google/android/gms/maps/internal/ILocationSourceDelegate;->activate(Lcom/google/android/gms/maps/internal/IOnLocationChangeListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-direct {p0}, Lmaps/z/bh;->f()V

    iget-object v0, p0, Lmaps/z/bh;->g:Landroid/location/Location;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/z/bh;->g:Landroid/location/Location;

    invoke-static {v0}, Lcom/google/android/gms/dynamic/ObjectWrapper;->wrap(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmaps/z/bh;->onLocationChanged(Lcom/google/android/gms/dynamic/IObjectWrapper;)V

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public a(Lcom/google/android/gms/maps/internal/ILocationSourceDelegate;)V
    .locals 2

    invoke-virtual {p0}, Lmaps/z/bh;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lmaps/z/bh;->h:Lcom/google/android/gms/maps/internal/ILocationSourceDelegate;

    invoke-interface {v0}, Lcom/google/android/gms/maps/internal/ILocationSourceDelegate;->deactivate()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    if-eqz p1, :cond_2

    :goto_0
    iput-object p1, p0, Lmaps/z/bh;->h:Lcom/google/android/gms/maps/internal/ILocationSourceDelegate;

    invoke-virtual {p0}, Lmaps/z/bh;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :try_start_1
    iget-object v0, p0, Lmaps/z/bh;->h:Lcom/google/android/gms/maps/internal/ILocationSourceDelegate;

    invoke-interface {v0, p0}, Lcom/google/android/gms/maps/internal/ILocationSourceDelegate;->activate(Lcom/google/android/gms/maps/internal/IOnLocationChangeListener;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1

    :cond_2
    iget-object p1, p0, Lmaps/z/bh;->c:Lcom/google/android/gms/maps/internal/ILocationSourceDelegate;

    goto :goto_0

    :catch_1
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public a(Lcom/google/android/gms/maps/internal/IOnMyLocationChangeListener;)V
    .locals 0

    iput-object p1, p0, Lmaps/z/bh;->l:Lcom/google/android/gms/maps/internal/IOnMyLocationChangeListener;

    return-void
.end method

.method public a(Z)V
    .locals 1

    iget-boolean v0, p0, Lmaps/z/bh;->k:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lmaps/z/bh;->k:Z

    invoke-direct {p0}, Lmaps/z/bh;->f()V

    :cond_0
    return-void
.end method

.method public b()V
    .locals 2

    invoke-virtual {p0}, Lmaps/z/bh;->c()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/z/bh;->j:Z

    invoke-direct {p0}, Lmaps/z/bh;->f()V

    :try_start_0
    iget-object v0, p0, Lmaps/z/bh;->h:Lcom/google/android/gms/maps/internal/ILocationSourceDelegate;

    invoke-interface {v0}, Lcom/google/android/gms/maps/internal/ILocationSourceDelegate;->deactivate()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lmaps/z/bh;->b:Lmaps/i/h;

    iget-object v1, p0, Lmaps/z/bh;->f:Lmaps/y/x;

    invoke-interface {v0, v1}, Lmaps/i/h;->b(Lmaps/y/bc;)V

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public c()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/z/bh;->j:Z

    return v0
.end method

.method public d()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/z/bh;->k:Z

    return v0
.end method

.method public e()Landroid/location/Location;
    .locals 2

    invoke-virtual {p0}, Lmaps/z/bh;->c()Z

    move-result v0

    const-string v1, "MyLocation layer not enabled"

    invoke-static {v0, v1}, Lmaps/ap/q;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Lmaps/z/bh;->g:Landroid/location/Location;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lmaps/z/bh;->i:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->aI:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->a(Lmaps/bf/c;)V

    invoke-direct {p0}, Lmaps/z/bh;->h()V

    return-void
.end method

.method public onLocationChanged(Lcom/google/android/gms/dynamic/IObjectWrapper;)V
    .locals 6

    const/4 v5, 0x1

    invoke-static {p1}, Lcom/google/android/gms/dynamic/ObjectWrapper;->unwrap(Lcom/google/android/gms/dynamic/IObjectWrapper;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    invoke-static {v1, v2, v3, v4}, Lmaps/t/bx;->a(DD)Lmaps/t/bx;

    move-result-object v1

    new-instance v2, Lmaps/t/aj;

    invoke-virtual {v0}, Landroid/location/Location;->getBearing()F

    move-result v3

    invoke-virtual {v0}, Landroid/location/Location;->getAccuracy()F

    move-result v4

    float-to-int v4, v4

    invoke-direct {v2, v1, v3, v4}, Lmaps/t/aj;-><init>(Lmaps/t/bx;FI)V

    invoke-virtual {v2, v1}, Lmaps/t/aj;->b(Lmaps/t/bx;)V

    invoke-virtual {v0}, Landroid/location/Location;->hasBearing()Z

    move-result v1

    invoke-virtual {v2, v1}, Lmaps/t/aj;->a(Z)V

    iget-object v1, p0, Lmaps/z/bh;->f:Lmaps/y/x;

    invoke-virtual {v1, v2}, Lmaps/y/x;->a(Lmaps/t/aj;)V

    iget-object v1, p0, Lmaps/z/bh;->b:Lmaps/i/h;

    invoke-interface {v1, v5, v5}, Lmaps/i/h;->a(ZZ)V

    iget-object v1, p0, Lmaps/z/bh;->l:Lcom/google/android/gms/maps/internal/IOnMyLocationChangeListener;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lmaps/z/bh;->l:Lcom/google/android/gms/maps/internal/IOnMyLocationChangeListener;

    new-instance v2, Landroid/location/Location;

    invoke-direct {v2, v0}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    invoke-static {v2}, Lcom/google/android/gms/dynamic/ObjectWrapper;->wrap(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/gms/maps/internal/IOnMyLocationChangeListener;->onMyLocationChange(Lcom/google/android/gms/dynamic/IObjectWrapper;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    iput-object v0, p0, Lmaps/z/bh;->g:Landroid/location/Location;

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method
