.class final Lmaps/z/bi;
.super Lmaps/y/bc;

# interfaces
.implements Lmaps/z/bn;


# static fields
.field private static final b:Lmaps/y/am;


# instance fields
.field final a:Lmaps/au/e;

.field private final c:Lmaps/i/h;

.field private final d:Ljava/util/Map;

.field private final e:Lmaps/z/bl;

.field private final f:Ljava/util/SortedSet;

.field private g:I

.field private final h:Ljava/util/concurrent/ScheduledExecutorService;

.field private final i:Lmaps/bf/b;

.field private j:Lmaps/af/f;

.field private k:Lmaps/cr/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lmaps/y/am;->e:Lmaps/y/am;

    sput-object v0, Lmaps/z/bi;->b:Lmaps/y/am;

    return-void
.end method

.method constructor <init>(Lmaps/i/h;Lmaps/z/bl;Lmaps/au/e;Ljava/util/concurrent/ScheduledExecutorService;Lmaps/bf/b;)V
    .locals 1

    invoke-direct {p0}, Lmaps/y/bc;-><init>()V

    invoke-static {}, Lmaps/f/cs;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/z/bi;->d:Ljava/util/Map;

    new-instance v0, Lmaps/z/bb;

    invoke-direct {v0, p0}, Lmaps/z/bb;-><init>(Lmaps/z/bi;)V

    invoke-static {v0}, Lmaps/f/a;->a(Ljava/util/Comparator;)Ljava/util/TreeSet;

    move-result-object v0

    iput-object v0, p0, Lmaps/z/bi;->f:Ljava/util/SortedSet;

    iput-object p1, p0, Lmaps/z/bi;->c:Lmaps/i/h;

    iput-object p2, p0, Lmaps/z/bi;->e:Lmaps/z/bl;

    iput-object p3, p0, Lmaps/z/bi;->a:Lmaps/au/e;

    iput-object p4, p0, Lmaps/z/bi;->h:Ljava/util/concurrent/ScheduledExecutorService;

    iput-object p5, p0, Lmaps/z/bi;->i:Lmaps/bf/b;

    return-void
.end method

.method public static a(Lmaps/i/h;Lmaps/bf/b;)Lmaps/z/bn;
    .locals 6

    invoke-static {}, Lmaps/au/e;->a()Lmaps/au/e;

    move-result-object v3

    invoke-interface {p0}, Lmaps/i/h;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lmaps/z/bl;->a(Landroid/content/Context;)Lmaps/z/bl;

    move-result-object v2

    const/16 v0, 0xa

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newScheduledThreadPool(I)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v4

    new-instance v0, Lmaps/z/bi;

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lmaps/z/bi;-><init>(Lmaps/i/h;Lmaps/z/bl;Lmaps/au/e;Ljava/util/concurrent/ScheduledExecutorService;Lmaps/bf/b;)V

    return-object v0
.end method


# virtual methods
.method public a(Lcom/google/android/gms/maps/model/GroundOverlayOptions;)Lmaps/z/aa;
    .locals 6

    invoke-virtual {p0}, Lmaps/z/bi;->e()V

    monitor-enter p0

    :try_start_0
    new-instance v0, Lmaps/z/aa;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "go"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lmaps/z/bi;->g:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lmaps/z/bi;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lmaps/z/bi;->e:Lmaps/z/bl;

    iget-object v5, p0, Lmaps/z/bi;->i:Lmaps/bf/b;

    move-object v3, p1

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lmaps/z/aa;-><init>(Ljava/lang/String;Lmaps/z/bl;Lcom/google/android/gms/maps/model/GroundOverlayOptions;Lmaps/z/bi;Lmaps/bf/b;)V

    iget-object v1, p0, Lmaps/z/bi;->d:Ljava/util/Map;

    invoke-virtual {v0}, Lmaps/z/aa;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lmaps/z/bi;->f:Ljava/util/SortedSet;

    invoke-interface {v1, v0}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lmaps/z/bi;->h()V

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Lcom/google/android/gms/maps/model/PolylineOptions;)Lmaps/z/ac;
    .locals 4

    invoke-virtual {p0}, Lmaps/z/bi;->e()V

    monitor-enter p0

    :try_start_0
    new-instance v1, Lmaps/z/ac;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pl"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lmaps/z/bi;->g:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lmaps/z/bi;->g:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lmaps/z/bi;->i:Lmaps/bf/b;

    invoke-direct {v1, v0, p1, p0, v2}, Lmaps/z/ac;-><init>(Ljava/lang/String;Lcom/google/android/gms/maps/model/PolylineOptions;Lmaps/z/bi;Lmaps/bf/b;)V

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {v1}, Lmaps/z/ac;->c()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v0, p0, Lmaps/z/bi;->d:Ljava/util/Map;

    invoke-virtual {v1}, Lmaps/z/ac;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lmaps/z/bi;->f:Ljava/util/SortedSet;

    invoke-interface {v0, v1}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-virtual {p0}, Lmaps/z/bi;->h()V

    return-object v1

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0

    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0
.end method

.method public a(Lcom/google/android/gms/maps/model/TileOverlayOptions;)Lmaps/z/ar;
    .locals 6

    invoke-virtual {p0}, Lmaps/z/bi;->e()V

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "to"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmaps/z/bi;->g:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lmaps/z/bi;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lmaps/z/bi;->c:Lmaps/i/h;

    invoke-interface {v1}, Lmaps/i/h;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v4, p0, Lmaps/z/bi;->h:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v5, p0, Lmaps/z/bi;->i:Lmaps/bf/b;

    move-object v1, p1

    move-object v3, p0

    invoke-static/range {v0 .. v5}, Lmaps/z/ar;->a(Ljava/lang/String;Lcom/google/android/gms/maps/model/TileOverlayOptions;Landroid/content/res/Resources;Lmaps/z/bi;Ljava/util/concurrent/ScheduledExecutorService;Lmaps/bf/b;)Lmaps/z/ar;

    move-result-object v0

    iget-object v1, p0, Lmaps/z/bi;->d:Ljava/util/Map;

    invoke-virtual {v0}, Lmaps/z/ar;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lmaps/z/bi;->f:Ljava/util/SortedSet;

    invoke-interface {v1, v0}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lmaps/z/bi;->k:Lmaps/cr/c;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/z/bi;->k:Lmaps/cr/c;

    iget-object v2, p0, Lmaps/z/bi;->j:Lmaps/af/f;

    invoke-virtual {v0, v1, v2}, Lmaps/z/ar;->a(Lmaps/cr/c;Lmaps/af/f;)V

    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lmaps/z/bi;->h()V

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Lcom/google/android/gms/maps/model/PolygonOptions;)Lmaps/z/bd;
    .locals 4

    invoke-virtual {p0}, Lmaps/z/bi;->e()V

    monitor-enter p0

    :try_start_0
    new-instance v1, Lmaps/z/bd;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pg"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lmaps/z/bi;->g:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lmaps/z/bi;->g:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lmaps/z/bi;->i:Lmaps/bf/b;

    invoke-direct {v1, v0, p1, p0, v2}, Lmaps/z/bd;-><init>(Ljava/lang/String;Lcom/google/android/gms/maps/model/PolygonOptions;Lmaps/z/bi;Lmaps/bf/b;)V

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {v1}, Lmaps/z/bd;->c()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v0, p0, Lmaps/z/bi;->d:Ljava/util/Map;

    invoke-virtual {v1}, Lmaps/z/bd;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lmaps/z/bi;->f:Ljava/util/SortedSet;

    invoke-interface {v0, v1}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-virtual {p0}, Lmaps/z/bi;->h()V

    return-object v1

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0

    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0
.end method

.method public a(Lcom/google/android/gms/maps/model/CircleOptions;)Lmaps/z/n;
    .locals 4

    invoke-virtual {p0}, Lmaps/z/bi;->e()V

    monitor-enter p0

    :try_start_0
    new-instance v0, Lmaps/z/n;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ci"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lmaps/z/bi;->g:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lmaps/z/bi;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lmaps/z/bi;->i:Lmaps/bf/b;

    invoke-direct {v0, v1, p1, p0, v2}, Lmaps/z/n;-><init>(Ljava/lang/String;Lcom/google/android/gms/maps/model/CircleOptions;Lmaps/z/bi;Lmaps/bf/b;)V

    invoke-virtual {v0}, Lmaps/z/n;->c()V

    iget-object v1, p0, Lmaps/z/bi;->d:Ljava/util/Map;

    invoke-virtual {v0}, Lmaps/z/n;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lmaps/z/bi;->f:Ljava/util/SortedSet;

    invoke-interface {v1, v0}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lmaps/z/bi;->h()V

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public declared-synchronized a(I)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/z/bi;->f:Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/z/a;

    invoke-interface {v0, p1}, Lmaps/z/a;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized a(Lmaps/cr/c;Lmaps/af/f;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lmaps/z/bi;->k:Lmaps/cr/c;

    iput-object p2, p0, Lmaps/z/bi;->j:Lmaps/af/f;

    iget-object v0, p0, Lmaps/z/bi;->f:Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/z/a;

    iget-object v2, p0, Lmaps/z/bi;->k:Lmaps/cr/c;

    iget-object v3, p0, Lmaps/z/bi;->j:Lmaps/af/f;

    invoke-interface {v0, v2, v3}, Lmaps/z/a;->a(Lmaps/cr/c;Lmaps/af/f;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/z/bi;->f:Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/z/a;

    invoke-virtual {p1}, Lmaps/cr/c;->D()V

    invoke-interface {v0, p1, p2, p3}, Lmaps/z/a;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    invoke-virtual {p1}, Lmaps/cr/c;->F()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    monitor-exit p0

    return-void
.end method

.method a(Lmaps/z/a;)V
    .locals 2

    iget-object v0, p0, Lmaps/z/bi;->d:Ljava/util/Map;

    invoke-interface {p1}, Lmaps/z/a;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lmaps/z/bi;->f:Ljava/util/SortedSet;

    invoke-interface {v0, p1}, Ljava/util/SortedSet;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/z/bi;->d:Ljava/util/Map;

    invoke-interface {p1}, Lmaps/z/a;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p1}, Lmaps/z/a;->b()V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/z/bi;->f:Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/z/a;

    invoke-interface {v0, p1}, Lmaps/z/a;->a(Z)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public declared-synchronized a(Lmaps/bq/d;Lmaps/cr/c;)Z
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/z/bi;->f:Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/z/a;

    invoke-interface {v0, p1, p2}, Lmaps/z/a;->a(Lmaps/bq/d;Lmaps/cr/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    const/4 v0, 0x1

    monitor-exit p0

    return v0
.end method

.method public declared-synchronized a_(Lmaps/cr/c;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/z/bi;->f:Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/z/a;

    invoke-interface {v0, p1}, Lmaps/z/a;->a(Lmaps/cr/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    monitor-exit p0

    return-void
.end method

.method public bridge synthetic b(Lcom/google/android/gms/maps/model/CircleOptions;)Lcom/google/android/gms/maps/model/internal/ICircleDelegate;
    .locals 1

    invoke-virtual {p0, p1}, Lmaps/z/bi;->a(Lcom/google/android/gms/maps/model/CircleOptions;)Lmaps/z/n;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic b(Lcom/google/android/gms/maps/model/GroundOverlayOptions;)Lcom/google/android/gms/maps/model/internal/IGroundOverlayDelegate;
    .locals 1

    invoke-virtual {p0, p1}, Lmaps/z/bi;->a(Lcom/google/android/gms/maps/model/GroundOverlayOptions;)Lmaps/z/aa;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic b(Lcom/google/android/gms/maps/model/PolygonOptions;)Lcom/google/android/gms/maps/model/internal/IPolygonDelegate;
    .locals 1

    invoke-virtual {p0, p1}, Lmaps/z/bi;->a(Lcom/google/android/gms/maps/model/PolygonOptions;)Lmaps/z/bd;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic b(Lcom/google/android/gms/maps/model/PolylineOptions;)Lcom/google/android/gms/maps/model/internal/IPolylineDelegate;
    .locals 1

    invoke-virtual {p0, p1}, Lmaps/z/bi;->a(Lcom/google/android/gms/maps/model/PolylineOptions;)Lmaps/z/ac;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic b(Lcom/google/android/gms/maps/model/TileOverlayOptions;)Lcom/google/android/gms/maps/model/internal/ITileOverlayDelegate;
    .locals 1

    invoke-virtual {p0, p1}, Lmaps/z/bi;->a(Lcom/google/android/gms/maps/model/TileOverlayOptions;)Lmaps/z/ar;

    move-result-object v0

    return-object v0
.end method

.method public b()Lmaps/y/am;
    .locals 1

    sget-object v0, Lmaps/z/bi;->b:Lmaps/y/am;

    return-object v0
.end method

.method b(Lmaps/z/a;)V
    .locals 1

    iget-object v0, p0, Lmaps/z/bi;->f:Ljava/util/SortedSet;

    invoke-interface {v0, p1}, Ljava/util/SortedSet;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method c(Lmaps/z/a;)V
    .locals 1

    iget-object v0, p0, Lmaps/z/bi;->f:Ljava/util/SortedSet;

    invoke-interface {v0, p1}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method e()V
    .locals 1

    iget-object v0, p0, Lmaps/z/bi;->a:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    return-void
.end method

.method public e_()V
    .locals 2

    iget-object v0, p0, Lmaps/z/bi;->f:Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/z/a;

    invoke-interface {v0}, Lmaps/z/a;->a()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method h()V
    .locals 1

    iget-object v0, p0, Lmaps/z/bi;->c:Lmaps/i/h;

    invoke-interface {v0}, Lmaps/i/h;->a()V

    return-void
.end method

.method public i()V
    .locals 2

    invoke-virtual {p0}, Lmaps/z/bi;->e()V

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/z/bi;->f:Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/z/a;

    invoke-interface {v0}, Lmaps/z/a;->b()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lmaps/z/bi;->f:Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->clear()V

    iget-object v0, p0, Lmaps/z/bi;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p0}, Lmaps/z/bi;->h()V

    return-void
.end method

.method public j()Lmaps/y/bc;
    .locals 0

    return-object p0
.end method
