.class public Lmaps/z/bm;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnKeyListener;


# instance fields
.field private a:I

.field private b:J

.field private final c:Lmaps/ae/d;

.field private final d:Lmaps/i/c;

.field private final e:Lmaps/bf/b;


# direct methods
.method constructor <init>(Lmaps/i/c;Lmaps/ae/d;Lmaps/bf/b;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lmaps/z/bm;->a:I

    iput-object p1, p0, Lmaps/z/bm;->d:Lmaps/i/c;

    iput-object p2, p0, Lmaps/z/bm;->c:Lmaps/ae/d;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lmaps/z/bm;->b:J

    iput-object p3, p0, Lmaps/z/bm;->e:Lmaps/bf/b;

    return-void
.end method

.method static a(Lmaps/i/c;Lmaps/bf/b;)Lmaps/z/bm;
    .locals 2

    new-instance v0, Lmaps/z/bm;

    new-instance v1, Lmaps/co/a;

    invoke-direct {v1}, Lmaps/co/a;-><init>()V

    invoke-direct {v0, p0, v1, p1}, Lmaps/z/bm;-><init>(Lmaps/i/c;Lmaps/ae/d;Lmaps/bf/b;)V

    return-object v0
.end method

.method private a(Landroid/view/KeyEvent;)V
    .locals 5

    const/16 v0, 0x19

    iget v1, p0, Lmaps/z/bm;->a:I

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lmaps/z/bm;->c:Lmaps/ae/d;

    invoke-interface {v1}, Lmaps/ae/d;->b()J

    move-result-wide v1

    iget-wide v3, p0, Lmaps/z/bm;->b:J

    sub-long/2addr v1, v3

    const-wide/16 v3, 0xfa

    cmp-long v1, v1, v3

    if-gez v1, :cond_0

    const/16 v0, 0xc

    :cond_0
    invoke-direct {p0, p1, v0}, Lmaps/z/bm;->a(Landroid/view/KeyEvent;I)V

    return-void
.end method

.method private a(Landroid/view/KeyEvent;I)V
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lmaps/z/bm;->e:Lmaps/bf/b;

    sget-object v2, Lmaps/bf/c;->ba:Lmaps/bf/c;

    invoke-interface {v1, v2}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    neg-int p2, p2

    :goto_1
    iget-object v1, p0, Lmaps/z/bm;->d:Lmaps/i/c;

    int-to-float v0, v0

    int-to-float v2, p2

    invoke-interface {v1, v0, v2}, Lmaps/i/c;->a(FF)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lmaps/z/bm;->e:Lmaps/bf/b;

    sget-object v2, Lmaps/bf/c;->bb:Lmaps/bf/c;

    invoke-interface {v1, v2}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    goto :goto_1

    :pswitch_2
    iget-object v1, p0, Lmaps/z/bm;->e:Lmaps/bf/b;

    sget-object v2, Lmaps/bf/c;->bd:Lmaps/bf/c;

    invoke-interface {v1, v2}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    move v3, v0

    move v0, p2

    move p2, v3

    goto :goto_1

    :pswitch_3
    iget-object v1, p0, Lmaps/z/bm;->e:Lmaps/bf/b;

    sget-object v2, Lmaps/bf/c;->bc:Lmaps/bf/c;

    invoke-interface {v1, v2}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    neg-int p2, p2

    move v3, v0

    move v0, p2

    move p2, v3

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method private b(Landroid/view/KeyEvent;)Z
    .locals 1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3

    const/4 v0, 0x1

    invoke-direct {p0, p3}, Lmaps/z/bm;->b(Landroid/view/KeyEvent;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_2

    invoke-direct {p0, p3}, Lmaps/z/bm;->a(Landroid/view/KeyEvent;)V

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    iput v1, p0, Lmaps/z/bm;->a:I

    iget-object v1, p0, Lmaps/z/bm;->c:Lmaps/ae/d;

    invoke-interface {v1}, Lmaps/ae/d;->b()J

    move-result-wide v1

    iput-wide v1, p0, Lmaps/z/bm;->b:J

    goto :goto_0

    :cond_2
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    iget v1, p0, Lmaps/z/bm;->a:I

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v1, -0x1

    iput v1, p0, Lmaps/z/bm;->a:I

    goto :goto_0
.end method
