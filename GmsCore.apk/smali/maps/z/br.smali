.class public abstract Lmaps/z/br;
.super Ljava/lang/Object;


# static fields
.field private static final a:Lmaps/z/ba;


# instance fields
.field public final c:B


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lmaps/z/ba;

    sget v1, Lmaps/cc/d;->U:I

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lmaps/z/ba;-><init>(ILmaps/z/bt;)V

    sput-object v0, Lmaps/z/br;->a:Lmaps/z/ba;

    return-void
.end method

.method private constructor <init>(B)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-byte p1, p0, Lmaps/z/br;->c:B

    return-void
.end method

.method synthetic constructor <init>(BLmaps/z/bt;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/z/br;-><init>(B)V

    return-void
.end method

.method public static a(F)Lmaps/z/ap;
    .locals 2

    new-instance v0, Lmaps/z/ap;

    invoke-static {}, Lmaps/z/br;->a()Lmaps/z/ba;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lmaps/z/ap;-><init>(Lmaps/z/br;F)V

    return-object v0
.end method

.method public static a(Landroid/graphics/Bitmap;)Lmaps/z/aq;
    .locals 2

    new-instance v0, Lmaps/z/aq;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lmaps/z/aq;-><init>(Landroid/graphics/Bitmap;Lmaps/z/bt;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Lmaps/z/ay;
    .locals 2

    new-instance v0, Lmaps/z/ay;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lmaps/z/ay;-><init>(Ljava/lang/String;Lmaps/z/bt;)V

    return-object v0
.end method

.method public static a()Lmaps/z/ba;
    .locals 1

    sget-object v0, Lmaps/z/br;->a:Lmaps/z/ba;

    return-object v0
.end method

.method public static a(I)Lmaps/z/q;
    .locals 2

    new-instance v0, Lmaps/z/q;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lmaps/z/q;-><init>(ILmaps/z/bt;)V

    return-object v0
.end method

.method public static b(Ljava/lang/String;)Lmaps/z/ah;
    .locals 2

    new-instance v0, Lmaps/z/ah;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lmaps/z/ah;-><init>(Ljava/lang/String;Lmaps/z/bt;)V

    return-object v0
.end method

.method public static b(I)Lmaps/z/ba;
    .locals 2

    new-instance v0, Lmaps/z/ba;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lmaps/z/ba;-><init>(ILmaps/z/bt;)V

    return-object v0
.end method

.method public static c(Ljava/lang/String;)Lmaps/z/m;
    .locals 2

    new-instance v0, Lmaps/z/m;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lmaps/z/m;-><init>(Ljava/lang/String;Lmaps/z/bt;)V

    return-object v0
.end method


# virtual methods
.method public abstract a(Landroid/content/Context;)Landroid/graphics/Bitmap;
.end method
