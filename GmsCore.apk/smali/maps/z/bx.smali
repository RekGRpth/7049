.class Lmaps/z/bx;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lmaps/z/az;

.field private final b:Ljava/util/Random;

.field private final c:Lmaps/t/ah;

.field private d:Ljava/util/concurrent/ScheduledFuture;

.field private e:I


# direct methods
.method public constructor <init>(Lmaps/z/az;Lmaps/t/ah;Ljava/util/Random;)V
    .locals 1

    iput-object p1, p0, Lmaps/z/bx;->a:Lmaps/z/az;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/z/bx;->d:Ljava/util/concurrent/ScheduledFuture;

    const/4 v0, 0x0

    iput v0, p0, Lmaps/z/bx;->e:I

    iput-object p2, p0, Lmaps/z/bx;->c:Lmaps/t/ah;

    iput-object p3, p0, Lmaps/z/bx;->b:Ljava/util/Random;

    iget-object v0, p1, Lmaps/z/az;->b:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private declared-synchronized a(Lcom/google/android/gms/maps/model/Tile;)V
    .locals 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    iget v2, p1, Lcom/google/android/gms/maps/model/Tile;->width:I

    if-lez v2, :cond_0

    move v2, v0

    :goto_0
    const-string v3, "width of tile image must be positive"

    invoke-static {v2, v3}, Lmaps/ap/q;->a(ZLjava/lang/Object;)V

    iget v2, p1, Lcom/google/android/gms/maps/model/Tile;->height:I

    if-lez v2, :cond_1

    :goto_1
    const-string v1, "height of tile image must be positive"

    invoke-static {v0, v1}, Lmaps/ap/q;->a(ZLjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    new-instance v0, Lmaps/t/bz;

    iget-object v1, p0, Lmaps/z/bx;->c:Lmaps/t/ah;

    const/4 v2, 0x0

    iget v3, p1, Lcom/google/android/gms/maps/model/Tile;->width:I

    iget v4, p1, Lcom/google/android/gms/maps/model/Tile;->height:I

    iget-object v5, p1, Lcom/google/android/gms/maps/model/Tile;->data:[B

    sget-object v6, Lmaps/o/c;->t:Lmaps/o/c;

    invoke-direct/range {v0 .. v6}, Lmaps/t/bz;-><init>(Lmaps/t/ah;III[BLmaps/o/c;)V

    const/4 v1, 0x0

    iput-object v1, p0, Lmaps/z/bx;->d:Ljava/util/concurrent/ScheduledFuture;

    iget-object v1, p0, Lmaps/z/bx;->a:Lmaps/z/az;

    iget-object v1, v1, Lmaps/z/az;->b:Ljava/util/Set;

    invoke-interface {v1, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    iget-object v1, p0, Lmaps/z/bx;->a:Lmaps/z/az;

    iget-object v2, p0, Lmaps/z/bx;->c:Lmaps/t/ah;

    invoke-static {v1, v2, v0}, Lmaps/z/az;->a(Lmaps/z/az;Lmaps/t/ah;Lmaps/t/bz;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_2
    monitor-exit p0

    return-void

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_2
    invoke-direct {p0}, Lmaps/z/bx;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b()V
    .locals 2

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lmaps/z/bx;->d:Ljava/util/concurrent/ScheduledFuture;

    iget-object v0, p0, Lmaps/z/bx;->a:Lmaps/z/az;

    iget-object v0, v0, Lmaps/z/az;->b:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/z/bx;->a:Lmaps/z/az;

    iget-object v1, p0, Lmaps/z/bx;->c:Lmaps/t/ah;

    invoke-static {v0, v1}, Lmaps/z/az;->a(Lmaps/z/az;Lmaps/t/ah;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public declared-synchronized a()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/z/bx;->d:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/z/bx;->d:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/z/bx;->d:Ljava/util/concurrent/ScheduledFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized run()V
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/z/bx;->a:Lmaps/z/az;

    invoke-static {v0}, Lmaps/z/az;->a(Lmaps/z/az;)Lcom/google/android/gms/maps/model/TileProvider;

    move-result-object v0

    iget-object v1, p0, Lmaps/z/bx;->c:Lmaps/t/ah;

    invoke-virtual {v1}, Lmaps/t/ah;->d()I

    move-result v1

    iget-object v2, p0, Lmaps/z/bx;->c:Lmaps/t/ah;

    invoke-virtual {v2}, Lmaps/t/ah;->e()I

    move-result v2

    iget-object v3, p0, Lmaps/z/bx;->c:Lmaps/t/ah;

    invoke-virtual {v3}, Lmaps/t/ah;->c()I

    move-result v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/maps/model/TileProvider;->getTile(III)Lcom/google/android/gms/maps/model/Tile;

    move-result-object v0

    if-nez v0, :cond_1

    const-wide/high16 v0, 0x4069000000000000L

    const-wide/high16 v2, 0x4000000000000000L

    iget v4, p0, Lmaps/z/bx;->e:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lmaps/z/bx;->e:I

    int-to-double v4, v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    mul-double/2addr v0, v2

    iget-object v2, p0, Lmaps/z/bx;->b:Ljava/util/Random;

    const/16 v3, 0x64

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    int-to-double v2, v2

    add-double/2addr v0, v2

    double-to-long v0, v0

    const-wide/32 v2, 0x493e0

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    iget-object v2, p0, Lmaps/z/bx;->a:Lmaps/z/az;

    invoke-static {v2}, Lmaps/z/az;->b(Lmaps/z/az;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v2

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, p0, v0, v1, v3}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Lmaps/z/bx;->d:Ljava/util/concurrent/ScheduledFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    invoke-direct {p0}, Lmaps/z/bx;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_2
    sget-object v1, Lcom/google/android/gms/maps/model/TileProvider;->NO_TILE:Lcom/google/android/gms/maps/model/Tile;

    if-ne v0, v1, :cond_2

    invoke-direct {p0}, Lmaps/z/bx;->b()V

    goto :goto_0

    :cond_2
    invoke-direct {p0, v0}, Lmaps/z/bx;->a(Lcom/google/android/gms/maps/model/Tile;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method
