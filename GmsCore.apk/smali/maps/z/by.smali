.class public final Lmaps/z/by;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/y/al;
.implements Lmaps/y/bn;
.implements Lmaps/y/t;
.implements Lmaps/z/bu;


# instance fields
.field private final a:Lmaps/i/c;

.field private final b:Lmaps/i/h;

.field private final c:Lmaps/y/k;

.field private final d:Lmaps/y/f;

.field private final e:Ljava/util/Map;

.field private final f:Lmaps/z/bl;

.field private final g:Landroid/view/ViewGroup;

.field private final h:Lmaps/aa/e;

.field private final i:Lmaps/au/e;

.field private j:Lcom/google/android/gms/maps/internal/IOnMarkerClickListener;

.field private k:Lcom/google/android/gms/maps/internal/IOnMarkerDragListener;

.field private l:Lcom/google/android/gms/maps/internal/IOnInfoWindowClickListener;

.field private m:Lcom/google/android/gms/maps/internal/IInfoWindowAdapter;

.field private n:I

.field private final o:Lmaps/bf/b;


# direct methods
.method constructor <init>(Lmaps/i/h;Lmaps/i/c;Lmaps/y/k;Lmaps/y/f;Landroid/view/ViewGroup;Lmaps/aa/e;Lmaps/z/bl;Lmaps/au/e;Lmaps/bf/b;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lmaps/f/cs;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/z/by;->e:Ljava/util/Map;

    iput-object p1, p0, Lmaps/z/by;->b:Lmaps/i/h;

    iput-object p2, p0, Lmaps/z/by;->a:Lmaps/i/c;

    iput-object p3, p0, Lmaps/z/by;->c:Lmaps/y/k;

    iput-object p4, p0, Lmaps/z/by;->d:Lmaps/y/f;

    iput-object p5, p0, Lmaps/z/by;->g:Landroid/view/ViewGroup;

    iput-object p6, p0, Lmaps/z/by;->h:Lmaps/aa/e;

    iput-object p7, p0, Lmaps/z/by;->f:Lmaps/z/bl;

    iput-object p8, p0, Lmaps/z/by;->i:Lmaps/au/e;

    iput-object p9, p0, Lmaps/z/by;->o:Lmaps/bf/b;

    iget-object v0, p0, Lmaps/z/by;->c:Lmaps/y/k;

    invoke-virtual {v0, p0}, Lmaps/y/k;->a(Lmaps/y/bn;)V

    iget-object v0, p0, Lmaps/z/by;->c:Lmaps/y/k;

    invoke-virtual {v0, p0}, Lmaps/y/k;->a(Lmaps/y/al;)V

    invoke-virtual {p4, p0}, Lmaps/y/f;->a(Lmaps/y/t;)V

    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/content/res/Resources;)Landroid/view/ViewGroup;
    .locals 3

    const/4 v2, -0x2

    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    sget v1, Lmaps/cc/d;->ab:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    return-object v0
.end method

.method static synthetic a(Lmaps/z/by;)Lmaps/au/e;
    .locals 1

    iget-object v0, p0, Lmaps/z/by;->i:Lmaps/au/e;

    return-object v0
.end method

.method private a(Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLng;Lmaps/z/br;FFZZ)Lmaps/l/ay;
    .locals 10

    const/4 v4, 0x0

    const/4 v8, 0x0

    iget-object v1, p0, Lmaps/z/by;->f:Lmaps/z/bl;

    invoke-virtual {v1, p3}, Lmaps/z/bl;->a(Lmaps/z/br;)Landroid/graphics/Bitmap;

    move-result-object v3

    const/4 v9, 0x0

    new-instance v1, Lmaps/l/ay;

    invoke-static {p2}, Lmaps/au/c;->b(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/t/bx;

    move-result-object v2

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, p4

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, p5

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    move-object v7, p1

    invoke-direct/range {v1 .. v9}, Lmaps/l/ay;-><init>(Lmaps/t/bx;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;IILjava/lang/String;Ljava/lang/String;Z)V

    move/from16 v0, p6

    invoke-virtual {v1, v0}, Lmaps/l/ay;->a(Z)V

    move/from16 v0, p7

    invoke-virtual {v1, v0}, Lmaps/l/ay;->b(Z)V

    return-object v1
.end method

.method static a(Lmaps/i/h;Lmaps/i/c;Landroid/content/Context;Landroid/content/res/Resources;Lmaps/bf/b;)Lmaps/z/by;
    .locals 10

    invoke-static {p2, p3}, Lmaps/z/by;->a(Landroid/content/Context;Landroid/content/res/Resources;)Landroid/view/ViewGroup;

    move-result-object v5

    invoke-static {p2}, Lmaps/aa/d;->a(Landroid/content/Context;)Lmaps/aa/d;

    move-result-object v6

    invoke-static {}, Lmaps/au/e;->a()Lmaps/au/e;

    move-result-object v8

    sget-object v0, Lmaps/y/am;->w:Lmaps/y/am;

    invoke-interface {p0, v0}, Lmaps/i/h;->a(Lmaps/y/am;)Lmaps/y/k;

    move-result-object v3

    invoke-interface {p0}, Lmaps/i/h;->p()Lmaps/af/v;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/af/v;->i()Lmaps/y/f;

    move-result-object v4

    invoke-static {p2}, Lmaps/z/bl;->a(Landroid/content/Context;)Lmaps/z/bl;

    move-result-object v7

    new-instance v0, Lmaps/z/by;

    move-object v1, p0

    move-object v2, p1

    move-object v9, p4

    invoke-direct/range {v0 .. v9}, Lmaps/z/by;-><init>(Lmaps/i/h;Lmaps/i/c;Lmaps/y/k;Lmaps/y/f;Landroid/view/ViewGroup;Lmaps/aa/e;Lmaps/z/bl;Lmaps/au/e;Lmaps/bf/b;)V

    return-object v0
.end method

.method private a(Lmaps/z/bv;)V
    .locals 3

    iget-object v0, p0, Lmaps/z/by;->i:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->c()V

    iget-object v0, p0, Lmaps/z/by;->e:Ljava/util/Map;

    invoke-virtual {p1}, Lmaps/z/bv;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/z/bv;

    if-eq p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lmaps/z/by;->e:Ljava/util/Map;

    invoke-static {v0}, Lmaps/z/bv;->b(Lmaps/z/bv;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lmaps/z/by;->f:Lmaps/z/bl;

    invoke-static {v0}, Lmaps/z/bv;->c(Lmaps/z/bv;)Lmaps/z/br;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmaps/z/bl;->b(Lmaps/z/br;)V

    iget-object v1, p0, Lmaps/z/by;->c:Lmaps/y/k;

    invoke-static {v0}, Lmaps/z/bv;->a(Lmaps/z/bv;)Lmaps/l/ay;

    move-result-object v0

    invoke-virtual {v1, v0}, Lmaps/y/k;->b(Lmaps/l/ay;)V

    goto :goto_0
.end method

.method static synthetic a(Lmaps/z/by;Lmaps/z/bv;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/z/by;->a(Lmaps/z/bv;)V

    return-void
.end method

.method private b(Lmaps/z/bv;)V
    .locals 4

    const/4 v1, 0x0

    invoke-static {p1}, Lmaps/z/bv;->a(Lmaps/z/bv;)Lmaps/l/ay;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/l/ay;->b()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    iget-object v0, p0, Lmaps/z/by;->m:Lcom/google/android/gms/maps/internal/IInfoWindowAdapter;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lmaps/z/by;->m:Lcom/google/android/gms/maps/internal/IInfoWindowAdapter;

    invoke-interface {v0, p1}, Lcom/google/android/gms/maps/internal/IInfoWindowAdapter;->getInfoWindow(Lcom/google/android/gms/maps/model/internal/IMarkerDelegate;)Lcom/google/android/gms/dynamic/IObjectWrapper;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/dynamic/ObjectWrapper;->unwrap(Lcom/google/android/gms/dynamic/IObjectWrapper;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    if-nez v0, :cond_3

    :try_start_1
    iget-object v0, p0, Lmaps/z/by;->m:Lcom/google/android/gms/maps/internal/IInfoWindowAdapter;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lmaps/z/by;->m:Lcom/google/android/gms/maps/internal/IInfoWindowAdapter;

    invoke-interface {v0, p1}, Lcom/google/android/gms/maps/internal/IInfoWindowAdapter;->getInfoContents(Lcom/google/android/gms/maps/model/internal/IMarkerDelegate;)Lcom/google/android/gms/dynamic/IObjectWrapper;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/dynamic/ObjectWrapper;->unwrap(Lcom/google/android/gms/dynamic/IObjectWrapper;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    if-nez v0, :cond_2

    invoke-static {p1}, Lmaps/z/bv;->d(Lmaps/z/bv;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lmaps/ap/p;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/z/by;->h:Lmaps/aa/e;

    invoke-virtual {p1}, Lmaps/z/bv;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lmaps/aa/e;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lmaps/z/by;->h:Lmaps/aa/e;

    invoke-virtual {p1}, Lmaps/z/bv;->getSnippet()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lmaps/aa/e;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lmaps/z/by;->h:Lmaps/aa/e;

    invoke-interface {v0}, Lmaps/aa/e;->a()Landroid/view/View;

    move-result-object v0

    :cond_2
    iget-object v1, p0, Lmaps/z/by;->g:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v1, p0, Lmaps/z/by;->g:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lmaps/z/by;->g:Landroid/view/ViewGroup;

    :cond_3
    iget-object v1, p0, Lmaps/z/by;->b:Lmaps/i/h;

    invoke-static {p1}, Lmaps/z/bv;->a(Lmaps/z/bv;)Lmaps/l/ay;

    move-result-object v2

    new-instance v3, Lmaps/y/ah;

    invoke-direct {v3, v0}, Lmaps/y/ah;-><init>(Landroid/view/View;)V

    invoke-interface {v1, v2, v3}, Lmaps/i/h;->a(Lmaps/y/bh;Lmaps/y/ah;)V

    goto :goto_0

    :cond_4
    move-object v0, v1

    goto :goto_1

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1

    :cond_5
    move-object v0, v1

    goto :goto_2

    :catch_1
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method static synthetic b(Lmaps/z/by;)V
    .locals 0

    invoke-direct {p0}, Lmaps/z/by;->c()V

    return-void
.end method

.method static synthetic b(Lmaps/z/by;Lmaps/z/bv;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/z/by;->b(Lmaps/z/bv;)V

    return-void
.end method

.method private c(Lmaps/y/bh;)Lmaps/z/bv;
    .locals 2

    instance-of v0, p1, Lmaps/l/ay;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    check-cast p1, Lmaps/l/ay;

    iget-object v0, p0, Lmaps/z/by;->e:Ljava/util/Map;

    invoke-virtual {p1}, Lmaps/l/ay;->p()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/z/bv;

    goto :goto_0
.end method

.method private c()V
    .locals 1

    iget-object v0, p0, Lmaps/z/by;->b:Lmaps/i/h;

    invoke-interface {v0}, Lmaps/i/h;->a()V

    return-void
.end method

.method private c(Lmaps/z/bv;)V
    .locals 2

    invoke-static {p1}, Lmaps/z/bv;->a(Lmaps/z/bv;)Lmaps/l/ay;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/l/ay;->b()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lmaps/z/by;->d:Lmaps/y/f;

    monitor-enter v1

    :try_start_0
    invoke-static {p1}, Lmaps/z/bv;->a(Lmaps/z/bv;)Lmaps/l/ay;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/l/ay;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/z/by;->b:Lmaps/i/h;

    invoke-interface {v0}, Lmaps/i/h;->j()V

    :cond_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic c(Lmaps/z/by;)V
    .locals 0

    invoke-direct {p0}, Lmaps/z/by;->d()V

    return-void
.end method

.method static synthetic c(Lmaps/z/by;Lmaps/z/bv;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/z/by;->c(Lmaps/z/bv;)V

    return-void
.end method

.method private d()V
    .locals 1

    iget-object v0, p0, Lmaps/z/by;->c:Lmaps/y/k;

    invoke-virtual {v0}, Lmaps/y/k;->e()V

    return-void
.end method

.method private d(Lmaps/z/bv;)Z
    .locals 2

    iget-object v1, p0, Lmaps/z/by;->d:Lmaps/y/f;

    monitor-enter v1

    :try_start_0
    invoke-static {p1}, Lmaps/z/bv;->a(Lmaps/z/bv;)Lmaps/l/ay;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/l/ay;->o()Z

    move-result v0

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic d(Lmaps/z/by;Lmaps/z/bv;)Z
    .locals 1

    invoke-direct {p0, p1}, Lmaps/z/by;->d(Lmaps/z/bv;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public bridge synthetic a(Lcom/google/android/gms/maps/model/MarkerOptions;)Lcom/google/android/gms/maps/model/internal/IMarkerDelegate;
    .locals 1

    invoke-virtual {p0, p1}, Lmaps/z/by;->b(Lcom/google/android/gms/maps/model/MarkerOptions;)Lmaps/z/bv;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 3

    iget-object v0, p0, Lmaps/z/by;->i:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v1, p0, Lmaps/z/by;->d:Lmaps/y/f;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lmaps/z/by;->c:Lmaps/y/k;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, p0, Lmaps/z/by;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lmaps/z/by;->c:Lmaps/y/k;

    invoke-virtual {v0}, Lmaps/y/k;->d()V

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    return-void

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0
.end method

.method public a(Lcom/google/android/gms/maps/internal/IInfoWindowAdapter;)V
    .locals 1

    iget-object v0, p0, Lmaps/z/by;->i:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iput-object p1, p0, Lmaps/z/by;->m:Lcom/google/android/gms/maps/internal/IInfoWindowAdapter;

    return-void
.end method

.method public a(Lcom/google/android/gms/maps/internal/IOnInfoWindowClickListener;)V
    .locals 1

    iget-object v0, p0, Lmaps/z/by;->i:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iput-object p1, p0, Lmaps/z/by;->l:Lcom/google/android/gms/maps/internal/IOnInfoWindowClickListener;

    return-void
.end method

.method public a(Lcom/google/android/gms/maps/internal/IOnMarkerClickListener;)V
    .locals 1

    iget-object v0, p0, Lmaps/z/by;->i:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iput-object p1, p0, Lmaps/z/by;->j:Lcom/google/android/gms/maps/internal/IOnMarkerClickListener;

    return-void
.end method

.method public a(Lcom/google/android/gms/maps/internal/IOnMarkerDragListener;)V
    .locals 1

    iget-object v0, p0, Lmaps/z/by;->i:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iput-object p1, p0, Lmaps/z/by;->k:Lcom/google/android/gms/maps/internal/IOnMarkerDragListener;

    return-void
.end method

.method public a(Lmaps/l/ay;)V
    .locals 2

    iget-object v0, p0, Lmaps/z/by;->i:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    invoke-direct {p0, p1}, Lmaps/z/by;->c(Lmaps/y/bh;)Lmaps/z/bv;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lmaps/z/by;->k:Lcom/google/android/gms/maps/internal/IOnMarkerDragListener;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lmaps/z/by;->k:Lcom/google/android/gms/maps/internal/IOnMarkerDragListener;

    invoke-interface {v1, v0}, Lcom/google/android/gms/maps/internal/IOnMarkerDragListener;->onMarkerDragStart(Lcom/google/android/gms/maps/model/internal/IMarkerDelegate;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public a(Lmaps/y/ba;Lmaps/y/bh;)V
    .locals 3

    iget-object v0, p0, Lmaps/z/by;->i:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    invoke-direct {p0, p2}, Lmaps/z/by;->c(Lmaps/y/bh;)Lmaps/z/bv;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    iget-object v1, p0, Lmaps/z/by;->j:Lcom/google/android/gms/maps/internal/IOnMarkerClickListener;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lmaps/z/by;->j:Lcom/google/android/gms/maps/internal/IOnMarkerClickListener;

    invoke-interface {v1, v0}, Lcom/google/android/gms/maps/internal/IOnMarkerClickListener;->onMarkerClick(Lcom/google/android/gms/maps/model/internal/IMarkerDelegate;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    invoke-direct {p0, v0}, Lmaps/z/by;->b(Lmaps/z/bv;)V

    const/16 v1, 0x12c

    iget-object v2, p0, Lmaps/z/by;->a:Lmaps/i/c;

    invoke-static {v0}, Lmaps/z/bv;->a(Lmaps/z/bv;)Lmaps/l/ay;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/l/ay;->c_()Lmaps/t/bx;

    move-result-object v0

    invoke-interface {v2, v0, v1}, Lmaps/i/c;->a(Lmaps/t/bx;I)V

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public a(Lmaps/y/bh;)V
    .locals 2

    iget-object v0, p0, Lmaps/z/by;->i:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v0, p0, Lmaps/z/by;->l:Lcom/google/android/gms/maps/internal/IOnInfoWindowClickListener;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lmaps/z/by;->c(Lmaps/y/bh;)Lmaps/z/bv;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v1, p0, Lmaps/z/by;->l:Lcom/google/android/gms/maps/internal/IOnInfoWindowClickListener;

    invoke-interface {v1, v0}, Lcom/google/android/gms/maps/internal/IOnInfoWindowClickListener;->onInfoWindowClick(Lcom/google/android/gms/maps/model/internal/IMarkerDelegate;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public b()Lmaps/y/bc;
    .locals 1

    iget-object v0, p0, Lmaps/z/by;->c:Lmaps/y/k;

    return-object v0
.end method

.method public b(Lcom/google/android/gms/maps/model/MarkerOptions;)Lmaps/z/bv;
    .locals 10

    iget-object v0, p0, Lmaps/z/by;->i:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->getPosition()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "no position in marker options"

    invoke-static {v0, v1}, Lmaps/ap/q;->a(ZLjava/lang/Object;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "m"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmaps/z/by;->n:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lmaps/z/by;->n:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->getIcon()Lcom/google/android/gms/maps/model/BitmapDescriptor;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v3, 0x0

    :goto_1
    if-nez v3, :cond_0

    sget-object v3, Lmaps/z/bj;->a:Lmaps/z/ba;

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->getPosition()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->getAnchorU()F

    move-result v4

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->getAnchorV()F

    move-result v5

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->isDraggable()Z

    move-result v6

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->isVisible()Z

    move-result v7

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lmaps/z/by;->a(Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLng;Lmaps/z/br;FFZZ)Lmaps/l/ay;

    move-result-object v7

    new-instance v4, Lmaps/z/bv;

    iget-object v9, p0, Lmaps/z/by;->o:Lmaps/bf/b;

    move-object v5, v1

    move-object v6, v3

    move-object v8, p0

    invoke-direct/range {v4 .. v9}, Lmaps/z/bv;-><init>(Ljava/lang/String;Lmaps/z/br;Lmaps/l/ay;Lmaps/z/by;Lmaps/bf/b;)V

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->getSnippet()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lmaps/z/bv;->a(Lmaps/z/bv;Ljava/lang/String;)Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lmaps/z/bv;->b(Lmaps/z/bv;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lmaps/z/by;->e:Ljava/util/Map;

    invoke-virtual {v4}, Lmaps/z/bv;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lmaps/z/by;->c:Lmaps/y/k;

    invoke-static {v4}, Lmaps/z/bv;->a(Lmaps/z/bv;)Lmaps/l/ay;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/y/k;->a(Lmaps/l/ay;)V

    invoke-direct {p0}, Lmaps/z/by;->c()V

    return-object v4

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->getIcon()Lcom/google/android/gms/maps/model/BitmapDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/maps/model/BitmapDescriptor;->getRemoteObject()Lcom/google/android/gms/dynamic/IObjectWrapper;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/dynamic/ObjectWrapper;->unwrap(Lcom/google/android/gms/dynamic/IObjectWrapper;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/z/br;

    move-object v3, v0

    goto :goto_1
.end method

.method public b(Lmaps/l/ay;)V
    .locals 2

    iget-object v0, p0, Lmaps/z/by;->i:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    invoke-direct {p0, p1}, Lmaps/z/by;->c(Lmaps/y/bh;)Lmaps/z/bv;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lmaps/z/by;->k:Lcom/google/android/gms/maps/internal/IOnMarkerDragListener;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lmaps/z/by;->k:Lcom/google/android/gms/maps/internal/IOnMarkerDragListener;

    invoke-interface {v1, v0}, Lcom/google/android/gms/maps/internal/IOnMarkerDragListener;->onMarkerDrag(Lcom/google/android/gms/maps/model/internal/IMarkerDelegate;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public b(Lmaps/y/bh;)V
    .locals 0

    return-void
.end method

.method public c(Lmaps/l/ay;)V
    .locals 2

    iget-object v0, p0, Lmaps/z/by;->i:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    invoke-direct {p0, p1}, Lmaps/z/by;->c(Lmaps/y/bh;)Lmaps/z/bv;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lmaps/z/by;->k:Lcom/google/android/gms/maps/internal/IOnMarkerDragListener;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lmaps/z/by;->k:Lcom/google/android/gms/maps/internal/IOnMarkerDragListener;

    invoke-interface {v1, v0}, Lcom/google/android/gms/maps/internal/IOnMarkerDragListener;->onMarkerDragEnd(Lcom/google/android/gms/maps/model/internal/IMarkerDelegate;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method
