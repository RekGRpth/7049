.class final Lmaps/z/n;
.super Lcom/google/android/gms/maps/model/internal/ICircleDelegate$Stub;

# interfaces
.implements Lmaps/z/a;


# static fields
.field public static final a:Ljava/util/List;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Lmaps/z/bi;

.field private d:F

.field private e:I

.field private f:I

.field private g:Lcom/google/android/gms/maps/model/LatLng;

.field private h:D

.field private i:Lmaps/y/bk;

.field private j:F

.field private k:Z

.field private final l:Lmaps/bf/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lmaps/z/n;->a:Ljava/util/List;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Lcom/google/android/gms/maps/model/CircleOptions;Lmaps/z/bi;Lmaps/bf/b;)V
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/gms/maps/model/internal/ICircleDelegate$Stub;-><init>()V

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/CircleOptions;->getCenter()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    invoke-static {v0}, Lmaps/ap/q;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/CircleOptions;->getStrokeWidth()F

    move-result v0

    const/4 v3, 0x0

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "line width is negative"

    invoke-static {v0, v3}, Lmaps/ap/q;->a(ZLjava/lang/Object;)V

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/CircleOptions;->getRadius()D

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmpl-double v0, v3, v5

    if-ltz v0, :cond_1

    :goto_1
    const-string v0, "radius is negative"

    invoke-static {v1, v0}, Lmaps/ap/q;->a(ZLjava/lang/Object;)V

    iput-object p1, p0, Lmaps/z/n;->b:Ljava/lang/String;

    iput-object p3, p0, Lmaps/z/n;->c:Lmaps/z/bi;

    iput-object p4, p0, Lmaps/z/n;->l:Lmaps/bf/b;

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/CircleOptions;->getStrokeWidth()F

    move-result v0

    iput v0, p0, Lmaps/z/n;->d:F

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/CircleOptions;->getStrokeColor()I

    move-result v0

    iput v0, p0, Lmaps/z/n;->e:I

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/CircleOptions;->getFillColor()I

    move-result v0

    iput v0, p0, Lmaps/z/n;->f:I

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/CircleOptions;->getZIndex()F

    move-result v0

    iput v0, p0, Lmaps/z/n;->j:F

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/CircleOptions;->isVisible()Z

    move-result v0

    iput-boolean v0, p0, Lmaps/z/n;->k:Z

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/CircleOptions;->getRadius()D

    move-result-wide v0

    iput-wide v0, p0, Lmaps/z/n;->h:D

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/CircleOptions;->getCenter()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    iput-object v0, p0, Lmaps/z/n;->g:Lcom/google/android/gms/maps/model/LatLng;

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method static a(Lcom/google/android/gms/maps/model/LatLng;D)Lmaps/t/cg;
    .locals 22

    const/16 v2, 0x3e8

    new-array v5, v2, [I

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    invoke-static {v6, v7}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v6

    const-wide v8, 0x41584db040000000L

    div-double v8, p1, v8

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v12

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    const/4 v2, 0x0

    move v4, v2

    :goto_0
    const/16 v2, 0x1f4

    if-ge v4, v2, :cond_2

    const-wide v2, 0x401921fb54442d18L

    int-to-double v0, v4

    move-wide/from16 v16, v0

    mul-double v2, v2, v16

    const-wide v16, 0x407f300000000000L

    div-double v2, v2, v16

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v16

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    mul-double v18, v14, v10

    mul-double v20, v12, v8

    mul-double v16, v16, v20

    add-double v16, v16, v18

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->asin(D)D

    move-result-wide v18

    mul-double/2addr v2, v8

    mul-double/2addr v2, v12

    mul-double v16, v16, v14

    sub-double v16, v10, v16

    move-wide/from16 v0, v16

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v2

    add-double/2addr v2, v6

    :goto_1
    const-wide v16, -0x3ff6de04abbbd2e8L

    cmpg-double v16, v2, v16

    if-gez v16, :cond_0

    const-wide v16, 0x401921fb54442d18L

    add-double v2, v2, v16

    goto :goto_1

    :cond_0
    :goto_2
    const-wide v16, 0x400921fb54442d18L

    cmpl-double v16, v2, v16

    if-lez v16, :cond_1

    const-wide v16, 0x401921fb54442d18L

    sub-double v2, v2, v16

    goto :goto_2

    :cond_1
    const-wide/high16 v16, 0x3fe0000000000000L

    mul-double v16, v16, v18

    const-wide v18, 0x3fe921fb54442d18L

    add-double v16, v16, v18

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->tan(D)D

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->log(D)D

    move-result-wide v16

    rsub-int v0, v4, 0x1f4

    move/from16 v18, v0

    add-int/lit8 v18, v18, -0x1

    mul-int/lit8 v18, v18, 0x2

    const-wide v19, 0x41a45f306dc9c883L

    mul-double v2, v2, v19

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v2, v2

    aput v2, v5, v18

    rsub-int v2, v4, 0x1f4

    add-int/lit8 v2, v2, -0x1

    mul-int/lit8 v2, v2, 0x2

    add-int/lit8 v2, v2, 0x1

    const-wide v18, 0x41a45f306dc9c883L

    mul-double v16, v16, v18

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->round(D)J

    move-result-wide v16

    move-wide/from16 v0, v16

    long-to-int v3, v0

    aput v3, v5, v2

    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto/16 :goto_0

    :cond_2
    invoke-static {v5}, Lmaps/t/cg;->a([I)Lmaps/t/cg;

    move-result-object v2

    return-object v2
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public a(I)V
    .locals 0

    return-void
.end method

.method public declared-synchronized a(Lmaps/bq/d;Lmaps/cr/c;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/z/n;->k:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lmaps/z/n;->i:Lmaps/y/bk;

    invoke-virtual {v0, p1, p2}, Lmaps/y/bk;->a(Lmaps/bq/d;Lmaps/cr/c;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lmaps/cr/c;)V
    .locals 0

    return-void
.end method

.method public a(Lmaps/cr/c;Lmaps/af/f;)V
    .locals 0

    return-void
.end method

.method public declared-synchronized a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/z/n;->k:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lmaps/z/n;->i:Lmaps/y/bk;

    invoke-virtual {v0, p1, p2, p3}, Lmaps/y/bk;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Z)V
    .locals 0

    return-void
.end method

.method public b()V
    .locals 0

    return-void
.end method

.method c()V
    .locals 7

    iget-object v0, p0, Lmaps/z/n;->g:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v1, p0, Lmaps/z/n;->h:D

    invoke-static {v0, v1, v2}, Lmaps/z/n;->a(Lcom/google/android/gms/maps/model/LatLng;D)Lmaps/t/cg;

    move-result-object v1

    new-instance v0, Lmaps/y/bk;

    sget-object v2, Lmaps/z/n;->a:Ljava/util/List;

    iget v3, p0, Lmaps/z/n;->d:F

    float-to-int v3, v3

    iget v4, p0, Lmaps/z/n;->e:I

    invoke-static {v4}, Lmaps/au/c;->a(I)I

    move-result v4

    iget v5, p0, Lmaps/z/n;->f:I

    invoke-static {v5}, Lmaps/au/c;->a(I)I

    move-result v5

    const/4 v6, 0x1

    invoke-direct/range {v0 .. v6}, Lmaps/y/bk;-><init>(Lmaps/t/cg;Ljava/util/List;IIIZ)V

    iput-object v0, p0, Lmaps/z/n;->i:Lmaps/y/bk;

    return-void
.end method

.method public equalsRemote(Lcom/google/android/gms/maps/model/internal/ICircleDelegate;)Z
    .locals 1

    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getCenter()Lcom/google/android/gms/maps/model/LatLng;
    .locals 1

    iget-object v0, p0, Lmaps/z/n;->c:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/z/n;->g:Lcom/google/android/gms/maps/model/LatLng;

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getFillColor()I
    .locals 1

    iget-object v0, p0, Lmaps/z/n;->c:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/z/n;->f:I

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/z/n;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getRadius()D
    .locals 2

    iget-object v0, p0, Lmaps/z/n;->c:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lmaps/z/n;->h:D

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getStrokeColor()I
    .locals 1

    iget-object v0, p0, Lmaps/z/n;->c:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/z/n;->e:I

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getStrokeWidth()F
    .locals 1

    iget-object v0, p0, Lmaps/z/n;->c:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/z/n;->d:F

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getZIndex()F
    .locals 1

    iget-object v0, p0, Lmaps/z/n;->c:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    iget v0, p0, Lmaps/z/n;->j:F

    return v0
.end method

.method public hashCodeRemote()I
    .locals 1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public isVisible()Z
    .locals 1

    iget-object v0, p0, Lmaps/z/n;->c:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/z/n;->k:Z

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public remove()V
    .locals 2

    iget-object v0, p0, Lmaps/z/n;->c:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    iget-object v0, p0, Lmaps/z/n;->l:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->F:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iget-object v1, p0, Lmaps/z/n;->c:Lmaps/z/bi;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/z/n;->c:Lmaps/z/bi;

    invoke-virtual {v0, p0}, Lmaps/z/bi;->a(Lmaps/z/a;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/z/n;->c:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->h()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public setCenter(Lcom/google/android/gms/maps/model/LatLng;)V
    .locals 2

    iget-object v0, p0, Lmaps/z/n;->c:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    iget-object v0, p0, Lmaps/z/n;->l:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->G:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    invoke-static {p1}, Lmaps/ap/q;->a(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lmaps/z/n;->g:Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {p0}, Lmaps/z/n;->c()V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/z/n;->c:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->h()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public setFillColor(I)V
    .locals 2

    iget-object v0, p0, Lmaps/z/n;->c:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    iget-object v0, p0, Lmaps/z/n;->l:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->K:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lmaps/z/n;->f:I

    iget-object v0, p0, Lmaps/z/n;->i:Lmaps/y/bk;

    invoke-static {p1}, Lmaps/au/c;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lmaps/y/bk;->c(I)V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/z/n;->c:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->h()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public setRadius(D)V
    .locals 2

    iget-object v0, p0, Lmaps/z/n;->c:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    iget-object v0, p0, Lmaps/z/n;->l:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->H:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    const-wide/16 v0, 0x0

    cmpl-double v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "radius is negative"

    invoke-static {v0, v1}, Lmaps/ap/q;->a(ZLjava/lang/Object;)V

    monitor-enter p0

    :try_start_0
    iput-wide p1, p0, Lmaps/z/n;->h:D

    invoke-virtual {p0}, Lmaps/z/n;->c()V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/z/n;->c:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->h()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public setStrokeColor(I)V
    .locals 2

    iget-object v0, p0, Lmaps/z/n;->c:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    iget-object v0, p0, Lmaps/z/n;->l:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->J:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lmaps/z/n;->e:I

    iget-object v0, p0, Lmaps/z/n;->i:Lmaps/y/bk;

    invoke-static {p1}, Lmaps/au/c;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lmaps/y/bk;->b(I)V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/z/n;->c:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->h()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public setStrokeWidth(F)V
    .locals 2

    iget-object v0, p0, Lmaps/z/n;->c:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    iget-object v0, p0, Lmaps/z/n;->l:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->I:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "line width is negative"

    invoke-static {v0, v1}, Lmaps/ap/q;->a(ZLjava/lang/Object;)V

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lmaps/z/n;->d:F

    iget-object v0, p0, Lmaps/z/n;->i:Lmaps/y/bk;

    float-to-int v1, p1

    invoke-virtual {v0, v1}, Lmaps/y/bk;->d(I)V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/z/n;->c:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->h()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public setVisible(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/z/n;->c:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    iget-object v0, p0, Lmaps/z/n;->l:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->M:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lmaps/z/n;->k:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/z/n;->c:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->h()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public setZIndex(F)V
    .locals 2

    iget-object v0, p0, Lmaps/z/n;->c:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    iget-object v0, p0, Lmaps/z/n;->l:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->L:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iget-object v1, p0, Lmaps/z/n;->c:Lmaps/z/bi;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/z/n;->c:Lmaps/z/bi;

    invoke-virtual {v0, p0}, Lmaps/z/bi;->b(Lmaps/z/a;)V

    iput p1, p0, Lmaps/z/n;->j:F

    iget-object v0, p0, Lmaps/z/n;->c:Lmaps/z/bi;

    invoke-virtual {v0, p0}, Lmaps/z/bi;->c(Lmaps/z/a;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/z/n;->c:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->h()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lmaps/z/n;->c:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    monitor-enter p0

    :try_start_0
    invoke-static {p0}, Lmaps/ap/e;->a(Ljava/lang/Object;)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "center"

    iget-object v2, p0, Lmaps/z/n;->g:Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "radius"

    iget-wide v2, p0, Lmaps/z/n;->h:D

    invoke-virtual {v0, v1, v2, v3}, Lmaps/ap/n;->a(Ljava/lang/String;D)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "strokeWidth"

    iget v2, p0, Lmaps/z/n;->d:F

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;F)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "strokeColor"

    iget v2, p0, Lmaps/z/n;->e:I

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;I)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "fillColor"

    iget v2, p0, Lmaps/z/n;->f:I

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;I)Lmaps/ap/n;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ap/n;->toString()Ljava/lang/String;

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
