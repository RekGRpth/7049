.class public Lmaps/z/u;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/ao/a;


# instance fields
.field private final a:Lmaps/i/m;

.field private final b:Lmaps/t/e;

.field private final c:Lmaps/bf/b;

.field private d:Lmaps/f/ef;


# direct methods
.method public constructor <init>(Lmaps/i/m;Lmaps/t/e;Lmaps/bf/b;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p2}, Lmaps/t/e;->a()Lmaps/t/bg;

    move-result-object v0

    const-string v1, "Building must have an id"

    invoke-static {v0, v1}, Lmaps/ap/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lmaps/z/u;->a:Lmaps/i/m;

    iput-object p2, p0, Lmaps/z/u;->b:Lmaps/t/e;

    iput-object p3, p0, Lmaps/z/u;->c:Lmaps/bf/b;

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/z/u;->b:Lmaps/t/e;

    invoke-virtual {v0}, Lmaps/t/e;->a()Lmaps/t/bg;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/bg;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()Lmaps/f/ef;
    .locals 6

    iget-object v0, p0, Lmaps/z/u;->c:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->bj:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iget-object v0, p0, Lmaps/z/u;->d:Lmaps/f/ef;

    if-nez v0, :cond_1

    invoke-static {}, Lmaps/f/ef;->g()Lmaps/f/bk;

    move-result-object v1

    iget-object v0, p0, Lmaps/z/u;->b:Lmaps/t/e;

    invoke-virtual {v0}, Lmaps/t/e;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/bi;

    new-instance v3, Lmaps/z/at;

    iget-object v4, p0, Lmaps/z/u;->a:Lmaps/i/m;

    iget-object v5, p0, Lmaps/z/u;->c:Lmaps/bf/b;

    invoke-direct {v3, v4, v0, v5}, Lmaps/z/at;-><init>(Lmaps/i/m;Lmaps/t/bi;Lmaps/bf/b;)V

    invoke-virtual {v1, v3}, Lmaps/f/bk;->a(Ljava/lang/Object;)Lmaps/f/bk;

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Lmaps/f/bk;->a()Lmaps/f/ef;

    move-result-object v0

    iput-object v0, p0, Lmaps/z/u;->d:Lmaps/f/ef;

    :cond_1
    iget-object v0, p0, Lmaps/z/u;->d:Lmaps/f/ef;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    instance-of v0, p1, Lmaps/z/u;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    check-cast p1, Lmaps/z/u;

    iget-object v0, p0, Lmaps/z/u;->b:Lmaps/t/e;

    invoke-virtual {v0}, Lmaps/t/e;->a()Lmaps/t/bg;

    move-result-object v0

    iget-object v1, p1, Lmaps/z/u;->b:Lmaps/t/e;

    invoke-virtual {v1}, Lmaps/t/e;->a()Lmaps/t/bg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/t/bg;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lmaps/z/u;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lmaps/ap/e;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Lmaps/ap/e;->a(Ljava/lang/Object;)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {p0}, Lmaps/z/u;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "levels"

    invoke-virtual {p0}, Lmaps/z/u;->b()Lmaps/f/ef;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/ap/n;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ap/n;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
