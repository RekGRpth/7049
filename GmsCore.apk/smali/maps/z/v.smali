.class public Lmaps/z/v;
.super Lcom/google/android/gms/maps/internal/ICameraUpdateFactoryDelegate$Stub;


# static fields
.field private static final a:D

.field private static final b:Lmaps/z/b;

.field private static final c:Lmaps/z/b;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const-wide/high16 v0, 0x3ff0000000000000L

    const-wide/high16 v2, 0x4000000000000000L

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    sput-wide v0, Lmaps/z/v;->a:D

    new-instance v0, Lmaps/z/d;

    invoke-direct {v0}, Lmaps/z/d;-><init>()V

    sput-object v0, Lmaps/z/v;->b:Lmaps/z/b;

    new-instance v0, Lmaps/z/f;

    invoke-direct {v0}, Lmaps/z/f;-><init>()V

    sput-object v0, Lmaps/z/v;->c:Lmaps/z/b;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/maps/internal/ICameraUpdateFactoryDelegate$Stub;-><init>()V

    return-void
.end method

.method static synthetic a(Lmaps/bq/a;Lmaps/i/c;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lmaps/z/v;->b(Lmaps/bq/a;Lmaps/i/c;I)V

    return-void
.end method

.method static synthetic a(Lmaps/i/h;Lmaps/i/c;ILcom/google/android/gms/maps/model/LatLngBounds;III)V
    .locals 0

    invoke-static/range {p0 .. p6}, Lmaps/z/v;->b(Lmaps/i/h;Lmaps/i/c;ILcom/google/android/gms/maps/model/LatLngBounds;III)V

    return-void
.end method

.method private static b(Lmaps/bq/a;Lmaps/i/c;I)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, -0x1

    if-nez p2, :cond_0

    invoke-interface {p1, p0, v1, v1}, Lmaps/i/c;->a(Lmaps/bq/b;II)V

    :goto_0
    return-void

    :cond_0
    if-ne p2, v0, :cond_1

    invoke-interface {p1, p0, v0, v0}, Lmaps/i/c;->a(Lmaps/bq/b;II)V

    goto :goto_0

    :cond_1
    invoke-interface {p1, p0, p2, p2}, Lmaps/i/c;->a(Lmaps/bq/b;II)V

    goto :goto_0
.end method

.method private static b(Lmaps/i/h;Lmaps/i/c;ILcom/google/android/gms/maps/model/LatLngBounds;III)V
    .locals 12

    move/from16 v0, p4

    int-to-double v1, v0

    move/from16 v0, p5

    int-to-double v3, v0

    mul-int/lit8 v5, p6, 0x2

    int-to-double v5, v5

    sub-double v5, v1, v5

    mul-int/lit8 v1, p6, 0x2

    int-to-double v1, v1

    sub-double v2, v3, v1

    const-wide/16 v7, 0x0

    cmpl-double v1, v5, v7

    if-lez v1, :cond_0

    const-wide/16 v7, 0x0

    cmpl-double v1, v2, v7

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v4, "View size is too small after padding"

    invoke-static {v1, v4}, Lmaps/ap/q;->b(ZLjava/lang/Object;)V

    invoke-interface {p0}, Lmaps/i/h;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    float-to-double v7, v1

    const-wide/high16 v9, 0x4070000000000000L

    mul-double/2addr v7, v9

    iget-object v1, p3, Lcom/google/android/gms/maps/model/LatLngBounds;->northeast:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v1}, Lmaps/au/c;->b(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/t/bx;

    move-result-object v4

    iget-object v1, p3, Lcom/google/android/gms/maps/model/LatLngBounds;->southwest:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v1}, Lmaps/au/c;->b(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/t/bx;

    move-result-object v9

    invoke-virtual {v4}, Lmaps/t/bx;->f()I

    move-result v1

    invoke-virtual {v9}, Lmaps/t/bx;->f()I

    move-result v10

    if-ge v1, v10, :cond_1

    const/high16 v1, 0x40000000

    invoke-virtual {v9}, Lmaps/t/bx;->f()I

    move-result v10

    sub-int/2addr v1, v10

    invoke-virtual {v4}, Lmaps/t/bx;->f()I

    move-result v10

    add-int/2addr v1, v10

    :goto_1
    invoke-virtual {v4}, Lmaps/t/bx;->g()I

    move-result v4

    invoke-virtual {v9}, Lmaps/t/bx;->g()I

    move-result v10

    sub-int/2addr v4, v10

    int-to-double v10, v1

    mul-double/2addr v10, v7

    div-double v5, v10, v5

    int-to-double v10, v4

    mul-double/2addr v7, v10

    div-double v2, v7, v2

    invoke-static {v5, v6, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    const-wide/high16 v5, 0x403e000000000000L

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    sget-wide v7, Lmaps/z/v;->a:D

    mul-double/2addr v2, v7

    sub-double/2addr v5, v2

    new-instance v2, Lmaps/t/bx;

    invoke-virtual {v9}, Lmaps/t/bx;->f()I

    move-result v3

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v3

    const/high16 v3, 0x40000000

    rem-int/2addr v1, v3

    invoke-virtual {v9}, Lmaps/t/bx;->g()I

    move-result v3

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    invoke-direct {v2, v1, v3}, Lmaps/t/bx;-><init>(II)V

    new-instance v1, Lmaps/bq/a;

    double-to-float v3, v5

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, Lmaps/bq/a;-><init>(Lmaps/t/bx;FFFF)V

    invoke-static {v1, p1, p2}, Lmaps/z/v;->b(Lmaps/bq/a;Lmaps/i/c;I)V

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {v4}, Lmaps/t/bx;->f()I

    move-result v1

    invoke-virtual {v9}, Lmaps/t/bx;->f()I

    move-result v10

    sub-int/2addr v1, v10

    goto :goto_1
.end method


# virtual methods
.method public newCameraPosition(Lcom/google/android/gms/maps/model/CameraPosition;)Lcom/google/android/gms/dynamic/IObjectWrapper;
    .locals 1

    new-instance v0, Lmaps/z/l;

    invoke-direct {v0, p0, p1}, Lmaps/z/l;-><init>(Lmaps/z/v;Lcom/google/android/gms/maps/model/CameraPosition;)V

    invoke-static {v0}, Lcom/google/android/gms/dynamic/ObjectWrapper;->wrap(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;

    move-result-object v0

    return-object v0
.end method

.method public newLatLng(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/dynamic/IObjectWrapper;
    .locals 1

    new-instance v0, Lmaps/z/g;

    invoke-direct {v0, p0, p1}, Lmaps/z/g;-><init>(Lmaps/z/v;Lcom/google/android/gms/maps/model/LatLng;)V

    invoke-static {v0}, Lcom/google/android/gms/dynamic/ObjectWrapper;->wrap(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;

    move-result-object v0

    return-object v0
.end method

.method public newLatLngBounds(Lcom/google/android/gms/maps/model/LatLngBounds;I)Lcom/google/android/gms/dynamic/IObjectWrapper;
    .locals 1

    new-instance v0, Lmaps/z/s;

    invoke-direct {v0, p0, p1, p2}, Lmaps/z/s;-><init>(Lmaps/z/v;Lcom/google/android/gms/maps/model/LatLngBounds;I)V

    invoke-static {v0}, Lcom/google/android/gms/dynamic/ObjectWrapper;->wrap(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;

    move-result-object v0

    return-object v0
.end method

.method public newLatLngBoundsWithSize(Lcom/google/android/gms/maps/model/LatLngBounds;III)Lcom/google/android/gms/dynamic/IObjectWrapper;
    .locals 6

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Map size should not be 0"

    invoke-static {v0, v1}, Lmaps/ap/q;->b(ZLjava/lang/Object;)V

    new-instance v0, Lmaps/z/t;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lmaps/z/t;-><init>(Lmaps/z/v;Lcom/google/android/gms/maps/model/LatLngBounds;III)V

    invoke-static {v0}, Lcom/google/android/gms/dynamic/ObjectWrapper;->wrap(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public newLatLngZoom(Lcom/google/android/gms/maps/model/LatLng;F)Lcom/google/android/gms/dynamic/IObjectWrapper;
    .locals 1

    new-instance v0, Lmaps/z/h;

    invoke-direct {v0, p0, p1, p2}, Lmaps/z/h;-><init>(Lmaps/z/v;Lcom/google/android/gms/maps/model/LatLng;F)V

    invoke-static {v0}, Lcom/google/android/gms/dynamic/ObjectWrapper;->wrap(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;

    move-result-object v0

    return-object v0
.end method

.method public scrollBy(FF)Lcom/google/android/gms/dynamic/IObjectWrapper;
    .locals 1

    new-instance v0, Lmaps/z/e;

    invoke-direct {v0, p0, p1, p2}, Lmaps/z/e;-><init>(Lmaps/z/v;FF)V

    invoke-static {v0}, Lcom/google/android/gms/dynamic/ObjectWrapper;->wrap(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;

    move-result-object v0

    return-object v0
.end method

.method public zoomBy(F)Lcom/google/android/gms/dynamic/IObjectWrapper;
    .locals 1

    new-instance v0, Lmaps/z/j;

    invoke-direct {v0, p0, p1}, Lmaps/z/j;-><init>(Lmaps/z/v;F)V

    invoke-static {v0}, Lcom/google/android/gms/dynamic/ObjectWrapper;->wrap(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;

    move-result-object v0

    return-object v0
.end method

.method public zoomByWithFocus(FII)Lcom/google/android/gms/dynamic/IObjectWrapper;
    .locals 1

    new-instance v0, Lmaps/z/k;

    invoke-direct {v0, p0, p1, p2, p3}, Lmaps/z/k;-><init>(Lmaps/z/v;FII)V

    invoke-static {v0}, Lcom/google/android/gms/dynamic/ObjectWrapper;->wrap(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;

    move-result-object v0

    return-object v0
.end method

.method public zoomIn()Lcom/google/android/gms/dynamic/IObjectWrapper;
    .locals 1

    sget-object v0, Lmaps/z/v;->b:Lmaps/z/b;

    invoke-static {v0}, Lcom/google/android/gms/dynamic/ObjectWrapper;->wrap(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;

    move-result-object v0

    return-object v0
.end method

.method public zoomOut()Lcom/google/android/gms/dynamic/IObjectWrapper;
    .locals 1

    sget-object v0, Lmaps/z/v;->c:Lmaps/z/b;

    invoke-static {v0}, Lcom/google/android/gms/dynamic/ObjectWrapper;->wrap(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;

    move-result-object v0

    return-object v0
.end method

.method public zoomTo(F)Lcom/google/android/gms/dynamic/IObjectWrapper;
    .locals 1

    new-instance v0, Lmaps/z/i;

    invoke-direct {v0, p0, p1}, Lmaps/z/i;-><init>(Lmaps/z/v;F)V

    invoke-static {v0}, Lcom/google/android/gms/dynamic/ObjectWrapper;->wrap(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;

    move-result-object v0

    return-object v0
.end method
