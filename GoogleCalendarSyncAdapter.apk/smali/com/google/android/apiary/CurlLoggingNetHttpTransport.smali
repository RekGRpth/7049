.class public Lcom/google/android/apiary/CurlLoggingNetHttpTransport;
.super Lcom/google/api/client/http/HttpTransport;
.source "CurlLoggingNetHttpTransport.java"


# instance fields
.field private mTag:Ljava/lang/String;

.field private final mTransport:Lcom/google/api/client/http/javanet/NetHttpTransport;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/api/client/http/HttpTransport;-><init>()V

    new-instance v0, Lcom/google/api/client/http/javanet/NetHttpTransport;

    invoke-direct {v0}, Lcom/google/api/client/http/javanet/NetHttpTransport;-><init>()V

    iput-object v0, p0, Lcom/google/android/apiary/CurlLoggingNetHttpTransport;->mTransport:Lcom/google/api/client/http/javanet/NetHttpTransport;

    return-void
.end method


# virtual methods
.method public buildDeleteRequest(Ljava/lang/String;)Lcom/google/android/apiary/CurlLoggingHttpRequest;
    .locals 4
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/apiary/CurlLoggingHttpRequest;

    iget-object v1, p0, Lcom/google/android/apiary/CurlLoggingNetHttpTransport;->mTransport:Lcom/google/api/client/http/javanet/NetHttpTransport;

    invoke-virtual {v1, p1}, Lcom/google/api/client/http/javanet/NetHttpTransport;->buildDeleteRequest(Ljava/lang/String;)Lcom/google/api/client/http/javanet/NetHttpRequest;

    move-result-object v1

    const-string v2, "DELETE"

    iget-object v3, p0, Lcom/google/android/apiary/CurlLoggingNetHttpTransport;->mTag:Ljava/lang/String;

    invoke-direct {v0, v1, v2, p1, v3}, Lcom/google/android/apiary/CurlLoggingHttpRequest;-><init>(Lcom/google/api/client/http/LowLevelHttpRequest;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic buildDeleteRequest(Ljava/lang/String;)Lcom/google/api/client/http/LowLevelHttpRequest;
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/apiary/CurlLoggingNetHttpTransport;->buildDeleteRequest(Ljava/lang/String;)Lcom/google/android/apiary/CurlLoggingHttpRequest;

    move-result-object v0

    return-object v0
.end method

.method public buildGetRequest(Ljava/lang/String;)Lcom/google/android/apiary/CurlLoggingHttpRequest;
    .locals 4
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/apiary/CurlLoggingHttpRequest;

    iget-object v1, p0, Lcom/google/android/apiary/CurlLoggingNetHttpTransport;->mTransport:Lcom/google/api/client/http/javanet/NetHttpTransport;

    invoke-virtual {v1, p1}, Lcom/google/api/client/http/javanet/NetHttpTransport;->buildGetRequest(Ljava/lang/String;)Lcom/google/api/client/http/javanet/NetHttpRequest;

    move-result-object v1

    const-string v2, "GET"

    iget-object v3, p0, Lcom/google/android/apiary/CurlLoggingNetHttpTransport;->mTag:Ljava/lang/String;

    invoke-direct {v0, v1, v2, p1, v3}, Lcom/google/android/apiary/CurlLoggingHttpRequest;-><init>(Lcom/google/api/client/http/LowLevelHttpRequest;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic buildGetRequest(Ljava/lang/String;)Lcom/google/api/client/http/LowLevelHttpRequest;
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/apiary/CurlLoggingNetHttpTransport;->buildGetRequest(Ljava/lang/String;)Lcom/google/android/apiary/CurlLoggingHttpRequest;

    move-result-object v0

    return-object v0
.end method

.method public buildHeadRequest(Ljava/lang/String;)Lcom/google/android/apiary/CurlLoggingHttpRequest;
    .locals 4
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/apiary/CurlLoggingHttpRequest;

    iget-object v1, p0, Lcom/google/android/apiary/CurlLoggingNetHttpTransport;->mTransport:Lcom/google/api/client/http/javanet/NetHttpTransport;

    invoke-virtual {v1, p1}, Lcom/google/api/client/http/javanet/NetHttpTransport;->buildHeadRequest(Ljava/lang/String;)Lcom/google/api/client/http/javanet/NetHttpRequest;

    move-result-object v1

    const-string v2, "HEAD"

    iget-object v3, p0, Lcom/google/android/apiary/CurlLoggingNetHttpTransport;->mTag:Ljava/lang/String;

    invoke-direct {v0, v1, v2, p1, v3}, Lcom/google/android/apiary/CurlLoggingHttpRequest;-><init>(Lcom/google/api/client/http/LowLevelHttpRequest;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic buildHeadRequest(Ljava/lang/String;)Lcom/google/api/client/http/LowLevelHttpRequest;
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/apiary/CurlLoggingNetHttpTransport;->buildHeadRequest(Ljava/lang/String;)Lcom/google/android/apiary/CurlLoggingHttpRequest;

    move-result-object v0

    return-object v0
.end method

.method public buildPostRequest(Ljava/lang/String;)Lcom/google/android/apiary/CurlLoggingHttpRequest;
    .locals 4
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/apiary/CurlLoggingHttpRequest;

    iget-object v1, p0, Lcom/google/android/apiary/CurlLoggingNetHttpTransport;->mTransport:Lcom/google/api/client/http/javanet/NetHttpTransport;

    invoke-virtual {v1, p1}, Lcom/google/api/client/http/javanet/NetHttpTransport;->buildPostRequest(Ljava/lang/String;)Lcom/google/api/client/http/javanet/NetHttpRequest;

    move-result-object v1

    const-string v2, "POST"

    iget-object v3, p0, Lcom/google/android/apiary/CurlLoggingNetHttpTransport;->mTag:Ljava/lang/String;

    invoke-direct {v0, v1, v2, p1, v3}, Lcom/google/android/apiary/CurlLoggingHttpRequest;-><init>(Lcom/google/api/client/http/LowLevelHttpRequest;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic buildPostRequest(Ljava/lang/String;)Lcom/google/api/client/http/LowLevelHttpRequest;
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/apiary/CurlLoggingNetHttpTransport;->buildPostRequest(Ljava/lang/String;)Lcom/google/android/apiary/CurlLoggingHttpRequest;

    move-result-object v0

    return-object v0
.end method

.method public buildPutRequest(Ljava/lang/String;)Lcom/google/android/apiary/CurlLoggingHttpRequest;
    .locals 4
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/apiary/CurlLoggingHttpRequest;

    iget-object v1, p0, Lcom/google/android/apiary/CurlLoggingNetHttpTransport;->mTransport:Lcom/google/api/client/http/javanet/NetHttpTransport;

    invoke-virtual {v1, p1}, Lcom/google/api/client/http/javanet/NetHttpTransport;->buildPutRequest(Ljava/lang/String;)Lcom/google/api/client/http/javanet/NetHttpRequest;

    move-result-object v1

    const-string v2, "PUT"

    iget-object v3, p0, Lcom/google/android/apiary/CurlLoggingNetHttpTransport;->mTag:Ljava/lang/String;

    invoke-direct {v0, v1, v2, p1, v3}, Lcom/google/android/apiary/CurlLoggingHttpRequest;-><init>(Lcom/google/api/client/http/LowLevelHttpRequest;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic buildPutRequest(Ljava/lang/String;)Lcom/google/api/client/http/LowLevelHttpRequest;
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/apiary/CurlLoggingNetHttpTransport;->buildPutRequest(Ljava/lang/String;)Lcom/google/android/apiary/CurlLoggingHttpRequest;

    move-result-object v0

    return-object v0
.end method

.method public setTag(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apiary/CurlLoggingNetHttpTransport;->mTag:Ljava/lang/String;

    return-void
.end method

.method public supportsHead()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
