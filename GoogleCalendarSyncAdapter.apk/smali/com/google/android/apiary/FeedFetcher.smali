.class public Lcom/google/android/apiary/FeedFetcher;
.super Ljava/lang/Object;
.source "FeedFetcher.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field private volatile mAuthenticationFailed:Z

.field private final mEnvelopeParsedLatch:Ljava/util/concurrent/CountDownLatch;

.field private volatile mForcedClosed:Z

.field private volatile mIoException:Z

.field private final mItemClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final mItemEndMarker:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final mItemQueue:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<TT;>;"
        }
    .end annotation
.end field

.field private mItemsName:Ljava/lang/String;

.field private final mJsonFactory:Lcom/google/api/client/json/JsonFactory;

.field protected final mLogTag:Ljava/lang/String;

.field private volatile mPartialSyncUnavailable:Z

.field protected final mRequest:Lcom/google/api/client/http/json/JsonHttpRequest;

.field private volatile mResourceUnavailable:Z

.field private volatile mRetryAfter:J

.field private volatile mThread:Ljava/lang/Thread;


# direct methods
.method public constructor <init>(Lcom/google/api/client/json/JsonFactory;Lcom/google/api/client/http/json/JsonHttpRequest;Ljava/lang/String;Ljava/lang/Class;Ljava/util/concurrent/BlockingQueue;Ljava/lang/Object;Ljava/lang/String;)V
    .locals 3
    .param p1    # Lcom/google/api/client/json/JsonFactory;
    .param p2    # Lcom/google/api/client/http/json/JsonHttpRequest;
    .param p3    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/api/client/json/JsonFactory;",
            "Lcom/google/api/client/http/json/JsonHttpRequest;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/util/concurrent/BlockingQueue",
            "<TT;>;TT;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apiary/FeedFetcher;->mEnvelopeParsedLatch:Ljava/util/concurrent/CountDownLatch;

    iput-boolean v2, p0, Lcom/google/android/apiary/FeedFetcher;->mAuthenticationFailed:Z

    iput-boolean v2, p0, Lcom/google/android/apiary/FeedFetcher;->mPartialSyncUnavailable:Z

    iput-boolean v2, p0, Lcom/google/android/apiary/FeedFetcher;->mResourceUnavailable:Z

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apiary/FeedFetcher;->mRetryAfter:J

    iput-boolean v2, p0, Lcom/google/android/apiary/FeedFetcher;->mForcedClosed:Z

    iput-object p1, p0, Lcom/google/android/apiary/FeedFetcher;->mJsonFactory:Lcom/google/api/client/json/JsonFactory;

    iput-object p2, p0, Lcom/google/android/apiary/FeedFetcher;->mRequest:Lcom/google/api/client/http/json/JsonHttpRequest;

    iput-object p3, p0, Lcom/google/android/apiary/FeedFetcher;->mItemsName:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apiary/FeedFetcher;->mItemClass:Ljava/lang/Class;

    iput-object p5, p0, Lcom/google/android/apiary/FeedFetcher;->mItemQueue:Ljava/util/concurrent/BlockingQueue;

    iput-object p6, p0, Lcom/google/android/apiary/FeedFetcher;->mItemEndMarker:Ljava/lang/Object;

    iput-object p7, p0, Lcom/google/android/apiary/FeedFetcher;->mLogTag:Ljava/lang/String;

    return-void
.end method

.method private closeResponse(Lcom/google/api/client/http/HttpResponse;)V
    .locals 0
    .param p1    # Lcom/google/api/client/http/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/api/client/http/HttpResponse;->ignore()V

    return-void
.end method

.method private fetchFeed()V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    const/4 v2, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apiary/FeedFetcher;->mRequest:Lcom/google/api/client/http/json/JsonHttpRequest;

    invoke-virtual {v0}, Lcom/google/api/client/http/json/JsonHttpRequest;->executeUnparsed()Lcom/google/api/client/http/HttpResponse;
    :try_end_0
    .catch Lcom/google/api/client/googleapis/json/GoogleJsonResponseException; {:try_start_0 .. :try_end_0} :catch_a
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_9
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    :goto_0
    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/apiary/FeedFetcher;->mForcedClosed:Z

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/google/android/apiary/FeedFetcher;->mJsonFactory:Lcom/google/api/client/json/JsonFactory;

    invoke-static {v0, v1}, Lcom/google/api/client/http/json/JsonHttpParser;->parserForResponse(Lcom/google/api/client/json/JsonFactory;Lcom/google/api/client/http/HttpResponse;)Lcom/google/api/client/json/JsonParser;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    invoke-virtual {v4}, Lcom/google/api/client/json/JsonParser;->getCurrentToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v0

    move-object v3, v0

    move-object v0, v2

    :goto_1
    sget-object v5, Lcom/google/api/client/json/JsonToken;->FIELD_NAME:Lcom/google/api/client/json/JsonToken;

    if-ne v3, v5, :cond_8

    iget-boolean v3, p0, Lcom/google/android/apiary/FeedFetcher;->mForcedClosed:Z
    :try_end_1
    .catch Lcom/google/api/client/googleapis/json/GoogleJsonResponseException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v3, :cond_2

    iget-object v0, p0, Lcom/google/android/apiary/FeedFetcher;->mEnvelopeParsedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    iget-boolean v0, p0, Lcom/google/android/apiary/FeedFetcher;->mForcedClosed:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apiary/FeedFetcher;->mItemQueue:Ljava/util/concurrent/BlockingQueue;

    iget-object v2, p0, Lcom/google/android/apiary/FeedFetcher;->mItemEndMarker:Ljava/lang/Object;

    invoke-interface {v0, v2}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V

    :cond_0
    if-eqz v1, :cond_1

    :try_start_2
    invoke-direct {p0, v1}, Lcom/google/android/apiary/FeedFetcher;->closeResponse(Lcom/google/api/client/http/HttpResponse;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_1
    :goto_2
    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apiary/FeedFetcher;->mLogTag:Ljava/lang/String;

    const-string v2, "IOException, while closing connection"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    :cond_2
    :try_start_3
    invoke-virtual {v4}, Lcom/google/api/client/json/JsonParser;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    const-string v5, "nextPageToken"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    const-class v0, Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v4, v0, v3}, Lcom/google/api/client/json/JsonParser;->parse(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :cond_3
    :goto_3
    invoke-virtual {v4}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v3

    goto :goto_1

    :cond_4
    iget-object v5, p0, Lcom/google/android/apiary/FeedFetcher;->mItemsName:Ljava/lang/String;

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    iget-object v3, p0, Lcom/google/android/apiary/FeedFetcher;->mEnvelopeParsedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    :goto_4
    invoke-virtual {v4}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    invoke-virtual {v4}, Lcom/google/api/client/json/JsonParser;->getCurrentToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v3

    sget-object v5, Lcom/google/api/client/json/JsonToken;->END_ARRAY:Lcom/google/api/client/json/JsonToken;

    if-eq v3, v5, :cond_3

    iget-object v3, p0, Lcom/google/android/apiary/FeedFetcher;->mItemQueue:Ljava/util/concurrent/BlockingQueue;

    iget-object v5, p0, Lcom/google/android/apiary/FeedFetcher;->mItemClass:Ljava/lang/Class;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Lcom/google/api/client/json/JsonParser;->parse(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_3
    .catch Lcom/google/api/client/googleapis/json/GoogleJsonResponseException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_4

    :catch_1
    move-exception v0

    :goto_5
    :try_start_4
    iget-object v3, p0, Lcom/google/android/apiary/FeedFetcher;->mLogTag:Ljava/lang/String;

    const-string v4, "GoogleJsonResponseException, ignoring rest of feed"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-virtual {v0}, Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;->getStatusCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apiary/FeedFetcher;->mIoException:Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :goto_6
    iget-object v0, p0, Lcom/google/android/apiary/FeedFetcher;->mEnvelopeParsedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    iget-boolean v0, p0, Lcom/google/android/apiary/FeedFetcher;->mForcedClosed:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apiary/FeedFetcher;->mItemQueue:Ljava/util/concurrent/BlockingQueue;

    iget-object v2, p0, Lcom/google/android/apiary/FeedFetcher;->mItemEndMarker:Ljava/lang/Object;

    invoke-interface {v0, v2}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V

    :cond_5
    if-eqz v1, :cond_1

    :try_start_5
    invoke-direct {p0, v1}, Lcom/google/android/apiary/FeedFetcher;->closeResponse(Lcom/google/api/client/http/HttpResponse;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_2

    :catch_2
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apiary/FeedFetcher;->mLogTag:Ljava/lang/String;

    const-string v2, "IOException, while closing connection"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    :cond_6
    :try_start_6
    invoke-virtual {p0, v4, v3}, Lcom/google/android/apiary/FeedFetcher;->parseField(Lcom/google/api/client/json/JsonParser;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {v4}, Lcom/google/api/client/json/JsonParser;->skipChildren()Lcom/google/api/client/json/JsonParser;
    :try_end_6
    .catch Lcom/google/api/client/googleapis/json/GoogleJsonResponseException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_3

    :catch_3
    move-exception v0

    :goto_7
    :try_start_7
    iget-object v2, p0, Lcom/google/android/apiary/FeedFetcher;->mLogTag:Ljava/lang/String;

    const-string v3, "IOException, ignoring rest of feed"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apiary/FeedFetcher;->mIoException:Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    iget-object v0, p0, Lcom/google/android/apiary/FeedFetcher;->mEnvelopeParsedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    iget-boolean v0, p0, Lcom/google/android/apiary/FeedFetcher;->mForcedClosed:Z

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apiary/FeedFetcher;->mItemQueue:Ljava/util/concurrent/BlockingQueue;

    iget-object v2, p0, Lcom/google/android/apiary/FeedFetcher;->mItemEndMarker:Ljava/lang/Object;

    invoke-interface {v0, v2}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V

    :cond_7
    if-eqz v1, :cond_1

    :try_start_8
    invoke-direct {p0, v1}, Lcom/google/android/apiary/FeedFetcher;->closeResponse(Lcom/google/api/client/http/HttpResponse;)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    goto/16 :goto_2

    :catch_4
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apiary/FeedFetcher;->mLogTag:Ljava/lang/String;

    const-string v2, "IOException, while closing connection"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_2

    :cond_8
    if-nez v0, :cond_b

    :cond_9
    iget-object v0, p0, Lcom/google/android/apiary/FeedFetcher;->mEnvelopeParsedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    iget-boolean v0, p0, Lcom/google/android/apiary/FeedFetcher;->mForcedClosed:Z

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/google/android/apiary/FeedFetcher;->mItemQueue:Ljava/util/concurrent/BlockingQueue;

    iget-object v2, p0, Lcom/google/android/apiary/FeedFetcher;->mItemEndMarker:Ljava/lang/Object;

    invoke-interface {v0, v2}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V

    :cond_a
    if-eqz v1, :cond_1

    :try_start_9
    invoke-direct {p0, v1}, Lcom/google/android/apiary/FeedFetcher;->closeResponse(Lcom/google/api/client/http/HttpResponse;)V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5

    goto/16 :goto_2

    :catch_5
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apiary/FeedFetcher;->mLogTag:Ljava/lang/String;

    const-string v2, "IOException, while closing connection"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_2

    :cond_b
    :try_start_a
    invoke-direct {p0, v1}, Lcom/google/android/apiary/FeedFetcher;->closeResponse(Lcom/google/api/client/http/HttpResponse;)V

    iget-object v3, p0, Lcom/google/android/apiary/FeedFetcher;->mRequest:Lcom/google/api/client/http/json/JsonHttpRequest;

    const-string v4, "pageToken"

    invoke-virtual {v3, v4, v0}, Lcom/google/api/client/http/json/JsonHttpRequest;->set(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apiary/FeedFetcher;->mRequest:Lcom/google/api/client/http/json/JsonHttpRequest;

    invoke-virtual {v0}, Lcom/google/api/client/http/json/JsonHttpRequest;->executeUnparsed()Lcom/google/api/client/http/HttpResponse;
    :try_end_a
    .catch Lcom/google/api/client/googleapis/json/GoogleJsonResponseException; {:try_start_a .. :try_end_a} :catch_1
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    move-result-object v1

    goto/16 :goto_0

    :sswitch_0
    :try_start_b
    invoke-static {v0}, Lcom/google/android/apiary/FeedFetcher;->isPartialSyncUnavailable(Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;)Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apiary/FeedFetcher;->mPartialSyncUnavailable:Z
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto/16 :goto_6

    :catchall_0
    move-exception v0

    :goto_8
    iget-object v2, p0, Lcom/google/android/apiary/FeedFetcher;->mEnvelopeParsedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v2}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    iget-boolean v2, p0, Lcom/google/android/apiary/FeedFetcher;->mForcedClosed:Z

    if-nez v2, :cond_c

    iget-object v2, p0, Lcom/google/android/apiary/FeedFetcher;->mItemQueue:Ljava/util/concurrent/BlockingQueue;

    iget-object v3, p0, Lcom/google/android/apiary/FeedFetcher;->mItemEndMarker:Ljava/lang/Object;

    invoke-interface {v2, v3}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V

    :cond_c
    if-eqz v1, :cond_d

    :try_start_c
    invoke-direct {p0, v1}, Lcom/google/android/apiary/FeedFetcher;->closeResponse(Lcom/google/api/client/http/HttpResponse;)V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_8

    :cond_d
    :goto_9
    throw v0

    :cond_e
    const/4 v0, 0x1

    :try_start_d
    iput-boolean v0, p0, Lcom/google/android/apiary/FeedFetcher;->mIoException:Z

    goto/16 :goto_6

    :sswitch_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apiary/FeedFetcher;->mAuthenticationFailed:Z

    goto/16 :goto_6

    :sswitch_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apiary/FeedFetcher;->mPartialSyncUnavailable:Z

    goto/16 :goto_6

    :sswitch_3
    invoke-virtual {v0}, Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;->getHeaders()Lcom/google/api/client/http/HttpHeaders;

    move-result-object v0

    if-eqz v0, :cond_f

    invoke-virtual {v0}, Lcom/google/api/client/http/HttpHeaders;->getRetryAfter()Ljava/lang/String;
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    move-result-object v2

    :cond_f
    if-eqz v2, :cond_10

    :try_start_e
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    const-wide/16 v7, 0x3e8

    div-long/2addr v5, v7

    add-long/2addr v3, v5

    iput-wide v3, p0, Lcom/google/android/apiary/FeedFetcher;->mRetryAfter:J
    :try_end_e
    .catch Ljava/lang/NumberFormatException; {:try_start_e .. :try_end_e} :catch_6
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    :goto_a
    const/4 v0, 0x1

    :try_start_f
    iput-boolean v0, p0, Lcom/google/android/apiary/FeedFetcher;->mResourceUnavailable:Z
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    goto/16 :goto_6

    :catch_6
    move-exception v0

    :try_start_10
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->parse3339(Ljava/lang/String;)Z

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    div-long/2addr v3, v5

    iput-wide v3, p0, Lcom/google/android/apiary/FeedFetcher;->mRetryAfter:J
    :try_end_10
    .catch Landroid/util/TimeFormatException; {:try_start_10 .. :try_end_10} :catch_7
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    goto :goto_a

    :catch_7
    move-exception v0

    :try_start_11
    iget-object v3, p0, Lcom/google/android/apiary/FeedFetcher;->mLogTag:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unable to parse "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_a

    :cond_10
    iget-object v0, p0, Lcom/google/android/apiary/FeedFetcher;->mLogTag:Ljava/lang/String;

    const-string v2, "No Retry-After header"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    goto :goto_a

    :catch_8
    move-exception v1

    iget-object v2, p0, Lcom/google/android/apiary/FeedFetcher;->mLogTag:Ljava/lang/String;

    const-string v3, "IOException, while closing connection"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_9

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto/16 :goto_8

    :catch_9
    move-exception v0

    move-object v1, v2

    goto/16 :goto_7

    :catch_a
    move-exception v0

    move-object v1, v2

    goto/16 :goto_5

    :sswitch_data_0
    .sparse-switch
        0x190 -> :sswitch_0
        0x191 -> :sswitch_1
        0x19a -> :sswitch_2
        0x1f7 -> :sswitch_3
    .end sparse-switch
.end method

.method private static isPartialSyncUnavailable(Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;)Z
    .locals 6
    .param p0    # Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;

    invoke-virtual {p0}, Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;->getDetails()Lcom/google/api/client/googleapis/json/GoogleJsonError;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/api/client/googleapis/json/GoogleJsonError;->getErrors()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/client/googleapis/json/GoogleJsonError$ErrorInfo;

    const-string v4, "updatedMinTooLongAgo"

    invoke-virtual {v1}, Lcom/google/api/client/googleapis/json/GoogleJsonError$ErrorInfo;->getReason()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apiary/FeedFetcher;->mThread:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apiary/FeedFetcher;->mForcedClosed:Z

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    :cond_0
    return-void
.end method

.method public getRetryAfter()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apiary/FeedFetcher;->mRetryAfter:J

    return-wide v0
.end method

.method public isAuthenticationFailed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apiary/FeedFetcher;->mAuthenticationFailed:Z

    return v0
.end method

.method public isIoException()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apiary/FeedFetcher;->mIoException:Z

    return v0
.end method

.method public isPartialSyncUnavailable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apiary/FeedFetcher;->mPartialSyncUnavailable:Z

    return v0
.end method

.method public isResourceUnavailable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apiary/FeedFetcher;->mResourceUnavailable:Z

    return v0
.end method

.method protected parseField(Lcom/google/api/client/json/JsonParser;Ljava/lang/String;)Z
    .locals 1
    .param p1    # Lcom/google/api/client/json/JsonParser;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    const/4 v0, 0x0

    return v0
.end method

.method public run()V
    .locals 4

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apiary/FeedFetcher;->mThread:Ljava/lang/Thread;

    const/16 v0, 0xa

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apiary/FeedFetcher;->fetchFeed()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-object v1, p0, Lcom/google/android/apiary/FeedFetcher;->mThread:Ljava/lang/Thread;

    iget-boolean v0, p0, Lcom/google/android/apiary/FeedFetcher;->mForcedClosed:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apiary/FeedFetcher;->mLogTag:Ljava/lang/String;

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apiary/FeedFetcher;->mLogTag:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FeedFetcher thread ended: mForcedClosed is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/apiary/FeedFetcher;->mForcedClosed:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    iput-object v1, p0, Lcom/google/android/apiary/FeedFetcher;->mThread:Ljava/lang/Thread;

    iget-boolean v0, p0, Lcom/google/android/apiary/FeedFetcher;->mForcedClosed:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apiary/FeedFetcher;->mLogTag:Ljava/lang/String;

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/apiary/FeedFetcher;->mLogTag:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FeedFetcher thread ended: mForcedClosed is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/apiary/FeedFetcher;->mForcedClosed:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-object v1, p0, Lcom/google/android/apiary/FeedFetcher;->mThread:Ljava/lang/Thread;

    iget-boolean v1, p0, Lcom/google/android/apiary/FeedFetcher;->mForcedClosed:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apiary/FeedFetcher;->mLogTag:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    iget-object v1, p0, Lcom/google/android/apiary/FeedFetcher;->mLogTag:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "FeedFetcher thread ended: mForcedClosed is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/google/android/apiary/FeedFetcher;->mForcedClosed:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    throw v0
.end method

.method public waitForEnvelope()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apiary/FeedFetcher;->mEnvelopeParsedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V

    return-void
.end method
