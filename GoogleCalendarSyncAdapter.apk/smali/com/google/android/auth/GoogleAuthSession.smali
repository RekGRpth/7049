.class public Lcom/google/android/auth/GoogleAuthSession;
.super Ljava/lang/Object;
.source "GoogleAuthSession.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/auth/GoogleAuthSession$AuthServiceConnection;
    }
.end annotation


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field mAuthServiceConnection:Lcom/google/android/auth/GoogleAuthSession$AuthServiceConnection;

.field private mException:Ljava/lang/String;

.field private mExtras:Landroid/os/Bundle;

.field private mScope:Ljava/lang/String;

.field private mSession:Ljava/lang/String;

.field private mTimeoutMs:I

.field private mToken:Ljava/lang/String;

.field private mUIIntent:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/auth/GoogleAuthSession$AuthServiceConnection;

    invoke-direct {v0}, Lcom/google/android/auth/GoogleAuthSession$AuthServiceConnection;-><init>()V

    iput-object v0, p0, Lcom/google/android/auth/GoogleAuthSession;->mAuthServiceConnection:Lcom/google/android/auth/GoogleAuthSession$AuthServiceConnection;

    const/16 v0, 0x2710

    iput v0, p0, Lcom/google/android/auth/GoogleAuthSession;->mTimeoutMs:I

    if-nez p3, :cond_0

    new-instance p3, Landroid/os/Bundle;

    invoke-direct {p3}, Landroid/os/Bundle;-><init>()V

    :cond_0
    iput-object p2, p0, Lcom/google/android/auth/GoogleAuthSession;->mScope:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/auth/GoogleAuthSession;->mExtras:Landroid/os/Bundle;

    new-instance v0, Landroid/accounts/Account;

    const-string v1, "com.google"

    invoke-direct {v0, p1, v1}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/auth/GoogleAuthSession;->mAccount:Landroid/accounts/Account;

    return-void
.end method

.method private processResult(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    const-string v0, "session"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/auth/GoogleAuthSession;->mSession:Ljava/lang/String;

    const-string v0, "authtoken"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/auth/GoogleAuthSession;->mToken:Ljava/lang/String;

    const-string v0, "Error"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/auth/GoogleAuthSession;->mException:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public authenticate(Landroid/content/Context;)Ljava/lang/String;
    .locals 7
    .param p1    # Landroid/content/Context;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/auth/GoogleAuthSession;->mToken:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v3, p0, Lcom/google/android/auth/GoogleAuthSession;->mToken:Ljava/lang/String;

    :goto_0
    return-object v3

    :cond_0
    iput-object v3, p0, Lcom/google/android/auth/GoogleAuthSession;->mException:Ljava/lang/String;

    iput-object v3, p0, Lcom/google/android/auth/GoogleAuthSession;->mUIIntent:Landroid/content/Intent;

    :try_start_0
    iget-object v4, p0, Lcom/google/android/auth/GoogleAuthSession;->mAuthServiceConnection:Lcom/google/android/auth/GoogleAuthSession$AuthServiceConnection;

    iget-object v5, p0, Lcom/google/android/auth/GoogleAuthSession;->mAccount:Landroid/accounts/Account;

    iget-object v5, v5, Landroid/accounts/Account;->type:Ljava/lang/String;

    iget v6, p0, Lcom/google/android/auth/GoogleAuthSession;->mTimeoutMs:I

    invoke-virtual {v4, p1, v5, v6}, Lcom/google/android/auth/GoogleAuthSession$AuthServiceConnection;->get(Landroid/content/Context;Ljava/lang/String;I)Lcom/google/android/auth/IAuthManagerService;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v4, "AppDownloadRequired"

    iput-object v4, p0, Lcom/google/android/auth/GoogleAuthSession;->mException:Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v4, "GoogleAuthToken"

    const-string v5, "GMS remote exception "

    invoke-static {v4, v5, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const-string v4, "InternalError"

    iput-object v4, p0, Lcom/google/android/auth/GoogleAuthSession;->mException:Ljava/lang/String;

    goto :goto_0

    :cond_1
    :try_start_1
    iget-object v4, p0, Lcom/google/android/auth/GoogleAuthSession;->mExtras:Landroid/os/Bundle;

    const-string v5, "androidPackageName"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/google/android/auth/GoogleAuthSession;->mExtras:Landroid/os/Bundle;

    const-string v5, "androidPackageName"

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_2
    :try_start_2
    iget-object v4, p0, Lcom/google/android/auth/GoogleAuthSession;->mAccount:Landroid/accounts/Account;

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/auth/GoogleAuthSession;->mScope:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/auth/GoogleAuthSession;->mExtras:Landroid/os/Bundle;

    invoke-interface {v0, v4, v5, v6}, Lcom/google/android/auth/IAuthManagerService;->getToken(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/auth/GoogleAuthSession;->processResult(Landroid/os/Bundle;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    iget-object v4, p0, Lcom/google/android/auth/GoogleAuthSession;->mAuthServiceConnection:Lcom/google/android/auth/GoogleAuthSession$AuthServiceConnection;

    invoke-virtual {v4, p1}, Lcom/google/android/auth/GoogleAuthSession$AuthServiceConnection;->release(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/google/android/auth/GoogleAuthSession;->mToken:Ljava/lang/String;

    goto :goto_0

    :catchall_0
    move-exception v4

    iget-object v5, p0, Lcom/google/android/auth/GoogleAuthSession;->mAuthServiceConnection:Lcom/google/android/auth/GoogleAuthSession$AuthServiceConnection;

    invoke-virtual {v5, p1}, Lcom/google/android/auth/GoogleAuthSession$AuthServiceConnection;->release(Landroid/content/Context;)V

    throw v4
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1

    :catch_1
    move-exception v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->interrupt()V

    const-string v4, "Interrupted"

    iput-object v4, p0, Lcom/google/android/auth/GoogleAuthSession;->mException:Ljava/lang/String;

    goto :goto_0
.end method

.method public getError()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/auth/GoogleAuthSession;->mException:Ljava/lang/String;

    return-object v0
.end method

.method public invalidateToken(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/auth/GoogleAuthSession;->mToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/auth/GoogleAuthSession;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->type:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/auth/GoogleAuthSession;->mToken:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/auth/GoogleAuthSession;->mToken:Ljava/lang/String;

    return-void
.end method
