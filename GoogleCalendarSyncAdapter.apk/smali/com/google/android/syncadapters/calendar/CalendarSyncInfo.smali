.class public Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;
.super Ljava/lang/Object;
.source "CalendarSyncInfo.java"


# instance fields
.field public accessLevel:I

.field public calendarId:J

.field public calendarTimezone:Ljava/lang/String;

.field public defaultReminders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/calendar/model/EventReminder;",
            ">;"
        }
    .end annotation
.end field

.field public slidingWindowEnd:J


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
