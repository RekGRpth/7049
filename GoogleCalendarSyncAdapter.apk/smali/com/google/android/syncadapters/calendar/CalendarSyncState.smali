.class public Lcom/google/android/syncadapters/calendar/CalendarSyncState;
.super Ljava/lang/Object;
.source "CalendarSyncState.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/syncadapters/calendar/CalendarSyncState$1;,
        Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;
    }
.end annotation


# instance fields
.field private final mData:Lorg/json/JSONObject;

.field private final uri:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->uri:Landroid/net/Uri;

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    iput-object v0, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->mData:Lorg/json/JSONObject;

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->setVersion(I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->setFirstSeen(Z)V

    return-void
.end method

.method private constructor <init>(Landroid/net/Uri;Lorg/json/JSONObject;)V
    .locals 0
    .param p1    # Landroid/net/Uri;
    .param p2    # Lorg/json/JSONObject;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->uri:Landroid/net/Uri;

    iput-object p2, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->mData:Lorg/json/JSONObject;

    return-void
.end method

.method public static create(Landroid/content/ContentProviderClient;Landroid/accounts/Account;)Lcom/google/android/syncadapters/calendar/CalendarSyncState;
    .locals 5
    .param p0    # Landroid/content/ContentProviderClient;
    .param p1    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    new-instance v0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;

    invoke-direct {v0}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;-><init>()V

    const-string v3, "data"

    invoke-static {v0}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->toBytes(Lcom/google/android/syncadapters/calendar/CalendarSyncState;)[B

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-string v3, "account_name"

    iget-object v4, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "account_type"

    iget-object v4, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v3, Landroid/provider/CalendarContract$SyncState;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3, p1}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {p0, v3, v2}, Landroid/content/ContentProviderClient;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v1

    new-instance v3, Lcom/google/android/syncadapters/calendar/CalendarSyncState;

    iget-object v4, v0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->mData:Lorg/json/JSONObject;

    invoke-direct {v3, v1, v4}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;-><init>(Landroid/net/Uri;Lorg/json/JSONObject;)V

    return-object v3
.end method

.method private static findUpgradeMethod(I)Ljava/lang/reflect/Method;
    .locals 5

    :try_start_0
    const-class v0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "upgradeFrom"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Landroid/content/ContentProviderClient;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-class v4, Landroid/accounts/Account;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Missing upgrade from version: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method static fromBytes(Landroid/net/Uri;[B)Lcom/google/android/syncadapters/calendar/CalendarSyncState;
    .locals 5
    .param p0    # Landroid/net/Uri;
    .param p1    # [B

    if-eqz p1, :cond_0

    :try_start_0
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p1}, Ljava/lang/String;-><init>([B)V

    new-instance v2, Lcom/google/android/syncadapters/calendar/CalendarSyncState;

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, p0, v3}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;-><init>(Landroid/net/Uri;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    const/4 v2, 0x0

    goto :goto_0

    :cond_0
    const-string v2, "CalendarSyncAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Resetting sync state for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Lcom/google/android/syncadapters/calendar/CalendarSyncState;

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    invoke-direct {v2, p0, v3}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;-><init>(Landroid/net/Uri;Lorg/json/JSONObject;)V

    goto :goto_0
.end method

.method public static getOrCreate(Landroid/content/ContentProviderClient;Landroid/accounts/Account;)Lcom/google/android/syncadapters/calendar/CalendarSyncState;
    .locals 4
    .param p0    # Landroid/content/ContentProviderClient;
    .param p1    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v2, Landroid/provider/CalendarContract$SyncState;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {p0, v2, p1}, Landroid/provider/SyncStateContract$Helpers;->getWithUri(Landroid/content/ContentProviderClient;Landroid/net/Uri;Landroid/accounts/Account;)Landroid/util/Pair;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {p0, p1}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->create(Landroid/content/ContentProviderClient;Landroid/accounts/Account;)Lcom/google/android/syncadapters/calendar/CalendarSyncState;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Landroid/net/Uri;

    invoke-static {v2, p1}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v3

    iget-object v2, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, [B

    invoke-static {v3, v2}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->fromBytes(Landroid/net/Uri;[B)Lcom/google/android/syncadapters/calendar/CalendarSyncState;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-static {p0, p1}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->getSyncStateForUpgrade(Landroid/content/ContentProviderClient;Landroid/accounts/Account;)Lcom/google/android/syncadapters/calendar/CalendarSyncState;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-static {v0, p0, p1}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->upgradeVersion(Lcom/google/android/syncadapters/calendar/CalendarSyncState;Landroid/content/ContentProviderClient;Landroid/accounts/Account;)V

    goto :goto_0
.end method

.method private static getSyncStateForUpgrade(Landroid/content/ContentProviderClient;Landroid/accounts/Account;)Lcom/google/android/syncadapters/calendar/CalendarSyncState;
    .locals 2
    .param p0    # Landroid/content/ContentProviderClient;
    .param p1    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "CalendarSyncAdapter"

    const-string v1, "Upgrading to Apiary Sync Adapter"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0, p1}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->wipeEventsAndCalendars(Landroid/content/ContentProviderClient;Landroid/accounts/Account;)V

    const-string v0, "com.android.calendar"

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-static {p1, v0, v1}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-static {p0, p1}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->create(Landroid/content/ContentProviderClient;Landroid/accounts/Account;)Lcom/google/android/syncadapters/calendar/CalendarSyncState;

    move-result-object v0

    return-object v0
.end method

.method private static toBytes(Lcom/google/android/syncadapters/calendar/CalendarSyncState;)[B
    .locals 1
    .param p0    # Lcom/google/android/syncadapters/calendar/CalendarSyncState;

    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->mData:Lorg/json/JSONObject;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method private upgradeFrom(ILandroid/content/ContentProviderClient;Landroid/accounts/Account;)I
    .locals 7
    .param p1    # I
    .param p2    # Landroid/content/ContentProviderClient;
    .param p3    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v6, 0x2

    invoke-static {p1}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->findUpgradeMethod(I)Ljava/lang/reflect/Method;

    move-result-object v3

    const/4 v4, 0x2

    :try_start_0
    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    const/4 v5, 0x1

    aput-object p3, v4, v5

    invoke-virtual {v3, p0, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-gez v2, :cond_1

    add-int/lit8 v2, p1, 0x1

    :cond_0
    invoke-virtual {p0, v2}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->setVersion(I)V

    invoke-virtual {p0, p2}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->updateInProvider(Landroid/content/ContentProviderClient;)V

    return v2

    :cond_1
    if-le v2, p1, :cond_2

    if-le v2, v6, :cond_0

    :cond_2
    new-instance v4, Ljava/lang/IllegalStateException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Upgrade method "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " returned invalid new version: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " <= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " or > "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x2

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    if-nez v0, :cond_3

    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Invocation failed with null cause."

    invoke-direct {v4, v5, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4

    :cond_3
    instance-of v4, v0, Ljava/lang/RuntimeException;

    if-eqz v4, :cond_4

    check-cast v0, Ljava/lang/RuntimeException;

    throw v0

    :cond_4
    instance-of v4, v0, Landroid/os/RemoteException;

    if-eqz v4, :cond_5

    check-cast v0, Landroid/os/RemoteException;

    throw v0

    :cond_5
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Failed to invoke upgrade Method"

    invoke-direct {v4, v5, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4

    :catch_1
    move-exception v1

    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Failed to invoke upgrade Method"

    invoke-direct {v4, v5, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
.end method

.method private static upgradeVersion(Lcom/google/android/syncadapters/calendar/CalendarSyncState;Landroid/content/ContentProviderClient;Landroid/accounts/Account;)V
    .locals 5
    .param p0    # Lcom/google/android/syncadapters/calendar/CalendarSyncState;
    .param p1    # Landroid/content/ContentProviderClient;
    .param p2    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->getVersion()I

    move-result v0

    :goto_0
    if-ge v0, v4, :cond_0

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->upgradeFrom(ILandroid/content/ContentProviderClient;Landroid/accounts/Account;)I

    move-result v0

    goto :goto_0

    :cond_0
    if-le v0, v4, :cond_1

    const-string v1, "CalendarSyncAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Current version "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " > "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void
.end method

.method private wipeDataAndForceFullSync(Landroid/content/ContentProviderClient;Landroid/accounts/Account;)V
    .locals 4
    .param p1    # Landroid/content/ContentProviderClient;
    .param p2    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-static {p1, p2}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->wipeEventsAndCalendars(Landroid/content/ContentProviderClient;Landroid/accounts/Account;)V

    iget-object v2, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->mData:Lorg/json/JSONObject;

    invoke-virtual {v2}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/16 v2, 0x40

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    if-ltz v2, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->getFeedState(Ljava/lang/String;)Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->clear()V

    goto :goto_0

    :cond_1
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->setFirstSeen(Z)V

    const-string v2, "com.android.calendar"

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    invoke-static {p2, v2, v3}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method private static wipeEventsAndCalendars(Landroid/content/ContentProviderClient;Landroid/accounts/Account;)V
    .locals 7
    .param p0    # Landroid/content/ContentProviderClient;
    .param p1    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    sget-object v0, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, p1}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "account_name=? AND account_type=?"

    new-array v2, v6, [Ljava/lang/String;

    iget-object v3, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v3, v2, v4

    iget-object v3, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    aput-object v3, v2, v5

    invoke-virtual {p0, v0, v1, v2}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, p1}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "account_name=? AND account_type=?"

    new-array v2, v6, [Ljava/lang/String;

    iget-object v3, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v3, v2, v4

    iget-object v3, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    aput-object v3, v2, v5

    invoke-virtual {p0, v0, v1, v2}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public addFeed(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->mData:Lorg/json/JSONObject;

    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {v0, p1, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public getFeedState(Ljava/lang/String;)Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;
    .locals 5
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->mData:Lorg/json/JSONObject;

    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    :try_start_0
    new-instance v1, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;

    iget-object v3, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->mData:Lorg/json/JSONObject;

    invoke-virtual {v3, p1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v1, p0, v3, v4}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;-><init>(Lcom/google/android/syncadapters/calendar/CalendarSyncState;Lorg/json/JSONObject;Lcom/google/android/syncadapters/calendar/CalendarSyncState$1;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const-string v1, "CalendarSyncAdapter"

    const-string v3, "Bad feed object in sync state"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v1, v2

    goto :goto_0

    :cond_0
    move-object v1, v2

    goto :goto_0
.end method

.method public getVersion()I
    .locals 4

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->mData:Lorg/json/JSONObject;

    const-string v3, "version"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->mData:Lorg/json/JSONObject;

    const-string v3, "version"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public hasFeed(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->mData:Lorg/json/JSONObject;

    invoke-virtual {v0, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isFirstSeen()Z
    .locals 3

    :try_start_0
    iget-object v1, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->mData:Lorg/json/JSONObject;

    const-string v2, "firstSeen"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public newUpdateOperation()Landroid/content/ContentProviderOperation;
    .locals 2

    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->uri:Landroid/net/Uri;

    invoke-static {p0}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->toBytes(Lcom/google/android/syncadapters/calendar/CalendarSyncState;)[B

    move-result-object v1

    invoke-static {v0, v1}, Landroid/provider/SyncStateContract$Helpers;->newUpdateOperation(Landroid/net/Uri;[B)Landroid/content/ContentProviderOperation;

    move-result-object v0

    return-object v0
.end method

.method public setFirstSeen(Z)V
    .locals 2
    .param p1    # Z

    :try_start_0
    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->mData:Lorg/json/JSONObject;

    const-string v1, "firstSeen"

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setVersion(I)V
    .locals 2
    .param p1    # I

    :try_start_0
    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->mData:Lorg/json/JSONObject;

    const-string v1, "version"

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->mData:Lorg/json/JSONObject;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public updateInProvider(Landroid/content/ContentProviderClient;)V
    .locals 2
    .param p1    # Landroid/content/ContentProviderClient;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->uri:Landroid/net/Uri;

    invoke-static {p0}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->toBytes(Lcom/google/android/syncadapters/calendar/CalendarSyncState;)[B

    move-result-object v1

    invoke-static {p1, v0, v1}, Landroid/provider/SyncStateContract$Helpers;->update(Landroid/content/ContentProviderClient;Landroid/net/Uri;[B)V

    return-void
.end method

.method public upgradeFrom0(Landroid/content/ContentProviderClient;Landroid/accounts/Account;)I
    .locals 1
    .param p1    # Landroid/content/ContentProviderClient;
    .param p2    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->wipeDataAndForceFullSync(Landroid/content/ContentProviderClient;Landroid/accounts/Account;)V

    const/4 v0, -0x1

    return v0
.end method

.method public upgradeFrom1(Landroid/content/ContentProviderClient;Landroid/accounts/Account;)I
    .locals 6
    .param p1    # Landroid/content/ContentProviderClient;
    .param p2    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "allowedReminders"

    const-string v2, "0,1,2"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v1, p2}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "account_name=? AND account_type=?"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p2, Landroid/accounts/Account;->type:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/content/ContentProviderClient;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    const/4 v1, -0x1

    return v1
.end method

.method public upgradeFrom2(Landroid/content/ContentProviderClient;Landroid/accounts/Account;)I
    .locals 6
    .param p1    # Landroid/content/ContentProviderClient;
    .param p2    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "allowedReminders"

    const-string v2, "0,1,2,4"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v1, p2}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "account_name=? AND account_type=?"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p2, Landroid/accounts/Account;->type:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/content/ContentProviderClient;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    const/4 v1, -0x1

    return v1
.end method
