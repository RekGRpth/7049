.class public Lcom/google/android/syncadapters/calendar/EventFeedFetcher;
.super Lcom/google/android/apiary/FeedFetcher;
.source "EventFeedFetcher.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apiary/FeedFetcher",
        "<",
        "Lcom/google/api/services/calendar/model/Event;",
        ">;"
    }
.end annotation


# instance fields
.field private mAccessRole:Ljava/lang/String;

.field private volatile mDefaultReminders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/calendar/model/EventReminder;",
            ">;"
        }
    .end annotation
.end field

.field private volatile mLastUpdated:Ljava/lang/String;

.field private mMaxAttendees:I

.field private final mMaxResults:I

.field private final mPrimary:Z

.field private final mThreadStatsTag:I

.field private volatile mTimeZone:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/api/client/json/JsonFactory;Lcom/google/api/services/calendar/Calendar$Events$List;Ljava/util/concurrent/BlockingQueue;Lcom/google/api/services/calendar/model/Event;Ljava/lang/String;Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;IIZI)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/api/client/json/JsonFactory;",
            "Lcom/google/api/services/calendar/Calendar$Events$List;",
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Lcom/google/api/services/calendar/model/Event;",
            ">;",
            "Lcom/google/api/services/calendar/model/Event;",
            "Ljava/lang/String;",
            "Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;",
            "IIZI)V"
        }
    .end annotation

    const-string v4, "items"

    const-class v5, Lcom/google/api/services/calendar/model/Event;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v6, p3

    move-object v7, p4

    move-object v8, p5

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apiary/FeedFetcher;-><init>(Lcom/google/api/client/json/JsonFactory;Lcom/google/api/client/http/json/JsonHttpRequest;Ljava/lang/String;Ljava/lang/Class;Ljava/util/concurrent/BlockingQueue;Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mDefaultReminders:Ljava/util/List;

    move/from16 v0, p7

    iput v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mMaxAttendees:I

    move/from16 v0, p8

    iput v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mMaxResults:I

    move/from16 v0, p9

    iput-boolean v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mPrimary:Z

    move/from16 v0, p10

    iput v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mThreadStatsTag:I

    invoke-virtual {p0, p2, p6}, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->setRequestParams(Lcom/google/api/services/calendar/Calendar$Events$List;Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;)V

    return-void
.end method

.method private setBaseParams(Lcom/google/api/services/calendar/Calendar$Events$List;Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;Z)V
    .locals 4
    .param p1    # Lcom/google/api/services/calendar/Calendar$Events$List;
    .param p2    # Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;
    .param p3    # Z

    iget v2, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mMaxResults:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/google/api/services/calendar/Calendar$Events$List;->setMaxResults(Ljava/lang/Integer;)Lcom/google/api/services/calendar/Calendar$Events$List;

    const-string v2, "updated"

    invoke-virtual {p1, v2}, Lcom/google/api/services/calendar/Calendar$Events$List;->setOrderBy(Ljava/lang/String;)Lcom/google/api/services/calendar/Calendar$Events$List;

    const-string v2, "do_incremental_sync"

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    const-string v2, "feed_updated_time"

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_0

    if-nez p3, :cond_0

    invoke-virtual {p1, v1}, Lcom/google/api/services/calendar/Calendar$Events$List;->setUpdatedMin(Ljava/lang/String;)Lcom/google/api/services/calendar/Calendar$Events$List;

    :cond_0
    iget v2, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mMaxAttendees:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/google/api/services/calendar/Calendar$Events$List;->setMaxAttendees(Ljava/lang/Integer;)Lcom/google/api/services/calendar/Calendar$Events$List;

    return-void
.end method


# virtual methods
.method public getAccessRole()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mAccessRole:Ljava/lang/String;

    return-object v0
.end method

.method public getDefaultReminders()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/calendar/model/EventReminder;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mDefaultReminders:Ljava/util/List;

    return-object v0
.end method

.method public getLastUpdated()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mLastUpdated:Ljava/lang/String;

    return-object v0
.end method

.method public getTimeZone()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mTimeZone:Ljava/lang/String;

    return-object v0
.end method

.method protected parseField(Lcom/google/api/client/json/JsonParser;Ljava/lang/String;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v1, 0x1

    const-string v0, "timeZone"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0, v3}, Lcom/google/api/client/json/JsonParser;->parse(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mTimeZone:Ljava/lang/String;

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const-string v0, "updated"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0, v3}, Lcom/google/api/client/json/JsonParser;->parse(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mLastUpdated:Ljava/lang/String;

    move v0, v1

    goto :goto_0

    :cond_1
    const-string v0, "defaultReminders"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mDefaultReminders:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mDefaultReminders:Ljava/util/List;

    const-class v2, Lcom/google/api/services/calendar/model/EventReminder;

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/api/client/json/JsonParser;->parseArray(Ljava/util/Collection;Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)V

    move v0, v1

    goto :goto_0

    :cond_2
    const-string v0, "accessRole"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0, v3}, Lcom/google/api/client/json/JsonParser;->parse(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mAccessRole:Ljava/lang/String;

    move v0, v1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public run()V
    .locals 2

    iget v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mThreadStatsTag:I

    or-int/lit8 v0, v0, 0x4

    invoke-static {v0}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    invoke-super {p0}, Lcom/google/android/apiary/FeedFetcher;->run()V

    iget v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mThreadStatsTag:I

    or-int/lit8 v0, v0, 0x4

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    return-void
.end method

.method protected setRequestParams(Lcom/google/api/services/calendar/Calendar$Events$List;Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;)V
    .locals 12
    .param p1    # Lcom/google/api/services/calendar/Calendar$Events$List;
    .param p2    # Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;

    const-string v9, "new_window_end"

    const-wide/16 v10, 0x0

    invoke-virtual {p2, v9, v10, v11}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    const-wide/16 v9, 0x0

    cmp-long v9, v1, v9

    if-nez v9, :cond_3

    const/4 v9, 0x0

    invoke-direct {p0, p1, p2, v9}, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->setBaseParams(Lcom/google/api/services/calendar/Calendar$Events$List;Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;Z)V

    new-instance v6, Landroid/text/format/Time;

    const-string v9, "UTC"

    invoke-direct {v6, v9}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    const-string v9, "window_end"

    const-wide/16 v10, 0x0

    invoke-virtual {p2, v9, v10, v11}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    iget-object v9, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mLogTag:Ljava/lang/String;

    const/4 v10, 0x2

    invoke-static {v9, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mLogTag:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "mFeedSyncState: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", startMaxMs: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-wide/16 v9, 0x0

    cmp-long v9, v4, v9

    if-lez v9, :cond_1

    invoke-virtual {v6, v4, v5}, Landroid/text/format/Time;->set(J)V

    const-string v9, "%Y-%m-%dT%H:%M:%S.000Z"

    invoke-virtual {v6, v9}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/google/api/services/calendar/Calendar$Events$List;->setTimeMax(Ljava/lang/String;)Lcom/google/api/services/calendar/Calendar$Events$List;

    :cond_1
    invoke-virtual {p1}, Lcom/google/api/services/calendar/Calendar$Events$List;->getUpdatedMin()Ljava/lang/String;

    move-result-object v9

    if-nez v9, :cond_2

    new-instance v0, Landroid/text/format/Time;

    const-string v9, "UTC"

    invoke-direct {v0, v9}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    iget v9, v0, Landroid/text/format/Time;->month:I

    add-int/lit8 v9, v9, -0x1

    iput v9, v0, Landroid/text/format/Time;->month:I

    const/4 v9, 0x1

    invoke-virtual {v0, v9}, Landroid/text/format/Time;->normalize(Z)J

    const-string v9, "%Y-%m-%dT%H:%M:%S.000Z"

    invoke-virtual {v0, v9}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Lcom/google/api/services/calendar/Calendar$Events$List;->setTimeMin(Ljava/lang/String;)Lcom/google/api/services/calendar/Calendar$Events$List;

    :cond_2
    :goto_0
    return-void

    :cond_3
    const/4 v9, 0x1

    invoke-direct {p0, p1, p2, v9}, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->setBaseParams(Lcom/google/api/services/calendar/Calendar$Events$List;Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;Z)V

    new-instance v8, Landroid/text/format/Time;

    const-string v9, "UTC"

    invoke-direct {v8, v9}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    new-instance v6, Landroid/text/format/Time;

    const-string v9, "UTC"

    invoke-direct {v6, v9}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    const-string v9, "window_end"

    const-wide/16 v10, 0x0

    invoke-virtual {p2, v9, v10, v11}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->getLong(Ljava/lang/String;J)J

    move-result-wide v9

    invoke-virtual {v8, v9, v10}, Landroid/text/format/Time;->set(J)V

    invoke-virtual {v6, v1, v2}, Landroid/text/format/Time;->set(J)V

    const-string v9, "%Y-%m-%dT%H:%M:%S.000Z"

    invoke-virtual {v8, v9}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v9, "%Y-%m-%dT%H:%M:%S.000Z"

    invoke-virtual {v6, v9}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/google/api/services/calendar/Calendar$Events$List;->setTimeMax(Ljava/lang/String;)Lcom/google/api/services/calendar/Calendar$Events$List;

    invoke-virtual {p1, v7}, Lcom/google/api/services/calendar/Calendar$Events$List;->setTimeMin(Ljava/lang/String;)Lcom/google/api/services/calendar/Calendar$Events$List;

    goto :goto_0
.end method
