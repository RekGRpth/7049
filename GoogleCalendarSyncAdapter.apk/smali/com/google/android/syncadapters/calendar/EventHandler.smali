.class Lcom/google/android/syncadapters/calendar/EventHandler;
.super Ljava/lang/Object;
.source "EventHandler.java"

# interfaces
.implements Lcom/google/android/apiary/ItemAndEntityHandler;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apiary/ItemAndEntityHandler",
        "<",
        "Lcom/google/api/services/calendar/model/Event;",
        ">;"
    }
.end annotation


# static fields
.field private static final ENTRY_TYPE_TO_PROVIDER_ATTENDEE:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final PROVIDER_TYPE_TO_ENTRY_ATTENDEE:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field private final mClient:Lcom/google/api/services/calendar/Calendar;

.field private final mEventPlusPattern:Ljava/util/regex/Pattern;

.field private mProvider:Landroid/content/ContentProviderClient;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "needsAction"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "accepted"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "declined"

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "tentative"

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sput-object v0, Lcom/google/android/syncadapters/calendar/EventHandler;->ENTRY_TYPE_TO_PROVIDER_ATTENDEE:Ljava/util/HashMap;

    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    sput-object v1, Lcom/google/android/syncadapters/calendar/EventHandler;->PROVIDER_TYPE_TO_ENTRY_ATTENDEE:Landroid/util/SparseArray;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    sget-object v3, Lcom/google/android/syncadapters/calendar/EventHandler;->PROVIDER_TYPE_TO_ENTRY_ATTENDEE:Landroid/util/SparseArray;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "value "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was already encountered"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    sget-object v3, Lcom/google/android/syncadapters/calendar/EventHandler;->PROVIDER_TYPE_TO_ENTRY_ATTENDEE:Landroid/util/SparseArray;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public constructor <init>(Lcom/google/api/services/calendar/Calendar;Landroid/accounts/Account;Landroid/content/ContentProviderClient;Landroid/content/ContentResolver;)V
    .locals 3
    .param p1    # Lcom/google/api/services/calendar/Calendar;
    .param p2    # Landroid/accounts/Account;
    .param p3    # Landroid/content/ContentProviderClient;
    .param p4    # Landroid/content/ContentResolver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mClient:Lcom/google/api/services/calendar/Calendar;

    iput-object p2, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    iput-object p3, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mProvider:Landroid/content/ContentProviderClient;

    const-string v1, "google_calendar_event_plus_pattern"

    const-string v2, "^http[s]?://plus(\\.[a-z0-9]+)*\\.google\\.com/events/"

    invoke-static {p4, v1, v2}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mEventPlusPattern:Ljava/util/regex/Pattern;

    return-void
.end method

.method static synthetic access$000(Ljava/lang/Comparable;Ljava/lang/Comparable;)I
    .locals 1
    .param p0    # Ljava/lang/Comparable;
    .param p1    # Ljava/lang/Comparable;

    invoke-static {p0, p1}, Lcom/google/android/syncadapters/calendar/EventHandler;->compareObjects(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    return v0
.end method

.method private addAttendeesToEntry(Ljava/util/ArrayList;Lcom/google/api/services/calendar/model/Event;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/Entity$NamedContentValues;",
            ">;",
            "Lcom/google/api/services/calendar/model/Event;",
            ")V"
        }
    .end annotation

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Entity$NamedContentValues;

    iget-object v3, v0, Landroid/content/Entity$NamedContentValues;->uri:Landroid/net/Uri;

    iget-object v4, v0, Landroid/content/Entity$NamedContentValues;->values:Landroid/content/ContentValues;

    sget-object v0, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v3, Lcom/google/api/services/calendar/model/EventAttendee;

    invoke-direct {v3}, Lcom/google/api/services/calendar/model/EventAttendee;-><init>()V

    const-string v0, "attendeeName"

    invoke-virtual {v4, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/api/services/calendar/model/EventAttendee;->setDisplayName(Ljava/lang/String;)Lcom/google/api/services/calendar/model/EventAttendee;

    const-string v0, "attendeeEmail"

    invoke-virtual {v4, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/api/services/calendar/model/EventAttendee;->setEmail(Ljava/lang/String;)Lcom/google/api/services/calendar/model/EventAttendee;

    const-string v0, "attendeeStatus"

    invoke-virtual {v4, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    sget-object v0, Lcom/google/android/syncadapters/calendar/EventHandler;->PROVIDER_TYPE_TO_ENTRY_ATTENDEE:Landroid/util/SparseArray;

    invoke-virtual {v0, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_1

    const-string v0, "EventHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unknown attendee status: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "needsAction"

    :cond_1
    invoke-virtual {v3, v0}, Lcom/google/api/services/calendar/model/EventAttendee;->setResponseStatus(Ljava/lang/String;)Lcom/google/api/services/calendar/model/EventAttendee;

    const-string v0, "attendeeType"

    invoke-virtual {v4, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v4, 0x2

    if-ne v0, v4, :cond_2

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/api/services/calendar/model/EventAttendee;->setOptional(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/model/EventAttendee;

    :cond_2
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_4

    invoke-virtual {p2, v1}, Lcom/google/api/services/calendar/model/Event;->setAttendees(Ljava/util/List;)Lcom/google/api/services/calendar/model/Event;

    :cond_4
    return-void
.end method

.method private static addDeletedProperties(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    if-nez p0, :cond_0

    move-object v2, p1

    :goto_0
    return-object v2

    :cond_0
    if-nez p1, :cond_1

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    :cond_1
    invoke-interface {p0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {p1, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, ""

    invoke-interface {p1, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_3
    move-object v2, p1

    goto :goto_0
.end method

.method private addExtendedPropertiesToEntry(Ljava/util/ArrayList;Lcom/google/api/services/calendar/model/Event;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/Entity$NamedContentValues;",
            ">;",
            "Lcom/google/api/services/calendar/model/Event;",
            ")V"
        }
    .end annotation

    invoke-virtual {p2}, Lcom/google/api/services/calendar/model/Event;->getExtendedProperties()Lcom/google/api/services/calendar/model/Event$ExtendedProperties;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;

    invoke-direct {v0}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;-><init>()V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->setPrivate(Ljava/util/Map;)Lcom/google/api/services/calendar/model/Event$ExtendedProperties;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/google/api/services/calendar/model/Event;->setExtendedProperties(Lcom/google/api/services/calendar/model/Event$ExtendedProperties;)Lcom/google/api/services/calendar/model/Event;

    :cond_0
    invoke-virtual {v0}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->getPrivate()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Entity$NamedContentValues;

    iget-object v3, v0, Landroid/content/Entity$NamedContentValues;->uri:Landroid/net/Uri;

    iget-object v0, v0, Landroid/content/Entity$NamedContentValues;->values:Landroid/content/ContentValues;

    sget-object v4, Landroid/provider/CalendarContract$ExtendedProperties;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "name"

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "value"

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    return-void
.end method

.method private addRecurrenceToEntry(Lcom/android/calendarcommon/ICalendar$Component;Lcom/google/api/services/calendar/model/Event;)V
    .locals 6
    .param p1    # Lcom/android/calendarcommon/ICalendar$Component;
    .param p2    # Lcom/google/api/services/calendar/model/Event;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Lcom/android/calendarcommon/ICalendar$Component;->getPropertyNames()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v5, "RRULE"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, "RDATE"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, "EXRULE"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, "EXDATE"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    :cond_1
    invoke-virtual {p1, v3}, Lcom/android/calendarcommon/ICalendar$Component;->getProperties(Ljava/lang/String;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendarcommon/ICalendar$Property;

    invoke-virtual {v2}, Lcom/android/calendarcommon/ICalendar$Property;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-virtual {p2, v4}, Lcom/google/api/services/calendar/model/Event;->setRecurrence(Ljava/util/List;)Lcom/google/api/services/calendar/model/Event;

    return-void
.end method

.method private addRemindersToEntry(Ljava/util/ArrayList;Lcom/google/api/services/calendar/model/Event;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/Entity$NamedContentValues;",
            ">;",
            "Lcom/google/api/services/calendar/model/Event;",
            ")V"
        }
    .end annotation

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Entity$NamedContentValues;

    iget-object v4, v0, Landroid/content/Entity$NamedContentValues;->uri:Landroid/net/Uri;

    iget-object v0, v0, Landroid/content/Entity$NamedContentValues;->values:Landroid/content/ContentValues;

    sget-object v5, Landroid/provider/CalendarContract$Reminders;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5, v4}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "method"

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const-string v5, "minutes"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    new-instance v5, Lcom/google/api/services/calendar/model/EventReminder;

    invoke-direct {v5}, Lcom/google/api/services/calendar/model/EventReminder;-><init>()V

    invoke-virtual {v5, v0}, Lcom/google/api/services/calendar/model/EventReminder;->setMinutes(Ljava/lang/Integer;)Lcom/google/api/services/calendar/model/EventReminder;

    move-result-object v5

    packed-switch v4, :pswitch_data_0

    const-string v0, "popup"

    invoke-virtual {v5, v0}, Lcom/google/api/services/calendar/model/EventReminder;->setMethod(Ljava/lang/String;)Lcom/google/api/services/calendar/model/EventReminder;

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :pswitch_0
    const-string v0, "popup"

    invoke-virtual {v5, v0}, Lcom/google/api/services/calendar/model/EventReminder;->setMethod(Ljava/lang/String;)Lcom/google/api/services/calendar/model/EventReminder;

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :pswitch_1
    const-string v0, "email"

    invoke-virtual {v5, v0}, Lcom/google/api/services/calendar/model/EventReminder;->setMethod(Ljava/lang/String;)Lcom/google/api/services/calendar/model/EventReminder;

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :pswitch_2
    const-string v0, "sms"

    invoke-virtual {v5, v0}, Lcom/google/api/services/calendar/model/EventReminder;->setMethod(Ljava/lang/String;)Lcom/google/api/services/calendar/model/EventReminder;

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :pswitch_3
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "name"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "alarmReminder-"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "value"

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    new-instance v0, Landroid/content/Entity$NamedContentValues;

    sget-object v5, Landroid/provider/CalendarContract$ExtendedProperties;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v0, v5, v4}, Landroid/content/Entity$NamedContentValues;-><init>(Landroid/net/Uri;Landroid/content/ContentValues;)V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    new-instance v0, Lcom/google/api/services/calendar/model/Event$Reminders;

    invoke-direct {v0}, Lcom/google/api/services/calendar/model/Event$Reminders;-><init>()V

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/api/services/calendar/model/Event$Reminders;->setUseDefault(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/model/Event$Reminders;

    invoke-virtual {v0, v1}, Lcom/google/api/services/calendar/model/Event$Reminders;->setOverrides(Ljava/util/List;)Lcom/google/api/services/calendar/model/Event$Reminders;

    invoke-virtual {p2, v0}, Lcom/google/api/services/calendar/model/Event;->setReminders(Lcom/google/api/services/calendar/model/Event$Reminders;)Lcom/google/api/services/calendar/model/Event;

    :cond_2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_3

    invoke-direct {p0, v2, p2}, Lcom/google/android/syncadapters/calendar/EventHandler;->addExtendedPropertiesToEntry(Ljava/util/ArrayList;Lcom/google/api/services/calendar/model/Event;)V

    :cond_3
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static appendRecurrenceString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    if-nez p0, :cond_0

    :goto_0
    return-object p1

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private static compareObjects(Ljava/lang/Comparable;Ljava/lang/Comparable;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<TT;>;>(TT;TT;)I"
        }
    .end annotation

    if-nez p0, :cond_1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0

    :cond_1
    invoke-interface {p0, p1}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method private convertValuesToPartialDiff(Landroid/content/Entity;Landroid/content/Entity;)Lcom/google/api/services/calendar/model/Event;
    .locals 28
    .param p1    # Landroid/content/Entity;
    .param p2    # Landroid/content/Entity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual/range {p1 .. p1}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v25

    invoke-virtual/range {p2 .. p2}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v22

    new-instance v4, Landroid/content/ContentValues;

    move-object/from16 v0, v25

    invoke-direct {v4, v0}, Landroid/content/ContentValues;-><init>(Landroid/content/ContentValues;)V

    invoke-virtual/range {v25 .. v25}, Landroid/content/ContentValues;->keySet()Ljava/util/Set;

    move-result-object v26

    invoke-interface/range {v26 .. v26}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v26

    if-eqz v26, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    move-object/from16 v0, v25

    invoke-virtual {v0, v9}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v24

    move-object/from16 v0, v22

    invoke-virtual {v0, v9}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v21

    if-nez v24, :cond_1

    if-eqz v21, :cond_2

    :cond_1
    if-eqz v24, :cond_0

    move-object/from16 v0, v24

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_0

    :cond_2
    invoke-virtual {v4, v9}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string v26, "allDay"

    invoke-virtual/range {v25 .. v26}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Integer;->intValue()I

    move-result v10

    const-string v26, "allDay"

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Integer;->intValue()I

    move-result v15

    const-string v26, "dtstart"

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v26

    if-nez v26, :cond_4

    const-string v26, "dtend"

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v26

    if-eqz v26, :cond_5

    :cond_4
    const-string v26, "allDay"

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_5
    const-string v26, "allDay"

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v26

    if-eqz v26, :cond_6

    const-string v26, "dtstart"

    const-string v27, "dtstart"

    move-object/from16 v0, v25

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v27

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v26, "dtend"

    const-string v27, "dtend"

    move-object/from16 v0, v25

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v27

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_6
    new-instance v3, Landroid/content/Entity;

    invoke-direct {v3, v4}, Landroid/content/Entity;-><init>(Landroid/content/ContentValues;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/syncadapters/calendar/EventHandler;->convertEntityToEntry(Landroid/content/Entity;)Lcom/google/api/services/calendar/model/Event;

    move-result-object v5

    if-eq v10, v15, :cond_7

    if-nez v10, :cond_10

    invoke-virtual {v5}, Lcom/google/api/services/calendar/model/Event;->getStart()Lcom/google/api/services/calendar/model/EventDateTime;

    move-result-object v26

    sget-object v27, Lcom/google/api/client/util/Data;->NULL_STRING:Ljava/lang/String;

    invoke-virtual/range {v26 .. v27}, Lcom/google/api/services/calendar/model/EventDateTime;->setDate(Ljava/lang/String;)Lcom/google/api/services/calendar/model/EventDateTime;

    invoke-virtual {v5}, Lcom/google/api/services/calendar/model/Event;->getEnd()Lcom/google/api/services/calendar/model/EventDateTime;

    move-result-object v26

    sget-object v27, Lcom/google/api/client/util/Data;->NULL_STRING:Ljava/lang/String;

    invoke-virtual/range {v26 .. v27}, Lcom/google/api/services/calendar/model/EventDateTime;->setDate(Ljava/lang/String;)Lcom/google/api/services/calendar/model/EventDateTime;

    :cond_7
    :goto_1
    invoke-virtual {v5}, Lcom/google/api/services/calendar/model/Event;->size()I

    move-result v26

    const/16 v27, 0x1

    move/from16 v0, v26

    move/from16 v1, v27

    if-le v0, v1, :cond_11

    const/4 v7, 0x1

    :goto_2
    const/4 v6, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/content/Entity;->getSubValues()Ljava/util/ArrayList;

    move-result-object v23

    invoke-virtual/range {p2 .. p2}, Landroid/content/Entity;->getSubValues()Ljava/util/ArrayList;

    move-result-object v20

    new-instance v12, Lcom/google/api/services/calendar/model/Event;

    invoke-direct {v12}, Lcom/google/api/services/calendar/model/Event;-><init>()V

    new-instance v17, Lcom/google/api/services/calendar/model/Event;

    invoke-direct/range {v17 .. v17}, Lcom/google/api/services/calendar/model/Event;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1, v12}, Lcom/google/android/syncadapters/calendar/EventHandler;->addRemindersToEntry(Ljava/util/ArrayList;Lcom/google/api/services/calendar/model/Event;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/calendar/EventHandler;->addRemindersToEntry(Ljava/util/ArrayList;Lcom/google/api/services/calendar/model/Event;)V

    invoke-virtual {v12}, Lcom/google/api/services/calendar/model/Event;->getReminders()Lcom/google/api/services/calendar/model/Event$Reminders;

    move-result-object v14

    invoke-virtual/range {v17 .. v17}, Lcom/google/api/services/calendar/model/Event;->getReminders()Lcom/google/api/services/calendar/model/Event$Reminders;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v14, v1}, Lcom/google/android/syncadapters/calendar/EventHandler;->sameReminders(Lcom/google/api/services/calendar/model/Event$Reminders;Lcom/google/api/services/calendar/model/Event$Reminders;)Z

    move-result v26

    if-nez v26, :cond_9

    const/4 v6, 0x1

    if-eqz v19, :cond_8

    if-nez v14, :cond_8

    new-instance v26, Lcom/google/api/services/calendar/model/Event$Reminders;

    invoke-direct/range {v26 .. v26}, Lcom/google/api/services/calendar/model/Event$Reminders;-><init>()V

    const/16 v27, 0x0

    invoke-static/range {v27 .. v27}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Lcom/google/api/services/calendar/model/Event$Reminders;->setUseDefault(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/model/Event$Reminders;

    move-result-object v26

    new-instance v27, Ljava/util/ArrayList;

    invoke-direct/range {v27 .. v27}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {v26 .. v27}, Lcom/google/api/services/calendar/model/Event$Reminders;->setOverrides(Ljava/util/List;)Lcom/google/api/services/calendar/model/Event$Reminders;

    move-result-object v14

    :cond_8
    invoke-virtual {v5, v14}, Lcom/google/api/services/calendar/model/Event;->setReminders(Lcom/google/api/services/calendar/model/Event$Reminders;)Lcom/google/api/services/calendar/model/Event;

    :cond_9
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1, v12}, Lcom/google/android/syncadapters/calendar/EventHandler;->addAttendeesToEntry(Ljava/util/ArrayList;Lcom/google/api/services/calendar/model/Event;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/calendar/EventHandler;->addAttendeesToEntry(Ljava/util/ArrayList;Lcom/google/api/services/calendar/model/Event;)V

    invoke-virtual {v12}, Lcom/google/api/services/calendar/model/Event;->getAttendees()Ljava/util/List;

    move-result-object v11

    invoke-virtual/range {v17 .. v17}, Lcom/google/api/services/calendar/model/Event;->getAttendees()Ljava/util/List;

    move-result-object v16

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v11, v1}, Lcom/google/android/syncadapters/calendar/EventHandler;->sameAttendees(Ljava/util/List;Ljava/util/List;)Z

    move-result v26

    if-nez v26, :cond_b

    if-eqz v16, :cond_a

    if-nez v11, :cond_a

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    :cond_a
    invoke-virtual {v5, v11}, Lcom/google/api/services/calendar/model/Event;->setAttendees(Ljava/util/List;)Lcom/google/api/services/calendar/model/Event;

    :cond_b
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1, v12}, Lcom/google/android/syncadapters/calendar/EventHandler;->addExtendedPropertiesToEntry(Ljava/util/ArrayList;Lcom/google/api/services/calendar/model/Event;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/calendar/EventHandler;->addExtendedPropertiesToEntry(Ljava/util/ArrayList;Lcom/google/api/services/calendar/model/Event;)V

    invoke-virtual {v12}, Lcom/google/api/services/calendar/model/Event;->getExtendedProperties()Lcom/google/api/services/calendar/model/Event$ExtendedProperties;

    move-result-object v13

    invoke-virtual/range {v17 .. v17}, Lcom/google/api/services/calendar/model/Event;->getExtendedProperties()Lcom/google/api/services/calendar/model/Event$ExtendedProperties;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v13, v1}, Lcom/google/android/syncadapters/calendar/EventHandler;->sameExtendedProperties(Lcom/google/api/services/calendar/model/Event$ExtendedProperties;Lcom/google/api/services/calendar/model/Event$ExtendedProperties;)Z

    move-result v26

    if-nez v26, :cond_e

    const/4 v6, 0x1

    if-eqz v18, :cond_d

    if-nez v13, :cond_c

    new-instance v13, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;

    invoke-direct {v13}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;-><init>()V

    :cond_c
    invoke-virtual/range {v18 .. v18}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->getPrivate()Ljava/util/Map;

    move-result-object v26

    invoke-virtual {v13}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->getPrivate()Ljava/util/Map;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Lcom/google/android/syncadapters/calendar/EventHandler;->addDeletedProperties(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v13, v0}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->setPrivate(Ljava/util/Map;)Lcom/google/api/services/calendar/model/Event$ExtendedProperties;

    invoke-virtual/range {v18 .. v18}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->getShared()Ljava/util/Map;

    move-result-object v26

    invoke-virtual {v13}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->getShared()Ljava/util/Map;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Lcom/google/android/syncadapters/calendar/EventHandler;->addDeletedProperties(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v13, v0}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->setShared(Ljava/util/Map;)Lcom/google/api/services/calendar/model/Event$ExtendedProperties;

    :cond_d
    invoke-virtual {v5, v13}, Lcom/google/api/services/calendar/model/Event;->setExtendedProperties(Lcom/google/api/services/calendar/model/Event$ExtendedProperties;)Lcom/google/api/services/calendar/model/Event;

    :cond_e
    if-eqz v6, :cond_f

    if-nez v7, :cond_f

    const-string v26, "description"

    invoke-virtual/range {v25 .. v26}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v5, v1}, Lcom/google/android/syncadapters/calendar/EventHandler;->touchDescription(Lcom/google/api/services/calendar/model/Event;Ljava/lang/String;)V

    :cond_f
    return-object v5

    :cond_10
    invoke-virtual {v5}, Lcom/google/api/services/calendar/model/Event;->getStart()Lcom/google/api/services/calendar/model/EventDateTime;

    move-result-object v26

    sget-object v27, Lcom/google/api/client/util/Data;->NULL_DATE_TIME:Lcom/google/api/client/util/DateTime;

    invoke-virtual/range {v26 .. v27}, Lcom/google/api/services/calendar/model/EventDateTime;->setDateTime(Lcom/google/api/client/util/DateTime;)Lcom/google/api/services/calendar/model/EventDateTime;

    invoke-virtual {v5}, Lcom/google/api/services/calendar/model/Event;->getEnd()Lcom/google/api/services/calendar/model/EventDateTime;

    move-result-object v26

    sget-object v27, Lcom/google/api/client/util/Data;->NULL_DATE_TIME:Lcom/google/api/client/util/DateTime;

    invoke-virtual/range {v26 .. v27}, Lcom/google/api/services/calendar/model/EventDateTime;->setDateTime(Lcom/google/api/client/util/DateTime;)Lcom/google/api/services/calendar/model/EventDateTime;

    goto/16 :goto_1

    :cond_11
    const/4 v7, 0x0

    goto/16 :goto_2
.end method

.method private static createEventDateTime(JZ)Lcom/google/api/services/calendar/model/EventDateTime;
    .locals 4
    .param p0    # J
    .param p2    # Z

    new-instance v0, Lcom/google/api/services/calendar/model/EventDateTime;

    invoke-direct {v0}, Lcom/google/api/services/calendar/model/EventDateTime;-><init>()V

    if-eqz p2, :cond_0

    new-instance v1, Landroid/text/format/Time;

    const-string v2, "UTC"

    invoke-direct {v1, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0, p1}, Landroid/text/format/Time;->set(J)V

    invoke-virtual {v1, p2}, Landroid/text/format/Time;->format3339(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/api/services/calendar/model/EventDateTime;->setDate(Ljava/lang/String;)Lcom/google/api/services/calendar/model/EventDateTime;

    :goto_0
    return-object v0

    :cond_0
    new-instance v2, Lcom/google/api/client/util/DateTime;

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v2, p0, p1, v3}, Lcom/google/api/client/util/DateTime;-><init>(JLjava/lang/Integer;)V

    invoke-virtual {v0, v2}, Lcom/google/api/services/calendar/model/EventDateTime;->setDateTime(Lcom/google/api/client/util/DateTime;)Lcom/google/api/services/calendar/model/EventDateTime;

    goto :goto_0
.end method

.method private fetchEntity(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Entity;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/google/android/apiary/ParseException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/syncadapters/calendar/EventHandler;->newEntityIterator(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/EntityIterator;

    move-result-object v0

    :try_start_0
    invoke-interface {v0}, Landroid/content/EntityIterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Landroid/content/EntityIterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Entity;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    invoke-interface {v0}, Landroid/content/EntityIterator;->close()V

    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-interface {v0}, Landroid/content/EntityIterator;->close()V

    throw v1
.end method

.method private getAttendeeSet(Ljava/util/List;)Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/calendar/model/EventAttendee;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Lcom/google/api/services/calendar/model/EventAttendee;",
            ">;"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lcom/google/android/syncadapters/calendar/EventHandler$1;

    invoke-direct {v1, p0}, Lcom/google/android/syncadapters/calendar/EventHandler$1;-><init>(Lcom/google/android/syncadapters/calendar/EventHandler;)V

    invoke-static {v1}, Lcom/google/common/collect/Sets;->newTreeSet(Ljava/util/Comparator;)Ljava/util/TreeSet;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method private static getRecurrenceDate(Lcom/android/calendarcommon/ICalendar$Property;)Ljava/lang/String;
    .locals 2

    const-string v0, "TZID"

    invoke-virtual {p0, v0}, Lcom/android/calendarcommon/ICalendar$Property;->getFirstParameter(Ljava/lang/String;)Lcom/android/calendarcommon/ICalendar$Parameter;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, v0, Lcom/android/calendarcommon/ICalendar$Parameter;->value:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/calendarcommon/ICalendar$Property;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/android/calendarcommon/ICalendar$Property;->getValue()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getReminderSet(Lcom/google/api/services/calendar/model/Event$Reminders;)Ljava/util/Set;
    .locals 2
    .param p1    # Lcom/google/api/services/calendar/model/Event$Reminders;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/api/services/calendar/model/Event$Reminders;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lcom/google/api/services/calendar/model/EventReminder;",
            ">;"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lcom/google/android/syncadapters/calendar/EventHandler$2;

    invoke-direct {v1, p0}, Lcom/google/android/syncadapters/calendar/EventHandler$2;-><init>(Lcom/google/android/syncadapters/calendar/EventHandler;)V

    invoke-static {v1}, Lcom/google/common/collect/Sets;->newTreeSet(Ljava/util/Comparator;)Ljava/util/TreeSet;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/api/services/calendar/model/Event$Reminders;->getOverrides()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method private insertExtendedProperties(Ljava/util/List;Ljava/lang/Long;Ljava/lang/Integer;Ljava/util/Map;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const/4 v5, 0x0

    if-eqz p4, :cond_2

    invoke-interface {p4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "alarmReminder"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "name"

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "value"

    invoke-virtual {v2, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Landroid/provider/CalendarContract$ExtendedProperties;->CONTENT_URI:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v0, v1}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p1

    move-object v3, p2

    move-object v4, p3

    invoke-static/range {v0 .. v5}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addInsertOperation(Ljava/util/List;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/Long;Ljava/lang/Integer;Z)V

    goto :goto_0

    :cond_1
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "method"

    const/4 v3, 0x4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "minutes"

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v0, Landroid/provider/CalendarContract$Reminders;->CONTENT_URI:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v0, v1}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p1

    move-object v3, p2

    move-object v4, p3

    invoke-static/range {v0 .. v5}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addInsertOperation(Ljava/util/List;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/Long;Ljava/lang/Integer;Z)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method private isEqual(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    if-nez p1, :cond_1

    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method private parseDuration(Ljava/lang/String;)J
    .locals 7
    .param p1    # Ljava/lang/String;

    const-wide/16 v2, -0x1

    if-nez p1, :cond_0

    :goto_0
    return-wide v2

    :cond_0
    new-instance v0, Lcom/android/calendarcommon/Duration;

    invoke-direct {v0}, Lcom/android/calendarcommon/Duration;-><init>()V

    :try_start_0
    invoke-virtual {v0, p1}, Lcom/android/calendarcommon/Duration;->parse(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/android/calendarcommon/Duration;->getMillis()J
    :try_end_0
    .catch Lcom/android/calendarcommon/DateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v4, "EventHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Bad DURATION: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private sameAttendees(Ljava/util/List;Ljava/util/List;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/calendar/model/EventAttendee;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/calendar/model/EventAttendee;",
            ">;)Z"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/syncadapters/calendar/EventHandler;->getAttendeeSet(Ljava/util/List;)Ljava/util/Set;

    move-result-object v0

    invoke-direct {p0, p2}, Lcom/google/android/syncadapters/calendar/EventHandler;->getAttendeeSet(Ljava/util/List;)Ljava/util/Set;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/syncadapters/calendar/EventHandler;->isEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private sameExtendedProperties(Lcom/google/api/services/calendar/model/Event$ExtendedProperties;Lcom/google/api/services/calendar/model/Event$ExtendedProperties;)Z
    .locals 4
    .param p1    # Lcom/google/api/services/calendar/model/Event$ExtendedProperties;
    .param p2    # Lcom/google/api/services/calendar/model/Event$ExtendedProperties;

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p1, :cond_2

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    if-eqz p2, :cond_3

    invoke-virtual {p1}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->getPrivate()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->getPrivate()Ljava/util/Map;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lcom/google/android/syncadapters/calendar/EventHandler;->isEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p1}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->getShared()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->getShared()Ljava/util/Map;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lcom/google/android/syncadapters/calendar/EventHandler;->isEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method private sameReminders(Lcom/google/api/services/calendar/model/Event$Reminders;Lcom/google/api/services/calendar/model/Event$Reminders;)Z
    .locals 2
    .param p1    # Lcom/google/api/services/calendar/model/Event$Reminders;
    .param p2    # Lcom/google/api/services/calendar/model/Event$Reminders;

    invoke-direct {p0, p1}, Lcom/google/android/syncadapters/calendar/EventHandler;->getReminderSet(Lcom/google/api/services/calendar/model/Event$Reminders;)Ljava/util/Set;

    move-result-object v0

    invoke-direct {p0, p2}, Lcom/google/android/syncadapters/calendar/EventHandler;->getReminderSet(Lcom/google/api/services/calendar/model/Event$Reminders;)Ljava/util/Set;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/syncadapters/calendar/EventHandler;->isEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private setDtendFromDuration(Landroid/content/ContentValues;J)V
    .locals 4
    .param p1    # Landroid/content/ContentValues;
    .param p2    # J

    const-wide/16 v1, 0x0

    cmp-long v1, p2, v1

    if-ltz v1, :cond_0

    const-string v1, "dtstart"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v1, "EventHandler"

    const-string v2, "Event has DURATION but no DTSTART"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "dtend"

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    add-long/2addr v2, p2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_0
.end method

.method private touchDescription(Lcom/google/api/services/calendar/model/Event;Ljava/lang/String;)V
    .locals 2
    .param p1    # Lcom/google/api/services/calendar/model/Event;
    .param p2    # Ljava/lang/String;

    if-nez p2, :cond_0

    const-string p2, " "

    :goto_0
    invoke-virtual {p1, p2}, Lcom/google/api/services/calendar/model/Event;->setDescription(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;

    return-void

    :cond_0
    const-string v0, " "

    invoke-virtual {p2, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p2, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto :goto_0
.end method


# virtual methods
.method public applyItemToEntity(Ljava/util/List;Lcom/google/api/services/calendar/model/Event;Landroid/content/Entity;ZLandroid/content/SyncResult;Ljava/lang/Object;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Lcom/google/api/services/calendar/model/Event;",
            "Landroid/content/Entity;",
            "Z",
            "Landroid/content/SyncResult;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/google/android/apiary/ParseException;
        }
    .end annotation

    const-string v1, "EventHandler"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "EventHandler"

    const-string v2, "============= applyItemToEntity ============="

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "EventHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "event is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "EventHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "entity is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-nez p3, :cond_3

    const/4 v1, 0x1

    :goto_0
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    check-cast p6, Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;

    if-nez p2, :cond_4

    const/4 v2, 0x1

    :goto_1
    if-eqz p4, :cond_1

    const-string v4, "dirty"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_1
    const/4 v4, 0x1

    if-ne v2, v4, :cond_5

    if-eqz p3, :cond_2

    invoke-virtual {p3}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v1

    const-string v2, "_id"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    sget-object v2, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v1, v2}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {p1, v1, v2, v3}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addDeleteOperation(Ljava/util/List;Landroid/net/Uri;Ljava/lang/Long;Z)V

    move-object/from16 v0, p5

    iget-object v1, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numDeletes:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v1, Landroid/content/SyncStats;->numDeletes:J

    :cond_2
    :goto_2
    return-void

    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    :cond_4
    move-object/from16 v0, p6

    invoke-virtual {p0, p2, v3, v0}, Lcom/google/android/syncadapters/calendar/EventHandler;->entryToContentValues(Lcom/google/api/services/calendar/model/Event;Landroid/content/ContentValues;Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;)I

    move-result v2

    goto :goto_1

    :cond_5
    if-nez v2, :cond_2

    const-string v2, "EventHandler"

    const/4 v4, 0x2

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v2, "EventHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Got eventEntry from server: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    if-eqz v1, :cond_7

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    sget-object v1, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v1, v2}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addInsertOperation(Ljava/util/List;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/Long;Ljava/lang/Integer;Z)V

    const/4 v7, 0x0

    move-object/from16 v0, p5

    iget-object v1, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v1, Landroid/content/SyncStats;->numInserts:J

    const-wide/16 v9, 0x1

    add-long/2addr v4, v9

    iput-wide v4, v1, Landroid/content/SyncStats;->numInserts:J

    :goto_3
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "hasAlarm"

    invoke-virtual {v3, v2}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_f

    invoke-virtual {p2}, Lcom/google/api/services/calendar/model/Event;->getReminders()Lcom/google/api/services/calendar/model/Event$Reminders;

    move-result-object v1

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Lcom/google/api/services/calendar/model/Event$Reminders;->getUseDefault()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_b

    if-nez p6, :cond_a

    const/4 v1, 0x0

    :goto_4
    if-eqz v1, :cond_f

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_f

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/calendar/model/EventReminder;

    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    invoke-virtual {v1}, Lcom/google/api/services/calendar/model/EventReminder;->getMethod()Ljava/lang/String;

    move-result-object v4

    const-string v5, "popup"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_c

    const-string v4, "method"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v6, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :goto_6
    invoke-virtual {v1}, Lcom/google/api/services/calendar/model/EventReminder;->getMinutes()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const-string v4, "minutes"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v6, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v1, Landroid/provider/CalendarContract$Reminders;->CONTENT_URI:Landroid/net/Uri;

    iget-object v4, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v1, v4}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v5

    const/4 v9, 0x0

    move-object v4, p1

    invoke-static/range {v4 .. v9}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addInsertOperation(Ljava/util/List;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/Long;Ljava/lang/Integer;Z)V

    goto :goto_5

    :cond_7
    const/4 v8, 0x0

    invoke-virtual {p3}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v1

    const-string v2, "_id"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    sget-object v1, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v1, v2}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v2

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addUpdateOperation(Ljava/util/List;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/Long;Ljava/lang/Integer;Z)V

    sget-object v1, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v1, v2}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p1, v1, v4, v2}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addDeleteOperation(Ljava/util/List;Landroid/net/Uri;Ljava/lang/Long;Z)V

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "hasAlarm"

    invoke-virtual {v3, v2}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    if-eqz p6, :cond_9

    :cond_8
    sget-object v1, Landroid/provider/CalendarContract$Reminders;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v1, v2}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p1, v1, v4, v2}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addDeleteOperation(Ljava/util/List;Landroid/net/Uri;Ljava/lang/Long;Z)V

    :cond_9
    sget-object v1, Landroid/provider/CalendarContract$ExtendedProperties;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v1, v2}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p1, v1, v4, v2}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addDeleteOperation(Ljava/util/List;Landroid/net/Uri;Ljava/lang/Long;Z)V

    move-object/from16 v0, p5

    iget-object v1, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v5, v1, Landroid/content/SyncStats;->numUpdates:J

    const-wide/16 v9, 0x1

    add-long/2addr v5, v9

    iput-wide v5, v1, Landroid/content/SyncStats;->numUpdates:J

    move-object v7, v4

    goto/16 :goto_3

    :cond_a
    move-object/from16 v0, p6

    iget-object v1, v0, Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;->defaultReminders:Ljava/util/List;

    goto/16 :goto_4

    :cond_b
    invoke-virtual {v1}, Lcom/google/api/services/calendar/model/Event$Reminders;->getOverrides()Ljava/util/List;

    move-result-object v1

    goto/16 :goto_4

    :cond_c
    const-string v5, "email"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_d

    const-string v4, "method"

    const/4 v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v6, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_6

    :cond_d
    const-string v5, "sms"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_e

    const-string v4, "method"

    const/4 v5, 0x3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v6, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_6

    :cond_e
    const-string v5, "method"

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v6, v5, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "EventHandler"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Unknown reminder method: "

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v9, " "

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v9, "should not happen!"

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6

    :cond_f
    invoke-virtual {p2}, Lcom/google/api/services/calendar/model/Event;->getAttendees()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_16

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_7
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_16

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/calendar/model/EventAttendee;

    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    invoke-virtual {v1}, Lcom/google/api/services/calendar/model/EventAttendee;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_10

    const-string v2, ""

    :cond_10
    const-string v4, "attendeeName"

    invoke-virtual {v6, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/api/services/calendar/model/EventAttendee;->getEmail()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_11

    const-string v4, "attendeeEmail"

    invoke-virtual {v6, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_11
    const-string v2, "id"

    invoke-virtual {v1, v2}, Lcom/google/api/services/calendar/model/EventAttendee;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    if-eqz v2, :cond_12

    const-string v4, "attendeeIdNamespace"

    const-string v5, "com.google"

    invoke-virtual {v6, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "attendeeIdentity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "gprofile:"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "attendeeEmail"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "@profile.calendar.google.com"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_12
    invoke-virtual {v1}, Lcom/google/api/services/calendar/model/EventAttendee;->getResponseStatus()Ljava/lang/String;

    move-result-object v4

    sget-object v2, Lcom/google/android/syncadapters/calendar/EventHandler;->ENTRY_TYPE_TO_PROVIDER_ATTENDEE:Ljava/util/HashMap;

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    if-nez v2, :cond_13

    const-string v2, "EventHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Unknown attendee status "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    :cond_13
    const-string v4, "attendeeStatus"

    invoke-virtual {v6, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1}, Lcom/google/api/services/calendar/model/EventAttendee;->getOrganizer()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_14

    const/4 v2, 0x2

    :goto_8
    const-string v4, "attendeeRelationship"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v6, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1}, Lcom/google/api/services/calendar/model/EventAttendee;->getOptional()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    const/4 v1, 0x2

    :goto_9
    const-string v2, "attendeeType"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v6, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v1, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v1, v2}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v5

    const/4 v9, 0x0

    move-object v4, p1

    invoke-static/range {v4 .. v9}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addInsertOperation(Ljava/util/List;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/Long;Ljava/lang/Integer;Z)V

    goto/16 :goto_7

    :cond_14
    const/4 v2, 0x1

    goto :goto_8

    :cond_15
    const/4 v1, 0x1

    goto :goto_9

    :cond_16
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "hasExtendedProperties"

    invoke-virtual {v3, v2}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_17

    if-eqz v10, :cond_2

    :cond_17
    invoke-virtual {p2}, Lcom/google/api/services/calendar/model/Event;->getExtendedProperties()Lcom/google/api/services/calendar/model/Event$ExtendedProperties;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->getShared()Ljava/util/Map;

    move-result-object v2

    invoke-direct {p0, p1, v7, v8, v2}, Lcom/google/android/syncadapters/calendar/EventHandler;->insertExtendedProperties(Ljava/util/List;Ljava/lang/Long;Ljava/lang/Integer;Ljava/util/Map;)V

    invoke-virtual {v1}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->getPrivate()Ljava/util/Map;

    move-result-object v1

    invoke-direct {p0, p1, v7, v8, v1}, Lcom/google/android/syncadapters/calendar/EventHandler;->insertExtendedProperties(Ljava/util/List;Ljava/lang/Long;Ljava/lang/Integer;Ljava/util/Map;)V

    goto/16 :goto_2
.end method

.method public convertEntityToEntry(Landroid/content/Entity;)Lcom/google/api/services/calendar/model/Event;
    .locals 38
    .param p1    # Landroid/content/Entity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    new-instance v16, Lcom/google/api/services/calendar/model/Event;

    invoke-direct/range {v16 .. v16}, Lcom/google/api/services/calendar/model/Event;-><init>()V

    invoke-virtual/range {p1 .. p1}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v36

    const-string v3, "_sync_id"

    move-object/from16 v0, v36

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "_sync_id"

    move-object/from16 v0, v36

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lcom/google/api/services/calendar/model/Event;->setId(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;

    :cond_0
    const-string v3, "eventStatus"

    move-object/from16 v0, v36

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "eventStatus"

    move-object/from16 v0, v36

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v22

    if-nez v22, :cond_1

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    :cond_1
    invoke-virtual/range {v22 .. v22}, Ljava/lang/Integer;->intValue()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    const-string v3, "EventHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unexpected value for status: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "; using tentative."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v32, "tentative"

    :goto_0
    move-object/from16 v0, v16

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/google/api/services/calendar/model/Event;->setStatus(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;

    :cond_2
    const-string v3, "accessLevel"

    move-object/from16 v0, v36

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "accessLevel"

    move-object/from16 v0, v36

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v24

    if-nez v24, :cond_3

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    :cond_3
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Integer;->intValue()I

    move-result v3

    packed-switch v3, :pswitch_data_1

    const-string v3, "EventHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unexpected value for visibility: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "; using default visibility."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v37, "default"

    :goto_1
    move-object/from16 v0, v16

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Lcom/google/api/services/calendar/model/Event;->setVisibility(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;

    :cond_4
    const-string v3, "availability"

    move-object/from16 v0, v36

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    const-string v3, "availability"

    move-object/from16 v0, v36

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v23

    if-nez v23, :cond_5

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    :cond_5
    invoke-virtual/range {v23 .. v23}, Ljava/lang/Integer;->intValue()I

    move-result v3

    packed-switch v3, :pswitch_data_2

    const-string v3, "EventHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unexpected value for transparency: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "; using opaque transparency."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v35, "opaque"

    :goto_2
    move-object/from16 v0, v16

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/google/api/services/calendar/model/Event;->setTransparency(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;

    :cond_6
    const-string v3, "title"

    move-object/from16 v0, v36

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    const-string v3, "title"

    move-object/from16 v0, v36

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lcom/google/api/services/calendar/model/Event;->setSummary(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;

    :cond_7
    const-string v3, "description"

    move-object/from16 v0, v36

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    const-string v3, "description"

    move-object/from16 v0, v36

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lcom/google/api/services/calendar/model/Event;->setDescription(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;

    :cond_8
    const-string v3, "eventLocation"

    move-object/from16 v0, v36

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    const-string v3, "eventLocation"

    move-object/from16 v0, v36

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lcom/google/api/services/calendar/model/Event;->setLocation(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;

    :cond_9
    invoke-virtual/range {p1 .. p1}, Landroid/content/Entity;->getSubValues()Ljava/util/ArrayList;

    move-result-object v33

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    move-object/from16 v2, v16

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/calendar/EventHandler;->addAttendeesToEntry(Ljava/util/ArrayList;Lcom/google/api/services/calendar/model/Event;)V

    new-instance v20, Lcom/android/calendarcommon/ICalendar$Component;

    const-string v3, "DUMMY"

    const/4 v4, 0x0

    move-object/from16 v0, v20

    invoke-direct {v0, v3, v4}, Lcom/android/calendarcommon/ICalendar$Component;-><init>(Ljava/lang/String;Lcom/android/calendarcommon/ICalendar$Component;)V

    const-string v3, "RRULE"

    const-string v4, "rrule"

    move-object/from16 v0, v36

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-static {v0, v3, v4}, Lcom/android/calendarcommon/RecurrenceSet;->addPropertiesForRuleStr(Lcom/android/calendarcommon/ICalendar$Component;Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "RDATE"

    const-string v4, "rdate"

    move-object/from16 v0, v36

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-static {v0, v3, v4}, Lcom/android/calendarcommon/RecurrenceSet;->addPropertyForDateStr(Lcom/android/calendarcommon/ICalendar$Component;Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "EXRULE"

    const-string v4, "exrule"

    move-object/from16 v0, v36

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-static {v0, v3, v4}, Lcom/android/calendarcommon/RecurrenceSet;->addPropertiesForRuleStr(Lcom/android/calendarcommon/ICalendar$Component;Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "EXDATE"

    const-string v4, "exdate"

    move-object/from16 v0, v36

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-static {v0, v3, v4}, Lcom/android/calendarcommon/RecurrenceSet;->addPropertyForDateStr(Lcom/android/calendarcommon/ICalendar$Component;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {v20 .. v20}, Lcom/android/calendarcommon/ICalendar$Component;->getPropertyNames()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    if-lez v3, :cond_19

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v16

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/calendar/EventHandler;->addRecurrenceToEntry(Lcom/android/calendarcommon/ICalendar$Component;Lcom/google/api/services/calendar/model/Event;)V

    const/16 v21, 0x1

    :goto_3
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v4, "allDay"

    move-object/from16 v0, v36

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v9

    const-string v3, "eventTimezone"

    move-object/from16 v0, v36

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v34

    invoke-static/range {v34 .. v34}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1a

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v34

    const/16 v19, 0x0

    :goto_4
    const-string v3, "dtstart"

    move-object/from16 v0, v36

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_c

    const-string v3, "dtstart"

    move-object/from16 v0, v36

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    invoke-static {v14, v15, v9}, Lcom/google/android/syncadapters/calendar/EventHandler;->createEventDateTime(JZ)Lcom/google/api/services/calendar/model/EventDateTime;

    move-result-object v11

    if-nez v21, :cond_a

    if-eqz v19, :cond_b

    :cond_a
    move-object/from16 v0, v34

    invoke-virtual {v11, v0}, Lcom/google/api/services/calendar/model/EventDateTime;->setTimeZone(Ljava/lang/String;)Lcom/google/api/services/calendar/model/EventDateTime;

    :cond_b
    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Lcom/google/api/services/calendar/model/Event;->setStart(Lcom/google/api/services/calendar/model/EventDateTime;)Lcom/google/api/services/calendar/model/Event;

    :cond_c
    const-string v3, "dtend"

    move-object/from16 v0, v36

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_f

    const-string v3, "dtend"

    move-object/from16 v0, v36

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    invoke-static {v12, v13, v9}, Lcom/google/android/syncadapters/calendar/EventHandler;->createEventDateTime(JZ)Lcom/google/api/services/calendar/model/EventDateTime;

    move-result-object v11

    if-nez v21, :cond_d

    if-eqz v19, :cond_e

    :cond_d
    move-object/from16 v0, v34

    invoke-virtual {v11, v0}, Lcom/google/api/services/calendar/model/EventDateTime;->setTimeZone(Ljava/lang/String;)Lcom/google/api/services/calendar/model/EventDateTime;

    :cond_e
    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Lcom/google/api/services/calendar/model/Event;->setEnd(Lcom/google/api/services/calendar/model/EventDateTime;)Lcom/google/api/services/calendar/model/Event;

    :cond_f
    const-string v3, "hasAlarm"

    move-object/from16 v0, v36

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v18

    if-eqz v18, :cond_10

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-eqz v3, :cond_10

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    move-object/from16 v2, v16

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/calendar/EventHandler;->addRemindersToEntry(Ljava/util/ArrayList;Lcom/google/api/services/calendar/model/Event;)V

    :cond_10
    const-string v3, "hasExtendedProperties"

    move-object/from16 v0, v36

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v17

    if-eqz v17, :cond_11

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-eqz v3, :cond_11

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    move-object/from16 v2, v16

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/calendar/EventHandler;->addExtendedPropertiesToEntry(Ljava/util/ArrayList;Lcom/google/api/services/calendar/model/Event;)V

    :cond_11
    const-wide/16 v28, -0x1

    const-string v3, "original_sync_id"

    move-object/from16 v0, v36

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    const-string v3, "originalInstanceTime"

    move-object/from16 v0, v36

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_12

    const-string v3, "originalInstanceTime"

    move-object/from16 v0, v36

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v28

    :cond_12
    const-wide/16 v3, -0x1

    cmp-long v3, v28, v3

    if-eqz v3, :cond_14

    invoke-static/range {v27 .. v27}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_14

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v4, "originalAllDay"

    move-object/from16 v0, v36

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v26

    new-instance v30, Landroid/text/format/Time;

    move-object/from16 v0, v30

    move-object/from16 v1, v34

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    move-wide/from16 v1, v28

    invoke-virtual {v0, v1, v2}, Landroid/text/format/Time;->set(J)V

    move-wide/from16 v0, v28

    move/from16 v2, v26

    invoke-static {v0, v1, v2}, Lcom/google/android/syncadapters/calendar/EventHandler;->createEventDateTime(JZ)Lcom/google/api/services/calendar/model/EventDateTime;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lcom/google/api/services/calendar/model/Event;->setOriginalStartTime(Lcom/google/api/services/calendar/model/EventDateTime;)Lcom/google/api/services/calendar/model/Event;

    move-object/from16 v0, v16

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/google/api/services/calendar/model/Event;->setRecurringEventId(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mProvider:Landroid/content/ContentProviderClient;

    sget-object v4, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "sync_data2"

    aput-object v7, v5, v6

    const-string v6, "_sync_id=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object v27, v7, v8

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    if-eqz v10, :cond_14

    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_13

    const/4 v3, 0x0

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v31

    if-eqz v31, :cond_13

    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lcom/google/api/services/calendar/model/Event;->setSequence(Ljava/lang/Integer;)Lcom/google/api/services/calendar/model/Event;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_13
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_14
    const-string v3, "guestsCanInviteOthers"

    move-object/from16 v0, v36

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_15

    const-string v3, "guestsCanInviteOthers"

    move-object/from16 v0, v36

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-eqz v3, :cond_1b

    const/4 v3, 0x1

    :goto_5
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lcom/google/api/services/calendar/model/Event;->setGuestsCanInviteOthers(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/model/Event;

    :cond_15
    const-string v3, "guestsCanModify"

    move-object/from16 v0, v36

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_16

    const-string v3, "guestsCanModify"

    move-object/from16 v0, v36

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-eqz v3, :cond_1c

    const/4 v3, 0x1

    :goto_6
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lcom/google/api/services/calendar/model/Event;->setGuestsCanModify(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/model/Event;

    :cond_16
    const-string v3, "guestsCanSeeGuests"

    move-object/from16 v0, v36

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_17

    const-string v3, "guestsCanSeeGuests"

    move-object/from16 v0, v36

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-eqz v3, :cond_1d

    const/4 v3, 0x1

    :goto_7
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lcom/google/api/services/calendar/model/Event;->setGuestsCanSeeOtherGuests(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/model/Event;

    :cond_17
    const-string v3, "organizer"

    move-object/from16 v0, v36

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_18

    new-instance v25, Lcom/google/api/services/calendar/model/Event$Organizer;

    invoke-direct/range {v25 .. v25}, Lcom/google/api/services/calendar/model/Event$Organizer;-><init>()V

    const-string v3, "organizer"

    move-object/from16 v0, v36

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Lcom/google/api/services/calendar/model/Event$Organizer;->setEmail(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event$Organizer;

    move-object/from16 v0, v16

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/google/api/services/calendar/model/Event;->setOrganizer(Lcom/google/api/services/calendar/model/Event$Organizer;)Lcom/google/api/services/calendar/model/Event;

    :cond_18
    return-object v16

    :pswitch_0
    const-string v32, "cancelled"

    goto/16 :goto_0

    :pswitch_1
    const-string v32, "confirmed"

    goto/16 :goto_0

    :pswitch_2
    const-string v32, "tentative"

    goto/16 :goto_0

    :pswitch_3
    const-string v37, "default"

    goto/16 :goto_1

    :pswitch_4
    const-string v37, "confidential"

    goto/16 :goto_1

    :pswitch_5
    const-string v37, "private"

    goto/16 :goto_1

    :pswitch_6
    const-string v37, "public"

    goto/16 :goto_1

    :pswitch_7
    const-string v35, "opaque"

    goto/16 :goto_2

    :pswitch_8
    const-string v35, "transparent"

    goto/16 :goto_2

    :cond_19
    const/16 v21, 0x0

    goto/16 :goto_3

    :cond_1a
    const/16 v19, 0x1

    goto/16 :goto_4

    :catchall_0
    move-exception v3

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v3

    :cond_1b
    const/4 v3, 0x0

    goto/16 :goto_5

    :cond_1c
    const/4 v3, 0x0

    goto/16 :goto_6

    :cond_1d
    const/4 v3, 0x0

    goto :goto_7

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method protected entryToContentValues(Lcom/google/api/services/calendar/model/Event;Landroid/content/ContentValues;Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;)I
    .locals 21
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/google/android/apiary/ParseException;
        }
    .end annotation

    invoke-virtual/range {p2 .. p2}, Landroid/content/ContentValues;->clear()V

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getId()Ljava/lang/String;

    move-result-object v13

    const-string v1, "sync_data4"

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getEtag()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "_sync_id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getSequence()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v2, "sync_data2"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_0
    new-instance v6, Landroid/text/format/Time;

    const-string v1, "UTC"

    invoke-direct {v6, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getRecurringEventId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getOriginalStartTime()Lcom/google/api/services/calendar/model/EventDateTime;

    move-result-object v2

    const/4 v1, 0x0

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    if-eqz v2, :cond_1

    const/4 v4, 0x1

    invoke-virtual {v2}, Lcom/google/api/services/calendar/model/EventDateTime;->getDate()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {v6, v1}, Landroid/text/format/Time;->parse3339(Ljava/lang/String;)Z

    const/4 v1, 0x0

    invoke-virtual {v6, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    const/4 v1, 0x1

    :goto_0
    const-string v7, "original_sync_id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v7, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "originalInstanceTime"

    const-wide/16 v7, 0x3e8

    div-long/2addr v2, v7

    const-wide/16 v7, 0x3e8

    mul-long/2addr v2, v7

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v5, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "originalAllDay"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    move v1, v4

    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getStatus()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    const-string v3, "confirmed"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    :cond_2
    const/4 v2, 0x1

    :goto_1
    const-string v3, "eventStatus"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getUpdated()Lcom/google/api/client/util/DateTime;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v3}, Lcom/google/api/client/util/DateTime;->getValue()J

    move-result-wide v3

    invoke-virtual {v6, v3, v4}, Landroid/text/format/Time;->set(J)V

    const-string v3, "sync_data5"

    const/4 v4, 0x0

    invoke-virtual {v6, v4}, Landroid/text/format/Time;->format3339(Z)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    if-eqz p3, :cond_4

    const-string v3, "calendar_id"

    move-object/from16 v0, p3

    iget-wide v4, v0, Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;->calendarId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_4
    if-eqz v1, :cond_a

    const/4 v1, 0x2

    if-ne v2, v1, :cond_a

    const/4 v1, 0x0

    :goto_2
    return v1

    :cond_5
    invoke-virtual {v2}, Lcom/google/api/services/calendar/model/EventDateTime;->getDateTime()Lcom/google/api/client/util/DateTime;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/api/client/util/DateTime;->getValue()J

    move-result-wide v2

    const/4 v1, 0x0

    goto :goto_0

    :cond_6
    const-string v3, "cancelled"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    if-nez v1, :cond_7

    const/4 v1, 0x1

    goto :goto_2

    :cond_7
    const/4 v2, 0x2

    goto :goto_1

    :cond_8
    const-string v3, "tentative"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    const/4 v2, 0x0

    goto :goto_1

    :cond_9
    const-string v1, "EventHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid status: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x2

    goto :goto_2

    :cond_a
    const-string v1, "dirty"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getStart()Lcom/google/api/services/calendar/model/EventDateTime;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/api/services/calendar/model/EventDateTime;->getDate()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_15

    const-string v1, "eventTimezone"

    const-string v2, "UTC"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x1

    invoke-virtual {v6, v3}, Landroid/text/format/Time;->parse3339(Ljava/lang/String;)Z

    const/4 v2, 0x0

    invoke-virtual {v6, v2}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getEnd()Lcom/google/api/services/calendar/model/EventDateTime;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/api/services/calendar/model/EventDateTime;->getDate()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Landroid/text/format/Time;->parse3339(Ljava/lang/String;)Z

    const/4 v2, 0x0

    invoke-virtual {v6, v2}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    move-wide v7, v4

    move-wide v5, v2

    move v4, v1

    :goto_3
    const-string v1, "allDay"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "dtstart"

    const-wide/16 v2, 0x3e8

    div-long v2, v7, v2

    const-wide/16 v9, 0x3e8

    mul-long/2addr v2, v9

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getVisibility()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_b

    const-string v2, "default"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_18

    :cond_b
    const-string v1, "accessLevel"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :goto_4
    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getTransparency()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_c

    const-string v2, "opaque"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1c

    :cond_c
    const-string v1, "availability"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :goto_5
    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getSummary()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_d

    const-string v1, ""

    :cond_d
    const-string v2, "title"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getDescription()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_e

    const-string v1, ""

    :cond_e
    const-string v2, "description"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getLocation()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_f

    const-string v1, ""

    :cond_f
    const-string v2, "eventLocation"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getHtmlLink()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_10

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mEventPlusPattern:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_10

    const-string v2, "customAppUri"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "customAppPackage"

    const-string v2, "com.google.android.apps.plus"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_10
    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getColorId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_11

    const-string v2, "eventColor_index"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_11
    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getReminders()Lcom/google/api/services/calendar/model/Event$Reminders;

    move-result-object v1

    if-eqz v1, :cond_12

    invoke-virtual {v1}, Lcom/google/api/services/calendar/model/Event$Reminders;->getUseDefault()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1f

    if-nez p3, :cond_1e

    const/4 v1, 0x0

    :goto_6
    if-eqz v1, :cond_12

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_12

    const-string v1, "hasAlarm"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_12
    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getExtendedProperties()Lcom/google/api/services/calendar/model/Event$ExtendedProperties;

    move-result-object v3

    const/4 v2, 0x0

    const/4 v1, 0x1

    if-eqz v3, :cond_13

    invoke-virtual {v3}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->getPrivate()Ljava/util/Map;

    move-result-object v9

    invoke-virtual {v3}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->getShared()Ljava/util/Map;

    move-result-object v3

    if-eqz v3, :cond_20

    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_20

    const/4 v2, 0x1

    :cond_13
    :goto_7
    const-string v1, "hasExtendedProperties"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "hasAlarm"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "hasAttendeeData"

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getAttendeesOmitted()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_23

    const/4 v1, 0x0

    :goto_8
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getRecurrence()Ljava/util/List;

    move-result-object v14

    const/4 v12, 0x0

    const/4 v11, 0x0

    const/4 v10, 0x0

    const/4 v9, 0x0

    if-eqz v14, :cond_2e

    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_14
    :goto_9
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2a

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Ljava/lang/String;

    if-nez v3, :cond_24

    const-string v1, "EventHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid null recurrence line in event "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_9

    :cond_15
    const/4 v1, 0x0

    invoke-virtual {v2}, Lcom/google/api/services/calendar/model/EventDateTime;->getTimeZone()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_16

    const-string v4, "eventTimezone"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :goto_a
    invoke-virtual {v2}, Lcom/google/api/services/calendar/model/EventDateTime;->getDateTime()Lcom/google/api/client/util/DateTime;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/api/client/util/DateTime;->getValue()J

    move-result-wide v4

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getEnd()Lcom/google/api/services/calendar/model/EventDateTime;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/api/services/calendar/model/EventDateTime;->getDateTime()Lcom/google/api/client/util/DateTime;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/api/client/util/DateTime;->getValue()J

    move-result-wide v2

    move-wide v7, v4

    move-wide v5, v2

    move v4, v1

    goto/16 :goto_3

    :cond_16
    if-eqz p3, :cond_17

    const-string v3, "eventTimezone"

    move-object/from16 v0, p3

    iget-object v4, v0, Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;->calendarTimezone:Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_a

    :cond_17
    const-string v3, "eventTimezone"

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_a

    :cond_18
    const-string v2, "confidential"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_19

    const-string v1, "accessLevel"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_4

    :cond_19
    const-string v2, "private"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1a

    const-string v1, "accessLevel"

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_4

    :cond_1a
    const-string v2, "public"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1b

    const-string v1, "accessLevel"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_4

    :cond_1b
    const-string v2, "EventHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unexpected visibility: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x2

    goto/16 :goto_2

    :cond_1c
    const-string v2, "transparent"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1d

    const-string v1, "availability"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_5

    :cond_1d
    const-string v2, "EventHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unexpected transparency: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x2

    goto/16 :goto_2

    :cond_1e
    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;->defaultReminders:Ljava/util/List;

    goto/16 :goto_6

    :cond_1f
    invoke-virtual {v1}, Lcom/google/api/services/calendar/model/Event$Reminders;->getOverrides()Ljava/util/List;

    move-result-object v1

    goto/16 :goto_6

    :cond_20
    if-eqz v9, :cond_13

    invoke-interface {v9}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v3, v2

    move v2, v1

    :goto_b
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_36

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v10, "alarmReminder"

    invoke-virtual {v1, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_22

    const/4 v1, 0x1

    move v2, v3

    :goto_c
    if-eqz v1, :cond_21

    const/4 v3, 0x1

    if-eq v2, v3, :cond_13

    :cond_21
    move v3, v2

    move v2, v1

    goto :goto_b

    :cond_22
    const/4 v3, 0x1

    move v1, v2

    move v2, v3

    goto :goto_c

    :cond_23
    const/4 v1, 0x1

    goto/16 :goto_8

    :cond_24
    :try_start_0
    new-instance v1, Lcom/android/calendarcommon/ICalendar$Component;

    const-string v2, "DUMMY"

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-direct {v1, v2, v0}, Lcom/android/calendarcommon/ICalendar$Component;-><init>(Ljava/lang/String;Lcom/android/calendarcommon/ICalendar$Component;)V

    invoke-static {v1, v3}, Lcom/android/calendarcommon/ICalendar;->parseComponent(Lcom/android/calendarcommon/ICalendar$Component;Ljava/lang/String;)Lcom/android/calendarcommon/ICalendar$Component;
    :try_end_0
    .catch Lcom/android/calendarcommon/ICalendar$FormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/android/calendarcommon/ICalendar$Component;->getPropertyNames()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :cond_25
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_14

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Lcom/android/calendarcommon/ICalendar$Component;->getProperties(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :goto_d
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_25

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/calendarcommon/ICalendar$Property;

    const-string v19, "RRULE"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_26

    invoke-virtual {v1}, Lcom/android/calendarcommon/ICalendar$Property;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v12, v1}, Lcom/google/android/syncadapters/calendar/EventHandler;->appendRecurrenceString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v20, v9

    move-object v9, v10

    move-object v10, v11

    move-object v11, v1

    move-object/from16 v1, v20

    :goto_e
    move-object v12, v11

    move-object v11, v10

    move-object v10, v9

    move-object v9, v1

    goto :goto_d

    :catch_0
    move-exception v1

    const-string v2, "EventHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Invalid recurrence line in event "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v1, 0x2

    goto/16 :goto_2

    :cond_26
    const-string v19, "RDATE"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_27

    invoke-static {v1}, Lcom/google/android/syncadapters/calendar/EventHandler;->getRecurrenceDate(Lcom/android/calendarcommon/ICalendar$Property;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v11, v1}, Lcom/google/android/syncadapters/calendar/EventHandler;->appendRecurrenceString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v11, v12

    move-object/from16 v20, v10

    move-object v10, v1

    move-object v1, v9

    move-object/from16 v9, v20

    goto :goto_e

    :cond_27
    const-string v19, "EXRULE"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_28

    invoke-virtual {v1}, Lcom/android/calendarcommon/ICalendar$Property;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v10, v1}, Lcom/google/android/syncadapters/calendar/EventHandler;->appendRecurrenceString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v10, v11

    move-object v11, v12

    move-object/from16 v20, v9

    move-object v9, v1

    move-object/from16 v1, v20

    goto :goto_e

    :cond_28
    const-string v19, "EXDATE"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_29

    invoke-static {v1}, Lcom/google/android/syncadapters/calendar/EventHandler;->getRecurrenceDate(Lcom/android/calendarcommon/ICalendar$Property;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v9, v1}, Lcom/google/android/syncadapters/calendar/EventHandler;->appendRecurrenceString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v9, v10

    move-object v10, v11

    move-object v11, v12

    goto :goto_e

    :cond_29
    const-string v1, "EventHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid recurrence line in event "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ": "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x2

    goto/16 :goto_2

    :cond_2a
    :try_start_1
    new-instance v1, Lcom/android/calendarcommon/RecurrenceSet;

    invoke-direct {v1, v12, v11, v10, v9}, Lcom/android/calendarcommon/RecurrenceSet;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/android/calendarcommon/EventRecurrence$InvalidFormatException; {:try_start_1 .. :try_end_1} :catch_1

    sub-long v1, v5, v7

    const-wide/16 v5, 0x3e8

    div-long/2addr v1, v5

    if-nez v4, :cond_2d

    const-string v3, "duration"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "P"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "S"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :goto_f
    const-string v1, "rrule"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "rdate"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "exrule"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "exdate"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "guestsCanInviteOthers"

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getGuestsCanInviteOthers()Ljava/lang/Boolean;

    move-result-object v1

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    if-ne v1, v3, :cond_2f

    const/4 v1, 0x1

    :goto_10
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "guestsCanModify"

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getGuestsCanModify()Ljava/lang/Boolean;

    move-result-object v1

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    if-ne v1, v3, :cond_30

    const/4 v1, 0x1

    :goto_11
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "guestsCanSeeGuests"

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getGuestsCanSeeOtherGuests()Ljava/lang/Boolean;

    move-result-object v1

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    if-ne v1, v3, :cond_31

    const/4 v1, 0x1

    :goto_12
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getOrganizer()Lcom/google/api/services/calendar/model/Event$Organizer;

    move-result-object v8

    if-eqz v8, :cond_2c

    const/4 v7, 0x0

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v2, "self"

    invoke-virtual {v8, v2}, Lcom/google/api/services/calendar/model/Event$Organizer;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_35

    if-nez p3, :cond_32

    const-string v1, "EventHandler"

    const-string v2, "SyncInfo should not be null for an event that has an organizer"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v7

    :goto_13
    if-nez v2, :cond_2b

    invoke-virtual {v8}, Lcom/google/api/services/calendar/model/Event$Organizer;->getEmail()Ljava/lang/String;

    move-result-object v2

    :cond_2b
    if-nez v2, :cond_33

    const-string v1, "id"

    invoke-virtual {v8, v1}, Lcom/google/api/services/calendar/model/Event$Organizer;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-eqz v1, :cond_33

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "@profile.calendar.google.com"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_14
    const-string v2, "organizer"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2c
    const/4 v1, 0x0

    goto/16 :goto_2

    :catch_1
    move-exception v1

    const-string v2, "EventHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to parse recurrence in event "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v1, 0x2

    goto/16 :goto_2

    :cond_2d
    const-string v3, "duration"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "P"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-wide/32 v5, 0x15180

    div-long/2addr v1, v5

    invoke-virtual {v4, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "D"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_f

    :cond_2e
    const-string v1, "dtend"

    const-wide/16 v2, 0x3e8

    div-long v2, v5, v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto/16 :goto_f

    :cond_2f
    const/4 v1, 0x0

    goto/16 :goto_10

    :cond_30
    const/4 v1, 0x0

    goto/16 :goto_11

    :cond_31
    const/4 v1, 0x0

    goto/16 :goto_12

    :cond_32
    move-object/from16 v0, p3

    iget-wide v2, v0, Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;->calendarId:J

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mProvider:Landroid/content/ContentProviderClient;

    sget-object v4, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "ownerAccount"

    aput-object v5, v3, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/google/android/apiary/ProviderHelper;->queryProvider(Landroid/content/ContentProviderClient;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_35

    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_34

    const/4 v1, 0x0

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v1

    :goto_15
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    move-object v2, v1

    goto/16 :goto_13

    :catchall_0
    move-exception v1

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v1

    :cond_33
    move-object v1, v2

    goto/16 :goto_14

    :cond_34
    move-object v1, v7

    goto :goto_15

    :cond_35
    move-object v2, v7

    goto/16 :goto_13

    :cond_36
    move v2, v3

    goto/16 :goto_7
.end method

.method public getDeletedColumnName()Ljava/lang/String;
    .locals 1

    const-string v0, "deleted"

    return-object v0
.end method

.method public getEntitySelection()Ljava/lang/String;
    .locals 1

    const-string v0, "_sync_id IS NULL OR (_sync_id IS NOT NULL AND (dirty != 0 OR deleted != 0))"

    return-object v0
.end method

.method public getEntityUri()Landroid/net/Uri;
    .locals 2

    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v0, v1}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public itemToSourceId(Lcom/google/api/services/calendar/model/Event;)Ljava/lang/String;
    .locals 1
    .param p1    # Lcom/google/api/services/calendar/model/Event;

    invoke-virtual {p1}, Lcom/google/api/services/calendar/model/Event;->getId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic itemToSourceId(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/calendar/model/Event;

    invoke-virtual {p0, p1}, Lcom/google/android/syncadapters/calendar/EventHandler;->itemToSourceId(Lcom/google/api/services/calendar/model/Event;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public newEntityIterator(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/EntityIterator;
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/google/android/apiary/ParseException;
        }
    .end annotation

    const/4 v2, 0x0

    sget-object v0, Landroid/provider/CalendarContract$EventsEntity;->CONTENT_URI:Landroid/net/Uri;

    iget-object v3, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v0, v3}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mProvider:Landroid/content/ContentProviderClient;

    move-object v3, p1

    move-object v4, p2

    move-object v5, v2

    invoke-static/range {v0 .. v5}, Lcom/google/android/apiary/ProviderHelper;->queryProvider(Landroid/content/ContentProviderClient;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mProvider:Landroid/content/ContentProviderClient;

    invoke-static {v0, v2}, Landroid/provider/CalendarContract$EventsEntity;->newEntityIterator(Landroid/database/Cursor;Landroid/content/ContentProviderClient;)Landroid/content/EntityIterator;

    move-result-object v0

    return-object v0
.end method

.method public sendEntityToServer(Landroid/content/Entity;Landroid/content/SyncResult;)Ljava/util/ArrayList;
    .locals 25
    .param p1    # Landroid/content/Entity;
    .param p2    # Landroid/content/SyncResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Entity;",
            "Landroid/content/SyncResult;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/io/IOException;,
            Lcom/google/android/apiary/ParseException;
        }
    .end annotation

    invoke-virtual/range {p1 .. p1}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v24

    const-string v4, "cal_sync1"

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    if-nez v14, :cond_0

    const-string v4, "EventHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Entity with no calendar id found: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x0

    :goto_0
    return-object v3

    :cond_0
    const-string v4, "local android etag magic value"

    const-string v6, "sync_data4"

    move-object/from16 v0, v24

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "EventHandler"

    const/4 v6, 0x2

    invoke-static {v4, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "EventHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Entity with magic ETAG found: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    :cond_2
    const-string v4, "_sync_id"

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    const-string v4, "deleted"

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-eqz v4, :cond_3

    const/16 v19, 0x1

    :goto_1
    if-eqz v19, :cond_4

    if-nez v18, :cond_4

    const-string v4, "EventHandler"

    const-string v6, "Local event deleted. Do nothing"

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x0

    goto :goto_0

    :cond_3
    const/16 v19, 0x0

    goto :goto_1

    :cond_4
    invoke-static {}, Landroid/net/TrafficStats;->getThreadStatsTag()I

    move-result v13

    const/16 v23, 0x0

    const-string v4, "duration"

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/syncadapters/calendar/EventHandler;->parseDuration(Ljava/lang/String;)J

    move-result-wide v6

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-direct {v0, v1, v6, v7}, Lcom/google/android/syncadapters/calendar/EventHandler;->setDtendFromDuration(Landroid/content/ContentValues;J)V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    if-nez v18, :cond_8

    or-int/lit8 v23, v13, 0x1

    :try_start_0
    invoke-static/range {v23 .. v23}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    invoke-virtual/range {p0 .. p1}, Lcom/google/android/syncadapters/calendar/EventHandler;->convertEntityToEntry(Landroid/content/Entity;)Lcom/google/api/services/calendar/model/Event;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v17

    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mClient:Lcom/google/api/services/calendar/Calendar;

    invoke-virtual {v4}, Lcom/google/api/services/calendar/Calendar;->events()Lcom/google/api/services/calendar/Calendar$Events;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v4, v14, v0}, Lcom/google/api/services/calendar/Calendar$Events;->insert(Ljava/lang/String;Lcom/google/api/services/calendar/model/Event;)Lcom/google/api/services/calendar/Calendar$Events$Insert;

    move-result-object v4

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/google/api/services/calendar/Calendar$Events$Insert;->setSendNotifications(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/Calendar$Events$Insert;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/api/services/calendar/Calendar$Events$Insert;->execute()Lcom/google/api/services/calendar/model/Event;
    :try_end_1
    .catch Lcom/google/api/client/googleapis/json/GoogleJsonResponseException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v8

    :goto_2
    if-eqz v23, :cond_5

    const/4 v4, 0x1

    move/from16 v0, v23

    invoke-static {v0, v4}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    :cond_5
    invoke-static {v13}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    const/4 v10, 0x1

    const/4 v12, 0x0

    move-object/from16 v6, p0

    move-object v7, v3

    move-object/from16 v9, p1

    move-object/from16 v11, p2

    invoke-virtual/range {v6 .. v12}, Lcom/google/android/syncadapters/calendar/EventHandler;->applyItemToEntity(Ljava/util/List;Lcom/google/api/services/calendar/model/Event;Landroid/content/Entity;ZLandroid/content/SyncResult;Ljava/lang/Object;)V

    goto/16 :goto_0

    :catch_0
    move-exception v16

    :try_start_2
    invoke-virtual/range {v16 .. v16}, Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;->getStatusCode()I

    move-result v4

    const/16 v6, 0x193

    if-ne v4, v6, :cond_7

    const-string v4, "EventHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Can\'t insert event into "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": Forbidden"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v4, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    const-string v6, "_id"

    move-object/from16 v0, v24

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v4, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v4, v6}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v4

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v3, v4, v6, v7}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addDeleteOperation(Ljava/util/List;Landroid/net/Uri;Ljava/lang/Long;Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v23, :cond_6

    const/4 v4, 0x1

    move/from16 v0, v23

    invoke-static {v0, v4}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    :cond_6
    :goto_3
    invoke-static {v13}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    goto/16 :goto_0

    :cond_7
    :try_start_3
    const-string v4, "EventHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Can\'t insert event in "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-static {v4, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const/4 v3, 0x0

    if-eqz v23, :cond_6

    const/4 v4, 0x1

    move/from16 v0, v23

    invoke-static {v0, v4}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    goto :goto_3

    :cond_8
    if-eqz v19, :cond_9

    or-int/lit8 v23, v13, 0x3

    :try_start_4
    invoke-static/range {v23 .. v23}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mClient:Lcom/google/api/services/calendar/Calendar;

    invoke-virtual {v4}, Lcom/google/api/services/calendar/Calendar;->events()Lcom/google/api/services/calendar/Calendar$Events;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v4, v14, v0}, Lcom/google/api/services/calendar/Calendar$Events;->delete(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/services/calendar/Calendar$Events$Delete;

    move-result-object v4

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/google/api/services/calendar/Calendar$Events$Delete;->setSendNotifications(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/Calendar$Events$Delete;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/api/services/calendar/Calendar$Events$Delete;->execute()V
    :try_end_5
    .catch Lcom/google/api/client/googleapis/json/GoogleJsonResponseException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    const/4 v8, 0x0

    goto/16 :goto_2

    :catch_1
    move-exception v16

    :try_start_6
    invoke-virtual/range {v16 .. v16}, Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;->getStatusCode()I

    move-result v15

    sparse-switch v15, :sswitch_data_0

    const-string v4, "EventHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Can\'t delete event "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-static {v4, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    const/4 v3, 0x0

    if-eqz v23, :cond_6

    const/4 v4, 0x1

    move/from16 v0, v23

    invoke-static {v0, v4}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    goto/16 :goto_3

    :sswitch_0
    :try_start_7
    const-string v4, "EventHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Can\'t delete event "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": Forbidden"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "deleted"

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v4, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v4, v6}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v4

    const-string v6, "_id"

    move-object/from16 v0, v24

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-static/range {v3 .. v8}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addUpdateOperation(Ljava/util/List;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/Long;Ljava/lang/Integer;Z)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mClient:Lcom/google/api/services/calendar/Calendar;

    invoke-virtual {v4}, Lcom/google/api/services/calendar/Calendar;->events()Lcom/google/api/services/calendar/Calendar$Events;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v4, v14, v0}, Lcom/google/api/services/calendar/Calendar$Events;->get(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/services/calendar/Calendar$Events$Get;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/api/services/calendar/Calendar$Events$Get;->execute()Lcom/google/api/services/calendar/model/Event;

    move-result-object v8

    goto/16 :goto_2

    :sswitch_1
    const-string v4, "EventHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Can\'t delete event "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": Not found"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v8, 0x0

    goto/16 :goto_2

    :sswitch_2
    const-string v4, "EventHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Can\'t delete event "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": Deleted from server"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v8, 0x0

    goto/16 :goto_2

    :cond_9
    or-int/lit8 v23, v13, 0x2

    invoke-static/range {v23 .. v23}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    const-string v4, "_sync_id =? AND lastSynced =?"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v18, v6, v7

    const/4 v7, 0x1

    const-string v9, "1"

    aput-object v9, v6, v7

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v6}, Lcom/google/android/syncadapters/calendar/EventHandler;->fetchEntity(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Entity;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result-object v20

    if-nez v20, :cond_b

    :try_start_8
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/syncadapters/calendar/EventHandler;->convertEntityToEntry(Landroid/content/Entity;)Lcom/google/api/services/calendar/model/Event;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mClient:Lcom/google/api/services/calendar/Calendar;

    invoke-virtual {v4}, Lcom/google/api/services/calendar/Calendar;->events()Lcom/google/api/services/calendar/Calendar$Events;

    move-result-object v4

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v4, v14, v0, v1}, Lcom/google/api/services/calendar/Calendar$Events;->update(Ljava/lang/String;Ljava/lang/String;Lcom/google/api/services/calendar/model/Event;)Lcom/google/api/services/calendar/Calendar$Events$Update;

    move-result-object v4

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/google/api/services/calendar/Calendar$Events$Update;->setSendNotifications(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/Calendar$Events$Update;

    move-result-object v22

    const-string v4, "sync_data4"

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_a

    const-string v4, "ifmatch"

    const-string v6, "sync_data4"

    move-object/from16 v0, v24

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v22

    invoke-virtual {v0, v4, v6}, Lcom/google/api/services/calendar/Calendar$Events$Update;->set(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_a
    invoke-virtual/range {v22 .. v22}, Lcom/google/api/services/calendar/Calendar$Events$Update;->execute()Lcom/google/api/services/calendar/model/Event;

    move-result-object v8

    goto/16 :goto_2

    :cond_b
    invoke-virtual/range {v20 .. v20}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v21

    const-string v4, "duration"

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/syncadapters/calendar/EventHandler;->parseDuration(Ljava/lang/String;)J

    move-result-wide v6

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v6, v7}, Lcom/google/android/syncadapters/calendar/EventHandler;->setDtendFromDuration(Landroid/content/ContentValues;J)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/calendar/EventHandler;->convertValuesToPartialDiff(Landroid/content/Entity;Landroid/content/Entity;)Lcom/google/api/services/calendar/model/Event;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mClient:Lcom/google/api/services/calendar/Calendar;

    invoke-virtual {v4}, Lcom/google/api/services/calendar/Calendar;->events()Lcom/google/api/services/calendar/Calendar$Events;

    move-result-object v4

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v4, v14, v0, v1}, Lcom/google/api/services/calendar/Calendar$Events;->patch(Ljava/lang/String;Ljava/lang/String;Lcom/google/api/services/calendar/model/Event;)Lcom/google/api/services/calendar/Calendar$Events$Patch;

    move-result-object v4

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/google/api/services/calendar/Calendar$Events$Patch;->setSendNotifications(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/Calendar$Events$Patch;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/google/api/services/calendar/Calendar$Events$Patch;->execute()Lcom/google/api/services/calendar/model/Event;
    :try_end_8
    .catch Lcom/google/api/client/googleapis/json/GoogleJsonResponseException; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move-result-object v8

    goto/16 :goto_2

    :catch_2
    move-exception v16

    :try_start_9
    invoke-virtual/range {v16 .. v16}, Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;->getStatusCode()I

    move-result v15

    sparse-switch v15, :sswitch_data_1

    const-string v4, "EventHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Can\'t update event "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-static {v4, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    const/4 v3, 0x0

    if-eqz v23, :cond_6

    const/4 v4, 0x1

    move/from16 v0, v23

    invoke-static {v0, v4}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    goto/16 :goto_3

    :sswitch_3
    :try_start_a
    const-string v4, "EventHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Can\'t update event "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": Forbidden"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mClient:Lcom/google/api/services/calendar/Calendar;

    invoke-virtual {v4}, Lcom/google/api/services/calendar/Calendar;->events()Lcom/google/api/services/calendar/Calendar$Events;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v4, v14, v0}, Lcom/google/api/services/calendar/Calendar$Events;->get(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/services/calendar/Calendar$Events$Get;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/api/services/calendar/Calendar$Events$Get;->execute()Lcom/google/api/services/calendar/model/Event;

    move-result-object v8

    goto/16 :goto_2

    :sswitch_4
    const-string v4, "EventHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Can\'t update event "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": Not found"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v8, 0x0

    goto/16 :goto_2

    :sswitch_5
    const-string v4, "EventHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Can\'t update event "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": Deleted from server"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v8, 0x0

    goto/16 :goto_2

    :sswitch_6
    const-string v4, "EventHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Can\'t update event "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": Conflict detected"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mClient:Lcom/google/api/services/calendar/Calendar;

    invoke-virtual {v4}, Lcom/google/api/services/calendar/Calendar;->events()Lcom/google/api/services/calendar/Calendar$Events;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v4, v14, v0}, Lcom/google/api/services/calendar/Calendar$Events;->get(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/services/calendar/Calendar$Events$Get;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/api/services/calendar/Calendar$Events$Get;->execute()Lcom/google/api/services/calendar/model/Event;
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    move-result-object v8

    goto/16 :goto_2

    :catchall_0
    move-exception v4

    if-eqz v23, :cond_c

    const/4 v6, 0x1

    move/from16 v0, v23

    invoke-static {v0, v6}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    :cond_c
    invoke-static {v13}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    throw v4

    :sswitch_data_0
    .sparse-switch
        0x193 -> :sswitch_0
        0x194 -> :sswitch_1
        0x19e -> :sswitch_2
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x193 -> :sswitch_3
        0x194 -> :sswitch_4
        0x19c -> :sswitch_6
        0x19e -> :sswitch_5
    .end sparse-switch
.end method
