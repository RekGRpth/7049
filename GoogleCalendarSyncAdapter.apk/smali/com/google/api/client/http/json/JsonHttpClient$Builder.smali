.class public Lcom/google/api/client/http/json/JsonHttpClient$Builder;
.super Ljava/lang/Object;
.source "JsonHttpClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/api/client/http/json/JsonHttpClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private applicationName:Ljava/lang/String;

.field private baseUrl:Lcom/google/api/client/http/GenericUrl;

.field private httpRequestInitializer:Lcom/google/api/client/http/HttpRequestInitializer;

.field private final jsonFactory:Lcom/google/api/client/json/JsonFactory;

.field private jsonHttpRequestInitializer:Lcom/google/api/client/http/json/JsonHttpRequestInitializer;

.field private final transport:Lcom/google/api/client/http/HttpTransport;


# direct methods
.method protected constructor <init>(Lcom/google/api/client/http/HttpTransport;Lcom/google/api/client/json/JsonFactory;Lcom/google/api/client/http/GenericUrl;)V
    .locals 0
    .param p1    # Lcom/google/api/client/http/HttpTransport;
    .param p2    # Lcom/google/api/client/json/JsonFactory;
    .param p3    # Lcom/google/api/client/http/GenericUrl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/api/client/http/json/JsonHttpClient$Builder;->transport:Lcom/google/api/client/http/HttpTransport;

    iput-object p2, p0, Lcom/google/api/client/http/json/JsonHttpClient$Builder;->jsonFactory:Lcom/google/api/client/json/JsonFactory;

    invoke-virtual {p0, p3}, Lcom/google/api/client/http/json/JsonHttpClient$Builder;->setBaseUrl(Lcom/google/api/client/http/GenericUrl;)Lcom/google/api/client/http/json/JsonHttpClient$Builder;

    return-void
.end method


# virtual methods
.method public final getApplicationName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/http/json/JsonHttpClient$Builder;->applicationName:Ljava/lang/String;

    return-object v0
.end method

.method public final getBaseUrl()Lcom/google/api/client/http/GenericUrl;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/http/json/JsonHttpClient$Builder;->baseUrl:Lcom/google/api/client/http/GenericUrl;

    return-object v0
.end method

.method public final getHttpRequestInitializer()Lcom/google/api/client/http/HttpRequestInitializer;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/http/json/JsonHttpClient$Builder;->httpRequestInitializer:Lcom/google/api/client/http/HttpRequestInitializer;

    return-object v0
.end method

.method public final getJsonFactory()Lcom/google/api/client/json/JsonFactory;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/http/json/JsonHttpClient$Builder;->jsonFactory:Lcom/google/api/client/json/JsonFactory;

    return-object v0
.end method

.method public getJsonHttpRequestInitializer()Lcom/google/api/client/http/json/JsonHttpRequestInitializer;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/http/json/JsonHttpClient$Builder;->jsonHttpRequestInitializer:Lcom/google/api/client/http/json/JsonHttpRequestInitializer;

    return-object v0
.end method

.method public final getTransport()Lcom/google/api/client/http/HttpTransport;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/http/json/JsonHttpClient$Builder;->transport:Lcom/google/api/client/http/HttpTransport;

    return-object v0
.end method

.method public setBaseUrl(Lcom/google/api/client/http/GenericUrl;)Lcom/google/api/client/http/json/JsonHttpClient$Builder;
    .locals 2
    .param p1    # Lcom/google/api/client/http/GenericUrl;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/http/GenericUrl;

    iput-object v0, p0, Lcom/google/api/client/http/json/JsonHttpClient$Builder;->baseUrl:Lcom/google/api/client/http/GenericUrl;

    invoke-virtual {p1}, Lcom/google/api/client/http/GenericUrl;->build()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    return-object p0
.end method

.method public setHttpRequestInitializer(Lcom/google/api/client/http/HttpRequestInitializer;)Lcom/google/api/client/http/json/JsonHttpClient$Builder;
    .locals 0
    .param p1    # Lcom/google/api/client/http/HttpRequestInitializer;

    iput-object p1, p0, Lcom/google/api/client/http/json/JsonHttpClient$Builder;->httpRequestInitializer:Lcom/google/api/client/http/HttpRequestInitializer;

    return-object p0
.end method
