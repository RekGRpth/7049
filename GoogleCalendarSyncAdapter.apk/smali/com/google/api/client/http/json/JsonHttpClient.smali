.class public Lcom/google/api/client/http/json/JsonHttpClient;
.super Ljava/lang/Object;
.source "JsonHttpClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/api/client/http/json/JsonHttpClient$Builder;
    }
.end annotation


# static fields
.field static final LOGGER:Ljava/util/logging/Logger;


# instance fields
.field private final applicationName:Ljava/lang/String;

.field private final baseUrl:Ljava/lang/String;

.field private final jsonFactory:Lcom/google/api/client/json/JsonFactory;

.field private jsonHttpParser:Lcom/google/api/client/http/json/JsonHttpParser;

.field private final jsonHttpRequestInitializer:Lcom/google/api/client/http/json/JsonHttpRequestInitializer;

.field private final requestFactory:Lcom/google/api/client/http/HttpRequestFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/api/client/http/json/JsonHttpClient;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/api/client/http/json/JsonHttpClient;->LOGGER:Ljava/util/logging/Logger;

    return-void
.end method

.method protected constructor <init>(Lcom/google/api/client/http/HttpTransport;Lcom/google/api/client/http/json/JsonHttpRequestInitializer;Lcom/google/api/client/http/HttpRequestInitializer;Lcom/google/api/client/json/JsonFactory;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/google/api/client/http/HttpTransport;
    .param p2    # Lcom/google/api/client/http/json/JsonHttpRequestInitializer;
    .param p3    # Lcom/google/api/client/http/HttpRequestInitializer;
    .param p4    # Lcom/google/api/client/json/JsonFactory;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/api/client/http/json/JsonHttpClient;->jsonHttpRequestInitializer:Lcom/google/api/client/http/json/JsonHttpRequestInitializer;

    invoke-static {p5}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/api/client/http/json/JsonHttpClient;->baseUrl:Ljava/lang/String;

    const-string v0, "/"

    invoke-virtual {p5, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    iput-object p6, p0, Lcom/google/api/client/http/json/JsonHttpClient;->applicationName:Ljava/lang/String;

    invoke-static {p4}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/json/JsonFactory;

    iput-object v0, p0, Lcom/google/api/client/http/json/JsonHttpClient;->jsonFactory:Lcom/google/api/client/json/JsonFactory;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-nez p3, :cond_0

    invoke-virtual {p1}, Lcom/google/api/client/http/HttpTransport;->createRequestFactory()Lcom/google/api/client/http/HttpRequestFactory;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/api/client/http/json/JsonHttpClient;->requestFactory:Lcom/google/api/client/http/HttpRequestFactory;

    return-void

    :cond_0
    invoke-virtual {p1, p3}, Lcom/google/api/client/http/HttpTransport;->createRequestFactory(Lcom/google/api/client/http/HttpRequestInitializer;)Lcom/google/api/client/http/HttpRequestFactory;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected buildHttpRequest(Lcom/google/api/client/http/HttpMethod;Lcom/google/api/client/http/GenericUrl;Ljava/lang/Object;)Lcom/google/api/client/http/HttpRequest;
    .locals 3
    .param p1    # Lcom/google/api/client/http/HttpMethod;
    .param p2    # Lcom/google/api/client/http/GenericUrl;
    .param p3    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/google/api/client/http/json/JsonHttpClient;->requestFactory:Lcom/google/api/client/http/HttpRequestFactory;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, p2, v2}, Lcom/google/api/client/http/HttpRequestFactory;->buildRequest(Lcom/google/api/client/http/HttpMethod;Lcom/google/api/client/http/GenericUrl;Lcom/google/api/client/http/HttpContent;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/api/client/http/json/JsonHttpClient;->getJsonHttpParser()Lcom/google/api/client/http/json/JsonHttpParser;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/api/client/http/HttpRequest;->addParser(Lcom/google/api/client/http/HttpParser;)V

    invoke-virtual {p0}, Lcom/google/api/client/http/json/JsonHttpClient;->getApplicationName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/api/client/http/HttpRequest;->getHeaders()Lcom/google/api/client/http/HttpHeaders;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/api/client/http/json/JsonHttpClient;->getApplicationName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/api/client/http/HttpHeaders;->setUserAgent(Ljava/lang/String;)V

    :cond_0
    if-eqz p3, :cond_1

    invoke-virtual {p0, p3}, Lcom/google/api/client/http/json/JsonHttpClient;->createSerializer(Ljava/lang/Object;)Lcom/google/api/client/http/json/JsonHttpContent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/api/client/http/HttpRequest;->setContent(Lcom/google/api/client/http/HttpContent;)Lcom/google/api/client/http/HttpRequest;

    :cond_1
    return-object v0
.end method

.method protected createParser()Lcom/google/api/client/http/json/JsonHttpParser;
    .locals 2

    new-instance v0, Lcom/google/api/client/http/json/JsonHttpParser;

    iget-object v1, p0, Lcom/google/api/client/http/json/JsonHttpClient;->jsonFactory:Lcom/google/api/client/json/JsonFactory;

    invoke-direct {v0, v1}, Lcom/google/api/client/http/json/JsonHttpParser;-><init>(Lcom/google/api/client/json/JsonFactory;)V

    return-object v0
.end method

.method protected createSerializer(Ljava/lang/Object;)Lcom/google/api/client/http/json/JsonHttpContent;
    .locals 2
    .param p1    # Ljava/lang/Object;

    new-instance v0, Lcom/google/api/client/http/json/JsonHttpContent;

    invoke-virtual {p0}, Lcom/google/api/client/http/json/JsonHttpClient;->getJsonFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/google/api/client/http/json/JsonHttpContent;-><init>(Lcom/google/api/client/json/JsonFactory;Ljava/lang/Object;)V

    return-object v0
.end method

.method protected executeUnparsed(Lcom/google/api/client/http/HttpMethod;Lcom/google/api/client/http/GenericUrl;Ljava/lang/Object;)Lcom/google/api/client/http/HttpResponse;
    .locals 2
    .param p1    # Lcom/google/api/client/http/HttpMethod;
    .param p2    # Lcom/google/api/client/http/GenericUrl;
    .param p3    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/api/client/http/json/JsonHttpClient;->buildHttpRequest(Lcom/google/api/client/http/HttpMethod;Lcom/google/api/client/http/GenericUrl;Ljava/lang/Object;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/client/http/HttpRequest;->execute()Lcom/google/api/client/http/HttpResponse;

    move-result-object v1

    return-object v1
.end method

.method public final getApplicationName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/http/json/JsonHttpClient;->applicationName:Ljava/lang/String;

    return-object v0
.end method

.method public final getBaseUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/http/json/JsonHttpClient;->baseUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final getJsonFactory()Lcom/google/api/client/json/JsonFactory;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/http/json/JsonHttpClient;->jsonFactory:Lcom/google/api/client/json/JsonFactory;

    return-object v0
.end method

.method public final getJsonHttpParser()Lcom/google/api/client/http/json/JsonHttpParser;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/http/json/JsonHttpClient;->jsonHttpParser:Lcom/google/api/client/http/json/JsonHttpParser;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/api/client/http/json/JsonHttpClient;->createParser()Lcom/google/api/client/http/json/JsonHttpParser;

    move-result-object v0

    iput-object v0, p0, Lcom/google/api/client/http/json/JsonHttpClient;->jsonHttpParser:Lcom/google/api/client/http/json/JsonHttpParser;

    :cond_0
    iget-object v0, p0, Lcom/google/api/client/http/json/JsonHttpClient;->jsonHttpParser:Lcom/google/api/client/http/json/JsonHttpParser;

    return-object v0
.end method

.method public final getJsonHttpRequestInitializer()Lcom/google/api/client/http/json/JsonHttpRequestInitializer;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/http/json/JsonHttpClient;->jsonHttpRequestInitializer:Lcom/google/api/client/http/json/JsonHttpRequestInitializer;

    return-object v0
.end method

.method protected initialize(Lcom/google/api/client/http/json/JsonHttpRequest;)V
    .locals 1
    .param p1    # Lcom/google/api/client/http/json/JsonHttpRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/api/client/http/json/JsonHttpClient;->getJsonHttpRequestInitializer()Lcom/google/api/client/http/json/JsonHttpRequestInitializer;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/api/client/http/json/JsonHttpClient;->getJsonHttpRequestInitializer()Lcom/google/api/client/http/json/JsonHttpRequestInitializer;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/api/client/http/json/JsonHttpRequestInitializer;->initialize(Lcom/google/api/client/http/json/JsonHttpRequest;)V

    :cond_0
    return-void
.end method
