.class public Lcom/google/api/client/http/json/JsonHttpRequest;
.super Lcom/google/api/client/util/GenericData;
.source "JsonHttpRequest.java"


# instance fields
.field private final client:Lcom/google/api/client/http/json/JsonHttpClient;

.field private final content:Ljava/lang/Object;

.field private final method:Lcom/google/api/client/http/HttpMethod;

.field private final uriTemplate:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/api/client/http/json/JsonHttpClient;Lcom/google/api/client/http/HttpMethod;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1
    .param p1    # Lcom/google/api/client/http/json/JsonHttpClient;
    .param p2    # Lcom/google/api/client/http/HttpMethod;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/api/client/util/GenericData;-><init>()V

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/http/json/JsonHttpClient;

    iput-object v0, p0, Lcom/google/api/client/http/json/JsonHttpRequest;->client:Lcom/google/api/client/http/json/JsonHttpClient;

    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/http/HttpMethod;

    iput-object v0, p0, Lcom/google/api/client/http/json/JsonHttpRequest;->method:Lcom/google/api/client/http/HttpMethod;

    invoke-static {p3}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/api/client/http/json/JsonHttpRequest;->uriTemplate:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/api/client/http/json/JsonHttpRequest;->content:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final buildHttpRequestUrl()Lcom/google/api/client/http/GenericUrl;
    .locals 4

    new-instance v0, Lcom/google/api/client/http/GenericUrl;

    invoke-virtual {p0}, Lcom/google/api/client/http/json/JsonHttpRequest;->getClient()Lcom/google/api/client/http/json/JsonHttpClient;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/api/client/http/json/JsonHttpClient;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/api/client/http/json/JsonHttpRequest;->uriTemplate:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-static {v1, v2, p0, v3}, Lcom/google/api/client/http/UriTemplate;->expand(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Z)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/api/client/http/GenericUrl;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public executeUnparsed()Lcom/google/api/client/http/HttpResponse;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/api/client/http/json/JsonHttpRequest;->client:Lcom/google/api/client/http/json/JsonHttpClient;

    iget-object v1, p0, Lcom/google/api/client/http/json/JsonHttpRequest;->method:Lcom/google/api/client/http/HttpMethod;

    invoke-virtual {p0}, Lcom/google/api/client/http/json/JsonHttpRequest;->buildHttpRequestUrl()Lcom/google/api/client/http/GenericUrl;

    move-result-object v2

    iget-object v3, p0, Lcom/google/api/client/http/json/JsonHttpRequest;->content:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/api/client/http/json/JsonHttpClient;->executeUnparsed(Lcom/google/api/client/http/HttpMethod;Lcom/google/api/client/http/GenericUrl;Ljava/lang/Object;)Lcom/google/api/client/http/HttpResponse;

    move-result-object v0

    return-object v0
.end method

.method public final getClient()Lcom/google/api/client/http/json/JsonHttpClient;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/http/json/JsonHttpRequest;->client:Lcom/google/api/client/http/json/JsonHttpClient;

    return-object v0
.end method
