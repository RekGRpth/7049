.class public Lcom/google/api/services/calendar/Calendar$CalendarList$List;
.super Lcom/google/api/services/calendar/CalendarRequest;
.source "Calendar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/api/services/calendar/Calendar$CalendarList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "List"
.end annotation


# instance fields
.field private maxResults:Ljava/lang/Integer;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private pageToken:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field final synthetic this$1:Lcom/google/api/services/calendar/Calendar$CalendarList;


# direct methods
.method private constructor <init>(Lcom/google/api/services/calendar/Calendar$CalendarList;)V
    .locals 4

    iput-object p1, p0, Lcom/google/api/services/calendar/Calendar$CalendarList$List;->this$1:Lcom/google/api/services/calendar/Calendar$CalendarList;

    iget-object v0, p1, Lcom/google/api/services/calendar/Calendar$CalendarList;->this$0:Lcom/google/api/services/calendar/Calendar;

    sget-object v1, Lcom/google/api/client/http/HttpMethod;->GET:Lcom/google/api/client/http/HttpMethod;

    const-string v2, "users/me/calendarList"

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/api/services/calendar/CalendarRequest;-><init>(Lcom/google/api/client/http/json/JsonHttpClient;Lcom/google/api/client/http/HttpMethod;Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/api/services/calendar/Calendar$CalendarList;Lcom/google/api/services/calendar/Calendar$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/api/services/calendar/Calendar$CalendarList$List;-><init>(Lcom/google/api/services/calendar/Calendar$CalendarList;)V

    return-void
.end method


# virtual methods
.method public execute()Lcom/google/api/services/calendar/model/CalendarList;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/api/services/calendar/Calendar$CalendarList$List;->executeUnparsed()Lcom/google/api/client/http/HttpResponse;

    move-result-object v1

    const-class v0, Lcom/google/api/services/calendar/model/CalendarList;

    invoke-virtual {v1, v0}, Lcom/google/api/client/http/HttpResponse;->parseAs(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/calendar/model/CalendarList;

    invoke-virtual {v1}, Lcom/google/api/client/http/HttpResponse;->getHeaders()Lcom/google/api/client/http/HttpHeaders;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/api/services/calendar/model/CalendarList;->setResponseHeaders(Lcom/google/api/client/http/HttpHeaders;)V

    return-object v0
.end method

.method public setMaxResults(Ljava/lang/Integer;)Lcom/google/api/services/calendar/Calendar$CalendarList$List;
    .locals 0

    iput-object p1, p0, Lcom/google/api/services/calendar/Calendar$CalendarList$List;->maxResults:Ljava/lang/Integer;

    return-object p0
.end method

.method public setPageToken(Ljava/lang/String;)Lcom/google/api/services/calendar/Calendar$CalendarList$List;
    .locals 0

    iput-object p1, p0, Lcom/google/api/services/calendar/Calendar$CalendarList$List;->pageToken:Ljava/lang/String;

    return-object p0
.end method
