.class public Lcom/google/api/services/calendar/Calendar$Events;
.super Ljava/lang/Object;
.source "Calendar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/api/services/calendar/Calendar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Events"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/api/services/calendar/Calendar$Events$Delete;,
        Lcom/google/api/services/calendar/Calendar$Events$Patch;,
        Lcom/google/api/services/calendar/Calendar$Events$Update;,
        Lcom/google/api/services/calendar/Calendar$Events$List;,
        Lcom/google/api/services/calendar/Calendar$Events$Get;,
        Lcom/google/api/services/calendar/Calendar$Events$Insert;
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/api/services/calendar/Calendar;


# direct methods
.method public constructor <init>(Lcom/google/api/services/calendar/Calendar;)V
    .locals 0

    iput-object p1, p0, Lcom/google/api/services/calendar/Calendar$Events;->this$0:Lcom/google/api/services/calendar/Calendar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public delete(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/services/calendar/Calendar$Events$Delete;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/api/services/calendar/Calendar$Events$Delete;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/google/api/services/calendar/Calendar$Events$Delete;-><init>(Lcom/google/api/services/calendar/Calendar$Events;Ljava/lang/String;Ljava/lang/String;Lcom/google/api/services/calendar/Calendar$1;)V

    iget-object v1, p0, Lcom/google/api/services/calendar/Calendar$Events;->this$0:Lcom/google/api/services/calendar/Calendar;

    # invokes: Lcom/google/api/services/calendar/Calendar;->initialize(Lcom/google/api/client/http/json/JsonHttpRequest;)V
    invoke-static {v1, v0}, Lcom/google/api/services/calendar/Calendar;->access$6300(Lcom/google/api/services/calendar/Calendar;Lcom/google/api/client/http/json/JsonHttpRequest;)V

    return-object v0
.end method

.method public get(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/services/calendar/Calendar$Events$Get;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/api/services/calendar/Calendar$Events$Get;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/google/api/services/calendar/Calendar$Events$Get;-><init>(Lcom/google/api/services/calendar/Calendar$Events;Ljava/lang/String;Ljava/lang/String;Lcom/google/api/services/calendar/Calendar$1;)V

    iget-object v1, p0, Lcom/google/api/services/calendar/Calendar$Events;->this$0:Lcom/google/api/services/calendar/Calendar;

    # invokes: Lcom/google/api/services/calendar/Calendar;->initialize(Lcom/google/api/client/http/json/JsonHttpRequest;)V
    invoke-static {v1, v0}, Lcom/google/api/services/calendar/Calendar;->access$4700(Lcom/google/api/services/calendar/Calendar;Lcom/google/api/client/http/json/JsonHttpRequest;)V

    return-object v0
.end method

.method public insert(Ljava/lang/String;Lcom/google/api/services/calendar/model/Event;)Lcom/google/api/services/calendar/Calendar$Events$Insert;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/api/services/calendar/Calendar$Events$Insert;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/google/api/services/calendar/Calendar$Events$Insert;-><init>(Lcom/google/api/services/calendar/Calendar$Events;Ljava/lang/String;Lcom/google/api/services/calendar/model/Event;Lcom/google/api/services/calendar/Calendar$1;)V

    iget-object v1, p0, Lcom/google/api/services/calendar/Calendar$Events;->this$0:Lcom/google/api/services/calendar/Calendar;

    # invokes: Lcom/google/api/services/calendar/Calendar;->initialize(Lcom/google/api/client/http/json/JsonHttpRequest;)V
    invoke-static {v1, v0}, Lcom/google/api/services/calendar/Calendar;->access$4500(Lcom/google/api/services/calendar/Calendar;Lcom/google/api/client/http/json/JsonHttpRequest;)V

    return-object v0
.end method

.method public list(Ljava/lang/String;)Lcom/google/api/services/calendar/Calendar$Events$List;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/api/services/calendar/Calendar$Events$List;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/google/api/services/calendar/Calendar$Events$List;-><init>(Lcom/google/api/services/calendar/Calendar$Events;Ljava/lang/String;Lcom/google/api/services/calendar/Calendar$1;)V

    iget-object v1, p0, Lcom/google/api/services/calendar/Calendar$Events;->this$0:Lcom/google/api/services/calendar/Calendar;

    # invokes: Lcom/google/api/services/calendar/Calendar;->initialize(Lcom/google/api/client/http/json/JsonHttpRequest;)V
    invoke-static {v1, v0}, Lcom/google/api/services/calendar/Calendar;->access$5100(Lcom/google/api/services/calendar/Calendar;Lcom/google/api/client/http/json/JsonHttpRequest;)V

    return-object v0
.end method

.method public patch(Ljava/lang/String;Ljava/lang/String;Lcom/google/api/services/calendar/model/Event;)Lcom/google/api/services/calendar/Calendar$Events$Patch;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/api/services/calendar/Calendar$Events$Patch;

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/api/services/calendar/Calendar$Events$Patch;-><init>(Lcom/google/api/services/calendar/Calendar$Events;Ljava/lang/String;Ljava/lang/String;Lcom/google/api/services/calendar/model/Event;Lcom/google/api/services/calendar/Calendar$1;)V

    iget-object v1, p0, Lcom/google/api/services/calendar/Calendar$Events;->this$0:Lcom/google/api/services/calendar/Calendar;

    # invokes: Lcom/google/api/services/calendar/Calendar;->initialize(Lcom/google/api/client/http/json/JsonHttpRequest;)V
    invoke-static {v1, v0}, Lcom/google/api/services/calendar/Calendar;->access$5500(Lcom/google/api/services/calendar/Calendar;Lcom/google/api/client/http/json/JsonHttpRequest;)V

    return-object v0
.end method

.method public update(Ljava/lang/String;Ljava/lang/String;Lcom/google/api/services/calendar/model/Event;)Lcom/google/api/services/calendar/Calendar$Events$Update;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/api/services/calendar/Calendar$Events$Update;

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/api/services/calendar/Calendar$Events$Update;-><init>(Lcom/google/api/services/calendar/Calendar$Events;Ljava/lang/String;Ljava/lang/String;Lcom/google/api/services/calendar/model/Event;Lcom/google/api/services/calendar/Calendar$1;)V

    iget-object v1, p0, Lcom/google/api/services/calendar/Calendar$Events;->this$0:Lcom/google/api/services/calendar/Calendar;

    # invokes: Lcom/google/api/services/calendar/Calendar;->initialize(Lcom/google/api/client/http/json/JsonHttpRequest;)V
    invoke-static {v1, v0}, Lcom/google/api/services/calendar/Calendar;->access$5300(Lcom/google/api/services/calendar/Calendar;Lcom/google/api/client/http/json/JsonHttpRequest;)V

    return-object v0
.end method
