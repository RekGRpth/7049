.class public Lcom/google/api/services/calendar/Calendar;
.super Lcom/google/api/client/googleapis/services/GoogleClient;
.source "Calendar.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/api/services/calendar/Calendar$1;,
        Lcom/google/api/services/calendar/Calendar$Builder;,
        Lcom/google/api/services/calendar/Calendar$Events;,
        Lcom/google/api/services/calendar/Calendar$CalendarList;
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/google/api/client/http/HttpTransport;Lcom/google/api/client/http/json/JsonHttpRequestInitializer;Lcom/google/api/client/http/HttpRequestInitializer;Lcom/google/api/client/json/JsonFactory;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct/range {p0 .. p6}, Lcom/google/api/client/googleapis/services/GoogleClient;-><init>(Lcom/google/api/client/http/HttpTransport;Lcom/google/api/client/http/json/JsonHttpRequestInitializer;Lcom/google/api/client/http/HttpRequestInitializer;Lcom/google/api/client/json/JsonFactory;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/api/services/calendar/Calendar;Lcom/google/api/client/http/json/JsonHttpRequest;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/api/services/calendar/Calendar;->initialize(Lcom/google/api/client/http/json/JsonHttpRequest;)V

    return-void
.end method

.method static synthetic access$4500(Lcom/google/api/services/calendar/Calendar;Lcom/google/api/client/http/json/JsonHttpRequest;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/api/services/calendar/Calendar;->initialize(Lcom/google/api/client/http/json/JsonHttpRequest;)V

    return-void
.end method

.method static synthetic access$4700(Lcom/google/api/services/calendar/Calendar;Lcom/google/api/client/http/json/JsonHttpRequest;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/api/services/calendar/Calendar;->initialize(Lcom/google/api/client/http/json/JsonHttpRequest;)V

    return-void
.end method

.method static synthetic access$5100(Lcom/google/api/services/calendar/Calendar;Lcom/google/api/client/http/json/JsonHttpRequest;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/api/services/calendar/Calendar;->initialize(Lcom/google/api/client/http/json/JsonHttpRequest;)V

    return-void
.end method

.method static synthetic access$5300(Lcom/google/api/services/calendar/Calendar;Lcom/google/api/client/http/json/JsonHttpRequest;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/api/services/calendar/Calendar;->initialize(Lcom/google/api/client/http/json/JsonHttpRequest;)V

    return-void
.end method

.method static synthetic access$5500(Lcom/google/api/services/calendar/Calendar;Lcom/google/api/client/http/json/JsonHttpRequest;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/api/services/calendar/Calendar;->initialize(Lcom/google/api/client/http/json/JsonHttpRequest;)V

    return-void
.end method

.method static synthetic access$6300(Lcom/google/api/services/calendar/Calendar;Lcom/google/api/client/http/json/JsonHttpRequest;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/api/services/calendar/Calendar;->initialize(Lcom/google/api/client/http/json/JsonHttpRequest;)V

    return-void
.end method

.method public static builder(Lcom/google/api/client/http/HttpTransport;Lcom/google/api/client/json/JsonFactory;)Lcom/google/api/services/calendar/Calendar$Builder;
    .locals 1

    new-instance v0, Lcom/google/api/services/calendar/Calendar$Builder;

    invoke-direct {v0, p0, p1}, Lcom/google/api/services/calendar/Calendar$Builder;-><init>(Lcom/google/api/client/http/HttpTransport;Lcom/google/api/client/json/JsonFactory;)V

    return-object v0
.end method


# virtual methods
.method public calendarList()Lcom/google/api/services/calendar/Calendar$CalendarList;
    .locals 1

    new-instance v0, Lcom/google/api/services/calendar/Calendar$CalendarList;

    invoke-direct {v0, p0}, Lcom/google/api/services/calendar/Calendar$CalendarList;-><init>(Lcom/google/api/services/calendar/Calendar;)V

    return-object v0
.end method

.method public events()Lcom/google/api/services/calendar/Calendar$Events;
    .locals 1

    new-instance v0, Lcom/google/api/services/calendar/Calendar$Events;

    invoke-direct {v0, p0}, Lcom/google/api/services/calendar/Calendar$Events;-><init>(Lcom/google/api/services/calendar/Calendar;)V

    return-object v0
.end method
