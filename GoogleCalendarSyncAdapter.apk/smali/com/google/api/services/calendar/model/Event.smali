.class public final Lcom/google/api/services/calendar/model/Event;
.super Lcom/google/api/client/json/GenericJson;
.source "Event.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/api/services/calendar/model/Event$Reminders;,
        Lcom/google/api/services/calendar/model/Event$ExtendedProperties;,
        Lcom/google/api/services/calendar/model/Event$Organizer;
    }
.end annotation


# instance fields
.field private attendees:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/calendar/model/EventAttendee;",
            ">;"
        }
    .end annotation
.end field

.field private attendeesOmitted:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private colorId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private description:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private end:Lcom/google/api/services/calendar/model/EventDateTime;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private etag:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private extendedProperties:Lcom/google/api/services/calendar/model/Event$ExtendedProperties;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private guestsCanInviteOthers:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private guestsCanModify:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private guestsCanSeeOtherGuests:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private htmlLink:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private id:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private location:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private organizer:Lcom/google/api/services/calendar/model/Event$Organizer;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private originalStartTime:Lcom/google/api/services/calendar/model/EventDateTime;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private recurrence:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private recurringEventId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private reminders:Lcom/google/api/services/calendar/model/Event$Reminders;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private responseHeaders:Lcom/google/api/client/http/HttpHeaders;

.field private sequence:Ljava/lang/Integer;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private start:Lcom/google/api/services/calendar/model/EventDateTime;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private status:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private summary:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private transparency:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private updated:Lcom/google/api/client/util/DateTime;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private visibility:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method


# virtual methods
.method public getAttendees()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/calendar/model/EventAttendee;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->attendees:Ljava/util/List;

    return-object v0
.end method

.method public getAttendeesOmitted()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->attendeesOmitted:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getColorId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->colorId:Ljava/lang/String;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getEnd()Lcom/google/api/services/calendar/model/EventDateTime;
    .locals 1

    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->end:Lcom/google/api/services/calendar/model/EventDateTime;

    return-object v0
.end method

.method public getEtag()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->etag:Ljava/lang/String;

    return-object v0
.end method

.method public getExtendedProperties()Lcom/google/api/services/calendar/model/Event$ExtendedProperties;
    .locals 1

    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->extendedProperties:Lcom/google/api/services/calendar/model/Event$ExtendedProperties;

    return-object v0
.end method

.method public getGuestsCanInviteOthers()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->guestsCanInviteOthers:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getGuestsCanModify()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->guestsCanModify:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getGuestsCanSeeOtherGuests()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->guestsCanSeeOtherGuests:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getHtmlLink()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->htmlLink:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getLocation()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->location:Ljava/lang/String;

    return-object v0
.end method

.method public getOrganizer()Lcom/google/api/services/calendar/model/Event$Organizer;
    .locals 1

    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->organizer:Lcom/google/api/services/calendar/model/Event$Organizer;

    return-object v0
.end method

.method public getOriginalStartTime()Lcom/google/api/services/calendar/model/EventDateTime;
    .locals 1

    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->originalStartTime:Lcom/google/api/services/calendar/model/EventDateTime;

    return-object v0
.end method

.method public getRecurrence()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->recurrence:Ljava/util/List;

    return-object v0
.end method

.method public getRecurringEventId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->recurringEventId:Ljava/lang/String;

    return-object v0
.end method

.method public getReminders()Lcom/google/api/services/calendar/model/Event$Reminders;
    .locals 1

    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->reminders:Lcom/google/api/services/calendar/model/Event$Reminders;

    return-object v0
.end method

.method public getSequence()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->sequence:Ljava/lang/Integer;

    return-object v0
.end method

.method public getStart()Lcom/google/api/services/calendar/model/EventDateTime;
    .locals 1

    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->start:Lcom/google/api/services/calendar/model/EventDateTime;

    return-object v0
.end method

.method public getStatus()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->status:Ljava/lang/String;

    return-object v0
.end method

.method public getSummary()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->summary:Ljava/lang/String;

    return-object v0
.end method

.method public getTransparency()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->transparency:Ljava/lang/String;

    return-object v0
.end method

.method public getUpdated()Lcom/google/api/client/util/DateTime;
    .locals 1

    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->updated:Lcom/google/api/client/util/DateTime;

    return-object v0
.end method

.method public getVisibility()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->visibility:Ljava/lang/String;

    return-object v0
.end method

.method public setAttendees(Ljava/util/List;)Lcom/google/api/services/calendar/model/Event;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/calendar/model/EventAttendee;",
            ">;)",
            "Lcom/google/api/services/calendar/model/Event;"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/api/services/calendar/model/Event;->attendees:Ljava/util/List;

    return-object p0
.end method

.method public setDescription(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;
    .locals 0

    iput-object p1, p0, Lcom/google/api/services/calendar/model/Event;->description:Ljava/lang/String;

    return-object p0
.end method

.method public setEnd(Lcom/google/api/services/calendar/model/EventDateTime;)Lcom/google/api/services/calendar/model/Event;
    .locals 0

    iput-object p1, p0, Lcom/google/api/services/calendar/model/Event;->end:Lcom/google/api/services/calendar/model/EventDateTime;

    return-object p0
.end method

.method public setExtendedProperties(Lcom/google/api/services/calendar/model/Event$ExtendedProperties;)Lcom/google/api/services/calendar/model/Event;
    .locals 0

    iput-object p1, p0, Lcom/google/api/services/calendar/model/Event;->extendedProperties:Lcom/google/api/services/calendar/model/Event$ExtendedProperties;

    return-object p0
.end method

.method public setGuestsCanInviteOthers(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/model/Event;
    .locals 0

    iput-object p1, p0, Lcom/google/api/services/calendar/model/Event;->guestsCanInviteOthers:Ljava/lang/Boolean;

    return-object p0
.end method

.method public setGuestsCanModify(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/model/Event;
    .locals 0

    iput-object p1, p0, Lcom/google/api/services/calendar/model/Event;->guestsCanModify:Ljava/lang/Boolean;

    return-object p0
.end method

.method public setGuestsCanSeeOtherGuests(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/model/Event;
    .locals 0

    iput-object p1, p0, Lcom/google/api/services/calendar/model/Event;->guestsCanSeeOtherGuests:Ljava/lang/Boolean;

    return-object p0
.end method

.method public setId(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;
    .locals 0

    iput-object p1, p0, Lcom/google/api/services/calendar/model/Event;->id:Ljava/lang/String;

    return-object p0
.end method

.method public setLocation(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;
    .locals 0

    iput-object p1, p0, Lcom/google/api/services/calendar/model/Event;->location:Ljava/lang/String;

    return-object p0
.end method

.method public setOrganizer(Lcom/google/api/services/calendar/model/Event$Organizer;)Lcom/google/api/services/calendar/model/Event;
    .locals 0

    iput-object p1, p0, Lcom/google/api/services/calendar/model/Event;->organizer:Lcom/google/api/services/calendar/model/Event$Organizer;

    return-object p0
.end method

.method public setOriginalStartTime(Lcom/google/api/services/calendar/model/EventDateTime;)Lcom/google/api/services/calendar/model/Event;
    .locals 0

    iput-object p1, p0, Lcom/google/api/services/calendar/model/Event;->originalStartTime:Lcom/google/api/services/calendar/model/EventDateTime;

    return-object p0
.end method

.method public setRecurrence(Ljava/util/List;)Lcom/google/api/services/calendar/model/Event;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/api/services/calendar/model/Event;"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/api/services/calendar/model/Event;->recurrence:Ljava/util/List;

    return-object p0
.end method

.method public setRecurringEventId(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;
    .locals 0

    iput-object p1, p0, Lcom/google/api/services/calendar/model/Event;->recurringEventId:Ljava/lang/String;

    return-object p0
.end method

.method public setReminders(Lcom/google/api/services/calendar/model/Event$Reminders;)Lcom/google/api/services/calendar/model/Event;
    .locals 0

    iput-object p1, p0, Lcom/google/api/services/calendar/model/Event;->reminders:Lcom/google/api/services/calendar/model/Event$Reminders;

    return-object p0
.end method

.method public setResponseHeaders(Lcom/google/api/client/http/HttpHeaders;)V
    .locals 0

    iput-object p1, p0, Lcom/google/api/services/calendar/model/Event;->responseHeaders:Lcom/google/api/client/http/HttpHeaders;

    return-void
.end method

.method public setSequence(Ljava/lang/Integer;)Lcom/google/api/services/calendar/model/Event;
    .locals 0

    iput-object p1, p0, Lcom/google/api/services/calendar/model/Event;->sequence:Ljava/lang/Integer;

    return-object p0
.end method

.method public setStart(Lcom/google/api/services/calendar/model/EventDateTime;)Lcom/google/api/services/calendar/model/Event;
    .locals 0

    iput-object p1, p0, Lcom/google/api/services/calendar/model/Event;->start:Lcom/google/api/services/calendar/model/EventDateTime;

    return-object p0
.end method

.method public setStatus(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;
    .locals 0

    iput-object p1, p0, Lcom/google/api/services/calendar/model/Event;->status:Ljava/lang/String;

    return-object p0
.end method

.method public setSummary(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;
    .locals 0

    iput-object p1, p0, Lcom/google/api/services/calendar/model/Event;->summary:Ljava/lang/String;

    return-object p0
.end method

.method public setTransparency(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;
    .locals 0

    iput-object p1, p0, Lcom/google/api/services/calendar/model/Event;->transparency:Ljava/lang/String;

    return-object p0
.end method

.method public setVisibility(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;
    .locals 0

    iput-object p1, p0, Lcom/google/api/services/calendar/model/Event;->visibility:Ljava/lang/String;

    return-object p0
.end method
