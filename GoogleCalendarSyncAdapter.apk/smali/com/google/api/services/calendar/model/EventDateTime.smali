.class public final Lcom/google/api/services/calendar/model/EventDateTime;
.super Lcom/google/api/client/json/GenericJson;
.source "EventDateTime.java"


# instance fields
.field private date:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private dateTime:Lcom/google/api/client/util/DateTime;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private timeZone:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method


# virtual methods
.method public getDate()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/api/services/calendar/model/EventDateTime;->date:Ljava/lang/String;

    return-object v0
.end method

.method public getDateTime()Lcom/google/api/client/util/DateTime;
    .locals 1

    iget-object v0, p0, Lcom/google/api/services/calendar/model/EventDateTime;->dateTime:Lcom/google/api/client/util/DateTime;

    return-object v0
.end method

.method public getTimeZone()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/api/services/calendar/model/EventDateTime;->timeZone:Ljava/lang/String;

    return-object v0
.end method

.method public setDate(Ljava/lang/String;)Lcom/google/api/services/calendar/model/EventDateTime;
    .locals 0

    iput-object p1, p0, Lcom/google/api/services/calendar/model/EventDateTime;->date:Ljava/lang/String;

    return-object p0
.end method

.method public setDateTime(Lcom/google/api/client/util/DateTime;)Lcom/google/api/services/calendar/model/EventDateTime;
    .locals 0

    iput-object p1, p0, Lcom/google/api/services/calendar/model/EventDateTime;->dateTime:Lcom/google/api/client/util/DateTime;

    return-object p0
.end method

.method public setTimeZone(Ljava/lang/String;)Lcom/google/api/services/calendar/model/EventDateTime;
    .locals 0

    iput-object p1, p0, Lcom/google/api/services/calendar/model/EventDateTime;->timeZone:Ljava/lang/String;

    return-object p0
.end method
