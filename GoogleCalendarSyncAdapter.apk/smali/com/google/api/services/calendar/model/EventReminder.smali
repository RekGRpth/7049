.class public final Lcom/google/api/services/calendar/model/EventReminder;
.super Lcom/google/api/client/json/GenericJson;
.source "EventReminder.java"


# instance fields
.field private method:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private minutes:Ljava/lang/Integer;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method


# virtual methods
.method public getMethod()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/api/services/calendar/model/EventReminder;->method:Ljava/lang/String;

    return-object v0
.end method

.method public getMinutes()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/google/api/services/calendar/model/EventReminder;->minutes:Ljava/lang/Integer;

    return-object v0
.end method

.method public setMethod(Ljava/lang/String;)Lcom/google/api/services/calendar/model/EventReminder;
    .locals 0

    iput-object p1, p0, Lcom/google/api/services/calendar/model/EventReminder;->method:Ljava/lang/String;

    return-object p0
.end method

.method public setMinutes(Ljava/lang/Integer;)Lcom/google/api/services/calendar/model/EventReminder;
    .locals 0

    iput-object p1, p0, Lcom/google/api/services/calendar/model/EventReminder;->minutes:Ljava/lang/Integer;

    return-object p0
.end method
