.class public Lcom/google/android/syncadapters/contacts/SyncHighResPhotoIntentService;
.super Landroid/app/IntentService;
.source "SyncHighResPhotoIntentService.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "SyncHighResPhotoIntentService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 7
    .param p1    # Landroid/content/Intent;

    const-string v4, "ContactsSyncAdapter"

    const/4 v5, 0x2

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "ContactsSyncAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Received Intent: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/SyncHighResPhotoIntentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;

    move-result-object v2

    invoke-static {v0}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapterService;->getOrMakeContactsSyncAdapter(Landroid/content/Context;)Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;

    move-result-object v1

    invoke-virtual {v1, v3, v2}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->markPhotoForHighResSync(Landroid/net/Uri;Landroid/content/ContentProviderClient;)V

    return-void
.end method
