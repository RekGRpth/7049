.class public Lcom/google/wireless/gdata2/client/HttpException;
.super Lcom/google/wireless/gdata2/GDataException;
.source "HttpException.java"


# instance fields
.field private final responseStream:Ljava/io/InputStream;

.field private retryAfter:J

.field private final statusCode:I


# direct methods
.method public constructor <init>(Ljava/lang/String;ILjava/io/InputStream;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Ljava/io/InputStream;

    invoke-direct {p0, p1}, Lcom/google/wireless/gdata2/GDataException;-><init>(Ljava/lang/String;)V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/wireless/gdata2/client/HttpException;->retryAfter:J

    iput p2, p0, Lcom/google/wireless/gdata2/client/HttpException;->statusCode:I

    iput-object p3, p0, Lcom/google/wireless/gdata2/client/HttpException;->responseStream:Ljava/io/InputStream;

    return-void
.end method


# virtual methods
.method public getResponseStream()Ljava/io/InputStream;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/gdata2/client/HttpException;->responseStream:Ljava/io/InputStream;

    return-object v0
.end method

.method public getRetryAfter()J
    .locals 2

    iget-wide v0, p0, Lcom/google/wireless/gdata2/client/HttpException;->retryAfter:J

    return-wide v0
.end method

.method public getStatusCode()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/gdata2/client/HttpException;->statusCode:I

    return v0
.end method

.method public setRetryAfter(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/google/wireless/gdata2/client/HttpException;->retryAfter:J

    return-void
.end method
