.class public abstract Lcom/google/wireless/gdata2/contacts/data/ContactsElement;
.super Lcom/google/wireless/gdata2/contacts/data/TypedElement;
.source "ContactsElement.java"


# instance fields
.field private isPrimary:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/wireless/gdata2/contacts/data/TypedElement;-><init>()V

    return-void
.end method

.method public constructor <init>(BLjava/lang/String;Z)V
    .locals 0
    .param p1    # B
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/gdata2/contacts/data/TypedElement;-><init>(BLjava/lang/String;)V

    iput-boolean p3, p0, Lcom/google/wireless/gdata2/contacts/data/ContactsElement;->isPrimary:Z

    return-void
.end method


# virtual methods
.method public isPrimary()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/gdata2/contacts/data/ContactsElement;->isPrimary:Z

    return v0
.end method

.method public setIsPrimary(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/gdata2/contacts/data/ContactsElement;->isPrimary:Z

    return-void
.end method

.method public toString(Ljava/lang/StringBuffer;)V
    .locals 2
    .param p1    # Ljava/lang/StringBuffer;

    invoke-super {p0, p1}, Lcom/google/wireless/gdata2/contacts/data/TypedElement;->toString(Ljava/lang/StringBuffer;)V

    const-string v0, " isPrimary:"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/wireless/gdata2/contacts/data/ContactsElement;->isPrimary:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    return-void
.end method
