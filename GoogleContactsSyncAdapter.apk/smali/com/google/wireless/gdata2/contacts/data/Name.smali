.class public Lcom/google/wireless/gdata2/contacts/data/Name;
.super Ljava/lang/Object;
.source "Name.java"


# instance fields
.field private additionalName:Ljava/lang/String;

.field private additionalNameYomi:Ljava/lang/String;

.field private familyName:Ljava/lang/String;

.field private familyNameYomi:Ljava/lang/String;

.field private fullName:Ljava/lang/String;

.field private fullNameYomi:Ljava/lang/String;

.field private givenName:Ljava/lang/String;

.field private givenNameYomi:Ljava/lang/String;

.field private namePrefix:Ljava/lang/String;

.field private nameSuffix:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAdditionalName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/gdata2/contacts/data/Name;->additionalName:Ljava/lang/String;

    return-object v0
.end method

.method public getAdditionalNameYomi()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/gdata2/contacts/data/Name;->additionalNameYomi:Ljava/lang/String;

    return-object v0
.end method

.method public getFamilyName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/gdata2/contacts/data/Name;->familyName:Ljava/lang/String;

    return-object v0
.end method

.method public getFamilyNameYomi()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/gdata2/contacts/data/Name;->familyNameYomi:Ljava/lang/String;

    return-object v0
.end method

.method public getFullName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/gdata2/contacts/data/Name;->fullName:Ljava/lang/String;

    return-object v0
.end method

.method public getFullNameYomi()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/gdata2/contacts/data/Name;->fullNameYomi:Ljava/lang/String;

    return-object v0
.end method

.method public getGivenName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/gdata2/contacts/data/Name;->givenName:Ljava/lang/String;

    return-object v0
.end method

.method public getGivenNameYomi()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/gdata2/contacts/data/Name;->givenNameYomi:Ljava/lang/String;

    return-object v0
.end method

.method public getNamePrefix()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/gdata2/contacts/data/Name;->namePrefix:Ljava/lang/String;

    return-object v0
.end method

.method public getNameSuffix()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/gdata2/contacts/data/Name;->nameSuffix:Ljava/lang/String;

    return-object v0
.end method

.method public setAdditionalName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/gdata2/contacts/data/Name;->additionalName:Ljava/lang/String;

    return-void
.end method

.method public setAdditionalNameYomi(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/gdata2/contacts/data/Name;->additionalNameYomi:Ljava/lang/String;

    return-void
.end method

.method public setFamilyName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/gdata2/contacts/data/Name;->familyName:Ljava/lang/String;

    return-void
.end method

.method public setFamilyNameYomi(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/gdata2/contacts/data/Name;->familyNameYomi:Ljava/lang/String;

    return-void
.end method

.method public setFullName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/gdata2/contacts/data/Name;->fullName:Ljava/lang/String;

    return-void
.end method

.method public setFullNameYomi(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/gdata2/contacts/data/Name;->fullNameYomi:Ljava/lang/String;

    return-void
.end method

.method public setGivenName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/gdata2/contacts/data/Name;->givenName:Ljava/lang/String;

    return-void
.end method

.method public setGivenNameYomi(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/gdata2/contacts/data/Name;->givenNameYomi:Ljava/lang/String;

    return-void
.end method

.method public setNamePrefix(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/gdata2/contacts/data/Name;->namePrefix:Ljava/lang/String;

    return-void
.end method

.method public setNameSuffix(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/gdata2/contacts/data/Name;->nameSuffix:Ljava/lang/String;

    return-void
.end method

.method public toString(Ljava/lang/StringBuffer;)V
    .locals 2
    .param p1    # Ljava/lang/StringBuffer;

    const-string v0, "Name"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v0, p0, Lcom/google/wireless/gdata2/contacts/data/Name;->fullName:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, " fullName:"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/wireless/gdata2/contacts/data/Name;->fullName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/gdata2/contacts/data/Name;->nameSuffix:Ljava/lang/String;

    if-eqz v0, :cond_1

    const-string v0, " nameSuffix:"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/wireless/gdata2/contacts/data/Name;->nameSuffix:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_1
    iget-object v0, p0, Lcom/google/wireless/gdata2/contacts/data/Name;->namePrefix:Ljava/lang/String;

    if-eqz v0, :cond_2

    const-string v0, " namePrefix:"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/wireless/gdata2/contacts/data/Name;->namePrefix:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_2
    iget-object v0, p0, Lcom/google/wireless/gdata2/contacts/data/Name;->familyName:Ljava/lang/String;

    if-eqz v0, :cond_3

    const-string v0, " familyName:"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/wireless/gdata2/contacts/data/Name;->familyName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_3
    iget-object v0, p0, Lcom/google/wireless/gdata2/contacts/data/Name;->additionalName:Ljava/lang/String;

    if-eqz v0, :cond_4

    const-string v0, " additionalName:"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/wireless/gdata2/contacts/data/Name;->additionalName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_4
    iget-object v0, p0, Lcom/google/wireless/gdata2/contacts/data/Name;->givenName:Ljava/lang/String;

    if-eqz v0, :cond_5

    const-string v0, " givenName:"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/wireless/gdata2/contacts/data/Name;->givenName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_5
    iget-object v0, p0, Lcom/google/wireless/gdata2/contacts/data/Name;->givenNameYomi:Ljava/lang/String;

    if-eqz v0, :cond_6

    const-string v0, " givenNameYomi:"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/wireless/gdata2/contacts/data/Name;->givenNameYomi:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_6
    iget-object v0, p0, Lcom/google/wireless/gdata2/contacts/data/Name;->familyNameYomi:Ljava/lang/String;

    if-eqz v0, :cond_7

    const-string v0, " familyNameYomi:"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/wireless/gdata2/contacts/data/Name;->familyNameYomi:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_7
    iget-object v0, p0, Lcom/google/wireless/gdata2/contacts/data/Name;->additionalNameYomi:Ljava/lang/String;

    if-eqz v0, :cond_8

    const-string v0, " additionalNameYomi:"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/wireless/gdata2/contacts/data/Name;->additionalNameYomi:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_8
    iget-object v0, p0, Lcom/google/wireless/gdata2/contacts/data/Name;->fullNameYomi:Ljava/lang/String;

    if-eqz v0, :cond_9

    const-string v0, " fullNameYomi:"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/wireless/gdata2/contacts/data/Name;->fullNameYomi:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_9
    return-void
.end method
