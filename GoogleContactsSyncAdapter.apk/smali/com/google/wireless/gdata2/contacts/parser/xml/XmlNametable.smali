.class public final Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;
.super Ljava/lang/Object;
.source "XmlNametable.java"


# static fields
.field public static CODE:Ljava/lang/String;

.field public static ETAG:Ljava/lang/String;

.field public static GC_BILLINGINFO:Ljava/lang/String;

.field public static GC_BIRTHDAY:Ljava/lang/String;

.field public static GC_CALENDARLINK:Ljava/lang/String;

.field public static GC_DIRECTORYSERVER:Ljava/lang/String;

.field public static GC_EVENT:Ljava/lang/String;

.field public static GC_EXTERNALID:Ljava/lang/String;

.field public static GC_GENDER:Ljava/lang/String;

.field public static GC_GMI:Ljava/lang/String;

.field public static GC_HOBBY:Ljava/lang/String;

.field public static GC_INITIALS:Ljava/lang/String;

.field public static GC_JOT:Ljava/lang/String;

.field public static GC_LANGUAGE:Ljava/lang/String;

.field public static GC_LINKSTO:Ljava/lang/String;

.field public static GC_MAIDENNAME:Ljava/lang/String;

.field public static GC_MILEAGE:Ljava/lang/String;

.field public static GC_NICKNAME:Ljava/lang/String;

.field public static GC_OCCUPATION:Ljava/lang/String;

.field public static GC_PRIORITY:Ljava/lang/String;

.field public static GC_RELATION:Ljava/lang/String;

.field public static GC_SENSITIVITY:Ljava/lang/String;

.field public static GC_SHORTNAME:Ljava/lang/String;

.field public static GC_SIP:Ljava/lang/String;

.field public static GC_SUBJECT:Ljava/lang/String;

.field public static GC_UDF:Ljava/lang/String;

.field public static GC_WEBSITE:Ljava/lang/String;

.field public static GD_ADDRESS:Ljava/lang/String;

.field public static GD_DELETED:Ljava/lang/String;

.field public static GD_EMAIL:Ljava/lang/String;

.field public static GD_EMAIL_DISPLAYNAME:Ljava/lang/String;

.field public static GD_EXTENDEDPROPERTY:Ljava/lang/String;

.field public static GD_IM:Ljava/lang/String;

.field public static GD_NAME:Ljava/lang/String;

.field public static GD_NAME_ADDITIONALNAME:Ljava/lang/String;

.field public static GD_NAME_FAMILYNAME:Ljava/lang/String;

.field public static GD_NAME_FULLNAME:Ljava/lang/String;

.field public static GD_NAME_GIVENNAME:Ljava/lang/String;

.field public static GD_NAME_PREFIX:Ljava/lang/String;

.field public static GD_NAME_SUFFIX:Ljava/lang/String;

.field public static GD_NAME_YOMI:Ljava/lang/String;

.field public static GD_ORGANIZATION:Ljava/lang/String;

.field public static GD_ORG_DEPARTMENT:Ljava/lang/String;

.field public static GD_ORG_JOBDESC:Ljava/lang/String;

.field public static GD_ORG_NAME:Ljava/lang/String;

.field public static GD_ORG_SYMBOL:Ljava/lang/String;

.field public static GD_ORG_TITLE:Ljava/lang/String;

.field public static GD_PHONENUMBER:Ljava/lang/String;

.field public static GD_PROTOCOL:Ljava/lang/String;

.field public static GD_SPA:Ljava/lang/String;

.field public static GD_SPA_CITY:Ljava/lang/String;

.field public static GD_SPA_COUNTRY:Ljava/lang/String;

.field public static GD_SPA_FORMATTEDADDRESS:Ljava/lang/String;

.field public static GD_SPA_NEIGHBORHOOD:Ljava/lang/String;

.field public static GD_SPA_POBOX:Ljava/lang/String;

.field public static GD_SPA_POSTCODE:Ljava/lang/String;

.field public static GD_SPA_REGION:Ljava/lang/String;

.field public static GD_SPA_STREET:Ljava/lang/String;

.field public static GD_WHEN:Ljava/lang/String;

.field public static GD_WHERE:Ljava/lang/String;

.field public static HREF:Ljava/lang/String;

.field public static KEY:Ljava/lang/String;

.field public static LABEL:Ljava/lang/String;

.field public static PRIMARY:Ljava/lang/String;

.field public static REL:Ljava/lang/String;

.field public static STARTTIME:Ljava/lang/String;

.field public static VALUE:Ljava/lang/String;

.field public static VALUESTRING:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "href"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->HREF:Ljava/lang/String;

    const-string v0, "label"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->LABEL:Ljava/lang/String;

    const-string v0, "value"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->VALUE:Ljava/lang/String;

    const-string v0, "code"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->CODE:Ljava/lang/String;

    const-string v0, "rel"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->REL:Ljava/lang/String;

    const-string v0, "key"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->KEY:Ljava/lang/String;

    const-string v0, "etag"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->ETAG:Ljava/lang/String;

    const-string v0, "valueString"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->VALUESTRING:Ljava/lang/String;

    const-string v0, "startTime"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->STARTTIME:Ljava/lang/String;

    const-string v0, "primary"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->PRIMARY:Ljava/lang/String;

    const-string v0, "email"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GD_EMAIL:Ljava/lang/String;

    const-string v0, "displayName"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GD_EMAIL_DISPLAYNAME:Ljava/lang/String;

    const-string v0, "address"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GD_ADDRESS:Ljava/lang/String;

    const-string v0, "protocol"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GD_PROTOCOL:Ljava/lang/String;

    const-string v0, "im"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GD_IM:Ljava/lang/String;

    const-string v0, "deleted"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GD_DELETED:Ljava/lang/String;

    const-string v0, "name"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GD_NAME:Ljava/lang/String;

    const-string v0, "givenName"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GD_NAME_GIVENNAME:Ljava/lang/String;

    const-string v0, "additionalName"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GD_NAME_ADDITIONALNAME:Ljava/lang/String;

    const-string v0, "yomi"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GD_NAME_YOMI:Ljava/lang/String;

    const-string v0, "familyName"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GD_NAME_FAMILYNAME:Ljava/lang/String;

    const-string v0, "namePrefix"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GD_NAME_PREFIX:Ljava/lang/String;

    const-string v0, "nameSuffix"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GD_NAME_SUFFIX:Ljava/lang/String;

    const-string v0, "fullName"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GD_NAME_FULLNAME:Ljava/lang/String;

    const-string v0, "structuredPostalAddress"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GD_SPA:Ljava/lang/String;

    const-string v0, "street"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GD_SPA_STREET:Ljava/lang/String;

    const-string v0, "pobox"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GD_SPA_POBOX:Ljava/lang/String;

    const-string v0, "neighborhood"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GD_SPA_NEIGHBORHOOD:Ljava/lang/String;

    const-string v0, "city"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GD_SPA_CITY:Ljava/lang/String;

    const-string v0, "region"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GD_SPA_REGION:Ljava/lang/String;

    const-string v0, "postcode"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GD_SPA_POSTCODE:Ljava/lang/String;

    const-string v0, "country"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GD_SPA_COUNTRY:Ljava/lang/String;

    const-string v0, "formattedAddress"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GD_SPA_FORMATTEDADDRESS:Ljava/lang/String;

    const-string v0, "phoneNumber"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GD_PHONENUMBER:Ljava/lang/String;

    const-string v0, "organization"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GD_ORGANIZATION:Ljava/lang/String;

    const-string v0, "extendedProperty"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GD_EXTENDEDPROPERTY:Ljava/lang/String;

    const-string v0, "when"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GD_WHEN:Ljava/lang/String;

    const-string v0, "where"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GD_WHERE:Ljava/lang/String;

    const-string v0, "orgName"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GD_ORG_NAME:Ljava/lang/String;

    const-string v0, "orgTitle"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GD_ORG_TITLE:Ljava/lang/String;

    const-string v0, "orgDepartment"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GD_ORG_DEPARTMENT:Ljava/lang/String;

    const-string v0, "orgJobDescription"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GD_ORG_JOBDESC:Ljava/lang/String;

    const-string v0, "orgSymbol"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GD_ORG_SYMBOL:Ljava/lang/String;

    const-string v0, "groupMembershipInfo"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GC_GMI:Ljava/lang/String;

    const-string v0, "birthday"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GC_BIRTHDAY:Ljava/lang/String;

    const-string v0, "billingInformation"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GC_BILLINGINFO:Ljava/lang/String;

    const-string v0, "calendarLink"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GC_CALENDARLINK:Ljava/lang/String;

    const-string v0, "directoryServer"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GC_DIRECTORYSERVER:Ljava/lang/String;

    const-string v0, "event"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GC_EVENT:Ljava/lang/String;

    const-string v0, "externalId"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GC_EXTERNALID:Ljava/lang/String;

    const-string v0, "gender"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GC_GENDER:Ljava/lang/String;

    const-string v0, "hobby"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GC_HOBBY:Ljava/lang/String;

    const-string v0, "initials"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GC_INITIALS:Ljava/lang/String;

    const-string v0, "jot"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GC_JOT:Ljava/lang/String;

    const-string v0, "language"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GC_LANGUAGE:Ljava/lang/String;

    const-string v0, "maidenName"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GC_MAIDENNAME:Ljava/lang/String;

    const-string v0, "mileage"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GC_MILEAGE:Ljava/lang/String;

    const-string v0, "nickname"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GC_NICKNAME:Ljava/lang/String;

    const-string v0, "occupation"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GC_OCCUPATION:Ljava/lang/String;

    const-string v0, "priority"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GC_PRIORITY:Ljava/lang/String;

    const-string v0, "relation"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GC_RELATION:Ljava/lang/String;

    const-string v0, "sensitivity"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GC_SENSITIVITY:Ljava/lang/String;

    const-string v0, "shortName"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GC_SHORTNAME:Ljava/lang/String;

    const-string v0, "sip"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GC_SIP:Ljava/lang/String;

    const-string v0, "subject"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GC_SUBJECT:Ljava/lang/String;

    const-string v0, "userDefinedField"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GC_UDF:Ljava/lang/String;

    const-string v0, "website"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GC_WEBSITE:Ljava/lang/String;

    const-string v0, "linksto"

    sput-object v0, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlNametable;->GC_LINKSTO:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
