.class public Lcom/google/wireless/gdata2/data/Entry;
.super Ljava/lang/Object;
.source "Entry.java"


# instance fields
.field private author:Ljava/lang/String;

.field private batchInfo:Lcom/google/wireless/gdata2/data/batch/BatchInfo;

.field private category:Ljava/lang/String;

.field private categoryScheme:Ljava/lang/String;

.field private content:Ljava/lang/String;

.field private contentSource:Ljava/lang/String;

.field private contentType:Ljava/lang/String;

.field private deleted:Z

.field private eTagValue:Ljava/lang/String;

.field private editUri:Ljava/lang/String;

.field private email:Ljava/lang/String;

.field private fields:Ljava/lang/String;

.field private htmlUri:Ljava/lang/String;

.field private id:Ljava/lang/String;

.field private publicationDate:Ljava/lang/String;

.field private summary:Ljava/lang/String;

.field private title:Ljava/lang/String;

.field private updateDate:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->id:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->title:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->editUri:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->htmlUri:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->summary:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->content:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->author:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->email:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->category:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->categoryScheme:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->publicationDate:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->updateDate:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->eTagValue:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/wireless/gdata2/data/Entry;->deleted:Z

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->batchInfo:Lcom/google/wireless/gdata2/data/batch/BatchInfo;

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->fields:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->contentSource:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->contentType:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/wireless/gdata2/data/Entry;)V
    .locals 2
    .param p1    # Lcom/google/wireless/gdata2/data/Entry;

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->id:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->title:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->editUri:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->htmlUri:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->summary:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->content:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->author:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->email:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->category:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->categoryScheme:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->publicationDate:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->updateDate:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->eTagValue:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/wireless/gdata2/data/Entry;->deleted:Z

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->batchInfo:Lcom/google/wireless/gdata2/data/batch/BatchInfo;

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->fields:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->contentSource:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->contentType:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/wireless/gdata2/data/Entry;->id:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/wireless/gdata2/data/Entry;->id:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/wireless/gdata2/data/Entry;->title:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/wireless/gdata2/data/Entry;->title:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/wireless/gdata2/data/Entry;->editUri:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/wireless/gdata2/data/Entry;->editUri:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/wireless/gdata2/data/Entry;->htmlUri:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/wireless/gdata2/data/Entry;->htmlUri:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/wireless/gdata2/data/Entry;->summary:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/wireless/gdata2/data/Entry;->summary:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/wireless/gdata2/data/Entry;->content:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/wireless/gdata2/data/Entry;->content:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/wireless/gdata2/data/Entry;->author:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/wireless/gdata2/data/Entry;->author:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/wireless/gdata2/data/Entry;->email:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/wireless/gdata2/data/Entry;->email:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/wireless/gdata2/data/Entry;->category:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/wireless/gdata2/data/Entry;->category:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/wireless/gdata2/data/Entry;->categoryScheme:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/wireless/gdata2/data/Entry;->categoryScheme:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/wireless/gdata2/data/Entry;->publicationDate:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/wireless/gdata2/data/Entry;->publicationDate:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/wireless/gdata2/data/Entry;->updateDate:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/wireless/gdata2/data/Entry;->updateDate:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/wireless/gdata2/data/Entry;->eTagValue:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/wireless/gdata2/data/Entry;->eTagValue:Ljava/lang/String;

    iget-boolean v0, p1, Lcom/google/wireless/gdata2/data/Entry;->deleted:Z

    iput-boolean v0, p0, Lcom/google/wireless/gdata2/data/Entry;->deleted:Z

    iget-object v0, p1, Lcom/google/wireless/gdata2/data/Entry;->fields:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/wireless/gdata2/data/Entry;->fields:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/wireless/gdata2/data/Entry;->contentSource:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/wireless/gdata2/data/Entry;->contentSource:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/wireless/gdata2/data/Entry;->contentType:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/wireless/gdata2/data/Entry;->contentType:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected appendIfNotNull(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/StringBuffer;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    invoke-static {p3}, Lcom/google/wireless/gdata2/data/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v0, ": "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {p1, p3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_0
    return-void
.end method

.method public clear()V
    .locals 2

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->id:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->title:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->editUri:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->htmlUri:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->summary:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->content:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->contentType:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->contentSource:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->author:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->email:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->category:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->categoryScheme:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->publicationDate:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->updateDate:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/wireless/gdata2/data/Entry;->deleted:Z

    iput-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->batchInfo:Lcom/google/wireless/gdata2/data/batch/BatchInfo;

    return-void
.end method

.method public getAuthor()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/gdata2/data/Entry;->author:Ljava/lang/String;

    return-object v0
.end method

.method public getBatchInfo()Lcom/google/wireless/gdata2/data/batch/BatchInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/gdata2/data/Entry;->batchInfo:Lcom/google/wireless/gdata2/data/batch/BatchInfo;

    return-object v0
.end method

.method public getCategory()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/gdata2/data/Entry;->category:Ljava/lang/String;

    return-object v0
.end method

.method public getCategoryScheme()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/gdata2/data/Entry;->categoryScheme:Ljava/lang/String;

    return-object v0
.end method

.method public getContent()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/gdata2/data/Entry;->content:Ljava/lang/String;

    return-object v0
.end method

.method public getETag()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/gdata2/data/Entry;->eTagValue:Ljava/lang/String;

    return-object v0
.end method

.method public getEditUri()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/gdata2/data/Entry;->editUri:Ljava/lang/String;

    return-object v0
.end method

.method public getEmail()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/gdata2/data/Entry;->email:Ljava/lang/String;

    return-object v0
.end method

.method public getFields()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/gdata2/data/Entry;->fields:Ljava/lang/String;

    return-object v0
.end method

.method public getHtmlUri()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/gdata2/data/Entry;->htmlUri:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/gdata2/data/Entry;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getPublicationDate()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/gdata2/data/Entry;->publicationDate:Ljava/lang/String;

    return-object v0
.end method

.method public getSummary()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/gdata2/data/Entry;->summary:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/gdata2/data/Entry;->title:Ljava/lang/String;

    return-object v0
.end method

.method public getUpdateDate()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/gdata2/data/Entry;->updateDate:Ljava/lang/String;

    return-object v0
.end method

.method public isDeleted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/gdata2/data/Entry;->deleted:Z

    return v0
.end method

.method public setAuthor(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/gdata2/data/Entry;->author:Ljava/lang/String;

    return-void
.end method

.method public setBatchInfo(Lcom/google/wireless/gdata2/data/batch/BatchInfo;)V
    .locals 0
    .param p1    # Lcom/google/wireless/gdata2/data/batch/BatchInfo;

    iput-object p1, p0, Lcom/google/wireless/gdata2/data/Entry;->batchInfo:Lcom/google/wireless/gdata2/data/batch/BatchInfo;

    return-void
.end method

.method public setCategory(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/gdata2/data/Entry;->category:Ljava/lang/String;

    return-void
.end method

.method public setCategoryScheme(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/gdata2/data/Entry;->categoryScheme:Ljava/lang/String;

    return-void
.end method

.method public setContent(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/gdata2/data/Entry;->content:Ljava/lang/String;

    return-void
.end method

.method public setContentSource(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/gdata2/data/Entry;->contentSource:Ljava/lang/String;

    return-void
.end method

.method public setContentType(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/gdata2/data/Entry;->contentType:Ljava/lang/String;

    return-void
.end method

.method public setDeleted(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/gdata2/data/Entry;->deleted:Z

    return-void
.end method

.method public setETag(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/gdata2/data/Entry;->eTagValue:Ljava/lang/String;

    return-void
.end method

.method public setEditUri(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/gdata2/data/Entry;->editUri:Ljava/lang/String;

    return-void
.end method

.method public setEmail(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/gdata2/data/Entry;->email:Ljava/lang/String;

    return-void
.end method

.method public setFields(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/gdata2/data/Entry;->fields:Ljava/lang/String;

    return-void
.end method

.method public setHtmlUri(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/gdata2/data/Entry;->htmlUri:Ljava/lang/String;

    return-void
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/gdata2/data/Entry;->id:Ljava/lang/String;

    return-void
.end method

.method public setPublicationDate(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/gdata2/data/Entry;->publicationDate:Ljava/lang/String;

    return-void
.end method

.method public setSummary(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/gdata2/data/Entry;->summary:Ljava/lang/String;

    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/gdata2/data/Entry;->title:Ljava/lang/String;

    return-void
.end method

.method public setUpdateDate(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/gdata2/data/Entry;->updateDate:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {p0, v0}, Lcom/google/wireless/gdata2/data/Entry;->toString(Ljava/lang/StringBuffer;)V

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected toString(Ljava/lang/StringBuffer;)V
    .locals 2
    .param p1    # Ljava/lang/StringBuffer;

    const-string v0, "ID"

    iget-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->id:Ljava/lang/String;

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/wireless/gdata2/data/Entry;->appendIfNotNull(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "TITLE"

    iget-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->title:Ljava/lang/String;

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/wireless/gdata2/data/Entry;->appendIfNotNull(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "EDIT URI"

    iget-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->editUri:Ljava/lang/String;

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/wireless/gdata2/data/Entry;->appendIfNotNull(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "HTML URI"

    iget-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->htmlUri:Ljava/lang/String;

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/wireless/gdata2/data/Entry;->appendIfNotNull(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "SUMMARY"

    iget-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->summary:Ljava/lang/String;

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/wireless/gdata2/data/Entry;->appendIfNotNull(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "CONTENT"

    iget-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->content:Ljava/lang/String;

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/wireless/gdata2/data/Entry;->appendIfNotNull(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "AUTHOR"

    iget-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->author:Ljava/lang/String;

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/wireless/gdata2/data/Entry;->appendIfNotNull(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "CATEGORY"

    iget-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->category:Ljava/lang/String;

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/wireless/gdata2/data/Entry;->appendIfNotNull(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "CATEGORY SCHEME"

    iget-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->categoryScheme:Ljava/lang/String;

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/wireless/gdata2/data/Entry;->appendIfNotNull(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "PUBLICATION DATE"

    iget-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->publicationDate:Ljava/lang/String;

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/wireless/gdata2/data/Entry;->appendIfNotNull(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "UPDATE DATE"

    iget-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->updateDate:Ljava/lang/String;

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/wireless/gdata2/data/Entry;->appendIfNotNull(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "DELETED"

    iget-boolean v1, p0, Lcom/google/wireless/gdata2/data/Entry;->deleted:Z

    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/wireless/gdata2/data/Entry;->appendIfNotNull(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "ETAG"

    iget-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->eTagValue:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/wireless/gdata2/data/Entry;->appendIfNotNull(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/wireless/gdata2/data/Entry;->batchInfo:Lcom/google/wireless/gdata2/data/batch/BatchInfo;

    if-eqz v0, :cond_0

    const-string v0, "BATCH"

    iget-object v1, p0, Lcom/google/wireless/gdata2/data/Entry;->batchInfo:Lcom/google/wireless/gdata2/data/batch/BatchInfo;

    invoke-virtual {v1}, Lcom/google/wireless/gdata2/data/batch/BatchInfo;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/wireless/gdata2/data/Entry;->appendIfNotNull(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public validate()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/wireless/gdata2/parser/ParseException;
        }
    .end annotation

    return-void
.end method
