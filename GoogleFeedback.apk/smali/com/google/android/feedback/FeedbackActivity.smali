.class public Lcom/google/android/feedback/FeedbackActivity;
.super Landroid/app/Activity;
.source "FeedbackActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/feedback/SystemLogFetcher$Listener;


# static fields
.field private static final mHandler:Landroid/os/Handler;

.field private static mSessionStack:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/google/android/feedback/FeedbackSession;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mIsUifRequest:Z

.field private mStarted:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    sput-object v0, Lcom/google/android/feedback/FeedbackActivity;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-boolean v0, p0, Lcom/google/android/feedback/FeedbackActivity;->mIsUifRequest:Z

    iput-boolean v0, p0, Lcom/google/android/feedback/FeedbackActivity;->mStarted:Z

    return-void
.end method

.method private configureOptionalUiFields(Landroid/os/Parcelable;)V
    .locals 5
    .param p1    # Landroid/os/Parcelable;

    const v4, 0x7f08000f

    const/4 v3, 0x0

    const v2, 0x7f08000d

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/android/feedback/FeedbackActivity;->isUifReport()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, v4}, Lcom/google/android/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    if-nez p1, :cond_0

    invoke-virtual {p0, v2}, Lcom/google/android/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v2}, Lcom/google/android/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v4}, Lcom/google/android/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v2}, Lcom/google/android/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private createFeedbackSession(Landroid/content/Intent;Landroid/os/Parcelable;Landroid/app/ApplicationErrorReport;)V
    .locals 7
    .param p1    # Landroid/content/Intent;
    .param p2    # Landroid/os/Parcelable;
    .param p3    # Landroid/app/ApplicationErrorReport;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/feedback/FeedbackActivity;->isUifReport()Z

    move-result v4

    if-eqz v4, :cond_1

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/feedback/FeedbackActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    iget-object v5, p3, Landroid/app/ApplicationErrorReport;->packageName:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    if-eqz p2, :cond_0

    move-object v0, p2

    check-cast v0, Lcom/google/android/feedback/FeedbackSession$Screenshot;

    move-object v3, v0

    :cond_0
    iget-object v4, v1, Landroid/content/pm/ApplicationInfo;->processName:Ljava/lang/String;

    iput-object v4, p3, Landroid/app/ApplicationErrorReport;->processName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    invoke-direct {p0, v3}, Lcom/google/android/feedback/FeedbackActivity;->configureOptionalUiFields(Landroid/os/Parcelable;)V

    invoke-direct {p0, p3, v3}, Lcom/google/android/feedback/FeedbackActivity;->pushSession(Landroid/app/ApplicationErrorReport;Lcom/google/android/feedback/FeedbackSession$Screenshot;)V

    :cond_2
    :goto_0
    return-void

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    sget-object v4, Lcom/google/android/feedback/FeedbackActivity;->mSessionStack:Ljava/util/Stack;

    invoke-virtual {v4}, Ljava/util/Stack;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {p0}, Lcom/google/android/feedback/FeedbackActivity;->finish()V

    goto :goto_0
.end method

.method public static getReport()Lcom/google/android/feedback/ExtendedErrorReport;
    .locals 1

    sget-object v0, Lcom/google/android/feedback/FeedbackActivity;->mSessionStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/feedback/FeedbackActivity;->mSessionStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/feedback/FeedbackSession;

    invoke-virtual {v0}, Lcom/google/android/feedback/FeedbackSession;->getReport()Lcom/google/android/feedback/ExtendedErrorReport;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private initContentView(Landroid/content/Intent;Landroid/os/Parcelable;)V
    .locals 4
    .param p1    # Landroid/content/Intent;
    .param p2    # Landroid/os/Parcelable;

    const v2, 0x7f030002

    invoke-virtual {p0, v2}, Lcom/google/android/feedback/FeedbackActivity;->setContentView(I)V

    const v2, 0x7f080002

    invoke-virtual {p0, v2}, Lcom/google/android/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f080003

    invoke-virtual {p0, v2}, Lcom/google/android/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/high16 v2, 0x7f080000

    invoke-virtual {p0, v2}, Lcom/google/android/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f080009

    invoke-virtual {p0, v2}, Lcom/google/android/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f08000b

    invoke-virtual {p0, v2}, Lcom/google/android/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f08000d

    invoke-virtual {p0, v2}, Lcom/google/android/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f080001

    invoke-virtual {p0, v2}, Lcom/google/android/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0, p2}, Lcom/google/android/feedback/FeedbackActivity;->configureOptionalUiFields(Landroid/os/Parcelable;)V

    return-void
.end method

.method public static isCurrentSession(Lcom/google/android/feedback/FeedbackSession;)Z
    .locals 2
    .param p0    # Lcom/google/android/feedback/FeedbackSession;

    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/feedback/FeedbackActivity;->mSessionStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    sget-object v1, Lcom/google/android/feedback/FeedbackActivity;->mSessionStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    if-ne v1, p0, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private pushSession(Landroid/app/ApplicationErrorReport;Lcom/google/android/feedback/FeedbackSession$Screenshot;)V
    .locals 3
    .param p1    # Landroid/app/ApplicationErrorReport;
    .param p2    # Lcom/google/android/feedback/FeedbackSession$Screenshot;

    sget-object v2, Lcom/google/android/feedback/FeedbackActivity;->mSessionStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/google/android/feedback/FeedbackActivity;->mSessionStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/feedback/FeedbackSession;

    iget-boolean v2, p0, Lcom/google/android/feedback/FeedbackActivity;->mStarted:Z

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/android/feedback/FeedbackSession;->onStop()V

    :cond_0
    if-nez p2, :cond_2

    new-instance v0, Lcom/google/android/feedback/FeedbackSession;

    invoke-direct {v0, p0, p1}, Lcom/google/android/feedback/FeedbackSession;-><init>(Lcom/google/android/feedback/FeedbackActivity;Landroid/app/ApplicationErrorReport;)V

    :goto_0
    sget-object v2, Lcom/google/android/feedback/FeedbackActivity;->mSessionStack:Ljava/util/Stack;

    invoke-virtual {v2, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    iget-boolean v2, p0, Lcom/google/android/feedback/FeedbackActivity;->mStarted:Z

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/google/android/feedback/FeedbackSession;->onStart()V

    :cond_1
    return-void

    :cond_2
    new-instance v0, Lcom/google/android/feedback/FeedbackSession;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/feedback/FeedbackSession;-><init>(Lcom/google/android/feedback/FeedbackActivity;Landroid/app/ApplicationErrorReport;Lcom/google/android/feedback/FeedbackSession$Screenshot;)V

    goto :goto_0
.end method

.method private showPrivacyPolicy()V
    .locals 5

    new-instance v0, Landroid/text/SpannableString;

    const v2, 0x7f06001a

    invoke-virtual {p0, v2}, Lcom/google/android/feedback/FeedbackActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    const/4 v2, 0x1

    invoke-static {v0, v2}, Landroid/text/util/Linkify;->addLinks(Landroid/text/Spannable;I)Z

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f060019

    invoke-virtual {p0, v3}, Lcom/google/android/feedback/FeedbackActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    const/4 v2, -0x1

    const-string v3, "OK"

    new-instance v4, Lcom/google/android/feedback/FeedbackActivity$1;

    invoke-direct {v4, p0}, Lcom/google/android/feedback/FeedbackActivity$1;-><init>(Lcom/google/android/feedback/FeedbackActivity;)V

    invoke-virtual {v1, v2, v3, v4}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    const v2, 0x102000b

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    return-void
.end method

.method private showTosOnFirstRun()V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/google/android/feedback/FeedbackActivity;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "tos_accepted"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/feedback/ShowTosActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0, v2}, Lcom/google/android/feedback/FeedbackActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_0
    return-void
.end method


# virtual methods
.method public isUifReport()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/feedback/FeedbackActivity;->mIsUifRequest:Z

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v2, 0x1

    if-ne p2, v2, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/feedback/FeedbackActivity;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "tos_accepted"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :goto_0
    return-void

    :cond_0
    const/4 v0, -0x1

    invoke-virtual {p0, v0, p3}, Lcom/google/android/feedback/FeedbackActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/feedback/FeedbackActivity;->finish()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    sget-object v1, Lcom/google/android/feedback/FeedbackActivity;->mSessionStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/google/android/feedback/FeedbackActivity;->mSessionStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/feedback/FeedbackSession;

    invoke-virtual {v1, p1}, Lcom/google/android/feedback/FeedbackSession;->onClick(Landroid/view/View;)V

    :cond_0
    :goto_0
    return-void

    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/feedback/FeedbackActivity;->showPrivacyPolicy()V

    goto :goto_0

    :pswitch_2
    const v1, 0x7f08000c

    invoke-virtual {p0, v1}, Lcom/google/android/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Checkable;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/widget/Checkable;->toggle()V

    goto :goto_0

    :pswitch_3
    const v1, 0x7f08000e

    invoke-virtual {p0, v1}, Lcom/google/android/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Checkable;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/widget/Checkable;->toggle()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f080009
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    new-instance v3, Ljava/util/Stack;

    invoke-direct {v3}, Ljava/util/Stack;-><init>()V

    sput-object v3, Lcom/google/android/feedback/FeedbackActivity;->mSessionStack:Ljava/util/Stack;

    invoke-static {p0}, Lcom/google/android/feedback/SystemLogFetcher;->setListener(Lcom/google/android/feedback/SystemLogFetcher$Listener;)V

    invoke-virtual {p0}, Lcom/google/android/feedback/FeedbackActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "com.android.feedback.SCREENSHOT_EXTRA"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    const-string v3, "android.intent.extra.BUG_REPORT"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/app/ApplicationErrorReport;

    iget v3, v2, Landroid/app/ApplicationErrorReport;->type:I

    const/16 v4, 0xb

    if-ne v3, v4, :cond_1

    const/4 v3, 0x1

    :goto_0
    iput-boolean v3, p0, Lcom/google/android/feedback/FeedbackActivity;->mIsUifRequest:Z

    invoke-direct {p0, v0, v1}, Lcom/google/android/feedback/FeedbackActivity;->initContentView(Landroid/content/Intent;Landroid/os/Parcelable;)V

    invoke-direct {p0}, Lcom/google/android/feedback/FeedbackActivity;->showTosOnFirstRun()V

    if-nez p1, :cond_0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/feedback/FeedbackActivity;->createFeedbackSession(Landroid/content/Intent;Landroid/os/Parcelable;Landroid/app/ApplicationErrorReport;)V

    :cond_0
    return-void

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/feedback/SystemLogFetcher;->setListener(Lcom/google/android/feedback/SystemLogFetcher$Listener;)V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onGotSystemLog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    sget-object v2, Lcom/google/android/feedback/FeedbackActivity;->mSessionStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/feedback/FeedbackSession;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/feedback/FeedbackSession;->onGotSystemLog(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/feedback/FeedbackActivity;->popSession()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Intent;

    const-string v1, "android.intent.extra.BUG_REPORT"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/ApplicationErrorReport;

    iget v1, v0, Landroid/app/ApplicationErrorReport;->type:I

    const/16 v2, 0xb

    if-ne v1, v2, :cond_1

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/feedback/FeedbackActivity;->mIsUifRequest:Z

    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    iget-boolean v1, p0, Lcom/google/android/feedback/FeedbackActivity;->mStarted:Z

    if-nez v1, :cond_0

    const-string v1, "com.android.feedback.SCREENSHOT_EXTRA"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-direct {p0, p1, v1, v0}, Lcom/google/android/feedback/FeedbackActivity;->createFeedbackSession(Landroid/content/Intent;Landroid/os/Parcelable;Landroid/app/ApplicationErrorReport;)V

    :cond_0
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    const/4 v0, 0x0

    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "feedback.SESSION_BUNDLE"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-boolean v3, p0, Lcom/google/android/feedback/FeedbackActivity;->mStarted:Z

    if-eqz v3, :cond_0

    sget-object v3, Lcom/google/android/feedback/FeedbackActivity;->mSessionStack:Ljava/util/Stack;

    invoke-virtual {v3}, Ljava/util/Stack;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lcom/google/android/feedback/FeedbackActivity;->mSessionStack:Ljava/util/Stack;

    invoke-virtual {v3}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/feedback/FeedbackSession;

    invoke-virtual {v3}, Lcom/google/android/feedback/FeedbackSession;->onStart()V

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    sget-object v3, Lcom/google/android/feedback/FeedbackActivity;->mSessionStack:Ljava/util/Stack;

    new-instance v4, Lcom/google/android/feedback/FeedbackSession;

    invoke-direct {v4, p0, v2}, Lcom/google/android/feedback/FeedbackSession;-><init>(Lcom/google/android/feedback/FeedbackActivity;Landroid/os/Bundle;)V

    invoke-virtual {v3, v4}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const/4 v0, 0x0

    sget-object v5, Lcom/google/android/feedback/FeedbackActivity;->mSessionStack:Ljava/util/Stack;

    invoke-virtual {v5}, Ljava/util/Stack;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/feedback/FeedbackSession;

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v3, v4}, Lcom/google/android/feedback/FeedbackSession;->onSaveInstanceState(Landroid/os/Bundle;)V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "feedback.SESSION_BUNDLE"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2, v4}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected onStart()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/feedback/FeedbackActivity;->mStarted:Z

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    sget-object v0, Lcom/google/android/feedback/FeedbackActivity;->mSessionStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/feedback/FeedbackActivity;->mSessionStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/feedback/FeedbackSession;

    invoke-virtual {v0}, Lcom/google/android/feedback/FeedbackSession;->onStart()V

    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/feedback/FeedbackActivity;->mStarted:Z

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    sget-object v0, Lcom/google/android/feedback/FeedbackActivity;->mSessionStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/feedback/FeedbackActivity;->mSessionStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/feedback/FeedbackSession;

    invoke-virtual {v0}, Lcom/google/android/feedback/FeedbackSession;->onStop()V

    :cond_0
    return-void
.end method

.method public popSession()V
    .locals 3

    sget-object v2, Lcom/google/android/feedback/FeedbackActivity;->mSessionStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/google/android/feedback/FeedbackActivity;->mSessionStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/feedback/FeedbackSession;

    iget-boolean v2, p0, Lcom/google/android/feedback/FeedbackActivity;->mStarted:Z

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/google/android/feedback/FeedbackSession;->onStop()V

    :cond_0
    sget-object v2, Lcom/google/android/feedback/FeedbackActivity;->mSessionStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/feedback/FeedbackActivity;->finish()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    sget-object v2, Lcom/google/android/feedback/FeedbackActivity;->mSessionStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/feedback/FeedbackSession;

    iget-boolean v2, p0, Lcom/google/android/feedback/FeedbackActivity;->mStarted:Z

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/google/android/feedback/FeedbackSession;->onStart()V

    goto :goto_0
.end method

.method public runSendService()V
    .locals 2

    sget-object v0, Lcom/google/android/feedback/FeedbackActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/feedback/FeedbackActivity$2;

    invoke-direct {v1, p0}, Lcom/google/android/feedback/FeedbackActivity$2;-><init>(Lcom/google/android/feedback/FeedbackActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method sendErrorReport(Lcom/google/android/feedback/ExtendedErrorReport;)V
    .locals 1
    .param p1    # Lcom/google/android/feedback/ExtendedErrorReport;

    new-instance v0, Lcom/google/android/feedback/SaveReportThread;

    invoke-direct {v0, p0, p1}, Lcom/google/android/feedback/SaveReportThread;-><init>(Lcom/google/android/feedback/FeedbackActivity;Lcom/google/android/feedback/ExtendedErrorReport;)V

    invoke-virtual {v0}, Lcom/google/android/feedback/SaveReportThread;->start()V

    return-void
.end method

.method public setProgressBarVisible(Z)V
    .locals 6
    .param p1    # Z

    const/4 v5, 0x4

    const/4 v4, 0x0

    const v3, 0x7f080006

    invoke-virtual {p0, v3}, Lcom/google/android/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    const v3, 0x7f080011

    invoke-virtual {p0, v3}, Lcom/google/android/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v3, 0x7f080007

    invoke-virtual {p0, v3}, Lcom/google/android/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz p1, :cond_0

    move v3, v4

    :goto_0
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    move v3, v5

    goto :goto_0

    :cond_1
    move v5, v4

    goto :goto_1
.end method

.method public showToast(I)V
    .locals 2
    .param p1    # I

    sget-object v0, Lcom/google/android/feedback/FeedbackActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/feedback/FeedbackActivity$3;

    invoke-direct {v1, p0, p1}, Lcom/google/android/feedback/FeedbackActivity$3;-><init>(Lcom/google/android/feedback/FeedbackActivity;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
