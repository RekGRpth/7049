.class public Lcom/google/android/feedback/FeedbackSession$Screenshot;
.super Ljava/lang/Object;
.source "FeedbackSession.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/feedback/FeedbackSession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "Screenshot"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/feedback/FeedbackSession$Screenshot;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public base64EncodedScreenshot:Ljava/lang/String;

.field public height:I

.field public width:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/feedback/FeedbackSession$Screenshot$1;

    invoke-direct {v0}, Lcom/google/android/feedback/FeedbackSession$Screenshot$1;-><init>()V

    sput-object v0, Lcom/google/android/feedback/FeedbackSession$Screenshot;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static from(Landroid/graphics/Bitmap;)Lcom/google/android/feedback/FeedbackSession$Screenshot;
    .locals 4
    .param p0    # Landroid/graphics/Bitmap;

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x46

    invoke-virtual {p0, v1, v2, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/google/android/feedback/FeedbackSession$Screenshot;->from([BII)Lcom/google/android/feedback/FeedbackSession$Screenshot;

    move-result-object v1

    return-object v1
.end method

.method public static from([BII)Lcom/google/android/feedback/FeedbackSession$Screenshot;
    .locals 2
    .param p0    # [B
    .param p1    # I
    .param p2    # I

    new-instance v0, Lcom/google/android/feedback/FeedbackSession$Screenshot;

    invoke-direct {v0}, Lcom/google/android/feedback/FeedbackSession$Screenshot;-><init>()V

    iput p1, v0, Lcom/google/android/feedback/FeedbackSession$Screenshot;->width:I

    iput p2, v0, Lcom/google/android/feedback/FeedbackSession$Screenshot;->height:I

    const/4 v1, 0x0

    invoke-static {p0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/feedback/FeedbackSession$Screenshot;->base64EncodedScreenshot:Ljava/lang/String;

    return-object v0
.end method

.method public static readFromParcel(Landroid/os/Parcel;)Lcom/google/android/feedback/FeedbackSession$Screenshot;
    .locals 4
    .param p0    # Landroid/os/Parcel;

    const/4 v3, 0x0

    new-instance v1, Lcom/google/android/feedback/FeedbackSession$Screenshot;

    invoke-direct {v1}, Lcom/google/android/feedback/FeedbackSession$Screenshot;-><init>()V

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/feedback/FeedbackSession$Screenshot;->base64EncodedScreenshot:Ljava/lang/String;

    :try_start_0
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, v1, Lcom/google/android/feedback/FeedbackSession$Screenshot;->width:I

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, v1, Lcom/google/android/feedback/FeedbackSession$Screenshot;->height:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    iput v3, v1, Lcom/google/android/feedback/FeedbackSession$Screenshot;->width:I

    iput v3, v1, Lcom/google/android/feedback/FeedbackSession$Screenshot;->height:I

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/feedback/FeedbackSession$Screenshot;->base64EncodedScreenshot:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/feedback/FeedbackSession$Screenshot;->width:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/google/android/feedback/FeedbackSession$Screenshot;->height:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
