.class public Lcom/google/android/feedback/FeedbackSession;
.super Ljava/lang/Object;
.source "FeedbackSession.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/feedback/FeedbackSession$Screenshot;
    }
.end annotation


# static fields
.field private static final EMPTY_STRING_LIST:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final mHandler:Landroid/os/Handler;


# instance fields
.field private mAnrStackTraces:Ljava/lang/String;

.field private mAppIcon:Landroid/graphics/drawable/Drawable;

.field private mAppLabel:Ljava/lang/String;

.field private mChooseAccount:Landroid/widget/Spinner;

.field private mContext:Lcom/google/android/feedback/FeedbackActivity;

.field private mEventLog:Ljava/lang/String;

.field private mGotSystemLogs:Z

.field private mInstalledPackages:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mReport:Lcom/google/android/feedback/ExtendedErrorReport;

.field private mRunningApps:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mScreenshot:Lcom/google/android/feedback/FeedbackSession$Screenshot;

.field private mSendPrivateData:Z

.field private mSendScreenshot:Z

.field private mSystemLog:Ljava/lang/String;

.field private spinnerPosition:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/android/feedback/FeedbackSession;->EMPTY_STRING_LIST:Ljava/util/List;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    sput-object v0, Lcom/google/android/feedback/FeedbackSession;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/feedback/FeedbackActivity;Landroid/app/ApplicationErrorReport;)V
    .locals 1
    .param p1    # Lcom/google/android/feedback/FeedbackActivity;
    .param p2    # Landroid/app/ApplicationErrorReport;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/feedback/FeedbackSession;-><init>(Lcom/google/android/feedback/FeedbackActivity;Landroid/app/ApplicationErrorReport;Lcom/google/android/feedback/FeedbackSession$Screenshot;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/feedback/FeedbackActivity;Landroid/app/ApplicationErrorReport;Lcom/google/android/feedback/FeedbackSession$Screenshot;)V
    .locals 1
    .param p1    # Lcom/google/android/feedback/FeedbackActivity;
    .param p2    # Landroid/app/ApplicationErrorReport;
    .param p3    # Lcom/google/android/feedback/FeedbackSession$Screenshot;

    const/4 v0, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/google/android/feedback/FeedbackSession;->mSendPrivateData:Z

    iput-boolean v0, p0, Lcom/google/android/feedback/FeedbackSession;->mSendScreenshot:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/feedback/FeedbackSession;->spinnerPosition:I

    iput-object p1, p0, Lcom/google/android/feedback/FeedbackSession;->mContext:Lcom/google/android/feedback/FeedbackActivity;

    new-instance v0, Lcom/google/android/feedback/ExtendedErrorReport;

    invoke-direct {v0, p2}, Lcom/google/android/feedback/ExtendedErrorReport;-><init>(Landroid/app/ApplicationErrorReport;)V

    iput-object v0, p0, Lcom/google/android/feedback/FeedbackSession;->mReport:Lcom/google/android/feedback/ExtendedErrorReport;

    invoke-direct {p0}, Lcom/google/android/feedback/FeedbackSession;->getSystemInfo()V

    invoke-direct {p0}, Lcom/google/android/feedback/FeedbackSession;->getAppInfo()V

    iget-object v0, p0, Lcom/google/android/feedback/FeedbackSession;->mReport:Lcom/google/android/feedback/ExtendedErrorReport;

    invoke-direct {p0, v0}, Lcom/google/android/feedback/FeedbackSession;->getBuildInfo(Lcom/google/android/feedback/ExtendedErrorReport;)V

    iget-object v0, p0, Lcom/google/android/feedback/FeedbackSession;->mReport:Lcom/google/android/feedback/ExtendedErrorReport;

    invoke-direct {p0, v0}, Lcom/google/android/feedback/FeedbackSession;->getTelephonyInfo(Lcom/google/android/feedback/ExtendedErrorReport;)V

    iput-object p3, p0, Lcom/google/android/feedback/FeedbackSession;->mScreenshot:Lcom/google/android/feedback/FeedbackSession$Screenshot;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/feedback/FeedbackActivity;Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Lcom/google/android/feedback/FeedbackActivity;
    .param p2    # Landroid/os/Bundle;

    const/4 v0, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/google/android/feedback/FeedbackSession;->mSendPrivateData:Z

    iput-boolean v0, p0, Lcom/google/android/feedback/FeedbackSession;->mSendScreenshot:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/feedback/FeedbackSession;->spinnerPosition:I

    iput-object p1, p0, Lcom/google/android/feedback/FeedbackSession;->mContext:Lcom/google/android/feedback/FeedbackActivity;

    invoke-virtual {p0, p2}, Lcom/google/android/feedback/FeedbackSession;->onRestoreInstanceState(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/google/android/feedback/FeedbackSession;->getAppInfo()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/feedback/FeedbackSession;)Landroid/widget/Spinner;
    .locals 1
    .param p0    # Lcom/google/android/feedback/FeedbackSession;

    iget-object v0, p0, Lcom/google/android/feedback/FeedbackSession;->mChooseAccount:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/feedback/FeedbackSession;)Lcom/google/android/feedback/FeedbackActivity;
    .locals 1
    .param p0    # Lcom/google/android/feedback/FeedbackSession;

    iget-object v0, p0, Lcom/google/android/feedback/FeedbackSession;->mContext:Lcom/google/android/feedback/FeedbackActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/feedback/FeedbackSession;)Lcom/google/android/feedback/ExtendedErrorReport;
    .locals 1
    .param p0    # Lcom/google/android/feedback/FeedbackSession;

    iget-object v0, p0, Lcom/google/android/feedback/FeedbackSession;->mReport:Lcom/google/android/feedback/ExtendedErrorReport;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/feedback/FeedbackSession;I)I
    .locals 0
    .param p0    # Lcom/google/android/feedback/FeedbackSession;
    .param p1    # I

    iput p1, p0, Lcom/google/android/feedback/FeedbackSession;->spinnerPosition:I

    return p1
.end method

.method private getAnrStackTraces()Ljava/lang/String;
    .locals 7

    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Ljava/io/FileReader;

    const-string v5, "/data/anr/traces.txt"

    invoke-direct {v1, v5}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    new-instance v3, Ljava/io/BufferedReader;

    invoke-direct {v3, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    :goto_0
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v5

    if-eqz v1, :cond_0

    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    :cond_0
    :goto_1
    move-object v0, v1

    :goto_2
    return-object v5

    :cond_1
    :try_start_3
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catch_0
    move-exception v5

    move-object v0, v1

    :goto_3
    if-eqz v0, :cond_2

    :try_start_4
    invoke-virtual {v0}, Ljava/io/FileReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    :cond_2
    :goto_4
    const/4 v5, 0x0

    goto :goto_2

    :catch_1
    move-exception v5

    :goto_5
    if-eqz v0, :cond_2

    :try_start_5
    invoke-virtual {v0}, Ljava/io/FileReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_4

    :catch_2
    move-exception v5

    goto :goto_4

    :catchall_0
    move-exception v5

    :goto_6
    if-eqz v0, :cond_3

    :try_start_6
    invoke-virtual {v0}, Ljava/io/FileReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    :cond_3
    :goto_7
    throw v5

    :catch_3
    move-exception v6

    goto :goto_1

    :catch_4
    move-exception v5

    goto :goto_4

    :catch_5
    move-exception v6

    goto :goto_7

    :catchall_1
    move-exception v5

    move-object v0, v1

    goto :goto_6

    :catch_6
    move-exception v5

    move-object v0, v1

    goto :goto_5

    :catch_7
    move-exception v5

    goto :goto_3
.end method

.method private getAppInfo()V
    .locals 5

    iget-object v3, p0, Lcom/google/android/feedback/FeedbackSession;->mContext:Lcom/google/android/feedback/FeedbackActivity;

    invoke-virtual {v3}, Lcom/google/android/feedback/FeedbackActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    :try_start_0
    iget-object v3, p0, Lcom/google/android/feedback/FeedbackSession;->mReport:Lcom/google/android/feedback/ExtendedErrorReport;

    iget-object v3, v3, Lcom/google/android/feedback/ExtendedErrorReport;->packageName:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/content/pm/PackageManager;->getApplicationIcon(Landroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/feedback/FeedbackSession;->mAppIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v1}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/feedback/FeedbackSession;->mAppLabel:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/google/android/feedback/FeedbackSession;->mAppLabel:Ljava/lang/String;

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/google/android/feedback/FeedbackSession;->mContext:Lcom/google/android/feedback/FeedbackActivity;

    invoke-virtual {v3}, Lcom/google/android/feedback/FeedbackActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f060012

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/feedback/FeedbackSession;->mAppLabel:Ljava/lang/String;

    :cond_1
    iget-object v3, p0, Lcom/google/android/feedback/FeedbackSession;->mAppIcon:Landroid/graphics/drawable/Drawable;

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/feedback/FeedbackSession;->mContext:Lcom/google/android/feedback/FeedbackActivity;

    invoke-virtual {v3}, Lcom/google/android/feedback/FeedbackActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02000a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/feedback/FeedbackSession;->mAppIcon:Landroid/graphics/drawable/Drawable;

    :cond_2
    return-void

    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method private getBuildInfo(Lcom/google/android/feedback/ExtendedErrorReport;)V
    .locals 4
    .param p1    # Lcom/google/android/feedback/ExtendedErrorReport;

    sget-object v2, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    iput-object v2, p1, Lcom/google/android/feedback/ExtendedErrorReport;->device:Ljava/lang/String;

    sget-object v2, Landroid/os/Build;->DISPLAY:Ljava/lang/String;

    iput-object v2, p1, Lcom/google/android/feedback/ExtendedErrorReport;->buildId:Ljava/lang/String;

    sget-object v2, Landroid/os/Build;->TYPE:Ljava/lang/String;

    iput-object v2, p1, Lcom/google/android/feedback/ExtendedErrorReport;->buildType:Ljava/lang/String;

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iput-object v2, p1, Lcom/google/android/feedback/ExtendedErrorReport;->model:Ljava/lang/String;

    sget-object v2, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    iput-object v2, p1, Lcom/google/android/feedback/ExtendedErrorReport;->product:Ljava/lang/String;

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    iput v2, p1, Lcom/google/android/feedback/ExtendedErrorReport;->sdk_int:I

    sget-object v2, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    iput-object v2, p1, Lcom/google/android/feedback/ExtendedErrorReport;->release:Ljava/lang/String;

    sget-object v2, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    iput-object v2, p1, Lcom/google/android/feedback/ExtendedErrorReport;->incremental:Ljava/lang/String;

    sget-object v2, Landroid/os/Build$VERSION;->CODENAME:Ljava/lang/String;

    iput-object v2, p1, Lcom/google/android/feedback/ExtendedErrorReport;->codename:Ljava/lang/String;

    sget-object v2, Landroid/os/Build;->BOARD:Ljava/lang/String;

    iput-object v2, p1, Lcom/google/android/feedback/ExtendedErrorReport;->board:Ljava/lang/String;

    sget-object v2, Landroid/os/Build;->BRAND:Ljava/lang/String;

    iput-object v2, p1, Lcom/google/android/feedback/ExtendedErrorReport;->brand:Ljava/lang/String;

    sget-object v2, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    iput-object v2, p1, Lcom/google/android/feedback/ExtendedErrorReport;->buildFingerprint:Ljava/lang/String;

    :try_start_0
    iget-object v2, p0, Lcom/google/android/feedback/FeedbackSession;->mContext:Lcom/google/android/feedback/FeedbackActivity;

    invoke-virtual {v2}, Lcom/google/android/feedback/FeedbackActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/feedback/ExtendedErrorReport;->packageName:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget v2, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    iput v2, p1, Lcom/google/android/feedback/ExtendedErrorReport;->packageVersion:I

    iget-object v2, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    iput-object v2, p1, Lcom/google/android/feedback/ExtendedErrorReport;->packageVersionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method private getSystemInfo()V
    .locals 6

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/android/feedback/FeedbackSession;->mInstalledPackages:Ljava/util/ArrayList;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/android/feedback/FeedbackSession;->mRunningApps:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/google/android/feedback/FeedbackSession;->mContext:Lcom/google/android/feedback/FeedbackActivity;

    const-string v5, "activity"

    invoke-virtual {v4, v5}, Lcom/google/android/feedback/FeedbackActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager$RunningAppProcessInfo;

    iget-object v4, p0, Lcom/google/android/feedback/FeedbackSession;->mRunningApps:Ljava/util/ArrayList;

    iget-object v5, v1, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v4, p0, Lcom/google/android/feedback/FeedbackSession;->mReport:Lcom/google/android/feedback/ExtendedErrorReport;

    iget v4, v4, Lcom/google/android/feedback/ExtendedErrorReport;->type:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    invoke-direct {p0}, Lcom/google/android/feedback/FeedbackSession;->getAnrStackTraces()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/feedback/FeedbackSession;->mAnrStackTraces:Ljava/lang/String;

    :cond_1
    return-void
.end method

.method private getTelephonyInfo(Lcom/google/android/feedback/ExtendedErrorReport;)V
    .locals 3
    .param p1    # Lcom/google/android/feedback/ExtendedErrorReport;

    iget-object v1, p0, Lcom/google/android/feedback/FeedbackSession;->mContext:Lcom/google/android/feedback/FeedbackActivity;

    const-string v2, "phone"

    invoke-virtual {v1, v2}, Lcom/google/android/feedback/FeedbackActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v1

    iput v1, p1, Lcom/google/android/feedback/ExtendedErrorReport;->phoneType:I

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p1, Lcom/google/android/feedback/ExtendedErrorReport;->networkName:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v1

    iput v1, p1, Lcom/google/android/feedback/ExtendedErrorReport;->networkType:I

    return-void
.end method

.method private send()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/feedback/FeedbackSession;->updateReport()V

    iget-object v0, p0, Lcom/google/android/feedback/FeedbackSession;->mContext:Lcom/google/android/feedback/FeedbackActivity;

    invoke-virtual {v0}, Lcom/google/android/feedback/FeedbackActivity;->popSession()V

    iget-object v0, p0, Lcom/google/android/feedback/FeedbackSession;->mContext:Lcom/google/android/feedback/FeedbackActivity;

    iget-object v1, p0, Lcom/google/android/feedback/FeedbackSession;->mReport:Lcom/google/android/feedback/ExtendedErrorReport;

    invoke-virtual {v0, v1}, Lcom/google/android/feedback/FeedbackActivity;->sendErrorReport(Lcom/google/android/feedback/ExtendedErrorReport;)V

    return-void
.end method

.method private setProgressBarVisible(Z)V
    .locals 1
    .param p1    # Z

    invoke-static {p0}, Lcom/google/android/feedback/FeedbackActivity;->isCurrentSession(Lcom/google/android/feedback/FeedbackSession;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/feedback/FeedbackSession;->mContext:Lcom/google/android/feedback/FeedbackActivity;

    invoke-virtual {v0, p1}, Lcom/google/android/feedback/FeedbackActivity;->setProgressBarVisible(Z)V

    goto :goto_0
.end method

.method private setScreenshotParams(Ljava/lang/String;II)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/feedback/FeedbackSession;->mReport:Lcom/google/android/feedback/ExtendedErrorReport;

    iget-object v1, p0, Lcom/google/android/feedback/FeedbackSession;->mScreenshot:Lcom/google/android/feedback/FeedbackSession$Screenshot;

    iget v1, v1, Lcom/google/android/feedback/FeedbackSession$Screenshot;->width:I

    iput v1, v0, Lcom/google/android/feedback/ExtendedErrorReport;->screenshotWidth:I

    iget-object v0, p0, Lcom/google/android/feedback/FeedbackSession;->mReport:Lcom/google/android/feedback/ExtendedErrorReport;

    iget-object v1, p0, Lcom/google/android/feedback/FeedbackSession;->mScreenshot:Lcom/google/android/feedback/FeedbackSession$Screenshot;

    iget v1, v1, Lcom/google/android/feedback/FeedbackSession$Screenshot;->height:I

    iput v1, v0, Lcom/google/android/feedback/ExtendedErrorReport;->screenshotHeight:I

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/feedback/FeedbackSession;->mReport:Lcom/google/android/feedback/ExtendedErrorReport;

    iget-object v1, p0, Lcom/google/android/feedback/FeedbackSession;->mScreenshot:Lcom/google/android/feedback/FeedbackSession$Screenshot;

    iget-object v1, v1, Lcom/google/android/feedback/FeedbackSession$Screenshot;->base64EncodedScreenshot:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/feedback/ExtendedErrorReport;->screenshot:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/feedback/FeedbackSession;->mReport:Lcom/google/android/feedback/ExtendedErrorReport;

    iget-object v1, p0, Lcom/google/android/feedback/FeedbackSession;->mScreenshot:Lcom/google/android/feedback/FeedbackSession$Screenshot;

    iget-object v1, v1, Lcom/google/android/feedback/FeedbackSession$Screenshot;->base64EncodedScreenshot:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/feedback/ExtendedErrorReport;->screenshotBytes:[B

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/feedback/FeedbackSession;->mReport:Lcom/google/android/feedback/ExtendedErrorReport;

    iput-object v2, v0, Lcom/google/android/feedback/ExtendedErrorReport;->screenshot:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/feedback/FeedbackSession;->mReport:Lcom/google/android/feedback/ExtendedErrorReport;

    iput-object v2, v0, Lcom/google/android/feedback/ExtendedErrorReport;->screenshotBytes:[B

    goto :goto_0
.end method

.method private showPreview()V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/feedback/FeedbackSession;->updateReport()V

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/feedback/FeedbackSession;->mContext:Lcom/google/android/feedback/FeedbackActivity;

    const-class v2, Lcom/google/android/feedback/PreviewActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/google/android/feedback/FeedbackSession;->mContext:Lcom/google/android/feedback/FeedbackActivity;

    invoke-virtual {v1, v0}, Lcom/google/android/feedback/FeedbackActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private updateReport()V
    .locals 8

    const v7, 0x7f08000d

    const/4 v6, 0x0

    const/4 v4, 0x0

    invoke-static {p0}, Lcom/google/android/feedback/FeedbackActivity;->isCurrentSession(Lcom/google/android/feedback/FeedbackSession;)Z

    move-result v3

    if-nez v3, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v3, p0, Lcom/google/android/feedback/FeedbackSession;->mContext:Lcom/google/android/feedback/FeedbackActivity;

    const v5, 0x7f080008

    invoke-virtual {v3, v5}, Lcom/google/android/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iget-object v3, p0, Lcom/google/android/feedback/FeedbackSession;->mReport:Lcom/google/android/feedback/ExtendedErrorReport;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/google/android/feedback/ExtendedErrorReport;->description:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/feedback/FeedbackSession;->mContext:Lcom/google/android/feedback/FeedbackActivity;

    const v5, 0x7f08000c

    invoke-virtual {v3, v5}, Lcom/google/android/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/feedback/FeedbackSession;->mSendPrivateData:Z

    iget-object v3, p0, Lcom/google/android/feedback/FeedbackSession;->mScreenshot:Lcom/google/android/feedback/FeedbackSession$Screenshot;

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/feedback/FeedbackSession;->mContext:Lcom/google/android/feedback/FeedbackActivity;

    invoke-virtual {v3, v7}, Lcom/google/android/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/16 v5, 0x8

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    iget-object v5, p0, Lcom/google/android/feedback/FeedbackSession;->mReport:Lcom/google/android/feedback/ExtendedErrorReport;

    iget-boolean v3, p0, Lcom/google/android/feedback/FeedbackSession;->mSendPrivateData:Z

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/feedback/FeedbackSession;->mSystemLog:Ljava/lang/String;

    :goto_2
    iput-object v3, v5, Lcom/google/android/feedback/ExtendedErrorReport;->systemLog:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/feedback/FeedbackSession;->mReport:Lcom/google/android/feedback/ExtendedErrorReport;

    iget-boolean v3, p0, Lcom/google/android/feedback/FeedbackSession;->mSendPrivateData:Z

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/android/feedback/FeedbackSession;->mEventLog:Ljava/lang/String;

    :goto_3
    iput-object v3, v5, Lcom/google/android/feedback/ExtendedErrorReport;->eventLog:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/feedback/FeedbackSession;->mReport:Lcom/google/android/feedback/ExtendedErrorReport;

    iget-boolean v3, p0, Lcom/google/android/feedback/FeedbackSession;->mSendPrivateData:Z

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/google/android/feedback/FeedbackSession;->mInstalledPackages:Ljava/util/ArrayList;

    :goto_4
    iput-object v3, v5, Lcom/google/android/feedback/ExtendedErrorReport;->installedPackages:Ljava/util/List;

    iget-object v5, p0, Lcom/google/android/feedback/FeedbackSession;->mReport:Lcom/google/android/feedback/ExtendedErrorReport;

    iget-boolean v3, p0, Lcom/google/android/feedback/FeedbackSession;->mSendPrivateData:Z

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/google/android/feedback/FeedbackSession;->mRunningApps:Ljava/util/ArrayList;

    :goto_5
    iput-object v3, v5, Lcom/google/android/feedback/ExtendedErrorReport;->runningApplications:Ljava/util/List;

    iget-object v3, p0, Lcom/google/android/feedback/FeedbackSession;->mReport:Lcom/google/android/feedback/ExtendedErrorReport;

    iget-boolean v5, p0, Lcom/google/android/feedback/FeedbackSession;->mSendPrivateData:Z

    if-eqz v5, :cond_1

    iget-object v4, p0, Lcom/google/android/feedback/FeedbackSession;->mAnrStackTraces:Ljava/lang/String;

    :cond_1
    iput-object v4, v3, Lcom/google/android/feedback/ExtendedErrorReport;->anrStackTraces:Ljava/lang/String;

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/google/android/feedback/FeedbackSession;->mContext:Lcom/google/android/feedback/FeedbackActivity;

    invoke-virtual {v3, v7}, Lcom/google/android/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/google/android/feedback/FeedbackSession;->mContext:Lcom/google/android/feedback/FeedbackActivity;

    const v5, 0x7f08000e

    invoke-virtual {v3, v5}, Lcom/google/android/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/feedback/FeedbackSession;->mSendScreenshot:Z

    iget-boolean v3, p0, Lcom/google/android/feedback/FeedbackSession;->mSendScreenshot:Z

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/feedback/FeedbackSession;->mScreenshot:Lcom/google/android/feedback/FeedbackSession$Screenshot;

    iget-object v3, v3, Lcom/google/android/feedback/FeedbackSession$Screenshot;->base64EncodedScreenshot:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/feedback/FeedbackSession;->mScreenshot:Lcom/google/android/feedback/FeedbackSession$Screenshot;

    iget v5, v5, Lcom/google/android/feedback/FeedbackSession$Screenshot;->width:I

    iget-object v6, p0, Lcom/google/android/feedback/FeedbackSession;->mScreenshot:Lcom/google/android/feedback/FeedbackSession$Screenshot;

    iget v6, v6, Lcom/google/android/feedback/FeedbackSession$Screenshot;->height:I

    invoke-direct {p0, v3, v5, v6}, Lcom/google/android/feedback/FeedbackSession;->setScreenshotParams(Ljava/lang/String;II)V

    goto :goto_1

    :cond_3
    invoke-direct {p0, v4, v6, v6}, Lcom/google/android/feedback/FeedbackSession;->setScreenshotParams(Ljava/lang/String;II)V

    goto :goto_1

    :cond_4
    move-object v3, v4

    goto :goto_2

    :cond_5
    move-object v3, v4

    goto :goto_3

    :cond_6
    sget-object v3, Lcom/google/android/feedback/FeedbackSession;->EMPTY_STRING_LIST:Ljava/util/List;

    goto :goto_4

    :cond_7
    sget-object v3, Lcom/google/android/feedback/FeedbackSession;->EMPTY_STRING_LIST:Ljava/util/List;

    goto :goto_5
.end method

.method private updateSpinner()V
    .locals 12

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    iget-object v9, p0, Lcom/google/android/feedback/FeedbackSession;->mContext:Lcom/google/android/feedback/FeedbackActivity;

    invoke-static {v9}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v9

    invoke-virtual {v9}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v5

    move-object v4, v5

    array-length v8, v4

    const/4 v7, 0x0

    :goto_0
    if-ge v7, v8, :cond_1

    aget-object v0, v4, v7

    iget-object v9, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    const-string v10, "com.google"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    iget-object v9, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v3, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/VerifyError; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :catch_0
    move-exception v9

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v9

    add-int/lit8 v9, v9, 0x1

    new-array v1, v9, [Ljava/lang/String;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/google/android/feedback/FeedbackSession;->mContext:Lcom/google/android/feedback/FeedbackActivity;

    const v11, 0x7f06001e

    invoke-virtual {v10, v11}, Lcom/google/android/feedback/FeedbackActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v1, v9

    const/4 v6, 0x0

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    add-int/lit8 v6, v6, 0x1

    aput-object v0, v1, v6

    goto :goto_2

    :cond_2
    new-instance v2, Landroid/widget/ArrayAdapter;

    iget-object v9, p0, Lcom/google/android/feedback/FeedbackSession;->mContext:Lcom/google/android/feedback/FeedbackActivity;

    const v10, 0x7f030008

    invoke-direct {v2, v9, v10, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    const v9, 0x1090009

    invoke-virtual {v2, v9}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v9, p0, Lcom/google/android/feedback/FeedbackSession;->mChooseAccount:Landroid/widget/Spinner;

    invoke-virtual {v9, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v9, p0, Lcom/google/android/feedback/FeedbackSession;->mChooseAccount:Landroid/widget/Spinner;

    iget v10, p0, Lcom/google/android/feedback/FeedbackSession;->spinnerPosition:I

    invoke-virtual {v9, v10}, Landroid/widget/Spinner;->setSelection(I)V

    return-void

    :catch_1
    move-exception v9

    goto :goto_1
.end method

.method private updateUi()V
    .locals 11

    const v10, 0x7f08000f

    const/4 v3, 0x1

    const v9, 0x7f08000d

    const/16 v8, 0x8

    const/4 v4, 0x0

    invoke-static {p0}, Lcom/google/android/feedback/FeedbackActivity;->isCurrentSession(Lcom/google/android/feedback/FeedbackSession;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/feedback/FeedbackSession;->mContext:Lcom/google/android/feedback/FeedbackActivity;

    invoke-virtual {v2}, Lcom/google/android/feedback/FeedbackActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/feedback/FeedbackSession;->mContext:Lcom/google/android/feedback/FeedbackActivity;

    const v5, 0x7f080008

    invoke-virtual {v2, v5}, Lcom/google/android/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iget-object v5, p0, Lcom/google/android/feedback/FeedbackSession;->mReport:Lcom/google/android/feedback/ExtendedErrorReport;

    iget-object v5, v5, Lcom/google/android/feedback/ExtendedErrorReport;->description:Ljava/lang/String;

    invoke-virtual {v2, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/google/android/feedback/FeedbackSession;->mContext:Lcom/google/android/feedback/FeedbackActivity;

    const v5, 0x7f08000c

    invoke-virtual {v2, v5}, Lcom/google/android/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iget-boolean v5, p0, Lcom/google/android/feedback/FeedbackSession;->mSendPrivateData:Z

    invoke-virtual {v2, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v2, p0, Lcom/google/android/feedback/FeedbackSession;->mContext:Lcom/google/android/feedback/FeedbackActivity;

    const v5, 0x7f060003

    new-array v6, v3, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/google/android/feedback/FeedbackSession;->mAppLabel:Ljava/lang/String;

    aput-object v7, v6, v4

    invoke-virtual {v1, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/google/android/feedback/FeedbackActivity;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/google/android/feedback/FeedbackSession;->mContext:Lcom/google/android/feedback/FeedbackActivity;

    invoke-virtual {v2}, Lcom/google/android/feedback/FeedbackActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/google/android/feedback/FeedbackSession;->mAppIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setIcon(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    iget-object v2, p0, Lcom/google/android/feedback/FeedbackSession;->mReport:Lcom/google/android/feedback/ExtendedErrorReport;

    iget v2, v2, Lcom/google/android/feedback/ExtendedErrorReport;->type:I

    const/16 v5, 0xb

    if-ne v2, v5, :cond_3

    iget-object v2, p0, Lcom/google/android/feedback/FeedbackSession;->mScreenshot:Lcom/google/android/feedback/FeedbackSession$Screenshot;

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/feedback/FeedbackSession;->mContext:Lcom/google/android/feedback/FeedbackActivity;

    invoke-virtual {v2, v9}, Lcom/google/android/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    iget-object v2, p0, Lcom/google/android/feedback/FeedbackSession;->mContext:Lcom/google/android/feedback/FeedbackActivity;

    invoke-virtual {v2, v10}, Lcom/google/android/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/feedback/FeedbackSession;->mContext:Lcom/google/android/feedback/FeedbackActivity;

    const v5, 0x7f080010

    invoke-virtual {v2, v5}, Lcom/google/android/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Spinner;

    iput-object v2, p0, Lcom/google/android/feedback/FeedbackSession;->mChooseAccount:Landroid/widget/Spinner;

    iget-object v2, p0, Lcom/google/android/feedback/FeedbackSession;->mChooseAccount:Landroid/widget/Spinner;

    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Landroid/widget/Spinner;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v2, p0, Lcom/google/android/feedback/FeedbackSession;->mChooseAccount:Landroid/widget/Spinner;

    new-instance v5, Lcom/google/android/feedback/FeedbackSession$1;

    invoke-direct {v5, p0}, Lcom/google/android/feedback/FeedbackSession$1;-><init>(Lcom/google/android/feedback/FeedbackSession;)V

    invoke-virtual {v2, v5}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    invoke-direct {p0}, Lcom/google/android/feedback/FeedbackSession;->updateSpinner()V

    :goto_2
    iget-boolean v2, p0, Lcom/google/android/feedback/FeedbackSession;->mGotSystemLogs:Z

    if-nez v2, :cond_4

    move v2, v3

    :goto_3
    invoke-direct {p0, v2}, Lcom/google/android/feedback/FeedbackSession;->setProgressBarVisible(Z)V

    goto/16 :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/feedback/FeedbackSession;->mContext:Lcom/google/android/feedback/FeedbackActivity;

    invoke-virtual {v2, v9}, Lcom/google/android/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/feedback/FeedbackSession;->mContext:Lcom/google/android/feedback/FeedbackActivity;

    const v5, 0x7f08000e

    invoke-virtual {v2, v5}, Lcom/google/android/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iget-boolean v5, p0, Lcom/google/android/feedback/FeedbackSession;->mSendScreenshot:Z

    invoke-virtual {v2, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_1

    :cond_3
    iget-object v2, p0, Lcom/google/android/feedback/FeedbackSession;->mContext:Lcom/google/android/feedback/FeedbackActivity;

    invoke-virtual {v2, v9}, Lcom/google/android/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/feedback/FeedbackSession;->mContext:Lcom/google/android/feedback/FeedbackActivity;

    invoke-virtual {v2, v10}, Lcom/google/android/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_4
    move v2, v4

    goto :goto_3
.end method


# virtual methods
.method public getReport()Lcom/google/android/feedback/ExtendedErrorReport;
    .locals 1

    iget-object v0, p0, Lcom/google/android/feedback/FeedbackSession;->mReport:Lcom/google/android/feedback/ExtendedErrorReport;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/feedback/FeedbackSession;->showPreview()V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/feedback/FeedbackSession;->send()V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/feedback/FeedbackSession;->mContext:Lcom/google/android/feedback/FeedbackActivity;

    invoke-virtual {v0}, Lcom/google/android/feedback/FeedbackActivity;->popSession()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f080000
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onGotSystemLog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-boolean v0, p0, Lcom/google/android/feedback/FeedbackSession;->mGotSystemLogs:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/feedback/FeedbackSession;->mGotSystemLogs:Z

    iput-object p1, p0, Lcom/google/android/feedback/FeedbackSession;->mSystemLog:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/feedback/FeedbackSession;->mEventLog:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/feedback/FeedbackSession;->updateReport()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/feedback/FeedbackSession;->setProgressBarVisible(Z)V

    :cond_0
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    const-string v0, "feedback.REPORT"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/feedback/ExtendedErrorReport;

    iput-object v0, p0, Lcom/google/android/feedback/FeedbackSession;->mReport:Lcom/google/android/feedback/ExtendedErrorReport;

    const-string v0, "feedback.SYSTEM_LOG"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/feedback/FeedbackSession;->mSystemLog:Ljava/lang/String;

    const-string v0, "feedback.EVENT_LOG"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/feedback/FeedbackSession;->mEventLog:Ljava/lang/String;

    const-string v0, "feedback.INSTALLED_PACKAGES"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/feedback/FeedbackSession;->mInstalledPackages:Ljava/util/ArrayList;

    const-string v0, "feedback.RUNNING_APPS"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/feedback/FeedbackSession;->mRunningApps:Ljava/util/ArrayList;

    const-string v0, "feedback.GET_SYSTEM_LOG_THREAD_DONE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/feedback/FeedbackSession;->mGotSystemLogs:Z

    const-string v0, "feedback.SEND_PRIVATE_DATA"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/feedback/FeedbackSession;->mSendPrivateData:Z

    const-string v0, "feedback.ANR_STACK_TRACES_KEY"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/feedback/FeedbackSession;->mAnrStackTraces:Ljava/lang/String;

    const-string v0, "feedback.SCREENSHOT_KEY"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/feedback/FeedbackSession$Screenshot;

    iput-object v0, p0, Lcom/google/android/feedback/FeedbackSession;->mScreenshot:Lcom/google/android/feedback/FeedbackSession$Screenshot;

    return-void
.end method

.method onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-direct {p0}, Lcom/google/android/feedback/FeedbackSession;->updateReport()V

    const-string v0, "feedback.REPORT"

    iget-object v1, p0, Lcom/google/android/feedback/FeedbackSession;->mReport:Lcom/google/android/feedback/ExtendedErrorReport;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "feedback.SYSTEM_LOG"

    iget-object v1, p0, Lcom/google/android/feedback/FeedbackSession;->mSystemLog:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "feedback.EVENT_LOG"

    iget-object v1, p0, Lcom/google/android/feedback/FeedbackSession;->mEventLog:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "feedback.INSTALLED_PACKAGES"

    iget-object v1, p0, Lcom/google/android/feedback/FeedbackSession;->mInstalledPackages:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v0, "feedback.RUNNING_APPS"

    iget-object v1, p0, Lcom/google/android/feedback/FeedbackSession;->mRunningApps:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v0, "feedback.GET_SYSTEM_LOG_THREAD_DONE"

    iget-boolean v1, p0, Lcom/google/android/feedback/FeedbackSession;->mGotSystemLogs:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "feedback.SEND_PRIVATE_DATA"

    iget-boolean v1, p0, Lcom/google/android/feedback/FeedbackSession;->mSendPrivateData:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "feedback.ANR_STACK_TRACES_KEY"

    iget-object v1, p0, Lcom/google/android/feedback/FeedbackSession;->mAnrStackTraces:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "feedback.SCREENSHOT_KEY"

    iget-object v1, p0, Lcom/google/android/feedback/FeedbackSession;->mScreenshot:Lcom/google/android/feedback/FeedbackSession$Screenshot;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method

.method public onStart()V
    .locals 4

    invoke-direct {p0}, Lcom/google/android/feedback/FeedbackSession;->updateUi()V

    iget-boolean v0, p0, Lcom/google/android/feedback/FeedbackSession;->mGotSystemLogs:Z

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/feedback/SystemLogFetcher;->fetch()V

    sget-object v0, Lcom/google/android/feedback/FeedbackSession;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/feedback/FeedbackSession$2;

    invoke-direct {v1, p0}, Lcom/google/android/feedback/FeedbackSession$2;-><init>(Lcom/google/android/feedback/FeedbackSession;)V

    const-wide/16 v2, 0xfa0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/feedback/FeedbackSession;->updateReport()V

    return-void
.end method
