.class public Lcom/google/android/feedback/SendService;
.super Landroid/app/Service;
.source "SendService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/feedback/SendService$ScannerThread;
    }
.end annotation


# instance fields
.field private mFirstScan:Z

.field private mHandler:Landroid/os/Handler;

.field private mHttpClient:Lcom/google/android/common/http/GoogleHttpClient;

.field private mScanAgain:Z

.field private mScannerThread:Lcom/google/android/feedback/SendService$ScannerThread;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/feedback/SendService;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/feedback/SendService;)Lcom/google/android/common/http/GoogleHttpClient;
    .locals 1
    .param p0    # Lcom/google/android/feedback/SendService;

    iget-object v0, p0, Lcom/google/android/feedback/SendService;->mHttpClient:Lcom/google/android/common/http/GoogleHttpClient;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/feedback/SendService;ZZ)V
    .locals 0
    .param p0    # Lcom/google/android/feedback/SendService;
    .param p1    # Z
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/google/android/feedback/SendService;->onThreadFinished(ZZ)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/feedback/SendService;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/google/android/feedback/SendService;

    iget-object v0, p0, Lcom/google/android/feedback/SendService;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private onThreadFinished(ZZ)V
    .locals 3
    .param p1    # Z
    .param p2    # Z

    const-string v0, "GoogleFeedbackSendService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "finished report scan: scanAgain: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/feedback/SendService;->mScanAgain:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " networkError: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/feedback/SendService;->mScannerThread:Lcom/google/android/feedback/SendService$ScannerThread;

    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/feedback/SendService;->mScanAgain:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/feedback/SendService;->scanForReports()V

    :goto_0
    return-void

    :cond_0
    if-nez p2, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {p0, v0}, Lcom/google/android/feedback/FeedbackConnectivityReceiver;->setEnabled(Landroid/content/Context;Z)V

    invoke-virtual {p0}, Lcom/google/android/feedback/SendService;->stopSelf()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private scanForReports()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/feedback/SendService;->mScannerThread:Lcom/google/android/feedback/SendService$ScannerThread;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/feedback/SendService;->mScanAgain:Z

    const-string v1, "GoogleFeedbackSendService"

    const-string v2, "starting report scan"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/google/android/feedback/SendService;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "reports"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/feedback/SendService$ScannerThread;

    invoke-direct {v1, p0, v0}, Lcom/google/android/feedback/SendService$ScannerThread;-><init>(Lcom/google/android/feedback/SendService;Ljava/io/File;)V

    iput-object v1, p0, Lcom/google/android/feedback/SendService;->mScannerThread:Lcom/google/android/feedback/SendService$ScannerThread;

    iget-object v1, p0, Lcom/google/android/feedback/SendService;->mScannerThread:Lcom/google/android/feedback/SendService$ScannerThread;

    invoke-virtual {v1}, Lcom/google/android/feedback/SendService$ScannerThread;->start()V

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/feedback/SendService;->mScanAgain:Z

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    new-instance v0, Lcom/google/android/common/http/GoogleHttpClient;

    const-string v1, "AndroidGoogleFeedback/1.0"

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/common/http/GoogleHttpClient;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/google/android/feedback/SendService;->mHttpClient:Lcom/google/android/common/http/GoogleHttpClient;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/feedback/SendService;->mFirstScan:Z

    invoke-direct {p0}, Lcom/google/android/feedback/SendService;->scanForReports()V

    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 1
    .param p1    # Landroid/content/Intent;
    .param p2    # I

    invoke-super {p0, p1, p2}, Landroid/app/Service;->onStart(Landroid/content/Intent;I)V

    iget-boolean v0, p0, Lcom/google/android/feedback/SendService;->mFirstScan:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/feedback/SendService;->mFirstScan:Z

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/feedback/SendService;->scanForReports()V

    goto :goto_0
.end method
