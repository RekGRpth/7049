.class public Lcom/google/android/feedback/ShowTextActivity;
.super Landroid/app/Activity;
.source "ShowTextActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/16 v3, 0x8

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v2, 0x7f030007

    invoke-virtual {p0, v2}, Lcom/google/android/feedback/ShowTextActivity;->setContentView(I)V

    const/high16 v2, 0x7f080000

    invoke-virtual {p0, v2}, Lcom/google/android/feedback/ShowTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    const v2, 0x7f080002

    invoke-virtual {p0, v2}, Lcom/google/android/feedback/ShowTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    const v2, 0x7f080003

    invoke-virtual {p0, v2}, Lcom/google/android/feedback/ShowTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    const v2, 0x7f080001

    invoke-virtual {p0, v2}, Lcom/google/android/feedback/ShowTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v3, Lcom/google/android/feedback/ShowTextActivity$1;

    invoke-direct {v3, p0}, Lcom/google/android/feedback/ShowTextActivity$1;-><init>(Lcom/google/android/feedback/ShowTextActivity;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/google/android/feedback/ShowTextActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "feedback.FIELD_NAME"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/google/android/feedback/FeedbackActivity;->getReport()Lcom/google/android/feedback/ExtendedErrorReport;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/feedback/ShowTextActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    const-string v2, "stackTrace"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const v2, 0x7f060034

    invoke-virtual {p0, v2}, Lcom/google/android/feedback/ShowTextActivity;->setTitle(I)V

    iget-object v2, v1, Lcom/google/android/feedback/ExtendedErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    iget-object v2, v2, Landroid/app/ApplicationErrorReport$CrashInfo;->stackTrace:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/google/android/feedback/ShowTextActivity;->setText(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v2, "info"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const v2, 0x7f060037

    invoke-virtual {p0, v2}, Lcom/google/android/feedback/ShowTextActivity;->setTitle(I)V

    iget-object v2, v1, Lcom/google/android/feedback/ExtendedErrorReport;->anrInfo:Landroid/app/ApplicationErrorReport$AnrInfo;

    iget-object v2, v2, Landroid/app/ApplicationErrorReport$AnrInfo;->info:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/google/android/feedback/ShowTextActivity;->setText(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v2, "systemLog"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    const v2, 0x7f060046

    invoke-virtual {p0, v2}, Lcom/google/android/feedback/ShowTextActivity;->setTitle(I)V

    iget-object v2, v1, Lcom/google/android/feedback/ExtendedErrorReport;->systemLog:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/google/android/feedback/ShowTextActivity;->setText(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string v2, "eventLog"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const v2, 0x7f060048

    invoke-virtual {p0, v2}, Lcom/google/android/feedback/ShowTextActivity;->setTitle(I)V

    iget-object v2, v1, Lcom/google/android/feedback/ExtendedErrorReport;->eventLog:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/google/android/feedback/ShowTextActivity;->setText(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const-string v2, "anrStackTraces"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    const v2, 0x7f060049

    invoke-virtual {p0, v2}, Lcom/google/android/feedback/ShowTextActivity;->setTitle(I)V

    iget-object v2, v1, Lcom/google/android/feedback/ExtendedErrorReport;->anrStackTraces:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/google/android/feedback/ShowTextActivity;->setText(Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    const-string v2, "usageDetails"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    const v2, 0x7f06004d

    invoke-virtual {p0, v2}, Lcom/google/android/feedback/ShowTextActivity;->setTitle(I)V

    iget-object v2, v1, Lcom/google/android/feedback/ExtendedErrorReport;->batteryInfo:Landroid/app/ApplicationErrorReport$BatteryInfo;

    iget-object v2, v2, Landroid/app/ApplicationErrorReport$BatteryInfo;->usageDetails:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/google/android/feedback/ShowTextActivity;->setText(Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    const-string v2, "checkinDetails"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    const v2, 0x7f06004e

    invoke-virtual {p0, v2}, Lcom/google/android/feedback/ShowTextActivity;->setTitle(I)V

    iget-object v2, v1, Lcom/google/android/feedback/ExtendedErrorReport;->batteryInfo:Landroid/app/ApplicationErrorReport$BatteryInfo;

    iget-object v2, v2, Landroid/app/ApplicationErrorReport$BatteryInfo;->checkinDetails:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/google/android/feedback/ShowTextActivity;->setText(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    const-string v2, "serviceDetails"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    const v2, 0x7f060055

    invoke-virtual {p0, v2}, Lcom/google/android/feedback/ShowTextActivity;->setTitle(I)V

    iget-object v2, v1, Lcom/google/android/feedback/ExtendedErrorReport;->runningServiceInfo:Landroid/app/ApplicationErrorReport$RunningServiceInfo;

    iget-object v2, v2, Landroid/app/ApplicationErrorReport$RunningServiceInfo;->serviceDetails:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/google/android/feedback/ShowTextActivity;->setText(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "ShowTextActivity unknown EXTRA_FIELD_NAME"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method protected setText(Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;

    const v5, 0x7f08001a

    const v4, 0x7f080018

    const/16 v3, 0x8

    const/4 v2, 0x0

    if-nez p1, :cond_0

    invoke-virtual {p0, v5}, Lcom/google/android/feedback/ShowTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v4}, Lcom/google/android/feedback/ShowTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    const v1, 0x7f08001b

    invoke-virtual {p0, v1}, Lcom/google/android/feedback/ShowTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v4}, Lcom/google/android/feedback/ShowTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v5}, Lcom/google/android/feedback/ShowTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
