.class public Lcom/google/android/gsf/login/AccountIntroActivity;
.super Lcom/google/android/gsf/login/BaseActivity;
.source "AccountIntroActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gsf/login/BaseActivity;-><init>()V

    return-void
.end method

.method private afterAccountIntro(I)V
    .locals 2
    .param p1    # I

    const/4 v1, 0x0

    sparse-switch p1, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    invoke-virtual {p0, v1}, Lcom/google/android/gsf/login/AccountIntroActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->finish()V

    goto :goto_0

    :sswitch_1
    invoke-virtual {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->accountAuthenticatorResultForSkip()V

    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/AccountIntroActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->finish()V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x3f7

    iput v0, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mNextRequest:I

    iget-object v0, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iput-boolean v1, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mCreatingAccount:Z

    invoke-direct {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->checkConnectionAndCheckin()V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x3f6

    iput v0, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mNextRequest:I

    iget-object v0, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mCreatingAccount:Z

    invoke-direct {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->checkConnectionAndCheckin()V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x7 -> :sswitch_1
        0x8 -> :sswitch_2
        0x9 -> :sswitch_3
    .end sparse-switch
.end method

.method private afterWaitForCheckin()V
    .locals 2

    iget v0, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mNextRequest:I

    const/16 v1, 0x3f6

    if-ne v0, v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gsf/login/CreateAccountActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget v1, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mNextRequest:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/login/AccountIntroActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mError:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gsf/login/LoginActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget v1, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mNextRequest:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/login/AccountIntroActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method private afterWifi()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gsf/login/WaitForDeviceCountryActivity;->haveCheckin(Landroid/content/ContentResolver;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gsf/login/WaitForDeviceCountryActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x3f0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/login/AccountIntroActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->afterWaitForCheckin()V

    goto :goto_0
.end method

.method private checkConnectionAndCheckin()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->hasNetworkConnection()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gsf/login/SetupWirelessActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x3fd

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/login/AccountIntroActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->afterWifi()V

    goto :goto_0
.end method

.method private maybeSkipAccountSetup()Z
    .locals 3

    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/gsf/login/AccountIntroActivity;->sTestHooks:Lcom/google/android/gsf/loginservice/BaseActivity$TestHooks;

    iget-boolean v1, v1, Lcom/google/android/gsf/loginservice/BaseActivity$TestHooks;->mSkipExistingAccountCheck:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-boolean v1, v1, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mSetupWizard:Z

    if-eqz v1, :cond_0

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    const-string v2, "com.google"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    array-length v1, v1

    if-lez v1, :cond_0

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/gsf/login/AccountIntroActivity;->setSetupWizardResults(Landroid/content/Intent;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/AccountIntroActivity;->onSetupComplete(Z)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->finish()V

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private setSetupWizardResults(Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Intent;

    if-nez p1, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    :goto_0
    const-string v1, "specialNotificationMsgHtml"

    iget-object v2, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v2, v2, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mOfferMessageHtml:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "nameCompleted"

    iget-object v2, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-boolean v2, v2, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mNameActivityCompleted:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "photoCompleted"

    iget-object v2, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-boolean v2, v2, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mPhotoActivityCompleted:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    sget-object v1, Lcom/google/android/gsf/login/BackendStub$Key;->FIRST_NAME:Lcom/google/android/gsf/login/BackendStub$Key;

    invoke-virtual {v1}, Lcom/google/android/gsf/login/BackendStub$Key;->getWire()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->getUserData()Ljava/util/HashMap;

    move-result-object v1

    sget-object v3, Lcom/google/android/gsf/login/BackendStub$Key;->FIRST_NAME:Lcom/google/android/gsf/login/BackendStub$Key;

    invoke-virtual {v3}, Lcom/google/android/gsf/login/BackendStub$Key;->getWire()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v1, Lcom/google/android/gsf/login/BackendStub$Key;->LAST_NAME:Lcom/google/android/gsf/login/BackendStub$Key;

    invoke-virtual {v1}, Lcom/google/android/gsf/login/BackendStub$Key;->getWire()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->getUserData()Ljava/util/HashMap;

    move-result-object v1

    sget-object v3, Lcom/google/android/gsf/login/BackendStub$Key;->LAST_NAME:Lcom/google/android/gsf/login/BackendStub$Key;

    invoke-virtual {v3}, Lcom/google/android/gsf/login/BackendStub$Key;->getWire()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "mUserData"

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gsf/login/AccountIntroActivity;->setResult(ILandroid/content/Intent;)V

    return-void

    :cond_0
    move-object v0, p1

    goto :goto_0
.end method

.method private startAccountIntro()V
    .locals 3

    const/16 v2, 0x408

    iget-object v0, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mProvisionedEmail:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mProvisionedName:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/android/gsf/login/AccountIntroActivity;->afterAccountIntro(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-boolean v0, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mSetupWizard:Z

    if-eqz v0, :cond_1

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gsf/login/AccountPreIntroUIActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gsf/login/AccountIntroActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gsf/login/AccountIntroUIActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gsf/login/AccountIntroActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v1, 0x0

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gsf/login/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    :goto_0
    return-void

    :sswitch_0
    invoke-direct {p0, p2}, Lcom/google/android/gsf/login/AccountIntroActivity;->afterAccountIntro(I)V

    goto :goto_0

    :sswitch_1
    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iput-object v1, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mProvisionedEmail:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iput-object v1, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mProvisionedName:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->startAccountIntro()V

    goto :goto_0

    :cond_0
    const-string v0, "GLSActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AccountIntro: activity result: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->isSetupWizard()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p3}, Lcom/google/android/gsf/login/AccountIntroActivity;->setSetupWizardResults(Landroid/content/Intent;)V

    :goto_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mCallAuthenticatorResponseOnFinish:Z

    iget-object v0, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mActivities:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    const/4 v0, -0x1

    if-ne p2, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUsername:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/AccountIntroActivity;->accountAuthenticatorResultForAdd(Ljava/lang/String;)V

    :goto_2
    invoke-virtual {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->finish()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p2}, Lcom/google/android/gsf/login/AccountIntroActivity;->setResult(I)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->accountAuthenticatorResultForSkip()V

    goto :goto_2

    :sswitch_2
    invoke-direct {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->afterWaitForCheckin()V

    goto :goto_0

    :sswitch_3
    if-nez p2, :cond_3

    invoke-direct {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->startAccountIntro()V

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->afterWifi()V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x3f0 -> :sswitch_2
        0x3f6 -> :sswitch_1
        0x3f7 -> :sswitch_1
        0x3fd -> :sswitch_3
        0x408 -> :sswitch_0
    .end sparse-switch
.end method

.method public onBackPressed()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/AccountIntroActivity;->setResult(I)V

    invoke-super {p0}, Lcom/google/android/gsf/login/BaseActivity;->onBackPressed()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/gsf/login/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->maybeSkipAccountSetup()Z

    move-result v6

    if-eqz v6, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v6, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-boolean v6, v6, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mSetupWizard:Z

    if-eqz v6, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "google_setup:provisioned_info"

    invoke-static {v6, v7}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v5}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v6, "purchaser_gaia_email"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v6, "purchaser_name"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iput-object v1, v6, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mProvisionedEmail:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iput-object v4, v6, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mProvisionedName:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_1
    if-nez p1, :cond_0

    const-string v6, "GLSActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Starting account intro "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->startAccountIntro()V

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v6, "GLSActivity"

    const-string v7, "Unable to read providionedInfo."

    invoke-static {v6, v7, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :cond_3
    if-eqz v2, :cond_2

    const-string v6, "purchaser_gaia_email"

    invoke-virtual {v2, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v6, "purchaser_name"

    invoke-virtual {v2, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iput-object v1, v6, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mProvisionedEmail:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iput-object v4, v6, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mProvisionedName:Ljava/lang/String;

    goto :goto_1
.end method
