.class public Lcom/google/android/gsf/login/AddAccountActivity;
.super Lcom/google/android/gsf/login/BaseActivity;
.source "AddAccountActivity.java"


# static fields
.field private static BEFORE_PROFILE:I

.field private static PROFILE_PROGRESS:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, -0x1

    sput v0, Lcom/google/android/gsf/login/AddAccountActivity;->BEFORE_PROFILE:I

    const/4 v0, 0x0

    sput v0, Lcom/google/android/gsf/login/AddAccountActivity;->PROFILE_PROGRESS:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gsf/login/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public afterAddAccount()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/gsf/login/AddAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-boolean v0, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAgreedToPlayTos:Z

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.vending.TOS_ACKED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.android.vending"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "TosAckedReceiver.account"

    iget-object v2, p0, Lcom/google/android/gsf/login/AddAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v2, v2, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUsername:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "TosAckedReceiver.optIn"

    iget-object v2, p0, Lcom/google/android/gsf/login/AddAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-boolean v2, v2, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAgreedToPlayEmail:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "com.android.vending.TOS_ACKED"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/login/AddAccountActivity;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gsf/login/AddAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-boolean v0, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAgreedToChromeTosAndPrivacy:Z

    if-eqz v0, :cond_1

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.chrome.TOS_ACKED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.android.chrome"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "TosAckedReceiver.account"

    iget-object v2, p0, Lcom/google/android/gsf/login/AddAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v2, v2, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUsername:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.android.chrome.TOS_ACKED"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/login/AddAccountActivity;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/gsf/login/AddAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-boolean v0, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUserSelectedGooglePlus:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gsf/login/AddAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget v0, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mProfileResult:I

    sget v1, Lcom/google/android/gsf/login/AddAccountActivity;->BEFORE_PROFILE:I

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/gsf/login/AddAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    sget v1, Lcom/google/android/gsf/login/AddAccountActivity;->PROFILE_PROGRESS:I

    iput v1, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mProfileResult:I

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gsf/login/ProfileTask;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x401

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/login/AddAccountActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/gsf/login/AddAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-boolean v0, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mShowOffer:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gsf/login/AddAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mOfferIntent:Landroid/content/Intent;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/google/android/gsf/login/AddAccountActivity;->sTestHooks:Lcom/google/android/gsf/loginservice/BaseActivity$TestHooks;

    iget-boolean v0, v0, Lcom/google/android/gsf/loginservice/BaseActivity$TestHooks;->mSkipCreditCard:Z

    if-nez v0, :cond_4

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gsf/login/AddAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mOfferIntent:Landroid/content/Intent;

    sget-object v1, Lcom/google/android/gsf/login/AddAccountActivity;->sTestHooks:Lcom/google/android/gsf/loginservice/BaseActivity$TestHooks;

    iget-object v1, v1, Lcom/google/android/gsf/loginservice/BaseActivity$TestHooks;->mMarketIntent:Landroid/content/Intent;

    if-eqz v1, :cond_5

    sget-object v0, Lcom/google/android/gsf/login/AddAccountActivity;->sTestHooks:Lcom/google/android/gsf/loginservice/BaseActivity$TestHooks;

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/BaseActivity$TestHooks;->mMarketIntent:Landroid/content/Intent;

    move-object v2, v0

    :goto_1
    iget-object v0, p0, Lcom/google/android/gsf/login/AddAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUserData:Ljava/util/HashMap;

    sget-object v1, Lcom/google/android/gsf/login/BackendStub$Key;->FIRST_NAME:Lcom/google/android/gsf/login/BackendStub$Key;

    invoke-virtual {v1}, Lcom/google/android/gsf/login/BackendStub$Key;->getWire()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gsf/login/AddAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v1, v1, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUserData:Ljava/util/HashMap;

    sget-object v3, Lcom/google/android/gsf/login/BackendStub$Key;->LAST_NAME:Lcom/google/android/gsf/login/BackendStub$Key;

    invoke-virtual {v3}, Lcom/google/android/gsf/login/BackendStub$Key;->getWire()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    const-string v3, "cardholder_name"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_3
    const-string v0, "on_initial_setup"

    iget-object v1, p0, Lcom/google/android/gsf/login/AddAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-boolean v1, v1, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mSetupWizard:Z

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/16 v0, 0x3fc

    invoke-virtual {p0, v2, v0}, Lcom/google/android/gsf/login/AddAccountActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    :cond_4
    :goto_2
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gsf/login/SyncIntroActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x3ed

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/login/AddAccountActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_2

    :cond_5
    move-object v2, v0

    goto :goto_1
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    const/4 v0, 0x1

    const/4 v2, -0x1

    if-ne p2, v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/AddAccountActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/AddAccountActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    sparse-switch p1, :sswitch_data_0

    const-string v0, "GLSActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown activity result req="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " rc="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gsf/login/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0

    :sswitch_0
    if-eq p2, v2, :cond_1

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gsf/login/ShowErrorActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x40a

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/login/AddAccountActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gsf/login/AddAccountActivity;->afterAddAccount()V

    goto :goto_0

    :sswitch_1
    invoke-virtual {p0}, Lcom/google/android/gsf/login/AddAccountActivity;->afterAddAccount()V

    goto :goto_0

    :sswitch_2
    if-eqz p3, :cond_2

    iget-object v0, p0, Lcom/google/android/gsf/login/AddAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    const-string v1, "redeemed_offer_message_html"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mOfferMessageHtml:Ljava/lang/String;

    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gsf/login/SyncIntroActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x3ed

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/login/AddAccountActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/AddAccountActivity;->onSetupComplete(Z)V

    iget-object v0, p0, Lcom/google/android/gsf/login/AddAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mOfferMessageHtml:Ljava/lang/String;

    if-eqz v0, :cond_4

    if-eqz p3, :cond_3

    :goto_1
    const-string v0, "specialNotificationMsgHtml"

    iget-object v1, p0, Lcom/google/android/gsf/login/AddAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v1, v1, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mOfferMessageHtml:Ljava/lang/String;

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v2, p3}, Lcom/google/android/gsf/login/AddAccountActivity;->setResult(ILandroid/content/Intent;)V

    :goto_2
    invoke-virtual {p0}, Lcom/google/android/gsf/login/AddAccountActivity;->finish()V

    goto :goto_0

    :cond_3
    new-instance p3, Landroid/content/Intent;

    invoke-direct {p3}, Landroid/content/Intent;-><init>()V

    goto :goto_1

    :cond_4
    invoke-virtual {p0, v2, p3}, Lcom/google/android/gsf/login/AddAccountActivity;->setResult(ILandroid/content/Intent;)V

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x3ed -> :sswitch_3
        0x3fc -> :sswitch_2
        0x401 -> :sswitch_0
        0x40a -> :sswitch_1
    .end sparse-switch
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 0
    .param p1    # Landroid/content/Intent;
    .param p2    # I

    iput p2, p0, Lcom/google/android/gsf/login/AddAccountActivity;->mNextRequest:I

    invoke-super {p0, p1, p2}, Lcom/google/android/gsf/login/BaseActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method
