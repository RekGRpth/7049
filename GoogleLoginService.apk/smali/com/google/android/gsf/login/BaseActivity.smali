.class public Lcom/google/android/gsf/login/BaseActivity;
.super Lcom/google/android/gsf/loginservice/BaseActivity;
.source "BaseActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Lcom/google/android/gsf/login/BottomScrollView$BottomScrollListener;


# static fields
.field private static final sWirelessSettingsIntent:Landroid/content/Intent;


# instance fields
.field protected LOCAL_LOGV:Z

.field mAgreementView:Landroid/app/AlertDialog;

.field protected mAllowBackHardKey:Z

.field protected mBackButtonClickListener:Landroid/view/View$OnClickListener;

.field protected mBackendStub:Lcom/google/android/gsf/login/BackendStub;

.field protected mFrameLayout:Landroid/widget/FrameLayout;

.field private mHandler:Landroid/os/Handler;

.field protected mLastResult:I

.field private mMarket:Lcom/google/android/gsf/login/MarketHelper;

.field protected mNextRequest:I

.field private mOnDefaultButtonClicked:Landroid/view/View$OnClickListener;

.field protected mPrimaryButton:Landroid/view/View;

.field protected mScrollDownButtonClickListener:Landroid/view/View$OnClickListener;

.field protected mScrollUpButtonClickListener:Landroid/view/View$OnClickListener;

.field protected mScrollView:Lcom/google/android/gsf/login/BottomScrollView;

.field private mStartOnEnterActionListener:Landroid/widget/TextView$OnEditorActionListener;

.field protected mTitle:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.android.phone"

    const-string v2, "com.android.phone.Settings"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    sput-object v0, Lcom/google/android/gsf/login/BaseActivity;->sWirelessSettingsIntent:Landroid/content/Intent;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/gsf/loginservice/BaseActivity;-><init>()V

    const-string v0, "GLSActivity"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gsf/login/BaseActivity;->LOCAL_LOGV:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gsf/login/BaseActivity;->mAllowBackHardKey:Z

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/gsf/login/BaseActivity;->mHandler:Landroid/os/Handler;

    const/16 v0, -0x65

    iput v0, p0, Lcom/google/android/gsf/login/BaseActivity;->mLastResult:I

    new-instance v0, Lcom/google/android/gsf/login/BaseActivity$3;

    invoke-direct {v0, p0}, Lcom/google/android/gsf/login/BaseActivity$3;-><init>(Lcom/google/android/gsf/login/BaseActivity;)V

    iput-object v0, p0, Lcom/google/android/gsf/login/BaseActivity;->mBackButtonClickListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/google/android/gsf/login/BaseActivity$4;

    invoke-direct {v0, p0}, Lcom/google/android/gsf/login/BaseActivity$4;-><init>(Lcom/google/android/gsf/login/BaseActivity;)V

    iput-object v0, p0, Lcom/google/android/gsf/login/BaseActivity;->mScrollDownButtonClickListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/google/android/gsf/login/BaseActivity$5;

    invoke-direct {v0, p0}, Lcom/google/android/gsf/login/BaseActivity$5;-><init>(Lcom/google/android/gsf/login/BaseActivity;)V

    iput-object v0, p0, Lcom/google/android/gsf/login/BaseActivity;->mScrollUpButtonClickListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/google/android/gsf/login/BaseActivity$6;

    invoke-direct {v0, p0}, Lcom/google/android/gsf/login/BaseActivity$6;-><init>(Lcom/google/android/gsf/login/BaseActivity;)V

    iput-object v0, p0, Lcom/google/android/gsf/login/BaseActivity;->mOnDefaultButtonClicked:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/google/android/gsf/login/BaseActivity$7;

    invoke-direct {v0, p0}, Lcom/google/android/gsf/login/BaseActivity$7;-><init>(Lcom/google/android/gsf/login/BaseActivity;)V

    iput-object v0, p0, Lcom/google/android/gsf/login/BaseActivity;->mStartOnEnterActionListener:Landroid/widget/TextView$OnEditorActionListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/gsf/login/BaseActivity;)Landroid/view/View$OnClickListener;
    .locals 1
    .param p0    # Lcom/google/android/gsf/login/BaseActivity;

    iget-object v0, p0, Lcom/google/android/gsf/login/BaseActivity;->mOnDefaultButtonClicked:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public static isDomainNameValid(Ljava/lang/String;)Z
    .locals 3
    .param p0    # Ljava/lang/String;

    const/16 v2, 0x2e

    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-lez v0, :cond_0

    const/16 v0, 0x40

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected static isFirstGoogleAccount(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/gsf/login/BaseActivity;->sTestHooks:Lcom/google/android/gsf/loginservice/BaseActivity$TestHooks;

    iget-boolean v2, v2, Lcom/google/android/gsf/loginservice/BaseActivity$TestHooks;->mSkipExistingAccountCheck:Z

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    const-string v3, "com.google"

    invoke-virtual {v2, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    array-length v2, v0

    if-eq v2, v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isUsernameValid(Ljava/lang/String;)Z
    .locals 3
    .param p0    # Ljava/lang/String;

    sget-object v1, Landroid/util/Patterns;->EMAIL_ADDRESS:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->start()I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->end()I

    move-result v1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1    # Landroid/text/Editable;

    invoke-virtual {p0}, Lcom/google/android/gsf/login/BaseActivity;->updateWidgetState()V

    return-void
.end method

.method public appendGmailHost(Landroid/content/res/Resources;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Landroid/content/res/Resources;
    .param p2    # Ljava/lang/String;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 p2, 0x0

    :cond_0
    :goto_0
    return-object p2

    :cond_1
    const-string v0, "@"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1}, Lcom/google/android/gsf/login/BaseActivity;->getGmailHost(Landroid/content/res/Resources;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public createErrorIntent(Lcom/google/android/gsf/loginservice/GLSUser$Status;)Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v1, Lcom/google/android/gsf/login/ShowErrorActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gsf/login/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    invoke-virtual {v1, v0}, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->addSession(Landroid/content/Intent;)V

    invoke-virtual {p1, p0, v0}, Lcom/google/android/gsf/loginservice/GLSUser$Status;->toIntent(Landroid/content/Context;Landroid/content/Intent;)V

    return-object v0
.end method

.method protected disableBackKey()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 8
    .param p1    # Landroid/view/KeyEvent;

    const/4 v5, 0x0

    const/4 v4, 0x1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v6

    const/4 v7, 0x4

    if-ne v6, v7, :cond_1

    move v0, v4

    :goto_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v6

    const/4 v7, 0x5

    if-ne v6, v7, :cond_2

    move v1, v4

    :goto_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v6

    const/16 v7, 0x18

    if-ne v6, v7, :cond_3

    move v3, v4

    :goto_2
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v6

    const/16 v7, 0x19

    if-ne v6, v7, :cond_4

    move v2, v4

    :goto_3
    invoke-virtual {p0}, Lcom/google/android/gsf/login/BaseActivity;->isSetupWizard()Z

    move-result v5

    if-nez v5, :cond_5

    invoke-super {p0, p1}, Lcom/google/android/gsf/loginservice/BaseActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v4

    :cond_0
    :goto_4
    return v4

    :cond_1
    move v0, v5

    goto :goto_0

    :cond_2
    move v1, v5

    goto :goto_1

    :cond_3
    move v3, v5

    goto :goto_2

    :cond_4
    move v2, v5

    goto :goto_3

    :cond_5
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isSystem()Z

    move-result v5

    if-eqz v5, :cond_6

    if-nez v1, :cond_6

    if-nez v3, :cond_6

    if-nez v2, :cond_6

    if-eqz v0, :cond_0

    iget-boolean v5, p0, Lcom/google/android/gsf/login/BaseActivity;->mAllowBackHardKey:Z

    if-eqz v5, :cond_0

    :cond_6
    invoke-super {p0, p1}, Lcom/google/android/gsf/loginservice/BaseActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v4

    goto :goto_4
.end method

.method protected getActivityContentView()Landroid/view/View;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gsf/login/BaseActivity;->mFrameLayout:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/login/BaseActivity;->mFrameLayout:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getGmailHost(Landroid/content/res/Resources;Z)Ljava/lang/String;
    .locals 4
    .param p1    # Landroid/content/res/Resources;
    .param p2    # Z

    if-eqz p2, :cond_1

    const v2, 0x7f0800c5

    :goto_0
    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gsf/login/Compat;->isWifiOnlyBuild()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gsf/login/BaseActivity;->getCountry()Ljava/lang/String;

    move-result-object v0

    const-string v2, "de"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz p2, :cond_2

    const v2, 0x7f0800c6

    :goto_1
    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    :cond_0
    if-nez v1, :cond_3

    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Couldn\'t find gmail_host_name"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    const v2, 0x7f08000f

    goto :goto_0

    :cond_2
    const v2, 0x7f080010

    goto :goto_1

    :cond_3
    return-object v1
.end method

.method protected getGooglePlayOptInDefault()Z
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/gsf/login/BaseActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "google_setup:play_email_opt_in"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getMarket()Lcom/google/android/gsf/login/MarketHelper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/login/BaseActivity;->mMarket:Lcom/google/android/gsf/login/MarketHelper;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gsf/login/MarketHelper;

    invoke-direct {v0, p0}, Lcom/google/android/gsf/login/MarketHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gsf/login/BaseActivity;->mMarket:Lcom/google/android/gsf/login/MarketHelper;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gsf/login/BaseActivity;->mMarket:Lcom/google/android/gsf/login/MarketHelper;

    return-object v0
.end method

.method public getTitleId()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getUserData()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gsf/login/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUserData:Ljava/util/HashMap;

    return-object v0
.end method

.method protected hasMenu()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected isChromeInstalled()Z
    .locals 5

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gsf/login/BaseActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.android.chrome"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected isSetupWizard()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/login/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-boolean v0, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mSetupWizard:Z

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const v4, 0x7f020012

    const/4 v3, 0x0

    invoke-super {p0, p1}, Lcom/google/android/gsf/loginservice/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/BaseActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v1, Lcom/google/android/gsf/login/BackendStub;

    invoke-direct {v1, p0}, Lcom/google/android/gsf/login/BackendStub;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/gsf/login/BaseActivity;->mBackendStub:Lcom/google/android/gsf/login/BackendStub;

    invoke-virtual {p0}, Lcom/google/android/gsf/login/BaseActivity;->useActionBars()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {p0}, Lcom/google/android/gsf/login/BaseActivity;->hasMenu()Z

    move-result v1

    if-eqz v1, :cond_6

    const/16 v1, 0x9

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/login/BaseActivity;->requestWindowFeature(I)Z

    invoke-static {p0, v3}, Lcom/google/android/gsf/login/Compat;->actionBarSetDisplayShowHomeEnabled(Landroid/app/Activity;Z)V

    invoke-static {p0, v3}, Lcom/google/android/gsf/login/Compat;->actionBarSetDisplayShowTitleEnabled(Landroid/app/Activity;Z)V

    :goto_1
    new-instance v1, Landroid/widget/FrameLayout;

    invoke-direct {v1, p0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/gsf/login/BaseActivity;->mFrameLayout:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/gsf/login/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-boolean v1, v1, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mSetupWizard:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gsf/login/BaseActivity;->mFrameLayout:Landroid/widget/FrameLayout;

    const/high16 v2, 0x1e40000

    invoke-static {v1, v2}, Lcom/google/android/gsf/login/Compat;->viewSetSystemUiVisibility(Landroid/widget/FrameLayout;I)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/BaseActivity;->disableBackKey()Z

    move-result v1

    if-eqz v1, :cond_1

    iput-boolean v3, p0, Lcom/google/android/gsf/login/BaseActivity;->mAllowBackHardKey:Z

    :cond_1
    if-eqz p1, :cond_2

    const-string v1, "nextRequest"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/gsf/login/BaseActivity;->mNextRequest:I

    :cond_2
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-le v1, v2, :cond_5

    const v1, 0x7f0b000a

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/login/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_3

    const v1, 0x7f02000c

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_3
    const v1, 0x7f0b0013

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/login/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0, v4}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_4
    const v1, 0x7f0b001c

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/login/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v0, v4}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_5
    iget-object v1, p0, Lcom/google/android/gsf/login/BaseActivity;->mFrameLayout:Landroid/widget/FrameLayout;

    invoke-super {p0, v1}, Lcom/google/android/gsf/loginservice/BaseActivity;->setContentView(Landroid/view/View;)V

    goto :goto_0

    :cond_6
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/login/BaseActivity;->requestWindowFeature(I)Z

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/gsf/loginservice/BaseActivity;->onDestroy()V

    iget-object v0, p0, Lcom/google/android/gsf/login/BaseActivity;->mMarket:Lcom/google/android/gsf/login/MarketHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/login/BaseActivity;->mMarket:Lcom/google/android/gsf/login/MarketHelper;

    invoke-virtual {v0}, Lcom/google/android/gsf/login/MarketHelper;->onDestroy()V

    :cond_0
    return-void
.end method

.method public onRequiresScroll()V
    .locals 4

    const/4 v3, 0x0

    const v2, 0x7f0b0014

    invoke-virtual {p0, v2}, Lcom/google/android/gsf/login/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/gsf/login/BaseActivity;->mScrollDownButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    const v2, 0x7f0b000a

    invoke-virtual {p0, v2}, Lcom/google/android/gsf/login/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    if-eqz v1, :cond_2

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/4 v3, -0x1

    invoke-super {p0, p1}, Lcom/google/android/gsf/loginservice/BaseActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    const-string v2, "currentFocus"

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-eq v0, v3, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/gsf/loginservice/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/BaseActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    const-string v2, "currentFocus"

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v1

    :goto_0
    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "nextRequest"

    iget v2, p0, Lcom/google/android/gsf/login/BaseActivity;->mNextRequest:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public onScrolledToBottom()V
    .locals 3

    const v2, 0x7f0b0014

    invoke-virtual {p0, v2}, Lcom/google/android/gsf/login/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    const v2, 0x7f0b000a

    invoke-virtual {p0, v2}, Lcom/google/android/gsf/login/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    return-void
.end method

.method protected onSetupComplete(Z)V
    .locals 3
    .param p1    # Z

    const v2, 0x320cf

    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    invoke-static {v2, v1}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    sget-object v1, Lcom/google/android/gsf/login/BackendStub$Key;->SHARED_PREFS:Lcom/google/android/gsf/login/BackendStub$Key;

    invoke-virtual {v1}, Lcom/google/android/gsf/login/BackendStub$Key;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gsf/login/BaseActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    invoke-static {v0}, Lcom/google/android/gsf/login/SharedPreferencesCompat;->apply(Landroid/content/SharedPreferences$Editor;)V

    return-void
.end method

.method protected onStart()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/gsf/loginservice/BaseActivity;->onStart()V

    const v0, 0x7f0b000e

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gsf/login/BottomScrollView;

    iput-object v0, p0, Lcom/google/android/gsf/login/BaseActivity;->mScrollView:Lcom/google/android/gsf/login/BottomScrollView;

    iget-object v0, p0, Lcom/google/android/gsf/login/BaseActivity;->mScrollView:Lcom/google/android/gsf/login/BottomScrollView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/login/BaseActivity;->mScrollView:Lcom/google/android/gsf/login/BottomScrollView;

    invoke-virtual {v0, p0}, Lcom/google/android/gsf/login/BottomScrollView;->setBottomScrollListener(Lcom/google/android/gsf/login/BottomScrollView$BottomScrollListener;)V

    :cond_0
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method protected overrideAllowBackHardkey()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gsf/login/BaseActivity;->mAllowBackHardKey:Z

    return-void
.end method

.method public returnResult(I)V
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gsf/login/BaseActivity;->returnResult(ILandroid/content/Intent;)V

    return-void
.end method

.method public returnResult(ILandroid/content/Intent;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/content/Intent;

    iput p1, p0, Lcom/google/android/gsf/login/BaseActivity;->mLastResult:I

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gsf/login/BaseActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/BaseActivity;->finish()V

    return-void
.end method

.method protected setBackButton(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/login/BaseActivity;->mBackButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gsf/login/BaseActivity;->mAllowBackHardKey:Z

    :cond_0
    return-void
.end method

.method public setContentView(I)V
    .locals 3
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/gsf/login/BaseActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/login/BaseActivity;->setContentView(Landroid/view/View;)V

    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gsf/login/BaseActivity;->mFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/BaseActivity;->updateTitle()V

    return-void
.end method

.method protected setDefaultButton(Landroid/view/View;Z)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Z

    if-eqz p2, :cond_0

    iput-object p1, p0, Lcom/google/android/gsf/login/BaseActivity;->mPrimaryButton:Landroid/view/View;

    :cond_0
    instance-of v0, p1, Landroid/widget/EditText;

    if-eqz v0, :cond_1

    check-cast p1, Landroid/widget/EditText;

    iget-object v0, p0, Lcom/google/android/gsf/login/BaseActivity;->mStartOnEnterActionListener:Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gsf/login/BaseActivity;->mOnDefaultButtonClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public setTitle(I)V
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/google/android/gsf/login/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/BaseActivity;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    const v0, 0x7f0b0002

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gsf/login/BaseActivity;->mTitle:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/gsf/login/BaseActivity;->mTitle:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/login/BaseActivity;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gsf/loginservice/BaseActivity;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected showAgreement(Lcom/google/android/gsf/login/LinkSpan$AndroidPolicy;)V
    .locals 1
    .param p1    # Lcom/google/android/gsf/login/LinkSpan$AndroidPolicy;

    new-instance v0, Lcom/google/android/gsf/login/LinkSpan$WebViewDialog;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gsf/login/LinkSpan$WebViewDialog;-><init>(Lcom/google/android/gsf/login/BaseActivity;Lcom/google/android/gsf/login/LinkSpan$AndroidPolicy;)V

    iput-object v0, p0, Lcom/google/android/gsf/login/BaseActivity;->mAgreementView:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/google/android/gsf/login/BaseActivity;->mAgreementView:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method protected start()V
    .locals 0

    return-void
.end method

.method protected updateTitle()V
    .locals 1

    const v0, 0x7f0b0002

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gsf/login/BaseActivity;->mTitle:Landroid/widget/TextView;

    return-void
.end method

.method public updateWidgetState()V
    .locals 0

    return-void
.end method

.method protected useActionBars()Z
    .locals 2

    const/4 v0, 0x0

    invoke-static {}, Lcom/google/android/gsf/login/Compat;->hasActionBar()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {p0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gsf/login/Compat;->hasPermanentMenuKey(Landroid/view/ViewConfiguration;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected validateDomainNameOnly(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v3, 0x1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {p1}, Lcom/google/android/gsf/login/BaseActivity;->isDomainNameValid(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-object p1

    :cond_0
    const-string v1, "@"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    aget-object v1, v0, v3

    invoke-static {v1}, Lcom/google/android/gsf/login/BaseActivity;->isDomainNameValid(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    aget-object p1, v0, v3

    goto :goto_0

    :cond_1
    const-string p1, ""

    goto :goto_0
.end method

.method protected validateUsername(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/gsf/login/BaseActivity;->isUsernameValid(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-object p1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gsf/login/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p0, v1, p1}, Lcom/google/android/gsf/login/BaseActivity;->appendGmailHost(Landroid/content/res/Resources;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gsf/login/BaseActivity;->isUsernameValid(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :goto_1
    move-object p1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
