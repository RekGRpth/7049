.class public Lcom/google/android/gsf/login/BrowserActivity;
.super Lcom/google/android/gsf/login/BaseActivity;
.source "BrowserActivity.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gsf/login/BrowserActivity$MyWebViewClient;,
        Lcom/google/android/gsf/login/BrowserActivity$MyChromeClient;
    }
.end annotation


# static fields
.field public static OAUTH_START_URL:Ljava/lang/String;

.field private static sGlsJS:Ljava/lang/Object;

.field public static sTestBrowser:Lcom/google/android/gsf/login/BrowserActivity;


# instance fields
.field private mBackgroundTaskStarted:Z

.field private mCookieManager:Landroid/webkit/CookieManager;

.field private mInteractivityCompleted:Z

.field private mIsInitialLoad:Z

.field private mIsLoading:Z

.field public mLastLoaded:Ljava/lang/String;

.field private mWaitingForNetwork:Z

.field mWebView:Lcom/google/android/gsf/login/CustomWebView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "https://accounts.google.com/o/android/auth?"

    sput-object v0, Lcom/google/android/gsf/login/BrowserActivity;->OAUTH_START_URL:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/android/gsf/login/BaseActivity;-><init>()V

    iput-boolean v0, p0, Lcom/google/android/gsf/login/BrowserActivity;->mInteractivityCompleted:Z

    iput-boolean v0, p0, Lcom/google/android/gsf/login/BrowserActivity;->mBackgroundTaskStarted:Z

    iput-boolean v0, p0, Lcom/google/android/gsf/login/BrowserActivity;->mWaitingForNetwork:Z

    return-void
.end method

.method static synthetic access$202(Lcom/google/android/gsf/login/BrowserActivity;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gsf/login/BrowserActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gsf/login/BrowserActivity;->mIsLoading:Z

    return p1
.end method

.method static synthetic access$302(Lcom/google/android/gsf/login/BrowserActivity;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gsf/login/BrowserActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gsf/login/BrowserActivity;->mIsInitialLoad:Z

    return p1
.end method

.method private setupOptionsAndCallbacks()V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/google/android/gsf/login/BrowserActivity;->mWebView:Lcom/google/android/gsf/login/CustomWebView;

    new-instance v2, Lcom/google/android/gsf/login/BrowserActivity$MyWebViewClient;

    invoke-direct {v2, p0, v5}, Lcom/google/android/gsf/login/BrowserActivity$MyWebViewClient;-><init>(Lcom/google/android/gsf/login/BrowserActivity;Lcom/google/android/gsf/login/BrowserActivity$1;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gsf/login/CustomWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    iget-object v1, p0, Lcom/google/android/gsf/login/BrowserActivity;->mWebView:Lcom/google/android/gsf/login/CustomWebView;

    new-instance v2, Lcom/google/android/gsf/login/BrowserActivity$MyChromeClient;

    invoke-direct {v2, p0, v5}, Lcom/google/android/gsf/login/BrowserActivity$MyChromeClient;-><init>(Lcom/google/android/gsf/login/BrowserActivity;Lcom/google/android/gsf/login/BrowserActivity$1;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gsf/login/CustomWebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    iget-object v1, p0, Lcom/google/android/gsf/login/BrowserActivity;->mWebView:Lcom/google/android/gsf/login/CustomWebView;

    invoke-virtual {v1}, Lcom/google/android/gsf/login/CustomWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setSupportMultipleWindows(Z)V

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setSaveFormData(Z)V

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setSavePassword(Z)V

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setDatabaseEnabled(Z)V

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setJavaScriptCanOpenWindowsAutomatically(Z)V

    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setLoadsImagesAutomatically(Z)V

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setLightTouchEnabled(Z)V

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setNeedInitialFocus(Z)V

    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    iget-object v1, p0, Lcom/google/android/gsf/login/BrowserActivity;->mWebView:Lcom/google/android/gsf/login/CustomWebView;

    invoke-virtual {v1, v3}, Lcom/google/android/gsf/login/CustomWebView;->setMapTrackballToArrowKeys(Z)V

    iget-object v1, p0, Lcom/google/android/gsf/login/BrowserActivity;->mWebView:Lcom/google/android/gsf/login/CustomWebView;

    invoke-virtual {v1, v4}, Lcom/google/android/gsf/login/CustomWebView;->setFocusable(Z)V

    iget-object v1, p0, Lcom/google/android/gsf/login/BrowserActivity;->mWebView:Lcom/google/android/gsf/login/CustomWebView;

    invoke-virtual {v1, v4}, Lcom/google/android/gsf/login/CustomWebView;->setFocusableInTouchMode(Z)V

    return-void
.end method


# virtual methods
.method protected createWidgets()V
    .locals 3

    const/4 v1, 0x0

    const v0, 0x7f0b0017

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/BrowserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gsf/login/CustomWebView;

    iput-object v0, p0, Lcom/google/android/gsf/login/BrowserActivity;->mWebView:Lcom/google/android/gsf/login/CustomWebView;

    invoke-static {p0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gsf/login/BrowserActivity;->mCookieManager:Landroid/webkit/CookieManager;

    invoke-direct {p0}, Lcom/google/android/gsf/login/BrowserActivity;->setupOptionsAndCallbacks()V

    iget-object v0, p0, Lcom/google/android/gsf/login/BrowserActivity;->mWebView:Lcom/google/android/gsf/login/CustomWebView;

    invoke-virtual {v0}, Lcom/google/android/gsf/login/CustomWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    iget-object v2, p0, Lcom/google/android/gsf/login/BrowserActivity;->mWebView:Lcom/google/android/gsf/login/CustomWebView;

    iget-object v0, p0, Lcom/google/android/gsf/login/BrowserActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-boolean v0, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mSetupWizard:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/android/gsf/login/CustomWebView;->allowLongClicks(Z)V

    sget-object v0, Lcom/google/android/gsf/login/BrowserActivity;->sGlsJS:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/login/BrowserActivity;->mWebView:Lcom/google/android/gsf/login/CustomWebView;

    sget-object v1, Lcom/google/android/gsf/login/BrowserActivity;->sGlsJS:Ljava/lang/Object;

    const-string v2, "gls"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gsf/login/CustomWebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_0
    sput-object p0, Lcom/google/android/gsf/login/BrowserActivity;->sTestBrowser:Lcom/google/android/gsf/login/BrowserActivity;

    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method protected disableBackKey()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # Landroid/view/KeyEvent;

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/login/BrowserActivity;->mWebView:Lcom/google/android/gsf/login/CustomWebView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/login/BrowserActivity;->mWebView:Lcom/google/android/gsf/login/CustomWebView;

    invoke-virtual {v0}, Lcom/google/android/gsf/login/CustomWebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/login/BrowserActivity;->mWebView:Lcom/google/android/gsf/login/CustomWebView;

    invoke-virtual {v0}, Lcom/google/android/gsf/login/CustomWebView;->goBack()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gsf/login/BaseActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public getTitleId()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/login/BrowserActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUsername:Ljava/lang/String;

    if-eqz v0, :cond_0

    const v0, 0x7f0800bc

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0800bd

    goto :goto_0
.end method

.method protected hasMenu()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public loadUrl(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/gsf/login/BrowserActivity$2;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gsf/login/BrowserActivity$2;-><init>(Lcom/google/android/gsf/login/BrowserActivity;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/BrowserActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/BrowserActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/BrowserActivity;->finish()V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1    # Landroid/content/res/Configuration;

    invoke-super {p0, p1}, Lcom/google/android/gsf/login/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lcom/google/android/gsf/login/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030006

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/BrowserActivity;->setContentView(I)V

    if-eqz p1, :cond_0

    const-string v0, "interactivity_completed"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gsf/login/BrowserActivity;->mInteractivityCompleted:Z

    const-string v0, "waiting_for_network"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gsf/login/BrowserActivity;->mWaitingForNetwork:Z

    const-string v0, "background_task_started"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gsf/login/BrowserActivity;->mBackgroundTaskStarted:Z

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gsf/login/BrowserActivity;->createWidgets()V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/BrowserActivity;->startWebLogin()V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/BrowserActivity;->getTitleId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/BrowserActivity;->setTitle(I)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1    # Landroid/view/Menu;

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/google/android/gsf/login/BaseActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    const v1, 0x7f08006d

    invoke-interface {p1, v2, v2, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f02002c

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/gsf/login/Compat;->menuItemSetShowAsAction(Landroid/view/MenuItem;I)V

    const/4 v1, 0x1

    return v1
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 0
    .param p1    # Landroid/view/View;
    .param p2    # Z

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    invoke-super {p0, p1}, Lcom/google/android/gsf/login/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    :pswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/BrowserActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/BrowserActivity;->finish()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onPageLoadFinished(Ljava/lang/String;)V
    .locals 14
    .param p1    # Ljava/lang/String;

    const/4 v9, 0x0

    const/4 v10, 0x1

    iput-object p1, p0, Lcom/google/android/gsf/login/BrowserActivity;->mLastLoaded:Ljava/lang/String;

    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v11

    invoke-virtual {v11, p1}, Landroid/webkit/CookieManager;->getCookie(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x0

    if-eqz v3, :cond_0

    const-string v11, "\\;"

    invoke-virtual {v3, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v6, v1

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v6, :cond_0

    aget-object v2, v1, v5

    const/16 v11, 0x3d

    invoke-virtual {v2, v11}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    if-ltz v4, :cond_4

    invoke-virtual {v2, v9, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    add-int/lit8 v11, v4, 0x1

    invoke-virtual {v2, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    const-string v11, "oauth_token"

    invoke-virtual {v11, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_4

    move-object v0, v8

    :cond_0
    if-eqz v0, :cond_1

    iput-boolean v10, p0, Lcom/google/android/gsf/login/BrowserActivity;->mInteractivityCompleted:Z

    :cond_1
    invoke-static {p0}, Lcom/google/android/gsf/loginservice/GLSUser;->getGLSContext(Landroid/content/Context;)Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    move-result-object v11

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "onPageLoadFinished():  access token="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    if-eqz v0, :cond_2

    move v9, v10

    :cond_2
    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v12, " "

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v11, v9}, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->log(Ljava/lang/String;)V

    iget-boolean v9, p0, Lcom/google/android/gsf/login/BrowserActivity;->mInteractivityCompleted:Z

    if-eqz v9, :cond_3

    iget-boolean v9, p0, Lcom/google/android/gsf/login/BrowserActivity;->mBackgroundTaskStarted:Z

    if-nez v9, :cond_3

    iput-boolean v10, p0, Lcom/google/android/gsf/login/BrowserActivity;->mBackgroundTaskStarted:Z

    iget-object v9, p0, Lcom/google/android/gsf/login/BrowserActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iput-object v0, v9, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAccessToken:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/gsf/login/BrowserActivity;->mWebView:Lcom/google/android/gsf/login/CustomWebView;

    invoke-virtual {v9}, Lcom/google/android/gsf/login/CustomWebView;->clearView()V

    const/4 v9, -0x1

    invoke-virtual {p0, v9}, Lcom/google/android/gsf/login/BrowserActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/BrowserActivity;->finish()V

    :cond_3
    return-void

    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_0
.end method

.method protected onPause()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/gsf/login/BrowserActivity;->stop()V

    invoke-super {p0}, Lcom/google/android/gsf/login/BaseActivity;->onPause()V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/gsf/login/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "interactivity_completed"

    iget-boolean v1, p0, Lcom/google/android/gsf/login/BrowserActivity;->mInteractivityCompleted:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "waiting_for_network"

    iget-boolean v1, p0, Lcom/google/android/gsf/login/BrowserActivity;->mWaitingForNetwork:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "background_task_started"

    iget-boolean v1, p0, Lcom/google/android/gsf/login/BrowserActivity;->mBackgroundTaskStarted:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public onWebLoginError(Lcom/google/android/gsf/loginservice/GLSUser$Status;ILjava/lang/String;)V
    .locals 2
    .param p1    # Lcom/google/android/gsf/loginservice/GLSUser$Status;
    .param p2    # I
    .param p3    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/gsf/login/BrowserActivity;->createErrorIntent(Lcom/google/android/gsf/loginservice/GLSUser$Status;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x3f1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/login/BrowserActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method protected startWebLogin()V
    .locals 7

    const/4 v6, 0x1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "&lang="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "&langCountry="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Landroid/content/res/Configuration;

    invoke-direct {v0}, Landroid/content/res/Configuration;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/BrowserActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v4, v0}, Landroid/provider/Settings$System;->getConfiguration(Landroid/content/ContentResolver;Landroid/content/res/Configuration;)V

    iget v4, v0, Landroid/content/res/Configuration;->mcc:I

    if-eqz v4, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "&mcc="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v0, Landroid/content/res/Configuration;->mcc:I

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "&xoauth_display_name=Android%20Phone"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "&cc="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/gsf/login/BrowserActivity;->getCountry()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/gsf/login/BrowserActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v4, v4, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUrl:Ljava/lang/String;

    if-nez v4, :cond_1

    sget-object v3, Lcom/google/android/gsf/login/BrowserActivity;->OAUTH_START_URL:Ljava/lang/String;

    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "&source=android"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gsf/login/BrowserActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v4, v4, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUsername:Ljava/lang/String;

    if-eqz v4, :cond_2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "&Email="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gsf/login/BrowserActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v5, v5, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUsername:Ljava/lang/String;

    invoke-static {v5}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "&tmpl=reauth"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_1
    iput-boolean v6, p0, Lcom/google/android/gsf/login/BrowserActivity;->mIsLoading:Z

    iput-boolean v6, p0, Lcom/google/android/gsf/login/BrowserActivity;->mIsInitialLoad:Z

    iget-object v4, p0, Lcom/google/android/gsf/login/BrowserActivity;->mCookieManager:Landroid/webkit/CookieManager;

    invoke-virtual {v4}, Landroid/webkit/CookieManager;->removeAllCookie()V

    iget-object v4, p0, Lcom/google/android/gsf/login/BrowserActivity;->mWebView:Lcom/google/android/gsf/login/CustomWebView;

    invoke-virtual {v4, v3}, Lcom/google/android/gsf/login/CustomWebView;->loadUrl(Ljava/lang/String;)V

    return-void

    :cond_1
    iget-object v4, p0, Lcom/google/android/gsf/login/BrowserActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v3, v4, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUrl:Ljava/lang/String;

    goto :goto_0

    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "&tmpl=new_account"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method

.method public stop()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gsf/login/BrowserActivity;->mIsLoading:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/login/BrowserActivity;->mWebView:Lcom/google/android/gsf/login/CustomWebView;

    invoke-virtual {v0}, Lcom/google/android/gsf/login/CustomWebView;->stopLoading()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gsf/login/BrowserActivity;->mIsLoading:Z

    :cond_0
    return-void
.end method
