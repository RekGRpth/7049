.class public Lcom/google/android/gsf/login/LoginActivityTask;
.super Lcom/google/android/gsf/login/BackgroundTask;
.source "LoginActivityTask.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final mPostAddCond:Ljava/util/concurrent/locks/Condition;

.field final mPostAddLock:Ljava/util/concurrent/locks/Lock;

.field final mPostAddTime:Ljava/util/concurrent/atomic/AtomicLong;


# direct methods
.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/gsf/login/BackgroundTask;-><init>()V

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mPostAddLock:Ljava/util/concurrent/locks/Lock;

    iget-object v0, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mPostAddLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mPostAddCond:Ljava/util/concurrent/locks/Condition;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v1, -0x1

    invoke-direct {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mPostAddTime:Ljava/util/concurrent/atomic/AtomicLong;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/gsf/login/LoginActivityTask;)Z
    .locals 1
    .param p0    # Lcom/google/android/gsf/login/LoginActivityTask;

    iget-boolean v0, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mConfirmCredentials:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/gsf/login/LoginActivityTask;)Z
    .locals 1
    .param p0    # Lcom/google/android/gsf/login/LoginActivityTask;

    iget-boolean v0, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mAddAccount:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/google/android/gsf/login/LoginActivityTask;)Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    .locals 1
    .param p0    # Lcom/google/android/gsf/login/LoginActivityTask;

    iget-object v0, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/gsf/login/LoginActivityTask;)Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    .locals 1
    .param p0    # Lcom/google/android/gsf/login/LoginActivityTask;

    iget-object v0, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/gsf/login/LoginActivityTask;)Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    .locals 1
    .param p0    # Lcom/google/android/gsf/login/LoginActivityTask;

    iget-object v0, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/gsf/login/LoginActivityTask;)Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    .locals 1
    .param p0    # Lcom/google/android/gsf/login/LoginActivityTask;

    iget-object v0, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/gsf/login/LoginActivityTask;)Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    .locals 1
    .param p0    # Lcom/google/android/gsf/login/LoginActivityTask;

    iget-object v0, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/gsf/login/LoginActivityTask;)Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    .locals 1
    .param p0    # Lcom/google/android/gsf/login/LoginActivityTask;

    iget-object v0, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/gsf/login/LoginActivityTask;)Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    .locals 1
    .param p0    # Lcom/google/android/gsf/login/LoginActivityTask;

    iget-object v0, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/google/android/gsf/login/LoginActivityTask;)Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    .locals 1
    .param p0    # Lcom/google/android/gsf/login/LoginActivityTask;

    iget-object v0, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/gsf/login/LoginActivityTask;Landroid/os/Message;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/gsf/login/LoginActivityTask;
    .param p1    # Landroid/os/Message;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/google/android/gsf/login/LoginActivityTask;->updateAccount(Landroid/os/Message;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/gsf/login/LoginActivityTask;)Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    .locals 1
    .param p0    # Lcom/google/android/gsf/login/LoginActivityTask;

    iget-object v0, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/gsf/login/LoginActivityTask;Lcom/google/android/gsf/loginservice/GLSUser;)Lcom/google/android/gsf/loginservice/GLSUser;
    .locals 0
    .param p0    # Lcom/google/android/gsf/login/LoginActivityTask;
    .param p1    # Lcom/google/android/gsf/loginservice/GLSUser;

    iput-object p1, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mGlsUser:Lcom/google/android/gsf/loginservice/GLSUser;

    return-object p1
.end method

.method static synthetic access$500(Lcom/google/android/gsf/login/LoginActivityTask;)Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    .locals 1
    .param p0    # Lcom/google/android/gsf/login/LoginActivityTask;

    iget-object v0, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/gsf/login/LoginActivityTask;)Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    .locals 1
    .param p0    # Lcom/google/android/gsf/login/LoginActivityTask;

    iget-object v0, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/gsf/login/LoginActivityTask;)Lcom/google/android/gsf/loginservice/GLSUser;
    .locals 1
    .param p0    # Lcom/google/android/gsf/login/LoginActivityTask;

    iget-object v0, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mGlsUser:Lcom/google/android/gsf/loginservice/GLSUser;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/gsf/login/LoginActivityTask;)Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    .locals 1
    .param p0    # Lcom/google/android/gsf/login/LoginActivityTask;

    iget-object v0, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/gsf/login/LoginActivityTask;)Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    .locals 1
    .param p0    # Lcom/google/android/gsf/login/LoginActivityTask;

    iget-object v0, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    return-object v0
.end method

.method private updateAccount(Landroid/os/Message;Ljava/lang/String;)V
    .locals 8
    .param p1    # Landroid/os/Message;
    .param p2    # Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v4, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v4, v4, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAccessToken:Ljava/lang/String;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mGlsUser:Lcom/google/android/gsf/loginservice/GLSUser;

    iget-object v5, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v6, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v6, v6, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAccessToken:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v7, v7, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUsername:Ljava/lang/String;

    invoke-virtual {v4, v5, v6, v7}, Lcom/google/android/gsf/loginservice/GLSUser;->updateWithRequestToken(Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    :cond_0
    :goto_0
    if-eqz v2, :cond_1

    iget-object v4, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mGlsUser:Lcom/google/android/gsf/loginservice/GLSUser;

    invoke-virtual {v4, v2}, Lcom/google/android/gsf/loginservice/GLSUser;->getToken(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    :cond_1
    iget-object v4, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mGlsUser:Lcom/google/android/gsf/loginservice/GLSUser;

    iget-object v5, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mService:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget v6, v6, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mCallingUID:I

    iget-object v7, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    invoke-virtual {v4, v5, v6, v7}, Lcom/google/android/gsf/loginservice/GLSUser;->getAuthtoken(Ljava/lang/String;ILcom/google/android/gsf/loginservice/GLSUser$GLSSession;)Landroid/content/Intent;

    move-result-object v2

    :cond_2
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "intent"

    invoke-virtual {v4, v5, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-static {p0, v2}, Lcom/google/android/gsf/loginservice/GLSUser$Status;->fromIntent(Landroid/content/Context;Landroid/content/Intent;)Lcom/google/android/gsf/loginservice/GLSUser$Status;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/google/android/gsf/loginservice/GLSUser$Status;->toMessage(Landroid/os/Message;)V

    return-void

    :cond_3
    iget-object v4, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v0, v4, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mCaptchaAnswer:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v1, v4, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mCaptchaToken:Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v4, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mGlsUser:Lcom/google/android/gsf/loginservice/GLSUser;

    invoke-virtual {v4, v1, v0}, Lcom/google/android/gsf/loginservice/GLSUser;->setCaptcha(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    if-eqz p2, :cond_0

    iget-object v4, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mGlsUser:Lcom/google/android/gsf/loginservice/GLSUser;

    invoke-virtual {v4, p2}, Lcom/google/android/gsf/loginservice/GLSUser;->setPassword(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mGlsUser:Lcom/google/android/gsf/loginservice/GLSUser;

    iget-object v5, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    invoke-virtual {v4, v5}, Lcom/google/android/gsf/loginservice/GLSUser;->updateWithPassword(Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;)Landroid/content/Intent;

    move-result-object v2

    goto :goto_0
.end method


# virtual methods
.method public confirmCredentials(Landroid/os/Message;Ljava/lang/String;)V
    .locals 6
    .param p1    # Landroid/os/Message;
    .param p2    # Ljava/lang/String;

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v2, v2, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUsername:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    invoke-static {p0, v2, p2, v5}, Lcom/google/android/gsf/loginservice/GoogleLoginService;->onlineConfirmPassword(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;)Lcom/google/android/gsf/loginservice/GLSUser$Status;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "authAccount"

    iget-object v5, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v5, v5, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUsername:Ljava/lang/String;

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "accountType"

    const-string v5, "com.google"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v5, "booleanResult"

    sget-object v2, Lcom/google/android/gsf/loginservice/GLSUser$Status;->SUCCESS:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    if-ne v0, v2, :cond_0

    move v2, v3

    :goto_0
    invoke-virtual {v1, v5, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v2, "confirmResult"

    sget-object v5, Lcom/google/android/gsf/loginservice/GLSUser$Status;->SUCCESS:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    if-ne v0, v5, :cond_1

    :goto_1
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "intent"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    sget-object v2, Lcom/google/android/gsf/loginservice/GLSUser$Status;->SUCCESS:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    if-ne v0, v2, :cond_2

    sget-object v2, Lcom/google/android/gsf/loginservice/GLSUser$Status;->SUCCESS:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    invoke-virtual {v2, p1}, Lcom/google/android/gsf/loginservice/GLSUser$Status;->toMessage(Landroid/os/Message;)V

    :goto_2
    return-void

    :cond_0
    move v2, v4

    goto :goto_0

    :cond_1
    move v3, v4

    goto :goto_1

    :cond_2
    sget-object v2, Lcom/google/android/gsf/loginservice/GLSUser$Status;->BAD_AUTHENTICATION:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    invoke-virtual {v2, p1}, Lcom/google/android/gsf/loginservice/GLSUser$Status;->toMessage(Landroid/os/Message;)V

    goto :goto_2
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mCancelButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mCancelable:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gsf/login/LoginActivityTask;->onCancel()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mCancelButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mPostAddLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mPostAddTime:Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    iget-object v0, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mPostAddCond:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mPostAddLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mPostAddLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/gsf/login/BackgroundTask;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f080033

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/LoginActivityTask;->setMessage(I)V

    const v0, 0x7f0800d7

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/LoginActivityTask;->setTitle(I)V

    return-void
.end method

.method protected onError(Lcom/google/android/gsf/loginservice/GLSUser$Status;Landroid/content/Intent;)V
    .locals 1
    .param p1    # Lcom/google/android/gsf/loginservice/GLSUser$Status;
    .param p2    # Landroid/content/Intent;

    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser$Status;->BAD_AUTHENTICATION:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x4

    invoke-virtual {p0, v0, p2}, Lcom/google/android/gsf/login/LoginActivityTask;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/LoginActivityTask;->finish()V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/gsf/login/BackgroundTask;->onError(Lcom/google/android/gsf/loginservice/GLSUser$Status;Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public start()V
    .locals 9

    invoke-super {p0}, Lcom/google/android/gsf/login/BackgroundTask;->start()V

    iget-object v7, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v7, v7, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAccessToken:Ljava/lang/String;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v7, v7, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUsername:Ljava/lang/String;

    if-nez v7, :cond_1

    :cond_0
    iget-boolean v7, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mAddAccount:Z

    if-nez v7, :cond_2

    :cond_1
    iget-object v7, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mHandler:Landroid/os/Handler;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v5

    new-instance v7, Lcom/google/android/gsf/login/LoginActivityTask$1;

    invoke-direct {v7, p0, v5, v5}, Lcom/google/android/gsf/login/LoginActivityTask$1;-><init>(Lcom/google/android/gsf/login/LoginActivityTask;Landroid/os/Message;Landroid/os/Message;)V

    iput-object v7, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mTaskThread:Lcom/google/android/gsf/login/CancelableCallbackThread;

    iget-object v7, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mTaskThread:Lcom/google/android/gsf/login/CancelableCallbackThread;

    invoke-virtual {v7}, Lcom/google/android/gsf/login/CancelableCallbackThread;->start()V

    :goto_0
    return-void

    :cond_2
    iget-object v7, p0, Lcom/google/android/gsf/login/LoginActivityTask;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v6, v7, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUsername:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v7

    const-string v8, "com.google"

    invoke-virtual {v7, v8}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    move-object v2, v1

    array-length v4, v2

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v4, :cond_1

    aget-object v0, v2, v3

    iget-object v7, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-virtual {p0}, Lcom/google/android/gsf/login/LoginActivityTask;->isSetupWizard()Z

    move-result v7

    if-eqz v7, :cond_3

    const/4 v7, -0x1

    invoke-virtual {p0, v7}, Lcom/google/android/gsf/login/LoginActivityTask;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/LoginActivityTask;->finish()V

    goto :goto_0

    :cond_3
    const/4 v7, 0x4

    sget-object v8, Lcom/google/android/gsf/loginservice/GLSUser$Status;->EXISTING_USERNAME:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    invoke-virtual {p0, v8}, Lcom/google/android/gsf/login/LoginActivityTask;->createErrorIntent(Lcom/google/android/gsf/loginservice/GLSUser$Status;)Landroid/content/Intent;

    move-result-object v8

    invoke-virtual {p0, v7, v8}, Lcom/google/android/gsf/login/LoginActivityTask;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/LoginActivityTask;->finish()V

    goto :goto_0

    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method
