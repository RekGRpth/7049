.class public Lcom/google/android/gsf/login/MarketHelper;
.super Ljava/lang/Object;
.source "MarketHelper.java"


# instance fields
.field mBillingService:Lcom/android/vending/billing/IBillingAccountService;

.field mContext:Landroid/content/Context;

.field mServiceConnection:Landroid/content/ServiceConnection;

.field tokenCondition:Ljava/util/concurrent/locks/Condition;

.field tokenLock:Ljava/util/concurrent/locks/Lock;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/google/android/gsf/login/MarketHelper;->tokenLock:Ljava/util/concurrent/locks/Lock;

    iget-object v0, p0, Lcom/google/android/gsf/login/MarketHelper;->tokenLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gsf/login/MarketHelper;->tokenCondition:Ljava/util/concurrent/locks/Condition;

    iput-object p1, p0, Lcom/google/android/gsf/login/MarketHelper;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public allowCreditCard(Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;)Z
    .locals 4
    .param p1    # Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    const/4 v2, 0x1

    iget-boolean v3, p1, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mSetupWizard:Z

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v1, p1, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mPendingIntent:Landroid/app/PendingIntent;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v0

    const-string v3, "com.android.settings"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "com.android.vending"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public bind()Lcom/android/vending/billing/IBillingAccountService;
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gsf/login/MarketHelper;->mBillingService:Lcom/android/vending/billing/IBillingAccountService;

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/android/gsf/login/MarketHelper;->mBillingService:Lcom/android/vending/billing/IBillingAccountService;

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/gsf/login/MarketHelper;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gsf/loginservice/GLSUser;->ensureNotOnMainThread(Landroid/content/Context;)V

    new-instance v1, Lcom/google/android/gsf/login/MarketHelper$1;

    invoke-direct {v1, p0}, Lcom/google/android/gsf/login/MarketHelper$1;-><init>(Lcom/google/android/gsf/login/MarketHelper;)V

    iput-object v1, p0, Lcom/google/android/gsf/login/MarketHelper;->mServiceConnection:Landroid/content/ServiceConnection;

    iget-object v1, p0, Lcom/google/android/gsf/login/MarketHelper;->mContext:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-class v4, Lcom/android/vending/billing/IBillingAccountService;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".BIND"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gsf/login/MarketHelper;->mServiceConnection:Landroid/content/ServiceConnection;

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gsf/login/MarketHelper;->tokenLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gsf/login/MarketHelper;->mBillingService:Lcom/android/vending/billing/IBillingAccountService;

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gsf/login/MarketHelper;->tokenCondition:Ljava/util/concurrent/locks/Condition;

    const-wide/16 v2, 0x5

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v2, v3, v4}, Ljava/util/concurrent/locks/Condition;->await(JLjava/util/concurrent/TimeUnit;)Z

    :cond_2
    iget-object v0, p0, Lcom/google/android/gsf/login/MarketHelper;->mBillingService:Lcom/android/vending/billing/IBillingAccountService;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lcom/google/android/gsf/login/MarketHelper;->tokenLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    :catch_0
    move-exception v1

    iget-object v1, p0, Lcom/google/android/gsf/login/MarketHelper;->tokenLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gsf/login/MarketHelper;->tokenLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public marketAvailable()Z
    .locals 7

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/android/gsf/login/MarketHelper;->bind()Lcom/android/vending/billing/IBillingAccountService;

    move-result-object v5

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    iget-object v5, p0, Lcom/google/android/gsf/login/MarketHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    :try_start_0
    const-string v5, "com.android.vending"

    const/4 v6, 0x0

    invoke-virtual {v3, v5, v6}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    new-instance v5, Landroid/content/Intent;

    const-string v6, "com.android.vending.billing.ADD_CREDIT_CARD"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v6, 0x0

    invoke-virtual {v3, v5, v6}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    const/4 v4, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gsf/login/MarketHelper;->mServiceConnection:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/login/MarketHelper;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gsf/login/MarketHelper;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    :cond_0
    return-void
.end method

.method public setOffersValues(Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;)V
    .locals 7
    .param p1    # Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/google/android/gsf/login/MarketHelper;->marketAvailable()Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "GLSUser"

    const-string v4, "Play is unavailable"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v6, p1, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mShowOffer:Z

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gsf/login/MarketHelper;->bind()Lcom/android/vending/billing/IBillingAccountService;

    move-result-object v3

    iget-object v4, p1, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUsername:Ljava/lang/String;

    invoke-interface {v3, v4}, Lcom/android/vending/billing/IBillingAccountService;->getOffers(Ljava/lang/String;)Landroid/os/Bundle;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    if-nez v0, :cond_1

    iput-boolean v6, p1, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mShowOffer:Z

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v3, "GLSUser"

    const-string v4, "Error getting offers"

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :cond_1
    const-string v3, "result_code"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const-string v3, "GLSUser"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Play result: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch v2, :pswitch_data_0

    iput-boolean v6, p1, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mShowOffer:Z

    goto :goto_0

    :pswitch_0
    const/4 v3, 0x1

    iput-boolean v3, p1, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mShowOffer:Z

    const-string v3, "available_offer_redemption_intent"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/content/Intent;

    iput-object v3, p1, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mOfferIntent:Landroid/content/Intent;

    goto :goto_0

    :pswitch_1
    iput-boolean v6, p1, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mShowOffer:Z

    const-string v3, "redeemed_offer_message_html"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p1, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mOfferMessageHtml:Ljava/lang/String;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
