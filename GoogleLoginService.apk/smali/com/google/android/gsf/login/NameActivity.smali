.class public Lcom/google/android/gsf/login/NameActivity;
.super Lcom/google/android/gsf/login/BaseActivity;
.source "NameActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gsf/login/NameActivity$1;,
        Lcom/google/android/gsf/login/NameActivity$MinWaitAsyncTask;,
        Lcom/google/android/gsf/login/NameActivity$UpdateProfileAsyncTask;,
        Lcom/google/android/gsf/login/NameActivity$MyTextWatcher;
    }
.end annotation


# instance fields
.field private mBackButton:Landroid/view/View;

.field private mButtons:Landroid/view/View;

.field private mContents:Landroid/view/View;

.field private mFirstNameEdit:Landroid/widget/EditText;

.field private mLastNameEdit:Landroid/widget/EditText;

.field private mLocalOnly:Z

.field private mMessage:Landroid/widget/TextView;

.field private mNextButton:Landroid/view/View;

.field private mNextButton2:Landroid/view/View;

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mRetryMode:Z

.field private mSkipButton:Landroid/view/View;

.field private volatile mTaskCounter:I

.field private mTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gsf/login/BaseActivity;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gsf/login/NameActivity;->mTaskCounter:I

    return-void
.end method

.method static synthetic access$208(Lcom/google/android/gsf/login/NameActivity;)I
    .locals 2
    .param p0    # Lcom/google/android/gsf/login/NameActivity;

    iget v0, p0, Lcom/google/android/gsf/login/NameActivity;->mTaskCounter:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/gsf/login/NameActivity;->mTaskCounter:I

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/gsf/login/NameActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/gsf/login/NameActivity;

    invoke-direct {p0}, Lcom/google/android/gsf/login/NameActivity;->finishProgress()V

    return-void
.end method

.method private finishProgress()V
    .locals 2

    iget v0, p0, Lcom/google/android/gsf/login/NameActivity;->mTaskCounter:I

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/login/NameActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-boolean v0, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUserSelectedGooglePlus:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gsf/login/NameActivity;->mTaskCounter:I

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gsf/login/NameCheckTask;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x406

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/login/NameActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/NameActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/NameActivity;->finish()V

    goto :goto_0
.end method

.method private initViews()V
    .locals 8

    const v7, 0x7f0b0002

    const/4 v6, 0x0

    const/16 v5, 0x8

    const/4 v4, 0x1

    invoke-virtual {p0, v7}, Lcom/google/android/gsf/login/NameActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/gsf/login/NameActivity;->mTitle:Landroid/widget/TextView;

    const v1, 0x7f0b0032

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/login/NameActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/google/android/gsf/login/NameActivity;->mProgressBar:Landroid/widget/ProgressBar;

    const v1, 0x7f0b000e

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/login/NameActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gsf/login/NameActivity;->mContents:Landroid/view/View;

    const v1, 0x7f0b0006

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/login/NameActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gsf/login/NameActivity;->mButtons:Landroid/view/View;

    const v1, 0x7f0b000b

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/login/NameActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/gsf/login/NameActivity;->mMessage:Landroid/widget/TextView;

    const v1, 0x7f0b0033

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/login/NameActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/google/android/gsf/login/NameActivity;->mFirstNameEdit:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/gsf/login/NameActivity;->mFirstNameEdit:Landroid/widget/EditText;

    new-instance v2, Lcom/google/android/gsf/login/NameActivity$MyTextWatcher;

    iget-object v3, p0, Lcom/google/android/gsf/login/NameActivity;->mFirstNameEdit:Landroid/widget/EditText;

    invoke-direct {v2, p0, v3}, Lcom/google/android/gsf/login/NameActivity$MyTextWatcher;-><init>(Lcom/google/android/gsf/login/NameActivity;Landroid/widget/EditText;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v1, p0, Lcom/google/android/gsf/login/NameActivity;->mFirstNameEdit:Landroid/widget/EditText;

    invoke-virtual {v1, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    const v1, 0x7f0b0034

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/login/NameActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/google/android/gsf/login/NameActivity;->mLastNameEdit:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/gsf/login/NameActivity;->mLastNameEdit:Landroid/widget/EditText;

    new-instance v2, Lcom/google/android/gsf/login/NameActivity$MyTextWatcher;

    iget-object v3, p0, Lcom/google/android/gsf/login/NameActivity;->mLastNameEdit:Landroid/widget/EditText;

    invoke-direct {v2, p0, v3}, Lcom/google/android/gsf/login/NameActivity$MyTextWatcher;-><init>(Lcom/google/android/gsf/login/NameActivity;Landroid/widget/EditText;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v1, p0, Lcom/google/android/gsf/login/NameActivity;->mLastNameEdit:Landroid/widget/EditText;

    invoke-virtual {v1, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    invoke-virtual {p0, v7}, Lcom/google/android/gsf/login/NameActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-boolean v1, p0, Lcom/google/android/gsf/login/NameActivity;->mLocalOnly:Z

    if-eqz v1, :cond_1

    const v1, 0x7f08003b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, p0, Lcom/google/android/gsf/login/NameActivity;->mMessage:Landroid/widget/TextView;

    const v2, 0x7f08003d

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    :goto_0
    const v1, 0x7f0b000a

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/login/NameActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gsf/login/NameActivity;->mNextButton:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gsf/login/NameActivity;->mNextButton:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0b0037

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/login/NameActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gsf/login/NameActivity;->mNextButton2:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gsf/login/NameActivity;->mNextButton2:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0b0008

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/login/NameActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gsf/login/NameActivity;->mSkipButton:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gsf/login/NameActivity;->mSkipButton:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0b0013

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/login/NameActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gsf/login/NameActivity;->mBackButton:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/gsf/login/NameActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "noBack"

    invoke-virtual {v1, v2, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gsf/login/NameActivity;->mBackButton:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    iget-boolean v1, p0, Lcom/google/android/gsf/login/NameActivity;->mRetryMode:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gsf/login/NameActivity;->mMessage:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    const v1, 0x7f0b0035

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/login/NameActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    const v1, 0x7f0b0036

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/login/NameActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/gsf/login/NameActivity;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/gsf/login/NameActivity;->mFirstNameEdit:Landroid/widget/EditText;

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-object v1, p0, Lcom/google/android/gsf/login/NameActivity;->mLastNameEdit:Landroid/widget/EditText;

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-object v1, p0, Lcom/google/android/gsf/login/NameActivity;->mNextButton2:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, p0, Lcom/google/android/gsf/login/NameActivity;->mSkipButton:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, p0, Lcom/google/android/gsf/login/NameActivity;->mNextButton2:Landroid/view/View;

    invoke-virtual {p0, v1, v4}, Lcom/google/android/gsf/login/NameActivity;->setDefaultButton(Landroid/view/View;Z)V

    :goto_2
    iget-object v1, p0, Lcom/google/android/gsf/login/NameActivity;->mLastNameEdit:Landroid/widget/EditText;

    invoke-virtual {p0, v1, v6}, Lcom/google/android/gsf/login/NameActivity;->setDefaultButton(Landroid/view/View;Z)V

    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/gsf/login/NameActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-boolean v1, v1, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUserSelectedGooglePlus:Z

    if-eqz v1, :cond_0

    const v1, 0x7f08003a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/gsf/login/NameActivity;->mBackButton:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/login/NameActivity;->setBackButton(Landroid/view/View;)V

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/google/android/gsf/login/NameActivity;->mNextButton:Landroid/view/View;

    invoke-virtual {p0, v1, v4}, Lcom/google/android/gsf/login/NameActivity;->setDefaultButton(Landroid/view/View;Z)V

    goto :goto_2
.end method

.method private populateFieldsAndCheckForExistingNames()V
    .locals 6

    invoke-virtual {p0}, Lcom/google/android/gsf/login/NameActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    sget-object v4, Lcom/google/android/gsf/login/BackendStub$Key;->FIRST_NAME:Lcom/google/android/gsf/login/BackendStub$Key;

    invoke-virtual {v4}, Lcom/google/android/gsf/login/BackendStub$Key;->getWire()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    sget-object v4, Lcom/google/android/gsf/login/BackendStub$Key;->FIRST_NAME:Lcom/google/android/gsf/login/BackendStub$Key;

    invoke-virtual {v4}, Lcom/google/android/gsf/login/BackendStub$Key;->getWire()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    sget-object v4, Lcom/google/android/gsf/login/BackendStub$Key;->LAST_NAME:Lcom/google/android/gsf/login/BackendStub$Key;

    invoke-virtual {v4}, Lcom/google/android/gsf/login/BackendStub$Key;->getWire()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    sget-object v4, Lcom/google/android/gsf/login/BackendStub$Key;->LAST_NAME:Lcom/google/android/gsf/login/BackendStub$Key;

    invoke-virtual {v4}, Lcom/google/android/gsf/login/BackendStub$Key;->getWire()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_1
    iget-object v4, p0, Lcom/google/android/gsf/login/NameActivity;->mFirstNameEdit:Landroid/widget/EditText;

    invoke-virtual {v4, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/google/android/gsf/login/NameActivity;->mLastNameEdit:Landroid/widget/EditText;

    invoke-virtual {v4, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/google/android/gsf/login/NameActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-boolean v3, v4, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mShownName:Z

    iget-object v4, p0, Lcom/google/android/gsf/login/NameActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    const/4 v5, 0x1

    iput-boolean v5, v4, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mShownName:Z

    if-nez v3, :cond_1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gsf/login/NameActivity;->showEmptySavingUi()V

    invoke-direct {p0, v0, v2}, Lcom/google/android/gsf/login/NameActivity;->startWithName(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gsf/login/NameActivity;->getUserData()Ljava/util/HashMap;

    move-result-object v4

    sget-object v5, Lcom/google/android/gsf/login/BackendStub$Key;->FIRST_NAME:Lcom/google/android/gsf/login/BackendStub$Key;

    invoke-virtual {v5}, Lcom/google/android/gsf/login/BackendStub$Key;->getWire()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gsf/login/NameActivity;->getUserData()Ljava/util/HashMap;

    move-result-object v4

    sget-object v5, Lcom/google/android/gsf/login/BackendStub$Key;->LAST_NAME:Lcom/google/android/gsf/login/BackendStub$Key;

    invoke-virtual {v5}, Lcom/google/android/gsf/login/BackendStub$Key;->getWire()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    goto :goto_1
.end method

.method private showEmptySavingUi()V
    .locals 3

    const/16 v2, 0x8

    iget-object v0, p0, Lcom/google/android/gsf/login/NameActivity;->mTitle:Landroid/widget/TextView;

    const v1, 0x7f08003e

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/login/NameActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gsf/login/NameActivity;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gsf/login/NameActivity;->mContents:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gsf/login/NameActivity;->mButtons:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private showFullSavingUi()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gsf/login/NameActivity;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gsf/login/NameActivity;->mFirstNameEdit:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gsf/login/NameActivity;->mLastNameEdit:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gsf/login/NameActivity;->mNextButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gsf/login/NameActivity;->mNextButton2:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gsf/login/NameActivity;->mSkipButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gsf/login/NameActivity;->mBackButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method

.method private startWithName(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-super {p0}, Lcom/google/android/gsf/login/BaseActivity;->start()V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/NameActivity;->getUserData()Ljava/util/HashMap;

    move-result-object v0

    sget-object v1, Lcom/google/android/gsf/login/BackendStub$Key;->FIRST_NAME:Lcom/google/android/gsf/login/BackendStub$Key;

    invoke-virtual {v1}, Lcom/google/android/gsf/login/BackendStub$Key;->getWire()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/gsf/login/NameActivity;->getUserData()Ljava/util/HashMap;

    move-result-object v0

    sget-object v1, Lcom/google/android/gsf/login/BackendStub$Key;->LAST_NAME:Lcom/google/android/gsf/login/BackendStub$Key;

    invoke-virtual {v1}, Lcom/google/android/gsf/login/BackendStub$Key;->getWire()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/gsf/login/NameActivity$MinWaitAsyncTask;

    invoke-direct {v0, p0, v6}, Lcom/google/android/gsf/login/NameActivity$MinWaitAsyncTask;-><init>(Lcom/google/android/gsf/login/NameActivity;Lcom/google/android/gsf/login/NameActivity$1;)V

    new-array v1, v5, [Ljava/lang/Long;

    const-wide/16 v2, 0x3e8

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/google/android/gsf/login/NameActivity$MinWaitAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    new-instance v0, Lcom/google/android/gsf/login/NameActivity$UpdateProfileAsyncTask;

    invoke-direct {v0, p0, v6}, Lcom/google/android/gsf/login/NameActivity$UpdateProfileAsyncTask;-><init>(Lcom/google/android/gsf/login/NameActivity;Lcom/google/android/gsf/login/NameActivity$1;)V

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    aput-object p1, v1, v4

    aput-object p2, v1, v5

    invoke-virtual {v0, v1}, Lcom/google/android/gsf/login/NameActivity$UpdateProfileAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    const/16 v2, 0x407

    const/4 v1, -0x1

    const/16 v0, 0x406

    if-ne v0, p1, :cond_2

    if-ne p2, v1, :cond_1

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/login/NameActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/NameActivity;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p2, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gsf/login/BadNameActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gsf/login/NameActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_2
    if-ne v2, p1, :cond_4

    if-ne p2, v1, :cond_3

    invoke-virtual {p0, p2}, Lcom/google/android/gsf/login/NameActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/NameActivity;->finish()V

    goto :goto_0

    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gsf/login/NameActivity;->mRetryMode:Z

    invoke-direct {p0}, Lcom/google/android/gsf/login/NameActivity;->initViews()V

    goto :goto_0

    :cond_4
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gsf/login/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gsf/login/NameActivity;->mNextButton:Landroid/view/View;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/login/NameActivity;->mNextButton2:Landroid/view/View;

    if-ne p1, v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gsf/login/NameActivity;->start()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/gsf/login/NameActivity;->mSkipButton:Landroid/view/View;

    if-ne p1, v0, :cond_2

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/NameActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/NameActivity;->finish()V

    :cond_2
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/gsf/login/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/NameActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "localOnly"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gsf/login/NameActivity;->mLocalOnly:Z

    invoke-virtual {p0}, Lcom/google/android/gsf/login/NameActivity;->shouldDisplayLastNameFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f030013

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/NameActivity;->setContentView(I)V

    invoke-direct {p0}, Lcom/google/android/gsf/login/NameActivity;->initViews()V

    invoke-direct {p0}, Lcom/google/android/gsf/login/NameActivity;->populateFieldsAndCheckForExistingNames()V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/NameActivity;->updateWidgetState()V

    return-void

    :cond_0
    const v0, 0x7f030012

    goto :goto_0
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 5
    .param p1    # Landroid/view/View;
    .param p2    # Z

    const v4, 0x7f080021

    iget-object v3, p0, Lcom/google/android/gsf/login/NameActivity;->mFirstNameEdit:Landroid/widget/EditText;

    if-ne p1, v3, :cond_2

    if-nez p2, :cond_2

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/google/android/gsf/login/NameActivity;->mFirstNameEdit:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isGraphic(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p0, v4}, Lcom/google/android/gsf/login/NameActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    iget-object v3, p0, Lcom/google/android/gsf/login/NameActivity;->mFirstNameEdit:Landroid/widget/EditText;

    invoke-virtual {v3, v0}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v3, p0, Lcom/google/android/gsf/login/NameActivity;->mLastNameEdit:Landroid/widget/EditText;

    if-ne p1, v3, :cond_1

    if-nez p2, :cond_1

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/google/android/gsf/login/NameActivity;->mLastNameEdit:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-static {v2}, Landroid/text/TextUtils;->isGraphic(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {p0, v4}, Lcom/google/android/gsf/login/NameActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_3
    iget-object v3, p0, Lcom/google/android/gsf/login/NameActivity;->mLastNameEdit:Landroid/widget/EditText;

    invoke-virtual {v3, v0}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/gsf/login/BaseActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    const-string v0, "retryMode"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gsf/login/NameActivity;->mRetryMode:Z

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/gsf/login/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "retryMode"

    iget-boolean v1, p0, Lcom/google/android/gsf/login/NameActivity;->mRetryMode:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public start()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/gsf/login/NameActivity;->showFullSavingUi()V

    iget-object v0, p0, Lcom/google/android/gsf/login/NameActivity;->mFirstNameEdit:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gsf/login/NameActivity;->mLastNameEdit:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gsf/login/NameActivity;->startWithName(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public updateWidgetState()V
    .locals 6

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-super {p0}, Lcom/google/android/gsf/login/BaseActivity;->updateWidgetState()V

    iget-object v5, p0, Lcom/google/android/gsf/login/NameActivity;->mFirstNameEdit:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getError()Ljava/lang/CharSequence;

    move-result-object v5

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/google/android/gsf/login/NameActivity;->mLastNameEdit:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getError()Ljava/lang/CharSequence;

    move-result-object v5

    if-nez v5, :cond_2

    move v0, v3

    :goto_0
    iget-boolean v5, p0, Lcom/google/android/gsf/login/NameActivity;->mLocalOnly:Z

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/google/android/gsf/login/NameActivity;->mFirstNameEdit:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isGraphic(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    move v1, v3

    :goto_1
    iget-object v5, p0, Lcom/google/android/gsf/login/NameActivity;->mLastNameEdit:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isGraphic(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    move v2, v3

    :goto_2
    if-nez v1, :cond_0

    if-eqz v2, :cond_1

    :cond_0
    const/4 v0, 0x0

    :cond_1
    iget-object v3, p0, Lcom/google/android/gsf/login/NameActivity;->mNextButton:Landroid/view/View;

    invoke-virtual {v3, v0}, Landroid/view/View;->setEnabled(Z)V

    iget-object v3, p0, Lcom/google/android/gsf/login/NameActivity;->mNextButton2:Landroid/view/View;

    invoke-virtual {v3, v0}, Landroid/view/View;->setEnabled(Z)V

    return-void

    :cond_2
    move v0, v4

    goto :goto_0

    :cond_3
    move v1, v4

    goto :goto_1

    :cond_4
    move v2, v4

    goto :goto_2
.end method
