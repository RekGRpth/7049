.class public final Lcom/google/android/gsf/login/ShowErrorActivity;
.super Lcom/google/android/gsf/login/BaseActivity;
.source "ShowErrorActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gsf/login/ShowErrorActivity$1;
    }
.end annotation


# instance fields
.field private mClearNotifications:Z

.field private mExplanation:Landroid/widget/TextView;

.field private mNextButton:Landroid/widget/Button;

.field mResult:I

.field private mSkipButton:Landroid/widget/Button;

.field private mSubmissionTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gsf/login/BaseActivity;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mClearNotifications:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mResult:I

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gsf/login/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    :goto_0
    return-void

    :pswitch_0
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/ShowErrorActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/ShowErrorActivity;->finish()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3fd
        :pswitch_0
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1    # Landroid/view/View;

    const/4 v8, 0x1

    const/4 v7, 0x0

    iget-boolean v5, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mClearNotifications:Z

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v5, v5, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUsername:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v6, v6, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAccountManagerOptions:Landroid/os/Bundle;

    invoke-static {p0, v5, v6}, Lcom/google/android/gsf/loginservice/GoogleLoginService;->onIntentDone(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    iget-object v5, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v3, v5, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mError:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    if-nez v3, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gsf/login/ShowErrorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-static {p0, v5}, Lcom/google/android/gsf/loginservice/GLSUser$Status;->fromIntent(Landroid/content/Context;Landroid/content/Intent;)Lcom/google/android/gsf/loginservice/GLSUser$Status;

    move-result-object v3

    :cond_1
    iget-object v5, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    sget-object v6, Lcom/google/android/gsf/loginservice/GLSUser$Status;->SUCCESS:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    iput-object v6, v5, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mError:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    iget v5, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mResult:I

    invoke-virtual {p0, v5}, Lcom/google/android/gsf/login/ShowErrorActivity;->setResult(I)V

    sget-object v5, Lcom/google/android/gsf/login/ShowErrorActivity$1;->$SwitchMap$com$google$android$gsf$loginservice$GLSUser$Status:[I

    invoke-virtual {v3}, Lcom/google/android/gsf/loginservice/GLSUser$Status;->ordinal()I

    move-result v6

    aget v5, v5, v6

    sparse-switch v5, :sswitch_data_0

    :cond_2
    iget-object v5, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mSkipButton:Landroid/widget/Button;

    if-ne p1, v5, :cond_3

    invoke-virtual {p0, v7}, Lcom/google/android/gsf/login/ShowErrorActivity;->setResult(I)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gsf/login/ShowErrorActivity;->finish()V

    :goto_0
    return-void

    :sswitch_0
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gsf/login/ShowErrorActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const-string v6, "com.google.android.apps.enterprise.dmagent"

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    new-instance v5, Landroid/content/ComponentName;

    const-string v6, "com.google.android.apps.enterprise.dmagent"

    const-string v7, "com.google.android.apps.enterprise.dmagent.DMAgentActivity"

    invoke-direct {v5, v6, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v5}, Landroid/content/Intent;->makeMainActivity(Landroid/content/ComponentName;)Landroid/content/Intent;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    :goto_1
    :try_start_1
    invoke-virtual {p0, v2}, Lcom/google/android/gsf/login/ShowErrorActivity;->startActivity(Landroid/content/Intent;)V

    const/4 v5, 0x1

    invoke-virtual {p0, v5}, Lcom/google/android/gsf/login/ShowErrorActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/ShowErrorActivity;->finish()V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v5, "GLSActivity"

    const-string v6, "Market not found for dmagent"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v0

    new-instance v2, Landroid/content/Intent;

    const-string v5, "android.intent.action.VIEW"

    const-string v6, "market://details?id=com.google.android.apps.enterprise.dmagent"

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-direct {v2, v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_1

    :sswitch_1
    iget-object v5, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mSkipButton:Landroid/widget/Button;

    if-ne p1, v5, :cond_4

    invoke-virtual {p0, v7}, Lcom/google/android/gsf/login/ShowErrorActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/ShowErrorActivity;->finish()V

    goto :goto_0

    :cond_4
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    new-instance v5, Landroid/content/ComponentName;

    const-string v6, "com.google.android.gsf.login"

    const-string v7, "com.google.android.gsf.login.SetupWirelessActivity"

    invoke-direct {v5, v6, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const/16 v5, 0x3fd

    invoke-virtual {p0, v4, v5}, Lcom/google/android/gsf/login/ShowErrorActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :sswitch_2
    iget-boolean v5, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mAddAccount:Z

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mSkipButton:Landroid/widget/Button;

    if-ne p1, v5, :cond_2

    invoke-virtual {p0, v8}, Lcom/google/android/gsf/login/ShowErrorActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/ShowErrorActivity;->finish()V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x5 -> :sswitch_2
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 15
    .param p1    # Landroid/os/Bundle;

    invoke-super/range {p0 .. p1}, Lcom/google/android/gsf/login/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    const/4 v10, 0x0

    invoke-virtual {p0, v10}, Lcom/google/android/gsf/login/ShowErrorActivity;->setResult(I)V

    const v10, 0x7f03001a

    invoke-virtual {p0, v10}, Lcom/google/android/gsf/login/ShowErrorActivity;->setContentView(I)V

    const v10, 0x7f0b0043

    invoke-virtual {p0, v10}, Lcom/google/android/gsf/login/ShowErrorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    iput-object v10, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mSubmissionTitle:Landroid/widget/TextView;

    const v10, 0x7f0b0044

    invoke-virtual {p0, v10}, Lcom/google/android/gsf/login/ShowErrorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    iput-object v10, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mExplanation:Landroid/widget/TextView;

    const v10, 0x7f0b000a

    invoke-virtual {p0, v10}, Lcom/google/android/gsf/login/ShowErrorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/Button;

    iput-object v10, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mNextButton:Landroid/widget/Button;

    iget-object v10, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mNextButton:Landroid/widget/Button;

    invoke-virtual {v10, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v10, 0x7f0b0008

    invoke-virtual {p0, v10}, Lcom/google/android/gsf/login/ShowErrorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/Button;

    iput-object v10, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mSkipButton:Landroid/widget/Button;

    iget-object v10, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mSkipButton:Landroid/widget/Button;

    invoke-virtual {v10, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/ShowErrorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v10, "clearNotification"

    const/4 v11, 0x0

    invoke-virtual {v4, v10, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    iput-boolean v10, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mClearNotifications:Z

    const-string v10, "title"

    invoke-virtual {v4, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_0

    invoke-virtual {p0, v9}, Lcom/google/android/gsf/login/ShowErrorActivity;->setTitle(Ljava/lang/CharSequence;)V

    :cond_0
    const-string v10, "label"

    invoke-virtual {v4, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    iget-object v10, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mNextButton:Landroid/widget/Button;

    invoke-virtual {v10, v5}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    invoke-static {p0, v4}, Lcom/google/android/gsf/loginservice/GLSUser$Status;->fromIntent(Landroid/content/Context;Landroid/content/Intent;)Lcom/google/android/gsf/loginservice/GLSUser$Status;

    move-result-object v8

    const-string v10, "GLSActivity"

    const/4 v11, 0x2

    invoke-static {v10, v11}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_2

    const-string v10, "GLSActivity"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "ShowError: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, v8, Lcom/google/android/gsf/loginservice/GLSUser$Status;->resource:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v12, v12, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mDetail:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    sget-object v10, Lcom/google/android/gsf/login/ShowErrorActivity$1;->$SwitchMap$com$google$android$gsf$loginservice$GLSUser$Status:[I

    invoke-virtual {v8}, Lcom/google/android/gsf/loginservice/GLSUser$Status;->ordinal()I

    move-result v11

    aget v10, v10, v11

    packed-switch v10, :pswitch_data_0

    :goto_0
    iget v10, v8, Lcom/google/android/gsf/loginservice/GLSUser$Status;->resource:I

    if-eqz v10, :cond_b

    iget-object v10, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mExplanation:Landroid/widget/TextView;

    iget v11, v8, Lcom/google/android/gsf/loginservice/GLSUser$Status;->resource:I

    invoke-virtual {p0, v11}, Lcom/google/android/gsf/login/ShowErrorActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    iget-object v10, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v10, v10, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUsername:Ljava/lang/String;

    iget-object v11, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v11, v11, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAccountManagerOptions:Landroid/os/Bundle;

    invoke-static {p0, v10, v11}, Lcom/google/android/gsf/loginservice/GoogleLoginService;->onIntentDone(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)V

    return-void

    :pswitch_0
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gsf/login/ShowErrorActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    const-string v11, "com.google.android.apps.enterprise.dmagent"

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    const v0, 0x7f0800c1

    const v6, 0x7f0800c0

    :goto_2
    iget-object v10, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mNextButton:Landroid/widget/Button;

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/ShowErrorActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v6}, Lcom/google/android/gsf/login/ShowErrorActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v10

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/CharSequence;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v13, v13, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUsername:Ljava/lang/String;

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v10, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mExplanation:Landroid/widget/TextView;

    invoke-virtual {v10, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :catch_0
    move-exception v2

    const v0, 0x7f0800c3

    const v6, 0x7f0800c2

    goto :goto_2

    :pswitch_1
    iget-object v10, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mSkipButton:Landroid/widget/Button;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v10, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mSkipButton:Landroid/widget/Button;

    const v11, 0x7f080005

    invoke-virtual {v10, v11}, Landroid/widget/Button;->setText(I)V

    iget-object v10, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mExplanation:Landroid/widget/TextView;

    const v11, 0x7f0800dd

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(I)V

    const/4 v10, 0x6

    iput v10, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mResult:I

    goto :goto_1

    :pswitch_2
    iget-object v10, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mSkipButton:Landroid/widget/Button;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v10, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mSkipButton:Landroid/widget/Button;

    const v11, 0x7f080005

    invoke-virtual {v10, v11}, Landroid/widget/Button;->setText(I)V

    const/4 v10, 0x6

    iput v10, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mResult:I

    iget-object v10, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v10, v10, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mDetail:Ljava/lang/String;

    if-eqz v10, :cond_3

    iget-object v10, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mExplanation:Landroid/widget/TextView;

    iget-object v11, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v11, v11, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mDetail:Ljava/lang/String;

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v10, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mSkipButton:Landroid/widget/Button;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v10, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mSkipButton:Landroid/widget/Button;

    const v11, 0x7f080005

    invoke-virtual {v10, v11}, Landroid/widget/Button;->setText(I)V

    goto/16 :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gsf/login/ShowErrorActivity;->finish()V

    goto/16 :goto_1

    :pswitch_3
    iget-object v10, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mExplanation:Landroid/widget/TextView;

    const v11, 0x7f080084

    invoke-virtual {p0, v11}, Lcom/google/android/gsf/login/ShowErrorActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v11

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/CharSequence;

    const/4 v13, 0x0

    iget-object v14, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v14, v14, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUsername:Ljava/lang/String;

    aput-object v14, v12, v13

    invoke-static {v11, v12}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v10, 0x5

    iput v10, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mResult:I

    goto/16 :goto_1

    :pswitch_4
    const v10, 0x7f0800d6

    invoke-virtual {p0, v10}, Lcom/google/android/gsf/login/ShowErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/google/android/gsf/login/ShowErrorActivity;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v10, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mSkipButton:Landroid/widget/Button;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v10, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mNextButton:Landroid/widget/Button;

    const v11, 0x7f080013

    invoke-virtual {v10, v11}, Landroid/widget/Button;->setText(I)V

    iget-boolean v10, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mAddAccount:Z

    if-eqz v10, :cond_4

    iget-object v10, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mExplanation:Landroid/widget/TextView;

    const v11, 0x7f08006b

    invoke-virtual {p0, v11}, Lcom/google/android/gsf/login/ShowErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v10, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mSkipButton:Landroid/widget/Button;

    const v11, 0x7f08000a

    invoke-virtual {v10, v11}, Landroid/widget/Button;->setText(I)V

    goto/16 :goto_1

    :cond_4
    iget-object v10, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mExplanation:Landroid/widget/TextView;

    const v11, 0x7f0800c4

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(I)V

    iget-object v10, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mSkipButton:Landroid/widget/Button;

    const v11, 0x7f080012

    invoke-virtual {v10, v11}, Landroid/widget/Button;->setText(I)V

    goto/16 :goto_1

    :pswitch_5
    iget-object v10, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mExplanation:Landroid/widget/TextView;

    const v11, 0x7f080087

    invoke-virtual {p0, v11}, Lcom/google/android/gsf/login/ShowErrorActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v11

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/CharSequence;

    const/4 v13, 0x0

    iget-object v14, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v14, v14, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUsername:Ljava/lang/String;

    aput-object v14, v12, v13

    invoke-static {v11, v12}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v10, 0x7f080086

    invoke-virtual {p0, v10}, Lcom/google/android/gsf/login/ShowErrorActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/google/android/gsf/login/ShowErrorActivity;->setErrorTitle(Ljava/lang/CharSequence;)V

    const/4 v10, 0x5

    iput v10, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mResult:I

    goto/16 :goto_1

    :pswitch_6
    invoke-virtual {p0}, Lcom/google/android/gsf/login/ShowErrorActivity;->hasNetworkConnection()Z

    move-result v10

    if-nez v10, :cond_5

    const/4 v7, 0x1

    :goto_3
    const/4 v10, 0x0

    iput v10, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mResult:I

    if-eqz v7, :cond_7

    const v11, 0x320ce

    const/4 v10, 0x0

    check-cast v10, Ljava/lang/String;

    invoke-static {v11, v10}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    iget-object v11, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mExplanation:Landroid/widget/TextView;

    invoke-static {}, Lcom/google/android/gsf/login/Compat;->isWifiOnlyBuild()Z

    move-result v10

    if-eqz v10, :cond_6

    const v10, 0x7f08006a

    :goto_4
    invoke-virtual {p0, v10}, Lcom/google/android/gsf/login/ShowErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v11, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v10, 0x7f080068

    invoke-virtual {p0, v10}, Lcom/google/android/gsf/login/ShowErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/google/android/gsf/login/ShowErrorActivity;->setErrorTitle(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_5
    const/4 v7, 0x0

    goto :goto_3

    :cond_6
    const v10, 0x7f080069

    goto :goto_4

    :cond_7
    sget-object v10, Lcom/google/android/gsf/loginservice/GLSUser$Status;->NETWORK_ERROR:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    if-ne v8, v10, :cond_9

    const v11, 0x320cc

    const/4 v10, 0x0

    check-cast v10, Ljava/lang/String;

    invoke-static {v11, v10}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    iget-object v11, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mExplanation:Landroid/widget/TextView;

    invoke-static {}, Lcom/google/android/gsf/login/Compat;->isWifiOnlyBuild()Z

    move-result v10

    if-eqz v10, :cond_8

    const v10, 0x7f08001e

    :goto_5
    invoke-virtual {p0, v10}, Lcom/google/android/gsf/login/ShowErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v11, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v10, 0x7f08001c

    invoke-virtual {p0, v10}, Lcom/google/android/gsf/login/ShowErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/google/android/gsf/login/ShowErrorActivity;->setErrorTitle(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_8
    const v10, 0x7f08001d

    goto :goto_5

    :cond_9
    iget-object v10, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mExplanation:Landroid/widget/TextView;

    const v11, 0x7f08001b

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(I)V

    const v10, 0x7f08001a

    invoke-virtual {p0, v10}, Lcom/google/android/gsf/login/ShowErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/google/android/gsf/login/ShowErrorActivity;->setErrorTitle(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :pswitch_7
    const v10, 0x7f080055

    invoke-virtual {p0, v10}, Lcom/google/android/gsf/login/ShowErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/google/android/gsf/login/ShowErrorActivity;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v11, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mExplanation:Landroid/widget/TextView;

    iget-object v10, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-boolean v10, v10, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mCreatingAccount:Z

    if-eqz v10, :cond_a

    const v10, 0x7f080056

    :goto_6
    invoke-virtual {v11, v10}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_1

    :cond_a
    const v10, 0x7f080057

    goto :goto_6

    :pswitch_8
    const/4 v10, 0x1

    iput v10, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mResult:I

    goto/16 :goto_0

    :cond_b
    iget-object v10, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v10, v10, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mDetail:Ljava/lang/String;

    if-nez v10, :cond_c

    iget-object v10, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mExplanation:Landroid/widget/TextView;

    const v11, 0x7f08001b

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(I)V

    const v10, 0x7f08001a

    invoke-virtual {p0, v10}, Lcom/google/android/gsf/login/ShowErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/google/android/gsf/login/ShowErrorActivity;->setErrorTitle(Ljava/lang/CharSequence;)V

    const-string v10, "GLSActivity"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "GAIA ERROR WITH NO RESOURCE STRING "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v8}, Lcom/google/android/gsf/loginservice/GLSUser$Status;->getWire()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_c
    iget-object v10, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mExplanation:Landroid/widget/TextView;

    iget-object v11, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v11, v11, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mDetail:Ljava/lang/String;

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
    .end packed-switch
.end method

.method public setErrorTitle(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1    # Ljava/lang/CharSequence;

    if-eqz p1, :cond_0

    const v0, 0x7f080088

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/ShowErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mSubmissionTitle:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mSubmissionTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gsf/login/ShowErrorActivity;->mSubmissionTitle:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
