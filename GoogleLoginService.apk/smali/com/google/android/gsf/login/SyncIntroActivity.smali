.class public Lcom/google/android/gsf/login/SyncIntroActivity;
.super Lcom/google/android/gsf/login/BaseActivity;
.source "SyncIntroActivity.java"


# static fields
.field private static final RESTORE_INTENT:Landroid/content/Intent;


# instance fields
.field private mAgreeBackup:Landroid/widget/CompoundButton;

.field private mAgreeRestore:Landroid/widget/CompoundButton;

.field private final mBackupServiceConnection:Landroid/content/ServiceConnection;

.field private mDoneButton:Landroid/view/View;

.field protected mHandler:Landroid/os/Handler;

.field private mPrimaryMessage:Landroid/widget/TextView;

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mState:I

.field private mSyncSettings:Landroid/widget/ListView;

.field private mTopDivider:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.START_RESTORE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gsf/login/SyncIntroActivity;->RESTORE_INTENT:Landroid/content/Intent;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gsf/login/BaseActivity;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mState:I

    new-instance v0, Lcom/google/android/gsf/login/SyncIntroActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/gsf/login/SyncIntroActivity$1;-><init>(Lcom/google/android/gsf/login/SyncIntroActivity;)V

    iput-object v0, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/gsf/login/SyncIntroActivity$2;

    invoke-direct {v0, p0}, Lcom/google/android/gsf/login/SyncIntroActivity$2;-><init>(Lcom/google/android/gsf/login/SyncIntroActivity;)V

    iput-object v0, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mBackupServiceConnection:Landroid/content/ServiceConnection;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/gsf/login/SyncIntroActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/gsf/login/SyncIntroActivity;

    invoke-direct {p0}, Lcom/google/android/gsf/login/SyncIntroActivity;->onRestoreDone()V

    return-void
.end method

.method protected static enableBackup(Landroid/content/Context;Z)Z
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # Z

    const/4 v2, 0x0

    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.google.android.backup.BackupEnabler"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "BACKUP_ENABLE"

    invoke-virtual {v0, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :try_start_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return p1

    :catch_0
    move-exception v1

    const-string v3, "GLSActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Could not enable backup "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move p1, v2

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v3, "GLSActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Could not enable backup "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move p1, v2

    goto :goto_0
.end method

.method private onRestoreDone()V
    .locals 1

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mState:I

    invoke-virtual {p0}, Lcom/google/android/gsf/login/SyncIntroActivity;->relockRotation()V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/SyncIntroActivity;->setupSyncEnableBackup()V

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/SyncIntroActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/SyncIntroActivity;->finish()V

    return-void
.end method

.method private showRestoringScreen()V
    .locals 5

    const/4 v4, 0x0

    const/16 v3, 0x8

    iget-object v1, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mDoneButton:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    const v1, 0x7f0800b8

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/login/SyncIntroActivity;->setTitle(I)V

    iget-object v1, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mProgressBar:Landroid/widget/ProgressBar;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mTopDivider:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mTopDivider:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mPrimaryMessage:Landroid/widget/TextView;

    const v2, 0x7f0800b9

    invoke-virtual {p0, v2}, Lcom/google/android/gsf/login/SyncIntroActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mPrimaryMessage:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    const v1, 0x7f0b004a

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/login/SyncIntroActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    const v1, 0x7f0b000e

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/login/SyncIntroActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    return-void
.end method


# virtual methods
.method protected backupExists()Z
    .locals 8

    const/4 v3, 0x1

    const/4 v4, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gsf/login/SyncIntroActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const-string v6, "com.google.android.backup"

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gsf/login/SyncIntroActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    new-instance v6, Landroid/content/Intent;

    const-string v7, "com.google.android.backup.BackupEnabler"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/pm/PackageManager;->resolveService(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    const-string v6, "GLSActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "backupExists() info: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    if-eqz v2, :cond_0

    move v5, v3

    :goto_0
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", bi: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    if-eqz v0, :cond_1

    move v5, v3

    :goto_1
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v2, :cond_2

    if-eqz v0, :cond_2

    :goto_2
    return v3

    :cond_0
    move v5, v4

    goto :goto_0

    :cond_1
    move v5, v4

    goto :goto_1

    :cond_2
    move v3, v4

    goto :goto_2

    :catch_0
    move-exception v1

    const-string v3, "GLSActivity"

    const-string v5, "Could not find Backup package: com.google.android.backup"

    invoke-static {v3, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v4

    goto :goto_2
.end method

.method public onBackPressed()V
    .locals 2

    const-string v0, "GLSActivity"

    const-string v1, "ignore back key"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 13
    .param p1    # Landroid/os/Bundle;

    const/16 v10, 0x8

    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-super {p0, p1}, Lcom/google/android/gsf/login/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/SyncIntroActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    iget-object v7, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-boolean v0, v7, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mIsNewAccount:Z

    const v7, 0x7f03001c

    invoke-virtual {p0, v7}, Lcom/google/android/gsf/login/SyncIntroActivity;->setContentView(I)V

    const v7, 0x7f0b0049

    invoke-virtual {p0, v7}, Lcom/google/android/gsf/login/SyncIntroActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mPrimaryMessage:Landroid/widget/TextView;

    const v7, 0x7f0b004b

    invoke-virtual {p0, v7}, Lcom/google/android/gsf/login/SyncIntroActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mDoneButton:Landroid/view/View;

    const v7, 0x7f0b0050

    invoke-virtual {p0, v7}, Lcom/google/android/gsf/login/SyncIntroActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/CompoundButton;

    iput-object v7, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mAgreeBackup:Landroid/widget/CompoundButton;

    const v7, 0x7f0b004f

    invoke-virtual {p0, v7}, Lcom/google/android/gsf/login/SyncIntroActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/CompoundButton;

    iput-object v7, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mAgreeRestore:Landroid/widget/CompoundButton;

    const v7, 0x7f0b0032

    invoke-virtual {p0, v7}, Lcom/google/android/gsf/login/SyncIntroActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ProgressBar;

    iput-object v7, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mProgressBar:Landroid/widget/ProgressBar;

    const v7, 0x7f0b0031

    invoke-virtual {p0, v7}, Lcom/google/android/gsf/login/SyncIntroActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mTopDivider:Landroid/view/View;

    iget-object v7, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-boolean v7, v7, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mSetupWizard:Z

    if-eqz v7, :cond_3

    invoke-virtual {p0}, Lcom/google/android/gsf/login/SyncIntroActivity;->backupExists()Z

    move-result v7

    if-eqz v7, :cond_3

    if-nez v0, :cond_3

    move v1, v8

    :goto_0
    iget-object v11, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mAgreeRestore:Landroid/widget/CompoundButton;

    if-eqz v1, :cond_4

    move v7, v9

    :goto_1
    invoke-virtual {v11, v7}, Landroid/widget/CompoundButton;->setVisibility(I)V

    iget-object v7, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v7, v7, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUsername:Ljava/lang/String;

    invoke-static {p0, v7}, Lcom/google/android/gsf/login/BaseActivity;->isFirstGoogleAccount(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    const-string v7, "GLSActivity"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "allowRestore = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", mSetupWizard = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-boolean v12, v12, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mSetupWizard:Z

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", accountWasNewlyCreated = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", first = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v7, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v3, :cond_6

    iget-object v7, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mAgreeBackup:Landroid/widget/CompoundButton;

    invoke-virtual {v7, v8}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v7, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mAgreeRestore:Landroid/widget/CompoundButton;

    invoke-virtual {v7, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    if-eqz v0, :cond_5

    const v7, 0x7f0800d9

    invoke-virtual {p0, v7}, Lcom/google/android/gsf/login/SyncIntroActivity;->setTitle(I)V

    :goto_2
    if-nez v1, :cond_0

    const v7, 0x7f0b004e

    invoke-virtual {p0, v7}, Lcom/google/android/gsf/login/SyncIntroActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    const v10, 0x7f0800b5

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    :goto_3
    invoke-virtual {p0}, Lcom/google/android/gsf/login/SyncIntroActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v7

    const v10, 0x7f0b004c

    invoke-virtual {v7, v10}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v4

    check-cast v4, Landroid/support/v4/app/ListFragment;

    invoke-virtual {v4}, Landroid/support/v4/app/ListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mSyncSettings:Landroid/widget/ListView;

    const v7, 0x7f0b004a

    invoke-virtual {p0, v7}, Lcom/google/android/gsf/login/SyncIntroActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    if-eqz v3, :cond_1

    const/4 v9, 0x4

    :cond_1
    invoke-virtual {v6, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v7, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mDoneButton:Landroid/view/View;

    invoke-virtual {p0, v7, v8}, Lcom/google/android/gsf/login/SyncIntroActivity;->setDefaultButton(Landroid/view/View;Z)V

    if-eqz p1, :cond_2

    const-string v7, "instance_state"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    iput v7, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mState:I

    iget v7, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mState:I

    if-ne v7, v8, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gsf/login/SyncIntroActivity;->performRestore()V

    :cond_2
    return-void

    :cond_3
    move v1, v9

    goto/16 :goto_0

    :cond_4
    move v7, v10

    goto/16 :goto_1

    :cond_5
    const v7, 0x7f0800d8

    invoke-virtual {p0, v7}, Lcom/google/android/gsf/login/SyncIntroActivity;->setTitle(I)V

    goto :goto_2

    :cond_6
    iget-object v7, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mAgreeRestore:Landroid/widget/CompoundButton;

    invoke-virtual {v7, v9}, Landroid/widget/CompoundButton;->setChecked(Z)V

    const v7, 0x7f0b000e

    invoke-virtual {p0, v7}, Lcom/google/android/gsf/login/SyncIntroActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2, v10}, Landroid/view/View;->setVisibility(I)V

    const/4 v7, 0x0

    iput-object v7, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mAgreeBackup:Landroid/widget/CompoundButton;

    goto :goto_3
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/gsf/login/BaseActivity;->onDestroy()V

    :try_start_0
    iget v0, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mBackupServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/SyncIntroActivity;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onRequiresScroll()V
    .locals 3

    const v1, 0x7f0b0014

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/login/SyncIntroActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mScrollDownButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget v1, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mState:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mDoneButton:Landroid/view/View;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/gsf/login/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "instance_state"

    iget v1, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mState:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method public onScrolledToBottom()V
    .locals 3

    const v1, 0x7f0b0014

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/login/SyncIntroActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget v1, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mState:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mDoneButton:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public performRestore()V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mState:I

    invoke-direct {p0}, Lcom/google/android/gsf/login/SyncIntroActivity;->showRestoringScreen()V

    iget-object v0, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mGlsUser:Lcom/google/android/gsf/loginservice/GLSUser;

    invoke-virtual {v0}, Lcom/google/android/gsf/loginservice/GLSUser;->setBackupAccount()Z

    sget-object v0, Lcom/google/android/gsf/login/SyncIntroActivity;->RESTORE_INTENT:Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mBackupServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gsf/login/SyncIntroActivity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "GLSActivity"

    const-string v1, "Could not connect to restore service... skipping"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "GLSActivity"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "GLSActivity"

    const-string v1, "Starting restore service"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    sget-object v0, Lcom/google/android/gsf/login/SyncIntroActivity;->RESTORE_INTENT:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/SyncIntroActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method protected relockRotation()V
    .locals 6

    const/4 v5, 0x0

    const-string v3, "SetupWizardPrefs"

    const/4 v4, 0x4

    invoke-virtual {p0, v3, v4}, Lcom/google/android/gsf/login/SyncIntroActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gsf/login/SyncIntroActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "accelerometer_rotation"

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/gsf/login/SyncIntroActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "user_rotation"

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "setupwizard.accelerometer_rotation"

    invoke-interface {v3, v4, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "setupwizard.user_rotation"

    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-virtual {p0}, Lcom/google/android/gsf/login/SyncIntroActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "accelerometer_rotation"

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-virtual {p0}, Lcom/google/android/gsf/login/SyncIntroActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "user_rotation"

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void
.end method

.method public setupSyncEnableBackup()V
    .locals 18

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gsf/login/SyncIntroActivity;->getIntent()Landroid/content/Intent;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/gsf/login/SyncIntroActivity;->mGlsUser:Lcom/google/android/gsf/loginservice/GLSUser;

    invoke-virtual {v15}, Lcom/google/android/gsf/loginservice/GLSUser;->getAccount()Landroid/accounts/Account;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/gsf/login/SyncIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v15, v15, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUsername:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-static {v0, v15}, Lcom/google/android/gsf/login/BaseActivity;->isFirstGoogleAccount(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_2

    invoke-static {}, Landroid/content/ContentResolver;->getSyncAdapterTypes()[Landroid/content/SyncAdapterType;

    move-result-object v14

    move-object v2, v14

    array-length v12, v2

    const/4 v9, 0x0

    :goto_0
    if-ge v9, v12, :cond_0

    aget-object v13, v2, v9

    iget-object v15, v13, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-static {v1, v15, v0}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    :cond_0
    sget-object v15, Lcom/google/android/gsf/login/SyncIntroActivity;->sTestHooks:Lcom/google/android/gsf/loginservice/BaseActivity$TestHooks;

    array-length v0, v14

    move/from16 v16, v0

    move/from16 v0, v16

    iput v0, v15, Lcom/google/android/gsf/loginservice/BaseActivity$TestHooks;->mSyncEnabledCount:I

    :goto_1
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    const-string v15, "expedited"

    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-virtual {v7, v15, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v15, "com.google.android.apps.magazines"

    invoke-static {v1, v15, v7}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    const-string v15, "com.google.android.apps.books"

    invoke-static {v1, v15, v7}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    const-string v15, "com.google.android.videos.sync"

    invoke-static {v1, v15, v7}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    const-string v15, "com.google.android.music.MusicContent"

    invoke-static {v1, v15, v7}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/gsf/login/SyncIntroActivity;->mAgreeBackup:Landroid/widget/CompoundButton;

    if-eqz v15, :cond_1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/gsf/login/SyncIntroActivity;->mAgreeBackup:Landroid/widget/CompoundButton;

    invoke-virtual {v15}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v15

    if-eqz v15, :cond_1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/gsf/login/SyncIntroActivity;->mGlsUser:Lcom/google/android/gsf/loginservice/GLSUser;

    invoke-virtual {v15}, Lcom/google/android/gsf/loginservice/GLSUser;->setBackupAccount()Z

    sget-object v15, Lcom/google/android/gsf/login/SyncIntroActivity;->sTestHooks:Lcom/google/android/gsf/loginservice/BaseActivity$TestHooks;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/login/SyncIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUsername:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iput-object v0, v15, Lcom/google/android/gsf/loginservice/BaseActivity$TestHooks;->mBackupUserSet:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/gsf/login/SyncIntroActivity;->mAgreeBackup:Landroid/widget/CompoundButton;

    invoke-virtual {v15}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v15

    move-object/from16 v0, p0

    invoke-static {v0, v15}, Lcom/google/android/gsf/login/SyncIntroActivity;->enableBackup(Landroid/content/Context;Z)Z

    :cond_1
    return-void

    :cond_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/gsf/login/SyncIntroActivity;->mSyncSettings:Landroid/widget/ListView;

    invoke-virtual {v15}, Landroid/widget/ListView;->getCount()I

    move-result v5

    const/4 v6, 0x0

    const/4 v8, 0x0

    :goto_2
    if-ge v8, v5, :cond_6

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/gsf/login/SyncIntroActivity;->mSyncSettings:Landroid/widget/ListView;

    invoke-virtual {v15, v8}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/android/gsf/login/SyncSettingsFragment$SyncSettingsItem;

    invoke-virtual {v11}, Lcom/google/android/gsf/login/SyncSettingsFragment$SyncSettingsItem;->getSyncAdapterType()Landroid/content/SyncAdapterType;

    move-result-object v15

    iget-object v3, v15, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/gsf/login/SyncIntroActivity;->mSyncSettings:Landroid/widget/ListView;

    invoke-virtual {v15, v8}, Landroid/widget/ListView;->isItemChecked(I)Z

    move-result v4

    const-string v15, "GLSActivity"

    const/16 v16, 0x2

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v15

    if-eqz v15, :cond_3

    const-string v15, "GLSActivity"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Setting auto sync for account="

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    iget-object v0, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, " authority="

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    invoke-static {v1, v3, v4}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    const-string v15, "GLSActivity"

    const/16 v16, 0x2

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v15

    if-eqz v15, :cond_4

    const-string v15, "GLSActivity"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Requesting sync for account="

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    iget-object v0, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, " authority="

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    if-eqz v4, :cond_5

    add-int/lit8 v6, v6, 0x1

    :cond_5
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_2

    :cond_6
    sget-object v15, Lcom/google/android/gsf/login/SyncIntroActivity;->sTestHooks:Lcom/google/android/gsf/loginservice/BaseActivity$TestHooks;

    iput v6, v15, Lcom/google/android/gsf/loginservice/BaseActivity$TestHooks;->mSyncEnabledCount:I

    goto/16 :goto_1
.end method

.method public start()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, -0x1

    iget v0, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mState:I

    packed-switch v0, :pswitch_data_0

    iget-object v0, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mAgreeBackup:Landroid/widget/CompoundButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mAgreeBackup:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v2}, Landroid/widget/CompoundButton;->setEnabled(Z)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mAgreeRestore:Landroid/widget/CompoundButton;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mAgreeRestore:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v2}, Landroid/widget/CompoundButton;->setEnabled(Z)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mAgreeRestore:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gsf/login/SyncIntroActivity;->performRestore()V

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0, v1}, Lcom/google/android/gsf/login/SyncIntroActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/SyncIntroActivity;->finish()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gsf/login/SyncIntroActivity;->setupSyncEnableBackup()V

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/login/SyncIntroActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/SyncIntroActivity;->finish()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
