.class public Lcom/google/android/gsf/login/UsernameActivity;
.super Lcom/google/android/gsf/login/BaseActivity;
.source "UsernameActivity.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field private mBackButton:Landroid/view/View;

.field private mNextButton:Landroid/view/View;

.field private mUsernameEdit:Landroid/widget/EditText;

.field private mUsernameError:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gsf/login/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/gsf/login/UsernameActivity;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gsf/login/UsernameActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gsf/login/UsernameActivity;->mUsernameError:Z

    return p1
.end method

.method private checkUsernameValidity(Z)V
    .locals 3
    .param p1    # Z

    iget-object v1, p0, Lcom/google/android/gsf/login/UsernameActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/gsf/login/UsernameActivity;->mUsernameError:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gsf/login/UsernameActivity;->mUsernameEdit:Landroid/widget/EditText;

    const v2, 0x7f08001f

    invoke-virtual {p0, v2}, Lcom/google/android/gsf/login/UsernameActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    if-nez p1, :cond_1

    iget-object v1, p0, Lcom/google/android/gsf/login/UsernameActivity;->mUsernameEdit:Landroid/widget/EditText;

    const v2, 0x7f080027

    invoke-virtual {p0, v2}, Lcom/google/android/gsf/login/UsernameActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/UsernameActivity;->validateUsername(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gsf/login/UsernameActivity;->mUsernameEdit:Landroid/widget/EditText;

    const v2, 0x7f080063

    invoke-virtual {p0, v2}, Lcom/google/android/gsf/login/UsernameActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/gsf/login/UsernameActivity;->mUsernameEdit:Landroid/widget/EditText;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private initViews()V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    const v1, 0x7f0b0021

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/login/UsernameActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/google/android/gsf/login/UsernameActivity;->mUsernameEdit:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/gsf/login/UsernameActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {v1, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v1, p0, Lcom/google/android/gsf/login/UsernameActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {v1, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lcom/google/android/gsf/login/UsernameActivity;->mUsernameEdit:Landroid/widget/EditText;

    new-array v2, v5, [Landroid/text/InputFilter;

    new-instance v3, Lcom/google/android/gsf/login/UsernameActivity$1;

    invoke-direct {v3, p0, v5}, Lcom/google/android/gsf/login/UsernameActivity$1;-><init>(Lcom/google/android/gsf/login/UsernameActivity;Z)V

    aput-object v3, v2, v4

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    iget-object v1, p0, Lcom/google/android/gsf/login/UsernameActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {p0, v1, v4}, Lcom/google/android/gsf/login/UsernameActivity;->setDefaultButton(Landroid/view/View;Z)V

    const v1, 0x7f0b000a

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/login/UsernameActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gsf/login/UsernameActivity;->mNextButton:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gsf/login/UsernameActivity;->mNextButton:Landroid/view/View;

    invoke-virtual {p0, v1, v5}, Lcom/google/android/gsf/login/UsernameActivity;->setDefaultButton(Landroid/view/View;Z)V

    const v1, 0x7f0b0013

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/login/UsernameActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gsf/login/UsernameActivity;->mBackButton:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gsf/login/UsernameActivity;->mBackButton:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/login/UsernameActivity;->setBackButton(Landroid/view/View;)V

    const v1, 0x7f0b0022

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/login/UsernameActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gsf/login/UsernameActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p0, v1, v4}, Lcom/google/android/gsf/login/UsernameActivity;->getGmailHost(Landroid/content/res/Resources;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method private populateFields()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/gsf/login/UsernameActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v1, v2, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUsername:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const/16 v2, 0x40

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    :cond_0
    iget-object v2, p0, Lcom/google/android/gsf/login/UsernameActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/gsf/login/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030009

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/UsernameActivity;->setContentView(I)V

    invoke-direct {p0}, Lcom/google/android/gsf/login/UsernameActivity;->initViews()V

    invoke-direct {p0}, Lcom/google/android/gsf/login/UsernameActivity;->populateFields()V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/UsernameActivity;->updateWidgetState()V

    return-void
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Z

    iget-object v0, p0, Lcom/google/android/gsf/login/UsernameActivity;->mUsernameEdit:Landroid/widget/EditText;

    if-ne p1, v0, :cond_0

    invoke-direct {p0, p2}, Lcom/google/android/gsf/login/UsernameActivity;->checkUsernameValidity(Z)V

    :cond_0
    return-void
.end method

.method public start()V
    .locals 5

    invoke-super {p0}, Lcom/google/android/gsf/login/BaseActivity;->start()V

    iget-object v2, p0, Lcom/google/android/gsf/login/UsernameActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "GLSActivity"

    const-string v3, "username was empty in CreateAccountActivity"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/UsernameActivity;->validateUsername(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/google/android/gsf/login/UsernameActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iput-object v1, v2, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUsername:Ljava/lang/String;

    const/4 v2, -0x1

    invoke-virtual {p0, v2}, Lcom/google/android/gsf/login/UsernameActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/UsernameActivity;->finish()V

    :goto_0
    return-void

    :cond_1
    const-string v2, "GLSActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Can\'t create a valid email from \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/google/android/gsf/login/UsernameActivity;->mUsernameEdit:Landroid/widget/EditText;

    const v3, 0x7f080063

    invoke-virtual {p0, v3}, Lcom/google/android/gsf/login/UsernameActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public updateWidgetState()V
    .locals 5

    const/4 v3, 0x1

    invoke-super {p0}, Lcom/google/android/gsf/login/BaseActivity;->updateWidgetState()V

    iget-object v4, p0, Lcom/google/android/gsf/login/UsernameActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-boolean v4, p0, Lcom/google/android/gsf/login/UsernameActivity;->mUsernameError:Z

    if-nez v4, :cond_0

    move v2, v3

    :goto_0
    move v0, v2

    iget-object v4, p0, Lcom/google/android/gsf/login/UsernameActivity;->mNextButton:Landroid/view/View;

    invoke-virtual {v4, v0}, Landroid/view/View;->setEnabled(Z)V

    iget-object v4, p0, Lcom/google/android/gsf/login/UsernameActivity;->mNextButton:Landroid/view/View;

    invoke-virtual {v4, v0}, Landroid/view/View;->setFocusable(Z)V

    invoke-direct {p0, v3}, Lcom/google/android/gsf/login/UsernameActivity;->checkUsernameValidity(Z)V

    return-void

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
