.class Lcom/google/android/gsf/login/UsernamePasswordActivity$6;
.super Ljava/lang/Object;
.source "UsernamePasswordActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gsf/login/UsernamePasswordActivity;->showAgreementDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gsf/login/UsernamePasswordActivity;

.field final synthetic val$optin:Landroid/widget/CheckBox;


# direct methods
.method constructor <init>(Lcom/google/android/gsf/login/UsernamePasswordActivity;Landroid/widget/CheckBox;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity$6;->this$0:Lcom/google/android/gsf/login/UsernamePasswordActivity;

    iput-object p2, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity$6;->val$optin:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity$6;->this$0:Lcom/google/android/gsf/login/UsernamePasswordActivity;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/gsf/login/UsernamePasswordActivity;->mNeedsDialog:Z
    invoke-static {v0, v1}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->access$402(Lcom/google/android/gsf/login/UsernamePasswordActivity;Z)Z

    iget-object v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity$6;->this$0:Lcom/google/android/gsf/login/UsernamePasswordActivity;

    # getter for: Lcom/google/android/gsf/login/UsernamePasswordActivity;->mGooglePlayOptInCheckBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->access$500(Lcom/google/android/gsf/login/UsernamePasswordActivity;)Landroid/widget/CheckBox;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity$6;->val$optin:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity$6;->this$0:Lcom/google/android/gsf/login/UsernamePasswordActivity;

    invoke-virtual {v0}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->start()V

    return-void
.end method
