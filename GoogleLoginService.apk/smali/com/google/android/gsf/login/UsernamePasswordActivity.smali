.class public Lcom/google/android/gsf/login/UsernamePasswordActivity;
.super Lcom/google/android/gsf/login/BaseActivity;
.source "UsernamePasswordActivity.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field private mBackButton:Landroid/view/View;

.field private mBrowserSignin:Z

.field private mGooglePlayOptInCheckBox:Landroid/widget/CheckBox;

.field private mLastPauseMillis:J

.field private mNeedsDialog:Z

.field private mNextButton:Landroid/view/View;

.field private mNotUserLabel:Landroid/widget/TextView;

.field protected mPasswordEdit:Landroid/widget/EditText;

.field private mPasswordError:Z

.field private mProvisionedText:Landroid/widget/TextView;

.field private mScrolledToBottom:Z

.field private mShouldFocusToPassword:Z

.field private mShowAgreement:Z

.field private mShowChrome:Z

.field private mSignInAgreementLabel:Landroid/widget/TextView;

.field protected mUsernameEdit:Landroid/widget/EditText;

.field private mUsernameError:Z

.field protected prefilledPassword:Ljava/lang/String;

.field protected prefilledUsername:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gsf/login/BaseActivity;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->prefilledUsername:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->prefilledPassword:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/gsf/login/UsernamePasswordActivity;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gsf/login/UsernamePasswordActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mUsernameError:Z

    return p1
.end method

.method static synthetic access$102(Lcom/google/android/gsf/login/UsernamePasswordActivity;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gsf/login/UsernamePasswordActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mPasswordError:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/gsf/login/UsernamePasswordActivity;)Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    .locals 1
    .param p0    # Lcom/google/android/gsf/login/UsernamePasswordActivity;

    iget-object v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/gsf/login/UsernamePasswordActivity;)Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    .locals 1
    .param p0    # Lcom/google/android/gsf/login/UsernamePasswordActivity;

    iget-object v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/gsf/login/UsernamePasswordActivity;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gsf/login/UsernamePasswordActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mNeedsDialog:Z

    return p1
.end method

.method static synthetic access$500(Lcom/google/android/gsf/login/UsernamePasswordActivity;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/google/android/gsf/login/UsernamePasswordActivity;

    iget-object v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mGooglePlayOptInCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method private showAgreementDialog()V
    .locals 8

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f030011

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v5, 0x7f0b002f

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v5, 0xf

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setAutoLinkMask(I)V

    iget-boolean v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mShowChrome:Z

    if-eqz v5, :cond_0

    const v5, 0x7f080060

    invoke-static {p0, v5}, Lcom/google/android/gsf/login/LinkSpan;->linkify(Lcom/google/android/gsf/login/BaseActivity;I)Ljava/lang/CharSequence;

    move-result-object v3

    :goto_0
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    const v5, 0x7f0b0030

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    iget-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mGooglePlayOptInCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v5}, Landroid/widget/CheckBox;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mGooglePlayOptInCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v5}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    new-instance v5, Landroid/app/AlertDialog$Builder;

    invoke-direct {v5, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v6, 0x7f080011

    new-instance v7, Lcom/google/android/gsf/login/UsernamePasswordActivity$6;

    invoke-direct {v7, p0, v4}, Lcom/google/android/gsf/login/UsernamePasswordActivity$6;-><init>(Lcom/google/android/gsf/login/UsernamePasswordActivity;Landroid/widget/CheckBox;)V

    invoke-virtual {v5, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    const v6, 0x7f080012

    new-instance v7, Lcom/google/android/gsf/login/UsernamePasswordActivity$5;

    invoke-direct {v7, p0}, Lcom/google/android/gsf/login/UsernamePasswordActivity$5;-><init>(Lcom/google/android/gsf/login/UsernamePasswordActivity;)V

    invoke-virtual {v5, v6, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    new-instance v6, Lcom/google/android/gsf/login/UsernamePasswordActivity$4;

    invoke-direct {v6, p0}, Lcom/google/android/gsf/login/UsernamePasswordActivity$4;-><init>(Lcom/google/android/gsf/login/UsernamePasswordActivity;)V

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v2

    return-void

    :cond_0
    const v5, 0x7f08005f

    invoke-static {p0, v5}, Lcom/google/android/gsf/login/LinkSpan;->linkify(Lcom/google/android/gsf/login/BaseActivity;I)Ljava/lang/CharSequence;

    move-result-object v3

    goto :goto_0
.end method

.method private updateViews(Z)V
    .locals 3
    .param p1    # Z

    const/16 v2, 0x8

    const/4 v1, 0x0

    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mShowAgreement:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mGooglePlayOptInCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mSignInAgreementLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mSignInAgreementLabel:Landroid/widget/TextView;

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAutoLinkMask(I)V

    iget-object v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mSignInAgreementLabel:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mGooglePlayOptInCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mSignInAgreementLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private validateEmail(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p0, v1, p1}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->appendGmailHost(Landroid/content/res/Resources;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->validateDomainNameOnly(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lt v1, v3, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x40

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-ge v1, v3, :cond_1

    :cond_0
    const/4 v0, 0x0

    :cond_1
    return-object v0
.end method


# virtual methods
.method protected getContentView()I
    .locals 1

    const v0, 0x7f030010

    return v0
.end method

.method protected hasMenu()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected initViews()V
    .locals 12

    const/16 v11, 0x8

    const/4 v10, 0x1

    const/4 v9, 0x0

    const v5, 0x7f0b000a

    invoke-virtual {p0, v5}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mNextButton:Landroid/view/View;

    iget-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mNextButton:Landroid/view/View;

    invoke-virtual {p0, v5, v10}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->setDefaultButton(Landroid/view/View;Z)V

    const v5, 0x7f0b0013

    invoke-virtual {p0, v5}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mBackButton:Landroid/view/View;

    iget-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mBackButton:Landroid/view/View;

    invoke-virtual {p0, v5}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->setBackButton(Landroid/view/View;)V

    const v5, 0x7f0b0002

    invoke-virtual {p0, v5}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v5, 0x7f0800d4

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(I)V

    const v5, 0x7f0b0021

    invoke-virtual {p0, v5}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    iput-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    const v5, 0x7f0b001d

    invoke-virtual {p0, v5}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    iput-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mPasswordEdit:Landroid/widget/EditText;

    const v5, 0x7f0b002c

    invoke-virtual {p0, v5}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mProvisionedText:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v5, v5, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mProvisionedEmail:Ljava/lang/String;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v5, v5, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mProvisionedName:Ljava/lang/String;

    if-eqz v5, :cond_0

    const v5, 0x7f080059

    invoke-virtual {p0, v5}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, v10, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v7, v7, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mProvisionedName:Ljava/lang/String;

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mProvisionedText:Landroid/widget/TextView;

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mProvisionedText:Landroid/widget/TextView;

    const v6, 0x7f08005a

    invoke-virtual {p0, v6}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-array v7, v10, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v8, v8, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mProvisionedEmail:Ljava/lang/String;

    aput-object v8, v7, v9

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {v5, v11}, Landroid/widget/EditText;->setVisibility(I)V

    iput-boolean v10, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mShouldFocusToPassword:Z

    :goto_0
    iget-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mPasswordEdit:Landroid/widget/EditText;

    new-array v6, v10, [Landroid/text/InputFilter;

    new-instance v7, Lcom/google/android/gsf/login/UsernamePasswordActivity$2;

    invoke-direct {v7, p0, v10}, Lcom/google/android/gsf/login/UsernamePasswordActivity$2;-><init>(Lcom/google/android/gsf/login/UsernamePasswordActivity;Z)V

    aput-object v7, v6, v9

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    iget-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mPasswordEdit:Landroid/widget/EditText;

    invoke-virtual {v5, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mPasswordEdit:Landroid/widget/EditText;

    invoke-virtual {v5, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mPasswordEdit:Landroid/widget/EditText;

    invoke-virtual {p0, v5, v9}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->setDefaultButton(Landroid/view/View;Z)V

    const v5, 0x7f0b002e

    invoke-virtual {p0, v5}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mNotUserLabel:Landroid/widget/TextView;

    const v5, 0x7f0b002f

    invoke-virtual {p0, v5}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mSignInAgreementLabel:Landroid/widget/TextView;

    const v5, 0x7f0b0030

    invoke-virtual {p0, v5}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    iput-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mGooglePlayOptInCheckBox:Landroid/widget/CheckBox;

    iget-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mGooglePlayOptInCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {p0}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->getGooglePlayOptInDefault()Z

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v5, v5, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mProvisionedEmail:Ljava/lang/String;

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v5, v5, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mProvisionedName:Ljava/lang/String;

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mBackButton:Landroid/view/View;

    invoke-virtual {v5, v11}, Landroid/view/View;->setVisibility(I)V

    const v5, 0x7f08005b

    invoke-virtual {p0, v5}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, v10, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v7, v7, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mProvisionedName:Ljava/lang/String;

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Landroid/text/Spannable$Factory;->getInstance()Landroid/text/Spannable$Factory;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/text/Spannable$Factory;->newSpannable(Ljava/lang/CharSequence;)Landroid/text/Spannable;

    move-result-object v2

    new-instance v5, Lcom/google/android/gsf/login/UsernamePasswordActivity$3;

    invoke-direct {v5, p0}, Lcom/google/android/gsf/login/UsernamePasswordActivity$3;-><init>(Lcom/google/android/gsf/login/UsernamePasswordActivity;)V

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    const/16 v7, 0x21

    invoke-interface {v2, v5, v9, v6, v7}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    iget-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mNotUserLabel:Landroid/widget/TextView;

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mNotUserLabel:Landroid/widget/TextView;

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mNotUserLabel:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    :goto_1
    iget-boolean v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mAddAccount:Z

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mGooglePlayOptInCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v5, v9}, Landroid/widget/CheckBox;->setVisibility(I)V

    iget-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mSignInAgreementLabel:Landroid/widget/TextView;

    const/16 v6, 0xf

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setAutoLinkMask(I)V

    iget-boolean v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mShowChrome:Z

    if-eqz v5, :cond_5

    const v5, 0x7f080060

    invoke-static {p0, v5}, Lcom/google/android/gsf/login/LinkSpan;->linkify(Lcom/google/android/gsf/login/BaseActivity;I)Ljava/lang/CharSequence;

    move-result-object v1

    :goto_2
    iget-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mSignInAgreementLabel:Landroid/widget/TextView;

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mSignInAgreementLabel:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    iput-boolean v10, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mShowAgreement:Z

    :goto_3
    return-void

    :cond_0
    iget-boolean v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mAddAccount:Z

    if-nez v5, :cond_2

    iget-boolean v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mConfirmCredentials:Z

    if-nez v5, :cond_1

    const v5, 0x7f0800d5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(I)V

    :cond_1
    iget-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {v5, v11}, Landroid/widget/EditText;->setVisibility(I)V

    const v5, 0x7f0b002d

    invoke-virtual {p0, v5}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iget-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v5, v5, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUsername:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mPasswordEdit:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->requestFocus()Z

    goto/16 :goto_0

    :cond_2
    iget-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    new-array v6, v10, [Landroid/text/InputFilter;

    new-instance v7, Lcom/google/android/gsf/login/UsernamePasswordActivity$1;

    invoke-direct {v7, p0, v10}, Lcom/google/android/gsf/login/UsernamePasswordActivity$1;-><init>(Lcom/google/android/gsf/login/UsernamePasswordActivity;Z)V

    aput-object v7, v6, v9

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    iget-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {v5, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {v5, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v5, v5, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUsername:Ljava/lang/String;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    iget-object v6, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v6, v6, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUsername:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mPasswordEdit:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->requestFocus()Z

    iput-boolean v10, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mShouldFocusToPassword:Z

    goto/16 :goto_0

    :cond_3
    iget-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->requestFocus()Z

    goto/16 :goto_0

    :cond_4
    iget-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mNotUserLabel:Landroid/widget/TextView;

    invoke-virtual {v5, v11}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    :cond_5
    const v5, 0x7f08005f

    invoke-static {p0, v5}, Lcom/google/android/gsf/login/LinkSpan;->linkify(Lcom/google/android/gsf/login/BaseActivity;I)Ljava/lang/CharSequence;

    move-result-object v1

    goto/16 :goto_2

    :cond_6
    iget-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mGooglePlayOptInCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v5, v11}, Landroid/widget/CheckBox;->setVisibility(I)V

    iget-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mSignInAgreementLabel:Landroid/widget/TextView;

    invoke-virtual {v5, v11}, Landroid/widget/TextView;->setVisibility(I)V

    iput-boolean v9, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mShowAgreement:Z

    goto/16 :goto_3
.end method

.method protected maybePrefillFields()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->prefilledUsername:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mPasswordEdit:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mPasswordEdit:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->prefilledPassword:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1    # Landroid/content/res/Configuration;

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mShowAgreement:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mGooglePlayOptInCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mSignInAgreementLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mScrollView:Lcom/google/android/gsf/login/BottomScrollView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mScrollView:Lcom/google/android/gsf/login/BottomScrollView;

    invoke-virtual {v0}, Lcom/google/android/gsf/login/BottomScrollView;->reset()V

    iput-boolean v1, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mNeedsDialog:Z

    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/gsf/login/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/gsf/login/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->isChromeInstalled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mShowChrome:Z

    invoke-virtual {p0}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->getContentView()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->initViews()V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->maybePrefillFields()V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->updateWidgetState()V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1    # Landroid/view/Menu;

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/google/android/gsf/login/BaseActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    const v1, 0x7f080077

    invoke-interface {p1, v2, v3, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f02002c

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/gsf/login/Compat;->menuItemSetShowAsAction(Landroid/view/MenuItem;I)V

    return v3
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # Z

    const v3, 0x7f080027

    iget-object v2, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    if-ne p1, v2, :cond_4

    if-nez p2, :cond_4

    iget-boolean v2, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mAddAccount:Z

    if-eqz v2, :cond_4

    iget-boolean v2, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mUsernameError:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    const v3, 0x7f08001f

    invoke-virtual {p0, v3}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {p0, v3}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, v0}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->validateEmail(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v2, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    const v3, 0x7f080063

    invoke-virtual {p0, v3}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mPasswordEdit:Landroid/widget/EditText;

    if-ne p1, v2, :cond_0

    if-nez p2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mPasswordError:Z

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mPasswordEdit:Landroid/widget/EditText;

    const v3, 0x7f080020

    invoke-virtual {p0, v3}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mPasswordEdit:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mPasswordEdit:Landroid/widget/EditText;

    invoke-virtual {p0, v3}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    const/4 v1, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    invoke-super {p0, p1}, Lcom/google/android/gsf/login/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    :pswitch_0
    iput-boolean v1, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mBrowserSignin:Z

    iput-boolean v1, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mNeedsDialog:Z

    invoke-virtual {p0}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->start()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/gsf/login/BaseActivity;->onPause()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mLastPauseMillis:J

    return-void
.end method

.method public onRequiresScroll()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/gsf/login/BaseActivity;->onRequiresScroll()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mNeedsDialog:Z

    invoke-virtual {p0}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->updateWidgetState()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->updateViews(Z)V

    return-void
.end method

.method protected onResume()V
    .locals 5

    const/4 v4, 0x0

    invoke-super {p0}, Lcom/google/android/gsf/login/BaseActivity;->onResume()V

    iget-object v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->prefilledUsername:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTextKeepState(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->updateWidgetState()V

    iget-object v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mLastPauseMillis:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mPasswordEdit:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->prefilledPassword:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTextKeepState(Ljava/lang/CharSequence;)V

    iget-boolean v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mShouldFocusToPassword:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mPasswordEdit:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->prefilledPassword:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mPasswordError:Z

    iget-object v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mPasswordEdit:Landroid/widget/EditText;

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_1
.end method

.method public onScrolledToBottom()V
    .locals 2

    const/4 v1, 0x1

    invoke-super {p0}, Lcom/google/android/gsf/login/BaseActivity;->onScrolledToBottom()V

    iget-boolean v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mNeedsDialog:Z

    if-nez v0, :cond_0

    iput-boolean v1, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mScrolledToBottom:Z

    invoke-virtual {p0}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->updateWidgetState()V

    invoke-direct {p0, v1}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->updateViews(Z)V

    :cond_0
    return-void
.end method

.method public start()V
    .locals 4

    const/4 v3, 0x1

    invoke-super {p0}, Lcom/google/android/gsf/login/BaseActivity;->start()V

    iget-boolean v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mNeedsDialog:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mShowAgreement:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->showAgreementDialog()V

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mBrowserSignin:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mProvisionedEmail:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v1, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v1, v1, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mProvisionedEmail:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUsername:Ljava/lang/String;

    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->getUserData()Ljava/util/HashMap;

    move-result-object v0

    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->PASSWORD:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v1}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mPasswordEdit:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mShowAgreement:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iput-boolean v3, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAgreedToPlayTos:Z

    iget-object v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v1, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mGooglePlayOptInCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    iput-boolean v1, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAgreedToPlayEmail:Z

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mShowChrome:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iput-boolean v3, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAgreedToChromeTosAndPrivacy:Z

    :cond_4
    iget-boolean v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mBrowserSignin:Z

    if-eqz v0, :cond_6

    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->setResult(I)V

    :goto_2
    invoke-virtual {p0}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->finish()V

    goto :goto_0

    :cond_5
    iget-boolean v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mAddAccount:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v1, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUsername:Ljava/lang/String;

    goto :goto_1

    :cond_6
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->setResult(I)V

    goto :goto_2
.end method

.method public updateWidgetState()V
    .locals 8

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-super {p0}, Lcom/google/android/gsf/login/BaseActivity;->updateWidgetState()V

    iget-object v7, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    iget-object v7, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mPasswordEdit:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    iget-boolean v7, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mUsernameError:Z

    if-nez v7, :cond_3

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/google/android/gsf/login/UsernamePasswordActivity;->validateEmail(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_3

    move v4, v5

    :goto_0
    iget-object v7, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v7, v7, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mProvisionedEmail:Ljava/lang/String;

    if-nez v7, :cond_0

    iget-boolean v7, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mAddAccount:Z

    if-nez v7, :cond_1

    :cond_0
    const/4 v4, 0x1

    :cond_1
    iget-boolean v7, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mPasswordError:Z

    if-nez v7, :cond_4

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_4

    move v3, v5

    :goto_1
    if-eqz v4, :cond_5

    if-eqz v3, :cond_5

    iget-boolean v7, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mScrolledToBottom:Z

    if-nez v7, :cond_2

    iget-boolean v7, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mNeedsDialog:Z

    if-eqz v7, :cond_5

    :cond_2
    move v0, v5

    :goto_2
    iget-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mNextButton:Landroid/view/View;

    invoke-virtual {v5, v0}, Landroid/view/View;->setEnabled(Z)V

    iget-object v5, p0, Lcom/google/android/gsf/login/UsernamePasswordActivity;->mNextButton:Landroid/view/View;

    invoke-virtual {v5, v0}, Landroid/view/View;->setFocusable(Z)V

    return-void

    :cond_3
    move v4, v6

    goto :goto_0

    :cond_4
    move v3, v6

    goto :goto_1

    :cond_5
    move v0, v6

    goto :goto_2
.end method
