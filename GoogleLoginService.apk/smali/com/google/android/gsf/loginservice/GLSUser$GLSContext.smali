.class public Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;
.super Ljava/lang/Object;
.source "GLSUser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gsf/loginservice/GLSUser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GLSContext"
.end annotation


# instance fields
.field mAccountManager:Landroid/accounts/AccountManager;

.field private mAndroidIdHex:Ljava/lang/String;

.field mContext:Landroid/content/Context;

.field mHttpClient:Lorg/apache/http/client/HttpClient;

.field mHttpTestInjector:Lcom/google/android/gsf/loginservice/GLSUser$HttpTestInjector;

.field final mLastErrors:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mPM:Landroid/content/pm/PackageManager;

.field final mSessions:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;",
            ">;"
        }
    .end annotation
.end field

.field mTestNoPermission:Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/content/Context;Landroid/accounts/AccountManager;Landroid/content/pm/PackageManager;Lorg/apache/http/client/HttpClient;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/accounts/AccountManager;
    .param p3    # Landroid/content/pm/PackageManager;
    .param p4    # Lorg/apache/http/client/HttpClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mLastErrors:Ljava/util/LinkedList;

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mSessions:Ljava/util/LinkedHashMap;

    iput-object p1, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iput-object p3, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mPM:Landroid/content/pm/PackageManager;

    iput-object p4, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mHttpClient:Lorg/apache/http/client/HttpClient;

    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Landroid/accounts/AccountManager;Landroid/content/pm/PackageManager;Lorg/apache/http/client/HttpClient;Lcom/google/android/gsf/loginservice/GLSUser$1;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/accounts/AccountManager;
    .param p3    # Landroid/content/pm/PackageManager;
    .param p4    # Lorg/apache/http/client/HttpClient;
    .param p5    # Lcom/google/android/gsf/loginservice/GLSUser$1;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;-><init>(Landroid/content/Context;Landroid/accounts/AccountManager;Landroid/content/pm/PackageManager;Lorg/apache/http/client/HttpClient;)V

    return-void
.end method

.method static jsonError(Lcom/google/android/gsf/loginservice/GLSUser$Status;)Lorg/json/JSONObject;
    .locals 3
    .param p0    # Lcom/google/android/gsf/loginservice/GLSUser$Status;

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->JSON_STATUS:Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;

    invoke-virtual {v1}, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gsf/loginservice/GLSUser$Status;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public addSession(Landroid/content/Intent;Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;)Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    .locals 1
    .param p1    # Landroid/content/Intent;
    .param p2    # Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    if-eqz p2, :cond_0

    invoke-virtual {p2, p1}, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->addSession(Landroid/content/Intent;)V

    move-object v0, p2

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->newSession()Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->addSession(Landroid/content/Intent;)V

    move-object v0, p2

    goto :goto_0
.end method

.method public checkRealName(Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;)Lcom/google/android/gsf/loginservice/GLSUser$Status;
    .locals 7
    .param p1    # Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    :try_start_0
    new-instance v1, Lorg/json/JSONStringer;

    invoke-direct {v1}, Lorg/json/JSONStringer;-><init>()V

    invoke-virtual {v1}, Lorg/json/JSONStringer;->object()Lorg/json/JSONStringer;

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->prepareRequestNoUser(Lorg/json/JSONStringer;)V

    sget-object v4, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->FIRST_NAME:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v4}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, p1, v1, v4}, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->copyFromProfile(Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;Lorg/json/JSONStringer;Ljava/lang/String;)V

    sget-object v4, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->LAST_NAME:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v4}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, p1, v1, v4}, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->copyFromProfile(Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;Lorg/json/JSONStringer;Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/json/JSONStringer;->endObject()Lorg/json/JSONStringer;

    # getter for: Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;
    invoke-static {}, Lcom/google/android/gsf/loginservice/GLSUser;->access$000()Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    move-result-object v4

    const-string v5, "https://android.clients.google.com/setup/checkname"

    const-string v6, "checkRealName"

    invoke-virtual {v4, v5, v1, v6}, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->httpJson(Ljava/lang/String;Lorg/json/JSONStringer;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "checkRealNam: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lorg/json/JSONStringer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Res: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->log(Ljava/lang/String;)V

    invoke-static {v2}, Lcom/google/android/gsf/loginservice/GLSUser$Status;->fromJSON(Lorg/json/JSONObject;)Lcom/google/android/gsf/loginservice/GLSUser$Status;

    move-result-object v3

    sget-object v4, Lcom/google/android/gsf/loginservice/GLSUser$Status;->GPLUS_INTERSTITIAL:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    if-eq v3, v4, :cond_0

    sget-object v4, Lcom/google/android/gsf/loginservice/GLSUser$Status;->GPLUS_INVALID_CHAR:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    if-eq v3, v4, :cond_0

    sget-object v4, Lcom/google/android/gsf/loginservice/GLSUser$Status;->GPLUS_NICKNAME:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    if-eq v3, v4, :cond_0

    sget-object v4, Lcom/google/android/gsf/loginservice/GLSUser$Status;->GPLUS_OTHER:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    if-ne v3, v4, :cond_1

    :cond_0
    :goto_0
    return-object v3

    :cond_1
    sget-object v3, Lcom/google/android/gsf/loginservice/GLSUser$Status;->SUCCESS:Lcom/google/android/gsf/loginservice/GLSUser$Status;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v3, Lcom/google/android/gsf/loginservice/GLSUser$Status;->SUCCESS:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    goto :goto_0
.end method

.method copyFromProfile(Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;Lorg/json/JSONStringer;Ljava/lang/String;)V
    .locals 2
    .param p1    # Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    .param p2    # Lorg/json/JSONStringer;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    invoke-virtual {p2, p3}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUserData:Ljava/util/HashMap;

    invoke-virtual {v1, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    return-void
.end method

.method public dump(Ljava/io/PrintWriter;)V
    .locals 5
    .param p1    # Ljava/io/PrintWriter;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Sessions: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mSessions:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mLastErrors:Ljava/util/LinkedList;

    monitor-enter v3

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mLastErrors:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public getAndroidIdHex()Ljava/lang/String;
    .locals 6

    const-wide/16 v4, 0x0

    iget-object v2, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mAndroidIdHex:Ljava/lang/String;

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "android_id"

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    cmp-long v2, v0, v4

    if-eqz v2, :cond_0

    invoke-static {v0, v1}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mAndroidIdHex:Ljava/lang/String;

    :cond_0
    iget-object v2, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mAndroidIdHex:Ljava/lang/String;

    return-object v2
.end method

.method public getInjector()Lcom/google/android/gsf/loginservice/GLSUser$HttpTestInjector;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mHttpTestInjector:Lcom/google/android/gsf/loginservice/GLSUser$HttpTestInjector;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gsf/loginservice/GLSUser$HttpTestInjector;

    invoke-direct {v0}, Lcom/google/android/gsf/loginservice/GLSUser$HttpTestInjector;-><init>()V

    iput-object v0, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mHttpTestInjector:Lcom/google/android/gsf/loginservice/GLSUser$HttpTestInjector;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mHttpTestInjector:Lcom/google/android/gsf/loginservice/GLSUser$HttpTestInjector;

    return-object v0
.end method

.method public getSession(Ljava/lang/String;)Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mSessions:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    if-nez v0, :cond_1

    const-string v1, "GLSUser"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "GLSUser"

    const-string v2, "Session was previously removed, creating new one"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    invoke-direct {v0, p1}, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;-><init>(Ljava/lang/String;)V

    # getter for: Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;
    invoke-static {}, Lcom/google/android/gsf/loginservice/GLSUser;->access$000()Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mSessions:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public getSessionOrNull(Ljava/lang/String;)Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mSessions:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    return-object v0
.end method

.method public httpJson(Ljava/lang/String;Lorg/json/JSONStringer;Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Lorg/json/JSONStringer;
    .param p3    # Ljava/lang/String;

    const-string v2, ""

    :try_start_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->requestJson(Ljava/lang/String;Lorg/json/JSONStringer;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-object v3

    :catch_0
    move-exception v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Json request failed: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->log(Ljava/lang/String;)V

    sget-object v4, Lcom/google/android/gsf/loginservice/GLSUser$Status;->NETWORK_ERROR:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    invoke-static {v4}, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->jsonError(Lcom/google/android/gsf/loginservice/GLSUser$Status;)Lorg/json/JSONObject;

    move-result-object v3

    goto :goto_0

    :catch_1
    move-exception v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Json request failed: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->log(Ljava/lang/String;)V

    sget-object v4, Lcom/google/android/gsf/loginservice/GLSUser$Status;->NETWORK_ERROR:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    invoke-static {v4}, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->jsonError(Lcom/google/android/gsf/loginservice/GLSUser$Status;)Lorg/json/JSONObject;

    move-result-object v3

    goto :goto_0
.end method

.method public httpPost(Ljava/lang/String;Lorg/apache/http/HttpEntity;Lorg/apache/http/Header;Ljava/lang/String;)Lorg/apache/http/HttpResponse;
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Lorg/apache/http/HttpEntity;
    .param p3    # Lorg/apache/http/Header;
    .param p4    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mHttpTestInjector:Lcom/google/android/gsf/loginservice/GLSUser$HttpTestInjector;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mHttpTestInjector:Lcom/google/android/gsf/loginservice/GLSUser$HttpTestInjector;

    invoke-virtual {v3, p1, p2, p3, p4}, Lcom/google/android/gsf/loginservice/GLSUser$HttpTestInjector;->httpPost(Ljava/lang/String;Lorg/apache/http/HttpEntity;Lorg/apache/http/Header;Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v2

    :cond_0
    if-nez v2, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v3

    const v4, 0xffffff

    and-int/2addr v3, v4

    invoke-static {v3}, Landroid/support/v4/net/TrafficStatsCompat;->setThreadStatsTag(I)V

    :try_start_0
    new-instance v1, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v1, p1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    if-eqz p3, :cond_1

    invoke-virtual {v1, p3}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Lorg/apache/http/Header;)V

    :cond_1
    # getter for: Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;
    invoke-static {}, Lcom/google/android/gsf/loginservice/GLSUser;->access$000()Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mHttpClient:Lorg/apache/http/client/HttpClient;

    invoke-interface {v3, v1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    invoke-static {}, Landroid/support/v4/net/TrafficStatsCompat;->clearThreadStatsTag()V

    :cond_2
    return-object v2

    :catch_0
    move-exception v0

    :try_start_1
    new-instance v3, Ljava/io/IOException;

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v3

    invoke-static {}, Landroid/support/v4/net/TrafficStatsCompat;->clearThreadStatsTag()V

    throw v3
.end method

.method public log(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v0, "Token=[^&\n;]*"

    const-string v1, "Token=SECRET"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "LSID=[^&\n;]*"

    const-string v1, "LSID=SECRET"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "SID=[^&\n;]*"

    const-string v1, "SID=SECRET"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "auth=[^&\n;]*"

    const-string v1, "auth=SECRET"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "EncryptedPasswd=[^&\n;]*"

    const-string v1, "EncryptedPasswd=SECRET"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "Passwd=[^&\n;]*"

    const-string v1, "Passwd=SECRET"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "GLSUser"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "GLSUser"

    invoke-static {v0, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v1, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mLastErrors:Ljava/util/LinkedList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mLastErrors:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mLastErrors:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/16 v2, 0x28

    if-le v0, v2, :cond_2

    iget-object v0, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mLastErrors:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    :cond_2
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public newSession()Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    .locals 4

    # getter for: Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;
    invoke-static {}, Lcom/google/android/gsf/loginservice/GLSUser;->access$000()Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mSessions:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->size()I

    move-result v2

    const/16 v3, 0x80

    if-le v2, v3, :cond_0

    # getter for: Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;
    invoke-static {}, Lcom/google/android/gsf/loginservice/GLSUser;->access$000()Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mSessions:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    # getter for: Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;
    invoke-static {}, Lcom/google/android/gsf/loginservice/GLSUser;->access$000()Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mSessions:Ljava/util/LinkedHashMap;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    new-instance v1, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    invoke-virtual {v2}, Ljava/util/Random;->nextLong()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;-><init>(Ljava/lang/String;)V

    # getter for: Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;
    invoke-static {}, Lcom/google/android/gsf/loginservice/GLSUser;->access$000()Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mSessions:Ljava/util/LinkedHashMap;

    iget-object v3, v1, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mKey:Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v1
.end method

.method public prepareRequestNoUser(Lorg/json/JSONStringer;)V
    .locals 5
    .param p1    # Lorg/json/JSONStringer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    # getter for: Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;
    invoke-static {}, Lcom/google/android/gsf/loginservice/GLSUser;->access$000()Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->getAndroidIdHex()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v2, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->ANDROID_ID:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v2}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v2

    invoke-virtual {v2, v0}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    :cond_0
    # getter for: Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;
    invoke-static {}, Lcom/google/android/gsf/loginservice/GLSUser;->access$000()Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "device_country"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->OPERATOR_COUNTRY:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v2}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v2

    invoke-virtual {v2, v1}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    sget-object v2, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->DEVICE_COUNTRY:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v2}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v2

    invoke-virtual {v2, v1}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    sget-object v2, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->LANGUAGE:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v2}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    return-void
.end method

.method public requestJson(Ljava/lang/String;Lorg/json/JSONStringer;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Lorg/json/JSONStringer;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    new-instance v0, Lorg/apache/http/entity/StringEntity;

    invoke-virtual {p2}, Lorg/json/JSONStringer;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "UTF-8"

    invoke-direct {v0, v4, v5}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v4, 0x0

    invoke-virtual {p0, p1, v0, v4, p3}, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->httpPost(Ljava/lang/String;Lorg/apache/http/HttpEntity;Lorg/apache/http/Header;Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    return-object v3

    :catch_0
    move-exception v1

    throw v1
.end method
