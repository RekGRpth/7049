.class public Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
.super Ljava/lang/Object;
.source "GLSUser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gsf/loginservice/GLSUser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GLSSession"
.end annotation


# instance fields
.field public mAccessToken:Ljava/lang/String;

.field public mAccountAuthenticatorResponse:Landroid/accounts/AccountAuthenticatorResponse;

.field public mAccountAuthenticatorResponseCalled:Z

.field public mAccountManagerOptions:Landroid/os/Bundle;

.field public mActivities:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gsf/loginservice/BaseActivity;",
            ">;"
        }
    .end annotation
.end field

.field public mAgreedToChromeTosAndPrivacy:Z

.field public mAgreedToPlayEmail:Z

.field public mAgreedToPlayTos:Z

.field public mAllowGooglePlus:Z

.field public mCallingUID:I

.field public mCaptchaAnswer:Ljava/lang/String;

.field public mCaptchaToken:Ljava/lang/String;

.field public mCreatingAccount:Z

.field public mDetail:Ljava/lang/String;

.field public mError:Lcom/google/android/gsf/loginservice/GLSUser$Status;

.field public mHasGooglePlus:Z

.field public mIsNewAccount:Z

.field public mKey:Ljava/lang/String;

.field public mNameActivityCompleted:Z

.field public mOfferIntent:Landroid/content/Intent;

.field public mOfferMessageHtml:Ljava/lang/String;

.field public mPendingIntent:Landroid/app/PendingIntent;

.field public mPermission:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mPhotoActivityCompleted:Z

.field public mProfilePhoto:Landroid/graphics/Bitmap;

.field public mProfileResult:I

.field public mProvisionedEmail:Ljava/lang/String;

.field public mProvisionedName:Ljava/lang/String;

.field public mSecondaryEmail:Ljava/lang/String;

.field public mSecurityAnswer:Ljava/lang/String;

.field public mSecurityQuestion:Ljava/lang/String;

.field public mSetupWizard:Z

.field public mShowOffer:Z

.field public mShownName:Z

.field public mTermsOfServiceShown:Z

.field public mUrl:Ljava/lang/String;

.field public mUserData:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public mUserSelectedGooglePlus:Z

.field public mUsername:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAccountAuthenticatorResponseCalled:Z

    iput-object v2, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mCaptchaAnswer:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mCaptchaToken:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAllowGooglePlus:Z

    iput-boolean v1, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mHasGooglePlus:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mProfileResult:I

    iput-boolean v1, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUserSelectedGooglePlus:Z

    iput-boolean v1, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mTermsOfServiceShown:Z

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAccountManagerOptions:Landroid/os/Bundle;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUserData:Ljava/util/HashMap;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mActivities:Ljava/util/List;

    iput-object v2, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mProvisionedEmail:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mProvisionedName:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mKey:Ljava/lang/String;

    return-void
.end method

.method public static fromBundle(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/os/Bundle;

    new-instance v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    invoke-direct {v3, p1}, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/google/android/gsf/loginservice/GLSUser;->getGLSContext(Landroid/content/Context;)Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    move-result-object v4

    iget-object v4, v4, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mSessions:Ljava/util/LinkedHashMap;

    invoke-virtual {v4, p1, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v4, "username"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUsername:Ljava/lang/String;

    const-string v4, "callingUID"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mCallingUID:I

    const-string v4, "error"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v4, 0x0

    :goto_0
    iput-object v4, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mError:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    const-string v4, "detail"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mDetail:Ljava/lang/String;

    sget-object v4, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->URL:Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;

    invoke-virtual {v4}, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUrl:Ljava/lang/String;

    const-string v4, "permission"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mPermission:Ljava/util/ArrayList;

    const-string v4, "accountAuthenticatorResponse"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/accounts/AccountAuthenticatorResponse;

    iput-object v4, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAccountAuthenticatorResponse:Landroid/accounts/AccountAuthenticatorResponse;

    const-string v4, "accountAuthenticatorResponseCalled"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAccountAuthenticatorResponseCalled:Z

    const-string v4, "key"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mKey:Ljava/lang/String;

    const-string v4, "isNewAccount"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mIsNewAccount:Z

    const-string v4, "setupWizard"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mSetupWizard:Z

    const-string v4, "termsOfServiceShown"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mTermsOfServiceShown:Z

    const-string v4, "nameActivityCompleted"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mNameActivityCompleted:Z

    const-string v4, "photoActivityCompleted"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mPhotoActivityCompleted:Z

    const-string v4, "secondaryEmail"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mSecondaryEmail:Ljava/lang/String;

    const-string v4, "securityQuestion"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mSecurityQuestion:Ljava/lang/String;

    const-string v4, "securityAnswer"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mSecurityAnswer:Ljava/lang/String;

    const-string v4, "accessToken"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAccessToken:Ljava/lang/String;

    const-string v4, "accountManagerOptions"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAccountManagerOptions:Landroid/os/Bundle;

    const-string v4, "userSelectedGooglePlus"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUserSelectedGooglePlus:Z

    const-string v4, "creatingAccount"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mCreatingAccount:Z

    const-string v4, "allowGooglePlus"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAllowGooglePlus:Z

    const-string v4, "hasGooglePlus"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mHasGooglePlus:Z

    const-string v4, "profileResult"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mProfileResult:I

    const-string v4, "url"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUrl:Ljava/lang/String;

    const-string v4, "userData"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    const/4 v1, 0x0

    :goto_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    if-ge v1, v4, :cond_1

    iget-object v4, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUserData:Ljava/util/HashMap;

    mul-int/lit8 v5, v1, 0x2

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    mul-int/lit8 v6, v1, 0x2

    add-int/lit8 v6, v6, 0x1

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    invoke-static {v0}, Lcom/google/android/gsf/loginservice/GLSUser$Status;->fromJSON(Ljava/lang/String;)Lcom/google/android/gsf/loginservice/GLSUser$Status;

    move-result-object v4

    goto/16 :goto_0

    :cond_1
    const-string v4, "showOffer"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mShowOffer:Z

    const-string v4, "offerIntent"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/content/Intent;

    iput-object v4, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mOfferIntent:Landroid/content/Intent;

    const-string v4, "offerMessageHtml"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mOfferMessageHtml:Ljava/lang/String;

    const-string v4, "agreedToPlayTos"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAgreedToPlayTos:Z

    const-string v4, "agreedToPlayEmail"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAgreedToPlayEmail:Z

    const-string v4, "shownName"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mShownName:Z

    const-string v4, "provisionedEmail"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mProvisionedEmail:Ljava/lang/String;

    const-string v4, "provisionedName"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mProvisionedName:Ljava/lang/String;

    const-string v4, "agreedToChromeTosAndPrivacy"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAgreedToChromeTosAndPrivacy:Z

    return-object v3
.end method


# virtual methods
.method public addSession(Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Intent;

    const-string v0, "session"

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mKey:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-void
.end method

.method public toBundle(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const-string v4, "username"

    iget-object v5, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUsername:Ljava/lang/String;

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "callingUID"

    iget v5, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mCallingUID:I

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v4, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mError:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    if-eqz v4, :cond_0

    const-string v4, "error"

    iget-object v5, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mError:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    invoke-virtual {v5}, Lcom/google/android/gsf/loginservice/GLSUser$Status;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string v4, "detail"

    iget-object v5, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mDetail:Ljava/lang/String;

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v4, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->URL:Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;

    invoke-virtual {v4}, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUrl:Ljava/lang/String;

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "permission"

    iget-object v5, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mPermission:Ljava/util/ArrayList;

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v4, "accountAuthenticatorResponse"

    iget-object v5, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAccountAuthenticatorResponse:Landroid/accounts/AccountAuthenticatorResponse;

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v4, "accountAuthenticatorResponseCalled"

    iget-boolean v5, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAccountAuthenticatorResponseCalled:Z

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v4, "key"

    iget-object v5, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mKey:Ljava/lang/String;

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "isNewAccount"

    iget-boolean v5, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mIsNewAccount:Z

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v4, "setupWizard"

    iget-boolean v5, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mSetupWizard:Z

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v4, "termsOfServiceShown"

    iget-boolean v5, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mTermsOfServiceShown:Z

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v4, "nameActivityCompleted"

    iget-boolean v5, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mNameActivityCompleted:Z

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v4, "photoActivityCompleted"

    iget-boolean v5, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mPhotoActivityCompleted:Z

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v4, "secondaryEmail"

    iget-object v5, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mSecondaryEmail:Ljava/lang/String;

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "securityQuestion"

    iget-object v5, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mSecurityQuestion:Ljava/lang/String;

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "securityAnwser"

    iget-object v5, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mSecurityAnswer:Ljava/lang/String;

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "accessToken"

    iget-object v5, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAccessToken:Ljava/lang/String;

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "accountManagerOptions"

    iget-object v5, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAccountManagerOptions:Landroid/os/Bundle;

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    const-string v4, "userSelectedGooglePlus"

    iget-boolean v5, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUserSelectedGooglePlus:Z

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v4, "creatingAccount"

    iget-boolean v5, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mCreatingAccount:Z

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v4, "allowGooglePlus"

    iget-boolean v5, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAllowGooglePlus:Z

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v4, "hasGooglePlus"

    iget-boolean v5, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mHasGooglePlus:Z

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v4, "profileResult"

    iget v5, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mProfileResult:I

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v4, "url"

    iget-object v5, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUrl:Ljava/lang/String;

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v4, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUserData:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUserData:Ljava/util/HashMap;

    invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    instance-of v4, v3, Ljava/lang/String;

    if-eqz v4, :cond_1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    const-string v4, "userData"

    invoke-virtual {p1, v4, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v4, "showOffer"

    iget-boolean v5, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mShowOffer:Z

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v4, "offerIntent"

    iget-object v5, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mOfferIntent:Landroid/content/Intent;

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v4, "offerMessageHtml"

    iget-object v5, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mOfferMessageHtml:Ljava/lang/String;

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "agreedToPlayTos"

    iget-boolean v5, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAgreedToPlayTos:Z

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v4, "agreedToPlayEmail"

    iget-boolean v5, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAgreedToPlayEmail:Z

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v4, "shownName"

    iget-boolean v5, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mShownName:Z

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v4, "provisionedEmail"

    iget-object v5, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mProvisionedEmail:Ljava/lang/String;

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "provisionedName"

    iget-object v5, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mProvisionedName:Ljava/lang/String;

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "agreedToChromeTosAndPrivacy"

    iget-boolean v5, p0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAgreedToChromeTosAndPrivacy:Z

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method
