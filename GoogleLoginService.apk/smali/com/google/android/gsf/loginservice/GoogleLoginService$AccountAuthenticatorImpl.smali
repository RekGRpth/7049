.class Lcom/google/android/gsf/loginservice/GoogleLoginService$AccountAuthenticatorImpl;
.super Landroid/accounts/AbstractAccountAuthenticator;
.source "GoogleLoginService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gsf/loginservice/GoogleLoginService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AccountAuthenticatorImpl"
.end annotation


# instance fields
.field private final mAccountManager:Landroid/accounts/AccountManager;

.field private final mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/google/android/gsf/loginservice/GoogleLoginService;


# direct methods
.method public constructor <init>(Lcom/google/android/gsf/loginservice/GoogleLoginService;Landroid/content/Context;)V
    .locals 1
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/google/android/gsf/loginservice/GoogleLoginService$AccountAuthenticatorImpl;->this$0:Lcom/google/android/gsf/loginservice/GoogleLoginService;

    invoke-direct {p0, p2}, Landroid/accounts/AbstractAccountAuthenticator;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/google/android/gsf/loginservice/GoogleLoginService$AccountAuthenticatorImpl;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/gsf/loginservice/GoogleLoginService$AccountAuthenticatorImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gsf/loginservice/GoogleLoginService$AccountAuthenticatorImpl;->mAccountManager:Landroid/accounts/AccountManager;

    return-void
.end method


# virtual methods
.method public addAccount(Landroid/accounts/AccountAuthenticatorResponse;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v4, 0x0

    if-nez p5, :cond_0

    new-instance p5, Landroid/os/Bundle;

    invoke-direct {p5}, Landroid/os/Bundle;-><init>()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gsf/loginservice/GoogleLoginService$AccountAuthenticatorImpl;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gsf/loginservice/GLSUser;->getGLSContext(Landroid/content/Context;)Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->newSession()Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    move-result-object v1

    const-string v0, "callerUid"

    invoke-virtual {p5, v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mCallingUID:I

    iput-object p1, v1, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAccountAuthenticatorResponse:Landroid/accounts/AccountAuthenticatorResponse;

    const-string v0, "pendingIntent"

    invoke-virtual {p5, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    iput-object v0, v1, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mPendingIntent:Landroid/app/PendingIntent;

    iput-object p5, v1, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAccountManagerOptions:Landroid/os/Bundle;

    const-string v0, "password"

    invoke-virtual {p5, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "code:"

    invoke-virtual {v2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x5

    invoke-virtual {v2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/gsf/loginservice/GoogleLoginService$AccountAuthenticatorImpl;->mContext:Landroid/content/Context;

    invoke-static {v0, v6}, Lcom/google/android/gsf/loginservice/GLSUser;->get(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gsf/loginservice/GLSUser;

    move-result-object v0

    const-string v2, "useBrowser"

    invoke-virtual {p5, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    const-string v3, "created"

    invoke-virtual {p5, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    const-string v6, "setupWizard"

    invoke-virtual {p5, v6, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gsf/loginservice/GLSUser;->addAccount(Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;ZZZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    :goto_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "authtoken"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    const-string v2, "authAccount"

    const-string v3, "authAccount"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "accountType"

    const-string v2, "com.google"

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    move-object v0, v1

    :goto_2
    return-object v0

    :cond_1
    const-string v0, "username"

    invoke-virtual {p5, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gsf/loginservice/GoogleLoginService$AccountAuthenticatorImpl;->mContext:Landroid/content/Context;

    invoke-static {v3, v0}, Lcom/google/android/gsf/loginservice/GLSUser;->get(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gsf/loginservice/GLSUser;

    move-result-object v0

    iget-boolean v3, v0, Lcom/google/android/gsf/loginservice/GLSUser;->existing:Z

    if-eqz v3, :cond_2

    move-object v0, v6

    goto :goto_2

    :cond_2
    const-string v3, "oauth1:"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x7

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gsf/loginservice/GLSUser;->setToken(Ljava/lang/String;)V

    :goto_3
    const-string v2, "useBrowser"

    invoke-virtual {p5, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    const-string v3, "created"

    invoke-virtual {p5, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    const-string v5, "setupWizard"

    invoke-virtual {p5, v5, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    move-object v5, v6

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gsf/loginservice/GLSUser;->addAccount(Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;ZZZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    :cond_3
    invoke-virtual {v0, v2}, Lcom/google/android/gsf/loginservice/GLSUser;->setPassword(Ljava/lang/String;)V

    goto :goto_3

    :cond_4
    const-string v0, "errorCode"

    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_1

    :cond_5
    if-eqz p4, :cond_a

    array-length v3, p4

    move v2, v4

    move v0, v4

    :goto_4
    if-ge v2, v3, :cond_a

    aget-object v5, p4, v2

    const-string v6, "hosted_or_google"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    or-int/lit8 v0, v0, 0x1

    or-int/lit8 v0, v0, 0x2

    :cond_6
    :goto_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_7
    const-string v6, "google"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    or-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_8
    const-string v6, "youtubelinked"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    or-int/lit8 v0, v0, 0x4

    goto :goto_5

    :cond_9
    const-string v6, "saml"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    or-int/lit8 v0, v0, 0x8

    goto :goto_5

    :cond_a
    new-instance v2, Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/gsf/loginservice/GoogleLoginService$AccountAuthenticatorImpl;->mContext:Landroid/content/Context;

    const-class v3, Lcom/google/android/gsf/login/AccountIntroActivity;

    invoke-direct {v2, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "isTop"

    invoke-virtual {v2, v0, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "addAccount"

    invoke-virtual {v2, v0, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {v1, v2}, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->addSession(Landroid/content/Intent;)V

    const/high16 v0, 0x80000

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v0, "firstRun"

    const-string v1, "firstRun"

    invoke-virtual {p5, v1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "intent"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto/16 :goto_2
.end method

.method public confirmCredentials(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 12
    .param p1    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2    # Landroid/accounts/Account;
    .param p3    # Landroid/os/Bundle;

    const/4 v11, 0x1

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/GoogleLoginService$AccountAuthenticatorImpl;->this$0:Lcom/google/android/gsf/loginservice/GoogleLoginService;

    iget-object v2, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gsf/loginservice/GLSUser;->get(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gsf/loginservice/GLSUser;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/GoogleLoginService$AccountAuthenticatorImpl;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gsf/loginservice/GLSUser;->getGLSContext(Landroid/content/Context;)Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->newSession()Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    move-result-object v5

    iput-object p1, v5, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAccountAuthenticatorResponse:Landroid/accounts/AccountAuthenticatorResponse;

    if-eqz p3, :cond_0

    iput-object p3, v5, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAccountManagerOptions:Landroid/os/Bundle;

    const-string v1, "pendingIntent"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/app/PendingIntent;

    iput-object v1, v5, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mPendingIntent:Landroid/app/PendingIntent;

    :cond_0
    if-eqz p3, :cond_2

    const-string v1, "password"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "password"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser$Status;->SUCCESS:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    iget-object v2, p0, Lcom/google/android/gsf/loginservice/GoogleLoginService$AccountAuthenticatorImpl;->mContext:Landroid/content/Context;

    iget-object v4, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v2, v4, v9, v5}, Lcom/google/android/gsf/loginservice/GoogleLoginService;->onlineConfirmPassword(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;)Lcom/google/android/gsf/loginservice/GLSUser$Status;

    move-result-object v2

    if-ne v1, v2, :cond_1

    :goto_0
    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    const-string v1, "booleanResult"

    invoke-virtual {v10, v1, v11}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :goto_1
    return-object v10

    :cond_1
    move v11, v3

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/gsf/loginservice/GLSUser$Status;->BAD_AUTHENTICATION:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    const-string v4, "SID"

    move-object v6, p3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gsf/loginservice/GLSUser;->errorIntent(Ljava/util/Map;Lcom/google/android/gsf/loginservice/GLSUser$Status;ILjava/lang/String;Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v8

    const-string v1, "confirmCredentials"

    invoke-virtual {v8, v1, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    const-string v1, "intent"

    invoke-virtual {v7, v1, v8}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    move-object v10, v7

    goto :goto_1
.end method

.method public editProperties(Landroid/accounts/AccountAuthenticatorResponse;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 1
    .param p1    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2    # Ljava/lang/String;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getAuthToken(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 11
    .param p1    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2    # Landroid/accounts/Account;
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/NetworkErrorException;
        }
    .end annotation

    if-nez p4, :cond_0

    new-instance p4, Landroid/os/Bundle;

    invoke-direct {p4}, Landroid/os/Bundle;-><init>()V

    :cond_0
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1

    new-instance v8, Ljava/lang/IllegalArgumentException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "the mService is empty: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    :cond_1
    const-string v8, "callerUid"

    invoke-virtual {p4, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iget-object v8, p0, Lcom/google/android/gsf/loginservice/GoogleLoginService$AccountAuthenticatorImpl;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/google/android/gsf/loginservice/GLSUser;->getGLSContext(Landroid/content/Context;)Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->newSession()Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    move-result-object v5

    iput-object p1, v5, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAccountAuthenticatorResponse:Landroid/accounts/AccountAuthenticatorResponse;

    iput v1, v5, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mCallingUID:I

    const-string v8, "pendingIntent"

    invoke-virtual {p4, v8}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v8

    check-cast v8, Landroid/app/PendingIntent;

    iput-object v8, v5, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mPendingIntent:Landroid/app/PendingIntent;

    iput-object p4, v5, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAccountManagerOptions:Landroid/os/Bundle;

    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v9, 0xa

    if-le v8, v9, :cond_2

    iput v1, v5, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mCallingUID:I

    :cond_2
    iget-object v8, p0, Lcom/google/android/gsf/loginservice/GoogleLoginService$AccountAuthenticatorImpl;->this$0:Lcom/google/android/gsf/loginservice/GoogleLoginService;

    iget-object v9, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v8, v9}, Lcom/google/android/gsf/loginservice/GLSUser;->get(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gsf/loginservice/GLSUser;

    move-result-object v2

    invoke-virtual {v2, p3, v1, v5}, Lcom/google/android/gsf/loginservice/GLSUser;->getAuthtoken(Ljava/lang/String;ILcom/google/android/gsf/loginservice/GLSUser$GLSSession;)Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/gsf/loginservice/GLSUser;->getToken(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_3

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_3
    iget-object v8, p0, Lcom/google/android/gsf/loginservice/GoogleLoginService$AccountAuthenticatorImpl;->mContext:Landroid/content/Context;

    invoke-static {v8, v4}, Lcom/google/android/gsf/loginservice/GLSUser$Status;->fromIntent(Landroid/content/Context;Landroid/content/Intent;)Lcom/google/android/gsf/loginservice/GLSUser$Status;

    move-result-object v6

    sget-object v8, Lcom/google/android/gsf/loginservice/GLSUser$Status;->NETWORK_ERROR:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    if-eq v8, v6, :cond_4

    sget-object v8, Lcom/google/android/gsf/loginservice/GLSUser$Status;->SERVICE_UNAVAILABLE:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    if-ne v8, v6, :cond_5

    :cond_4
    new-instance v8, Landroid/accounts/NetworkErrorException;

    invoke-direct {v8}, Landroid/accounts/NetworkErrorException;-><init>()V

    throw v8

    :cond_5
    const-string v8, "GoogleLoginService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Returning error intent with: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v4}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v8, "notifyOnAuthFailure"

    const/4 v9, 0x0

    invoke-virtual {p4, v8, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v8, p0, Lcom/google/android/gsf/loginservice/GoogleLoginService$AccountAuthenticatorImpl;->mContext:Landroid/content/Context;

    invoke-static {v8, p2, v1, v4, v6}, Lcom/google/android/gsf/loginservice/GoogleLoginService;->setNotification(Landroid/content/Context;Landroid/accounts/Account;ILandroid/content/Intent;Lcom/google/android/gsf/loginservice/GLSUser$Status;)V

    :cond_6
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v8, "isTop"

    const/4 v9, 0x1

    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/high16 v8, 0x10000000

    invoke-virtual {v4, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v8, "intent"

    invoke-virtual {v0, v8, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method public getAuthTokenLabel(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/GoogleLoginService$AccountAuthenticatorImpl;->mContext:Landroid/content/Context;

    invoke-static {v1, p1}, Lcom/google/android/gsf/loginservice/GoogleLoginService;->getAuthTokenLabel(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-object p1

    :cond_0
    move-object p1, v0

    goto :goto_0
.end method

.method public hasFeatures(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;[Ljava/lang/String;)Landroid/os/Bundle;
    .locals 4
    .param p1    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2    # Landroid/accounts/Account;
    .param p3    # [Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gsf/loginservice/GoogleLoginService$AccountAuthenticatorImpl;->this$0:Lcom/google/android/gsf/loginservice/GoogleLoginService;

    iget-object v3, p0, Lcom/google/android/gsf/loginservice/GoogleLoginService$AccountAuthenticatorImpl;->mAccountManager:Landroid/accounts/AccountManager;

    # invokes: Lcom/google/android/gsf/loginservice/GoogleLoginService;->accountHasFeatures(Landroid/accounts/AccountManager;Landroid/accounts/Account;[Ljava/lang/String;)Z
    invoke-static {v2, v3, p2, p3}, Lcom/google/android/gsf/loginservice/GoogleLoginService;->access$000(Lcom/google/android/gsf/loginservice/GoogleLoginService;Landroid/accounts/AccountManager;Landroid/accounts/Account;[Ljava/lang/String;)Z

    move-result v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "booleanResult"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-object v1
.end method

.method public updateCredentials(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 10
    .param p1    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2    # Landroid/accounts/Account;
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/os/Bundle;

    const/4 v9, 0x1

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/GoogleLoginService$AccountAuthenticatorImpl;->this$0:Lcom/google/android/gsf/loginservice/GoogleLoginService;

    iget-object v2, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gsf/loginservice/GLSUser;->get(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gsf/loginservice/GLSUser;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/GoogleLoginService$AccountAuthenticatorImpl;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gsf/loginservice/GLSUser;->getGLSContext(Landroid/content/Context;)Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->newSession()Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    move-result-object v5

    iput-object p1, v5, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAccountAuthenticatorResponse:Landroid/accounts/AccountAuthenticatorResponse;

    if-eqz p4, :cond_0

    iput-object p4, v5, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAccountManagerOptions:Landroid/os/Bundle;

    const-string v1, "pendingIntent"

    invoke-virtual {p4, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/app/PendingIntent;

    iput-object v1, v5, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mPendingIntent:Landroid/app/PendingIntent;

    :cond_0
    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/gsf/loginservice/GLSUser$Status;->BAD_AUTHENTICATION:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    const/4 v3, 0x0

    const-string v4, "SID"

    move-object v6, p4

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gsf/loginservice/GLSUser;->errorIntent(Ljava/util/Map;Lcom/google/android/gsf/loginservice/GLSUser$Status;ILjava/lang/String;Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v8

    const-string v1, "updateCredentials"

    invoke-virtual {v8, v1, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    const-string v1, "intent"

    invoke-virtual {v7, v1, v8}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v1, "isTop"

    invoke-virtual {v8, v1, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    return-object v7
.end method
