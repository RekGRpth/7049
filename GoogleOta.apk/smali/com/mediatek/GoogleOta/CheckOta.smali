.class Lcom/mediatek/GoogleOta/CheckOta;
.super Ljava/lang/Object;
.source "CheckOta.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/GoogleOta/CheckOta$1;,
        Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;,
        Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;
    }
.end annotation


# static fields
.field private static final BUFF_SIZE:I = 0x400

.field private static final DELTA_PKG:I = 0x0

.field private static final ENTRY_BUILD_INFO:Ljava/lang/String; = "META-INF/com/google/android/updater-script"

.field private static final ENTRY_SCATTER:Ljava/lang/String; = "scatter.txt"

.field private static final ENTRY_TYPE:Ljava/lang/String; = "type.txt"

.field private static final LAST_PARTITION_NAME:Ljava/lang/String; = "bmtpool"

.field private static final NAME_DEV_NUM:Ljava/lang/String; = "getprop(\"ro.product.device\") == \""

.field private static final NAME_FINGERPRINT:Ljava/lang/String; = "file_getprop(\"/system/build.prop\", \"ro.build.fingerprint\") == \""

.field private static final NAME_PRO_NUM:Ljava/lang/String; = "getprop(\"ro.build.product\") == \""

.field private static final OFFSET_RADIX:I = 0x10

.field private static final PARTITION_FILE_PATH:Ljava/lang/String; = "/proc/dumchar_info"

.field private static final PARTITION_NAME:[[Ljava/lang/String;

.field private static final PARTITION_NUM_MAX:I = 0x21

.field private static final PARTITION_SEC_NUM_DEV:I = 0x5

.field private static final PARTITION_SEC_NUM_PKG:I = 0x2

.field private static final PROPERTY_DEV_NUM:Ljava/lang/String; = "ro.product.device"

.field private static final PROPERTY_FINGERPRINT:Ljava/lang/String; = "ro.build.fingerprint"

.field private static final PROPERTY_PRO_NUM:Ljava/lang/String; = "ro.build.product"

.field private static final TAG:Ljava/lang/String; = "GoogleOta/CheckOta"


# instance fields
.field private mBuildInfoFilePath:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mIsEmmcPhone:Z

.field private mOtaFilePath:Ljava/lang/String;

.field private mOtaResult:I

.field private mOtaTypeFilePath:Ljava/lang/String;

.field private mPartitionChange:Z

.field private mScatterFilePath:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-array v0, v7, [[Ljava/lang/String;

    new-array v1, v7, [Ljava/lang/String;

    const-string v2, ""

    aput-object v2, v1, v3

    const-string v2, "USRDATA"

    aput-object v2, v1, v4

    const-string v2, "__NODL_CACHE"

    aput-object v2, v1, v5

    const-string v2, "ANDROID"

    aput-object v2, v1, v6

    aput-object v1, v0, v3

    new-array v1, v7, [Ljava/lang/String;

    const-string v2, ""

    aput-object v2, v1, v3

    const-string v2, "USRDATA"

    aput-object v2, v1, v4

    const-string v2, "CACHE"

    aput-object v2, v1, v5

    const-string v2, "ANDROID"

    aput-object v2, v1, v6

    aput-object v1, v0, v4

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, ""

    aput-object v2, v1, v3

    const-string v2, "__NODL_FAT"

    aput-object v2, v1, v4

    const-string v2, "USRDATA"

    aput-object v2, v1, v5

    const-string v2, "CACHE"

    aput-object v2, v1, v6

    const-string v2, "ANDROID"

    aput-object v2, v1, v7

    aput-object v1, v0, v5

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, ""

    aput-object v2, v1, v3

    const-string v2, "FAT"

    aput-object v2, v1, v4

    const-string v2, "USRDATA"

    aput-object v2, v1, v5

    const-string v2, "CACHE"

    aput-object v2, v1, v6

    const-string v2, "ANDROID"

    aput-object v2, v1, v7

    aput-object v1, v0, v6

    sput-object v0, Lcom/mediatek/GoogleOta/CheckOta;->PARTITION_NAME:[[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/mediatek/GoogleOta/CheckOta;->mOtaResult:I

    iput-boolean v0, p0, Lcom/mediatek/GoogleOta/CheckOta;->mIsEmmcPhone:Z

    iput-boolean v0, p0, Lcom/mediatek/GoogleOta/CheckOta;->mPartitionChange:Z

    iput-object p2, p0, Lcom/mediatek/GoogleOta/CheckOta;->mOtaFilePath:Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/GoogleOta/CheckOta;->mContext:Landroid/content/Context;

    const-string v0, "GoogleOta/CheckOta"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mOtaFilePath = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/GoogleOta/CheckOta;->mOtaFilePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/mediatek/GoogleOta/CheckOta;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/mediatek/GoogleOta/Util;->getPackagePathName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "type.txt"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/GoogleOta/CheckOta;->mOtaTypeFilePath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/mediatek/GoogleOta/CheckOta;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/mediatek/GoogleOta/Util;->getPackagePathName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "scatter.txt"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/GoogleOta/CheckOta;->mScatterFilePath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/mediatek/GoogleOta/CheckOta;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/mediatek/GoogleOta/Util;->getPackagePathName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "script"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/GoogleOta/CheckOta;->mBuildInfoFilePath:Ljava/lang/String;

    return-void
.end method

.method private checkPartitionLayout(Ljava/util/List;)Z
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;",
            ">;)Z"
        }
    .end annotation

    const/4 v7, 0x0

    const-string v6, "GoogleOta/CheckOta"

    const-string v8, "into: checkPartitionLayout"

    invoke-static {v6, v8}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    const/4 v1, 0x0

    :goto_0
    sget-object v6, Lcom/mediatek/GoogleOta/CheckOta;->PARTITION_NAME:[[Ljava/lang/String;

    array-length v6, v6

    if-ge v1, v6, :cond_3

    const/4 v0, 0x1

    sget-object v6, Lcom/mediatek/GoogleOta/CheckOta;->PARTITION_NAME:[[Ljava/lang/String;

    aget-object v6, v6, v1

    array-length v3, v6

    :goto_1
    if-ge v0, v3, :cond_0

    sub-int v6, v4, v0

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;

    iget-object v5, v6, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;->mName:Ljava/lang/String;

    sget-object v6, Lcom/mediatek/GoogleOta/CheckOta;->PARTITION_NAME:[[Ljava/lang/String;

    aget-object v6, v6, v1

    aget-object v6, v6, v0

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v5, :cond_0

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    if-lt v6, v2, :cond_0

    invoke-virtual {v5, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    const-string v6, "GoogleOta/CheckOta"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "PARTITION_NAME[k][index] = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/mediatek/GoogleOta/CheckOta;->PARTITION_NAME:[[Ljava/lang/String;

    aget-object v9, v9, v1

    aget-object v9, v9, v0

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " strName = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v6, Lcom/mediatek/GoogleOta/CheckOta;->PARTITION_NAME:[[Ljava/lang/String;

    aget-object v6, v6, v1

    aget-object v6, v6, v0

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    :cond_0
    if-ne v0, v3, :cond_2

    const/4 v6, 0x1

    :goto_2
    return v6

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    move v6, v7

    goto :goto_2
.end method

.method private checkPartiton(Ljava/util/zip/ZipFile;I)Z
    .locals 7
    .param p1    # Ljava/util/zip/ZipFile;
    .param p2    # I

    const/4 v6, 0x6

    const/4 v2, 0x0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    :cond_0
    const/4 v3, 0x5

    iput v3, p0, Lcom/mediatek/GoogleOta/CheckOta;->mOtaResult:I

    :goto_0
    return v2

    :cond_1
    invoke-direct {p0, v0}, Lcom/mediatek/GoogleOta/CheckOta;->getPartitionInfoDev(Ljava/util/List;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "GoogleOta/CheckOta"

    const-string v4, "getPartitionInfoDev false"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const-string v3, "GoogleOta/CheckOta"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mIsEmmcPhone = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/mediatek/GoogleOta/CheckOta;->mIsEmmcPhone:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1, v1}, Lcom/mediatek/GoogleOta/CheckOta;->getPartitionInfoPkg(Ljava/util/zip/ZipFile;Ljava/util/List;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "GoogleOta/CheckOta"

    const-string v4, "getPartitionInfoPkg false"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-eq v3, v4, :cond_4

    const-string v3, "GoogleOta/CheckOta"

    const-string v4, "partitionDev.size() != partitionPkg.size()"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput v6, p0, Lcom/mediatek/GoogleOta/CheckOta;->mOtaResult:I

    goto :goto_0

    :cond_4
    invoke-direct {p0, v1}, Lcom/mediatek/GoogleOta/CheckOta;->checkPartitionLayout(Ljava/util/List;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string v3, "GoogleOta/CheckOta"

    const-string v4, "checkPartitionLayout(partitionPkg) false"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput v6, p0, Lcom/mediatek/GoogleOta/CheckOta;->mOtaResult:I

    goto :goto_0

    :cond_5
    invoke-direct {p0, v0, v1, p2}, Lcom/mediatek/GoogleOta/CheckOta;->comparePartition(Ljava/util/List;Ljava/util/List;I)Z

    move-result v3

    if-nez v3, :cond_6

    const-string v3, "GoogleOta/CheckOta"

    const-string v4, "comparePartition false"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_6
    if-nez p2, :cond_7

    iget-boolean v3, p0, Lcom/mediatek/GoogleOta/CheckOta;->mPartitionChange:Z

    if-eqz v3, :cond_7

    const/4 v3, 0x7

    iput v3, p0, Lcom/mediatek/GoogleOta/CheckOta;->mOtaResult:I

    goto :goto_0

    :cond_7
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private checkVersion(Ljava/util/zip/ZipFile;I)Z
    .locals 6
    .param p1    # Ljava/util/zip/ZipFile;
    .param p2    # I

    const/4 v3, 0x0

    const/4 v2, 0x0

    new-instance v0, Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;

    invoke-direct {v0, p0, v3}, Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;-><init>(Lcom/mediatek/GoogleOta/CheckOta;Lcom/mediatek/GoogleOta/CheckOta$1;)V

    new-instance v1, Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;

    invoke-direct {v1, p0, v3}, Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;-><init>(Lcom/mediatek/GoogleOta/CheckOta;Lcom/mediatek/GoogleOta/CheckOta$1;)V

    invoke-direct {p0, p1, v1, p2}, Lcom/mediatek/GoogleOta/CheckOta;->getVersionInfoPkg(Ljava/util/zip/ZipFile;Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;I)Z

    move-result v3

    if-nez v3, :cond_0

    :goto_0
    return v2

    :cond_0
    const-string v3, "GoogleOta/CheckOta"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "infoPkg: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;->mDevNum:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;->mProNum:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;->mFingerPrint1:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;->mFingerPrint2:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v0, p2}, Lcom/mediatek/GoogleOta/CheckOta;->getVersionInfoDev(Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;I)V

    const-string v3, "GoogleOta/CheckOta"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "infoDev: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;->mDevNum:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;->mProNum:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;->mFingerPrint1:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;->mFingerPrint2:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v0, v1}, Lcom/mediatek/GoogleOta/CheckOta;->compDevAndProInfo(Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;)Z

    move-result v3

    if-nez v3, :cond_1

    const/16 v3, 0xd

    iput v3, p0, Lcom/mediatek/GoogleOta/CheckOta;->mOtaResult:I

    goto/16 :goto_0

    :cond_1
    if-nez p2, :cond_2

    invoke-direct {p0, v0, v1}, Lcom/mediatek/GoogleOta/CheckOta;->compFingerPrint(Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;)Z

    move-result v3

    if-nez v3, :cond_2

    const/16 v3, 0xe

    iput v3, p0, Lcom/mediatek/GoogleOta/CheckOta;->mOtaResult:I

    goto/16 :goto_0

    :cond_2
    const/4 v2, 0x1

    goto/16 :goto_0
.end method

.method private compDevAndProInfo(Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;)Z
    .locals 3
    .param p1    # Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;
    .param p2    # Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;

    const/4 v0, 0x1

    iget-object v1, p1, Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;->mDevNum:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p1, Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;->mDevNum:Ljava/lang/String;

    iget-object v2, p2, Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;->mDevNum:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p1, Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;->mProNum:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p1, Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;->mProNum:Ljava/lang/String;

    iget-object v2, p2, Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;->mProNum:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private compFingerPrint(Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;)Z
    .locals 2
    .param p1    # Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;
    .param p2    # Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;

    iget-object v0, p1, Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;->mFingerPrint1:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;->mFingerPrint1:Ljava/lang/String;

    iget-object v1, p2, Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;->mFingerPrint1:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;->mFingerPrint1:Ljava/lang/String;

    iget-object v1, p2, Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;->mFingerPrint2:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private comparePartition(Ljava/util/List;Ljava/util/List;I)Z
    .locals 11
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;",
            ">;I)Z"
        }
    .end annotation

    const/4 v10, 0x6

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    iget-boolean v3, p0, Lcom/mediatek/GoogleOta/CheckOta;->mIsEmmcPhone:Z

    if-nez v3, :cond_4

    const/4 v0, 0x0

    :goto_0
    add-int/lit8 v3, v2, -0x2

    if-ge v0, v3, :cond_1

    const-string v6, "GoogleOta/CheckOta"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "i = "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, " partitionDev.get(i).mOffset = "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;

    iget-wide v8, v3, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;->mOffset:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, " partitionPkg.get(i).mOffset = "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;

    iget-wide v8, v3, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;->mOffset:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v6, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;

    iget-wide v6, v3, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;->mOffset:J

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;

    iget-wide v8, v3, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;->mOffset:J

    cmp-long v3, v6, v8

    if-eqz v3, :cond_0

    iput v10, p0, Lcom/mediatek/GoogleOta/CheckOta;->mOtaResult:I

    move v3, v4

    :goto_1
    return v3

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    if-nez p3, :cond_2

    add-int/lit8 v1, v2, -0x2

    :goto_2
    if-ge v1, v2, :cond_2

    const-string v4, "GoogleOta/CheckOta"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "k = "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " partitionDev.get(k).mOffset = "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;

    iget-wide v7, v3, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;->mOffset:J

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " partitionPkg.get(k).mOffset = "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;

    iget-wide v7, v3, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;->mOffset:J

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;

    iget-wide v6, v3, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;->mOffset:J

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;

    iget-wide v3, v3, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;->mOffset:J

    cmp-long v3, v6, v3

    if-eqz v3, :cond_3

    iput-boolean v5, p0, Lcom/mediatek/GoogleOta/CheckOta;->mPartitionChange:Z

    :cond_2
    :goto_3
    move v3, v5

    goto :goto_1

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    :goto_4
    add-int/lit8 v3, v2, -0x3

    if-ge v0, v3, :cond_6

    const-string v6, "GoogleOta/CheckOta"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "i = "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, " partitionDev.get(i).mOffset = "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;

    iget-wide v8, v3, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;->mOffset:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, " partitionPkg.get(i).mOffset = "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;

    iget-wide v8, v3, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;->mOffset:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v6, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;

    iget-wide v6, v3, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;->mOffset:J

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;

    iget-wide v8, v3, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;->mOffset:J

    cmp-long v3, v6, v8

    if-eqz v3, :cond_5

    iput v10, p0, Lcom/mediatek/GoogleOta/CheckOta;->mOtaResult:I

    move v3, v4

    goto/16 :goto_1

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    const-string v6, "GoogleOta/CheckOta"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "partitionDev.get(length-1).mOffset = "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    add-int/lit8 v3, v2, -0x1

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;

    iget-wide v8, v3, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;->mOffset:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, " partitionPkg.get(length - 1).mOffset = "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    add-int/lit8 v3, v2, -0x1

    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;

    iget-wide v8, v3, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;->mOffset:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v6, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v3, v2, -0x1

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;

    iget-wide v6, v3, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;->mOffset:J

    add-int/lit8 v3, v2, -0x1

    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;

    iget-wide v8, v3, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;->mOffset:J

    cmp-long v3, v6, v8

    if-eqz v3, :cond_7

    iput v10, p0, Lcom/mediatek/GoogleOta/CheckOta;->mOtaResult:I

    move v3, v4

    goto/16 :goto_1

    :cond_7
    if-nez p3, :cond_2

    add-int/lit8 v1, v2, -0x3

    :goto_5
    add-int/lit8 v3, v2, -0x1

    if-ge v1, v3, :cond_2

    const-string v4, "GoogleOta/CheckOta"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "k = "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " partitionDev.get(k).mOffset = "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;

    iget-wide v7, v3, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;->mOffset:J

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " partitionPkg.get(k).mOffset = "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;

    iget-wide v7, v3, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;->mOffset:J

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;

    iget-wide v6, v3, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;->mOffset:J

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;

    iget-wide v3, v3, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;->mOffset:J

    cmp-long v3, v6, v3

    if-eqz v3, :cond_8

    iput-boolean v5, p0, Lcom/mediatek/GoogleOta/CheckOta;->mPartitionChange:Z

    goto/16 :goto_3

    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_5
.end method

.method private extractVerInfo(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v3, -0x1

    invoke-virtual {p1, p2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-eq v1, v3, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, v1

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    const/16 v2, 0x22

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-eq v0, v3, :cond_0

    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private getOtaType(Ljava/util/zip/ZipFile;)I
    .locals 8
    .param p1    # Ljava/util/zip/ZipFile;

    const/4 v4, -0x1

    const-string v5, "GoogleOta/CheckOta"

    const-string v6, "into:getOtaType"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "type.txt"

    iget-object v6, p0, Lcom/mediatek/GoogleOta/CheckOta;->mOtaTypeFilePath:Ljava/lang/String;

    invoke-direct {p0, p1, v5, v6}, Lcom/mediatek/GoogleOta/CheckOta;->unzipFileElement(Ljava/util/zip/ZipFile;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "GoogleOta/CheckOta"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "unzip fail from type.txt to "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/mediatek/GoogleOta/CheckOta;->mOtaTypeFilePath:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v4

    :cond_0
    :try_start_0
    new-instance v3, Ljava/io/FileReader;

    iget-object v5, p0, Lcom/mediatek/GoogleOta/CheckOta;->mOtaTypeFilePath:Ljava/lang/String;

    invoke-direct {v3, v5}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/io/BufferedReader;

    invoke-direct {v0, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_1
    const/4 v5, 0x3

    iput v5, p0, Lcom/mediatek/GoogleOta/CheckOta;->mOtaResult:I

    goto :goto_0
.end method

.method private getPartitionInfoDev(Ljava/util/List;)Z
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;",
            ">;)Z"
        }
    .end annotation

    const-string v9, "GoogleOta/CheckOta"

    const-string v10, "into: getPartitionInfoDev"

    invoke-static {v9, v10}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    new-instance v7, Ljava/io/FileReader;

    const-string v9, "/proc/dumchar_info"

    invoke-direct {v7, v9}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/io/BufferedReader;

    invoke-direct {v0, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    const/4 v9, 0x1

    iput v9, p0, Lcom/mediatek/GoogleOta/CheckOta;->mOtaResult:I

    const/4 v9, 0x0

    :goto_0
    return v9

    :cond_0
    const-wide/16 v4, 0x0

    :cond_1
    :goto_1
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    const-string v9, "GoogleOta/CheckOta"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "line = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v9, "\\s+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    array-length v9, v6

    const/4 v10, 0x5

    if-ne v9, v10, :cond_1

    const-string v9, "bmtpool"

    const/4 v10, 0x0

    aget-object v10, v6, v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    :cond_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v9

    const/16 v10, 0x21

    if-le v9, v10, :cond_5

    const/4 v9, 0x6

    iput v9, p0, Lcom/mediatek/GoogleOta/CheckOta;->mOtaResult:I

    const/4 v9, 0x0

    goto :goto_0

    :cond_3
    new-instance v2, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;

    const/4 v9, 0x0

    aget-object v9, v6, v9

    invoke-direct {v2, p0, v9, v4, v5}, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;-><init>(Lcom/mediatek/GoogleOta/CheckOta;Ljava/lang/String;J)V

    const-string v9, "GoogleOta/CheckOta"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "name = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const/4 v11, 0x0

    aget-object v11, v6, v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " offset is "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v9, 0x3

    aget-object v9, v6, v9

    if-eqz v9, :cond_4

    const/4 v9, 0x3

    aget-object v9, v6, v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    const/4 v10, 0x2

    if-ne v9, v10, :cond_4

    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/mediatek/GoogleOta/CheckOta;->mIsEmmcPhone:Z

    :cond_4
    const/4 v9, 0x1

    aget-object v9, v6, v9

    if-eqz v9, :cond_1

    const/4 v9, 0x1

    aget-object v9, v6, v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    const/4 v10, 0x3

    if-lt v9, v10, :cond_1

    const/4 v9, 0x1

    aget-object v9, v6, v9

    const/4 v10, 0x2

    invoke-virtual {v9, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    const/16 v9, 0x10

    invoke-static {v8, v9}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v9

    add-long/2addr v4, v9

    goto/16 :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    const/4 v9, 0x1

    iput v9, p0, Lcom/mediatek/GoogleOta/CheckOta;->mOtaResult:I

    const/4 v9, 0x0

    goto/16 :goto_0

    :cond_5
    const/4 v9, 0x1

    goto/16 :goto_0
.end method

.method private getPartitionInfoPkg(Ljava/util/zip/ZipFile;Ljava/util/List;)Z
    .locals 11
    .param p1    # Ljava/util/zip/ZipFile;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/zip/ZipFile;",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;",
            ">;)Z"
        }
    .end annotation

    const-string v7, "GoogleOta/CheckOta"

    const-string v8, "into: getPartitionInfoPkg"

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v7, "scatter.txt"

    iget-object v8, p0, Lcom/mediatek/GoogleOta/CheckOta;->mScatterFilePath:Ljava/lang/String;

    invoke-direct {p0, p1, v7, v8}, Lcom/mediatek/GoogleOta/CheckOta;->unzipFileElement(Ljava/util/zip/ZipFile;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "GoogleOta/CheckOta"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "unzip fail from scatter.txt to "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/mediatek/GoogleOta/CheckOta;->mScatterFilePath:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v7, 0x0

    :goto_0
    return v7

    :cond_0
    :try_start_0
    new-instance v5, Ljava/io/FileReader;

    iget-object v7, p0, Lcom/mediatek/GoogleOta/CheckOta;->mScatterFilePath:Ljava/lang/String;

    invoke-direct {v5, v7}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/io/BufferedReader;

    invoke-direct {v0, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    :cond_1
    :goto_1
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    const-string v7, "GoogleOta/CheckOta"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "line = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v7

    const/4 v8, 0x1

    if-lt v7, v8, :cond_1

    const/4 v7, 0x0

    invoke-virtual {v3, v7}, Ljava/lang/String;->charAt(I)C

    move-result v7

    const/16 v8, 0x7b

    if-eq v7, v8, :cond_1

    const/4 v7, 0x0

    invoke-virtual {v3, v7}, Ljava/lang/String;->charAt(I)C

    move-result v7

    const/16 v8, 0x7d

    if-eq v7, v8, :cond_1

    const-string v7, "\\s+"

    invoke-virtual {v3, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    array-length v7, v4

    const/4 v8, 0x2

    if-ne v7, v8, :cond_1

    const/4 v7, 0x1

    aget-object v7, v4, v7

    if-eqz v7, :cond_1

    const/4 v7, 0x1

    aget-object v7, v4, v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    const/4 v8, 0x3

    if-lt v7, v8, :cond_1

    const/4 v7, 0x1

    aget-object v7, v4, v7

    const/4 v8, 0x2

    invoke-virtual {v7, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    new-instance v2, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;

    const/4 v7, 0x0

    aget-object v7, v4, v7

    const/16 v8, 0x10

    invoke-static {v6, v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v8

    invoke-direct {v2, p0, v7, v8, v9}, Lcom/mediatek/GoogleOta/CheckOta$PartitionInfo;-><init>(Lcom/mediatek/GoogleOta/CheckOta;Ljava/lang/String;J)V

    const-string v7, "GoogleOta/CheckOta"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "name = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x0

    aget-object v9, v4, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " offset is "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/16 v9, 0x10

    invoke-static {v6, v9}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v9

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {p2, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    const/4 v7, 0x1

    iput v7, p0, Lcom/mediatek/GoogleOta/CheckOta;->mOtaResult:I

    const/4 v7, 0x0

    goto/16 :goto_0

    :cond_2
    const/4 v7, 0x1

    goto/16 :goto_0
.end method

.method private getVersionInfoDev(Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;I)V
    .locals 1
    .param p1    # Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;
    .param p2    # I

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;->mDevNum:Ljava/lang/String;

    const-string v0, "ro.build.product"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;->mProNum:Ljava/lang/String;

    if-nez p2, :cond_0

    const-string v0, "ro.build.fingerprint"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;->mFingerPrint1:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method private getVersionInfoPkg(Ljava/util/zip/ZipFile;Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;I)Z
    .locals 8
    .param p1    # Ljava/util/zip/ZipFile;
    .param p2    # Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;
    .param p3    # I

    const/4 v4, 0x0

    const-string v5, "META-INF/com/google/android/updater-script"

    iget-object v6, p0, Lcom/mediatek/GoogleOta/CheckOta;->mBuildInfoFilePath:Ljava/lang/String;

    invoke-direct {p0, p1, v5, v6}, Lcom/mediatek/GoogleOta/CheckOta;->unzipFileElement(Ljava/util/zip/ZipFile;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "GoogleOta/CheckOta"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "unzip fail from META-INF/com/google/android/updater-script to "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/mediatek/GoogleOta/CheckOta;->mBuildInfoFilePath:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v4

    :cond_0
    :try_start_0
    new-instance v3, Ljava/io/FileReader;

    iget-object v5, p0, Lcom/mediatek/GoogleOta/CheckOta;->mBuildInfoFilePath:Ljava/lang/String;

    invoke-direct {v3, v5}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/io/BufferedReader;

    invoke-direct {v0, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    :cond_1
    :goto_1
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    iget-object v5, p2, Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;->mDevNum:Ljava/lang/String;

    if-nez v5, :cond_2

    const-string v5, "getprop(\"ro.product.device\") == \""

    invoke-direct {p0, v2, v5}, Lcom/mediatek/GoogleOta/CheckOta;->extractVerInfo(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p2, Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;->mDevNum:Ljava/lang/String;

    :cond_2
    iget-object v5, p2, Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;->mProNum:Ljava/lang/String;

    if-nez v5, :cond_3

    const-string v5, "getprop(\"ro.build.product\") == \""

    invoke-direct {p0, v2, v5}, Lcom/mediatek/GoogleOta/CheckOta;->extractVerInfo(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p2, Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;->mProNum:Ljava/lang/String;

    :cond_3
    if-nez p3, :cond_1

    iget-object v5, p2, Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;->mFingerPrint1:Ljava/lang/String;

    if-nez v5, :cond_4

    const-string v5, "file_getprop(\"/system/build.prop\", \"ro.build.fingerprint\") == \""

    invoke-direct {p0, v2, v5}, Lcom/mediatek/GoogleOta/CheckOta;->extractVerInfo(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p2, Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;->mFingerPrint1:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    const/4 v5, 0x3

    iput v5, p0, Lcom/mediatek/GoogleOta/CheckOta;->mOtaResult:I

    goto :goto_0

    :cond_4
    :try_start_1
    iget-object v5, p2, Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;->mFingerPrint2:Ljava/lang/String;

    if-nez v5, :cond_1

    const-string v5, "file_getprop(\"/system/build.prop\", \"ro.build.fingerprint\") == \""

    invoke-direct {p0, v2, v5}, Lcom/mediatek/GoogleOta/CheckOta;->extractVerInfo(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p2, Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;->mFingerPrint2:Ljava/lang/String;

    goto :goto_1

    :cond_5
    iget-object v5, p2, Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;->mDevNum:Ljava/lang/String;

    if-nez v5, :cond_6

    iget-object v5, p2, Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;->mProNum:Ljava/lang/String;

    if-eqz v5, :cond_7

    :cond_6
    if-nez p3, :cond_8

    iget-object v5, p2, Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;->mFingerPrint1:Ljava/lang/String;

    if-nez v5, :cond_8

    iget-object v5, p2, Lcom/mediatek/GoogleOta/CheckOta$LoadVersionInfo;->mFingerPrint2:Ljava/lang/String;

    if-nez v5, :cond_8

    :cond_7
    const/4 v5, 0x2

    iput v5, p0, Lcom/mediatek/GoogleOta/CheckOta;->mOtaResult:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :cond_8
    const/4 v4, 0x1

    goto :goto_0
.end method

.method private unzipFileElement(Ljava/util/zip/ZipFile;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 11
    .param p1    # Ljava/util/zip/ZipFile;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "GoogleOta/CheckOta"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "unzipFileElement:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ";"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1, p2}, Ljava/util/zip/ZipFile;->getEntry(Ljava/lang/String;)Ljava/util/zip/ZipEntry;

    move-result-object v6

    if-nez v6, :cond_0

    const/4 v8, 0x2

    iput v8, p0, Lcom/mediatek/GoogleOta/CheckOta;->mOtaResult:I

    :goto_0
    return v7

    :cond_0
    :try_start_0
    invoke-virtual {p1, v6}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    move-result-object v3

    if-nez v3, :cond_1

    const/4 v8, 0x3

    iput v8, p0, Lcom/mediatek/GoogleOta/CheckOta;->mOtaResult:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    const/4 v8, 0x4

    iput v8, p0, Lcom/mediatek/GoogleOta/CheckOta;->mOtaResult:I

    goto :goto_0

    :cond_1
    :try_start_1
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    if-nez v1, :cond_2

    const/4 v8, 0x3

    iput v8, p0, Lcom/mediatek/GoogleOta/CheckOta;->mOtaResult:I

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_3
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    if-nez v4, :cond_4

    const/4 v8, 0x3

    iput v8, p0, Lcom/mediatek/GoogleOta/CheckOta;->mOtaResult:I

    goto :goto_0

    :cond_4
    const/16 v8, 0x400

    new-array v0, v8, [B

    const/4 v5, 0x0

    :goto_1
    invoke-virtual {v3, v0}, Ljava/io/InputStream;->read([B)I

    move-result v5

    if-lez v5, :cond_5

    const/4 v8, 0x0

    invoke-virtual {v4, v0, v8, v5}, Ljava/io/FileOutputStream;->write([BII)V

    goto :goto_1

    :cond_5
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V

    const/4 v4, 0x0

    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    const/4 v7, 0x1

    goto :goto_0
.end method


# virtual methods
.method execForResult()I
    .locals 6

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/mediatek/GoogleOta/CheckOta;->mIsEmmcPhone:Z

    iput-boolean v3, p0, Lcom/mediatek/GoogleOta/CheckOta;->mPartitionChange:Z

    :try_start_0
    new-instance v2, Ljava/util/zip/ZipFile;

    iget-object v3, p0, Lcom/mediatek/GoogleOta/CheckOta;->mOtaFilePath:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/util/zip/ZipFile;-><init>(Ljava/lang/String;)V

    if-nez v2, :cond_0

    const/4 v3, 0x2

    iput v3, p0, Lcom/mediatek/GoogleOta/CheckOta;->mOtaResult:I

    const-string v3, "GoogleOta/CheckOta"

    const-string v4, "zipFile == null"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v3, p0, Lcom/mediatek/GoogleOta/CheckOta;->mOtaResult:I

    :goto_0
    return v3

    :cond_0
    invoke-direct {p0, v2}, Lcom/mediatek/GoogleOta/CheckOta;->getOtaType(Ljava/util/zip/ZipFile;)I

    move-result v1

    const-string v3, "GoogleOta/CheckOta"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "otaType = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mOtaResult = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/GoogleOta/CheckOta;->mOtaResult:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, -0x1

    if-ne v1, v3, :cond_1

    iget v3, p0, Lcom/mediatek/GoogleOta/CheckOta;->mOtaResult:I

    goto :goto_0

    :cond_1
    invoke-direct {p0, v2, v1}, Lcom/mediatek/GoogleOta/CheckOta;->checkPartiton(Ljava/util/zip/ZipFile;I)Z

    move-result v3

    if-nez v3, :cond_2

    iget v3, p0, Lcom/mediatek/GoogleOta/CheckOta;->mOtaResult:I

    goto :goto_0

    :cond_2
    invoke-direct {p0, v2, v1}, Lcom/mediatek/GoogleOta/CheckOta;->checkVersion(Ljava/util/zip/ZipFile;I)Z

    move-result v3

    if-nez v3, :cond_3

    iget v3, p0, Lcom/mediatek/GoogleOta/CheckOta;->mOtaResult:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    const/4 v3, 0x3

    iput v3, p0, Lcom/mediatek/GoogleOta/CheckOta;->mOtaResult:I

    iget v3, p0, Lcom/mediatek/GoogleOta/CheckOta;->mOtaResult:I

    goto :goto_0

    :cond_3
    iget v3, p0, Lcom/mediatek/GoogleOta/CheckOta;->mOtaResult:I

    goto :goto_0
.end method
