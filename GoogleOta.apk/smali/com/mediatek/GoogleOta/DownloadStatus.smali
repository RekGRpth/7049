.class final Lcom/mediatek/GoogleOta/DownloadStatus;
.super Ljava/lang/Object;
.source "DownloadStatus.java"

# interfaces
.implements Lcom/mediatek/GoogleOta/IDownloadStatus;


# static fields
.field private static final OTA_PREFERENCE:Ljava/lang/String; = "googleota"

.field private static final OTA_PRE_DELTA_ID:Ljava/lang/String; = "versionDeltaId"

.field private static final OTA_PRE_DOWNLOAND_PERCENT:Ljava/lang/String; = "downloadpercent"

.field private static final OTA_PRE_IMAGE_SIZE:Ljava/lang/String; = "imageSize"

.field private static final OTA_PRE_STATUS:Ljava/lang/String; = "downlaodStatus"

.field private static final OTA_PRE_VER:Ljava/lang/String; = "version"

.field private static final OTA_PRE_VER_NOTE:Ljava/lang/String; = "versionNote"

.field private static final OTA_QUERY_DATE:Ljava/lang/String; = "query_date"

.field private static final OTA_REN_STATUS:Ljava/lang/String; = "isrename"

.field private static final OTA_UNZ_STATUS:Ljava/lang/String; = "isunzip"

.field private static final OTA_UPGRADE_STATED:Ljava/lang/String; = "upgrade_stated"

.field private static final TAG:Ljava/lang/String; = "DownloadStatus:"

.field private static mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;


# instance fields
.field private mPreference:Landroid/content/SharedPreferences;

.field private mVersionInfo:Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/mediatek/GoogleOta/DownloadStatus;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/mediatek/GoogleOta/DownloadStatus;->mPreference:Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/mediatek/GoogleOta/DownloadStatus;->mVersionInfo:Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    const-string v0, "googleota"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/GoogleOta/DownloadStatus;->mPreference:Landroid/content/SharedPreferences;

    return-void
.end method

.method private getDLSessionDeltaId()I
    .locals 3

    iget-object v0, p0, Lcom/mediatek/GoogleOta/DownloadStatus;->mPreference:Landroid/content/SharedPreferences;

    const-string v1, "versionDeltaId"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method static declared-synchronized getInstance(Landroid/content/Context;)Lcom/mediatek/GoogleOta/DownloadStatus;
    .locals 2
    .param p0    # Landroid/content/Context;

    const-class v1, Lcom/mediatek/GoogleOta/DownloadStatus;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lcom/mediatek/GoogleOta/DownloadStatus;

    invoke-direct {v0, p0}, Lcom/mediatek/GoogleOta/DownloadStatus;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/mediatek/GoogleOta/DownloadStatus;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    sget-object v0, Lcom/mediatek/GoogleOta/DownloadStatus;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getVersionNote()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/mediatek/GoogleOta/DownloadStatus;->mPreference:Landroid/content/SharedPreferences;

    const-string v1, "versionNote"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getDLSessionRenameState()Z
    .locals 4

    iget-object v1, p0, Lcom/mediatek/GoogleOta/DownloadStatus;->mPreference:Landroid/content/SharedPreferences;

    const-string v2, "isrename"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    const-string v1, "DownloadStatus:"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getDLSessionRenameState, get status = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getDLSessionStatus()I
    .locals 4

    iget-object v1, p0, Lcom/mediatek/GoogleOta/DownloadStatus;->mPreference:Landroid/content/SharedPreferences;

    const-string v2, "downlaodStatus"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getDLSessionUnzipState()Z
    .locals 4

    iget-object v1, p0, Lcom/mediatek/GoogleOta/DownloadStatus;->mPreference:Landroid/content/SharedPreferences;

    const-string v2, "isunzip"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    const-string v1, "DownloadStatus:"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getDLSessionUnzipState, get status = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getDownLoadPercent()I
    .locals 4

    iget-object v1, p0, Lcom/mediatek/GoogleOta/DownloadStatus;->mPreference:Landroid/content/SharedPreferences;

    const-string v2, "downloadpercent"

    const/4 v3, -0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    const-string v1, "DownloadStatus:"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getDLSessionRenameState, get percent = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getDownloadDescriptor()Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;
    .locals 3

    iget-object v0, p0, Lcom/mediatek/GoogleOta/DownloadStatus;->mVersionInfo:Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    invoke-direct {v0}, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;-><init>()V

    iput-object v0, p0, Lcom/mediatek/GoogleOta/DownloadStatus;->mVersionInfo:Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    iget-object v0, p0, Lcom/mediatek/GoogleOta/DownloadStatus;->mVersionInfo:Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    invoke-virtual {p0}, Lcom/mediatek/GoogleOta/DownloadStatus;->getUpdateImageSize()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->size:J

    iget-object v0, p0, Lcom/mediatek/GoogleOta/DownloadStatus;->mVersionInfo:Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    invoke-virtual {p0}, Lcom/mediatek/GoogleOta/DownloadStatus;->getVersion()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->version:Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/GoogleOta/DownloadStatus;->mVersionInfo:Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/DownloadStatus;->getVersionNote()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->newNote:Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/GoogleOta/DownloadStatus;->mVersionInfo:Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/DownloadStatus;->getDLSessionDeltaId()I

    move-result v1

    iput v1, v0, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->pkgId:I

    :cond_0
    iget-object v0, p0, Lcom/mediatek/GoogleOta/DownloadStatus;->mVersionInfo:Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    return-object v0
.end method

.method public getQueryDate()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lcom/mediatek/GoogleOta/DownloadStatus;->mPreference:Landroid/content/SharedPreferences;

    const-string v2, "query_date"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "DownloadStatus:"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getQueryTime, time = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-object v0
.end method

.method public getUpdateImageSize()J
    .locals 4

    iget-object v0, p0, Lcom/mediatek/GoogleOta/DownloadStatus;->mPreference:Landroid/content/SharedPreferences;

    const-string v1, "imageSize"

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getUpgradeStartedState()Z
    .locals 4

    iget-object v1, p0, Lcom/mediatek/GoogleOta/DownloadStatus;->mPreference:Landroid/content/SharedPreferences;

    const-string v2, "upgrade_stated"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    const-string v1, "DownloadStatus:"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getUpgradeStartedState, get status = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/mediatek/GoogleOta/DownloadStatus;->mPreference:Landroid/content/SharedPreferences;

    const-string v1, "version"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setDLSessionRenameState(Z)V
    .locals 3
    .param p1    # Z

    const-string v0, "DownloadStatus:"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setDLSessionRenameState, status = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/GoogleOta/DownloadStatus;->mPreference:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "isrename"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public setDLSessionStatus(I)V
    .locals 3
    .param p1    # I

    const-string v0, "DownloadStatus:"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setDLSessionStatus, status = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/GoogleOta/DownloadStatus;->mPreference:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "downlaodStatus"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public setDLSessionUnzipState(Z)V
    .locals 3
    .param p1    # Z

    const-string v0, "DownloadStatus:"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setDLSessionUnzipState, status = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/GoogleOta/DownloadStatus;->mPreference:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "isunzip"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public setDownLoadPercent(I)V
    .locals 3
    .param p1    # I

    const-string v0, "DownloadStatus:"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setDownLoadPercent, percent = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/GoogleOta/DownloadStatus;->mPreference:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "downloadpercent"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public setDownloadDesctiptor(Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;)V
    .locals 4
    .param p1    # Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v0, "DownloadStatus:"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setDownloadDesctiptor, savedDd.version = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->version:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "DownloadStatus:"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setDownloadDesctiptor, savedDd.size = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p1, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->size:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/mediatek/GoogleOta/DownloadStatus;->mVersionInfo:Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    iget-object v0, p0, Lcom/mediatek/GoogleOta/DownloadStatus;->mPreference:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "versionNote"

    iget-object v2, p1, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->newNote:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object v0, p0, Lcom/mediatek/GoogleOta/DownloadStatus;->mPreference:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "imageSize"

    iget-wide v2, p1, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->size:J

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object v0, p1, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->version:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/GoogleOta/DownloadStatus;->mPreference:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "version"

    iget-object v2, p1, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->version:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_1
    iget-object v0, p0, Lcom/mediatek/GoogleOta/DownloadStatus;->mPreference:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "versionDeltaId"

    iget v2, p1, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->pkgId:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method public setQueryDate(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const-string v0, "DownloadStatus:"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setQueryTime, time = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/GoogleOta/DownloadStatus;->mPreference:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "query_date"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public setUpgradeStartedState(Z)V
    .locals 3
    .param p1    # Z

    const-string v0, "DownloadStatus:"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setUpgradeStartedState, status = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/GoogleOta/DownloadStatus;->mPreference:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "upgrade_stated"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method
