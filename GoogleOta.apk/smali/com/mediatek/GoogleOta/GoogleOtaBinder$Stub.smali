.class public abstract Lcom/mediatek/GoogleOta/GoogleOtaBinder$Stub;
.super Landroid/os/Binder;
.source "GoogleOtaBinder.java"

# interfaces
.implements Lcom/mediatek/GoogleOta/GoogleOtaBinder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/GoogleOta/GoogleOtaBinder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/GoogleOta/GoogleOtaBinder$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "GoogleOtaBinder"

.field static final TRANSACTION_CLEAR_UPGRADE_RESULT:I = 0x66

.field static final TRANSACTION_READ_UPGRADE_RESULT:I = 0x70

.field static final TRANSACTION_SET_REBOOT_FLAG:I = 0x65


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    const-string v0, "GoogleOtaBinder"

    invoke-virtual {p0, p0, v0}, Landroid/os/Binder;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/mediatek/GoogleOta/GoogleOtaBinder;
    .locals 2
    .param p0    # Landroid/os/IBinder;

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v1, "GoogleOtaBinder"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/mediatek/GoogleOta/GoogleOtaBinder;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/mediatek/GoogleOta/GoogleOtaBinder;

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/mediatek/GoogleOta/GoogleOtaBinder$Stub$Proxy;

    invoke-direct {v0, p0}, Lcom/mediatek/GoogleOta/GoogleOtaBinder$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/os/Parcel;
    .param p3    # Landroid/os/Parcel;
    .param p4    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v0, 0x1

    return v0
.end method
