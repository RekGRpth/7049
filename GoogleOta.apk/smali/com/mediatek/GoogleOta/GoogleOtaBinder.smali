.class public interface abstract Lcom/mediatek/GoogleOta/GoogleOtaBinder;
.super Ljava/lang/Object;
.source "GoogleOtaBinder.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/GoogleOta/GoogleOtaBinder$Stub;
    }
.end annotation


# virtual methods
.method public abstract clearUpdateResult()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract readUpgradeResult()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract setRebootFlag()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
