.class Lcom/mediatek/GoogleOta/GoogleOtaClient$15;
.super Ljava/lang/Object;
.source "GoogleOtaClient.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/GoogleOta/GoogleOtaClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;


# direct methods
.method constructor <init>(Lcom/mediatek/GoogleOta/GoogleOtaClient;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$15;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Landroid/os/IBinder;

    const-string v1, "GoogleOta/Client"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onServiceConnected, thread name = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$15;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    check-cast p2, Lcom/mediatek/GoogleOta/GoogleOtaService$ServiceBinder;

    invoke-virtual {p2}, Lcom/mediatek/GoogleOta/GoogleOtaService$ServiceBinder;->getService()Lcom/mediatek/GoogleOta/GoogleOtaService;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$802(Lcom/mediatek/GoogleOta/GoogleOtaClient;Lcom/mediatek/GoogleOta/GoogleOtaService;)Lcom/mediatek/GoogleOta/GoogleOtaService;

    const-string v1, "GoogleOta/Client"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onServiceConnected, mService = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$15;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v3}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$800(Lcom/mediatek/GoogleOta/GoogleOtaClient;)Lcom/mediatek/GoogleOta/GoogleOtaService;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$15;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v1}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$200(Lcom/mediatek/GoogleOta/GoogleOtaClient;)Lcom/mediatek/GoogleOta/DownloadStatus;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/GoogleOta/DownloadStatus;->getDLSessionStatus()I

    move-result v0

    const-string v1, "GoogleOta/Client"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onServiceConnected, download status = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$15;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v1}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$1000(Lcom/mediatek/GoogleOta/GoogleOtaClient;)V

    goto :goto_0

    :pswitch_2
    invoke-static {}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$1100()Lcom/mediatek/GoogleOta/GoogleOtaClient;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/mediatek/GoogleOta/Util;->clearNotification(Landroid/content/Context;I)V

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$15;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v1}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$1200(Lcom/mediatek/GoogleOta/GoogleOtaClient;)V

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$15;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v1}, Lcom/mediatek/GoogleOta/Util;->isSdcardAvailable(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$15;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v1}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$200(Lcom/mediatek/GoogleOta/GoogleOtaClient;)Lcom/mediatek/GoogleOta/DownloadStatus;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/mediatek/GoogleOta/DownloadStatus;->setDLSessionStatus(I)V

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$15;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v1}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$300(Lcom/mediatek/GoogleOta/GoogleOtaClient;)V

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$1100()Lcom/mediatek/GoogleOta/GoogleOtaClient;

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lcom/mediatek/GoogleOta/Util;->clearNotification(Landroid/content/Context;I)V

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$15;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v1}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$1300(Lcom/mediatek/GoogleOta/GoogleOtaClient;)V

    goto :goto_0

    :pswitch_4
    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$15;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v1}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$300(Lcom/mediatek/GoogleOta/GoogleOtaClient;)V

    goto :goto_0

    :pswitch_5
    invoke-static {}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$1100()Lcom/mediatek/GoogleOta/GoogleOtaClient;

    move-result-object v1

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/mediatek/GoogleOta/Util;->clearNotification(Landroid/content/Context;I)V

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$15;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v1}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$1400(Lcom/mediatek/GoogleOta/GoogleOtaClient;)V

    goto :goto_0

    :pswitch_6
    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$15;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v1}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$1500(Lcom/mediatek/GoogleOta/GoogleOtaClient;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;

    const-string v0, "GoogleOta/Client"

    const-string v1, "onServiceDisconnected"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$15;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$802(Lcom/mediatek/GoogleOta/GoogleOtaClient;Lcom/mediatek/GoogleOta/GoogleOtaService;)Lcom/mediatek/GoogleOta/GoogleOtaService;

    return-void
.end method
