.class Lcom/mediatek/GoogleOta/GoogleOtaClient$16;
.super Landroid/os/Handler;
.source "GoogleOtaClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/GoogleOta/GoogleOtaClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;


# direct methods
.method constructor <init>(Lcom/mediatek/GoogleOta/GoogleOtaClient;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .param p1    # Landroid/os/Message;

    const v8, 0x7f040028

    const/4 v7, 0x0

    const/4 v6, 0x1

    const v5, 0x7f040027

    const/4 v4, 0x7

    const-string v1, "GoogleOta/Client"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleMessage msg.what = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "GoogleOta/Client"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleMessage thread name = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$1600()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    :goto_0
    return-void

    :cond_0
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto :goto_0

    :pswitch_0
    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    iget v2, p1, Landroid/os/Message;->arg1:I

    iget v3, p1, Landroid/os/Message;->arg2:I

    invoke-static {v1, v2, v3}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$1700(Lcom/mediatek/GoogleOta/GoogleOtaClient;II)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v1}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$1800(Lcom/mediatek/GoogleOta/GoogleOtaClient;)V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v1}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$1200(Lcom/mediatek/GoogleOta/GoogleOtaClient;)V

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    iget-object v2, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v2}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$100(Lcom/mediatek/GoogleOta/GoogleOtaClient;)Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$1900(Lcom/mediatek/GoogleOta/GoogleOtaClient;Landroid/app/Dialog;)V

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v1}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$1400(Lcom/mediatek/GoogleOta/GoogleOtaClient;)V

    goto :goto_0

    :pswitch_4
    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v1}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$200(Lcom/mediatek/GoogleOta/GoogleOtaClient;)Lcom/mediatek/GoogleOta/DownloadStatus;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/GoogleOta/DownloadStatus;->getDownloadDescriptor()Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    iget-wide v2, v0, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->size:J

    invoke-static {v1, v2, v3}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$2000(Lcom/mediatek/GoogleOta/GoogleOtaClient;J)V

    goto :goto_0

    :pswitch_5
    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    const v2, 0x7f040024

    invoke-static {v1, v2}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$2100(Lcom/mediatek/GoogleOta/GoogleOtaClient;I)V

    goto :goto_0

    :pswitch_6
    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    const v2, 0x7f040025

    invoke-static {v1, v2}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$2100(Lcom/mediatek/GoogleOta/GoogleOtaClient;I)V

    goto :goto_0

    :pswitch_7
    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v1}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$2200(Lcom/mediatek/GoogleOta/GoogleOtaClient;)V

    goto :goto_0

    :pswitch_8
    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    const v2, 0x7f040015

    invoke-static {v1, v2}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$2100(Lcom/mediatek/GoogleOta/GoogleOtaClient;I)V

    goto :goto_0

    :pswitch_9
    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    iget-object v2, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v2}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$100(Lcom/mediatek/GoogleOta/GoogleOtaClient;)Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$1900(Lcom/mediatek/GoogleOta/GoogleOtaClient;Landroid/app/Dialog;)V

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$2300(Lcom/mediatek/GoogleOta/GoogleOtaClient;I)V

    goto :goto_0

    :pswitch_a
    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    iget-object v2, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v2}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$100(Lcom/mediatek/GoogleOta/GoogleOtaClient;)Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$1900(Lcom/mediatek/GoogleOta/GoogleOtaClient;Landroid/app/Dialog;)V

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$2300(Lcom/mediatek/GoogleOta/GoogleOtaClient;I)V

    goto/16 :goto_0

    :pswitch_b
    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v1}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$2400(Lcom/mediatek/GoogleOta/GoogleOtaClient;)V

    goto/16 :goto_0

    :pswitch_c
    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v1, v6}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$602(Lcom/mediatek/GoogleOta/GoogleOtaClient;Z)Z

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v1, v5}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$2502(Lcom/mediatek/GoogleOta/GoogleOtaClient;I)I

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v1, v8}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$2602(Lcom/mediatek/GoogleOta/GoogleOtaClient;I)I

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-virtual {v1, v4}, Landroid/app/Activity;->showDialog(I)V

    goto/16 :goto_0

    :pswitch_d
    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v1, v6}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$602(Lcom/mediatek/GoogleOta/GoogleOtaClient;Z)Z

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v1, v5}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$2502(Lcom/mediatek/GoogleOta/GoogleOtaClient;I)I

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    const v2, 0x7f04002b

    invoke-static {v1, v2}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$2602(Lcom/mediatek/GoogleOta/GoogleOtaClient;I)I

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-virtual {v1, v4}, Landroid/app/Activity;->showDialog(I)V

    goto/16 :goto_0

    :pswitch_e
    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v1, v7}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$602(Lcom/mediatek/GoogleOta/GoogleOtaClient;Z)Z

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v1, v5}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$2502(Lcom/mediatek/GoogleOta/GoogleOtaClient;I)I

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    const v2, 0x7f040014

    invoke-static {v1, v2}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$2602(Lcom/mediatek/GoogleOta/GoogleOtaClient;I)I

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-virtual {v1, v4}, Landroid/app/Activity;->showDialog(I)V

    goto/16 :goto_0

    :pswitch_f
    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v1, v6}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$602(Lcom/mediatek/GoogleOta/GoogleOtaClient;Z)Z

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v1, v5}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$2502(Lcom/mediatek/GoogleOta/GoogleOtaClient;I)I

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    const v2, 0x7f04002a

    invoke-static {v1, v2}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$2602(Lcom/mediatek/GoogleOta/GoogleOtaClient;I)I

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-virtual {v1, v4}, Landroid/app/Activity;->showDialog(I)V

    goto/16 :goto_0

    :pswitch_10
    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v1, v7}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$602(Lcom/mediatek/GoogleOta/GoogleOtaClient;Z)Z

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v1, v5}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$2502(Lcom/mediatek/GoogleOta/GoogleOtaClient;I)I

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    const v2, 0x7f040016

    invoke-static {v1, v2}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$2602(Lcom/mediatek/GoogleOta/GoogleOtaClient;I)I

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-virtual {v1, v4}, Landroid/app/Activity;->showDialog(I)V

    goto/16 :goto_0

    :pswitch_11
    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v1, v7}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$602(Lcom/mediatek/GoogleOta/GoogleOtaClient;Z)Z

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v1, v5}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$2502(Lcom/mediatek/GoogleOta/GoogleOtaClient;I)I

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    const v2, 0x7f040029

    invoke-static {v1, v2}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$2602(Lcom/mediatek/GoogleOta/GoogleOtaClient;I)I

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-virtual {v1, v4}, Landroid/app/Activity;->showDialog(I)V

    goto/16 :goto_0

    :pswitch_12
    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/app/Activity;->showDialog(I)V

    goto/16 :goto_0

    :pswitch_13
    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    iget-object v2, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v2}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$100(Lcom/mediatek/GoogleOta/GoogleOtaClient;)Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$1900(Lcom/mediatek/GoogleOta/GoogleOtaClient;Landroid/app/Dialog;)V

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v1, v6}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$602(Lcom/mediatek/GoogleOta/GoogleOtaClient;Z)Z

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    const v2, 0x7f040030

    invoke-static {v1, v2}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$2502(Lcom/mediatek/GoogleOta/GoogleOtaClient;I)I

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v1, v8}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$2602(Lcom/mediatek/GoogleOta/GoogleOtaClient;I)I

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-virtual {v1, v4}, Landroid/app/Activity;->showDialog(I)V

    goto/16 :goto_0

    :pswitch_14
    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_c
        :pswitch_d
        :pswitch_f
        :pswitch_11
        :pswitch_14
        :pswitch_10
        :pswitch_e
        :pswitch_13
        :pswitch_13
        :pswitch_12
    .end packed-switch
.end method
