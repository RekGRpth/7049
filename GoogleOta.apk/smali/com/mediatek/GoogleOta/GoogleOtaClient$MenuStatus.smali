.class final enum Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;
.super Ljava/lang/Enum;
.source "GoogleOtaClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/GoogleOta/GoogleOtaClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "MenuStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

.field public static final enum Menu_Download:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

.field public static final enum Menu_Downloading:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

.field public static final enum Menu_Finish:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

.field public static final enum Menu_None:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

.field public static final enum Menu_Resume:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

.field public static final enum Menu_Retry:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

.field public static final enum Menu_Upgrade:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    const-string v1, "Menu_None"

    invoke-direct {v0, v1, v3}, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;->Menu_None:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    new-instance v0, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    const-string v1, "Menu_Download"

    invoke-direct {v0, v1, v4}, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;->Menu_Download:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    new-instance v0, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    const-string v1, "Menu_Downloading"

    invoke-direct {v0, v1, v5}, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;->Menu_Downloading:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    new-instance v0, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    const-string v1, "Menu_Resume"

    invoke-direct {v0, v1, v6}, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;->Menu_Resume:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    new-instance v0, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    const-string v1, "Menu_Finish"

    invoke-direct {v0, v1, v7}, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;->Menu_Finish:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    new-instance v0, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    const-string v1, "Menu_Retry"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;->Menu_Retry:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    new-instance v0, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    const-string v1, "Menu_Upgrade"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;->Menu_Upgrade:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    sget-object v1, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;->Menu_None:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;->Menu_Download:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;->Menu_Downloading:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;->Menu_Resume:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;->Menu_Finish:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;->Menu_Retry:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;->Menu_Upgrade:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;->$VALUES:[Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    return-object v0
.end method

.method public static values()[Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;
    .locals 1

    sget-object v0, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;->$VALUES:[Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    invoke-virtual {v0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    return-object v0
.end method
