.class Lcom/mediatek/GoogleOta/GoogleOtaClient$SessionStateControlThread;
.super Ljava/lang/Thread;
.source "GoogleOtaClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/GoogleOta/GoogleOtaClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SessionStateControlThread"
.end annotation


# instance fields
.field private status:I

.field final synthetic this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;


# direct methods
.method public constructor <init>(Lcom/mediatek/GoogleOta/GoogleOtaClient;I)V
    .locals 0
    .param p2    # I

    iput-object p1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$SessionStateControlThread;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput p2, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$SessionStateControlThread;->status:I

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$SessionStateControlThread;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v1}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$800(Lcom/mediatek/GoogleOta/GoogleOtaClient;)Lcom/mediatek/GoogleOta/GoogleOtaService;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v1, "GoogleOta/Client"

    const-string v2, "SessionStateControlThread mService = null"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const-string v1, "GoogleOta/Client"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SessionStateControlThread, status = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$SessionStateControlThread;->status:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", thread name = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$SessionStateControlThread;->status:I

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    const-wide/16 v1, 0x96

    :try_start_0
    invoke-static {v1, v2}, Lcom/mediatek/GoogleOta/GoogleOtaClient$SessionStateControlThread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$SessionStateControlThread;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v1}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$800(Lcom/mediatek/GoogleOta/GoogleOtaClient;)Lcom/mediatek/GoogleOta/GoogleOtaService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/GoogleOta/GoogleOtaService;->queryNewVersion()V

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_1

    :pswitch_2
    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$SessionStateControlThread;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v1}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$800(Lcom/mediatek/GoogleOta/GoogleOtaClient;)Lcom/mediatek/GoogleOta/GoogleOtaService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/GoogleOta/GoogleOtaService;->startDlPkg()V

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$SessionStateControlThread;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v1}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$800(Lcom/mediatek/GoogleOta/GoogleOtaClient;)Lcom/mediatek/GoogleOta/GoogleOtaService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/GoogleOta/GoogleOtaService;->cancelDlPkg()V

    goto :goto_0

    :pswitch_4
    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$SessionStateControlThread;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v1}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$800(Lcom/mediatek/GoogleOta/GoogleOtaClient;)Lcom/mediatek/GoogleOta/GoogleOtaService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/GoogleOta/GoogleOtaService;->pauseDlPkg()V

    goto :goto_0

    :pswitch_5
    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$SessionStateControlThread;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v1}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$800(Lcom/mediatek/GoogleOta/GoogleOtaClient;)Lcom/mediatek/GoogleOta/GoogleOtaService;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$SessionStateControlThread;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v2}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$900(Lcom/mediatek/GoogleOta/GoogleOtaClient;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/mediatek/GoogleOta/GoogleOtaService;->setUpdateType(I)V

    goto :goto_0

    :pswitch_6
    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient$SessionStateControlThread;->this$0:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {v1}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->access$800(Lcom/mediatek/GoogleOta/GoogleOtaClient;)Lcom/mediatek/GoogleOta/GoogleOtaService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/GoogleOta/GoogleOtaService;->resetDescriptionInfo()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
