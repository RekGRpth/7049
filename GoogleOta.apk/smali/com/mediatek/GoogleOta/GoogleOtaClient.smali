.class public Lcom/mediatek/GoogleOta/GoogleOtaClient;
.super Landroid/app/Activity;
.source "GoogleOtaClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/GoogleOta/GoogleOtaClient$SessionStateControlThread;,
        Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;
    }
.end annotation


# static fields
.field private static final DECOMPRESS_RATIO:D = 2.5

.field private static final DIALOG_CANCELDOWNLOAD:I = 0x1

.field private static final DIALOG_NETWORKERROR:I = 0x3

.field private static final DIALOG_NOENOUGHSPACE:I = 0x2

.field private static final DIALOG_NOSDCARD:I = 0x5

.field private static final DIALOG_OTARESULT:I = 0x7

.field private static final DIALOG_QUERYWAITING:I = 0x0

.field private static final DIALOG_SERVERERROR:I = 0x4

.field private static final DIALOG_UNKNOWNERROR:I = 0x6

.field private static final DIALOG_UNZIPPING:I = 0x8

.field private static final MENU_ID_CANCEL:I = 0x3

.field private static final MENU_ID_DOWNLOAD:I = 0x1

.field private static final MENU_ID_OK:I = 0x5

.field private static final MENU_ID_PAUSE:I = 0x2

.field private static final MENU_ID_RESUME:I = 0x4

.field private static final MENU_ID_RETRY:I = 0x6

.field private static final MENU_ID_UPGRADE:I = 0x7

.field private static final NETWORKERROR_SPECIFIED:I = 0x1

.field private static final OTARESULT_DLG_MSG:Ljava/lang/String; = "otaresult_dlg_msg"

.field private static final OTARESULT_DLG_TITLE:Ljava/lang/String; = "otaresult_dlg_title"

.field private static final TAG:Ljava/lang/String; = "GoogleOta/Client"

.field private static isBg:Z

.field private static mClientInstance:Lcom/mediatek/GoogleOta/GoogleOtaClient;

.field private static mQueryNewVersionThread:Lcom/mediatek/GoogleOta/GoogleOtaClient$SessionStateControlThread;


# instance fields
.field private mAlertDialog:Landroid/app/AlertDialog;

.field private mConnection:Landroid/content/ServiceConnection;

.field private mDlNewContentTitle:Landroid/widget/TextView;

.field private mDlNewNotesContent:Landroid/widget/TextView;

.field private mDlPkgProgressThread:Lcom/mediatek/GoogleOta/GoogleOtaClient$SessionStateControlThread;

.field private mDlProgress:Landroid/widget/TextView;

.field private mDlRatio:Landroid/widget/TextView;

.field private mDlRatioProgressBar:Landroid/widget/ProgressBar;

.field private mDownloadCancelDialog:Landroid/app/AlertDialog;

.field private mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

.field private mDownloadStorageDialog:Landroid/app/AlertDialog;

.field private mHandler:Landroid/os/Handler;

.field private mMenuStatus:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

.field private mNeedReset:Z

.field private mNegativeButton:Landroid/widget/Button;

.field private mOTADialogMessageResId:I

.field private mOTADialogTitleResId:I

.field private mPositiveButton:Landroid/widget/Button;

.field private mQueryType:I

.field private mScreenButton:Landroid/widget/Button;

.field private mScreenText:Landroid/widget/TextView;

.field private mService:Lcom/mediatek/GoogleOta/GoogleOtaService;

.field private mUnzipProgressDialog:Landroid/app/ProgressDialog;

.field private mUpdateButton:Landroid/widget/Button;

.field private mUpdateRadioList:Landroid/widget/ListView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mClientInstance:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->isBg:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mService:Lcom/mediatek/GoogleOta/GoogleOtaService;

    iput-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    iput v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mQueryType:I

    iput v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mOTADialogTitleResId:I

    iput v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mOTADialogMessageResId:I

    iput-boolean v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mNeedReset:Z

    sget-object v0, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;->Menu_None:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    iput-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mMenuStatus:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    new-instance v0, Lcom/mediatek/GoogleOta/GoogleOtaClient$15;

    invoke-direct {v0, p0}, Lcom/mediatek/GoogleOta/GoogleOtaClient$15;-><init>(Lcom/mediatek/GoogleOta/GoogleOtaClient;)V

    iput-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mConnection:Landroid/content/ServiceConnection;

    new-instance v0, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;

    invoke-direct {v0, p0}, Lcom/mediatek/GoogleOta/GoogleOtaClient$16;-><init>(Lcom/mediatek/GoogleOta/GoogleOtaClient;)V

    iput-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/GoogleOta/GoogleOtaClient;)V
    .locals 0
    .param p0    # Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->onQueryNewVersionAbort()V

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/GoogleOta/GoogleOtaClient;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0    # Lcom/mediatek/GoogleOta/GoogleOtaClient;

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mUnzipProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/mediatek/GoogleOta/GoogleOtaClient;)V
    .locals 0
    .param p0    # Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->onQueryNewVersion()V

    return-void
.end method

.method static synthetic access$102(Lcom/mediatek/GoogleOta/GoogleOtaClient;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0
    .param p0    # Lcom/mediatek/GoogleOta/GoogleOtaClient;
    .param p1    # Landroid/app/ProgressDialog;

    iput-object p1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mUnzipProgressDialog:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic access$1100()Lcom/mediatek/GoogleOta/GoogleOtaClient;
    .locals 1

    sget-object v0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mClientInstance:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/mediatek/GoogleOta/GoogleOtaClient;)V
    .locals 0
    .param p0    # Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->onNewVersionDetected()V

    return-void
.end method

.method static synthetic access$1300(Lcom/mediatek/GoogleOta/GoogleOtaClient;)V
    .locals 0
    .param p0    # Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->onDownloadingPkg()V

    return-void
.end method

.method static synthetic access$1400(Lcom/mediatek/GoogleOta/GoogleOtaClient;)V
    .locals 0
    .param p0    # Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->onDlPkgComplete()V

    return-void
.end method

.method static synthetic access$1500(Lcom/mediatek/GoogleOta/GoogleOtaClient;)V
    .locals 0
    .param p0    # Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->onPkgUnzipping()V

    return-void
.end method

.method static synthetic access$1600()Z
    .locals 1

    sget-boolean v0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->isBg:Z

    return v0
.end method

.method static synthetic access$1700(Lcom/mediatek/GoogleOta/GoogleOtaClient;II)V
    .locals 0
    .param p0    # Lcom/mediatek/GoogleOta/GoogleOtaClient;
    .param p1    # I
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->onNetworkError(II)V

    return-void
.end method

.method static synthetic access$1800(Lcom/mediatek/GoogleOta/GoogleOtaClient;)V
    .locals 0
    .param p0    # Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->onNoNewVersionDetected()V

    return-void
.end method

.method static synthetic access$1900(Lcom/mediatek/GoogleOta/GoogleOtaClient;Landroid/app/Dialog;)V
    .locals 0
    .param p0    # Lcom/mediatek/GoogleOta/GoogleOtaClient;
    .param p1    # Landroid/app/Dialog;

    invoke-direct {p0, p1}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->dismissDialog(Landroid/app/Dialog;)V

    return-void
.end method

.method static synthetic access$200(Lcom/mediatek/GoogleOta/GoogleOtaClient;)Lcom/mediatek/GoogleOta/DownloadStatus;
    .locals 1
    .param p0    # Lcom/mediatek/GoogleOta/GoogleOtaClient;

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/mediatek/GoogleOta/GoogleOtaClient;J)V
    .locals 0
    .param p0    # Lcom/mediatek/GoogleOta/GoogleOtaClient;
    .param p1    # J

    invoke-direct {p0, p1, p2}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->onDlPkgUpgrade(J)V

    return-void
.end method

.method static synthetic access$2100(Lcom/mediatek/GoogleOta/GoogleOtaClient;I)V
    .locals 0
    .param p0    # Lcom/mediatek/GoogleOta/GoogleOtaClient;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->onNonDialogPrompt(I)V

    return-void
.end method

.method static synthetic access$2200(Lcom/mediatek/GoogleOta/GoogleOtaClient;)V
    .locals 0
    .param p0    # Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->onRequeryNeed()V

    return-void
.end method

.method static synthetic access$2300(Lcom/mediatek/GoogleOta/GoogleOtaClient;I)V
    .locals 0
    .param p0    # Lcom/mediatek/GoogleOta/GoogleOtaClient;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->onDialogPrompt(I)V

    return-void
.end method

.method static synthetic access$2400(Lcom/mediatek/GoogleOta/GoogleOtaClient;)V
    .locals 0
    .param p0    # Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->onUnknowPrompt()V

    return-void
.end method

.method static synthetic access$2502(Lcom/mediatek/GoogleOta/GoogleOtaClient;I)I
    .locals 0
    .param p0    # Lcom/mediatek/GoogleOta/GoogleOtaClient;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mOTADialogTitleResId:I

    return p1
.end method

.method static synthetic access$2602(Lcom/mediatek/GoogleOta/GoogleOtaClient;I)I
    .locals 0
    .param p0    # Lcom/mediatek/GoogleOta/GoogleOtaClient;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mOTADialogMessageResId:I

    return p1
.end method

.method static synthetic access$300(Lcom/mediatek/GoogleOta/GoogleOtaClient;)V
    .locals 0
    .param p0    # Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->onDlPkgResume()V

    return-void
.end method

.method static synthetic access$400(Lcom/mediatek/GoogleOta/GoogleOtaClient;)V
    .locals 0
    .param p0    # Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->resetDescriptionInfo()V

    return-void
.end method

.method static synthetic access$502(Lcom/mediatek/GoogleOta/GoogleOtaClient;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0    # Lcom/mediatek/GoogleOta/GoogleOtaClient;
    .param p1    # Landroid/app/AlertDialog;

    iput-object p1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDownloadCancelDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$600(Lcom/mediatek/GoogleOta/GoogleOtaClient;)Z
    .locals 1
    .param p0    # Lcom/mediatek/GoogleOta/GoogleOtaClient;

    iget-boolean v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mNeedReset:Z

    return v0
.end method

.method static synthetic access$602(Lcom/mediatek/GoogleOta/GoogleOtaClient;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/GoogleOta/GoogleOtaClient;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mNeedReset:Z

    return p1
.end method

.method static synthetic access$700(Lcom/mediatek/GoogleOta/GoogleOtaClient;)V
    .locals 0
    .param p0    # Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->resetDownloadDesctiptor()V

    return-void
.end method

.method static synthetic access$800(Lcom/mediatek/GoogleOta/GoogleOtaClient;)Lcom/mediatek/GoogleOta/GoogleOtaService;
    .locals 1
    .param p0    # Lcom/mediatek/GoogleOta/GoogleOtaClient;

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mService:Lcom/mediatek/GoogleOta/GoogleOtaService;

    return-object v0
.end method

.method static synthetic access$802(Lcom/mediatek/GoogleOta/GoogleOtaClient;Lcom/mediatek/GoogleOta/GoogleOtaService;)Lcom/mediatek/GoogleOta/GoogleOtaService;
    .locals 0
    .param p0    # Lcom/mediatek/GoogleOta/GoogleOtaClient;
    .param p1    # Lcom/mediatek/GoogleOta/GoogleOtaService;

    iput-object p1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mService:Lcom/mediatek/GoogleOta/GoogleOtaService;

    return-object p1
.end method

.method static synthetic access$900(Lcom/mediatek/GoogleOta/GoogleOtaClient;)I
    .locals 1
    .param p0    # Lcom/mediatek/GoogleOta/GoogleOtaClient;

    iget v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mQueryType:I

    return v0
.end method

.method private dismissDialog(Landroid/app/Dialog;)V
    .locals 0
    .param p1    # Landroid/app/Dialog;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/app/Dialog;->dismiss()V

    :cond_0
    return-void
.end method

.method static getHandler()Landroid/os/Handler;
    .locals 1

    sget-object v0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mClientInstance:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    iget-object v0, v0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static getInstance()Lcom/mediatek/GoogleOta/GoogleOtaClient;
    .locals 1

    sget-object v0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mClientInstance:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    return-object v0
.end method

.method private onDialogPrompt(I)V
    .locals 5
    .param p1    # I

    const/4 v4, 0x5

    const/4 v3, 0x2

    const-string v0, "GoogleOta/Client"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDialogPrompt, id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-ne p1, v3, :cond_0

    invoke-virtual {p0, v3}, Landroid/app/Activity;->showDialog(I)V

    :goto_0
    return-void

    :cond_0
    if-ne p1, v4, :cond_1

    invoke-virtual {p0, v4}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0
.end method

.method private onDlPkgCanceled()V
    .locals 2

    const-string v0, "GoogleOta/Client"

    const-string v1, "onDlPkgCancelled"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->onDlPkgPaused(Z)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/app/Activity;->showDialog(I)V

    return-void
.end method

.method private onDlPkgComplete()V
    .locals 3

    const-string v1, "GoogleOta/Client"

    const-string v2, "onDlPkgComplete"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f050000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f030002

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setContentView(I)V

    invoke-direct {p0, v0}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->onShowUpdateList([Ljava/lang/String;)V

    sget-object v1, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;->Menu_Upgrade:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    iput-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mMenuStatus:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    invoke-virtual {p0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    return-void
.end method

.method private onDlPkgPaused(Z)V
    .locals 2
    .param p1    # Z

    const-string v0, "GoogleOta/Client"

    const-string v1, "onDlPkgPaused"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/mediatek/GoogleOta/GoogleOtaClient$SessionStateControlThread;

    const/4 v1, 0x4

    invoke-direct {v0, p0, v1}, Lcom/mediatek/GoogleOta/GoogleOtaClient$SessionStateControlThread;-><init>(Lcom/mediatek/GoogleOta/GoogleOtaClient;I)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_0
    return-void
.end method

.method private onDlPkgResume()V
    .locals 2

    const-string v0, "GoogleOta/Client"

    const-string v1, "onDlPkgResume"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/mediatek/GoogleOta/GoogleOtaClient$SessionStateControlThread;

    const/4 v1, 0x2

    invoke-direct {v0, p0, v1}, Lcom/mediatek/GoogleOta/GoogleOtaClient$SessionStateControlThread;-><init>(Lcom/mediatek/GoogleOta/GoogleOtaClient;I)V

    iput-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDlPkgProgressThread:Lcom/mediatek/GoogleOta/GoogleOtaClient$SessionStateControlThread;

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDlPkgProgressThread:Lcom/mediatek/GoogleOta/GoogleOtaClient$SessionStateControlThread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->onDownloadingPkg()V

    return-void
.end method

.method private onDlPkgUpgrade(J)V
    .locals 11
    .param p1    # J

    const/high16 v2, -0x40800000

    const/high16 v6, -0x40800000

    invoke-static {p0}, Lcom/mediatek/GoogleOta/Util;->getPackageFileName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/mediatek/GoogleOta/Util;->getFileSize(Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v7, 0x0

    cmp-long v7, v0, v7

    if-gez v7, :cond_0

    const-wide/16 v0, 0x0

    :cond_0
    const-wide/16 v7, 0x0

    cmp-long v7, p1, v7

    if-nez v7, :cond_1

    const-wide/16 p1, -0x1

    :cond_1
    const-string v7, "GoogleOta/Client"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onDlPkgUpgrade dlSize:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " totalSize:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v7, 0x0

    cmp-long v7, p1, v7

    if-gez v7, :cond_2

    :goto_0
    return-void

    :cond_2
    const v7, 0x7f070008

    invoke-virtual {p0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ProgressBar;

    iput-object v7, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDlRatioProgressBar:Landroid/widget/ProgressBar;

    const v7, 0x7f07000a

    invoke-virtual {p0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDlRatio:Landroid/widget/TextView;

    const v7, 0x7f07000b

    invoke-virtual {p0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDlProgress:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    invoke-virtual {v7}, Lcom/mediatek/GoogleOta/DownloadStatus;->getDLSessionRenameState()Z

    move-result v7

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    invoke-virtual {v7}, Lcom/mediatek/GoogleOta/DownloadStatus;->getDownLoadPercent()I

    move-result v7

    const/16 v8, 0x64

    if-ne v7, v8, :cond_3

    move-wide v0, p1

    const-string v7, "GoogleOta/Client"

    const-string v8, "onDlPkgUpgrade, download complete but upzip terminate by exception"

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    long-to-double v7, v0

    long-to-double v9, p1

    div-double/2addr v7, v9

    const-wide/high16 v9, 0x4059000000000000L

    mul-double/2addr v7, v9

    double-to-int v4, v7

    const/16 v7, 0x64

    if-le v4, v7, :cond_4

    const/16 v4, 0x64

    move-wide v0, p1

    :cond_4
    iget-object v7, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    invoke-virtual {v7, v4}, Lcom/mediatek/GoogleOta/DownloadStatus;->setDownLoadPercent(I)V

    iget-object v7, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDlRatioProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v7, v4}, Landroid/widget/ProgressBar;->setProgress(I)V

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "%"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v7, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDlRatio:Landroid/widget/TextView;

    invoke-virtual {v7, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    long-to-float v7, v0

    float-to-double v7, v7

    const-wide/high16 v9, 0x4090000000000000L

    div-double/2addr v7, v9

    double-to-float v2, v7

    const/high16 v7, 0x42c80000

    mul-float/2addr v7, v2

    float-to-int v7, v7

    int-to-double v7, v7

    const-wide/high16 v9, 0x4059000000000000L

    div-double/2addr v7, v9

    double-to-float v2, v7

    long-to-float v7, p1

    float-to-double v7, v7

    const-wide/high16 v9, 0x4090000000000000L

    div-double/2addr v7, v9

    double-to-float v6, v7

    const/high16 v7, 0x42c80000

    mul-float/2addr v7, v6

    float-to-int v7, v7

    int-to-double v7, v7

    const-wide/high16 v9, 0x4059000000000000L

    div-double/2addr v7, v9

    double-to-float v6, v7

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v2}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " / "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v6}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " (KB)"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v7, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDlProgress:Landroid/widget/TextView;

    invoke-virtual {v7, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method private onDoClearAction()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mUnzipProgressDialog:Landroid/app/ProgressDialog;

    invoke-direct {p0, v0}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->dismissDialog(Landroid/app/Dialog;)V

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDownloadCancelDialog:Landroid/app/AlertDialog;

    invoke-direct {p0, v0}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->dismissDialog(Landroid/app/Dialog;)V

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDownloadStorageDialog:Landroid/app/AlertDialog;

    invoke-direct {p0, v0}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->dismissDialog(Landroid/app/Dialog;)V

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-direct {p0, v0}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->dismissDialog(Landroid/app/Dialog;)V

    iput-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mUnzipProgressDialog:Landroid/app/ProgressDialog;

    iput-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDownloadCancelDialog:Landroid/app/AlertDialog;

    iput-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDownloadStorageDialog:Landroid/app/AlertDialog;

    iput-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mAlertDialog:Landroid/app/AlertDialog;

    return-void
.end method

.method private onDownloadingPkg()V
    .locals 3

    const-string v1, "GoogleOta/Client"

    const-string v2, "onDownloadingPkg"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    invoke-virtual {v1}, Lcom/mediatek/GoogleOta/DownloadStatus;->getDownloadDescriptor()Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->onLayoutNewVersionInfo(Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;)V

    const v1, 0x7f070007

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    sget-object v1, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;->Menu_Downloading:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    iput-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mMenuStatus:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    invoke-virtual {p0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    iget-wide v1, v0, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->size:J

    invoke-direct {p0, v1, v2}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->onDlPkgUpgrade(J)V

    return-void
.end method

.method private onLayoutErrorInfo(II)V
    .locals 7
    .param p1    # I
    .param p2    # I

    const v6, 0x7f07000c

    const/4 v5, 0x1

    const/4 v4, 0x4

    const/4 v3, 0x0

    const-string v0, "GoogleOta/Client"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onLayoutErrorInfo, status = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_2

    invoke-virtual {p0, v3}, Landroid/app/Activity;->removeDialog(I)V

    const v0, 0x7f030001

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    if-ne p2, v5, :cond_1

    invoke-virtual {p0, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v4}, Landroid/app/Activity;->showDialog(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f07000d

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mScreenText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mScreenText:Landroid/widget/TextView;

    const v1, 0x7f04000d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    sget-object v0, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;->Menu_Retry:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    iput-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mMenuStatus:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    invoke-virtual {p0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    goto :goto_0

    :cond_2
    const/4 v0, 0x2

    if-eq p1, v0, :cond_3

    if-ne p1, v4, :cond_0

    :cond_3
    if-ne p2, v5, :cond_4

    invoke-virtual {p0, v4}, Landroid/app/Activity;->showDialog(I)V

    :goto_1
    invoke-direct {p0, v3}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->onDlPkgPaused(Z)V

    sget-object v0, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;->Menu_Resume:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    iput-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mMenuStatus:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    invoke-virtual {p0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    goto :goto_0

    :cond_4
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_1
.end method

.method private onLayoutNewVersionInfo(Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;)V
    .locals 5
    .param p1    # Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    const-string v1, "GoogleOta/Client"

    const-string v2, "onLayoutNewVersionInfo"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/high16 v1, 0x7f030000

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setContentView(I)V

    const v1, 0x7f070004

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDlNewContentTitle:Landroid/widget/TextView;

    const v1, 0x7f070006

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDlNewNotesContent:Landroid/widget/TextView;

    iget-wide v1, p1, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->size:J

    long-to-float v1, v1

    float-to-double v1, v1

    const-wide/high16 v3, 0x4090000000000000L

    div-double/2addr v1, v3

    double-to-float v0, v1

    const/high16 v1, 0x42c80000

    mul-float/2addr v1, v0

    float-to-int v1, v1

    int-to-double v1, v1

    const-wide/high16 v3, 0x4059000000000000L

    div-double/2addr v1, v3

    double-to-float v0, v1

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDlNewContentTitle:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p1, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->version:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " KB)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p1, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->newNote:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->newNote:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDlNewNotesContent:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDlNewNotesContent:Landroid/widget/TextView;

    iget-object v2, p1, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->newNote:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private onNetworkError(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    const-string v0, "GoogleOta/Client"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onNetworkError, isServerNetwork = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    invoke-virtual {v0}, Lcom/mediatek/GoogleOta/DownloadStatus;->getDLSessionStatus()I

    move-result v0

    invoke-direct {p0, v0, p2}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->onLayoutErrorInfo(II)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    invoke-virtual {v0}, Lcom/mediatek/GoogleOta/DownloadStatus;->getDLSessionStatus()I

    move-result v0

    invoke-direct {p0, v0, p2}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->onLayoutErrorInfo(II)V

    goto :goto_0
.end method

.method private onNewVersionDetected()V
    .locals 3

    const-string v1, "GoogleOta/Client"

    const-string v2, "onNewVersionDetected"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Landroid/app/Activity;->removeDialog(I)V

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    invoke-virtual {v1}, Lcom/mediatek/GoogleOta/DownloadStatus;->getDownloadDescriptor()Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->onLayoutNewVersionInfo(Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;)V

    const v1, 0x7f070007

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    sget-object v1, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;->Menu_Download:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    iput-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mMenuStatus:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    invoke-virtual {p0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    return-void
.end method

.method private onNoNewVersionDetected()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "GoogleOta/Client"

    const-string v1, "onNoNewVersionDetected"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v2}, Landroid/app/Activity;->removeDialog(I)V

    const v0, 0x7f030001

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    const v0, 0x7f07000c

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f07000d

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mScreenText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mScreenText:Landroid/widget/TextView;

    const v1, 0x7f04000a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    sget-object v0, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;->Menu_Finish:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    iput-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mMenuStatus:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    invoke-virtual {p0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    return-void
.end method

.method private onNonDialogPrompt(I)V
    .locals 3
    .param p1    # I

    const/4 v2, 0x0

    const-string v0, "GoogleOta/Client"

    const-string v1, "onNonDialogPrompt"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v2}, Landroid/app/Activity;->removeDialog(I)V

    const v0, 0x7f030001

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    const v0, 0x7f07000c

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f07000d

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mScreenText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mScreenText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    sget-object v0, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;->Menu_Finish:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    iput-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mMenuStatus:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    invoke-virtual {p0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    return-void
.end method

.method private onPkgUnzipping()V
    .locals 3

    const/16 v2, 0x8

    const-string v0, "GoogleOta/Client"

    const-string v1, "onPkgUnzipping"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->onDownloadingPkg()V

    invoke-virtual {p0, v2}, Landroid/app/Activity;->removeDialog(I)V

    invoke-virtual {p0, v2}, Landroid/app/Activity;->showDialog(I)V

    return-void
.end method

.method private onQueryNewVersion()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "GoogleOta/Client"

    const-string v1, "onQueryNewVersion"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const v0, 0x7f030001

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    const v0, 0x7f07000c

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    sget-object v0, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;->Menu_None:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    iput-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mMenuStatus:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    invoke-virtual {p0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    invoke-virtual {p0, v2}, Landroid/app/Activity;->showDialog(I)V

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mService:Lcom/mediatek/GoogleOta/GoogleOtaService;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mService:Lcom/mediatek/GoogleOta/GoogleOtaService;

    invoke-virtual {v0}, Lcom/mediatek/GoogleOta/GoogleOtaService;->setStartFlag()V

    sget-object v0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mQueryNewVersionThread:Lcom/mediatek/GoogleOta/GoogleOtaClient$SessionStateControlThread;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mQueryNewVersionThread:Lcom/mediatek/GoogleOta/GoogleOtaClient$SessionStateControlThread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "GoogleOta/Client"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onQueryNewVersion back from interrupt, mQueryNewVersionThread="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mQueryNewVersionThread:Lcom/mediatek/GoogleOta/GoogleOtaClient$SessionStateControlThread;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/mediatek/GoogleOta/GoogleOtaClient$SessionStateControlThread;

    invoke-direct {v0, p0, v2}, Lcom/mediatek/GoogleOta/GoogleOtaClient$SessionStateControlThread;-><init>(Lcom/mediatek/GoogleOta/GoogleOtaClient;I)V

    sput-object v0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mQueryNewVersionThread:Lcom/mediatek/GoogleOta/GoogleOtaClient$SessionStateControlThread;

    sget-object v0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mQueryNewVersionThread:Lcom/mediatek/GoogleOta/GoogleOtaClient$SessionStateControlThread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method private onQueryNewVersionAbort()V
    .locals 2

    const-string v0, "GoogleOta/Client"

    const-string v1, "onQueryNewVersionAbort"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mService:Lcom/mediatek/GoogleOta/GoogleOtaService;

    invoke-virtual {v0}, Lcom/mediatek/GoogleOta/GoogleOtaService;->queryNewVersionAbort()V

    return-void
.end method

.method private onQueryTypeSelect(J)V
    .locals 3
    .param p1    # J

    const-string v0, "GoogleOta/Client"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onQueryTypeSelect typeId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    long-to-int v0, p1

    iput v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mQueryType:I

    new-instance v0, Lcom/mediatek/GoogleOta/GoogleOtaClient$SessionStateControlThread;

    const/4 v1, 0x5

    invoke-direct {v0, p0, v1}, Lcom/mediatek/GoogleOta/GoogleOtaClient$SessionStateControlThread;-><init>(Lcom/mediatek/GoogleOta/GoogleOtaClient;I)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private onRequeryNeed()V
    .locals 2

    const v0, 0x7f030001

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    const v0, 0x7f07000c

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f07000d

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mScreenText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mScreenText:Landroid/widget/TextView;

    const v1, 0x7f040026

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    sget-object v0, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;->Menu_Retry:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    iput-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mMenuStatus:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    invoke-virtual {p0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    return-void
.end method

.method private onShowUpdateList([Ljava/lang/String;)V
    .locals 4
    .param p1    # [Ljava/lang/String;

    const/4 v3, 0x1

    const v0, 0x7f070010

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mUpdateRadioList:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mUpdateRadioList:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/AdapterView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mUpdateRadioList:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/AdapterView;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mUpdateRadioList:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mUpdateRadioList:Landroid/widget/ListView;

    new-instance v1, Landroid/widget/ArrayAdapter;

    const v2, 0x109000f

    invoke-direct {v1, p0, v2, p1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mUpdateRadioList:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/AbsListView;->setChoiceMode(I)V

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mUpdateRadioList:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/view/View;->setClickable(Z)V

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mUpdateRadioList:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    return-void
.end method

.method private onUnknowPrompt()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    invoke-virtual {v0}, Lcom/mediatek/GoogleOta/DownloadStatus;->getDLSessionStatus()I

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f04000c

    invoke-direct {p0, v0}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->onNonDialogPrompt(I)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->onDialogPrompt(I)V

    goto :goto_0
.end method

.method private resetDescriptionInfo()V
    .locals 3

    const-string v1, "GoogleOta/Client"

    const-string v2, "resetDescriptionInfo"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/io/File;

    invoke-static {p0}, Lcom/mediatek/GoogleOta/Util;->getPackageFileName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "GoogleOta/Client"

    const-string v2, "resetDescriptionInfo, image exist, delete it"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0}, Lcom/mediatek/GoogleOta/Util;->getPackageFileName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/mediatek/GoogleOta/Util;->deleteFile(Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->resetDownloadDesctiptor()V

    return-void
.end method

.method private resetDownloadDesctiptor()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    invoke-virtual {v1, v3}, Lcom/mediatek/GoogleOta/DownloadStatus;->setDownLoadPercent(I)V

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    invoke-virtual {v1, v2}, Lcom/mediatek/GoogleOta/DownloadStatus;->setDLSessionUnzipState(Z)V

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    invoke-virtual {v1, v2}, Lcom/mediatek/GoogleOta/DownloadStatus;->setDLSessionRenameState(Z)V

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    invoke-virtual {v1, v2}, Lcom/mediatek/GoogleOta/DownloadStatus;->setDLSessionStatus(I)V

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    invoke-virtual {v1}, Lcom/mediatek/GoogleOta/DownloadStatus;->getDownloadDescriptor()Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput v3, v0, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->pkgId:I

    const-wide/16 v1, -0x1

    iput-wide v1, v0, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->size:J

    iput-object v4, v0, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->newNote:Ljava/lang/String;

    iput-object v4, v0, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->version:Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    invoke-virtual {v1, v0}, Lcom/mediatek/GoogleOta/DownloadStatus;->setDownloadDesctiptor(Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;)V

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const-string v0, "GoogleOta/Client"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "On create enter, thread name = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Lcom/mediatek/GoogleOta/DownloadStatus;->getInstance(Landroid/content/Context;)Lcom/mediatek/GoogleOta/DownloadStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    sput-object p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mClientInstance:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    return-void
.end method

.method protected onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8
    .param p1    # I
    .param p2    # Landroid/os/Bundle;

    const v7, 0x7f04001c

    const v6, 0x1010079

    const/4 v5, 0x1

    const v4, 0x7f04001b

    const-string v1, "GoogleOta/Client"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCreateDialog id, dialog id = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v5}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    invoke-virtual {v0, v5}, Landroid/app/Dialog;->setCancelable(Z)V

    invoke-virtual {v0, v6}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    const v1, 0x7f040009

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    new-instance v1, Lcom/mediatek/GoogleOta/GoogleOtaClient$1;

    invoke-direct {v1, p0}, Lcom/mediatek/GoogleOta/GoogleOtaClient$1;-><init>(Lcom/mediatek/GoogleOta/GoogleOtaClient;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    goto :goto_0

    :pswitch_1
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mUnzipProgressDialog:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mUnzipProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v5}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mUnzipProgressDialog:Landroid/app/ProgressDialog;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setCancelable(Z)V

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mUnzipProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v6}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mUnzipProgressDialog:Landroid/app/ProgressDialog;

    const v2, 0x7f04002f

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mUnzipProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mUnzipProgressDialog:Landroid/app/ProgressDialog;

    new-instance v2, Lcom/mediatek/GoogleOta/GoogleOtaClient$2;

    invoke-direct {v2, p0}, Lcom/mediatek/GoogleOta/GoogleOtaClient$2;-><init>(Lcom/mediatek/GoogleOta/GoogleOtaClient;)V

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mUnzipProgressDialog:Landroid/app/ProgressDialog;

    new-instance v2, Lcom/mediatek/GoogleOta/GoogleOtaClient$3;

    invoke-direct {v2, p0}, Lcom/mediatek/GoogleOta/GoogleOtaClient$3;-><init>(Lcom/mediatek/GoogleOta/GoogleOtaClient;)V

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mUnzipProgressDialog:Landroid/app/ProgressDialog;

    goto :goto_0

    :pswitch_2
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f04000d

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f04000e

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/mediatek/GoogleOta/GoogleOtaClient$5;

    invoke-direct {v2, p0}, Lcom/mediatek/GoogleOta/GoogleOtaClient$5;-><init>(Lcom/mediatek/GoogleOta/GoogleOtaClient;)V

    invoke-virtual {v1, v4, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/mediatek/GoogleOta/GoogleOtaClient$4;

    invoke-direct {v2, p0}, Lcom/mediatek/GoogleOta/GoogleOtaClient$4;-><init>(Lcom/mediatek/GoogleOta/GoogleOtaClient;)V

    invoke-virtual {v1, v7, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mAlertDialog:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mAlertDialog:Landroid/app/AlertDialog;

    goto/16 :goto_0

    :pswitch_3
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f04000f

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f040010

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/mediatek/GoogleOta/GoogleOtaClient$7;

    invoke-direct {v2, p0}, Lcom/mediatek/GoogleOta/GoogleOtaClient$7;-><init>(Lcom/mediatek/GoogleOta/GoogleOtaClient;)V

    invoke-virtual {v1, v4, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/mediatek/GoogleOta/GoogleOtaClient$6;

    invoke-direct {v2, p0}, Lcom/mediatek/GoogleOta/GoogleOtaClient$6;-><init>(Lcom/mediatek/GoogleOta/GoogleOtaClient;)V

    invoke-virtual {v1, v7, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mAlertDialog:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mAlertDialog:Landroid/app/AlertDialog;

    goto/16 :goto_0

    :pswitch_4
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f040011

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f040016

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/mediatek/GoogleOta/GoogleOtaClient$8;

    invoke-direct {v2, p0}, Lcom/mediatek/GoogleOta/GoogleOtaClient$8;-><init>(Lcom/mediatek/GoogleOta/GoogleOtaClient;)V

    invoke-virtual {v1, v4, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDownloadStorageDialog:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDownloadStorageDialog:Landroid/app/AlertDialog;

    goto/16 :goto_0

    :pswitch_5
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f040013

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f040015

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/mediatek/GoogleOta/GoogleOtaClient$9;

    invoke-direct {v2, p0}, Lcom/mediatek/GoogleOta/GoogleOtaClient$9;-><init>(Lcom/mediatek/GoogleOta/GoogleOtaClient;)V

    invoke-virtual {v1, v4, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDownloadStorageDialog:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDownloadStorageDialog:Landroid/app/AlertDialog;

    goto/16 :goto_0

    :pswitch_6
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f04000b

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f04000c

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/mediatek/GoogleOta/GoogleOtaClient$10;

    invoke-direct {v2, p0}, Lcom/mediatek/GoogleOta/GoogleOtaClient$10;-><init>(Lcom/mediatek/GoogleOta/GoogleOtaClient;)V

    invoke-virtual {v1, v4, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mAlertDialog:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mAlertDialog:Landroid/app/AlertDialog;

    goto/16 :goto_0

    :pswitch_7
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f04001f

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f040020

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f040021

    new-instance v3, Lcom/mediatek/GoogleOta/GoogleOtaClient$12;

    invoke-direct {v3, p0}, Lcom/mediatek/GoogleOta/GoogleOtaClient$12;-><init>(Lcom/mediatek/GoogleOta/GoogleOtaClient;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f040022

    new-instance v3, Lcom/mediatek/GoogleOta/GoogleOtaClient$11;

    invoke-direct {v3, p0}, Lcom/mediatek/GoogleOta/GoogleOtaClient$11;-><init>(Lcom/mediatek/GoogleOta/GoogleOtaClient;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDownloadCancelDialog:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDownloadCancelDialog:Landroid/app/AlertDialog;

    goto/16 :goto_0

    :pswitch_8
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget v2, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mOTADialogTitleResId:I

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mOTADialogMessageResId:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/mediatek/GoogleOta/GoogleOtaClient$14;

    invoke-direct {v2, p0}, Lcom/mediatek/GoogleOta/GoogleOtaClient$14;-><init>(Lcom/mediatek/GoogleOta/GoogleOtaClient;)V

    invoke-virtual {v1, v4, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/mediatek/GoogleOta/GoogleOtaClient$13;

    invoke-direct {v2, p0}, Lcom/mediatek/GoogleOta/GoogleOtaClient$13;-><init>(Lcom/mediatek/GoogleOta/GoogleOtaClient;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mAlertDialog:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mAlertDialog:Landroid/app/AlertDialog;

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_7
        :pswitch_4
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_8
        :pswitch_1
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 7
    .param p1    # Landroid/view/Menu;

    const v6, 0x7f04001c

    const v5, 0x7f04001b

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mMenuStatus:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    sget-object v1, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;->Menu_Download:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    const v1, 0x7f04001a

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setShowAsAction(I)V

    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0

    :cond_1
    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mMenuStatus:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    sget-object v1, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;->Menu_Downloading:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    if-ne v0, v1, :cond_2

    const v0, 0x7f040018

    invoke-interface {p1, v2, v3, v2, v0}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setShowAsAction(I)V

    invoke-interface {p1, v2, v4, v2, v6}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setShowAsAction(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mMenuStatus:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    sget-object v1, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;->Menu_Finish:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    if-ne v0, v1, :cond_3

    const/4 v0, 0x5

    invoke-interface {p1, v2, v0, v2, v5}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setShowAsAction(I)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mMenuStatus:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    sget-object v1, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;->Menu_Retry:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    if-ne v0, v1, :cond_4

    const/4 v0, 0x6

    const v1, 0x7f040017

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setShowAsAction(I)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mMenuStatus:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    sget-object v1, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;->Menu_Upgrade:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    if-ne v0, v1, :cond_5

    const/4 v0, 0x7

    invoke-interface {p1, v2, v0, v2, v5}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setShowAsAction(I)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mMenuStatus:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    sget-object v1, Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;->Menu_Resume:Lcom/mediatek/GoogleOta/GoogleOtaClient$MenuStatus;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x4

    const v1, 0x7f040019

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setShowAsAction(I)V

    invoke-interface {p1, v2, v4, v2, v6}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setShowAsAction(I)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "GoogleOta/Client"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-object v2, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    sput-object v2, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mClientInstance:Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method onDownloadCancel()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, -0x1

    const-string v1, "GoogleOta/Client"

    const-string v2, "onDownloadCancel"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0}, Lcom/mediatek/GoogleOta/Util;->getPackageFileName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/mediatek/GoogleOta/Util;->deleteFile(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/mediatek/GoogleOta/DownloadStatus;->setDLSessionStatus(I)V

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    invoke-virtual {v1}, Lcom/mediatek/GoogleOta/DownloadStatus;->getDownloadDescriptor()Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    move-result-object v0

    iput v3, v0, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->pkgId:I

    iput-object v4, v0, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->newNote:Ljava/lang/String;

    const-wide/16 v1, -0x1

    iput-wide v1, v0, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->size:J

    iput-object v4, v0, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->version:Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    invoke-virtual {v1, v0}, Lcom/mediatek/GoogleOta/DownloadStatus;->setDownloadDesctiptor(Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;)V

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    invoke-virtual {v1, v3}, Lcom/mediatek/GoogleOta/DownloadStatus;->setDownLoadPercent(I)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 8
    .param p1    # Landroid/view/MenuItem;

    const/4 v3, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v3

    :goto_0
    return v3

    :pswitch_0
    iget-object v4, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    invoke-virtual {v4}, Lcom/mediatek/GoogleOta/DownloadStatus;->getDownloadDescriptor()Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    move-result-object v0

    invoke-static {p0}, Lcom/mediatek/GoogleOta/Util;->isSdcardAvailable(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_0

    const v4, 0x7f040015

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_0
    iget-object v4, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    invoke-virtual {v4}, Lcom/mediatek/GoogleOta/DownloadStatus;->getDLSessionStatus()I

    move-result v4

    if-ne v4, v3, :cond_1

    invoke-static {p0}, Lcom/mediatek/GoogleOta/Util;->getPackageFileName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/mediatek/GoogleOta/Util;->deleteFile(Ljava/lang/String;)V

    :cond_1
    iget-wide v4, v0, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->size:J

    long-to-double v4, v4

    const-wide/high16 v6, 0x4004000000000000L

    mul-double/2addr v4, v6

    double-to-long v4, v4

    invoke-static {p0, v4, v5}, Lcom/mediatek/GoogleOta/Util;->checkSdcardSpaceNeeded(Landroid/content/Context;J)J

    move-result-wide v1

    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_2

    const v4, 0x7f040014

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    const-wide/16 v4, 0x0

    cmp-long v4, v1, v4

    if-lez v4, :cond_3

    const v4, 0x7f040012

    new-array v5, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_3
    new-instance v4, Lcom/mediatek/GoogleOta/GoogleOtaClient$SessionStateControlThread;

    const/4 v5, 0x2

    invoke-direct {v4, p0, v5}, Lcom/mediatek/GoogleOta/GoogleOtaClient$SessionStateControlThread;-><init>(Lcom/mediatek/GoogleOta/GoogleOtaClient;I)V

    iput-object v4, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDlPkgProgressThread:Lcom/mediatek/GoogleOta/GoogleOtaClient$SessionStateControlThread;

    iget-object v4, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDlPkgProgressThread:Lcom/mediatek/GoogleOta/GoogleOtaClient$SessionStateControlThread;

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->onDownloadingPkg()V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, v3}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->onDlPkgPaused(Z)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->onDlPkgCanceled()V

    goto/16 :goto_0

    :pswitch_3
    invoke-direct {p0}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->onDlPkgResume()V

    goto/16 :goto_0

    :pswitch_4
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    :pswitch_5
    invoke-direct {p0}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->onQueryNewVersion()V

    goto/16 :goto_0

    :pswitch_6
    iget-object v4, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mUpdateRadioList:Landroid/widget/ListView;

    invoke-virtual {v4}, Landroid/widget/AbsListView;->getCheckedItemPosition()I

    move-result v4

    int-to-long v4, v4

    invoke-direct {p0, v4, v5}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->onQueryTypeSelect(J)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V
    .locals 1
    .param p1    # I
    .param p2    # Landroid/app/Dialog;
    .param p3    # Landroid/os/Bundle;

    sparse-switch p1, :sswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onPrepareDialog(ILandroid/app/Dialog;)V

    return-void

    :sswitch_0
    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mUnzipProgressDialog:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    move-object v0, p2

    check-cast v0, Landroid/app/ProgressDialog;

    iput-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mUnzipProgressDialog:Landroid/app/ProgressDialog;

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDownloadCancelDialog:Landroid/app/AlertDialog;

    if-nez v0, :cond_0

    move-object v0, p2

    check-cast v0, Landroid/app/AlertDialog;

    iput-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mDownloadCancelDialog:Landroid/app/AlertDialog;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x8 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    const-string v0, "otaresult_dlg_title"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mOTADialogTitleResId:I

    const-string v0, "otaresult_dlg_msg"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mOTADialogMessageResId:I

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const-string v0, "otaresult_dlg_title"

    iget v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mOTADialogTitleResId:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "otaresult_dlg_msg"

    iget v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mOTADialogMessageResId:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method protected onStart()V
    .locals 3

    const-string v1, "GoogleOta/Client"

    const-string v2, "onStart"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    sput-boolean v1, Lcom/mediatek/GoogleOta/GoogleOtaClient;->isBg:Z

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mediatek/GoogleOta/GoogleOtaService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mConnection:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Landroid/content/ContextWrapper;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    return-void
.end method

.method protected onStop()V
    .locals 2

    const-string v0, "GoogleOta/Client"

    const-string v1, "onStop"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    sput-boolean v0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->isBg:Z

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mService:Lcom/mediatek/GoogleOta/GoogleOtaService;

    invoke-virtual {v0}, Lcom/mediatek/GoogleOta/GoogleOtaService;->runningBg()V

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->onDoClearAction()V

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaClient;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unbindService(Landroid/content/ServiceConnection;)V

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method
