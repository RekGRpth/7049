.class public Lcom/mediatek/GoogleOta/GoogleOtaReceiver;
.super Landroid/content/BroadcastReceiver;
.source "GoogleOtaReceiver.java"


# static fields
.field private static final ACTION_AUTO_QUERY_NEWVERSION:Ljava/lang/String; = "com.mediatek.GoogleOta.AUTO_QUERY_NEWVERSION"

.field private static final ACTION_BOOT_COMPLETED:Ljava/lang/String; = "android.intent.action.BOOT_COMPLETED"

.field private static final ACTION_CONNECTIVITY_CHANGE:Ljava/lang/String; = "android.net.conn.CONNECTIVITY_CHANGE"

.field private static final ACTION_REPORT_ACTIVITY:Ljava/lang/String; = "com.mediatek.intent.GoogleOtaReport"

.field static final ACTION_UPDATE_REMIND:Ljava/lang/String; = "com.mediatek.GoogleOta.UPDATE_REMIND"

.field private static final ACTION_UPDATE_REPORT:Ljava/lang/String; = "com.mediatek.GoogleOta.UPDATE_REPORT"

.field private static final NEWVERSION_AUTO_QUERY_DAY_OF_WEEK1:I = 0x2

.field private static final NEWVERSION_AUTO_QUERY_DAY_OF_WEEK2:I = 0x5

.field private static final QUERY_TIME_DELAY:I = 0x7530

.field private static final QUERY_TIME_HOUR:I = 0xc

.field private static final SHOW_RESULT_TIME_DELAY:I = 0xbb8

.field private static final TAG:Ljava/lang/String; = "GoogleOta/Receiver"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private getFullPathResultFileName()Ljava/lang/String;
    .locals 4

    iget-object v2, p0, Lcom/mediatek/GoogleOta/GoogleOtaReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/GoogleOtaReceiver;->getResultFileName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private getNextAlarmTime()J
    .locals 11

    const/16 v10, 0xc

    const/4 v9, 0x5

    const/4 v8, 0x2

    const/4 v7, 0x0

    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v4, "yyyy-MM-dd HH:mm:ss"

    invoke-direct {v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const-string v4, "GoogleOta/Receiver"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "current time:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x7

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v1

    const/4 v2, 0x0

    if-ge v1, v8, :cond_0

    rsub-int/lit8 v2, v1, 0x2

    :goto_0
    invoke-virtual {v0, v9, v2}, Ljava/util/Calendar;->add(II)V

    const/16 v4, 0xb

    invoke-virtual {v0, v4, v10}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v0, v10, v7}, Ljava/util/Calendar;->set(II)V

    const/16 v4, 0xd

    invoke-virtual {v0, v4, v7}, Ljava/util/Calendar;->set(II)V

    const-string v4, "GoogleOta/Receiver"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "alarm time:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    return-wide v4

    :cond_0
    if-lt v1, v8, :cond_1

    if-ge v1, v9, :cond_1

    rsub-int/lit8 v2, v1, 0x5

    goto :goto_0

    :cond_1
    rsub-int/lit8 v2, v1, 0x9

    goto :goto_0
.end method

.method private getResultFileName()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f040004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getUpdateResult()Z
    .locals 6

    const/4 v3, 0x0

    const-string v4, "GoogleOta/Receiver"

    const-string v5, "getUpdateResult"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    const-string v4, "GoogleOtaBinder"

    invoke-static {v4}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/mediatek/GoogleOta/GoogleOtaBinder$Stub;->asInterface(Landroid/os/IBinder;)Lcom/mediatek/GoogleOta/GoogleOtaBinder;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v4, "GoogleOta/Receiver"

    const-string v5, "Agent is null."

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v3

    :cond_0
    invoke-interface {v0}, Lcom/mediatek/GoogleOta/GoogleOtaBinder;->readUpgradeResult()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method private getUpdateStatus()Z
    .locals 6

    const-string v3, "GoogleOta/Receiver"

    const-string v4, "getUpdateStatus"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/mediatek/GoogleOta/GoogleOtaReceiver;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/mediatek/GoogleOta/DownloadStatus;->getInstance(Landroid/content/Context;)Lcom/mediatek/GoogleOta/DownloadStatus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/GoogleOta/DownloadStatus;->getUpgradeStartedState()Z

    move-result v2

    const-string v3, "GoogleOta/Receiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "upgradeStarted: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v2, :cond_0

    const-string v3, "GoogleOta/Receiver"

    const-string v4, "getUpdateStatus, update finished"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Ljava/io/File;

    iget-object v3, p0, Lcom/mediatek/GoogleOta/GoogleOtaReceiver;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/mediatek/GoogleOta/Util;->getPackageFileName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/GoogleOta/GoogleOtaReceiver;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/mediatek/GoogleOta/Util;->getPackageFileName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/mediatek/GoogleOta/Util;->deleteFile(Ljava/lang/String;)V

    :cond_0
    return v2
.end method

.method private hasQueryThisDay(Ljava/lang/String;)Z
    .locals 3
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy-MM-dd"

    invoke-direct {v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    return v2
.end method

.method private resetDescriptionInfo(Lcom/mediatek/GoogleOta/DownloadStatus;)V
    .locals 6
    .param p1    # Lcom/mediatek/GoogleOta/DownloadStatus;

    const/4 v5, 0x0

    const/4 v4, -0x1

    const-string v2, "GoogleOta/Receiver"

    const-string v3, "resetDescriptionInfo"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/mediatek/GoogleOta/GoogleOtaReceiver;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/mediatek/GoogleOta/Util;->getPackageFileName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "GoogleOta/Receiver"

    const-string v3, "resetDescriptionInfo, image exist, delete it"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/GoogleOta/GoogleOtaReceiver;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/mediatek/GoogleOta/Util;->getPackageFileName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/mediatek/GoogleOta/Util;->deleteFile(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1, v4}, Lcom/mediatek/GoogleOta/DownloadStatus;->setDownLoadPercent(I)V

    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Lcom/mediatek/GoogleOta/DownloadStatus;->setDLSessionStatus(I)V

    invoke-virtual {p1}, Lcom/mediatek/GoogleOta/DownloadStatus;->getDownloadDescriptor()Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    move-result-object v0

    iput v4, v0, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->pkgId:I

    const-wide/16 v2, -0x1

    iput-wide v2, v0, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->size:J

    iput-object v5, v0, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->newNote:Ljava/lang/String;

    iput-object v5, v0, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->version:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/mediatek/GoogleOta/DownloadStatus;->setDownloadDesctiptor(Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;)V

    return-void
.end method

.method private resetUpdateResult()V
    .locals 3

    const-string v1, "GoogleOta/Receiver"

    const-string v2, "deleteUpdateResult"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaReceiver;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/mediatek/GoogleOta/DownloadStatus;->getInstance(Landroid/content/Context;)Lcom/mediatek/GoogleOta/DownloadStatus;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/mediatek/GoogleOta/DownloadStatus;->setUpgradeStartedState(Z)V

    return-void
.end method

.method private startReportActivity()V
    .locals 5

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/GoogleOtaReceiver;->getUpdateResult()Z

    move-result v1

    const-string v2, "GoogleOta/Receiver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getUpdateResult="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v2, "ERRORCODE"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/high16 v2, 0x10000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v2, "com.mediatek.intent.GoogleOtaReport"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/mediatek/GoogleOta/GoogleOtaReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/GoogleOtaReceiver;->resetUpdateResult()V

    return-void
.end method


# virtual methods
.method getQueryRequest()Z
    .locals 4

    const/4 v3, 0x7

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-eq v1, v2, :cond_0

    const/4 v1, 0x5

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-ne v1, v2, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 14
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    iput-object p1, p0, Lcom/mediatek/GoogleOta/GoogleOtaReceiver;->mContext:Landroid/content/Context;

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v9, "GoogleOta/Receiver"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "onReceive: action = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v9, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-static {p1}, Lcom/mediatek/GoogleOta/Util;->isNetWorkAvailable(Landroid/content/Context;)Z

    move-result v9

    if-nez v9, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p1}, Lcom/mediatek/GoogleOta/DownloadStatus;->getInstance(Landroid/content/Context;)Lcom/mediatek/GoogleOta/DownloadStatus;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/GoogleOta/DownloadStatus;->getQueryDate()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-direct {p0, v6}, Lcom/mediatek/GoogleOta/GoogleOtaReceiver;->hasQueryThisDay(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_0

    :cond_2
    invoke-virtual {v1}, Lcom/mediatek/GoogleOta/DownloadStatus;->getDLSessionStatus()I

    move-result v5

    if-nez v5, :cond_3

    invoke-virtual {p0}, Lcom/mediatek/GoogleOta/GoogleOtaReceiver;->getQueryRequest()Z

    move-result v9

    if-eqz v9, :cond_3

    const-string v9, "GoogleOta/Receiver"

    const-string v10, "query new version"

    invoke-static {v9, v10}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v9, 0x1

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v10

    const-wide/16 v12, 0x7530

    add-long/2addr v10, v12

    const-string v12, "com.mediatek.GoogleOta.AUTO_QUERY_NEWVERSION"

    invoke-static {p1, v9, v10, v11, v12}, Lcom/mediatek/GoogleOta/Util;->serAlarm(Landroid/content/Context;IJLjava/lang/String;)V

    goto :goto_0

    :cond_3
    const/4 v9, 0x1

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/GoogleOtaReceiver;->getNextAlarmTime()J

    move-result-wide v10

    const-string v12, "com.mediatek.GoogleOta.AUTO_QUERY_NEWVERSION"

    invoke-static {p1, v9, v10, v11, v12}, Lcom/mediatek/GoogleOta/Util;->serAlarm(Landroid/content/Context;IJLjava/lang/String;)V

    const-string v9, "GoogleOta/Receiver"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "status = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ". Need not query, set next query alarm"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    const-string v9, "com.mediatek.GoogleOta.AUTO_QUERY_NEWVERSION"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_7

    invoke-static {p1}, Lcom/mediatek/GoogleOta/Util;->isNetWorkAvailable(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-static {p1}, Lcom/mediatek/GoogleOta/DownloadStatus;->getInstance(Landroid/content/Context;)Lcom/mediatek/GoogleOta/DownloadStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/GoogleOta/DownloadStatus;->getDLSessionStatus()I

    move-result v5

    const-string v9, "GoogleOta/Receiver"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "status = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/GoogleOtaReceiver;->getUpdateStatus()Z

    move-result v7

    const/4 v9, 0x5

    if-ne v5, v9, :cond_5

    if-eqz v7, :cond_5

    invoke-direct {p0, v2}, Lcom/mediatek/GoogleOta/GoogleOtaReceiver;->resetDescriptionInfo(Lcom/mediatek/GoogleOta/DownloadStatus;)V

    const/4 v5, 0x0

    const-string v9, "GoogleOta/Receiver"

    const-string v10, "update complete"

    invoke-static {v9, v10}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    if-nez v5, :cond_6

    invoke-virtual {p0}, Lcom/mediatek/GoogleOta/GoogleOtaReceiver;->getQueryRequest()Z

    move-result v9

    if-eqz v9, :cond_6

    new-instance v9, Landroid/content/Intent;

    const-class v10, Lcom/mediatek/GoogleOta/GoogleOtaService;

    invoke-direct {v9, p1, v10}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v9}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0

    :cond_6
    const/4 v9, 0x1

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/GoogleOtaReceiver;->getNextAlarmTime()J

    move-result-wide v10

    const-string v12, "com.mediatek.GoogleOta.AUTO_QUERY_NEWVERSION"

    invoke-static {p1, v9, v10, v11, v12}, Lcom/mediatek/GoogleOta/Util;->serAlarm(Landroid/content/Context;IJLjava/lang/String;)V

    const-string v9, "GoogleOta/Receiver"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "status = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ". Need not query, set next query alarm"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_7
    const-string v9, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_c

    invoke-static {p1}, Lcom/mediatek/GoogleOta/DownloadStatus;->getInstance(Landroid/content/Context;)Lcom/mediatek/GoogleOta/DownloadStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/GoogleOta/DownloadStatus;->getDLSessionStatus()I

    move-result v5

    const-string v9, "GoogleOta/Receiver"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "status = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/GoogleOtaReceiver;->getUpdateStatus()Z

    move-result v7

    const/4 v9, 0x2

    if-ne v5, v9, :cond_8

    const/4 v9, 0x4

    invoke-virtual {v2, v9}, Lcom/mediatek/GoogleOta/DownloadStatus;->setDLSessionStatus(I)V

    const-string v9, "GoogleOta/Receiver"

    const-string v10, "Abnormal stop during downloading. Re-query version"

    invoke-static {v9, v10}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    const/4 v9, 0x5

    if-ne v5, v9, :cond_9

    if-eqz v7, :cond_9

    const/4 v9, 0x1

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v10

    const-wide/16 v12, 0xbb8

    add-long/2addr v10, v12

    const-string v12, "com.mediatek.GoogleOta.UPDATE_REPORT"

    invoke-static {p1, v9, v10, v11, v12}, Lcom/mediatek/GoogleOta/Util;->serAlarm(Landroid/content/Context;IJLjava/lang/String;)V

    invoke-direct {p0, v2}, Lcom/mediatek/GoogleOta/GoogleOtaReceiver;->resetDescriptionInfo(Lcom/mediatek/GoogleOta/DownloadStatus;)V

    const/4 v5, 0x0

    const-string v9, "GoogleOta/Receiver"

    const-string v10, "ACTION_BOOT_COMPLETED, update complete"

    invoke-static {v9, v10}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    invoke-virtual {v2}, Lcom/mediatek/GoogleOta/DownloadStatus;->getQueryDate()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_a

    invoke-direct {p0, v6}, Lcom/mediatek/GoogleOta/GoogleOtaReceiver;->hasQueryThisDay(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_0

    :cond_a
    if-nez v5, :cond_b

    invoke-virtual {p0}, Lcom/mediatek/GoogleOta/GoogleOtaReceiver;->getQueryRequest()Z

    move-result v9

    if-eqz v9, :cond_b

    const-string v9, "GoogleOta/Receiver"

    const-string v10, "ACTION_BOOT_COMPLETED: query day to query new version"

    invoke-static {v9, v10}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v9, 0x1

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v10

    const-wide/16 v12, 0x7530

    add-long/2addr v10, v12

    const-string v12, "com.mediatek.GoogleOta.AUTO_QUERY_NEWVERSION"

    invoke-static {p1, v9, v10, v11, v12}, Lcom/mediatek/GoogleOta/Util;->serAlarm(Landroid/content/Context;IJLjava/lang/String;)V

    goto/16 :goto_0

    :cond_b
    const/4 v9, 0x1

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/GoogleOtaReceiver;->getNextAlarmTime()J

    move-result-wide v10

    const-string v12, "com.mediatek.GoogleOta.AUTO_QUERY_NEWVERSION"

    invoke-static {p1, v9, v10, v11, v12}, Lcom/mediatek/GoogleOta/Util;->serAlarm(Landroid/content/Context;IJLjava/lang/String;)V

    const-string v9, "GoogleOta/Receiver"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "ACTION_BOOT_COMPLETED, status = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", need not query, set next query alarm"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_c
    const-string v9, "com.mediatek.GoogleOta.UPDATE_REMIND"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_d

    const-string v9, "GoogleOta/Receiver"

    const-string v10, "ACTION_UPDATE_REMIND"

    invoke-static {v9, v10}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v4, Lcom/mediatek/GoogleOta/NotifyManager;

    invoke-direct {v4, p1}, Lcom/mediatek/GoogleOta/NotifyManager;-><init>(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/mediatek/GoogleOta/DownloadStatus;->getInstance(Landroid/content/Context;)Lcom/mediatek/GoogleOta/DownloadStatus;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/GoogleOta/DownloadStatus;->getVersion()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Lcom/mediatek/GoogleOta/NotifyManager;->showDownloadCompletedNotification(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_d
    const-string v9, "com.mediatek.GoogleOta.UPDATE_REPORT"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    const-string v9, "GoogleOta/Receiver"

    const-string v10, "ACTION_UPDATE_REPORT"

    invoke-static {v9, v10}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/GoogleOtaReceiver;->startReportActivity()V

    goto/16 :goto_0
.end method
