.class public Lcom/mediatek/GoogleOta/GoogleOtaService;
.super Landroid/app/Service;
.source "GoogleOtaService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/GoogleOta/GoogleOtaService$ServiceBinder;
    }
.end annotation


# static fields
.field private static final SECOND_PER_MINUTE:I = 0xea60

.field private static final STATUS_IDLE:I = 0x0

.field private static final STATUS_QUERY:I = 0x1

.field private static final TAG:Ljava/lang/String; = "GoogleOta/Service"

.field private static final TIME_SECTION_NUM:I = 0x3

.field private static mServiceInstance:Lcom/mediatek/GoogleOta/GoogleOtaService;


# instance fields
.field private httpManager:Lcom/mediatek/GoogleOta/HttpManager;

.field private mBinder:Lcom/mediatek/GoogleOta/GoogleOtaService$ServiceBinder;

.field private mHandler:Landroid/os/Handler;

.field private mStatus:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/mediatek/GoogleOta/GoogleOtaService;->mServiceInstance:Lcom/mediatek/GoogleOta/GoogleOtaService;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaService;->mStatus:I

    new-instance v0, Lcom/mediatek/GoogleOta/GoogleOtaService$ServiceBinder;

    invoke-direct {v0, p0}, Lcom/mediatek/GoogleOta/GoogleOtaService$ServiceBinder;-><init>(Lcom/mediatek/GoogleOta/GoogleOtaService;)V

    iput-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaService;->mBinder:Lcom/mediatek/GoogleOta/GoogleOtaService$ServiceBinder;

    return-void
.end method

.method static getInstance()Lcom/mediatek/GoogleOta/GoogleOtaService;
    .locals 1

    sget-object v0, Lcom/mediatek/GoogleOta/GoogleOtaService;->mServiceInstance:Lcom/mediatek/GoogleOta/GoogleOtaService;

    return-object v0
.end method


# virtual methods
.method public cancelDlPkg()V
    .locals 0

    invoke-virtual {p0}, Lcom/mediatek/GoogleOta/GoogleOtaService;->onStop()V

    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1    # Landroid/content/Intent;

    const-string v0, "GoogleOta/Service"

    const-string v1, "onBind"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->getInstance()Lcom/mediatek/GoogleOta/GoogleOtaClient;

    invoke-static {}, Lcom/mediatek/GoogleOta/GoogleOtaClient;->getHandler()Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaService;->mHandler:Landroid/os/Handler;

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaService;->httpManager:Lcom/mediatek/GoogleOta/HttpManager;

    iget-object v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/mediatek/GoogleOta/HttpManager;->onSetMessageHandler(Landroid/os/Handler;)V

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaService;->mBinder:Lcom/mediatek/GoogleOta/GoogleOtaService$ServiceBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    const-string v0, "GoogleOta/Service"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate, thread name = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    invoke-static {p0}, Lcom/mediatek/GoogleOta/HttpManager;->getInstance(Landroid/content/Context;)Lcom/mediatek/GoogleOta/HttpManager;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaService;->httpManager:Lcom/mediatek/GoogleOta/HttpManager;

    sput-object p0, Lcom/mediatek/GoogleOta/GoogleOtaService;->mServiceInstance:Lcom/mediatek/GoogleOta/GoogleOtaService;

    return-void
.end method

.method public onDestroy()V
    .locals 2

    const-string v0, "GoogleOta/Service"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaService;->mStatus:I

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method

.method onParseTime(Ljava/lang/String;)J
    .locals 4
    .param p1    # Ljava/lang/String;

    const-string v1, "GoogleOta/Service"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onParseTime string = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "/"

    const/4 v2, 0x3

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v1, v0, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x1

    aget-object v2, v0, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    mul-int/2addr v1, v2

    const/4 v2, 0x2

    aget-object v2, v0, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    mul-int/2addr v1, v2

    const v2, 0xea60

    mul-int/2addr v1, v2

    int-to-long v1, v1

    return-wide v1
.end method

.method public onRebind(Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Intent;

    const-string v0, "GoogleOta/Service"

    const-string v1, "onRebind"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Landroid/app/Service;->onRebind(Landroid/content/Intent;)V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    const/4 v3, 0x1

    const-string v0, "GoogleOta/Service"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStartCommand, mStatus = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/GoogleOta/GoogleOtaService;->mStatus:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaService;->mStatus:I

    if-eq v0, v3, :cond_0

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/mediatek/GoogleOta/GoogleOtaService$1;

    invoke-direct {v1, p0}, Lcom/mediatek/GoogleOta/GoogleOtaService$1;-><init>(Lcom/mediatek/GoogleOta/GoogleOtaService;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_0
    return v3
.end method

.method onStop()V
    .locals 2

    const-string v0, "GoogleOta/Service"

    const-string v1, "onStop"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Service;->stopSelf()V

    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 2
    .param p1    # Landroid/content/Intent;

    const-string v0, "GoogleOta/Service"

    const-string v1, "onUnbind"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method public pauseDlPkg()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaService;->httpManager:Lcom/mediatek/GoogleOta/HttpManager;

    invoke-virtual {v0}, Lcom/mediatek/GoogleOta/HttpManager;->onDownloadPause()V

    return-void
.end method

.method public queryNewVersion()V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaService;->mStatus:I

    if-eq v0, v1, :cond_0

    iput v1, p0, Lcom/mediatek/GoogleOta/GoogleOtaService;->mStatus:I

    const-string v0, "GoogleOta/Service"

    const-string v1, "queryNewVersion, mQueryAbort = false"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaService;->httpManager:Lcom/mediatek/GoogleOta/HttpManager;

    invoke-virtual {v0, v2}, Lcom/mediatek/GoogleOta/HttpManager;->onSetAbort(Z)V

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaService;->httpManager:Lcom/mediatek/GoogleOta/HttpManager;

    invoke-virtual {v0}, Lcom/mediatek/GoogleOta/HttpManager;->onQueryNewVersion()V

    const-string v0, "GoogleOta/Service"

    const-string v1, "queryNewVersion, done"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput v2, p0, Lcom/mediatek/GoogleOta/GoogleOtaService;->mStatus:I

    :cond_0
    return-void
.end method

.method public queryNewVersionAbort()V
    .locals 2

    invoke-virtual {p0}, Lcom/mediatek/GoogleOta/GoogleOtaService;->onStop()V

    const-string v0, "GoogleOta/Service"

    const-string v1, "queryNewVersionAbort, mQueryAbort = true"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaService;->httpManager:Lcom/mediatek/GoogleOta/HttpManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/mediatek/GoogleOta/HttpManager;->onSetAbort(Z)V

    return-void
.end method

.method public resetDescriptionInfo()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaService;->httpManager:Lcom/mediatek/GoogleOta/HttpManager;

    invoke-virtual {v0}, Lcom/mediatek/GoogleOta/HttpManager;->resetDescriptionInfo()V

    return-void
.end method

.method public runningBg()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaService;->httpManager:Lcom/mediatek/GoogleOta/HttpManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/mediatek/GoogleOta/HttpManager;->onSetMessageHandler(Landroid/os/Handler;)V

    return-void
.end method

.method public setStartFlag()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaService;->httpManager:Lcom/mediatek/GoogleOta/HttpManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/mediatek/GoogleOta/HttpManager;->onSetAbort(Z)V

    return-void
.end method

.method public setUpdateType(I)V
    .locals 13
    .param p1    # I

    const-string v9, "GoogleOta/Service"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "setUpdateTypetype type = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_2

    iget-object v9, p0, Lcom/mediatek/GoogleOta/GoogleOtaService;->httpManager:Lcom/mediatek/GoogleOta/HttpManager;

    invoke-virtual {v9}, Lcom/mediatek/GoogleOta/HttpManager;->isDeltaPackageOk()Z

    move-result v7

    if-nez v7, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v9, p0, Lcom/mediatek/GoogleOta/GoogleOtaService;->httpManager:Lcom/mediatek/GoogleOta/HttpManager;

    invoke-virtual {v9}, Lcom/mediatek/GoogleOta/HttpManager;->onSetRebootRecoveryFlag()Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v4, Landroid/content/Intent;

    const-string v9, "android.intent.action.GoogleOta.RebootService"

    invoke-direct {v4, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Landroid/content/ContextWrapper;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f050001

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v8

    array-length v9, v8

    add-int/lit8 v9, v9, -0x1

    if-ne p1, v9, :cond_3

    invoke-virtual {p0}, Lcom/mediatek/GoogleOta/GoogleOtaService;->onStop()V

    iget-object v9, p0, Lcom/mediatek/GoogleOta/GoogleOtaService;->httpManager:Lcom/mediatek/GoogleOta/HttpManager;

    invoke-virtual {v9}, Lcom/mediatek/GoogleOta/HttpManager;->onSendCloseClientMessage()V

    goto :goto_0

    :cond_3
    aget-object v9, v8, p1

    invoke-virtual {p0, v9}, Lcom/mediatek/GoogleOta/GoogleOtaService;->onParseTime(Ljava/lang/String;)J

    move-result-wide v5

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    const/4 v9, 0x1

    add-long v10, v1, v5

    const-string v12, "com.mediatek.GoogleOta.UPDATE_REMIND"

    invoke-static {p0, v9, v10, v11, v12}, Lcom/mediatek/GoogleOta/Util;->serAlarm(Landroid/content/Context;IJLjava/lang/String;)V

    invoke-virtual {p0}, Lcom/mediatek/GoogleOta/GoogleOtaService;->onStop()V

    iget-object v9, p0, Lcom/mediatek/GoogleOta/GoogleOtaService;->httpManager:Lcom/mediatek/GoogleOta/HttpManager;

    invoke-virtual {v9}, Lcom/mediatek/GoogleOta/HttpManager;->onSendCloseClientMessage()V

    goto :goto_0
.end method

.method public startDlPkg()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/GoogleOta/GoogleOtaService;->httpManager:Lcom/mediatek/GoogleOta/HttpManager;

    invoke-virtual {v0}, Lcom/mediatek/GoogleOta/HttpManager;->onDownloadImage()V

    return-void
.end method
