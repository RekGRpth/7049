.class Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;
.super Ljava/lang/Object;
.source "HttpManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/GoogleOta/HttpManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HttpResponseContent"
.end annotation


# instance fields
.field fileSize:J

.field fingerprint:Ljava/lang/String;

.field isFullPkg:Z

.field pkgId:I

.field rand:I

.field releaseNote:Ljava/lang/String;

.field sessionId:Ljava/lang/String;

.field final synthetic this$0:Lcom/mediatek/GoogleOta/HttpManager;

.field versionName:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/mediatek/GoogleOta/HttpManager;)V
    .locals 4

    const/4 v3, -0x1

    const/4 v2, 0x0

    iput-object p1, p0, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;->this$0:Lcom/mediatek/GoogleOta/HttpManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v3, p0, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;->rand:I

    iput-object v2, p0, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;->sessionId:Ljava/lang/String;

    iput-object v2, p0, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;->versionName:Ljava/lang/String;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;->fileSize:J

    iput-object v2, p0, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;->releaseNote:Ljava/lang/String;

    iput v3, p0, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;->pkgId:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;->isFullPkg:Z

    iput-object v2, p0, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;->fingerprint:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/mediatek/GoogleOta/HttpManager;Lcom/mediatek/GoogleOta/HttpManager$1;)V
    .locals 0
    .param p1    # Lcom/mediatek/GoogleOta/HttpManager;
    .param p2    # Lcom/mediatek/GoogleOta/HttpManager$1;

    invoke-direct {p0, p1}, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;-><init>(Lcom/mediatek/GoogleOta/HttpManager;)V

    return-void
.end method
