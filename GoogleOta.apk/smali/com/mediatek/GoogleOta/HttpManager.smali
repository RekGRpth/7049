.class final Lcom/mediatek/GoogleOta/HttpManager;
.super Ljava/lang/Object;
.source "HttpManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/GoogleOta/HttpManager$IncomingHandler;,
        Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;
    }
.end annotation


# static fields
.field private static final CMD_FILE_KEY:Ljava/lang/String; = "COMMANDFILE"

.field private static final FINGER_PRINT_NAME:Ljava/lang/String; = "ro.build.fingerprint"

.field private static final HTTP_DETECTED_SDCARD_CRASH_OR_UNMOUNT:I = 0x76d

.field private static final HTTP_DETECTED_SDCARD_ERROR:I = 0x76e

.field private static final HTTP_DETECTED_SDCARD_INSUFFICENT:I = 0x76f

.field private static final HTTP_RESPONSE_AUTHEN_ERROR:I = 0x3ea

.field private static final HTTP_RESPONSE_CONN_TIMEOUT:I = 0x7d1

.field private static final HTTP_RESPONSE_DATABASE_ERROR:I = 0x44f

.field private static final HTTP_RESPONSE_DELTA_DELETE:I = 0x76c

.field private static final HTTP_RESPONSE_ILLEGAL_ACCESS:I = 0x3ec

.field private static final HTTP_RESPONSE_NETWORK_ERROR:I = 0x4b1

.field private static final HTTP_RESPONSE_NO_NEW_VERSION:I = 0x3f2

.field private static final HTTP_RESPONSE_NO_SPEC_NETWORK:I = 0x4b0

.field private static final HTTP_RESPONSE_PARAM_ERROR:I = 0x450

.field private static final HTTP_RESPONSE_SN_LOST:I = 0x3f0

.field private static final HTTP_RESPONSE_SN_REQUIRE:I = 0x3e9

.field private static final HTTP_RESPONSE_SUCCESS:I = 0x3e8

.field private static final HTTP_RESPONSE_TOKEN_INVALID:I = 0x3ee

.field private static final HTTP_RESPONSE_TOKEN_REQUIRE:I = 0x3ed

.field private static final HTTP_RESPONSE_UNZIP_CKSUM:I = 0x7d3

.field private static final HTTP_RESPONSE_UNZIP_ERROR:I = 0x7d2

.field private static final HTTP_RESPONSE_VERSION_DELETE:I = 0x452

.field private static final HTTP_RESPONSE_VERSION_ILLEGAL:I = 0x451

.field private static final HTTP_RESPONSE_VERSION_REQUIRE:I = 0x3f1

.field private static final HTTP_UNKNOW_ERROR:I = 0x7d0

.field private static final MSG_DELETE_COMMANDFILE:I = 0x1

.field private static final MSG_NONE:I = 0x0

.field private static final NETTYPENAME:Ljava/lang/String; = ""

.field private static final PORT_NUMBER:I = 0x1bb

.field private static final TAG:Ljava/lang/String; = "GoogleOta/HttpManager"

.field private static final TIME_OUT:I = 0x7530

.field private static cookies:Lorg/apache/http/client/CookieStore;

.field private static mHttpManager:Lcom/mediatek/GoogleOta/HttpManager;


# instance fields
.field private mConnection:Landroid/content/ServiceConnection;

.field private mContext:Landroid/content/Context;

.field private mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

.field private mErrorCode:I

.field private mHandler:Landroid/os/Handler;

.field private mHttpClient:Lorg/apache/http/impl/client/DefaultHttpClient;

.field private mIsBound:Z

.field private mIsDownloading:Z

.field private final mMessenger:Landroid/os/Messenger;

.field private mNeedServiceDo:I

.field private mNewVersionInfo:Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

.field private mNotification:Lcom/mediatek/GoogleOta/NotifyManager;

.field private mOTAresult:I

.field private mQueryAbort:Z

.field private mService:Landroid/os/Messenger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/mediatek/GoogleOta/HttpManager;->cookies:Lorg/apache/http/client/CookieStore;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/content/Context;

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v2, p0, Lcom/mediatek/GoogleOta/HttpManager;->mQueryAbort:Z

    iput-boolean v2, p0, Lcom/mediatek/GoogleOta/HttpManager;->mIsDownloading:Z

    iput-object v3, p0, Lcom/mediatek/GoogleOta/HttpManager;->mContext:Landroid/content/Context;

    iput-object v3, p0, Lcom/mediatek/GoogleOta/HttpManager;->mNewVersionInfo:Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    iput-object v3, p0, Lcom/mediatek/GoogleOta/HttpManager;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    const/16 v0, 0x3e8

    iput v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    iput v2, p0, Lcom/mediatek/GoogleOta/HttpManager;->mOTAresult:I

    new-instance v0, Landroid/os/Messenger;

    new-instance v1, Lcom/mediatek/GoogleOta/HttpManager$IncomingHandler;

    invoke-direct {v1, p0}, Lcom/mediatek/GoogleOta/HttpManager$IncomingHandler;-><init>(Lcom/mediatek/GoogleOta/HttpManager;)V

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mMessenger:Landroid/os/Messenger;

    iput-object v3, p0, Lcom/mediatek/GoogleOta/HttpManager;->mService:Landroid/os/Messenger;

    iput-boolean v2, p0, Lcom/mediatek/GoogleOta/HttpManager;->mIsBound:Z

    iput v2, p0, Lcom/mediatek/GoogleOta/HttpManager;->mNeedServiceDo:I

    new-instance v0, Lcom/mediatek/GoogleOta/HttpManager$1;

    invoke-direct {v0, p0}, Lcom/mediatek/GoogleOta/HttpManager$1;-><init>(Lcom/mediatek/GoogleOta/HttpManager;)V

    iput-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mConnection:Landroid/content/ServiceConnection;

    iput-object p1, p0, Lcom/mediatek/GoogleOta/HttpManager;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/mediatek/GoogleOta/DownloadStatus;->getInstance(Landroid/content/Context;)Lcom/mediatek/GoogleOta/DownloadStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    new-instance v0, Lcom/mediatek/GoogleOta/NotifyManager;

    iget-object v1, p0, Lcom/mediatek/GoogleOta/HttpManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/mediatek/GoogleOta/NotifyManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mNotification:Lcom/mediatek/GoogleOta/NotifyManager;

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/GoogleOta/HttpManager;)I
    .locals 1
    .param p0    # Lcom/mediatek/GoogleOta/HttpManager;

    iget v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mNeedServiceDo:I

    return v0
.end method

.method static synthetic access$202(Lcom/mediatek/GoogleOta/HttpManager;Landroid/os/Messenger;)Landroid/os/Messenger;
    .locals 0
    .param p0    # Lcom/mediatek/GoogleOta/HttpManager;
    .param p1    # Landroid/os/Messenger;

    iput-object p1, p0, Lcom/mediatek/GoogleOta/HttpManager;->mService:Landroid/os/Messenger;

    return-object p1
.end method

.method private checkFingerPrint(Ljava/lang/String;)Z
    .locals 4
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    :cond_0
    const-string v1, "GoogleOta/HttpManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "fingerPrintPkg = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "ro.build.fingerprint"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GoogleOta/HttpManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "fingerPrintLocal = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private deletePackage()V
    .locals 3

    const-string v1, "GoogleOta/HttpManager"

    const-string v2, "deletePackage"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/mediatek/GoogleOta/HttpManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/mediatek/GoogleOta/Util;->getPackageFileName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    if-nez v0, :cond_1

    const-string v1, "GoogleOta/HttpManager"

    const-string v2, "deletePackage, new file error"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "GoogleOta/HttpManager"

    const-string v2, "deletePackage, delte package"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/GoogleOta/HttpManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/mediatek/GoogleOta/Util;->getPackageFileName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/mediatek/GoogleOta/Util;->deleteFile(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private doActionUseService()V
    .locals 5

    :try_start_0
    iget v3, p0, Lcom/mediatek/GoogleOta/HttpManager;->mNeedServiceDo:I

    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-static {v3, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/GoogleOta/HttpManager;->mMessenger:Landroid/os/Messenger;

    iput-object v3, v2, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    if-eqz v0, :cond_0

    const-string v3, "COMMANDFILE"

    const-string v4, "/cache/recovery/command"

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v3, p0, Lcom/mediatek/GoogleOta/HttpManager;->mService:Landroid/os/Messenger;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/GoogleOta/HttpManager;->mService:Landroid/os/Messenger;

    invoke-virtual {v3, v2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private doBindService(Landroid/content/Context;)Z
    .locals 5
    .param p1    # Landroid/content/Context;

    const/4 v4, 0x1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.GoogleOta.SysOperService"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/GoogleOta/HttpManager;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p1, v1, v2, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    :cond_0
    const-string v1, "GoogleOta/HttpManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dobindService, isbind="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v4, p0, Lcom/mediatek/GoogleOta/HttpManager;->mIsBound:Z

    return v0
.end method

.method private doPost(Ljava/lang/String;Ljava/util/Map;Ljava/util/ArrayList;)Lorg/apache/http/HttpResponse;
    .locals 17
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/http/message/BasicNameValuePair;",
            ">;)",
            "Lorg/apache/http/HttpResponse;"
        }
    .end annotation

    const/4 v11, 0x0

    new-instance v6, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v6}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    const/16 v14, 0x7530

    invoke-static {v6, v14}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    const/16 v14, 0x7530

    invoke-static {v6, v14}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    new-instance v14, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v14, v6}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/params/HttpParams;)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/mediatek/GoogleOta/HttpManager;->mHttpClient:Lorg/apache/http/impl/client/DefaultHttpClient;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/GoogleOta/HttpManager;->mHttpClient:Lorg/apache/http/impl/client/DefaultHttpClient;

    const-string v14, "GoogleOta/HttpManager"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "doPost, url = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", cookies = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sget-object v16, Lcom/mediatek/GoogleOta/HttpManager;->cookies:Lorg/apache/http/client/CookieStore;

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v10, Lorg/apache/http/protocol/BasicHttpContext;

    invoke-direct {v10}, Lorg/apache/http/protocol/BasicHttpContext;-><init>()V

    sget-object v14, Lcom/mediatek/GoogleOta/HttpManager;->cookies:Lorg/apache/http/client/CookieStore;

    if-eqz v14, :cond_0

    const-string v14, "http.cookie-store"

    sget-object v15, Lcom/mediatek/GoogleOta/HttpManager;->cookies:Lorg/apache/http/client/CookieStore;

    invoke-interface {v10, v14, v15}, Lorg/apache/http/protocol/HttpContext;->setAttribute(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_0
    const/4 v4, 0x0

    const/4 v7, 0x0

    :try_start_0
    const-string v14, "https"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_1

    invoke-static/range {p1 .. p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v13

    new-instance v5, Lorg/apache/http/HttpHost;

    invoke-virtual {v13}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v14

    const/16 v15, 0x1bb

    invoke-virtual {v13}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-direct {v5, v14, v15, v0}, Lorg/apache/http/HttpHost;-><init>(Ljava/lang/String;ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    new-instance v8, Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {v13}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v8, v14}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    move-object v7, v8

    move-object v4, v5

    :goto_0
    if-eqz p2, :cond_2

    :try_start_2
    invoke-interface/range {p2 .. p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v14

    invoke-interface {v14}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    invoke-virtual {v7, v14, v15}, Lorg/apache/http/message/AbstractHttpMessage;->addHeader(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    :catch_0
    move-exception v2

    :goto_2
    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    const/16 v14, 0x7d1

    move-object/from16 v0, p0

    iput v14, v0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    :goto_3
    move-object v12, v11

    :goto_4
    return-object v12

    :cond_1
    :try_start_3
    new-instance v8, Lorg/apache/http/client/methods/HttpPost;

    move-object/from16 v0, p1

    invoke-direct {v8, v0}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    move-object v7, v8

    goto :goto_0

    :cond_2
    if-eqz p3, :cond_3

    new-instance v14, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    move-object/from16 v0, p3

    invoke-direct {v14, v0}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;-><init>(Ljava/util/List;)V

    invoke-virtual {v7, v14}, Lorg/apache/http/client/methods/HttpEntityEnclosingRequestBase;->setEntity(Lorg/apache/http/HttpEntity;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    :cond_3
    :try_start_4
    const-string v14, "https"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_5

    const-string v14, "GoogleOta/HttpManager"

    const-string v15, "doPost, https"

    invoke-static {v14, v15}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1, v4, v7}, Lorg/apache/http/impl/client/AbstractHttpClient;->execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v11

    :goto_5
    sget-object v14, Lcom/mediatek/GoogleOta/HttpManager;->cookies:Lorg/apache/http/client/CookieStore;

    if-nez v14, :cond_4

    invoke-virtual {v1}, Lorg/apache/http/impl/client/AbstractHttpClient;->getCookieStore()Lorg/apache/http/client/CookieStore;

    move-result-object v14

    sput-object v14, Lcom/mediatek/GoogleOta/HttpManager;->cookies:Lorg/apache/http/client/CookieStore;

    const-string v14, "GoogleOta/HttpManager"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "cookies size = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sget-object v16, Lcom/mediatek/GoogleOta/HttpManager;->cookies:Lorg/apache/http/client/CookieStore;

    invoke-interface/range {v16 .. v16}, Lorg/apache/http/client/CookieStore;->getCookies()Ljava/util/List;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    move-object v12, v11

    goto :goto_4

    :cond_5
    const-string v14, "GoogleOta/HttpManager"

    const-string v15, "doPost, http"

    invoke-static {v14, v15}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1, v7, v10}, Lorg/apache/http/impl/client/AbstractHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;
    :try_end_4
    .catch Lorg/apache/http/conn/ConnectTimeoutException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    move-result-object v11

    goto :goto_5

    :catch_1
    move-exception v2

    :try_start_5
    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    const/16 v14, 0x7d1

    move-object/from16 v0, p0

    iput v14, v0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    goto :goto_3

    :catch_2
    move-exception v2

    move-object v4, v5

    goto :goto_2
.end method

.method private doUnbindService(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const-string v0, "GoogleOta/HttpManager"

    const-string v1, "doUnbindService"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mIsBound:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mService:Landroid/os/Messenger;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p1, v0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mIsBound:Z

    :cond_1
    return-void
.end method

.method private getChunkedContent(Lorg/apache/http/HttpEntity;)Ljava/lang/String;
    .locals 9
    .param p1    # Lorg/apache/http/HttpEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v7, 0x400

    const/4 v6, 0x0

    const/4 v3, 0x0

    new-array v4, v6, [B

    new-array v0, v7, [B

    const-string v6, "GoogleOta/HttpManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getChunkedContent, isChunked = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {p1}, Lorg/apache/http/HttpEntity;->isChunked()Z

    move-result v8

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {p1}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v2

    new-instance v5, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v5}, Ljava/io/ByteArrayOutputStream;-><init>()V

    :goto_0
    const/4 v6, 0x0

    const/16 v7, 0x400

    :try_start_0
    invoke-virtual {v2, v0, v6, v7}, Ljava/io/InputStream;->read([BII)I

    move-result v3

    if-lez v3, :cond_1

    const/4 v6, 0x0

    invoke-virtual {v5, v0, v6, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/HttpManager;->onShutdownConn()V

    iget-object v6, p0, Lcom/mediatek/GoogleOta/HttpManager;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    invoke-virtual {v6}, Lcom/mediatek/GoogleOta/DownloadStatus;->getDLSessionStatus()I

    move-result v6

    const/4 v7, 0x2

    if-ne v6, v7, :cond_0

    iget-object v6, p0, Lcom/mediatek/GoogleOta/HttpManager;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    const/4 v7, 0x4

    invoke-virtual {v6, v7}, Lcom/mediatek/GoogleOta/DownloadStatus;->setDLSessionStatus(I)V

    const-string v6, "GoogleOta/HttpManager"

    const-string v7, "getChunkedContent, exception to set pause state"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/16 v6, 0x7d1

    iput v6, p0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    :goto_1
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([B)V

    return-object v6

    :cond_1
    :try_start_1
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v4

    goto :goto_1
.end method

.method static getInstance(Landroid/content/Context;)Lcom/mediatek/GoogleOta/HttpManager;
    .locals 3
    .param p0    # Landroid/content/Context;

    const-string v0, "GoogleOta/HttpManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mHttpManager = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/mediatek/GoogleOta/HttpManager;->mHttpManager:Lcom/mediatek/GoogleOta/HttpManager;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/mediatek/GoogleOta/HttpManager;->mHttpManager:Lcom/mediatek/GoogleOta/HttpManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/GoogleOta/HttpManager;

    invoke-direct {v0, p0}, Lcom/mediatek/GoogleOta/HttpManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/mediatek/GoogleOta/HttpManager;->mHttpManager:Lcom/mediatek/GoogleOta/HttpManager;

    :cond_0
    sget-object v0, Lcom/mediatek/GoogleOta/HttpManager;->mHttpManager:Lcom/mediatek/GoogleOta/HttpManager;

    return-object v0
.end method

.method private getToken()Ljava/lang/String;
    .locals 14

    const-string v11, "GoogleOta/HttpManager"

    const-string v12, "getToken"

    invoke-static {v11, v12}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v6, 0x0

    :try_start_0
    const-string v11, "MD5"

    invoke-static {v11}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v6

    const/4 v10, 0x0

    sget-object v11, Lcom/mediatek/GoogleOta/HttpManager;->cookies:Lorg/apache/http/client/CookieStore;

    if-eqz v11, :cond_6

    sget-object v11, Lcom/mediatek/GoogleOta/HttpManager;->cookies:Lorg/apache/http/client/CookieStore;

    invoke-interface {v11}, Lorg/apache/http/client/CookieStore;->getCookies()Ljava/util/List;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v8

    const/4 v11, 0x1

    if-ge v8, v11, :cond_0

    const-string v11, "GoogleOta/HttpManager"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Error: cookies size is "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v11, 0x0

    :goto_0
    return-object v11

    :cond_0
    sget-object v11, Lcom/mediatek/GoogleOta/HttpManager;->cookies:Lorg/apache/http/client/CookieStore;

    invoke-interface {v11}, Lorg/apache/http/client/CookieStore;->getCookies()Ljava/util/List;

    move-result-object v11

    add-int/lit8 v12, v8, -0x1

    invoke-interface {v11, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/http/cookie/Cookie;

    const/4 v7, 0x0

    :goto_1
    if-ge v7, v8, :cond_2

    sget-object v11, Lcom/mediatek/GoogleOta/HttpManager;->cookies:Lorg/apache/http/client/CookieStore;

    invoke-interface {v11}, Lorg/apache/http/client/CookieStore;->getCookies()Ljava/util/List;

    move-result-object v11

    invoke-interface {v11, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/http/cookie/Cookie;

    const-string v11, "GoogleOta/HttpManager"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "index:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "cookieRand.getName = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-interface {v2}, Lorg/apache/http/cookie/Cookie;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v2, :cond_1

    invoke-interface {v2}, Lorg/apache/http/cookie/Cookie;->getName()Ljava/lang/String;

    move-result-object v11

    const-string v12, "PHPRAND"

    if-ne v11, v12, :cond_1

    move-object v3, v2

    :cond_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_2
    const-string v11, "GoogleOta/HttpManager"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "cookieRand.getName = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-interface {v3}, Lorg/apache/http/cookie/Cookie;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v3, :cond_3

    invoke-interface {v3}, Lorg/apache/http/cookie/Cookie;->getName()Ljava/lang/String;

    move-result-object v11

    const-string v12, "PHPRAND"

    if-eq v11, v12, :cond_4

    :cond_3
    const/4 v11, 0x0

    goto :goto_0

    :cond_4
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "15811375356"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-interface {v3}, Lorg/apache/http/cookie/Cookie;->getValue()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v11, "GoogleOta/HttpManager"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "getToken, str = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    invoke-virtual {v10}, Ljava/lang/String;->getBytes()[B

    move-result-object v11

    invoke-virtual {v6, v11}, Ljava/security/MessageDigest;->update([B)V

    invoke-virtual {v6}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v1

    const/4 v5, 0x0

    :goto_3
    array-length v11, v1

    if-ge v5, v11, :cond_8

    aget-byte v11, v1, v5

    and-int/lit16 v11, v11, 0xff

    invoke-static {v11}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v11

    const/4 v12, 0x1

    if-ne v11, v12, :cond_5

    const-string v11, "0"

    invoke-virtual {v0, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_5
    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    :cond_6
    invoke-direct {p0}, Lcom/mediatek/GoogleOta/HttpManager;->onHandsakeAuthentication()Z

    move-result v11

    if-eqz v11, :cond_7

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "15811375356"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    sget-object v11, Lcom/mediatek/GoogleOta/HttpManager;->cookies:Lorg/apache/http/client/CookieStore;

    invoke-interface {v11}, Lorg/apache/http/client/CookieStore;->getCookies()Ljava/util/List;

    move-result-object v11

    const/4 v13, 0x1

    invoke-interface {v11, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/apache/http/cookie/Cookie;

    invoke-interface {v11}, Lorg/apache/http/cookie/Cookie;->getValue()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto :goto_2

    :cond_7
    const-string v11, "GoogleOta/HttpManager"

    const-string v12, "getToken, reHandshake error"

    invoke-static {v11, v12}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v11, 0x0

    goto/16 :goto_0

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Ljava/lang/Throwable;->printStackTrace()V

    const/4 v11, 0x0

    goto/16 :goto_0

    :cond_8
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    goto/16 :goto_0
.end method

.method private isNetworkAvailable(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    invoke-static {p1, p2}, Lcom/mediatek/GoogleOta/Util;->isSpecifiedNetWorkAvailable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const/16 v2, 0x4b0

    iput v2, p0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    :goto_0
    return v1

    :cond_0
    invoke-static {p1}, Lcom/mediatek/GoogleOta/Util;->isNetWorkAvailable(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    const/16 v2, 0x4b1

    iput v2, p0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private onCheckDeltaPackage()I
    .locals 3

    new-instance v0, Lcom/mediatek/GoogleOta/CheckOta;

    iget-object v1, p0, Lcom/mediatek/GoogleOta/HttpManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/mediatek/GoogleOta/HttpManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/mediatek/GoogleOta/Util;->getPackageFileName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/mediatek/GoogleOta/CheckOta;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mediatek/GoogleOta/CheckOta;->execForResult()I

    move-result v1

    return v1
.end method

.method private onCheckNewVersion()Z
    .locals 14

    const-string v10, "GoogleOta/HttpManager"

    const-string v11, "onCheckNewVersion"

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v10, p0, Lcom/mediatek/GoogleOta/HttpManager;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f040001

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    :try_start_0
    invoke-direct {p0}, Lcom/mediatek/GoogleOta/HttpManager;->getToken()Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_0

    const/16 v10, 0x3ea

    iput v10, p0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    const/4 v10, 0x0

    :goto_0
    return v10

    :cond_0
    new-instance v6, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "token"

    invoke-static {v7}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v6, v10, v11}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "version"

    iget-object v11, p0, Lcom/mediatek/GoogleOta/HttpManager;->mContext:Landroid/content/Context;

    invoke-static {v11}, Lcom/mediatek/GoogleOta/Util;->getDeviceVersionInfo(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-direct {p0, v8, v10, v0}, Lcom/mediatek/GoogleOta/HttpManager;->doPost(Ljava/lang/String;Ljava/util/Map;Ljava/util/ArrayList;)Lorg/apache/http/HttpResponse;

    move-result-object v4

    if-nez v4, :cond_1

    const-string v10, "GoogleOta/HttpManager"

    const-string v11, "onCheckNewVersion: response = null"

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v10, 0x0

    goto :goto_0

    :cond_1
    invoke-interface {v4}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v5

    invoke-interface {v5}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v10

    const/16 v11, 0xc8

    if-eq v10, v11, :cond_2

    const-string v10, "GoogleOta/HttpManager"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "onCheckNewVersion, ReasonPhrase = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-interface {v5}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v10, 0x0

    sput-object v10, Lcom/mediatek/GoogleOta/HttpManager;->cookies:Lorg/apache/http/client/CookieStore;

    const/16 v10, 0x3ea

    iput v10, p0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    const/4 v10, 0x0

    goto :goto_0

    :cond_2
    invoke-interface {v4}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v10

    invoke-direct {p0, v10}, Lcom/mediatek/GoogleOta/HttpManager;->getChunkedContent(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v1

    const-string v10, "GoogleOta/HttpManager"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "onCheckNewVersion, response content = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v1}, Lcom/mediatek/GoogleOta/HttpManager;->parseCheckVersionInfo(Ljava/lang/String;)Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;

    move-result-object v3

    const-string v10, "GoogleOta/HttpManager"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "onCheckNewVersion, res = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v3, :cond_3

    const/4 v10, 0x0

    goto/16 :goto_0

    :cond_3
    iget-wide v10, v3, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;->fileSize:J

    const-wide/16 v12, 0x0

    cmp-long v10, v10, v12

    if-lez v10, :cond_4

    iget v10, v3, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;->pkgId:I

    if-gez v10, :cond_5

    :cond_4
    const/16 v10, 0x3f2

    iput v10, p0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    const-string v10, "GoogleOta/HttpManager"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "onCheckNewVersion, fileSize = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-wide v12, v3, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;->fileSize:J

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", deltaId = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, v3, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;->pkgId:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v10, 0x0

    goto/16 :goto_0

    :cond_5
    iget-boolean v10, v3, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;->isFullPkg:Z

    if-nez v10, :cond_6

    iget-object v10, v3, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;->fingerprint:Ljava/lang/String;

    invoke-direct {p0, v10}, Lcom/mediatek/GoogleOta/HttpManager;->checkFingerPrint(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_6

    const/16 v10, 0x3f2

    iput v10, p0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    const/4 v10, 0x0

    goto/16 :goto_0

    :cond_6
    iget-object v10, p0, Lcom/mediatek/GoogleOta/HttpManager;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    invoke-virtual {v10}, Lcom/mediatek/GoogleOta/DownloadStatus;->getDownloadDescriptor()Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    move-result-object v10

    iput-object v10, p0, Lcom/mediatek/GoogleOta/HttpManager;->mNewVersionInfo:Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    iget-object v10, p0, Lcom/mediatek/GoogleOta/HttpManager;->mNewVersionInfo:Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    iget-object v11, v3, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;->releaseNote:Ljava/lang/String;

    iput-object v11, v10, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->newNote:Ljava/lang/String;

    iget-object v10, p0, Lcom/mediatek/GoogleOta/HttpManager;->mNewVersionInfo:Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    iget-object v11, v3, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;->versionName:Ljava/lang/String;

    iput-object v11, v10, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->version:Ljava/lang/String;

    iget-object v10, p0, Lcom/mediatek/GoogleOta/HttpManager;->mNewVersionInfo:Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    iget-wide v11, v3, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;->fileSize:J

    iput-wide v11, v10, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->size:J

    iget-object v10, p0, Lcom/mediatek/GoogleOta/HttpManager;->mNewVersionInfo:Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    iget v11, v3, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;->pkgId:I

    iput v11, v10, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->pkgId:I

    iget-object v10, p0, Lcom/mediatek/GoogleOta/HttpManager;->mNewVersionInfo:Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    iget-boolean v11, v3, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;->isFullPkg:Z

    iput-boolean v11, v10, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->isFullPkg:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v10, 0x1

    goto/16 :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    const/16 v10, 0x3ea

    iput v10, p0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    const/4 v10, 0x0

    goto/16 :goto_0
.end method

.method private onDownloadPackageUnzipAndCheck()V
    .locals 10

    const/16 v9, 0x7d2

    const/4 v8, 0x0

    const/4 v7, 0x1

    invoke-virtual {p0}, Lcom/mediatek/GoogleOta/HttpManager;->onPackageUnzipping()V

    iget-object v4, p0, Lcom/mediatek/GoogleOta/HttpManager;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    invoke-virtual {v4, v7}, Lcom/mediatek/GoogleOta/DownloadStatus;->setDLSessionUnzipState(Z)V

    iget-object v4, p0, Lcom/mediatek/GoogleOta/HttpManager;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    invoke-virtual {v4}, Lcom/mediatek/GoogleOta/DownloadStatus;->getDLSessionRenameState()Z

    move-result v4

    if-nez v4, :cond_1

    new-instance v0, Ljava/io/File;

    iget-object v4, p0, Lcom/mediatek/GoogleOta/HttpManager;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/mediatek/GoogleOta/Util;->getPackageFileName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/mediatek/GoogleOta/HttpManager;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/mediatek/GoogleOta/Util;->getPackagePathName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/package.zip"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v1

    if-nez v1, :cond_0

    iput v9, p0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/HttpManager;->sendErrorMessage()V

    :goto_0
    return-void

    :cond_0
    iget-object v4, p0, Lcom/mediatek/GoogleOta/HttpManager;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    invoke-virtual {v4, v7}, Lcom/mediatek/GoogleOta/DownloadStatus;->setDLSessionRenameState(Z)V

    :cond_1
    iget-object v4, p0, Lcom/mediatek/GoogleOta/HttpManager;->mContext:Landroid/content/Context;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/mediatek/GoogleOta/HttpManager;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/mediatek/GoogleOta/Util;->getPackagePathName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/package.zip"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/GoogleOta/GoogleOtaUnzipChecksum;->checkUnzipSpace(Landroid/content/Context;Ljava/lang/String;)I

    move-result v3

    packed-switch v3, :pswitch_data_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/mediatek/GoogleOta/HttpManager;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/mediatek/GoogleOta/Util;->getPackagePathName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/package.zip"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/mediatek/GoogleOta/GoogleOtaUnzipChecksum;->unzipDelta(Ljava/lang/String;)I

    move-result v2

    const/4 v4, 0x2

    if-ne v2, v4, :cond_2

    invoke-virtual {p0}, Lcom/mediatek/GoogleOta/HttpManager;->onDownloadComplete()V

    iget-object v4, p0, Lcom/mediatek/GoogleOta/HttpManager;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    invoke-virtual {v4, v8}, Lcom/mediatek/GoogleOta/DownloadStatus;->setDLSessionUnzipState(Z)V

    iget-object v4, p0, Lcom/mediatek/GoogleOta/HttpManager;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    invoke-virtual {v4, v8}, Lcom/mediatek/GoogleOta/DownloadStatus;->setDLSessionRenameState(Z)V

    iget-object v4, p0, Lcom/mediatek/GoogleOta/HttpManager;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/mediatek/GoogleOta/Util;->getPackagePathName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/mediatek/GoogleOta/GoogleOtaUnzipChecksum;->deleteUnuseFile(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_0
    const/16 v4, 0x76f

    iput v4, p0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/HttpManager;->sendErrorMessage()V

    goto :goto_0

    :pswitch_1
    const/16 v4, 0x76e

    iput v4, p0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/HttpManager;->sendErrorMessage()V

    goto :goto_0

    :cond_2
    if-ne v2, v7, :cond_3

    const/16 v4, 0x7d3

    iput v4, p0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/HttpManager;->sendErrorMessage()V

    goto :goto_0

    :cond_3
    iput v9, p0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/HttpManager;->sendErrorMessage()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private onHandsakeAuthentication()Z
    .locals 17

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/GoogleOta/HttpManager;->mContext:Landroid/content/Context;

    if-nez v14, :cond_0

    const/4 v14, 0x0

    :goto_0
    return v14

    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/GoogleOta/HttpManager;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const/high16 v15, 0x7f040000

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    sput-object v14, Lcom/mediatek/GoogleOta/HttpManager;->cookies:Lorg/apache/http/client/CookieStore;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/GoogleOta/HttpManager;->mContext:Landroid/content/Context;

    invoke-static {v14}, Lcom/mediatek/GoogleOta/Util;->getDeviceInfo(Landroid/content/Context;)Lcom/mediatek/GoogleOta/Util$DeviceInfo;

    move-result-object v4

    const-string v14, "GoogleOta/HttpManager"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "onHandsakeAuthentication, imei = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    iget-object v0, v4, Lcom/mediatek/GoogleOta/Util$DeviceInfo;->imei:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", sn = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    iget-object v0, v4, Lcom/mediatek/GoogleOta/Util$DeviceInfo;->sn:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", sim = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    iget-object v0, v4, Lcom/mediatek/GoogleOta/Util$DeviceInfo;->sim:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", operator = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    iget-object v0, v4, Lcom/mediatek/GoogleOta/Util$DeviceInfo;->operator:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v6, Lorg/apache/http/message/BasicNameValuePair;

    const-string v14, "imei"

    iget-object v15, v4, Lcom/mediatek/GoogleOta/Util$DeviceInfo;->imei:Ljava/lang/String;

    invoke-direct {v6, v14, v15}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v10, Lorg/apache/http/message/BasicNameValuePair;

    const-string v14, "sim"

    iget-object v15, v4, Lcom/mediatek/GoogleOta/Util$DeviceInfo;->sim:Ljava/lang/String;

    invoke-direct {v10, v14, v15}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    const-string v14, "operator"

    iget-object v15, v4, Lcom/mediatek/GoogleOta/Util$DeviceInfo;->operator:Ljava/lang/String;

    invoke-direct {v7, v14, v15}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v11, Lorg/apache/http/message/BasicNameValuePair;

    const-string v14, "sn"

    const-string v15, "15811375356"

    invoke-direct {v11, v14, v15}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v13, v14, v1}, Lcom/mediatek/GoogleOta/HttpManager;->doPost(Ljava/lang/String;Ljava/util/Map;Ljava/util/ArrayList;)Lorg/apache/http/HttpResponse;

    move-result-object v9

    if-nez v9, :cond_1

    const-string v14, "GoogleOta/HttpManager"

    const-string v15, "onHandsakeAuthentication, response = null"

    invoke-static {v14, v15}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v14, 0x0

    goto/16 :goto_0

    :cond_1
    invoke-interface {v9}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v12

    invoke-interface {v12}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v14

    const/16 v15, 0xc8

    if-eq v14, v15, :cond_2

    const-string v14, "GoogleOta/HttpManager"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "onHandsakeAuthentication, ReasonPhrase = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-interface {v12}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v14, 0x3ea

    move-object/from16 v0, p0

    iput v14, v0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    const/4 v14, 0x0

    goto/16 :goto_0

    :cond_2
    :try_start_0
    invoke-interface {v9}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/mediatek/GoogleOta/HttpManager;->getChunkedContent(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v2

    const-string v14, "GoogleOta/HttpManager"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "onHandsakeAuthentication, response content = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/mediatek/GoogleOta/HttpManager;->parseAuthenInfo(Ljava/lang/String;)Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;

    move-result-object v8

    if-nez v8, :cond_3

    const/4 v14, 0x0

    goto/16 :goto_0

    :cond_3
    new-instance v3, Lorg/apache/http/impl/cookie/BasicClientCookie;

    const-string v14, "PHPRAND"

    iget v15, v8, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;->rand:I

    invoke-static {v15}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v3, v14, v15}, Lorg/apache/http/impl/cookie/BasicClientCookie;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v14, Lcom/mediatek/GoogleOta/HttpManager;->cookies:Lorg/apache/http/client/CookieStore;

    if-nez v14, :cond_4

    const-string v14, "GoogleOta/HttpManager"

    const-string v15, "onHandsakeAuthentication: cookies = null"

    invoke-static {v14, v15}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v14, 0x7d1

    move-object/from16 v0, p0

    iput v14, v0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    const/4 v14, 0x0

    goto/16 :goto_0

    :cond_4
    sget-object v14, Lcom/mediatek/GoogleOta/HttpManager;->cookies:Lorg/apache/http/client/CookieStore;

    invoke-interface {v14, v3}, Lorg/apache/http/client/CookieStore;->addCookie(Lorg/apache/http/cookie/Cookie;)V

    const-string v14, "GoogleOta/HttpManager"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "cookies size = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sget-object v16, Lcom/mediatek/GoogleOta/HttpManager;->cookies:Lorg/apache/http/client/CookieStore;

    invoke-interface/range {v16 .. v16}, Lorg/apache/http/client/CookieStore;->getCookies()Ljava/util/List;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v14, "GoogleOta/HttpManager"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "onHandsakeAuthentication, rand = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    iget v0, v8, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;->rand:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", sessionId = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    iget-object v0, v8, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;->sessionId:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    const/4 v14, 0x1

    goto/16 :goto_0

    :catch_0
    move-exception v5

    invoke-virtual {v5}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_1
.end method

.method private onQuery()Z
    .locals 6

    const/4 v3, 0x0

    const-string v4, "GoogleOta/HttpManager"

    const-string v5, "onQuery"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/mediatek/GoogleOta/HttpManager;->mContext:Landroid/content/Context;

    const-string v5, ""

    invoke-direct {p0, v4, v5}, Lcom/mediatek/GoogleOta/HttpManager;->isNetworkAvailable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    invoke-direct {p0}, Lcom/mediatek/GoogleOta/HttpManager;->onHandsakeAuthentication()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/HttpManager;->onCheckNewVersion()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v3, 0x1

    goto :goto_0
.end method

.method private onShutdownConn()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "GoogleOta/HttpManager"

    const-string v1, "onShutdownConn"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHttpClient:Lorg/apache/http/impl/client/DefaultHttpClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHttpClient:Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-virtual {v0}, Lorg/apache/http/impl/client/AbstractHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    :cond_0
    iput-object v2, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHttpClient:Lorg/apache/http/impl/client/DefaultHttpClient;

    sput-object v2, Lcom/mediatek/GoogleOta/HttpManager;->cookies:Lorg/apache/http/client/CookieStore;

    return-void
.end method

.method private onTransferRatio()V
    .locals 9

    const-wide/16 v6, 0x0

    iget-object v5, p0, Lcom/mediatek/GoogleOta/HttpManager;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    invoke-virtual {v5}, Lcom/mediatek/GoogleOta/DownloadStatus;->getUpdateImageSize()J

    move-result-wide v3

    iget-object v5, p0, Lcom/mediatek/GoogleOta/HttpManager;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/mediatek/GoogleOta/Util;->getPackageFileName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/mediatek/GoogleOta/Util;->getFileSize(Ljava/lang/String;)J

    move-result-wide v0

    cmp-long v5, v0, v6

    if-gez v5, :cond_0

    const-wide/16 v0, 0x0

    :cond_0
    cmp-long v5, v3, v6

    if-nez v5, :cond_1

    const-wide/16 v3, -0x1

    :cond_1
    cmp-long v5, v3, v6

    if-gez v5, :cond_2

    :goto_0
    return-void

    :cond_2
    long-to-double v5, v0

    long-to-double v7, v3

    div-double/2addr v5, v7

    const-wide/high16 v7, 0x4059000000000000L

    mul-double/2addr v5, v7

    double-to-int v2, v5

    const/16 v5, 0x64

    if-le v2, v5, :cond_3

    const/16 v2, 0x64

    move-wide v0, v3

    :cond_3
    iget-object v5, p0, Lcom/mediatek/GoogleOta/HttpManager;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    invoke-virtual {v5, v2}, Lcom/mediatek/GoogleOta/DownloadStatus;->setDownLoadPercent(I)V

    goto :goto_0
.end method

.method private parseAuthenInfo(Ljava/lang/String;)Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;
    .locals 8
    .param p1    # Ljava/lang/String;

    const/16 v6, 0x3e8

    const/4 v4, 0x0

    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    new-instance v3, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;

    const/4 v5, 0x0

    invoke-direct {v3, p0, v5}, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;-><init>(Lcom/mediatek/GoogleOta/HttpManager;Lcom/mediatek/GoogleOta/HttpManager$1;)V

    const-string v5, "status"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    if-ne v5, v6, :cond_0

    const-string v5, "rand"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    iput v5, v3, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;->rand:I

    const-string v5, "sessionId"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;->sessionId:Ljava/lang/String;

    const/16 v5, 0x3e8

    iput v5, p0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    :goto_0
    return-object v3

    :cond_0
    const-string v5, "info"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v5, "status"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    const-string v5, "GoogleOta/HttpManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "parseAuthenInfo, error info = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v3, v4

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/HttpManager;->onShutdownConn()V

    const/16 v5, 0x7d1

    iput v5, p0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    move-object v3, v4

    goto :goto_0
.end method

.method private parseCheckVersionInfo(Ljava/lang/String;)Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;
    .locals 9
    .param p1    # Ljava/lang/String;

    const/16 v7, 0x3e8

    const/4 v4, 0x0

    const-string v5, "GoogleOta/HttpManager"

    const-string v6, "parseCheckVersionInfo"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    new-instance v3, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;

    const/4 v5, 0x0

    invoke-direct {v3, p0, v5}, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;-><init>(Lcom/mediatek/GoogleOta/HttpManager;Lcom/mediatek/GoogleOta/HttpManager$1;)V

    const-string v5, "status"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    if-ne v5, v7, :cond_2

    const-string v5, "GoogleOta/HttpManager"

    const-string v6, "HTTP_RESPONSE_SUCCESS"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "size"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v5

    iput-wide v5, v3, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;->fileSize:J

    const-string v5, "GoogleOta/HttpManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "res.fileSize = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-wide v7, v3, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;->fileSize:J

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "release_notes"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;->releaseNote:Ljava/lang/String;

    const-string v5, "GoogleOta/HttpManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "res.releaseNote = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v3, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;->releaseNote:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "versionId"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v5, "versionId"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    iput v5, v3, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;->pkgId:I

    const/4 v5, 0x1

    iput-boolean v5, v3, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;->isFullPkg:Z

    const-string v5, "version"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;->versionName:Ljava/lang/String;

    const-string v5, "GoogleOta/HttpManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "res.versionName = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v3, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;->versionName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "GoogleOta/HttpManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "full package: res.packageId = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v3, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;->pkgId:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    const/16 v5, 0x3e8

    iput v5, p0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    :goto_1
    return-object v3

    :cond_1
    const-string v5, "deltaId"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "deltaId"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    iput v5, v3, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;->pkgId:I

    const/4 v5, 0x0

    iput-boolean v5, v3, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;->isFullPkg:Z

    const-string v5, "name"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;->versionName:Ljava/lang/String;

    const-string v5, "GoogleOta/HttpManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "res.versionName = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v3, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;->versionName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "GoogleOta/HttpManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "delta package: res.packageId = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v3, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;->pkgId:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "fingerprint"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/mediatek/GoogleOta/HttpManager$HttpResponseContent;->fingerprint:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/HttpManager;->onShutdownConn()V

    const/16 v5, 0x7d1

    iput v5, p0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    move-object v3, v4

    goto :goto_1

    :cond_2
    :try_start_1
    const-string v5, "info"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v5, "status"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    const-string v5, "GoogleOta/HttpManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "parseCheckVersionInfo, error info = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-object v3, v4

    goto/16 :goto_1
.end method

.method private sdcardCheck()Z
    .locals 11

    const/16 v10, 0x76e

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/mediatek/GoogleOta/HttpManager;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    invoke-virtual {v6}, Lcom/mediatek/GoogleOta/DownloadStatus;->getDownloadDescriptor()Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    move-result-object v2

    iget-object v6, p0, Lcom/mediatek/GoogleOta/HttpManager;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/mediatek/GoogleOta/Util;->isSdcardAvailable(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_0

    iput v10, p0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    :goto_0
    return v5

    :cond_0
    iget-object v6, p0, Lcom/mediatek/GoogleOta/HttpManager;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/mediatek/GoogleOta/Util;->getPackageFileName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/mediatek/GoogleOta/Util;->getFileSize(Ljava/lang/String;)J

    move-result-wide v0

    iget-wide v6, v2, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->size:J

    long-to-double v6, v6

    const-wide/high16 v8, 0x4004000000000000L

    mul-double/2addr v6, v8

    double-to-long v3, v6

    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-lez v6, :cond_1

    sub-long/2addr v3, v0

    :cond_1
    iget-object v6, p0, Lcom/mediatek/GoogleOta/HttpManager;->mContext:Landroid/content/Context;

    invoke-static {v6, v3, v4}, Lcom/mediatek/GoogleOta/Util;->checkSdcardIsAvailable(Landroid/content/Context;J)I

    move-result v6

    packed-switch v6, :pswitch_data_0

    const/4 v5, 0x1

    goto :goto_0

    :pswitch_0
    const/16 v6, 0x76f

    iput v6, p0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    goto :goto_0

    :pswitch_1
    iput v10, p0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private sendCheckOTAMessage()V
    .locals 4

    const/16 v3, 0xd

    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    const-string v0, "GoogleOta/HttpManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sendCheckOTAMessage, mOTAresult = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/GoogleOta/HttpManager;->mOTAresult:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mOTAresult:I

    packed-switch v0, :pswitch_data_0

    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/HttpManager;->deletePackage()V

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x12

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/HttpManager;->deletePackage()V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    const/16 v2, 0xe

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/HttpManager;->deletePackage()V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    const/16 v2, 0xf

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/HttpManager;->deletePackage()V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x14

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :pswitch_6
    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x13

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :pswitch_7
    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_4
        :pswitch_7
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private sendErrorMessage()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    const-string v0, "GoogleOta/HttpManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sendErrorMessage, mErrorCode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    const/16 v1, 0x4b0

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3, v4, v4}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    const/16 v1, 0x3f2

    if-eq v0, v1, :cond_2

    iget v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    const/16 v1, 0x3e9

    if-ne v0, v1, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    const/16 v1, 0x451

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_4
    iget v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    const/16 v1, 0x452

    if-ne v0, v1, :cond_5

    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_5
    iget v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    const/16 v1, 0x3f1

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_6
    iget v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    const/16 v1, 0x76c

    if-ne v0, v1, :cond_7

    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_7
    iget v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    const/16 v1, 0x76e

    if-ne v0, v1, :cond_8

    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_8
    iget v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    const/16 v1, 0x76f

    if-ne v0, v1, :cond_9

    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    const/16 v2, 0xb

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    :cond_9
    iget v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    const/16 v1, 0x76d

    if-ne v0, v1, :cond_a

    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    :cond_a
    iget v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    const/16 v1, 0x4b1

    if-eq v0, v1, :cond_b

    iget v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    const/16 v1, 0x7d1

    if-ne v0, v1, :cond_c

    :cond_b
    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3, v3, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    :cond_c
    iget v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    const/16 v1, 0x7d2

    if-ne v0, v1, :cond_d

    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x15

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    :cond_d
    iget v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    const/16 v1, 0x7d3

    if-ne v0, v1, :cond_e

    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x16

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    :cond_e
    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0
.end method


# virtual methods
.method isDeltaPackageOk()Z
    .locals 3

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/HttpManager;->onCheckDeltaPackage()I

    move-result v0

    iput v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mOTAresult:I

    const-string v0, "GoogleOta/HttpManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "check_ota result = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/GoogleOta/HttpManager;->mOTAresult:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mOTAresult:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/GoogleOta/HttpManager;->sendCheckOTAMessage()V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method onDownloadComplete()V
    .locals 3

    const-string v0, "GoogleOta/HttpManager"

    const-string v1, "onDownloadComplete"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    invoke-virtual {v0}, Lcom/mediatek/GoogleOta/DownloadStatus;->getDownloadDescriptor()Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mNewVersionInfo:Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/mediatek/GoogleOta/DownloadStatus;->setDLSessionStatus(I)V

    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mNotification:Lcom/mediatek/GoogleOta/NotifyManager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/mediatek/GoogleOta/NotifyManager;->clearNotification(I)V

    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mNotification:Lcom/mediatek/GoogleOta/NotifyManager;

    iget-object v1, p0, Lcom/mediatek/GoogleOta/HttpManager;->mNewVersionInfo:Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    iget-object v1, v1, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->version:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/mediatek/GoogleOta/NotifyManager;->showDownloadCompletedNotification(Ljava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mNewVersionInfo:Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    const/4 v1, -0x1

    iput v1, v0, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->pkgId:I

    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mNewVersionInfo:Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->newNote:Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mNewVersionInfo:Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    const-wide/16 v1, -0x1

    iput-wide v1, v0, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->size:J

    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    iget-object v1, p0, Lcom/mediatek/GoogleOta/HttpManager;->mNewVersionInfo:Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    invoke-virtual {v0, v1}, Lcom/mediatek/GoogleOta/DownloadStatus;->setDownloadDesctiptor(Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;)V

    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Lcom/mediatek/GoogleOta/DownloadStatus;->setDownLoadPercent(I)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method onDownloadImage()V
    .locals 22

    const-string v19, "GoogleOta/HttpManager"

    const-string v20, "onDownloadImage"

    invoke-static/range {v19 .. v20}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/mediatek/GoogleOta/HttpManager;->mIsDownloading:Z

    move/from16 v19, v0

    if-eqz v19, :cond_0

    :goto_0
    return-void

    :cond_0
    const/16 v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/mediatek/GoogleOta/HttpManager;->mIsDownloading:Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/GoogleOta/HttpManager;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/mediatek/GoogleOta/DownloadStatus;->getDLSessionUnzipState()Z

    move-result v10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/GoogleOta/HttpManager;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/mediatek/GoogleOta/DownloadStatus;->getDLSessionRenameState()Z

    move-result v9

    if-eqz v9, :cond_1

    if-eqz v10, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/GoogleOta/HttpManager;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/mediatek/GoogleOta/Util;->getPackagePathName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/mediatek/GoogleOta/GoogleOtaUnzipChecksum;->deleteCrashFile(Ljava/lang/String;)V

    invoke-direct/range {p0 .. p0}, Lcom/mediatek/GoogleOta/HttpManager;->onDownloadPackageUnzipAndCheck()V

    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/mediatek/GoogleOta/HttpManager;->mIsDownloading:Z

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/GoogleOta/HttpManager;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    const-string v20, ""

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lcom/mediatek/GoogleOta/HttpManager;->isNetworkAvailable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_2

    invoke-direct/range {p0 .. p0}, Lcom/mediatek/GoogleOta/HttpManager;->sendErrorMessage()V

    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/mediatek/GoogleOta/HttpManager;->mIsDownloading:Z

    goto :goto_0

    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/mediatek/GoogleOta/HttpManager;->sdcardCheck()Z

    move-result v19

    if-nez v19, :cond_3

    invoke-direct/range {p0 .. p0}, Lcom/mediatek/GoogleOta/HttpManager;->sendErrorMessage()V

    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/mediatek/GoogleOta/HttpManager;->mIsDownloading:Z

    goto :goto_0

    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/GoogleOta/HttpManager;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    move-object/from16 v19, v0

    const/16 v20, 0x2

    invoke-virtual/range {v19 .. v20}, Lcom/mediatek/GoogleOta/DownloadStatus;->setDLSessionStatus(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/GoogleOta/HttpManager;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f040003

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {p0 .. p0}, Lcom/mediatek/GoogleOta/HttpManager;->getToken()Ljava/lang/String;

    move-result-object v16

    if-nez v16, :cond_4

    const/16 v19, 0x3ea

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    invoke-direct/range {p0 .. p0}, Lcom/mediatek/GoogleOta/HttpManager;->sendErrorMessage()V

    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/mediatek/GoogleOta/HttpManager;->mIsDownloading:Z

    goto/16 :goto_0

    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/GoogleOta/HttpManager;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/mediatek/GoogleOta/DownloadStatus;->getDownloadDescriptor()Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/GoogleOta/HttpManager;->mNewVersionInfo:Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    new-instance v15, Lorg/apache/http/message/BasicNameValuePair;

    const-string v19, "token"

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-direct {v15, v0, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v6, Lorg/apache/http/message/BasicNameValuePair;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/GoogleOta/HttpManager;->mNewVersionInfo:Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-boolean v0, v0, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->isFullPkg:Z

    move/from16 v19, v0

    if-eqz v19, :cond_7

    const-string v19, "versionId"

    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/GoogleOta/HttpManager;->mNewVersionInfo:Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->pkgId:I

    move/from16 v20, v0

    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v6, v0, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v19, "GoogleOta/HttpManager"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "onDownloadImage, deltaId = "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/GoogleOta/HttpManager;->mNewVersionInfo:Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->pkgId:I

    move/from16 v21, v0

    invoke-static/range {v21 .. v21}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v3, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/GoogleOta/HttpManager;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/mediatek/GoogleOta/Util;->getPackageFileName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/mediatek/GoogleOta/Util;->getFileSize(Ljava/lang/String;)J

    move-result-wide v4

    const-wide/16 v19, 0x400

    sub-long v4, v4, v19

    const-wide/16 v19, 0x0

    cmp-long v19, v4, v19

    if-gez v19, :cond_5

    const-wide/16 v4, 0x0

    :cond_5
    new-instance v13, Lorg/apache/http/message/BasicNameValuePair;

    const-string v19, "HTTP_RANGE"

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v13, v0, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/GoogleOta/HttpManager;->mNewVersionInfo:Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-boolean v0, v0, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->isFullPkg:Z

    move/from16 v19, v0

    if-eqz v19, :cond_6

    new-instance v18, Lorg/apache/http/message/BasicNameValuePair;

    const-string v19, "version"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/GoogleOta/HttpManager;->mNewVersionInfo:Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->version:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-direct/range {v18 .. v20}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/GoogleOta/HttpManager;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f040002

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    :cond_6
    const/16 v19, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2, v3}, Lcom/mediatek/GoogleOta/HttpManager;->doPost(Ljava/lang/String;Ljava/util/Map;Ljava/util/ArrayList;)Lorg/apache/http/HttpResponse;

    move-result-object v11

    if-nez v11, :cond_8

    const-string v19, "GoogleOta/HttpManager"

    const-string v20, "onDownloadImage: response = null"

    invoke-static/range {v19 .. v20}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct/range {p0 .. p0}, Lcom/mediatek/GoogleOta/HttpManager;->sendErrorMessage()V

    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/mediatek/GoogleOta/HttpManager;->mIsDownloading:Z

    goto/16 :goto_0

    :cond_7
    const-string v19, "deltaId"

    goto/16 :goto_1

    :cond_8
    invoke-interface {v11}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v14

    invoke-interface {v14}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v19

    const/16 v20, 0xc8

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_a

    invoke-interface {v14}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v19

    const/16 v20, 0xce

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_a

    const-string v19, "GoogleOta/HttpManager"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "onDownloadImage, ReasonPhrase = "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-interface {v14}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", status.getStatusCode() = "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-interface {v14}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v19, 0x0

    sput-object v19, Lcom/mediatek/GoogleOta/HttpManager;->cookies:Lorg/apache/http/client/CookieStore;

    invoke-interface {v14}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v19

    const/16 v20, 0x194

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_9

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/GoogleOta/HttpManager;->resetDescriptionInfo()V

    const/16 v19, 0x76c

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    :goto_2
    invoke-direct/range {p0 .. p0}, Lcom/mediatek/GoogleOta/HttpManager;->sendErrorMessage()V

    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/mediatek/GoogleOta/HttpManager;->mIsDownloading:Z

    goto/16 :goto_0

    :cond_9
    const/16 v19, 0x3ea

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    goto :goto_2

    :cond_a
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v19

    const/16 v20, 0x1

    invoke-virtual/range {v19 .. v20}, Ljava/lang/Thread;->setPriority(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v4, v5}, Lcom/mediatek/GoogleOta/HttpManager;->writeFile(Lorg/apache/http/HttpResponse;J)I

    move-result v12

    const-string v19, "GoogleOta/HttpManager"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "onDownloadImage, download result = "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v12, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/GoogleOta/HttpManager;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/mediatek/GoogleOta/DownloadStatus;->getDLSessionStatus()I

    move-result v7

    const/16 v19, 0x4

    move/from16 v0, v19

    if-eq v7, v0, :cond_b

    if-nez v7, :cond_c

    :cond_b
    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/mediatek/GoogleOta/HttpManager;->mIsDownloading:Z

    goto/16 :goto_0

    :cond_c
    const/16 v19, 0x76d

    move/from16 v0, v19

    if-ne v12, v0, :cond_d

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/GoogleOta/HttpManager;->resetDescriptionInfo()V

    invoke-direct/range {p0 .. p0}, Lcom/mediatek/GoogleOta/HttpManager;->sendErrorMessage()V

    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/mediatek/GoogleOta/HttpManager;->mIsDownloading:Z

    goto/16 :goto_0

    :cond_d
    const/16 v19, 0x7d1

    move/from16 v0, v19

    if-ne v12, v0, :cond_e

    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/mediatek/GoogleOta/HttpManager;->mIsDownloading:Z

    goto/16 :goto_0

    :cond_e
    invoke-direct/range {p0 .. p0}, Lcom/mediatek/GoogleOta/HttpManager;->onDownloadPackageUnzipAndCheck()V

    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/mediatek/GoogleOta/HttpManager;->mIsDownloading:Z

    goto/16 :goto_0
.end method

.method onDownloadPause()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/mediatek/GoogleOta/DownloadStatus;->setDLSessionStatus(I)V

    return-void
.end method

.method onDownloadProcessUpdate()V
    .locals 6

    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    invoke-virtual {v0}, Lcom/mediatek/GoogleOta/DownloadStatus;->getDownloadDescriptor()Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mNewVersionInfo:Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mNotification:Lcom/mediatek/GoogleOta/NotifyManager;

    iget-object v1, p0, Lcom/mediatek/GoogleOta/HttpManager;->mNewVersionInfo:Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    iget-object v1, v1, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->version:Ljava/lang/String;

    iget-object v2, p0, Lcom/mediatek/GoogleOta/HttpManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/mediatek/GoogleOta/Util;->getPackageFileName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/mediatek/GoogleOta/Util;->getFileSize(Ljava/lang/String;)J

    move-result-wide v2

    long-to-double v2, v2

    iget-object v4, p0, Lcom/mediatek/GoogleOta/HttpManager;->mNewVersionInfo:Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    iget-wide v4, v4, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->size:J

    long-to-double v4, v4

    div-double/2addr v2, v4

    const-wide/high16 v4, 0x4059000000000000L

    mul-double/2addr v2, v4

    double-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/GoogleOta/NotifyManager;->showDownloadingNotificaton(Ljava/lang/String;I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method onPackageUnzipping()V
    .locals 3

    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/mediatek/GoogleOta/DownloadStatus;->setDLSessionStatus(I)V

    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x17

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    return-void
.end method

.method onQueryNewVersion()V
    .locals 7

    const/4 v6, 0x1

    invoke-virtual {p0}, Lcom/mediatek/GoogleOta/HttpManager;->resetDescriptionInfo()V

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/HttpManager;->onQuery()Z

    move-result v1

    const-string v3, "GoogleOta/HttpManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onQueryNewVersion, hasNewVersion = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v3, p0, Lcom/mediatek/GoogleOta/HttpManager;->mQueryAbort:Z

    if-eqz v3, :cond_0

    const-string v3, "GoogleOta/HttpManager"

    const-string v4, "onQueryNewVersion, query abort"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyy-MM-dd"

    invoke-direct {v0, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/GoogleOta/HttpManager;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    invoke-virtual {v3, v2}, Lcom/mediatek/GoogleOta/DownloadStatus;->setQueryDate(Ljava/lang/String;)V

    if-eqz v1, :cond_2

    iget-object v3, p0, Lcom/mediatek/GoogleOta/HttpManager;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    iget-object v4, p0, Lcom/mediatek/GoogleOta/HttpManager;->mNewVersionInfo:Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    invoke-virtual {v3, v4}, Lcom/mediatek/GoogleOta/DownloadStatus;->setDownloadDesctiptor(Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;)V

    iget-object v3, p0, Lcom/mediatek/GoogleOta/HttpManager;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    invoke-virtual {v3, v6}, Lcom/mediatek/GoogleOta/DownloadStatus;->setDLSessionStatus(I)V

    iget-object v3, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/GoogleOta/HttpManager;->mNotification:Lcom/mediatek/GoogleOta/NotifyManager;

    iget-object v4, p0, Lcom/mediatek/GoogleOta/HttpManager;->mNewVersionInfo:Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    iget-object v4, v4, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->version:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/mediatek/GoogleOta/NotifyManager;->showNewVersionNotification(Ljava/lang/String;)V

    const-string v3, "GoogleOta/HttpManager"

    const-string v4, "onQueryNewVersion, Show new version notification."

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const-string v3, "GoogleOta/HttpManager"

    const-string v4, "onQueryNewVersion, Send new version founded message."

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/mediatek/GoogleOta/HttpManager;->sendErrorMessage()V

    goto :goto_0
.end method

.method onSendCloseClientMessage()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/GoogleOta/HttpManager;->mOTAresult:I

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/HttpManager;->sendCheckOTAMessage()V

    return-void
.end method

.method onSetAbort(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/GoogleOta/HttpManager;->mQueryAbort:Z

    return-void
.end method

.method onSetMessageHandler(Landroid/os/Handler;)V
    .locals 3
    .param p1    # Landroid/os/Handler;

    const-string v0, "GoogleOta/HttpManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSetMessageHandler, mHandler = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method onSetRebootRecoveryFlag()Z
    .locals 8

    const/4 v5, 0x1

    const/4 v4, 0x0

    const-string v6, "GoogleOta/HttpManager"

    const-string v7, "onSetRebootRecoveryFlag"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    const-string v6, "GoogleOtaBinder"

    invoke-static {v6}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/mediatek/GoogleOta/GoogleOtaBinder$Stub;->asInterface(Landroid/os/IBinder;)Lcom/mediatek/GoogleOta/GoogleOtaBinder;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v5, "GoogleOta/HttpManager"

    const-string v6, "agent is null"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v4

    :cond_0
    invoke-interface {v0}, Lcom/mediatek/GoogleOta/GoogleOtaBinder;->clearUpdateResult()Z

    move-result v6

    if-nez v6, :cond_1

    const-string v5, "GoogleOta/HttpManager"

    const-string v6, "clearUpdateResult() false"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-interface {v0}, Lcom/mediatek/GoogleOta/GoogleOtaBinder;->setRebootFlag()Z

    move-result v6

    if-nez v6, :cond_2

    const-string v5, "GoogleOta/HttpManager"

    const-string v6, "setRebootFlag() false"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    iget-object v6, p0, Lcom/mediatek/GoogleOta/HttpManager;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/mediatek/GoogleOta/DownloadStatus;->setUpgradeStartedState(Z)V

    new-instance v3, Landroid/content/Intent;

    const-string v6, "android.intent.action.GoogleOta.WriteCommandService"

    invoke-direct {v3, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v6, "COMMANDPART2"

    const-string v7, "/sdcard/googleota/update.zip"

    invoke-virtual {v3, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v6, p0, Lcom/mediatek/GoogleOta/HttpManager;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v3}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move v4, v5

    goto :goto_0
.end method

.method resetDescriptionInfo()V
    .locals 7

    const/4 v6, 0x0

    const/4 v5, -0x1

    const/4 v4, 0x0

    const-string v2, "GoogleOta/HttpManager"

    const-string v3, "resetDescriptionInfo"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x1

    iput v2, p0, Lcom/mediatek/GoogleOta/HttpManager;->mNeedServiceDo:I

    iget-object v2, p0, Lcom/mediatek/GoogleOta/HttpManager;->mContext:Landroid/content/Context;

    invoke-direct {p0, v2}, Lcom/mediatek/GoogleOta/HttpManager;->doBindService(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/mediatek/GoogleOta/HttpManager;->doActionUseService()V

    :cond_0
    iget-object v2, p0, Lcom/mediatek/GoogleOta/HttpManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/mediatek/GoogleOta/Util;->getPackagePathName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/mediatek/GoogleOta/Util;->deleteFile(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/GoogleOta/HttpManager;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    invoke-virtual {v2, v5}, Lcom/mediatek/GoogleOta/DownloadStatus;->setDownLoadPercent(I)V

    iget-object v2, p0, Lcom/mediatek/GoogleOta/HttpManager;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    invoke-virtual {v2, v4}, Lcom/mediatek/GoogleOta/DownloadStatus;->setDLSessionUnzipState(Z)V

    iget-object v2, p0, Lcom/mediatek/GoogleOta/HttpManager;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    invoke-virtual {v2, v4}, Lcom/mediatek/GoogleOta/DownloadStatus;->setDLSessionRenameState(Z)V

    iget-object v2, p0, Lcom/mediatek/GoogleOta/HttpManager;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    invoke-virtual {v2, v4}, Lcom/mediatek/GoogleOta/DownloadStatus;->setDLSessionStatus(I)V

    iget-object v2, p0, Lcom/mediatek/GoogleOta/HttpManager;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    invoke-virtual {v2}, Lcom/mediatek/GoogleOta/DownloadStatus;->getDownloadDescriptor()Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;

    move-result-object v0

    if-eqz v0, :cond_1

    iput v5, v0, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->pkgId:I

    const-wide/16 v2, -0x1

    iput-wide v2, v0, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->size:J

    iput-object v6, v0, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->newNote:Ljava/lang/String;

    iput-object v6, v0, Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;->version:Ljava/lang/String;

    iget-object v2, p0, Lcom/mediatek/GoogleOta/HttpManager;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    invoke-virtual {v2, v0}, Lcom/mediatek/GoogleOta/DownloadStatus;->setDownloadDesctiptor(Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;)V

    :cond_1
    return-void
.end method

.method writeFile(Lorg/apache/http/HttpResponse;J)I
    .locals 19
    .param p1    # Lorg/apache/http/HttpResponse;
    .param p2    # J

    const-string v14, "GoogleOta/HttpManager"

    const-string v15, "writeFile"

    invoke-static {v14, v15}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-interface/range {p1 .. p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v14

    invoke-interface {v14}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v7

    new-instance v6, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/GoogleOta/HttpManager;->mContext:Landroid/content/Context;

    invoke-static {v14}, Lcom/mediatek/GoogleOta/Util;->getPackagePathName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v6, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v14

    if-nez v14, :cond_0

    invoke-virtual {v6}, Ljava/io/File;->mkdirs()Z
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    :cond_0
    const/4 v9, 0x0

    :try_start_1
    new-instance v10, Ljava/io/RandomAccessFile;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/GoogleOta/HttpManager;->mContext:Landroid/content/Context;

    invoke-static {v14}, Lcom/mediatek/GoogleOta/Util;->getPackageFileName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v14

    const-string v15, "rws"

    invoke-direct {v10, v14, v15}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_2

    :try_start_2
    move-wide/from16 v0, p2

    invoke-virtual {v10, v0, v1}, Ljava/io/RandomAccessFile;->seek(J)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/net/SocketTimeoutException; {:try_start_2 .. :try_end_2} :catch_2

    const/16 v14, 0x1000

    :try_start_3
    new-array v2, v14, [B

    const/4 v11, 0x0

    const/4 v5, 0x0

    const/4 v8, 0x0

    const/4 v12, 0x0

    const/4 v4, 0x0

    :goto_0
    const/4 v14, 0x0

    const/16 v15, 0x1000

    invoke-virtual {v7, v2, v14, v15}, Ljava/io/InputStream;->read([BII)I
    :try_end_3
    .catch Ljava/net/SocketTimeoutException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    move-result v11

    if-lez v11, :cond_2

    const/4 v14, 0x0

    :try_start_4
    invoke-virtual {v10, v2, v14, v11}, Ljava/io/RandomAccessFile;->write([BII)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_4 .. :try_end_4} :catch_2

    add-int/lit8 v5, v5, 0x1

    :try_start_5
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/GoogleOta/HttpManager;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    invoke-virtual {v14}, Lcom/mediatek/GoogleOta/DownloadStatus;->getDLSessionStatus()I

    move-result v13

    const/4 v14, 0x4

    if-eq v13, v14, :cond_1

    if-nez v13, :cond_4

    :cond_1
    const-string v14, "GoogleOta/HttpManager"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "writeFile, DownloadStatus = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v14, 0x0

    sput-object v14, Lcom/mediatek/GoogleOta/HttpManager;->cookies:Lorg/apache/http/client/CookieStore;

    const/4 v4, 0x0

    :cond_2
    const-string v14, "GoogleOta/HttpManager"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "writeFile, finish, rc = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "bytes"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ". finish = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v4, :cond_3

    invoke-direct/range {p0 .. p0}, Lcom/mediatek/GoogleOta/HttpManager;->onTransferRatio()V

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/GoogleOta/HttpManager;->onDownloadProcessUpdate()V

    :cond_3
    invoke-virtual {v10}, Ljava/io/RandomAccessFile;->close()V

    invoke-direct/range {p0 .. p0}, Lcom/mediatek/GoogleOta/HttpManager;->onShutdownConn()V

    const/4 v14, 0x0

    :goto_1
    return v14

    :catch_0
    move-exception v3

    :goto_2
    invoke-virtual {v3}, Ljava/lang/Throwable;->printStackTrace()V

    invoke-direct/range {p0 .. p0}, Lcom/mediatek/GoogleOta/HttpManager;->onShutdownConn()V

    const/16 v14, 0x76d

    move-object/from16 v0, p0

    iput v14, v0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    goto :goto_1

    :catch_1
    move-exception v3

    invoke-virtual {v3}, Ljava/lang/Throwable;->printStackTrace()V

    invoke-direct/range {p0 .. p0}, Lcom/mediatek/GoogleOta/HttpManager;->onShutdownConn()V

    const/16 v14, 0x76d

    move-object/from16 v0, p0

    iput v14, v0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    goto :goto_1

    :cond_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    if-nez v14, :cond_8

    if-eqz v12, :cond_5

    const/16 v5, 0xc8

    const/4 v12, 0x0

    :cond_5
    const/16 v14, 0xc8

    if-ne v5, v14, :cond_6

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/GoogleOta/HttpManager;->onDownloadProcessUpdate()V

    const/4 v5, 0x0

    :cond_6
    :goto_3
    add-int/lit8 v8, v8, 0x1

    const/16 v14, 0x14

    if-ne v8, v14, :cond_7

    invoke-direct/range {p0 .. p0}, Lcom/mediatek/GoogleOta/HttpManager;->onTransferRatio()V

    const/4 v8, 0x0

    :cond_7
    const/4 v4, 0x1

    goto/16 :goto_0

    :cond_8
    if-nez v12, :cond_9

    const/16 v5, 0x12

    const/4 v12, 0x1

    :cond_9
    const/16 v14, 0x14

    if-ne v5, v14, :cond_6

    const/4 v5, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/GoogleOta/HttpManager;->onDownloadProcessUpdate()V
    :try_end_5
    .catch Ljava/net/SocketTimeoutException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_3

    :catch_2
    move-exception v3

    invoke-virtual {v3}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    if-eqz v14, :cond_a

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/mediatek/GoogleOta/HttpManager;->mHandler:Landroid/os/Handler;

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    invoke-virtual/range {v15 .. v18}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_a
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/GoogleOta/HttpManager;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    invoke-virtual {v14}, Lcom/mediatek/GoogleOta/DownloadStatus;->getDLSessionStatus()I

    move-result v14

    const/4 v15, 0x2

    if-ne v14, v15, :cond_b

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/GoogleOta/HttpManager;->mDownloadStatus:Lcom/mediatek/GoogleOta/DownloadStatus;

    const/4 v15, 0x4

    invoke-virtual {v14, v15}, Lcom/mediatek/GoogleOta/DownloadStatus;->setDLSessionStatus(I)V

    const-string v14, "GoogleOta/HttpManager"

    const-string v15, "writeFile, exception to set pause state"

    invoke-static {v14, v15}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_b
    invoke-direct/range {p0 .. p0}, Lcom/mediatek/GoogleOta/HttpManager;->onShutdownConn()V

    const/16 v14, 0x7d1

    move-object/from16 v0, p0

    iput v14, v0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/mediatek/GoogleOta/HttpManager;->mErrorCode:I

    goto/16 :goto_1

    :catch_3
    move-exception v3

    invoke-virtual {v3}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_4

    :catch_4
    move-exception v3

    move-object v9, v10

    goto/16 :goto_2
.end method
