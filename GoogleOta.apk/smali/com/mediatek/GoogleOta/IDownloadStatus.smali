.class interface abstract Lcom/mediatek/GoogleOta/IDownloadStatus;
.super Ljava/lang/Object;
.source "IDownloadStatus.java"


# static fields
.field public static final MSG_CKSUM_ERROR:I = 0x16

.field public static final MSG_DELTADELETED:I = 0x8

.field public static final MSG_DLPKGCOMPLETE:I = 0x3

.field public static final MSG_DLPKGUPGRADE:I = 0x4

.field public static final MSG_NETWORKERROR:I = 0x0

.field public static final MSG_NEWVERSIONDETECTED:I = 0x1

.field public static final MSG_NONEWVERSIONDETECTED:I = 0x2

.field public static final MSG_NOTSUPPORT:I = 0x5

.field public static final MSG_NOTSUPPORT_TEMP:I = 0x6

.field public static final MSG_NOVERSIONINFO:I = 0x7

.field public static final MSG_OTA_CLOSECLIENTUI:I = 0x12

.field public static final MSG_OTA_NEEDFULLPACKAGE:I = 0xf

.field public static final MSG_OTA_PACKAGEERROR:I = 0xd

.field public static final MSG_OTA_RUNCHECKERROR:I = 0xe

.field public static final MSG_OTA_SDCARDERROR:I = 0x14

.field public static final MSG_OTA_SDCARDINFUFFICENT:I = 0x13

.field public static final MSG_OTA_USERDATAERROR:I = 0x10

.field public static final MSG_OTA_USERDATAINSUFFICENT:I = 0x11

.field public static final MSG_SDCARDCRASHORUNMOUNT:I = 0x9

.field public static final MSG_SDCARDINSUFFICENT:I = 0xb

.field public static final MSG_SDCARDUNKNOWNERROR:I = 0xa

.field public static final MSG_UNKNOWERROR:I = 0xc

.field public static final MSG_UNZIP_ERROR:I = 0x15

.field public static final MSG_UNZIP_LODING:I = 0x17

.field public static final STATE_CANCELDOWNLOAD:I = 0x3

.field public static final STATE_DLPKGCOMPLETE:I = 0x5

.field public static final STATE_DOWNLOADING:I = 0x2

.field public static final STATE_NEWVERSION_READY:I = 0x1

.field public static final STATE_PACKAGEERROR:I = 0x6

.field public static final STATE_PACKAGEUNZIPPING:I = 0x7

.field public static final STATE_PAUSEDOWNLOAD:I = 0x4

.field public static final STATE_QUERYNEWVERSION:I


# virtual methods
.method public abstract getDLSessionStatus()I
.end method

.method public abstract getDownloadDescriptor()Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;
.end method

.method public abstract getUpdateImageSize()J
.end method

.method public abstract getVersion()Ljava/lang/String;
.end method

.method public abstract setDLSessionStatus(I)V
.end method

.method public abstract setDownloadDesctiptor(Lcom/mediatek/GoogleOta/Util$DownloadDescriptor;)V
.end method
