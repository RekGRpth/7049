.class Lcom/mediatek/GoogleOta/NotifyManager;
.super Ljava/lang/Object;
.source "NotifyManager.java"


# static fields
.field static final NOTIFY_DL_COMPLETED:I = 0x3

.field static final NOTIFY_DOWNLOADING:I = 0x2

.field static final NOTIFY_NEW_VERSION:I = 0x1

.field static final OTA_CLIENT_INTENT:Ljava/lang/String; = "com.mediatek.intent.GoogleOtaClient"

.field static final TAG:Ljava/lang/String; = "NotifyManager:"


# instance fields
.field private mNotification:Landroid/app/Notification$Builder;

.field private mNotificationContext:Landroid/content/Context;

.field private mNotificationManager:Landroid/app/NotificationManager;

.field private mNotificationType:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mediatek/GoogleOta/NotifyManager;->mNotificationContext:Landroid/content/Context;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/GoogleOta/NotifyManager;->mNotification:Landroid/app/Notification$Builder;

    iget-object v0, p0, Lcom/mediatek/GoogleOta/NotifyManager;->mNotificationContext:Landroid/content/Context;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/mediatek/GoogleOta/NotifyManager;->mNotificationManager:Landroid/app/NotificationManager;

    return-void
.end method

.method private configAndShowNotification(ILjava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # I
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    new-instance v0, Landroid/app/Notification$Builder;

    iget-object v1, p0, Lcom/mediatek/GoogleOta/NotifyManager;->mNotificationContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/GoogleOta/NotifyManager;->mNotification:Landroid/app/Notification$Builder;

    iget-object v0, p0, Lcom/mediatek/GoogleOta/NotifyManager;->mNotification:Landroid/app/Notification$Builder;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/GoogleOta/NotifyManager;->mNotification:Landroid/app/Notification$Builder;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-direct {p0, p4}, Lcom/mediatek/GoogleOta/NotifyManager;->getPendingIntenActivity(Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    iget-object v0, p0, Lcom/mediatek/GoogleOta/NotifyManager;->mNotificationManager:Landroid/app/NotificationManager;

    iget v1, p0, Lcom/mediatek/GoogleOta/NotifyManager;->mNotificationType:I

    iget-object v2, p0, Lcom/mediatek/GoogleOta/NotifyManager;->mNotification:Landroid/app/Notification$Builder;

    invoke-virtual {v2}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/GoogleOta/NotifyManager;->mNotification:Landroid/app/Notification$Builder;

    goto :goto_0
.end method

.method private getPendingIntenActivity(Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 5
    .param p1    # Ljava/lang/String;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/GoogleOta/NotifyManager;->mNotificationContext:Landroid/content/Context;

    const/4 v3, 0x0

    const/high16 v4, 0x8000000

    invoke-static {v2, v3, v1, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private setNotificationProgress(ILjava/lang/String;I)V
    .locals 4
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # I

    const/4 v3, 0x1

    iget-object v1, p0, Lcom/mediatek/GoogleOta/NotifyManager;->mNotification:Landroid/app/Notification$Builder;

    if-nez v1, :cond_1

    new-instance v1, Landroid/app/Notification$Builder;

    iget-object v2, p0, Lcom/mediatek/GoogleOta/NotifyManager;->mNotificationContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/mediatek/GoogleOta/NotifyManager;->mNotification:Landroid/app/Notification$Builder;

    iget-object v1, p0, Lcom/mediatek/GoogleOta/NotifyManager;->mNotification:Landroid/app/Notification$Builder;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/mediatek/GoogleOta/NotifyManager;->mNotification:Landroid/app/Notification$Builder;

    invoke-virtual {v1, v3}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v1

    const-string v2, "com.mediatek.intent.GoogleOtaClient"

    invoke-direct {p0, v2}, Lcom/mediatek/GoogleOta/NotifyManager;->getPendingIntenActivity(Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/GoogleOta/NotifyManager;->mNotification:Landroid/app/Notification$Builder;

    const/16 v2, 0x64

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p3, v3}, Landroid/app/Notification$Builder;->setProgress(IIZ)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setContentInfo(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    iget-object v1, p0, Lcom/mediatek/GoogleOta/NotifyManager;->mNotificationManager:Landroid/app/NotificationManager;

    iget v2, p0, Lcom/mediatek/GoogleOta/NotifyManager;->mNotificationType:I

    iget-object v3, p0, Lcom/mediatek/GoogleOta/NotifyManager;->mNotification:Landroid/app/Notification$Builder;

    invoke-virtual {v3}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0
.end method


# virtual methods
.method public clearNotification(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/GoogleOta/NotifyManager;->mNotificationManager:Landroid/app/NotificationManager;

    invoke-virtual {v0, p1}, Landroid/app/NotificationManager;->cancel(I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/GoogleOta/NotifyManager;->mNotification:Landroid/app/Notification$Builder;

    return-void
.end method

.method showDownloadCompletedNotification(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x3

    iput v2, p0, Lcom/mediatek/GoogleOta/NotifyManager;->mNotificationType:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/mediatek/GoogleOta/NotifyManager;->mNotificationContext:Landroid/content/Context;

    const v4, 0x7f040007

    invoke-virtual {v3, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/GoogleOta/NotifyManager;->mNotificationContext:Landroid/content/Context;

    const v3, 0x7f040008

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f020001

    const-string v3, "com.mediatek.intent.GoogleOtaClient"

    invoke-direct {p0, v2, v1, v0, v3}, Lcom/mediatek/GoogleOta/NotifyManager;->configAndShowNotification(ILjava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method showDownloadingNotificaton(Ljava/lang/String;I)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v1, 0x2

    iput v1, p0, Lcom/mediatek/GoogleOta/NotifyManager;->mNotificationType:I

    iget-object v1, p0, Lcom/mediatek/GoogleOta/NotifyManager;->mNotificationContext:Landroid/content/Context;

    const v2, 0x7f040007

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f020003

    invoke-direct {p0, v1, v0, p2}, Lcom/mediatek/GoogleOta/NotifyManager;->setNotificationProgress(ILjava/lang/String;I)V

    return-void
.end method

.method showNewVersionNotification(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x1

    iput v1, p0, Lcom/mediatek/GoogleOta/NotifyManager;->mNotificationType:I

    iget-object v1, p0, Lcom/mediatek/GoogleOta/NotifyManager;->mNotificationContext:Landroid/content/Context;

    const v2, 0x7f040007

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    const v1, 0x7f020002

    const-string v2, "com.mediatek.intent.GoogleOtaClient"

    invoke-direct {p0, v1, v0, p1, v2}, Lcom/mediatek/GoogleOta/NotifyManager;->configAndShowNotification(ILjava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
