.class public final Lcom/mediatek/GoogleOta/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/GoogleOta/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final address_check_version:I = 0x7f040001

.field public static final address_download_delta:I = 0x7f040003

.field public static final address_download_full:I = 0x7f040002

.field public static final address_login:I = 0x7f040000

.field public static final address_update_result:I = 0x7f040004

.field public static final app_name:I = 0x7f040006

.field public static final btn_cancel:I = 0x7f04001c

.field public static final btn_download:I = 0x7f04001a

.field public static final btn_ok:I = 0x7f04001b

.field public static final btn_pause:I = 0x7f040018

.field public static final btn_resume:I = 0x7f040019

.field public static final btn_retry:I = 0x7f040017

.field public static final cancel_download_content:I = 0x7f040020

.field public static final cancel_download_negative_btn:I = 0x7f040022

.field public static final cancel_download_positive_btn:I = 0x7f040021

.field public static final cancel_download_title:I = 0x7f04001f

.field public static final delta_has_deleted:I = 0x7f040026

.field public static final downloading_title:I = 0x7f04001d

.field public static final error_sdcard:I = 0x7f040013

.field public static final gms_load_flag:I = 0x7f040005

.field public static final insufficient_space:I = 0x7f040016

.field public static final insufficient_space_content:I = 0x7f040012

.field public static final insufficient_space_title:I = 0x7f040011

.field public static final need_target_info:I = 0x7f040025

.field public static final network_error:I = 0x7f04000d

.field public static final network_error_content:I = 0x7f04000e

.field public static final new_version_query:I = 0x7f040009

.field public static final no_new_version:I = 0x7f04000a

.field public static final not_support_version:I = 0x7f040024

.field public static final notification_content_text_completed:I = 0x7f040008

.field public static final notification_content_title:I = 0x7f040007

.field public static final package_error_message_crash:I = 0x7f04002a

.field public static final package_error_message_full:I = 0x7f04002b

.field public static final package_error_message_insuff:I = 0x7f040029

.field public static final package_error_message_invalid:I = 0x7f040028

.field public static final package_error_title:I = 0x7f040027

.field public static final package_unzip:I = 0x7f04002f

.field public static final package_unzip_error:I = 0x7f040030

.field public static final sdcard_crash_or_unmount:I = 0x7f040015

.field public static final server_error_content:I = 0x7f040010

.field public static final server_error_title:I = 0x7f04000f

.field public static final unknown_error:I = 0x7f04000b

.field public static final unknown_error_content:I = 0x7f04000c

.field public static final unmount_sdcard:I = 0x7f040014

.field public static final updateFailed:I = 0x7f04002e

.field public static final updateResultTitle:I = 0x7f04002c

.field public static final updateSuccess:I = 0x7f04002d

.field public static final update_prompt:I = 0x7f040023

.field public static final ver_content:I = 0x7f04001e


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
