.class public Lcom/mediatek/googleota/sysoper/SysOperService;
.super Landroid/app/Service;
.source "SysOperService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/googleota/sysoper/SysOperService$IncomingHandler;
    }
.end annotation


# static fields
.field private static final CMD_FILE_KEY:Ljava/lang/String; = "COMMANDFILE"

.field private static final MSG_DELETE_COMMANDFILE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "GoogleOta/SysOper"


# instance fields
.field private final mMessenger:Landroid/os/Messenger;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Landroid/os/Messenger;

    new-instance v1, Lcom/mediatek/googleota/sysoper/SysOperService$IncomingHandler;

    invoke-direct {v1, p0}, Lcom/mediatek/googleota/sysoper/SysOperService$IncomingHandler;-><init>(Lcom/mediatek/googleota/sysoper/SysOperService;)V

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/mediatek/googleota/sysoper/SysOperService;->mMessenger:Landroid/os/Messenger;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/googleota/sysoper/SysOperService;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/googleota/sysoper/SysOperService;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/googleota/sysoper/SysOperService;->deleteCommandFile(Ljava/lang/String;)V

    return-void
.end method

.method private deleteCommandFile(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "GoogleOta/SysOper"

    const-string v2, "deleteCommandFile, command exist, delete it"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/mediatek/googleota/sysoper/SysOperService;->mMessenger:Landroid/os/Messenger;

    invoke-virtual {v0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method
