.class public Lcom/google/android/partnersetup/GservicesChangedReceiver;
.super Landroid/content/BroadcastReceiver;
.source "GservicesChangedReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const-string v0, "GooglePartnerSetup"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GooglePartnerSetup"

    const-string v1, "GservicesChangedReceiver.onReceive"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p1}, Lcom/google/android/partnersetup/AppHiderService;->startSetVisibilityService(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/google/android/partnersetup/ClientIdService;->startSetClientIdService(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/google/android/partnersetup/MccFallbackService;->startSetFallbackMccService(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/google/android/partnersetup/MccOverrideService;->startMccOverrideService(Landroid/content/Context;)V

    return-void
.end method
