.class public Lcom/google/android/partnersetup/RlzDebugViewAppInfo;
.super Ljava/lang/Object;
.source "RlzDebugViewAppInfo.java"

# interfaces
.implements Lcom/google/android/partnersetup/RlzDebugViewListActivity$ViewInfo;


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mResources:Landroid/content/res/Resources;

.field private sortOrderMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1
    .param p1    # Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/partnersetup/RlzDebugViewAppInfo;->mResources:Landroid/content/res/Resources;

    iput-object p1, p0, Lcom/google/android/partnersetup/RlzDebugViewAppInfo;->mActivity:Landroid/app/Activity;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/partnersetup/RlzDebugViewAppInfo;)Landroid/app/Activity;
    .locals 1
    .param p0    # Lcom/google/android/partnersetup/RlzDebugViewAppInfo;

    iget-object v0, p0, Lcom/google/android/partnersetup/RlzDebugViewAppInfo;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/partnersetup/RlzDebugViewAppInfo;J)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/partnersetup/RlzDebugViewAppInfo;
    .param p1    # J

    invoke-direct {p0, p1, p2}, Lcom/google/android/partnersetup/RlzDebugViewAppInfo;->getMoreInfo(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/partnersetup/RlzDebugViewAppInfo;)Landroid/content/res/Resources;
    .locals 1
    .param p0    # Lcom/google/android/partnersetup/RlzDebugViewAppInfo;

    iget-object v0, p0, Lcom/google/android/partnersetup/RlzDebugViewAppInfo;->mResources:Landroid/content/res/Resources;

    return-object v0
.end method

.method private getMoreInfo(J)Ljava/lang/String;
    .locals 12
    .param p1    # J

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v3, 0x0

    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v8

    const-string v0, "app_name"

    aput-object v0, v2, v9

    const-string v0, "current_rlz"

    aput-object v0, v2, v10

    const-string v0, "status"

    aput-object v0, v2, v11

    sget-object v0, Lcom/google/android/partnersetup/RlzProtocol$Apps;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/partnersetup/RlzDebugViewAppInfo;->mActivity:Landroid/app/Activity;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/app/Activity;->managedQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/google/android/partnersetup/RlzDebugViewAppInfo;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f030028

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ": "

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v6, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-virtual {v7, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, "\n"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/android/partnersetup/RlzDebugViewAppInfo;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f030029

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ": "

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v6, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\n"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/android/partnersetup/RlzDebugViewAppInfo;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f03002a

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ": "

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\n"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/android/partnersetup/RlzDebugViewAppInfo;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f030023

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ": "

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/android/partnersetup/RlzDebugViewAppInfo;->mResources:Landroid/content/res/Resources;

    invoke-interface {v6, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Lcom/google/android/partnersetup/RlzProtocol$Apps;->getResourceIdForStatus(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/partnersetup/RlzDebugViewAppInfo;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f030027

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public buildItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1

    new-instance v0, Lcom/google/android/partnersetup/RlzDebugViewAppInfo$1;

    invoke-direct {v0, p0}, Lcom/google/android/partnersetup/RlzDebugViewAppInfo$1;-><init>(Lcom/google/android/partnersetup/RlzDebugViewAppInfo;)V

    return-object v0
.end method

.method public getActivityTitleResource()I
    .locals 1

    const v0, 0x7f030017

    return v0
.end method

.method public getContentUri()Landroid/net/Uri;
    .locals 1

    sget-object v0, Lcom/google/android/partnersetup/RlzProtocol$Apps;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method public getDefaultSortOrder()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getListEmptyStringResource()I
    .locals 1

    const v0, 0x7f030018

    return v0
.end method

.method public getOutColumns()[I
    .locals 1

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    return-object v0

    nop

    :array_0
    .array-data 4
        0x1020014
        0x1020014
        0x1020014
        0x1020015
    .end array-data
.end method

.method public getProjection()[Ljava/lang/String;
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "app_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "status"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "current_rlz"

    aput-object v2, v0, v1

    return-object v0
.end method

.method public getSortColumns()[Ljava/lang/String;
    .locals 4

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/partnersetup/RlzDebugViewAppInfo;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f030029

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/partnersetup/RlzDebugViewAppInfo;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f030023

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    return-object v0
.end method

.method public getSortOrderMap()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/partnersetup/RlzDebugViewAppInfo;->sortOrderMap:Ljava/util/Map;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/partnersetup/RlzDebugViewAppInfo;->sortOrderMap:Ljava/util/Map;

    iget-object v0, p0, Lcom/google/android/partnersetup/RlzDebugViewAppInfo;->sortOrderMap:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "app_name ASC"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/partnersetup/RlzDebugViewAppInfo;->sortOrderMap:Ljava/util/Map;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "status ASC"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v0, p0, Lcom/google/android/partnersetup/RlzDebugViewAppInfo;->sortOrderMap:Ljava/util/Map;

    return-object v0
.end method

.method public getViewBinder()Landroid/widget/SimpleCursorAdapter$ViewBinder;
    .locals 1

    new-instance v0, Lcom/google/android/partnersetup/RlzDebugViewAppInfo$2;

    invoke-direct {v0, p0}, Lcom/google/android/partnersetup/RlzDebugViewAppInfo$2;-><init>(Lcom/google/android/partnersetup/RlzDebugViewAppInfo;)V

    return-object v0
.end method
