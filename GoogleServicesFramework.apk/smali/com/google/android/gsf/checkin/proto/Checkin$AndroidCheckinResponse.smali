.class public final Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Checkin.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gsf/checkin/proto/Checkin;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AndroidCheckinResponse"
.end annotation


# instance fields
.field private androidId_:J

.field private cachedSize:I

.field private deleteSetting_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private digest_:Ljava/lang/String;

.field private hasAndroidId:Z

.field private hasDigest:Z

.field private hasMarketOk:Z

.field private hasSecurityToken:Z

.field private hasSettingsDiff:Z

.field private hasStatsOk:Z

.field private hasTimeMsec:Z

.field private intent_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gsf/checkin/proto/Logs$AndroidIntentProto;",
            ">;"
        }
    .end annotation
.end field

.field private marketOk_:Z

.field private securityToken_:J

.field private setting_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gsf/checkin/proto/Checkin$GservicesSetting;",
            ">;"
        }
    .end annotation
.end field

.field private settingsDiff_:Z

.field private statsOk_:Z

.field private timeMsec_:J


# direct methods
.method public constructor <init>()V
    .locals 4

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-boolean v1, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->statsOk_:Z

    iput-wide v2, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->timeMsec_:J

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->intent_:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->digest_:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->settingsDiff_:Z

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->deleteSetting_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->setting_:Ljava/util/List;

    iput-boolean v1, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->marketOk_:Z

    iput-wide v2, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->androidId_:J

    iput-wide v2, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->securityToken_:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addDeleteSetting(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->deleteSetting_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->deleteSetting_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->deleteSetting_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addIntent(Lcom/google/android/gsf/checkin/proto/Logs$AndroidIntentProto;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;
    .locals 1
    .param p1    # Lcom/google/android/gsf/checkin/proto/Logs$AndroidIntentProto;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->intent_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->intent_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->intent_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addSetting(Lcom/google/android/gsf/checkin/proto/Checkin$GservicesSetting;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;
    .locals 1
    .param p1    # Lcom/google/android/gsf/checkin/proto/Checkin$GservicesSetting;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->setting_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->setting_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->setting_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getAndroidId()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->androidId_:J

    return-wide v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->cachedSize:I

    return v0
.end method

.method public getDeleteSetting(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->deleteSetting_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getDeleteSettingCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->deleteSetting_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getDeleteSettingList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->deleteSetting_:Ljava/util/List;

    return-object v0
.end method

.method public getDigest()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->digest_:Ljava/lang/String;

    return-object v0
.end method

.method public getIntent(I)Lcom/google/android/gsf/checkin/proto/Logs$AndroidIntentProto;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->intent_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidIntentProto;

    return-object v0
.end method

.method public getIntentCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->intent_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getIntentList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gsf/checkin/proto/Logs$AndroidIntentProto;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->intent_:Ljava/util/List;

    return-object v0
.end method

.method public getMarketOk()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->marketOk_:Z

    return v0
.end method

.method public getSecurityToken()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->securityToken_:J

    return-wide v0
.end method

.method public getSerializedSize()I
    .locals 7

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->hasStatsOk()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->getStatsOk()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->getIntentList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gsf/checkin/proto/Logs$AndroidIntentProto;

    const/4 v4, 0x2

    invoke-static {v4, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->hasTimeMsec()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->getTimeMsec()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->hasDigest()Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x4

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->getDigest()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->getSettingList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gsf/checkin/proto/Checkin$GservicesSetting;

    const/4 v4, 0x5

    invoke-static {v4, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->hasMarketOk()Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v4, 0x6

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->getMarketOk()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->hasAndroidId()Z

    move-result v4

    if-eqz v4, :cond_6

    const/4 v4, 0x7

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->getAndroidId()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFixed64Size(IJ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->hasSecurityToken()Z

    move-result v4

    if-eqz v4, :cond_7

    const/16 v4, 0x8

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->getSecurityToken()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFixed64Size(IJ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->hasSettingsDiff()Z

    move-result v4

    if-eqz v4, :cond_8

    const/16 v4, 0x9

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->getSettingsDiff()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_8
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->getDeleteSettingList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_2

    :cond_9
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->getDeleteSettingList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->cachedSize:I

    return v3
.end method

.method public getSetting(I)Lcom/google/android/gsf/checkin/proto/Checkin$GservicesSetting;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->setting_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gsf/checkin/proto/Checkin$GservicesSetting;

    return-object v0
.end method

.method public getSettingCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->setting_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getSettingList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gsf/checkin/proto/Checkin$GservicesSetting;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->setting_:Ljava/util/List;

    return-object v0
.end method

.method public getSettingsDiff()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->settingsDiff_:Z

    return v0
.end method

.method public getStatsOk()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->statsOk_:Z

    return v0
.end method

.method public getTimeMsec()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->timeMsec_:J

    return-wide v0
.end method

.method public hasAndroidId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->hasAndroidId:Z

    return v0
.end method

.method public hasDigest()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->hasDigest:Z

    return v0
.end method

.method public hasMarketOk()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->hasMarketOk:Z

    return v0
.end method

.method public hasSecurityToken()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->hasSecurityToken:Z

    return v0
.end method

.method public hasSettingsDiff()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->hasSettingsDiff:Z

    return v0
.end method

.method public hasStatsOk()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->hasStatsOk:Z

    return v0
.end method

.method public hasTimeMsec()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->hasTimeMsec:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->setStatsOk(Z)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/android/gsf/checkin/proto/Logs$AndroidIntentProto;

    invoke-direct {v1}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidIntentProto;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->addIntent(Lcom/google/android/gsf/checkin/proto/Logs$AndroidIntentProto;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->setTimeMsec(J)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->setDigest(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/android/gsf/checkin/proto/Checkin$GservicesSetting;

    invoke-direct {v1}, Lcom/google/android/gsf/checkin/proto/Checkin$GservicesSetting;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->addSetting(Lcom/google/android/gsf/checkin/proto/Checkin$GservicesSetting;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->setMarketOk(Z)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFixed64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->setAndroidId(J)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFixed64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->setSecurityToken(J)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->setSettingsDiff(Z)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->addDeleteSetting(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x39 -> :sswitch_7
        0x41 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;

    move-result-object v0

    return-object v0
.end method

.method public setAndroidId(J)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->hasAndroidId:Z

    iput-wide p1, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->androidId_:J

    return-object p0
.end method

.method public setDigest(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->hasDigest:Z

    iput-object p1, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->digest_:Ljava/lang/String;

    return-object p0
.end method

.method public setMarketOk(Z)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->hasMarketOk:Z

    iput-boolean p1, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->marketOk_:Z

    return-object p0
.end method

.method public setSecurityToken(J)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->hasSecurityToken:Z

    iput-wide p1, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->securityToken_:J

    return-object p0
.end method

.method public setSettingsDiff(Z)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->hasSettingsDiff:Z

    iput-boolean p1, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->settingsDiff_:Z

    return-object p0
.end method

.method public setStatsOk(Z)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->hasStatsOk:Z

    iput-boolean p1, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->statsOk_:Z

    return-object p0
.end method

.method public setTimeMsec(J)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->hasTimeMsec:Z

    iput-wide p1, p0, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->timeMsec_:J

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->hasStatsOk()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->getStatsOk()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->getIntentList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidIntentProto;

    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->hasTimeMsec()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->getTimeMsec()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->hasDigest()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->getDigest()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->getSettingList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gsf/checkin/proto/Checkin$GservicesSetting;

    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->hasMarketOk()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->getMarketOk()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->hasAndroidId()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->getAndroidId()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFixed64(IJ)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->hasSecurityToken()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->getSecurityToken()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFixed64(IJ)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->hasSettingsDiff()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->getSettingsDiff()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->getDeleteSettingList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/16 v2, 0xa

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    goto :goto_2

    :cond_9
    return-void
.end method
