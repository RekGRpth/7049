.class public Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;
.super Lcom/google/android/gsf/gtalkservice/Endpoint;
.source "AndroidEndpoint.java"


# instance fields
.field private d2cmWhitelist:[Ljava/lang/String;

.field private enableD2cm:I

.field private mAlarmManager:Landroid/app/AlarmManager;

.field private mConnectivityManager:Landroid/net/ConnectivityManager;

.field private mEnableIdleNotification:Z

.field private mEnablePowerStateNotification:Z

.field private mEnableSignalsProtocol:Z

.field protected mHeartbeatAlarm:Lcom/google/android/gsf/gtalkservice/HeartbeatAlarm;

.field protected mLastDataMessageReceivedTs:J

.field protected mLastDataMessageSentTs:J


# direct methods
.method public constructor <init>(Lcom/google/android/gsf/gtalkservice/service/GTalkService;Lcom/google/android/gsf/gtalkservice/Account;Landroid/os/Looper;)V
    .locals 2
    .param p1    # Lcom/google/android/gsf/gtalkservice/service/GTalkService;
    .param p2    # Lcom/google/android/gsf/gtalkservice/Account;
    .param p3    # Landroid/os/Looper;

    const/4 v1, 0x0

    invoke-direct {p0, p1, p3}, Lcom/google/android/gsf/gtalkservice/Endpoint;-><init>(Lcom/google/android/gsf/gtalkservice/service/GTalkService;Landroid/os/Looper;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mHeartbeatAlarm:Lcom/google/android/gsf/gtalkservice/HeartbeatAlarm;

    iput-boolean v1, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mEnableIdleNotification:Z

    iput-boolean v1, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mEnablePowerStateNotification:Z

    invoke-virtual {p0, p2}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->init(Lcom/google/android/gsf/gtalkservice/Account;)V

    return-void
.end method

.method private asyncCloseConnection(Lorg/jivesoftware/smack/XMPPConnection;)V
    .locals 2
    .param p1    # Lorg/jivesoftware/smack/XMPPConnection;

    new-instance v0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint$1;

    const-string v1, "close-XMPP-connection"

    invoke-direct {v0, p0, v1, p1}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint$1;-><init>(Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;Ljava/lang/String;Lorg/jivesoftware/smack/XMPPConnection;)V

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint$1;->start()V

    return-void
.end method

.method private clearHeartbeatAlarm()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mHeartbeatAlarm:Lcom/google/android/gsf/gtalkservice/HeartbeatAlarm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mHeartbeatAlarm:Lcom/google/android/gsf/gtalkservice/HeartbeatAlarm;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/HeartbeatAlarm;->clearAlarm()V

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private getJidResourceFromSettings()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mSettingsQueryMap:Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;

    invoke-virtual {v0}, Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;->getJidResource()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private initHeartbeatAlarm()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mHeartbeatAlarm:Lcom/google/android/gsf/gtalkservice/HeartbeatAlarm;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mAccount:Lcom/google/android/gsf/gtalkservice/Account;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/Account;->getActiveHeartbeatInterval()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mAccount:Lcom/google/android/gsf/gtalkservice/Account;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/Account;->getIdleHeartbeatInterval()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mAccount:Lcom/google/android/gsf/gtalkservice/Account;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/Account;->getSyncHeartbeatInterval()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mAccount:Lcom/google/android/gsf/gtalkservice/Account;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/Account;->getDefaultHeartbeatInterval()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mAccount:Lcom/google/android/gsf/gtalkservice/Account;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/Account;->getHeartbeatAckTimeout()I

    move-result v0

    if-lez v0, :cond_0

    new-instance v0, Lcom/google/android/gsf/gtalkservice/HeartbeatAlarm;

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mAlarmManager:Landroid/app/AlarmManager;

    invoke-direct {v0, v1, v2, p0}, Lcom/google/android/gsf/gtalkservice/HeartbeatAlarm;-><init>(Landroid/content/Context;Landroid/app/AlarmManager;Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;)V

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mHeartbeatAlarm:Lcom/google/android/gsf/gtalkservice/HeartbeatAlarm;

    :cond_0
    return-void
.end method

.method private login(Lorg/jivesoftware/smack/XMPPConnection;Ljava/lang/String;)Ljava/lang/String;
    .locals 22
    .param p1    # Lorg/jivesoftware/smack/XMPPConnection;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/jivesoftware/smack/XMPPException;
        }
    .end annotation

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mAccount:Lcom/google/android/gsf/gtalkservice/Account;

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/Account;->getUsername()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mAccount:Lcom/google/android/gsf/gtalkservice/Account;

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/Account;->getAuthToken()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mAccount:Lcom/google/android/gsf/gtalkservice/Account;

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/Account;->getDeviceId()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mAccount:Lcom/google/android/gsf/gtalkservice/Account;

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/Account;->getUseAndroidId()Z

    move-result v3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mAccount:Lcom/google/android/gsf/gtalkservice/Account;

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/Account;->getLoginSettings()Ljava/util/Map;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lorg/jivesoftware/smack/XMPPConnection;->addConnectionListener(Lorg/jivesoftware/smack/ConnectionListener;)V

    invoke-virtual/range {p0 .. p1}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->setConnection(Lorg/jivesoftware/smack/XMPPConnection;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v6, "gtalk_compress2"

    const/4 v8, 0x0

    invoke-static {v2, v6, v8}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v16

    const/4 v2, 0x1

    move/from16 v0, v16

    if-ne v0, v2, :cond_0

    invoke-virtual/range {p1 .. p1}, Lorg/jivesoftware/smack/XMPPConnection;->getConnectionConfiguration()Lorg/jivesoftware/smack/ConnectionConfiguration;

    move-result-object v2

    const/4 v6, 0x1

    invoke-virtual {v2, v6}, Lorg/jivesoftware/smack/ConnectionConfiguration;->setCompressionEnabled(Z)V

    :cond_0
    const-wide/16 v9, -0x1

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->getRmq2Manager()Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->processAndRetrieveInitialS2dIds()Ljava/util/List;

    move-result-object v12

    invoke-direct/range {p0 .. p0}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->initHeartbeatAlarm()V

    const/16 v18, 0x0

    invoke-direct/range {p0 .. p0}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->shouldUploadHeartbeatStat()Z

    move-result v2

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mHeartbeatAlarm:Lcom/google/android/gsf/gtalkservice/HeartbeatAlarm;

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/HeartbeatAlarm;->getHeartbeatStat()Lorg/jivesoftware/smack/packet/HeartbeatStat;

    move-result-object v18

    const-string v2, "GTalkService/c"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Heartbeat stat uploaded: "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v6}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->logv(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->getAccountId()J

    move-result-wide v14

    const-string v2, "GTalkService/c"

    const-string v6, "AndroidEndpoint.login"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v6}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->logv(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v8, 0x0

    const/4 v11, 0x1

    const/4 v13, 0x0

    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->getReconnectManager()Lcom/google/android/gsf/gtalkservice/ReconnectManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->getNetworkType()I

    move-result v20

    move-object/from16 v2, p1

    move-object/from16 v6, p2

    invoke-virtual/range {v2 .. v20}, Lorg/jivesoftware/smack/XMPPConnection;->login(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJZLjava/util/List;ZJIZLorg/jivesoftware/smack/packet/HeartbeatStat;Ljava/util/Map;I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mHeartbeatAlarm:Lcom/google/android/gsf/gtalkservice/HeartbeatAlarm;

    invoke-virtual/range {p1 .. p1}, Lorg/jivesoftware/smack/XMPPConnection;->getHeartbeatConfig()Lorg/jivesoftware/smack/packet/HeartbeatConfig;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/google/android/gsf/gtalkservice/HeartbeatAlarm;->initStat(Lorg/jivesoftware/smack/packet/HeartbeatConfig;)V

    const-string v2, "GTalkService/c"

    const-string v6, "AndroidEndpoint.login successful"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v6}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->logd(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lorg/jivesoftware/smack/XMPPConnection;->getUser()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private makeXmppConnection(Ljava/lang/String;ILjava/lang/String;)Lorg/jivesoftware/smack/XMPPConnection;
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/jivesoftware/smack/XMPPException;
        }
    .end annotation

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    if-gez p2, :cond_1

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "EMPTY HOSTNAME or INVALID PORT("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "), cannot make connection!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->loge(Ljava/lang/String;)V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mLastConnectionAttemptSuccessful:Z

    new-instance v1, Lorg/jivesoftware/smack/XMPPException;

    new-instance v2, Ljava/net/UnknownHostException;

    const-string v4, "empty hostname/invalid port"

    invoke-direct {v2, v4}, Ljava/net/UnknownHostException;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lorg/jivesoftware/smack/XMPPException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_1
    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "gtalk_ssl_handshake_timeout_ms"

    const v4, 0xea60

    invoke-static {v1, v2, v4}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    const-string v1, "GTalkService/c"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "makeXmppConnection: sslhandshake timeout="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->logv(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Landroid/net/SSLSessionCache;

    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/net/SSLSessionCache;-><init>(Landroid/content/Context;)V

    invoke-static {v3, v1}, Landroid/net/SSLCertificateSocketFactory;->getDefault(ILandroid/net/SSLSessionCache;)Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v6

    new-instance v0, Lorg/jivesoftware/smack/SSLXMPPConnection;

    const/4 v4, 0x1

    move-object v1, p1

    move v2, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v6}, Lorg/jivesoftware/smack/SSLXMPPConnection;-><init>(Ljava/lang/String;IIZLjava/lang/String;Ljavax/net/SocketFactory;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->getAccountId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lorg/jivesoftware/smack/SSLXMPPConnection;->setDefaultAccountId(J)V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->bumpConnectionsAttemptedCount()V

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mSimpleWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0, v1}, Lorg/jivesoftware/smack/SSLXMPPConnection;->setWakeLock(Landroid/os/PowerManager$WakeLock;)V

    invoke-virtual {v0}, Lorg/jivesoftware/smack/SSLXMPPConnection;->connect()V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    const-wide/16 v4, 0x64

    div-long/2addr v1, v4

    long-to-int v1, v1

    invoke-virtual {v0, v1}, Lorg/jivesoftware/smack/SSLXMPPConnection;->setLocalConnectionId(I)V

    return-object v0
.end method

.method private saveJidResourceToSettings(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mSettingsQueryMap:Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;->setJidResource(Ljava/lang/String;)V

    return-void
.end method

.method private setD2cmGservices()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "enable_d2cm"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->enableD2cm:I

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "d2cm_whitelist"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->d2cmWhitelist:[Ljava/lang/String;

    return-void
.end method

.method private shouldUploadHeartbeatStat()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mHeartbeatAlarm:Lcom/google/android/gsf/gtalkservice/HeartbeatAlarm;

    if-eqz v0, :cond_0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private startHeartbeatAlarm(Lorg/jivesoftware/smack/XMPPConnection;)V
    .locals 1
    .param p1    # Lorg/jivesoftware/smack/XMPPConnection;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mHeartbeatAlarm:Lcom/google/android/gsf/gtalkservice/HeartbeatAlarm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mHeartbeatAlarm:Lcom/google/android/gsf/gtalkservice/HeartbeatAlarm;

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/HeartbeatAlarm;->setConnection(Lorg/jivesoftware/smack/XMPPConnection;)V

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mHeartbeatAlarm:Lcom/google/android/gsf/gtalkservice/HeartbeatAlarm;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/HeartbeatAlarm;->startAlarm()V

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mHeartbeatAlarm:Lcom/google/android/gsf/gtalkservice/HeartbeatAlarm;

    invoke-virtual {p1, v0}, Lorg/jivesoftware/smack/XMPPConnection;->setHeartbeatAlarm(Lcom/google/android/gsf/gtalkservice/HeartbeatAlarm;)V

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public broadcastConnected()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.GTALK_CONNECTED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "account"

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->getUsername()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "resource"

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->getJidResource()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method public broadcastDisconnected(I)V
    .locals 3
    .param p1    # I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.GTALK_DISCONNECTED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "account"

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->getUsername()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "err"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method protected closeXmppConnection(Lorg/jivesoftware/smack/XMPPConnection;Z)V
    .locals 1
    .param p1    # Lorg/jivesoftware/smack/XMPPConnection;
    .param p2    # Z

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p2, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->asyncCloseConnection(Lorg/jivesoftware/smack/XMPPConnection;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/jivesoftware/smack/XMPPConnection;->close(Z)V

    goto :goto_0
.end method

.method protected connectionEstablishedDelegate(Lorg/jivesoftware/smack/XMPPConnection;)V
    .locals 0
    .param p1    # Lorg/jivesoftware/smack/XMPPConnection;

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->maybeSendSignals()V

    invoke-direct {p0, p1}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->startHeartbeatAlarm(Lorg/jivesoftware/smack/XMPPConnection;)V

    return-void
.end method

.method protected createAsyncWakelockTag()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GTALK_ASYNC_CONN_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mAsyncWakelockTag:Ljava/lang/String;

    return-void
.end method

.method protected doConnectDelegate(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/jivesoftware/smack/XMPPException;
        }
    .end annotation

    const-string v1, "GTalkService/c"

    const-string v2, "doConnectDelegate: making main connection"

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->logd(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->makeXmppConnection(Ljava/lang/String;ILjava/lang/String;)Lorg/jivesoftware/smack/XMPPConnection;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->doXMPPConnectionWriteLock()V

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-virtual {v1, v0}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->setXMPPConnection(Lorg/jivesoftware/smack/XMPPConnection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->doXMPPConnectionWriteUnlock()V

    invoke-direct {p0, v0, p4}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->login(Lorg/jivesoftware/smack/XMPPConnection;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->doXMPPConnectionWriteUnlock()V

    throw v1
.end method

.method protected doConnectFailedDelegate()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->getReconnectManager()Lcom/google/android/gsf/gtalkservice/ReconnectManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->reportInetCondition(Z)V

    return-void
.end method

.method protected doConnectSucceededDelegate()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->getRmq2Manager()Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->getAccountId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->resendPacketsForAccount(J)V

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->getReconnectManager()Lcom/google/android/gsf/gtalkservice/ReconnectManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->reportInetCondition(Z)V

    return-void
.end method

.method public dump(Ljava/io/PrintWriter;)V
    .locals 13
    .param p1    # Ljava/io/PrintWriter;

    const-wide/16 v11, 0x3e8

    const-wide/16 v9, 0x0

    const-string v5, "Type: Android Endpoint"

    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Host: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mAccount:Lcom/google/android/gsf/gtalkservice/Account;

    invoke-virtual {v6}, Lcom/google/android/gsf/gtalkservice/Account;->getHost()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mAccount:Lcom/google/android/gsf/gtalkservice/Account;

    invoke-virtual {v6}, Lcom/google/android/gsf/gtalkservice/Account;->getPort()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->dump(Ljava/io/PrintWriter;)V

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mDataMessageManager:Lcom/google/android/gsf/gtalkservice/DataMessageManager;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->getWakeLock()Landroid/os/PowerManager$WakeLock;

    move-result-object v4

    if-eqz v4, :cond_0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "GOOGLE_DATA_MESSAGING wakelock "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4}, Landroid/os/PowerManager$WakeLock;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_0
    iget-object v5, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mHeartbeatAlarm:Lcom/google/android/gsf/gtalkservice/HeartbeatAlarm;

    if-eqz v5, :cond_1

    const-string v5, ""

    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mHeartbeatAlarm:Lcom/google/android/gsf/gtalkservice/HeartbeatAlarm;

    invoke-virtual {v5, p1}, Lcom/google/android/gsf/gtalkservice/HeartbeatAlarm;->dump(Ljava/io/PrintWriter;)V

    :cond_1
    const-string v5, ""

    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string v5, "c2dm"

    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string v5, "------------------"

    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-wide v5, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mLastDataMessageReceivedTs:J

    cmp-long v5, v5, v9

    if-eqz v5, :cond_2

    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    iget-wide v5, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mLastDataMessageReceivedTs:J

    invoke-virtual {v1, v5, v6}, Landroid/text/format/Time;->set(J)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    iget-wide v7, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mLastDataMessageReceivedTs:J

    sub-long/2addr v5, v7

    div-long v2, v5, v11

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Last c2dm msg received at "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%H:%M:%S"

    invoke-virtual {v1, v6}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v2, v3}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ago)"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :goto_0
    const-string v5, ""

    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string v5, "d2cm"

    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string v5, "------------------"

    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-wide v5, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mLastDataMessageSentTs:J

    cmp-long v5, v5, v9

    if-eqz v5, :cond_3

    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    iget-wide v5, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mLastDataMessageSentTs:J

    invoke-virtual {v1, v5, v6}, Landroid/text/format/Time;->set(J)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    iget-wide v7, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mLastDataMessageSentTs:J

    sub-long/2addr v5, v7

    div-long v2, v5, v11

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Last d2cm msg sent at "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%H:%M:%S"

    invoke-virtual {v1, v6}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v2, v3}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ago)"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :goto_1
    invoke-virtual {p0, p1}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->dumpConnectionHistory(Ljava/io/PrintWriter;)V

    return-void

    :cond_2
    const-string v5, "Never received a c2dm msg!"

    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string v5, "Never sent a d2cm msg!"

    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public ensureRouteOverMobileHipriNetworkInterface()Z
    .locals 7

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->isConnected()Z

    move-result v5

    if-nez v5, :cond_0

    :goto_0
    return v3

    :cond_0
    iget-object v5, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mConnection:Lorg/jivesoftware/smack/XMPPConnection;

    invoke-virtual {v5}, Lorg/jivesoftware/smack/XMPPConnection;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v0

    const/4 v5, 0x3

    aget-byte v5, v0, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x18

    const/4 v6, 0x2

    aget-byte v6, v0, v6

    and-int/lit16 v6, v6, 0xff

    shl-int/lit8 v6, v6, 0x10

    or-int/2addr v5, v6

    aget-byte v6, v0, v4

    and-int/lit16 v6, v6, 0xff

    shl-int/lit8 v6, v6, 0x8

    or-int/2addr v5, v6

    aget-byte v3, v0, v3

    and-int/lit16 v3, v3, 0xff

    or-int v2, v5, v3

    const-string v3, "GTalkService/c"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "--- requestRouteToHost for TYPE_MOBILE_HIPRI, host_addr="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v3, v5}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->logd(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mConnectivityManager:Landroid/net/ConnectivityManager;

    const/4 v5, 0x5

    invoke-virtual {v3, v5, v2}, Landroid/net/ConnectivityManager;->requestRouteToHost(II)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "GTalkService/c"

    const-string v5, "requestRouteToHost: failed!"

    invoke-virtual {p0, v3, v5}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->logd(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    move v3, v4

    goto :goto_0
.end method

.method public getDataMessageManager()Lcom/google/android/gsf/gtalkservice/DataMessageManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mDataMessageManager:Lcom/google/android/gsf/gtalkservice/DataMessageManager;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected getResourcePrefix()Ljava/lang/String;
    .locals 1

    const-string v0, "android"

    return-object v0
.end method

.method public getShowAwayOnIdle()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mSettingsQueryMap:Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;

    invoke-virtual {v0}, Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;->getShowAwayOnIdle()Z

    move-result v0

    return v0
.end method

.method public handleMCSDataMessage(Lorg/jivesoftware/smack/packet/DataMessage;)V
    .locals 5
    .param p1    # Lorg/jivesoftware/smack/packet/DataMessage;

    invoke-virtual {p1}, Lorg/jivesoftware/smack/packet/DataMessage;->getAppDataIterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/jivesoftware/smack/packet/DataMessage$AppData;

    invoke-virtual {v0}, Lorg/jivesoftware/smack/packet/DataMessage$AppData;->getKey()Ljava/lang/String;

    move-result-object v2

    const-string v4, "IdleNotification"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v0}, Lorg/jivesoftware/smack/packet/DataMessage$AppData;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mEnableIdleNotification:Z

    goto :goto_0

    :cond_1
    const-string v4, "PowerNotification"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lorg/jivesoftware/smack/packet/DataMessage$AppData;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mEnablePowerStateNotification:Z

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->maybeSendSignals()V

    return-void
.end method

.method public init(Lcom/google/android/gsf/gtalkservice/Account;)V
    .locals 3
    .param p1    # Lcom/google/android/gsf/gtalkservice/Account;

    invoke-super {p0, p1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->init(Lcom/google/android/gsf/gtalkservice/Account;)V

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mContext:Landroid/content/Context;

    const-string v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mAlarmManager:Landroid/app/AlarmManager;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mContext:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mConnectivityManager:Landroid/net/ConnectivityManager;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->getRmq2Manager()Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->setPacketSender(Lcom/google/android/gsf/gtalkservice/PacketSender;)V

    new-instance v0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mServiceHandler:Landroid/os/Handler;

    invoke-direct {v0, v1, p0, v2}, Lcom/google/android/gsf/gtalkservice/DataMessageManager;-><init>(Landroid/content/Context;Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mDataMessageManager:Lcom/google/android/gsf/gtalkservice/DataMessageManager;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "enable_signals"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mEnableSignalsProtocol:Z

    invoke-direct {p0}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->setD2cmGservices()V

    return-void
.end method

.method protected initJidResource()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->getJidResourceFromSettings()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->computeJIDResource()Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->setJidResource(Ljava/lang/String;)V

    return-void
.end method

.method public isD2cmEnabled()Z
    .locals 1

    iget v0, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->enableD2cm:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isD2cmWhitelisted()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->enableD2cm:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isIdleNotificationEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mEnableIdleNotification:Z

    return v0
.end method

.method public isPackageNameWhitelisted(Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->d2cmWhitelist:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->d2cmWhitelist:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_1
    return v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public isPowerNotificationEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mEnablePowerStateNotification:Z

    return v0
.end method

.method protected logConnectionClosed(III)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I

    invoke-static {p1, p2, p3}, Lcom/google/android/gsf/gtalkservice/LogTag;->logConnectionClosed(III)V

    return-void
.end method

.method protected logConnectionEvent(IIII)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-static {p1, p2, p3, p4}, Lcom/google/android/gsf/gtalkservice/LogTag;->logConnectionEvent(IIII)V

    return-void
.end method

.method public login()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->login()V

    return-void
.end method

.method public logout()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logout()V

    invoke-direct {p0}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->clearHeartbeatAlarm()V

    return-void
.end method

.method public maybeSendSignals()V
    .locals 5

    iget-boolean v2, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mEnableSignalsProtocol:Z

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v1, Lorg/jivesoftware/smack/packet/DataMessage;

    invoke-direct {v1}, Lorg/jivesoftware/smack/packet/DataMessage;-><init>()V

    const/4 v0, 0x0

    iget-boolean v2, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mEnablePowerStateNotification:Z

    if-eqz v2, :cond_2

    const/4 v0, 0x1

    new-instance v2, Lorg/jivesoftware/smack/packet/DataMessage$AppData;

    const-string v3, "PowerNotification"

    iget-object v4, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-virtual {v4}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->isDevicePluggedIn()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/jivesoftware/smack/packet/DataMessage$AppData;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lorg/jivesoftware/smack/packet/DataMessage;->addAppData(Lorg/jivesoftware/smack/packet/DataMessage$AppData;)V

    :cond_2
    iget-boolean v2, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mEnableIdleNotification:Z

    if-eqz v2, :cond_3

    const/4 v0, 0x1

    new-instance v2, Lorg/jivesoftware/smack/packet/DataMessage$AppData;

    const-string v3, "IdleNotification"

    iget-object v4, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-virtual {v4}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->isUserIdle()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/jivesoftware/smack/packet/DataMessage$AppData;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lorg/jivesoftware/smack/packet/DataMessage;->addAppData(Lorg/jivesoftware/smack/packet/DataMessage$AppData;)V

    new-instance v2, Lorg/jivesoftware/smack/packet/DataMessage$AppData;

    const-string v3, "ShowAwayOnIdle"

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->getShowAwayOnIdle()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/jivesoftware/smack/packet/DataMessage$AppData;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lorg/jivesoftware/smack/packet/DataMessage;->addAppData(Lorg/jivesoftware/smack/packet/DataMessage$AppData;)V

    :cond_3
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->getAccountId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/jivesoftware/smack/packet/DataMessage;->setFrom(Ljava/lang/String;)V

    const-string v2, "com.google.android.gsf.gtalkservice"

    invoke-virtual {v1, v2}, Lorg/jivesoftware/smack/packet/DataMessage;->setCategory(Ljava/lang/String;)V

    const-string v2, "GTalkService/c"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gsf/gtalkservice/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "GTalkService/c"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sending signals: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->logd(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->sendPacket(Lorg/jivesoftware/smack/packet/Packet;Z)Z

    goto/16 :goto_0
.end method

.method public onConnectionClosed(I)V
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->getReconnectManager()Lcom/google/android/gsf/gtalkservice/ReconnectManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->reportInetCondition(Z)V

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->getXMPPConnection()Lorg/jivesoftware/smack/XMPPConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/jivesoftware/smack/XMPPConnection;->getLocalConnectionId()I

    move-result v1

    if-ne v1, p1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->doXMPPConnectionWriteLock()V

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->setXMPPConnection(Lorg/jivesoftware/smack/XMPPConnection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->doXMPPConnectionWriteUnlock()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->clearHeartbeatAlarm()V

    return-void

    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->doXMPPConnectionWriteUnlock()V

    throw v1
.end method

.method public onGServicesChangeDelegate()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "enable_signals"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mEnableSignalsProtocol:Z

    invoke-direct {p0}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->setD2cmGservices()V

    return-void
.end method

.method protected reachedRetryThresholdForAuthExpiration()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public sendDataMessageStanza(Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mDataMessageManager:Lcom/google/android/gsf/gtalkservice/DataMessageManager;

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->sendDataMessageStanza(Landroid/content/Intent;)V

    return-void
.end method

.method public sendHeartbeatToServer()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "not connected!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mHeartbeatAlarm:Lcom/google/android/gsf/gtalkservice/HeartbeatAlarm;

    if-nez v0, :cond_1

    const-string v0, "GTalkService/c"

    const-string v1, "sendHeartbeatToServer: heartbeat alarm not set for this GTalkConnection"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->logd(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mHeartbeatAlarm:Lcom/google/android/gsf/gtalkservice/HeartbeatAlarm;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/HeartbeatAlarm;->sendHeartbeatToServer()V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public sendIqStanza(Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Intent;

    const-string v0, "GTalkService/c"

    const-string v1, "trying to send an IQ stanza without an endpoint. should not happen."

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->logd(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public sendMessageStanza(Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Intent;

    const-string v0, "GTalkService/c"

    const-string v1, "trying to send a message stanza without an endpoint. should not happen."

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->logd(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public sendPresenceStanza(Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Intent;

    const-string v0, "GTalkService/c"

    const-string v1, "trying to send a presence stanza without an endpoint. should not happen."

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->logd(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected setConnection(Lorg/jivesoftware/smack/XMPPConnection;)V
    .locals 2
    .param p1    # Lorg/jivesoftware/smack/XMPPConnection;

    invoke-super {p0, p1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->setConnection(Lorg/jivesoftware/smack/XMPPConnection;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->getRmq2Manager()Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->initConnection(Lorg/jivesoftware/smack/XMPPConnection;)V

    invoke-virtual {p1, v0}, Lorg/jivesoftware/smack/XMPPConnection;->setRmq2Manager(Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;)V

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mDataMessageManager:Lcom/google/android/gsf/gtalkservice/DataMessageManager;

    invoke-virtual {v1, p1}, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->initConnection(Lorg/jivesoftware/smack/XMPPConnection;)V

    :cond_0
    return-void
.end method

.method protected setJidResource(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-super {p0, p1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->setJidResource(Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->saveJidResourceToSettings(Ljava/lang/String;)V

    return-void
.end method

.method public setLastDataMessageReceived()V
    .locals 2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mLastDataMessageReceivedTs:J

    return-void
.end method

.method public setLastDataMessageSent()V
    .locals 2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->mLastDataMessageSentTs:J

    return-void
.end method
