.class public Lcom/google/android/gsf/gtalkservice/ApplicationEndpoint;
.super Lcom/google/android/gsf/gtalkservice/Endpoint;
.source "ApplicationEndpoint.java"


# instance fields
.field private mBindToSessionServers:Z

.field private mPackageName:Ljava/lang/String;

.field private mPacketManager:Lcom/google/android/gsf/gtalkservice/gtalk/ApplicationEndpointPacketManager;


# direct methods
.method public constructor <init>(Lcom/google/android/gsf/gtalkservice/service/GTalkService;Lcom/google/android/gsf/gtalkservice/Account;Landroid/os/Looper;ZLjava/lang/String;)V
    .locals 1
    .param p1    # Lcom/google/android/gsf/gtalkservice/service/GTalkService;
    .param p2    # Lcom/google/android/gsf/gtalkservice/Account;
    .param p3    # Landroid/os/Looper;
    .param p4    # Z
    .param p5    # Ljava/lang/String;

    invoke-direct {p0, p1, p3}, Lcom/google/android/gsf/gtalkservice/Endpoint;-><init>(Lcom/google/android/gsf/gtalkservice/service/GTalkService;Landroid/os/Looper;)V

    iput-boolean p4, p0, Lcom/google/android/gsf/gtalkservice/ApplicationEndpoint;->mBindToSessionServers:Z

    iput-object p5, p0, Lcom/google/android/gsf/gtalkservice/ApplicationEndpoint;->mPackageName:Ljava/lang/String;

    new-instance v0, Lcom/google/android/gsf/gtalkservice/gtalk/ApplicationEndpointPacketManager;

    invoke-direct {v0, p0}, Lcom/google/android/gsf/gtalkservice/gtalk/ApplicationEndpointPacketManager;-><init>(Lcom/google/android/gsf/gtalkservice/ApplicationEndpoint;)V

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/ApplicationEndpoint;->mPacketManager:Lcom/google/android/gsf/gtalkservice/gtalk/ApplicationEndpointPacketManager;

    invoke-virtual {p0, p2}, Lcom/google/android/gsf/gtalkservice/ApplicationEndpoint;->init(Lcom/google/android/gsf/gtalkservice/Account;)V

    return-void
.end method

.method private setupOnlinePacketListeners()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/ApplicationEndpoint;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/ApplicationEndpoint;->mPacketManager:Lcom/google/android/gsf/gtalkservice/gtalk/ApplicationEndpointPacketManager;

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/ApplicationEndpoint;->getConnection()Lorg/jivesoftware/smack/XMPPConnection;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gsf/gtalkservice/gtalk/ApplicationEndpointPacketManager;->initConnection(Lorg/jivesoftware/smack/XMPPConnection;)V

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected connectionEstablishedDelegate(Lorg/jivesoftware/smack/XMPPConnection;)V
    .locals 2
    .param p1    # Lorg/jivesoftware/smack/XMPPConnection;

    invoke-direct {p0}, Lcom/google/android/gsf/gtalkservice/ApplicationEndpoint;->setupOnlinePacketListeners()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "could not setup packet listeners."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/ApplicationEndpoint;->sendSessionServerBindRequest()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "sending ss-bind request failed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-void
.end method

.method protected createAsyncWakelockTag()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GTALK_ASYNC_CONN_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/ApplicationEndpoint;->getJid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/ApplicationEndpoint;->mAsyncWakelockTag:Ljava/lang/String;

    return-void
.end method

.method protected doConnectDelegate(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/jivesoftware/smack/XMPPException;
        }
    .end annotation

    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/ApplicationEndpoint;->mAccount:Lcom/google/android/gsf/gtalkservice/Account;

    invoke-virtual {v3}, Lcom/google/android/gsf/gtalkservice/Account;->getUsername()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/ApplicationEndpoint;->mAccount:Lcom/google/android/gsf/gtalkservice/Account;

    invoke-virtual {v3}, Lcom/google/android/gsf/gtalkservice/Account;->getAuthToken()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/ApplicationEndpoint;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-virtual {v3}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->getXMPPConnection()Lorg/jivesoftware/smack/XMPPConnection;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/gtalkservice/ApplicationEndpoint;->setConnection(Lorg/jivesoftware/smack/XMPPConnection;)V

    if-nez v0, :cond_0

    const-string v3, "GTalkService/c"

    const-string v7, "doConnectDelegate: null XMPP connection"

    invoke-virtual {p0, v3, v7}, Lcom/google/android/gsf/gtalkservice/ApplicationEndpoint;->logd(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, Lorg/jivesoftware/smack/XMPPException;

    new-instance v7, Ljava/io/IOException;

    const-string v8, "not connected"

    invoke-direct {v7, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v7}, Lorg/jivesoftware/smack/XMPPException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    :cond_0
    invoke-virtual {v0, p0}, Lorg/jivesoftware/smack/XMPPConnection;->addConnectionListener(Lorg/jivesoftware/smack/ConnectionListener;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/ApplicationEndpoint;->getAccountId()J

    move-result-wide v4

    const-string v3, "GTalkService/c"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "bindAccount: username="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", account_id="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v3, v7}, Lcom/google/android/gsf/gtalkservice/ApplicationEndpoint;->logd(Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, p4

    invoke-virtual/range {v0 .. v5}, Lorg/jivesoftware/smack/XMPPConnection;->bindAccount(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v6

    const-string v3, "GTalkService/c"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "bind app endpoint successful for "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", serverJid="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v3, v7}, Lcom/google/android/gsf/gtalkservice/ApplicationEndpoint;->logv(Ljava/lang/String;Ljava/lang/String;)V

    return-object v6
.end method

.method public doConnectSucceededDelegate()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/ApplicationEndpoint;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->getRmq2Manager()Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/ApplicationEndpoint;->getAccountId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->resendPacketsForAccount(J)V

    return-void
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/ApplicationEndpoint;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method protected getResourcePrefix()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/ApplicationEndpoint;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public onConnectionClosed(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public sendIqStanza(Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/ApplicationEndpoint;->mPacketManager:Lcom/google/android/gsf/gtalkservice/gtalk/ApplicationEndpointPacketManager;

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/ApplicationEndpointPacketManager;->sendIqStanza(Landroid/content/Intent;)V

    return-void
.end method

.method public sendMessageStanza(Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/ApplicationEndpoint;->mPacketManager:Lcom/google/android/gsf/gtalkservice/gtalk/ApplicationEndpointPacketManager;

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/ApplicationEndpointPacketManager;->sendMessageStanza(Landroid/content/Intent;)V

    return-void
.end method

.method public sendPresenceStanza(Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/ApplicationEndpoint;->mPacketManager:Lcom/google/android/gsf/gtalkservice/gtalk/ApplicationEndpointPacketManager;

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/ApplicationEndpointPacketManager;->sendPresenceStanza(Landroid/content/Intent;)V

    return-void
.end method

.method public sendSessionServerBindRequest()Z
    .locals 3

    iget-boolean v1, p0, Lcom/google/android/gsf/gtalkservice/ApplicationEndpoint;->mBindToSessionServers:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    new-instance v0, Lcom/google/android/gsf/gtalkservice/extensions/MobileBind;

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/ApplicationEndpoint;->getJid()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gsf/gtalkservice/extensions/MobileBind;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/ApplicationEndpoint;->getAccountId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gsf/gtalkservice/extensions/MobileBind;->setAccountId(J)V

    const-string v1, "GTalkService/c"

    const-string v2, "request ss-bind"

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gsf/gtalkservice/ApplicationEndpoint;->logv(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/gtalkservice/ApplicationEndpoint;->sendPacketOverMcsConnection(Lorg/jivesoftware/smack/packet/Packet;)Z

    move-result v1

    goto :goto_0
.end method
