.class Lcom/google/android/gsf/gtalkservice/DataMessageManager$DataMessageParsingResult;
.super Ljava/lang/Object;
.source "DataMessageManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gsf/gtalkservice/DataMessageManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DataMessageParsingResult"
.end annotation


# instance fields
.field public dataMsg:Lorg/jivesoftware/smack/packet/DataMessage;

.field public intent:Landroid/content/Intent;

.field public isMCS:Z

.field public permission:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Intent;Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/content/Intent;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gsf/gtalkservice/DataMessageManager$DataMessageParsingResult;->intent:Landroid/content/Intent;

    iput-object p2, p0, Lcom/google/android/gsf/gtalkservice/DataMessageManager$DataMessageParsingResult;->permission:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lorg/jivesoftware/smack/packet/DataMessage;)V
    .locals 1
    .param p1    # Lorg/jivesoftware/smack/packet/DataMessage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gsf/gtalkservice/DataMessageManager$DataMessageParsingResult;->isMCS:Z

    iput-object p1, p0, Lcom/google/android/gsf/gtalkservice/DataMessageManager$DataMessageParsingResult;->dataMsg:Lorg/jivesoftware/smack/packet/DataMessage;

    return-void
.end method
