.class public Lcom/google/android/gsf/gtalkservice/DataMessageManager;
.super Ljava/lang/Object;
.source "DataMessageManager.java"

# interfaces
.implements Lorg/jivesoftware/smack/PacketListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gsf/gtalkservice/DataMessageManager$DataMessageParsingResult;,
        Lcom/google/android/gsf/gtalkservice/DataMessageManager$BroadcastDoneReceiver;,
        Lcom/google/android/gsf/gtalkservice/DataMessageManager$MissInfo;
    }
.end annotation


# static fields
.field private static final NON_PACKAGE_CATEGORIES:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final RESERVED_INTENT_KEYS:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mContext:Landroid/content/Context;

.field private mEndpoint:Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;

.field private mHandler:Landroid/os/Handler;

.field private mLockObject:Ljava/lang/Object;

.field private final mReceiverLookupMissInfo:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gsf/gtalkservice/DataMessageManager$MissInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->RESERVED_INTENT_KEYS:Ljava/util/HashSet;

    sget-object v0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->RESERVED_INTENT_KEYS:Ljava/util/HashSet;

    const-string v1, "from"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->NON_PACKAGE_CATEGORIES:Ljava/util/HashSet;

    sget-object v0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->NON_PACKAGE_CATEGORIES:Ljava/util/HashSet;

    const-string v1, "android.intent.category.MASTER_CLEAR"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->NON_PACKAGE_CATEGORIES:Ljava/util/HashSet;

    const-string v1, "android.server.checkin.CHECKIN"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->NON_PACKAGE_CATEGORIES:Ljava/util/HashSet;

    const-string v1, "INSTALL_ASSET"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->NON_PACKAGE_CATEGORIES:Ljava/util/HashSet;

    const-string v1, "REMOVE_ASSET"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->NON_PACKAGE_CATEGORIES:Ljava/util/HashSet;

    const-string v1, "SERVER_NOTIFICATION"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->NON_PACKAGE_CATEGORIES:Ljava/util/HashSet;

    const-string v1, "DECLINE_ASSET"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->NON_PACKAGE_CATEGORIES:Ljava/util/HashSet;

    const-string v1, "com.google.android.gsf.subscribedfeeds"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->NON_PACKAGE_CATEGORIES:Ljava/util/HashSet;

    const-string v1, "com.google.android.gsf"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->NON_PACKAGE_CATEGORIES:Ljava/util/HashSet;

    const-string v1, "com.google.android.apps.googlevoice.INBOX_NOTIFICATION"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;Landroid/os/Handler;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;
    .param p3    # Landroid/os/Handler;

    const/4 v3, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/android/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->mReceiverLookupMissInfo:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->mLockObject:Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->mHandler:Landroid/os/Handler;

    iput-object p2, p0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->mEndpoint:Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->mContext:Landroid/content/Context;

    const-string v2, "power"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const-string v1, "GOOGLE_C2DM"

    invoke-virtual {v0, v3, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1, v3}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    return-void
.end method

.method static synthetic access$000(Ljava/lang/String;)V
    .locals 0
    .param p0    # Ljava/lang/String;

    invoke-static {p0}, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->log(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/gsf/gtalkservice/DataMessageManager;Landroid/content/Intent;)Z
    .locals 1
    .param p0    # Lcom/google/android/gsf/gtalkservice/DataMessageManager;
    .param p1    # Landroid/content/Intent;

    invoke-direct {p0, p1}, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->shouldUnregisterRegIdForApp(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/gsf/gtalkservice/DataMessageManager;Landroid/content/Intent;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/gsf/gtalkservice/DataMessageManager;
    .param p1    # Landroid/content/Intent;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->reportNoReceiverError(Landroid/content/Intent;Ljava/lang/String;)V

    return-void
.end method

.method private static log(Ljava/lang/String;)V
    .locals 3
    .param p0    # Ljava/lang/String;

    const-string v0, "GTalkService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[DataMsgMgr] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private parseDataMessageIntent(Lorg/jivesoftware/smack/packet/DataMessage;)Lcom/google/android/gsf/gtalkservice/DataMessageManager$DataMessageParsingResult;
    .locals 14
    .param p1    # Lorg/jivesoftware/smack/packet/DataMessage;

    const/4 v13, 0x2

    invoke-virtual {p1}, Lorg/jivesoftware/smack/packet/DataMessage;->getFrom()Ljava/lang/String;

    move-result-object v2

    const-string v10, "GTalkService"

    invoke-static {v10, v13}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_0

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "parseDataMessageIntent: from="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->log(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lorg/jivesoftware/smack/packet/DataMessage;->getCategory()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_1

    const-string v10, "GTalkService"

    const-string v11, "[DataMsgMgr] found msg w/o category, dropping"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v10, 0x0

    :goto_0
    return-object v10

    :cond_1
    const-string v10, "com.google.android.gsf.gtalkservice"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    new-instance v10, Lcom/google/android/gsf/gtalkservice/DataMessageManager$DataMessageParsingResult;

    invoke-direct {v10, p1}, Lcom/google/android/gsf/gtalkservice/DataMessageManager$DataMessageParsingResult;-><init>(Lorg/jivesoftware/smack/packet/DataMessage;)V

    goto :goto_0

    :cond_2
    const-string v10, "GSYNC_TICKLE"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    const-string v1, "com.google.android.gsf.subscribedfeeds"

    :cond_3
    iget-object v10, p0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->mEndpoint:Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;

    if-eqz v10, :cond_4

    iget-object v10, p0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->mEndpoint:Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;

    invoke-virtual {v10}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->setLastDataMessageReceived()V

    :cond_4
    new-instance v3, Landroid/content/Intent;

    const-string v10, "com.google.android.c2dm.intent.RECEIVE"

    invoke-direct {v3, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v10, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->NON_PACKAGE_CATEGORIES:Ljava/util/HashSet;

    invoke-virtual {v10, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    invoke-virtual {v3, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    :goto_1
    invoke-virtual {p1}, Lorg/jivesoftware/smack/packet/DataMessage;->getAppDataIterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_9

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/jivesoftware/smack/packet/DataMessage$AppData;

    invoke-virtual {v0}, Lorg/jivesoftware/smack/packet/DataMessage$AppData;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lorg/jivesoftware/smack/packet/DataMessage$AppData;->getValue()Ljava/lang/String;

    move-result-object v9

    sget-object v10, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->RESERVED_INTENT_KEYS:Ljava/util/HashSet;

    invoke-virtual {v10, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_5

    const-string v10, "GOOGLE."

    invoke-virtual {v5, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_7

    :cond_5
    const-string v10, "GTalkService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "[DataMsgMgr] parseDataMessageIntent: not including app data -- key is reserved: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_6
    invoke-virtual {v3, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    :cond_7
    const-string v10, "GTalkService"

    invoke-static {v10, v13}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_8

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "parse intent data: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->log(Ljava/lang/String;)V

    :cond_8
    invoke-virtual {v3, v5, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_2

    :cond_9
    const-string v10, "com.google.android.gsf"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a

    const-string v10, "google.com"

    invoke-virtual {v10, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a

    const-string v10, "registration_id"

    invoke-virtual {v3, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_a

    const-string v10, "com.google.android.c2dm.intent.REGISTRATION"

    invoke-virtual {v3, v10}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v3, v1}, Landroid/content/Intent;->removeCategory(Ljava/lang/String;)V

    const-string v10, "app"

    invoke-virtual {v3, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    :cond_a
    const-string v10, "from"

    invoke-virtual {v3, v10, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v10, "GTalkService"

    invoke-static {v10, v13}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_b

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "parse intent, category="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->log(Ljava/lang/String;)V

    :cond_b
    invoke-virtual {p1}, Lorg/jivesoftware/smack/packet/DataMessage;->getToken()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_c

    const-string v10, "collapse_key"

    invoke-virtual {v3, v10, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_c
    invoke-virtual {p1}, Lorg/jivesoftware/smack/packet/DataMessage;->getPermission()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_10

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ".permission.C2D_MESSAGE"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    :cond_d
    :goto_3
    const-string v10, "INSTALL_ASSET"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_e

    const-string v10, "REMOVE_ASSET"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_e

    const-string v10, "DECLINE_ASSET"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_e

    const-string v10, "UPDATES_AVAILABLE"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_e

    const-string v10, "SERVER_NOTIFICATION"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_f

    :cond_e
    const/4 v6, 0x0

    :cond_f
    new-instance v10, Lcom/google/android/gsf/gtalkservice/DataMessageManager$DataMessageParsingResult;

    invoke-direct {v10, v3, v6}, Lcom/google/android/gsf/gtalkservice/DataMessageManager$DataMessageParsingResult;-><init>(Landroid/content/Intent;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_10
    const-string v10, "NONE"

    invoke-virtual {v10, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_d

    const/4 v6, 0x0

    goto :goto_3
.end method

.method private processSendDataMessageFailed(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.c2dm.intent.ERROR"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "error_type"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    return-void
.end method

.method private reportNoReceiverError(Landroid/content/Intent;Ljava/lang/String;)V
    .locals 6
    .param p1    # Landroid/content/Intent;
    .param p2    # Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Should not happen. Received intent with no package name. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->log(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v2, "GTalkService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Receiver package not found, unregister application "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " sender "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.c2dm.intent.UNREGISTER"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "app"

    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->mContext:Landroid/content/Context;

    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    invoke-static {v3, v5, v4, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v2, "app_gsf"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method private shouldUnregisterRegIdForApp(Landroid/content/Intent;)Z
    .locals 14
    .param p1    # Landroid/content/Intent;

    const/4 v9, 0x1

    const/4 v8, 0x0

    iget-object v10, p0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v10, "c2dm_num_app_lookup_misses_before_unregister"

    invoke-static {v0, v10, v9}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v7

    const-string v10, "c2dm_min_seconds_before_missing_app_unregister"

    const-wide/16 v11, 0x0

    invoke-static {v0, v10, v11, v12}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    mul-long v2, v10, v12

    invoke-virtual {p1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    iget-object v10, p0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->mReceiverLookupMissInfo:Ljava/util/HashMap;

    monitor-enter v10

    :try_start_0
    iget-object v11, p0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->mReceiverLookupMissInfo:Ljava/util/HashMap;

    invoke-virtual {v11, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gsf/gtalkservice/DataMessageManager$MissInfo;

    if-nez v4, :cond_0

    new-instance v4, Lcom/google/android/gsf/gtalkservice/DataMessageManager$MissInfo;

    invoke-direct {v4}, Lcom/google/android/gsf/gtalkservice/DataMessageManager$MissInfo;-><init>()V

    iget-object v11, p0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->mReceiverLookupMissInfo:Ljava/util/HashMap;

    invoke-virtual {v11, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->queryPackageManagerForBroadcastIntentReceiver(Landroid/content/Intent;)Z

    move-result v11

    if-eqz v11, :cond_1

    const/4 v9, 0x0

    iput v9, v4, Lcom/google/android/gsf/gtalkservice/DataMessageManager$MissInfo;->count:I

    monitor-exit v10

    :goto_0
    return v8

    :cond_1
    iget v11, v4, Lcom/google/android/gsf/gtalkservice/DataMessageManager$MissInfo;->count:I

    if-nez v11, :cond_2

    iput-wide v5, v4, Lcom/google/android/gsf/gtalkservice/DataMessageManager$MissInfo;->timeOfFirstMiss:J

    :cond_2
    iget v11, v4, Lcom/google/android/gsf/gtalkservice/DataMessageManager$MissInfo;->count:I

    add-int/lit8 v11, v11, 0x1

    iput v11, v4, Lcom/google/android/gsf/gtalkservice/DataMessageManager$MissInfo;->count:I

    iget v11, v4, Lcom/google/android/gsf/gtalkservice/DataMessageManager$MissInfo;->count:I

    if-lt v11, v7, :cond_3

    iget-wide v11, v4, Lcom/google/android/gsf/gtalkservice/DataMessageManager$MissInfo;->timeOfFirstMiss:J

    add-long/2addr v11, v2

    cmp-long v11, v5, v11

    if-ltz v11, :cond_3

    move v8, v9

    :cond_3
    monitor-exit v10

    goto :goto_0

    :catchall_0
    move-exception v8

    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v8
.end method


# virtual methods
.method public acquireWakeLock()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->mLockObject:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getWakeLock()Landroid/os/PowerManager$WakeLock;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    return-object v0
.end method

.method public initConnection(Lorg/jivesoftware/smack/XMPPConnection;)V
    .locals 3
    .param p1    # Lorg/jivesoftware/smack/XMPPConnection;

    const-string v0, "GTalkService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "initConnection for accountId "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->mEndpoint:Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->getAccountId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->log(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1, p0}, Lorg/jivesoftware/smack/XMPPConnection;->addDataMessageListener(Lorg/jivesoftware/smack/PacketListener;)V

    return-void
.end method

.method public processDataMessageIntent(Landroid/content/Intent;Ljava/lang/String;Lcom/google/android/gsf/gtalkservice/DataMessageManager$BroadcastDoneReceiver;)V
    .locals 9
    .param p1    # Landroid/content/Intent;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/gsf/gtalkservice/DataMessageManager$BroadcastDoneReceiver;

    const/4 v6, 0x0

    if-nez p1, :cond_0

    const-string v0, "GTalkService"

    const-string v1, "parseDataMessageIntent() returned null intent!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const-string v0, "GTalkService"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "broadcast "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->log(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v8

    if-eqz v8, :cond_1

    invoke-virtual {v8}, Landroid/os/Bundle;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "extras: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v8}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->log(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 p2, 0x0

    :cond_2
    const-string v0, "GTalkService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "send ordered broadcast for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-nez p2, :cond_4

    const-string v0, ""

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->log(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->acquireWakeLock()V

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->mHandler:Landroid/os/Handler;

    const/4 v5, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v7, v6

    invoke-virtual/range {v0 .. v7}, Landroid/content/Context;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V

    goto/16 :goto_0

    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " with permission="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public processPacket(Lorg/jivesoftware/smack/packet/Packet;)V
    .locals 10
    .param p1    # Lorg/jivesoftware/smack/packet/Packet;

    instance-of v0, p1, Lorg/jivesoftware/smack/packet/DataMessage;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lorg/jivesoftware/smack/packet/DataMessage;

    invoke-direct {p0, v0}, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->parseDataMessageIntent(Lorg/jivesoftware/smack/packet/DataMessage;)Lcom/google/android/gsf/gtalkservice/DataMessageManager$DataMessageParsingResult;

    move-result-object v7

    if-nez v7, :cond_1

    const-string v0, "GTalkService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[DataMessageMgr] processPacket: cannot parse data message "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lorg/jivesoftware/smack/packet/Packet;->toXML()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, v7, Lcom/google/android/gsf/gtalkservice/DataMessageManager$DataMessageParsingResult;->isMCS:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->acquireWakeLock()V

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x190

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v6

    iget-object v0, v7, Lcom/google/android/gsf/gtalkservice/DataMessageManager$DataMessageParsingResult;->dataMsg:Lorg/jivesoftware/smack/packet/DataMessage;

    iput-object v0, v6, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v6}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    :cond_2
    iget-object v8, v7, Lcom/google/android/gsf/gtalkservice/DataMessageManager$DataMessageParsingResult;->intent:Landroid/content/Intent;

    iget-object v9, v7, Lcom/google/android/gsf/gtalkservice/DataMessageManager$DataMessageParsingResult;->permission:Ljava/lang/String;

    new-instance v0, Lcom/google/android/gsf/gtalkservice/DataMessageManager$BroadcastDoneReceiver;

    iget-object v2, v7, Lcom/google/android/gsf/gtalkservice/DataMessageManager$DataMessageParsingResult;->intent:Landroid/content/Intent;

    invoke-virtual {p1}, Lorg/jivesoftware/smack/packet/Packet;->getFrom()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gsf/gtalkservice/DataMessageManager$BroadcastDoneReceiver;-><init>(Lcom/google/android/gsf/gtalkservice/DataMessageManager;Landroid/content/Intent;Ljava/lang/String;J)V

    invoke-virtual {p0, v8, v9, v0}, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->processDataMessageIntent(Landroid/content/Intent;Ljava/lang/String;Lcom/google/android/gsf/gtalkservice/DataMessageManager$BroadcastDoneReceiver;)V

    goto :goto_0
.end method

.method public queryPackageManagerForBroadcastIntentReceiver(Landroid/content/Intent;)Z
    .locals 8
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    iget-object v5, p0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    invoke-virtual {v1, p1, v0}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    const/4 v0, 0x1

    :cond_0
    const-string v5, "GTalkService"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "findReceiverForIntent: queryBroadcastReceivers took "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    sub-long/2addr v6, v3

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "ms, found="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->log(Ljava/lang/String;)V

    :cond_1
    return v0
.end method

.method public releaseWakeLock()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->mLockObject:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public sendDataMessageStanza(Landroid/content/Intent;)V
    .locals 10
    .param p1    # Landroid/content/Intent;

    const-string v8, "use_rmq"

    const/4 v9, 0x0

    invoke-virtual {p1, v8, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    const-string v8, "use_rmq"

    invoke-virtual {p1, v8}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/google/android/gsf/gtalkservice/gtalk/ApplicationEndpointPacketManager;->getAppPackage(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v8, "missing_package_name"

    invoke-direct {p0, v8, v0}, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->processSendDataMessageFailed(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v8, p0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->mEndpoint:Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;

    invoke-virtual {v8}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->isD2cmWhitelisted()Z

    move-result v8

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->mEndpoint:Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;

    invoke-virtual {v8, v0}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->isPackageNameWhitelisted(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    :cond_2
    const-string v8, "app"

    invoke-virtual {p1, v8}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    const-string v8, "registration_id"

    invoke-virtual {p1, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v8, "registration_id"

    invoke-virtual {p1, v8}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    if-nez v5, :cond_3

    const-string v8, "missing_reg_id"

    invoke-direct {p0, v8, v0}, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->processSendDataMessageFailed(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    new-instance v6, Lorg/jivesoftware/smack/packet/DataMessage;

    invoke-direct {v6}, Lorg/jivesoftware/smack/packet/DataMessage;-><init>()V

    const-string v8, "d2cm@google.com"

    invoke-virtual {v6, v8}, Lorg/jivesoftware/smack/packet/DataMessage;->setFrom(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Lorg/jivesoftware/smack/packet/DataMessage;->setCategory(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Lorg/jivesoftware/smack/packet/DataMessage;->setRegId(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_4

    instance-of v8, v4, Ljava/lang/String;

    if-eqz v8, :cond_5

    new-instance v8, Lorg/jivesoftware/smack/packet/DataMessage$AppData;

    check-cast v4, Ljava/lang/String;

    invoke-direct {v8, v3, v4}, Lorg/jivesoftware/smack/packet/DataMessage$AppData;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v8}, Lorg/jivesoftware/smack/packet/DataMessage;->addAppData(Lorg/jivesoftware/smack/packet/DataMessage$AppData;)V

    goto :goto_1

    :cond_5
    const-string v8, "invalid_params"

    invoke-direct {p0, v8, v0}, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->processSendDataMessageFailed(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    iget-object v8, p0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->mEndpoint:Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;

    invoke-virtual {v8, v6, v7}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->sendPacket(Lorg/jivesoftware/smack/packet/Packet;Z)Z

    move-result v8

    if-nez v8, :cond_7

    const-string v8, "no_connection"

    invoke-direct {p0, v8, v0}, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->processSendDataMessageFailed(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_7
    iget-object v8, p0, Lcom/google/android/gsf/gtalkservice/DataMessageManager;->mEndpoint:Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;

    invoke-virtual {v8}, Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;->setLastDataMessageSent()V

    goto/16 :goto_0
.end method
