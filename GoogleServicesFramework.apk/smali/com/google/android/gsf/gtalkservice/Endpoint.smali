.class public abstract Lcom/google/android/gsf/gtalkservice/Endpoint;
.super Ljava/lang/Object;
.source "Endpoint.java"

# interfaces
.implements Lcom/google/android/gsf/gtalkservice/PacketSender;
.implements Lorg/jivesoftware/smack/ConnectionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gsf/gtalkservice/Endpoint$1;,
        Lcom/google/android/gsf/gtalkservice/Endpoint$SpecialConnectionEvent;,
        Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionClosedEvent;,
        Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionMadeEvent;,
        Lcom/google/android/gsf/gtalkservice/Endpoint$PendingConnectEvent;,
        Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionStateEvent;,
        Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionEvent;,
        Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionLifeCycle;,
        Lcom/google/android/gsf/gtalkservice/Endpoint$MissingAuthTokenError;,
        Lcom/google/android/gsf/gtalkservice/Endpoint$ClearWakeLockTimer;
    }
.end annotation


# static fields
.field protected static sNonTalkAccountId:J


# instance fields
.field protected mAccount:Lcom/google/android/gsf/gtalkservice/Account;

.field protected mAccountId:J

.field private mAccountIdFilter:Lorg/jivesoftware/smack/filter/AccountIdFilter;

.field protected mAsyncConnWakeLock:Landroid/os/PowerManager$WakeLock;

.field protected mAsyncMessageHandler:Landroid/os/Handler;

.field protected mAsyncWakelockTag:Ljava/lang/String;

.field private mAuthExpiredCount:I

.field private mClearWakeLockTimer:Lcom/google/android/gsf/gtalkservice/Endpoint$ClearWakeLockTimer;

.field private mClearWakeLockTimerFiredCount:I

.field protected mConnection:Lorg/jivesoftware/smack/XMPPConnection;

.field private mConnectionClosedLock:Ljava/lang/Object;

.field private mConnectionDurations:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field protected mConnectionError:Lcom/google/android/gtalkservice/ConnectionError;

.field private mConnectionHistory:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionLifeCycle;",
            ">;"
        }
    .end annotation
.end field

.field private mConnectionStartTime:J

.field protected mConnectionState:Lcom/google/android/gtalkservice/ConnectionState;

.field private mConnectionWrapper:Lcom/google/android/gsf/gtalkservice/service/EndpointWrapper;

.field public mContext:Landroid/content/Context;

.field private mCurrentConnectionCycle:Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionLifeCycle;

.field protected mDataMessageManager:Lcom/google/android/gsf/gtalkservice/DataMessageManager;

.field protected final mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

.field protected mHttpResponseManager:Lcom/google/android/gsf/gtalkservice/HttpResponseManager;

.field protected mIqPacketManager:Lcom/google/android/gsf/gtalkservice/IQPacketManager;

.field private mJid:Ljava/lang/String;

.field protected mJidResource:Ljava/lang/String;

.field private mLastConnectAttemptTs:J

.field protected mLastConnectionAttemptSuccessful:Z

.field private mLastLoginAttemptTimeStamp:J

.field private mNumConnectionsAttempted:I

.field private mNumConnectionsMade:I

.field protected mOpened:Z

.field private mOriginalUserBareAddress:Ljava/lang/String;

.field private mPowerManager:Landroid/os/PowerManager;

.field protected mRawStanzaSendReceiveManager:Lcom/google/android/gsf/gtalkservice/RawStanzaSendReceiveManager;

.field private mServerAddress:Ljava/lang/String;

.field protected mServiceHandler:Landroid/os/Handler;

.field protected mSettingsQueryMap:Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;

.field protected mSimpleWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mUserBareAddress:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-wide/32 v0, 0xf4240

    sput-wide v0, Lcom/google/android/gsf/gtalkservice/Endpoint;->sNonTalkAccountId:J

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gsf/gtalkservice/service/GTalkService;Landroid/os/Looper;)V
    .locals 3
    .param p1    # Lcom/google/android/gsf/gtalkservice/service/GTalkService;
    .param p2    # Landroid/os/Looper;

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/gsf/gtalkservice/Endpoint$ClearWakeLockTimer;

    invoke-direct {v0, p0, v2}, Lcom/google/android/gsf/gtalkservice/Endpoint$ClearWakeLockTimer;-><init>(Lcom/google/android/gsf/gtalkservice/Endpoint;Lcom/google/android/gsf/gtalkservice/Endpoint$1;)V

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mClearWakeLockTimer:Lcom/google/android/gsf/gtalkservice/Endpoint$ClearWakeLockTimer;

    iput-object v2, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnection:Lorg/jivesoftware/smack/XMPPConnection;

    new-instance v0, Lcom/google/android/gtalkservice/ConnectionState;

    invoke-direct {v0, v1}, Lcom/google/android/gtalkservice/ConnectionState;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionState:Lcom/google/android/gtalkservice/ConnectionState;

    new-instance v0, Lcom/google/android/gtalkservice/ConnectionError;

    invoke-direct {v0, v1}, Lcom/google/android/gtalkservice/ConnectionError;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionError:Lcom/google/android/gtalkservice/ConnectionError;

    iput v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mNumConnectionsMade:I

    iput v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mNumConnectionsAttempted:I

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionDurations:Ljava/util/Vector;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mLastConnectionAttemptSuccessful:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionClosedLock:Ljava/lang/Object;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionHistory:Ljava/util/ArrayList;

    iput v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAuthExpiredCount:I

    iput-object p1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    iput-object p1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->getHandler()Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mServiceHandler:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/gsf/gtalkservice/EndpointMessageHandler;

    invoke-direct {v0, p0, p2}, Lcom/google/android/gsf/gtalkservice/EndpointMessageHandler;-><init>(Lcom/google/android/gsf/gtalkservice/Endpoint;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAsyncMessageHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$108(Lcom/google/android/gsf/gtalkservice/Endpoint;)I
    .locals 2
    .param p0    # Lcom/google/android/gsf/gtalkservice/Endpoint;

    iget v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mClearWakeLockTimerFiredCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mClearWakeLockTimerFiredCount:I

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/gsf/gtalkservice/Endpoint;)Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionLifeCycle;
    .locals 1
    .param p0    # Lcom/google/android/gsf/gtalkservice/Endpoint;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mCurrentConnectionCycle:Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionLifeCycle;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/gsf/gtalkservice/Endpoint;)J
    .locals 2
    .param p0    # Lcom/google/android/gsf/gtalkservice/Endpoint;

    iget-wide v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionStartTime:J

    return-wide v0
.end method

.method private addConnectionClosedEvent(I)V
    .locals 9

    const-wide/16 v2, 0x0

    const-string v0, "GTalkService/c"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "addConnectionClosedEvent: mError="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logv(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionHistory:Ljava/util/ArrayList;

    monitor-enter v4

    :try_start_0
    new-instance v0, Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionClosedEvent;

    invoke-direct {v0, p1}, Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionClosedEvent;-><init>(I)V

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mCurrentConnectionCycle:Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionLifeCycle;

    invoke-virtual {v1, v0}, Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionLifeCycle;->appendConnectionEvent(Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionEvent;)V

    iget-wide v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionStartTime:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    move-wide v0, v2

    :goto_0
    iget-object v5, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mCurrentConnectionCycle:Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionLifeCycle;

    iput-wide v0, v5, Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionLifeCycle;->mConnectionUptime:J

    iget-object v5, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mCurrentConnectionCycle:Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionLifeCycle;

    iget-wide v5, v5, Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionLifeCycle;->mConnectionUptime:J

    cmp-long v5, v5, v2

    if-gtz v5, :cond_0

    const-string v5, "GTalkService/c"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "??? addConnectionClosedEvent: connection uptime is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mCurrentConnectionCycle:Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionLifeCycle;

    iget-wide v7, v7, Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionLifeCycle;->mConnectionUptime:J

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", mConnectionStartTime="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-wide v7, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionStartTime:J

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v5, v6}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logd(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    new-instance v5, Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionLifeCycle;

    const/4 v6, 0x0

    invoke-direct {v5, p0, v6}, Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionLifeCycle;-><init>(Lcom/google/android/gsf/gtalkservice/Endpoint;Lcom/google/android/gsf/gtalkservice/Endpoint$1;)V

    iput-object v5, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mCurrentConnectionCycle:Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionLifeCycle;

    iget-object v5, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionHistory:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mCurrentConnectionCycle:Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionLifeCycle;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v5, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionHistory:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-direct {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getMaxConnectionHistoryRecords()I

    move-result v6

    if-le v5, v6, :cond_1

    iget-object v5, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionHistory:Ljava/util/ArrayList;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_1
    iget-object v5, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-virtual {v5}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->getReconnectManager()Lcom/google/android/gsf/gtalkservice/ReconnectManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->getNetworkState()Landroid/net/NetworkInfo$State;

    move-result-object v6

    invoke-virtual {v5}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->getNetworkType()I

    move-result v5

    cmp-long v2, v0, v2

    if-lez v2, :cond_3

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    long-to-int v0, v0

    invoke-virtual {p0, p1, v5, v0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logConnectionClosed(III)V

    :goto_1
    monitor-exit v4

    return-void

    :cond_2
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v5, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionStartTime:J

    sub-long/2addr v0, v5

    goto :goto_0

    :cond_3
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getConnectionState()Lcom/google/android/gtalkservice/ConnectionState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gtalkservice/ConnectionState;->getState()I

    move-result v1

    invoke-virtual {v6}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logConnectionEvent(IIII)V

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private addConnectionEvent(I)V
    .locals 8

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->getReconnectManager()Lcom/google/android/gsf/gtalkservice/ReconnectManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->getNetworkState()Landroid/net/NetworkInfo$State;

    move-result-object v3

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    new-instance v0, Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionStateEvent;

    invoke-direct {v0, p1}, Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionStateEvent;-><init>(I)V

    :goto_0
    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionHistory:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    const-string v2, "GTalkService/c"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "addConnectionEvent: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v2, v4}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logv(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mCurrentConnectionCycle:Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionLifeCycle;

    invoke-virtual {v2, v0}, Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionLifeCycle;->appendConnectionEvent(Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionEvent;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getConnectionError()Lcom/google/android/gtalkservice/ConnectionError;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gtalkservice/ConnectionError;->getError()I

    move-result v1

    invoke-virtual {v3}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, p1, v1, v2}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logConnectionEvent(IIII)V

    return-void

    :pswitch_1
    new-instance v0, Lcom/google/android/gsf/gtalkservice/Endpoint$PendingConnectEvent;

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getConnectionError()Lcom/google/android/gtalkservice/ConnectionError;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gtalkservice/ConnectionError;->getError()I

    move-result v2

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->getNetworkType()I

    move-result v4

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->getLastNetworkAvailable()Z

    move-result v5

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->getReconnectManager()Lcom/google/android/gsf/gtalkservice/ReconnectManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->getReconnectTime()J

    move-result-wide v6

    move v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gsf/gtalkservice/Endpoint$PendingConnectEvent;-><init>(IILandroid/net/NetworkInfo$State;IZJ)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getConnection()Lorg/jivesoftware/smack/XMPPConnection;

    move-result-object v0

    new-instance v1, Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionMadeEvent;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/jivesoftware/smack/XMPPConnection;->getHostIpAddress()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {v1, p1, v2, v0}, Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionMadeEvent;-><init>(ILcom/google/android/gtalkservice/Presence;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0

    :cond_0
    const-string v0, ""

    goto :goto_1

    :pswitch_3
    new-instance v0, Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionMadeEvent;

    invoke-direct {v0, p1, v2, v2}, Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionMadeEvent;-><init>(ILcom/google/android/gtalkservice/Presence;Ljava/lang/String;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private cancelReconnectAlarm()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->getReconnectManager()Lcom/google/android/gsf/gtalkservice/ReconnectManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->cancelReconnectAlarm()V

    return-void
.end method

.method private checkConnection()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not connected to server"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method private clearReconnectAlarm()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->cancelReconnectAlarm()V

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->getReconnectManager()Lcom/google/android/gsf/gtalkservice/ReconnectManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->clearReconnectAlarm()V

    return-void
.end method

.method private constructException(Ljava/lang/String;)Ljava/lang/IllegalStateException;
    .locals 3
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "user="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getUsername()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p1, :cond_2

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    :goto_0
    const-string v1, ", mState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getConnectionState()Lcom/google/android/gtalkservice/ConnectionState;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnection:Lorg/jivesoftware/smack/XMPPConnection;

    if-eqz v1, :cond_1

    const-string v1, ", mConnection.isConnected="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnection:Lorg/jivesoftware/smack/XMPPConnection;

    invoke-virtual {v1}, Lorg/jivesoftware/smack/XMPPConnection;->isConnected()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    return-object v1

    :cond_2
    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnection:Lorg/jivesoftware/smack/XMPPConnection;

    if-nez v1, :cond_0

    const-string v1, "mConnection=null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private getMaxConnectionHistoryRecords()I
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getAccount()Lcom/google/android/gsf/gtalkservice/Account;

    move-result-object v0

    if-nez v0, :cond_0

    const/16 v1, 0xa

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/Account;->getMaxConnectionHistoryRecords()I

    move-result v1

    goto :goto_0
.end method

.method private static getNextAvaialableNonTalkAccountId()J
    .locals 4

    sget-wide v0, Lcom/google/android/gsf/gtalkservice/Endpoint;->sNonTalkAccountId:J

    const-wide/16 v2, 0x1

    add-long/2addr v2, v0

    sput-wide v2, Lcom/google/android/gsf/gtalkservice/Endpoint;->sNonTalkAccountId:J

    return-wide v0
.end method

.method private handleDoConnectError(ILorg/jivesoftware/smack/packet/XMPPError;)V
    .locals 7

    const/4 v0, 0x1

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAccount:Lcom/google/android/gsf/gtalkservice/Account;

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/Account;->getUsername()Ljava/lang/String;

    move-result-object v1

    const-string v3, "GTalkService/c"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "handleDoConnectError for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getAccountId()J

    move-result-wide v5

    invoke-static {v5, v6, v1}, Lcom/google/android/gsf/gtalkservice/Log;->sanitizeUsername(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", error="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", xmppError="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v3, v1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logd(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    :try_start_0
    invoke-direct {p0, p2}, Lcom/google/android/gsf/gtalkservice/Endpoint;->translateXMPPError(Lorg/jivesoftware/smack/packet/XMPPError;)I

    move-result p1

    const-string v1, "GTalkService/c"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "run: translateXMPPError => "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v1, v3}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logv(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->updateAuthErrorStats(I)V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->reachedRetryThresholdForAuthExpiration()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    :try_start_1
    invoke-virtual {p0, p1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->setConnectionError(I)Z

    const/4 v3, 0x1

    if-nez v1, :cond_4

    const/16 v4, 0xb

    if-eq p1, v4, :cond_4

    :goto_0
    const/4 v4, 0x0

    invoke-virtual {p0, v3, v0, v4}, Lcom/google/android/gsf/gtalkservice/Endpoint;->closeConnection(ZZZ)V

    const/4 v0, 0x4

    if-eq p1, v0, :cond_1

    const/4 v0, 0x5

    if-ne p1, v0, :cond_2

    if-nez v1, :cond_2

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->requestToRefreshAuthToken()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_2
    if-eqz v1, :cond_3

    const-string v0, "handleDoConnectError: repeated AUTH_EXPIRED failures, stop retrying."

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logw(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getGTalkService()Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->getStatusBarNotifier()Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getUsername()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getAccountId()J

    move-result-wide v3

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getSettingsMap()Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;

    move-result-object v5

    invoke-virtual {v0, v1, v3, v4, v5}, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->notifyAuthError(Ljava/lang/String;JLcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;)V

    invoke-direct {p0, v2}, Lcom/google/android/gsf/gtalkservice/Endpoint;->updateAuthErrorStats(I)V

    :cond_3
    const-string v0, "GTalkService/c"

    const-string v1, "handleDoConnectError: connection failed"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logd(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "GTalkService/wake"

    const-string v1, "release wakelock"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logv(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->releaseAsyncWakeLock()V

    return-void

    :cond_4
    move v0, v2

    goto :goto_0

    :catchall_0
    move-exception v0

    move v1, v2

    :goto_1
    if-eqz v1, :cond_5

    const-string v1, "handleDoConnectError: repeated AUTH_EXPIRED failures, stop retrying."

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logw(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getGTalkService()Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->getStatusBarNotifier()Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getUsername()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getAccountId()J

    move-result-wide v4

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getSettingsMap()Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;

    move-result-object v6

    invoke-virtual {v1, v3, v4, v5, v6}, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->notifyAuthError(Ljava/lang/String;JLcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;)V

    invoke-direct {p0, v2}, Lcom/google/android/gsf/gtalkservice/Endpoint;->updateAuthErrorStats(I)V

    :cond_5
    const-string v1, "GTalkService/c"

    const-string v2, "handleDoConnectError: connection failed"

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logd(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "GTalkService/wake"

    const-string v2, "release wakelock"

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logv(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->releaseAsyncWakeLock()V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method private initJid()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAccount:Lcom/google/android/gsf/gtalkservice/Account;

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/Account;->getUsername()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v2, 0x2f

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getJidResource()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/gsf/gtalkservice/Endpoint;->setJid(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->setUserBareAddress(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->setOriginalUserBareAddress(Ljava/lang/String;)V

    return-void
.end method

.method private internalSetConnectionState(I)Z
    .locals 4
    .param p1    # I

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionState:Lcom/google/android/gtalkservice/ConnectionState;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionState:Lcom/google/android/gtalkservice/ConnectionState;

    invoke-virtual {v0}, Lcom/google/android/gtalkservice/ConnectionState;->getState()I

    move-result v0

    if-eq v0, p1, :cond_0

    const-string v0, "GTalkService/c"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "internalSetConnectionState: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Lcom/google/android/gtalkservice/ConnectionState;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logv(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionState:Lcom/google/android/gtalkservice/ConnectionState;

    invoke-virtual {v0, p1}, Lcom/google/android/gtalkservice/ConnectionState;->setState(I)V

    const/4 v0, 0x1

    monitor-exit v1

    :goto_0
    return v0

    :cond_0
    monitor-exit v1

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private sendConnectionClosedMessage(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAsyncMessageHandler:Landroid/os/Handler;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAsyncMessageHandler:Landroid/os/Handler;

    const/16 v2, 0x1f4

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iput p2, v0, Landroid/os/Message;->arg1:I

    iput p1, v0, Landroid/os/Message;->arg2:I

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAsyncMessageHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private setJid(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mJid:Ljava/lang/String;

    return-void
.end method

.method private setOriginalUserBareAddress(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mOriginalUserBareAddress:Ljava/lang/String;

    return-void
.end method

.method private setServerAddress(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mServerAddress:Ljava/lang/String;

    return-void
.end method

.method private setUserBareAddress(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mUserBareAddress:Ljava/lang/String;

    return-void
.end method

.method private translateXMPPError(Lorg/jivesoftware/smack/packet/XMPPError;)I
    .locals 3
    .param p1    # Lorg/jivesoftware/smack/packet/XMPPError;

    const/4 v0, 0x0

    invoke-virtual {p1}, Lorg/jivesoftware/smack/packet/XMPPError;->getCode()I

    move-result v1

    if-eqz v1, :cond_0

    const/16 v2, 0x190

    if-lt v1, v2, :cond_2

    const/16 v2, 0x196

    if-gt v1, v2, :cond_2

    instance-of v2, p1, Lcom/google/android/gsf/gtalkservice/Endpoint$MissingAuthTokenError;

    if-eqz v2, :cond_1

    const/4 v0, 0x4

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x5

    goto :goto_0

    :cond_2
    const/16 v2, 0x197

    if-ne v1, v2, :cond_3

    const/16 v0, 0xb

    goto :goto_0

    :cond_3
    const/16 v2, 0x1f4

    if-eq v1, v2, :cond_4

    const/16 v2, 0x1f6

    if-lt v1, v2, :cond_5

    const/16 v2, 0x1f8

    if-gt v1, v2, :cond_5

    :cond_4
    const/4 v0, 0x7

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->getReconnectManager()Lcom/google/android/gsf/gtalkservice/ReconnectManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->isNetworkAvailable()Z

    move-result v2

    if-nez v2, :cond_6

    const/4 v0, 0x1

    goto :goto_0

    :cond_6
    const/4 v0, 0x2

    goto :goto_0
.end method

.method private updateAuthErrorStats(I)V
    .locals 1
    .param p1    # I

    const/4 v0, 0x5

    if-ne p1, v0, :cond_1

    iget v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAuthExpiredCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAuthExpiredCount:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p1, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAuthExpiredCount:I

    goto :goto_0
.end method


# virtual methods
.method public acquireAsyncWakeLock()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAsyncConnWakeLock:Landroid/os/PowerManager$WakeLock;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAsyncConnWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    const-string v0, "GTalkService/wake"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "acquireAsyncWakeLock: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAsyncConnWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logv(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mServiceHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mClearWakeLockTimer:Lcom/google/android/gsf/gtalkservice/Endpoint$ClearWakeLockTimer;

    const-wide/32 v2, 0x11170

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public acquireSimpleWakeLock(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mSimpleWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    const-string v0, "GTalkService/wake"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "acquireWakeLock: token="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", wakelock="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mSimpleWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logv(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected addConnectionDuration()V
    .locals 8

    iget-wide v2, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionStartTime:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionDurations:Ljava/util/Vector;

    monitor-enter v3

    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionStartTime:J

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x3e8

    div-long v0, v4, v6

    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionDurations:Ljava/util/Vector;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public addConnectionEvent(Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionEvent;)V
    .locals 3

    const-string v0, "GTalkService/c"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addConnectionEvent "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logv(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionHistory:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mCurrentConnectionCycle:Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionLifeCycle;

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionLifeCycle;->appendConnectionEvent(Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionEvent;)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public asyncCloseSettingsQueryMap()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAsyncMessageHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAsyncMessageHandler:Landroid/os/Handler;

    const/16 v2, 0x258

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAsyncMessageHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    return-void
.end method

.method protected broadcastConnected()V
    .locals 0

    return-void
.end method

.method protected broadcastDisconnected(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method protected bumpConnectionCount()V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mNumConnectionsMade:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mNumConnectionsMade:I

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v1, "GTalkService/c"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "connections made: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logv(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected bumpConnectionsAttemptedCount()V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mNumConnectionsAttempted:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mNumConnectionsAttempted:I

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v1, "GTalkService/c"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "connections made: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logv(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public clearStats()V
    .locals 2

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mNumConnectionsMade:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mNumConnectionsAttempted:I

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionDurations:Ljava/util/Vector;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionDurations:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    return-void

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0

    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0
.end method

.method public closeConnection(ZZZ)V
    .locals 6

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getConnection()Lorg/jivesoftware/smack/XMPPConnection;

    move-result-object v1

    const-string v0, "GTalkService/c"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "closeConnection for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getAccountId()J

    move-result-wide v3

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getUsername()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/google/android/gsf/gtalkservice/Log;->sanitizeUsername(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", notify="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", xmppConn="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logd(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getConnectionError()Lcom/google/android/gtalkservice/ConnectionError;

    move-result-object v2

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lorg/jivesoftware/smack/XMPPConnection;->getLocalConnectionId()I

    move-result v0

    :goto_0
    invoke-virtual {v2}, Lcom/google/android/gtalkservice/ConnectionError;->getError()I

    move-result v2

    invoke-virtual {p0, v0, v2, p1, p2}, Lcom/google/android/gsf/gtalkservice/Endpoint;->connectionClosed(IIZZ)V

    invoke-virtual {p0, v1, p3}, Lcom/google/android/gsf/gtalkservice/Endpoint;->closeXmppConnection(Lorg/jivesoftware/smack/XMPPConnection;Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public closeSettingsQueryMap()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mSettingsQueryMap:Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mSettingsQueryMap:Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;

    invoke-virtual {v0}, Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;->deleteObservers()V

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mSettingsQueryMap:Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;

    invoke-virtual {v0}, Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;->close()V

    :cond_0
    return-void
.end method

.method protected closeXmppConnection(Lorg/jivesoftware/smack/XMPPConnection;Z)V
    .locals 0
    .param p1    # Lorg/jivesoftware/smack/XMPPConnection;
    .param p2    # Z

    return-void
.end method

.method protected computeJIDResource()Ljava/lang/String;
    .locals 8

    const/4 v1, 0x0

    const/16 v7, 0xc

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getResourcePrefix()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v5

    add-long/2addr v3, v5

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0, v3, v4}, Ljava/util/Random;-><init>(J)V

    invoke-virtual {v0}, Ljava/util/Random;->nextLong()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-le v3, v7, :cond_1

    invoke-virtual {v0, v1, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    if-ge v3, v7, :cond_0

    rsub-int/lit8 v3, v3, 0xc

    :goto_0
    if-ge v1, v3, :cond_0

    const-string v4, "0"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public connect()Z
    .locals 8

    const/4 v6, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {}, Lcom/google/android/gsf/gtalkservice/LogTag;->queryLoggingLevel()V

    invoke-direct {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->cancelReconnectAlarm()V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mLastConnectAttemptTs:J

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getConnectionState()Lcom/google/android/gtalkservice/ConnectionState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gtalkservice/ConnectionState;->getState()I

    move-result v4

    if-lt v4, v6, :cond_0

    const-string v3, "GTalkService/c"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "connect: acct="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getAccountId()J

    move-result-wide v5

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getUsername()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/google/android/gsf/gtalkservice/Log;->sanitizeUsername(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", current conn state is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", bail"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v3, v4}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logd(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return v2

    :cond_0
    invoke-virtual {p0, v6}, Lcom/google/android/gsf/gtalkservice/Endpoint;->setConnectionState(I)Z

    iget-object v4, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-virtual {v4}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->getReconnectManager()Lcom/google/android/gsf/gtalkservice/ReconnectManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->pollNetworkAvailable()Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "GTalkService/c"

    const-string v5, "connect: network unavailable, set state=PENDING, err=NO_NETWORK"

    invoke-virtual {p0, v4, v5}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logd(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lcom/google/android/gsf/gtalkservice/Endpoint;->setConnectionError(I)Z

    invoke-virtual {p0, v3}, Lcom/google/android/gsf/gtalkservice/Endpoint;->setConnectionState(I)Z

    goto :goto_0

    :cond_1
    const-string v2, "GTalkService/c"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "connect: acct="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getAccountId()J

    move-result-wide v5

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getUsername()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/google/android/gsf/gtalkservice/Log;->sanitizeUsername(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", state="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getConnectionState()Lcom/google/android/gtalkservice/ConnectionState;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v2, v4}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logi(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->acquireAsyncWakeLock()V

    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAsyncMessageHandler:Landroid/os/Handler;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAsyncMessageHandler:Landroid/os/Handler;

    const/16 v4, 0x64

    invoke-virtual {v2, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAsyncMessageHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_2
    move v2, v3

    goto :goto_0
.end method

.method public connectionClosed(I)V
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getConnectionError()Lcom/google/android/gtalkservice/ConnectionError;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gtalkservice/ConnectionError;->getError()I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->sendConnectionClosedMessage(II)V

    return-void
.end method

.method public connectionClosed(IIZ)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Z

    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->connectionClosed(IIZZ)V

    return-void
.end method

.method public connectionClosed(IIZZ)V
    .locals 10
    .param p1    # I
    .param p2    # I
    .param p3    # Z
    .param p4    # Z

    new-instance v4, Lcom/google/android/gsf/gtalkservice/WakeLockDiagnostics;

    const-string v5, "connectionClosed"

    invoke-direct {v4, v5}, Lcom/google/android/gsf/gtalkservice/WakeLockDiagnostics;-><init>(Ljava/lang/String;)V

    const-string v5, "connectionClosed"

    invoke-virtual {p0, v5}, Lcom/google/android/gsf/gtalkservice/Endpoint;->acquireSimpleWakeLock(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getUsername()Ljava/lang/String;

    move-result-object v3

    :try_start_0
    invoke-virtual {v4}, Lcom/google/android/gsf/gtalkservice/WakeLockDiagnostics;->wakeLockAcquired()V

    const/4 v2, 0x0

    iget-object v6, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionClosedLock:Ljava/lang/Object;

    monitor-enter v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getConnection()Lorg/jivesoftware/smack/XMPPConnection;

    move-result-object v0

    if-nez v0, :cond_4

    const-string v5, "GTalkService"

    const-string v7, "connectionClosed: no XMPPConnection - That\'s strange!"

    invoke-static {v5, v7}, Lcom/google/android/gsf/gtalkservice/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string v5, "GTalkService"

    const/4 v7, 0x3

    invoke-static {v5, v7}, Lcom/google/android/gsf/gtalkservice/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v5, "GTalkService/c"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "connectionClosed for "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAccountId:J

    invoke-static {v8, v9, v3}, Lcom/google/android/gsf/gtalkservice/Log;->sanitizeUsername(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", connId="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", error="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {p2}, Lcom/google/android/gtalkservice/ConnectionError;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v5, v7}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logi(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Lcom/google/android/gsf/gtalkservice/Endpoint;->setConnection(Lorg/jivesoftware/smack/XMPPConnection;)V

    invoke-virtual {p0, p2}, Lcom/google/android/gsf/gtalkservice/Endpoint;->setConnectionError(I)Z

    invoke-virtual {p0, p1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->onConnectionClosed(I)V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->addConnectionDuration()V

    invoke-direct {p0, p2}, Lcom/google/android/gsf/gtalkservice/Endpoint;->addConnectionClosedEvent(I)V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->isSessionOpen()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-static {p2}, Lcom/google/android/gtalkservice/ConnectionError;->isAuthenticationError(I)Z

    move-result v5

    if-nez v5, :cond_5

    if-eqz p4, :cond_5

    const/4 v5, 0x1

    invoke-virtual {p0, v5}, Lcom/google/android/gsf/gtalkservice/Endpoint;->setConnectionState(I)Z

    const/4 v2, 0x1

    :goto_0
    const-wide/16 v7, 0x0

    iput-wide v7, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionStartTime:J

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v2, :cond_2

    const/4 v5, 0x0

    :try_start_2
    invoke-virtual {p0, v5}, Lcom/google/android/gsf/gtalkservice/Endpoint;->retryConnection(Z)V

    :cond_2
    if-eqz p3, :cond_3

    invoke-virtual {p0, p2}, Lcom/google/android/gsf/gtalkservice/Endpoint;->broadcastDisconnected(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_3
    invoke-virtual {v4}, Lcom/google/android/gsf/gtalkservice/WakeLockDiagnostics;->timeElapsedSinceWakelockAcquired()J

    const-string v5, "connectionClosed"

    invoke-virtual {p0, v5}, Lcom/google/android/gsf/gtalkservice/Endpoint;->releaseSimpleWakeLock(Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_4
    :try_start_3
    invoke-virtual {v0}, Lorg/jivesoftware/smack/XMPPConnection;->getLocalConnectionId()I

    move-result v1

    if-eq p1, v1, :cond_0

    const-string v5, "GTalkService/c"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "connectionClosed for "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getAccountId()J

    move-result-wide v8

    invoke-static {v8, v9, v3}, Lcom/google/android/gsf/gtalkservice/Log;->sanitizeUsername(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", old conn(id="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "), curr connection id is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", ignore"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v5, v7}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logd(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-virtual {v4}, Lcom/google/android/gsf/gtalkservice/WakeLockDiagnostics;->timeElapsedSinceWakelockAcquired()J

    const-string v5, "connectionClosed"

    invoke-virtual {p0, v5}, Lcom/google/android/gsf/gtalkservice/Endpoint;->releaseSimpleWakeLock(Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    const/4 v5, 0x0

    :try_start_4
    invoke-virtual {p0, v5}, Lcom/google/android/gsf/gtalkservice/Endpoint;->setConnectionState(I)Z

    goto :goto_0

    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catchall_1
    move-exception v5

    invoke-virtual {v4}, Lcom/google/android/gsf/gtalkservice/WakeLockDiagnostics;->timeElapsedSinceWakelockAcquired()J

    const-string v6, "connectionClosed"

    invoke-virtual {p0, v6}, Lcom/google/android/gsf/gtalkservice/Endpoint;->releaseSimpleWakeLock(Ljava/lang/String;)V

    throw v5
.end method

.method public connectionClosedOnError(ILjava/lang/Exception;)V
    .locals 7

    const/4 v0, 0x3

    const-string v1, "GTalkService"

    invoke-static {v1, v0}, Lcom/google/android/gsf/gtalkservice/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "GTalkService/c"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "connectionClosedOnError for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getUsername()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logd(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getConnectionError()Lcom/google/android/gtalkservice/ConnectionError;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gtalkservice/ConnectionError;->getError()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_2

    const/4 v2, 0x6

    if-eq v1, v2, :cond_2

    const/4 v2, 0x5

    if-eq v1, v2, :cond_2

    instance-of v2, p2, Lorg/jivesoftware/smack/XMPPException;

    if-eqz v2, :cond_5

    check-cast p2, Lorg/jivesoftware/smack/XMPPException;

    invoke-virtual {p2}, Lorg/jivesoftware/smack/XMPPException;->getXMPPError()Lorg/jivesoftware/smack/packet/XMPPError;

    move-result-object v2

    invoke-virtual {p2}, Lorg/jivesoftware/smack/XMPPException;->getWrappedThrowable()Ljava/lang/Throwable;

    move-result-object v3

    const-string v4, "GTalkService/c"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "connectionClosedOnError: xmppError = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", ex="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v4, v3}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logd(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v2, :cond_3

    invoke-direct {p0, v2}, Lcom/google/android/gsf/gtalkservice/Endpoint;->translateXMPPError(Lorg/jivesoftware/smack/packet/XMPPError;)I

    move-result v0

    :cond_1
    :goto_0
    move v1, v0

    :cond_2
    :goto_1
    invoke-direct {p0, p1, v1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->sendConnectionClosedMessage(II)V

    return-void

    :cond_3
    invoke-virtual {p2}, Lorg/jivesoftware/smack/XMPPException;->getStreamError()Lorg/jivesoftware/smack/packet/StreamError;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lorg/jivesoftware/smack/packet/StreamError;->getCode()Ljava/lang/String;

    move-result-object v2

    const-string v3, "host-unknown"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    instance-of v0, p2, Ljava/io/IOException;

    if-nez v0, :cond_6

    instance-of v0, p2, Lcom/google/android/gsf/gtalkservice/proto/ProtoBufStreamException;

    if-eqz v0, :cond_7

    :cond_6
    const/4 v1, 0x2

    goto :goto_1

    :cond_7
    const/16 v1, 0xa

    goto :goto_1
.end method

.method public connectionEstablished(Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionStartTime:J

    const-string v0, "GTalkService/c"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "connectionEstablished for jid "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mConnectionStartTime="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionStartTime:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logv(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getConnection()Lorg/jivesoftware/smack/XMPPConnection;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->checkConnection()V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->bumpConnectionCount()V

    invoke-virtual {v0}, Lorg/jivesoftware/smack/XMPPConnection;->getServiceName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->setServerAddress(Ljava/lang/String;)V

    const-string v1, "@"

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    const-string v1, "GTalkService"

    const-string v2, "connectionEstablished:got username with no domain - should not happen."

    invoke-static {v1, v2}, Lcom/google/android/gsf/gtalkservice/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "@"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lorg/jivesoftware/smack/XMPPConnection;->getServiceName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->setJid(Ljava/lang/String;)V

    invoke-static {p1}, Lorg/jivesoftware/smack/util/StringUtils;->parseBareAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->setUserBareAddress(Ljava/lang/String;)V

    invoke-static {p1}, Lorg/jivesoftware/smack/util/StringUtils;->parseResource(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "GTalkService/c"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "connectionEstablished: JID resource="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logv(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->setJidResource(Ljava/lang/String;)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mLastConnectionAttemptSuccessful:Z

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->broadcastConnected()V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getIQPacketManager()Lcom/google/android/gsf/gtalkservice/IQPacketManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gsf/gtalkservice/IQPacketManager;->initConnection(Lorg/jivesoftware/smack/XMPPConnection;)V

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mHttpResponseManager:Lcom/google/android/gsf/gtalkservice/HttpResponseManager;

    invoke-virtual {v1, v0}, Lcom/google/android/gsf/gtalkservice/HttpResponseManager;->initConnection(Lorg/jivesoftware/smack/XMPPConnection;)V

    invoke-direct {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->checkConnection()V

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->connectionEstablishedDelegate(Lorg/jivesoftware/smack/XMPPConnection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v0, "GTalkService/wake"

    const-string v1, "connectionEstablished: release wakelock"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logv(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->releaseAsyncWakeLock()V

    return-void

    :catchall_0
    move-exception v0

    const-string v1, "GTalkService/wake"

    const-string v2, "connectionEstablished: release wakelock"

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logv(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->releaseAsyncWakeLock()V

    throw v0
.end method

.method protected connectionEstablishedDelegate(Lorg/jivesoftware/smack/XMPPConnection;)V
    .locals 0
    .param p1    # Lorg/jivesoftware/smack/XMPPConnection;

    return-void
.end method

.method protected abstract createAsyncWakelockTag()V
.end method

.method public doConnect()V
    .locals 14

    const/16 v6, 0xa

    const/4 v2, 0x3

    const/4 v4, 0x2

    const/4 v13, 0x1

    const/4 v3, 0x0

    const/4 v5, 0x0

    const-string v1, ""

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAccount:Lcom/google/android/gsf/gtalkservice/Account;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/Account;->getUsername()Ljava/lang/String;

    move-result-object v7

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mLastConnectionAttemptSuccessful:Z

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->setConnectionError(I)Z

    const/16 v0, 0x4e20

    invoke-static {v0}, Lorg/jivesoftware/smack/SmackConfiguration;->setPacketReplyTimeout(I)V

    const/4 v0, 0x0

    invoke-static {v0}, Lorg/jivesoftware/smack/SmackConfiguration;->setKeepAliveInterval(I)V

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAccount:Lcom/google/android/gsf/gtalkservice/Account;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/Account;->getAuthToken()Ljava/lang/String;

    move-result-object v8

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAccount:Lcom/google/android/gsf/gtalkservice/Account;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/Account;->getDeviceId()Ljava/lang/String;

    move-result-object v9

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAccount:Lcom/google/android/gsf/gtalkservice/Account;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/Account;->getUseAndroidId()Z

    move-result v10

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "doConnect: EMPTY AUTH-TOKEN, cannot make connection!"

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logw(Ljava/lang/String;)V

    new-instance v0, Lorg/jivesoftware/smack/XMPPException;

    new-instance v8, Lcom/google/android/gsf/gtalkservice/Endpoint$MissingAuthTokenError;

    const/16 v9, 0x190

    const-string v10, "empty auth-token"

    invoke-direct {v8, p0, v9, v10}, Lcom/google/android/gsf/gtalkservice/Endpoint$MissingAuthTokenError;-><init>(Lcom/google/android/gsf/gtalkservice/Endpoint;ILjava/lang/String;)V

    invoke-direct {v0, v8}, Lorg/jivesoftware/smack/XMPPException;-><init>(Lorg/jivesoftware/smack/packet/XMPPError;)V

    throw v0
    :try_end_0
    .catch Lorg/jivesoftware/smack/XMPPException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0}, Lorg/jivesoftware/smack/XMPPException;->getWrappedThrowable()Ljava/lang/Throwable;

    move-result-object v8

    instance-of v9, v8, Ljava/net/UnknownHostException;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v9, :cond_5

    :try_start_2
    const-string v4, "GTalkService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "doConnect: caught "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v4, v0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logd(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :goto_0
    if-nez v2, :cond_0

    if-eqz v5, :cond_b

    :cond_0
    invoke-direct {p0, v2, v5}, Lcom/google/android/gsf/gtalkservice/Endpoint;->handleDoConnectError(ILorg/jivesoftware/smack/packet/XMPPError;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    :try_start_3
    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-static {v0, v7, v8, v9, v10}, Lcom/google/android/gsf/gtalkservice/Account;->createAccountFromSettings(Lcom/google/android/gsf/gtalkservice/service/GTalkService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/gsf/gtalkservice/Account;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/Account;->getHost()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/Account;->getPort()I

    move-result v9

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/Account;->getDomain()Ljava/lang/String;

    move-result-object v10

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAccount:Lcom/google/android/gsf/gtalkservice/Account;

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getJidResource()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v8, "empty JID resource"

    invoke-direct {v0, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catch Lorg/jivesoftware/smack/XMPPException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catch_1
    move-exception v0

    :try_start_4
    const-string v2, "GTalkService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "doConnect("

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getAccountId()J

    move-result-wide v8

    invoke-static {v8, v9, v7}, Lcom/google/android/gsf/gtalkservice/Log;->sanitizeUsername(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ") caught "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logd(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    invoke-direct {p0, v4, v5}, Lcom/google/android/gsf/gtalkservice/Endpoint;->handleDoConnectError(ILorg/jivesoftware/smack/packet/XMPPError;)V

    goto :goto_1

    :cond_3
    :try_start_5
    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getConnectionState()Lcom/google/android/gtalkservice/ConnectionState;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/gtalkservice/ConnectionState;->isLoggedIn()Z

    move-result v12

    if-eqz v12, :cond_4

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->isConnected()Z

    move-result v12

    if-eqz v12, :cond_4

    const-string v0, "GTalkService/c"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "doConnect: acct="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getAccountId()J

    move-result-wide v9

    invoke-static {v9, v10, v7}, Lcom/google/android/gsf/gtalkservice/Log;->sanitizeUsername(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", current conn_state is "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v0, v8}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logd(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Lorg/jivesoftware/smack/XMPPException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    const-string v0, "GTalkService/c"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "doConnect("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getAccountId()J

    move-result-wide v2

    invoke-static {v2, v3, v7}, Lcom/google/android/gsf/gtalkservice/Log;->sanitizeUsername(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") cancel connect: release wakelock"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logd(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->releaseAsyncWakeLock()V

    goto/16 :goto_1

    :cond_4
    :try_start_6
    invoke-virtual {p0, v8, v9, v10, v0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->doConnectDelegate(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->setConnectionState(I)Z
    :try_end_6
    .catch Lorg/jivesoftware/smack/XMPPException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    if-eqz v5, :cond_d

    invoke-direct {p0, v3, v5}, Lcom/google/android/gsf/gtalkservice/Endpoint;->handleDoConnectError(ILorg/jivesoftware/smack/packet/XMPPError;)V

    goto/16 :goto_1

    :cond_5
    :try_start_7
    instance-of v2, v8, Ljava/io/IOException;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    if-eqz v2, :cond_6

    :try_start_8
    const-string v2, "GTalkService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "doConnect: caught "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logd(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    move v2, v4

    goto/16 :goto_0

    :cond_6
    :try_start_9
    invoke-virtual {v0}, Lorg/jivesoftware/smack/XMPPException;->getXMPPError()Lorg/jivesoftware/smack/packet/XMPPError;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move-result-object v2

    :try_start_a
    const-string v4, "GTalkService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "xmppError = "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, " "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lorg/jivesoftware/smack/XMPPException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v4, v0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logd(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    if-nez v2, :cond_f

    move-object v5, v2

    move v2, v6

    goto/16 :goto_0

    :catch_2
    move-exception v0

    :try_start_b
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "doConnect("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getAccountId()J

    move-result-wide v8

    invoke-static {v8, v9, v7}, Lcom/google/android/gsf/gtalkservice/Log;->sanitizeUsername(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ") caught "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, v0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    invoke-direct {p0, v6, v5}, Lcom/google/android/gsf/gtalkservice/Endpoint;->handleDoConnectError(ILorg/jivesoftware/smack/packet/XMPPError;)V

    goto/16 :goto_1

    :cond_7
    invoke-direct {p0, v3}, Lcom/google/android/gsf/gtalkservice/Endpoint;->updateAuthErrorStats(I)V

    :try_start_c
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    const-string v2, "GTalkService/c"

    const-string v3, "(WARNING) doConnect: serverJid is empty or null"

    invoke-virtual {p0, v2, v3}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logd(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    invoke-virtual {p0, v1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->connectionEstablished(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->doConnectSucceededDelegate()V

    const-string v2, "GTalkService/c"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "doConnect: DONE. serverJid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v2, v1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logv(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_c
    .catch Ljava/lang/IllegalStateException; {:try_start_c .. :try_end_c} :catch_3

    :cond_9
    :goto_2
    throw v0

    :catchall_0
    move-exception v0

    move v2, v3

    move-object v4, v5

    :goto_3
    if-nez v2, :cond_a

    if-eqz v4, :cond_7

    :cond_a
    invoke-direct {p0, v2, v4}, Lcom/google/android/gsf/gtalkservice/Endpoint;->handleDoConnectError(ILorg/jivesoftware/smack/packet/XMPPError;)V

    goto/16 :goto_1

    :catch_3
    move-exception v1

    const-string v2, "GTalkService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "connectionEstablished for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getAccountId()J

    move-result-wide v4

    invoke-static {v4, v5, v7}, Lcom/google/android/gsf/gtalkservice/Log;->sanitizeUsername(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " - caught "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/android/gsf/gtalkservice/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->doConnectFailedDelegate()V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getConnectionState()Lcom/google/android/gtalkservice/ConnectionState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gtalkservice/ConnectionState;->isLoggedIn()Z

    move-result v1

    if-eqz v1, :cond_9

    const-string v1, "doConnect: reset connection state to PENDING"

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logw(Ljava/lang/String;)V

    invoke-virtual {p0, v13}, Lcom/google/android/gsf/gtalkservice/Endpoint;->setConnectionState(I)Z

    goto :goto_2

    :cond_b
    invoke-direct {p0, v3}, Lcom/google/android/gsf/gtalkservice/Endpoint;->updateAuthErrorStats(I)V

    :try_start_d
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_c

    const-string v0, "GTalkService/c"

    const-string v2, "(WARNING) doConnect: serverJid is empty or null"

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logd(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    invoke-virtual {p0, v1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->connectionEstablished(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->doConnectSucceededDelegate()V

    const-string v0, "GTalkService/c"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "doConnect: DONE. serverJid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logv(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_d
    .catch Ljava/lang/IllegalStateException; {:try_start_d .. :try_end_d} :catch_4

    goto/16 :goto_1

    :catch_4
    move-exception v0

    const-string v1, "GTalkService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "connectionEstablished for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getAccountId()J

    move-result-wide v3

    invoke-static {v3, v4, v7}, Lcom/google/android/gsf/gtalkservice/Log;->sanitizeUsername(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - caught "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gsf/gtalkservice/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->doConnectFailedDelegate()V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getConnectionState()Lcom/google/android/gtalkservice/ConnectionState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gtalkservice/ConnectionState;->isLoggedIn()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "doConnect: reset connection state to PENDING"

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logw(Ljava/lang/String;)V

    :goto_4
    invoke-virtual {p0, v13}, Lcom/google/android/gsf/gtalkservice/Endpoint;->setConnectionState(I)Z

    goto/16 :goto_1

    :cond_d
    invoke-direct {p0, v3}, Lcom/google/android/gsf/gtalkservice/Endpoint;->updateAuthErrorStats(I)V

    :try_start_e
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_e

    const-string v0, "GTalkService/c"

    const-string v2, "(WARNING) doConnect: serverJid is empty or null"

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logd(Ljava/lang/String;Ljava/lang/String;)V

    :cond_e
    invoke-virtual {p0, v1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->connectionEstablished(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->doConnectSucceededDelegate()V

    const-string v0, "GTalkService/c"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "doConnect: DONE. serverJid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logv(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_e
    .catch Ljava/lang/IllegalStateException; {:try_start_e .. :try_end_e} :catch_5

    goto/16 :goto_1

    :catch_5
    move-exception v0

    const-string v1, "GTalkService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "connectionEstablished for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getAccountId()J

    move-result-wide v3

    invoke-static {v3, v4, v7}, Lcom/google/android/gsf/gtalkservice/Log;->sanitizeUsername(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - caught "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gsf/gtalkservice/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->doConnectFailedDelegate()V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getConnectionState()Lcom/google/android/gtalkservice/ConnectionState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gtalkservice/ConnectionState;->isLoggedIn()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "doConnect: reset connection state to PENDING"

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logw(Ljava/lang/String;)V

    goto :goto_4

    :catchall_1
    move-exception v0

    move-object v4, v5

    goto/16 :goto_3

    :catchall_2
    move-exception v0

    move v2, v4

    move-object v4, v5

    goto/16 :goto_3

    :catchall_3
    move-exception v0

    move-object v4, v2

    move v2, v3

    goto/16 :goto_3

    :cond_f
    move-object v5, v2

    move v2, v3

    goto/16 :goto_0
.end method

.method protected doConnectDelegate(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/jivesoftware/smack/XMPPException;
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method protected doConnectFailedDelegate()V
    .locals 0

    return-void
.end method

.method protected doConnectSucceededDelegate()V
    .locals 0

    return-void
.end method

.method protected doLogout()V
    .locals 2

    const/4 v1, 0x1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mOpened:Z

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->setConnectionError(I)Z

    invoke-virtual {p0, v1, v1, v0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->closeConnection(ZZZ)V

    return-void
.end method

.method public dump(Ljava/io/PrintWriter;)V
    .locals 7

    const-wide/16 v5, 0x3e8

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "XmppConnection="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnection:Lorg/jivesoftware/smack/XMPPConnection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "JID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getJid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Device ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAccount:Lcom/google/android/gsf/gtalkservice/Account;

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/Account;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Session: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->isSessionOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "open"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Connected: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->isConnected()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Connection state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getConnectionState()Lcom/google/android/gtalkservice/ConnectionState;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "connection error: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getConnectionError()Lcom/google/android/gtalkservice/ConnectionError;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string v0, "Connection stats"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string v0, "------------------"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Connections made/attempts: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getNumberOfConnectionsMade()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getNumberOfConnectionsAttempted()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Connection uptime: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getConnectionUptime()I

    move-result v3

    int-to-long v3, v3

    invoke-static {v3, v4}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Last server activity: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getLastActivityFromServerTime()J

    move-result-wide v3

    sub-long v3, v0, v3

    div-long/2addr v3, v5

    invoke-static {v3, v4}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Last device activity: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getLastActivityToServerTime()J

    move-result-wide v3

    sub-long/2addr v0, v3

    div-long/2addr v0, v5

    invoke-static {v0, v1}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :goto_1
    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string v0, "Wakelocks"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string v0, "------------------"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "async conn wakelock: tag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getAsyncWakelockTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAsyncConnWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GTALK_CONN wakelock "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mSimpleWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ClearWakeLockTimer fired count: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mClearWakeLockTimerFiredCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    return-void

    :cond_0
    const-string v0, "closed"

    goto/16 :goto_0

    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Last connect attempt time: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mLastConnectAttemptTs:J

    sub-long/2addr v0, v3

    div-long/2addr v0, v5

    invoke-static {v0, v1}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ago"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method protected dumpConnectionHistory(Ljava/io/PrintWriter;)V
    .locals 6

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionHistory:Ljava/util/ArrayList;

    monitor-enter v3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionHistory:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    neg-int v0, v0

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionHistory:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionLifeCycle;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "conn #("

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "):"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionLifeCycle;->dump(Ljava/io/PrintWriter;)V

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v1, v2

    goto :goto_0

    :cond_0
    monitor-exit v3

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public ensureRouteOverMobileHipriNetworkInterface()Z
    .locals 2

    const-string v0, "GTalkService/c"

    const-string v1, "ensureRouteOverMobileHipriNetworkInterface: not the right endpoint"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logv(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method

.method public forceCloseConnection()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAsyncMessageHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAsyncMessageHandler:Landroid/os/Handler;

    const/16 v2, 0x12c

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAsyncMessageHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    return-void
.end method

.method public getAccount()Lcom/google/android/gsf/gtalkservice/Account;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAccount:Lcom/google/android/gsf/gtalkservice/Account;

    return-object v0
.end method

.method public getAccountId()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAccountId:J

    return-wide v0
.end method

.method public getAccountIdFilter()Lorg/jivesoftware/smack/filter/AccountIdFilter;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAccountIdFilter:Lorg/jivesoftware/smack/filter/AccountIdFilter;

    return-object v0
.end method

.method protected getAsyncWakelockTag()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAsyncWakelockTag:Ljava/lang/String;

    return-object v0
.end method

.method public getConnection()Lorg/jivesoftware/smack/XMPPConnection;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnection:Lorg/jivesoftware/smack/XMPPConnection;

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getConnectionError()Lcom/google/android/gtalkservice/ConnectionError;
    .locals 2

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionError:Lcom/google/android/gtalkservice/ConnectionError;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionError:Lcom/google/android/gtalkservice/ConnectionError;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getConnectionState()Lcom/google/android/gtalkservice/ConnectionState;
    .locals 2

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionState:Lcom/google/android/gtalkservice/ConnectionState;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionState:Lcom/google/android/gtalkservice/ConnectionState;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getConnectionUptime()I
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionStartTime:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    div-int/lit16 v0, v0, 0x3e8

    goto :goto_0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getGTalkService()Lcom/google/android/gsf/gtalkservice/service/GTalkService;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    return-object v0
.end method

.method public getHeartbeatInterval()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mSettingsQueryMap:Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;

    invoke-virtual {v0}, Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;->getHeartbeatInterval()J

    move-result-wide v0

    return-wide v0
.end method

.method public getIQPacketManager()Lcom/google/android/gsf/gtalkservice/IQPacketManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mIqPacketManager:Lcom/google/android/gsf/gtalkservice/IQPacketManager;

    return-object v0
.end method

.method public getJid()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mJid:Ljava/lang/String;

    return-object v0
.end method

.method public getJidResource()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mJidResource:Ljava/lang/String;

    return-object v0
.end method

.method public getLastActivityFromServerTime()J
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnection:Lorg/jivesoftware/smack/XMPPConnection;

    if-nez v0, :cond_1

    :cond_0
    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getConnection()Lorg/jivesoftware/smack/XMPPConnection;

    move-result-object v0

    invoke-virtual {v0}, Lorg/jivesoftware/smack/XMPPConnection;->getLastActivityFromServerTime()J

    move-result-wide v0

    goto :goto_0
.end method

.method public getLastActivityToServerTime()J
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnection:Lorg/jivesoftware/smack/XMPPConnection;

    if-nez v0, :cond_1

    :cond_0
    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getConnection()Lorg/jivesoftware/smack/XMPPConnection;

    move-result-object v0

    invoke-virtual {v0}, Lorg/jivesoftware/smack/XMPPConnection;->getLastActivityToServerTime()J

    move-result-wide v0

    goto :goto_0
.end method

.method public getLastConnectionAttemptSuccessful()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mLastConnectionAttemptSuccessful:Z

    return v0
.end method

.method public getNumberOfConnectionsAttempted()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mNumConnectionsAttempted:I

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getNumberOfConnectionsMade()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mNumConnectionsMade:I

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getOriginalUsername()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mOriginalUserBareAddress:Ljava/lang/String;

    return-object v0
.end method

.method public abstract getPackageName()Ljava/lang/String;
.end method

.method public getRawStanzaSendReceiveManager()Lcom/google/android/gsf/gtalkservice/RawStanzaSendReceiveManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mRawStanzaSendReceiveManager:Lcom/google/android/gsf/gtalkservice/RawStanzaSendReceiveManager;

    return-object v0
.end method

.method protected getResourcePrefix()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    return-object v0
.end method

.method public getServerAddress()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mServerAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getSettingsMap()Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mSettingsQueryMap:Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;

    return-object v0
.end method

.method public getUsername()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mUserBareAddress:Ljava/lang/String;

    return-object v0
.end method

.method public goOnline()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected init(Lcom/google/android/gsf/gtalkservice/Account;)V
    .locals 7
    .param p1    # Lcom/google/android/gsf/gtalkservice/Account;

    const/4 v6, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0, p1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->internalSetAccount(Lcom/google/android/gsf/gtalkservice/Account;)V

    new-instance v0, Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getAccountId()J

    move-result-wide v3

    iget-object v5, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAsyncMessageHandler:Landroid/os/Handler;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;-><init>(Landroid/content/ContentResolver;ZJLandroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mSettingsQueryMap:Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->initJidResource()V

    invoke-direct {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->initJid()V

    new-instance v0, Lorg/jivesoftware/smack/filter/AccountIdFilter;

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getAccountId()J

    move-result-wide v3

    invoke-direct {v0, v3, v4}, Lorg/jivesoftware/smack/filter/AccountIdFilter;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAccountIdFilter:Lorg/jivesoftware/smack/filter/AccountIdFilter;

    new-instance v0, Lcom/google/android/gsf/gtalkservice/IQPacketManager;

    invoke-direct {v0, p0}, Lcom/google/android/gsf/gtalkservice/IQPacketManager;-><init>(Lcom/google/android/gsf/gtalkservice/Endpoint;)V

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mIqPacketManager:Lcom/google/android/gsf/gtalkservice/IQPacketManager;

    new-instance v0, Lcom/google/android/gsf/gtalkservice/HttpResponseManager;

    invoke-direct {v0}, Lcom/google/android/gsf/gtalkservice/HttpResponseManager;-><init>()V

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mHttpResponseManager:Lcom/google/android/gsf/gtalkservice/HttpResponseManager;

    new-instance v0, Lcom/google/android/gsf/gtalkservice/RawStanzaSendReceiveManager;

    invoke-direct {v0}, Lcom/google/android/gsf/gtalkservice/RawStanzaSendReceiveManager;-><init>()V

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mRawStanzaSendReceiveManager:Lcom/google/android/gsf/gtalkservice/RawStanzaSendReceiveManager;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mRawStanzaSendReceiveManager:Lcom/google/android/gsf/gtalkservice/RawStanzaSendReceiveManager;

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-virtual {v0, v1, p0}, Lcom/google/android/gsf/gtalkservice/RawStanzaSendReceiveManager;->init(Lcom/google/android/gsf/gtalkservice/service/GTalkService;Lcom/google/android/gsf/gtalkservice/Endpoint;)V

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionHistory:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionLifeCycle;

    const/4 v3, 0x0

    invoke-direct {v0, p0, v3}, Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionLifeCycle;-><init>(Lcom/google/android/gsf/gtalkservice/Endpoint;Lcom/google/android/gsf/gtalkservice/Endpoint$1;)V

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mCurrentConnectionCycle:Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionLifeCycle;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionHistory:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mCurrentConnectionCycle:Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionLifeCycle;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0, v6}, Lcom/google/android/gsf/gtalkservice/Endpoint;->setConnectionState(I)Z

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mContext:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mPowerManager:Landroid/os/PowerManager;

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->createAsyncWakelockTag()V

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mPowerManager:Landroid/os/PowerManager;

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getAsyncWakelockTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAsyncConnWakeLock:Landroid/os/PowerManager$WakeLock;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAsyncConnWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0, v6}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mPowerManager:Landroid/os/PowerManager;

    const-string v1, "GTALK_CONN"

    invoke-virtual {v0, v2, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mSimpleWakeLock:Landroid/os/PowerManager$WakeLock;

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected initJidResource()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->computeJIDResource()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GTalkService/c"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initJidResource: computeJIDResource returned "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for endpoint "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getUsername()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logv(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->setJidResource(Ljava/lang/String;)V

    return-void
.end method

.method protected internalSetAccount(Lcom/google/android/gsf/gtalkservice/Account;)V
    .locals 2
    .param p1    # Lcom/google/android/gsf/gtalkservice/Account;

    iput-object p1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAccount:Lcom/google/android/gsf/gtalkservice/Account;

    invoke-static {}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getNextAvaialableNonTalkAccountId()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAccountId:J

    return-void
.end method

.method public isConnected()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getConnection()Lorg/jivesoftware/smack/XMPPConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/jivesoftware/smack/XMPPConnection;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isSessionOpen()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mOpened:Z

    return v0
.end method

.method protected logConnectionClosed(III)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I

    return-void
.end method

.method protected logConnectionEvent(IIII)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method protected logd(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/gsf/gtalkservice/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected loge(Ljava/lang/String;)V
    .locals 3

    const-string v0, "GTalkService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ERROR: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "@"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gsf/gtalkservice/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected loge(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 3

    const-string v0, "GTalkService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ERROR: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "@"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Lcom/google/android/gsf/gtalkservice/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method protected logi(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/gsf/gtalkservice/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public login()V
    .locals 7

    const/4 v6, 0x1

    const-string v2, "GTalkService/c"

    const-string v3, "login"

    invoke-virtual {p0, v2, v3}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logd(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    new-instance v2, Lcom/google/android/gsf/gtalkservice/Endpoint$SpecialConnectionEvent;

    const/4 v3, 0x4

    invoke-direct {v2, v3}, Lcom/google/android/gsf/gtalkservice/Endpoint$SpecialConnectionEvent;-><init>(I)V

    invoke-virtual {p0, v2}, Lcom/google/android/gsf/gtalkservice/Endpoint;->addConnectionEvent(Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionEvent;)V

    invoke-static {v6}, Lcom/google/android/gsf/gtalkservice/LogTag;->logEvent(I)V

    iput-wide v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mLastLoginAttemptTimeStamp:J

    iput-boolean v6, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mOpened:Z

    iget-wide v2, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mLastLoginAttemptTimeStamp:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    iget-wide v2, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mLastLoginAttemptTimeStamp:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0xbb8

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    sget-boolean v2, Lcom/google/android/gsf/gtalkservice/LogTag;->sDebug:Z

    if-eqz v2, :cond_0

    const-string v2, "GTalkService"

    const-string v3, "too many login attempts, delay connecting"

    invoke-virtual {p0, v2, v3}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logd(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0, v6}, Lcom/google/android/gsf/gtalkservice/Endpoint;->retryConnection(Z)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->connect()Z

    goto :goto_0
.end method

.method public logout()V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->clearReconnectAlarm()V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->asyncCloseSettingsQueryMap()V

    new-instance v1, Lcom/google/android/gsf/gtalkservice/Endpoint$SpecialConnectionEvent;

    const/4 v2, 0x5

    invoke-direct {v1, v2}, Lcom/google/android/gsf/gtalkservice/Endpoint$SpecialConnectionEvent;-><init>(I)V

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->addConnectionEvent(Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionEvent;)V

    const/4 v1, 0x2

    invoke-static {v1}, Lcom/google/android/gsf/gtalkservice/LogTag;->logEvent(I)V

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAsyncMessageHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAsyncMessageHandler:Landroid/os/Handler;

    const/16 v2, 0xc8

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAsyncMessageHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->setConnectionState(I)Z

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAsyncMessageHandler:Landroid/os/Handler;

    :goto_0
    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionWrapper:Lcom/google/android/gsf/gtalkservice/service/EndpointWrapper;

    invoke-virtual {v1, v2}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->removeConnection(Lcom/google/android/gsf/gtalkservice/service/EndpointWrapper;)V

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->getReconnectManager()Lcom/google/android/gsf/gtalkservice/ReconnectManager;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->removeConnection(Lcom/google/android/gsf/gtalkservice/Endpoint;)V

    return-void

    :cond_0
    const-string v1, "GTalkService"

    const-string v2, "##### logout: mAsyncMessageHandler is already null!"

    invoke-static {v1, v2}, Lcom/google/android/gsf/gtalkservice/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected logv(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x2

    invoke-static {p1, v0}, Lcom/google/android/gsf/gtalkservice/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/gsf/gtalkservice/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected logw(Ljava/lang/String;)V
    .locals 3

    const-string v0, "GTalkService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "@"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gsf/gtalkservice/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected notifyConnectionListeners()V
    .locals 0

    return-void
.end method

.method public abstract onConnectionClosed(I)V
.end method

.method public onGServicesChangeDelegate()V
    .locals 0

    return-void
.end method

.method protected reachedRetryThresholdForAuthExpiration()Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "gtalk_max_retries_for_auth_expired"

    const/4 v2, 0x3

    invoke-static {v0, v1, v2}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iget v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAuthExpiredCount:I

    if-lt v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public releaseAsyncWakeLock()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAsyncConnWakeLock:Landroid/os/PowerManager$WakeLock;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAsyncConnWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAsyncConnWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_0
    const-string v0, "GTalkService/wake"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "releaseAsyncWakeLock: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mAsyncConnWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logv(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mServiceHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mClearWakeLockTimer:Lcom/google/android/gsf/gtalkservice/Endpoint$ClearWakeLockTimer;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public releaseSimpleWakeLock(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mSimpleWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    const-string v0, "GTalkService/wake"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "releaseWakeLock: token="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", wakelock="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mSimpleWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logv(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public requestToRefreshAuthToken()V
    .locals 2

    const-string v0, "GTalkService/c"

    const-string v1, "requestToRefreshAuthToken: send EVENT_REFRESH_AUTH_TOKEN msg"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logd(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mServiceHandler:Landroid/os/Handler;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iput-object p0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method public retryConnection(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    const-string v0, "GTalkService/c"

    const-string v1, "retryConnection now"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logv(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->connect()Z

    :goto_0
    return-void

    :cond_0
    const-string v0, "GTalkService/c"

    const-string v1, "retryConnection later"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logv(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->getReconnectManager()Lcom/google/android/gsf/gtalkservice/ReconnectManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->setReconnectAlarm()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->setConnectionState(I)Z

    goto :goto_0
.end method

.method public sendHeartbeatToServer()Z
    .locals 2

    const-string v0, "GTalkService/c"

    const-string v1, "sendHeartbeatToServer: not the right endpoint"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logv(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method

.method public sendHttpRequest([BLcom/google/android/gtalkservice/IHttpRequestCallback;)V
    .locals 4

    :try_start_0
    new-instance v0, Lorg/jivesoftware/smack/packet/HttpRequest;

    invoke-direct {v0, p1}, Lorg/jivesoftware/smack/packet/HttpRequest;-><init>([B)V

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mHttpResponseManager:Lcom/google/android/gsf/gtalkservice/HttpResponseManager;

    invoke-virtual {v0}, Lorg/jivesoftware/smack/packet/HttpRequest;->getPacketID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p2}, Lcom/google/android/gsf/gtalkservice/HttpResponseManager;->addPending(Ljava/lang/String;Lcom/google/android/gtalkservice/IHttpRequestCallback;)V

    sget-boolean v1, Lcom/google/android/gsf/gtalkservice/LogTag;->sVerbose:Z

    if-eqz v1, :cond_0

    const-string v1, "GTalkService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "req is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lorg/jivesoftware/smack/packet/HttpRequest;->toXML()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gsf/gtalkservice/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->sendPacket(Lorg/jivesoftware/smack/packet/Packet;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "GTalkService"

    const-string v2, "caught exception"

    invoke-static {v1, v2, v0}, Lcom/google/android/gsf/gtalkservice/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public abstract sendIqStanza(Landroid/content/Intent;)V
.end method

.method public abstract sendMessageStanza(Landroid/content/Intent;)V
.end method

.method public sendPacket(Lorg/jivesoftware/smack/packet/Packet;)Z
    .locals 1
    .param p1    # Lorg/jivesoftware/smack/packet/Packet;

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->sendPacket(Lorg/jivesoftware/smack/packet/Packet;Z)Z

    move-result v0

    return v0
.end method

.method public sendPacket(Lorg/jivesoftware/smack/packet/Packet;Z)Z
    .locals 4
    .param p1    # Lorg/jivesoftware/smack/packet/Packet;
    .param p2    # Z

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getAccountId()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Lorg/jivesoftware/smack/packet/Packet;->setAccountId(J)V

    if-eqz p2, :cond_0

    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->getRmq2Manager()Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->send(Lorg/jivesoftware/smack/packet/Packet;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->sendPacketOverMcsConnection(Lorg/jivesoftware/smack/packet/Packet;)Z

    move-result v0

    goto :goto_0
.end method

.method public sendPacketOverMcsConnection(Lorg/jivesoftware/smack/packet/Packet;)Z
    .locals 7
    .param p1    # Lorg/jivesoftware/smack/packet/Packet;

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getConnection()Lorg/jivesoftware/smack/XMPPConnection;

    move-result-object v0

    if-eqz v0, :cond_1

    :try_start_0
    invoke-virtual {v0, p1}, Lorg/jivesoftware/smack/XMPPConnection;->sendPacket(Lorg/jivesoftware/smack/packet/Packet;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v3, 0x1

    :goto_0
    if-eqz v2, :cond_0

    const-string v4, "GTalkService/c"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "send packet "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " caught "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logd(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return v3

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/google/android/gsf/gtalkservice/Endpoint;->constructException(Ljava/lang/String;)Ljava/lang/IllegalStateException;

    move-result-object v2

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    invoke-direct {p0, v4}, Lcom/google/android/gsf/gtalkservice/Endpoint;->constructException(Ljava/lang/String;)Ljava/lang/IllegalStateException;

    move-result-object v2

    goto :goto_0
.end method

.method public abstract sendPresenceStanza(Landroid/content/Intent;)V
.end method

.method public setAccount(Lcom/google/android/gsf/gtalkservice/Account;)V
    .locals 0
    .param p1    # Lcom/google/android/gsf/gtalkservice/Account;

    invoke-virtual {p0, p1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->internalSetAccount(Lcom/google/android/gsf/gtalkservice/Account;)V

    invoke-direct {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->initJid()V

    return-void
.end method

.method protected setConnection(Lorg/jivesoftware/smack/XMPPConnection;)V
    .locals 1
    .param p1    # Lorg/jivesoftware/smack/XMPPConnection;

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnection:Lorg/jivesoftware/smack/XMPPConnection;

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setConnectionError(I)Z
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionError:Lcom/google/android/gtalkservice/ConnectionError;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionError:Lcom/google/android/gtalkservice/ConnectionError;

    invoke-virtual {v0}, Lcom/google/android/gtalkservice/ConnectionError;->getError()I

    move-result v0

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionError:Lcom/google/android/gtalkservice/ConnectionError;

    invoke-virtual {v0, p1}, Lcom/google/android/gtalkservice/ConnectionError;->setError(I)V

    const/4 v0, 0x1

    monitor-exit v1

    :goto_0
    return v0

    :cond_0
    monitor-exit v1

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setConnectionState(I)Z
    .locals 2
    .param p1    # I

    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->setConnectionState(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->addConnectionEvent(I)V

    :cond_0
    return v0
.end method

.method protected setConnectionState(IZ)Z
    .locals 1
    .param p1    # I
    .param p2    # Z

    invoke-direct {p0, p1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->internalSetConnectionState(I)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->notifyConnectionListeners()V

    :cond_0
    return v0
.end method

.method public setConnectionStateAndError(IIZ)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # Z

    invoke-direct {p0, p1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->internalSetConnectionState(I)Z

    move-result v0

    invoke-virtual {p0, p2}, Lcom/google/android/gsf/gtalkservice/Endpoint;->setConnectionError(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    if-nez v0, :cond_1

    if-eqz p3, :cond_2

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->notifyConnectionListeners()V

    :cond_2
    return-void
.end method

.method public setHeartbeatInterval(J)V
    .locals 1
    .param p1    # J

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mSettingsQueryMap:Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;->setHeartbeatInterval(J)V

    return-void
.end method

.method protected setJidResource(Ljava/lang/String;)V
    .locals 3

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "GTalkService/c"

    const-string v1, "setJidResource: failed, jidResource is empty"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logd(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mJidResource:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "GTalkService/c"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gsf/gtalkservice/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "GTalkService/c"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setJidResource: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for endpoint "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getUsername()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logv(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iput-object p1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mJidResource:Ljava/lang/String;

    goto :goto_0
.end method

.method public setSessionWrapper(Lcom/google/android/gsf/gtalkservice/service/EndpointWrapper;)V
    .locals 0
    .param p1    # Lcom/google/android/gsf/gtalkservice/service/EndpointWrapper;

    iput-object p1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionWrapper:Lcom/google/android/gsf/gtalkservice/service/EndpointWrapper;

    return-void
.end method

.method public setUploadHeartbeatStat(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mSettingsQueryMap:Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;->setUploadHeartbeatStat(Z)V

    return-void
.end method

.method public updateAccountStatus()V
    .locals 0

    return-void
.end method

.method public wasLastConnectionRemainConnectedForLongEnough()Z
    .locals 7

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionDurations:Ljava/util/Vector;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionDurations:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-nez v0, :cond_0

    monitor-exit v2

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint;->mConnectionDurations:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->lastElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const-wide/16 v5, 0xa

    cmp-long v0, v3, v5

    if-ltz v0, :cond_1

    move v0, v1

    :goto_1
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
