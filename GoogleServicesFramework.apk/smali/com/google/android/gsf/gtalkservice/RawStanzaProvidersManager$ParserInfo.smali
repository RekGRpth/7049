.class public Lcom/google/android/gsf/gtalkservice/RawStanzaProvidersManager$ParserInfo;
.super Ljava/lang/Object;
.source "RawStanzaProvidersManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gsf/gtalkservice/RawStanzaProvidersManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ParserInfo"
.end annotation


# instance fields
.field elementName:Ljava/lang/String;

.field intentReceiverInfo:Landroid/content/ComponentName;

.field isReceiverEnabled:Z

.field nameSpace:Ljava/lang/String;

.field priority:I

.field type:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/ComponentName;Z)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/content/ComponentName;
    .param p5    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gsf/gtalkservice/RawStanzaProvidersManager$ParserInfo;->elementName:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/gsf/gtalkservice/RawStanzaProvidersManager$ParserInfo;->nameSpace:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gsf/gtalkservice/RawStanzaProvidersManager$ParserInfo;->type:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gsf/gtalkservice/RawStanzaProvidersManager$ParserInfo;->intentReceiverInfo:Landroid/content/ComponentName;

    iput-boolean p5, p0, Lcom/google/android/gsf/gtalkservice/RawStanzaProvidersManager$ParserInfo;->isReceiverEnabled:Z

    return-void
.end method
