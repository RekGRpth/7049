.class public Lcom/google/android/gsf/gtalkservice/ReconnectManager;
.super Ljava/lang/Object;
.source "ReconnectManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gsf/gtalkservice/ReconnectManager$EndpointTask;
    }
.end annotation


# instance fields
.field private mConnectivityManager:Landroid/net/ConnectivityManager;

.field private mContext:Landroid/content/Context;

.field private mEndpoints:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gsf/gtalkservice/Endpoint;",
            ">;"
        }
    .end annotation
.end field

.field private mInMobileHipriorityMode:Z

.field private mInitialReconnectDelay:J

.field mLastInetReportNetworkType:I

.field mLastInetReportStatus:I

.field mLastInetReportSuccessful:Z

.field mLastInetReportTs:J

.field private mLastMobileNetworkOutageTs:J

.field private mLastNetworkAvailable:Z

.field private mLastNetworkBroadcastTs:J

.field private mLock:Ljava/lang/Object;

.field private mMainEndpoint:Lcom/google/android/gsf/gtalkservice/Endpoint;

.field private mMaxReconnectDelay:J

.field mNetworkState:Landroid/net/NetworkInfo$State;

.field private mNetworkSuspended:Z

.field mNetworkType:I

.field mNotifyNetworkState:Landroid/net/NetworkInfo$State;

.field mNotifyNetworkType:I

.field private mRandomGenerator:Ljava/util/Random;

.field private mReconnectAlarm:Lcom/google/android/gsf/gtalkservice/Alarm;

.field private mReconnectAlarmSet:Z

.field private mReconnectBackoffRateMultiplier:D

.field private mReconnectDelay:J

.field private mWifiDisconnectedTimeStamp:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1    # Landroid/content/Context;

    const/4 v5, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v5, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mNetworkSuspended:Z

    sget-object v2, Landroid/net/NetworkInfo$State;->UNKNOWN:Landroid/net/NetworkInfo$State;

    iput-object v2, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mNetworkState:Landroid/net/NetworkInfo$State;

    sget-object v2, Landroid/net/NetworkInfo$State;->UNKNOWN:Landroid/net/NetworkInfo$State;

    iput-object v2, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mNotifyNetworkState:Landroid/net/NetworkInfo$State;

    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mLock:Ljava/lang/Object;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mEndpoints:Ljava/util/List;

    new-instance v2, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Ljava/util/Random;-><init>(J)V

    iput-object v2, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mRandomGenerator:Ljava/util/Random;

    iput-object p1, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mContext:Landroid/content/Context;

    const-string v3, "connectivity"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/ConnectivityManager;

    iput-object v2, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-direct {p0}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->clearNetworkOutageTimestamp()V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->initConnectionState()V

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v2}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    :cond_0
    invoke-virtual {p0, v5, v0}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->reportInetCondition(ZI)V

    invoke-virtual {p0, v5}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->resetReconnectionTimer(Z)V

    new-instance v2, Lcom/google/android/gsf/gtalkservice/Alarm;

    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mContext:Landroid/content/Context;

    const-string v4, "GTALK_CONN_ALARM"

    new-instance v5, Lcom/google/android/gsf/gtalkservice/ReconnectManager$1;

    invoke-direct {v5, p0}, Lcom/google/android/gsf/gtalkservice/ReconnectManager$1;-><init>(Lcom/google/android/gsf/gtalkservice/ReconnectManager;)V

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/gsf/gtalkservice/Alarm;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Runnable;)V

    iput-object v2, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mReconnectAlarm:Lcom/google/android/gsf/gtalkservice/Alarm;

    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mReconnectAlarm:Lcom/google/android/gsf/gtalkservice/Alarm;

    const-string v3, "com.google.android.intent.action.GTALK_RECONNECT"

    invoke-virtual {v2, v3}, Lcom/google/android/gsf/gtalkservice/Alarm;->setAction(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/gsf/gtalkservice/ReconnectManager;)V
    .locals 0
    .param p0    # Lcom/google/android/gsf/gtalkservice/ReconnectManager;

    invoke-direct {p0}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->handleAlarmCallback()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/gsf/gtalkservice/ReconnectManager;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/gsf/gtalkservice/ReconnectManager;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->log(Ljava/lang/String;)V

    return-void
.end method

.method private checkThrottleReconnect(JJ)Z
    .locals 7
    .param p1    # J
    .param p3    # J

    const/4 v2, 0x0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sub-long v3, v0, p1

    const-wide/16 v5, 0x4e20

    cmp-long v3, v3, v5

    if-gtz v3, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    sub-long v3, v0, p3

    const-wide/32 v5, 0xea60

    cmp-long v3, v3, v5

    if-lez v3, :cond_0

    const-string v2, "GTalkService"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gsf/gtalkservice/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "checkThrottleReconnect = true"

    invoke-direct {p0, v2}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->log(Ljava/lang/String;)V

    :cond_2
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private clearNetworkOutageTimestamp()V
    .locals 2

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mLastMobileNetworkOutageTs:J

    return-void
.end method

.method private handleAlarmCallback()V
    .locals 2

    new-instance v0, Lcom/google/android/gsf/gtalkservice/ReconnectManager$2;

    invoke-direct {v0, p0}, Lcom/google/android/gsf/gtalkservice/ReconnectManager$2;-><init>(Lcom/google/android/gsf/gtalkservice/ReconnectManager;)V

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->runTaskForEndpoints(Lcom/google/android/gsf/gtalkservice/ReconnectManager$EndpointTask;I)V

    return-void
.end method

.method private isWanMobileNetwork(I)Z
    .locals 2
    .param p1    # I

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    const/4 v1, 0x7

    if-eq p1, v1, :cond_0

    const/16 v1, 0x9

    if-eq p1, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private log(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const-string v0, "GTalkService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ReconnectMgr] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gsf/gtalkservice/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private runTaskForEndpoints(Lcom/google/android/gsf/gtalkservice/ReconnectManager$EndpointTask;I)V
    .locals 9
    .param p1    # Lcom/google/android/gsf/gtalkservice/ReconnectManager$EndpointTask;
    .param p2    # I

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->getMainEndpoint()Lcom/google/android/gsf/gtalkservice/Endpoint;

    move-result-object v5

    if-nez v5, :cond_1

    const-string v7, "GTalkService"

    const-string v8, "runTaskForEndpoints: mainEndpoint not found, bail"

    invoke-static {v7, v8}, Lcom/google/android/gsf/gtalkservice/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {p1, v5, p2}, Lcom/google/android/gsf/gtalkservice/ReconnectManager$EndpointTask;->run(Lcom/google/android/gsf/gtalkservice/Endpoint;I)V

    const/4 v6, 0x0

    const/4 v3, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v7, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mEndpoints:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_2

    iget-object v7, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mEndpoints:Ljava/util/List;

    const/4 v8, 0x0

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Lcom/google/android/gsf/gtalkservice/Endpoint;

    move-object v6, v0

    :goto_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v6, :cond_3

    invoke-interface {p1, v6, p2}, Lcom/google/android/gsf/gtalkservice/ReconnectManager$EndpointTask;->run(Lcom/google/android/gsf/gtalkservice/Endpoint;I)V

    goto :goto_0

    :cond_2
    :try_start_1
    new-instance v4, Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mEndpoints:Ljava/util/List;

    invoke-direct {v4, v7}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object v3, v4

    goto :goto_1

    :catchall_0
    move-exception v7

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v7

    :cond_3
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gsf/gtalkservice/Endpoint;

    invoke-interface {p1, v1, p2}, Lcom/google/android/gsf/gtalkservice/ReconnectManager$EndpointTask;->run(Lcom/google/android/gsf/gtalkservice/Endpoint;I)V

    goto :goto_2
.end method

.method private setConnectionError(I)V
    .locals 1
    .param p1    # I

    new-instance v0, Lcom/google/android/gsf/gtalkservice/ReconnectManager$4;

    invoke-direct {v0, p0}, Lcom/google/android/gsf/gtalkservice/ReconnectManager$4;-><init>(Lcom/google/android/gsf/gtalkservice/ReconnectManager;)V

    invoke-direct {p0, v0, p1}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->runTaskForEndpoints(Lcom/google/android/gsf/gtalkservice/ReconnectManager$EndpointTask;I)V

    return-void
.end method

.method private setInternalNetworkState(Landroid/net/NetworkInfo$State;I)V
    .locals 2
    .param p1    # Landroid/net/NetworkInfo$State;
    .param p2    # I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setInternalNetworkState: type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->log(Ljava/lang/String;)V

    sget-object v0, Landroid/net/NetworkInfo$State;->SUSPENDED:Landroid/net/NetworkInfo$State;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mNetworkSuspended:Z

    iput-object p1, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mNetworkState:Landroid/net/NetworkInfo$State;

    iput p2, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mNetworkType:I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private shouldResetReconnectTimer()Z
    .locals 6

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->getMainEndpoint()Lcom/google/android/gsf/gtalkservice/Endpoint;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v4, "GTalkService"

    const-string v5, "[ReconnectMgr] shouldResetReconnectTimer: no connection!"

    invoke-static {v4, v5}, Lcom/google/android/gsf/gtalkservice/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return v3

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getLastConnectionAttemptSuccessful()Z

    move-result v1

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->wasLastConnectionRemainConnectedForLongEnough()Z

    move-result v2

    if-eqz v1, :cond_2

    if-eqz v2, :cond_2

    const/4 v3, 0x1

    :cond_2
    if-nez v3, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "shouldResetReconnectTimer: lastConnectionWasOfMininumDuration="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", lastAttemptSuccessful="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->log(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public addConnection(Lcom/google/android/gsf/gtalkservice/Endpoint;)V
    .locals 1
    .param p1    # Lcom/google/android/gsf/gtalkservice/Endpoint;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mEndpoints:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public cancelReconnectAlarm()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mReconnectAlarmSet:Z

    if-eqz v0, :cond_1

    const-string v0, "GTalkService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gsf/gtalkservice/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "cancelReconnectAlarm"

    invoke-direct {p0, v0}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->log(Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mReconnectAlarmSet:Z

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mReconnectAlarm:Lcom/google/android/gsf/gtalkservice/Alarm;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/Alarm;->stop()V

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public clearReconnectAlarm()V
    .locals 2

    const-string v0, "GTalkService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gsf/gtalkservice/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "clearReconnectAlarm"

    invoke-direct {p0, v0}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->log(Ljava/lang/String;)V

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mReconnectAlarm:Lcom/google/android/gsf/gtalkservice/Alarm;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/Alarm;->clearAlarm()V

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public dump(Ljava/io/PrintWriter;)V
    .locals 12
    .param p1    # Ljava/io/PrintWriter;

    const-wide/16 v10, 0x3e8

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ReconnectManager (now="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mReconnectAlarm:Lcom/google/android/gsf/gtalkservice/Alarm;

    if-eqz v0, :cond_2

    const-string v7, "-------------"

    invoke-virtual {p1, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/Alarm;->dump(Ljava/io/PrintWriter;)V

    const-string v7, "-------------"

    invoke-virtual {p1, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->getReconnectTime()J

    move-result-wide v3

    sub-long v1, v3, v5

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-lez v7, :cond_1

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Next reconnect alarm will trigger in "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    div-long v8, v1, v10

    invoke-static {v8, v9}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "s"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :goto_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Last network state notification: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mNotifyNetworkType:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mNotifyNetworkState:Landroid/net/NetworkInfo$State;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", time: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mLastNetworkBroadcastTs:J

    sub-long v8, v5, v8

    div-long/2addr v8, v10

    invoke-static {v8, v9}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "s ago"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "active network type (polled): "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mNetworkType:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "active network state (polled): "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mNetworkState:Landroid/net/NetworkInfo$State;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-boolean v7, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mNetworkSuspended:Z

    if-eqz v7, :cond_0

    const-string v7, "network is suspended"

    invoke-virtual {p1, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Last network available: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-boolean v8, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mLastNetworkAvailable:Z

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Current network available: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->isNetworkAvailable()Z

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "InMobileHipriMode: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-boolean v8, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mInMobileHipriorityMode:Z

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Last Inet report: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-boolean v7, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mLastInetReportSuccessful:Z

    if-eqz v7, :cond_3

    const-string v7, "successful"

    :goto_1
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", status="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mLastInetReportStatus:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", netType="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mLastInetReportNetworkType:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mLastInetReportTs:J

    sub-long v8, v5, v8

    div-long/2addr v8, v10

    invoke-static {v8, v9}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "s ago"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    return-void

    :cond_1
    const-string v7, "No reconnect alarm set"

    invoke-virtual {p1, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_2
    const-string v7, "null alarm!"

    invoke-virtual {p1, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_3
    const-string v7, "failed"

    goto :goto_1
.end method

.method public getInitialDelay()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mInitialReconnectDelay:J

    return-wide v0
.end method

.method public getLastNetworkAvailable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mLastNetworkAvailable:Z

    return v0
.end method

.method public getMainEndpoint()Lcom/google/android/gsf/gtalkservice/Endpoint;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mMainEndpoint:Lcom/google/android/gsf/gtalkservice/Endpoint;

    return-object v0
.end method

.method public getNetworkState()Landroid/net/NetworkInfo$State;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mNetworkState:Landroid/net/NetworkInfo$State;

    return-object v0
.end method

.method public getNetworkType()I
    .locals 1

    iget v0, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mNetworkType:I

    return v0
.end method

.method public getReconnectTime()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mReconnectAlarm:Lcom/google/android/gsf/gtalkservice/Alarm;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/Alarm;->getNextAlarmTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public initConnectionState()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->pollNetworkAvailable()Z

    return-void
.end method

.method public isNetworkAvailable()Z
    .locals 2

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public networkStateChanged(Landroid/net/NetworkInfo;JJ)V
    .locals 24
    .param p1    # Landroid/net/NetworkInfo;
    .param p2    # J
    .param p4    # J

    invoke-virtual/range {p1 .. p1}, Landroid/net/NetworkInfo;->getType()I

    move-result v17

    invoke-virtual/range {p1 .. p1}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v5

    const/4 v6, -0x1

    const/4 v9, 0x0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v20

    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mLastNetworkBroadcastTs:J

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mNotifyNetworkState:Landroid/net/NetworkInfo$State;

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mNotifyNetworkType:I

    if-eqz v5, :cond_1

    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mLastNetworkAvailable:Z

    invoke-virtual {v5}, Landroid/net/NetworkInfo;->getType()I

    move-result v6

    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->getMainEndpoint()Lcom/google/android/gsf/gtalkservice/Endpoint;

    move-result-object v12

    if-nez v12, :cond_2

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->reportInetCondition(Z)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mLastNetworkAvailable:Z

    goto :goto_0

    :cond_2
    invoke-virtual {v12}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getConnectionState()Lcom/google/android/gtalkservice/ConnectionState;

    move-result-object v9

    const/16 v20, 0x5

    move/from16 v0, v17

    move/from16 v1, v20

    if-ne v0, v1, :cond_7

    sget-object v20, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    if-ne v0, v1, :cond_6

    if-eqz v6, :cond_3

    const/16 v20, 0x6

    move/from16 v0, v20

    if-ne v6, v0, :cond_4

    :cond_3
    invoke-virtual {v12}, Lcom/google/android/gsf/gtalkservice/Endpoint;->ensureRouteOverMobileHipriNetworkInterface()Z

    move-result v20

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mInMobileHipriorityMode:Z

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mInMobileHipriorityMode:Z

    move/from16 v20, v0

    if-eqz v20, :cond_5

    const-string v20, "networkStateChanged for MOBILE_HIPRI: set MOBILE_HIPRI=true"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->log(Ljava/lang/String;)V

    :cond_4
    :goto_2
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mLastNetworkAvailable:Z

    move/from16 v20, v0

    if-eqz v20, :cond_16

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mInMobileHipriorityMode:Z

    move/from16 v20, v0

    if-eqz v20, :cond_a

    invoke-virtual {v12}, Lcom/google/android/gsf/gtalkservice/Endpoint;->isConnected()Z

    move-result v20

    if-eqz v20, :cond_9

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "networkStateChanged: active_net_type="

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", current_net_type="

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mNetworkType:I

    move/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", in MOBILE_HIPRI, ignore"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->log(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_5
    const-string v20, "networkStateChanged for MOBILE_HIPRI: MOBILE_HIPRI=false, ensureRouteOverMobileHipriNetworkInterface() failed"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->log(Ljava/lang/String;)V

    goto :goto_2

    :cond_6
    sget-object v20, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    if-ne v0, v1, :cond_4

    const-string v20, "networkStateChanged: MOBILE_HIPRI disconnected"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->log(Ljava/lang/String;)V

    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mInMobileHipriorityMode:Z

    goto :goto_2

    :cond_7
    const/16 v20, 0x1

    move/from16 v0, v17

    move/from16 v1, v20

    if-ne v0, v1, :cond_4

    sget-object v20, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    if-ne v0, v1, :cond_8

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v20

    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mWifiDisconnectedTimeStamp:J

    goto/16 :goto_2

    :cond_8
    sget-object v20, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    if-ne v0, v1, :cond_4

    const-wide/16 v20, 0x0

    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mWifiDisconnectedTimeStamp:J

    goto/16 :goto_2

    :cond_9
    const-string v20, "networkStateChanged: reset MOBILE_HIPRI to false"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->log(Ljava/lang/String;)V

    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mInMobileHipriorityMode:Z

    :cond_a
    const-string v20, "GTalkService"

    const/16 v21, 0x3

    invoke-static/range {v20 .. v21}, Lcom/google/android/gsf/gtalkservice/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v20

    if-eqz v20, :cond_b

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "networkStateChanged (has active network): active_network_type="

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", curr_network_type="

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mNetworkType:I

    move/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", curr_network_state="

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mNetworkState:Landroid/net/NetworkInfo$State;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", mcs_conn_state="

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->log(Ljava/lang/String;)V

    :cond_b
    invoke-virtual {v5}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v7

    invoke-virtual {v9}, Lcom/google/android/gtalkservice/ConnectionState;->getState()I

    move-result v20

    const/16 v21, 0x2

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_d

    const/4 v14, 0x1

    :goto_3
    if-eqz v12, :cond_e

    invoke-virtual {v12}, Lcom/google/android/gsf/gtalkservice/Endpoint;->isConnected()Z

    move-result v13

    :goto_4
    const/16 v18, 0x0

    if-nez v14, :cond_c

    if-eqz v13, :cond_f

    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mNetworkState:Landroid/net/NetworkInfo$State;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    if-ne v7, v0, :cond_f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mNetworkType:I

    move/from16 v20, v0

    move/from16 v0, v20

    if-ne v6, v0, :cond_f

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "### networkStateChanged: active and curr network type/state are the same("

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "), ignore"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->log(Ljava/lang/String;)V

    if-eqz v13, :cond_0

    const/16 v20, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->reportInetCondition(Z)V

    goto/16 :goto_1

    :cond_d
    const/4 v14, 0x0

    goto :goto_3

    :cond_e
    const/4 v13, 0x0

    goto :goto_4

    :cond_f
    invoke-virtual {v5}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v20

    invoke-virtual {v5}, Landroid/net/NetworkInfo;->getType()I

    move-result v21

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->setInternalNetworkState(Landroid/net/NetworkInfo$State;I)V

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->isWanMobileNetwork(I)Z

    move-result v20

    if-eqz v20, :cond_10

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mWifiDisconnectedTimeStamp:J

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    move-wide/from16 v3, p4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->checkThrottleReconnect(JJ)Z

    move-result v18

    :cond_10
    invoke-virtual {v12}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getConnectionError()Lcom/google/android/gtalkservice/ConnectionError;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/gtalkservice/ConnectionError;->getError()I

    move-result v20

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_11

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->setConnectionError(I)V

    :cond_11
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mNetworkSuspended:Z

    if-nez v13, :cond_12

    if-eqz v18, :cond_12

    if-eqz v15, :cond_14

    :cond_12
    const/16 v20, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->retryConnection(Z)V

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->resetReconnectionTimer(Z)V

    :cond_13
    :goto_5
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->clearNetworkOutageTimestamp()V

    goto/16 :goto_1

    :cond_14
    invoke-virtual {v9}, Lcom/google/android/gtalkservice/ConnectionState;->getState()I

    move-result v20

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_13

    const/16 v19, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->isWanMobileNetwork(I)Z

    move-result v20

    if-eqz v20, :cond_15

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mLastMobileNetworkOutageTs:J

    move-wide/from16 v20, v0

    const-wide/16 v22, 0x0

    cmp-long v20, v20, v22

    if-eqz v20, :cond_15

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mLastMobileNetworkOutageTs:J

    move-wide/from16 v22, v0

    sub-long v10, v20, v22

    invoke-virtual {v12}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getAccount()Lcom/google/android/gsf/gtalkservice/Account;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/gsf/gtalkservice/Account;->getShortNetworkDowntime()I

    move-result v20

    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v20, v0

    cmp-long v20, v10, v20

    if-lez v20, :cond_15

    const/16 v19, 0x1

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "networkStateChanged: mLastMobileNetworkOutageTs="

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mLastMobileNetworkOutageTs:J

    move-wide/from16 v21, v0

    invoke-virtual/range {v20 .. v22}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", diff="

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", use long delay"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->log(Ljava/lang/String;)V

    :cond_15
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->resetReconnectionTimer(Z)V

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->retryConnection(Z)V

    goto/16 :goto_5

    :cond_16
    const-string v20, "GTalkService"

    const/16 v21, 0x3

    invoke-static/range {v20 .. v21}, Lcom/google/android/gsf/gtalkservice/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v20

    if-eqz v20, :cond_17

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "networkStateChanged (no active network): , notify_network_type="

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", notify_network_state="

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->log(Ljava/lang/String;)V

    :cond_17
    sget-object v20, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    const/16 v21, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->setInternalNetworkState(Landroid/net/NetworkInfo$State;I)V

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mLastMobileNetworkOutageTs:J

    move-wide/from16 v20, v0

    const-wide/16 v22, 0x0

    cmp-long v20, v20, v22

    if-nez v20, :cond_18

    sget-object v20, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    if-ne v0, v1, :cond_1a

    const-wide/16 v20, 0x0

    cmp-long v20, p2, v20

    if-lez v20, :cond_1a

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v20

    sub-long v20, v20, p2

    const-wide/16 v22, 0x4e20

    cmp-long v20, v20, v22

    if-gez v20, :cond_1a

    const/4 v8, 0x1

    :goto_6
    if-nez v8, :cond_18

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mLastMobileNetworkOutageTs:J

    :cond_18
    const/16 v20, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->setConnectionError(I)V

    invoke-virtual {v12}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getConnectionState()Lcom/google/android/gtalkservice/ConnectionState;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/gtalkservice/ConnectionState;->isLoggedIn()Z

    move-result v20

    if-eqz v20, :cond_0

    const-string v20, "GTalkService"

    const/16 v21, 0x3

    invoke-static/range {v20 .. v21}, Lcom/google/android/gsf/gtalkservice/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v20

    if-eqz v20, :cond_19

    const-string v20, "network down, force close conn"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->log(Ljava/lang/String;)V

    :cond_19
    invoke-virtual {v12}, Lcom/google/android/gsf/gtalkservice/Endpoint;->forceCloseConnection()V

    goto/16 :goto_1

    :cond_1a
    const/4 v8, 0x0

    goto :goto_6
.end method

.method public pollNetworkAvailable()Z
    .locals 3

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mLastNetworkAvailable:Z

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->setInternalNetworkState(Landroid/net/NetworkInfo$State;I)V

    :goto_0
    iget-boolean v1, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mLastNetworkAvailable:Z

    return v1

    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mLastNetworkAvailable:Z

    sget-object v1, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    const/4 v2, -0x1

    invoke-direct {p0, v1, v2}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->setInternalNetworkState(Landroid/net/NetworkInfo$State;I)V

    goto :goto_0
.end method

.method public removeConnection(Lcom/google/android/gsf/gtalkservice/Endpoint;)V
    .locals 1
    .param p1    # Lcom/google/android/gsf/gtalkservice/Endpoint;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mMainEndpoint:Lcom/google/android/gsf/gtalkservice/Endpoint;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->setMainEndpoint(Lcom/google/android/gsf/gtalkservice/Endpoint;)V

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mEndpoints:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public reportInetCondition(Z)V
    .locals 1
    .param p1    # Z

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->getNetworkType()I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->reportInetCondition(ZI)V

    return-void
.end method

.method public reportInetCondition(ZI)V
    .locals 9
    .param p1    # Z
    .param p2    # I

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mLock:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "### report Inet status: online="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", networkType="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->log(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p1, :cond_0

    const/16 v1, 0x64

    :cond_0
    :try_start_1
    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v5, "reportInetCondition"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    sget-object v8, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    sget-object v8, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v8, v6, v7

    invoke-virtual {v3, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v0, v3, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mLastInetReportSuccessful:Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    :try_start_2
    iput v1, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mLastInetReportStatus:I

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    iput-wide v5, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mLastInetReportTs:J

    iput p2, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mLastInetReportNetworkType:I

    monitor-exit v4

    return-void

    :catch_0
    move-exception v2

    const-string v3, "GTalkService"

    const-string v5, "calling reportInetCondition failed"

    invoke-static {v3, v5, v2}, Lcom/google/android/gsf/gtalkservice/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mLastInetReportSuccessful:Z

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3
.end method

.method public resetReconnectionTimer(Z)V
    .locals 9
    .param p1    # Z

    const-wide/16 v7, 0x3e8

    const/4 v6, 0x3

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->getMainEndpoint()Lcom/google/android/gsf/gtalkservice/Endpoint;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-wide v2, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mReconnectDelay:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    iget-wide v2, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mReconnectDelay:J

    iget-wide v4, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mInitialReconnectDelay:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    const-string v2, "GTalkService"

    invoke-static {v2, v6}, Lcom/google/android/gsf/gtalkservice/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mReconnDelay set to initial value "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mInitialReconnectDelay:J

    div-long/2addr v3, v7

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->log(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getAccount()Lcom/google/android/gsf/gtalkservice/Account;

    move-result-object v0

    if-eqz p1, :cond_4

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/Account;->getMinReconnectDelayLong()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mRandomGenerator:Ljava/util/Random;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/Account;->getReconnectVariantLong()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/2addr v2, v3

    int-to-long v2, v2

    iput-wide v2, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mInitialReconnectDelay:J

    const-string v2, "GTalkService"

    invoke-static {v2, v6}, Lcom/google/android/gsf/gtalkservice/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "use long initial value: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mInitialReconnectDelay:J

    div-long/2addr v3, v7

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->log(Ljava/lang/String;)V

    :cond_3
    :goto_1
    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/Account;->getMaxReconnectDelay()I

    move-result v2

    int-to-long v2, v2

    iput-wide v2, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mMaxReconnectDelay:J

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/Account;->getReconnectBackoffRateMultiplier()D

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mReconnectBackoffRateMultiplier:D

    iget-wide v2, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mInitialReconnectDelay:J

    iput-wide v2, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mReconnectDelay:J

    goto :goto_0

    :cond_4
    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/Account;->getMinReconnectDelayShort()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mRandomGenerator:Ljava/util/Random;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/Account;->getReconnectVariantShort()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/2addr v2, v3

    int-to-long v2, v2

    iput-wide v2, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mInitialReconnectDelay:J

    const-string v2, "GTalkService"

    invoke-static {v2, v6}, Lcom/google/android/gsf/gtalkservice/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "use short initial value: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mInitialReconnectDelay:J

    div-long/2addr v3, v7

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->log(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public retryConnection(Z)V
    .locals 2
    .param p1    # Z

    new-instance v1, Lcom/google/android/gsf/gtalkservice/ReconnectManager$3;

    invoke-direct {v1, p0}, Lcom/google/android/gsf/gtalkservice/ReconnectManager$3;-><init>(Lcom/google/android/gsf/gtalkservice/ReconnectManager;)V

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v1, v0}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->runTaskForEndpoints(Lcom/google/android/gsf/gtalkservice/ReconnectManager$EndpointTask;I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setMainEndpoint(Lcom/google/android/gsf/gtalkservice/Endpoint;)V
    .locals 0
    .param p1    # Lcom/google/android/gsf/gtalkservice/Endpoint;

    iput-object p1, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mMainEndpoint:Lcom/google/android/gsf/gtalkservice/Endpoint;

    return-void
.end method

.method public setReconnectAlarm()V
    .locals 11

    monitor-enter p0

    :try_start_0
    iget-boolean v6, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mReconnectAlarmSet:Z

    if-eqz v6, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->getReconnectTime()J

    move-result-wide v0

    cmp-long v6, v0, v4

    if-gez v6, :cond_5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "alarm failed to fire: alarmTime="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", now="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->log(Ljava/lang/String;)V

    :cond_0
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mReconnectAlarmSet:Z

    invoke-direct {p0}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->shouldResetReconnectTimer()Z

    move-result v6

    if-eqz v6, :cond_2

    const-string v6, "GTalkService"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Lcom/google/android/gsf/gtalkservice/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_1

    const-string v6, "setReconAlarm: reset timer"

    invoke-direct {p0, v6}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->log(Ljava/lang/String;)V

    :cond_1
    const/4 v6, 0x0

    invoke-virtual {p0, v6}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->resetReconnectionTimer(Z)V

    :cond_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "setReconAlarm: retry in "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-wide v7, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mReconnectDelay:J

    const-wide/16 v9, 0x3e8

    div-long/2addr v7, v9

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "s"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->log(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mReconnectAlarm:Lcom/google/android/gsf/gtalkservice/Alarm;

    invoke-virtual {v6}, Lcom/google/android/gsf/gtalkservice/Alarm;->isStarted()Z

    move-result v6

    if-nez v6, :cond_3

    iget-object v6, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mReconnectAlarm:Lcom/google/android/gsf/gtalkservice/Alarm;

    invoke-virtual {v6}, Lcom/google/android/gsf/gtalkservice/Alarm;->initAlarm()V

    iget-object v6, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mReconnectAlarm:Lcom/google/android/gsf/gtalkservice/Alarm;

    iget-wide v7, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mReconnectDelay:J

    invoke-virtual {v6, v7, v8}, Lcom/google/android/gsf/gtalkservice/Alarm;->setTimer(J)V

    iget-object v6, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mReconnectAlarm:Lcom/google/android/gsf/gtalkservice/Alarm;

    invoke-virtual {v6}, Lcom/google/android/gsf/gtalkservice/Alarm;->start()V

    :cond_3
    iget-wide v6, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mReconnectDelay:J

    long-to-double v6, v6

    iget-wide v8, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mReconnectBackoffRateMultiplier:D

    mul-double/2addr v6, v8

    double-to-long v2, v6

    iget-wide v6, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mMaxReconnectDelay:J

    cmp-long v6, v2, v6

    if-gez v6, :cond_7

    :goto_0
    iput-wide v2, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mReconnectDelay:J

    const-string v6, "GTalkService"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Lcom/google/android/gsf/gtalkservice/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "setReconAlarm: set delay to "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-wide v7, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mReconnectDelay:J

    const-wide/16 v9, 0x3e8

    div-long/2addr v7, v9

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "s"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->log(Ljava/lang/String;)V

    :cond_4
    monitor-exit p0

    :goto_1
    return-void

    :cond_5
    const-string v6, "GTalkService"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Lcom/google/android/gsf/gtalkservice/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_6

    const-string v6, "setReconAlarm: alarm already set"

    invoke-direct {p0, v6}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->log(Ljava/lang/String;)V

    :cond_6
    monitor-exit p0

    goto :goto_1

    :catchall_0
    move-exception v6

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6

    :cond_7
    :try_start_1
    iget-wide v2, p0, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->mMaxReconnectDelay:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method
