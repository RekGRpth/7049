.class public Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery$Item;
.super Ljava/lang/Object;
.source "OtrQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Item"
.end annotation


# instance fields
.field public mChangedByBuddy:Z

.field public mEnabled:Z

.field public mJid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery$Item;-><init>(Ljava/lang/String;ZZ)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ZZ)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Z
    .param p3    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery$Item;->mJid:Ljava/lang/String;

    iput-boolean p2, p0, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery$Item;->mEnabled:Z

    iput-boolean p3, p0, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery$Item;->mChangedByBuddy:Z

    return-void
.end method


# virtual methods
.method public getChangedByBuddy()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery$Item;->mChangedByBuddy:Z

    return v0
.end method

.method public getJid()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery$Item;->mJid:Ljava/lang/String;

    return-object v0
.end method

.method public isEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery$Item;->mEnabled:Z

    return v0
.end method

.method public toProtoBuf()Lcom/google/common/io/protocol/ProtoBuf;
    .locals 3

    new-instance v0, Lcom/google/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/gsf/gtalkservice/proto/GtalkExtensionsMessageTypes;->OTR_ITEM:Lcom/google/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/common/io/protocol/ProtoBuf;-><init>(Lcom/google/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery$Item;->mJid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery$Item;->mEnabled:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setBool(IZ)V

    iget-boolean v1, p0, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery$Item;->mChangedByBuddy:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery$Item;->mChangedByBuddy:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setBool(IZ)V

    :cond_0
    return-object v0
.end method
