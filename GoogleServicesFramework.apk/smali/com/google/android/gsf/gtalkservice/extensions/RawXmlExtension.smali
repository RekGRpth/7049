.class public Lcom/google/android/gsf/gtalkservice/extensions/RawXmlExtension;
.super Ljava/lang/Object;
.source "RawXmlExtension.java"

# interfaces
.implements Lorg/jivesoftware/smack/packet/PacketExtension;


# instance fields
.field private mElementName:Ljava/lang/String;

.field private mExtension:Ljava/lang/String;

.field private mNamespace:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gsf/gtalkservice/extensions/RawXmlExtension;->mElementName:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/gsf/gtalkservice/extensions/RawXmlExtension;->mNamespace:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getElementName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/extensions/RawXmlExtension;->mElementName:Ljava/lang/String;

    return-object v0
.end method

.method public getNamespace()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/extensions/RawXmlExtension;->mNamespace:Ljava/lang/String;

    return-object v0
.end method

.method public setExtension(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gsf/gtalkservice/extensions/RawXmlExtension;->mExtension:Ljava/lang/String;

    return-void
.end method

.method public toProtoBuf()Lcom/google/common/io/protocol/ProtoBuf;
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/extensions/RawXmlExtension;->toXML()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/android/gsf/gtalkservice/proto/GtalkCoreMessageTypes;->EXTENSION:Lcom/google/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v2}, Lcom/google/common/io/protocol/ProtoBuf;-><init>(Lcom/google/common/io/protocol/ProtoBufType;)V

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/google/common/io/protocol/ProtoBuf;->setInt(II)V

    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public toXML()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/extensions/RawXmlExtension;->mExtension:Ljava/lang/String;

    return-object v0
.end method
