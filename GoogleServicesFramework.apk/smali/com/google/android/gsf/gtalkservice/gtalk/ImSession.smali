.class public abstract Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;
.super Ljava/lang/Object;
.source "ImSession.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gsf/gtalkservice/gtalk/ImSession$2;
    }
.end annotation


# instance fields
.field private mConnectionListenerCallable:Lcom/google/android/gsf/gtalkservice/gtalk/RemoteListenerCallback$Callable;

.field private mConnectionListenerCallback:Lcom/google/android/gsf/gtalkservice/gtalk/RemoteListenerCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gsf/gtalkservice/gtalk/RemoteListenerCallback",
            "<",
            "Lcom/google/android/gtalkservice/IConnectionStateListener;",
            ">;"
        }
    .end annotation
.end field

.field private mConnectionListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gtalkservice/IConnectionStateListener;",
            ">;"
        }
    .end annotation
.end field

.field protected mHostConnection:Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

.field private mHostConnectionContext:Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

.field private mPresenceLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

.field private mPresenceReadLock:Ljava/util/concurrent/locks/Lock;

.field private mPresenceWriteLock:Ljava/util/concurrent/locks/Lock;

.field protected mSharedPresence:Lcom/google/android/gtalkservice/Presence;


# direct methods
.method public constructor <init>(Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;)V
    .locals 1
    .param p1    # Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/gsf/gtalkservice/gtalk/RemoteListenerCallback;

    invoke-direct {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/RemoteListenerCallback;-><init>()V

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mConnectionListenerCallback:Lcom/google/android/gsf/gtalkservice/gtalk/RemoteListenerCallback;

    new-instance v0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession$1;

    invoke-direct {v0, p0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession$1;-><init>(Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;)V

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mConnectionListenerCallable:Lcom/google/android/gsf/gtalkservice/gtalk/RemoteListenerCallback$Callable;

    new-instance v0, Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;-><init>()V

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mPresenceLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mPresenceLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mPresenceReadLock:Ljava/util/concurrent/locks/Lock;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mPresenceLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mPresenceWriteLock:Ljava/util/concurrent/locks/Lock;

    iput-object p1, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mHostConnectionContext:Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    invoke-virtual {p1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getGTalkConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mHostConnection:Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mConnectionListeners:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mHostConnection:Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getConnection()Lorg/jivesoftware/smack/XMPPConnection;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->initConnection(Lorg/jivesoftware/smack/XMPPConnection;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;Lcom/google/android/gtalkservice/IConnectionStateListener;)V
    .locals 0
    .param p0    # Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;
    .param p1    # Lcom/google/android/gtalkservice/IConnectionStateListener;

    invoke-direct {p0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->notifyListener(Lcom/google/android/gtalkservice/IConnectionStateListener;)V

    return-void
.end method

.method private addPresenceExtensions(Lorg/jivesoftware/smack/packet/Presence;Lcom/google/android/gtalkservice/Presence;)V
    .locals 6
    .param p1    # Lorg/jivesoftware/smack/packet/Presence;
    .param p2    # Lcom/google/android/gtalkservice/Presence;

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getShowMobileIndicator()Z

    move-result v4

    if-eqz v4, :cond_4

    const-string v2, "http://www.android.com/gtalk/client/caps"

    :goto_0
    new-instance v0, Lorg/jivesoftware/smack/packet/PresenceCapability;

    invoke-direct {v0, v2}, Lorg/jivesoftware/smack/packet/PresenceCapability;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/google/android/gtalkservice/Presence;->getCapabilities()I

    move-result v1

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x14

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    and-int/lit8 v4, v1, 0x8

    if-eqz v4, :cond_0

    const-string v4, "pmuc-v1"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    and-int/lit8 v4, v1, 0x1

    if-eqz v4, :cond_1

    const-string v4, "voice-v1"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    and-int/lit8 v4, v1, 0x2

    if-eqz v4, :cond_2

    const-string v4, "video-v1"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_3

    const-string v4, "camera-v1"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lorg/jivesoftware/smack/packet/PresenceCapability;->setExtension(Ljava/lang/String;)V

    const-string v4, "1.1"

    invoke-virtual {v0, v4}, Lorg/jivesoftware/smack/packet/PresenceCapability;->setVersion(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lorg/jivesoftware/smack/packet/Presence;->addExtension(Lorg/jivesoftware/smack/packet/PacketExtension;)V

    return-void

    :cond_4
    const-string v2, "http://www.android.com/gtalk/client/caps2"

    goto :goto_0
.end method

.method private createChatSessionWrapper(Lcom/google/android/gsf/gtalkservice/gtalk/ChatSession;)Lcom/google/android/gtalkservice/IChatSession;
    .locals 1
    .param p1    # Lcom/google/android/gsf/gtalkservice/gtalk/ChatSession;

    new-instance v0, Lcom/google/android/gsf/gtalkservice/service/ChatSessionWrapper;

    invoke-direct {v0, p1}, Lcom/google/android/gsf/gtalkservice/service/ChatSessionWrapper;-><init>(Lcom/google/android/gsf/gtalkservice/gtalk/ChatSession;)V

    return-object v0
.end method

.method public static createImSession(Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;)Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;
    .locals 1
    .param p0    # Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    new-instance v0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;

    invoke-direct {v0, p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;-><init>(Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;)V

    return-object v0
.end method

.method private static log(Ljava/lang/String;)V
    .locals 3
    .param p0    # Ljava/lang/String;

    const-string v0, "GTalkService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ImSession] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gsf/gtalkservice/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private notifyListener(Lcom/google/android/gtalkservice/IConnectionStateListener;)V
    .locals 7
    .param p1    # Lcom/google/android/gtalkservice/IConnectionStateListener;

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->getConnectionState()Lcom/google/android/gtalkservice/ConnectionState;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->getConnectionError()Lcom/google/android/gtalkservice/ConnectionError;

    move-result-object v2

    sget-boolean v0, Lcom/google/android/gsf/gtalkservice/LogTag;->sVerbose:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "notifyConnectionListener: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", state="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", error="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->log(Ljava/lang/String;)V

    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->getAccountId()J

    move-result-wide v3

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->getUsername()Ljava/lang/String;

    move-result-object v5

    move-object v0, p1

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gtalkservice/IConnectionStateListener;->connectionStateChanged(Lcom/google/android/gtalkservice/ConnectionState;Lcom/google/android/gtalkservice/ConnectionError;JLjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v6

    const-string v0, "GTalkService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "notifyListener caught "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", removing listener "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/gsf/gtalkservice/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mConnectionListeners:Ljava/util/ArrayList;

    monitor-enter v3

    :try_start_1
    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mConnectionListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static sendPresence(Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;Lorg/jivesoftware/smack/packet/Packet;)V
    .locals 4
    .param p0    # Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;
    .param p1    # Lorg/jivesoftware/smack/packet/Packet;

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->sendPacket(Lorg/jivesoftware/smack/packet/Packet;Z)Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "GTalkService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendPresence caught "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gsf/gtalkservice/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private translateGTalkPresence(Lcom/google/android/gtalkservice/Presence;)I
    .locals 4
    .param p1    # Lcom/google/android/gtalkservice/Presence;

    invoke-virtual {p1}, Lcom/google/android/gtalkservice/Presence;->isAvailable()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gtalkservice/Presence;->isInvisible()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gtalkservice/Presence;->getShow()Lcom/google/android/gtalkservice/Presence$Show;

    move-result-object v1

    sget-object v2, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession$2;->$SwitchMap$com$google$android$gtalkservice$Presence$Show:[I

    invoke-virtual {v1}, Lcom/google/android/gtalkservice/Presence$Show;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    const/4 v0, 0x5

    goto :goto_0

    :pswitch_0
    const/4 v0, 0x4

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public addConnectionStateListener(Lcom/google/android/gtalkservice/IConnectionStateListener;)V
    .locals 5
    .param p1    # Lcom/google/android/gtalkservice/IConnectionStateListener;

    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mConnectionListeners:Ljava/util/ArrayList;

    monitor-enter v3

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mConnectionListeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gtalkservice/IConnectionStateListener;

    invoke-interface {v1}, Lcom/google/android/gtalkservice/IConnectionStateListener;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/android/gtalkservice/IConnectionStateListener;->asBinder()Landroid/os/IBinder;

    move-result-object v4

    if-ne v2, v4, :cond_0

    monitor-exit v3

    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mConnectionListeners:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->notifyListener(Lcom/google/android/gtalkservice/IConnectionStateListener;)V

    goto :goto_0

    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public abstract addContact(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
.end method

.method public abstract addGroupChatInvitationListener(Lcom/google/android/gtalkservice/IGroupChatInvitationListener;)V
.end method

.method public addRemoteChatListener(Lcom/google/android/gtalkservice/IChatListener;)V
    .locals 2
    .param p1    # Lcom/google/android/gtalkservice/IChatListener;

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mHostConnection:Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getSessionContext()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getChatManager()Lcom/google/android/gsf/gtalkservice/gtalk/ChatMgr;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/ChatMgr;->addRemoteChatListener(Lcom/google/android/gtalkservice/IChatListener;)V

    return-void
.end method

.method public addRemoteJingleInfoStanzaListener(Lcom/google/android/gtalkservice/IJingleInfoStanzaListener;)V
    .locals 2
    .param p1    # Lcom/google/android/gtalkservice/IJingleInfoStanzaListener;

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mHostConnection:Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getSessionContext()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getVideoChatSessionManager()Lcom/google/android/gsf/gtalkservice/gtalk/VideoChatSessionManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/VideoChatSessionManager;->addRemoteJingleInfoListener(Lcom/google/android/gtalkservice/IJingleInfoStanzaListener;)V

    return-void
.end method

.method public addRemoteRosterListener(Lcom/google/android/gtalkservice/IRosterListener;)V
    .locals 2
    .param p1    # Lcom/google/android/gtalkservice/IRosterListener;

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mHostConnection:Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getSessionContext()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getRosterHandler()Lcom/google/android/gsf/gtalkservice/gtalk/RosterListenerImpl;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/RosterListenerImpl;->addRemoteRosterListener(Lcom/google/android/gtalkservice/IRosterListener;)V

    return-void
.end method

.method public addRemoteSessionStanzaListener(Lcom/google/android/gtalkservice/ISessionStanzaListener;)V
    .locals 2
    .param p1    # Lcom/google/android/gtalkservice/ISessionStanzaListener;

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mHostConnection:Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getSessionContext()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getVideoChatSessionManager()Lcom/google/android/gsf/gtalkservice/gtalk/VideoChatSessionManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/VideoChatSessionManager;->addRemoteSessionListener(Lcom/google/android/gtalkservice/ISessionStanzaListener;)V

    return-void
.end method

.method public approveSubscriptionRequest(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getSessionContext()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getSubscriptionManager()Lcom/google/android/gsf/gtalkservice/gtalk/SubscriptionManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->getAccountId()J

    move-result-wide v1

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gsf/gtalkservice/gtalk/SubscriptionManager;->acceptRequestForUser(JLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method public abstract blockContact(Ljava/lang/String;)V
.end method

.method public abstract clearContactFlags(Ljava/lang/String;)V
.end method

.method public abstract closeAllChatSessions()V
.end method

.method public createChatSession(Ljava/lang/String;)Lcom/google/android/gtalkservice/IChatSession;
    .locals 8
    .param p1    # Ljava/lang/String;

    const/4 v5, 0x1

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mHostConnection:Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getSessionContext()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getChatManager()Lcom/google/android/gsf/gtalkservice/gtalk/ChatMgr;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->getUsername()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->getAccountId()J

    move-result-wide v3

    move-object v2, p1

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gsf/gtalkservice/gtalk/ChatMgr;->createChatSession(Ljava/lang/String;Ljava/lang/String;JZZ)Lcom/google/android/gsf/gtalkservice/gtalk/ChatSession;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->createChatSessionWrapper(Lcom/google/android/gsf/gtalkservice/gtalk/ChatSession;)Lcom/google/android/gtalkservice/IChatSession;

    move-result-object v1

    return-object v1
.end method

.method public abstract createGroupChatSession(Ljava/lang/String;[Ljava/lang/String;)V
.end method

.method public abstract declineGroupChatInvitation(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public declineSubscriptionRequest(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getSessionContext()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getSubscriptionManager()Lcom/google/android/gsf/gtalkservice/gtalk/SubscriptionManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->getAccountId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/SubscriptionManager;->declineRequestForUser(JLjava/lang/String;)V

    return-void
.end method

.method public abstract editContact(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
.end method

.method public abstract getAccountId()J
.end method

.method public getChatSession(Ljava/lang/String;)Lcom/google/android/gtalkservice/IChatSession;
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mHostConnection:Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getSessionContext()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getChatManager()Lcom/google/android/gsf/gtalkservice/gtalk/ChatMgr;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/ChatMgr;->getChatSession(Ljava/lang/String;)Lcom/google/android/gsf/gtalkservice/gtalk/ChatSession;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_0
    invoke-direct {p0, v1}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->createChatSessionWrapper(Lcom/google/android/gsf/gtalkservice/gtalk/ChatSession;)Lcom/google/android/gtalkservice/IChatSession;

    move-result-object v2

    goto :goto_0
.end method

.method public getConnectionError()Lcom/google/android/gtalkservice/ConnectionError;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mHostConnection:Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getConnectionError()Lcom/google/android/gtalkservice/ConnectionError;

    move-result-object v0

    return-object v0
.end method

.method public getConnectionState()Lcom/google/android/gtalkservice/ConnectionState;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mHostConnection:Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getConnectionState()Lcom/google/android/gtalkservice/ConnectionState;

    move-result-object v0

    return-object v0
.end method

.method public getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mHostConnection:Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    return-object v0
.end method

.method protected getHostConnectionContext()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mHostConnectionContext:Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    return-object v0
.end method

.method public getIntendedPresence()Lcom/google/android/gtalkservice/Presence;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mPresenceReadLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getPresence()Lcom/google/android/gtalkservice/Presence;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mPresenceReadLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-object v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mPresenceReadLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public getPresence()Lcom/google/android/gtalkservice/Presence;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->getConnectionState()Lcom/google/android/gtalkservice/ConnectionState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gtalkservice/ConnectionState;->isOnline()Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/google/android/gtalkservice/Presence;->OFFLINE:Lcom/google/android/gtalkservice/Presence;

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->getIntendedPresence()Lcom/google/android/gtalkservice/Presence;

    move-result-object v1

    goto :goto_0
.end method

.method public getSharedPresence()Lcom/google/android/gtalkservice/Presence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mSharedPresence:Lcom/google/android/gtalkservice/Presence;

    return-object v0
.end method

.method public getUsername()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mHostConnectionContext:Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getGTalkConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getUsername()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public goOffRecordInRoom(Ljava/lang/String;Z)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mHostConnection:Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getSessionContext()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getOtrManager()Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;->goOffRecordInRoom(Ljava/lang/String;Z)V

    return-void
.end method

.method public goOffRecordWithContact(Ljava/util/ArrayList;Z)V
    .locals 2
    .param p2    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mHostConnection:Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getSessionContext()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getOtrManager()Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;->goOffRecordWithContact(Ljava/util/ArrayList;Z)V

    return-void
.end method

.method public abstract hideContact(Ljava/lang/String;)V
.end method

.method public abstract initConnection(Lorg/jivesoftware/smack/XMPPConnection;)V
.end method

.method public abstract inviteContactsToGroupchat(Ljava/lang/String;[Ljava/lang/String;)V
.end method

.method public isLoggedIn()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->getIntendedPresence()Lcom/google/android/gtalkservice/Presence;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gtalkservice/Presence;->isAvailable()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isOffRecordWithContact(Ljava/lang/String;)Z
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mHostConnectionContext:Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getService()Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getAccountId()J

    move-result-wide v1

    invoke-static {v0, v1, v2, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/DatabaseHelper;->isOffTheRecordWithContact(Landroid/content/ContentResolver;JLjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public abstract joinGroupChatSession(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public loadVCard()V
    .locals 5

    const-string v3, "GTalkService"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/gsf/gtalkservice/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "load user\'s vCard from server"

    invoke-static {v3}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->log(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->getUsername()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getSessionContext()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getVCardManager()Lcom/google/android/gsf/gtalkservice/gtalk/VCardMgr;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/gsf/gtalkservice/gtalk/VCardMgr;->getStoredAvatarHashForContact(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lcom/google/android/gsf/gtalkservice/gtalk/VCardMgr;->loadAvatarForUser(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Lcom/google/android/gsf/gtalkservice/gtalk/VCardMgr;->addToNotificationMap(Ljava/lang/String;)V

    return-void
.end method

.method public abstract login(Ljava/lang/String;Z)V
.end method

.method public abstract logout()V
.end method

.method protected notifyConnectionListeners()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mConnectionListenerCallback:Lcom/google/android/gsf/gtalkservice/gtalk/RemoteListenerCallback;

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mConnectionListeners:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mConnectionListenerCallable:Lcom/google/android/gsf/gtalkservice/gtalk/RemoteListenerCallback$Callable;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gsf/gtalkservice/gtalk/RemoteListenerCallback;->call(Ljava/util/ArrayList;Lcom/google/android/gsf/gtalkservice/gtalk/RemoteListenerCallback$Callable;)Z

    return-void
.end method

.method protected onLoggedOut()V
    .locals 3

    const-string v0, "GTalkService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gsf/gtalkservice/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "onLoggedOut"

    invoke-static {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->log(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->getHostConnectionContext()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getChatManager()Lcom/google/android/gsf/gtalkservice/gtalk/ChatMgr;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->getAccountId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gsf/gtalkservice/gtalk/ChatMgr;->onLogout(J)V

    return-void
.end method

.method public abstract pinContact(Ljava/lang/String;)V
.end method

.method public abstract pruneOldChatSessions(JJJZ)V
.end method

.method public queryJingleInfo()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mHostConnection:Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getSessionContext()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getVideoChatSessionManager()Lcom/google/android/gsf/gtalkservice/gtalk/VideoChatSessionManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/VideoChatSessionManager;->queryJingleInfo()V

    return-void
.end method

.method public removeConnectionStateListener(Lcom/google/android/gtalkservice/IConnectionStateListener;)V
    .locals 5
    .param p1    # Lcom/google/android/gtalkservice/IConnectionStateListener;

    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mConnectionListeners:Ljava/util/ArrayList;

    monitor-enter v3

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mConnectionListeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gtalkservice/IConnectionStateListener;

    invoke-interface {v1}, Lcom/google/android/gtalkservice/IConnectionStateListener;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/android/gtalkservice/IConnectionStateListener;->asBinder()Landroid/os/IBinder;

    move-result-object v4

    if-ne v2, v4, :cond_0

    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mConnectionListeners:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    monitor-exit v3

    :goto_0
    return-void

    :cond_1
    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public abstract removeContact(Ljava/lang/String;)V
.end method

.method public abstract removeGroupChatInvitationListener(Lcom/google/android/gtalkservice/IGroupChatInvitationListener;)V
.end method

.method public removeRemoteChatListener(Lcom/google/android/gtalkservice/IChatListener;)V
    .locals 2
    .param p1    # Lcom/google/android/gtalkservice/IChatListener;

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mHostConnection:Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getSessionContext()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getChatManager()Lcom/google/android/gsf/gtalkservice/gtalk/ChatMgr;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/ChatMgr;->removeRemoteChatListener(Lcom/google/android/gtalkservice/IChatListener;)V

    return-void
.end method

.method public removeRemoteJingleInfoStanzaListener(Lcom/google/android/gtalkservice/IJingleInfoStanzaListener;)V
    .locals 2
    .param p1    # Lcom/google/android/gtalkservice/IJingleInfoStanzaListener;

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mHostConnection:Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getSessionContext()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getVideoChatSessionManager()Lcom/google/android/gsf/gtalkservice/gtalk/VideoChatSessionManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/VideoChatSessionManager;->removeRemoteJingleInfoListener(Lcom/google/android/gtalkservice/IJingleInfoStanzaListener;)V

    return-void
.end method

.method public removeRemoteRosterListener(Lcom/google/android/gtalkservice/IRosterListener;)V
    .locals 2
    .param p1    # Lcom/google/android/gtalkservice/IRosterListener;

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mHostConnection:Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getSessionContext()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getRosterHandler()Lcom/google/android/gsf/gtalkservice/gtalk/RosterListenerImpl;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/RosterListenerImpl;->removeRemoteRosterListener(Lcom/google/android/gtalkservice/IRosterListener;)V

    return-void
.end method

.method public removeRemoteSessionStanzaListener(Lcom/google/android/gtalkservice/ISessionStanzaListener;)V
    .locals 2
    .param p1    # Lcom/google/android/gtalkservice/ISessionStanzaListener;

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mHostConnection:Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getSessionContext()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getVideoChatSessionManager()Lcom/google/android/gsf/gtalkservice/gtalk/VideoChatSessionManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/VideoChatSessionManager;->removeRemoteSessionListener(Lcom/google/android/gtalkservice/ISessionStanzaListener;)V

    return-void
.end method

.method public requestBatchedBuddyPresence()V
    .locals 6

    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mHostConnection:Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    invoke-virtual {v3}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getConnection()Lorg/jivesoftware/smack/XMPPConnection;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lorg/jivesoftware/smack/XMPPConnection;->isConnected()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    const-string v3, "requestBatchedBuddyPresence: not connected to server, bail"

    invoke-static {v3}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->log(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    new-instance v0, Lorg/jivesoftware/smack/packet/BatchPresence;

    sget-object v3, Lorg/jivesoftware/smack/packet/BatchPresence$Type;->GET:Lorg/jivesoftware/smack/packet/BatchPresence$Type;

    invoke-direct {v0, v3}, Lorg/jivesoftware/smack/packet/BatchPresence;-><init>(Lorg/jivesoftware/smack/packet/BatchPresence$Type;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->getAccountId()J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Lorg/jivesoftware/smack/packet/BatchPresence;->setAccountId(J)V

    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mHostConnection:Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    invoke-virtual {v3}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getUsername()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lorg/jivesoftware/smack/packet/BatchPresence;->setTo(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mHostConnectionContext:Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    invoke-virtual {v3}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getService()Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->onSentBatchPresenceRequest()V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->sendPacket(Lorg/jivesoftware/smack/packet/Packet;Z)Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    const-string v3, "GTalkService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sendPresence caught "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/gsf/gtalkservice/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public sendCallPerfStatsStanza(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mHostConnection:Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getSessionContext()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getVideoChatSessionManager()Lcom/google/android/gsf/gtalkservice/gtalk/VideoChatSessionManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/VideoChatSessionManager;->sendCallPerfStatsStanza(Ljava/lang/String;)V

    return-void
.end method

.method public sendPresence(Lcom/google/android/gtalkservice/Presence;)V
    .locals 2
    .param p1    # Lcom/google/android/gtalkservice/Presence;

    invoke-virtual {p0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->translatePresence(Lcom/google/android/gtalkservice/Presence;)Lorg/jivesoftware/smack/packet/Packet;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->sendPresence(Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;Lorg/jivesoftware/smack/packet/Packet;)V

    return-void
.end method

.method public sendPresenceCapability()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getPresence()Lcom/google/android/gtalkservice/Presence;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gtalkservice/Presence;->isAvailable()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->translateXmppPresence(Lcom/google/android/gtalkservice/Presence;)Lorg/jivesoftware/smack/packet/Packet;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendPresenceCapability: p="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lorg/jivesoftware/smack/packet/Packet;->toLogString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->log(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->sendPresence(Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;Lorg/jivesoftware/smack/packet/Packet;)V

    :cond_0
    return-void
.end method

.method public sendSessionStanza(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mHostConnection:Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getSessionContext()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getVideoChatSessionManager()Lcom/google/android/gsf/gtalkservice/gtalk/VideoChatSessionManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/VideoChatSessionManager;->sendSessionStanza(Ljava/lang/String;)V

    return-void
.end method

.method public setAndSendPresence(Lcom/google/android/gtalkservice/Presence;)V
    .locals 1
    .param p1    # Lcom/google/android/gtalkservice/Presence;

    invoke-virtual {p0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->setPresence(Lcom/google/android/gtalkservice/Presence;)Z

    invoke-virtual {p0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->sendPresence(Lcom/google/android/gtalkservice/Presence;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->getHostConnectionContext()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getRosterHandler()Lcom/google/android/gsf/gtalkservice/gtalk/RosterListenerImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/RosterListenerImpl;->notifySelfPresenceChanged()Z

    return-void
.end method

.method protected setLastLoginState(Z)V
    .locals 8
    .param p1    # Z

    const/4 v7, 0x0

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    if-eqz p1, :cond_1

    const/4 v2, 0x1

    :goto_0
    const-string v4, "last_login_state"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v4, Lcom/google/android/gsf/TalkContract$Account;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->getAccountId()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mHostConnectionContext:Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    invoke-virtual {v4}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-virtual {v4, v0, v3, v7, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    const-string v4, "GTalkService"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/gsf/gtalkservice/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setLastLoginState("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") for acct "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->getAccountId()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " updated "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " rows"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->log(Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setPresence(Lcom/google/android/gtalkservice/Presence;)Z
    .locals 6
    .param p1    # Lcom/google/android/gtalkservice/Presence;

    sget-boolean v3, Lcom/google/android/gsf/gtalkservice/LogTag;->sVerbose:Z

    if-eqz v3, :cond_2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setPresence for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->getUsername()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/gtalkservice/Presence;->printDetails()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->log(Ljava/lang/String;)V

    :cond_0
    :goto_0
    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mPresenceWriteLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getPresence()Lcom/google/android/gtalkservice/Presence;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/gtalkservice/Presence;->equals(Lcom/google/android/gtalkservice/Presence;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->setPresence(Lcom/google/android/gtalkservice/Presence;)V

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->asyncUpdateAccountStatus()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v2, 0x1

    :cond_1
    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mPresenceWriteLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return v2

    :cond_2
    const-string v3, "GTalkService"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/gsf/gtalkservice/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setPresence(acct="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mHostConnection:Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    invoke-virtual {v4}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getAccountId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/gtalkservice/Presence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->log(Ljava/lang/String;)V

    goto :goto_0

    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mPresenceWriteLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v3
.end method

.method public setSharedPresence(Lcom/google/android/gtalkservice/Presence;)V
    .locals 1
    .param p1    # Lcom/google/android/gtalkservice/Presence;

    new-instance v0, Lcom/google/android/gtalkservice/Presence;

    invoke-direct {v0, p1}, Lcom/google/android/gtalkservice/Presence;-><init>(Lcom/google/android/gtalkservice/Presence;)V

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mSharedPresence:Lcom/google/android/gtalkservice/Presence;

    return-void
.end method

.method protected abstract translatePresence(Lcom/google/android/gtalkservice/Presence;)Lorg/jivesoftware/smack/packet/Packet;
.end method

.method protected translateXmppPresence(Lcom/google/android/gtalkservice/Presence;)Lorg/jivesoftware/smack/packet/Packet;
    .locals 7
    .param p1    # Lcom/google/android/gtalkservice/Presence;

    const/16 v2, 0x18

    if-nez p1, :cond_0

    const/4 v1, 0x0

    sget-object v4, Lcom/google/android/gtalkservice/Presence$Show;->NONE:Lcom/google/android/gtalkservice/Presence$Show;

    const/4 v0, 0x0

    :goto_0
    new-instance v3, Lorg/jivesoftware/smack/packet/Presence;

    if-eqz v1, :cond_1

    sget-object v5, Lorg/jivesoftware/smack/packet/Presence$Type;->AVAILABLE:Lorg/jivesoftware/smack/packet/Presence$Type;

    :goto_1
    invoke-direct {v3, v5}, Lorg/jivesoftware/smack/packet/Presence;-><init>(Lorg/jivesoftware/smack/packet/Presence$Type;)V

    invoke-virtual {v3, v0}, Lorg/jivesoftware/smack/packet/Presence;->setStatus(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Lorg/jivesoftware/smack/packet/Presence;->setPriority(I)V

    sget-object v5, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession$2;->$SwitchMap$com$google$android$gtalkservice$Presence$Show:[I

    invoke-virtual {v4}, Lcom/google/android/gtalkservice/Presence$Show;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    :goto_2
    invoke-direct {p0, v3, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->addPresenceExtensions(Lorg/jivesoftware/smack/packet/Presence;Lcom/google/android/gtalkservice/Presence;)V

    return-object v3

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gtalkservice/Presence;->isAvailable()Z

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/gtalkservice/Presence;->getShow()Lcom/google/android/gtalkservice/Presence$Show;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/gtalkservice/Presence;->getStatus()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    sget-object v5, Lorg/jivesoftware/smack/packet/Presence$Type;->UNAVAILABLE:Lorg/jivesoftware/smack/packet/Presence$Type;

    goto :goto_1

    :pswitch_0
    sget-object v5, Lorg/jivesoftware/smack/packet/Presence$Mode;->AVAILABLE:Lorg/jivesoftware/smack/packet/Presence$Mode;

    invoke-virtual {v3, v5}, Lorg/jivesoftware/smack/packet/Presence;->setMode(Lorg/jivesoftware/smack/packet/Presence$Mode;)V

    goto :goto_2

    :pswitch_1
    sget-object v5, Lorg/jivesoftware/smack/packet/Presence$Mode;->AWAY:Lorg/jivesoftware/smack/packet/Presence$Mode;

    invoke-virtual {v3, v5}, Lorg/jivesoftware/smack/packet/Presence;->setMode(Lorg/jivesoftware/smack/packet/Presence$Mode;)V

    goto :goto_2

    :pswitch_2
    sget-object v5, Lorg/jivesoftware/smack/packet/Presence$Mode;->EXTENDED_AWAY:Lorg/jivesoftware/smack/packet/Presence$Mode;

    invoke-virtual {v3, v5}, Lorg/jivesoftware/smack/packet/Presence;->setMode(Lorg/jivesoftware/smack/packet/Presence$Mode;)V

    goto :goto_2

    :pswitch_3
    sget-object v5, Lorg/jivesoftware/smack/packet/Presence$Mode;->DO_NOT_DISTURB:Lorg/jivesoftware/smack/packet/Presence$Mode;

    invoke-virtual {v3, v5}, Lorg/jivesoftware/smack/packet/Presence;->setMode(Lorg/jivesoftware/smack/packet/Presence$Mode;)V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public updateAccountStatus()V
    .locals 13

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->getConnectionState()Lcom/google/android/gtalkservice/ConnectionState;

    move-result-object v10

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->getPresence()Lcom/google/android/gtalkservice/Presence;

    move-result-object v9

    const/4 v4, 0x0

    const/4 v7, 0x0

    invoke-virtual {v10}, Lcom/google/android/gtalkservice/ConnectionState;->isOnline()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-direct {p0, v9}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->translateGTalkPresence(Lcom/google/android/gtalkservice/Presence;)I

    move-result v4

    const/4 v7, 0x3

    :cond_0
    iget-object v11, p0, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->mHostConnectionContext:Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    invoke-virtual {v11}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getService()Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getAccountId()J

    move-result-wide v1

    invoke-virtual {v8}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getUsername()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9}, Lcom/google/android/gtalkservice/Presence;->getStatus()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v9}, Lcom/google/android/gtalkservice/Presence;->getCapabilities()I

    move-result v6

    invoke-static {v0, v1, v2, v4, v7}, Lcom/google/android/gsf/gtalkservice/gtalk/DatabaseHelper;->setAccountStatus(Landroid/content/ContentResolver;JII)V

    const-string v11, "GTalkService"

    const/4 v12, 0x3

    invoke-static {v11, v12}, Lcom/google/android/gsf/gtalkservice/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v11

    if-eqz v11, :cond_1

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "updateAccountStatus: update contact presence for "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", account="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", presence="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", status="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", connStatus="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", state.isOnline()="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v10}, Lcom/google/android/gtalkservice/ConnectionState;->isOnline()Z

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->log(Ljava/lang/String;)V

    :cond_1
    if-nez v4, :cond_2

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gsf/gtalkservice/gtalk/ContactPresence;->removeContactPresenceFor(Landroid/content/ContentResolver;JLjava/lang/String;)V

    :goto_0
    return-void

    :cond_2
    invoke-static/range {v0 .. v6}, Lcom/google/android/gsf/gtalkservice/gtalk/ContactPresence;->updateContactPresence(Landroid/content/ContentResolver;JLjava/lang/String;ILjava/lang/String;I)V

    goto :goto_0
.end method

.method public uploadAvatar(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1    # Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getSessionContext()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getVCardManager()Lcom/google/android/gsf/gtalkservice/gtalk/VCardMgr;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getJid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Lcom/google/android/gsf/gtalkservice/gtalk/VCardMgr;->uploadAvatar(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    return-void
.end method

.method public uploadAvatarFromDb()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getSessionContext()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getVCardManager()Lcom/google/android/gsf/gtalkservice/gtalk/VCardMgr;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getUsername()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getJid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gsf/gtalkservice/gtalk/VCardMgr;->uploadAvatarFromDb(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
