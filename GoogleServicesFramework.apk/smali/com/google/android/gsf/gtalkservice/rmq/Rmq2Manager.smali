.class public Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;
.super Ljava/lang/Object;
.source "Rmq2Manager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager$D2sInfo;,
        Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager$MessageQueueCallbackEntry;,
        Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager$MessageQueueCallbacks;
    }
.end annotation


# static fields
.field private static sIncludeStreamId:Z


# instance fields
.field private final mAckedS2dMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mAckingInterval:I

.field private final mD2sIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager$D2sInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mLastStreamIdAcked:I

.field private mMessageQueue:Lcom/google/android/gsf/gtalkservice/rmq/ReliableMessageQueue;

.field private mMessageQueueCallback:Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager$MessageQueueCallbacks;

.field private mOkToResendAccounts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mOkToResendPackets:Z

.field private mPacketSender:Lcom/google/android/gsf/gtalkservice/PacketSender;

.field private mProtobufConfig:Lcom/google/android/gsf/gtalkservice/proto/MobileProtoBufStreamConfiguration;

.field private mProtobufSerializer:Lcom/google/android/gsf/gtalkservice/proto/ProtoBufStreamSerializer;

.field private final mResendQueue:Ljava/util/LinkedList;

.field private mRmqAckLock:Ljava/lang/Object;

.field private mS2dIdStore:Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Server2DeviceIdStore;

.field private mStreamIdIn:I

.field private mStreamIdOut:I

.field private mUnackedS2dIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->sIncludeStreamId:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;)V
    .locals 2
    .param p1    # Landroid/content/ContentResolver;

    new-instance v0, Lcom/google/android/gsf/gtalkservice/rmq/PersistentMessageQueue;

    invoke-direct {v0, p1}, Lcom/google/android/gsf/gtalkservice/rmq/PersistentMessageQueue;-><init>(Landroid/content/ContentResolver;)V

    new-instance v1, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Server2DeviceIdStoreImpl;

    invoke-direct {v1, p1}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Server2DeviceIdStoreImpl;-><init>(Landroid/content/ContentResolver;)V

    invoke-direct {p0, v0, v1}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;-><init>(Lcom/google/android/gsf/gtalkservice/rmq/ReliableMessageQueue;Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Server2DeviceIdStore;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gsf/gtalkservice/rmq/ReliableMessageQueue;Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Server2DeviceIdStore;)V
    .locals 2
    .param p1    # Lcom/google/android/gsf/gtalkservice/rmq/ReliableMessageQueue;
    .param p2    # Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Server2DeviceIdStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/gsf/gtalkservice/proto/MobileProtoBufStreamConfiguration;

    invoke-direct {v0}, Lcom/google/android/gsf/gtalkservice/proto/MobileProtoBufStreamConfiguration;-><init>()V

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mProtobufConfig:Lcom/google/android/gsf/gtalkservice/proto/MobileProtoBufStreamConfiguration;

    new-instance v0, Lcom/google/android/gsf/gtalkservice/proto/ProtoBufStreamSerializerImpl;

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mProtobufConfig:Lcom/google/android/gsf/gtalkservice/proto/MobileProtoBufStreamConfiguration;

    invoke-direct {v0, v1}, Lcom/google/android/gsf/gtalkservice/proto/ProtoBufStreamSerializerImpl;-><init>(Lcom/google/android/gsf/gtalkservice/proto/ProtoBufStreamConfiguration;)V

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mProtobufSerializer:Lcom/google/android/gsf/gtalkservice/proto/ProtoBufStreamSerializer;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mD2sIds:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mUnackedS2dIds:Ljava/util/List;

    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mAckedS2dMap:Ljava/util/Map;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mRmqAckLock:Ljava/lang/Object;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mOkToResendAccounts:Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mMessageQueue:Lcom/google/android/gsf/gtalkservice/rmq/ReliableMessageQueue;

    iput-object p2, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mS2dIdStore:Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Server2DeviceIdStore;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mResendQueue:Ljava/util/LinkedList;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;)V
    .locals 0
    .param p0    # Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;

    invoke-direct {p0}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->resendPackets()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->log(Ljava/lang/String;)V

    return-void
.end method

.method private ackD2sMessagesInternal(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager$D2sInfo;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    sget-boolean v4, Lcom/google/android/gsf/gtalkservice/LogTag;->sDebugRmq:Z

    if-eqz v4, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ackD2sMessagesInternal: size="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->log(Ljava/lang/String;)V

    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v5, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mD2sIds:Ljava/util/ArrayList;

    monitor-enter v5

    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager$D2sInfo;

    iget-object v4, v0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager$D2sInfo;->d2sId:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mD2sIds:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    :cond_1
    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v4, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mMessageQueue:Lcom/google/android/gsf/gtalkservice/rmq/ReliableMessageQueue;

    invoke-interface {v4, v2}, Lcom/google/android/gsf/gtalkservice/rmq/ReliableMessageQueue;->removeMessagesByRmq2Ids(Ljava/util/List;)I

    return-void
.end method

.method public static getIncludeStreamIdFromGservices(Landroid/content/ContentResolver;)Z
    .locals 2
    .param p0    # Landroid/content/ContentResolver;

    const-string v0, "gtalk_rmq2_include_stream_id"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getIncludeStreamIdInProtobuf()Z
    .locals 1

    sget-boolean v0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->sIncludeStreamId:Z

    return v0
.end method

.method public static getRmqAckIntervalFromGservices(Landroid/content/ContentResolver;)I
    .locals 2
    .param p0    # Landroid/content/ContentResolver;

    const-string v0, "gtalk_rmq_ack_interval"

    const/16 v1, 0xa

    invoke-static {p0, v0, v1}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private internalProcessS2dMessage(Ljava/lang/String;I)V
    .locals 15
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const-string v12, "Rmq"

    const/4 v13, 0x2

    invoke-static {v12, v13}, Lcom/google/android/gsf/gtalkservice/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v12

    if-eqz v12, :cond_0

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "process S2d: s2dId="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", lastStreamId="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", mStreamIdIn="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v13, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mStreamIdIn:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {p0, v12}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->log(Ljava/lang/String;)V

    :cond_0
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_1

    iget-object v13, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mUnackedS2dIds:Ljava/util/List;

    monitor-enter v13

    :try_start_0
    iget-object v12, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mUnackedS2dIds:Ljava/util/List;

    move-object/from16 v0, p1

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    monitor-exit v13
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    const/4 v5, 0x0

    const/4 v10, 0x0

    const/4 v12, -0x1

    move/from16 v0, p2

    if-eq v0, v12, :cond_5

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v13, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mD2sIds:Ljava/util/ArrayList;

    monitor-enter v13

    :try_start_1
    iget-object v12, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mD2sIds:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager$D2sInfo;

    iget v12, v3, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager$D2sInfo;->streamId:I

    move/from16 v0, p2

    if-ge v0, v12, :cond_6

    :cond_2
    monitor-exit v13
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v12

    if-lez v12, :cond_3

    invoke-direct {p0, v2}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->ackD2sMessagesInternal(Ljava/util/List;)V

    :cond_3
    iget-object v13, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mAckedS2dMap:Ljava/util/Map;

    monitor-enter v13

    :try_start_2
    iget-object v12, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mAckedS2dMap:Ljava/util/Map;

    invoke-interface {v12}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v7

    move-object v11, v10

    move-object v6, v5

    :goto_1
    :try_start_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v4

    move/from16 v0, p2

    if-ge v0, v4, :cond_7

    :cond_4
    monitor-exit v13
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    move-object v10, v11

    move-object v5, v6

    :cond_5
    move-object/from16 v0, p1

    invoke-direct {p0, v0, v10, v5}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->updateS2dIds(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V

    return-void

    :catchall_0
    move-exception v12

    :try_start_4
    monitor-exit v13
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v12

    :cond_6
    :try_start_5
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :catchall_1
    move-exception v12

    monitor-exit v13
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v12

    :cond_7
    if-nez v6, :cond_9

    :try_start_6
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    :try_start_7
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    :goto_2
    :try_start_8
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-interface {v5, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v12, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mAckedS2dMap:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-interface {v12, v14}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_8

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-interface {v10, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :catchall_2
    move-exception v12

    :goto_4
    monitor-exit v13
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    throw v12

    :cond_8
    move-object v11, v10

    move-object v6, v5

    goto :goto_1

    :catchall_3
    move-exception v12

    move-object v10, v11

    move-object v5, v6

    goto :goto_4

    :catchall_4
    move-exception v12

    move-object v10, v11

    goto :goto_4

    :cond_9
    move-object v10, v11

    move-object v5, v6

    goto :goto_2
.end method

.method private log(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const-string v0, "Rmq"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Rmq2Mgr] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gsf/gtalkservice/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private makeRmqPacket(JLorg/jivesoftware/smack/packet/Packet;)Lcom/google/android/gsf/gtalkservice/rmq/RmqPacket;
    .locals 9
    .param p1    # J
    .param p3    # Lorg/jivesoftware/smack/packet/Packet;

    const/4 v8, 0x0

    :try_start_0
    invoke-virtual {p3}, Lorg/jivesoftware/smack/packet/Packet;->toProtoBuf()Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v7

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mProtobufConfig:Lcom/google/android/gsf/gtalkservice/proto/MobileProtoBufStreamConfiguration;

    invoke-virtual {v7}, Lcom/google/common/io/protocol/ProtoBuf;->getType()Lcom/google/common/io/protocol/ProtoBufType;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gsf/gtalkservice/proto/MobileProtoBufStreamConfiguration;->getTag(Lcom/google/common/io/protocol/ProtoBufType;)B

    move-result v4

    invoke-virtual {v7}, Lcom/google/common/io/protocol/ProtoBuf;->toByteArray()[B

    move-result-object v5

    new-instance v0, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacket;

    move-object v1, p3

    move-wide v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacket;-><init>(Lorg/jivesoftware/smack/packet/Packet;JB[B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v6

    const-string v1, "Rmq"

    const-string v2, "makeRmqPacket: caught IOException"

    invoke-static {v1, v2, v6}, Lcom/google/android/gsf/gtalkservice/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v8

    goto :goto_0
.end method

.method private processSelectiveAck(Lcom/google/android/gsf/gtalkservice/extensions/SelectiveAck;)V
    .locals 3
    .param p1    # Lcom/google/android/gsf/gtalkservice/extensions/SelectiveAck;

    invoke-virtual {p1}, Lcom/google/android/gsf/gtalkservice/extensions/SelectiveAck;->getIdList()Ljava/util/List;

    move-result-object v0

    const-string v1, "Rmq"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/gsf/gtalkservice/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "got SelectiveAck, size="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->log(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mMessageQueue:Lcom/google/android/gsf/gtalkservice/rmq/ReliableMessageQueue;

    invoke-interface {v1, v0}, Lcom/google/android/gsf/gtalkservice/rmq/ReliableMessageQueue;->removeMessagesByRmq2Ids(Ljava/util/List;)I

    invoke-direct {p0}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->resendRmqPackets()V

    return-void
.end method

.method private resendPackets()V
    .locals 9

    const/4 v0, 0x0

    iget-object v5, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mPacketSender:Lcom/google/android/gsf/gtalkservice/PacketSender;

    if-nez v5, :cond_5

    const-string v5, "Rmq"

    const-string v6, "[Rmq2Mgr] resendPackts: mPacketSender is null"

    invoke-static {v5, v6}, Lcom/google/android/gsf/gtalkservice/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    iget-object v5, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mResendQueue:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/jivesoftware/smack/packet/Packet;

    sget-boolean v5, Lcom/google/android/gsf/gtalkservice/LogTag;->sDebugRmq:Z

    if-eqz v5, :cond_2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "resend packet "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Lorg/jivesoftware/smack/packet/Packet;->getRmq2Id()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->log(Ljava/lang/String;)V

    :cond_2
    iget-object v5, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mPacketSender:Lcom/google/android/gsf/gtalkservice/PacketSender;

    invoke-interface {v5, v2}, Lcom/google/android/gsf/gtalkservice/PacketSender;->sendPacketOverMcsConnection(Lorg/jivesoftware/smack/packet/Packet;)Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v5, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mMessageQueueCallback:Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager$MessageQueueCallbacks;

    if-eqz v5, :cond_4

    invoke-virtual {v2}, Lorg/jivesoftware/smack/packet/Packet;->getPacketID()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4

    if-nez v0, :cond_3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move-object v0, v1

    :cond_3
    new-instance v5, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager$MessageQueueCallbackEntry;

    invoke-virtual {v2}, Lorg/jivesoftware/smack/packet/Packet;->getAccountId()J

    move-result-wide v7

    invoke-direct {v5, p0, v3, v7, v8}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager$MessageQueueCallbackEntry;-><init>(Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;Ljava/lang/String;J)V

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_5
    iget-object v6, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mResendQueue:Ljava/util/LinkedList;

    monitor-enter v6

    :try_start_1
    iget-object v5, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mResendQueue:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->size()I

    move-result v5

    if-nez v5, :cond_1

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_0

    iget-object v5, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mMessageQueueCallback:Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager$MessageQueueCallbacks;

    invoke-interface {v5, v0}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager$MessageQueueCallbacks;->onMessagesSent(Ljava/util/ArrayList;)V

    goto :goto_0

    :catchall_0
    move-exception v5

    :try_start_2
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v5
.end method

.method private resendRmqPackets()V
    .locals 3

    const/4 v0, 0x0

    monitor-enter p0

    const/4 v2, 0x1

    :try_start_0
    iput-boolean v2, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mOkToResendPackets:Z

    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mOkToResendAccounts:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mOkToResendAccounts:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mOkToResendAccounts:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v0, v1

    :cond_0
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v0, :cond_1

    invoke-direct {p0, v0}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->resendRmqPacketsForReadyAccount(Ljava/util/List;)V

    :cond_1
    return-void

    :catchall_0
    move-exception v2

    :goto_0
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v2

    :catchall_1
    move-exception v2

    move-object v0, v1

    goto :goto_0
.end method

.method private resendRmqPacketsForReadyAccount(Ljava/util/List;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    const/4 v8, 0x3

    const-string v7, "GTalkService"

    invoke-static {v7, v8}, Lcom/google/android/gsf/gtalkservice/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v7, "resendRmqPacketsForReadyAccount for accounts: "

    invoke-direct {v1, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->log(Ljava/lang/String;)V

    :cond_1
    iget-object v7, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mMessageQueue:Lcom/google/android/gsf/gtalkservice/rmq/ReliableMessageQueue;

    invoke-interface {v7, p1}, Lcom/google/android/gsf/gtalkservice/rmq/ReliableMessageQueue;->getRmq2Packets(Ljava/util/List;)Lcom/google/android/gsf/gtalkservice/rmq/RmqPacketList;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacketList;->size()I

    move-result v2

    if-nez v2, :cond_3

    :try_start_0
    const-string v7, "GTalkService"

    const/4 v8, 0x3

    invoke-static {v7, v8}, Lcom/google/android/gsf/gtalkservice/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_2

    const-string v7, "no rmq packets to resend"

    invoke-direct {p0, v7}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->log(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :cond_2
    invoke-virtual {v5}, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacketList;->close()V

    :goto_1
    return-void

    :cond_3
    :try_start_1
    iget-object v8, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mResendQueue:Ljava/util/LinkedList;

    monitor-enter v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v2, :cond_5

    :try_start_2
    invoke-virtual {v5, v3}, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacketList;->getPacketAt(I)Lorg/jivesoftware/smack/packet/Packet;

    move-result-object v6

    sget-boolean v7, Lcom/google/android/gsf/gtalkservice/LogTag;->sDebugRmq:Z

    if-eqz v7, :cond_4

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "resendRmqPackets: add packet "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v6}, Lorg/jivesoftware/smack/packet/Packet;->getRmqId()J

    move-result-wide v9

    invoke-virtual {v7, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, " to queue"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->log(Ljava/lang/String;)V

    :cond_4
    iget-object v7, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mResendQueue:Ljava/util/LinkedList;

    invoke-virtual {v7, v6}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_5
    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    new-instance v7, Ljava/lang/Thread;

    new-instance v8, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager$1;

    invoke-direct {v8, p0}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager$1;-><init>(Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;)V

    invoke-direct {v7, v8}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v7}, Ljava/lang/Thread;->start()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    invoke-virtual {v5}, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacketList;->close()V

    goto :goto_1

    :catchall_0
    move-exception v7

    :try_start_4
    monitor-exit v8
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v7
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catchall_1
    move-exception v7

    invoke-virtual {v5}, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacketList;->close()V

    throw v7
.end method

.method private sendPacket(Lorg/jivesoftware/smack/packet/Packet;)Z
    .locals 6
    .param p1    # Lorg/jivesoftware/smack/packet/Packet;

    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mPacketSender:Lcom/google/android/gsf/gtalkservice/PacketSender;

    invoke-interface {v2, p1}, Lcom/google/android/gsf/gtalkservice/PacketSender;->sendPacketOverMcsConnection(Lorg/jivesoftware/smack/packet/Packet;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mMessageQueueCallback:Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager$MessageQueueCallbacks;

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lorg/jivesoftware/smack/packet/Packet;->getPacketID()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mMessageQueueCallback:Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager$MessageQueueCallbacks;

    new-instance v3, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager$MessageQueueCallbackEntry;

    invoke-virtual {p1}, Lorg/jivesoftware/smack/packet/Packet;->getAccountId()J

    move-result-wide v4

    invoke-direct {v3, p0, v0, v4, v5}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager$MessageQueueCallbackEntry;-><init>(Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;Ljava/lang/String;J)V

    invoke-interface {v2, v3}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager$MessageQueueCallbacks;->onMessageSent(Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager$MessageQueueCallbackEntry;)V

    :cond_0
    return v1
.end method

.method private sendStreamAck()V
    .locals 6

    const/4 v2, 0x0

    iget-object v4, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mRmqAckLock:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->getAckingInterval()I

    move-result v1

    const-string v3, "Rmq"

    const/4 v5, 0x2

    invoke-static {v3, v5}, Lcom/google/android/gsf/gtalkservice/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sendStreamAck(?): mStreamIdIn="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v5, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mStreamIdIn:I

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", mLastStreamIdAcked="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v5, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mLastStreamIdAcked:I

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", ackInterval="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->log(Ljava/lang/String;)V

    :cond_0
    iget v3, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mStreamIdIn:I

    iget v5, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mLastStreamIdAcked:I

    sub-int/2addr v3, v5

    if-lt v3, v1, :cond_1

    const/4 v2, 0x1

    :cond_1
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_2

    new-instance v0, Lcom/google/android/gsf/gtalkservice/extensions/StreamAck;

    invoke-direct {v0}, Lcom/google/android/gsf/gtalkservice/extensions/StreamAck;-><init>()V

    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mPacketSender:Lcom/google/android/gsf/gtalkservice/PacketSender;

    if-nez v3, :cond_3

    const-string v3, "Rmq"

    const-string v4, "[Rmq2Mgr] sendStreamAck: mPacketSender is null"

    invoke-static {v3, v4}, Lcom/google/android/gsf/gtalkservice/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    :goto_0
    return-void

    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    :cond_3
    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mPacketSender:Lcom/google/android/gsf/gtalkservice/PacketSender;

    invoke-interface {v3, v0}, Lcom/google/android/gsf/gtalkservice/PacketSender;->sendPacketOverMcsConnection(Lorg/jivesoftware/smack/packet/Packet;)Z

    goto :goto_0
.end method

.method public static setIncludeStreamId(Z)V
    .locals 0
    .param p0    # Z

    sput-boolean p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->sIncludeStreamId:Z

    return-void
.end method

.method private setLastStreamIdAcked(I)Z
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mRmqAckLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mLastStreamIdAcked:I

    if-le p1, v0, :cond_0

    iput p1, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mLastStreamIdAcked:I

    const/4 v0, 0x1

    monitor-exit v1

    :goto_0
    return v0

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget v0, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mLastStreamIdAcked:I

    if-le v0, p1, :cond_1

    const-string v0, "Rmq"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "!!! mLastStreamIdAcked="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mLastStreamIdAcked:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is greater than lastStreamIdReceived="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gsf/gtalkservice/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private updateS2dIds(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    sget-boolean v3, Lcom/google/android/gsf/gtalkservice/LogTag;->sDebugRmq:Z

    if-eqz v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateS2dIds: add s2did="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->log(Ljava/lang/String;)V

    :cond_0
    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mS2dIdStore:Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Server2DeviceIdStore;

    invoke-interface {v3, p1}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Server2DeviceIdStore;->addS2dId(Ljava/lang/String;)V

    :cond_1
    if-eqz p2, :cond_5

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    :goto_0
    if-lez v1, :cond_3

    sget-boolean v3, Lcom/google/android/gsf/gtalkservice/LogTag;->sDebugRmq:Z

    if-eqz v3, :cond_2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateS2dIds: delete "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " s2d ids"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->log(Ljava/lang/String;)V

    :cond_2
    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mS2dIdStore:Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Server2DeviceIdStore;

    invoke-interface {v3, p2}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Server2DeviceIdStore;->deleteS2dIds(Ljava/util/List;)V

    :cond_3
    if-eqz p3, :cond_6

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_6

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    sget-boolean v3, Lcom/google/android/gsf/gtalkservice/LogTag;->sDebugRmq:Z

    if-eqz v3, :cond_4

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateS2dIds: remove streamId "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " from map"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->log(Ljava/lang/String;)V

    :cond_4
    iget-object v4, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mAckedS2dMap:Ljava/util/Map;

    monitor-enter v4

    :try_start_0
    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mAckedS2dMap:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v4

    goto :goto_1

    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    :cond_5
    const/4 v1, 0x0

    goto :goto_0

    :cond_6
    return-void
.end method


# virtual methods
.method public getAckingInterval()I
    .locals 1

    iget v0, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mAckingInterval:I

    return v0
.end method

.method public getLastStreamIdReceived()I
    .locals 2

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mRmqAckLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mStreamIdIn:I

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getStreamId()I
    .locals 2

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mRmqAckLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mStreamIdOut:I

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public initConnection(Lorg/jivesoftware/smack/XMPPConnection;)V
    .locals 4
    .param p1    # Lorg/jivesoftware/smack/XMPPConnection;

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->resetStreamIds()V

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mOkToResendAccounts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mOkToResendAccounts:Ljava/util/List;

    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, -0x1

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public processAndRetrieveInitialS2dIds()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mS2dIdStore:Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Server2DeviceIdStore;

    invoke-interface {v3}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Server2DeviceIdStore;->getS2dIds()Ljava/util/List;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mUnackedS2dIds:Ljava/util/List;

    monitor-enter v4

    :try_start_0
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mUnackedS2dIds:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    :cond_0
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v2
.end method

.method public processD2sMessage(Ljava/lang/String;)I
    .locals 5
    .param p1    # Ljava/lang/String;

    iget v2, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mStreamIdOut:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mStreamIdOut:I

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v1, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager$D2sInfo;

    iget v2, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mStreamIdOut:I

    invoke-direct {v1, v2, p1}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager$D2sInfo;-><init>(ILjava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mD2sIds:Ljava/util/ArrayList;

    monitor-enter v3

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mD2sIds:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    const-string v2, "Rmq"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/gsf/gtalkservice/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "processD2sMessage: mStreamIdOut="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mStreamIdOut:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->log(Ljava/lang/String;)V

    :cond_1
    const/4 v0, 0x0

    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mUnackedS2dIds:Ljava/util/List;

    monitor-enter v3

    :try_start_1
    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mUnackedS2dIds:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_2

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mUnackedS2dIds:Ljava/util/List;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mUnackedS2dIds:Ljava/util/List;

    :cond_2
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mAckedS2dMap:Ljava/util/Map;

    monitor-enter v3

    if-eqz v0, :cond_3

    :try_start_2
    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mAckedS2dMap:Ljava/util/Map;

    iget v4, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mStreamIdOut:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    iget v2, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mStreamIdOut:I

    return v2

    :catchall_0
    move-exception v2

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v2

    :catchall_1
    move-exception v2

    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v2

    :catchall_2
    move-exception v2

    :try_start_5
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v2
.end method

.method public processS2dMessage(Lorg/jivesoftware/smack/packet/Packet;)V
    .locals 2
    .param p1    # Lorg/jivesoftware/smack/packet/Packet;

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mRmqAckLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mStreamIdIn:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mStreamIdIn:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p1}, Lorg/jivesoftware/smack/packet/Packet;->getRmq2Id()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lorg/jivesoftware/smack/packet/Packet;->getLastStreamId()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->internalProcessS2dMessage(Ljava/lang/String;I)V

    invoke-direct {p0}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->sendStreamAck()V

    instance-of v0, p1, Lcom/google/android/gsf/gtalkservice/extensions/SelectiveAck;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/gsf/gtalkservice/extensions/SelectiveAck;

    invoke-direct {p0, p1}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->processSelectiveAck(Lcom/google/android/gsf/gtalkservice/extensions/SelectiveAck;)V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public resendPacketsForAccount(J)V
    .locals 4
    .param p1    # J

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "##### resendPacketsForAccount: acct="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mOkToResendPackets="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mOkToResendPackets:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->log(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mOkToResendPackets:Z

    if-eqz v2, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v0, v1

    :goto_0
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->resendRmqPacketsForReadyAccount(Ljava/util/List;)V

    :cond_0
    return-void

    :cond_1
    :try_start_3
    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mOkToResendAccounts:Ljava/util/List;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :catchall_0
    move-exception v2

    :goto_1
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v2

    :catchall_1
    move-exception v2

    move-object v0, v1

    goto :goto_1
.end method

.method public resetStreamIds()V
    .locals 2

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mStreamIdIn:I

    iput v0, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mStreamIdOut:I

    iput v0, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mLastStreamIdAcked:I

    const-string v0, "GTalkService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gsf/gtalkservice/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "### resetStreamIds: mStreamIdIn="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mStreamIdIn:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mLastStreamIdAcked="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mLastStreamIdAcked:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->log(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mD2sIds:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mD2sIds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mUnackedS2dIds:Ljava/util/List;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mUnackedS2dIds:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mAckedS2dMap:Ljava/util/Map;

    monitor-enter v1

    :try_start_2
    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mAckedS2dMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    return-void

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0
.end method

.method public send(Lorg/jivesoftware/smack/packet/Packet;)Z
    .locals 11
    .param p1    # Lorg/jivesoftware/smack/packet/Packet;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mMessageQueue:Lcom/google/android/gsf/gtalkservice/rmq/ReliableMessageQueue;

    invoke-interface {v8}, Lcom/google/android/gsf/gtalkservice/rmq/ReliableMessageQueue;->getNextRmqId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lorg/jivesoftware/smack/packet/Packet;->getPacketID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lorg/jivesoftware/smack/packet/Packet;->getAccountId()J

    move-result-wide v0

    sget-boolean v8, Lcom/google/android/gsf/gtalkservice/LogTag;->sDebugRmq:Z

    if-eqz v8, :cond_0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "send: rmq2Id="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->log(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1, v3}, Lorg/jivesoftware/smack/packet/Packet;->setRmq2Id(Ljava/lang/String;)V

    invoke-direct {p0, v4, v5, p1}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->makeRmqPacket(JLorg/jivesoftware/smack/packet/Packet;)Lcom/google/android/gsf/gtalkservice/rmq/RmqPacket;

    move-result-object v6

    if-nez v6, :cond_1

    const-string v8, "Rmq"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "send: cannot make RmqPacket from "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/google/android/gsf/gtalkservice/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return v7

    :cond_1
    invoke-virtual {v6, v2}, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacket;->setPacketID(Ljava/lang/String;)V

    invoke-virtual {v6, v0, v1}, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacket;->setAccountId(J)V

    iget-object v8, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mMessageQueue:Lcom/google/android/gsf/gtalkservice/rmq/ReliableMessageQueue;

    invoke-interface {v8, v4, v5, v6}, Lcom/google/android/gsf/gtalkservice/rmq/ReliableMessageQueue;->enqueueMessage(JLcom/google/android/gsf/gtalkservice/rmq/RmqPacket;)V

    iget-object v8, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mPacketSender:Lcom/google/android/gsf/gtalkservice/PacketSender;

    if-nez v8, :cond_2

    const-string v8, "Rmq"

    const-string v9, "[Rmq2Mgr] send: mPacketSender is null"

    invoke-static {v8, v9}, Lcom/google/android/gsf/gtalkservice/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v8, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mResendQueue:Ljava/util/LinkedList;

    monitor-enter v8

    :try_start_0
    iget-object v7, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mResendQueue:Ljava/util/LinkedList;

    invoke-virtual {v7}, Ljava/util/LinkedList;->size()I

    move-result v7

    if-nez v7, :cond_3

    invoke-direct {p0, v6}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->sendPacket(Lorg/jivesoftware/smack/packet/Packet;)Z

    move-result v7

    monitor-exit v8

    goto :goto_0

    :catchall_0
    move-exception v7

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v7

    :cond_3
    :try_start_1
    sget-boolean v7, Lcom/google/android/gsf/gtalkservice/LogTag;->sDebugRmq:Z

    if-eqz v7, :cond_4

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "send: add packet "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, " to resend queue"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->log(Ljava/lang/String;)V

    :cond_4
    iget-object v7, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mResendQueue:Ljava/util/LinkedList;

    invoke-virtual {v7, p1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    const/4 v7, 0x1

    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public serializePacketAsProtoBuf(Lcom/google/android/gsf/gtalkservice/proto/ProtoBufStreamSerializer;Lorg/jivesoftware/smack/packet/Packet;)[B
    .locals 11
    .param p1    # Lcom/google/android/gsf/gtalkservice/proto/ProtoBufStreamSerializer;
    .param p2    # Lorg/jivesoftware/smack/packet/Packet;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gsf/gtalkservice/proto/ProtoBufStreamException;,
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v8, 0x2

    const/4 v10, 0x1

    const/4 v4, 0x0

    invoke-virtual {p2}, Lorg/jivesoftware/smack/packet/Packet;->toProtoBuf()Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v3

    if-nez v3, :cond_2

    const-string v7, "Rmq"

    invoke-static {v7, v8}, Lcom/google/android/gsf/gtalkservice/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_0

    const-string v7, "serializePacketAsProtoBuf: protobuf==null"

    invoke-direct {p0, v7}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->log(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0, v4}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->processD2sMessage(Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-object v4

    :cond_2
    invoke-virtual {v3}, Lcom/google/common/io/protocol/ProtoBuf;->getType()Lcom/google/common/io/protocol/ProtoBufType;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->getLastStreamIdReceived()I

    move-result v1

    const/4 v2, 0x0

    :try_start_0
    sget-object v7, Lcom/google/android/gsf/gtalkservice/proto/GtalkCoreMessageTypes;->HEARTBEAT_PING:Lcom/google/common/io/protocol/ProtoBufType;

    if-ne v6, v7, :cond_5

    const/4 v7, 0x0

    invoke-virtual {p0, v7}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->processD2sMessage(Ljava/lang/String;)I

    move-result v5

    invoke-static {}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->getIncludeStreamIdInProtobuf()Z

    move-result v7

    if-eqz v7, :cond_3

    const/4 v7, 0x1

    invoke-virtual {v3, v7, v5}, Lcom/google/common/io/protocol/ProtoBuf;->setInt(II)V

    invoke-virtual {p2, v5}, Lorg/jivesoftware/smack/packet/Packet;->setStreamId(I)V

    :cond_3
    invoke-direct {p0, v1}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->setLastStreamIdAcked(I)Z

    move-result v7

    if-eqz v7, :cond_4

    const/4 v7, 0x2

    invoke-virtual {v3, v7, v1}, Lcom/google/common/io/protocol/ProtoBuf;->setInt(II)V

    invoke-virtual {p2, v1}, Lorg/jivesoftware/smack/packet/Packet;->setLastStreamId(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_4
    :goto_1
    invoke-interface {p1, v3}, Lcom/google/android/gsf/gtalkservice/proto/ProtoBufStreamSerializer;->serialize(Lcom/google/common/io/protocol/ProtoBuf;)[B

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {p2}, Lorg/jivesoftware/smack/packet/Packet;->getRmq2Id()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->getStreamId()I

    move-result v8

    invoke-virtual {p2}, Lorg/jivesoftware/smack/packet/Packet;->getLastStreamId()I

    move-result v9

    invoke-static {v6, v7, v8, v9, v10}, Lcom/google/android/gsf/gtalkservice/LogTag;->logRmq2(Lcom/google/common/io/protocol/ProtoBufType;Ljava/lang/String;IIZ)V

    goto :goto_0

    :cond_5
    :try_start_1
    sget-object v7, Lcom/google/android/gsf/gtalkservice/proto/GtalkCoreMessageTypes;->HEARTBEAT_ACK:Lcom/google/common/io/protocol/ProtoBufType;

    if-ne v6, v7, :cond_7

    const/4 v7, 0x0

    invoke-virtual {p0, v7}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->processD2sMessage(Ljava/lang/String;)I

    move-result v5

    invoke-static {}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->getIncludeStreamIdInProtobuf()Z

    move-result v7

    if-eqz v7, :cond_6

    const/4 v7, 0x1

    invoke-virtual {v3, v7, v5}, Lcom/google/common/io/protocol/ProtoBuf;->setInt(II)V

    invoke-virtual {p2, v5}, Lorg/jivesoftware/smack/packet/Packet;->setStreamId(I)V

    :cond_6
    invoke-direct {p0, v1}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->setLastStreamIdAcked(I)Z

    move-result v7

    if-eqz v7, :cond_4

    const/4 v7, 0x2

    invoke-virtual {v3, v7, v1}, Lcom/google/common/io/protocol/ProtoBuf;->setInt(II)V

    invoke-virtual {p2, v1}, Lorg/jivesoftware/smack/packet/Packet;->setLastStreamId(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v7, "GTalkService"

    const-string v8, "serializePacketAsProtoBuf: caught "

    invoke-static {v7, v8, v0}, Lcom/google/android/gsf/gtalkservice/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_7
    :try_start_2
    sget-object v7, Lcom/google/android/gsf/gtalkservice/proto/GtalkCoreMessageTypes;->IQ_STANZA:Lcom/google/common/io/protocol/ProtoBufType;

    if-ne v6, v7, :cond_a

    const/16 v7, 0x8

    invoke-virtual {v3, v7}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v7

    if-eqz v7, :cond_8

    const/16 v7, 0x8

    invoke-virtual {v3, v7}, Lcom/google/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v2

    :cond_8
    invoke-virtual {p0, v2}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->processD2sMessage(Ljava/lang/String;)I

    move-result v5

    invoke-static {}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->getIncludeStreamIdInProtobuf()Z

    move-result v7

    if-eqz v7, :cond_9

    const/16 v7, 0x9

    invoke-virtual {v3, v7, v5}, Lcom/google/common/io/protocol/ProtoBuf;->setInt(II)V

    invoke-virtual {p2, v5}, Lorg/jivesoftware/smack/packet/Packet;->setStreamId(I)V

    :cond_9
    invoke-direct {p0, v1}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->setLastStreamIdAcked(I)Z

    move-result v7

    if-eqz v7, :cond_4

    const/16 v7, 0xa

    invoke-virtual {v3, v7, v1}, Lcom/google/common/io/protocol/ProtoBuf;->setInt(II)V

    invoke-virtual {p2, v1}, Lorg/jivesoftware/smack/packet/Packet;->setLastStreamId(I)V

    goto :goto_1

    :cond_a
    sget-object v7, Lcom/google/android/gsf/gtalkservice/proto/GtalkCoreMessageTypes;->MESSAGE_STANZA:Lcom/google/common/io/protocol/ProtoBufType;

    if-ne v6, v7, :cond_d

    const/16 v7, 0xd

    invoke-virtual {v3, v7}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v7

    if-eqz v7, :cond_b

    const/16 v7, 0xd

    invoke-virtual {v3, v7}, Lcom/google/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v2

    :cond_b
    invoke-virtual {p0, v2}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->processD2sMessage(Ljava/lang/String;)I

    move-result v5

    invoke-static {}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->getIncludeStreamIdInProtobuf()Z

    move-result v7

    if-eqz v7, :cond_c

    const/16 v7, 0xe

    invoke-virtual {v3, v7, v5}, Lcom/google/common/io/protocol/ProtoBuf;->setInt(II)V

    invoke-virtual {p2, v5}, Lorg/jivesoftware/smack/packet/Packet;->setStreamId(I)V

    :cond_c
    invoke-direct {p0, v1}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->setLastStreamIdAcked(I)Z

    move-result v7

    if-eqz v7, :cond_4

    const/16 v7, 0xf

    invoke-virtual {v3, v7, v1}, Lcom/google/common/io/protocol/ProtoBuf;->setInt(II)V

    invoke-virtual {p2, v1}, Lorg/jivesoftware/smack/packet/Packet;->setLastStreamId(I)V

    goto/16 :goto_1

    :cond_d
    sget-object v7, Lcom/google/android/gsf/gtalkservice/proto/GtalkCoreMessageTypes;->HTTP_REQUEST:Lcom/google/common/io/protocol/ProtoBufType;

    if-ne v6, v7, :cond_10

    const/4 v7, 0x4

    invoke-virtual {v3, v7}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v7

    if-eqz v7, :cond_e

    const/4 v7, 0x4

    invoke-virtual {v3, v7}, Lcom/google/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v2

    :cond_e
    invoke-virtual {p0, v2}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->processD2sMessage(Ljava/lang/String;)I

    move-result v5

    invoke-static {}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->getIncludeStreamIdInProtobuf()Z

    move-result v7

    if-eqz v7, :cond_f

    const/4 v7, 0x5

    invoke-virtual {v3, v7, v5}, Lcom/google/common/io/protocol/ProtoBuf;->setInt(II)V

    invoke-virtual {p2, v5}, Lorg/jivesoftware/smack/packet/Packet;->setStreamId(I)V

    :cond_f
    invoke-direct {p0, v1}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->setLastStreamIdAcked(I)Z

    move-result v7

    if-eqz v7, :cond_4

    const/4 v7, 0x6

    invoke-virtual {v3, v7, v1}, Lcom/google/common/io/protocol/ProtoBuf;->setInt(II)V

    invoke-virtual {p2, v1}, Lorg/jivesoftware/smack/packet/Packet;->setLastStreamId(I)V

    goto/16 :goto_1

    :cond_10
    sget-object v7, Lcom/google/android/gsf/gtalkservice/proto/GtalkCoreMessageTypes;->PRESENCE_STANZA:Lcom/google/common/io/protocol/ProtoBufType;

    if-ne v6, v7, :cond_13

    const/16 v7, 0xd

    invoke-virtual {v3, v7}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v7

    if-eqz v7, :cond_11

    const/16 v7, 0xd

    invoke-virtual {v3, v7}, Lcom/google/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v2

    :cond_11
    invoke-virtual {p0, v2}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->processD2sMessage(Ljava/lang/String;)I

    move-result v5

    invoke-static {}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->getIncludeStreamIdInProtobuf()Z

    move-result v7

    if-eqz v7, :cond_12

    const/16 v7, 0xe

    invoke-virtual {v3, v7, v5}, Lcom/google/common/io/protocol/ProtoBuf;->setInt(II)V

    invoke-virtual {p2, v5}, Lorg/jivesoftware/smack/packet/Packet;->setStreamId(I)V

    :cond_12
    invoke-direct {p0, v1}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->setLastStreamIdAcked(I)Z

    move-result v7

    if-eqz v7, :cond_4

    const/16 v7, 0xf

    invoke-virtual {v3, v7, v1}, Lcom/google/common/io/protocol/ProtoBuf;->setInt(II)V

    invoke-virtual {p2, v1}, Lorg/jivesoftware/smack/packet/Packet;->setLastStreamId(I)V

    goto/16 :goto_1

    :cond_13
    sget-object v7, Lcom/google/android/gsf/gtalkservice/proto/GtalkCoreMessageTypes;->DATA_MESSAGE_STANZA:Lcom/google/common/io/protocol/ProtoBufType;

    if-ne v6, v7, :cond_16

    const/16 v7, 0x9

    invoke-virtual {v3, v7}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v7

    if-eqz v7, :cond_14

    const/16 v7, 0x9

    invoke-virtual {v3, v7}, Lcom/google/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v2

    :cond_14
    invoke-virtual {p0, v2}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->processD2sMessage(Ljava/lang/String;)I

    move-result v5

    invoke-static {}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->getIncludeStreamIdInProtobuf()Z

    move-result v7

    if-eqz v7, :cond_15

    const/16 v7, 0xa

    invoke-virtual {v3, v7, v5}, Lcom/google/common/io/protocol/ProtoBuf;->setInt(II)V

    invoke-virtual {p2, v5}, Lorg/jivesoftware/smack/packet/Packet;->setStreamId(I)V

    :cond_15
    invoke-direct {p0, v1}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->setLastStreamIdAcked(I)Z

    move-result v7

    if-eqz v7, :cond_4

    const/16 v7, 0xb

    invoke-virtual {v3, v7, v1}, Lcom/google/common/io/protocol/ProtoBuf;->setInt(II)V

    invoke-virtual {p2, v1}, Lorg/jivesoftware/smack/packet/Packet;->setLastStreamId(I)V

    goto/16 :goto_1

    :cond_16
    sget-object v7, Lcom/google/android/gsf/gtalkservice/proto/GtalkCoreMessageTypes;->LOGIN_REQUEST:Lcom/google/common/io/protocol/ProtoBufType;

    if-ne v6, v7, :cond_17

    const/4 v7, 0x0

    invoke-virtual {p0, v7}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->processD2sMessage(Ljava/lang/String;)I

    move-result v5

    goto/16 :goto_1

    :cond_17
    const-string v7, "Rmq"

    const/4 v8, 0x3

    invoke-static {v7, v8}, Lcom/google/android/gsf/gtalkservice/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_4

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "serializePacketAsProtoBuf: unhandled type "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->log(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_1
.end method

.method public setAckingInterval(I)V
    .locals 1
    .param p1    # I

    if-gtz p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    iput p1, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mAckingInterval:I

    return-void
.end method

.method public setMessageQueueCallbacks(Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager$MessageQueueCallbacks;)V
    .locals 0
    .param p1    # Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager$MessageQueueCallbacks;

    iput-object p1, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mMessageQueueCallback:Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager$MessageQueueCallbacks;

    return-void
.end method

.method public setPacketSender(Lcom/google/android/gsf/gtalkservice/PacketSender;)V
    .locals 0
    .param p1    # Lcom/google/android/gsf/gtalkservice/PacketSender;

    iput-object p1, p0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager;->mPacketSender:Lcom/google/android/gsf/gtalkservice/PacketSender;

    return-void
.end method
