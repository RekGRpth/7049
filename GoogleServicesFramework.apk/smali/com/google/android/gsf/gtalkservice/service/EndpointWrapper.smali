.class public Lcom/google/android/gsf/gtalkservice/service/EndpointWrapper;
.super Lcom/google/android/gtalkservice/IGTalkConnection$Stub;
.source "EndpointWrapper.java"


# instance fields
.field private mEndpoint:Lcom/google/android/gsf/gtalkservice/Endpoint;

.field private mService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;


# direct methods
.method public constructor <init>(Lcom/google/android/gsf/gtalkservice/Endpoint;Lcom/google/android/gsf/gtalkservice/service/GTalkService;)V
    .locals 1
    .param p1    # Lcom/google/android/gsf/gtalkservice/Endpoint;
    .param p2    # Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-direct {p0}, Lcom/google/android/gtalkservice/IGTalkConnection$Stub;-><init>()V

    iput-object p1, p0, Lcom/google/android/gsf/gtalkservice/service/EndpointWrapper;->mEndpoint:Lcom/google/android/gsf/gtalkservice/Endpoint;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/EndpointWrapper;->mEndpoint:Lcom/google/android/gsf/gtalkservice/Endpoint;

    invoke-virtual {v0, p0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->setSessionWrapper(Lcom/google/android/gsf/gtalkservice/service/EndpointWrapper;)V

    iput-object p2, p0, Lcom/google/android/gsf/gtalkservice/service/EndpointWrapper;->mService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    return-void
.end method


# virtual methods
.method public clearConnectionStatistics()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/EndpointWrapper;->mEndpoint:Lcom/google/android/gsf/gtalkservice/Endpoint;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->clearStats()V

    return-void
.end method

.method public createImSession()Lcom/google/android/gtalkservice/IImSession;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/EndpointWrapper;->mEndpoint:Lcom/google/android/gsf/gtalkservice/Endpoint;

    instance-of v0, v0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/EndpointWrapper;->mEndpoint:Lcom/google/android/gsf/gtalkservice/Endpoint;

    check-cast v0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getImSession()Lcom/google/android/gtalkservice/IImSession;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getConnectionUptime()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/EndpointWrapper;->mEndpoint:Lcom/google/android/gsf/gtalkservice/Endpoint;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getConnectionUptime()I

    move-result v0

    return v0
.end method

.method public getDefaultImSession()Lcom/google/android/gtalkservice/IImSession;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/EndpointWrapper;->mEndpoint:Lcom/google/android/gsf/gtalkservice/Endpoint;

    instance-of v0, v0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/EndpointWrapper;->mEndpoint:Lcom/google/android/gsf/gtalkservice/Endpoint;

    check-cast v0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getImSession()Lcom/google/android/gtalkservice/IImSession;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDeviceId()Ljava/lang/String;
    .locals 3

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/service/EndpointWrapper;->mEndpoint:Lcom/google/android/gsf/gtalkservice/Endpoint;

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getAccount()Lcom/google/android/gsf/gtalkservice/Account;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/Account;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    :cond_0
    return-object v1
.end method

.method public getEndpoint()Lcom/google/android/gsf/gtalkservice/Endpoint;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/EndpointWrapper;->mEndpoint:Lcom/google/android/gsf/gtalkservice/Endpoint;

    return-object v0
.end method

.method public getImSessionForAccountId(J)Lcom/google/android/gtalkservice/IImSession;
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/EndpointWrapper;->mEndpoint:Lcom/google/android/gsf/gtalkservice/Endpoint;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getAccountId()J

    move-result-wide v0

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/EndpointWrapper;->mEndpoint:Lcom/google/android/gsf/gtalkservice/Endpoint;

    instance-of v0, v0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/EndpointWrapper;->mEndpoint:Lcom/google/android/gsf/gtalkservice/Endpoint;

    check-cast v0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getImSession()Lcom/google/android/gtalkservice/IImSession;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getJid()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/EndpointWrapper;->mEndpoint:Lcom/google/android/gsf/gtalkservice/Endpoint;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getJid()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLastActivityFromServerTime()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/EndpointWrapper;->mEndpoint:Lcom/google/android/gsf/gtalkservice/Endpoint;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getLastActivityFromServerTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public getLastActivityToServerTime()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/EndpointWrapper;->mEndpoint:Lcom/google/android/gsf/gtalkservice/Endpoint;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getLastActivityToServerTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public getNumberOfConnectionsAttempted()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/EndpointWrapper;->mEndpoint:Lcom/google/android/gsf/gtalkservice/Endpoint;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getNumberOfConnectionsAttempted()I

    move-result v0

    return v0
.end method

.method public getNumberOfConnectionsMade()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/EndpointWrapper;->mEndpoint:Lcom/google/android/gsf/gtalkservice/Endpoint;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getNumberOfConnectionsMade()I

    move-result v0

    return v0
.end method

.method public getOriginalUsername()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/EndpointWrapper;->mEndpoint:Lcom/google/android/gsf/gtalkservice/Endpoint;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getOriginalUsername()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUsername()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/EndpointWrapper;->mEndpoint:Lcom/google/android/gsf/gtalkservice/Endpoint;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->getUsername()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isConnected()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/EndpointWrapper;->mEndpoint:Lcom/google/android/gsf/gtalkservice/Endpoint;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->isConnected()Z

    move-result v0

    return v0
.end method

.method logout()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/EndpointWrapper;->mEndpoint:Lcom/google/android/gsf/gtalkservice/Endpoint;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/Endpoint;->logout()V

    return-void
.end method

.method public sendHeartbeat()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/EndpointWrapper;->mService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->sendHeartbeatToServer()V

    return-void
.end method

.method public sendHttpRequest([BLcom/google/android/gtalkservice/IHttpRequestCallback;)V
    .locals 1
    .param p1    # [B
    .param p2    # Lcom/google/android/gtalkservice/IHttpRequestCallback;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/EndpointWrapper;->mEndpoint:Lcom/google/android/gsf/gtalkservice/Endpoint;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gsf/gtalkservice/Endpoint;->sendHttpRequest([BLcom/google/android/gtalkservice/IHttpRequestCallback;)V

    return-void
.end method
