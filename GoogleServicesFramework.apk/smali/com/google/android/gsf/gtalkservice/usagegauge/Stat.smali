.class public Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;
.super Ljava/lang/Object;
.source "Stat.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gsf/gtalkservice/usagegauge/Stat$1;,
        Lcom/google/android/gsf/gtalkservice/usagegauge/Stat$Slot;
    }
.end annotation


# instance fields
.field private current:I

.field private interval:J

.field private slotMillis:J

.field private final slots:[Lcom/google/android/gsf/gtalkservice/usagegauge/Stat$Slot;


# direct methods
.method public constructor <init>(J)V
    .locals 1
    .param p1    # J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x1e

    new-array v0, v0, [Lcom/google/android/gsf/gtalkservice/usagegauge/Stat$Slot;

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;->slots:[Lcom/google/android/gsf/gtalkservice/usagegauge/Stat$Slot;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;->setInterval(J)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;->current:I

    return-void
.end method

.method private getSlot()Lcom/google/android/gsf/gtalkservice/usagegauge/Stat$Slot;
    .locals 4

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;->slots:[Lcom/google/android/gsf/gtalkservice/usagegauge/Stat$Slot;

    iget v2, p0, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;->current:I

    aget-object v0, v1, v2

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;->slots:[Lcom/google/android/gsf/gtalkservice/usagegauge/Stat$Slot;

    iget v2, p0, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;->current:I

    new-instance v0, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat$Slot;

    const/4 v3, 0x0

    invoke-direct {v0, v3}, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat$Slot;-><init>(Lcom/google/android/gsf/gtalkservice/usagegauge/Stat$1;)V

    aput-object v0, v1, v2

    :cond_0
    return-object v0
.end method

.method private getSlotBoundary(J)J
    .locals 4
    .param p1    # J

    iget-wide v0, p0, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;->slotMillis:J

    div-long v0, p1, v0

    iget-wide v2, p0, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;->slotMillis:J

    mul-long/2addr v0, v2

    iget-wide v2, p0, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;->slotMillis:J

    add-long/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public get()J
    .locals 2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;->get(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public declared-synchronized get(J)J
    .locals 11
    .param p1    # J

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;->getSlotBoundary(J)J

    move-result-wide v7

    iget-wide v9, p0, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;->interval:J

    sub-long v3, v7, v9

    const-wide/16 v5, 0x0

    iget v1, p0, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;->current:I

    const/4 v0, 0x0

    :goto_0
    const/16 v7, 0x1e

    if-ge v0, v7, :cond_0

    iget-object v7, p0, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;->slots:[Lcom/google/android/gsf/gtalkservice/usagegauge/Stat$Slot;

    aget-object v2, v7, v1

    if-eqz v2, :cond_0

    iget-wide v7, v2, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat$Slot;->end:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v7, v7, v3

    if-gtz v7, :cond_1

    :cond_0
    monitor-exit p0

    return-wide v5

    :cond_1
    :try_start_1
    iget-wide v7, v2, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat$Slot;->count:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    add-long/2addr v5, v7

    if-nez v1, :cond_2

    const/16 v1, 0x1e

    :cond_2
    add-int/lit8 v1, v1, -0x1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v7

    monitor-exit p0

    throw v7
.end method

.method public incBy(J)V
    .locals 2
    .param p1    # J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;->incBy(JJ)V

    return-void
.end method

.method public declared-synchronized incBy(JJ)V
    .locals 3
    .param p1    # J
    .param p3    # J

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;->getSlot()Lcom/google/android/gsf/gtalkservice/usagegauge/Stat$Slot;

    move-result-object v0

    iget-wide v1, v0, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat$Slot;->end:J

    cmp-long v1, p3, v1

    if-ltz v1, :cond_1

    iget v1, p0, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;->current:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;->current:I

    iget v1, p0, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;->current:I

    const/16 v2, 0x1e

    if-ne v1, v2, :cond_0

    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;->current:I

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;->getSlot()Lcom/google/android/gsf/gtalkservice/usagegauge/Stat$Slot;

    move-result-object v0

    const-wide/16 v1, 0x0

    iput-wide v1, v0, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat$Slot;->count:J

    invoke-direct {p0, p3, p4}, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;->getSlotBoundary(J)J

    move-result-wide v1

    iput-wide v1, v0, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat$Slot;->end:J

    :cond_1
    iget-wide v1, v0, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat$Slot;->count:J

    add-long/2addr v1, p1

    iput-wide v1, v0, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat$Slot;->count:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public setInterval(J)V
    .locals 2
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;->interval:J

    const-wide/16 v0, 0x1e

    div-long v0, p1, v0

    iput-wide v0, p0, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;->slotMillis:J

    return-void
.end method
