.class public Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;
.super Ljava/lang/Object;
.source "UsageGauge.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge$UsageType;
    }
.end annotation


# static fields
.field private static final PACKET_COUNT_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final PACKET_SIZE_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final TOTAL_RECEIVED_PACKET_COUNT:Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;

.field private static final TOTAL_RECEIVED_PACKET_SIZE:Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;

.field private static final TOTAL_SENT_PACKET_COUNT:Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;

.field private static final TOTAL_SENT_PACKET_SIZE:Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;

.field private static sInterval:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-wide/32 v0, 0x2932e00

    sput-wide v0, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;->sInterval:J

    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    sput-object v0, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;->PACKET_COUNT_MAP:Ljava/util/Map;

    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    sput-object v0, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;->PACKET_SIZE_MAP:Ljava/util/Map;

    new-instance v0, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;

    sget-wide v1, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;->sInterval:J

    invoke-direct {v0, v1, v2}, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;-><init>(J)V

    sput-object v0, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;->TOTAL_RECEIVED_PACKET_COUNT:Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;

    new-instance v0, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;

    sget-wide v1, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;->sInterval:J

    invoke-direct {v0, v1, v2}, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;-><init>(J)V

    sput-object v0, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;->TOTAL_RECEIVED_PACKET_SIZE:Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;

    new-instance v0, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;

    sget-wide v1, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;->sInterval:J

    invoke-direct {v0, v1, v2}, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;-><init>(J)V

    sput-object v0, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;->TOTAL_SENT_PACKET_COUNT:Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;

    new-instance v0, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;

    sget-wide v1, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;->sInterval:J

    invoke-direct {v0, v1, v2}, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;-><init>(J)V

    sput-object v0, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;->TOTAL_SENT_PACKET_SIZE:Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized dump(Ljava/io/PrintWriter;)V
    .locals 34
    .param p0    # Ljava/io/PrintWriter;

    const-class v29, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;

    monitor-enter v29

    :try_start_0
    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "Transmission statistics: (last "

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    sget-wide v30, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;->sInterval:J

    const-wide/16 v32, 0x3e8

    div-long v30, v30, v32

    const-wide/16 v32, 0xe10

    div-long v30, v30, v32

    move-object/from16 v0, v28

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v30, " hours)"

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string v28, "----------------------------------------"

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string v28, ""

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    sget-object v28, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;->TOTAL_RECEIVED_PACKET_COUNT:Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;

    invoke-virtual/range {v28 .. v28}, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;->get()J

    move-result-wide v11

    sget-object v28, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;->TOTAL_SENT_PACKET_COUNT:Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;

    invoke-virtual/range {v28 .. v28}, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;->get()J

    move-result-wide v15

    add-long v23, v11, v15

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "Packet count (received/sent/total): "

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v30, " / "

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-wide v1, v15

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v30, " / "

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-wide/from16 v1, v23

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    sget-object v28, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;->TOTAL_RECEIVED_PACKET_SIZE:Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;

    invoke-virtual/range {v28 .. v28}, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;->get()J

    move-result-wide v13

    sget-object v28, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;->TOTAL_SENT_PACKET_SIZE:Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;

    invoke-virtual/range {v28 .. v28}, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;->get()J

    move-result-wide v17

    add-long v25, v13, v17

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "Packet size (received/sent/total): "

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v30, " / "

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-wide/from16 v1, v17

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v30, " / "

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-wide/from16 v1, v25

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "Average packet size (received/sent/total): "

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-static {v13, v14, v11, v12}, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;->getQuotientString(JJ)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v30, " / "

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-wide/from16 v0, v17

    move-wide v2, v15

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;->getQuotientString(JJ)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v30, " / "

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-wide/from16 v0, v25

    move-wide/from16 v2, v23

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;->getQuotientString(JJ)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string v28, "Packet breakdown by types (type: count/count percentage/size percentage): "

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    sget-object v28, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;->PACKET_COUNT_MAP:Ljava/util/Map;

    invoke-interface/range {v28 .. v28}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v28

    invoke-interface/range {v28 .. v28}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v28

    if-eqz v28, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/Map$Entry;

    invoke-interface {v10}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/String;

    invoke-interface {v10}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map;

    sget-object v28, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;->PACKET_SIZE_MAP:Ljava/util/Map;

    move-object/from16 v0, v28

    move-object/from16 v1, v27

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/util/Map;

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "  "

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v30, ":"

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v28

    invoke-interface/range {v28 .. v28}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v28

    if-eqz v28, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Map$Entry;

    invoke-interface {v7}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/String;

    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;

    invoke-virtual/range {v28 .. v28}, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;->get()J

    move-result-wide v4

    invoke-interface/range {v21 .. v22}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;

    invoke-virtual/range {v28 .. v28}, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;->get()J

    move-result-wide v19

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "    "

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v30, ": "

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v30, " / "

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-wide/16 v30, 0x64

    mul-long v30, v30, v4

    move-wide/from16 v0, v30

    move-wide/from16 v2, v23

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;->getQuotientString(JJ)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v30, "% / "

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-wide/16 v30, 0x64

    mul-long v30, v30, v19

    move-wide/from16 v0, v30

    move-wide/from16 v2, v25

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;->getQuotientString(JJ)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v30, "%"

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v28

    monitor-exit v29

    throw v28

    :cond_1
    monitor-exit v29

    return-void
.end method

.method private static getDataMessageTarget(Lorg/jivesoftware/smack/packet/DataMessage;)Ljava/lang/String;
    .locals 1
    .param p0    # Lorg/jivesoftware/smack/packet/DataMessage;

    invoke-virtual {p0}, Lorg/jivesoftware/smack/packet/DataMessage;->getCategory()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getPacketUsageType(Lorg/jivesoftware/smack/packet/Packet;)Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge$UsageType;
    .locals 3
    .param p0    # Lorg/jivesoftware/smack/packet/Packet;

    if-eqz p0, :cond_a

    instance-of v0, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacket;

    if-eqz v0, :cond_0

    check-cast p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacket;

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacket;->getOriginalPacket()Lorg/jivesoftware/smack/packet/Packet;

    move-result-object p0

    :cond_0
    instance-of v0, p0, Lorg/jivesoftware/smack/packet/Message;

    if-eqz v0, :cond_2

    instance-of v0, p0, Lorg/jivesoftware/smack/packet/DataMessage;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge$UsageType;

    const-string v1, "data message"

    check-cast p0, Lorg/jivesoftware/smack/packet/DataMessage;

    invoke-static {p0}, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;->getDataMessageTarget(Lorg/jivesoftware/smack/packet/DataMessage;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge$UsageType;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge$UsageType;

    const-string v1, "talk"

    const-string v2, "chat"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge$UsageType;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    instance-of v0, p0, Lorg/jivesoftware/smack/packet/Presence;

    if-nez v0, :cond_3

    instance-of v0, p0, Lorg/jivesoftware/smack/packet/BatchPresence;

    if-eqz v0, :cond_4

    :cond_3
    new-instance v0, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge$UsageType;

    const-string v1, "talk"

    const-string v2, "presence"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge$UsageType;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    instance-of v0, p0, Lorg/jivesoftware/smack/packet/IQ;

    if-eqz v0, :cond_6

    instance-of v0, p0, Lcom/google/android/gsf/gtalkservice/extensions/PostAuthBatchQuery;

    if-eqz v0, :cond_5

    new-instance v0, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge$UsageType;

    const-string v1, "connection"

    const-string v2, "login"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge$UsageType;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    new-instance v0, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge$UsageType;

    const-string v1, "talk"

    const-string v2, "iq"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge$UsageType;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    instance-of v0, p0, Lorg/jivesoftware/smack/packet/LoginRequest;

    if-nez v0, :cond_7

    instance-of v0, p0, Lorg/jivesoftware/smack/packet/LoginResponse;

    if-eqz v0, :cond_8

    :cond_7
    new-instance v0, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge$UsageType;

    const-string v1, "connection"

    const-string v2, "login"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge$UsageType;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_8
    instance-of v0, p0, Lorg/jivesoftware/smack/packet/HttpRequest;

    if-nez v0, :cond_9

    instance-of v0, p0, Lorg/jivesoftware/smack/packet/HttpResponse;

    if-eqz v0, :cond_a

    :cond_9
    new-instance v0, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge$UsageType;

    const-string v1, "miscellaneous"

    const-string v2, "tunnelled http"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge$UsageType;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_a
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getQuotientString(JJ)Ljava/lang/String;
    .locals 2
    .param p0    # J
    .param p2    # J

    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-nez v0, :cond_0

    const-string v0, "-"

    :goto_0
    return-object v0

    :cond_0
    div-long v0, p0, p2

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static declared-synchronized update(Lorg/jivesoftware/smack/packet/Packet;JZ)V
    .locals 7
    .param p0    # Lorg/jivesoftware/smack/packet/Packet;
    .param p1    # J
    .param p3    # Z

    const-class v2, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;

    monitor-enter v2

    :try_start_0
    invoke-static {p0}, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;->getPacketUsageType(Lorg/jivesoftware/smack/packet/Packet;)Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge$UsageType;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v1, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;->PACKET_COUNT_MAP:Ljava/util/Map;

    iget-object v3, v0, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge$UsageType;->type:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge$UsageType;->subtype:Ljava/lang/String;

    const-wide/16 v5, 0x1

    invoke-static {v1, v3, v4, v5, v6}, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;->updateStatInMap(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;J)V

    sget-object v1, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;->PACKET_SIZE_MAP:Ljava/util/Map;

    iget-object v3, v0, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge$UsageType;->type:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge$UsageType;->subtype:Ljava/lang/String;

    invoke-static {v1, v3, v4, p1, p2}, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;->updateStatInMap(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;J)V

    :cond_0
    invoke-static {p1, p2, p3}, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;->updateTotalStats(JZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v2

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public static declared-synchronized updateHeartbeat(JZ)V
    .locals 6
    .param p0    # J
    .param p2    # Z

    const-class v1, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;->PACKET_COUNT_MAP:Ljava/util/Map;

    const-string v2, "connection"

    const-string v3, "heartbeat"

    const-wide/16 v4, 0x1

    invoke-static {v0, v2, v3, v4, v5}, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;->updateStatInMap(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;J)V

    sget-object v0, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;->PACKET_SIZE_MAP:Ljava/util/Map;

    const-string v2, "connection"

    const-string v3, "heartbeat"

    invoke-static {v0, v2, v3, p0, p1}, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;->updateStatInMap(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;J)V

    invoke-static {p0, p1, p2}, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;->updateTotalStats(JZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static updateStatInMap(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;",
            ">;>;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J)V"
        }
    .end annotation

    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    if-nez v1, :cond_0

    new-instance v1, Ljava/util/TreeMap;

    invoke-direct {v1}, Ljava/util/TreeMap;-><init>()V

    invoke-interface {p0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-interface {v1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;

    sget-wide v2, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;->sInterval:J

    invoke-direct {v0, v2, v3}, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;-><init>(J)V

    invoke-interface {v1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    invoke-virtual {v0, p3, p4}, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;->incBy(J)V

    return-void
.end method

.method private static updateTotalStats(JZ)V
    .locals 3
    .param p0    # J
    .param p2    # Z

    const-wide/16 v1, 0x1

    if-eqz p2, :cond_0

    sget-object v0, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;->TOTAL_RECEIVED_PACKET_COUNT:Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;->incBy(J)V

    sget-object v0, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;->TOTAL_RECEIVED_PACKET_SIZE:Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;

    invoke-virtual {v0, p0, p1}, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;->incBy(J)V

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;->TOTAL_SENT_PACKET_COUNT:Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;->incBy(J)V

    sget-object v0, Lcom/google/android/gsf/gtalkservice/usagegauge/UsageGauge;->TOTAL_SENT_PACKET_SIZE:Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;

    invoke-virtual {v0, p0, p1}, Lcom/google/android/gsf/gtalkservice/usagegauge/Stat;->incBy(J)V

    goto :goto_0
.end method
