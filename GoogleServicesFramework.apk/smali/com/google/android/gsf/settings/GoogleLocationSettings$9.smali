.class Lcom/google/android/gsf/settings/GoogleLocationSettings$9;
.super Ljava/lang/Object;
.source "GoogleLocationSettings.java"

# interfaces
.implements Lcom/google/android/gsf/settings/LocationHistoryClient$LocationHistoryCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gsf/settings/GoogleLocationSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gsf/settings/GoogleLocationSettings;


# direct methods
.method constructor <init>(Lcom/google/android/gsf/settings/GoogleLocationSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings$9;->this$0:Lcom/google/android/gsf/settings/GoogleLocationSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResult([I)V
    .locals 7
    .param p1    # [I

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    const/4 v2, 0x0

    :goto_0
    array-length v3, p1

    if-ge v2, v3, :cond_2

    aget v3, p1, v2

    const/4 v6, 0x2

    if-ne v3, v6, :cond_0

    move v3, v4

    :goto_1
    or-int/2addr v1, v3

    aget v3, p1, v2

    if-ne v3, v4, :cond_1

    move v3, v4

    :goto_2
    or-int/2addr v0, v3

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    move v3, v5

    goto :goto_1

    :cond_1
    move v3, v5

    goto :goto_2

    :cond_2
    iget-object v3, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings$9;->this$0:Lcom/google/android/gsf/settings/GoogleLocationSettings;

    # invokes: Lcom/google/android/gsf/settings/GoogleLocationSettings;->setErrorText(I)V
    invoke-static {v3, v5}, Lcom/google/android/gsf/settings/GoogleLocationSettings;->access$900(Lcom/google/android/gsf/settings/GoogleLocationSettings;I)V

    if-eqz v1, :cond_3

    iget-object v3, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings$9;->this$0:Lcom/google/android/gsf/settings/GoogleLocationSettings;

    invoke-virtual {v3}, Lcom/google/android/gsf/settings/GoogleLocationSettings;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const v6, 0x7f0600d3

    invoke-static {v3, v6, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    iget-object v3, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings$9;->this$0:Lcom/google/android/gsf/settings/GoogleLocationSettings;

    # invokes: Lcom/google/android/gsf/settings/GoogleLocationSettings;->showAllowHistorySection(Z)V
    invoke-static {v3, v5}, Lcom/google/android/gsf/settings/GoogleLocationSettings;->access$800(Lcom/google/android/gsf/settings/GoogleLocationSettings;Z)V

    iget-object v3, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings$9;->this$0:Lcom/google/android/gsf/settings/GoogleLocationSettings;

    # invokes: Lcom/google/android/gsf/settings/GoogleLocationSettings;->reloadAllowHistoryState()V
    invoke-static {v3}, Lcom/google/android/gsf/settings/GoogleLocationSettings;->access$1200(Lcom/google/android/gsf/settings/GoogleLocationSettings;)V

    :goto_3
    return-void

    :cond_3
    iget-object v3, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings$9;->this$0:Lcom/google/android/gsf/settings/GoogleLocationSettings;

    # invokes: Lcom/google/android/gsf/settings/GoogleLocationSettings;->showAllowHistorySection(Z)V
    invoke-static {v3, v4}, Lcom/google/android/gsf/settings/GoogleLocationSettings;->access$800(Lcom/google/android/gsf/settings/GoogleLocationSettings;Z)V

    iget-object v3, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings$9;->this$0:Lcom/google/android/gsf/settings/GoogleLocationSettings;

    # invokes: Lcom/google/android/gsf/settings/GoogleLocationSettings;->setAllowHistoryQuietly(Z)V
    invoke-static {v3, v0}, Lcom/google/android/gsf/settings/GoogleLocationSettings;->access$400(Lcom/google/android/gsf/settings/GoogleLocationSettings;Z)V

    iget-object v4, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings$9;->this$0:Lcom/google/android/gsf/settings/GoogleLocationSettings;

    invoke-virtual {p1}, [I->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [I

    # setter for: Lcom/google/android/gsf/settings/GoogleLocationSettings;->mLocationHistoryState:[I
    invoke-static {v4, v3}, Lcom/google/android/gsf/settings/GoogleLocationSettings;->access$702(Lcom/google/android/gsf/settings/GoogleLocationSettings;[I)[I

    goto :goto_3
.end method
