.class Lcom/google/android/gsf/subscribedfeeds/AbstractSyncableContentProvider$1;
.super Ljava/lang/Object;
.source "AbstractSyncableContentProvider.java"

# interfaces
.implements Landroid/accounts/OnAccountsUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gsf/subscribedfeeds/AbstractSyncableContentProvider;->onCreate()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gsf/subscribedfeeds/AbstractSyncableContentProvider;


# direct methods
.method constructor <init>(Lcom/google/android/gsf/subscribedfeeds/AbstractSyncableContentProvider;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gsf/subscribedfeeds/AbstractSyncableContentProvider$1;->this$0:Lcom/google/android/gsf/subscribedfeeds/AbstractSyncableContentProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccountsUpdated([Landroid/accounts/Account;)V
    .locals 10
    .param p1    # [Landroid/accounts/Account;

    iget-object v8, p0, Lcom/google/android/gsf/subscribedfeeds/AbstractSyncableContentProvider$1;->this$0:Lcom/google/android/gsf/subscribedfeeds/AbstractSyncableContentProvider;

    iget-object v9, p0, Lcom/google/android/gsf/subscribedfeeds/AbstractSyncableContentProvider$1;->this$0:Lcom/google/android/gsf/subscribedfeeds/AbstractSyncableContentProvider;

    iget-object v9, v9, Lcom/google/android/gsf/subscribedfeeds/AbstractSyncableContentProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v9}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v9

    iput-object v9, v8, Lcom/google/android/gsf/subscribedfeeds/AbstractSyncableContentProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object v1, p1

    array-length v6, v1

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v6, :cond_1

    aget-object v0, v1, v5

    iget-object v8, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    const-string v9, "com.google"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v8

    new-array p1, v8, [Landroid/accounts/Account;

    const/4 v3, 0x0

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    add-int/lit8 v4, v3, 0x1

    aput-object v0, p1, v3

    move v3, v4

    goto :goto_1

    :cond_2
    iget-object v8, p0, Lcom/google/android/gsf/subscribedfeeds/AbstractSyncableContentProvider$1;->this$0:Lcom/google/android/gsf/subscribedfeeds/AbstractSyncableContentProvider;

    invoke-virtual {v8, p1}, Lcom/google/android/gsf/subscribedfeeds/AbstractSyncableContentProvider;->onAccountsChanged([Landroid/accounts/Account;)V

    iget-object v8, p0, Lcom/google/android/gsf/subscribedfeeds/AbstractSyncableContentProvider$1;->this$0:Lcom/google/android/gsf/subscribedfeeds/AbstractSyncableContentProvider;

    invoke-virtual {v8}, Lcom/google/android/gsf/subscribedfeeds/AbstractSyncableContentProvider;->getTempProviderSyncAdapter()Lcom/google/android/gsf/subscribedfeeds/TempProviderSyncAdapter;

    move-result-object v7

    if-eqz v7, :cond_3

    invoke-virtual {v7, p1}, Lcom/google/android/gsf/subscribedfeeds/TempProviderSyncAdapter;->onAccountsChanged([Landroid/accounts/Account;)V

    :cond_3
    return-void
.end method
