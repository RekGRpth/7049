.class public Lcom/google/android/gsf/talk/SuggestionFooterCursor;
.super Landroid/database/CursorWrapper;
.source "SuggestionFooterCursor.java"

# interfaces
.implements Landroid/database/CrossProcessCursor;


# static fields
.field private static sAccountUsername:Ljava/lang/String;


# instance fields
.field private mAtFooterPosition:Z

.field private final mContext:Landroid/content/Context;

.field private mCursor:Landroid/database/CrossProcessCursor;

.field private mIntentAction:Ljava/lang/String;

.field private mIntentData:Ljava/lang/String;

.field private mQuery:Ljava/lang/String;

.field private mText1:Ljava/lang/String;

.field private mText2:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;

    invoke-direct {p0, p2}, Landroid/database/CursorWrapper;-><init>(Landroid/database/Cursor;)V

    check-cast p2, Landroid/database/CrossProcessCursor;

    iput-object p2, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mCursor:Landroid/database/CrossProcessCursor;

    iput-object p3, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mText1:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mText2:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mQuery:Ljava/lang/String;

    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mIntentAction:Ljava/lang/String;

    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mIntentData:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->sAccountUsername:Ljava/lang/String;

    if-nez v1, :cond_4

    const/4 v7, 0x0

    iget-object v1, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/google/android/gsf/TalkContract$Account;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "username"

    aput-object v5, v3, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    if-eqz v8, :cond_2

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v9

    const/4 v1, 0x1

    if-eq v9, v1, :cond_0

    const-string v1, "TalkProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Accounts table has "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " rows, using first account"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_2
    if-nez v7, :cond_3

    const-string v1, "TalkProvider"

    const-string v2, "SuggestionFooterCursor - Couldn\'t get account username"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    sput-object v7, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->sAccountUsername:Ljava/lang/String;

    :cond_4
    return-void

    :catchall_0
    move-exception v1

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method private computeCapabilityIndex(I)I
    .locals 1
    .param p1    # I

    invoke-static {p1}, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->isVideoChatCapable(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    invoke-static {p1}, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->isAudioChatCapable(I)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isAudioChatCapable(I)Z
    .locals 1
    .param p0    # I

    and-int/lit8 v0, p0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isVideoChatCapable(I)Z
    .locals 1
    .param p0    # I

    and-int/lit8 v0, p0, 0x4

    if-eqz v0, :cond_0

    and-int/lit8 v0, p0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public fillWindow(ILandroid/database/CursorWindow;)V
    .locals 1
    .param p1    # I
    .param p2    # Landroid/database/CursorWindow;

    iget-object v0, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mCursor:Landroid/database/CrossProcessCursor;

    invoke-static {v0, p1, p2}, Landroid/database/DatabaseUtils;->cursorFillWindow(Landroid/database/Cursor;ILandroid/database/CursorWindow;)V

    return-void
.end method

.method public getCount()I
    .locals 2

    iget-object v1, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mCursor:Landroid/database/CrossProcessCursor;

    invoke-interface {v1}, Landroid/database/CrossProcessCursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mQuery:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    :cond_1
    return v0
.end method

.method public getPosition()I
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mAtFooterPosition:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->getCount()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mCursor:Landroid/database/CrossProcessCursor;

    invoke-interface {v0}, Landroid/database/CrossProcessCursor;->getPosition()I

    move-result v0

    goto :goto_0
.end method

.method public getStatusIconResourceId(II)I
    .locals 2
    .param p1    # I
    .param p2    # I

    invoke-direct {p0, p2}, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->computeCapabilityIndex(I)I

    move-result v0

    packed-switch p1, :pswitch_data_0

    const v1, 0x108006a

    :goto_0
    return v1

    :pswitch_0
    packed-switch v0, :pswitch_data_1

    :pswitch_1
    packed-switch v0, :pswitch_data_2

    :pswitch_2
    packed-switch v0, :pswitch_data_3

    :pswitch_3
    const v1, 0x1080069

    goto :goto_0

    :pswitch_4
    const v1, 0x108006b

    goto :goto_0

    :pswitch_5
    const v1, 0x10800b1

    goto :goto_0

    :pswitch_6
    const v1, 0x10800ae

    goto :goto_0

    :pswitch_7
    const v1, 0x1080067

    goto :goto_0

    :pswitch_8
    const v1, 0x10800af

    goto :goto_0

    :pswitch_9
    const v1, 0x10800ac

    goto :goto_0

    :pswitch_a
    const v1, 0x1080068

    goto :goto_0

    :pswitch_b
    const v1, 0x10800b0

    goto :goto_0

    :pswitch_c
    const v1, 0x10800ad

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method public getString(I)Ljava/lang/String;
    .locals 11
    .param p1    # I

    const/4 v8, 0x0

    invoke-virtual {p0, p1}, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v4

    iget-boolean v9, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mAtFooterPosition:Z

    if-eqz v9, :cond_9

    const-string v9, "suggest_text_1"

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    iget-object v2, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mText1:Ljava/lang/String;

    :cond_0
    :goto_0
    return-object v2

    :cond_1
    const-string v9, "suggest_text_2"

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    iget-object v2, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mText2:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const-string v9, "suggest_intent_query"

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    iget-object v2, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mQuery:Ljava/lang/String;

    goto :goto_0

    :cond_3
    const-string v9, "suggest_intent_action"

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    iget-object v2, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mIntentAction:Ljava/lang/String;

    goto :goto_0

    :cond_4
    const-string v9, "suggest_intent_data"

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    iget-object v2, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mIntentData:Ljava/lang/String;

    goto :goto_0

    :cond_5
    const-string v9, "presenceMode"

    invoke-virtual {v4, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_6

    const-string v9, "presenceStatus"

    invoke-virtual {v4, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_6

    const-string v9, "capabilities"

    invoke-virtual {v4, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_6

    const-string v9, "userID"

    invoke-virtual {v4, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_6

    const-string v9, "suggest_icon_1"

    invoke-virtual {v4, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_6

    const-string v9, "suggest_icon_1"

    invoke-virtual {v4, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_6

    const-string v9, "suggest_icon_2"

    invoke-virtual {v4, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    :cond_6
    move-object v2, v8

    goto :goto_0

    :cond_7
    const-string v8, "_id"

    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    iget-object v8, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mCursor:Landroid/database/CrossProcessCursor;

    invoke-interface {v8}, Landroid/database/CrossProcessCursor;->getCount()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_8
    new-instance v8, Ljava/lang/UnsupportedOperationException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Footer column: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " not supported"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v8

    :cond_9
    const-string v9, "suggest_icon_1"

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_a

    move-object v2, v8

    goto/16 :goto_0

    :cond_a
    const-string v9, "suggest_icon_2"

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_d

    iget-object v9, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mCursor:Landroid/database/CrossProcessCursor;

    const-string v10, "presenceMode"

    invoke-interface {v9, v10}, Landroid/database/CrossProcessCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    if-gez v6, :cond_b

    move-object v2, v8

    goto/16 :goto_0

    :cond_b
    iget-object v9, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mCursor:Landroid/database/CrossProcessCursor;

    invoke-interface {v9, v6}, Landroid/database/CrossProcessCursor;->getInt(I)I

    move-result v5

    iget-object v9, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mCursor:Landroid/database/CrossProcessCursor;

    const-string v10, "capabilities"

    invoke-interface {v9, v10}, Landroid/database/CrossProcessCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    if-gez v1, :cond_c

    move-object v2, v8

    goto/16 :goto_0

    :cond_c
    iget-object v8, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mCursor:Landroid/database/CrossProcessCursor;

    invoke-interface {v8, v1}, Landroid/database/CrossProcessCursor;->getInt(I)I

    move-result v0

    invoke-virtual {p0, v5, v0}, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->getStatusIconResourceId(II)I

    move-result v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    :cond_d
    const-string v9, "suggest_text_2"

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_11

    iget-object v9, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mCursor:Landroid/database/CrossProcessCursor;

    const-string v10, "presenceStatus"

    invoke-interface {v9, v10}, Landroid/database/CrossProcessCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    if-gez v3, :cond_e

    move-object v2, v8

    goto/16 :goto_0

    :cond_e
    iget-object v9, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mCursor:Landroid/database/CrossProcessCursor;

    invoke-interface {v9, v3}, Landroid/database/CrossProcessCursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_f

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v9

    if-nez v9, :cond_0

    :cond_f
    iget-object v9, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mCursor:Landroid/database/CrossProcessCursor;

    const-string v10, "userID"

    invoke-interface {v9, v10}, Landroid/database/CrossProcessCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    if-gez v7, :cond_10

    move-object v2, v8

    goto/16 :goto_0

    :cond_10
    iget-object v8, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mCursor:Landroid/database/CrossProcessCursor;

    invoke-interface {v8, v7}, Landroid/database/CrossProcessCursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    :cond_11
    iget-object v8, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mCursor:Landroid/database/CrossProcessCursor;

    invoke-interface {v8, p1}, Landroid/database/CrossProcessCursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0
.end method

.method public getWindow()Landroid/database/CursorWindow;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public move(I)Z
    .locals 3
    .param p1    # I

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mCursor:Landroid/database/CrossProcessCursor;

    invoke-interface {v0}, Landroid/database/CrossProcessCursor;->getPosition()I

    move-result v0

    add-int/2addr v0, p1

    iget-object v2, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mCursor:Landroid/database/CrossProcessCursor;

    invoke-interface {v2}, Landroid/database/CrossProcessCursor;->getCount()I

    move-result v2

    if-lt v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mAtFooterPosition:Z

    iget-object v0, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mCursor:Landroid/database/CrossProcessCursor;

    iget-boolean v2, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mAtFooterPosition:Z

    if-eqz v2, :cond_0

    const/4 v1, -0x1

    :cond_0
    add-int/2addr v1, p1

    invoke-interface {v0, v1}, Landroid/database/CrossProcessCursor;->move(I)Z

    move-result v0

    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public moveToLast()Z
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mAtFooterPosition:Z

    iget-object v0, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mCursor:Landroid/database/CrossProcessCursor;

    invoke-interface {v0}, Landroid/database/CrossProcessCursor;->moveToLast()Z

    move-result v0

    return v0
.end method

.method public moveToNext()Z
    .locals 3

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mAtFooterPosition:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mCursor:Landroid/database/CrossProcessCursor;

    invoke-interface {v1}, Landroid/database/CrossProcessCursor;->getPosition()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mCursor:Landroid/database/CrossProcessCursor;

    invoke-interface {v2}, Landroid/database/CrossProcessCursor;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_1

    iput-boolean v0, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mAtFooterPosition:Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mCursor:Landroid/database/CrossProcessCursor;

    invoke-interface {v0}, Landroid/database/CrossProcessCursor;->moveToNext()Z

    move-result v0

    goto :goto_0
.end method

.method public moveToPosition(I)Z
    .locals 5
    .param p1    # I

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, -0x1

    iget-object v1, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mCursor:Landroid/database/CrossProcessCursor;

    invoke-interface {v1}, Landroid/database/CrossProcessCursor;->getCount()I

    move-result v1

    if-lt p1, v1, :cond_1

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mAtFooterPosition:Z

    iget-boolean v1, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mAtFooterPosition:Z

    if-eqz v1, :cond_0

    move v3, v4

    :cond_0
    add-int v0, p1, v3

    if-ne v0, v4, :cond_2

    :goto_1
    return v2

    :cond_1
    move v1, v3

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mCursor:Landroid/database/CrossProcessCursor;

    invoke-interface {v1, v0}, Landroid/database/CrossProcessCursor;->moveToPosition(I)Z

    move-result v2

    goto :goto_1
.end method

.method public moveToPrevious()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mAtFooterPosition:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mAtFooterPosition:Z

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mCursor:Landroid/database/CrossProcessCursor;

    invoke-interface {v0}, Landroid/database/CrossProcessCursor;->moveToPrevious()Z

    move-result v0

    goto :goto_0
.end method

.method public onMove(II)Z
    .locals 1
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/gsf/talk/SuggestionFooterCursor;->mCursor:Landroid/database/CrossProcessCursor;

    invoke-interface {v0, p1, p2}, Landroid/database/CrossProcessCursor;->onMove(II)Z

    move-result v0

    return v0
.end method
