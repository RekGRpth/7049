.class public Lcom/google/android/gsf/update/SystemUpdateActivity;
.super Landroid/app/Activity;
.source "SystemUpdateActivity.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Landroid/view/View$OnClickListener;


# static fields
.field static volatile sIsActivityUp:Z


# instance fields
.field private mButtonQualifier:Landroid/widget/TextView;

.field private mCountdownEnd:J

.field private mCountdownStopTime:J

.field private mCountdownUrl:Ljava/lang/String;

.field private mDownloadPercent:I

.field private mDownloadUpdate:Ljava/lang/Runnable;

.field private mHandler:Landroid/os/Handler;

.field private mInstallPending:Z

.field private mLastBatteryState:I

.field private mLastCheckinTime:J

.field private mLastMobile:Z

.field private mLastRoaming:Z

.field private mLastStatus:I

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mScreenReceiver:Landroid/content/BroadcastReceiver;

.field private mSharedPrefs:Landroid/content/SharedPreferences;

.field private mVerifyUpdate:Ljava/lang/Runnable;

.field private mWatcher:Lcom/google/android/gsf/update/StateWatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/gsf/update/SystemUpdateActivity;->sIsActivityUp:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-boolean v2, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mLastMobile:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mDownloadPercent:I

    iput-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    iput-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mWatcher:Lcom/google/android/gsf/update/StateWatcher;

    iput-boolean v2, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mInstallPending:Z

    iput-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mCountdownUrl:Ljava/lang/String;

    new-instance v0, Lcom/google/android/gsf/update/SystemUpdateActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/gsf/update/SystemUpdateActivity$1;-><init>(Lcom/google/android/gsf/update/SystemUpdateActivity;)V

    iput-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mScreenReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/google/android/gsf/update/SystemUpdateActivity$3;

    invoke-direct {v0, p0}, Lcom/google/android/gsf/update/SystemUpdateActivity$3;-><init>(Lcom/google/android/gsf/update/SystemUpdateActivity;)V

    iput-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mVerifyUpdate:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/gsf/update/SystemUpdateActivity$4;

    invoke-direct {v0, p0}, Lcom/google/android/gsf/update/SystemUpdateActivity$4;-><init>(Lcom/google/android/gsf/update/SystemUpdateActivity;)V

    iput-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mDownloadUpdate:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/gsf/update/SystemUpdateActivity;)J
    .locals 2
    .param p0    # Lcom/google/android/gsf/update/SystemUpdateActivity;

    iget-wide v0, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mCountdownEnd:J

    return-wide v0
.end method

.method static synthetic access$100(Lcom/google/android/gsf/update/SystemUpdateActivity;)J
    .locals 2
    .param p0    # Lcom/google/android/gsf/update/SystemUpdateActivity;

    iget-wide v0, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mCountdownStopTime:J

    return-wide v0
.end method

.method static synthetic access$200(Lcom/google/android/gsf/update/SystemUpdateActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/gsf/update/SystemUpdateActivity;

    invoke-direct {p0}, Lcom/google/android/gsf/update/SystemUpdateActivity;->startUpdate()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/gsf/update/SystemUpdateActivity;Z)V
    .locals 0
    .param p0    # Lcom/google/android/gsf/update/SystemUpdateActivity;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/gsf/update/SystemUpdateActivity;->refreshStatus(Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/gsf/update/SystemUpdateActivity;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0    # Lcom/google/android/gsf/update/SystemUpdateActivity;

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/gsf/update/SystemUpdateActivity;)I
    .locals 1
    .param p0    # Lcom/google/android/gsf/update/SystemUpdateActivity;

    iget v0, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mLastStatus:I

    return v0
.end method

.method static synthetic access$600(Lcom/google/android/gsf/update/SystemUpdateActivity;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0    # Lcom/google/android/gsf/update/SystemUpdateActivity;

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mProgressBar:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/gsf/update/SystemUpdateActivity;)I
    .locals 1
    .param p0    # Lcom/google/android/gsf/update/SystemUpdateActivity;

    iget v0, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mDownloadPercent:I

    return v0
.end method

.method static synthetic access$702(Lcom/google/android/gsf/update/SystemUpdateActivity;I)I
    .locals 0
    .param p0    # Lcom/google/android/gsf/update/SystemUpdateActivity;
    .param p1    # I

    iput p1, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mDownloadPercent:I

    return p1
.end method

.method static synthetic access$800(Lcom/google/android/gsf/update/SystemUpdateActivity;JZ)V
    .locals 0
    .param p0    # Lcom/google/android/gsf/update/SystemUpdateActivity;
    .param p1    # J
    .param p3    # Z

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gsf/update/SystemUpdateActivity;->updateCountdownMessage(JZ)V

    return-void
.end method

.method private refreshStatus(Z)V
    .locals 15
    .param p1    # Z

    iget-object v10, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    const-string v11, "status"

    const/4 v12, 0x0

    invoke-interface {v10, v11, v12}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v7

    iget-object v10, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    const-string v11, "download_mobile"

    const/4 v12, 0x0

    invoke-interface {v10, v11, v12}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    iget-object v10, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mWatcher:Lcom/google/android/gsf/update/StateWatcher;

    invoke-virtual {v10}, Lcom/google/android/gsf/update/StateWatcher;->getBatteryState()I

    move-result v0

    iget-object v10, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mWatcher:Lcom/google/android/gsf/update/StateWatcher;

    invoke-virtual {v10}, Lcom/google/android/gsf/update/StateWatcher;->isNetworkRoaming()Z

    move-result v6

    invoke-static {p0}, Lcom/google/android/gsf/checkin/CheckinService;->getLastCheckinSuccessTime(Landroid/content/Context;)J

    move-result-wide v2

    if-nez p1, :cond_1

    iget v10, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mLastStatus:I

    if-ne v7, v10, :cond_1

    iget-boolean v10, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mLastMobile:Z

    if-ne v4, v10, :cond_1

    iget v10, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mLastBatteryState:I

    if-ne v0, v10, :cond_1

    iget-boolean v10, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mLastRoaming:Z

    if-ne v6, v10, :cond_1

    iget-wide v10, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mLastCheckinTime:J

    cmp-long v10, v2, v10

    if-nez v10, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput v7, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mLastStatus:I

    iput-boolean v4, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mLastMobile:Z

    iput v0, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mLastBatteryState:I

    iput-boolean v6, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mLastRoaming:Z

    iput-wide v2, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mLastCheckinTime:J

    const/4 v10, 0x5

    if-ne v7, v10, :cond_2

    iget-boolean v10, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mInstallPending:Z

    if-eqz v10, :cond_2

    const-string v10, "SystemUpdateActivity"

    const-string v11, "skipping refresh; about to reboot"

    invoke-static {v10, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const/4 v10, 0x2

    if-eq v7, v10, :cond_3

    const/4 v10, -0x1

    iput v10, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mDownloadPercent:I

    :cond_3
    invoke-direct {p0}, Lcom/google/android/gsf/update/SystemUpdateActivity;->resetContent()V

    const/4 v5, 0x0

    packed-switch v7, :pswitch_data_0

    :cond_4
    :goto_1
    if-nez v5, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gsf/update/SystemUpdateActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string v11, "update_description"

    invoke-static {v10, v11}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_0

    const v10, 0x7f0c0015

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v11

    const/4 v12, 0x1

    invoke-direct {p0, v10, v11, v12}, Lcom/google/android/gsf/update/SystemUpdateActivity;->setText(ILjava/lang/CharSequence;Z)V

    goto :goto_0

    :pswitch_0
    const v10, 0x7f0c000c

    const v11, 0x7f060013

    const/4 v12, 0x1

    invoke-direct {p0, v10, v11, v12}, Lcom/google/android/gsf/update/SystemUpdateActivity;->setText(IIZ)V

    const v10, 0x7f0c0015

    const v11, 0x7f060014

    invoke-virtual {p0, v11}, Lcom/google/android/gsf/update/SystemUpdateActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v11

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/CharSequence;

    const/4 v13, 0x0

    const/4 v14, 0x1

    invoke-static {p0, v2, v3, v14}, Landroid/text/format/DateUtils;->getRelativeTimeSpanString(Landroid/content/Context;JZ)Ljava/lang/CharSequence;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v11, v12}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v11

    const/4 v12, 0x1

    invoke-direct {p0, v10, v11, v12}, Lcom/google/android/gsf/update/SystemUpdateActivity;->setText(ILjava/lang/CharSequence;Z)V

    const v10, 0x7f0c000f

    const/16 v11, 0x8

    invoke-direct {p0, v10, v11}, Lcom/google/android/gsf/update/SystemUpdateActivity;->setVisibility(II)V

    const v10, 0x7f0c000e

    const/16 v11, 0x8

    invoke-direct {p0, v10, v11}, Lcom/google/android/gsf/update/SystemUpdateActivity;->setVisibility(II)V

    const v10, 0x7f0c0012

    const v11, 0x7f060012

    const/4 v12, 0x1

    invoke-direct {p0, v10, v11, v12}, Lcom/google/android/gsf/update/SystemUpdateActivity;->setText(IIZ)V

    const/4 v5, 0x1

    goto :goto_1

    :pswitch_1
    const v10, 0x7f0c000f

    const v11, 0x7f060007

    const/4 v12, 0x1

    invoke-direct {p0, v10, v11, v12}, Lcom/google/android/gsf/update/SystemUpdateActivity;->setText(IIZ)V

    if-eqz v6, :cond_5

    const v10, 0x7f0c0012

    const v11, 0x7f060010

    const/4 v12, 0x0

    invoke-direct {p0, v10, v11, v12}, Lcom/google/android/gsf/update/SystemUpdateActivity;->setText(IIZ)V

    const v10, 0x7f0c0012

    const/16 v11, 0x8

    invoke-direct {p0, v10, v11}, Lcom/google/android/gsf/update/SystemUpdateActivity;->setVisibility(II)V

    iget-object v10, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mButtonQualifier:Landroid/widget/TextView;

    const v11, 0x7f060006

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(I)V

    iget-object v10, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mButtonQualifier:Landroid/widget/TextView;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    :cond_5
    const v10, 0x7f0c0012

    const v11, 0x7f060010

    const/4 v12, 0x1

    invoke-direct {p0, v10, v11, v12}, Lcom/google/android/gsf/update/SystemUpdateActivity;->setText(IIZ)V

    goto/16 :goto_1

    :pswitch_2
    iget-object v10, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    invoke-static {v10, p0}, Lcom/google/android/gsf/update/SystemUpdateService;->whenMobileAllowed(Landroid/content/SharedPreferences;Landroid/content/Context;)J

    move-result-wide v8

    const v11, 0x7f0c000f

    iget v10, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mDownloadPercent:I

    if-gez v10, :cond_7

    const v10, 0x7f060008

    :goto_2
    const/4 v12, 0x1

    invoke-direct {p0, v11, v10, v12}, Lcom/google/android/gsf/update/SystemUpdateActivity;->setText(IIZ)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    cmp-long v10, v8, v10

    if-lez v10, :cond_6

    const v10, 0x7f0c0010

    const/4 v11, 0x0

    invoke-direct {p0, v10, v11}, Lcom/google/android/gsf/update/SystemUpdateActivity;->setVisibility(II)V

    const v10, 0x7f0c0010

    const v11, 0x7f06000a

    invoke-virtual {p0, v11}, Lcom/google/android/gsf/update/SystemUpdateActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v11

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/CharSequence;

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static {p0, v8, v9, v14}, Landroid/text/format/DateUtils;->getRelativeTimeSpanString(Landroid/content/Context;JZ)Ljava/lang/CharSequence;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v11, v12}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v11

    const/4 v12, 0x1

    invoke-direct {p0, v10, v11, v12}, Lcom/google/android/gsf/update/SystemUpdateActivity;->setText(ILjava/lang/CharSequence;Z)V

    :cond_6
    const v10, 0x7f0c000d

    const/4 v11, 0x0

    invoke-direct {p0, v10, v11}, Lcom/google/android/gsf/update/SystemUpdateActivity;->setVisibility(II)V

    const v10, 0x7f0c0012

    const v11, 0x7f060010

    const/4 v12, 0x0

    invoke-direct {p0, v10, v11, v12}, Lcom/google/android/gsf/update/SystemUpdateActivity;->setText(IIZ)V

    const v10, 0x7f0c0012

    const/16 v11, 0x8

    invoke-direct {p0, v10, v11}, Lcom/google/android/gsf/update/SystemUpdateActivity;->setVisibility(II)V

    iget-object v10, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mDownloadUpdate:Ljava/lang/Runnable;

    invoke-interface {v10}, Ljava/lang/Runnable;->run()V

    goto/16 :goto_1

    :cond_7
    const v10, 0x7f060009

    goto :goto_2

    :pswitch_3
    const v10, 0x7f0c000f

    const v11, 0x7f06000e

    const/4 v12, 0x1

    invoke-direct {p0, v10, v11, v12}, Lcom/google/android/gsf/update/SystemUpdateActivity;->setText(IIZ)V

    const v10, 0x7f0c0012

    const v11, 0x7f06000f

    const/4 v12, 0x1

    invoke-direct {p0, v10, v11, v12}, Lcom/google/android/gsf/update/SystemUpdateActivity;->setText(IIZ)V

    goto/16 :goto_1

    :pswitch_4
    const v10, 0x7f0c000d

    const/4 v11, 0x0

    invoke-direct {p0, v10, v11}, Lcom/google/android/gsf/update/SystemUpdateActivity;->setVisibility(II)V

    const v10, 0x7f0c000f

    const v11, 0x7f06000b

    const/4 v12, 0x1

    invoke-direct {p0, v10, v11, v12}, Lcom/google/android/gsf/update/SystemUpdateActivity;->setText(IIZ)V

    const v10, 0x7f0c0012

    const v11, 0x7f060010

    const/4 v12, 0x0

    invoke-direct {p0, v10, v11, v12}, Lcom/google/android/gsf/update/SystemUpdateActivity;->setText(IIZ)V

    const v10, 0x7f0c0012

    const/16 v11, 0x8

    invoke-direct {p0, v10, v11}, Lcom/google/android/gsf/update/SystemUpdateActivity;->setVisibility(II)V

    iget-object v10, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mVerifyUpdate:Ljava/lang/Runnable;

    invoke-interface {v10}, Ljava/lang/Runnable;->run()V

    goto/16 :goto_1

    :pswitch_5
    const v10, 0x7f0c000d

    const/4 v11, 0x4

    invoke-direct {p0, v10, v11}, Lcom/google/android/gsf/update/SystemUpdateActivity;->setVisibility(II)V

    const v10, 0x7f0c000f

    const v11, 0x7f06000c

    const/4 v12, 0x1

    invoke-direct {p0, v10, v11, v12}, Lcom/google/android/gsf/update/SystemUpdateActivity;->setText(IIZ)V

    const v10, 0x7f0c0012

    const v11, 0x7f060012

    const/4 v12, 0x1

    invoke-direct {p0, v10, v11, v12}, Lcom/google/android/gsf/update/SystemUpdateActivity;->setText(IIZ)V

    goto/16 :goto_1

    :pswitch_6
    iget-wide v10, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mCountdownEnd:J

    const-wide/16 v12, 0x0

    cmp-long v10, v10, v12

    if-lez v10, :cond_8

    iget-wide v10, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mCountdownEnd:J

    const/4 v12, 0x0

    invoke-direct {p0, v10, v11, v12}, Lcom/google/android/gsf/update/SystemUpdateActivity;->updateCountdownMessage(JZ)V

    goto/16 :goto_1

    :cond_8
    const v10, 0x7f0c000f

    const v11, 0x7f06000d

    const/4 v12, 0x1

    invoke-direct {p0, v10, v11, v12}, Lcom/google/android/gsf/update/SystemUpdateActivity;->setText(IIZ)V

    const v11, 0x7f0c0012

    const v12, 0x7f060011

    if-nez v0, :cond_a

    const/4 v10, 0x1

    :goto_3
    invoke-direct {p0, v11, v12, v10}, Lcom/google/android/gsf/update/SystemUpdateActivity;->setText(IIZ)V

    if-eqz v0, :cond_9

    const v10, 0x7f0c0012

    const/16 v11, 0x8

    invoke-direct {p0, v10, v11}, Lcom/google/android/gsf/update/SystemUpdateActivity;->setVisibility(II)V

    :cond_9
    const/4 v10, 0x2

    if-ne v0, v10, :cond_b

    iget-object v10, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mButtonQualifier:Landroid/widget/TextView;

    const v11, 0x7f060004

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(I)V

    iget-object v10, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mButtonQualifier:Landroid/widget/TextView;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    :cond_a
    const/4 v10, 0x0

    goto :goto_3

    :cond_b
    const/4 v10, 0x1

    if-ne v0, v10, :cond_4

    iget-object v10, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mButtonQualifier:Landroid/widget/TextView;

    const v11, 0x7f060005

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(I)V

    iget-object v10, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mButtonQualifier:Landroid/widget/TextView;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_6
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_3
    .end packed-switch
.end method

.method private resetContent()V
    .locals 7

    const v6, 0x7f0c000e

    const/16 v5, 0x8

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mProgressBar:Landroid/widget/ProgressBar;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mButtonQualifier:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    const v0, 0x7f0c000c

    invoke-virtual {p0}, Lcom/google/android/gsf/update/SystemUpdateActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "update_title"

    invoke-static {v1, v2}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1, v4}, Lcom/google/android/gsf/update/SystemUpdateActivity;->setText(ILjava/lang/CharSequence;Z)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " \u00b7 "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gsf/update/SystemUpdateActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "update_size"

    invoke-static {v1, v2}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v6, v0, v4}, Lcom/google/android/gsf/update/SystemUpdateActivity;->setText(ILjava/lang/CharSequence;Z)V

    const v0, 0x7f0c000f

    invoke-direct {p0, v0, v3}, Lcom/google/android/gsf/update/SystemUpdateActivity;->setVisibility(II)V

    const v0, 0x7f0c0010

    invoke-direct {p0, v0, v5}, Lcom/google/android/gsf/update/SystemUpdateActivity;->setVisibility(II)V

    invoke-direct {p0, v6, v3}, Lcom/google/android/gsf/update/SystemUpdateActivity;->setVisibility(II)V

    const v0, 0x7f0c0012

    invoke-direct {p0, v0, v3}, Lcom/google/android/gsf/update/SystemUpdateActivity;->setVisibility(II)V

    return-void
.end method

.method private resumeCountdown()V
    .locals 3

    const/4 v2, 0x1

    const v0, 0x7f0c0012

    const v1, 0x7f060029

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gsf/update/SystemUpdateActivity;->setText(IIZ)V

    iget-wide v0, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mCountdownEnd:J

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gsf/update/SystemUpdateActivity;->updateCountdownMessage(JZ)V

    return-void
.end method

.method private setText(IIZ)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Z

    invoke-virtual {p0, p2}, Lcom/google/android/gsf/update/SystemUpdateActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {p0, p1, v0, p3}, Lcom/google/android/gsf/update/SystemUpdateActivity;->setText(ILjava/lang/CharSequence;Z)V

    return-void
.end method

.method private setText(ILjava/lang/CharSequence;Z)V
    .locals 2
    .param p1    # I
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # Z

    invoke-virtual {p0, p1}, Lcom/google/android/gsf/update/SystemUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    instance-of v1, v0, Landroid/widget/TextView;

    if-eqz v1, :cond_1

    move-object v1, v0

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    invoke-virtual {v0, p3}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0
.end method

.method private setVisibility(II)V
    .locals 1
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0, p1}, Lcom/google/android/gsf/update/SystemUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private startCountdown()V
    .locals 4

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.server.checkin.CHECKIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/update/SystemUpdateActivity;->sendBroadcast(Landroid/content/Intent;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x2710

    add-long/2addr v0, v2

    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mCountdownEnd:J

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    const-string v1, "url"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mCountdownUrl:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/gsf/update/SystemUpdateActivity;->resumeCountdown()V

    return-void
.end method

.method private startUpdate()V
    .locals 3

    const/4 v2, 0x1

    const v0, 0x7f0c000f

    const v1, 0x7f060028

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gsf/update/SystemUpdateActivity;->setText(IIZ)V

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "install_approved"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/gsf/update/SystemUpdateActivity;->sIsActivityUp:Z

    iput-boolean v2, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mInstallPending:Z

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/update/SystemUpdateActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method private stopCountdown()V
    .locals 4

    const-wide/16 v2, 0x0

    iget-wide v0, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mCountdownEnd:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const v0, 0x3112a

    const-string v1, "activity-countdown-cancel"

    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    iput-wide v2, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mCountdownEnd:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mCountdownStopTime:J

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mCountdownUrl:Ljava/lang/String;

    return-void
.end method

.method private updateCountdownMessage(JZ)V
    .locals 12
    .param p1    # J
    .param p3    # Z

    const/4 v11, 0x1

    iget-wide v5, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mCountdownEnd:J

    cmp-long v5, p1, v5

    if-eqz v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v5, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    const-string v6, "url"

    const/4 v7, 0x0

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    iget-object v5, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mCountdownUrl:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    :cond_2
    const-string v5, "SystemUpdateActivity"

    const-string v6, "URL changed during countdown; aborting"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/gsf/update/SystemUpdateActivity;->stopCountdown()V

    invoke-direct {p0, v11}, Lcom/google/android/gsf/update/SystemUpdateActivity;->refreshStatus(Z)V

    goto :goto_0

    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v5, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mCountdownEnd:J

    cmp-long v5, v1, v5

    if-ltz v5, :cond_4

    invoke-direct {p0}, Lcom/google/android/gsf/update/SystemUpdateActivity;->startUpdate()V

    goto :goto_0

    :cond_4
    iget-wide v5, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mCountdownEnd:J

    sub-long/2addr v5, v1

    const-wide/16 v7, 0x3e8

    div-long/2addr v5, v7

    long-to-int v5, v5

    add-int/lit8 v3, v5, 0x1

    const v5, 0x7f0c000f

    invoke-virtual {p0}, Lcom/google/android/gsf/update/SystemUpdateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const/high16 v7, 0x7f080000

    new-array v8, v11, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v3, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v5, v6, v11}, Lcom/google/android/gsf/update/SystemUpdateActivity;->setText(ILjava/lang/CharSequence;Z)V

    iget-wide v5, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mCountdownEnd:J

    add-int/lit8 v7, v3, -0x1

    mul-int/lit16 v7, v7, 0x3e8

    int-to-long v7, v7

    sub-long/2addr v5, v7

    sub-long/2addr v5, v1

    long-to-int v0, v5

    if-eqz p3, :cond_0

    iget-object v5, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mHandler:Landroid/os/Handler;

    new-instance v6, Lcom/google/android/gsf/update/SystemUpdateActivity$6;

    invoke-direct {v6, p0, p1, p2, p3}, Lcom/google/android/gsf/update/SystemUpdateActivity$6;-><init>(Lcom/google/android/gsf/update/SystemUpdateActivity;JZ)V

    int-to-long v7, v0

    invoke-virtual {v5, v6, v7, v8}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    const-wide/16 v5, 0x0

    const v2, 0x3112a

    const/4 v4, 0x1

    iget v0, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mLastStatus:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.server.checkin.CHECKIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/update/SystemUpdateActivity;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_2
    const-string v0, "download"

    invoke-static {v2, v0}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    const-string v1, "url"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "download_approved"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "rejected_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "download_now"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/update/SystemUpdateActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    :pswitch_3
    iget-wide v0, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mCountdownEnd:J

    cmp-long v0, v0, v5

    if-lez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gsf/update/SystemUpdateActivity;->stopCountdown()V

    iput-wide v5, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mCountdownStopTime:J

    invoke-direct {p0, v4}, Lcom/google/android/gsf/update/SystemUpdateActivity;->refreshStatus(Z)V

    goto :goto_0

    :cond_0
    const-string v0, "install"

    invoke-static {v2, v0}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/gsf/update/SystemUpdateActivity;->startCountdown()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const/4 v5, 0x0

    const-wide/16 v3, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mHandler:Landroid/os/Handler;

    const-string v1, "update"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gsf/update/SystemUpdateActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    const v1, 0x7f060003

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/update/SystemUpdateActivity;->setTitle(I)V

    const v1, 0x7f030002

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/update/SystemUpdateActivity;->setContentView(I)V

    const v1, 0x7f0c0012

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/update/SystemUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0c0013

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/update/SystemUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mButtonQualifier:Landroid/widget/TextView;

    const v1, 0x7f0c000d

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/update/SystemUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mProgressBar:Landroid/widget/ProgressBar;

    const v1, 0x7f0c0015

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/update/SystemUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    new-instance v1, Lcom/google/android/gsf/update/StateWatcher;

    new-instance v2, Lcom/google/android/gsf/update/SystemUpdateActivity$2;

    invoke-direct {v2, p0}, Lcom/google/android/gsf/update/SystemUpdateActivity$2;-><init>(Lcom/google/android/gsf/update/SystemUpdateActivity;)V

    invoke-direct {v1, p0, v2}, Lcom/google/android/gsf/update/StateWatcher;-><init>(Landroid/content/Context;Ljava/lang/Runnable;)V

    iput-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mWatcher:Lcom/google/android/gsf/update/StateWatcher;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mScreenReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gsf/update/SystemUpdateActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    if-eqz p1, :cond_0

    const-string v1, "countdown_end"

    invoke-virtual {p1, v1, v3, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mCountdownEnd:J

    iput-wide v3, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mCountdownStopTime:J

    const-string v1, "countdown_url"

    invoke-virtual {p1, v1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mCountdownUrl:Ljava/lang/String;

    :goto_0
    return-void

    :cond_0
    iput-wide v3, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mCountdownEnd:J

    iput-wide v3, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mCountdownStopTime:J

    iput-object v5, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mCountdownUrl:Ljava/lang/String;

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mScreenReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/update/SystemUpdateActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1    # Landroid/content/Intent;

    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    invoke-virtual {p0, p1}, Lcom/google/android/gsf/update/SystemUpdateActivity;->setIntent(Landroid/content/Intent;)V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "countdown_end"

    iget-wide v1, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mCountdownEnd:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v0, "countdown_url"

    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mCountdownUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/content/SharedPreferences;
    .param p2    # Ljava/lang/String;

    const-string v0, "status"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "download_mobile"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    new-instance v0, Lcom/google/android/gsf/update/SystemUpdateActivity$5;

    invoke-direct {v0, p0}, Lcom/google/android/gsf/update/SystemUpdateActivity$5;-><init>(Lcom/google/android/gsf/update/SystemUpdateActivity;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/update/SystemUpdateActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v0, "verify_progress"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mVerifyUpdate:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/update/SystemUpdateActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_3
    const-string v0, "download_progress"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mDownloadUpdate:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/update/SystemUpdateActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method protected onStart()V
    .locals 4

    const/4 v1, 0x1

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    sput-boolean v1, Lcom/google/android/gsf/update/SystemUpdateActivity;->sIsActivityUp:Z

    invoke-static {p0}, Lcom/google/android/gsf/update/SystemUpdateService;->cancelNotifications(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mWatcher:Lcom/google/android/gsf/update/StateWatcher;

    invoke-virtual {v0}, Lcom/google/android/gsf/update/StateWatcher;->start()V

    invoke-direct {p0, v1}, Lcom/google/android/gsf/update/SystemUpdateActivity;->refreshStatus(Z)V

    iget-wide v0, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mCountdownEnd:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const v0, 0x3112a

    const-string v1, "activity-countdown-resume"

    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/gsf/update/SystemUpdateActivity;->resumeCountdown()V

    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    invoke-direct {p0}, Lcom/google/android/gsf/update/SystemUpdateActivity;->stopCountdown()V

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mWatcher:Lcom/google/android/gsf/update/StateWatcher;

    invoke-virtual {v0}, Lcom/google/android/gsf/update/StateWatcher;->stop()V

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/gsf/update/SystemUpdateActivity;->sIsActivityUp:Z

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/update/SystemUpdateActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method
