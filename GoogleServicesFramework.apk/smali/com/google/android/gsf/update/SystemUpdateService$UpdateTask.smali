.class Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;
.super Landroid/os/AsyncTask;
.source "SystemUpdateService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gsf/update/SystemUpdateService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UpdateTask"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/content/Intent;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private mLastDownloadBytes:J

.field private mLastDownloadStatus:I

.field private mProvisioned:Z

.field final synthetic this$0:Lcom/google/android/gsf/update/SystemUpdateService;


# direct methods
.method private constructor <init>(Lcom/google/android/gsf/update/SystemUpdateService;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->mProvisioned:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gsf/update/SystemUpdateService;Lcom/google/android/gsf/update/SystemUpdateService$1;)V
    .locals 0
    .param p1    # Lcom/google/android/gsf/update/SystemUpdateService;
    .param p2    # Lcom/google/android/gsf/update/SystemUpdateService$1;

    invoke-direct {p0, p1}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;-><init>(Lcom/google/android/gsf/update/SystemUpdateService;)V

    return-void
.end method

.method private cancelUpdate()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    const-string v2, "alarm"

    invoke-virtual {v1, v2}, Lcom/google/android/gsf/update/SystemUpdateService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mPending:Landroid/app/PendingIntent;
    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1500(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mVerifierLock:Ljava/lang/Object;
    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1200(Lcom/google/android/gsf/update/SystemUpdateService;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mVerifier:Lcom/google/android/gsf/update/SystemUpdateVerifierTask;
    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1300(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/google/android/gsf/update/SystemUpdateVerifierTask;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v1, "SystemUpdateService"

    const-string v3, "cancelUpdate: cancelling verifier"

    invoke-static {v1, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mVerifier:Lcom/google/android/gsf/update/SystemUpdateVerifierTask;
    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1300(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/google/android/gsf/update/SystemUpdateVerifierTask;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lcom/google/android/gsf/update/SystemUpdateVerifierTask;->cancel(Z)Z

    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    const/4 v3, 0x0

    # setter for: Lcom/google/android/gsf/update/SystemUpdateService;->mVerifier:Lcom/google/android/gsf/update/SystemUpdateVerifierTask;
    invoke-static {v1, v3}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1302(Lcom/google/android/gsf/update/SystemUpdateService;Lcom/google/android/gsf/update/SystemUpdateVerifierTask;)Lcom/google/android/gsf/update/SystemUpdateVerifierTask;

    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mDownload:Lcom/google/android/gsf/update/Download;
    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1400(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/google/android/gsf/update/Download;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gsf/update/Download;->removeAllDownloads()V

    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "download_approved"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "install_approved"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "url"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "url_change"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "download_mobile"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "filename"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "download_id"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "verified"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "next_dialog"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "notified"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "install_time"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "started_download"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "pending_filename"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "success_message"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "failure_message"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "verify_progress"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "status"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateInstallDialog;->cancel(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateDownloadDialog;->cancel(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->cancelNotifications(Landroid/content/Context;)V

    return-void

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private checkDownload(Ljava/lang/String;I)Z
    .locals 10

    invoke-direct {p0, p2}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->isAutomaticallyDownloaded(I)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v3, 0x1

    :goto_0
    invoke-direct {p0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->isMobileDownloadAllowedNow()Z

    move-result v5

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mDownload:Lcom/google/android/gsf/update/Download;
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1400(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/google/android/gsf/update/Download;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gsf/update/Download;->getStatus()Lcom/google/android/gsf/update/Download$StatusInfo;

    move-result-object v0

    if-nez v0, :cond_6

    const-string v0, "SystemUpdateService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "starting download of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v0, 0x0

    iget-object v2, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v2}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    if-eqz v5, :cond_4

    iget-object v4, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-virtual {v4}, Lcom/google/android/gsf/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v6, "update_download_watchdog_sec"

    const v7, 0x15180

    invoke-static {v4, v6, v7}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    if-lez v4, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    mul-int/lit16 v4, v4, 0x3e8

    int-to-long v6, v4

    add-long/2addr v0, v6

    const-string v4, "watchdog_deadline"

    invoke-interface {v2, v4, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    :cond_0
    :goto_1
    const-string v4, "download_mobile"

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    if-nez v5, :cond_1

    iget-object v4, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v4}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v4

    iget-object v6, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-static {v4, v6}, Lcom/google/android/gsf/update/SystemUpdateService;->whenMobileAllowed(Landroid/content/SharedPreferences;Landroid/content/Context;)J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v4, v6, v8

    if-lez v4, :cond_1

    cmp-long v4, v6, v0

    if-gez v4, :cond_1

    move-wide v0, v6

    :cond_1
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    const-wide/16 v6, 0x0

    cmp-long v2, v0, v6

    if-lez v2, :cond_2

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->retryAt(JZ)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-virtual {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "update_token"

    invoke-static {v0, v1}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mDownload:Lcom/google/android/gsf/update/Download;
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1400(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/google/android/gsf/update/Download;

    move-result-object v0

    invoke-direct {p0, p2}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->getAllowRoaming(I)Z

    move-result v4

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gsf/update/Download;->downloadUpdate(Ljava/lang/String;Ljava/lang/String;ZZZ)Z

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mDownload:Lcom/google/android/gsf/update/Download;
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1400(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/google/android/gsf/update/Download;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gsf/update/Download;->getStatus()Lcom/google/android/gsf/update/Download$StatusInfo;

    move-result-object v0

    if-nez v0, :cond_5

    const-string v0, "SystemUpdateService"

    const-string v1, "just-started download disappeared"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->setStatus(I)V

    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->downloadFailed(II)V

    const/4 v0, 0x0

    :goto_2
    return v0

    :cond_3
    const/4 v3, 0x0

    goto/16 :goto_0

    :cond_4
    const-string v4, "watchdog_deadline"

    invoke-interface {v2, v4}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_1

    :cond_5
    iget v1, v0, Lcom/google/android/gsf/update/Download$StatusInfo;->reason:I

    iput v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->mLastDownloadStatus:I

    iget-wide v1, v0, Lcom/google/android/gsf/update/Download$StatusInfo;->bytesSoFar:J

    iput-wide v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->mLastDownloadBytes:J

    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "status"

    const/4 v3, 0x2

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "download_id"

    iget-wide v3, v0, Lcom/google/android/gsf/update/Download$StatusInfo;->id:J

    invoke-interface {v1, v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "verified"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    const/4 v0, 0x1

    goto :goto_2

    :cond_6
    const-string v1, "SystemUpdateService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "download in progress: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gsf/update/Download$StatusInfo;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v1, v0, Lcom/google/android/gsf/update/Download$StatusInfo;->reason:I

    iput v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->mLastDownloadStatus:I

    iget-wide v1, v0, Lcom/google/android/gsf/update/Download$StatusInfo;->bytesSoFar:J

    iput-wide v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->mLastDownloadBytes:J

    iget-boolean v1, v0, Lcom/google/android/gsf/update/Download$StatusInfo;->completed:Z

    if-nez v1, :cond_9

    if-eqz v5, :cond_7

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "download_mobile"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "SystemUpdateService"

    const-string v1, "non-mobile download took too long; will retry allowing mobile"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mDownload:Lcom/google/android/gsf/update/Download;
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1400(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/google/android/gsf/update/Download;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gsf/update/Download;->removeAllDownloads()V

    invoke-direct {p0, p1, p2}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->checkDownload(Ljava/lang/String;I)Z

    move-result v0

    goto/16 :goto_2

    :cond_7
    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "watchdog_deadline"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v0, v2, v0

    if-lez v0, :cond_8

    const-string v0, "SystemUpdateService"

    const-string v1, "download took too long; will cancel and retry"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->setStatus(I)V

    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->downloadFailed(II)V

    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_8
    const/4 v0, 0x1

    goto/16 :goto_2

    :cond_9
    iget-boolean v1, v0, Lcom/google/android/gsf/update/Download$StatusInfo;->successful:Z

    if-nez v1, :cond_a

    const-string v1, "SystemUpdateService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "download failed (reason = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v0, v0, Lcom/google/android/gsf/update/Download$StatusInfo;->reason:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->downloadFailed(II)V

    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_a
    iget-object v1, v0, Lcom/google/android/gsf/update/Download$StatusInfo;->filename:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_b

    const-string v0, "SystemUpdateService"

    const-string v1, "download completed but no filename available"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->downloadFailed(II)V

    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_b
    iget-wide v1, v0, Lcom/google/android/gsf/update/Download$StatusInfo;->id:J

    iget-object v4, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v4}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v5, "download_id"

    const-wide/16 v6, -0x1

    invoke-interface {v4, v5, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    cmp-long v1, v1, v4

    if-nez v1, :cond_c

    iget-object v1, v0, Lcom/google/android/gsf/update/Download$StatusInfo;->filename:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v2}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v4, "filename"

    const/4 v5, 0x0

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    :cond_c
    const-string v1, "SystemUpdateService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "download id now "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, v0, Lcom/google/android/gsf/update/Download$StatusInfo;->id:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "; filename now "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, v0, Lcom/google/android/gsf/update/Download$StatusInfo;->filename:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "filename"

    iget-object v4, v0, Lcom/google/android/gsf/update/Download$StatusInfo;->filename:Ljava/lang/String;

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "download_id"

    iget-wide v4, v0, Lcom/google/android/gsf/update/Download$StatusInfo;->id:J

    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "verified"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mVerifierLock:Ljava/lang/Object;
    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1200(Lcom/google/android/gsf/update/SystemUpdateService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mVerifier:Lcom/google/android/gsf/update/SystemUpdateVerifierTask;
    invoke-static {v2}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1300(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/google/android/gsf/update/SystemUpdateVerifierTask;

    move-result-object v2

    if-eqz v2, :cond_d

    const-string v2, "SystemUpdateService"

    const-string v4, "checkDownload: cancelling verifier"

    invoke-static {v2, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mVerifier:Lcom/google/android/gsf/update/SystemUpdateVerifierTask;
    invoke-static {v2}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1300(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/google/android/gsf/update/SystemUpdateVerifierTask;

    move-result-object v2

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Lcom/google/android/gsf/update/SystemUpdateVerifierTask;->cancel(Z)Z

    iget-object v2, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    const/4 v4, 0x0

    # setter for: Lcom/google/android/gsf/update/SystemUpdateService;->mVerifier:Lcom/google/android/gsf/update/SystemUpdateVerifierTask;
    invoke-static {v2, v4}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1302(Lcom/google/android/gsf/update/SystemUpdateService;Lcom/google/android/gsf/update/SystemUpdateVerifierTask;)Lcom/google/android/gsf/update/SystemUpdateVerifierTask;

    :cond_d
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_e
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-object v4, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v4}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v5, "verified"

    invoke-interface {v4, v5}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_14

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mVerifierLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1200(Lcom/google/android/gsf/update/SystemUpdateService;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    :try_start_1
    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    const/4 v4, 0x0

    # setter for: Lcom/google/android/gsf/update/SystemUpdateService;->mVerifier:Lcom/google/android/gsf/update/SystemUpdateVerifierTask;
    invoke-static {v0, v4}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1302(Lcom/google/android/gsf/update/SystemUpdateService;Lcom/google/android/gsf/update/SystemUpdateVerifierTask;)Lcom/google/android/gsf/update/SystemUpdateVerifierTask;

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v3, "verified"

    const/4 v4, 0x0

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_13

    const-string v0, "SystemUpdateService"

    const-string v3, "file has been verified"

    invoke-static {v0, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->checkFile()Ljava/lang/String;

    invoke-direct {p0, p2, v1, v2}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->isOkToInstallNow(IJ)Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-direct {p0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->install()V

    const/4 v0, 0x1

    goto/16 :goto_2

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_f
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->setStatus(I)V

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mDownloadRetry:Lcom/android/common/OperationScheduler;
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/android/common/OperationScheduler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/common/OperationScheduler;->onSuccess()V

    const/4 v0, 0x6

    if-ne p2, v0, :cond_11

    invoke-direct {p0, v1, v2}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->msUntilMaintenanceWindow(J)J

    move-result-wide v3

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->sUpdatesLockedUntil:J
    invoke-static {}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1100()J

    move-result-wide v5

    sub-long/2addr v5, v1

    invoke-static {v3, v4, v5, v6}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v3

    const-wide v5, 0x7fffffffffffffffL

    cmp-long v0, v3, v5

    if-eqz v0, :cond_10

    add-long v0, v1, v3

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->retryAt(JZ)V

    :cond_10
    :goto_3
    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_11
    sget-boolean v0, Lcom/google/android/gsf/update/SystemUpdateActivity;->sIsActivityUp:Z

    if-nez v0, :cond_10

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "next_dialog"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->sUpdatesLockedUntil:J
    invoke-static {}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1100()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v2, v2, v0

    if-gez v2, :cond_12

    const v2, 0x7f060015

    const v3, 0x7f060016

    const v4, 0x7f020007

    invoke-direct {p0, v2, v3, v4}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->notifyWithActivity(III)V

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->retryAt(JZ)V

    goto :goto_3

    :cond_12
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    const-class v2, Lcom/google/android/gsf/update/SystemUpdateInstallDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x10040000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-virtual {v1, v0}, Lcom/google/android/gsf/update/SystemUpdateService;->startActivity(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-virtual {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "update_install_reschedule_sec"

    const/16 v2, 0x708

    invoke-static {v0, v1, v2}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    int-to-long v3, v0

    const-wide/16 v5, 0x3e8

    mul-long/2addr v3, v5

    add-long v0, v1, v3

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->retryAt(JZ)V

    goto :goto_3

    :cond_13
    const-string v0, "SystemUpdateService"

    const-string v1, "verification of system update package failed"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-virtual {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "update_verify_redownload_delay_sec"

    const v2, 0xa8c0

    invoke-static {v0, v1, v2}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    invoke-direct {p0, p2, v0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->downloadFailed(II)V

    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->setStatus(I)V

    goto/16 :goto_3

    :cond_14
    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mVerifierLock:Ljava/lang/Object;
    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1200(Lcom/google/android/gsf/update/SystemUpdateService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_4
    invoke-direct {p0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->checkFile()Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mVerifier:Lcom/google/android/gsf/update/SystemUpdateVerifierTask;
    invoke-static {v2}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1300(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/google/android/gsf/update/SystemUpdateVerifierTask;

    move-result-object v2

    if-eqz v2, :cond_15

    const-string v0, "SystemUpdateService"

    const-string v2, "verification already in progress"

    invoke-static {v0, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    monitor-exit v1

    goto/16 :goto_2

    :catchall_2
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v0

    :cond_15
    :try_start_5
    const-string v2, "SystemUpdateService"

    const-string v4, "starting package verification"

    invoke-static {v2, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x3

    invoke-direct {p0, v2}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->setStatus(I)V

    iget-object v2, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    new-instance v4, Lcom/google/android/gsf/update/SystemUpdateVerifierTask;

    iget-object v5, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    new-instance v6, Ljava/io/File;

    iget-object v0, v0, Lcom/google/android/gsf/update/Download$StatusInfo;->filename:Ljava/lang/String;

    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-direct {v4, v5, v6, v0, v3}, Lcom/google/android/gsf/update/SystemUpdateVerifierTask;-><init>(Landroid/content/Context;Ljava/io/File;Landroid/content/SharedPreferences;Z)V

    # setter for: Lcom/google/android/gsf/update/SystemUpdateService;->mVerifier:Lcom/google/android/gsf/update/SystemUpdateVerifierTask;
    invoke-static {v2, v4}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1302(Lcom/google/android/gsf/update/SystemUpdateService;Lcom/google/android/gsf/update/SystemUpdateVerifierTask;)Lcom/google/android/gsf/update/SystemUpdateVerifierTask;

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mVerifier:Lcom/google/android/gsf/update/SystemUpdateVerifierTask;
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1300(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/google/android/gsf/update/SystemUpdateVerifierTask;

    move-result-object v0

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->EXECUTOR:Ljava/util/concurrent/Executor;
    invoke-static {}, Lcom/google/android/gsf/update/SystemUpdateService;->access$600()Ljava/util/concurrent/Executor;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gsf/update/SystemUpdateVerifierTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    const/4 v0, 0x1

    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    goto/16 :goto_2
.end method

.method private checkFile()Ljava/lang/String;
    .locals 7

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v5}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v5

    const-string v6, "filename"

    invoke-interface {v5, v6, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "SystemUpdateService"

    const-string v6, "OTA package filename empty"

    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v4

    :goto_0
    return-object v1

    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, "SystemUpdateService"

    const-string v6, "OTA package doesn\'t exist!"

    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v4

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    const-string v4, "SystemUpdateService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "OTA package size = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private checkPostInstallIntents()V
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "pending_filename"

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/FileReader;

    const-string v3, "/cache/recovery/last_install"

    invoke-direct {v2, v3}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    const-string v3, "1"

    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v2, :cond_2

    const-string v0, "SystemUpdateService"

    const-string v1, "can\'t determine last-installed OTA package name"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "SystemUpdateService"

    const-string v2, "failed to read last_install"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_2
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "SystemUpdateService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "first boot since "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " install of "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v3, :cond_4

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "success_message"

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    const-class v4, Lcom/google/android/gsf/update/CompleteDialog;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v3, 0x10040000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "message"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gsf/update/SystemUpdateService;->startActivity(Landroid/content/Intent;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "pending_filename"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "failure_message"

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private checkUpdateState(Landroid/content/Intent;)Z
    .locals 12

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "provisioned"

    const-wide/16 v4, 0x0

    invoke-interface {v0, v1, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-eqz v4, :cond_0

    cmp-long v4, v0, v2

    if-lez v4, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-virtual {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "device_provisioned"

    const/4 v4, 0x0

    invoke-static {v0, v1, v4}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_3

    const-wide/16 v0, 0x0

    :goto_0
    iget-object v4, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v4}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "provisioned"

    invoke-interface {v4, v5, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_1
    move-wide v4, v0

    const-wide/16 v0, 0x0

    cmp-long v0, v4, v0

    if-lez v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->mProvisioned:Z

    if-eqz p1, :cond_2

    const-string v0, "boot"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->checkPostInstallIntents()V

    :cond_2
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->mLastDownloadStatus:I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->mLastDownloadBytes:J

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-virtual {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "update_url"

    invoke-static {v0, v1}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "SystemUpdateService"

    const-string v1, "cancelUpdate (empty URL)"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->cancelUpdate()V

    invoke-virtual {p0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->logState()V

    const/4 v0, 0x0

    :goto_2
    return v0

    :cond_3
    move-wide v0, v2

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    if-eqz p1, :cond_6

    const-string v0, "download_now"

    const/4 v6, 0x0

    invoke-virtual {p1, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mDownloadRetry:Lcom/android/common/OperationScheduler;
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/android/common/OperationScheduler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/common/OperationScheduler;->resetTransientError()V

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mDownloadRetry:Lcom/android/common/OperationScheduler;
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/android/common/OperationScheduler;

    move-result-object v0

    const-wide/16 v6, 0x0

    invoke-virtual {v0, v6, v7}, Lcom/android/common/OperationScheduler;->setTriggerTimeMillis(J)V

    :cond_6
    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v6, "status"

    const/4 v7, -0x1

    invoke-interface {v0, v6, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    const/4 v6, 0x5

    if-ne v0, v6, :cond_8

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v6, "url"

    const/4 v7, 0x0

    invoke-interface {v0, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v6, "install_time"

    const-wide/16 v7, 0x0

    invoke-interface {v0, v6, v7, v8}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-virtual {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v8, "update_retry_delay_sec"

    const v9, 0x3f480

    invoke-static {v0, v8, v9}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    int-to-long v8, v0

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    add-long/2addr v6, v8

    cmp-long v0, v2, v6

    if-gtz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v6, "install_time"

    const-wide/16 v7, 0x0

    invoke-interface {v0, v6, v7, v8}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    const-wide/32 v8, 0x36ee80

    sub-long/2addr v6, v8

    cmp-long v0, v2, v6

    if-gez v0, :cond_8

    :cond_7
    const-string v0, "SystemUpdateService"

    const-string v6, "cancelUpdate (willing to retry now)"

    invoke-static {v0, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->cancelUpdate()V

    :cond_8
    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-virtual {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v6, "update_urgency"

    const/4 v7, 0x3

    invoke-static {v0, v6, v7}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v7, "url"

    const/4 v8, 0x0

    invoke-interface {v0, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    const-string v0, "SystemUpdateService"

    const-string v7, "cancelUpdate (update URL has changed)"

    invoke-static {v0, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->cancelUpdate()V

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v7, "url"

    invoke-interface {v0, v7, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    const-string v8, "status"

    const/4 v9, 0x1

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    const-string v8, "url_change"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    invoke-interface {v7, v8, v9, v10}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    packed-switch v6, :pswitch_data_0

    :goto_3
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mDownloadRetry:Lcom/android/common/OperationScheduler;
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/android/common/OperationScheduler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/common/OperationScheduler;->resetTransientError()V

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mDownloadRetry:Lcom/android/common/OperationScheduler;
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/android/common/OperationScheduler;

    move-result-object v0

    const-wide/16 v7, 0x0

    invoke-virtual {v0, v7, v8}, Lcom/android/common/OperationScheduler;->setTriggerTimeMillis(J)V

    :cond_9
    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-virtual {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v7, "update_provisioning_delay_sec"

    const/4 v8, 0x0

    invoke-static {v0, v7, v8}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ltz v0, :cond_c

    const-wide/16 v7, 0x0

    cmp-long v7, v4, v7

    if-eqz v7, :cond_a

    mul-int/lit16 v7, v0, 0x3e8

    int-to-long v7, v7

    add-long/2addr v7, v4

    cmp-long v7, v2, v7

    if-gez v7, :cond_c

    :cond_a
    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-nez v1, :cond_b

    const/16 v1, 0x384

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    add-long/2addr v0, v2

    :goto_4
    const-string v2, "SystemUpdateService"

    const-string v3, "cancelUpdate (not provisioned)"

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->cancelUpdate()V

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->retryAt(JZ)V

    invoke-virtual {p0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->logState()V

    const/4 v0, 0x0

    goto/16 :goto_2

    :pswitch_0
    const-string v7, "download_approved"

    invoke-interface {v0, v7}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    const-string v8, "install_approved"

    invoke-interface {v7, v8}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_3

    :pswitch_1
    const-string v7, "download_approved"

    const/4 v8, 0x1

    invoke-interface {v0, v7, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    const-string v8, "install_approved"

    invoke-interface {v7, v8}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_3

    :pswitch_2
    const-string v7, "download_approved"

    const/4 v8, 0x1

    invoke-interface {v0, v7, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    const-string v8, "install_approved"

    const/4 v9, 0x1

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto :goto_3

    :cond_b
    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    add-long/2addr v0, v4

    goto :goto_4

    :cond_c
    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v4, "status"

    const/4 v5, -0x1

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    const/4 v4, 0x6

    if-ne v0, v4, :cond_d

    invoke-direct {p0, v6}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->isAutomaticallyDownloaded(I)Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mDownloadRetry:Lcom/android/common/OperationScheduler;
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/android/common/OperationScheduler;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mDownloadRetryOptions:Lcom/android/common/OperationScheduler$Options;
    invoke-static {v4}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1000(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/android/common/OperationScheduler$Options;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/android/common/OperationScheduler;->getNextTimeMillis(Lcom/android/common/OperationScheduler$Options;)J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-gez v0, :cond_d

    invoke-virtual {p0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->logState()V

    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_d
    const/4 v0, 0x0

    iget-object v4, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v4}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v5, "status"

    const/4 v7, -0x1

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    const/4 v5, 0x5

    if-eq v4, v5, :cond_e

    iget-object v4, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v4}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v5, "download_approved"

    const/4 v7, 0x0

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_10

    invoke-direct {p0, v1, v6}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->checkDownload(Ljava/lang/String;I)Z

    move-result v0

    :cond_e
    :goto_5
    invoke-virtual {p0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->logState()V

    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mVerifierLock:Ljava/lang/Object;
    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1200(Lcom/google/android/gsf/update/SystemUpdateService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    if-nez v0, :cond_f

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mVerifier:Lcom/google/android/gsf/update/SystemUpdateVerifierTask;
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1300(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/google/android/gsf/update/SystemUpdateVerifierTask;

    move-result-object v0

    if-eqz v0, :cond_13

    :cond_f
    const/4 v0, 0x1

    :goto_6
    monitor-exit v1

    goto/16 :goto_2

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_10
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->setStatus(I)V

    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v4, "download_next_dialog"

    const-wide/16 v7, 0x0

    invoke-interface {v1, v4, v7, v8}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->sUpdatesLockedUntil:J
    invoke-static {}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1100()J

    move-result-wide v7

    invoke-static {v4, v5, v7, v8}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    const/4 v1, 0x2

    if-eq v6, v1, :cond_11

    const/4 v1, 0x5

    if-ne v6, v1, :cond_12

    cmp-long v1, v4, v2

    if-ltz v1, :cond_12

    :cond_11
    const v1, 0x7f060015

    const v4, 0x7f060016

    const v5, 0x7f020007

    invoke-direct {p0, v1, v4, v5}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->notifyWithActivity(III)V

    const-wide/32 v4, 0x1b7740

    add-long v1, v2, v4

    const/4 v3, 0x0

    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->retryAt(JZ)V

    goto :goto_5

    :cond_12
    const/4 v1, 0x5

    if-ne v6, v1, :cond_e

    cmp-long v1, v4, v2

    if-gez v1, :cond_e

    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "notified"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    const-class v3, Lcom/google/android/gsf/update/SystemUpdateDownloadDialog;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v2, 0x10040000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-virtual {v2, v1}, Lcom/google/android/gsf/update/SystemUpdateService;->startActivity(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-virtual {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "update_install_reschedule_sec"

    const/16 v3, 0x708

    invoke-static {v1, v2, v3}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    int-to-long v4, v1

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    add-long v1, v2, v4

    const/4 v3, 0x1

    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->retryAt(JZ)V

    goto/16 :goto_5

    :cond_13
    const/4 v0, 0x0

    goto/16 :goto_6

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private downloadFailed(II)V
    .locals 7
    .param p1    # I
    .param p2    # I

    const/4 v6, 0x6

    invoke-direct {p0, v6}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->setStatus(I)V

    iget-object v3, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mDownloadRetry:Lcom/android/common/OperationScheduler;
    invoke-static {v3}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/android/common/OperationScheduler;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/common/OperationScheduler;->onTransientError()V

    iget-object v3, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mDownload:Lcom/google/android/gsf/update/Download;
    invoke-static {v3}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1400(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/google/android/gsf/update/Download;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gsf/update/Download;->removeAllDownloads()V

    iget-object v3, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v3}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v3}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v4, "status"

    const/4 v5, -0x1

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    if-eq v3, v6, :cond_0

    const-string v3, "status"

    invoke-interface {v0, v3, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->isAutomaticallyDownloaded(I)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "download_approved"

    const/4 v4, 0x0

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const v3, 0x7f060017

    const v4, 0x7f060018

    const v5, 0x7f020008

    invoke-direct {p0, v3, v4, v5}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->notifyWithActivity(III)V

    :goto_0
    const-string v3, "watchdog_deadline"

    invoke-interface {v0, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void

    :cond_1
    if-lez p2, :cond_2

    mul-int/lit16 v3, p2, 0x3e8

    int-to-long v3, v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    add-long v1, v3, v5

    iget-object v3, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mDownloadRetry:Lcom/android/common/OperationScheduler;
    invoke-static {v3}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/android/common/OperationScheduler;

    move-result-object v3

    invoke-virtual {v3, v1, v2}, Lcom/android/common/OperationScheduler;->setMoratoriumTimeMillis(J)V

    :cond_2
    iget-object v3, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mDownloadRetry:Lcom/android/common/OperationScheduler;
    invoke-static {v3}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/android/common/OperationScheduler;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mDownloadRetryOptions:Lcom/android/common/OperationScheduler$Options;
    invoke-static {v4}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1000(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/android/common/OperationScheduler$Options;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/common/OperationScheduler;->getNextTimeMillis(Lcom/android/common/OperationScheduler$Options;)J

    move-result-wide v3

    const/4 v5, 0x1

    invoke-direct {p0, v3, v4, v5}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->retryAt(JZ)V

    goto :goto_0
.end method

.method private getAllowRoaming(I)Z
    .locals 6
    .param p1    # I

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-virtual {v3}, Lcom/google/android/gsf/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "update_allow_roaming"

    const/4 v5, -0x1

    invoke-static {v3, v4, v5}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-gez v0, :cond_2

    const/4 v3, 0x4

    if-ne p1, v3, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    move v1, v2

    goto :goto_0

    :cond_2
    if-nez v0, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method private getMaintenanceWindow()Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;
    .locals 9

    const/16 v8, 0x3b

    const/16 v7, 0x17

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-virtual {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "maintenance_window"

    invoke-static {v1, v2}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v2, ""

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    new-instance v1, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;-><init>(Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;Lcom/google/android/gsf/update/SystemUpdateService$1;)V

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_1
    const-string v2, "none"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v1, "SystemUpdateService"

    const-string v2, "maintenance window set to \"none\""

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    sget-object v2, Lcom/google/android/gsf/update/SystemUpdateService;->MAINTENANCE_WINDOW_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    if-nez v3, :cond_3

    const-string v2, "SystemUpdateService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "malformed maintenance window \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    const/4 v3, 0x1

    :try_start_0
    invoke-virtual {v2, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    const/4 v4, 0x2

    invoke-virtual {v2, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    const/4 v5, 0x3

    invoke-virtual {v2, v5}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    const/4 v6, 0x4

    invoke-virtual {v2, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-gt v3, v7, :cond_4

    if-gt v4, v8, :cond_4

    if-gt v5, v7, :cond_4

    if-le v2, v8, :cond_5

    :cond_4
    const-string v2, "SystemUpdateService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "malformed maintenance window \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_5
    new-instance v1, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;-><init>(Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;Lcom/google/android/gsf/update/SystemUpdateService$1;)V

    mul-int/lit8 v0, v3, 0x3c

    add-int/2addr v0, v4

    iput v0, v1, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;->start:I

    mul-int/lit8 v0, v5, 0x3c

    add-int/2addr v0, v2

    iput v0, v1, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;->end:I

    move-object v0, v1

    goto/16 :goto_0
.end method

.method private install()V
    .locals 8

    const-string v4, "SystemUpdateService"

    const-string v5, "called install()"

    invoke-static {v4, v5}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->checkFile()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v4, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v4}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v4, "status"

    const/4 v5, 0x5

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "install_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-interface {v4, v5, v6, v7}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "pending_filename"

    invoke-interface {v4, v5, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    iget-object v4, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-virtual {v4}, Lcom/google/android/gsf/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "update_install_success_message"

    invoke-static {v4, v5}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    const-string v4, "success_message"

    invoke-interface {v1, v4, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    :cond_1
    iget-object v4, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-virtual {v4}, Lcom/google/android/gsf/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "update_install_failure_message"

    invoke-static {v4, v5}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    const-string v4, "failure_message"

    invoke-interface {v1, v4, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    :cond_2
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-virtual {p0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->logState()V

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    const-string v4, "SystemUpdateService"

    const-string v5, "calling installPackage()"

    invoke-static {v4, v5}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v4, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$1;

    invoke-direct {v4, p0, v0, v2}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$1;-><init>(Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$1;->start()V

    goto :goto_0
.end method

.method private isAutomaticallyDownloaded(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    const/4 v0, 0x6

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isInMaintenanceWindow(J)Z
    .locals 7
    .param p1    # J

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->getMaintenanceWindow()Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    iget v5, v2, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;->start:I

    iget v6, v2, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;->end:I

    if-ne v5, v6, :cond_2

    move v3, v4

    goto :goto_0

    :cond_2
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/16 v5, 0xb

    invoke-virtual {v1, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    mul-int/lit8 v5, v5, 0x3c

    const/16 v6, 0xc

    invoke-virtual {v1, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    add-int v0, v5, v6

    iget v5, v2, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;->start:I

    if-gt v5, v0, :cond_3

    iget v5, v2, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;->end:I

    if-le v0, v5, :cond_4

    :cond_3
    iget v5, v2, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;->start:I

    iget v6, v2, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;->end:I

    if-le v5, v6, :cond_0

    iget v5, v2, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;->start:I

    if-le v5, v0, :cond_4

    iget v5, v2, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;->end:I

    if-gt v0, v5, :cond_0

    :cond_4
    move v3, v4

    goto :goto_0
.end method

.method private isMobileDownloadAllowedNow()Z
    .locals 4

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-static {v0, v1}, Lcom/google/android/gsf/update/SystemUpdateService;->whenMobileAllowed(Landroid/content/SharedPreferences;Landroid/content/Context;)J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isOkToInstallNow(IJ)Z
    .locals 3
    .param p1    # I
    .param p2    # J

    const/4 v0, 0x0

    const/4 v1, 0x6

    if-ne p1, v1, :cond_1

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->sUpdatesLockedUntil:J
    invoke-static {}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1100()J

    move-result-wide v1

    cmp-long v1, p2, v1

    if-lez v1, :cond_0

    invoke-direct {p0, p2, p3}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->isInMaintenanceWindow(J)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "install_approved"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method private msUntilMaintenanceWindow(J)J
    .locals 7
    .param p1    # J

    invoke-direct {p0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->getMaintenanceWindow()Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;

    move-result-object v2

    if-nez v2, :cond_0

    const-wide v3, 0x7fffffffffffffffL

    :goto_0
    return-wide v3

    :cond_0
    iget v3, v2, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;->start:I

    iget v4, v2, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;->end:I

    if-ne v3, v4, :cond_1

    const-wide/16 v3, 0x0

    goto :goto_0

    :cond_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/16 v3, 0xb

    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    mul-int/lit8 v3, v3, 0x3c

    const/16 v4, 0xc

    invoke-virtual {v1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    add-int v0, v3, v4

    iget v3, v2, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;->start:I

    if-le v0, v3, :cond_2

    iget v3, v2, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;->start:I

    add-int/lit16 v3, v3, 0x5a0

    iput v3, v2, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;->start:I

    :cond_2
    iget v3, v2, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;->start:I

    sub-int/2addr v3, v0

    mul-int/lit8 v3, v3, 0x3c

    int-to-long v3, v3

    const-wide/16 v5, 0x3e8

    mul-long/2addr v3, v5

    goto :goto_0
.end method

.method private notifyWithActivity(III)V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "rejected_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "url"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-boolean v1, Lcom/google/android/gsf/update/SystemUpdateActivity;->sIsActivityUp:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1, v0, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "SystemUpdateService"

    const-string v1, "skipping notification"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-virtual {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    const-class v4, Lcom/google/android/gsf/update/SystemUpdateActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v3, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-static {v3, v6, v2, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    new-instance v3, Landroid/app/Notification;

    invoke-direct {v3}, Landroid/app/Notification;-><init>()V

    iget-object v4, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v4}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v5, "notified"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_2

    iput-object v1, v3, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    iget-object v4, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v4}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "notified"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_2
    iput p3, v3, Landroid/app/Notification;->icon:I

    iget-object v4, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-virtual {v3, v4, v1, v0, v2}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    iget v0, v3, Landroid/app/Notification;->flags:I

    or-int/lit8 v0, v0, 0x10

    iput v0, v3, Landroid/app/Notification;->flags:I

    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "android.system.update"

    aput-object v1, v0, v6

    iput-object v0, v3, Landroid/app/Notification;->kind:[Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Lcom/google/android/gsf/update/SystemUpdateService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-virtual {v0, p3, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0
.end method

.method private retryAt(JZ)V
    .locals 5
    .param p1    # J
    .param p3    # Z

    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    const-string v2, "alarm"

    invoke-virtual {v1, v2}, Lcom/google/android/gsf/update/SystemUpdateService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mPending:Landroid/app/PendingIntent;
    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1500(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    if-eqz p3, :cond_0

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mPending:Landroid/app/PendingIntent;
    invoke-static {v2}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1500(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v0, v1, p1, p2, v2}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    const-string v1, "SystemUpdateService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "retry (wakeup: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sub-long v3, p1, v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private setStatus(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "status"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "status"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Landroid/content/Intent;)Ljava/lang/Boolean;
    .locals 1
    .param p1    # [Landroid/content/Intent;

    array-length v0, p1

    if-lez v0, :cond_0

    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-direct {p0, v0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->checkUpdateState(Landroid/content/Intent;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->checkUpdateState(Landroid/content/Intent;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Landroid/content/Intent;

    invoke-virtual {p0, p1}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->doInBackground([Landroid/content/Intent;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method logState()V
    .locals 7

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v6, 0x0

    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v4, "status"

    invoke-interface {v1, v4, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v4, "download_approved"

    invoke-interface {v1, v4, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    or-int/lit8 v0, v0, 0x20

    :cond_0
    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v4, "install_approved"

    invoke-interface {v1, v4, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    or-int/lit8 v0, v0, 0x40

    :cond_1
    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v4, "verified"

    invoke-interface {v1, v4}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v4, "verified"

    invoke-interface {v1, v4, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_5

    move v1, v2

    :goto_0
    shl-int/lit8 v1, v1, 0x7

    or-int/2addr v0, v1

    :cond_2
    iget-boolean v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->mProvisioned:Z

    if-eqz v1, :cond_3

    or-int/lit16 v0, v0, 0x200

    :cond_3
    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v4, "download_mobile"

    invoke-interface {v1, v4, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_4

    or-int/lit16 v0, v0, 0x400

    :cond_4
    const v1, 0x31129

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    iget v5, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->mLastDownloadStatus:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    iget-wide v5, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->mLastDownloadBytes:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v4, v3

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v3}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v5, "url"

    const/4 v6, 0x0

    invoke-interface {v3, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v4, v2

    invoke-static {v1, v4}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    return-void

    :cond_5
    move v1, v3

    goto :goto_0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 6
    .param p1    # Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mPendingLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$300(Lcom/google/android/gsf/update/SystemUpdateService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    const/4 v2, 0x0

    # setter for: Lcom/google/android/gsf/update/SystemUpdateService;->mRunning:Z
    invoke-static {v0, v2}, Lcom/google/android/gsf/update/SystemUpdateService;->access$402(Lcom/google/android/gsf/update/SystemUpdateService;Z)Z

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mStartPending:Z
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$500(Lcom/google/android/gsf/update/SystemUpdateService;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    const/4 v2, 0x0

    # setter for: Lcom/google/android/gsf/update/SystemUpdateService;->mStartPending:Z
    invoke-static {v0, v2}, Lcom/google/android/gsf/update/SystemUpdateService;->access$502(Lcom/google/android/gsf/update/SystemUpdateService;Z)Z

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    const/4 v2, 0x1

    # setter for: Lcom/google/android/gsf/update/SystemUpdateService;->mRunning:Z
    invoke-static {v0, v2}, Lcom/google/android/gsf/update/SystemUpdateService;->access$402(Lcom/google/android/gsf/update/SystemUpdateService;Z)Z

    new-instance v0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;

    iget-object v2, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-direct {v0, v2}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;-><init>(Lcom/google/android/gsf/update/SystemUpdateService;)V

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->EXECUTOR:Ljava/util/concurrent/Executor;
    invoke-static {}, Lcom/google/android/gsf/update/SystemUpdateService;->access$600()Ljava/util/concurrent/Executor;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/content/Intent;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mIntentPending:Landroid/content/Intent;
    invoke-static {v5}, Lcom/google/android/gsf/update/SystemUpdateService;->access$700(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/Intent;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-virtual {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->stopSelf()V

    :cond_3
    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->sWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {}, Lcom/google/android/gsf/update/SystemUpdateService;->access$100()Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    if-eqz v0, :cond_0

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->sWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {}, Lcom/google/android/gsf/update/SystemUpdateService;->access$100()Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->sWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {}, Lcom/google/android/gsf/update/SystemUpdateService;->access$100()Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
