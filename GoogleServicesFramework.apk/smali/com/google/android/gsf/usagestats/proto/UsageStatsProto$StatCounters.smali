.class public final Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "UsageStatsProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gsf/usagestats/proto/UsageStatsProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StatCounters"
.end annotation


# instance fields
.field private cachedSize:I

.field private counterData_:Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$CounterData;

.field private direction_:I

.field private fgBg_:I

.field private hasCounterData:Z

.field private hasDirection:Z

.field private hasFgBg:Z

.field private hasNetworkProto:Z

.field private networkProto_:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput v1, p0, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->networkProto_:I

    iput v1, p0, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->direction_:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->counterData_:Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$CounterData;

    iput v1, p0, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->fgBg_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->cachedSize:I

    return v0
.end method

.method public getCounterData()Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$CounterData;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->counterData_:Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$CounterData;

    return-object v0
.end method

.method public getDirection()I
    .locals 1

    iget v0, p0, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->direction_:I

    return v0
.end method

.method public getFgBg()I
    .locals 1

    iget v0, p0, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->fgBg_:I

    return v0
.end method

.method public getNetworkProto()I
    .locals 1

    iget v0, p0, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->networkProto_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->hasNetworkProto()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->getNetworkProto()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->hasDirection()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->getDirection()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->hasCounterData()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->getCounterData()Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$CounterData;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->hasFgBg()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->getFgBg()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iput v0, p0, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->cachedSize:I

    return v0
.end method

.method public hasCounterData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->hasCounterData:Z

    return v0
.end method

.method public hasDirection()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->hasDirection:Z

    return v0
.end method

.method public hasFgBg()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->hasFgBg:Z

    return v0
.end method

.method public hasNetworkProto()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->hasNetworkProto:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->setNetworkProto(I)Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->setDirection(I)Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$CounterData;

    invoke-direct {v1}, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$CounterData;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->setCounterData(Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$CounterData;)Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->setFgBg(I)Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;

    move-result-object v0

    return-object v0
.end method

.method public setCounterData(Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$CounterData;)Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;
    .locals 1
    .param p1    # Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$CounterData;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->hasCounterData:Z

    iput-object p1, p0, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->counterData_:Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$CounterData;

    return-object p0
.end method

.method public setDirection(I)Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->hasDirection:Z

    iput p1, p0, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->direction_:I

    return-object p0
.end method

.method public setFgBg(I)Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->hasFgBg:Z

    iput p1, p0, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->fgBg_:I

    return-object p0
.end method

.method public setNetworkProto(I)Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->hasNetworkProto:Z

    iput p1, p0, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->networkProto_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->hasNetworkProto()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->getNetworkProto()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->hasDirection()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->getDirection()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->hasCounterData()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->getCounterData()Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$CounterData;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->hasFgBg()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/gsf/usagestats/proto/UsageStatsProto$StatCounters;->getFgBg()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_3
    return-void
.end method
