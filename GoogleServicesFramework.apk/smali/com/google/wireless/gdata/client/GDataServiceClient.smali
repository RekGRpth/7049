.class public abstract Lcom/google/wireless/gdata/client/GDataServiceClient;
.super Ljava/lang/Object;
.source "GDataServiceClient.java"


# instance fields
.field private final gDataClient:Lcom/google/wireless/gdata/client/GDataClient;

.field private final gDataParserFactory:Lcom/google/wireless/gdata/client/GDataParserFactory;


# direct methods
.method public constructor <init>(Lcom/google/wireless/gdata/client/GDataClient;Lcom/google/wireless/gdata/client/GDataParserFactory;)V
    .locals 0
    .param p1    # Lcom/google/wireless/gdata/client/GDataClient;
    .param p2    # Lcom/google/wireless/gdata/client/GDataParserFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/wireless/gdata/client/GDataServiceClient;->gDataClient:Lcom/google/wireless/gdata/client/GDataClient;

    iput-object p2, p0, Lcom/google/wireless/gdata/client/GDataServiceClient;->gDataParserFactory:Lcom/google/wireless/gdata/client/GDataParserFactory;

    return-void
.end method

.method private parseEntry(Ljava/lang/Class;Ljava/io/InputStream;)Lcom/google/wireless/gdata/data/Entry;
    .locals 2
    .param p1    # Ljava/lang/Class;
    .param p2    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/wireless/gdata/parser/ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/google/wireless/gdata/client/GDataServiceClient;->gDataParserFactory:Lcom/google/wireless/gdata/client/GDataParserFactory;

    invoke-interface {v1, p1, p2}, Lcom/google/wireless/gdata/client/GDataParserFactory;->createParser(Ljava/lang/Class;Ljava/io/InputStream;)Lcom/google/wireless/gdata/parser/GDataParser;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/wireless/gdata/parser/GDataParser;->parseStandaloneEntry()Lcom/google/wireless/gdata/data/Entry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/wireless/gdata/parser/GDataParser;->close()V

    :cond_0
    return-object v1

    :catchall_0
    move-exception v1

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/google/wireless/gdata/parser/GDataParser;->close()V

    :cond_1
    throw v1
.end method


# virtual methods
.method public createEntry(Ljava/lang/String;Ljava/lang/String;Lcom/google/wireless/gdata/data/Entry;)Lcom/google/wireless/gdata/data/Entry;
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/wireless/gdata/data/Entry;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/wireless/gdata/parser/ParseException;,
            Ljava/io/IOException;,
            Lcom/google/wireless/gdata/client/HttpException;
        }
    .end annotation

    iget-object v2, p0, Lcom/google/wireless/gdata/client/GDataServiceClient;->gDataParserFactory:Lcom/google/wireless/gdata/client/GDataParserFactory;

    invoke-interface {v2, p3}, Lcom/google/wireless/gdata/client/GDataParserFactory;->createSerializer(Lcom/google/wireless/gdata/data/Entry;)Lcom/google/wireless/gdata/serializer/GDataSerializer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/wireless/gdata/client/GDataServiceClient;->gDataClient:Lcom/google/wireless/gdata/client/GDataClient;

    invoke-interface {v2, p1, p2, v1}, Lcom/google/wireless/gdata/client/GDataClient;->createEntry(Ljava/lang/String;Ljava/lang/String;Lcom/google/wireless/gdata/serializer/GDataSerializer;)Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-direct {p0, v2, v0}, Lcom/google/wireless/gdata/client/GDataServiceClient;->parseEntry(Ljava/lang/Class;Ljava/io/InputStream;)Lcom/google/wireless/gdata/data/Entry;

    move-result-object v2

    return-object v2
.end method

.method public createQueryParams()Lcom/google/wireless/gdata/client/QueryParams;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/gdata/client/GDataServiceClient;->gDataClient:Lcom/google/wireless/gdata/client/GDataClient;

    invoke-interface {v0}, Lcom/google/wireless/gdata/client/GDataClient;->createQueryParams()Lcom/google/wireless/gdata/client/QueryParams;

    move-result-object v0

    return-object v0
.end method

.method public deleteEntry(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/wireless/gdata/client/HttpException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/gdata/client/GDataServiceClient;->gDataClient:Lcom/google/wireless/gdata/client/GDataClient;

    invoke-interface {v0, p1, p2}, Lcom/google/wireless/gdata/client/GDataClient;->deleteEntry(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public getEntry(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/google/wireless/gdata/data/Entry;
    .locals 2
    .param p1    # Ljava/lang/Class;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/wireless/gdata/parser/ParseException;,
            Ljava/io/IOException;,
            Lcom/google/wireless/gdata/client/HttpException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/wireless/gdata/client/GDataServiceClient;->getGDataClient()Lcom/google/wireless/gdata/client/GDataClient;

    move-result-object v1

    invoke-interface {v1, p2, p3}, Lcom/google/wireless/gdata/client/GDataClient;->getFeedAsStream(Ljava/lang/String;Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/wireless/gdata/client/GDataServiceClient;->parseEntry(Ljava/lang/Class;Ljava/io/InputStream;)Lcom/google/wireless/gdata/data/Entry;

    move-result-object v1

    return-object v1
.end method

.method protected getGDataClient()Lcom/google/wireless/gdata/client/GDataClient;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/gdata/client/GDataServiceClient;->gDataClient:Lcom/google/wireless/gdata/client/GDataClient;

    return-object v0
.end method

.method public getParserForFeed(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/google/wireless/gdata/parser/GDataParser;
    .locals 2
    .param p1    # Ljava/lang/Class;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/wireless/gdata/parser/ParseException;,
            Ljava/io/IOException;,
            Lcom/google/wireless/gdata/client/HttpException;
        }
    .end annotation

    iget-object v1, p0, Lcom/google/wireless/gdata/client/GDataServiceClient;->gDataClient:Lcom/google/wireless/gdata/client/GDataClient;

    invoke-interface {v1, p2, p3}, Lcom/google/wireless/gdata/client/GDataClient;->getFeedAsStream(Ljava/lang/String;Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    iget-object v1, p0, Lcom/google/wireless/gdata/client/GDataServiceClient;->gDataParserFactory:Lcom/google/wireless/gdata/client/GDataParserFactory;

    invoke-interface {v1, p1, v0}, Lcom/google/wireless/gdata/client/GDataParserFactory;->createParser(Ljava/lang/Class;Ljava/io/InputStream;)Lcom/google/wireless/gdata/parser/GDataParser;

    move-result-object v1

    return-object v1
.end method

.method public abstract getServiceName()Ljava/lang/String;
.end method

.method public updateEntry(Lcom/google/wireless/gdata/data/Entry;Ljava/lang/String;)Lcom/google/wireless/gdata/data/Entry;
    .locals 5
    .param p1    # Lcom/google/wireless/gdata/data/Entry;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/wireless/gdata/parser/ParseException;,
            Ljava/io/IOException;,
            Lcom/google/wireless/gdata/client/HttpException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/wireless/gdata/data/Entry;->getEditUri()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/wireless/gdata/data/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v3, Lcom/google/wireless/gdata/parser/ParseException;

    const-string v4, "No edit URI -- cannot update."

    invoke-direct {v3, v4}, Lcom/google/wireless/gdata/parser/ParseException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    iget-object v3, p0, Lcom/google/wireless/gdata/client/GDataServiceClient;->gDataParserFactory:Lcom/google/wireless/gdata/client/GDataParserFactory;

    invoke-interface {v3, p1}, Lcom/google/wireless/gdata/client/GDataParserFactory;->createSerializer(Lcom/google/wireless/gdata/data/Entry;)Lcom/google/wireless/gdata/serializer/GDataSerializer;

    move-result-object v2

    iget-object v3, p0, Lcom/google/wireless/gdata/client/GDataServiceClient;->gDataClient:Lcom/google/wireless/gdata/client/GDataClient;

    invoke-interface {v3, v0, p2, v2}, Lcom/google/wireless/gdata/client/GDataClient;->updateEntry(Ljava/lang/String;Ljava/lang/String;Lcom/google/wireless/gdata/serializer/GDataSerializer;)Ljava/io/InputStream;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-direct {p0, v3, v1}, Lcom/google/wireless/gdata/client/GDataServiceClient;->parseEntry(Ljava/lang/Class;Ljava/io/InputStream;)Lcom/google/wireless/gdata/data/Entry;

    move-result-object v3

    return-object v3
.end method
