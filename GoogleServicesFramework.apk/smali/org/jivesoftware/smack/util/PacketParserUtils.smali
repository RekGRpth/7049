.class public Lorg/jivesoftware/smack/util/PacketParserUtils;
.super Ljava/lang/Object;
.source "PacketParserUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getIntOrDefault(Lcom/google/common/io/protocol/ProtoBuf;II)I
    .locals 1
    .param p0    # Lcom/google/common/io/protocol/ProtoBuf;
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0, p1}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result p2

    :cond_0
    return p2
.end method

.method public static getLongOrDefault(Lcom/google/common/io/protocol/ProtoBuf;IJ)J
    .locals 1
    .param p0    # Lcom/google/common/io/protocol/ProtoBuf;
    .param p1    # I
    .param p2    # J

    invoke-virtual {p0, p1}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide p2

    :cond_0
    return-wide p2
.end method

.method public static getStringOrDefault(Lcom/google/common/io/protocol/ProtoBuf;ILjava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/common/io/protocol/ProtoBuf;
    .param p1    # I
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object p2

    :cond_0
    return-object p2
.end method

.method public static newXmlParser()Lorg/xmlpull/v1/XmlPullParser;
    .locals 4

    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v0

    :try_start_0
    const-string v2, "http://xmlpull.org/v1/doc/features.html#process-namespaces"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->setFeature(Ljava/lang/String;Z)V

    sget-object v2, Landroid/util/Xml;->FEATURE_RELAXED:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->setFeature(Ljava/lang/String;Z)V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    goto :goto_0
.end method

.method public static parseBatchPresence(Lcom/google/common/io/protocol/ProtoBuf;J)Lorg/jivesoftware/smack/packet/Packet;
    .locals 17
    .param p0    # Lcom/google/common/io/protocol/ProtoBuf;
    .param p1    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v5, Lorg/jivesoftware/smack/packet/BatchPresence;

    sget-object v15, Lorg/jivesoftware/smack/packet/BatchPresence$Type;->SET:Lorg/jivesoftware/smack/packet/BatchPresence$Type;

    invoke-direct {v5, v15}, Lorg/jivesoftware/smack/packet/BatchPresence;-><init>(Lorg/jivesoftware/smack/packet/BatchPresence$Type;)V

    const/4 v15, 0x1

    const-string v16, "ID_NOT_AVAILABLE"

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-static {v0, v15, v1}, Lorg/jivesoftware/smack/util/PacketParserUtils;->getStringOrDefault(Lcom/google/common/io/protocol/ProtoBuf;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Lorg/jivesoftware/smack/packet/BatchPresence;->setPacketID(Ljava/lang/String;)V

    const/4 v15, 0x2

    const-string v16, ""

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-static {v0, v15, v1}, Lorg/jivesoftware/smack/util/PacketParserUtils;->getStringOrDefault(Lcom/google/common/io/protocol/ProtoBuf;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v5, v14}, Lorg/jivesoftware/smack/packet/BatchPresence;->setTo(Ljava/lang/String;)V

    const/4 v15, 0x7

    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    invoke-static {v0, v15, v1, v2}, Lorg/jivesoftware/smack/util/PacketParserUtils;->getLongOrDefault(Lcom/google/common/io/protocol/ProtoBuf;IJ)J

    move-result-wide v3

    invoke-virtual {v5, v3, v4}, Lorg/jivesoftware/smack/packet/BatchPresence;->setAccountId(J)V

    const/4 v15, 0x4

    const/16 v16, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-static {v0, v15, v1}, Lorg/jivesoftware/smack/util/PacketParserUtils;->getStringOrDefault(Lcom/google/common/io/protocol/ProtoBuf;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v5, v12}, Lorg/jivesoftware/smack/packet/BatchPresence;->setRmq2Id(Ljava/lang/String;)V

    const/4 v15, 0x6

    const/16 v16, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v15, v1}, Lorg/jivesoftware/smack/util/PacketParserUtils;->getIntOrDefault(Lcom/google/common/io/protocol/ProtoBuf;II)I

    move-result v9

    invoke-virtual {v5, v9}, Lorg/jivesoftware/smack/packet/BatchPresence;->setLastStreamId(I)V

    const/4 v15, 0x5

    const/16 v16, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v15, v1}, Lorg/jivesoftware/smack/util/PacketParserUtils;->getIntOrDefault(Lcom/google/common/io/protocol/ProtoBuf;II)I

    move-result v13

    invoke-virtual {v5, v13}, Lorg/jivesoftware/smack/packet/BatchPresence;->setStreamId(I)V

    const/4 v15, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/google/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v6

    const/4 v7, 0x0

    :goto_0
    if-ge v7, v6, :cond_0

    const/4 v15, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v15, v7}, Lcom/google/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v11

    move-wide/from16 v0, p1

    invoke-static {v11, v0, v1}, Lorg/jivesoftware/smack/util/PacketParserUtils;->parsePresence(Lcom/google/common/io/protocol/ProtoBuf;J)Lorg/jivesoftware/smack/packet/Packet;

    move-result-object v10

    check-cast v10, Lorg/jivesoftware/smack/packet/Presence;

    invoke-virtual {v10, v3, v4}, Lorg/jivesoftware/smack/packet/Presence;->setAccountId(J)V

    invoke-virtual {v5, v10}, Lorg/jivesoftware/smack/packet/BatchPresence;->addPresenceStanza(Lorg/jivesoftware/smack/packet/Presence;)V

    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_0
    return-object v5
.end method

.method public static parseDataMessage(Lcom/google/common/io/protocol/ProtoBuf;)Lorg/jivesoftware/smack/packet/DataMessage;
    .locals 25
    .param p0    # Lcom/google/common/io/protocol/ProtoBuf;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v7, Lorg/jivesoftware/smack/packet/DataMessage;

    invoke-direct {v7}, Lorg/jivesoftware/smack/packet/DataMessage;-><init>()V

    const/16 v22, 0x3

    const/16 v23, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v22

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Lorg/jivesoftware/smack/util/PacketParserUtils;->getStringOrDefault(Lcom/google/common/io/protocol/ProtoBuf;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lorg/jivesoftware/smack/packet/DataMessage;->setFrom(Ljava/lang/String;)V

    const/16 v22, 0x4

    const/16 v23, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v22

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Lorg/jivesoftware/smack/util/PacketParserUtils;->getStringOrDefault(Lcom/google/common/io/protocol/ProtoBuf;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Lorg/jivesoftware/smack/packet/DataMessage;->setTo(Ljava/lang/String;)V

    const/16 v22, 0x2

    const/16 v23, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v22

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Lorg/jivesoftware/smack/util/PacketParserUtils;->getStringOrDefault(Lcom/google/common/io/protocol/ProtoBuf;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Lorg/jivesoftware/smack/packet/DataMessage;->setPacketID(Ljava/lang/String;)V

    const/16 v22, 0x1

    const-wide/16 v23, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v22

    move-wide/from16 v2, v23

    invoke-static {v0, v1, v2, v3}, Lorg/jivesoftware/smack/util/PacketParserUtils;->getLongOrDefault(Lcom/google/common/io/protocol/ProtoBuf;IJ)J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v7, v0, v1}, Lorg/jivesoftware/smack/packet/DataMessage;->setRmqId(J)V

    const/16 v22, 0x9

    const/16 v23, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v22

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Lorg/jivesoftware/smack/util/PacketParserUtils;->getStringOrDefault(Lcom/google/common/io/protocol/ProtoBuf;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v7, v15}, Lorg/jivesoftware/smack/packet/DataMessage;->setRmq2Id(Ljava/lang/String;)V

    const/16 v22, 0xb

    const/16 v23, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-static {v0, v1, v2}, Lorg/jivesoftware/smack/util/PacketParserUtils;->getIntOrDefault(Lcom/google/common/io/protocol/ProtoBuf;II)I

    move-result v12

    invoke-virtual {v7, v12}, Lorg/jivesoftware/smack/packet/DataMessage;->setLastStreamId(I)V

    const/16 v22, 0xa

    const/16 v23, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-static {v0, v1, v2}, Lorg/jivesoftware/smack/util/PacketParserUtils;->getIntOrDefault(Lcom/google/common/io/protocol/ProtoBuf;II)I

    move-result v18

    move/from16 v0, v18

    invoke-virtual {v7, v0}, Lorg/jivesoftware/smack/packet/DataMessage;->setStreamId(I)V

    const/16 v22, 0x5

    const/16 v23, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v22

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Lorg/jivesoftware/smack/util/PacketParserUtils;->getStringOrDefault(Lcom/google/common/io/protocol/ProtoBuf;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Lorg/jivesoftware/smack/packet/DataMessage;->setCategory(Ljava/lang/String;)V

    const/16 v22, 0x6

    const/16 v23, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v22

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Lorg/jivesoftware/smack/util/PacketParserUtils;->getStringOrDefault(Lcom/google/common/io/protocol/ProtoBuf;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Lorg/jivesoftware/smack/packet/DataMessage;->setToken(Ljava/lang/String;)V

    const/16 v22, 0x8

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v22

    if-eqz v22, :cond_0

    const/16 v22, 0x8

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/google/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v22

    if-eqz v22, :cond_0

    const/16 v22, 0x1

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Lorg/jivesoftware/smack/packet/DataMessage;->setFromTrustedServer(Z)V

    :cond_0
    const/16 v22, 0xc

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v22

    if-eqz v22, :cond_1

    const/16 v22, 0xc

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/google/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v22

    if-nez v22, :cond_1

    invoke-virtual {v7, v14}, Lorg/jivesoftware/smack/packet/DataMessage;->setPermission(Ljava/lang/String;)V

    :cond_1
    const/16 v22, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/google/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v13

    const/4 v9, 0x0

    :goto_0
    if-ge v9, v13, :cond_2

    const/16 v22, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1, v9}, Lcom/google/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v5

    const/16 v22, 0x1

    move/from16 v0, v22

    invoke-virtual {v5, v0}, Lcom/google/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v11

    const/16 v22, 0x2

    move/from16 v0, v22

    invoke-virtual {v5, v0}, Lcom/google/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v21

    new-instance v4, Lorg/jivesoftware/smack/packet/DataMessage$AppData;

    move-object/from16 v0, v21

    invoke-direct {v4, v11, v0}, Lorg/jivesoftware/smack/packet/DataMessage$AppData;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Lorg/jivesoftware/smack/packet/DataMessage;->addAppData(Lorg/jivesoftware/smack/packet/DataMessage$AppData;)V

    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    :cond_2
    return-object v7
.end method

.method public static parseError(Lcom/google/common/io/protocol/ProtoBuf;)Lorg/jivesoftware/smack/packet/XMPPError;
    .locals 11
    .param p0    # Lcom/google/common/io/protocol/ProtoBuf;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v4, 0x0

    invoke-virtual {p0, v8}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {p0, v8}, Lcom/google/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v4

    :cond_0
    const/4 v7, 0x1

    invoke-virtual {p0, v7}, Lcom/google/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    const/4 v6, 0x0

    invoke-virtual {p0, v9}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {p0, v9}, Lcom/google/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v6

    :cond_1
    new-instance v1, Lorg/jivesoftware/smack/packet/XMPPError;

    invoke-direct {v1, v0, v4, v6}, Lorg/jivesoftware/smack/packet/XMPPError;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v10}, Lcom/google/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v5

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v5, :cond_3

    invoke-virtual {p0, v10, v3}, Lcom/google/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v7

    invoke-static {v7}, Lorg/jivesoftware/smack/util/PacketParserUtils;->parsePacketExtension(Lcom/google/common/io/protocol/ProtoBuf;)Lorg/jivesoftware/smack/packet/PacketExtension;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v1, v2}, Lorg/jivesoftware/smack/packet/XMPPError;->addExtension(Lorg/jivesoftware/smack/packet/PacketExtension;)V

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    return-object v1
.end method

.method public static parseError(Lorg/xmlpull/v1/XmlPullParser;)Lorg/jivesoftware/smack/packet/XMPPError;
    .locals 10
    .param p0    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const-string v3, "-1"

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    move-result v0

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v0, :cond_1

    invoke-interface {p0, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v8

    const-string v9, "code"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    const-string v8, ""

    const-string v9, "code"

    invoke-interface {p0, v8, v9}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :cond_2
    :goto_1
    if-nez v1, :cond_6

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v4

    const/4 v8, 0x2

    if-ne v4, v8, :cond_4

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v8, "text"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    if-nez v6, :cond_2

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    :cond_3
    const-string v8, "feature-not-implemented"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const-string v7, "feature not implemented"

    goto :goto_1

    :cond_4
    const/4 v8, 0x3

    if-ne v4, v8, :cond_5

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v8

    const-string v9, "error"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v1, 0x1

    goto :goto_1

    :cond_5
    const/4 v8, 0x4

    if-ne v4, v8, :cond_2

    if-nez v6, :cond_2

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    :cond_6
    if-nez v6, :cond_7

    move-object v6, v7

    :cond_7
    new-instance v8, Lorg/jivesoftware/smack/packet/XMPPError;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    invoke-direct {v8, v9, v6}, Lorg/jivesoftware/smack/packet/XMPPError;-><init>(ILjava/lang/String;)V

    return-object v8
.end method

.method public static parseMessage(Lcom/google/common/io/protocol/ProtoBuf;J)Lorg/jivesoftware/smack/packet/Packet;
    .locals 10
    .param p0    # Lcom/google/common/io/protocol/ProtoBuf;
    .param p1    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v4, Lorg/jivesoftware/smack/packet/Message;

    invoke-direct {v4}, Lorg/jivesoftware/smack/packet/Message;-><init>()V

    const/4 v7, 0x3

    const/4 v8, 0x0

    invoke-static {p0, v7, v8}, Lorg/jivesoftware/smack/util/PacketParserUtils;->getStringOrDefault(Lcom/google/common/io/protocol/ProtoBuf;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    const-string v3, "ID_NOT_AVAILABLE"

    :cond_0
    invoke-virtual {v4, v3}, Lorg/jivesoftware/smack/packet/Message;->setPacketID(Ljava/lang/String;)V

    const/4 v7, 0x1

    const-wide/16 v8, -0x1

    invoke-static {p0, v7, v8, v9}, Lorg/jivesoftware/smack/util/PacketParserUtils;->getLongOrDefault(Lcom/google/common/io/protocol/ProtoBuf;IJ)J

    move-result-wide v7

    invoke-virtual {v4, v7, v8}, Lorg/jivesoftware/smack/packet/Message;->setRmqId(J)V

    const/16 v7, 0xd

    const-string v8, ""

    invoke-static {p0, v7, v8}, Lorg/jivesoftware/smack/util/PacketParserUtils;->getStringOrDefault(Lcom/google/common/io/protocol/ProtoBuf;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lorg/jivesoftware/smack/packet/Message;->setRmq2Id(Ljava/lang/String;)V

    const/16 v7, 0xf

    const/4 v8, -0x1

    invoke-static {p0, v7, v8}, Lorg/jivesoftware/smack/util/PacketParserUtils;->getIntOrDefault(Lcom/google/common/io/protocol/ProtoBuf;II)I

    move-result v7

    invoke-virtual {v4, v7}, Lorg/jivesoftware/smack/packet/Message;->setLastStreamId(I)V

    const/16 v7, 0xe

    const/4 v8, -0x1

    invoke-static {p0, v7, v8}, Lorg/jivesoftware/smack/util/PacketParserUtils;->getIntOrDefault(Lcom/google/common/io/protocol/ProtoBuf;II)I

    move-result v7

    invoke-virtual {v4, v7}, Lorg/jivesoftware/smack/packet/Message;->setStreamId(I)V

    const/4 v7, 0x5

    const-string v8, ""

    invoke-static {p0, v7, v8}, Lorg/jivesoftware/smack/util/PacketParserUtils;->getStringOrDefault(Lcom/google/common/io/protocol/ProtoBuf;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lorg/jivesoftware/smack/packet/Message;->setTo(Ljava/lang/String;)V

    const/4 v7, 0x4

    const-string v8, ""

    invoke-static {p0, v7, v8}, Lorg/jivesoftware/smack/util/PacketParserUtils;->getStringOrDefault(Lcom/google/common/io/protocol/ProtoBuf;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lorg/jivesoftware/smack/packet/Message;->setFrom(Ljava/lang/String;)V

    sget-object v6, Lorg/jivesoftware/smack/packet/Message$Type;->NORMAL:Lorg/jivesoftware/smack/packet/Message$Type;

    const/4 v7, 0x2

    invoke-virtual {p0, v7}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v7

    if-eqz v7, :cond_1

    const/4 v7, 0x2

    invoke-virtual {p0, v7}, Lcom/google/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v7

    packed-switch v7, :pswitch_data_0

    :cond_1
    :goto_0
    :pswitch_0
    invoke-virtual {v4, v6}, Lorg/jivesoftware/smack/packet/Message;->setType(Lorg/jivesoftware/smack/packet/Message$Type;)V

    sget-object v7, Lorg/jivesoftware/smack/packet/Message$Type;->ERROR:Lorg/jivesoftware/smack/packet/Message$Type;

    if-ne v6, v7, :cond_2

    const/16 v7, 0x9

    invoke-virtual {p0, v7}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v7

    if-eqz v7, :cond_2

    const/16 v7, 0x9

    invoke-virtual {p0, v7}, Lcom/google/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lorg/jivesoftware/smack/util/PacketParserUtils;->parseError(Lcom/google/common/io/protocol/ProtoBuf;)Lorg/jivesoftware/smack/packet/XMPPError;

    move-result-object v7

    invoke-virtual {v4, v7}, Lorg/jivesoftware/smack/packet/Message;->setError(Lorg/jivesoftware/smack/packet/XMPPError;)V

    :cond_2
    const/4 v7, 0x6

    const/4 v8, 0x0

    invoke-static {p0, v7, v8}, Lorg/jivesoftware/smack/util/PacketParserUtils;->getStringOrDefault(Lcom/google/common/io/protocol/ProtoBuf;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lorg/jivesoftware/smack/packet/Message;->setSubject(Ljava/lang/String;)V

    const/4 v7, 0x7

    const/4 v8, 0x0

    invoke-static {p0, v7, v8}, Lorg/jivesoftware/smack/util/PacketParserUtils;->getStringOrDefault(Lcom/google/common/io/protocol/ProtoBuf;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lorg/jivesoftware/smack/packet/Message;->setBody(Ljava/lang/String;)V

    const/16 v7, 0x8

    const/4 v8, 0x0

    invoke-static {p0, v7, v8}, Lorg/jivesoftware/smack/util/PacketParserUtils;->getStringOrDefault(Lcom/google/common/io/protocol/ProtoBuf;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lorg/jivesoftware/smack/packet/Message;->setThread(Ljava/lang/String;)V

    const/16 v7, 0x11

    invoke-static {p0, v7, p1, p2}, Lorg/jivesoftware/smack/util/PacketParserUtils;->getLongOrDefault(Lcom/google/common/io/protocol/ProtoBuf;IJ)J

    move-result-wide v7

    invoke-virtual {v4, v7, v8}, Lorg/jivesoftware/smack/packet/Message;->setAccountId(J)V

    const/16 v7, 0xb

    invoke-virtual {p0, v7}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v7

    if-eqz v7, :cond_3

    const/4 v7, 0x1

    invoke-virtual {v4, v7}, Lorg/jivesoftware/smack/packet/Message;->setHasNoSave(Z)V

    const/16 v7, 0xb

    invoke-virtual {p0, v7}, Lcom/google/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v7

    invoke-virtual {v4, v7}, Lorg/jivesoftware/smack/packet/Message;->setNoSave(Z)V

    :cond_3
    const/16 v7, 0x10

    invoke-virtual {p0, v7}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v7

    if-eqz v7, :cond_4

    const/4 v7, 0x1

    invoke-virtual {v4, v7}, Lorg/jivesoftware/smack/packet/Message;->setIsRead(Z)V

    :cond_4
    const/16 v7, 0xc

    invoke-virtual {p0, v7}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v7

    if-eqz v7, :cond_5

    const/16 v7, 0xc

    invoke-virtual {p0, v7}, Lcom/google/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v7

    invoke-virtual {v4, v7, v8}, Lorg/jivesoftware/smack/packet/Message;->setTimestamp(J)V

    :cond_5
    const/16 v7, 0xa

    invoke-virtual {p0, v7}, Lcom/google/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v5

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v5, :cond_7

    const/16 v7, 0xa

    invoke-virtual {p0, v7, v2}, Lcom/google/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v7

    invoke-static {v7}, Lorg/jivesoftware/smack/util/PacketParserUtils;->parsePacketExtension(Lcom/google/common/io/protocol/ProtoBuf;)Lorg/jivesoftware/smack/packet/PacketExtension;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {v4, v1}, Lorg/jivesoftware/smack/packet/Message;->addExtension(Lorg/jivesoftware/smack/packet/PacketExtension;)V

    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :pswitch_1
    sget-object v6, Lorg/jivesoftware/smack/packet/Message$Type;->CHAT:Lorg/jivesoftware/smack/packet/Message$Type;

    goto/16 :goto_0

    :pswitch_2
    sget-object v6, Lorg/jivesoftware/smack/packet/Message$Type;->GROUP_CHAT:Lorg/jivesoftware/smack/packet/Message$Type;

    goto/16 :goto_0

    :pswitch_3
    sget-object v6, Lorg/jivesoftware/smack/packet/Message$Type;->HEADLINE:Lorg/jivesoftware/smack/packet/Message$Type;

    goto/16 :goto_0

    :pswitch_4
    sget-object v6, Lorg/jivesoftware/smack/packet/Message$Type;->ERROR:Lorg/jivesoftware/smack/packet/Message$Type;

    goto/16 :goto_0

    :cond_7
    return-object v4

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static parseMessage(Lorg/xmlpull/v1/XmlPullParser;)Lorg/jivesoftware/smack/packet/Packet;
    .locals 19
    .param p0    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v9, Lorg/jivesoftware/smack/packet/Message;

    invoke-direct {v9}, Lorg/jivesoftware/smack/packet/Message;-><init>()V

    const-string v17, ""

    const-string v18, "stanza-id"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lorg/jivesoftware/smack/util/PacketParserUtils;->parseRmqIdFromString(Ljava/lang/String;)J

    move-result-wide v13

    invoke-virtual {v9, v13, v14}, Lorg/jivesoftware/smack/packet/Message;->setRmqId(J)V

    const-string v17, ""

    const-string v18, "id"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_0

    const-string v8, "ID_NOT_AVAILABLE"

    :cond_0
    invoke-virtual {v9, v8}, Lorg/jivesoftware/smack/packet/Message;->setPacketID(Ljava/lang/String;)V

    const-string v17, ""

    const-string v18, "to"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Lorg/jivesoftware/smack/packet/Message;->setTo(Ljava/lang/String;)V

    const-string v17, ""

    const-string v18, "from"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Lorg/jivesoftware/smack/packet/Message;->setFrom(Ljava/lang/String;)V

    const-string v17, ""

    const-string v18, "type"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lorg/jivesoftware/smack/packet/Message$Type;->fromString(Ljava/lang/String;)Lorg/jivesoftware/smack/packet/Message$Type;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Lorg/jivesoftware/smack/packet/Message;->setType(Lorg/jivesoftware/smack/packet/Message$Type;)V

    const/4 v4, 0x0

    const/4 v15, 0x0

    const/4 v3, 0x0

    const/16 v16, 0x0

    const/4 v12, 0x0

    :cond_1
    :goto_0
    if-nez v4, :cond_8

    invoke-interface/range {p0 .. p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v6

    const/16 v17, 0x2

    move/from16 v0, v17

    if-ne v6, v0, :cond_7

    invoke-interface/range {p0 .. p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-interface/range {p0 .. p0}, Lorg/xmlpull/v1/XmlPullParser;->getNamespace()Ljava/lang/String;

    move-result-object v11

    const-string v17, "subject"

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    if-nez v15, :cond_1

    invoke-interface/range {p0 .. p0}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v15

    goto :goto_0

    :cond_2
    const-string v17, "body"

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_3

    move-object/from16 v0, p0

    invoke-static {v0, v9}, Lorg/jivesoftware/smack/util/PacketParserUtils;->parseMessageBody(Lorg/xmlpull/v1/XmlPullParser;Lorg/jivesoftware/smack/packet/Message;)V

    goto :goto_0

    :cond_3
    const-string v17, "thread"

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_4

    if-nez v16, :cond_1

    invoke-interface/range {p0 .. p0}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v16

    goto :goto_0

    :cond_4
    const-string v17, "error"

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_5

    invoke-static/range {p0 .. p0}, Lorg/jivesoftware/smack/util/PacketParserUtils;->parseError(Lorg/xmlpull/v1/XmlPullParser;)Lorg/jivesoftware/smack/packet/XMPPError;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Lorg/jivesoftware/smack/packet/Message;->setError(Lorg/jivesoftware/smack/packet/XMPPError;)V

    goto :goto_0

    :cond_5
    const-string v17, "properties"

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_6

    const-string v17, "http://www.jivesoftware.com/xmlns/xmpp/properties"

    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_6

    invoke-static/range {p0 .. p0}, Lorg/jivesoftware/smack/util/PacketParserUtils;->parseProperties(Lorg/xmlpull/v1/XmlPullParser;)Ljava/util/Map;

    move-result-object v12

    goto :goto_0

    :cond_6
    move-object/from16 v0, p0

    invoke-static {v5, v11, v0}, Lorg/jivesoftware/smack/util/PacketParserUtils;->parsePacketExtension(Ljava/lang/String;Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)Lorg/jivesoftware/smack/packet/PacketExtension;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Lorg/jivesoftware/smack/packet/Message;->addExtension(Lorg/jivesoftware/smack/packet/PacketExtension;)V

    goto :goto_0

    :cond_7
    const/16 v17, 0x3

    move/from16 v0, v17

    if-ne v6, v0, :cond_1

    invoke-interface/range {p0 .. p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v17

    const-string v18, "message"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_1

    const/4 v4, 0x1

    goto/16 :goto_0

    :cond_8
    invoke-virtual {v9, v15}, Lorg/jivesoftware/smack/packet/Message;->setSubject(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Lorg/jivesoftware/smack/packet/Message;->setThread(Ljava/lang/String;)V

    if-eqz v12, :cond_9

    invoke-interface {v12}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_9

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-interface {v12, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v9, v10, v0}, Lorg/jivesoftware/smack/packet/Message;->setProperty(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1

    :cond_9
    return-object v9
.end method

.method private static parseMessageBody(Lorg/xmlpull/v1/XmlPullParser;Lorg/jivesoftware/smack/packet/Message;)V
    .locals 5
    .param p0    # Lorg/xmlpull/v1/XmlPullParser;
    .param p1    # Lorg/jivesoftware/smack/packet/Message;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x0

    :cond_0
    :goto_0
    if-nez v1, :cond_3

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getNamespace()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, p0}, Lorg/jivesoftware/smack/util/PacketParserUtils;->parsePacketExtension(Ljava/lang/String;Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)Lorg/jivesoftware/smack/packet/PacketExtension;

    move-result-object v3

    invoke-virtual {p1, v3}, Lorg/jivesoftware/smack/packet/Message;->addExtension(Lorg/jivesoftware/smack/packet/PacketExtension;)V

    goto :goto_0

    :cond_1
    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    if-nez v0, :cond_0

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "body"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_3
    invoke-virtual {p1, v0}, Lorg/jivesoftware/smack/packet/Message;->setBody(Ljava/lang/String;)V

    return-void
.end method

.method private static parsePacketExtension(Lcom/google/common/io/protocol/ProtoBuf;)Lorg/jivesoftware/smack/packet/PacketExtension;
    .locals 15
    .param p0    # Lcom/google/common/io/protocol/ProtoBuf;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v13, 0x1

    invoke-virtual {p0, v13}, Lcom/google/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v6

    if-nez v6, :cond_1

    const/4 v13, 0x2

    invoke-virtual {p0, v13}, Lcom/google/common/io/protocol/ProtoBuf;->getBytes(I)[B

    move-result-object v12

    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, v12}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v9

    const-string v13, "http://xmlpull.org/v1/doc/features.html#process-namespaces"

    const/4 v14, 0x1

    invoke-interface {v9, v13, v14}, Lorg/xmlpull/v1/XmlPullParser;->setFeature(Ljava/lang/String;Z)V

    const-string v13, "UTF-8"

    invoke-interface {v9, v0, v13}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    const-string v13, "http://xmlpull.org/v1/doc/features.html#relaxed"

    const/4 v14, 0x1

    invoke-interface {v9, v13, v14}, Lorg/xmlpull/v1/XmlPullParser;->setFeature(Ljava/lang/String;Z)V

    :goto_0
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v4

    const/4 v13, 0x1

    if-eq v4, v13, :cond_0

    packed-switch v4, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->getNamespace()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7, v9, v12}, Lorg/jivesoftware/smack/util/PacketParserUtils;->parsePacketExtension(Ljava/lang/String;Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;[B)Lorg/jivesoftware/smack/packet/PacketExtension;

    move-result-object v13

    :goto_1
    return-object v13

    :cond_0
    const/4 v13, 0x0

    goto :goto_1

    :cond_1
    const/4 v13, 0x2

    invoke-virtual {p0, v13}, Lcom/google/common/io/protocol/ProtoBuf;->getBytes(I)[B

    move-result-object v2

    invoke-static {}, Lorg/jivesoftware/smack/provider/ProviderManager;->getDefault()Lorg/jivesoftware/smack/provider/ProviderManager;

    move-result-object v13

    invoke-virtual {v13, v6}, Lorg/jivesoftware/smack/provider/ProviderManager;->getExtensionProvider(I)Ljava/lang/Object;

    move-result-object v8

    if-nez v8, :cond_2

    const/4 v13, 0x0

    goto :goto_1

    :cond_2
    const/4 v10, 0x0

    instance-of v13, v8, Lorg/jivesoftware/smack/provider/PacketExtensionProvider;

    if-eqz v13, :cond_4

    move-object v10, v8

    check-cast v10, Lorg/jivesoftware/smack/provider/PacketExtensionProvider;

    :cond_3
    :goto_2
    if-eqz v10, :cond_5

    invoke-interface {v10}, Lorg/jivesoftware/smack/provider/PacketExtensionProvider;->getProtoBufType()Lcom/google/common/io/protocol/ProtoBufType;

    move-result-object v11

    new-instance v5, Lcom/google/common/io/protocol/ProtoBuf;

    invoke-direct {v5, v11}, Lcom/google/common/io/protocol/ProtoBuf;-><init>(Lcom/google/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v5, v2}, Lcom/google/common/io/protocol/ProtoBuf;->parse([B)Lcom/google/common/io/protocol/ProtoBuf;

    invoke-interface {v10, v5}, Lorg/jivesoftware/smack/provider/PacketExtensionProvider;->parseExtension(Lcom/google/common/io/protocol/ProtoBuf;)Lorg/jivesoftware/smack/packet/PacketExtension;

    move-result-object v13

    goto :goto_1

    :cond_4
    instance-of v13, v8, Ljava/lang/Class;

    if-eqz v13, :cond_3

    move-object v1, v8

    check-cast v1, Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/jivesoftware/smack/provider/PacketExtensionProvider;

    goto :goto_2

    :cond_5
    const/4 v13, 0x0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static parsePacketExtension(Ljava/lang/String;Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)Lorg/jivesoftware/smack/packet/PacketExtension;
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lorg/jivesoftware/smack/util/PacketParserUtils;->parsePacketExtension(Ljava/lang/String;Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;[B)Lorg/jivesoftware/smack/packet/PacketExtension;

    move-result-object v0

    return-object v0
.end method

.method public static parsePacketExtension(Ljava/lang/String;Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;[B)Lorg/jivesoftware/smack/packet/PacketExtension;
    .locals 8
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Lorg/xmlpull/v1/XmlPullParser;
    .param p3    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lorg/jivesoftware/smack/provider/ProviderManager;->getDefault()Lorg/jivesoftware/smack/provider/ProviderManager;

    move-result-object v5

    invoke-virtual {v5, p0, p1}, Lorg/jivesoftware/smack/provider/ProviderManager;->getExtensionProvider(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_1

    instance-of v7, v4, Lorg/jivesoftware/smack/provider/PacketExtensionProvider;

    if-eqz v7, :cond_0

    move-object v7, v4

    check-cast v7, Lorg/jivesoftware/smack/provider/PacketExtensionProvider;

    invoke-interface {v7, p2}, Lorg/jivesoftware/smack/provider/PacketExtensionProvider;->parseExtension(Lorg/xmlpull/v1/XmlPullParser;)Lorg/jivesoftware/smack/packet/PacketExtension;

    move-result-object v7

    :goto_0
    return-object v7

    :cond_0
    instance-of v7, v4, Ljava/lang/Class;

    if-eqz v7, :cond_2

    move-object v7, v4

    check-cast v7, Ljava/lang/Class;

    invoke-static {p0, v7, p2}, Lorg/jivesoftware/smack/util/PacketParserUtils;->parseWithIntrospection(Ljava/lang/String;Ljava/lang/Class;Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/jivesoftware/smack/packet/PacketExtension;

    goto :goto_0

    :cond_1
    invoke-virtual {v5, p0, p1}, Lorg/jivesoftware/smack/provider/ProviderManager;->getRawXmlExtensionProvider(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    instance-of v7, v4, Lorg/jivesoftware/smack/provider/RawXmlExtensionProvider;

    if-eqz v7, :cond_2

    move-object v7, v4

    check-cast v7, Lorg/jivesoftware/smack/provider/RawXmlExtensionProvider;

    invoke-virtual {v7, p0, p1, p3}, Lorg/jivesoftware/smack/provider/RawXmlExtensionProvider;->parseRawXml(Ljava/lang/String;Ljava/lang/String;[B)Lorg/jivesoftware/smack/packet/PacketExtension;

    move-result-object v7

    goto :goto_0

    :cond_2
    new-instance v2, Lorg/jivesoftware/smack/packet/DefaultPacketExtension;

    invoke-direct {v2, p0, p1}, Lorg/jivesoftware/smack/packet/DefaultPacketExtension;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_3
    :goto_1
    if-nez v0, :cond_6

    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    const/4 v7, 0x2

    if-ne v1, v7, :cond_5

    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->isEmptyElementTag()Z

    move-result v7

    if-eqz v7, :cond_4

    const-string v7, ""

    invoke-virtual {v2, v3, v7}, Lorg/jivesoftware/smack/packet/DefaultPacketExtension;->setValue(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    const/4 v7, 0x4

    if-ne v1, v7, :cond_3

    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v3, v6}, Lorg/jivesoftware/smack/packet/DefaultPacketExtension;->setValue(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    const/4 v7, 0x3

    if-ne v1, v7, :cond_3

    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    const/4 v0, 0x1

    goto :goto_1

    :cond_6
    move-object v7, v2

    goto :goto_0
.end method

.method public static parsePresence(Lcom/google/common/io/protocol/ProtoBuf;J)Lorg/jivesoftware/smack/packet/Packet;
    .locals 29
    .param p0    # Lcom/google/common/io/protocol/ProtoBuf;
    .param p1    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/16 v26, 0x3

    const-string v27, "ID_NOT_AVAILABLE"

    move-object/from16 v0, p0

    move/from16 v1, v26

    move-object/from16 v2, v27

    invoke-static {v0, v1, v2}, Lorg/jivesoftware/smack/util/PacketParserUtils;->getStringOrDefault(Lcom/google/common/io/protocol/ProtoBuf;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const/16 v26, 0x1

    const-wide/16 v27, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v26

    move-wide/from16 v2, v27

    invoke-static {v0, v1, v2, v3}, Lorg/jivesoftware/smack/util/PacketParserUtils;->getLongOrDefault(Lcom/google/common/io/protocol/ProtoBuf;IJ)J

    move-result-wide v20

    const/16 v26, 0xd

    const/16 v27, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v26

    move-object/from16 v2, v27

    invoke-static {v0, v1, v2}, Lorg/jivesoftware/smack/util/PacketParserUtils;->getStringOrDefault(Lcom/google/common/io/protocol/ProtoBuf;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v19

    const/16 v26, 0xf

    const/16 v27, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v26

    move/from16 v2, v27

    invoke-static {v0, v1, v2}, Lorg/jivesoftware/smack/util/PacketParserUtils;->getIntOrDefault(Lcom/google/common/io/protocol/ProtoBuf;II)I

    move-result v14

    const/16 v26, 0xe

    const/16 v27, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v26

    move/from16 v2, v27

    invoke-static {v0, v1, v2}, Lorg/jivesoftware/smack/util/PacketParserUtils;->getIntOrDefault(Lcom/google/common/io/protocol/ProtoBuf;II)I

    move-result v23

    const/16 v26, 0x5

    const-string v27, ""

    move-object/from16 v0, p0

    move/from16 v1, v26

    move-object/from16 v2, v27

    invoke-static {v0, v1, v2}, Lorg/jivesoftware/smack/util/PacketParserUtils;->getStringOrDefault(Lcom/google/common/io/protocol/ProtoBuf;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v24

    const/16 v26, 0x4

    const-string v27, ""

    move-object/from16 v0, p0

    move/from16 v1, v26

    move-object/from16 v2, v27

    invoke-static {v0, v1, v2}, Lorg/jivesoftware/smack/util/PacketParserUtils;->getStringOrDefault(Lcom/google/common/io/protocol/ProtoBuf;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const/16 v26, 0x11

    move-object/from16 v0, p0

    move/from16 v1, v26

    move-wide/from16 v2, p1

    invoke-static {v0, v1, v2, v3}, Lorg/jivesoftware/smack/util/PacketParserUtils;->getLongOrDefault(Lcom/google/common/io/protocol/ProtoBuf;IJ)J

    move-result-wide v4

    sget-object v25, Lorg/jivesoftware/smack/packet/Presence$Type;->AVAILABLE:Lorg/jivesoftware/smack/packet/Presence$Type;

    const/16 v26, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v26

    if-eqz v26, :cond_0

    const/16 v26, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/google/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v26

    packed-switch v26, :pswitch_data_0

    :cond_0
    :goto_0
    const/16 v26, 0x7

    const/16 v27, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v26

    move-object/from16 v2, v27

    invoke-static {v0, v1, v2}, Lorg/jivesoftware/smack/util/PacketParserUtils;->getStringOrDefault(Lcom/google/common/io/protocol/ProtoBuf;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v22

    const/16 v26, 0x8

    const/16 v27, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v26

    move/from16 v2, v27

    invoke-static {v0, v1, v2}, Lorg/jivesoftware/smack/util/PacketParserUtils;->getIntOrDefault(Lcom/google/common/io/protocol/ProtoBuf;II)I

    move-result v18

    sget-object v15, Lorg/jivesoftware/smack/packet/Presence$Mode;->AVAILABLE:Lorg/jivesoftware/smack/packet/Presence$Mode;

    const/16 v26, 0x6

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v26

    if-eqz v26, :cond_1

    const/16 v26, 0x6

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/google/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v26

    packed-switch v26, :pswitch_data_1

    :cond_1
    :goto_1
    :pswitch_0
    const/4 v8, 0x0

    const/16 v26, 0xb

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v26

    if-eqz v26, :cond_2

    const/16 v26, 0xb

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/google/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v26

    packed-switch v26, :pswitch_data_2

    :cond_2
    :goto_2
    const/4 v7, 0x0

    const/16 v26, 0x10

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v26

    if-eqz v26, :cond_3

    const/16 v26, 0x10

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/google/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v7

    :cond_3
    const/4 v6, 0x0

    const/16 v26, 0xc

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v26

    if-eqz v26, :cond_4

    const/16 v26, 0xc

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/google/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v6

    :cond_4
    new-instance v17, Lorg/jivesoftware/smack/packet/Presence;

    move-object/from16 v0, v17

    move-object/from16 v1, v25

    move-object/from16 v2, v22

    move/from16 v3, v18

    invoke-direct {v0, v1, v2, v3, v15}, Lorg/jivesoftware/smack/packet/Presence;-><init>(Lorg/jivesoftware/smack/packet/Presence$Type;Ljava/lang/String;ILorg/jivesoftware/smack/packet/Presence$Mode;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Lorg/jivesoftware/smack/packet/Presence;->setFrom(Ljava/lang/String;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/jivesoftware/smack/packet/Presence;->setTo(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v4, v5}, Lorg/jivesoftware/smack/packet/Presence;->setAccountId(J)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Lorg/jivesoftware/smack/packet/Presence;->setPacketID(Ljava/lang/String;)V

    move-object/from16 v0, v17

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lorg/jivesoftware/smack/packet/Presence;->setRmqId(J)V

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/jivesoftware/smack/packet/Presence;->setRmq2Id(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Lorg/jivesoftware/smack/packet/Presence;->setLastStreamId(I)V

    move-object/from16 v0, v17

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/jivesoftware/smack/packet/Presence;->setStreamId(I)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Lorg/jivesoftware/smack/packet/Presence;->setClientType(Lorg/jivesoftware/smack/packet/Presence$ClientType;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Lorg/jivesoftware/smack/packet/Presence;->setAvatarHash(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Lorg/jivesoftware/smack/packet/Presence;->setCapabilityFlag(I)V

    sget-object v26, Lorg/jivesoftware/smack/packet/Presence$Type;->ERROR:Lorg/jivesoftware/smack/packet/Presence$Type;

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-ne v0, v1, :cond_5

    const/16 v26, 0x9

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v26

    if-eqz v26, :cond_5

    const/16 v26, 0x9

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/google/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v9

    invoke-static {v9}, Lorg/jivesoftware/smack/util/PacketParserUtils;->parseError(Lcom/google/common/io/protocol/ProtoBuf;)Lorg/jivesoftware/smack/packet/XMPPError;

    move-result-object v26

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/jivesoftware/smack/packet/Presence;->setError(Lorg/jivesoftware/smack/packet/XMPPError;)V

    :cond_5
    const/16 v26, 0xa

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/google/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v16

    const/4 v12, 0x0

    :goto_3
    move/from16 v0, v16

    if-ge v12, v0, :cond_7

    const/16 v26, 0xa

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1, v12}, Lcom/google/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lorg/jivesoftware/smack/util/PacketParserUtils;->parsePacketExtension(Lcom/google/common/io/protocol/ProtoBuf;)Lorg/jivesoftware/smack/packet/PacketExtension;

    move-result-object v10

    if-eqz v10, :cond_6

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Lorg/jivesoftware/smack/packet/Presence;->addExtension(Lorg/jivesoftware/smack/packet/PacketExtension;)V

    :cond_6
    add-int/lit8 v12, v12, 0x1

    goto :goto_3

    :pswitch_1
    sget-object v25, Lorg/jivesoftware/smack/packet/Presence$Type;->UNAVAILABLE:Lorg/jivesoftware/smack/packet/Presence$Type;

    goto/16 :goto_0

    :pswitch_2
    sget-object v25, Lorg/jivesoftware/smack/packet/Presence$Type;->PROBE:Lorg/jivesoftware/smack/packet/Presence$Type;

    goto/16 :goto_0

    :pswitch_3
    sget-object v25, Lorg/jivesoftware/smack/packet/Presence$Type;->SUBSCRIBE:Lorg/jivesoftware/smack/packet/Presence$Type;

    goto/16 :goto_0

    :pswitch_4
    sget-object v25, Lorg/jivesoftware/smack/packet/Presence$Type;->SUBSCRIBED:Lorg/jivesoftware/smack/packet/Presence$Type;

    goto/16 :goto_0

    :pswitch_5
    sget-object v25, Lorg/jivesoftware/smack/packet/Presence$Type;->UNSUBSCRIBE:Lorg/jivesoftware/smack/packet/Presence$Type;

    goto/16 :goto_0

    :pswitch_6
    sget-object v25, Lorg/jivesoftware/smack/packet/Presence$Type;->UNSUBSCRIBED:Lorg/jivesoftware/smack/packet/Presence$Type;

    goto/16 :goto_0

    :pswitch_7
    sget-object v25, Lorg/jivesoftware/smack/packet/Presence$Type;->ERROR:Lorg/jivesoftware/smack/packet/Presence$Type;

    goto/16 :goto_0

    :pswitch_8
    sget-object v15, Lorg/jivesoftware/smack/packet/Presence$Mode;->AWAY:Lorg/jivesoftware/smack/packet/Presence$Mode;

    goto/16 :goto_1

    :pswitch_9
    sget-object v15, Lorg/jivesoftware/smack/packet/Presence$Mode;->EXTENDED_AWAY:Lorg/jivesoftware/smack/packet/Presence$Mode;

    goto/16 :goto_1

    :pswitch_a
    sget-object v15, Lorg/jivesoftware/smack/packet/Presence$Mode;->DO_NOT_DISTURB:Lorg/jivesoftware/smack/packet/Presence$Mode;

    goto/16 :goto_1

    :pswitch_b
    sget-object v8, Lorg/jivesoftware/smack/packet/Presence$ClientType;->ANDROID:Lorg/jivesoftware/smack/packet/Presence$ClientType;

    goto/16 :goto_2

    :pswitch_c
    sget-object v8, Lorg/jivesoftware/smack/packet/Presence$ClientType;->MOBILE:Lorg/jivesoftware/smack/packet/Presence$ClientType;

    goto/16 :goto_2

    :cond_7
    return-object v17

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_2
        :pswitch_7
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_8
        :pswitch_0
        :pswitch_a
        :pswitch_9
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_c
        :pswitch_b
    .end packed-switch
.end method

.method public static parsePresence(Lorg/xmlpull/v1/XmlPullParser;)Lorg/jivesoftware/smack/packet/Presence;
    .locals 19
    .param p0    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const-string v17, ""

    const-string v18, "type"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lorg/jivesoftware/smack/packet/Presence$Type;->fromString(Ljava/lang/String;)Lorg/jivesoftware/smack/packet/Presence$Type;

    move-result-object v16

    new-instance v11, Lorg/jivesoftware/smack/packet/Presence;

    move-object/from16 v0, v16

    invoke-direct {v11, v0}, Lorg/jivesoftware/smack/packet/Presence;-><init>(Lorg/jivesoftware/smack/packet/Presence$Type;)V

    const-string v17, ""

    const-string v18, "to"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Lorg/jivesoftware/smack/packet/Presence;->setTo(Ljava/lang/String;)V

    const-string v17, ""

    const-string v18, "from"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Lorg/jivesoftware/smack/packet/Presence;->setFrom(Ljava/lang/String;)V

    const-string v17, ""

    const-string v18, "id"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_0

    const-string v8, "ID_NOT_AVAILABLE"

    :cond_0
    invoke-virtual {v11, v8}, Lorg/jivesoftware/smack/packet/Presence;->setPacketID(Ljava/lang/String;)V

    const-string v17, ""

    const-string v18, "stanza-id"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lorg/jivesoftware/smack/util/PacketParserUtils;->parseRmqIdFromString(Ljava/lang/String;)J

    move-result-wide v14

    invoke-virtual {v11, v14, v15}, Lorg/jivesoftware/smack/packet/Presence;->setRmqId(J)V

    const/4 v3, 0x0

    :cond_1
    :goto_0
    if-nez v3, :cond_8

    invoke-interface/range {p0 .. p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v5

    const/16 v17, 0x2

    move/from16 v0, v17

    if-ne v5, v0, :cond_7

    invoke-interface/range {p0 .. p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-interface/range {p0 .. p0}, Lorg/xmlpull/v1/XmlPullParser;->getNamespace()Ljava/lang/String;

    move-result-object v10

    const-string v17, "status"

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    invoke-interface/range {p0 .. p0}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Lorg/jivesoftware/smack/packet/Presence;->setStatus(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v17, "priority"

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_3

    :try_start_0
    invoke-interface/range {p0 .. p0}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    invoke-virtual {v11, v12}, Lorg/jivesoftware/smack/packet/Presence;->setPriority(I)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v17

    goto :goto_0

    :catch_1
    move-exception v7

    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v11, v0}, Lorg/jivesoftware/smack/packet/Presence;->setPriority(I)V

    goto :goto_0

    :cond_3
    const-string v17, "show"

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_4

    invoke-interface/range {p0 .. p0}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lorg/jivesoftware/smack/packet/Presence$Mode;->fromString(Ljava/lang/String;)Lorg/jivesoftware/smack/packet/Presence$Mode;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Lorg/jivesoftware/smack/packet/Presence;->setMode(Lorg/jivesoftware/smack/packet/Presence$Mode;)V

    goto :goto_0

    :cond_4
    const-string v17, "error"

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_5

    invoke-static/range {p0 .. p0}, Lorg/jivesoftware/smack/util/PacketParserUtils;->parseError(Lorg/xmlpull/v1/XmlPullParser;)Lorg/jivesoftware/smack/packet/XMPPError;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Lorg/jivesoftware/smack/packet/Presence;->setError(Lorg/jivesoftware/smack/packet/XMPPError;)V

    goto :goto_0

    :cond_5
    const-string v17, "properties"

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_6

    const-string v17, "http://www.jivesoftware.com/xmlns/xmpp/properties"

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_6

    invoke-static/range {p0 .. p0}, Lorg/jivesoftware/smack/util/PacketParserUtils;->parseProperties(Lorg/xmlpull/v1/XmlPullParser;)Ljava/util/Map;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-interface {v13, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v11, v9, v0}, Lorg/jivesoftware/smack/packet/Presence;->setProperty(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1

    :cond_6
    move-object/from16 v0, p0

    invoke-static {v4, v10, v0}, Lorg/jivesoftware/smack/util/PacketParserUtils;->parsePacketExtension(Ljava/lang/String;Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)Lorg/jivesoftware/smack/packet/PacketExtension;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Lorg/jivesoftware/smack/packet/Presence;->addExtension(Lorg/jivesoftware/smack/packet/PacketExtension;)V

    goto/16 :goto_0

    :cond_7
    const/16 v17, 0x3

    move/from16 v0, v17

    if-ne v5, v0, :cond_1

    invoke-interface/range {p0 .. p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v17

    const-string v18, "presence"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_1

    const/4 v3, 0x1

    goto/16 :goto_0

    :cond_8
    return-object v11
.end method

.method public static parseProperties(Lorg/xmlpull/v1/XmlPullParser;)Ljava/util/Map;
    .locals 12
    .param p0    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v11, 0x3

    const/4 v10, 0x2

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    :cond_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v2

    if-ne v2, v10, :cond_b

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v8

    const-string v9, "property"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_b

    const/4 v0, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x0

    :cond_1
    :goto_0
    if-nez v0, :cond_0

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v2

    if-ne v2, v10, :cond_3

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v8, "name"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_2
    const-string v8, "value"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    const-string v8, ""

    const-string v9, "type"

    invoke-interface {p0, v8, v9}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    :cond_3
    if-ne v2, v11, :cond_1

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v8

    const-string v9, "property"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    const-string v8, "integer"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    :cond_4
    :goto_1
    if-eqz v3, :cond_5

    if-eqz v6, :cond_5

    invoke-interface {v4, v3, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    const/4 v0, 0x1

    goto :goto_0

    :cond_6
    const-string v8, "long"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    invoke-static {v7}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    goto :goto_1

    :cond_7
    const-string v8, "float"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    invoke-static {v7}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v6

    goto :goto_1

    :cond_8
    const-string v8, "double"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    invoke-static {v7}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v6

    goto :goto_1

    :cond_9
    const-string v8, "boolean"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_a

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v6

    goto :goto_1

    :cond_a
    const-string v8, "string"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    move-object v6, v7

    goto :goto_1

    :cond_b
    if-ne v2, v11, :cond_0

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v8

    const-string v9, "properties"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    return-object v4
.end method

.method public static parseRmqIdFromString(Ljava/lang/String;)J
    .locals 6
    .param p0    # Ljava/lang/String;

    const-wide/16 v1, -0x1

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    :try_start_0
    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v1

    :cond_0
    :goto_0
    return-wide v1

    :catch_0
    move-exception v0

    const-string v3, "Smack"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "parseRmqIdFromString caught "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static parseWithIntrospection(Ljava/lang/String;Ljava/lang/Class;Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/Object;
    .locals 2
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/Class;
    .param p2    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method
