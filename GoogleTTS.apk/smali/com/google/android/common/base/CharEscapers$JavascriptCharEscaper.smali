.class Lcom/google/android/common/base/CharEscapers$JavascriptCharEscaper;
.super Lcom/google/android/common/base/CharEscapers$FastCharEscaper;
.source "CharEscapers.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/common/base/CharEscapers;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "JavascriptCharEscaper"
.end annotation


# direct methods
.method public constructor <init>([[C)V
    .locals 2
    .param p1    # [[C

    const/16 v0, 0x20

    const/16 v1, 0x7e

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/common/base/CharEscapers$FastCharEscaper;-><init>([[CCC)V

    return-void
.end method


# virtual methods
.method protected escape(C)[C
    .locals 9
    .param p1    # C

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget v1, p0, Lcom/google/android/common/base/CharEscapers$JavascriptCharEscaper;->replacementLength:I

    if-ge p1, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/common/base/CharEscapers$JavascriptCharEscaper;->replacements:[[C

    aget-object v0, v1, p1

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-char v1, p0, Lcom/google/android/common/base/CharEscapers$JavascriptCharEscaper;->safeMin:C

    if-gt v1, p1, :cond_1

    iget-char v1, p0, Lcom/google/android/common/base/CharEscapers$JavascriptCharEscaper;->safeMax:C

    if-gt p1, v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/16 v1, 0x100

    if-ge p1, v1, :cond_2

    new-array v0, v8, [C

    # getter for: Lcom/google/android/common/base/CharEscapers;->HEX_DIGITS:[C
    invoke-static {}, Lcom/google/android/common/base/CharEscapers;->access$100()[C

    move-result-object v1

    and-int/lit8 v2, p1, 0xf

    aget-char v1, v1, v2

    aput-char v1, v0, v7

    ushr-int/lit8 v1, p1, 0x4

    int-to-char p1, v1

    # getter for: Lcom/google/android/common/base/CharEscapers;->HEX_DIGITS:[C
    invoke-static {}, Lcom/google/android/common/base/CharEscapers;->access$100()[C

    move-result-object v1

    and-int/lit8 v2, p1, 0xf

    aget-char v1, v1, v2

    aput-char v1, v0, v6

    const/16 v1, 0x78

    aput-char v1, v0, v5

    const/16 v1, 0x5c

    aput-char v1, v0, v4

    goto :goto_0

    :cond_2
    const/4 v1, 0x6

    new-array v0, v1, [C

    const/4 v1, 0x5

    # getter for: Lcom/google/android/common/base/CharEscapers;->HEX_DIGITS:[C
    invoke-static {}, Lcom/google/android/common/base/CharEscapers;->access$100()[C

    move-result-object v2

    and-int/lit8 v3, p1, 0xf

    aget-char v2, v2, v3

    aput-char v2, v0, v1

    ushr-int/lit8 v1, p1, 0x4

    int-to-char p1, v1

    # getter for: Lcom/google/android/common/base/CharEscapers;->HEX_DIGITS:[C
    invoke-static {}, Lcom/google/android/common/base/CharEscapers;->access$100()[C

    move-result-object v1

    and-int/lit8 v2, p1, 0xf

    aget-char v1, v1, v2

    aput-char v1, v0, v8

    ushr-int/lit8 v1, p1, 0x4

    int-to-char p1, v1

    # getter for: Lcom/google/android/common/base/CharEscapers;->HEX_DIGITS:[C
    invoke-static {}, Lcom/google/android/common/base/CharEscapers;->access$100()[C

    move-result-object v1

    and-int/lit8 v2, p1, 0xf

    aget-char v1, v1, v2

    aput-char v1, v0, v7

    ushr-int/lit8 v1, p1, 0x4

    int-to-char p1, v1

    # getter for: Lcom/google/android/common/base/CharEscapers;->HEX_DIGITS:[C
    invoke-static {}, Lcom/google/android/common/base/CharEscapers;->access$100()[C

    move-result-object v1

    and-int/lit8 v2, p1, 0xf

    aget-char v1, v1, v2

    aput-char v1, v0, v6

    const/16 v1, 0x75

    aput-char v1, v0, v5

    const/16 v1, 0x5c

    aput-char v1, v0, v4

    goto :goto_0
.end method
