.class public Lcom/google/android/common/html/parser/HtmlDocument$VisitorWrapper;
.super Ljava/lang/Object;
.source "HtmlDocument.java"

# interfaces
.implements Lcom/google/android/common/html/parser/HtmlDocument$Visitor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/common/html/parser/HtmlDocument;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "VisitorWrapper"
.end annotation


# instance fields
.field private final wrapped:Lcom/google/android/common/html/parser/HtmlDocument$Visitor;


# virtual methods
.method public finish()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/common/html/parser/HtmlDocument$VisitorWrapper;->wrapped:Lcom/google/android/common/html/parser/HtmlDocument$Visitor;

    invoke-interface {v0}, Lcom/google/android/common/html/parser/HtmlDocument$Visitor;->finish()V

    return-void
.end method

.method public start()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/common/html/parser/HtmlDocument$VisitorWrapper;->wrapped:Lcom/google/android/common/html/parser/HtmlDocument$Visitor;

    invoke-interface {v0}, Lcom/google/android/common/html/parser/HtmlDocument$Visitor;->start()V

    return-void
.end method

.method public visitComment(Lcom/google/android/common/html/parser/HtmlDocument$Comment;)V
    .locals 1
    .param p1    # Lcom/google/android/common/html/parser/HtmlDocument$Comment;

    iget-object v0, p0, Lcom/google/android/common/html/parser/HtmlDocument$VisitorWrapper;->wrapped:Lcom/google/android/common/html/parser/HtmlDocument$Visitor;

    invoke-interface {v0, p1}, Lcom/google/android/common/html/parser/HtmlDocument$Visitor;->visitComment(Lcom/google/android/common/html/parser/HtmlDocument$Comment;)V

    return-void
.end method

.method public visitEndTag(Lcom/google/android/common/html/parser/HtmlDocument$EndTag;)V
    .locals 1
    .param p1    # Lcom/google/android/common/html/parser/HtmlDocument$EndTag;

    iget-object v0, p0, Lcom/google/android/common/html/parser/HtmlDocument$VisitorWrapper;->wrapped:Lcom/google/android/common/html/parser/HtmlDocument$Visitor;

    invoke-interface {v0, p1}, Lcom/google/android/common/html/parser/HtmlDocument$Visitor;->visitEndTag(Lcom/google/android/common/html/parser/HtmlDocument$EndTag;)V

    return-void
.end method

.method public visitTag(Lcom/google/android/common/html/parser/HtmlDocument$Tag;)V
    .locals 1
    .param p1    # Lcom/google/android/common/html/parser/HtmlDocument$Tag;

    iget-object v0, p0, Lcom/google/android/common/html/parser/HtmlDocument$VisitorWrapper;->wrapped:Lcom/google/android/common/html/parser/HtmlDocument$Visitor;

    invoke-interface {v0, p1}, Lcom/google/android/common/html/parser/HtmlDocument$Visitor;->visitTag(Lcom/google/android/common/html/parser/HtmlDocument$Tag;)V

    return-void
.end method

.method public visitText(Lcom/google/android/common/html/parser/HtmlDocument$Text;)V
    .locals 1
    .param p1    # Lcom/google/android/common/html/parser/HtmlDocument$Text;

    iget-object v0, p0, Lcom/google/android/common/html/parser/HtmlDocument$VisitorWrapper;->wrapped:Lcom/google/android/common/html/parser/HtmlDocument$Visitor;

    invoke-interface {v0, p1}, Lcom/google/android/common/html/parser/HtmlDocument$Visitor;->visitText(Lcom/google/android/common/html/parser/HtmlDocument$Text;)V

    return-void
.end method
