.class public Lcom/google/android/tts/GeneralSettingsFragment;
.super Landroid/preference/PreferenceFragment;
.source "GeneralSettingsFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/tts/GeneralSettingsFragment$LicenseFileLoader;
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mSpinnerDlg:Landroid/app/ProgressDialog;

.field private mTextDlg:Landroid/app/AlertDialog;

.field private mWebView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/tts/GeneralSettingsFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/tts/GeneralSettingsFragment;

    invoke-direct {p0}, Lcom/google/android/tts/GeneralSettingsFragment;->showOpenSourceLicenses()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/tts/GeneralSettingsFragment;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/tts/GeneralSettingsFragment;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/tts/GeneralSettingsFragment;->showPageOfText(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/tts/GeneralSettingsFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/tts/GeneralSettingsFragment;

    invoke-direct {p0}, Lcom/google/android/tts/GeneralSettingsFragment;->showError()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/tts/GeneralSettingsFragment;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0    # Lcom/google/android/tts/GeneralSettingsFragment;

    iget-object v0, p0, Lcom/google/android/tts/GeneralSettingsFragment;->mSpinnerDlg:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/tts/GeneralSettingsFragment;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0
    .param p0    # Lcom/google/android/tts/GeneralSettingsFragment;
    .param p1    # Landroid/app/ProgressDialog;

    iput-object p1, p0, Lcom/google/android/tts/GeneralSettingsFragment;->mSpinnerDlg:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/tts/GeneralSettingsFragment;)Landroid/app/AlertDialog;
    .locals 1
    .param p0    # Lcom/google/android/tts/GeneralSettingsFragment;

    iget-object v0, p0, Lcom/google/android/tts/GeneralSettingsFragment;->mTextDlg:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/tts/GeneralSettingsFragment;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0    # Lcom/google/android/tts/GeneralSettingsFragment;
    .param p1    # Landroid/app/AlertDialog;

    iput-object p1, p0, Lcom/google/android/tts/GeneralSettingsFragment;->mTextDlg:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$500(Lcom/google/android/tts/GeneralSettingsFragment;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/tts/GeneralSettingsFragment;

    iget-object v0, p0, Lcom/google/android/tts/GeneralSettingsFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/tts/GeneralSettingsFragment;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/google/android/tts/GeneralSettingsFragment;

    iget-object v0, p0, Lcom/google/android/tts/GeneralSettingsFragment;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private showError()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/tts/GeneralSettingsFragment;->mSpinnerDlg:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/tts/GeneralSettingsFragment;->mSpinnerDlg:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/google/android/tts/GeneralSettingsFragment;->mContext:Landroid/content/Context;

    const v1, 0x7f060006

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method private showOpenSourceLicenses()V
    .locals 7

    const/4 v6, 0x0

    const v4, 0x7f060004

    invoke-virtual {p0, v4}, Lcom/google/android/tts/GeneralSettingsFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    const v4, 0x7f060005

    invoke-virtual {p0, v4}, Lcom/google/android/tts/GeneralSettingsFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/tts/GeneralSettingsFragment;->mContext:Landroid/content/Context;

    const/4 v5, 0x1

    invoke-static {v4, v3, v0, v5, v6}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    iput-object v1, p0, Lcom/google/android/tts/GeneralSettingsFragment;->mSpinnerDlg:Landroid/app/ProgressDialog;

    new-instance v2, Ljava/lang/Thread;

    new-instance v4, Lcom/google/android/tts/GeneralSettingsFragment$LicenseFileLoader;

    invoke-direct {v4, p0}, Lcom/google/android/tts/GeneralSettingsFragment$LicenseFileLoader;-><init>(Lcom/google/android/tts/GeneralSettingsFragment;)V

    invoke-direct {v2, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private showPageOfText(Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v6, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lcom/google/android/tts/GeneralSettingsFragment;->mContext:Landroid/content/Context;

    invoke-direct {v6, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    invoke-virtual {v6, v0}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/tts/GeneralSettingsFragment;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f060004

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/tts/GeneralSettingsFragment;->mTextDlg:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/google/android/tts/GeneralSettingsFragment;->mWebView:Landroid/webkit/WebView;

    const-string v3, "text/html"

    const-string v4, "utf-8"

    move-object v2, p1

    move-object v5, v1

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/tts/GeneralSettingsFragment;->mWebView:Landroid/webkit/WebView;

    new-instance v2, Lcom/google/android/tts/GeneralSettingsFragment$3;

    invoke-direct {v2, p0}, Lcom/google/android/tts/GeneralSettingsFragment$3;-><init>(Lcom/google/android/tts/GeneralSettingsFragment;)V

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    iput-object v1, p0, Lcom/google/android/tts/GeneralSettingsFragment;->mWebView:Landroid/webkit/WebView;

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const/high16 v1, 0x7f030000

    invoke-virtual {p0, v1}, Lcom/google/android/tts/GeneralSettingsFragment;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/google/android/tts/GeneralSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1}, Landroid/preference/PreferenceScreen;->getContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/tts/GeneralSettingsFragment;->mContext:Landroid/content/Context;

    const-string v1, "open_source_licenses"

    invoke-virtual {p0, v1}, Lcom/google/android/tts/GeneralSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    new-instance v1, Lcom/google/android/tts/GeneralSettingsFragment$1;

    invoke-direct {v1, p0}, Lcom/google/android/tts/GeneralSettingsFragment$1;-><init>(Lcom/google/android/tts/GeneralSettingsFragment;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    new-instance v1, Landroid/webkit/WebView;

    iget-object v2, p0, Lcom/google/android/tts/GeneralSettingsFragment;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/tts/GeneralSettingsFragment;->mWebView:Landroid/webkit/WebView;

    new-instance v1, Lcom/google/android/tts/GeneralSettingsFragment$2;

    invoke-direct {v1, p0}, Lcom/google/android/tts/GeneralSettingsFragment$2;-><init>(Lcom/google/android/tts/GeneralSettingsFragment;)V

    iput-object v1, p0, Lcom/google/android/tts/GeneralSettingsFragment;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onDestroy()V

    iget-object v0, p0, Lcom/google/android/tts/GeneralSettingsFragment;->mTextDlg:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/tts/GeneralSettingsFragment;->mTextDlg:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    :cond_0
    return-void
.end method
