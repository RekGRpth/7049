.class public Lcom/google/android/tts/GoogleTTSService;
.super Landroid/speech/tts/TextToSpeechService;
.source "GoogleTTSService.java"


# instance fields
.field private mLocal:Lcom/google/android/tts/Synthesizer;

.field private mNetwork:Lcom/google/android/tts/Synthesizer;

.field private mSynthesizer:Lcom/google/android/tts/DelegatingSynthesizer;

.field private mTtsConfig:Lcom/google/android/tts/TtsConfig;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/speech/tts/TextToSpeechService;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 7

    invoke-static {p0}, Lcom/google/android/tts/GoogleTtsApplication;->get(Landroid/content/Context;)Lcom/google/android/tts/GoogleTtsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/tts/GoogleTtsApplication;->getTtsConfig()Lcom/google/android/tts/TtsConfig;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/tts/GoogleTTSService;->mTtsConfig:Lcom/google/android/tts/TtsConfig;

    invoke-virtual {v0}, Lcom/google/android/tts/GoogleTtsApplication;->getVoiceDataManager()Lcom/google/android/tts/voicepack/VoiceDataManager;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/tts/GoogleTtsApplication;->getMetadataManager()Lcom/google/android/tts/voicepack/MetadataManager;

    move-result-object v1

    new-instance v3, Lcom/google/android/tts/local/PattsSynthesizer;

    iget-object v4, p0, Lcom/google/android/tts/GoogleTTSService;->mTtsConfig:Lcom/google/android/tts/TtsConfig;

    invoke-virtual {v0}, Lcom/google/android/tts/GoogleTtsApplication;->getLangCountryHelper()Lcom/google/android/tts/local/LangCountryHelper;

    move-result-object v5

    invoke-direct {v3, p0, v4, v5, v2}, Lcom/google/android/tts/local/PattsSynthesizer;-><init>(Landroid/content/Context;Lcom/google/android/tts/TtsConfig;Lcom/google/android/tts/local/LangCountryHelper;Lcom/google/android/tts/voicepack/VoiceDataManager;)V

    iput-object v3, p0, Lcom/google/android/tts/GoogleTTSService;->mLocal:Lcom/google/android/tts/Synthesizer;

    new-instance v3, Lcom/google/android/tts/network/NetworkSynthesizer;

    new-instance v4, Lcom/google/android/tts/network/NetworkLangCountryHelper;

    const v5, 0x7f040001

    const v6, 0x7f040002

    invoke-direct {v4, p0, v5, v6}, Lcom/google/android/tts/network/NetworkLangCountryHelper;-><init>(Landroid/content/Context;II)V

    invoke-virtual {v0}, Lcom/google/android/tts/GoogleTtsApplication;->getUrlRewriter()Lcom/google/android/common/base/Function;

    move-result-object v5

    invoke-direct {v3, p0, v4, v5}, Lcom/google/android/tts/network/NetworkSynthesizer;-><init>(Landroid/content/Context;Lcom/google/android/tts/network/NetworkLangCountryHelper;Lcom/google/android/common/base/Function;)V

    iput-object v3, p0, Lcom/google/android/tts/GoogleTTSService;->mNetwork:Lcom/google/android/tts/Synthesizer;

    new-instance v3, Lcom/google/android/tts/DelegatingSynthesizer;

    iget-object v4, p0, Lcom/google/android/tts/GoogleTTSService;->mLocal:Lcom/google/android/tts/Synthesizer;

    iget-object v5, p0, Lcom/google/android/tts/GoogleTTSService;->mNetwork:Lcom/google/android/tts/Synthesizer;

    new-instance v6, Lcom/google/android/tts/GoogleTTSService$1;

    invoke-direct {v6, p0, v1, v0}, Lcom/google/android/tts/GoogleTTSService$1;-><init>(Lcom/google/android/tts/GoogleTTSService;Lcom/google/android/tts/voicepack/MetadataManager;Lcom/google/android/tts/GoogleTtsApplication;)V

    invoke-direct {v3, v4, v5, v6}, Lcom/google/android/tts/DelegatingSynthesizer;-><init>(Lcom/google/android/tts/Synthesizer;Lcom/google/android/tts/Synthesizer;Lcom/google/android/tts/DownloadEnabler;)V

    iput-object v3, p0, Lcom/google/android/tts/GoogleTTSService;->mSynthesizer:Lcom/google/android/tts/DelegatingSynthesizer;

    invoke-super {p0}, Landroid/speech/tts/TextToSpeechService;->onCreate()V

    iget-object v3, p0, Lcom/google/android/tts/GoogleTTSService;->mSynthesizer:Lcom/google/android/tts/DelegatingSynthesizer;

    invoke-virtual {v3}, Lcom/google/android/tts/DelegatingSynthesizer;->onInit()V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/tts/GoogleTTSService;->mSynthesizer:Lcom/google/android/tts/DelegatingSynthesizer;

    invoke-virtual {v0}, Lcom/google/android/tts/DelegatingSynthesizer;->onClose()V

    invoke-super {p0}, Landroid/speech/tts/TextToSpeechService;->onDestroy()V

    return-void
.end method

.method protected onGetFeaturesForLanguage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Set;
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v6, 0x1

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-static {p1}, Lcom/google/android/tts/GoogleTtsRequest;->safeToLower(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p2}, Lcom/google/android/tts/GoogleTtsRequest;->safeToLower(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/tts/GoogleTTSService;->mNetwork:Lcom/google/android/tts/Synthesizer;

    invoke-interface {v5, v2, v0}, Lcom/google/android/tts/Synthesizer;->isLanguageAvailable(Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    iget-object v5, p0, Lcom/google/android/tts/GoogleTTSService;->mLocal:Lcom/google/android/tts/Synthesizer;

    invoke-interface {v5, v2, v0}, Lcom/google/android/tts/Synthesizer;->isLanguageAvailable(Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    if-eqz v4, :cond_0

    if-ne v4, v6, :cond_1

    :cond_0
    const-string v5, "networkTts"

    invoke-interface {v1, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_1
    if-eqz v3, :cond_2

    if-ne v3, v6, :cond_3

    :cond_2
    const-string v5, "embeddedTts"

    invoke-interface {v1, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_3
    return-object v1
.end method

.method protected onGetLanguage()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/tts/GoogleTTSService;->mSynthesizer:Lcom/google/android/tts/DelegatingSynthesizer;

    invoke-virtual {v0}, Lcom/google/android/tts/DelegatingSynthesizer;->getLanguage()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onIsLanguageAvailable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/tts/GoogleTTSService;->mSynthesizer:Lcom/google/android/tts/DelegatingSynthesizer;

    invoke-static {p1}, Lcom/google/android/tts/GoogleTtsRequest;->safeToLower(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p2}, Lcom/google/android/tts/GoogleTtsRequest;->safeToLower(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/tts/DelegatingSynthesizer;->isLanguageAvailable(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method protected onLoadLanguage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/tts/GoogleTTSService;->mSynthesizer:Lcom/google/android/tts/DelegatingSynthesizer;

    invoke-static {p1}, Lcom/google/android/tts/GoogleTtsRequest;->safeToLower(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p2}, Lcom/google/android/tts/GoogleTtsRequest;->safeToLower(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/tts/DelegatingSynthesizer;->onLoadLanguage(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method protected onStop()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/tts/GoogleTTSService;->mSynthesizer:Lcom/google/android/tts/DelegatingSynthesizer;

    invoke-virtual {v0}, Lcom/google/android/tts/DelegatingSynthesizer;->onStop()V

    return-void
.end method

.method protected onSynthesizeText(Landroid/speech/tts/SynthesisRequest;Landroid/speech/tts/SynthesisCallback;)V
    .locals 2
    .param p1    # Landroid/speech/tts/SynthesisRequest;
    .param p2    # Landroid/speech/tts/SynthesisCallback;

    iget-object v0, p0, Lcom/google/android/tts/GoogleTTSService;->mSynthesizer:Lcom/google/android/tts/DelegatingSynthesizer;

    iget-object v1, p0, Lcom/google/android/tts/GoogleTTSService;->mTtsConfig:Lcom/google/android/tts/TtsConfig;

    invoke-static {p1, v1}, Lcom/google/android/tts/GoogleTtsRequest;->wrap(Landroid/speech/tts/SynthesisRequest;Lcom/google/android/tts/TtsConfig;)Lcom/google/android/tts/GoogleTtsRequest;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/google/android/tts/DelegatingSynthesizer;->onSynthesize(Lcom/google/android/tts/GoogleTtsRequest;Landroid/speech/tts/SynthesisCallback;)V

    return-void
.end method
