.class public Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "DownloadBroadcastReceiver.java"


# instance fields
.field private mApp:Lcom/google/android/tts/GoogleTtsApplication;

.field private mContext:Landroid/content/Context;

.field private mDownloadInfo:Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;

.field private mDownloadManager:Lcom/google/android/tts/voicepack/VoiceDownloadHelper;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;Landroid/content/Intent;Landroid/content/BroadcastReceiver$PendingResult;)Z
    .locals 1
    .param p0    # Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;
    .param p1    # Landroid/content/Intent;
    .param p2    # Landroid/content/BroadcastReceiver$PendingResult;

    invoke-direct {p0, p1, p2}, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->handleCompletedDownload(Landroid/content/Intent;Landroid/content/BroadcastReceiver$PendingResult;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;Z)V
    .locals 0
    .param p0    # Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->broadcastDownloadComplete(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;Z)V
    .locals 0
    .param p0    # Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->showDownloadCompleteNotification(Z)V

    return-void
.end method

.method private broadcastDownloadComplete(Z)V
    .locals 3
    .param p1    # Z

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.speech.tts.engine.TTS_DATA_INSTALLED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "dataInstalled"

    if-eqz p1, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method private buildNotification(Ljava/lang/String;Z)Landroid/app/Notification;
    .locals 6

    const/4 v4, 0x1

    const/4 v5, 0x0

    new-instance v1, Landroid/app/Notification$Builder;

    iget-object v0, p0, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    invoke-virtual {v1, v4}, Landroid/app/Notification$Builder;->setOnlyAlertOnce(Z)Landroid/app/Notification$Builder;

    invoke-static {p1}, Lcom/google/android/tts/voicepack/MetadataManager;->getDisplayString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz p2, :cond_0

    const v2, 0x1080082

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    iget-object v2, p0, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->mContext:Landroid/content/Context;

    const v3, 0x7f060013

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    iget-object v0, p0, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->mContext:Landroid/content/Context;

    const v2, 0x7f060014

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.android.settings.TTS_SETTINGS"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    :goto_0
    iget-object v2, p0, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-static {v2, v5, v0, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    invoke-virtual {v1}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v0

    return-object v0

    :cond_0
    const v2, 0x1080078

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    iget-object v2, p0, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->mContext:Landroid/content/Context;

    const v3, 0x7f060015

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    iget-object v0, p0, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->mContext:Landroid/content/Context;

    const v2, 0x7f060016

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->mContext:Landroid/content/Context;

    const-class v3, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_0
.end method

.method private deleteDownloadedFile(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "TTS.DownloadBroadcastReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to delete downloaded file:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method private handleCompletedDownload(Landroid/content/Intent;Landroid/content/BroadcastReceiver$PendingResult;)Z
    .locals 6
    .param p1    # Landroid/content/Intent;
    .param p2    # Landroid/content/BroadcastReceiver$PendingResult;

    const/4 v2, 0x0

    const-string v3, "extra_download_id"

    const-wide/16 v4, -0x1

    invoke-virtual {p1, v3, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    const-wide/16 v3, 0x0

    cmp-long v3, v0, v3

    if-gez v3, :cond_1

    const-string v3, "TTS.DownloadBroadcastReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Received intent without download ID :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v3, p0, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->mDownloadManager:Lcom/google/android/tts/voicepack/VoiceDownloadHelper;

    invoke-virtual {v3, v0, v1}, Lcom/google/android/tts/voicepack/VoiceDownloadHelper;->getDownloadInfo(J)Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->mDownloadInfo:Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;

    iget-object v3, p0, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->mDownloadInfo:Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;

    if-eqz v3, :cond_0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->mDownloadInfo:Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;

    iget v3, v3, Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;->mStatus:I

    and-int/lit8 v3, v3, 0x8

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->mDownloadInfo:Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;

    iget-object v3, v3, Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;->mDownloadLocale:Ljava/lang/String;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->mDownloadInfo:Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;

    iget-object v3, v3, Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;->mFileName:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->processDownloadedFile(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v2, p0, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->mApp:Lcom/google/android/tts/GoogleTtsApplication;

    invoke-virtual {v2}, Lcom/google/android/tts/GoogleTtsApplication;->getVoiceDataManager()Lcom/google/android/tts/voicepack/VoiceDataManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/tts/voicepack/VoiceDataManager;->updateAvailableLangs()V

    iget-object v2, p0, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->mApp:Lcom/google/android/tts/GoogleTtsApplication;

    invoke-virtual {v2}, Lcom/google/android/tts/GoogleTtsApplication;->getLangCountryHelper()Lcom/google/android/tts/local/LangCountryHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/tts/local/LangCountryHelper;->updateLocaleList()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->mDownloadInfo:Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;

    iget-object v3, v3, Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;->mDownloadLocale:Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->mDownloadManager:Lcom/google/android/tts/voicepack/VoiceDownloadHelper;

    iget-object v4, p0, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->mDownloadInfo:Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;

    invoke-virtual {v3, v4}, Lcom/google/android/tts/voicepack/VoiceDownloadHelper;->markCompleted(Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;)V

    :cond_2
    iget-object v3, p0, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->mDownloadInfo:Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;

    iget-object v3, v3, Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;->mFileName:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->deleteDownloadedFile(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->mDownloadInfo:Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;

    iget-object v3, v3, Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;->mDownloadLocale:Ljava/lang/String;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->mDownloadManager:Lcom/google/android/tts/voicepack/VoiceDownloadHelper;

    iget-object v4, p0, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->mDownloadInfo:Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;

    invoke-virtual {v3, v4}, Lcom/google/android/tts/voicepack/VoiceDownloadHelper;->markCompleted(Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;)V

    :cond_4
    iget-object v3, p0, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->mDownloadInfo:Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;

    iget-object v3, v3, Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;->mFileName:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->deleteDownloadedFile(Ljava/lang/String;)V

    goto :goto_0

    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->mDownloadInfo:Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;

    iget-object v3, v3, Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;->mDownloadLocale:Ljava/lang/String;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->mDownloadManager:Lcom/google/android/tts/voicepack/VoiceDownloadHelper;

    iget-object v4, p0, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->mDownloadInfo:Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;

    invoke-virtual {v3, v4}, Lcom/google/android/tts/voicepack/VoiceDownloadHelper;->markCompleted(Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;)V

    :cond_5
    iget-object v3, p0, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->mDownloadInfo:Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;

    iget-object v3, v3, Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;->mFileName:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->deleteDownloadedFile(Ljava/lang/String;)V

    throw v2
.end method

.method private openDownloadsActivity(Landroid/content/Intent;)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->mContext:Landroid/content/Context;

    const-class v2, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private processDownloadedFile(Ljava/lang/String;)Z
    .locals 18
    .param p1    # Ljava/lang/String;

    :try_start_0
    new-instance v7, Ljava/util/zip/ZipFile;

    new-instance v15, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v15, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-direct {v7, v15, v0}, Ljava/util/zip/ZipFile;-><init>(Ljava/io/File;I)V

    invoke-virtual {v7}, Ljava/util/zip/ZipFile;->entries()Ljava/util/Enumeration;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v15

    if-eqz v15, :cond_6

    invoke-interface {v4}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/zip/ZipEntry;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->mContext:Landroid/content/Context;

    const-string v16, "patts"

    const/16 v17, 0x0

    invoke-virtual/range {v15 .. v17}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v5}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v6

    new-instance v12, Ljava/io/File;

    invoke-direct {v12, v3, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v12}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v5}, Ljava/util/zip/ZipEntry;->isDirectory()Z

    move-result v15

    if-eqz v15, :cond_1

    invoke-virtual {v12}, Ljava/io/File;->mkdirs()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v8

    const-string v15, "TTS.DownloadBroadcastReceiver"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Exception processing downloaded file :"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v15, 0x0

    :goto_1
    return v15

    :cond_1
    const/4 v9, 0x0

    const/4 v10, 0x0

    :try_start_1
    invoke-virtual {v7, v5}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    move-result-object v9

    new-instance v11, Ljava/io/FileOutputStream;

    invoke-direct {v11, v12}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v2, 0x0

    const/4 v14, 0x0

    const/high16 v15, 0x10000

    :try_start_2
    new-array v1, v15, [B

    :goto_2
    invoke-virtual {v9, v1}, Ljava/io/InputStream;->read([B)I

    move-result v2

    if-lez v2, :cond_2

    const/4 v15, 0x0

    invoke-virtual {v11, v1, v15, v2}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    add-int/2addr v14, v2

    goto :goto_2

    :cond_2
    if-eqz v9, :cond_3

    :try_start_3
    invoke-virtual {v9}, Ljava/io/InputStream;->close()V

    :cond_3
    if-eqz v11, :cond_0

    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V

    goto :goto_0

    :catchall_0
    move-exception v15

    :goto_3
    if-eqz v9, :cond_4

    invoke-virtual {v9}, Ljava/io/InputStream;->close()V

    :cond_4
    if-eqz v10, :cond_5

    invoke-virtual {v10}, Ljava/io/FileOutputStream;->close()V

    :cond_5
    throw v15

    :cond_6
    invoke-virtual {v7}, Ljava/util/zip/ZipFile;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    const/4 v15, 0x1

    goto :goto_1

    :catchall_1
    move-exception v15

    move-object v10, v11

    goto :goto_3
.end method

.method private showDownloadCompleteNotification(Z)V
    .locals 4
    .param p1    # Z

    iget-object v2, p0, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->mDownloadInfo:Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->mContext:Landroid/content/Context;

    const-string v3, "notification"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iget-object v2, p0, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->mDownloadInfo:Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;

    iget-object v2, v2, Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;->mDownloadLocale:Ljava/lang/String;

    invoke-direct {p0, v2, p1}, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->buildNotification(Ljava/lang/String;Z)Landroid/app/Notification;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    iput-object p1, p0, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->mContext:Landroid/content/Context;

    const-string v1, "android.intent.action.DOWNLOAD_COMPLETE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p1}, Lcom/google/android/tts/GoogleTtsApplication;->get(Landroid/content/Context;)Lcom/google/android/tts/GoogleTtsApplication;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->mApp:Lcom/google/android/tts/GoogleTtsApplication;

    iget-object v1, p0, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->mApp:Lcom/google/android/tts/GoogleTtsApplication;

    invoke-virtual {v1}, Lcom/google/android/tts/GoogleTtsApplication;->getVoiceDownloadManager()Lcom/google/android/tts/voicepack/VoiceDownloadHelper;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->mDownloadManager:Lcom/google/android/tts/voicepack/VoiceDownloadHelper;

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->goAsync()Landroid/content/BroadcastReceiver$PendingResult;

    move-result-object v0

    new-instance v1, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver$1;

    invoke-direct {v1, p0, p2, v0}, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver$1;-><init>(Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;Landroid/content/Intent;Landroid/content/BroadcastReceiver$PendingResult;)V

    invoke-virtual {v1}, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver$1;->start()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "android.intent.action.DOWNLOAD_NOTIFICATION_CLICKED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, p2}, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->openDownloadsActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
