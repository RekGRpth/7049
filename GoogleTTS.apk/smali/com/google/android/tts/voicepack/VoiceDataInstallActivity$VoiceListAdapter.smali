.class final Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter;
.super Landroid/widget/BaseAdapter;
.source "VoiceDataInstallActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "VoiceListAdapter"
.end annotation


# instance fields
.field private final mInflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;


# direct methods
.method constructor <init>(Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter;->this$0:Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter;->mInflater:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter;->this$0:Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;

    # getter for: Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->mVoiceList:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->access$100(Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter;->this$0:Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;

    # getter for: Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->mVoiceList:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->access$100(Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 14
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    if-eqz p2, :cond_0

    move-object/from16 v5, p2

    :goto_0
    const v7, 0x7f070003

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v7, 0x7f070004

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v7, 0x7f070005

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iget-object v7, p0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter;->this$0:Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;

    # getter for: Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->mVoiceList:Ljava/util/List;
    invoke-static {v7}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->access$100(Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v7, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;

    iget-object v7, p0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter;->this$0:Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;

    # invokes: Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->formatText1(Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;)Ljava/lang/String;
    invoke-static {v7, v6}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->access$200(Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v7, p0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter;->this$0:Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;

    # getter for: Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->mVoiceDataManager:Lcom/google/android/tts/voicepack/VoiceDataManager;
    invoke-static {v7}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->access$300(Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;)Lcom/google/android/tts/voicepack/VoiceDataManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/tts/voicepack/VoiceDataManager;->getAvailablePattsLocales()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v6}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->getLocale()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v2, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter;->this$0:Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;

    const v8, 0x7f060010

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter;->this$0:Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;

    iget-object v12, p0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter;->this$0:Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;

    # getter for: Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->mVoiceDataManager:Lcom/google/android/tts/voicepack/VoiceDataManager;
    invoke-static {v12}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->access$300(Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;)Lcom/google/android/tts/voicepack/VoiceDataManager;

    move-result-object v12

    invoke-virtual {v6}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->getLocale()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/google/android/tts/voicepack/VoiceDataManager;->getInstalledSize(Ljava/lang/String;)J

    move-result-wide v12

    invoke-static {v11, v12, v13}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v7, v8, v9}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v7, 0x7f06000e

    invoke-virtual {v1, v7}, Landroid/widget/Button;->setText(I)V

    new-instance v7, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter$1;

    invoke-direct {v7, p0, v6}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter$1;-><init>(Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter;Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;)V

    invoke-virtual {v1, v7}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_1
    return-object v5

    :cond_0
    iget-object v7, p0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v8, 0x7f020001

    const/4 v9, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v7, v8, v0, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    goto/16 :goto_0

    :cond_1
    iget-object v7, p0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter;->this$0:Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;

    # getter for: Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->mDownloadManager:Lcom/google/android/tts/voicepack/VoiceDownloadHelper;
    invoke-static {v7}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->access$500(Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;)Lcom/google/android/tts/voicepack/VoiceDownloadHelper;

    move-result-object v7

    invoke-virtual {v6}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->getLocale()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/tts/voicepack/VoiceDownloadHelper;->isActiveDownload(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    const v7, 0x7f06000b

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setText(I)V

    const v7, 0x7f06000d

    invoke-virtual {v1, v7}, Landroid/widget/Button;->setText(I)V

    new-instance v7, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter$2;

    invoke-direct {v7, p0, v6}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter$2;-><init>(Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter;Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;)V

    invoke-virtual {v1, v7}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    :cond_2
    iget-object v7, p0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter;->this$0:Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;

    # invokes: Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->formatText2(Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;)Ljava/lang/String;
    invoke-static {v7, v6}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->access$600(Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v7, 0x7f06000f

    invoke-virtual {v1, v7}, Landroid/widget/Button;->setText(I)V

    new-instance v7, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter$3;

    invoke-direct {v7, p0, v6}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter$3;-><init>(Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter;Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;)V

    invoke-virtual {v1, v7}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method
