.class Lcom/google/android/tts/voicepack/VoiceDataManager$FileInfo;
.super Ljava/lang/Object;
.source "VoiceDataManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/tts/voicepack/VoiceDataManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FileInfo"
.end annotation


# instance fields
.field final mIsApk:Z

.field final mSize:J


# direct methods
.method constructor <init>(ZJ)V
    .locals 0
    .param p1    # Z
    .param p2    # J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/google/android/tts/voicepack/VoiceDataManager$FileInfo;->mIsApk:Z

    iput-wide p2, p0, Lcom/google/android/tts/voicepack/VoiceDataManager$FileInfo;->mSize:J

    return-void
.end method
