.class public Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;
.super Lcom/google/speech/patts/api/common/SpeechSynthesizer;
.source "AndroidSpeechSynthesizer.java"


# instance fields
.field private assetManager:Landroid/content/res/AssetManager;

.field private dataInApk:Z


# direct methods
.method public constructor <init>(Landroid/content/res/AssetManager;)V
    .locals 1
    .param p1    # Landroid/content/res/AssetManager;

    invoke-direct {p0}, Lcom/google/speech/patts/api/common/SpeechSynthesizer;-><init>()V

    iput-object p1, p0, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->assetManager:Landroid/content/res/AssetManager;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->dataInApk:Z

    return-void
.end method


# virtual methods
.method public enableDataInApk(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->dataInApk:Z

    return-void
.end method

.method protected initializeProject(J)J
    .locals 3
    .param p1    # J

    const-string v0, "patts"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Loading project file (apk mode is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->dataInApk:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") ..."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->assetManager:Landroid/content/res/AssetManager;

    iget-boolean v1, p0, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->dataInApk:Z

    invoke-static {p1, p2, v0, v1}, Lcom/google/speech/patts/engine/api/ProjectFileApi;->newProjectFile(JLjava/lang/Object;Z)J

    move-result-wide v0

    return-wide v0
.end method

.method protected loadJni()Z
    .locals 1

    invoke-static {}, Lcom/google/speech/patts/api/android/StaticJniLoader;->loadJni()Z

    move-result v0

    return v0
.end method

.method protected logError(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-static {p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
