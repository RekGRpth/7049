.class public final Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;
.super Ljava/lang/Object;
.source "BufferedSynthesisHandle.java"


# instance fields
.field private id:J

.field private outputHandle:J

.field private stateHandle:J

.field private status:Lcom/google/speech/patts/engine/api/SynthesisStatus;

.field private synthesizerHandle:J


# direct methods
.method protected constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->initialize(J)V

    return-void
.end method

.method public constructor <init>(J)V
    .locals 0
    .param p1    # J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0, p1, p2}, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->initialize(J)V

    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 6

    const-wide/16 v4, 0x0

    iget-wide v0, p0, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->outputHandle:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->synthesizerHandle:J

    iget-wide v2, p0, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->outputHandle:J

    invoke-static {v0, v1, v2, v3}, Lcom/google/speech/patts/engine/api/CommonApi;->deleteSynthesisOutput(JJ)V

    iput-wide v4, p0, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->outputHandle:J

    :cond_0
    iget-wide v0, p0, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->stateHandle:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->stateHandle:J

    invoke-static {v0, v1}, Lcom/google/speech/patts/engine/api/CommonApi;->deleteSynthesisState(J)V

    iput-wide v4, p0, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->stateHandle:J

    :cond_1
    iput-wide v4, p0, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->synthesizerHandle:J

    return-void
.end method

.method public getOutputHandle()J
    .locals 2

    iget-wide v0, p0, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->outputHandle:J

    return-wide v0
.end method

.method public getStateHandle()J
    .locals 2

    iget-wide v0, p0, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->stateHandle:J

    return-wide v0
.end method

.method protected initialize(J)V
    .locals 3
    .param p1    # J

    const-wide/16 v1, 0x0

    iput-wide p1, p0, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->synthesizerHandle:J

    iput-wide v1, p0, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->outputHandle:J

    iput-wide v1, p0, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->stateHandle:J

    sget-object v0, Lcom/google/speech/patts/engine/api/SynthesisStatus;->STATUS_INITIAL:Lcom/google/speech/patts/engine/api/SynthesisStatus;

    iput-object v0, p0, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->status:Lcom/google/speech/patts/engine/api/SynthesisStatus;

    iput-wide v1, p0, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->id:J

    return-void
.end method

.method public isInProgress()Z
    .locals 2

    iget-object v0, p0, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->status:Lcom/google/speech/patts/engine/api/SynthesisStatus;

    sget-object v1, Lcom/google/speech/patts/engine/api/SynthesisStatus;->STATUS_IN_PROGRESS:Lcom/google/speech/patts/engine/api/SynthesisStatus;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setId(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->id:J

    return-void
.end method

.method public setOutputHandle(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->outputHandle:J

    return-void
.end method

.method public setStateHandle(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->stateHandle:J

    return-void
.end method

.method public setStatus(Lcom/google/speech/patts/engine/api/SynthesisStatus;)V
    .locals 0
    .param p1    # Lcom/google/speech/patts/engine/api/SynthesisStatus;

    iput-object p1, p0, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->status:Lcom/google/speech/patts/engine/api/SynthesisStatus;

    return-void
.end method
