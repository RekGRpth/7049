.class public abstract Lcom/google/speech/patts/api/common/SpeechSynthesizer;
.super Ljava/lang/Object;
.source "SpeechSynthesizer.java"


# instance fields
.field private numRequests:J

.field private projectFileHandle:J

.field private projectResourceHolderHandle:J

.field private synthesizerHandle:J

.field private textNormEnabled:Z

.field private textNormHandle:J


# direct methods
.method protected constructor <init>()V
    .locals 2

    const-wide/16 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->synthesizerHandle:J

    iput-wide v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->textNormHandle:J

    iput-wide v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->projectFileHandle:J

    iput-wide v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->projectResourceHolderHandle:J

    iput-wide v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->numRequests:J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->textNormEnabled:Z

    return-void
.end method

.method private appendFileBytes(Ljava/io/InputStream;Ljava/io/ByteArrayOutputStream;)V
    .locals 3
    .param p1    # Ljava/io/InputStream;
    .param p2    # Ljava/io/ByteArrayOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v2, 0x800

    new-array v1, v2, [B

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1}, Ljava/io/InputStream;->read([B)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2, v0}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private prepareText(Ljava/lang/String;Lcom/google/speech/patts/engine/api/SentenceControls;)J
    .locals 10
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/speech/patts/engine/api/SentenceControls;

    const-wide/16 v8, 0x0

    const-wide/16 v6, 0x0

    iget-boolean v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->textNormEnabled:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->synthesizerHandle:J

    invoke-static {v0, v1, p1}, Lcom/google/speech/patts/engine/api/TextAnalysisApi;->wlStringToWordIds(JLjava/lang/String;)J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-nez v0, :cond_1

    const-string v0, "patts"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to convert in-vocabulary (wl) words \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" to utterance"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->logError(Ljava/lang/String;Ljava/lang/String;)V

    move-wide v0, v8

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->synthesizerHandle:J

    iget-wide v2, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->textNormHandle:J

    move-object v4, p1

    move-object v5, p2

    invoke-static/range {v0 .. v5}, Lcom/google/speech/patts/engine/api/TextAnalysisApi;->textStringToWordIds(JJLjava/lang/String;Lcom/google/speech/patts/engine/api/SentenceControls;)J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-nez v0, :cond_1

    const-string v0, "patts"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to normalize \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->logError(Ljava/lang/String;Ljava/lang/String;)V

    move-wide v0, v8

    goto :goto_0

    :cond_1
    move-wide v0, v6

    goto :goto_0
.end method

.method private varargs readFileListToByteArray([Ljava/io/InputStream;)[B
    .locals 6
    .param p1    # [Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    array-length v5, p1

    if-nez v5, :cond_0

    const/4 v5, 0x0

    :goto_0
    return-object v5

    :cond_0
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    const v5, 0x8000

    invoke-direct {v3, v5}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    move-object v0, p1

    array-length v2, v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_1

    aget-object v4, v0, v1

    invoke-direct {p0, v4, v3}, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->appendFileBytes(Ljava/io/InputStream;Ljava/io/ByteArrayOutputStream;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    goto :goto_0
.end method


# virtual methods
.method public final getSampleRate()I
    .locals 2

    iget-wide v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->synthesizerHandle:J

    invoke-static {v0, v1}, Lcom/google/speech/patts/engine/api/HmmSynthesizerApi;->hmmGetVoiceSampleRate(J)I

    move-result v0

    return v0
.end method

.method public initBufferedSynthesis(Ljava/lang/String;Lcom/google/speech/patts/engine/api/SentenceControls;)Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/speech/patts/engine/api/SentenceControls;

    const-wide/16 v7, 0x0

    const/4 v4, 0x0

    invoke-direct {p0, p1, p2}, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->prepareText(Ljava/lang/String;Lcom/google/speech/patts/engine/api/SentenceControls;)J

    move-result-wide v0

    cmp-long v5, v0, v7

    if-nez v5, :cond_0

    const-string v5, "patts"

    const-string v6, "Failed to prepare text"

    invoke-virtual {p0, v5, v6}, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->logError(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v4

    :cond_0
    iget-wide v5, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->synthesizerHandle:J

    invoke-static {v5, v6, v0, v1}, Lcom/google/speech/patts/engine/api/HmmSynthesizerApi;->wordIdsToUnits(JJ)Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, "patts"

    const-string v6, "Failed to synthesize"

    invoke-virtual {p0, v5, v6}, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->logError(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-wide v5, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->synthesizerHandle:J

    invoke-static {v5, v6, v0, v1}, Lcom/google/speech/patts/engine/api/HmmSynthesizerApi;->hmmInitBuffered(JJ)J

    move-result-wide v2

    cmp-long v5, v2, v7

    if-nez v5, :cond_2

    const-string v5, "patts"

    const-string v6, "Failed to inialize buffered synthesizer"

    invoke-virtual {p0, v5, v6}, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->logError(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    new-instance v4, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;

    iget-wide v5, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->synthesizerHandle:J

    invoke-direct {v4, v5, v6}, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;-><init>(J)V

    iget-wide v5, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->numRequests:J

    const-wide/16 v7, 0x1

    add-long/2addr v5, v7

    iput-wide v5, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->numRequests:J

    invoke-virtual {v4, v5, v6}, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->setId(J)V

    invoke-virtual {v4, v0, v1}, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->setOutputHandle(J)V

    invoke-virtual {v4, v2, v3}, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->setStateHandle(J)V

    sget-object v5, Lcom/google/speech/patts/engine/api/SynthesisStatus;->STATUS_INITIAL:Lcom/google/speech/patts/engine/api/SynthesisStatus;

    invoke-virtual {v4, v5}, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->setStatus(Lcom/google/speech/patts/engine/api/SynthesisStatus;)V

    goto :goto_0
.end method

.method public initialize(Ljava/io/InputStream;Ljava/lang/String;)Z
    .locals 11
    .param p1    # Ljava/io/InputStream;
    .param p2    # Ljava/lang/String;

    const/4 v4, 0x1

    const-wide/16 v9, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->loadJni()Z

    move-result v5

    if-nez v5, :cond_0

    const-string v4, "patts"

    const-string v5, "Failed to load JNI"

    invoke-virtual {p0, v4, v5}, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->logError(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return v3

    :cond_0
    const-wide/16 v1, -0x1

    const/4 v5, 0x1

    :try_start_0
    new-array v5, v5, [Ljava/io/InputStream;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    invoke-direct {p0, v5}, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->readFileListToByteArray([Ljava/io/InputStream;)[B

    move-result-object v5

    invoke-static {v5}, Lcom/google/speech/patts/engine/api/FileResourceApi;->memoryResourceFromBuffer([B)J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v1

    cmp-long v5, v1, v9

    if-nez v5, :cond_1

    const-string v4, "patts"

    const-string v5, "Failed to read project file to memory buffer"

    invoke-virtual {p0, v4, v5}, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->logError(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v4, "patts"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to open project file: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->logError(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v1, v2}, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->initializeProject(J)J

    move-result-wide v5

    iput-wide v5, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->projectFileHandle:J

    invoke-static {v1, v2}, Lcom/google/speech/patts/engine/api/FileResourceApi;->deleteFileResource(J)V

    iget-wide v5, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->projectFileHandle:J

    cmp-long v5, v5, v9

    if-nez v5, :cond_2

    const-string v4, "patts"

    const-string v5, "Failed to initialize project file"

    invoke-virtual {p0, v4, v5}, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->logError(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    if-eqz p2, :cond_3

    iget-wide v5, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->projectFileHandle:J

    invoke-static {v5, v6, p2}, Lcom/google/speech/patts/engine/api/ProjectFileApi;->projectFileSetPrefix(JLjava/lang/String;)V

    :cond_3
    invoke-static {}, Lcom/google/speech/patts/engine/api/ProjectFileApi;->newProjectResourceHolder()J

    move-result-wide v5

    iput-wide v5, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->projectResourceHolderHandle:J

    iget-wide v5, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->projectFileHandle:J

    iget-wide v7, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->projectResourceHolderHandle:J

    invoke-static {v5, v6, v7, v8}, Lcom/google/speech/patts/engine/api/TextAnalysisApi;->createTextNormalizer(JJ)J

    move-result-wide v5

    iput-wide v5, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->textNormHandle:J

    iget-wide v5, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->textNormHandle:J

    cmp-long v5, v5, v9

    if-nez v5, :cond_4

    const-string v4, "patts"

    const-string v5, "Failed to load and initialize text normalizer"

    invoke-virtual {p0, v4, v5}, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->logError(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    iget-wide v5, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->projectFileHandle:J

    iget-wide v7, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->projectResourceHolderHandle:J

    invoke-static {v5, v6, v7, v8}, Lcom/google/speech/patts/engine/api/HmmSynthesizerApi;->hmmCreateSynthesizer(JJ)J

    move-result-wide v5

    iput-wide v5, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->synthesizerHandle:J

    iget-wide v5, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->synthesizerHandle:J

    cmp-long v5, v5, v9

    if-nez v5, :cond_5

    const-string v4, "patts"

    const-string v5, "Failed to load and initialize synthesizer"

    invoke-virtual {p0, v4, v5}, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->logError(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    move v3, v4

    goto/16 :goto_0
.end method

.method protected abstract initializeProject(J)J
.end method

.method public final isInitialized()Z
    .locals 4

    const-wide/16 v2, 0x0

    iget-wide v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->synthesizerHandle:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->textNormHandle:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->projectResourceHolderHandle:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->projectFileHandle:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract loadJni()Z
.end method

.method protected abstract logError(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public shutdown()V
    .locals 4

    const-wide/16 v2, 0x0

    iget-wide v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->textNormHandle:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->textNormHandle:J

    invoke-static {v0, v1}, Lcom/google/speech/patts/engine/api/TextAnalysisApi;->deleteTextNormalizer(J)V

    iput-wide v2, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->textNormHandle:J

    :cond_0
    iget-wide v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->synthesizerHandle:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->synthesizerHandle:J

    invoke-static {v0, v1}, Lcom/google/speech/patts/engine/api/HmmSynthesizerApi;->hmmDeleteSynthesizer(J)V

    iput-wide v2, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->synthesizerHandle:J

    :cond_1
    iget-wide v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->projectFileHandle:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    iget-wide v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->projectFileHandle:J

    invoke-static {v0, v1}, Lcom/google/speech/patts/engine/api/ProjectFileApi;->deleteProjectFile(J)V

    iput-wide v2, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->projectFileHandle:J

    :cond_2
    iget-wide v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->projectResourceHolderHandle:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3

    iget-wide v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->projectResourceHolderHandle:J

    invoke-static {v0, v1}, Lcom/google/speech/patts/engine/api/ProjectFileApi;->deleteProjectResourceHolder(J)V

    iput-wide v2, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->projectResourceHolderHandle:J

    :cond_3
    return-void
.end method

.method public synthesizeToDirectBuffer(Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;Ljava/nio/ByteBuffer;)I
    .locals 10
    .param p1    # Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;
    .param p2    # Ljava/nio/ByteBuffer;

    const/4 v9, -0x1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid buffered synthesis handle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    iget-wide v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->synthesizerHandle:J

    invoke-virtual {p1}, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->getOutputHandle()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->getStateHandle()J

    move-result-wide v5

    move-object v4, p2

    invoke-static/range {v0 .. v6}, Lcom/google/speech/patts/engine/api/HmmSynthesizerApi;->hmmWaveGenerationToByteBuffer(JJLjava/nio/ByteBuffer;J)I

    move-result v7

    invoke-virtual {p1}, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->getStateHandle()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/speech/patts/engine/api/CommonApi;->getSynthesisStatus(J)Lcom/google/speech/patts/engine/api/SynthesisStatus;

    move-result-object v8

    sget-object v0, Lcom/google/speech/patts/engine/api/SynthesisStatus;->STATUS_FAILED:Lcom/google/speech/patts/engine/api/SynthesisStatus;

    if-ne v8, v0, :cond_1

    const-string v0, "patts"

    const-string v1, "Buffered synthesis failed"

    invoke-virtual {p0, v0, v1}, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->logError(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/google/speech/patts/engine/api/SynthesisStatus;->STATUS_FAILED:Lcom/google/speech/patts/engine/api/SynthesisStatus;

    invoke-virtual {p1, v0}, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->setStatus(Lcom/google/speech/patts/engine/api/SynthesisStatus;)V

    move v7, v9

    :goto_0
    return v7

    :cond_1
    if-gez v7, :cond_2

    const-string v0, "patts"

    const-string v1, "Buffered synthesis failed: invalid number of samples returned"

    invoke-virtual {p0, v0, v1}, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->logError(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/google/speech/patts/engine/api/SynthesisStatus;->STATUS_FAILED:Lcom/google/speech/patts/engine/api/SynthesisStatus;

    invoke-virtual {p1, v0}, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->setStatus(Lcom/google/speech/patts/engine/api/SynthesisStatus;)V

    move v7, v9

    goto :goto_0

    :cond_2
    invoke-virtual {p1, v8}, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->setStatus(Lcom/google/speech/patts/engine/api/SynthesisStatus;)V

    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    invoke-virtual {p2, v7}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    goto :goto_0
.end method
