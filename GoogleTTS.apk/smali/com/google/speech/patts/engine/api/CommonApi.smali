.class public final Lcom/google/speech/patts/engine/api/CommonApi;
.super Lcom/google/speech/patts/engine/api/ApiBase;
.source "CommonApi.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/speech/patts/engine/api/ApiBase;-><init>()V

    return-void
.end method

.method public static native compileLinguisticAttributes(J)J
.end method

.method public static native deleteLinguisticAttributes(J)V
.end method

.method public static native deleteSynthesisOutput(JJ)V
.end method

.method public static native deleteSynthesisState(J)V
.end method

.method public static native getSynthesisStatus(J)Lcom/google/speech/patts/engine/api/SynthesisStatus;
.end method

.method public static native getVoiceType(J)Lcom/google/speech/patts/engine/api/VoiceType;
.end method

.method public static native getWaveData(J)Lcom/google/speech/patts/engine/api/WaveData;
.end method
