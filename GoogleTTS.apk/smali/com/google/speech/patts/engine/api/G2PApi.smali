.class public Lcom/google/speech/patts/engine/api/G2PApi;
.super Ljava/lang/Object;
.source "G2PApi.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static native getPronsFromG2P(JLjava/lang/String;Ljava/util/List;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation
.end method

.method public static native loadG2PData(J)J
.end method

.method public static native releaseG2PData(J)Z
.end method
