.class public Lcom/google/speech/patts/engine/api/SentenceControls;
.super Ljava/lang/Object;
.source "SentenceControls.java"


# instance fields
.field private pitchAddFactor:F

.field private pitchMultFactor:F

.field private speakingRate:F


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lcom/google/speech/patts/engine/api/SentenceControls;->speakingRate:F

    const/high16 v0, 0x3f800000

    iput v0, p0, Lcom/google/speech/patts/engine/api/SentenceControls;->pitchMultFactor:F

    iput v1, p0, Lcom/google/speech/patts/engine/api/SentenceControls;->pitchAddFactor:F

    return-void
.end method


# virtual methods
.method public getPitchAddFactor()F
    .locals 1

    iget v0, p0, Lcom/google/speech/patts/engine/api/SentenceControls;->pitchAddFactor:F

    return v0
.end method

.method public getPitchMultFactor()F
    .locals 1

    iget v0, p0, Lcom/google/speech/patts/engine/api/SentenceControls;->pitchMultFactor:F

    return v0
.end method

.method public getSpeakingRate()F
    .locals 1

    iget v0, p0, Lcom/google/speech/patts/engine/api/SentenceControls;->speakingRate:F

    return v0
.end method

.method public setPitchAddFactor(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/google/speech/patts/engine/api/SentenceControls;->pitchAddFactor:F

    return-void
.end method

.method public setPitchMultFactor(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/google/speech/patts/engine/api/SentenceControls;->pitchMultFactor:F

    return-void
.end method

.method public setSpeakingRate(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/google/speech/patts/engine/api/SentenceControls;->speakingRate:F

    return-void
.end method
