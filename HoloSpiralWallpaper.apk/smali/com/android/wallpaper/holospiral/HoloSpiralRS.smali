.class public Lcom/android/wallpaper/holospiral/HoloSpiralRS;
.super Ljava/lang/Object;
.source "HoloSpiralRS.java"


# static fields
.field private static final BG_COLOR_BLACK:I

.field private static final BG_COLOR_BLUE:I

.field private static final FAR_PLANE:F = 55.0f

.field private static final INNER_RADIUS:F = 5.0f

.field private static final INNER_SEPARATION_DEG:F = 23.0f

.field private static final INNER_SPIRAL_DEPTH:F = 50.0f

.field private static final LOG_TAG:Ljava/lang/String; = "HoloSpiralRS"

.field private static final MAX_POINT_SIZE:F = 75.0f

.field private static final NEAR_PLANE:F = 1.0f

.field private static final NUM_INNER_POINTS:I = 0x64

.field private static final NUM_OUTER_POINTS:I = 0x32

.field private static final OUTER_RADIUS:F = 10.0f

.field private static final OUTER_SEPARATION_DEG:F = 23.0f

.field private static final OUTER_SPIRAL_DEPTH:F = 30.0f

.field private static final POINTS_COLOR_AQUA:I

.field private static final POINTS_COLOR_BLUE:I

.field private static final POINTS_COLOR_GREEN:I


# instance fields
.field private mRS:Landroid/renderscript/RenderScriptGL;

.field private mResources:Landroid/content/res/Resources;

.field private mScript:Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/16 v6, 0x1a

    const/4 v5, 0x0

    const/16 v4, 0xff

    const/16 v0, 0xb3

    invoke-static {v0, v5, v5, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    sput v0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->POINTS_COLOR_BLUE:I

    const/16 v0, 0xd2

    const/16 v1, 0xa6

    const/16 v2, 0x33

    invoke-static {v0, v1, v2, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    sput v0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->POINTS_COLOR_GREEN:I

    const/16 v0, 0xdc

    const/16 v1, 0x26

    const/16 v2, 0x78

    const/16 v3, 0x94

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    sput v0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->POINTS_COLOR_AQUA:I

    const/16 v0, 0x53

    invoke-static {v4, v6, v6, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    sput v0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->BG_COLOR_BLACK:I

    const/16 v0, 0x8

    invoke-static {v4, v0, v5, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    sput v0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->BG_COLOR_BLUE:I

    return-void
.end method

.method public constructor <init>(Landroid/renderscript/RenderScriptGL;Landroid/content/res/Resources;)V
    .locals 1
    .param p1    # Landroid/renderscript/RenderScriptGL;
    .param p2    # Landroid/content/res/Resources;

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mRS:Landroid/renderscript/RenderScriptGL;

    iput-object v0, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mResources:Landroid/content/res/Resources;

    invoke-virtual {p0, p1, p2}, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->init(Landroid/renderscript/RenderScriptGL;Landroid/content/res/Resources;)V

    return-void
.end method

.method private static convertColor(I)Landroid/renderscript/Float4;
    .locals 6
    .param p0    # I

    const/high16 v5, 0x437f0000

    invoke-static {p0}, Landroid/graphics/Color;->red(I)I

    move-result v4

    int-to-float v4, v4

    div-float v3, v4, v5

    invoke-static {p0}, Landroid/graphics/Color;->green(I)I

    move-result v4

    int-to-float v4, v4

    div-float v2, v4, v5

    invoke-static {p0}, Landroid/graphics/Color;->blue(I)I

    move-result v4

    int-to-float v4, v4

    div-float v1, v4, v5

    invoke-static {p0}, Landroid/graphics/Color;->alpha(I)I

    move-result v4

    int-to-float v4, v4

    div-float v0, v4, v5

    new-instance v4, Landroid/renderscript/Float4;

    invoke-direct {v4, v3, v2, v1, v0}, Landroid/renderscript/Float4;-><init>(FFFF)V

    return-object v4
.end method

.method private createBackgroundMesh()V
    .locals 13

    const/4 v12, 0x1

    const/high16 v11, 0x3f800000

    const/4 v10, 0x0

    const/high16 v9, -0x40800000

    const/4 v8, 0x0

    new-instance v3, Lcom/android/wallpaper/holospiral/ScriptField_VertexColor_s;

    iget-object v6, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mRS:Landroid/renderscript/RenderScriptGL;

    const/4 v7, 0x4

    invoke-direct {v3, v6, v7}, Lcom/android/wallpaper/holospiral/ScriptField_VertexColor_s;-><init>(Landroid/renderscript/RenderScript;I)V

    new-instance v4, Landroid/renderscript/Float3;

    invoke-direct {v4, v9, v11, v10}, Landroid/renderscript/Float3;-><init>(FFF)V

    new-instance v1, Landroid/renderscript/Float3;

    invoke-direct {v1, v9, v9, v10}, Landroid/renderscript/Float3;-><init>(FFF)V

    new-instance v5, Landroid/renderscript/Float3;

    invoke-direct {v5, v11, v11, v10}, Landroid/renderscript/Float3;-><init>(FFF)V

    new-instance v2, Landroid/renderscript/Float3;

    invoke-direct {v2, v11, v9, v10}, Landroid/renderscript/Float3;-><init>(FFF)V

    invoke-virtual {v3, v8, v4, v8}, Lcom/android/wallpaper/holospiral/ScriptField_VertexColor_s;->set_position(ILandroid/renderscript/Float3;Z)V

    sget v6, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->BG_COLOR_BLUE:I

    invoke-static {v6}, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->convertColor(I)Landroid/renderscript/Float4;

    move-result-object v6

    invoke-virtual {v3, v8, v6, v8}, Lcom/android/wallpaper/holospiral/ScriptField_VertexColor_s;->set_color(ILandroid/renderscript/Float4;Z)V

    invoke-virtual {v3, v12, v1, v8}, Lcom/android/wallpaper/holospiral/ScriptField_VertexColor_s;->set_position(ILandroid/renderscript/Float3;Z)V

    sget v6, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->BG_COLOR_BLACK:I

    invoke-static {v6}, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->convertColor(I)Landroid/renderscript/Float4;

    move-result-object v6

    invoke-virtual {v3, v12, v6, v8}, Lcom/android/wallpaper/holospiral/ScriptField_VertexColor_s;->set_color(ILandroid/renderscript/Float4;Z)V

    const/4 v6, 0x2

    invoke-virtual {v3, v6, v5, v8}, Lcom/android/wallpaper/holospiral/ScriptField_VertexColor_s;->set_position(ILandroid/renderscript/Float3;Z)V

    const/4 v6, 0x2

    sget v7, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->BG_COLOR_BLUE:I

    invoke-static {v7}, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->convertColor(I)Landroid/renderscript/Float4;

    move-result-object v7

    invoke-virtual {v3, v6, v7, v8}, Lcom/android/wallpaper/holospiral/ScriptField_VertexColor_s;->set_color(ILandroid/renderscript/Float4;Z)V

    const/4 v6, 0x3

    invoke-virtual {v3, v6, v2, v8}, Lcom/android/wallpaper/holospiral/ScriptField_VertexColor_s;->set_position(ILandroid/renderscript/Float3;Z)V

    const/4 v6, 0x3

    sget v7, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->BG_COLOR_BLACK:I

    invoke-static {v7}, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->convertColor(I)Landroid/renderscript/Float4;

    move-result-object v7

    invoke-virtual {v3, v6, v7, v8}, Lcom/android/wallpaper/holospiral/ScriptField_VertexColor_s;->set_color(ILandroid/renderscript/Float4;Z)V

    invoke-virtual {v3}, Lcom/android/wallpaper/holospiral/ScriptField_VertexColor_s;->copyAll()V

    new-instance v0, Landroid/renderscript/Mesh$AllocationBuilder;

    iget-object v6, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v0, v6}, Landroid/renderscript/Mesh$AllocationBuilder;-><init>(Landroid/renderscript/RenderScript;)V

    sget-object v6, Landroid/renderscript/Mesh$Primitive;->TRIANGLE_STRIP:Landroid/renderscript/Mesh$Primitive;

    invoke-virtual {v0, v6}, Landroid/renderscript/Mesh$AllocationBuilder;->addIndexSetType(Landroid/renderscript/Mesh$Primitive;)Landroid/renderscript/Mesh$AllocationBuilder;

    invoke-virtual {v3}, Landroid/renderscript/Script$FieldBase;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/renderscript/Mesh$AllocationBuilder;->addVertexAllocation(Landroid/renderscript/Allocation;)Landroid/renderscript/Mesh$AllocationBuilder;

    iget-object v6, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mScript:Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;

    invoke-virtual {v0}, Landroid/renderscript/Mesh$AllocationBuilder;->create()Landroid/renderscript/Mesh;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->set_gBackgroundMesh(Landroid/renderscript/Mesh;)V

    return-void
.end method

.method private createFragmentPrograms()V
    .locals 6

    new-instance v0, Landroid/renderscript/ProgramFragment$Builder;

    iget-object v4, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v0, v4}, Landroid/renderscript/ProgramFragment$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    iget-object v4, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mResources:Landroid/content/res/Resources;

    const/high16 v5, 0x7f050000

    invoke-virtual {v0, v4, v5}, Landroid/renderscript/Program$BaseProgramBuilder;->setShader(Landroid/content/res/Resources;I)Landroid/renderscript/Program$BaseProgramBuilder;

    invoke-virtual {v0}, Landroid/renderscript/ProgramFragment$Builder;->create()Landroid/renderscript/ProgramFragment;

    move-result-object v2

    iget-object v4, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mScript:Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;

    invoke-virtual {v4, v2}, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->set_gPFBackground(Landroid/renderscript/ProgramFragment;)V

    new-instance v1, Landroid/renderscript/ProgramFragment$Builder;

    iget-object v4, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v1, v4}, Landroid/renderscript/ProgramFragment$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    iget-object v4, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mResources:Landroid/content/res/Resources;

    const v5, 0x7f050001

    invoke-virtual {v1, v4, v5}, Landroid/renderscript/Program$BaseProgramBuilder;->setShader(Landroid/content/res/Resources;I)Landroid/renderscript/Program$BaseProgramBuilder;

    sget-object v4, Landroid/renderscript/Program$TextureType;->TEXTURE_2D:Landroid/renderscript/Program$TextureType;

    invoke-virtual {v1, v4}, Landroid/renderscript/Program$BaseProgramBuilder;->addTexture(Landroid/renderscript/Program$TextureType;)Landroid/renderscript/Program$BaseProgramBuilder;

    invoke-virtual {v1}, Landroid/renderscript/ProgramFragment$Builder;->create()Landroid/renderscript/ProgramFragment;

    move-result-object v3

    iget-object v4, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-static {v4}, Landroid/renderscript/Sampler;->CLAMP_LINEAR(Landroid/renderscript/RenderScript;)Landroid/renderscript/Sampler;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/renderscript/Program;->bindSampler(Landroid/renderscript/Sampler;I)V

    iget-object v4, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mScript:Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;

    invoke-virtual {v4, v3}, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->set_gPFGeometry(Landroid/renderscript/ProgramFragment;)V

    return-void
.end method

.method private createPointGeometry()V
    .locals 14

    const/high16 v4, 0x41b80000

    new-instance v1, Lcom/android/wallpaper/holospiral/ScriptField_VertexColor_s;

    iget-object v0, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mRS:Landroid/renderscript/RenderScriptGL;

    const/16 v2, 0x64

    invoke-direct {v1, v0, v2}, Lcom/android/wallpaper/holospiral/ScriptField_VertexColor_s;-><init>(Landroid/renderscript/RenderScript;I)V

    const/high16 v2, 0x42480000

    const/high16 v3, 0x40a00000

    sget v5, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->POINTS_COLOR_BLUE:I

    sget v6, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->POINTS_COLOR_GREEN:I

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->generateSpiral(Lcom/android/wallpaper/holospiral/ScriptField_VertexColor_s;FFFII)V

    new-instance v12, Landroid/renderscript/Mesh$AllocationBuilder;

    iget-object v0, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v12, v0}, Landroid/renderscript/Mesh$AllocationBuilder;-><init>(Landroid/renderscript/RenderScript;)V

    sget-object v0, Landroid/renderscript/Mesh$Primitive;->POINT:Landroid/renderscript/Mesh$Primitive;

    invoke-virtual {v12, v0}, Landroid/renderscript/Mesh$AllocationBuilder;->addIndexSetType(Landroid/renderscript/Mesh$Primitive;)Landroid/renderscript/Mesh$AllocationBuilder;

    invoke-virtual {v1}, Landroid/renderscript/Script$FieldBase;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v0

    invoke-virtual {v12, v0}, Landroid/renderscript/Mesh$AllocationBuilder;->addVertexAllocation(Landroid/renderscript/Allocation;)Landroid/renderscript/Mesh$AllocationBuilder;

    iget-object v0, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mScript:Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;

    invoke-virtual {v12}, Landroid/renderscript/Mesh$AllocationBuilder;->create()Landroid/renderscript/Mesh;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->set_gInnerGeometry(Landroid/renderscript/Mesh;)V

    new-instance v6, Lcom/android/wallpaper/holospiral/ScriptField_VertexColor_s;

    iget-object v0, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mRS:Landroid/renderscript/RenderScriptGL;

    const/16 v2, 0x32

    invoke-direct {v6, v0, v2}, Lcom/android/wallpaper/holospiral/ScriptField_VertexColor_s;-><init>(Landroid/renderscript/RenderScript;I)V

    const/high16 v7, 0x41f00000

    const/high16 v8, 0x41200000

    sget v10, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->POINTS_COLOR_AQUA:I

    sget v11, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->POINTS_COLOR_AQUA:I

    move-object v5, p0

    move v9, v4

    invoke-direct/range {v5 .. v11}, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->generateSpiral(Lcom/android/wallpaper/holospiral/ScriptField_VertexColor_s;FFFII)V

    new-instance v13, Landroid/renderscript/Mesh$AllocationBuilder;

    iget-object v0, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v13, v0}, Landroid/renderscript/Mesh$AllocationBuilder;-><init>(Landroid/renderscript/RenderScript;)V

    sget-object v0, Landroid/renderscript/Mesh$Primitive;->POINT:Landroid/renderscript/Mesh$Primitive;

    invoke-virtual {v13, v0}, Landroid/renderscript/Mesh$AllocationBuilder;->addIndexSetType(Landroid/renderscript/Mesh$Primitive;)Landroid/renderscript/Mesh$AllocationBuilder;

    invoke-virtual {v6}, Landroid/renderscript/Script$FieldBase;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v0

    invoke-virtual {v13, v0}, Landroid/renderscript/Mesh$AllocationBuilder;->addVertexAllocation(Landroid/renderscript/Allocation;)Landroid/renderscript/Mesh$AllocationBuilder;

    iget-object v0, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mScript:Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;

    invoke-virtual {v13}, Landroid/renderscript/Mesh$AllocationBuilder;->create()Landroid/renderscript/Mesh;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->set_gOuterGeometry(Landroid/renderscript/Mesh;)V

    return-void
.end method

.method private createScript()V
    .locals 4

    new-instance v0, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;

    iget-object v1, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mRS:Landroid/renderscript/RenderScriptGL;

    iget-object v2, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f050002

    invoke-direct {v0, v1, v2, v3}, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;-><init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V

    iput-object v0, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mScript:Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;

    iget-object v0, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mScript:Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;

    const/high16 v1, 0x3f800000

    invoke-virtual {v0, v1}, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->set_gNearPlane(F)V

    iget-object v0, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mScript:Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;

    const/high16 v1, 0x425c0000

    invoke-virtual {v0, v1}, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->set_gFarPlane(F)V

    invoke-direct {p0}, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->createVertexPrograms()V

    invoke-direct {p0}, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->createFragmentPrograms()V

    invoke-direct {p0}, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->createStorePrograms()V

    invoke-direct {p0}, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->createPointGeometry()V

    invoke-direct {p0}, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->createBackgroundMesh()V

    invoke-direct {p0}, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->createTextures()V

    return-void
.end method

.method private createStorePrograms()V
    .locals 3

    new-instance v0, Landroid/renderscript/ProgramStore$Builder;

    iget-object v1, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v0, v1}, Landroid/renderscript/ProgramStore$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    sget-object v1, Landroid/renderscript/ProgramStore$BlendSrcFunc;->SRC_ALPHA:Landroid/renderscript/ProgramStore$BlendSrcFunc;

    sget-object v2, Landroid/renderscript/ProgramStore$BlendDstFunc;->ONE_MINUS_SRC_ALPHA:Landroid/renderscript/ProgramStore$BlendDstFunc;

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/ProgramStore$Builder;->setBlendFunc(Landroid/renderscript/ProgramStore$BlendSrcFunc;Landroid/renderscript/ProgramStore$BlendDstFunc;)Landroid/renderscript/ProgramStore$Builder;

    iget-object v1, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mScript:Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;

    invoke-virtual {v0}, Landroid/renderscript/ProgramStore$Builder;->create()Landroid/renderscript/ProgramStore;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->set_gPSGeometry(Landroid/renderscript/ProgramStore;)V

    sget-object v1, Landroid/renderscript/ProgramStore$BlendSrcFunc;->ONE:Landroid/renderscript/ProgramStore$BlendSrcFunc;

    sget-object v2, Landroid/renderscript/ProgramStore$BlendDstFunc;->ZERO:Landroid/renderscript/ProgramStore$BlendDstFunc;

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/ProgramStore$Builder;->setBlendFunc(Landroid/renderscript/ProgramStore$BlendSrcFunc;Landroid/renderscript/ProgramStore$BlendDstFunc;)Landroid/renderscript/ProgramStore$Builder;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/renderscript/ProgramStore$Builder;->setDitherEnabled(Z)Landroid/renderscript/ProgramStore$Builder;

    iget-object v1, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mScript:Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;

    invoke-virtual {v0}, Landroid/renderscript/ProgramStore$Builder;->create()Landroid/renderscript/ProgramStore;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->set_gPSBackground(Landroid/renderscript/ProgramStore;)V

    return-void
.end method

.method private createTextures()V
    .locals 5

    iget-object v2, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mResources:Landroid/content/res/Resources;

    const/high16 v3, 0x7f020000

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v2, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-static {v2, v0}, Landroid/renderscript/Allocation;->createFromBitmap(Landroid/renderscript/RenderScript;Landroid/graphics/Bitmap;)Landroid/renderscript/Allocation;

    move-result-object v1

    iget-object v2, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mScript:Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;

    invoke-virtual {v2, v1}, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->set_gPointTexture(Landroid/renderscript/Allocation;)V

    return-void
.end method

.method private createVertexPrograms()V
    .locals 8

    const/4 v7, 0x0

    new-instance v4, Lcom/android/wallpaper/holospiral/ScriptField_VertexShaderConstants_s;

    iget-object v5, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mRS:Landroid/renderscript/RenderScriptGL;

    const/4 v6, 0x1

    invoke-direct {v4, v5, v6}, Lcom/android/wallpaper/holospiral/ScriptField_VertexShaderConstants_s;-><init>(Landroid/renderscript/RenderScript;I)V

    iget-object v5, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mScript:Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;

    invoke-virtual {v5, v4}, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->bind_gVSConstants(Lcom/android/wallpaper/holospiral/ScriptField_VertexShaderConstants_s;)V

    const/high16 v5, 0x42960000

    invoke-virtual {v4, v7, v5, v7}, Lcom/android/wallpaper/holospiral/ScriptField_VertexShaderConstants_s;->set_maxPointSize(IFZ)V

    invoke-virtual {v4}, Lcom/android/wallpaper/holospiral/ScriptField_VertexShaderConstants_s;->copyAll()V

    new-instance v0, Landroid/renderscript/ProgramVertex$Builder;

    iget-object v5, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v0, v5}, Landroid/renderscript/ProgramVertex$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    iget-object v5, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mResources:Landroid/content/res/Resources;

    const v6, 0x7f050003

    invoke-virtual {v0, v5, v6}, Landroid/renderscript/Program$BaseProgramBuilder;->setShader(Landroid/content/res/Resources;I)Landroid/renderscript/Program$BaseProgramBuilder;

    iget-object v5, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-static {v5}, Lcom/android/wallpaper/holospiral/ScriptField_VertexColor_s;->createElement(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/renderscript/ProgramVertex$Builder;->addInput(Landroid/renderscript/Element;)Landroid/renderscript/ProgramVertex$Builder;

    invoke-virtual {v0}, Landroid/renderscript/ProgramVertex$Builder;->create()Landroid/renderscript/ProgramVertex;

    move-result-object v2

    iget-object v5, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mScript:Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;

    invoke-virtual {v5, v2}, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->set_gPVBackground(Landroid/renderscript/ProgramVertex;)V

    new-instance v1, Landroid/renderscript/ProgramVertex$Builder;

    iget-object v5, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v1, v5}, Landroid/renderscript/ProgramVertex$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    iget-object v5, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mResources:Landroid/content/res/Resources;

    const v6, 0x7f050004

    invoke-virtual {v1, v5, v6}, Landroid/renderscript/Program$BaseProgramBuilder;->setShader(Landroid/content/res/Resources;I)Landroid/renderscript/Program$BaseProgramBuilder;

    invoke-virtual {v4}, Landroid/renderscript/Script$FieldBase;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v5

    invoke-virtual {v5}, Landroid/renderscript/Allocation;->getType()Landroid/renderscript/Type;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/renderscript/Program$BaseProgramBuilder;->addConstant(Landroid/renderscript/Type;)Landroid/renderscript/Program$BaseProgramBuilder;

    iget-object v5, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-static {v5}, Lcom/android/wallpaper/holospiral/ScriptField_VertexColor_s;->createElement(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/renderscript/ProgramVertex$Builder;->addInput(Landroid/renderscript/Element;)Landroid/renderscript/ProgramVertex$Builder;

    invoke-virtual {v1}, Landroid/renderscript/ProgramVertex$Builder;->create()Landroid/renderscript/ProgramVertex;

    move-result-object v3

    invoke-virtual {v4}, Landroid/renderscript/Script$FieldBase;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v5

    invoke-virtual {v3, v5, v7}, Landroid/renderscript/Program;->bindConstants(Landroid/renderscript/Allocation;I)V

    iget-object v5, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mScript:Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;

    invoke-virtual {v5, v3}, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->set_gPVGeometry(Landroid/renderscript/ProgramVertex;)V

    return-void
.end method

.method private generateSpiral(Lcom/android/wallpaper/holospiral/ScriptField_VertexColor_s;FFFII)V
    .locals 16
    .param p1    # Lcom/android/wallpaper/holospiral/ScriptField_VertexColor_s;
    .param p2    # F
    .param p3    # F
    .param p4    # F
    .param p5    # I
    .param p6    # I

    const/high16 v13, 0x43b40000

    div-float v13, p4, v13

    const/high16 v14, 0x40000000

    mul-float/2addr v13, v14

    const v14, 0x40490fdb

    mul-float v11, v13, v14

    invoke-virtual/range {p1 .. p1}, Landroid/renderscript/Script$FieldBase;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v13

    invoke-virtual {v13}, Landroid/renderscript/Allocation;->getType()Landroid/renderscript/Type;

    move-result-object v13

    invoke-virtual {v13}, Landroid/renderscript/Type;->getX()I

    move-result v12

    const/high16 v13, 0x40000000

    div-float v2, p2, v13

    const/4 v9, 0x0

    invoke-static/range {p5 .. p5}, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->convertColor(I)Landroid/renderscript/Float4;

    move-result-object v7

    invoke-static/range {p6 .. p6}, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->convertColor(I)Landroid/renderscript/Float4;

    move-result-object v10

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v12, :cond_0

    int-to-float v13, v3

    int-to-float v14, v12

    div-float v5, v13, v14

    new-instance v6, Landroid/renderscript/Float3;

    float-to-double v13, v9

    invoke-static {v13, v14}, Ljava/lang/Math;->cos(D)D

    move-result-wide v13

    double-to-float v13, v13

    mul-float v13, v13, p3

    float-to-double v14, v9

    invoke-static {v14, v15}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    double-to-float v14, v14

    mul-float v14, v14, p3

    mul-float v15, v5, p2

    sub-float/2addr v15, v2

    invoke-direct {v6, v13, v14, v15}, Landroid/renderscript/Float3;-><init>(FFF)V

    const/high16 v13, 0x40000000

    div-float v13, v9, v13

    float-to-double v13, v13

    invoke-static {v13, v14}, Ljava/lang/Math;->sin(D)D

    move-result-wide v13

    double-to-float v8, v13

    new-instance v1, Landroid/renderscript/Float4;

    invoke-direct {v1}, Landroid/renderscript/Float4;-><init>()V

    iget v13, v7, Landroid/renderscript/Float4;->x:F

    iget v14, v10, Landroid/renderscript/Float4;->x:F

    iget v15, v7, Landroid/renderscript/Float4;->x:F

    sub-float/2addr v14, v15

    mul-float/2addr v14, v8

    add-float/2addr v13, v14

    iput v13, v1, Landroid/renderscript/Float4;->x:F

    iget v13, v7, Landroid/renderscript/Float4;->y:F

    iget v14, v10, Landroid/renderscript/Float4;->y:F

    iget v15, v7, Landroid/renderscript/Float4;->y:F

    sub-float/2addr v14, v15

    mul-float/2addr v14, v8

    add-float/2addr v13, v14

    iput v13, v1, Landroid/renderscript/Float4;->y:F

    iget v13, v7, Landroid/renderscript/Float4;->z:F

    iget v14, v10, Landroid/renderscript/Float4;->z:F

    iget v15, v7, Landroid/renderscript/Float4;->z:F

    sub-float/2addr v14, v15

    mul-float/2addr v14, v8

    add-float/2addr v13, v14

    iput v13, v1, Landroid/renderscript/Float4;->z:F

    iget v13, v7, Landroid/renderscript/Float4;->w:F

    iget v14, v10, Landroid/renderscript/Float4;->w:F

    iget v15, v7, Landroid/renderscript/Float4;->w:F

    sub-float/2addr v14, v15

    mul-float/2addr v14, v8

    add-float/2addr v13, v14

    iput v13, v1, Landroid/renderscript/Float4;->w:F

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6, v13}, Lcom/android/wallpaper/holospiral/ScriptField_VertexColor_s;->set_position(ILandroid/renderscript/Float3;Z)V

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v1, v13}, Lcom/android/wallpaper/holospiral/ScriptField_VertexColor_s;->set_color(ILandroid/renderscript/Float4;Z)V

    add-float/2addr v9, v11

    const v13, 0x40c90fdb

    div-float v13, v9, v13

    float-to-int v4, v13

    int-to-float v13, v4

    const/high16 v14, 0x40000000

    mul-float/2addr v13, v14

    const v14, 0x40490fdb

    mul-float/2addr v13, v14

    sub-float/2addr v9, v13

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/android/wallpaper/holospiral/ScriptField_VertexColor_s;->copyAll()V

    return-void
.end method


# virtual methods
.method public init(Landroid/renderscript/RenderScriptGL;Landroid/content/res/Resources;)V
    .locals 0
    .param p1    # Landroid/renderscript/RenderScriptGL;
    .param p2    # Landroid/content/res/Resources;

    iput-object p1, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mRS:Landroid/renderscript/RenderScriptGL;

    iput-object p2, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mResources:Landroid/content/res/Resources;

    invoke-direct {p0}, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->createScript()V

    return-void
.end method

.method public onCommand(Ljava/lang/String;IIILandroid/os/Bundle;Z)Landroid/os/Bundle;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Landroid/os/Bundle;
    .param p6    # Z

    const/4 v0, 0x0

    return-object v0
.end method

.method public resize(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mScript:Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;

    int-to-float v1, p1

    int-to-float v2, p2

    invoke-virtual {v0, v1, v2}, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->invoke_resize(FF)V

    return-void
.end method

.method public setOffset(FFII)V
    .locals 1
    .param p1    # F
    .param p2    # F
    .param p3    # I
    .param p4    # I

    iget-object v0, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mScript:Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;

    invoke-virtual {v0, p1}, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->set_gXOffset(F)V

    return-void
.end method

.method public start()V
    .locals 2

    iget-object v0, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mRS:Landroid/renderscript/RenderScriptGL;

    iget-object v1, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mScript:Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;

    invoke-virtual {v0, v1}, Landroid/renderscript/RenderScriptGL;->bindRootScript(Landroid/renderscript/Script;)V

    return-void
.end method

.method public stop()V
    .locals 2

    iget-object v0, p0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->mRS:Landroid/renderscript/RenderScriptGL;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/renderscript/RenderScriptGL;->bindRootScript(Landroid/renderscript/Script;)V

    return-void
.end method
