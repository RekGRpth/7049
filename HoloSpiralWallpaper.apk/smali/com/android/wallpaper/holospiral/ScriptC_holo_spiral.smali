.class public Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;
.super Landroid/renderscript/ScriptC;
.source "ScriptC_holo_spiral.java"


# static fields
.field private static final mExportFuncIdx_resize:I = 0x0

.field private static final mExportVarIdx_VertexColor_s_dummy:I = 0xe

.field private static final mExportVarIdx_gBackgroundMesh:I = 0x8

.field private static final mExportVarIdx_gFarPlane:I = 0xc

.field private static final mExportVarIdx_gInnerGeometry:I = 0x6

.field private static final mExportVarIdx_gNearPlane:I = 0xb

.field private static final mExportVarIdx_gOuterGeometry:I = 0x7

.field private static final mExportVarIdx_gPFBackground:I = 0x2

.field private static final mExportVarIdx_gPFGeometry:I = 0x3

.field private static final mExportVarIdx_gPSBackground:I = 0x4

.field private static final mExportVarIdx_gPSGeometry:I = 0x5

.field private static final mExportVarIdx_gPVBackground:I = 0x0

.field private static final mExportVarIdx_gPVGeometry:I = 0x1

.field private static final mExportVarIdx_gPointTexture:I = 0x9

.field private static final mExportVarIdx_gVSConstants:I = 0xd

.field private static final mExportVarIdx_gXOffset:I = 0xa


# instance fields
.field private __ALLOCATION:Landroid/renderscript/Element;

.field private __F32:Landroid/renderscript/Element;

.field private __MESH:Landroid/renderscript/Element;

.field private __PROGRAM_FRAGMENT:Landroid/renderscript/Element;

.field private __PROGRAM_STORE:Landroid/renderscript/Element;

.field private __PROGRAM_VERTEX:Landroid/renderscript/Element;

.field private mExportVar_VertexColor_s_dummy:Lcom/android/wallpaper/holospiral/ScriptField_VertexColor_s;

.field private mExportVar_gBackgroundMesh:Landroid/renderscript/Mesh;

.field private mExportVar_gFarPlane:F

.field private mExportVar_gInnerGeometry:Landroid/renderscript/Mesh;

.field private mExportVar_gNearPlane:F

.field private mExportVar_gOuterGeometry:Landroid/renderscript/Mesh;

.field private mExportVar_gPFBackground:Landroid/renderscript/ProgramFragment;

.field private mExportVar_gPFGeometry:Landroid/renderscript/ProgramFragment;

.field private mExportVar_gPSBackground:Landroid/renderscript/ProgramStore;

.field private mExportVar_gPSGeometry:Landroid/renderscript/ProgramStore;

.field private mExportVar_gPVBackground:Landroid/renderscript/ProgramVertex;

.field private mExportVar_gPVGeometry:Landroid/renderscript/ProgramVertex;

.field private mExportVar_gPointTexture:Landroid/renderscript/Allocation;

.field private mExportVar_gVSConstants:Lcom/android/wallpaper/holospiral/ScriptField_VertexShaderConstants_s;

.field private mExportVar_gXOffset:F


# direct methods
.method public constructor <init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V
    .locals 1
    .param p1    # Landroid/renderscript/RenderScript;
    .param p2    # Landroid/content/res/Resources;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/renderscript/ScriptC;-><init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V

    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_VERTEX(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->__PROGRAM_VERTEX:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_FRAGMENT(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->__PROGRAM_FRAGMENT:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_STORE(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->__PROGRAM_STORE:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->MESH(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->__MESH:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->ALLOCATION(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->__ALLOCATION:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->__F32:Landroid/renderscript/Element;

    return-void
.end method


# virtual methods
.method public bind_VertexColor_s_dummy(Lcom/android/wallpaper/holospiral/ScriptField_VertexColor_s;)V
    .locals 2
    .param p1    # Lcom/android/wallpaper/holospiral/ScriptField_VertexColor_s;

    const/16 v1, 0xe

    iput-object p1, p0, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->mExportVar_VertexColor_s_dummy:Lcom/android/wallpaper/holospiral/ScriptField_VertexColor_s;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/renderscript/Script;->bindAllocation(Landroid/renderscript/Allocation;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/renderscript/Script$FieldBase;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Landroid/renderscript/Script;->bindAllocation(Landroid/renderscript/Allocation;I)V

    goto :goto_0
.end method

.method public bind_gVSConstants(Lcom/android/wallpaper/holospiral/ScriptField_VertexShaderConstants_s;)V
    .locals 2
    .param p1    # Lcom/android/wallpaper/holospiral/ScriptField_VertexShaderConstants_s;

    const/16 v1, 0xd

    iput-object p1, p0, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->mExportVar_gVSConstants:Lcom/android/wallpaper/holospiral/ScriptField_VertexShaderConstants_s;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/renderscript/Script;->bindAllocation(Landroid/renderscript/Allocation;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/renderscript/Script$FieldBase;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Landroid/renderscript/Script;->bindAllocation(Landroid/renderscript/Allocation;I)V

    goto :goto_0
.end method

.method public get_VertexColor_s_dummy()Lcom/android/wallpaper/holospiral/ScriptField_VertexColor_s;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->mExportVar_VertexColor_s_dummy:Lcom/android/wallpaper/holospiral/ScriptField_VertexColor_s;

    return-object v0
.end method

.method public get_gBackgroundMesh()Landroid/renderscript/Mesh;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->mExportVar_gBackgroundMesh:Landroid/renderscript/Mesh;

    return-object v0
.end method

.method public get_gFarPlane()F
    .locals 1

    iget v0, p0, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->mExportVar_gFarPlane:F

    return v0
.end method

.method public get_gInnerGeometry()Landroid/renderscript/Mesh;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->mExportVar_gInnerGeometry:Landroid/renderscript/Mesh;

    return-object v0
.end method

.method public get_gNearPlane()F
    .locals 1

    iget v0, p0, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->mExportVar_gNearPlane:F

    return v0
.end method

.method public get_gOuterGeometry()Landroid/renderscript/Mesh;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->mExportVar_gOuterGeometry:Landroid/renderscript/Mesh;

    return-object v0
.end method

.method public get_gPFBackground()Landroid/renderscript/ProgramFragment;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->mExportVar_gPFBackground:Landroid/renderscript/ProgramFragment;

    return-object v0
.end method

.method public get_gPFGeometry()Landroid/renderscript/ProgramFragment;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->mExportVar_gPFGeometry:Landroid/renderscript/ProgramFragment;

    return-object v0
.end method

.method public get_gPSBackground()Landroid/renderscript/ProgramStore;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->mExportVar_gPSBackground:Landroid/renderscript/ProgramStore;

    return-object v0
.end method

.method public get_gPSGeometry()Landroid/renderscript/ProgramStore;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->mExportVar_gPSGeometry:Landroid/renderscript/ProgramStore;

    return-object v0
.end method

.method public get_gPVBackground()Landroid/renderscript/ProgramVertex;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->mExportVar_gPVBackground:Landroid/renderscript/ProgramVertex;

    return-object v0
.end method

.method public get_gPVGeometry()Landroid/renderscript/ProgramVertex;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->mExportVar_gPVGeometry:Landroid/renderscript/ProgramVertex;

    return-object v0
.end method

.method public get_gPointTexture()Landroid/renderscript/Allocation;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->mExportVar_gPointTexture:Landroid/renderscript/Allocation;

    return-object v0
.end method

.method public get_gVSConstants()Lcom/android/wallpaper/holospiral/ScriptField_VertexShaderConstants_s;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->mExportVar_gVSConstants:Lcom/android/wallpaper/holospiral/ScriptField_VertexShaderConstants_s;

    return-object v0
.end method

.method public get_gXOffset()F
    .locals 1

    iget v0, p0, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->mExportVar_gXOffset:F

    return v0
.end method

.method public invoke_resize(FF)V
    .locals 2
    .param p1    # F
    .param p2    # F

    new-instance v0, Landroid/renderscript/FieldPacker;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Landroid/renderscript/FieldPacker;-><init>(I)V

    invoke-virtual {v0, p1}, Landroid/renderscript/FieldPacker;->addF32(F)V

    invoke-virtual {v0, p2}, Landroid/renderscript/FieldPacker;->addF32(F)V

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Landroid/renderscript/Script;->invoke(ILandroid/renderscript/FieldPacker;)V

    return-void
.end method

.method public set_gBackgroundMesh(Landroid/renderscript/Mesh;)V
    .locals 1
    .param p1    # Landroid/renderscript/Mesh;

    iput-object p1, p0, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->mExportVar_gBackgroundMesh:Landroid/renderscript/Mesh;

    const/16 v0, 0x8

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gFarPlane(F)V
    .locals 1
    .param p1    # F

    iput p1, p0, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->mExportVar_gFarPlane:F

    const/16 v0, 0xc

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(IF)V

    return-void
.end method

.method public set_gInnerGeometry(Landroid/renderscript/Mesh;)V
    .locals 1
    .param p1    # Landroid/renderscript/Mesh;

    iput-object p1, p0, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->mExportVar_gInnerGeometry:Landroid/renderscript/Mesh;

    const/4 v0, 0x6

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gNearPlane(F)V
    .locals 1
    .param p1    # F

    iput p1, p0, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->mExportVar_gNearPlane:F

    const/16 v0, 0xb

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(IF)V

    return-void
.end method

.method public set_gOuterGeometry(Landroid/renderscript/Mesh;)V
    .locals 1
    .param p1    # Landroid/renderscript/Mesh;

    iput-object p1, p0, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->mExportVar_gOuterGeometry:Landroid/renderscript/Mesh;

    const/4 v0, 0x7

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gPFBackground(Landroid/renderscript/ProgramFragment;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramFragment;

    iput-object p1, p0, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->mExportVar_gPFBackground:Landroid/renderscript/ProgramFragment;

    const/4 v0, 0x2

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gPFGeometry(Landroid/renderscript/ProgramFragment;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramFragment;

    iput-object p1, p0, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->mExportVar_gPFGeometry:Landroid/renderscript/ProgramFragment;

    const/4 v0, 0x3

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gPSBackground(Landroid/renderscript/ProgramStore;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramStore;

    iput-object p1, p0, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->mExportVar_gPSBackground:Landroid/renderscript/ProgramStore;

    const/4 v0, 0x4

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gPSGeometry(Landroid/renderscript/ProgramStore;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramStore;

    iput-object p1, p0, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->mExportVar_gPSGeometry:Landroid/renderscript/ProgramStore;

    const/4 v0, 0x5

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gPVBackground(Landroid/renderscript/ProgramVertex;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramVertex;

    iput-object p1, p0, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->mExportVar_gPVBackground:Landroid/renderscript/ProgramVertex;

    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gPVGeometry(Landroid/renderscript/ProgramVertex;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramVertex;

    iput-object p1, p0, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->mExportVar_gPVGeometry:Landroid/renderscript/ProgramVertex;

    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gPointTexture(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    iput-object p1, p0, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->mExportVar_gPointTexture:Landroid/renderscript/Allocation;

    const/16 v0, 0x9

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gXOffset(F)V
    .locals 1
    .param p1    # F

    iput p1, p0, Lcom/android/wallpaper/holospiral/ScriptC_holo_spiral;->mExportVar_gXOffset:F

    const/16 v0, 0xa

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(IF)V

    return-void
.end method
