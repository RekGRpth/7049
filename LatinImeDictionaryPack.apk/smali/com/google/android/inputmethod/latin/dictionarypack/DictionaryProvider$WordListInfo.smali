.class Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider$WordListInfo;
.super Ljava/lang/Object;
.source "DictionaryProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "WordListInfo"
.end annotation


# instance fields
.field public final mId:Ljava/lang/String;

.field public final mLocale:Ljava/lang/String;

.field public final mMatchLevel:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider$WordListInfo;->mId:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider$WordListInfo;->mLocale:Ljava/lang/String;

    iput p3, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider$WordListInfo;->mMatchLevel:I

    return-void
.end method
