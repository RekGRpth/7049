.class public Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider;
.super Landroid/content/ContentProvider;
.source "DictionaryProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider$ResourcePathCursor;,
        Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider$WordListInfo;
    }
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri;

.field private static final TAG:Ljava/lang/String;

.field private static final sUriMatcher:Landroid/content/UriMatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const-class v0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider;->TAG:Ljava/lang/String;

    const-string v0, "content://com.android.inputmethod.latin.dictionarypack"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider;->CONTENT_URI:Landroid/net/Uri;

    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider;->sUriMatcher:Landroid/content/UriMatcher;

    sget-object v0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.android.inputmethod.latin.dictionarypack"

    const-string v2, "list"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.android.inputmethod.latin.dictionarypack"

    const-string v2, "*"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method

.method private getDictionaryWordListsForContentUri(Landroid/net/Uri;)Ljava/util/Collection;
    .locals 24
    .param p1    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider$WordListInfo;",
            ">;"
        }
    .end annotation

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v22

    const/16 v23, 0x1

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->getInstalledOrDeletingOrAvailableDictionaryMetadata(Landroid/content/Context;)Landroid/database/Cursor;

    move-result-object v14

    if-nez v14, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v22

    :goto_0
    return-object v22

    :cond_0
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    const-string v22, "id"

    move-object/from16 v0, v22

    invoke-interface {v14, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    const-string v22, "locale"

    move-object/from16 v0, v22

    invoke-interface {v14, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    const-string v22, "filename"

    move-object/from16 v0, v22

    invoke-interface {v14, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    const-string v22, "status"

    move-object/from16 v0, v22

    invoke-interface {v14, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v22

    if-eqz v22, :cond_3

    :cond_1
    invoke-interface {v14, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v22

    if-eqz v22, :cond_4

    :cond_2
    :goto_1
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v22

    if-nez v22, :cond_1

    :cond_3
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    invoke-virtual {v5}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v22

    goto :goto_0

    :cond_4
    const-string v22, ":"

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v18

    const/16 v22, 0x2

    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v23, v0

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_7

    const/16 v22, 0x0

    aget-object v16, v18, v22

    :goto_2
    invoke-interface {v14, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-interface {v14, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    invoke-interface {v14, v15}, Landroid/database/Cursor;->getInt(I)I

    move-result v21

    move-object/from16 v0, v20

    invoke-static {v0, v9}, Lcom/google/android/inputmethod/latin/dictionarypack/LocaleUtils;->getMatchLevel(Ljava/lang/String;Ljava/lang/String;)I

    move-result v11

    invoke-static {v11}, Lcom/google/android/inputmethod/latin/dictionarypack/LocaleUtils;->isMatch(I)Z

    move-result v22

    if-eqz v22, :cond_2

    const/16 v22, 0x3

    move/from16 v0, v22

    move/from16 v1, v21

    if-ne v0, v1, :cond_8

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider;->getContext()Landroid/content/Context;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->isFile()Z

    move-result v22

    if-eqz v22, :cond_2

    :cond_5
    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider$WordListInfo;

    if-eqz v4, :cond_6

    iget v0, v4, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider$WordListInfo;->mMatchLevel:I

    move/from16 v22, v0

    move/from16 v0, v22

    if-ge v0, v11, :cond_2

    :cond_6
    new-instance v22, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider$WordListInfo;

    move-object/from16 v0, v22

    move-object/from16 v1, v17

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2, v11}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider$WordListInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    invoke-virtual {v5, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    :cond_7
    const-string v16, "main"

    goto :goto_2

    :cond_8
    const/16 v22, 0x1

    move/from16 v0, v22

    move/from16 v1, v21

    if-ne v0, v1, :cond_5

    const-string v22, "mayPrompt"

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-string v22, "true"

    move-object/from16 v0, v22

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    move-object/from16 v0, v17

    invoke-static {v3, v0, v12}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->installIfNeverRequested(Landroid/content/Context;Ljava/lang/String;Z)V

    goto/16 :goto_1
.end method

.method private getWordListMetadataForUri(Landroid/net/Uri;)Landroid/content/ContentValues;
    .locals 6
    .param p1    # Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x0

    :goto_0
    return-object v4

    :cond_0
    const/16 v4, 0x2f

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-ne v4, v5, :cond_1

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    :goto_1
    invoke-static {v0}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->getDb(Landroid/content/Context;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-static {v1, v2}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->getInstalledOrDeletingWordListContentValuesByWordListId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v4

    goto :goto_0

    :cond_1
    move-object v2, v3

    goto :goto_1
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 11
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    const/4 v8, 0x1

    const/4 v7, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider;->getWordListMetadataForUri(Landroid/net/Uri;)Landroid/content/ContentValues;

    move-result-object v6

    if-nez v6, :cond_1

    :cond_0
    :goto_0
    return v7

    :cond_1
    const-string v9, "status"

    invoke-virtual {v6, v9}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const-string v9, "id"

    invoke-virtual {v6, v9}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v9, "version"

    invoke-virtual {v6, v9}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/4 v9, 0x5

    if-ne v9, v4, :cond_2

    invoke-virtual {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7, v1, v5, v4}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->markAsDeleted(Landroid/content/Context;Ljava/lang/String;II)V

    move v7, v8

    goto :goto_0

    :cond_2
    const/4 v9, 0x3

    if-ne v9, v4, :cond_4

    const-string v9, "result"

    invoke-virtual {p1, v9}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v9, "failure"

    invoke-virtual {v9, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-virtual {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9, v1, v5}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->markAsBroken(Landroid/content/Context;Ljava/lang/String;I)V

    :cond_3
    const-string v9, "filename"

    invoke-virtual {v6, v9}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9, v2}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v9

    if-eqz v9, :cond_0

    move v7, v8

    goto :goto_0

    :cond_4
    sget-object v8, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Attempt to delete a file whose status is "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 4
    .param p1    # Landroid/net/Uri;

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Asked for type of : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p0}, Lcom/google/android/inputmethod/latin/dictionarypack/log/PrivateLog;->log(Ljava/lang/String;Landroid/content/ContentProvider;)V

    sget-object v2, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-object v1

    :pswitch_1
    const-string v1, "vnd.android.cursor.item/vnd.google.dictionarylist"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 2
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/ContentValues;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Attempt to insert : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/google/android/inputmethod/latin/dictionarypack/log/PrivateLog;->log(Ljava/lang/String;Landroid/content/ContentProvider;)V

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Insertion in the dictionary is not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreate()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public openAssetFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    .locals 11
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;

    const/4 v6, 0x0

    if-eqz p2, :cond_0

    const-string v0, "r"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-object v6

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider;->getWordListMetadataForUri(Landroid/net/Uri;)Landroid/content/ContentValues;

    move-result-object v10

    if-eqz v10, :cond_0

    :try_start_0
    const-string v0, "status"

    invoke-virtual {v10, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v9

    const/4 v0, 0x5

    if-ne v0, v9, :cond_2

    invoke-virtual {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v2, 0x7f050000

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->openRawResourceFd(I)Landroid/content/res/AssetFileDescriptor;

    move-result-object v6

    goto :goto_0

    :cond_2
    const-string v0, "filename"

    invoke-virtual {v10, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v7

    const/high16 v0, 0x10000000

    invoke-static {v7, v0}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    new-instance v0, Landroid/content/res/AssetFileDescriptor;

    const-wide/16 v2, 0x0

    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->getStatSize()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Landroid/content/res/AssetFileDescriptor;-><init>(Landroid/os/ParcelFileDescriptor;JJ)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v6, v0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .param p1    # Landroid/net/Uri;
    .param p2    # [Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v6, 0x2

    const/4 v5, 0x1

    new-array v3, v6, [Ljava/lang/Object;

    const-string v4, "Uri ="

    aput-object v4, v3, v7

    aput-object p1, v3, v5

    invoke-static {v3}, Lcom/google/android/inputmethod/latin/dictionarypack/Utils;->l([Ljava/lang/Object;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Query : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, p0}, Lcom/google/android/inputmethod/latin/dictionarypack/log/PrivateLog;->log(Ljava/lang/String;Landroid/content/ContentProvider;)V

    sget-object v3, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v3, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    if-ne v5, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->getDictionariesList(Landroid/content/Context;)Landroid/database/Cursor;

    move-result-object v0

    new-array v3, v6, [Ljava/lang/Object;

    const-string v4, "List of dictionaries with count"

    aput-object v4, v3, v7

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v3}, Lcom/google/android/inputmethod/latin/dictionarypack/Utils;->l([Ljava/lang/Object;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Returned a list of "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " items"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, p0}, Lcom/google/android/inputmethod/latin/dictionarypack/log/PrivateLog;->log(Ljava/lang/String;Landroid/content/ContentProvider;)V

    :goto_0
    return-object v0

    :cond_0
    if-ne v6, v2, :cond_2

    invoke-direct {p0, p1}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider;->getDictionaryWordListsForContentUri(Landroid/net/Uri;)Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;->updateNowIfNotUpdatedInAVeryLongTime(Landroid/content/Context;)V

    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v3

    if-lez v3, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Returned "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " files"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, p0}, Lcom/google/android/inputmethod/latin/dictionarypack/log/PrivateLog;->log(Ljava/lang/String;Landroid/content/ContentProvider;)V

    new-instance v0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider$ResourcePathCursor;

    invoke-direct {v0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider$ResourcePathCursor;-><init>(Ljava/util/Collection;)V

    goto :goto_0

    :cond_1
    const-string v3, "No dictionary files for this URL"

    invoke-static {v3, p0}, Lcom/google/android/inputmethod/latin/dictionarypack/log/PrivateLog;->log(Ljava/lang/String;Landroid/content/ContentProvider;)V

    new-instance v0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider$ResourcePathCursor;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider$ResourcePathCursor;-><init>(Ljava/util/Collection;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 2
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/ContentValues;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Attempt to update : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/google/android/inputmethod/latin/dictionarypack/log/PrivateLog;->log(Ljava/lang/String;Landroid/content/ContentProvider;)V

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Updating dictionary words is not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
