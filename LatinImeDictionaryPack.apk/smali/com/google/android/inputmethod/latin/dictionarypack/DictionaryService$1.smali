.class Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService$1;
.super Ljava/lang/Thread;
.source "DictionaryService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;->onStartCommand(Landroid/content/Intent;II)I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;

.field final synthetic val$intent:Landroid/content/Intent;

.field final synthetic val$self:Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;


# direct methods
.method constructor <init>(Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;Ljava/lang/String;Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;Landroid/content/Intent;)V
    .locals 0
    .param p2    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService$1;->this$0:Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;

    iput-object p3, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService$1;->val$self:Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;

    iput-object p4, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService$1;->val$intent:Landroid/content/Intent;

    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService$1;->val$self:Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;

    iget-object v1, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService$1;->val$intent:Landroid/content/Intent;

    # invokes: Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;->dispatchBroadcast(Landroid/content/Context;Landroid/content/Intent;)V
    invoke-static {v0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;->access$000(Landroid/content/Context;Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService$1;->val$self:Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService$1;->this$0:Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;

    # --operator for: Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;->mCommandCount:I
    invoke-static {v0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;->access$106(Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;)I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService$1;->this$0:Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;

    iget-object v2, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService$1;->this$0:Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;

    # getter for: Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;->mLastSeenStartId:I
    invoke-static {v2}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;->access$200(Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;->stopSelfResult(I)Z

    move-result v0

    if-nez v0, :cond_0

    # getter for: Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v2, "Can\'t stop ourselves"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
