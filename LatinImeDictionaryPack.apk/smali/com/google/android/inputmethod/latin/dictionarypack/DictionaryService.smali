.class public Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;
.super Landroid/app/Service;
.source "DictionaryService.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mCommandCount:I

.field private mLastSeenStartId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method static synthetic access$000(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/content/Intent;

    invoke-static {p0, p1}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;->dispatchBroadcast(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$106(Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;)I
    .locals 1
    .param p0    # Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;

    iget v0, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;->mCommandCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;->mCommandCount:I

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;)I
    .locals 1
    .param p0    # Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;

    iget v0, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;->mLastSeenStartId:I

    return v0
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private static checkTimeAndMaybeSetupUpdateAlarm(Landroid/content/Context;)V
    .locals 9
    .param p0    # Landroid/content/Context;

    const-wide/32 v7, 0x14997000

    invoke-static {p0, v7, v8}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;->isLastUpdateAtLeastThisOld(Landroid/content/Context;J)Z

    move-result v7

    if-nez v7, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v7, "Date changed - registering alarm"

    invoke-static {v7, p0}, Lcom/google/android/inputmethod/latin/dictionarypack/log/PrivateLog;->log(Ljava/lang/String;Landroid/content/Context;)V

    const-string v7, "alarm"

    invoke-virtual {p0, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    new-instance v7, Ljava/util/Random;

    invoke-direct {v7}, Ljava/util/Random;-><init>()V

    const v8, 0x1499700

    invoke-virtual {v7, v8}, Ljava/util/Random;->nextInt(I)I

    move-result v7

    int-to-long v7, v7

    add-long v1, v3, v7

    new-instance v6, Landroid/content/Intent;

    const-string v7, "com.android.inputmethod.latin.dictionarypack.UPDATE_NOW"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v7, 0x0

    const/high16 v8, 0x10000000

    invoke-static {p0, v7, v6, v8}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    if-eqz v0, :cond_0

    const/4 v7, 0x1

    invoke-virtual {v0, v7, v1, v2, v5}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method private static dispatchBroadcast(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/content/Intent;

    const-string v0, "android.intent.action.DATE_CHANGED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;->checkTimeAndMaybeSetupUpdateAlarm(Landroid/content/Context;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "com.android.inputmethod.latin.dictionarypack.UPDATE_NOW"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->update(Landroid/content/Context;Z)V

    goto :goto_0

    :cond_1
    invoke-static {p0, p1}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->downloadFinished(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public static getLastUpdateDate(Landroid/content/Context;)Landroid/text/format/Time;
    .locals 7
    .param p0    # Landroid/content/Context;

    const-string v4, "dict_pack_update"

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v4, "last_update"

    const-wide/16 v5, 0x0

    invoke-interface {v2, v4, v5, v6}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    new-instance v3, Landroid/text/format/Time;

    invoke-direct {v3}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v3, v0, v1}, Landroid/text/format/Time;->set(J)V

    return-object v3
.end method

.method private static isLastUpdateAtLeastThisOld(Landroid/content/Context;J)Z
    .locals 9
    .param p0    # Landroid/content/Context;
    .param p1    # J

    const/4 v5, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-string v6, "dict_pack_update"

    invoke-virtual {p0, v6, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v6, "last_update"

    const-wide/16 v7, 0x0

    invoke-interface {v4, v6, v7, v8}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    add-long v6, v0, p1

    cmp-long v6, v6, v2

    if-gez v6, :cond_0

    const/4 v5, 0x1

    :cond_0
    return v5
.end method

.method static saveLastUpdateTime(Landroid/content/Context;)V
    .locals 5
    .param p0    # Landroid/content/Context;

    const-string v2, "dict_pack_update"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "last_update"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-interface {v0, v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public static updateNowIfNotUpdatedInAVeryLongTime(Landroid/content/Context;)V
    .locals 2
    .param p0    # Landroid/content/Context;

    const-wide/32 v0, 0x48190800

    invoke-static {p0, v0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;->isLastUpdateAtLeastThisOld(Landroid/content/Context;J)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->update(Landroid/content/Context;Z)V

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;->mLastSeenStartId:I

    iput v0, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;->mCommandCount:I

    return-void
.end method

.method public onDestroy()V
    .locals 0

    return-void
.end method

.method public declared-synchronized onStartCommand(Landroid/content/Intent;II)I
    .locals 3
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    monitor-enter p0

    move-object v0, p0

    :try_start_0
    iput p3, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;->mLastSeenStartId:I

    iget v1, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;->mCommandCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;->mCommandCount:I

    new-instance v1, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService$1;

    const-string v2, "updateOrFinishDownload"

    invoke-direct {v1, p0, v2, v0, p1}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService$1;-><init>(Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;Ljava/lang/String;Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;Landroid/content/Intent;)V

    invoke-virtual {v1}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService$1;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x3

    monitor-exit p0

    return v1

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
