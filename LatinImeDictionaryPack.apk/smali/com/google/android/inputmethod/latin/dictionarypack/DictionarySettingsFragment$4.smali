.class Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment$4;
.super Ljava/lang/Object;
.source "DictionarySettingsFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->refreshInterface()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;

.field final synthetic val$prefList:Ljava/util/Collection;

.field final synthetic val$prefScreen:Landroid/preference/PreferenceGroup;


# direct methods
.method constructor <init>(Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;Landroid/preference/PreferenceGroup;Ljava/util/Collection;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment$4;->this$0:Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;

    iput-object p2, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment$4;->val$prefScreen:Landroid/preference/PreferenceGroup;

    iput-object p3, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment$4;->val$prefList:Ljava/util/Collection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v2, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment$4;->this$0:Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;

    # invokes: Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->refreshNetworkState()V
    invoke-static {v2}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->access$000(Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;)V

    iget-object v2, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment$4;->this$0:Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;

    iget-object v3, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment$4;->val$prefScreen:Landroid/preference/PreferenceGroup;

    # invokes: Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->removeAnyDictSettings(Landroid/preference/PreferenceGroup;)V
    invoke-static {v2, v3}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->access$200(Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;Landroid/preference/PreferenceGroup;)V

    iget-object v2, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment$4;->val$prefList:Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/preference/Preference;

    iget-object v2, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment$4;->val$prefScreen:Landroid/preference/PreferenceGroup;

    invoke-virtual {v2, v1}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_0

    :cond_0
    return-void
.end method
