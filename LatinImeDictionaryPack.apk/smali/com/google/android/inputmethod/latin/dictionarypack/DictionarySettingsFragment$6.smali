.class Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment$6;
.super Ljava/lang/Object;
.source "DictionarySettingsFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->stopLoadingAnimation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;

.field final synthetic val$preferenceView:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment$6;->this$0:Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;

    iput-object p2, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment$6;->val$preferenceView:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment$6;->this$0:Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;

    # getter for: Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->mLoadingView:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->access$300(Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment$6;->val$preferenceView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment$6;->this$0:Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;

    # getter for: Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->mLoadingView:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->access$300(Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment$6;->this$0:Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;

    invoke-virtual {v1}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x10a0001

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment$6;->val$preferenceView:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment$6;->this$0:Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;

    invoke-virtual {v1}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/high16 v2, 0x10a0000

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment$6;->this$0:Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;

    # getter for: Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->mUpdateNowMenu:Landroid/view/MenuItem;
    invoke-static {v0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->access$400(Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f070013

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    return-void
.end method
