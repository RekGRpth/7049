.class public Lcom/google/android/inputmethod/latin/dictionarypack/DownloadOverMeteredDialog;
.super Landroid/app/Activity;
.source "DownloadOverMeteredDialog.java"


# instance fields
.field private mWordListToDownload:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private setTexts(Ljava/lang/String;J)V
    .locals 10
    .param p1    # Ljava/lang/String;
    .param p2    # J

    const/4 v7, 0x1

    const/4 v9, 0x0

    const v6, 0x7f07001e

    invoke-virtual {p0, v6}, Lcom/google/android/inputmethod/latin/dictionarypack/DownloadOverMeteredDialog;->getString(I)Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f07001f

    invoke-virtual {p0, v6}, Lcom/google/android/inputmethod/latin/dictionarypack/DownloadOverMeteredDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/inputmethod/latin/dictionarypack/LocaleUtils;->constructLocaleFromString(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v3

    if-nez v3, :cond_0

    const-string v2, ""

    :goto_0
    const/high16 v6, 0x7f080000

    invoke-virtual {p0, v6}, Lcom/google/android/inputmethod/latin/dictionarypack/DownloadOverMeteredDialog;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    new-array v6, v7, [Ljava/lang/Object;

    aput-object v2, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v6, 0x7f080003

    invoke-virtual {p0, v6}, Lcom/google/android/inputmethod/latin/dictionarypack/DownloadOverMeteredDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-array v6, v7, [Ljava/lang/Object;

    long-to-float v7, p2

    const/high16 v8, 0x49800000

    div-float/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v1, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    invoke-virtual {v3}, Ljava/util/Locale;->getDisplayLanguage()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method


# virtual methods
.method public onClickAllow(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->setDownloadOverMeteredSetting(Landroid/content/Context;Z)V

    iget-object v0, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DownloadOverMeteredDialog;->mWordListToDownload:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->installIfNeverRequested(Landroid/content/Context;Ljava/lang/String;Z)V

    invoke-virtual {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/DownloadOverMeteredDialog;->finish()V

    return-void
.end method

.method public onClickDeny(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->setDownloadOverMeteredSetting(Landroid/content/Context;Z)V

    invoke-virtual {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/DownloadOverMeteredDialog;->finish()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/DownloadOverMeteredDialog;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v4, "wordlist_to_download"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DownloadOverMeteredDialog;->mWordListToDownload:Ljava/lang/String;

    const-string v4, "locale"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v4, "size"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    int-to-long v2, v4

    const/high16 v4, 0x7f030000

    invoke-virtual {p0, v4}, Lcom/google/android/inputmethod/latin/dictionarypack/DownloadOverMeteredDialog;->setContentView(I)V

    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/inputmethod/latin/dictionarypack/DownloadOverMeteredDialog;->setTexts(Ljava/lang/String;J)V

    return-void
.end method
