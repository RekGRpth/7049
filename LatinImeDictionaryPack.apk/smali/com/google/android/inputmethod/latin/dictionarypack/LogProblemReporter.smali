.class Lcom/google/android/inputmethod/latin/dictionarypack/LogProblemReporter;
.super Ljava/lang/Object;
.source "LogProblemReporter.java"

# interfaces
.implements Lcom/google/android/inputmethod/latin/dictionarypack/ProblemReporter;


# instance fields
.field private final TAG:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/inputmethod/latin/dictionarypack/LogProblemReporter;->TAG:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public report(Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/google/android/inputmethod/latin/dictionarypack/LogProblemReporter;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Reporting problem : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
