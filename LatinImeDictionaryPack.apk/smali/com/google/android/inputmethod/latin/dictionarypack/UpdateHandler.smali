.class public Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;
.super Ljava/lang/Object;
.source "UpdateHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;
    }
.end annotation


# static fields
.field static final TAG:Ljava/lang/String;

.field private static sUpdateEventListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;",
            ">;"
        }
    .end annotation
.end field

.field static final sharedIdProtector:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DictionaryProvider:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->sharedIdProtector:Ljava/lang/Object;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->sUpdateEventListeners:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cancelUpdate(Landroid/content/Context;)V
    .locals 2
    .param p0    # Landroid/content/Context;

    const-string v1, "download"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/DownloadManager;

    if-eqz v0, :cond_0

    invoke-static {p0, v0}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->cancelUpdateWithDownloadManager(Landroid/content/Context;Landroid/app/DownloadManager;)V

    :cond_0
    return-void
.end method

.method private static cancelUpdateWithDownloadManager(Landroid/content/Context;Landroid/app/DownloadManager;)V
    .locals 9
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/app/DownloadManager;

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    sget-object v5, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->sharedIdProtector:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    invoke-static {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->readMetadataDownloadId(Landroid/content/Context;)J

    move-result-wide v2

    cmp-long v4, v6, v2

    if-nez v4, :cond_1

    monitor-exit v5

    :cond_0
    return-void

    :cond_1
    const/4 v4, 0x1

    new-array v4, v4, [J

    const/4 v6, 0x0

    aput-wide v2, v4, v6

    invoke-virtual {p1, v4}, Landroid/app/DownloadManager;->remove([J)I

    const-wide/16 v6, -0x1

    invoke-static {p0, v6, v7}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->writeMetadataDownloadId(Landroid/content/Context;J)V

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-object v4, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->sUpdateEventListeners:Ljava/util/List;

    invoke-static {v4}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->linkedCopyOfList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;

    invoke-interface {v1, v8}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;->downloadedMetadata(Z)V

    goto :goto_0

    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4
.end method

.method private static compareMetadataForUpgrade(Landroid/content/Context;Ljava/util/List;Ljava/util/List;)Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;
    .locals 15
    .param p0    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;",
            ">;)",
            "Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;"
        }
    .end annotation

    new-instance v1, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;

    invoke-direct {v1}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;-><init>()V

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    const-string v14, "Comparing dictionaries"

    aput-object v14, v12, v13

    invoke-static {v12}, Lcom/google/android/inputmethod/latin/dictionarypack/Utils;->l([Ljava/lang/Object;)V

    new-instance v11, Ljava/util/TreeSet;

    invoke-direct {v11}, Ljava/util/TreeSet;-><init>()V

    if-nez p1, :cond_0

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct/range {p1 .. p1}, Ljava/util/ArrayList;-><init>()V

    :cond_0
    if-nez p2, :cond_1

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct/range {p2 .. p2}, Ljava/util/ArrayList;-><init>()V

    :cond_1
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget-object v12, v10, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mId:Ljava/lang/String;

    invoke-interface {v11, v12}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget-object v12, v10, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mId:Ljava/lang/String;

    invoke-interface {v11, v12}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_e

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-static {v0, v5}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataHandler;->findWordListById(Ljava/util/List;Ljava/lang/String;)Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-static {v0, v5}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataHandler;->findWordListById(Ljava/util/List;Ljava/lang/String;)Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    move-result-object v6

    if-eqz v6, :cond_5

    iget v12, v6, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mFormatVersion:I

    const/4 v13, 0x2

    if-le v12, v13, :cond_6

    :cond_5
    const/4 v7, 0x0

    :goto_3
    const/4 v12, 0x4

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    const-string v14, "Considering updating "

    aput-object v14, v12, v13

    const/4 v13, 0x1

    aput-object v5, v12, v13

    const/4 v13, 0x2

    const-string v14, "currentInfo ="

    aput-object v14, v12, v13

    const/4 v13, 0x3

    aput-object v2, v12, v13

    invoke-static {v12}, Lcom/google/android/inputmethod/latin/dictionarypack/Utils;->l([Ljava/lang/Object;)V

    if-nez v2, :cond_8

    if-nez v7, :cond_8

    if-nez v6, :cond_7

    sget-object v12, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->TAG:Ljava/lang/String;

    const-string v13, "Got an id for a wordlist that is neither in from nor in to"

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_6
    move-object v7, v6

    goto :goto_3

    :cond_7
    sget-object v12, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Can\'t handle word list with id \'"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\' because it has format"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " version "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget v14, v6, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mFormatVersion:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " and the maximum version"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "we can handle is "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const/4 v14, 0x2

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_8
    if-nez v2, :cond_9

    new-instance v12, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$MakeAvailableAction;

    invoke-direct {v12, v7}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$MakeAvailableAction;-><init>(Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;)V

    invoke-virtual {v1, v12}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;->add(Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$Action;)V

    goto/16 :goto_2

    :cond_9
    if-nez v7, :cond_a

    new-instance v12, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$ForgetAction;

    const/4 v13, 0x0

    invoke-direct {v12, v2, v13}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$ForgetAction;-><init>(Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;Z)V

    invoke-virtual {v1, v12}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;->add(Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$Action;)V

    goto/16 :goto_2

    :cond_a
    invoke-static {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->getDb(Landroid/content/Context;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    iget v12, v7, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mVersion:I

    iget v13, v2, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mVersion:I

    if-ne v12, v13, :cond_b

    new-instance v12, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$UpdateDataAction;

    invoke-direct {v12, v7}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$UpdateDataAction;-><init>(Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;)V

    invoke-virtual {v1, v12}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;->add(Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$Action;)V

    goto/16 :goto_2

    :cond_b
    iget v12, v7, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mVersion:I

    iget v13, v2, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mVersion:I

    if-le v12, v13, :cond_4

    iget-object v12, v2, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mId:Ljava/lang/String;

    iget v13, v2, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mVersion:I

    invoke-static {v3, v12, v13}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->getContentValuesByWordListId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)Landroid/content/ContentValues;

    move-result-object v9

    const-string v12, "status"

    invoke-virtual {v9, v12}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v8

    new-instance v12, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$MakeAvailableAction;

    invoke-direct {v12, v7}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$MakeAvailableAction;-><init>(Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;)V

    invoke-virtual {v1, v12}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;->add(Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$Action;)V

    const/4 v12, 0x3

    if-eq v8, v12, :cond_c

    const/4 v12, 0x4

    if-ne v8, v12, :cond_d

    :cond_c
    new-instance v12, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$StartDownloadAction;

    const/4 v13, 0x0

    invoke-direct {v12, v7, v13}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$StartDownloadAction;-><init>(Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;Z)V

    invoke-virtual {v1, v12}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;->add(Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$Action;)V

    goto/16 :goto_2

    :cond_d
    new-instance v12, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$ForgetAction;

    const/4 v13, 0x1

    invoke-direct {v12, v2, v13}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$ForgetAction;-><init>(Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;Z)V

    invoke-virtual {v1, v12}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;->add(Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$Action;)V

    goto/16 :goto_2

    :cond_e
    return-object v1
.end method

.method public static computeUpgradeTo(Landroid/content/Context;Ljava/util/List;)Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;
    .locals 2
    .param p0    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;",
            ">;)",
            "Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;"
        }
    .end annotation

    invoke-static {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataHandler;->getCurrentMetadata(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    invoke-static {p0, v0, p1}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->compareMetadataForUpgrade(Landroid/content/Context;Ljava/util/List;Ljava/util/List;)Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;

    move-result-object v1

    return-object v1
.end method

.method private static copyFile(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 10
    .param p0    # Ljava/io/InputStream;
    .param p1    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v9, 0x1

    const/4 v8, 0x0

    new-array v2, v9, [Ljava/lang/Object;

    const-string v3, "Copying files"

    aput-object v3, v2, v8

    invoke-static {v2}, Lcom/google/android/inputmethod/latin/dictionarypack/Utils;->l([Ljava/lang/Object;)V

    instance-of v2, p0, Ljava/io/FileInputStream;

    if-eqz v2, :cond_0

    instance-of v2, p1, Ljava/io/FileOutputStream;

    if-nez v2, :cond_1

    :cond_0
    new-array v2, v9, [Ljava/lang/Object;

    const-string v3, "Not the right types"

    aput-object v3, v2, v8

    invoke-static {v2}, Lcom/google/android/inputmethod/latin/dictionarypack/Utils;->l([Ljava/lang/Object;)V

    invoke-static {p0, p1}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->copyFileFallback(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    :goto_0
    return-void

    :cond_1
    :try_start_0
    move-object v0, p0

    check-cast v0, Ljava/io/FileInputStream;

    move-object v2, v0

    invoke-virtual {v2}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v1

    move-object v0, p1

    check-cast v0, Ljava/io/FileOutputStream;

    move-object v2, v0

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v6

    const-wide/16 v2, 0x0

    const-wide/32 v4, 0x7fffffff

    invoke-virtual/range {v1 .. v6}, Ljava/nio/channels/FileChannel;->transferTo(JJLjava/nio/channels/WritableByteChannel;)J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v7

    new-array v2, v9, [Ljava/lang/Object;

    const-string v3, "Won\'t work"

    aput-object v3, v2, v8

    invoke-static {v2}, Lcom/google/android/inputmethod/latin/dictionarypack/Utils;->l([Ljava/lang/Object;)V

    invoke-static {p0, p1}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->copyFileFallback(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    goto :goto_0
.end method

.method private static copyFileFallback(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 5
    .param p0    # Ljava/io/InputStream;
    .param p1    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v4, 0x0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "Falling back to slow copy"

    aput-object v3, v2, v4

    invoke-static {v2}, Lcom/google/android/inputmethod/latin/dictionarypack/Utils;->l([Ljava/lang/Object;)V

    const/16 v2, 0x2000

    new-array v0, v2, [B

    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    :goto_0
    if-ltz v1, :cond_0

    invoke-virtual {p1, v0, v4, v1}, Ljava/io/OutputStream;->write([BII)V

    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    goto :goto_0

    :cond_0
    return-void
.end method

.method static downloadFinished(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 25
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/content/Intent;

    const-string v21, "extra_download_id"

    const-wide/16 v22, -0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    move-wide/from16 v2, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v11

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Download finished with id "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/log/PrivateLog;->log(Ljava/lang/String;Landroid/content/Context;)V

    const/16 v21, 0x2

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "DownloadFinished with id"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/google/android/inputmethod/latin/dictionarypack/Utils;->l([Ljava/lang/Object;)V

    const-wide/16 v21, -0x1

    cmp-long v21, v21, v11

    if-nez v21, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static/range {p0 .. p0}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->getDb(Landroid/content/Context;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    sget-object v22, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->sharedIdProtector:Ljava/lang/Object;

    monitor-enter v22

    :try_start_0
    invoke-static/range {p0 .. p0}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->readMetadataDownloadId(Landroid/content/Context;)J

    move-result-wide v17

    cmp-long v21, v11, v17

    if-nez v21, :cond_5

    const-wide/16 v23, -0x1

    move-object/from16 v0, p0

    move-wide/from16 v1, v23

    invoke-static {v0, v1, v2}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->writeMetadataDownloadId(Landroid/content/Context;J)V

    const/4 v9, 0x0

    const/4 v14, 0x1

    :goto_1
    monitor-exit v22
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v21, 0x2

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "Received result for download "

    aput-object v23, v21, v22

    const/16 v22, 0x1

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/google/android/inputmethod/latin/dictionarypack/Utils;->l([Ljava/lang/Object;)V

    const-string v21, "download"

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/app/DownloadManager;

    move-object/from16 v0, v16

    invoke-static {v0, v11, v12}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->getDownloadStatus(Landroid/app/DownloadManager;J)I

    move-result v6

    const/16 v21, 0x8

    move/from16 v0, v21

    if-ne v0, v6, :cond_7

    const/4 v7, 0x1

    :goto_2
    const/16 v19, 0x0

    if-eqz v7, :cond_1

    :try_start_1
    new-instance v8, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;

    move-object/from16 v0, v16

    invoke-virtual {v0, v11, v12}, Landroid/app/DownloadManager;->openDownloadedFile(J)Landroid/os/ParcelFileDescriptor;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-direct {v8, v0}, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V

    if-eqz v14, :cond_8

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "Data D/L\'d is metadata"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/google/android/inputmethod/latin/dictionarypack/Utils;->l([Ljava/lang/Object;)V

    move-object/from16 v0, p0

    invoke-static {v0, v8}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->handleMetadata(Landroid/content/Context;Ljava/io/InputStream;)V

    :cond_1
    :goto_3
    if-eqz v14, :cond_2

    invoke-static/range {p0 .. p0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;->saveLastUpdateTime(Landroid/content/Context;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/google/android/inputmethod/latin/dictionarypack/BadFormatException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_2
    sget-object v22, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->sharedIdProtector:Ljava/lang/Object;

    monitor-enter v22

    if-nez v14, :cond_3

    if-eqz v7, :cond_2d

    :try_start_2
    new-instance v4, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;

    invoke-direct {v4}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;-><init>()V

    new-instance v21, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$InstallAfterDownloadAction;

    move-object/from16 v0, v21

    invoke-direct {v0, v9}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$InstallAfterDownloadAction;-><init>(Landroid/content/ContentValues;)V

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;->add(Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$Action;)V

    new-instance v21, Lcom/google/android/inputmethod/latin/dictionarypack/LogProblemReporter;

    sget-object v23, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->TAG:Ljava/lang/String;

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/LogProblemReporter;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v4, v0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;->execute(Landroid/content/Context;Lcom/google/android/inputmethod/latin/dictionarypack/ProblemReporter;)V

    :cond_3
    :goto_4
    invoke-static {v5}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->isStillWaitingForSomeDownloads(Landroid/database/sqlite/SQLiteDatabase;)Z

    move-result v21

    if-nez v21, :cond_4

    const/16 v19, 0x1

    :cond_4
    monitor-exit v22
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_7

    if-eqz v14, :cond_2e

    sget-object v21, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->sUpdateEventListeners:Ljava/util/List;

    invoke-static/range {v21 .. v21}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->linkedCopyOfList(Ljava/util/List;)Ljava/util/List;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_5
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_2f

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;

    invoke-interface {v15, v7}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;->downloadedMetadata(Z)V

    goto :goto_5

    :cond_5
    :try_start_3
    invoke-static {v5, v11, v12}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->getContentValuesByPendingId(Landroid/database/sqlite/SQLiteDatabase;J)Landroid/content/ContentValues;

    move-result-object v9

    if-nez v9, :cond_6

    monitor-exit v22

    goto/16 :goto_0

    :catchall_0
    move-exception v21

    monitor-exit v22
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v21

    :cond_6
    const/4 v14, 0x0

    goto/16 :goto_1

    :cond_7
    const/4 v7, 0x0

    goto/16 :goto_2

    :cond_8
    const/16 v21, 0x1

    :try_start_4
    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "Data D/L\'d is a word list"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/google/android/inputmethod/latin/dictionarypack/Utils;->l([Ljava/lang/Object;)V

    const-string v21, "status"

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v20

    const/16 v21, 0x2

    move/from16 v0, v21

    move/from16 v1, v20

    if-ne v0, v1, :cond_b

    move-object/from16 v0, p0

    invoke-static {v0, v8, v9}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->handleWordList(Landroid/content/Context;Ljava/io/InputStream;Landroid/content/ContentValues;)V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Lcom/google/android/inputmethod/latin/dictionarypack/BadFormatException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_3

    :catch_0
    move-exception v10

    const/4 v7, 0x0

    :try_start_5
    sget-object v21, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->TAG:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "A file was downloaded but it can\'t be opened : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    sget-object v22, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->sharedIdProtector:Ljava/lang/Object;

    monitor-enter v22

    if-nez v14, :cond_9

    if-eqz v7, :cond_19

    :try_start_6
    new-instance v4, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;

    invoke-direct {v4}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;-><init>()V

    new-instance v21, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$InstallAfterDownloadAction;

    move-object/from16 v0, v21

    invoke-direct {v0, v9}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$InstallAfterDownloadAction;-><init>(Landroid/content/ContentValues;)V

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;->add(Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$Action;)V

    new-instance v21, Lcom/google/android/inputmethod/latin/dictionarypack/LogProblemReporter;

    sget-object v23, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->TAG:Ljava/lang/String;

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/LogProblemReporter;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v4, v0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;->execute(Landroid/content/Context;Lcom/google/android/inputmethod/latin/dictionarypack/ProblemReporter;)V

    :cond_9
    :goto_6
    invoke-static {v5}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->isStillWaitingForSomeDownloads(Landroid/database/sqlite/SQLiteDatabase;)Z

    move-result v21

    if-nez v21, :cond_a

    const/16 v19, 0x1

    :cond_a
    monitor-exit v22
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    if-eqz v14, :cond_1a

    sget-object v21, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->sUpdateEventListeners:Ljava/util/List;

    invoke-static/range {v21 .. v21}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->linkedCopyOfList(Ljava/util/List;)Ljava/util/List;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_7
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_1b

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;

    invoke-interface {v15, v7}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;->downloadedMetadata(Z)V

    goto :goto_7

    :cond_b
    :try_start_7
    sget-object v21, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->TAG:Ljava/lang/String;

    const-string v22, "Spurious download ended. Maybe a cancelled download?"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Lcom/google/android/inputmethod/latin/dictionarypack/BadFormatException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto/16 :goto_3

    :catch_1
    move-exception v10

    const/4 v7, 0x0

    :try_start_8
    sget-object v21, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->TAG:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Can\'t read a file : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    sget-object v22, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->sharedIdProtector:Ljava/lang/Object;

    monitor-enter v22

    if-nez v14, :cond_c

    if-eqz v7, :cond_1e

    :try_start_9
    new-instance v4, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;

    invoke-direct {v4}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;-><init>()V

    new-instance v21, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$InstallAfterDownloadAction;

    move-object/from16 v0, v21

    invoke-direct {v0, v9}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$InstallAfterDownloadAction;-><init>(Landroid/content/ContentValues;)V

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;->add(Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$Action;)V

    new-instance v21, Lcom/google/android/inputmethod/latin/dictionarypack/LogProblemReporter;

    sget-object v23, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->TAG:Ljava/lang/String;

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/LogProblemReporter;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v4, v0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;->execute(Landroid/content/Context;Lcom/google/android/inputmethod/latin/dictionarypack/ProblemReporter;)V

    :cond_c
    :goto_8
    invoke-static {v5}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->isStillWaitingForSomeDownloads(Landroid/database/sqlite/SQLiteDatabase;)Z

    move-result v21

    if-nez v21, :cond_d

    const/16 v19, 0x1

    :cond_d
    monitor-exit v22
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    if-eqz v14, :cond_1f

    sget-object v21, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->sUpdateEventListeners:Ljava/util/List;

    invoke-static/range {v21 .. v21}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->linkedCopyOfList(Ljava/util/List;)Ljava/util/List;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_9
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_20

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;

    invoke-interface {v15, v7}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;->downloadedMetadata(Z)V

    goto :goto_9

    :catch_2
    move-exception v10

    const/4 v7, 0x0

    :try_start_a
    sget-object v21, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->TAG:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Incorrect data received : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    sget-object v22, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->sharedIdProtector:Ljava/lang/Object;

    monitor-enter v22

    if-nez v14, :cond_e

    if-eqz v7, :cond_23

    :try_start_b
    new-instance v4, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;

    invoke-direct {v4}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;-><init>()V

    new-instance v21, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$InstallAfterDownloadAction;

    move-object/from16 v0, v21

    invoke-direct {v0, v9}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$InstallAfterDownloadAction;-><init>(Landroid/content/ContentValues;)V

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;->add(Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$Action;)V

    new-instance v21, Lcom/google/android/inputmethod/latin/dictionarypack/LogProblemReporter;

    sget-object v23, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->TAG:Ljava/lang/String;

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/LogProblemReporter;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v4, v0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;->execute(Landroid/content/Context;Lcom/google/android/inputmethod/latin/dictionarypack/ProblemReporter;)V

    :cond_e
    :goto_a
    invoke-static {v5}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->isStillWaitingForSomeDownloads(Landroid/database/sqlite/SQLiteDatabase;)Z

    move-result v21

    if-nez v21, :cond_f

    const/16 v19, 0x1

    :cond_f
    monitor-exit v22
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_5

    if-eqz v14, :cond_24

    sget-object v21, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->sUpdateEventListeners:Ljava/util/List;

    invoke-static/range {v21 .. v21}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->linkedCopyOfList(Ljava/util/List;)Ljava/util/List;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_b
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_25

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;

    invoke-interface {v15, v7}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;->downloadedMetadata(Z)V

    goto :goto_b

    :catch_3
    move-exception v10

    const/4 v7, 0x0

    :try_start_c
    sget-object v21, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->TAG:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Incorrect data received : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    sget-object v22, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->sharedIdProtector:Ljava/lang/Object;

    monitor-enter v22

    if-nez v14, :cond_10

    if-eqz v7, :cond_28

    :try_start_d
    new-instance v4, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;

    invoke-direct {v4}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;-><init>()V

    new-instance v21, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$InstallAfterDownloadAction;

    move-object/from16 v0, v21

    invoke-direct {v0, v9}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$InstallAfterDownloadAction;-><init>(Landroid/content/ContentValues;)V

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;->add(Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$Action;)V

    new-instance v21, Lcom/google/android/inputmethod/latin/dictionarypack/LogProblemReporter;

    sget-object v23, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->TAG:Ljava/lang/String;

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/LogProblemReporter;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v4, v0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;->execute(Landroid/content/Context;Lcom/google/android/inputmethod/latin/dictionarypack/ProblemReporter;)V

    :cond_10
    :goto_c
    invoke-static {v5}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->isStillWaitingForSomeDownloads(Landroid/database/sqlite/SQLiteDatabase;)Z

    move-result v21

    if-nez v21, :cond_11

    const/16 v19, 0x1

    :cond_11
    monitor-exit v22
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_6

    if-eqz v14, :cond_29

    sget-object v21, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->sUpdateEventListeners:Ljava/util/List;

    invoke-static/range {v21 .. v21}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->linkedCopyOfList(Ljava/util/List;)Ljava/util/List;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_d
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_2a

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;

    invoke-interface {v15, v7}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;->downloadedMetadata(Z)V

    goto :goto_d

    :cond_12
    invoke-static/range {p0 .. p0}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->signalNewDictionaryState(Landroid/content/Context;)V

    :cond_13
    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [J

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput-wide v11, v22, v23

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/app/DownloadManager;->remove([J)I

    throw v21

    :catchall_1
    move-exception v21

    sget-object v22, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->sharedIdProtector:Ljava/lang/Object;

    monitor-enter v22

    if-nez v14, :cond_14

    if-eqz v7, :cond_16

    :try_start_e
    new-instance v4, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;

    invoke-direct {v4}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;-><init>()V

    new-instance v23, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$InstallAfterDownloadAction;

    move-object/from16 v0, v23

    invoke-direct {v0, v9}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$InstallAfterDownloadAction;-><init>(Landroid/content/ContentValues;)V

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;->add(Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$Action;)V

    new-instance v23, Lcom/google/android/inputmethod/latin/dictionarypack/LogProblemReporter;

    sget-object v24, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->TAG:Ljava/lang/String;

    invoke-direct/range {v23 .. v24}, Lcom/google/android/inputmethod/latin/dictionarypack/LogProblemReporter;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v4, v0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;->execute(Landroid/content/Context;Lcom/google/android/inputmethod/latin/dictionarypack/ProblemReporter;)V

    :cond_14
    :goto_e
    invoke-static {v5}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->isStillWaitingForSomeDownloads(Landroid/database/sqlite/SQLiteDatabase;)Z

    move-result v23

    if-nez v23, :cond_15

    const/16 v19, 0x1

    :cond_15
    monitor-exit v22
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    if-eqz v14, :cond_17

    sget-object v22, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->sUpdateEventListeners:Ljava/util/List;

    invoke-static/range {v22 .. v22}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->linkedCopyOfList(Ljava/util/List;)Ljava/util/List;

    move-result-object v22

    invoke-interface/range {v22 .. v22}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_f
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_18

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;

    invoke-interface {v15, v7}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;->downloadedMetadata(Z)V

    goto :goto_f

    :cond_16
    :try_start_f
    invoke-static {v5, v11, v12}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->deleteDownloadingEntry(Landroid/database/sqlite/SQLiteDatabase;J)V

    goto :goto_e

    :catchall_2
    move-exception v21

    monitor-exit v22
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_2

    throw v21

    :cond_17
    sget-object v22, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->sUpdateEventListeners:Ljava/util/List;

    invoke-static/range {v22 .. v22}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->linkedCopyOfList(Ljava/util/List;)Ljava/util/List;

    move-result-object v22

    invoke-interface/range {v22 .. v22}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_10
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_18

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;

    const-string v22, "id"

    move-object/from16 v0, v22

    invoke-virtual {v9, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-interface {v15, v0, v7}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;->wordListDownloadFinished(Ljava/lang/String;Z)V

    goto :goto_10

    :cond_18
    if-eqz v19, :cond_13

    const-string v22, "Publishing new dictionaries"

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/log/PrivateLog;->log(Ljava/lang/String;Landroid/content/Context;)V

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    const-string v24, "Publishing new dictionaries"

    aput-object v24, v22, v23

    invoke-static/range {v22 .. v22}, Lcom/google/android/inputmethod/latin/dictionarypack/Utils;->l([Ljava/lang/Object;)V

    sget-object v22, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->sUpdateEventListeners:Ljava/util/List;

    invoke-static/range {v22 .. v22}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->linkedCopyOfList(Ljava/util/List;)Ljava/util/List;

    move-result-object v22

    invoke-interface/range {v22 .. v22}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_11
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_12

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;

    invoke-interface {v15}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;->updateCycleCompleted()V

    goto :goto_11

    :cond_19
    :try_start_10
    invoke-static {v5, v11, v12}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->deleteDownloadingEntry(Landroid/database/sqlite/SQLiteDatabase;J)V

    goto/16 :goto_6

    :catchall_3
    move-exception v21

    monitor-exit v22
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_3

    throw v21

    :cond_1a
    sget-object v21, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->sUpdateEventListeners:Ljava/util/List;

    invoke-static/range {v21 .. v21}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->linkedCopyOfList(Ljava/util/List;)Ljava/util/List;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_12
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_1b

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;

    const-string v21, "id"

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-interface {v15, v0, v7}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;->wordListDownloadFinished(Ljava/lang/String;Z)V

    goto :goto_12

    :cond_1b
    if-eqz v19, :cond_1d

    const-string v21, "Publishing new dictionaries"

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/log/PrivateLog;->log(Ljava/lang/String;Landroid/content/Context;)V

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "Publishing new dictionaries"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/google/android/inputmethod/latin/dictionarypack/Utils;->l([Ljava/lang/Object;)V

    sget-object v21, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->sUpdateEventListeners:Ljava/util/List;

    invoke-static/range {v21 .. v21}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->linkedCopyOfList(Ljava/util/List;)Ljava/util/List;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_13
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_1c

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;

    invoke-interface {v15}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;->updateCycleCompleted()V

    goto :goto_13

    :cond_1c
    invoke-static/range {p0 .. p0}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->signalNewDictionaryState(Landroid/content/Context;)V

    :cond_1d
    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [J

    move-object/from16 v21, v0

    const/16 v22, 0x0

    aput-wide v11, v21, v22

    :goto_14
    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/app/DownloadManager;->remove([J)I

    goto/16 :goto_0

    :cond_1e
    :try_start_11
    invoke-static {v5, v11, v12}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->deleteDownloadingEntry(Landroid/database/sqlite/SQLiteDatabase;J)V

    goto/16 :goto_8

    :catchall_4
    move-exception v21

    monitor-exit v22
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_4

    throw v21

    :cond_1f
    sget-object v21, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->sUpdateEventListeners:Ljava/util/List;

    invoke-static/range {v21 .. v21}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->linkedCopyOfList(Ljava/util/List;)Ljava/util/List;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_15
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_20

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;

    const-string v21, "id"

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-interface {v15, v0, v7}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;->wordListDownloadFinished(Ljava/lang/String;Z)V

    goto :goto_15

    :cond_20
    if-eqz v19, :cond_22

    const-string v21, "Publishing new dictionaries"

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/log/PrivateLog;->log(Ljava/lang/String;Landroid/content/Context;)V

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "Publishing new dictionaries"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/google/android/inputmethod/latin/dictionarypack/Utils;->l([Ljava/lang/Object;)V

    sget-object v21, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->sUpdateEventListeners:Ljava/util/List;

    invoke-static/range {v21 .. v21}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->linkedCopyOfList(Ljava/util/List;)Ljava/util/List;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_16
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_21

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;

    invoke-interface {v15}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;->updateCycleCompleted()V

    goto :goto_16

    :cond_21
    invoke-static/range {p0 .. p0}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->signalNewDictionaryState(Landroid/content/Context;)V

    :cond_22
    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [J

    move-object/from16 v21, v0

    const/16 v22, 0x0

    aput-wide v11, v21, v22

    goto :goto_14

    :cond_23
    :try_start_12
    invoke-static {v5, v11, v12}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->deleteDownloadingEntry(Landroid/database/sqlite/SQLiteDatabase;J)V

    goto/16 :goto_a

    :catchall_5
    move-exception v21

    monitor-exit v22
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_5

    throw v21

    :cond_24
    sget-object v21, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->sUpdateEventListeners:Ljava/util/List;

    invoke-static/range {v21 .. v21}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->linkedCopyOfList(Ljava/util/List;)Ljava/util/List;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_17
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_25

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;

    const-string v21, "id"

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-interface {v15, v0, v7}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;->wordListDownloadFinished(Ljava/lang/String;Z)V

    goto :goto_17

    :cond_25
    if-eqz v19, :cond_27

    const-string v21, "Publishing new dictionaries"

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/log/PrivateLog;->log(Ljava/lang/String;Landroid/content/Context;)V

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "Publishing new dictionaries"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/google/android/inputmethod/latin/dictionarypack/Utils;->l([Ljava/lang/Object;)V

    sget-object v21, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->sUpdateEventListeners:Ljava/util/List;

    invoke-static/range {v21 .. v21}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->linkedCopyOfList(Ljava/util/List;)Ljava/util/List;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_18
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_26

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;

    invoke-interface {v15}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;->updateCycleCompleted()V

    goto :goto_18

    :cond_26
    invoke-static/range {p0 .. p0}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->signalNewDictionaryState(Landroid/content/Context;)V

    :cond_27
    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [J

    move-object/from16 v21, v0

    const/16 v22, 0x0

    aput-wide v11, v21, v22

    goto/16 :goto_14

    :cond_28
    :try_start_13
    invoke-static {v5, v11, v12}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->deleteDownloadingEntry(Landroid/database/sqlite/SQLiteDatabase;J)V

    goto/16 :goto_c

    :catchall_6
    move-exception v21

    monitor-exit v22
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_6

    throw v21

    :cond_29
    sget-object v21, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->sUpdateEventListeners:Ljava/util/List;

    invoke-static/range {v21 .. v21}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->linkedCopyOfList(Ljava/util/List;)Ljava/util/List;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_19
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_2a

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;

    const-string v21, "id"

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-interface {v15, v0, v7}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;->wordListDownloadFinished(Ljava/lang/String;Z)V

    goto :goto_19

    :cond_2a
    if-eqz v19, :cond_2c

    const-string v21, "Publishing new dictionaries"

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/log/PrivateLog;->log(Ljava/lang/String;Landroid/content/Context;)V

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "Publishing new dictionaries"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/google/android/inputmethod/latin/dictionarypack/Utils;->l([Ljava/lang/Object;)V

    sget-object v21, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->sUpdateEventListeners:Ljava/util/List;

    invoke-static/range {v21 .. v21}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->linkedCopyOfList(Ljava/util/List;)Ljava/util/List;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_1a
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_2b

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;

    invoke-interface {v15}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;->updateCycleCompleted()V

    goto :goto_1a

    :cond_2b
    invoke-static/range {p0 .. p0}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->signalNewDictionaryState(Landroid/content/Context;)V

    :cond_2c
    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [J

    move-object/from16 v21, v0

    const/16 v22, 0x0

    aput-wide v11, v21, v22

    goto/16 :goto_14

    :cond_2d
    :try_start_14
    invoke-static {v5, v11, v12}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->deleteDownloadingEntry(Landroid/database/sqlite/SQLiteDatabase;J)V

    goto/16 :goto_4

    :catchall_7
    move-exception v21

    monitor-exit v22
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_7

    throw v21

    :cond_2e
    sget-object v21, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->sUpdateEventListeners:Ljava/util/List;

    invoke-static/range {v21 .. v21}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->linkedCopyOfList(Ljava/util/List;)Ljava/util/List;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_1b
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_2f

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;

    const-string v21, "id"

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-interface {v15, v0, v7}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;->wordListDownloadFinished(Ljava/lang/String;Z)V

    goto :goto_1b

    :cond_2f
    if-eqz v19, :cond_31

    const-string v21, "Publishing new dictionaries"

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/log/PrivateLog;->log(Ljava/lang/String;Landroid/content/Context;)V

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "Publishing new dictionaries"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/google/android/inputmethod/latin/dictionarypack/Utils;->l([Ljava/lang/Object;)V

    sget-object v21, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->sUpdateEventListeners:Ljava/util/List;

    invoke-static/range {v21 .. v21}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->linkedCopyOfList(Ljava/util/List;)Ljava/util/List;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_1c
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_30

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;

    invoke-interface {v15}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;->updateCycleCompleted()V

    goto :goto_1c

    :cond_30
    invoke-static/range {p0 .. p0}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->signalNewDictionaryState(Landroid/content/Context;)V

    :cond_31
    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [J

    move-object/from16 v21, v0

    const/16 v22, 0x0

    aput-wide v11, v21, v22

    goto/16 :goto_14
.end method

.method public static getDownloadOverMeteredSetting(Landroid/content/Context;)I
    .locals 4
    .param p0    # Landroid/content/Context;

    invoke-static {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/CommonPreferences;->getCommonPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v2, "downloadOverMetered"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method private static getDownloadStatus(Landroid/app/DownloadManager;J)I
    .locals 10
    .param p0    # Landroid/app/DownloadManager;
    .param p1    # J

    new-instance v7, Landroid/app/DownloadManager$Query;

    invoke-direct {v7}, Landroid/app/DownloadManager$Query;-><init>()V

    const/4 v8, 0x1

    new-array v8, v8, [J

    const/4 v9, 0x0

    aput-wide p1, v8, v9

    invoke-virtual {v7, v8}, Landroid/app/DownloadManager$Query;->setFilterById([J)Landroid/app/DownloadManager$Query;

    move-result-object v4

    invoke-virtual {p0, v4}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;

    move-result-object v2

    if-nez v2, :cond_0

    const/16 v5, 0x10

    :goto_0
    return v5

    :cond_0
    const/16 v5, 0x10

    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v7

    if-eqz v7, :cond_2

    const-string v7, "status"

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    const-string v7, "reason"

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const/16 v7, 0x8

    if-eq v7, v6, :cond_1

    sget-object v7, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Permanent failure of download "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " with error code: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    move v5, v6

    :cond_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v7

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v7
.end method

.method private static getMetadataUri(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p0    # Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const v3, 0x7f070001

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    :goto_0
    return-object v2

    :cond_0
    const v3, 0x7f070002

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private static getTempFileName(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-array v2, v5, [Ljava/lang/Object;

    const-string v3, "Entering openTempFileOutput"

    aput-object v3, v2, v4

    invoke-static {v2}, Lcom/google/android/inputmethod/latin/dictionarypack/Utils;->l([Ljava/lang/Object;)V

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "___"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, ".dict"

    invoke-static {v2, v3, v0}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "File name is"

    aput-object v3, v2, v4

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v2}, Lcom/google/android/inputmethod/latin/dictionarypack/Utils;->l([Ljava/lang/Object;)V

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private static handleMetadata(Landroid/content/Context;Ljava/io/InputStream;)V
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/inputmethod/latin/dictionarypack/BadFormatException;
        }
    .end annotation

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-array v2, v5, [Ljava/lang/Object;

    const-string v3, "Entering handleMetadata"

    aput-object v3, v2, v4

    invoke-static {v2}, Lcom/google/android/inputmethod/latin/dictionarypack/Utils;->l([Ljava/lang/Object;)V

    new-instance v2, Ljava/io/InputStreamReader;

    invoke-direct {v2, p1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-static {v2}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataHandler;->readMetadata(Ljava/io/InputStreamReader;)Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "Downloaded metadata :"

    aput-object v3, v2, v4

    aput-object v1, v2, v5

    invoke-static {v2}, Lcom/google/android/inputmethod/latin/dictionarypack/Utils;->l([Ljava/lang/Object;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Downloaded metadata\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p0}, Lcom/google/android/inputmethod/latin/dictionarypack/log/PrivateLog;->log(Ljava/lang/String;Landroid/content/Context;)V

    invoke-static {p0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->computeUpgradeTo(Landroid/content/Context;Ljava/util/List;)Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;

    move-result-object v0

    new-instance v2, Lcom/google/android/inputmethod/latin/dictionarypack/LogProblemReporter;

    sget-object v3, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->TAG:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/google/android/inputmethod/latin/dictionarypack/LogProblemReporter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0, v2}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;->execute(Landroid/content/Context;Lcom/google/android/inputmethod/latin/dictionarypack/ProblemReporter;)V

    return-void
.end method

.method private static handleWordList(Landroid/content/Context;Ljava/io/InputStream;Landroid/content/ContentValues;)V
    .locals 8
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/io/InputStream;
    .param p2    # Landroid/content/ContentValues;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/inputmethod/latin/dictionarypack/BadFormatException;
        }
    .end annotation

    const/4 v7, 0x0

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "Downloaded a new word list :"

    aput-object v5, v4, v7

    const/4 v5, 0x1

    const-string v6, "description"

    invoke-virtual {p2, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v4}, Lcom/google/android/inputmethod/latin/dictionarypack/Utils;->l([Ljava/lang/Object;)V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Downloaded a new word list with description : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "description"

    invoke-virtual {p2, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, p0}, Lcom/google/android/inputmethod/latin/dictionarypack/log/PrivateLog;->log(Ljava/lang/String;Landroid/content/Context;)V

    const-string v4, "locale"

    invoke-virtual {p2, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->getTempFileName(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v4, "filename"

    invoke-virtual {p2, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v1, v7}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->copyFile(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    invoke-virtual {p0, v1}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/inputmethod/latin/dictionarypack/MD5Calculator;->checksum(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    return-void

    :cond_1
    const-string v4, "checksum"

    invoke-virtual {p2, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {p0, v1}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    new-instance v4, Lcom/google/android/inputmethod/latin/dictionarypack/BadFormatException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "MD5 checksum check failed : \""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\" <> \""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "checksum"

    invoke-virtual {p2, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/android/inputmethod/latin/dictionarypack/BadFormatException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method public static installIfNeverRequested(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 4

    const/4 v3, 0x0

    const-string v0, ":"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    array-length v2, v0

    if-ne v1, v2, :cond_1

    aget-object v0, v0, v3

    :goto_0
    const-string v1, "main"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    :goto_1
    return-void

    :cond_1
    const-string v0, "main"

    goto :goto_0

    :cond_2
    invoke-static {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/CommonPreferences;->getCommonPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->getDb(Landroid/content/Context;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->getContentValuesOfLatestAvailableWordlistById(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v1

    const/4 v0, 0x1

    const-string v2, "status"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v0, v2, :cond_0

    if-eqz p2, :cond_3

    invoke-static {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->getDownloadOverMeteredSetting(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->isActiveNetworkMetered()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->showDictionaryAvailableNotification(Landroid/content/Context;Landroid/content/ContentValues;)V

    goto :goto_1

    :cond_3
    new-instance v0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;

    invoke-direct {v0}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;-><init>()V

    new-instance v2, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$StartDownloadAction;

    invoke-static {v1}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->createFromContentValues(Landroid/content/ContentValues;)Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    move-result-object v1

    invoke-direct {v2, v1, v3}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$StartDownloadAction;-><init>(Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;->add(Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$Action;)V

    new-instance v1, Lcom/google/android/inputmethod/latin/dictionarypack/LogProblemReporter;

    sget-object v2, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->TAG:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/google/android/inputmethod/latin/dictionarypack/LogProblemReporter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;->execute(Landroid/content/Context;Lcom/google/android/inputmethod/latin/dictionarypack/ProblemReporter;)V

    goto :goto_1
.end method

.method private static linkedCopyOfList(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0, p0}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public static markAsBroken(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-static {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->getDb(Landroid/content/Context;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->deleteEntry(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)V

    return-void
.end method

.method public static markAsDeleted(Landroid/content/Context;Ljava/lang/String;II)V
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I

    invoke-static {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataHandler;->getCurrentMetadata(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataHandler;->findWordListById(Ljava/util/List;Ljava/lang/String;)Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    move-result-object v2

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;

    invoke-direct {v0}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;-><init>()V

    new-instance v3, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$FinishDeleteAction;

    invoke-direct {v3, v2}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$FinishDeleteAction;-><init>(Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;)V

    invoke-virtual {v0, v3}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;->add(Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$Action;)V

    new-instance v3, Lcom/google/android/inputmethod/latin/dictionarypack/LogProblemReporter;

    sget-object v4, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->TAG:Ljava/lang/String;

    invoke-direct {v3, v4}, Lcom/google/android/inputmethod/latin/dictionarypack/LogProblemReporter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0, v3}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;->execute(Landroid/content/Context;Lcom/google/android/inputmethod/latin/dictionarypack/ProblemReporter;)V

    invoke-static {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->signalNewDictionaryState(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public static markAsDeleting(Landroid/content/Context;Ljava/lang/String;II)V
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I

    invoke-static {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataHandler;->getCurrentMetadata(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataHandler;->findWordListById(Ljava/util/List;Ljava/lang/String;)Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    move-result-object v2

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;

    invoke-direct {v0}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;-><init>()V

    new-instance v3, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$DisableAction;

    invoke-direct {v3, v2}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$DisableAction;-><init>(Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;)V

    invoke-virtual {v0, v3}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;->add(Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$Action;)V

    new-instance v3, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$StartDeleteAction;

    invoke-direct {v3, v2}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$StartDeleteAction;-><init>(Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;)V

    invoke-virtual {v0, v3}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;->add(Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$Action;)V

    new-instance v3, Lcom/google/android/inputmethod/latin/dictionarypack/LogProblemReporter;

    sget-object v4, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->TAG:Ljava/lang/String;

    invoke-direct {v3, v4}, Lcom/google/android/inputmethod/latin/dictionarypack/LogProblemReporter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0, v3}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;->execute(Landroid/content/Context;Lcom/google/android/inputmethod/latin/dictionarypack/ProblemReporter;)V

    invoke-static {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->signalNewDictionaryState(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public static markAsUnused(Landroid/content/Context;Ljava/lang/String;II)V
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I

    invoke-static {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataHandler;->getCurrentMetadata(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataHandler;->findWordListById(Ljava/util/List;Ljava/lang/String;)Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    move-result-object v2

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;

    invoke-direct {v0}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;-><init>()V

    new-instance v3, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$DisableAction;

    invoke-direct {v3, v2}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$DisableAction;-><init>(Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;)V

    invoke-virtual {v0, v3}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;->add(Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$Action;)V

    new-instance v3, Lcom/google/android/inputmethod/latin/dictionarypack/LogProblemReporter;

    sget-object v4, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->TAG:Ljava/lang/String;

    invoke-direct {v3, v4}, Lcom/google/android/inputmethod/latin/dictionarypack/LogProblemReporter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0, v3}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;->execute(Landroid/content/Context;Lcom/google/android/inputmethod/latin/dictionarypack/ProblemReporter;)V

    invoke-static {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->signalNewDictionaryState(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public static markAsUsed(Landroid/content/Context;Ljava/lang/String;IIZ)V
    .locals 4

    invoke-static {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataHandler;->getCurrentMetadata(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataHandler;->findWordListById(Ljava/util/List;Ljava/lang/String;)Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v1, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;

    invoke-direct {v1}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;-><init>()V

    const/4 v2, 0x4

    if-eq v2, p3, :cond_1

    const/4 v2, 0x5

    if-ne v2, p3, :cond_2

    :cond_1
    new-instance v2, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$EnableAction;

    invoke-direct {v2, v0}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$EnableAction;-><init>(Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;)V

    invoke-virtual {v1, v2}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;->add(Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$Action;)V

    :goto_1
    new-instance v0, Lcom/google/android/inputmethod/latin/dictionarypack/LogProblemReporter;

    sget-object v2, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->TAG:Ljava/lang/String;

    invoke-direct {v0, v2}, Lcom/google/android/inputmethod/latin/dictionarypack/LogProblemReporter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0, v0}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;->execute(Landroid/content/Context;Lcom/google/android/inputmethod/latin/dictionarypack/ProblemReporter;)V

    invoke-static {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->signalNewDictionaryState(Landroid/content/Context;)V

    goto :goto_0

    :cond_2
    const/4 v2, 0x1

    if-ne v2, p3, :cond_3

    new-instance v2, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$StartDownloadAction;

    invoke-direct {v2, v0, p4}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$StartDownloadAction;-><init>(Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;Z)V

    invoke-virtual {v1, v2}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;->add(Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$Action;)V

    goto :goto_1

    :cond_3
    sget-object v0, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected state of the word list for markAsUsed : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private static readMetadataDownloadId(Landroid/content/Context;)J
    .locals 4
    .param p0    # Landroid/content/Context;

    const-string v1, "metadata_download_id"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "downloadId"

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    return-wide v1
.end method

.method public static registerDownloadRequest(Landroid/app/DownloadManager;Landroid/app/DownloadManager$Request;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)J
    .locals 7
    .param p0    # Landroid/app/DownloadManager;
    .param p1    # Landroid/app/DownloadManager$Request;
    .param p2    # Landroid/database/sqlite/SQLiteDatabase;
    .param p3    # Ljava/lang/String;
    .param p4    # I

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "RegisterDownloadRequest for word list id : "

    aput-object v3, v2, v4

    aput-object p3, v2, v5

    const-string v3, ", version "

    aput-object v3, v2, v6

    const/4 v3, 0x3

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, Lcom/google/android/inputmethod/latin/dictionarypack/Utils;->l([Ljava/lang/Object;)V

    sget-object v3, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->sharedIdProtector:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    invoke-virtual {p0, p1}, Landroid/app/DownloadManager;->enqueue(Landroid/app/DownloadManager$Request;)J

    move-result-wide v0

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "Download requested with id"

    aput-object v5, v2, v4

    const/4 v4, 0x1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v2, v4

    invoke-static {v2}, Lcom/google/android/inputmethod/latin/dictionarypack/Utils;->l([Ljava/lang/Object;)V

    invoke-static {p2, p3, p4, v0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->markEntryAsDownloading(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;IJ)V

    monitor-exit v3

    return-wide v0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public static registerUpdateEventListener(Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;)V
    .locals 1
    .param p0    # Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;

    sget-object v0, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->sUpdateEventListeners:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public static setDownloadOverMeteredSetting(Landroid/content/Context;Z)V
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Z

    invoke-static {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/CommonPreferences;->getCommonPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "downloadOverMetered"

    if-eqz p1, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void

    :cond_0
    const/4 v2, 0x2

    goto :goto_0
.end method

.method private static showDictionaryAvailableNotification(Landroid/content/Context;Landroid/content/ContentValues;)V
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x1

    const-string v0, "locale"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v2, Lcom/google/android/inputmethod/latin/dictionarypack/DownloadOverMeteredDialog;

    invoke-virtual {v0, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v2, "wordlist_to_download"

    const-string v3, "id"

    invoke-virtual {p1, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "size"

    const-string v3, "filesize"

    invoke-virtual {p1, v3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v2, "locale"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v2, 0x10800000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const/high16 v2, 0x48000000

    invoke-static {p0, v6, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    const-string v0, "notification"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    if-eqz v2, :cond_0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {v1}, Lcom/google/android/inputmethod/latin/dictionarypack/LocaleUtils;->constructLocaleFromString(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v1

    if-nez v1, :cond_2

    const-string v1, ""

    :goto_1
    const v3, 0x7f070021

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v5, [Ljava/lang/Object;

    aput-object v1, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Landroid/app/Notification$Builder;

    invoke-direct {v3, p0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v5}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    const v3, 0x7f070022

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/app/Notification$Builder;->setOnlyAlertOnce(Z)Landroid/app/Notification$Builder;

    move-result-object v1

    const/high16 v2, 0x7f020000

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {v0, v5, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Ljava/util/Locale;->getDisplayLanguage()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method private static signalNewDictionaryState(Landroid/content/Context;)V
    .locals 2
    .param p0    # Landroid/content/Context;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.inputmethod.latin.dictionarypack.newdict"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method public static unregisterUpdateEventListener(Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;)V
    .locals 1
    .param p0    # Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;

    sget-object v0, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->sUpdateEventListeners:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public static update(Landroid/content/Context;Z)V
    .locals 11
    .param p0    # Landroid/content/Context;
    .param p1    # Z

    const/4 v7, 0x2

    const/4 v10, 0x1

    const/4 v6, 0x0

    const-string v8, "Update"

    invoke-static {v8, p0}, Lcom/google/android/inputmethod/latin/dictionarypack/log/PrivateLog;->log(Ljava/lang/String;Landroid/content/Context;)V

    new-array v8, v10, [Ljava/lang/Object;

    const-string v9, "Entering update"

    aput-object v9, v8, v6

    invoke-static {v8}, Lcom/google/android/inputmethod/latin/dictionarypack/Utils;->l([Ljava/lang/Object;)V

    new-instance v3, Landroid/app/DownloadManager$Request;

    invoke-static {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->getMetadataUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-direct {v3, v8}, Landroid/app/DownloadManager$Request;-><init>(Landroid/net/Uri;)V

    new-array v8, v7, [Ljava/lang/Object;

    const-string v9, "Request ="

    aput-object v9, v8, v6

    aput-object v3, v8, v10

    invoke-static {v8}, Lcom/google/android/inputmethod/latin/dictionarypack/Utils;->l([Ljava/lang/Object;)V

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    if-nez p1, :cond_0

    const/high16 v8, 0x7f060000

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v8

    invoke-virtual {v3, v8}, Landroid/app/DownloadManager$Request;->setAllowedOverMetered(Z)Landroid/app/DownloadManager$Request;

    const v8, 0x7f060001

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v8

    invoke-virtual {v3, v8}, Landroid/app/DownloadManager$Request;->setAllowedOverRoaming(Z)Landroid/app/DownloadManager$Request;

    :cond_0
    if-eqz p1, :cond_1

    const v8, 0x7f060005

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v4

    :goto_0
    const v8, 0x7f070007

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Landroid/app/DownloadManager$Request;->setTitle(Ljava/lang/CharSequence;)Landroid/app/DownloadManager$Request;

    if-eqz v4, :cond_2

    :goto_1
    invoke-virtual {v3, v6}, Landroid/app/DownloadManager$Request;->setNotificationVisibility(I)Landroid/app/DownloadManager$Request;

    const v6, 0x7f060003

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v6

    invoke-virtual {v3, v6}, Landroid/app/DownloadManager$Request;->setVisibleInDownloadsUi(Z)Landroid/app/DownloadManager$Request;

    const-string v6, "download"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/DownloadManager;

    if-nez v2, :cond_3

    :goto_2
    return-void

    :cond_1
    const v8, 0x7f060004

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v4

    goto :goto_0

    :cond_2
    move v6, v7

    goto :goto_1

    :cond_3
    invoke-static {p0, v2}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->cancelUpdateWithDownloadManager(Landroid/content/Context;Landroid/app/DownloadManager;)V

    sget-object v7, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->sharedIdProtector:Ljava/lang/Object;

    monitor-enter v7

    :try_start_0
    invoke-virtual {v2, v3}, Landroid/app/DownloadManager;->enqueue(Landroid/app/DownloadManager$Request;)J

    move-result-wide v0

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string v9, "Metadata download requested with id"

    aput-object v9, v6, v8

    const/4 v8, 0x1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v6, v8

    invoke-static {v6}, Lcom/google/android/inputmethod/latin/dictionarypack/Utils;->l([Ljava/lang/Object;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->writeMetadataDownloadId(Landroid/content/Context;J)V

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Requested download with id "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, p0}, Lcom/google/android/inputmethod/latin/dictionarypack/log/PrivateLog;->log(Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_2

    :catchall_0
    move-exception v6

    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v6
.end method

.method private static writeMetadataDownloadId(Landroid/content/Context;J)V
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # J

    const-string v2, "metadata_download_id"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "downloadId"

    invoke-interface {v0, v2, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method
