.class public Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;
.super Landroid/preference/DialogPreference;
.source "WordListPreference.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static final sStatusActionList:[[[I


# instance fields
.field final mContext:Landroid/content/Context;

.field public final mId:Ljava/lang/String;

.field public mStatus:I

.field public final mVersion:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x2

    const-class v0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->TAG:Ljava/lang/String;

    const/4 v0, 0x6

    new-array v0, v0, [[[I

    new-array v1, v6, [[I

    new-array v2, v5, [I

    aput-object v2, v1, v5

    aput-object v1, v0, v5

    new-array v1, v6, [[I

    new-array v2, v4, [I

    fill-array-data v2, :array_0

    aput-object v2, v1, v5

    aput-object v1, v0, v6

    new-array v1, v6, [[I

    new-array v2, v4, [I

    fill-array-data v2, :array_1

    aput-object v2, v1, v5

    aput-object v1, v0, v4

    const/4 v1, 0x3

    new-array v2, v4, [[I

    new-array v3, v4, [I

    fill-array-data v3, :array_2

    aput-object v3, v2, v5

    new-array v3, v4, [I

    fill-array-data v3, :array_3

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-array v2, v4, [[I

    new-array v3, v4, [I

    fill-array-data v3, :array_4

    aput-object v3, v2, v5

    new-array v3, v4, [I

    fill-array-data v3, :array_5

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-array v2, v6, [[I

    new-array v3, v4, [I

    fill-array-data v3, :array_6

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->sStatusActionList:[[[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f070019
        0x1
    .end array-data

    :array_1
    .array-data 4
        0x7f07001a
        0x2
    .end array-data

    :array_2
    .array-data 4
        0x7f07001b
        0x2
    .end array-data

    :array_3
    .array-data 4
        0x7f07001d
        0x3
    .end array-data

    :array_4
    .array-data 4
        0x7f07001c
        0x1
    .end array-data

    :array_5
    .array-data 4
        0x7f07001d
        0x3
    .end array-data

    :array_6
    .array-data 4
        0x7f070019
        0x1
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILjava/util/Locale;Ljava/lang/String;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # Ljava/util/Locale;
    .param p5    # Ljava/lang/String;
    .param p6    # I

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object p1, p0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->mContext:Landroid/content/Context;

    iput p3, p0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->mVersion:I

    iput-object p2, p0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->mId:Ljava/lang/String;

    invoke-virtual {p0, p5}, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->setTitle(Ljava/lang/CharSequence;)V

    invoke-direct {p0, p6}, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->setStatus(I)V

    invoke-virtual {p0, p2}, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->setKey(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$000(II)I
    .locals 1
    .param p0    # I
    .param p1    # I

    invoke-static {p0, p1}, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->getActionIdFromStatusAndMenuEntry(II)I

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;)V
    .locals 0
    .param p0    # Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;

    invoke-direct {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->enableDict()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;)V
    .locals 0
    .param p0    # Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;

    invoke-direct {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->disableDict()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;)V
    .locals 0
    .param p0    # Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;

    invoke-direct {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->deleteDict()V

    return-void
.end method

.method static synthetic access$400()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private deleteDict()V
    .locals 5

    iget-object v1, p0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/inputmethod/latin/dictionarypack/CommonPreferences;->getCommonPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->mId:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/CommonPreferences;->disable(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    const/4 v1, 0x5

    invoke-direct {p0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->setStatus(I)V

    iget-object v1, p0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->mId:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->mVersion:I

    iget v4, p0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->mStatus:I

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->markAsDeleting(Landroid/content/Context;Ljava/lang/String;II)V

    return-void
.end method

.method private disableDict()V
    .locals 5

    iget-object v1, p0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/inputmethod/latin/dictionarypack/CommonPreferences;->getCommonPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->mId:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/CommonPreferences;->disable(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->mId:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->mVersion:I

    iget v4, p0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->mStatus:I

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->markAsUnused(Landroid/content/Context;Ljava/lang/String;II)V

    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->mStatus:I

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->setStatus(I)V

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->mStatus:I

    if-ne v1, v2, :cond_1

    const/4 v1, 0x4

    invoke-direct {p0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->setStatus(I)V

    goto :goto_0

    :cond_1
    sget-object v1, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected state of the word list for disabling "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->mStatus:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private enableDict()V
    .locals 6

    const/4 v5, 0x1

    iget-object v1, p0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/inputmethod/latin/dictionarypack/CommonPreferences;->getCommonPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->mId:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/CommonPreferences;->enable(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->mId:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->mVersion:I

    iget v4, p0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->mStatus:I

    invoke-static {v1, v2, v3, v4, v5}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->markAsUsed(Landroid/content/Context;Ljava/lang/String;IIZ)V

    iget v1, p0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->mStatus:I

    if-ne v5, v1, :cond_0

    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->setStatus(I)V

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->mStatus:I

    if-eq v1, v2, :cond_1

    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->mStatus:I

    if-ne v1, v2, :cond_2

    :cond_1
    const/4 v1, 0x3

    invoke-direct {p0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->setStatus(I)V

    goto :goto_0

    :cond_2
    sget-object v1, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected state of the word list for enabling "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->mStatus:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static getActionIdFromStatusAndMenuEntry(II)I
    .locals 4
    .param p0    # I
    .param p1    # I

    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->sStatusActionList:[[[I

    array-length v1, v1

    if-lt p0, v1, :cond_0

    sget-object v1, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown status "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_0
    sget-object v1, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->sStatusActionList:[[[I

    aget-object v1, v1, p0

    array-length v1, v1

    if-lt p1, v1, :cond_1

    sget-object v1, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown menu entry "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->sStatusActionList:[[[I

    aget-object v0, v0, p0

    aget-object v0, v0, p1

    const/4 v1, 0x1

    aget v0, v0, v1

    goto :goto_0
.end method

.method private getMenuEntries(I)[Ljava/lang/CharSequence;
    .locals 7
    .param p1    # I

    const/4 v6, 0x0

    sget-object v3, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->sStatusActionList:[[[I

    array-length v3, v3

    if-lt p1, v3, :cond_1

    sget-object v3, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unknown status "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-array v1, v6, [Ljava/lang/CharSequence;

    :cond_0
    return-object v1

    :cond_1
    sget-object v3, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->sStatusActionList:[[[I

    aget-object v0, v3, p1

    array-length v3, v0

    new-array v1, v3, [Ljava/lang/CharSequence;

    const/4 v2, 0x0

    :goto_0
    array-length v3, v1

    if-ge v2, v3, :cond_0

    iget-object v3, p0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->mContext:Landroid/content/Context;

    aget-object v4, v0, v2

    aget v4, v4, v6

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private getSummary(I)Ljava/lang/String;
    .locals 2
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    const-string v0, ""

    :goto_0
    return-object v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->mContext:Landroid/content/Context;

    const v1, 0x7f07000d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->mContext:Landroid/content/Context;

    const v1, 0x7f07000e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->mContext:Landroid/content/Context;

    const v1, 0x7f07000f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->mContext:Landroid/content/Context;

    const v1, 0x7f070010

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method private setStatus(I)V
    .locals 2
    .param p1    # I

    iget v1, p0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->mStatus:I

    if-ne p1, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput p1, p0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->mStatus:I

    invoke-direct {p0, p1}, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->getSummary(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->setSummary(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    goto :goto_0
.end method


# virtual methods
.method protected onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V
    .locals 3
    .param p1    # Landroid/app/AlertDialog$Builder;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    iget v0, p0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->mStatus:I

    invoke-direct {p0, v0}, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->getMenuEntries(I)[Ljava/lang/CharSequence;

    move-result-object v0

    new-instance v1, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference$1;

    invoke-direct {v1, p0}, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference$1;-><init>(Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->mContext:Landroid/content/Context;

    const v1, 0x7f070018

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference$2;

    invoke-direct {v1, p0}, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference$2;-><init>(Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {p1, v2, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    return-void
.end method
