.class public Lcom/google/android/inputmethod/latin/dictionarypack/log/PrivateLog;
.super Ljava/lang/Object;
.source "PrivateLog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/inputmethod/latin/dictionarypack/log/PrivateLog$1;,
        Lcom/google/android/inputmethod/latin/dictionarypack/log/PrivateLog$DebugHelper;
    }
.end annotation


# static fields
.field private static mDebugHelper:Lcom/google/android/inputmethod/latin/dictionarypack/log/PrivateLog$DebugHelper;

.field private static final sDateFormat:Ljava/text/SimpleDateFormat;

.field private static sInstance:Lcom/google/android/inputmethod/latin/dictionarypack/log/PrivateLog;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy/MM/dd HH:mm:ss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/inputmethod/latin/dictionarypack/log/PrivateLog;->sDateFormat:Ljava/text/SimpleDateFormat;

    new-instance v0, Lcom/google/android/inputmethod/latin/dictionarypack/log/PrivateLog;

    invoke-direct {v0}, Lcom/google/android/inputmethod/latin/dictionarypack/log/PrivateLog;-><init>()V

    sput-object v0, Lcom/google/android/inputmethod/latin/dictionarypack/log/PrivateLog;->sInstance:Lcom/google/android/inputmethod/latin/dictionarypack/log/PrivateLog;

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/inputmethod/latin/dictionarypack/log/PrivateLog;->mDebugHelper:Lcom/google/android/inputmethod/latin/dictionarypack/log/PrivateLog$DebugHelper;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static log(Ljava/lang/String;Landroid/content/ContentProvider;)V
    .locals 0
    .param p0    # Ljava/lang/String;
    .param p1    # Landroid/content/ContentProvider;

    return-void
.end method

.method public static log(Ljava/lang/String;Landroid/content/Context;)V
    .locals 0
    .param p0    # Ljava/lang/String;
    .param p1    # Landroid/content/Context;

    return-void
.end method
