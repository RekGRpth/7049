.class public Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;
.super Ljava/lang/Object;
.source "WordListMetadata.java"


# instance fields
.field public final mChecksum:Ljava/lang/String;

.field public final mDescription:Ljava/lang/String;

.field public final mFileSize:J

.field public final mFlags:I

.field public final mFormatVersion:I

.field public final mId:Ljava/lang/String;

.field public final mLastUpdate:J

.field public final mLocalFilename:Ljava/lang/String;

.field public final mLocale:Ljava/lang/String;

.field public final mRemoteFilename:Ljava/lang/String;

.field public final mType:I

.field public final mVersion:I


# direct methods
.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # J
    .param p6    # J
    .param p8    # Ljava/lang/String;
    .param p9    # Ljava/lang/String;
    .param p10    # Ljava/lang/String;
    .param p11    # I
    .param p12    # I
    .param p13    # I
    .param p14    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mId:Ljava/lang/String;

    iput p2, p0, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mType:I

    iput-object p3, p0, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mDescription:Ljava/lang/String;

    iput-wide p4, p0, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mLastUpdate:J

    iput-wide p6, p0, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mFileSize:J

    iput-object p8, p0, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mChecksum:Ljava/lang/String;

    iput-object p9, p0, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mLocalFilename:Ljava/lang/String;

    iput-object p10, p0, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mRemoteFilename:Ljava/lang/String;

    iput p11, p0, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mVersion:I

    iput p12, p0, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mFormatVersion:I

    iput p13, p0, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mFlags:I

    iput-object p14, p0, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mLocale:Ljava/lang/String;

    return-void
.end method

.method public static createFromContentValues(Landroid/content/ContentValues;)Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;
    .locals 22
    .param p0    # Landroid/content/ContentValues;

    const-string v1, "id"

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v1, "type"

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v20

    const-string v1, "description"

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v1, "date"

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v19

    const-string v1, "filesize"

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v16

    const-string v1, "checksum"

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v1, "filename"

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v1, "url"

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v1, "version"

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v21

    const-string v1, "formatversion"

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v18

    const-string v1, "flags"

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v17

    const-string v1, "locale"

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    if-eqz v2, :cond_0

    if-eqz v20, :cond_0

    if-eqz v4, :cond_0

    if-eqz v19, :cond_0

    if-eqz v16, :cond_0

    if-eqz v9, :cond_0

    if-eqz v10, :cond_0

    if-eqz v11, :cond_0

    if-eqz v21, :cond_0

    if-eqz v18, :cond_0

    if-eqz v17, :cond_0

    if-nez v15, :cond_1

    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    :cond_1
    new-instance v1, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v12

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Integer;->intValue()I

    move-result v13

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v14

    invoke-direct/range {v1 .. v15}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;-><init>(Ljava/lang/String;ILjava/lang/String;JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;)V

    return-object v1
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    const-class v1, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\nType : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "\nDescription : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mDescription:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\nLastUpdate : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mLastUpdate:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, "\nFileSize : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mFileSize:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, "\nChecksum : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mChecksum:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\nLocalFilename : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mLocalFilename:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\nRemoteFilename : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mRemoteFilename:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\nVersion : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mVersion:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "\nFormatVersion : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mFormatVersion:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "\nFlags : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mFlags:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "\nLocale : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mLocale:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
