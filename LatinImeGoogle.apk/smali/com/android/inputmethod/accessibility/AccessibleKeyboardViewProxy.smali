.class public Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;
.super Landroid/support/v4/view/AccessibilityDelegateCompat;
.source "AccessibleKeyboardViewProxy.java"


# static fields
.field private static final sInstance:Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;


# instance fields
.field private mAccessibilityNodeProvider:Lcom/android/inputmethod/accessibility/AccessibilityEntityProvider;

.field private mEdgeSlop:I

.field private mInputMethod:Landroid/inputmethodservice/InputMethodService;

.field private mLastHoverKey:Lcom/android/inputmethod/keyboard/Key;

.field private mView:Lcom/android/inputmethod/keyboard/LatinKeyboardView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;

    invoke-direct {v0}, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;-><init>()V

    sput-object v0, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->sInstance:Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/view/AccessibilityDelegateCompat;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->mLastHoverKey:Lcom/android/inputmethod/keyboard/Key;

    return-void
.end method

.method private getAccessibilityNodeProvider()Lcom/android/inputmethod/accessibility/AccessibilityEntityProvider;
    .locals 3

    iget-object v0, p0, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->mAccessibilityNodeProvider:Lcom/android/inputmethod/accessibility/AccessibilityEntityProvider;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/inputmethod/accessibility/AccessibilityEntityProvider;

    iget-object v1, p0, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->mView:Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    iget-object v2, p0, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->mInputMethod:Landroid/inputmethodservice/InputMethodService;

    invoke-direct {v0, v1, v2}, Lcom/android/inputmethod/accessibility/AccessibilityEntityProvider;-><init>(Lcom/android/inputmethod/keyboard/KeyboardView;Landroid/inputmethodservice/InputMethodService;)V

    iput-object v0, p0, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->mAccessibilityNodeProvider:Lcom/android/inputmethod/accessibility/AccessibilityEntityProvider;

    :cond_0
    iget-object v0, p0, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->mAccessibilityNodeProvider:Lcom/android/inputmethod/accessibility/AccessibilityEntityProvider;

    return-object v0
.end method

.method public static getInstance()Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;
    .locals 1

    sget-object v0, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->sInstance:Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;

    return-object v0
.end method

.method public static init(Landroid/inputmethodservice/InputMethodService;)V
    .locals 1
    .param p0    # Landroid/inputmethodservice/InputMethodService;

    sget-object v0, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->sInstance:Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;

    invoke-direct {v0, p0}, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->initInternal(Landroid/inputmethodservice/InputMethodService;)V

    return-void
.end method

.method private initInternal(Landroid/inputmethodservice/InputMethodService;)V
    .locals 1
    .param p1    # Landroid/inputmethodservice/InputMethodService;

    iput-object p1, p0, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->mInputMethod:Landroid/inputmethodservice/InputMethodService;

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledEdgeSlop()I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->mEdgeSlop:I

    return-void
.end method

.method private onHoverKey(Lcom/android/inputmethod/keyboard/Key;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1    # Lcom/android/inputmethod/keyboard/Key;
    .param p2    # Landroid/view/MotionEvent;

    if-nez p1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-direct {p0}, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->getAccessibilityNodeProvider()Lcom/android/inputmethod/accessibility/AccessibilityEntityProvider;

    move-result-object v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    :pswitch_0
    const/16 v1, 0x80

    invoke-virtual {v0, p1, v1}, Lcom/android/inputmethod/accessibility/AccessibilityEntityProvider;->sendAccessibilityEventForKey(Lcom/android/inputmethod/keyboard/Key;I)V

    const/16 v1, 0x40

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Lcom/android/inputmethod/accessibility/AccessibilityEntityProvider;->performActionForKey(Lcom/android/inputmethod/keyboard/Key;ILandroid/os/Bundle;)Z

    goto :goto_1

    :pswitch_1
    const/16 v1, 0x100

    invoke-virtual {v0, p1, v1}, Lcom/android/inputmethod/accessibility/AccessibilityEntityProvider;->sendAccessibilityEventForKey(Lcom/android/inputmethod/keyboard/Key;I)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private onTransitionKey(Lcom/android/inputmethod/keyboard/Key;Lcom/android/inputmethod/keyboard/Key;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1    # Lcom/android/inputmethod/keyboard/Key;
    .param p2    # Lcom/android/inputmethod/keyboard/Key;
    .param p3    # Landroid/view/MotionEvent;

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/16 v2, 0xa

    invoke-virtual {p3, v2}, Landroid/view/MotionEvent;->setAction(I)V

    invoke-direct {p0, p2, p3}, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->onHoverKey(Lcom/android/inputmethod/keyboard/Key;Landroid/view/MotionEvent;)Z

    const/16 v2, 0x9

    invoke-virtual {p3, v2}, Landroid/view/MotionEvent;->setAction(I)V

    invoke-direct {p0, p1, p3}, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->onHoverKey(Lcom/android/inputmethod/keyboard/Key;Landroid/view/MotionEvent;)Z

    const/4 v2, 0x7

    invoke-virtual {p3, v2}, Landroid/view/MotionEvent;->setAction(I)V

    invoke-direct {p0, p1, p3}, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->onHoverKey(Lcom/android/inputmethod/keyboard/Key;Landroid/view/MotionEvent;)Z

    move-result v0

    invoke-virtual {p3, v1}, Landroid/view/MotionEvent;->setAction(I)V

    return v0
.end method

.method private pointInView(II)Z
    .locals 2
    .param p1    # I
    .param p2    # I

    iget v0, p0, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->mEdgeSlop:I

    if-lt p1, v0, :cond_0

    iget v0, p0, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->mEdgeSlop:I

    if-lt p2, v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->mView:Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->getWidth()I

    move-result v0

    iget v1, p0, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->mEdgeSlop:I

    sub-int/2addr v0, v1

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->mView:Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->getHeight()I

    move-result v0

    iget v1, p0, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->mEdgeSlop:I

    sub-int/2addr v0, v1

    if-ge p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public dispatchHoverEvent(Landroid/view/MotionEvent;Lcom/android/inputmethod/keyboard/PointerTracker;)Z
    .locals 5
    .param p1    # Landroid/view/MotionEvent;
    .param p2    # Lcom/android/inputmethod/keyboard/PointerTracker;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v2, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v3, v4

    invoke-virtual {p2, v2, v3}, Lcom/android/inputmethod/keyboard/PointerTracker;->getKeyOn(II)Lcom/android/inputmethod/keyboard/Key;

    move-result-object v0

    iget-object v1, p0, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->mLastHoverKey:Lcom/android/inputmethod/keyboard/Key;

    iput-object v0, p0, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->mLastHoverKey:Lcom/android/inputmethod/keyboard/Key;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :pswitch_0
    const/4 v4, 0x0

    :goto_0
    return v4

    :pswitch_1
    invoke-direct {p0, v2, v3}, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->pointInView(II)Z

    move-result v4

    if-eqz v4, :cond_0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->getAccessibilityNodeProvider()Lcom/android/inputmethod/accessibility/AccessibilityEntityProvider;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/android/inputmethod/accessibility/AccessibilityEntityProvider;->simulateKeyPress(Lcom/android/inputmethod/keyboard/Key;)V

    :cond_0
    :pswitch_2
    invoke-direct {p0, v0, p1}, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->onHoverKey(Lcom/android/inputmethod/keyboard/Key;Landroid/view/MotionEvent;)Z

    move-result v4

    goto :goto_0

    :pswitch_3
    if-eq v0, v1, :cond_1

    invoke-direct {p0, v0, v1, p1}, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->onTransitionKey(Lcom/android/inputmethod/keyboard/Key;Lcom/android/inputmethod/keyboard/Key;Landroid/view/MotionEvent;)Z

    move-result v4

    goto :goto_0

    :cond_1
    invoke-direct {p0, v0, p1}, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->onHoverKey(Lcom/android/inputmethod/keyboard/Key;Landroid/view/MotionEvent;)Z

    move-result v4

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic getAccessibilityNodeProvider(Landroid/view/View;)Landroid/support/v4/view/accessibility/AccessibilityNodeProviderCompat;
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->getAccessibilityNodeProvider(Landroid/view/View;)Lcom/android/inputmethod/accessibility/AccessibilityEntityProvider;

    move-result-object v0

    return-object v0
.end method

.method public getAccessibilityNodeProvider(Landroid/view/View;)Lcom/android/inputmethod/accessibility/AccessibilityEntityProvider;
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-direct {p0}, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->getAccessibilityNodeProvider()Lcom/android/inputmethod/accessibility/AccessibilityEntityProvider;

    move-result-object v0

    return-object v0
.end method

.method public notifyShiftState()V
    .locals 6

    iget-object v5, p0, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->mView:Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    invoke-virtual {v5}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->getKeyboard()Lcom/android/inputmethod/keyboard/Keyboard;

    move-result-object v2

    iget-object v3, v2, Lcom/android/inputmethod/keyboard/Keyboard;->mId:Lcom/android/inputmethod/keyboard/KeyboardId;

    iget v1, v3, Lcom/android/inputmethod/keyboard/KeyboardId;->mElementId:I

    iget-object v5, p0, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->mView:Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    invoke-virtual {v5}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->getContext()Landroid/content/Context;

    move-result-object v0

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    const v5, 0x7f0a0077

    invoke-virtual {v0, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    :goto_0
    invoke-static {}, Lcom/android/inputmethod/accessibility/AccessibilityUtils;->getInstance()Lcom/android/inputmethod/accessibility/AccessibilityUtils;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/android/inputmethod/accessibility/AccessibilityUtils;->speak(Ljava/lang/CharSequence;)V

    return-void

    :pswitch_1
    const v5, 0x7f0a0076

    invoke-virtual {v0, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    goto :goto_0

    :pswitch_2
    const v5, 0x7f0a0075

    invoke-virtual {v0, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public notifySymbolsState()V
    .locals 7

    iget-object v6, p0, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->mView:Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    invoke-virtual {v6}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->getKeyboard()Lcom/android/inputmethod/keyboard/Keyboard;

    move-result-object v2

    iget-object v6, p0, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->mView:Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    invoke-virtual {v6}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v3, v2, Lcom/android/inputmethod/keyboard/Keyboard;->mId:Lcom/android/inputmethod/keyboard/KeyboardId;

    iget v1, v3, Lcom/android/inputmethod/keyboard/KeyboardId;->mElementId:I

    packed-switch v1, :pswitch_data_0

    const/4 v4, -0x1

    :goto_0
    if-gez v4, :cond_0

    :goto_1
    return-void

    :pswitch_0
    const v4, 0x7f0a0079

    goto :goto_0

    :pswitch_1
    const v4, 0x7f0a0078

    goto :goto_0

    :pswitch_2
    const v4, 0x7f0a007a

    goto :goto_0

    :pswitch_3
    const v4, 0x7f0a007b

    goto :goto_0

    :cond_0
    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {}, Lcom/android/inputmethod/accessibility/AccessibilityUtils;->getInstance()Lcom/android/inputmethod/accessibility/AccessibilityUtils;

    move-result-object v6

    invoke-virtual {v6, v5}, Lcom/android/inputmethod/accessibility/AccessibilityUtils;->speak(Ljava/lang/CharSequence;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setKeyboard(Lcom/android/inputmethod/keyboard/Keyboard;)V
    .locals 1
    .param p1    # Lcom/android/inputmethod/keyboard/Keyboard;

    iget-object v0, p0, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->mAccessibilityNodeProvider:Lcom/android/inputmethod/accessibility/AccessibilityEntityProvider;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->mAccessibilityNodeProvider:Lcom/android/inputmethod/accessibility/AccessibilityEntityProvider;

    invoke-virtual {v0, p1}, Lcom/android/inputmethod/accessibility/AccessibilityEntityProvider;->setKeyboard(Lcom/android/inputmethod/keyboard/Keyboard;)V

    :cond_0
    return-void
.end method

.method public setView(Lcom/android/inputmethod/keyboard/LatinKeyboardView;)V
    .locals 1
    .param p1    # Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->mView:Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    invoke-static {p1, p0}, Landroid/support/v4/view/ViewCompat;->setAccessibilityDelegate(Landroid/view/View;Landroid/support/v4/view/AccessibilityDelegateCompat;)V

    iget-object v0, p0, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->mAccessibilityNodeProvider:Lcom/android/inputmethod/accessibility/AccessibilityEntityProvider;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->mAccessibilityNodeProvider:Lcom/android/inputmethod/accessibility/AccessibilityEntityProvider;

    invoke-virtual {v0, p1}, Lcom/android/inputmethod/accessibility/AccessibilityEntityProvider;->setView(Lcom/android/inputmethod/keyboard/KeyboardView;)V

    goto :goto_0
.end method
