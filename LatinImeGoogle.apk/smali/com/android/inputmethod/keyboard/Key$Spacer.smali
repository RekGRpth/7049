.class public Lcom/android/inputmethod/keyboard/Key$Spacer;
.super Lcom/android/inputmethod/keyboard/Key;
.source "Key.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/keyboard/Key;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Spacer"
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lcom/android/inputmethod/keyboard/Keyboard$Params;Lcom/android/inputmethod/keyboard/Keyboard$Builder$Row;Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 0
    .param p1    # Landroid/content/res/Resources;
    .param p2    # Lcom/android/inputmethod/keyboard/Keyboard$Params;
    .param p3    # Lcom/android/inputmethod/keyboard/Keyboard$Builder$Row;
    .param p4    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/inputmethod/keyboard/Key;-><init>(Landroid/content/res/Resources;Lcom/android/inputmethod/keyboard/Keyboard$Params;Lcom/android/inputmethod/keyboard/Keyboard$Builder$Row;Lorg/xmlpull/v1/XmlPullParser;)V

    return-void
.end method

.method protected constructor <init>(Lcom/android/inputmethod/keyboard/Keyboard$Params;IIII)V
    .locals 12
    .param p1    # Lcom/android/inputmethod/keyboard/Keyboard$Params;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, -0xb

    const/4 v6, 0x0

    const/4 v11, 0x0

    move-object v0, p0

    move-object v1, p1

    move v7, p2

    move v8, p3

    move/from16 v9, p4

    move/from16 v10, p5

    invoke-direct/range {v0 .. v11}, Lcom/android/inputmethod/keyboard/Key;-><init>(Lcom/android/inputmethod/keyboard/Keyboard$Params;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;IIIII)V

    return-void
.end method
