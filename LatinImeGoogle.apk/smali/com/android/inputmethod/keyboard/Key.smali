.class public Lcom/android/inputmethod/keyboard/Key;
.super Ljava/lang/Object;
.source "Key.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/inputmethod/keyboard/Key$Spacer;
    }
.end annotation


# static fields
.field private static final KEY_STATE_ACTIVE_NORMAL:[I

.field private static final KEY_STATE_ACTIVE_PRESSED:[I

.field private static final KEY_STATE_FUNCTIONAL_NORMAL:[I

.field private static final KEY_STATE_FUNCTIONAL_PRESSED:[I

.field private static final KEY_STATE_NORMAL:[I

.field private static final KEY_STATE_NORMAL_HIGHLIGHT_OFF:[I

.field private static final KEY_STATE_NORMAL_HIGHLIGHT_ON:[I

.field private static final KEY_STATE_PRESSED:[I

.field private static final KEY_STATE_PRESSED_HIGHLIGHT_OFF:[I

.field private static final KEY_STATE_PRESSED_HIGHLIGHT_ON:[I

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mActionFlags:I

.field public final mAltCode:I

.field public final mBackgroundType:I

.field public final mCode:I

.field private final mDisabledIconId:I

.field private mEnabled:Z

.field private final mHashCode:I

.field public final mHeight:I

.field public final mHintLabel:Ljava/lang/String;

.field public final mHitBox:Landroid/graphics/Rect;

.field public final mHorizontalGap:I

.field private final mIconId:I

.field public final mLabel:Ljava/lang/String;

.field private final mLabelFlags:I

.field public final mMoreKeys:[Lcom/android/inputmethod/keyboard/internal/KeySpecParser$MoreKeySpec;

.field private final mMoreKeysColumnAndFlags:I

.field public final mOutputText:Ljava/lang/CharSequence;

.field private mPressed:Z

.field private final mPreviewIconId:I

.field public final mVerticalGap:I

.field public final mVisualInsetsLeft:I

.field public final mVisualInsetsRight:I

.field public final mWidth:I

.field public final mX:I

.field public final mY:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    const-class v0, Lcom/android/inputmethod/keyboard/Key;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/inputmethod/keyboard/Key;->TAG:Ljava/lang/String;

    new-array v0, v4, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/inputmethod/keyboard/Key;->KEY_STATE_NORMAL_HIGHLIGHT_ON:[I

    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/inputmethod/keyboard/Key;->KEY_STATE_PRESSED_HIGHLIGHT_ON:[I

    new-array v0, v3, [I

    const v1, 0x101009f

    aput v1, v0, v2

    sput-object v0, Lcom/android/inputmethod/keyboard/Key;->KEY_STATE_NORMAL_HIGHLIGHT_OFF:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/android/inputmethod/keyboard/Key;->KEY_STATE_PRESSED_HIGHLIGHT_OFF:[I

    new-array v0, v2, [I

    sput-object v0, Lcom/android/inputmethod/keyboard/Key;->KEY_STATE_NORMAL:[I

    new-array v0, v3, [I

    const v1, 0x10100a7

    aput v1, v0, v2

    sput-object v0, Lcom/android/inputmethod/keyboard/Key;->KEY_STATE_PRESSED:[I

    new-array v0, v3, [I

    const v1, 0x10100a3

    aput v1, v0, v2

    sput-object v0, Lcom/android/inputmethod/keyboard/Key;->KEY_STATE_FUNCTIONAL_NORMAL:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/android/inputmethod/keyboard/Key;->KEY_STATE_FUNCTIONAL_PRESSED:[I

    new-array v0, v3, [I

    const v1, 0x10100a2

    aput v1, v0, v2

    sput-object v0, Lcom/android/inputmethod/keyboard/Key;->KEY_STATE_ACTIVE_NORMAL:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/android/inputmethod/keyboard/Key;->KEY_STATE_ACTIVE_PRESSED:[I

    return-void

    :array_0
    .array-data 4
        0x101009f
        0x10100a0
    .end array-data

    :array_1
    .array-data 4
        0x10100a7
        0x101009f
        0x10100a0
    .end array-data

    :array_2
    .array-data 4
        0x10100a7
        0x101009f
    .end array-data

    :array_3
    .array-data 4
        0x10100a3
        0x10100a7
    .end array-data

    :array_4
    .array-data 4
        0x10100a2
        0x10100a7
    .end array-data
.end method

.method public constructor <init>(Landroid/content/res/Resources;Lcom/android/inputmethod/keyboard/Keyboard$Params;Lcom/android/inputmethod/keyboard/Keyboard$Builder$Row;Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 25
    .param p1    # Landroid/content/res/Resources;
    .param p2    # Lcom/android/inputmethod/keyboard/Keyboard$Params;
    .param p3    # Lcom/android/inputmethod/keyboard/Keyboard$Builder$Row;
    .param p4    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    new-instance v21, Landroid/graphics/Rect;

    invoke-direct/range {v21 .. v21}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/inputmethod/keyboard/Key;->mHitBox:Landroid/graphics/Rect;

    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/inputmethod/keyboard/Key;->mEnabled:Z

    invoke-virtual/range {p0 .. p0}, Lcom/android/inputmethod/keyboard/Key;->isSpacer()Z

    move-result v21

    if-eqz v21, :cond_5

    const/4 v7, 0x0

    :goto_0
    move-object/from16 v0, p3

    iget v10, v0, Lcom/android/inputmethod/keyboard/Keyboard$Builder$Row;->mRowHeight:I

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mVerticalGap:I

    move/from16 v21, v0

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/inputmethod/keyboard/Key;->mVerticalGap:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/inputmethod/keyboard/Key;->mVerticalGap:I

    move/from16 v21, v0

    sub-int v21, v10, v21

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/inputmethod/keyboard/Key;->mHeight:I

    invoke-static/range {p4 .. p4}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v21

    sget-object v22, Lcom/android/inputmethod/latin/R$styleable;->Keyboard_Key:[I

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v9

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mKeyStyles:Lcom/android/inputmethod/keyboard/internal/KeyStyles;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, p4

    invoke-virtual {v0, v9, v1}, Lcom/android/inputmethod/keyboard/internal/KeyStyles;->getKeyStyle(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;)Lcom/android/inputmethod/keyboard/internal/KeyStyles$KeyStyle;

    move-result-object v19

    move-object/from16 v0, p3

    invoke-virtual {v0, v9}, Lcom/android/inputmethod/keyboard/Keyboard$Builder$Row;->getKeyX(Landroid/content/res/TypedArray;)F

    move-result v12

    move-object/from16 v0, p3

    invoke-virtual {v0, v9, v12}, Lcom/android/inputmethod/keyboard/Keyboard$Builder$Row;->getKeyWidth(Landroid/content/res/TypedArray;F)F

    move-result v11

    invoke-virtual/range {p3 .. p3}, Lcom/android/inputmethod/keyboard/Keyboard$Builder$Row;->getKeyY()I

    move-result v13

    const/high16 v21, 0x40000000

    div-float v21, v7, v21

    add-float v21, v21, v12

    invoke-static/range {v21 .. v21}, Ljava/lang/Math;->round(F)I

    move-result v21

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/inputmethod/keyboard/Key;->mX:I

    move-object/from16 v0, p0

    iput v13, v0, Lcom/android/inputmethod/keyboard/Key;->mY:I

    sub-float v21, v11, v7

    invoke-static/range {v21 .. v21}, Ljava/lang/Math;->round(F)I

    move-result v21

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/inputmethod/keyboard/Key;->mWidth:I

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v21

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/inputmethod/keyboard/Key;->mHorizontalGap:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/Key;->mHitBox:Landroid/graphics/Rect;

    move-object/from16 v21, v0

    invoke-static {v12}, Ljava/lang/Math;->round(F)I

    move-result v22

    add-float v23, v12, v11

    invoke-static/range {v23 .. v23}, Ljava/lang/Math;->round(F)I

    move-result v23

    add-int/lit8 v23, v23, 0x1

    add-int v24, v13, v10

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-virtual {v0, v1, v13, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    add-float v21, v12, v11

    move-object/from16 v0, p3

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/keyboard/Keyboard$Builder$Row;->setXPos(F)V

    const/16 v21, 0x5

    invoke-virtual/range {p3 .. p3}, Lcom/android/inputmethod/keyboard/Keyboard$Builder$Row;->getDefaultBackgroundType()I

    move-result v22

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v9, v1, v2}, Lcom/android/inputmethod/keyboard/internal/KeyStyles$KeyStyle;->getInt(Landroid/content/res/TypedArray;II)I

    move-result v21

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/inputmethod/keyboard/Key;->mBackgroundType:I

    const/16 v21, 0xf

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mBaseWidth:I

    move/from16 v22, v0

    const/16 v23, 0x0

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-static {v9, v0, v1, v2}, Lcom/android/inputmethod/keyboard/Keyboard$Builder;->getDimensionOrFraction(Landroid/content/res/TypedArray;IIF)F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Math;->round(F)I

    move-result v21

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/inputmethod/keyboard/Key;->mVisualInsetsLeft:I

    const/16 v21, 0x10

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mBaseWidth:I

    move/from16 v22, v0

    const/16 v23, 0x0

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-static {v9, v0, v1, v2}, Lcom/android/inputmethod/keyboard/Keyboard$Builder;->getDimensionOrFraction(Landroid/content/res/TypedArray;IIF)F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Math;->round(F)I

    move-result v21

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/inputmethod/keyboard/Key;->mVisualInsetsRight:I

    const/16 v21, 0xb

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v9, v1}, Lcom/android/inputmethod/keyboard/internal/KeyStyles$KeyStyle;->getString(Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser;->getIconId(Ljava/lang/String;)I

    move-result v21

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/inputmethod/keyboard/Key;->mIconId:I

    const/16 v21, 0xc

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v9, v1}, Lcom/android/inputmethod/keyboard/internal/KeyStyles$KeyStyle;->getString(Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser;->getIconId(Ljava/lang/String;)I

    move-result v21

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/inputmethod/keyboard/Key;->mDisabledIconId:I

    const/16 v21, 0xd

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v9, v1}, Lcom/android/inputmethod/keyboard/internal/KeyStyles$KeyStyle;->getString(Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser;->getIconId(Ljava/lang/String;)I

    move-result v21

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/inputmethod/keyboard/Key;->mPreviewIconId:I

    const/16 v21, 0xa

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v9, v1}, Lcom/android/inputmethod/keyboard/internal/KeyStyles$KeyStyle;->getFlag(Landroid/content/res/TypedArray;I)I

    move-result v21

    invoke-virtual/range {p3 .. p3}, Lcom/android/inputmethod/keyboard/Keyboard$Builder$Row;->getDefaultKeyLabelFlags()I

    move-result v22

    or-int v21, v21, v22

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    move/from16 v21, v0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mId:Lcom/android/inputmethod/keyboard/KeyboardId;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lcom/android/inputmethod/keyboard/KeyboardId;->mElementId:I

    move/from16 v22, v0

    invoke-static/range {v21 .. v22}, Lcom/android/inputmethod/keyboard/Key;->needsToUpperCase(II)Z

    move-result v17

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mId:Lcom/android/inputmethod/keyboard/KeyboardId;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v14, v0, Lcom/android/inputmethod/keyboard/KeyboardId;->mLocale:Ljava/util/Locale;

    const/16 v21, 0x6

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v9, v1}, Lcom/android/inputmethod/keyboard/internal/KeyStyles$KeyStyle;->getFlag(Landroid/content/res/TypedArray;I)I

    move-result v4

    const/16 v21, 0x2

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v9, v1}, Lcom/android/inputmethod/keyboard/internal/KeyStyles$KeyStyle;->getStringArray(Landroid/content/res/TypedArray;I)[Ljava/lang/String;

    move-result-object v15

    const/16 v21, 0x4

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mMaxMoreKeysKeyboardColumn:I

    move/from16 v22, v0

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v9, v1, v2}, Lcom/android/inputmethod/keyboard/internal/KeyStyles$KeyStyle;->getInt(Landroid/content/res/TypedArray;II)I

    move-result v16

    const-string v21, "!autoColumnOrder!"

    const/16 v22, -0x1

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-static {v15, v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser;->getIntValue([Ljava/lang/String;Ljava/lang/String;I)I

    move-result v20

    if-lez v20, :cond_0

    move/from16 v0, v20

    and-int/lit16 v0, v0, 0xff

    move/from16 v16, v0

    :cond_0
    const-string v21, "!fixedColumnOrder!"

    const/16 v22, -0x1

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-static {v15, v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser;->getIntValue([Ljava/lang/String;Ljava/lang/String;I)I

    move-result v20

    if-lez v20, :cond_1

    const/high16 v21, -0x80000000

    move/from16 v0, v20

    and-int/lit16 v0, v0, 0xff

    move/from16 v22, v0

    or-int v16, v21, v22

    :cond_1
    const-string v21, "!hasLabels!"

    move-object/from16 v0, v21

    invoke-static {v15, v0}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser;->getBooleanValue([Ljava/lang/String;Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_2

    const/high16 v21, 0x40000000

    or-int v16, v16, v21

    :cond_2
    const-string v21, "!needsDividers!"

    move-object/from16 v0, v21

    invoke-static {v15, v0}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser;->getBooleanValue([Ljava/lang/String;Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_3

    const/high16 v21, 0x20000000

    or-int v16, v16, v21

    :cond_3
    const-string v21, "!embeddedMoreKey!"

    move-object/from16 v0, v21

    invoke-static {v15, v0}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser;->getBooleanValue([Ljava/lang/String;Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_4

    const/high16 v21, 0x10000000

    or-int v16, v16, v21

    :cond_4
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/inputmethod/keyboard/Key;->mMoreKeysColumnAndFlags:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    move/from16 v21, v0

    const/high16 v22, -0x80000000

    and-int v21, v21, v22

    if-eqz v21, :cond_6

    const/4 v5, 0x0

    :goto_1
    invoke-static {v15, v5}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser;->insertAdditionalMoreKeys([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    if-eqz v15, :cond_7

    or-int/lit8 v4, v4, 0x8

    array-length v0, v15

    move/from16 v21, v0

    move/from16 v0, v21

    new-array v0, v0, [Lcom/android/inputmethod/keyboard/internal/KeySpecParser$MoreKeySpec;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/inputmethod/keyboard/Key;->mMoreKeys:[Lcom/android/inputmethod/keyboard/internal/KeySpecParser$MoreKeySpec;

    const/4 v8, 0x0

    :goto_2
    array-length v0, v15

    move/from16 v21, v0

    move/from16 v0, v21

    if-ge v8, v0, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/Key;->mMoreKeys:[Lcom/android/inputmethod/keyboard/internal/KeySpecParser$MoreKeySpec;

    move-object/from16 v21, v0

    new-instance v22, Lcom/android/inputmethod/keyboard/internal/KeySpecParser$MoreKeySpec;

    aget-object v23, v15, v8

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mCodesSet:Lcom/android/inputmethod/keyboard/internal/KeyboardCodesSet;

    move-object/from16 v24, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move/from16 v2, v17

    move-object/from16 v3, v24

    invoke-direct {v0, v1, v2, v14, v3}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser$MoreKeySpec;-><init>(Ljava/lang/String;ZLjava/util/Locale;Lcom/android/inputmethod/keyboard/internal/KeyboardCodesSet;)V

    aput-object v22, v21, v8

    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    :cond_5
    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mHorizontalGap:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-float v7, v0

    goto/16 :goto_0

    :cond_6
    const/16 v21, 0x3

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v9, v1}, Lcom/android/inputmethod/keyboard/internal/KeyStyles$KeyStyle;->getStringArray(Landroid/content/res/TypedArray;I)[Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    :cond_7
    const/16 v21, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/inputmethod/keyboard/Key;->mMoreKeys:[Lcom/android/inputmethod/keyboard/internal/KeySpecParser$MoreKeySpec;

    :cond_8
    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/inputmethod/keyboard/Key;->mActionFlags:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    move/from16 v21, v0

    const/high16 v22, 0x20000

    and-int v21, v21, v22

    if-eqz v21, :cond_a

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mId:Lcom/android/inputmethod/keyboard/KeyboardId;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/KeyboardId;->mCustomActionLabel:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/inputmethod/keyboard/Key;->mLabel:Ljava/lang/String;

    :goto_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    move/from16 v21, v0

    const/high16 v22, 0x40000000

    and-int v21, v21, v22

    if-eqz v21, :cond_b

    const/16 v21, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/inputmethod/keyboard/Key;->mHintLabel:Ljava/lang/String;

    :goto_4
    const/16 v21, 0x7

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v9, v1}, Lcom/android/inputmethod/keyboard/internal/KeyStyles$KeyStyle;->getString(Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v17

    invoke-static {v0, v1, v14}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser;->toUpperCaseOfStringForLocale(Ljava/lang/String;ZLjava/util/Locale;)Ljava/lang/String;

    move-result-object v18

    const/16 v21, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v9, v1}, Lcom/android/inputmethod/keyboard/internal/KeyStyles$KeyStyle;->getString(Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mCodesSet:Lcom/android/inputmethod/keyboard/internal/KeyboardCodesSet;

    move-object/from16 v22, v0

    const/16 v23, -0xb

    invoke-static/range {v21 .. v23}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser;->parseCode(Ljava/lang/String;Lcom/android/inputmethod/keyboard/internal/KeyboardCodesSet;I)I

    move-result v6

    const/16 v21, -0xb

    move/from16 v0, v21

    if-ne v6, v0, :cond_e

    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v21

    if-eqz v21, :cond_e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/Key;->mLabel:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v21

    if-nez v21, :cond_e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/Key;->mLabel:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/android/inputmethod/latin/StringUtils;->codePointCount(Ljava/lang/String;)I

    move-result v21

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_d

    invoke-virtual/range {p0 .. p0}, Lcom/android/inputmethod/keyboard/Key;->hasShiftedLetterHint()Z

    move-result v21

    if-eqz v21, :cond_c

    invoke-virtual/range {p0 .. p0}, Lcom/android/inputmethod/keyboard/Key;->isShiftedLetterActivated()Z

    move-result v21

    if-eqz v21, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/Key;->mHintLabel:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v21

    if-nez v21, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/Key;->mHintLabel:Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->codePointAt(I)I

    move-result v21

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    :goto_5
    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/inputmethod/keyboard/Key;->mOutputText:Ljava/lang/CharSequence;

    const/16 v21, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v9, v1}, Lcom/android/inputmethod/keyboard/internal/KeyStyles$KeyStyle;->getString(Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mCodesSet:Lcom/android/inputmethod/keyboard/internal/KeyboardCodesSet;

    move-object/from16 v22, v0

    const/16 v23, -0xb

    invoke-static/range {v21 .. v23}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser;->parseCode(Ljava/lang/String;Lcom/android/inputmethod/keyboard/internal/KeyboardCodesSet;I)I

    move-result v21

    move/from16 v0, v21

    move/from16 v1, v17

    invoke-static {v0, v1, v14}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser;->toUpperCaseOfCodeForLocale(IZLjava/util/Locale;)I

    move-result v21

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/inputmethod/keyboard/Key;->mAltCode:I

    invoke-static/range {p0 .. p0}, Lcom/android/inputmethod/keyboard/Key;->computeHashCode(Lcom/android/inputmethod/keyboard/Key;)I

    move-result v21

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/inputmethod/keyboard/Key;->mHashCode:I

    invoke-virtual {v9}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual/range {p0 .. p0}, Lcom/android/inputmethod/keyboard/Key;->hasShiftedLetterHint()Z

    move-result v21

    if-eqz v21, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/Key;->mHintLabel:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v21

    if-eqz v21, :cond_9

    sget-object v21, Lcom/android/inputmethod/keyboard/Key;->TAG:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "hasShiftedLetterHint specified without keyHintLabel: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    return-void

    :cond_a
    const/16 v21, 0x8

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v9, v1}, Lcom/android/inputmethod/keyboard/internal/KeyStyles$KeyStyle;->getString(Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v17

    invoke-static {v0, v1, v14}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser;->toUpperCaseOfStringForLocale(Ljava/lang/String;ZLjava/util/Locale;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/inputmethod/keyboard/Key;->mLabel:Ljava/lang/String;

    goto/16 :goto_3

    :cond_b
    const/16 v21, 0x9

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v9, v1}, Lcom/android/inputmethod/keyboard/internal/KeyStyles$KeyStyle;->getString(Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v17

    invoke-static {v0, v1, v14}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser;->toUpperCaseOfStringForLocale(Ljava/lang/String;ZLjava/util/Locale;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/inputmethod/keyboard/Key;->mHintLabel:Ljava/lang/String;

    goto/16 :goto_4

    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/Key;->mLabel:Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->codePointAt(I)I

    move-result v21

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    goto/16 :goto_5

    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/Key;->mLabel:Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v21, -0x3

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    goto/16 :goto_5

    :cond_e
    const/16 v21, -0xb

    move/from16 v0, v21

    if-ne v6, v0, :cond_10

    if-eqz v18, :cond_10

    invoke-static/range {v18 .. v18}, Lcom/android/inputmethod/latin/StringUtils;->codePointCount(Ljava/lang/String;)I

    move-result v21

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_f

    const/16 v21, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->codePointAt(I)I

    move-result v21

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    const/16 v18, 0x0

    goto/16 :goto_5

    :cond_f
    const/16 v21, -0x3

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    goto/16 :goto_5

    :cond_10
    move/from16 v0, v17

    invoke-static {v6, v0, v14}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser;->toUpperCaseOfCodeForLocale(IZLjava/util/Locale;)I

    move-result v21

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    goto/16 :goto_5
.end method

.method public constructor <init>(Lcom/android/inputmethod/keyboard/Keyboard$Params;Lcom/android/inputmethod/keyboard/internal/KeySpecParser$MoreKeySpec;IIIII)V
    .locals 12
    .param p1    # Lcom/android/inputmethod/keyboard/Keyboard$Params;
    .param p2    # Lcom/android/inputmethod/keyboard/internal/KeySpecParser$MoreKeySpec;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I

    iget-object v2, p2, Lcom/android/inputmethod/keyboard/internal/KeySpecParser$MoreKeySpec;->mLabel:Ljava/lang/String;

    const/4 v3, 0x0

    iget v4, p2, Lcom/android/inputmethod/keyboard/internal/KeySpecParser$MoreKeySpec;->mIconId:I

    iget v5, p2, Lcom/android/inputmethod/keyboard/internal/KeySpecParser$MoreKeySpec;->mCode:I

    iget-object v6, p2, Lcom/android/inputmethod/keyboard/internal/KeySpecParser$MoreKeySpec;->mOutputText:Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move v7, p3

    move/from16 v8, p4

    move/from16 v9, p5

    move/from16 v10, p6

    move/from16 v11, p7

    invoke-direct/range {v0 .. v11}, Lcom/android/inputmethod/keyboard/Key;-><init>(Lcom/android/inputmethod/keyboard/Keyboard$Params;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;IIIII)V

    return-void
.end method

.method public constructor <init>(Lcom/android/inputmethod/keyboard/Keyboard$Params;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;IIIII)V
    .locals 4
    .param p1    # Lcom/android/inputmethod/keyboard/Keyboard$Params;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # I
    .param p6    # Ljava/lang/String;
    .param p7    # I
    .param p8    # I
    .param p9    # I
    .param p10    # I
    .param p11    # I

    const/4 v0, 0x1

    const/16 v3, -0xb

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/android/inputmethod/keyboard/Key;->mHitBox:Landroid/graphics/Rect;

    iput-boolean v0, p0, Lcom/android/inputmethod/keyboard/Key;->mEnabled:Z

    iget v2, p1, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mVerticalGap:I

    sub-int v2, p10, v2

    iput v2, p0, Lcom/android/inputmethod/keyboard/Key;->mHeight:I

    iget v2, p1, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mHorizontalGap:I

    iput v2, p0, Lcom/android/inputmethod/keyboard/Key;->mHorizontalGap:I

    iget v2, p1, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mVerticalGap:I

    iput v2, p0, Lcom/android/inputmethod/keyboard/Key;->mVerticalGap:I

    iput v1, p0, Lcom/android/inputmethod/keyboard/Key;->mVisualInsetsRight:I

    iput v1, p0, Lcom/android/inputmethod/keyboard/Key;->mVisualInsetsLeft:I

    iget v2, p0, Lcom/android/inputmethod/keyboard/Key;->mHorizontalGap:I

    sub-int v2, p9, v2

    iput v2, p0, Lcom/android/inputmethod/keyboard/Key;->mWidth:I

    iput-object p3, p0, Lcom/android/inputmethod/keyboard/Key;->mHintLabel:Ljava/lang/String;

    iput p11, p0, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    iput v1, p0, Lcom/android/inputmethod/keyboard/Key;->mBackgroundType:I

    iput v1, p0, Lcom/android/inputmethod/keyboard/Key;->mActionFlags:I

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/android/inputmethod/keyboard/Key;->mMoreKeys:[Lcom/android/inputmethod/keyboard/internal/KeySpecParser$MoreKeySpec;

    iput v1, p0, Lcom/android/inputmethod/keyboard/Key;->mMoreKeysColumnAndFlags:I

    iput-object p2, p0, Lcom/android/inputmethod/keyboard/Key;->mLabel:Ljava/lang/String;

    iput-object p6, p0, Lcom/android/inputmethod/keyboard/Key;->mOutputText:Ljava/lang/CharSequence;

    iput p5, p0, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    if-eq p5, v3, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/android/inputmethod/keyboard/Key;->mEnabled:Z

    iput v3, p0, Lcom/android/inputmethod/keyboard/Key;->mAltCode:I

    iput p4, p0, Lcom/android/inputmethod/keyboard/Key;->mIconId:I

    iput v1, p0, Lcom/android/inputmethod/keyboard/Key;->mDisabledIconId:I

    iput v1, p0, Lcom/android/inputmethod/keyboard/Key;->mPreviewIconId:I

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mHorizontalGap:I

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, p7

    iput v0, p0, Lcom/android/inputmethod/keyboard/Key;->mX:I

    iput p8, p0, Lcom/android/inputmethod/keyboard/Key;->mY:I

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/Key;->mHitBox:Landroid/graphics/Rect;

    add-int v1, p7, p9

    add-int/lit8 v1, v1, 0x1

    add-int v2, p8, p10

    invoke-virtual {v0, p7, p8, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    invoke-static {p0}, Lcom/android/inputmethod/keyboard/Key;->computeHashCode(Lcom/android/inputmethod/keyboard/Key;)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/Key;->mHashCode:I

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private static backgroundName(I)Ljava/lang/String;
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "normal"

    goto :goto_0

    :pswitch_1
    const-string v0, "functional"

    goto :goto_0

    :pswitch_2
    const-string v0, "action"

    goto :goto_0

    :pswitch_3
    const-string v0, "stickyOff"

    goto :goto_0

    :pswitch_4
    const-string v0, "stickyOn"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private static computeHashCode(Lcom/android/inputmethod/keyboard/Key;)I
    .locals 3
    .param p0    # Lcom/android/inputmethod/keyboard/Key;

    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/android/inputmethod/keyboard/Key;->mX:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/android/inputmethod/keyboard/Key;->mY:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/android/inputmethod/keyboard/Key;->mWidth:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lcom/android/inputmethod/keyboard/Key;->mHeight:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget v2, p0, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/Key;->mLabel:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/Key;->mHintLabel:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget v2, p0, Lcom/android/inputmethod/keyboard/Key;->mIconId:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget v2, p0, Lcom/android/inputmethod/keyboard/Key;->mBackgroundType:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/Key;->mMoreKeys:[Lcom/android/inputmethod/keyboard/internal/KeySpecParser$MoreKeySpec;

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/Key;->mOutputText:Ljava/lang/CharSequence;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget v2, p0, Lcom/android/inputmethod/keyboard/Key;->mActionFlags:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget v2, p0, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private equals(Lcom/android/inputmethod/keyboard/Key;)Z
    .locals 3
    .param p1    # Lcom/android/inputmethod/keyboard/Key;

    const/4 v0, 0x1

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p1, Lcom/android/inputmethod/keyboard/Key;->mX:I

    iget v2, p0, Lcom/android/inputmethod/keyboard/Key;->mX:I

    if-ne v1, v2, :cond_2

    iget v1, p1, Lcom/android/inputmethod/keyboard/Key;->mY:I

    iget v2, p0, Lcom/android/inputmethod/keyboard/Key;->mY:I

    if-ne v1, v2, :cond_2

    iget v1, p1, Lcom/android/inputmethod/keyboard/Key;->mWidth:I

    iget v2, p0, Lcom/android/inputmethod/keyboard/Key;->mWidth:I

    if-ne v1, v2, :cond_2

    iget v1, p1, Lcom/android/inputmethod/keyboard/Key;->mHeight:I

    iget v2, p0, Lcom/android/inputmethod/keyboard/Key;->mHeight:I

    if-ne v1, v2, :cond_2

    iget v1, p1, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    iget v2, p0, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    if-ne v1, v2, :cond_2

    iget-object v1, p1, Lcom/android/inputmethod/keyboard/Key;->mLabel:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/Key;->mLabel:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p1, Lcom/android/inputmethod/keyboard/Key;->mHintLabel:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/Key;->mHintLabel:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget v1, p1, Lcom/android/inputmethod/keyboard/Key;->mIconId:I

    iget v2, p0, Lcom/android/inputmethod/keyboard/Key;->mIconId:I

    if-ne v1, v2, :cond_2

    iget v1, p1, Lcom/android/inputmethod/keyboard/Key;->mBackgroundType:I

    iget v2, p0, Lcom/android/inputmethod/keyboard/Key;->mBackgroundType:I

    if-ne v1, v2, :cond_2

    iget-object v1, p1, Lcom/android/inputmethod/keyboard/Key;->mMoreKeys:[Lcom/android/inputmethod/keyboard/internal/KeySpecParser$MoreKeySpec;

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/Key;->mMoreKeys:[Lcom/android/inputmethod/keyboard/internal/KeySpecParser$MoreKeySpec;

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p1, Lcom/android/inputmethod/keyboard/Key;->mOutputText:Ljava/lang/CharSequence;

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/Key;->mOutputText:Ljava/lang/CharSequence;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget v1, p1, Lcom/android/inputmethod/keyboard/Key;->mActionFlags:I

    iget v2, p0, Lcom/android/inputmethod/keyboard/Key;->mActionFlags:I

    if-ne v1, v2, :cond_2

    iget v1, p1, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    iget v2, p0, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    if-eq v1, v2, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static needsToUpperCase(II)Z
    .locals 2
    .param p0    # I
    .param p1    # I

    const/4 v0, 0x0

    const v1, 0x8000

    and-int/2addr v1, p0

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public altCodeWhileTyping()Z
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mActionFlags:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    instance-of v0, p1, Lcom/android/inputmethod/keyboard/Key;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/android/inputmethod/keyboard/Key;

    invoke-direct {p0, p1}, Lcom/android/inputmethod/keyboard/Key;->equals(Lcom/android/inputmethod/keyboard/Key;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCurrentDrawableState()[I
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mBackgroundType:I

    packed-switch v0, :pswitch_data_0

    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/Key;->mPressed:Z

    if-eqz v0, :cond_4

    sget-object v0, Lcom/android/inputmethod/keyboard/Key;->KEY_STATE_PRESSED:[I

    :goto_0
    return-object v0

    :pswitch_0
    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/Key;->mPressed:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/inputmethod/keyboard/Key;->KEY_STATE_FUNCTIONAL_PRESSED:[I

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/android/inputmethod/keyboard/Key;->KEY_STATE_FUNCTIONAL_NORMAL:[I

    goto :goto_0

    :pswitch_1
    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/Key;->mPressed:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/android/inputmethod/keyboard/Key;->KEY_STATE_ACTIVE_PRESSED:[I

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/android/inputmethod/keyboard/Key;->KEY_STATE_ACTIVE_NORMAL:[I

    goto :goto_0

    :pswitch_2
    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/Key;->mPressed:Z

    if-eqz v0, :cond_2

    sget-object v0, Lcom/android/inputmethod/keyboard/Key;->KEY_STATE_PRESSED_HIGHLIGHT_OFF:[I

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/android/inputmethod/keyboard/Key;->KEY_STATE_NORMAL_HIGHLIGHT_OFF:[I

    goto :goto_0

    :pswitch_3
    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/Key;->mPressed:Z

    if-eqz v0, :cond_3

    sget-object v0, Lcom/android/inputmethod/keyboard/Key;->KEY_STATE_PRESSED_HIGHLIGHT_ON:[I

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/android/inputmethod/keyboard/Key;->KEY_STATE_NORMAL_HIGHLIGHT_ON:[I

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/android/inputmethod/keyboard/Key;->KEY_STATE_NORMAL:[I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getIcon(Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;I)Landroid/graphics/drawable/Drawable;
    .locals 3
    .param p1    # Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;
    .param p2    # I

    iget-boolean v2, p0, Lcom/android/inputmethod/keyboard/Key;->mEnabled:Z

    if-eqz v2, :cond_1

    iget v1, p0, Lcom/android/inputmethod/keyboard/Key;->mIconId:I

    :goto_0
    invoke-virtual {p1, v1}, Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;->getIconDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    :cond_0
    return-object v0

    :cond_1
    iget v1, p0, Lcom/android/inputmethod/keyboard/Key;->mDisabledIconId:I

    goto :goto_0
.end method

.method public getMoreKeyLabelFlags()I
    .locals 1

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/Key;->hasLabelsInMoreKeys()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xc0

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x80

    goto :goto_0
.end method

.method public getMoreKeysColumn()I
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mMoreKeysColumnAndFlags:I

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method public getPreviewIcon(Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p1    # Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mPreviewIconId:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mPreviewIconId:I

    invoke-virtual {p1, v0}, Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;->getIconDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mIconId:I

    invoke-virtual {p1, v0}, Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;->getIconDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method public hasEmbeddedMoreKey()Z
    .locals 2

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mMoreKeysColumnAndFlags:I

    const/high16 v1, 0x10000000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasHintLabel()Z
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    and-int/lit16 v0, v0, 0x800

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLabelWithIconLeft()Z
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    and-int/lit16 v0, v0, 0x1000

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLabelWithIconRight()Z
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    and-int/lit16 v0, v0, 0x2000

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLabelsInMoreKeys()Z
    .locals 2

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mMoreKeysColumnAndFlags:I

    const/high16 v1, 0x40000000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPopupHint()Z
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasShiftedLetterHint()Z
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mHashCode:I

    return v0
.end method

.method public isAlignLeft()Z
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isAlignLeftOfCenter()Z
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isAlignRight()Z
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/Key;->mEnabled:Z

    return v0
.end method

.method public isFixedColumnOrderMoreKeys()Z
    .locals 2

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mMoreKeysColumnAndFlags:I

    const/high16 v1, -0x80000000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLongPressEnabled()Z
    .locals 2

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mActionFlags:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    const/high16 v1, 0x10000

    and-int/2addr v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isModifier()Z
    .locals 2

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    const/4 v1, -0x2

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isOnKey(II)Z
    .locals 1
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/Key;->mHitBox:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    return v0
.end method

.method public isRepeatable()Z
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mActionFlags:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isShift()Z
    .locals 2

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isShiftedLetterActivated()Z
    .locals 2

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    const/high16 v1, 0x10000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isSpacer()Z
    .locals 1

    instance-of v0, p0, Lcom/android/inputmethod/keyboard/Key$Spacer;

    return v0
.end method

.method public markAsBottomEdge(Lcom/android/inputmethod/keyboard/Keyboard$Params;)V
    .locals 3
    .param p1    # Lcom/android/inputmethod/keyboard/Keyboard$Params;

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/Key;->mHitBox:Landroid/graphics/Rect;

    iget v1, p1, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mOccupiedHeight:I

    iget v2, p1, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mBottomPadding:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    return-void
.end method

.method public markAsLeftEdge(Lcom/android/inputmethod/keyboard/Keyboard$Params;)V
    .locals 2
    .param p1    # Lcom/android/inputmethod/keyboard/Keyboard$Params;

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/Key;->mHitBox:Landroid/graphics/Rect;

    iget v1, p1, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mHorizontalEdgesPadding:I

    iput v1, v0, Landroid/graphics/Rect;->left:I

    return-void
.end method

.method public markAsRightEdge(Lcom/android/inputmethod/keyboard/Keyboard$Params;)V
    .locals 3
    .param p1    # Lcom/android/inputmethod/keyboard/Keyboard$Params;

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/Key;->mHitBox:Landroid/graphics/Rect;

    iget v1, p1, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mOccupiedWidth:I

    iget v2, p1, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mHorizontalEdgesPadding:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    return-void
.end method

.method public markAsTopEdge(Lcom/android/inputmethod/keyboard/Keyboard$Params;)V
    .locals 2
    .param p1    # Lcom/android/inputmethod/keyboard/Keyboard$Params;

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/Key;->mHitBox:Landroid/graphics/Rect;

    iget v1, p1, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mTopPadding:I

    iput v1, v0, Landroid/graphics/Rect;->top:I

    return-void
.end method

.method public needsDividersInMoreKeys()Z
    .locals 2

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mMoreKeysColumnAndFlags:I

    const/high16 v1, 0x20000000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public needsXScale()Z
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    and-int/lit16 v0, v0, 0x4000

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public noKeyPreview()Z
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mActionFlags:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPressed()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/inputmethod/keyboard/Key;->mPressed:Z

    return-void
.end method

.method public onReleased()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/inputmethod/keyboard/Key;->mPressed:Z

    return-void
.end method

.method public selectTextSize(IIIII)I
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    and-int/lit16 v0, v0, 0x1c0

    sparse-switch v0, :sswitch_data_0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/Key;->mLabel:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/inputmethod/latin/StringUtils;->codePointCount(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    :goto_0
    :sswitch_0
    return p1

    :sswitch_1
    move p1, p2

    goto :goto_0

    :sswitch_2
    move p1, p3

    goto :goto_0

    :sswitch_3
    move p1, p4

    goto :goto_0

    :sswitch_4
    move p1, p5

    goto :goto_0

    :cond_0
    move p1, p3

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x40 -> :sswitch_1
        0x80 -> :sswitch_0
        0xc0 -> :sswitch_2
        0x100 -> :sswitch_3
        0x140 -> :sswitch_4
    .end sparse-switch
.end method

.method public selectTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;
    .locals 1
    .param p1    # Landroid/graphics/Typeface;

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_1

    sget-object p1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    :cond_0
    :goto_0
    return-object p1

    :cond_1
    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    sget-object p1, Landroid/graphics/Typeface;->MONOSPACE:Landroid/graphics/Typeface;

    goto :goto_0
.end method

.method public setEnabled(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/inputmethod/keyboard/Key;->mEnabled:Z

    return-void
.end method

.method public squaredDistanceToEdge(II)I
    .locals 10
    .param p1    # I
    .param p2    # I

    iget v5, p0, Lcom/android/inputmethod/keyboard/Key;->mX:I

    iget v8, p0, Lcom/android/inputmethod/keyboard/Key;->mWidth:I

    add-int v6, v5, v8

    iget v7, p0, Lcom/android/inputmethod/keyboard/Key;->mY:I

    iget v8, p0, Lcom/android/inputmethod/keyboard/Key;->mHeight:I

    add-int v0, v7, v8

    if-ge p1, v5, :cond_0

    move v3, v5

    :goto_0
    if-ge p2, v7, :cond_2

    move v4, v7

    :goto_1
    sub-int v1, p1, v3

    sub-int v2, p2, v4

    mul-int v8, v1, v1

    mul-int v9, v2, v2

    add-int/2addr v8, v9

    return v8

    :cond_0
    if-le p1, v6, :cond_1

    move v3, v6

    goto :goto_0

    :cond_1
    move v3, p1

    goto :goto_0

    :cond_2
    if-le p2, v0, :cond_3

    move v4, v0

    goto :goto_1

    :cond_3
    move v4, p2

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const-string v0, "%s/%s %d,%d %dx%d %s/%s/%s"

    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    invoke-static {v3}, Lcom/android/inputmethod/keyboard/Keyboard;->printableCode(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/Key;->mLabel:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Lcom/android/inputmethod/keyboard/Key;->mX:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget v3, p0, Lcom/android/inputmethod/keyboard/Key;->mY:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget v3, p0, Lcom/android/inputmethod/keyboard/Key;->mWidth:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget v3, p0, Lcom/android/inputmethod/keyboard/Key;->mHeight:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/Key;->mHintLabel:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    iget v3, p0, Lcom/android/inputmethod/keyboard/Key;->mIconId:I

    invoke-static {v3}, Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;->getIconName(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x8

    iget v3, p0, Lcom/android/inputmethod/keyboard/Key;->mBackgroundType:I

    invoke-static {v3}, Lcom/android/inputmethod/keyboard/Key;->backgroundName(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
