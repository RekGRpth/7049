.class public Lcom/android/inputmethod/keyboard/KeyDetector;
.super Ljava/lang/Object;
.source "KeyDetector.java"


# instance fields
.field private mCorrectionX:I

.field private mCorrectionY:I

.field private final mKeyHysteresisDistanceSquared:I

.field private mKeyboard:Lcom/android/inputmethod/keyboard/Keyboard;

.field private mProximityCorrectOn:Z


# direct methods
.method public constructor <init>(F)V
    .locals 1
    .param p1    # F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    mul-float v0, p1, p1

    float-to-int v0, v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/KeyDetector;->mKeyHysteresisDistanceSquared:I

    return-void
.end method


# virtual methods
.method public alwaysAllowsSlidingInput()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public detectHitKey(II)Lcom/android/inputmethod/keyboard/Key;
    .locals 12
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0, p1}, Lcom/android/inputmethod/keyboard/KeyDetector;->getTouchX(I)I

    move-result v8

    invoke-virtual {p0, p2}, Lcom/android/inputmethod/keyboard/KeyDetector;->getTouchY(I)I

    move-result v9

    const v6, 0x7fffffff

    const/4 v7, 0x0

    iget-object v10, p0, Lcom/android/inputmethod/keyboard/KeyDetector;->mKeyboard:Lcom/android/inputmethod/keyboard/Keyboard;

    invoke-virtual {v10, v8, v9}, Lcom/android/inputmethod/keyboard/Keyboard;->getNearestKeys(II)[Lcom/android/inputmethod/keyboard/Key;

    move-result-object v0

    array-length v5, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v5, :cond_2

    aget-object v4, v0, v2

    invoke-virtual {v4, v8, v9}, Lcom/android/inputmethod/keyboard/Key;->isOnKey(II)Z

    move-result v3

    invoke-virtual {v4, v8, v9}, Lcom/android/inputmethod/keyboard/Key;->squaredDistanceToEdge(II)I

    move-result v1

    if-eqz v7, :cond_0

    if-lt v1, v6, :cond_0

    if-ne v1, v6, :cond_1

    if-eqz v3, :cond_1

    iget v10, v4, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    iget v11, v7, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    if-le v10, v11, :cond_1

    :cond_0
    move v6, v1

    move-object v7, v4

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    return-object v7
.end method

.method public getKeyHysteresisDistanceSquared()I
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/KeyDetector;->mKeyHysteresisDistanceSquared:I

    return v0
.end method

.method public getKeyboard()Lcom/android/inputmethod/keyboard/Keyboard;
    .locals 2

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyDetector;->mKeyboard:Lcom/android/inputmethod/keyboard/Keyboard;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "keyboard isn\'t set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyDetector;->mKeyboard:Lcom/android/inputmethod/keyboard/Keyboard;

    return-object v0
.end method

.method public getTouchX(I)I
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/android/inputmethod/keyboard/KeyDetector;->mCorrectionX:I

    add-int/2addr v0, p1

    return v0
.end method

.method public getTouchY(I)I
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/android/inputmethod/keyboard/KeyDetector;->mCorrectionY:I

    add-int/2addr v0, p1

    return v0
.end method

.method public setKeyboard(Lcom/android/inputmethod/keyboard/Keyboard;FF)V
    .locals 1
    .param p1    # Lcom/android/inputmethod/keyboard/Keyboard;
    .param p2    # F
    .param p3    # F

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    float-to-int v0, p2

    iput v0, p0, Lcom/android/inputmethod/keyboard/KeyDetector;->mCorrectionX:I

    float-to-int v0, p3

    iput v0, p0, Lcom/android/inputmethod/keyboard/KeyDetector;->mCorrectionY:I

    iput-object p1, p0, Lcom/android/inputmethod/keyboard/KeyDetector;->mKeyboard:Lcom/android/inputmethod/keyboard/Keyboard;

    return-void
.end method

.method public setProximityCorrectionEnabled(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/inputmethod/keyboard/KeyDetector;->mProximityCorrectOn:Z

    return-void
.end method
