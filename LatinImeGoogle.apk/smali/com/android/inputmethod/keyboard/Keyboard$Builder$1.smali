.class Lcom/android/inputmethod/keyboard/Keyboard$Builder$1;
.super Lcom/android/inputmethod/latin/LocaleUtils$RunInLocale;
.source "Keyboard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/inputmethod/keyboard/Keyboard$Builder;->parseKeyboardAttributes(Lorg/xmlpull/v1/XmlPullParser;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/inputmethod/latin/LocaleUtils$RunInLocale",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/inputmethod/keyboard/Keyboard$Builder;

.field final synthetic val$params:Lcom/android/inputmethod/keyboard/Keyboard$Params;


# direct methods
.method constructor <init>(Lcom/android/inputmethod/keyboard/Keyboard$Builder;Lcom/android/inputmethod/keyboard/Keyboard$Params;)V
    .locals 0

    iput-object p1, p0, Lcom/android/inputmethod/keyboard/Keyboard$Builder$1;->this$0:Lcom/android/inputmethod/keyboard/Keyboard$Builder;

    iput-object p2, p0, Lcom/android/inputmethod/keyboard/Keyboard$Builder$1;->val$params:Lcom/android/inputmethod/keyboard/Keyboard$Params;

    invoke-direct {p0}, Lcom/android/inputmethod/latin/LocaleUtils$RunInLocale;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic job(Landroid/content/res/Resources;)Ljava/lang/Object;
    .locals 1
    .param p1    # Landroid/content/res/Resources;

    invoke-virtual {p0, p1}, Lcom/android/inputmethod/keyboard/Keyboard$Builder$1;->job(Landroid/content/res/Resources;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected job(Landroid/content/res/Resources;)Ljava/lang/Void;
    .locals 2
    .param p1    # Landroid/content/res/Resources;

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/Keyboard$Builder$1;->val$params:Lcom/android/inputmethod/keyboard/Keyboard$Params;

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mTextsSet:Lcom/android/inputmethod/keyboard/internal/KeyboardTextsSet;

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/Keyboard$Builder$1;->this$0:Lcom/android/inputmethod/keyboard/Keyboard$Builder;

    iget-object v1, v1, Lcom/android/inputmethod/keyboard/Keyboard$Builder;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyboardTextsSet;->loadStringResources(Landroid/content/Context;)V

    const/4 v0, 0x0

    return-object v0
.end method
