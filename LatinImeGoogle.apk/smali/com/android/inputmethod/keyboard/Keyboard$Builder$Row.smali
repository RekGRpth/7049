.class public Lcom/android/inputmethod/keyboard/Keyboard$Builder$Row;
.super Ljava/lang/Object;
.source "Keyboard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/keyboard/Keyboard$Builder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Row"
.end annotation


# instance fields
.field private mCurrentX:F

.field private final mCurrentY:I

.field private mDefaultBackgroundType:I

.field private mDefaultKeyLabelFlags:I

.field private mDefaultKeyWidth:F

.field private final mParams:Lcom/android/inputmethod/keyboard/Keyboard$Params;

.field public final mRowHeight:I


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lcom/android/inputmethod/keyboard/Keyboard$Params;Lorg/xmlpull/v1/XmlPullParser;I)V
    .locals 6
    .param p1    # Landroid/content/res/Resources;
    .param p2    # Lcom/android/inputmethod/keyboard/Keyboard$Params;
    .param p3    # Lorg/xmlpull/v1/XmlPullParser;
    .param p4    # I

    const/4 v5, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/android/inputmethod/keyboard/Keyboard$Builder$Row;->mParams:Lcom/android/inputmethod/keyboard/Keyboard$Params;

    invoke-static {p3}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v2

    sget-object v3, Lcom/android/inputmethod/latin/R$styleable;->Keyboard:[I

    invoke-virtual {p1, v2, v3}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    const/16 v2, 0x8

    iget v3, p2, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mBaseHeight:I

    iget v4, p2, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mDefaultRowHeight:I

    int-to-float v4, v4

    invoke-static {v1, v2, v3, v4}, Lcom/android/inputmethod/keyboard/Keyboard$Builder;->getDimensionOrFraction(Landroid/content/res/TypedArray;IIF)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/android/inputmethod/keyboard/Keyboard$Builder$Row;->mRowHeight:I

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    invoke-static {p3}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v2

    sget-object v3, Lcom/android/inputmethod/latin/R$styleable;->Keyboard_Key:[I

    invoke-virtual {p1, v2, v3}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    const/16 v2, 0x11

    iget v3, p2, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mBaseWidth:I

    iget v4, p2, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mDefaultKeyWidth:I

    int-to-float v4, v4

    invoke-static {v0, v2, v3, v4}, Lcom/android/inputmethod/keyboard/Keyboard$Builder;->getDimensionOrFraction(Landroid/content/res/TypedArray;IIF)F

    move-result v2

    iput v2, p0, Lcom/android/inputmethod/keyboard/Keyboard$Builder$Row;->mDefaultKeyWidth:F

    const/4 v2, 0x5

    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, p0, Lcom/android/inputmethod/keyboard/Keyboard$Builder$Row;->mDefaultBackgroundType:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    iput v5, p0, Lcom/android/inputmethod/keyboard/Keyboard$Builder$Row;->mDefaultKeyLabelFlags:I

    iput p4, p0, Lcom/android/inputmethod/keyboard/Keyboard$Builder$Row;->mCurrentY:I

    const/4 v2, 0x0

    iput v2, p0, Lcom/android/inputmethod/keyboard/Keyboard$Builder$Row;->mCurrentX:F

    return-void
.end method


# virtual methods
.method public advanceXPos(F)V
    .locals 1
    .param p1    # F

    iget v0, p0, Lcom/android/inputmethod/keyboard/Keyboard$Builder$Row;->mCurrentX:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/android/inputmethod/keyboard/Keyboard$Builder$Row;->mCurrentX:F

    return-void
.end method

.method public getDefaultBackgroundType()I
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/Keyboard$Builder$Row;->mDefaultBackgroundType:I

    return v0
.end method

.method public getDefaultKeyLabelFlags()I
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/Keyboard$Builder$Row;->mDefaultKeyLabelFlags:I

    return v0
.end method

.method public getDefaultKeyWidth()F
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/Keyboard$Builder$Row;->mDefaultKeyWidth:F

    return v0
.end method

.method public getKeyWidth(Landroid/content/res/TypedArray;)F
    .locals 1
    .param p1    # Landroid/content/res/TypedArray;

    iget v0, p0, Lcom/android/inputmethod/keyboard/Keyboard$Builder$Row;->mCurrentX:F

    invoke-virtual {p0, p1, v0}, Lcom/android/inputmethod/keyboard/Keyboard$Builder$Row;->getKeyWidth(Landroid/content/res/TypedArray;F)F

    move-result v0

    return v0
.end method

.method public getKeyWidth(Landroid/content/res/TypedArray;F)F
    .locals 5
    .param p1    # Landroid/content/res/TypedArray;
    .param p2    # F

    const/16 v4, 0x11

    const/4 v2, 0x0

    invoke-static {p1, v4, v2}, Lcom/android/inputmethod/keyboard/Keyboard$Builder;->getEnumValue(Landroid/content/res/TypedArray;II)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/Keyboard$Builder$Row;->mParams:Lcom/android/inputmethod/keyboard/Keyboard$Params;

    iget v2, v2, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mBaseWidth:I

    iget v3, p0, Lcom/android/inputmethod/keyboard/Keyboard$Builder$Row;->mDefaultKeyWidth:F

    invoke-static {p1, v4, v2, v3}, Lcom/android/inputmethod/keyboard/Keyboard$Builder;->getDimensionOrFraction(Landroid/content/res/TypedArray;IIF)F

    move-result v2

    :goto_0
    return v2

    :pswitch_0
    iget-object v2, p0, Lcom/android/inputmethod/keyboard/Keyboard$Builder$Row;->mParams:Lcom/android/inputmethod/keyboard/Keyboard$Params;

    iget v2, v2, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mOccupiedWidth:I

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/Keyboard$Builder$Row;->mParams:Lcom/android/inputmethod/keyboard/Keyboard$Params;

    iget v3, v3, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mHorizontalEdgesPadding:I

    sub-int v0, v2, v3

    int-to-float v2, v0

    sub-float/2addr v2, p2

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch
.end method

.method public getKeyX(Landroid/content/res/TypedArray;)F
    .locals 7
    .param p1    # Landroid/content/res/TypedArray;

    const/16 v6, 0x12

    const/4 v5, 0x0

    const/16 v3, 0x11

    const/4 v4, 0x0

    invoke-static {p1, v3, v4}, Lcom/android/inputmethod/keyboard/Keyboard$Builder;->getEnumValue(Landroid/content/res/TypedArray;II)I

    move-result v2

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/Keyboard$Builder$Row;->mParams:Lcom/android/inputmethod/keyboard/Keyboard$Params;

    iget v3, v3, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mOccupiedWidth:I

    iget-object v4, p0, Lcom/android/inputmethod/keyboard/Keyboard$Builder$Row;->mParams:Lcom/android/inputmethod/keyboard/Keyboard$Params;

    iget v4, v4, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mHorizontalEdgesPadding:I

    sub-int v1, v3, v4

    invoke-virtual {p1, v6}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/Keyboard$Builder$Row;->mParams:Lcom/android/inputmethod/keyboard/Keyboard$Params;

    iget v3, v3, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mBaseWidth:I

    invoke-static {p1, v6, v3, v5}, Lcom/android/inputmethod/keyboard/Keyboard$Builder;->getDimensionOrFraction(Landroid/content/res/TypedArray;IIF)F

    move-result v0

    cmpg-float v3, v0, v5

    if-gez v3, :cond_0

    int-to-float v3, v1

    add-float/2addr v3, v0

    iget v4, p0, Lcom/android/inputmethod/keyboard/Keyboard$Builder$Row;->mCurrentX:F

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v3

    :goto_0
    return v3

    :cond_0
    iget-object v3, p0, Lcom/android/inputmethod/keyboard/Keyboard$Builder$Row;->mParams:Lcom/android/inputmethod/keyboard/Keyboard$Params;

    iget v3, v3, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mHorizontalEdgesPadding:I

    int-to-float v3, v3

    add-float/2addr v3, v0

    goto :goto_0

    :cond_1
    iget v3, p0, Lcom/android/inputmethod/keyboard/Keyboard$Builder$Row;->mCurrentX:F

    goto :goto_0
.end method

.method public getKeyY()I
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/Keyboard$Builder$Row;->mCurrentY:I

    return v0
.end method

.method public setDefaultBackgroundType(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/inputmethod/keyboard/Keyboard$Builder$Row;->mDefaultBackgroundType:I

    return-void
.end method

.method public setDefaultKeyLabelFlags(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/inputmethod/keyboard/Keyboard$Builder$Row;->mDefaultKeyLabelFlags:I

    return-void
.end method

.method public setDefaultKeyWidth(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/android/inputmethod/keyboard/Keyboard$Builder$Row;->mDefaultKeyWidth:F

    return-void
.end method

.method public setXPos(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/android/inputmethod/keyboard/Keyboard$Builder$Row;->mCurrentX:F

    return-void
.end method
