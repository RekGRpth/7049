.class public Lcom/android/inputmethod/keyboard/Keyboard$Params$TouchPositionCorrection;
.super Ljava/lang/Object;
.source "Keyboard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/keyboard/Keyboard$Params;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TouchPositionCorrection"
.end annotation


# instance fields
.field public mEnabled:Z

.field public mRadii:[F

.field public mXs:[F

.field public mYs:[F


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isValid()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params$TouchPositionCorrection;->mEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params$TouchPositionCorrection;->mXs:[F

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params$TouchPositionCorrection;->mYs:[F

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params$TouchPositionCorrection;->mRadii:[F

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params$TouchPositionCorrection;->mXs:[F

    array-length v0, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params$TouchPositionCorrection;->mYs:[F

    array-length v0, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params$TouchPositionCorrection;->mRadii:[F

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load([Ljava/lang/String;)V
    .locals 9
    .param p1    # [Ljava/lang/String;

    const/4 v8, 0x0

    array-length v0, p1

    rem-int/lit8 v7, v0, 0x3

    if-eqz v7, :cond_0

    sget-boolean v7, Lcom/android/inputmethod/latin/LatinImeLogger;->sDBG:Z

    if-eqz v7, :cond_4

    new-instance v7, Ljava/lang/RuntimeException;

    const-string v8, "the size of touch position correction data is invalid"

    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_0
    div-int/lit8 v4, v0, 0x3

    new-array v7, v4, [F

    iput-object v7, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params$TouchPositionCorrection;->mXs:[F

    new-array v7, v4, [F

    iput-object v7, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params$TouchPositionCorrection;->mYs:[F

    new-array v7, v4, [F

    iput-object v7, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params$TouchPositionCorrection;->mRadii:[F

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_4

    :try_start_0
    rem-int/lit8 v5, v2, 0x3

    div-int/lit8 v3, v2, 0x3

    aget-object v7, p1, v2

    invoke-static {v7}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v6

    if-nez v5, :cond_1

    iget-object v7, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params$TouchPositionCorrection;->mXs:[F

    aput v6, v7, v3

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v7, 0x1

    if-ne v5, v7, :cond_2

    iget-object v7, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params$TouchPositionCorrection;->mYs:[F

    aput v6, v7, v3
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    sget-boolean v7, Lcom/android/inputmethod/latin/LatinImeLogger;->sDBG:Z

    if-eqz v7, :cond_3

    new-instance v7, Ljava/lang/RuntimeException;

    const-string v8, "the number format for touch position correction data is invalid"

    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_2
    :try_start_1
    iget-object v7, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params$TouchPositionCorrection;->mRadii:[F

    aput v6, v7, v3
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :cond_3
    iput-object v8, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params$TouchPositionCorrection;->mXs:[F

    iput-object v8, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params$TouchPositionCorrection;->mYs:[F

    iput-object v8, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params$TouchPositionCorrection;->mRadii:[F

    :cond_4
    return-void
.end method

.method public setEnabled(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params$TouchPositionCorrection;->mEnabled:Z

    return-void
.end method
