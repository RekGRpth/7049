.class public Lcom/android/inputmethod/keyboard/Keyboard$Params;
.super Ljava/lang/Object;
.source "Keyboard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/keyboard/Keyboard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Params"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/inputmethod/keyboard/Keyboard$Params$TouchPositionCorrection;
    }
.end annotation


# instance fields
.field public GRID_HEIGHT:I

.field public GRID_WIDTH:I

.field public final mAltCodeKeysWhileTyping:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/keyboard/Key;",
            ">;"
        }
    .end annotation
.end field

.field public mBaseHeight:I

.field public mBaseWidth:I

.field public mBottomPadding:I

.field public final mCodesSet:Lcom/android/inputmethod/keyboard/internal/KeyboardCodesSet;

.field public mDefaultKeyWidth:I

.field public mDefaultRowHeight:I

.field private final mHeightHistogram:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public mHorizontalCenterPadding:I

.field public mHorizontalEdgesPadding:I

.field public mHorizontalGap:I

.field public final mIconsSet:Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;

.field public mId:Lcom/android/inputmethod/keyboard/KeyboardId;

.field public final mKeyStyles:Lcom/android/inputmethod/keyboard/internal/KeyStyles;

.field public final mKeys:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/android/inputmethod/keyboard/Key;",
            ">;"
        }
    .end annotation
.end field

.field public mKeysCache:Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$KeysCache;

.field private mMaxHeightCount:I

.field public mMaxMoreKeysKeyboardColumn:I

.field private mMaxWidthCount:I

.field public mMoreKeysTemplate:I

.field public mMostCommonKeyHeight:I

.field public mMostCommonKeyWidth:I

.field public mOccupiedHeight:I

.field public mOccupiedWidth:I

.field public mProximityCharsCorrectionEnabled:Z

.field public final mShiftKeys:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/keyboard/Key;",
            ">;"
        }
    .end annotation
.end field

.field public final mTextsSet:Lcom/android/inputmethod/keyboard/internal/KeyboardTextsSet;

.field public mThemeId:I

.field public mTopPadding:I

.field public final mTouchPositionCorrection:Lcom/android/inputmethod/keyboard/Keyboard$Params$TouchPositionCorrection;

.field public mVerticalGap:I

.field private final mWidthHistogram:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mKeys:Ljava/util/HashSet;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mShiftKeys:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mAltCodeKeysWhileTyping:Ljava/util/ArrayList;

    new-instance v0, Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;

    invoke-direct {v0}, Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mIconsSet:Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;

    new-instance v0, Lcom/android/inputmethod/keyboard/internal/KeyboardCodesSet;

    invoke-direct {v0}, Lcom/android/inputmethod/keyboard/internal/KeyboardCodesSet;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mCodesSet:Lcom/android/inputmethod/keyboard/internal/KeyboardCodesSet;

    new-instance v0, Lcom/android/inputmethod/keyboard/internal/KeyboardTextsSet;

    invoke-direct {v0}, Lcom/android/inputmethod/keyboard/internal/KeyboardTextsSet;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mTextsSet:Lcom/android/inputmethod/keyboard/internal/KeyboardTextsSet;

    new-instance v0, Lcom/android/inputmethod/keyboard/internal/KeyStyles;

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mTextsSet:Lcom/android/inputmethod/keyboard/internal/KeyboardTextsSet;

    invoke-direct {v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyStyles;-><init>(Lcom/android/inputmethod/keyboard/internal/KeyboardTextsSet;)V

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mKeyStyles:Lcom/android/inputmethod/keyboard/internal/KeyStyles;

    iput v2, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mMostCommonKeyHeight:I

    iput v2, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mMostCommonKeyWidth:I

    new-instance v0, Lcom/android/inputmethod/keyboard/Keyboard$Params$TouchPositionCorrection;

    invoke-direct {v0}, Lcom/android/inputmethod/keyboard/Keyboard$Params$TouchPositionCorrection;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mTouchPositionCorrection:Lcom/android/inputmethod/keyboard/Keyboard$Params$TouchPositionCorrection;

    iput v2, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mMaxHeightCount:I

    iput v2, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mMaxWidthCount:I

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mHeightHistogram:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mWidthHistogram:Ljava/util/HashMap;

    return-void
.end method

.method private clearHistogram()V
    .locals 2

    const/4 v1, 0x0

    iput v1, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mMostCommonKeyHeight:I

    iput v1, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mMaxHeightCount:I

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mHeightHistogram:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    iput v1, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mMaxWidthCount:I

    iput v1, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mMostCommonKeyWidth:I

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mWidthHistogram:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    return-void
.end method

.method private updateHistogram(Lcom/android/inputmethod/keyboard/Key;)V
    .locals 6
    .param p1    # Lcom/android/inputmethod/keyboard/Key;

    iget v4, p1, Lcom/android/inputmethod/keyboard/Key;->mHeight:I

    iget v5, p1, Lcom/android/inputmethod/keyboard/Key;->mVerticalGap:I

    add-int/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v4, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mHeightHistogram:Ljava/util/HashMap;

    invoke-static {v4, v0}, Lcom/android/inputmethod/keyboard/Keyboard$Params;->updateHistogramCounter(Ljava/util/HashMap;Ljava/lang/Integer;)I

    move-result v1

    iget v4, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mMaxHeightCount:I

    if-le v1, v4, :cond_0

    iput v1, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mMaxHeightCount:I

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iput v4, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mMostCommonKeyHeight:I

    :cond_0
    iget v4, p1, Lcom/android/inputmethod/keyboard/Key;->mWidth:I

    iget v5, p1, Lcom/android/inputmethod/keyboard/Key;->mHorizontalGap:I

    add-int/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v4, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mWidthHistogram:Ljava/util/HashMap;

    invoke-static {v4, v2}, Lcom/android/inputmethod/keyboard/Keyboard$Params;->updateHistogramCounter(Ljava/util/HashMap;Ljava/lang/Integer;)I

    move-result v3

    iget v4, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mMaxWidthCount:I

    if-le v3, v4, :cond_1

    iput v3, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mMaxWidthCount:I

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iput v4, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mMostCommonKeyWidth:I

    :cond_1
    return-void
.end method

.method private static updateHistogramCounter(Ljava/util/HashMap;Ljava/lang/Integer;)I
    .locals 2
    .param p1    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/Integer;",
            ")I"
        }
    .end annotation

    invoke-virtual {p0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_0
    add-int/lit8 v0, v1, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected clearKeys()V
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mKeys:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mShiftKeys:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/Keyboard$Params;->clearHistogram()V

    return-void
.end method

.method public onAddKey(Lcom/android/inputmethod/keyboard/Key;)V
    .locals 4
    .param p1    # Lcom/android/inputmethod/keyboard/Key;

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mKeysCache:Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$KeysCache;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mKeysCache:Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$KeysCache;

    invoke-virtual {v2, p1}, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$KeysCache;->get(Lcom/android/inputmethod/keyboard/Key;)Lcom/android/inputmethod/keyboard/Key;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/Key;->isSpacer()Z

    move-result v2

    if-eqz v2, :cond_4

    iget v2, v0, Lcom/android/inputmethod/keyboard/Key;->mWidth:I

    if-nez v2, :cond_4

    const/4 v1, 0x1

    :goto_1
    if-nez v1, :cond_0

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mKeys:Ljava/util/HashSet;

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-direct {p0, v0}, Lcom/android/inputmethod/keyboard/Keyboard$Params;->updateHistogram(Lcom/android/inputmethod/keyboard/Key;)V

    :cond_0
    iget v2, v0, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mShiftKeys:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/Key;->altCodeWhileTyping()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mAltCodeKeysWhileTyping:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    return-void

    :cond_3
    move-object v0, p1

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method
