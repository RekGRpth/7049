.class public Lcom/android/inputmethod/keyboard/Keyboard;
.super Ljava/lang/Object;
.source "Keyboard.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/inputmethod/keyboard/Keyboard$Builder;,
        Lcom/android/inputmethod/keyboard/Keyboard$Params;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field public final mAltCodeKeysWhileTyping:[Lcom/android/inputmethod/keyboard/Key;

.field public final mIconsSet:Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;

.field public final mId:Lcom/android/inputmethod/keyboard/KeyboardId;

.field private final mKeyCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/android/inputmethod/keyboard/Key;",
            ">;"
        }
    .end annotation
.end field

.field public final mKeys:[Lcom/android/inputmethod/keyboard/Key;

.field public final mMaxMoreKeysKeyboardColumn:I

.field public final mMoreKeysTemplate:I

.field public final mMostCommonKeyHeight:I

.field public final mMostCommonKeyWidth:I

.field public final mOccupiedHeight:I

.field public final mOccupiedWidth:I

.field private final mProximityCharsCorrectionEnabled:Z

.field private final mProximityInfo:Lcom/android/inputmethod/keyboard/ProximityInfo;

.field public final mShiftKeys:[Lcom/android/inputmethod/keyboard/Key;

.field public final mThemeId:I

.field public final mTopPadding:I

.field public final mVerticalGap:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/inputmethod/keyboard/Keyboard;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/inputmethod/keyboard/Keyboard;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/android/inputmethod/keyboard/Keyboard$Params;)V
    .locals 10
    .param p1    # Lcom/android/inputmethod/keyboard/Keyboard$Params;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mKeyCache:Ljava/util/HashMap;

    iget-object v0, p1, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mId:Lcom/android/inputmethod/keyboard/KeyboardId;

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mId:Lcom/android/inputmethod/keyboard/KeyboardId;

    iget v0, p1, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mThemeId:I

    iput v0, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mThemeId:I

    iget v0, p1, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mOccupiedHeight:I

    iput v0, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mOccupiedHeight:I

    iget v0, p1, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mOccupiedWidth:I

    iput v0, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mOccupiedWidth:I

    iget v0, p1, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mMostCommonKeyHeight:I

    iput v0, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mMostCommonKeyHeight:I

    iget v0, p1, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mMostCommonKeyWidth:I

    iput v0, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mMostCommonKeyWidth:I

    iget v0, p1, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mMoreKeysTemplate:I

    iput v0, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mMoreKeysTemplate:I

    iget v0, p1, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mMaxMoreKeysKeyboardColumn:I

    iput v0, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mMaxMoreKeysKeyboardColumn:I

    iget v0, p1, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mTopPadding:I

    iput v0, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mTopPadding:I

    iget v0, p1, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mVerticalGap:I

    iput v0, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mVerticalGap:I

    iget-object v0, p1, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mKeys:Ljava/util/HashSet;

    iget-object v1, p1, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mKeys:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    move-result v1

    new-array v1, v1, [Lcom/android/inputmethod/keyboard/Key;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/inputmethod/keyboard/Key;

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mKeys:[Lcom/android/inputmethod/keyboard/Key;

    iget-object v0, p1, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mShiftKeys:Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mShiftKeys:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lcom/android/inputmethod/keyboard/Key;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/inputmethod/keyboard/Key;

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mShiftKeys:[Lcom/android/inputmethod/keyboard/Key;

    iget-object v0, p1, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mAltCodeKeysWhileTyping:Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mAltCodeKeysWhileTyping:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lcom/android/inputmethod/keyboard/Key;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/inputmethod/keyboard/Key;

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mAltCodeKeysWhileTyping:[Lcom/android/inputmethod/keyboard/Key;

    iget-object v0, p1, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mIconsSet:Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mIconsSet:Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;

    new-instance v0, Lcom/android/inputmethod/keyboard/ProximityInfo;

    iget-object v1, p1, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mId:Lcom/android/inputmethod/keyboard/KeyboardId;

    iget-object v1, v1, Lcom/android/inputmethod/keyboard/KeyboardId;->mLocale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    iget v2, p1, Lcom/android/inputmethod/keyboard/Keyboard$Params;->GRID_WIDTH:I

    iget v3, p1, Lcom/android/inputmethod/keyboard/Keyboard$Params;->GRID_HEIGHT:I

    iget v4, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mOccupiedWidth:I

    iget v5, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mOccupiedHeight:I

    iget v6, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mMostCommonKeyWidth:I

    iget v7, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mMostCommonKeyHeight:I

    iget-object v8, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mKeys:[Lcom/android/inputmethod/keyboard/Key;

    iget-object v9, p1, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mTouchPositionCorrection:Lcom/android/inputmethod/keyboard/Keyboard$Params$TouchPositionCorrection;

    invoke-direct/range {v0 .. v9}, Lcom/android/inputmethod/keyboard/ProximityInfo;-><init>(Ljava/lang/String;IIIIII[Lcom/android/inputmethod/keyboard/Key;Lcom/android/inputmethod/keyboard/Keyboard$Params$TouchPositionCorrection;)V

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mProximityInfo:Lcom/android/inputmethod/keyboard/ProximityInfo;

    iget-boolean v0, p1, Lcom/android/inputmethod/keyboard/Keyboard$Params;->mProximityCharsCorrectionEnabled:Z

    iput-boolean v0, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mProximityCharsCorrectionEnabled:Z

    return-void
.end method

.method public static isLetterCode(I)Z
    .locals 1
    .param p0    # I

    const/16 v0, 0x9

    if-lt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static printableCode(I)Ljava/lang/String;
    .locals 5
    .param p0    # I

    const/4 v4, 0x1

    const/4 v3, 0x0

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    if-gtz p0, :cond_0

    sget-object v0, Lcom/android/inputmethod/keyboard/Keyboard;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown non-positive key code="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/16 v0, 0x20

    if-ge p0, v0, :cond_1

    const-string v0, "\'\\u%02x\'"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_1
    const-string v0, "shift"

    goto :goto_0

    :pswitch_2
    const-string v0, "symbol"

    goto :goto_0

    :pswitch_3
    const-string v0, "text"

    goto :goto_0

    :pswitch_4
    const-string v0, "delete"

    goto :goto_0

    :pswitch_5
    const-string v0, "settings"

    goto :goto_0

    :pswitch_6
    const-string v0, "shortcut"

    goto :goto_0

    :pswitch_7
    const-string v0, "actionEnter"

    goto :goto_0

    :pswitch_8
    const-string v0, "actionNext"

    goto :goto_0

    :pswitch_9
    const-string v0, "actionPrevious"

    goto :goto_0

    :pswitch_a
    const-string v0, "languageSwitch"

    goto :goto_0

    :pswitch_b
    const-string v0, "unspec"

    goto :goto_0

    :pswitch_c
    const-string v0, "tab"

    goto :goto_0

    :pswitch_d
    const-string v0, "enter"

    goto :goto_0

    :cond_1
    const/16 v0, 0x100

    if-ge p0, v0, :cond_2

    const-string v0, "\'%c\'"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const-string v0, "\'\\u%04x\'"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0xb
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method


# virtual methods
.method public getKey(I)Lcom/android/inputmethod/keyboard/Key;
    .locals 7
    .param p1    # I

    const/4 v5, 0x0

    const/16 v6, -0xb

    if-ne p1, v6, :cond_0

    :goto_0
    return-object v5

    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v6, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mKeyCache:Ljava/util/HashMap;

    invoke-virtual {v6, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    iget-object v5, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mKeyCache:Ljava/util/HashMap;

    invoke-virtual {v5, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/inputmethod/keyboard/Key;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mKeys:[Lcom/android/inputmethod/keyboard/Key;

    array-length v4, v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v4, :cond_3

    aget-object v2, v0, v1

    iget v6, v2, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    if-ne v6, p1, :cond_2

    iget-object v5, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mKeyCache:Ljava/util/HashMap;

    invoke-virtual {v5, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v5, v2

    goto :goto_0

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    iget-object v6, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mKeyCache:Ljava/util/HashMap;

    invoke-virtual {v6, v3, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public getNearestKeys(II)[Lcom/android/inputmethod/keyboard/Key;
    .locals 4
    .param p1    # I
    .param p2    # I

    const/4 v3, 0x0

    iget v2, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mOccupiedWidth:I

    add-int/lit8 v2, v2, -0x1

    invoke-static {p1, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget v2, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mOccupiedHeight:I

    add-int/lit8 v2, v2, -0x1

    invoke-static {p2, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mProximityInfo:Lcom/android/inputmethod/keyboard/ProximityInfo;

    invoke-virtual {v2, v0, v1}, Lcom/android/inputmethod/keyboard/ProximityInfo;->getNearestKeys(II)[Lcom/android/inputmethod/keyboard/Key;

    move-result-object v2

    return-object v2
.end method

.method public getProximityInfo()Lcom/android/inputmethod/keyboard/ProximityInfo;
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mProximityInfo:Lcom/android/inputmethod/keyboard/ProximityInfo;

    return-object v0
.end method

.method public hasKey(Lcom/android/inputmethod/keyboard/Key;)Z
    .locals 7
    .param p1    # Lcom/android/inputmethod/keyboard/Key;

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mKeyCache:Ljava/util/HashMap;

    invoke-virtual {v5, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    :goto_0
    return v4

    :cond_0
    iget-object v0, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mKeys:[Lcom/android/inputmethod/keyboard/Key;

    array-length v3, v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v2, v0, v1

    if-ne v2, p1, :cond_1

    iget-object v5, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mKeyCache:Ljava/util/HashMap;

    iget v6, v2, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public hasProximityCharsCorrection(I)Z
    .locals 5
    .param p1    # I

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-boolean v3, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mProximityCharsCorrectionEnabled:Z

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v3, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mId:Lcom/android/inputmethod/keyboard/KeyboardId;

    iget v3, v3, Lcom/android/inputmethod/keyboard/KeyboardId;->mElementId:I

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mId:Lcom/android/inputmethod/keyboard/KeyboardId;

    iget v3, v3, Lcom/android/inputmethod/keyboard/KeyboardId;->mElementId:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_4

    :cond_2
    move v0, v2

    :goto_1
    if-nez v0, :cond_3

    invoke-static {p1}, Ljava/lang/Character;->isLetter(I)Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method
