.class Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;
.super Ljava/lang/Object;
.source "KeyboardLayoutSet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Params"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params$ElementParams;
    }
.end annotation


# instance fields
.field mEditorInfo:Landroid/view/inputmethod/EditorInfo;

.field final mKeyboardLayoutSetElementIdToParamsMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params$ElementParams;",
            ">;"
        }
    .end annotation
.end field

.field mKeyboardLayoutSetName:Ljava/lang/String;

.field mLanguageSwitchKeyEnabled:Z

.field mMode:I

.field mNoSettingsKey:Z

.field mOrientation:I

.field mSubtype:Landroid/view/inputmethod/InputMethodSubtype;

.field mTouchPositionCorrectionEnabled:Z

.field mVoiceKeyEnabled:Z

.field mVoiceKeyOnMain:Z

.field mWidth:I


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;->mKeyboardLayoutSetElementIdToParamsMap:Ljava/util/HashMap;

    return-void
.end method
