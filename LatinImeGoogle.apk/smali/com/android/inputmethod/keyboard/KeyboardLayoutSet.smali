.class public Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;
.super Ljava/lang/Object;
.source "KeyboardLayoutSet.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$1;,
        Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Builder;,
        Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;,
        Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$KeysCache;,
        Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$KeyboardLayoutSetException;
    }
.end annotation


# static fields
.field private static final DEBUG_CACHE:Z

.field private static final TAG:Ljava/lang/String;

.field private static final sKeyboardCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/android/inputmethod/keyboard/KeyboardId;",
            "Ljava/lang/ref/SoftReference",
            "<",
            "Lcom/android/inputmethod/keyboard/Keyboard;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final sKeysCache:Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$KeysCache;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mParams:Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->TAG:Ljava/lang/String;

    sget-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sDBG:Z

    sput-boolean v0, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->DEBUG_CACHE:Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->sKeyboardCache:Ljava/util/HashMap;

    new-instance v0, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$KeysCache;

    invoke-direct {v0}, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$KeysCache;-><init>()V

    sput-object v0, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->sKeysCache:Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$KeysCache;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->mParams:Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;

    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$1;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;
    .param p3    # Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$1;

    invoke-direct {p0, p1, p2}, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;-><init>(Landroid/content/Context;Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;)V

    return-void
.end method

.method public static clearKeyboardCache()V
    .locals 1

    sget-object v0, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->sKeyboardCache:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    sget-object v0, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->sKeysCache:Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$KeysCache;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$KeysCache;->clear()V

    return-void
.end method

.method private getKeyboard(Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params$ElementParams;Lcom/android/inputmethod/keyboard/KeyboardId;)Lcom/android/inputmethod/keyboard/Keyboard;
    .locals 7
    .param p1    # Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params$ElementParams;
    .param p2    # Lcom/android/inputmethod/keyboard/KeyboardId;

    sget-object v4, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->sKeyboardCache:Ljava/util/HashMap;

    invoke-virtual {v4, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/ref/SoftReference;

    if-nez v3, :cond_2

    const/4 v1, 0x0

    :goto_0
    if-nez v1, :cond_4

    new-instance v0, Lcom/android/inputmethod/keyboard/Keyboard$Builder;

    iget-object v4, p0, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->mContext:Landroid/content/Context;

    new-instance v5, Lcom/android/inputmethod/keyboard/Keyboard$Params;

    invoke-direct {v5}, Lcom/android/inputmethod/keyboard/Keyboard$Params;-><init>()V

    invoke-direct {v0, v4, v5}, Lcom/android/inputmethod/keyboard/Keyboard$Builder;-><init>(Landroid/content/Context;Lcom/android/inputmethod/keyboard/Keyboard$Params;)V

    invoke-virtual {p2}, Lcom/android/inputmethod/keyboard/KeyboardId;->isAlphabetKeyboard()Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v4, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->sKeysCache:Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$KeysCache;

    invoke-virtual {v0, v4}, Lcom/android/inputmethod/keyboard/Keyboard$Builder;->setAutoGenerate(Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$KeysCache;)V

    :cond_0
    iget v2, p1, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params$ElementParams;->mKeyboardXmlId:I

    invoke-virtual {v0, v2, p2}, Lcom/android/inputmethod/keyboard/Keyboard$Builder;->load(ILcom/android/inputmethod/keyboard/KeyboardId;)Lcom/android/inputmethod/keyboard/Keyboard$Builder;

    iget-object v4, p0, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->mParams:Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;

    iget-boolean v4, v4, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;->mTouchPositionCorrectionEnabled:Z

    invoke-virtual {v0, v4}, Lcom/android/inputmethod/keyboard/Keyboard$Builder;->setTouchPositionCorrectionEnabled(Z)V

    iget-boolean v4, p1, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params$ElementParams;->mProximityCharsCorrectionEnabled:Z

    invoke-virtual {v0, v4}, Lcom/android/inputmethod/keyboard/Keyboard$Builder;->setProximityCharsCorrectionEnabled(Z)V

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/Keyboard$Builder;->build()Lcom/android/inputmethod/keyboard/Keyboard;

    move-result-object v1

    sget-object v4, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->sKeyboardCache:Ljava/util/HashMap;

    new-instance v5, Ljava/lang/ref/SoftReference;

    invoke-direct {v5, v1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v4, p2, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-boolean v4, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->DEBUG_CACHE:Z

    if-eqz v4, :cond_1

    sget-object v5, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "keyboard cache size="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v6, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->sKeyboardCache:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ": "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    if-nez v3, :cond_3

    const-string v4, "LOAD"

    :goto_1
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " id="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_2
    return-object v1

    :cond_2
    invoke-virtual {v3}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/inputmethod/keyboard/Keyboard;

    move-object v1, v4

    goto :goto_0

    :cond_3
    const-string v4, "GCed"

    goto :goto_1

    :cond_4
    sget-boolean v4, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->DEBUG_CACHE:Z

    if-eqz v4, :cond_1

    sget-object v4, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "keyboard cache size="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->sKeyboardCache:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": HIT  id="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private getKeyboardId(I)Lcom/android/inputmethod/keyboard/KeyboardId;
    .locals 14
    .param p1    # I

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v13, p0, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->mParams:Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;

    const/4 v2, 0x5

    if-eq p1, v2, :cond_0

    const/4 v2, 0x6

    if-ne p1, v2, :cond_1

    :cond_0
    move v11, v1

    :goto_0
    iget-object v2, v13, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;->mSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    invoke-static {v2}, Lcom/android/inputmethod/latin/SubtypeLocale;->isNoLanguage(Landroid/view/inputmethod/InputMethodSubtype;)Z

    move-result v12

    iget-boolean v2, v13, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;->mVoiceKeyEnabled:Z

    if-eqz v2, :cond_2

    if-nez v12, :cond_2

    move v8, v1

    :goto_1
    if-eqz v8, :cond_3

    iget-boolean v2, v13, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;->mVoiceKeyOnMain:Z

    if-eq v11, v2, :cond_3

    move v9, v1

    :goto_2
    new-instance v0, Lcom/android/inputmethod/keyboard/KeyboardId;

    iget-object v2, v13, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;->mSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    iget v3, v13, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;->mOrientation:I

    iget v4, v13, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;->mWidth:I

    iget v5, v13, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;->mMode:I

    iget-object v6, v13, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;->mEditorInfo:Landroid/view/inputmethod/EditorInfo;

    iget-boolean v7, v13, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;->mNoSettingsKey:Z

    iget-boolean v10, v13, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;->mLanguageSwitchKeyEnabled:Z

    move v1, p1

    invoke-direct/range {v0 .. v10}, Lcom/android/inputmethod/keyboard/KeyboardId;-><init>(ILandroid/view/inputmethod/InputMethodSubtype;IIILandroid/view/inputmethod/EditorInfo;ZZZZ)V

    return-object v0

    :cond_1
    move v11, v0

    goto :goto_0

    :cond_2
    move v8, v0

    goto :goto_1

    :cond_3
    move v9, v0

    goto :goto_2
.end method


# virtual methods
.method public getKeyboard(I)Lcom/android/inputmethod/keyboard/Keyboard;
    .locals 6
    .param p1    # I

    iget-object v4, p0, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->mParams:Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;

    iget v4, v4, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;->mMode:I

    packed-switch v4, :pswitch_data_0

    move v3, p1

    :goto_0
    iget-object v4, p0, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->mParams:Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;

    iget-object v4, v4, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;->mKeyboardLayoutSetElementIdToParamsMap:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params$ElementParams;

    if-nez v1, :cond_0

    iget-object v4, p0, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->mParams:Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;

    iget-object v4, v4, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;->mKeyboardLayoutSetElementIdToParamsMap:Ljava/util/HashMap;

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params$ElementParams;

    :cond_0
    invoke-direct {p0, v3}, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->getKeyboardId(I)Lcom/android/inputmethod/keyboard/KeyboardId;

    move-result-object v2

    :try_start_0
    invoke-direct {p0, v1, v2}, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->getKeyboard(Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params$ElementParams;Lcom/android/inputmethod/keyboard/KeyboardId;)Lcom/android/inputmethod/keyboard/Keyboard;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    return-object v4

    :pswitch_0
    const/4 v4, 0x5

    if-ne p1, v4, :cond_1

    const/16 v3, 0x8

    goto :goto_0

    :cond_1
    const/4 v3, 0x7

    goto :goto_0

    :pswitch_1
    const/16 v3, 0x9

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v4, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$KeyboardLayoutSetException;

    invoke-direct {v4, v0, v2}, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$KeyboardLayoutSetException;-><init>(Ljava/lang/Throwable;Lcom/android/inputmethod/keyboard/KeyboardId;)V

    throw v4

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
