.class Lcom/android/inputmethod/keyboard/KeyboardSwitcher$KeyboardTheme;
.super Ljava/lang/Object;
.source "KeyboardSwitcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/keyboard/KeyboardSwitcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "KeyboardTheme"
.end annotation


# instance fields
.field public final mName:Ljava/lang/String;

.field public final mStyleId:I

.field public final mThemeId:I


# direct methods
.method public constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher$KeyboardTheme;->mName:Ljava/lang/String;

    iput p2, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher$KeyboardTheme;->mThemeId:I

    iput p3, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher$KeyboardTheme;->mStyleId:I

    return-void
.end method
