.class public Lcom/android/inputmethod/keyboard/KeyboardSwitcher;
.super Ljava/lang/Object;
.source "KeyboardSwitcher.java"

# interfaces
.implements Lcom/android/inputmethod/keyboard/internal/KeyboardState$SwitchActions;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/inputmethod/keyboard/KeyboardSwitcher$KeyboardTheme;
    }
.end annotation


# static fields
.field private static final KEYBOARD_THEMES:[Lcom/android/inputmethod/keyboard/KeyboardSwitcher$KeyboardTheme;

.field private static final TAG:Ljava/lang/String;

.field private static final sInstance:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;


# instance fields
.field private mCurrentInputView:Lcom/android/inputmethod/latin/InputView;

.field private mForceNonDistinctMultitouch:Z

.field private mIsAutoCorrectionActive:Z

.field private mKeyboardLayoutSet:Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;

.field private mKeyboardTheme:Lcom/android/inputmethod/keyboard/KeyboardSwitcher$KeyboardTheme;

.field private mKeyboardView:Lcom/android/inputmethod/keyboard/LatinKeyboardView;

.field private mLatinIME:Lcom/android/inputmethod/latin/LatinIME;

.field private mPrefs:Landroid/content/SharedPreferences;

.field private mResources:Landroid/content/res/Resources;

.field private mState:Lcom/android/inputmethod/keyboard/internal/KeyboardState;

.field private mSubtypeSwitcher:Lcom/android/inputmethod/latin/SubtypeSwitcher;

.field private mThemeContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x6

    const/4 v6, 0x5

    const/4 v5, 0x1

    const/4 v4, 0x0

    const-class v0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->TAG:Ljava/lang/String;

    new-array v0, v7, [Lcom/android/inputmethod/keyboard/KeyboardSwitcher$KeyboardTheme;

    new-instance v1, Lcom/android/inputmethod/keyboard/KeyboardSwitcher$KeyboardTheme;

    const-string v2, "Basic"

    const v3, 0x7f0e002b

    invoke-direct {v1, v2, v4, v3}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher$KeyboardTheme;-><init>(Ljava/lang/String;II)V

    aput-object v1, v0, v4

    new-instance v1, Lcom/android/inputmethod/keyboard/KeyboardSwitcher$KeyboardTheme;

    const-string v2, "HighContrast"

    const v3, 0x7f0e002a

    invoke-direct {v1, v2, v5, v3}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher$KeyboardTheme;-><init>(Ljava/lang/String;II)V

    aput-object v1, v0, v5

    const/4 v1, 0x2

    new-instance v2, Lcom/android/inputmethod/keyboard/KeyboardSwitcher$KeyboardTheme;

    const-string v3, "Stone"

    const v4, 0x7f0e002f

    invoke-direct {v2, v3, v7, v4}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher$KeyboardTheme;-><init>(Ljava/lang/String;II)V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Lcom/android/inputmethod/keyboard/KeyboardSwitcher$KeyboardTheme;

    const-string v3, "Stne.Bold"

    const/4 v4, 0x7

    const v5, 0x7f0e002e

    invoke-direct {v2, v3, v4, v5}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher$KeyboardTheme;-><init>(Ljava/lang/String;II)V

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-instance v2, Lcom/android/inputmethod/keyboard/KeyboardSwitcher$KeyboardTheme;

    const-string v3, "GingerBread"

    const/16 v4, 0x8

    const v5, 0x7f0e002c

    invoke-direct {v2, v3, v4, v5}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher$KeyboardTheme;-><init>(Ljava/lang/String;II)V

    aput-object v2, v0, v1

    new-instance v1, Lcom/android/inputmethod/keyboard/KeyboardSwitcher$KeyboardTheme;

    const-string v2, "IceCreamSandwich"

    const v3, 0x7f0e002d

    invoke-direct {v1, v2, v6, v3}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher$KeyboardTheme;-><init>(Ljava/lang/String;II)V

    aput-object v1, v0, v6

    sput-object v0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->KEYBOARD_THEMES:[Lcom/android/inputmethod/keyboard/KeyboardSwitcher$KeyboardTheme;

    new-instance v0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-direct {v0}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;-><init>()V

    sput-object v0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->sInstance:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->KEYBOARD_THEMES:[Lcom/android/inputmethod/keyboard/KeyboardSwitcher$KeyboardTheme;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mKeyboardTheme:Lcom/android/inputmethod/keyboard/KeyboardSwitcher$KeyboardTheme;

    return-void
.end method

.method public static getInstance()Lcom/android/inputmethod/keyboard/KeyboardSwitcher;
    .locals 1

    sget-object v0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->sInstance:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    return-object v0
.end method

.method private static getKeyboardTheme(Landroid/content/Context;Landroid/content/SharedPreferences;)Lcom/android/inputmethod/keyboard/KeyboardSwitcher$KeyboardTheme;
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/content/SharedPreferences;

    const/high16 v3, 0x7f0a0000

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v3, "pref_keyboard_layout_20110916"

    invoke-interface {p1, v3, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :try_start_0
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ltz v1, :cond_0

    sget-object v3, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->KEYBOARD_THEMES:[Lcom/android/inputmethod/keyboard/KeyboardSwitcher$KeyboardTheme;

    array-length v3, v3

    if-ge v1, v3, :cond_0

    sget-object v3, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->KEYBOARD_THEMES:[Lcom/android/inputmethod/keyboard/KeyboardSwitcher$KeyboardTheme;

    aget-object v3, v3, v1
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v3

    :catch_0
    move-exception v3

    :cond_0
    sget-object v3, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Illegal keyboard theme in preference: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", default to 0"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v3, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->KEYBOARD_THEMES:[Lcom/android/inputmethod/keyboard/KeyboardSwitcher$KeyboardTheme;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    goto :goto_0
.end method

.method public static init(Lcom/android/inputmethod/latin/LatinIME;Landroid/content/SharedPreferences;)V
    .locals 1
    .param p0    # Lcom/android/inputmethod/latin/LatinIME;
    .param p1    # Landroid/content/SharedPreferences;

    sget-object v0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->sInstance:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-direct {v0, p0, p1}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->initInternal(Lcom/android/inputmethod/latin/LatinIME;Landroid/content/SharedPreferences;)V

    return-void
.end method

.method private initInternal(Lcom/android/inputmethod/latin/LatinIME;Landroid/content/SharedPreferences;)V
    .locals 2
    .param p1    # Lcom/android/inputmethod/latin/LatinIME;
    .param p2    # Landroid/content/SharedPreferences;

    iput-object p1, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mLatinIME:Lcom/android/inputmethod/latin/LatinIME;

    invoke-virtual {p1}, Lcom/android/inputmethod/latin/LatinIME;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mResources:Landroid/content/res/Resources;

    iput-object p2, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mPrefs:Landroid/content/SharedPreferences;

    invoke-static {}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->getInstance()Lcom/android/inputmethod/latin/SubtypeSwitcher;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mSubtypeSwitcher:Lcom/android/inputmethod/latin/SubtypeSwitcher;

    new-instance v0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;

    invoke-direct {v0, p0}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;-><init>(Lcom/android/inputmethod/keyboard/internal/KeyboardState$SwitchActions;)V

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mState:Lcom/android/inputmethod/keyboard/internal/KeyboardState;

    invoke-static {p1, p2}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getKeyboardTheme(Landroid/content/Context;Landroid/content/SharedPreferences;)Lcom/android/inputmethod/keyboard/KeyboardSwitcher$KeyboardTheme;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->setContextThemeWrapper(Landroid/content/Context;Lcom/android/inputmethod/keyboard/KeyboardSwitcher$KeyboardTheme;)V

    const-string v0, "force_non_distinct_multitouch"

    const/4 v1, 0x0

    invoke-interface {p2, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mForceNonDistinctMultitouch:Z

    return-void
.end method

.method private isSinglePointer()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mKeyboardView:Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mKeyboardView:Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->getPointerCount()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isVibrateAndSoundFeedbackRequired()Z
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mKeyboardView:Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mKeyboardView:Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->isInSlidingKeyInput()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setContextThemeWrapper(Landroid/content/Context;Lcom/android/inputmethod/keyboard/KeyboardSwitcher$KeyboardTheme;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/inputmethod/keyboard/KeyboardSwitcher$KeyboardTheme;

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mKeyboardTheme:Lcom/android/inputmethod/keyboard/KeyboardSwitcher$KeyboardTheme;

    iget v0, v0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher$KeyboardTheme;->mThemeId:I

    iget v1, p2, Lcom/android/inputmethod/keyboard/KeyboardSwitcher$KeyboardTheme;->mThemeId:I

    if-eq v0, v1, :cond_0

    iput-object p2, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mKeyboardTheme:Lcom/android/inputmethod/keyboard/KeyboardSwitcher$KeyboardTheme;

    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget v1, p2, Lcom/android/inputmethod/keyboard/KeyboardSwitcher$KeyboardTheme;->mStyleId:I

    invoke-direct {v0, p1, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mThemeContext:Landroid/content/Context;

    invoke-static {}, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->clearKeyboardCache()V

    :cond_0
    return-void
.end method

.method private setKeyboard(Lcom/android/inputmethod/keyboard/Keyboard;)V
    .locals 8
    .param p1    # Lcom/android/inputmethod/keyboard/Keyboard;

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mKeyboardView:Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    invoke-virtual {v4}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->getKeyboard()Lcom/android/inputmethod/keyboard/Keyboard;

    move-result-object v1

    iget-object v4, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mKeyboardView:Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    invoke-virtual {v4, p1}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->setKeyboard(Lcom/android/inputmethod/keyboard/Keyboard;)V

    iget-object v4, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mCurrentInputView:Lcom/android/inputmethod/latin/InputView;

    iget v5, p1, Lcom/android/inputmethod/keyboard/Keyboard;->mTopPadding:I

    invoke-virtual {v4, v5}, Lcom/android/inputmethod/latin/InputView;->setKeyboardGeometry(I)V

    iget-object v4, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mKeyboardView:Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    iget-object v5, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v6, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mResources:Landroid/content/res/Resources;

    invoke-static {v5, v6}, Lcom/android/inputmethod/latin/SettingsValues;->isKeyPreviewPopupEnabled(Landroid/content/SharedPreferences;Landroid/content/res/Resources;)Z

    move-result v5

    iget-object v6, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v7, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mResources:Landroid/content/res/Resources;

    invoke-static {v6, v7}, Lcom/android/inputmethod/latin/SettingsValues;->getKeyPreviewPopupDismissDelay(Landroid/content/SharedPreferences;Landroid/content/res/Resources;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->setKeyPreviewPopupEnabled(ZI)V

    iget-object v4, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mKeyboardView:Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    iget-boolean v5, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mIsAutoCorrectionActive:Z

    invoke-virtual {v4, v5}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->updateAutoCorrectionState(Z)V

    iget-object v4, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mKeyboardView:Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    iget-object v5, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mSubtypeSwitcher:Lcom/android/inputmethod/latin/SubtypeSwitcher;

    invoke-virtual {v5}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->isShortcutImeReady()Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->updateShortcutKey(Z)V

    if-eqz v1, :cond_0

    iget-object v4, p1, Lcom/android/inputmethod/keyboard/Keyboard;->mId:Lcom/android/inputmethod/keyboard/KeyboardId;

    iget-object v4, v4, Lcom/android/inputmethod/keyboard/KeyboardId;->mLocale:Ljava/util/Locale;

    iget-object v5, v1, Lcom/android/inputmethod/keyboard/Keyboard;->mId:Lcom/android/inputmethod/keyboard/KeyboardId;

    iget-object v5, v5, Lcom/android/inputmethod/keyboard/KeyboardId;->mLocale:Ljava/util/Locale;

    invoke-virtual {v4, v5}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    move v2, v3

    :goto_0
    iget-object v4, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mSubtypeSwitcher:Lcom/android/inputmethod/latin/SubtypeSwitcher;

    iget-object v5, p1, Lcom/android/inputmethod/keyboard/Keyboard;->mId:Lcom/android/inputmethod/keyboard/KeyboardId;

    iget-object v5, v5, Lcom/android/inputmethod/keyboard/KeyboardId;->mLocale:Ljava/util/Locale;

    invoke-virtual {v4, v5}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->needsToDisplayLanguage(Ljava/util/Locale;)Z

    move-result v0

    iget-object v4, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mKeyboardView:Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    iget-object v5, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mLatinIME:Lcom/android/inputmethod/latin/LatinIME;

    invoke-static {v5, v3}, Lcom/android/inputmethod/latin/ImfUtils;->hasMultipleEnabledIMEsOrSubtypes(Landroid/content/Context;Z)Z

    move-result v3

    invoke-virtual {v4, v2, v0, v3}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->startDisplayLanguageOnSpacebar(ZZZ)V

    return-void

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public cancelDoubleTapTimer()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getKeyboardView()Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->getTimerProxy()Lcom/android/inputmethod/keyboard/PointerTracker$TimerProxy;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/inputmethod/keyboard/PointerTracker$TimerProxy;->cancelDoubleTapTimer()V

    :cond_0
    return-void
.end method

.method public cancelLongPressTimer()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getKeyboardView()Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->getTimerProxy()Lcom/android/inputmethod/keyboard/PointerTracker$TimerProxy;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/inputmethod/keyboard/PointerTracker$TimerProxy;->cancelLongPressTimer()V

    :cond_0
    return-void
.end method

.method public getKeyboard()Lcom/android/inputmethod/keyboard/Keyboard;
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mKeyboardView:Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mKeyboardView:Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->getKeyboard()Lcom/android/inputmethod/keyboard/Keyboard;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getKeyboardView()Lcom/android/inputmethod/keyboard/LatinKeyboardView;
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mKeyboardView:Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    return-object v0
.end method

.method public hapticAndAudioFeedback(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mLatinIME:Lcom/android/inputmethod/latin/LatinIME;

    invoke-virtual {v0, p1}, Lcom/android/inputmethod/latin/LatinIME;->hapticAndAudioFeedback(I)V

    return-void
.end method

.method public isInDoubleTapTimeout()Z
    .locals 2

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getKeyboardView()Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->getTimerProxy()Lcom/android/inputmethod/keyboard/PointerTracker$TimerProxy;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/inputmethod/keyboard/PointerTracker$TimerProxy;->isInDoubleTapTimeout()Z

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isInMomentarySwitchState()Z
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mState:Lcom/android/inputmethod/keyboard/internal/KeyboardState;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->isInMomentarySwitchState()Z

    move-result v0

    return v0
.end method

.method public loadKeyboard(Landroid/view/inputmethod/EditorInfo;Lcom/android/inputmethod/latin/SettingsValues;)V
    .locals 5
    .param p1    # Landroid/view/inputmethod/EditorInfo;
    .param p2    # Lcom/android/inputmethod/latin/SettingsValues;

    new-instance v0, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Builder;

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mThemeContext:Landroid/content/Context;

    invoke-direct {v0, v2, p1}, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Builder;-><init>(Landroid/content/Context;Landroid/view/inputmethod/EditorInfo;)V

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mThemeContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mThemeContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v0, v2, v3}, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Builder;->setScreenGeometry(II)Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Builder;

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mSubtypeSwitcher:Lcom/android/inputmethod/latin/SubtypeSwitcher;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->getCurrentSubtype()Landroid/view/inputmethod/InputMethodSubtype;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Builder;->setSubtype(Landroid/view/inputmethod/InputMethodSubtype;)Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Builder;

    invoke-virtual {p2, p1}, Lcom/android/inputmethod/latin/SettingsValues;->isVoiceKeyEnabled(Landroid/view/inputmethod/EditorInfo;)Z

    move-result v2

    invoke-virtual {p2}, Lcom/android/inputmethod/latin/SettingsValues;->isVoiceKeyOnMain()Z

    move-result v3

    iget-object v4, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mThemeContext:Landroid/content/Context;

    invoke-virtual {p2, v4}, Lcom/android/inputmethod/latin/SettingsValues;->isLanguageSwitchKeyEnabled(Landroid/content/Context;)Z

    move-result v4

    invoke-virtual {v0, v2, v3, v4}, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Builder;->setOptions(ZZZ)Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Builder;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Builder;->build()Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;

    move-result-object v2

    iput-object v2, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mKeyboardLayoutSet:Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;

    :try_start_0
    iget-object v2, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mState:Lcom/android/inputmethod/keyboard/internal/KeyboardState;

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mResources:Landroid/content/res/Resources;

    const v4, 0x7f0a0008

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->onLoadKeyboard(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$KeyboardLayoutSetException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    sget-object v2, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "loading keyboard failed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$KeyboardLayoutSetException;->mKeyboardId:Lcom/android/inputmethod/keyboard/KeyboardId;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$KeyboardLayoutSetException;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    invoke-static {v2, v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v2, v1, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$KeyboardLayoutSetException;->mKeyboardId:Lcom/android/inputmethod/keyboard/KeyboardId;

    invoke-virtual {v2}, Lcom/android/inputmethod/keyboard/KeyboardId;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$KeyboardLayoutSetException;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/inputmethod/latin/LatinImeLogger;->logOnException(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onAutoCorrectionStateChanged(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mIsAutoCorrectionActive:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mIsAutoCorrectionActive:Z

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mKeyboardView:Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mKeyboardView:Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    invoke-virtual {v0, p1}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->updateAutoCorrectionState(Z)V

    :cond_0
    return-void
.end method

.method public onCancelInput()V
    .locals 2

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mState:Lcom/android/inputmethod/keyboard/internal/KeyboardState;

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->isSinglePointer()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->onCancelInput(Z)V

    return-void
.end method

.method public onCodeInput(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mState:Lcom/android/inputmethod/keyboard/internal/KeyboardState;

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->isSinglePointer()Z

    move-result v1

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mLatinIME:Lcom/android/inputmethod/latin/LatinIME;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/LatinIME;->getCurrentAutoCapsState()I

    move-result v2

    invoke-virtual {v0, p1, v1, v2}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->onCodeInput(IZI)V

    return-void
.end method

.method public onCreateInputView()Landroid/view/View;
    .locals 6

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mKeyboardView:Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mKeyboardView:Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    invoke-virtual {v3}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->closing()V

    :cond_0
    invoke-static {}, Lcom/android/inputmethod/latin/Utils$GCUtils;->getInstance()Lcom/android/inputmethod/latin/Utils$GCUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/inputmethod/latin/Utils$GCUtils;->reset()V

    const/4 v2, 0x1

    const/4 v1, 0x0

    :goto_0
    const/4 v3, 0x5

    if-ge v1, v3, :cond_1

    if-eqz v2, :cond_1

    :try_start_0
    iget-object v3, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mLatinIME:Lcom/android/inputmethod/latin/LatinIME;

    iget-object v4, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mKeyboardTheme:Lcom/android/inputmethod/keyboard/KeyboardSwitcher$KeyboardTheme;

    invoke-direct {p0, v3, v4}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->setContextThemeWrapper(Landroid/content/Context;Lcom/android/inputmethod/keyboard/KeyboardSwitcher$KeyboardTheme;)V

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mThemeContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f030002

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/inputmethod/latin/InputView;

    iput-object v3, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mCurrentInputView:Lcom/android/inputmethod/latin/InputView;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/view/InflateException; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v2, 0x0

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v3, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "load keyboard failed: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/android/inputmethod/latin/Utils$GCUtils;->getInstance()Lcom/android/inputmethod/latin/Utils$GCUtils;

    move-result-object v3

    iget-object v4, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mKeyboardTheme:Lcom/android/inputmethod/keyboard/KeyboardSwitcher$KeyboardTheme;

    iget-object v4, v4, Lcom/android/inputmethod/keyboard/KeyboardSwitcher$KeyboardTheme;->mName:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Lcom/android/inputmethod/latin/Utils$GCUtils;->tryGCOrWait(Ljava/lang/String;Ljava/lang/Throwable;)Z

    move-result v2

    goto :goto_1

    :catch_1
    move-exception v0

    sget-object v3, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "load keyboard failed: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/android/inputmethod/latin/Utils$GCUtils;->getInstance()Lcom/android/inputmethod/latin/Utils$GCUtils;

    move-result-object v3

    iget-object v4, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mKeyboardTheme:Lcom/android/inputmethod/keyboard/KeyboardSwitcher$KeyboardTheme;

    iget-object v4, v4, Lcom/android/inputmethod/keyboard/KeyboardSwitcher$KeyboardTheme;->mName:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Lcom/android/inputmethod/latin/Utils$GCUtils;->tryGCOrWait(Ljava/lang/String;Ljava/lang/Throwable;)Z

    move-result v2

    goto :goto_1

    :cond_1
    iget-object v3, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mCurrentInputView:Lcom/android/inputmethod/latin/InputView;

    const v4, 0x7f070041

    invoke-virtual {v3, v4}, Lcom/android/inputmethod/latin/InputView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    iput-object v3, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mKeyboardView:Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mKeyboardView:Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    iget-object v4, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mLatinIME:Lcom/android/inputmethod/latin/LatinIME;

    invoke-virtual {v3, v4}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->setKeyboardActionListener(Lcom/android/inputmethod/keyboard/KeyboardActionListener;)V

    iget-boolean v3, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mForceNonDistinctMultitouch:Z

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mKeyboardView:Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->setDistinctMultitouch(Z)V

    :cond_2
    invoke-static {}, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->getInstance()Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;

    move-result-object v3

    iget-object v4, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mKeyboardView:Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    invoke-virtual {v3, v4}, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->setView(Lcom/android/inputmethod/keyboard/LatinKeyboardView;)V

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mCurrentInputView:Lcom/android/inputmethod/latin/InputView;

    return-object v3
.end method

.method public onFinishInputView()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mIsAutoCorrectionActive:Z

    return-void
.end method

.method public onHideWindow()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mIsAutoCorrectionActive:Z

    return-void
.end method

.method public onLongPressTimeout(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mState:Lcom/android/inputmethod/keyboard/internal/KeyboardState;

    invoke-virtual {v0, p1}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->onLongPressTimeout(I)V

    return-void
.end method

.method public onNetworkStateChanged()V
    .locals 2

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mKeyboardView:Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mKeyboardView:Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mSubtypeSwitcher:Lcom/android/inputmethod/latin/SubtypeSwitcher;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->isShortcutImeReady()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->updateShortcutKey(Z)V

    :cond_0
    return-void
.end method

.method public onPressKey(I)V
    .locals 3
    .param p1    # I

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->isVibrateAndSoundFeedbackRequired()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mLatinIME:Lcom/android/inputmethod/latin/LatinIME;

    invoke-virtual {v0, p1}, Lcom/android/inputmethod/latin/LatinIME;->hapticAndAudioFeedback(I)V

    :cond_0
    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mState:Lcom/android/inputmethod/keyboard/internal/KeyboardState;

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->isSinglePointer()Z

    move-result v1

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mLatinIME:Lcom/android/inputmethod/latin/LatinIME;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/LatinIME;->getCurrentAutoCapsState()I

    move-result v2

    invoke-virtual {v0, p1, v1, v2}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->onPressKey(IZI)V

    return-void
.end method

.method public onReleaseKey(IZ)V
    .locals 1
    .param p1    # I
    .param p2    # Z

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mState:Lcom/android/inputmethod/keyboard/internal/KeyboardState;

    invoke-virtual {v0, p1, p2}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->onReleaseKey(IZ)V

    return-void
.end method

.method public requestUpdatingShiftState()V
    .locals 2

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mState:Lcom/android/inputmethod/keyboard/internal/KeyboardState;

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mLatinIME:Lcom/android/inputmethod/latin/LatinIME;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/LatinIME;->getCurrentAutoCapsState()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->onUpdateShiftState(I)V

    return-void
.end method

.method public saveKeyboardState()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getKeyboard()Lcom/android/inputmethod/keyboard/Keyboard;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mState:Lcom/android/inputmethod/keyboard/internal/KeyboardState;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->onSaveKeyboardState()V

    :cond_0
    return-void
.end method

.method public setAlphabetAutomaticShiftedKeyboard()V
    .locals 2

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mKeyboardLayoutSet:Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->getKeyboard(I)Lcom/android/inputmethod/keyboard/Keyboard;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->setKeyboard(Lcom/android/inputmethod/keyboard/Keyboard;)V

    return-void
.end method

.method public setAlphabetKeyboard()V
    .locals 2

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mKeyboardLayoutSet:Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->getKeyboard(I)Lcom/android/inputmethod/keyboard/Keyboard;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->setKeyboard(Lcom/android/inputmethod/keyboard/Keyboard;)V

    return-void
.end method

.method public setAlphabetManualShiftedKeyboard()V
    .locals 2

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mKeyboardLayoutSet:Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->getKeyboard(I)Lcom/android/inputmethod/keyboard/Keyboard;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->setKeyboard(Lcom/android/inputmethod/keyboard/Keyboard;)V

    return-void
.end method

.method public setAlphabetShiftLockShiftedKeyboard()V
    .locals 2

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mKeyboardLayoutSet:Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->getKeyboard(I)Lcom/android/inputmethod/keyboard/Keyboard;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->setKeyboard(Lcom/android/inputmethod/keyboard/Keyboard;)V

    return-void
.end method

.method public setAlphabetShiftLockedKeyboard()V
    .locals 2

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mKeyboardLayoutSet:Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->getKeyboard(I)Lcom/android/inputmethod/keyboard/Keyboard;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->setKeyboard(Lcom/android/inputmethod/keyboard/Keyboard;)V

    return-void
.end method

.method public setSymbolsKeyboard()V
    .locals 2

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mKeyboardLayoutSet:Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->getKeyboard(I)Lcom/android/inputmethod/keyboard/Keyboard;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->setKeyboard(Lcom/android/inputmethod/keyboard/Keyboard;)V

    return-void
.end method

.method public setSymbolsShiftedKeyboard()V
    .locals 2

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mKeyboardLayoutSet:Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->getKeyboard(I)Lcom/android/inputmethod/keyboard/Keyboard;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->setKeyboard(Lcom/android/inputmethod/keyboard/Keyboard;)V

    return-void
.end method

.method public startDoubleTapTimer()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getKeyboardView()Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->getTimerProxy()Lcom/android/inputmethod/keyboard/PointerTracker$TimerProxy;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/inputmethod/keyboard/PointerTracker$TimerProxy;->startDoubleTapTimer()V

    :cond_0
    return-void
.end method

.method public startLongPressTimer(I)V
    .locals 2
    .param p1    # I

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getKeyboardView()Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->getTimerProxy()Lcom/android/inputmethod/keyboard/PointerTracker$TimerProxy;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/android/inputmethod/keyboard/PointerTracker$TimerProxy;->startLongPressTimer(I)V

    :cond_0
    return-void
.end method

.method public updateShiftState()V
    .locals 2

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mState:Lcom/android/inputmethod/keyboard/internal/KeyboardState;

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->mLatinIME:Lcom/android/inputmethod/latin/LatinIME;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/LatinIME;->getCurrentAutoCapsState()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->onUpdateShiftState(I)V

    return-void
.end method
