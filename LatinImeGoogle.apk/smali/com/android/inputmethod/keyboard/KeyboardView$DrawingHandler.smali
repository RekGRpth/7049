.class public Lcom/android/inputmethod/keyboard/KeyboardView$DrawingHandler;
.super Lcom/android/inputmethod/latin/StaticInnerHandlerWrapper;
.source "KeyboardView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/keyboard/KeyboardView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DrawingHandler"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/inputmethod/latin/StaticInnerHandlerWrapper",
        "<",
        "Lcom/android/inputmethod/keyboard/KeyboardView;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/android/inputmethod/keyboard/KeyboardView;)V
    .locals 0
    .param p1    # Lcom/android/inputmethod/keyboard/KeyboardView;

    invoke-direct {p0, p1}, Lcom/android/inputmethod/latin/StaticInnerHandlerWrapper;-><init>(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public cancelAllDismissKeyPreviews()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/inputmethod/keyboard/KeyboardView$DrawingHandler;->removeMessages(I)V

    return-void
.end method

.method public cancelAllMessages()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/KeyboardView$DrawingHandler;->cancelAllDismissKeyPreviews()V

    return-void
.end method

.method public cancelDismissKeyPreview(Lcom/android/inputmethod/keyboard/PointerTracker;)V
    .locals 1
    .param p1    # Lcom/android/inputmethod/keyboard/PointerTracker;

    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1}, Lcom/android/inputmethod/keyboard/KeyboardView$DrawingHandler;->removeMessages(ILjava/lang/Object;)V

    return-void
.end method

.method public dismissKeyPreview(JLcom/android/inputmethod/keyboard/PointerTracker;)V
    .locals 1
    .param p1    # J
    .param p3    # Lcom/android/inputmethod/keyboard/PointerTracker;

    const/4 v0, 0x1

    invoke-virtual {p0, v0, p3}, Lcom/android/inputmethod/keyboard/KeyboardView$DrawingHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2}, Lcom/android/inputmethod/keyboard/KeyboardView$DrawingHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1    # Landroid/os/Message;

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/KeyboardView$DrawingHandler;->getOuterInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/inputmethod/keyboard/KeyboardView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/android/inputmethod/keyboard/PointerTracker;

    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/PointerTracker;->getKeyPreviewText()Landroid/widget/TextView;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
