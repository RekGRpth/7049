.class public Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;
.super Ljava/lang/Object;
.source "KeyboardView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/keyboard/KeyboardView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "KeyDrawParams"
.end annotation


# instance fields
.field public mAnimAlpha:I

.field public final mKeyBackground:Landroid/graphics/drawable/Drawable;

.field public final mKeyHintLabelColor:I

.field private final mKeyHintLabelRatio:F

.field public mKeyHintLabelSize:I

.field public final mKeyHintLetterColor:I

.field public final mKeyHintLetterPadding:F

.field private final mKeyHintLetterRatio:F

.field public mKeyHintLetterSize:I

.field public final mKeyLabelHorizontalPadding:F

.field private final mKeyLabelRatio:F

.field public mKeyLabelSize:I

.field private final mKeyLargeLabelRatio:F

.field public mKeyLargeLabelSize:I

.field private final mKeyLargeLetterRatio:F

.field public mKeyLargeLetterSize:I

.field final mKeyLetterRatio:F

.field public mKeyLetterSize:I

.field public final mKeyPopupHintLetterPadding:F

.field public final mKeyShiftedLetterHintActivatedColor:I

.field public final mKeyShiftedLetterHintInactivatedColor:I

.field public final mKeyShiftedLetterHintPadding:F

.field private final mKeyShiftedLetterHintRatio:F

.field public mKeyShiftedLetterHintSize:I

.field public final mKeyTextColor:I

.field public final mKeyTextInactivatedColor:I

.field public final mKeyTextStyle:Landroid/graphics/Typeface;

.field public final mPadding:Landroid/graphics/Rect;

.field public final mShadowColor:I

.field public final mShadowRadius:F


# direct methods
.method public constructor <init>(Landroid/content/res/TypedArray;)V
    .locals 6
    .param p1    # Landroid/content/res/TypedArray;

    const/4 v5, 0x1

    const/high16 v4, -0x1000000

    const/high16 v3, -0x40800000

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mPadding:Landroid/graphics/Rect;

    invoke-virtual {p1, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v5}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iput v3, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyLetterRatio:F

    invoke-virtual {p1, v5, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyLetterSize:I

    :goto_0
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iput v3, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyLabelRatio:F

    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyLabelSize:I

    :goto_1
    const/4 v0, 0x6

    invoke-static {p1, v0}, Lcom/android/inputmethod/keyboard/KeyboardView;->getRatio(Landroid/content/res/TypedArray;I)F

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyLargeLabelRatio:F

    const/4 v0, 0x4

    invoke-static {p1, v0}, Lcom/android/inputmethod/keyboard/KeyboardView;->getRatio(Landroid/content/res/TypedArray;I)F

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyLargeLetterRatio:F

    const/4 v0, 0x7

    invoke-static {p1, v0}, Lcom/android/inputmethod/keyboard/KeyboardView;->getRatio(Landroid/content/res/TypedArray;I)F

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyHintLetterRatio:F

    const/16 v0, 0x9

    invoke-static {p1, v0}, Lcom/android/inputmethod/keyboard/KeyboardView;->getRatio(Landroid/content/res/TypedArray;I)F

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyShiftedLetterHintRatio:F

    const/16 v0, 0x8

    invoke-static {p1, v0}, Lcom/android/inputmethod/keyboard/KeyboardView;->getRatio(Landroid/content/res/TypedArray;I)F

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyHintLabelRatio:F

    const/16 v0, 0xa

    invoke-virtual {p1, v0, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyLabelHorizontalPadding:F

    const/16 v0, 0xb

    invoke-virtual {p1, v0, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyHintLetterPadding:F

    const/16 v0, 0xc

    invoke-virtual {p1, v0, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyPopupHintLetterPadding:F

    const/16 v0, 0xd

    invoke-virtual {p1, v0, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyShiftedLetterHintPadding:F

    const/16 v0, 0xe

    invoke-virtual {p1, v0, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyTextColor:I

    const/16 v0, 0xf

    invoke-virtual {p1, v0, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyTextInactivatedColor:I

    const/16 v0, 0x10

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyHintLetterColor:I

    const/16 v0, 0x11

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyHintLabelColor:I

    const/16 v0, 0x12

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyShiftedLetterHintInactivatedColor:I

    const/16 v0, 0x13

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyShiftedLetterHintActivatedColor:I

    const/16 v0, 0x22

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    invoke-static {v0}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyTextStyle:Landroid/graphics/Typeface;

    const/16 v0, 0x1f

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mShadowColor:I

    const/16 v0, 0x20

    invoke-virtual {p1, v0, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mShadowRadius:F

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyBackground:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mPadding:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    return-void

    :cond_0
    const/4 v0, 0x3

    invoke-static {p1, v0}, Lcom/android/inputmethod/keyboard/KeyboardView;->getRatio(Landroid/content/res/TypedArray;I)F

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyLetterRatio:F

    goto/16 :goto_0

    :cond_1
    const/4 v0, 0x5

    invoke-static {p1, v0}, Lcom/android/inputmethod/keyboard/KeyboardView;->getRatio(Landroid/content/res/TypedArray;I)F

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyLabelRatio:F

    goto/16 :goto_1
.end method


# virtual methods
.method public blendAlpha(Landroid/graphics/Paint;)V
    .locals 5
    .param p1    # Landroid/graphics/Paint;

    invoke-virtual {p1}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Paint;->getAlpha()I

    move-result v1

    iget v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mAnimAlpha:I

    mul-int/2addr v1, v2

    div-int/lit16 v1, v1, 0xff

    invoke-static {v0}, Landroid/graphics/Color;->red(I)I

    move-result v2

    invoke-static {v0}, Landroid/graphics/Color;->green(I)I

    move-result v3

    invoke-static {v0}, Landroid/graphics/Color;->blue(I)I

    move-result v4

    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Paint;->setARGB(IIII)V

    return-void
.end method

.method public updateKeyHeight(I)V
    .locals 3
    .param p1    # I

    const/4 v2, 0x0

    iget v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyLetterRatio:F

    cmpl-float v0, v0, v2

    if-ltz v0, :cond_0

    int-to-float v0, p1

    iget v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyLetterRatio:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyLetterSize:I

    :cond_0
    iget v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyLabelRatio:F

    cmpl-float v0, v0, v2

    if-ltz v0, :cond_1

    int-to-float v0, p1

    iget v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyLabelRatio:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyLabelSize:I

    :cond_1
    int-to-float v0, p1

    iget v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyLargeLabelRatio:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyLargeLabelSize:I

    int-to-float v0, p1

    iget v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyLargeLetterRatio:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyLargeLetterSize:I

    int-to-float v0, p1

    iget v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyHintLetterRatio:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyHintLetterSize:I

    int-to-float v0, p1

    iget v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyShiftedLetterHintRatio:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyShiftedLetterHintSize:I

    int-to-float v0, p1

    iget v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyHintLabelRatio:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyHintLabelSize:I

    return-void
.end method
