.class public Lcom/android/inputmethod/keyboard/KeyboardView;
.super Landroid/view/View;
.source "KeyboardView.java"

# interfaces
.implements Lcom/android/inputmethod/keyboard/PointerTracker$DrawingProxy;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/inputmethod/keyboard/KeyboardView$KeyPreviewDrawParams;,
        Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;,
        Lcom/android/inputmethod/keyboard/KeyboardView$DrawingHandler;
    }
.end annotation


# static fields
.field private static final KEY_LABEL_REFERENCE_CHAR:[C

.field private static final KEY_NUMERIC_HINT_LABEL_REFERENCE_CHAR:[C

.field private static final LONG_PRESSABLE_STATE_SET:[I

.field private static final sTextHeightCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private static final sTextWidthCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mBackgroundDimAlpha:I

.field private mBuffer:Landroid/graphics/Bitmap;

.field private mBufferNeedsUpdate:Z

.field private mCanvas:Landroid/graphics/Canvas;

.field private mDelayAfterPreview:I

.field private final mDrawingHandler:Lcom/android/inputmethod/keyboard/KeyboardView$DrawingHandler;

.field private final mFontMetrics:Landroid/graphics/Paint$FontMetrics;

.field private mInvalidateAllKeys:Z

.field private final mInvalidatedKeys:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/android/inputmethod/keyboard/Key;",
            ">;"
        }
    .end annotation
.end field

.field private final mInvalidatedKeysRect:Landroid/graphics/Rect;

.field protected final mKeyDrawParams:Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;

.field protected final mKeyPreviewDrawParams:Lcom/android/inputmethod/keyboard/KeyboardView$KeyPreviewDrawParams;

.field private final mKeyPreviewLayoutId:I

.field private mKeyboard:Lcom/android/inputmethod/keyboard/Keyboard;

.field protected final mMoreKeysLayout:I

.field private mNeedsToDimEntireKeyboard:Z

.field private final mPaint:Landroid/graphics/Paint;

.field private mPreviewPlacer:Landroid/view/ViewGroup;

.field private mShowKeyPreviewPopup:Z

.field private final mTextBounds:Landroid/graphics/Rect;

.field protected final mVerticalCorrection:F


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v3, [I

    const v1, 0x101023c

    aput v1, v0, v2

    sput-object v0, Lcom/android/inputmethod/keyboard/KeyboardView;->LONG_PRESSABLE_STATE_SET:[I

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/inputmethod/keyboard/KeyboardView;->sTextHeightCache:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/inputmethod/keyboard/KeyboardView;->sTextWidthCache:Ljava/util/HashMap;

    new-array v0, v3, [C

    const/16 v1, 0x4d

    aput-char v1, v0, v2

    sput-object v0, Lcom/android/inputmethod/keyboard/KeyboardView;->KEY_LABEL_REFERENCE_CHAR:[C

    new-array v0, v3, [C

    const/16 v1, 0x38

    aput-char v1, v0, v2

    sput-object v0, Lcom/android/inputmethod/keyboard/KeyboardView;->KEY_NUMERIC_HINT_LABEL_REFERENCE_CHAR:[C

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const v0, 0x7f010002

    invoke-direct {p0, p1, p2, v0}, Lcom/android/inputmethod/keyboard/KeyboardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-boolean v4, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mShowKeyPreviewPopup:Z

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mInvalidatedKeys:Ljava/util/HashSet;

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mInvalidatedKeysRect:Landroid/graphics/Rect;

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/Paint$FontMetrics;

    invoke-direct {v1}, Landroid/graphics/Paint$FontMetrics;-><init>()V

    iput-object v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mFontMetrics:Landroid/graphics/Paint$FontMetrics;

    new-instance v1, Lcom/android/inputmethod/keyboard/KeyboardView$DrawingHandler;

    invoke-direct {v1, p0}, Lcom/android/inputmethod/keyboard/KeyboardView$DrawingHandler;-><init>(Lcom/android/inputmethod/keyboard/KeyboardView;)V

    iput-object v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mDrawingHandler:Lcom/android/inputmethod/keyboard/KeyboardView$DrawingHandler;

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mTextBounds:Landroid/graphics/Rect;

    sget-object v1, Lcom/android/inputmethod/latin/R$styleable;->KeyboardView:[I

    const v2, 0x7f0e0004

    invoke-virtual {p1, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    new-instance v1, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;

    invoke-direct {v1, v0}, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;-><init>(Landroid/content/res/TypedArray;)V

    iput-object v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyDrawParams:Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;

    new-instance v1, Lcom/android/inputmethod/keyboard/KeyboardView$KeyPreviewDrawParams;

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyDrawParams:Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;

    invoke-direct {v1, v0, v2}, Lcom/android/inputmethod/keyboard/KeyboardView$KeyPreviewDrawParams;-><init>(Landroid/content/res/TypedArray;Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;)V

    iput-object v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyPreviewDrawParams:Lcom/android/inputmethod/keyboard/KeyboardView$KeyPreviewDrawParams;

    const/16 v1, 0x14

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyPreviewLayoutId:I

    iget v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyPreviewLayoutId:I

    if-nez v1, :cond_0

    iput-boolean v3, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mShowKeyPreviewPopup:Z

    :cond_0
    const/16 v1, 0x1d

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mVerticalCorrection:F

    const/16 v1, 0x1e

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mMoreKeysLayout:I

    const/16 v1, 0x21

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mBackgroundDimAlpha:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyPreviewDrawParams:Lcom/android/inputmethod/keyboard/KeyboardView$KeyPreviewDrawParams;

    iget v1, v1, Lcom/android/inputmethod/keyboard/KeyboardView$KeyPreviewDrawParams;->mLingerTimeout:I

    iput v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mDelayAfterPreview:I

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    return-void
.end method

.method private addKeyPreview(Landroid/widget/TextView;)V
    .locals 4
    .param p1    # Landroid/widget/TextView;

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mPreviewPlacer:Landroid/view/ViewGroup;

    if-nez v1, :cond_0

    new-instance v1, Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mPreviewPlacer:Landroid/view/ViewGroup;

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->getRootView()Landroid/view/View;

    move-result-object v1

    const v2, 0x1020002

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mPreviewPlacer:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_0
    iget-object v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mPreviewPlacer:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mPreviewPlacer:Landroid/view/ViewGroup;

    invoke-static {v2, v3, v3}, Lcom/android/inputmethod/keyboard/ViewLayoutUtils;->newLayoutParam(Landroid/view/ViewGroup;II)Landroid/view/ViewGroup$MarginLayoutParams;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private static drawDimRectangle(Landroid/graphics/Canvas;Landroid/graphics/Rect;ILandroid/graphics/Paint;)V
    .locals 1
    .param p0    # Landroid/graphics/Canvas;
    .param p1    # Landroid/graphics/Rect;
    .param p2    # I
    .param p3    # Landroid/graphics/Paint;

    const/high16 v0, -0x1000000

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {p3, p2}, Landroid/graphics/Paint;->setAlpha(I)V

    invoke-virtual {p0, p1, p3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    return-void
.end method

.method private static drawHorizontalLine(Landroid/graphics/Canvas;FFILandroid/graphics/Paint;)V
    .locals 6
    .param p0    # Landroid/graphics/Canvas;
    .param p1    # F
    .param p2    # F
    .param p3    # I
    .param p4    # Landroid/graphics/Paint;

    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p4, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    const/high16 v0, 0x3f800000

    invoke-virtual {p4, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    invoke-virtual {p4, p3}, Landroid/graphics/Paint;->setColor(I)V

    const/4 v1, 0x0

    move-object v0, p0

    move v2, p1

    move v3, p2

    move v4, p1

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method protected static drawIcon(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;IIII)V
    .locals 3
    .param p0    # Landroid/graphics/Canvas;
    .param p1    # Landroid/graphics/drawable/Drawable;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v2, 0x0

    int-to-float v0, p2

    int-to-float v1, p3

    invoke-virtual {p0, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {p1, v2, v2, p4, p5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, p2

    int-to-float v0, v0

    neg-int v1, p3

    int-to-float v1, v1

    invoke-virtual {p0, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    return-void
.end method

.method private static drawRectangle(Landroid/graphics/Canvas;FFFFILandroid/graphics/Paint;)V
    .locals 6
    .param p0    # Landroid/graphics/Canvas;
    .param p1    # F
    .param p2    # F
    .param p3    # F
    .param p4    # F
    .param p5    # I
    .param p6    # Landroid/graphics/Paint;

    const/4 v1, 0x0

    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p6, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    const/high16 v0, 0x3f800000

    invoke-virtual {p6, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    invoke-virtual {p6, p5}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {p0, p1, p2}, Landroid/graphics/Canvas;->translate(FF)V

    move-object v0, p0

    move v2, v1

    move v3, p3

    move v4, p4

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    neg-float v0, p1

    neg-float v1, p2

    invoke-virtual {p0, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    return-void
.end method

.method private static drawVerticalLine(Landroid/graphics/Canvas;FFILandroid/graphics/Paint;)V
    .locals 6
    .param p0    # Landroid/graphics/Canvas;
    .param p1    # F
    .param p2    # F
    .param p3    # I
    .param p4    # Landroid/graphics/Paint;

    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p4, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    const/high16 v0, 0x3f800000

    invoke-virtual {p4, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    invoke-virtual {p4, p3}, Landroid/graphics/Paint;->setColor(I)V

    const/4 v2, 0x0

    move-object v0, p0

    move v1, p1

    move v3, p1

    move v4, p2

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method private static getCharGeometryCacheKey(CLandroid/graphics/Paint;)I
    .locals 4
    .param p0    # C
    .param p1    # Landroid/graphics/Paint;

    invoke-virtual {p1}, Landroid/graphics/Paint;->getTextSize()F

    move-result v3

    float-to-int v2, v3

    invoke-virtual {p1}, Landroid/graphics/Paint;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    shl-int/lit8 v0, p0, 0xf

    sget-object v3, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    if-ne v1, v3, :cond_0

    add-int v3, v0, v2

    :goto_0
    return v3

    :cond_0
    sget-object v3, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    if-ne v1, v3, :cond_1

    add-int v3, v0, v2

    add-int/lit16 v3, v3, 0x1000

    goto :goto_0

    :cond_1
    sget-object v3, Landroid/graphics/Typeface;->MONOSPACE:Landroid/graphics/Typeface;

    if-ne v1, v3, :cond_2

    add-int v3, v0, v2

    add-int/lit16 v3, v3, 0x2000

    goto :goto_0

    :cond_2
    add-int v3, v0, v2

    goto :goto_0
.end method

.method private getCharHeight([CLandroid/graphics/Paint;)F
    .locals 6
    .param p1    # [C
    .param p2    # Landroid/graphics/Paint;

    const/4 v5, 0x0

    aget-char v3, p1, v5

    invoke-static {v3, p2}, Lcom/android/inputmethod/keyboard/KeyboardView;->getCharGeometryCacheKey(CLandroid/graphics/Paint;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v3, Lcom/android/inputmethod/keyboard/KeyboardView;->sTextHeightCache:Ljava/util/HashMap;

    invoke-virtual {v3, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mTextBounds:Landroid/graphics/Rect;

    invoke-virtual {p2, p1, v5, v3, v4}, Landroid/graphics/Paint;->getTextBounds([CIILandroid/graphics/Rect;)V

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mTextBounds:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v1, v3

    sget-object v3, Lcom/android/inputmethod/keyboard/KeyboardView;->sTextHeightCache:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private getCharWidth([CLandroid/graphics/Paint;)F
    .locals 6
    .param p1    # [C
    .param p2    # Landroid/graphics/Paint;

    const/4 v5, 0x0

    aget-char v3, p1, v5

    invoke-static {v3, p2}, Lcom/android/inputmethod/keyboard/KeyboardView;->getCharGeometryCacheKey(CLandroid/graphics/Paint;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v3, Lcom/android/inputmethod/keyboard/KeyboardView;->sTextWidthCache:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v2

    :goto_0
    return v2

    :cond_0
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mTextBounds:Landroid/graphics/Rect;

    invoke-virtual {p2, p1, v5, v3, v4}, Landroid/graphics/Paint;->getTextBounds([CIILandroid/graphics/Rect;)V

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mTextBounds:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v2, v3

    sget-object v3, Lcom/android/inputmethod/keyboard/KeyboardView;->sTextWidthCache:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method static getRatio(Landroid/content/res/TypedArray;I)F
    .locals 2
    .param p0    # Landroid/content/res/TypedArray;
    .param p1    # I

    const/16 v1, 0x3e8

    const/high16 v0, 0x3f800000

    invoke-virtual {p0, p1, v1, v1, v0}, Landroid/content/res/TypedArray;->getFraction(IIIF)F

    move-result v0

    const/high16 v1, 0x447a0000

    div-float/2addr v0, v1

    return v0
.end method

.method private onBufferDraw()V
    .locals 14

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->getWidth()I

    move-result v8

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->getHeight()I

    move-result v2

    if-eqz v8, :cond_0

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v11, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mBuffer:Landroid/graphics/Bitmap;

    if-eqz v11, :cond_2

    iget-object v11, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mBuffer:Landroid/graphics/Bitmap;

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    if-ne v11, v8, :cond_2

    iget-object v11, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mBuffer:Landroid/graphics/Bitmap;

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    if-eq v11, v2, :cond_4

    :cond_2
    iget-object v11, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mBuffer:Landroid/graphics/Bitmap;

    if-eqz v11, :cond_3

    iget-object v11, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mBuffer:Landroid/graphics/Bitmap;

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->recycle()V

    :cond_3
    sget-object v11, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v8, v2, v11}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v11

    iput-object v11, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mBuffer:Landroid/graphics/Bitmap;

    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mInvalidateAllKeys:Z

    iget-object v11, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mCanvas:Landroid/graphics/Canvas;

    if-eqz v11, :cond_6

    iget-object v11, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mCanvas:Landroid/graphics/Canvas;

    iget-object v12, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mBuffer:Landroid/graphics/Bitmap;

    invoke-virtual {v11, v12}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    :cond_4
    :goto_1
    iget-object v11, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyboard:Lcom/android/inputmethod/keyboard/Keyboard;

    if-eqz v11, :cond_0

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mCanvas:Landroid/graphics/Canvas;

    iget-object v6, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mPaint:Landroid/graphics/Paint;

    iget-object v7, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyDrawParams:Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;

    iget-boolean v11, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mInvalidateAllKeys:Z

    if-nez v11, :cond_5

    iget-object v11, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mInvalidatedKeys:Ljava/util/HashSet;

    invoke-virtual {v11}, Ljava/util/HashSet;->isEmpty()Z

    move-result v11

    if-eqz v11, :cond_9

    :cond_5
    iget-object v11, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mInvalidatedKeysRect:Landroid/graphics/Rect;

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13, v8, v2}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v11, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mInvalidatedKeysRect:Landroid/graphics/Rect;

    sget-object v12, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    invoke-virtual {v1, v11, v12}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z

    const/high16 v11, -0x1000000

    sget-object v12, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v11, v12}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    iget-object v11, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyboard:Lcom/android/inputmethod/keyboard/Keyboard;

    iget-object v0, v11, Lcom/android/inputmethod/keyboard/Keyboard;->mKeys:[Lcom/android/inputmethod/keyboard/Key;

    array-length v5, v0

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v5, :cond_7

    aget-object v4, v0, v3

    invoke-direct {p0, v4, v1, v6, v7}, Lcom/android/inputmethod/keyboard/KeyboardView;->onDrawKey(Lcom/android/inputmethod/keyboard/Key;Landroid/graphics/Canvas;Landroid/graphics/Paint;Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_6
    new-instance v11, Landroid/graphics/Canvas;

    iget-object v12, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mBuffer:Landroid/graphics/Bitmap;

    invoke-direct {v11, v12}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v11, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mCanvas:Landroid/graphics/Canvas;

    goto :goto_1

    :cond_7
    iget-boolean v11, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mNeedsToDimEntireKeyboard:Z

    if-eqz v11, :cond_8

    iget-object v11, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mInvalidatedKeysRect:Landroid/graphics/Rect;

    iget v12, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mBackgroundDimAlpha:I

    invoke-static {v1, v11, v12, v6}, Lcom/android/inputmethod/keyboard/KeyboardView;->drawDimRectangle(Landroid/graphics/Canvas;Landroid/graphics/Rect;ILandroid/graphics/Paint;)V

    :cond_8
    iget-object v11, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mInvalidatedKeys:Ljava/util/HashSet;

    invoke-virtual {v11}, Ljava/util/HashSet;->clear()V

    iget-object v11, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mInvalidatedKeysRect:Landroid/graphics/Rect;

    invoke-virtual {v11}, Landroid/graphics/Rect;->setEmpty()V

    const/4 v11, 0x0

    iput-boolean v11, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mInvalidateAllKeys:Z

    goto/16 :goto_0

    :cond_9
    iget-object v11, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mInvalidatedKeys:Ljava/util/HashSet;

    invoke-virtual {v11}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_a
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/inputmethod/keyboard/Key;

    iget-object v11, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyboard:Lcom/android/inputmethod/keyboard/Keyboard;

    invoke-virtual {v11, v4}, Lcom/android/inputmethod/keyboard/Keyboard;->hasKey(Lcom/android/inputmethod/keyboard/Key;)Z

    move-result v11

    if-eqz v11, :cond_a

    iget v11, v4, Lcom/android/inputmethod/keyboard/Key;->mX:I

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->getPaddingLeft()I

    move-result v12

    add-int v9, v11, v12

    iget v11, v4, Lcom/android/inputmethod/keyboard/Key;->mY:I

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->getPaddingTop()I

    move-result v12

    add-int v10, v11, v12

    iget-object v11, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mInvalidatedKeysRect:Landroid/graphics/Rect;

    iget v12, v4, Lcom/android/inputmethod/keyboard/Key;->mWidth:I

    add-int/2addr v12, v9

    iget v13, v4, Lcom/android/inputmethod/keyboard/Key;->mHeight:I

    add-int/2addr v13, v10

    invoke-virtual {v11, v9, v10, v12, v13}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v11, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mInvalidatedKeysRect:Landroid/graphics/Rect;

    sget-object v12, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    invoke-virtual {v1, v11, v12}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z

    const/high16 v11, -0x1000000

    sget-object v12, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v11, v12}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-direct {p0, v4, v1, v6, v7}, Lcom/android/inputmethod/keyboard/KeyboardView;->onDrawKey(Lcom/android/inputmethod/keyboard/Key;Landroid/graphics/Canvas;Landroid/graphics/Paint;Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;)V

    iget-boolean v11, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mNeedsToDimEntireKeyboard:Z

    if-eqz v11, :cond_a

    iget-object v11, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mInvalidatedKeysRect:Landroid/graphics/Rect;

    iget v12, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mBackgroundDimAlpha:I

    invoke-static {v1, v11, v12, v6}, Lcom/android/inputmethod/keyboard/KeyboardView;->drawDimRectangle(Landroid/graphics/Canvas;Landroid/graphics/Rect;ILandroid/graphics/Paint;)V

    goto :goto_3
.end method

.method private onDrawKey(Lcom/android/inputmethod/keyboard/Key;Landroid/graphics/Canvas;Landroid/graphics/Paint;Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;)V
    .locals 4
    .param p1    # Lcom/android/inputmethod/keyboard/Key;
    .param p2    # Landroid/graphics/Canvas;
    .param p3    # Landroid/graphics/Paint;
    .param p4    # Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;

    iget v2, p1, Lcom/android/inputmethod/keyboard/Key;->mX:I

    iget v3, p1, Lcom/android/inputmethod/keyboard/Key;->mVisualInsetsLeft:I

    add-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->getPaddingLeft()I

    move-result v3

    add-int v0, v2, v3

    iget v2, p1, Lcom/android/inputmethod/keyboard/Key;->mY:I

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->getPaddingTop()I

    move-result v3

    add-int v1, v2, v3

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p2, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    const/16 v2, 0xff

    iput v2, p4, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mAnimAlpha:I

    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/Key;->isSpacer()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0, p1, p2, p4}, Lcom/android/inputmethod/keyboard/KeyboardView;->onDrawKeyBackground(Lcom/android/inputmethod/keyboard/Key;Landroid/graphics/Canvas;Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;)V

    :cond_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/android/inputmethod/keyboard/KeyboardView;->onDrawKeyTopVisuals(Lcom/android/inputmethod/keyboard/Key;Landroid/graphics/Canvas;Landroid/graphics/Paint;Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;)V

    neg-int v2, v0

    int-to-float v2, v2

    neg-int v3, v1

    int-to-float v3, v3

    invoke-virtual {p2, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    return-void
.end method


# virtual methods
.method public cancelAllMessages()V
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mDrawingHandler:Lcom/android/inputmethod/keyboard/KeyboardView$DrawingHandler;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/KeyboardView$DrawingHandler;->cancelAllMessages()V

    return-void
.end method

.method public closing()V
    .locals 1

    invoke-static {}, Lcom/android/inputmethod/keyboard/PointerTracker;->dismissAllKeyPreviews()V

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->cancelAllMessages()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mInvalidateAllKeys:Z

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->requestLayout()V

    return-void
.end method

.method public dimEntireKeyboard(Z)V
    .locals 2
    .param p1    # Z

    iget-boolean v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mNeedsToDimEntireKeyboard:Z

    if-eq v1, p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean p1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mNeedsToDimEntireKeyboard:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->invalidateAllKeys()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dismissKeyPreview(Lcom/android/inputmethod/keyboard/PointerTracker;)V
    .locals 3
    .param p1    # Lcom/android/inputmethod/keyboard/PointerTracker;

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mDrawingHandler:Lcom/android/inputmethod/keyboard/KeyboardView$DrawingHandler;

    iget v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mDelayAfterPreview:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2, p1}, Lcom/android/inputmethod/keyboard/KeyboardView$DrawingHandler;->dismissKeyPreview(JLcom/android/inputmethod/keyboard/PointerTracker;)V

    return-void
.end method

.method public dismissMoreKeysPanel()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected drawKeyPopupHint(Lcom/android/inputmethod/keyboard/Key;Landroid/graphics/Canvas;Landroid/graphics/Paint;Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;)V
    .locals 9
    .param p1    # Lcom/android/inputmethod/keyboard/Key;
    .param p2    # Landroid/graphics/Canvas;
    .param p3    # Landroid/graphics/Paint;
    .param p4    # Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;

    const v8, -0x3f7f8000

    iget v5, p1, Lcom/android/inputmethod/keyboard/Key;->mWidth:I

    iget v6, p1, Lcom/android/inputmethod/keyboard/Key;->mVisualInsetsLeft:I

    sub-int/2addr v5, v6

    iget v6, p1, Lcom/android/inputmethod/keyboard/Key;->mVisualInsetsRight:I

    sub-int v3, v5, v6

    iget v2, p1, Lcom/android/inputmethod/keyboard/Key;->mHeight:I

    iget-object v5, p4, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyTextStyle:Landroid/graphics/Typeface;

    invoke-virtual {p3, v5}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget v5, p4, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyHintLetterSize:I

    int-to-float v5, v5

    invoke-virtual {p3, v5}, Landroid/graphics/Paint;->setTextSize(F)V

    iget v5, p4, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyHintLabelColor:I

    invoke-virtual {p3, v5}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v5, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {p3, v5}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    int-to-float v5, v3

    iget v6, p4, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyHintLetterPadding:F

    sub-float/2addr v5, v6

    sget-object v6, Lcom/android/inputmethod/keyboard/KeyboardView;->KEY_LABEL_REFERENCE_CHAR:[C

    invoke-direct {p0, v6, p3}, Lcom/android/inputmethod/keyboard/KeyboardView;->getCharWidth([CLandroid/graphics/Paint;)F

    move-result v6

    const/high16 v7, 0x40000000

    div-float/2addr v6, v7

    sub-float v0, v5, v6

    int-to-float v5, v2

    iget v6, p4, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyPopupHintLetterPadding:F

    sub-float v1, v5, v6

    const-string v5, "\u2026"

    invoke-virtual {p2, v5, v0, v1, p3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    sget-boolean v5, Lcom/android/inputmethod/latin/LatinImeLogger;->sVISUALDEBUG:Z

    if-eqz v5, :cond_0

    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    float-to-int v5, v1

    int-to-float v5, v5

    int-to-float v6, v3

    invoke-static {p2, v5, v6, v8, v4}, Lcom/android/inputmethod/keyboard/KeyboardView;->drawHorizontalLine(Landroid/graphics/Canvas;FFILandroid/graphics/Paint;)V

    float-to-int v5, v0

    int-to-float v5, v5

    int-to-float v6, v2

    invoke-static {p2, v5, v6, v8, v4}, Lcom/android/inputmethod/keyboard/KeyboardView;->drawVerticalLine(Landroid/graphics/Canvas;FFILandroid/graphics/Paint;)V

    :cond_0
    return-void
.end method

.method public getKeyboard()Lcom/android/inputmethod/keyboard/Keyboard;
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyboard:Lcom/android/inputmethod/keyboard/Keyboard;

    return-object v0
.end method

.method public getLabelWidth(Ljava/lang/String;Landroid/graphics/Paint;)F
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/graphics/Paint;

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mTextBounds:Landroid/graphics/Rect;

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mTextBounds:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method public inflateKeyPreviewText()Landroid/widget/TextView;
    .locals 4

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyPreviewLayoutId:I

    if-eqz v1, :cond_0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    iget v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyPreviewLayoutId:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public invalidateAllKeys()V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mInvalidatedKeys:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    iput-boolean v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mInvalidateAllKeys:Z

    iput-boolean v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mBufferNeedsUpdate:Z

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->invalidate()V

    return-void
.end method

.method public invalidateKey(Lcom/android/inputmethod/keyboard/Key;)V
    .locals 5
    .param p1    # Lcom/android/inputmethod/keyboard/Key;

    iget-boolean v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mInvalidateAllKeys:Z

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_0

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mInvalidatedKeys:Ljava/util/HashSet;

    invoke-virtual {v2, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget v2, p1, Lcom/android/inputmethod/keyboard/Key;->mX:I

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->getPaddingLeft()I

    move-result v3

    add-int v0, v2, v3

    iget v2, p1, Lcom/android/inputmethod/keyboard/Key;->mY:I

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->getPaddingTop()I

    move-result v3

    add-int v1, v2, v3

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mInvalidatedKeysRect:Landroid/graphics/Rect;

    iget v3, p1, Lcom/android/inputmethod/keyboard/Key;->mWidth:I

    add-int/2addr v3, v0

    iget v4, p1, Lcom/android/inputmethod/keyboard/Key;->mHeight:I

    add-int/2addr v4, v1

    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/graphics/Rect;->union(IIII)V

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mBufferNeedsUpdate:Z

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mInvalidatedKeysRect:Landroid/graphics/Rect;

    invoke-virtual {p0, v2}, Lcom/android/inputmethod/keyboard/KeyboardView;->invalidate(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public isKeyPreviewPopupEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mShowKeyPreviewPopup:Z

    return v0
.end method

.method public newDefaultLabelPaint()Landroid/graphics/Paint;
    .locals 2

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyDrawParams:Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;

    iget-object v1, v1, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyTextStyle:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyDrawParams:Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;

    iget v1, v1, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyLabelSize:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->closing()V

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mPreviewPlacer:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mPreviewPlacer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    :cond_0
    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mBuffer:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mBuffer:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mBuffer:Landroid/graphics/Bitmap;

    :cond_1
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1    # Landroid/graphics/Canvas;

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mBufferNeedsUpdate:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mBuffer:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mBufferNeedsUpdate:Z

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->onBufferDraw()V

    :cond_1
    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mBuffer:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    return-void
.end method

.method protected onDrawKeyBackground(Lcom/android/inputmethod/keyboard/Key;Landroid/graphics/Canvas;Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;)V
    .locals 15
    .param p1    # Lcom/android/inputmethod/keyboard/Key;
    .param p2    # Landroid/graphics/Canvas;
    .param p3    # Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;

    move-object/from16 v0, p1

    iget v1, v0, Lcom/android/inputmethod/keyboard/Key;->mWidth:I

    move-object/from16 v0, p1

    iget v2, v0, Lcom/android/inputmethod/keyboard/Key;->mVisualInsetsLeft:I

    sub-int/2addr v1, v2

    move-object/from16 v0, p1

    iget v2, v0, Lcom/android/inputmethod/keyboard/Key;->mVisualInsetsRight:I

    sub-int/2addr v1, v2

    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mPadding:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v2

    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mPadding:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    add-int v10, v1, v2

    move-object/from16 v0, p1

    iget v1, v0, Lcom/android/inputmethod/keyboard/Key;->mHeight:I

    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mPadding:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v2

    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mPadding:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    add-int v9, v1, v2

    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mPadding:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    neg-int v11, v1

    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mPadding:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    neg-int v12, v1

    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/keyboard/Key;->getCurrentDrawableState()[I

    move-result-object v14

    move-object/from16 v0, p3

    iget-object v8, v0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v8, v14}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v13

    iget v1, v13, Landroid/graphics/Rect;->right:I

    if-ne v10, v1, :cond_0

    iget v1, v13, Landroid/graphics/Rect;->bottom:I

    if-eq v9, v1, :cond_1

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v8, v1, v2, v10, v9}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    :cond_1
    int-to-float v1, v11

    int-to-float v2, v12

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    move-object/from16 v0, p2

    invoke-virtual {v8, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    sget-boolean v1, Lcom/android/inputmethod/latin/LatinImeLogger;->sVISUALDEBUG:Z

    if-eqz v1, :cond_2

    const/4 v2, 0x0

    const/4 v3, 0x0

    int-to-float v4, v10

    int-to-float v5, v9

    const/high16 v6, -0x7f400000

    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v1, p2

    invoke-static/range {v1 .. v7}, Lcom/android/inputmethod/keyboard/KeyboardView;->drawRectangle(Landroid/graphics/Canvas;FFFFILandroid/graphics/Paint;)V

    :cond_2
    neg-int v1, v11

    int-to-float v1, v1

    neg-int v2, v12

    int-to-float v2, v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    return-void
.end method

.method protected onDrawKeyTopVisuals(Lcom/android/inputmethod/keyboard/Key;Landroid/graphics/Canvas;Landroid/graphics/Paint;Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;)V
    .locals 40
    .param p1    # Lcom/android/inputmethod/keyboard/Key;
    .param p2    # Landroid/graphics/Canvas;
    .param p3    # Landroid/graphics/Paint;
    .param p4    # Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;

    move-object/from16 v0, p1

    iget v3, v0, Lcom/android/inputmethod/keyboard/Key;->mWidth:I

    move-object/from16 v0, p1

    iget v4, v0, Lcom/android/inputmethod/keyboard/Key;->mVisualInsetsLeft:I

    sub-int/2addr v3, v4

    move-object/from16 v0, p1

    iget v4, v0, Lcom/android/inputmethod/keyboard/Key;->mVisualInsetsRight:I

    sub-int v34, v3, v4

    move-object/from16 v0, p1

    iget v0, v0, Lcom/android/inputmethod/keyboard/Key;->mHeight:I

    move/from16 v33, v0

    move/from16 v0, v34

    int-to-float v3, v0

    const/high16 v4, 0x3f000000

    mul-float v29, v3, v4

    move/from16 v0, v33

    int-to-float v3, v0

    const/high16 v4, 0x3f000000

    mul-float v30, v3, v4

    sget-boolean v3, Lcom/android/inputmethod/latin/LatinImeLogger;->sVISUALDEBUG:Z

    if-eqz v3, :cond_0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move/from16 v0, v34

    int-to-float v6, v0

    move/from16 v0, v33

    int-to-float v7, v0

    const v8, -0x7fffff40

    new-instance v9, Landroid/graphics/Paint;

    invoke-direct {v9}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v3, p2

    invoke-static/range {v3 .. v9}, Lcom/android/inputmethod/keyboard/KeyboardView;->drawRectangle(Landroid/graphics/Canvas;FFFFILandroid/graphics/Paint;)V

    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyboard:Lcom/android/inputmethod/keyboard/Keyboard;

    iget-object v3, v3, Lcom/android/inputmethod/keyboard/Keyboard;->mIconsSet:Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;

    move-object/from16 v0, p4

    iget v4, v0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mAnimAlpha:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Lcom/android/inputmethod/keyboard/Key;->getIcon(Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;I)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    move/from16 v7, v29

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/inputmethod/keyboard/Key;->mLabel:Ljava/lang/String;

    if-eqz v3, :cond_3

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/Key;->mLabel:Ljava/lang/String;

    move-object/from16 v35, v0

    move-object/from16 v0, p4

    iget-object v3, v0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyTextStyle:Landroid/graphics/Typeface;

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/android/inputmethod/keyboard/Key;->selectTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-object/from16 v0, p4

    iget v4, v0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyLetterSize:I

    move-object/from16 v0, p4

    iget v5, v0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyLargeLetterSize:I

    move-object/from16 v0, p4

    iget v6, v0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyLabelSize:I

    move-object/from16 v0, p4

    iget v7, v0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyLargeLabelSize:I

    move-object/from16 v0, p4

    iget v8, v0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyHintLabelSize:I

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v8}, Lcom/android/inputmethod/keyboard/Key;->selectTextSize(IIIII)I

    move-result v38

    move/from16 v0, v38

    int-to-float v3, v0

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    sget-object v3, Lcom/android/inputmethod/keyboard/KeyboardView;->KEY_LABEL_REFERENCE_CHAR:[C

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v3, v1}, Lcom/android/inputmethod/keyboard/KeyboardView;->getCharHeight([CLandroid/graphics/Paint;)F

    move-result v36

    sget-object v3, Lcom/android/inputmethod/keyboard/KeyboardView;->KEY_LABEL_REFERENCE_CHAR:[C

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v3, v1}, Lcom/android/inputmethod/keyboard/KeyboardView;->getCharWidth([CLandroid/graphics/Paint;)F

    move-result v37

    const/high16 v3, 0x40000000

    div-float v3, v36, v3

    add-float v8, v30, v3

    const/16 v39, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/keyboard/Key;->isAlignLeft()Z

    move-result v3

    if-eqz v3, :cond_7

    move-object/from16 v0, p4

    iget v3, v0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyLabelHorizontalPadding:F

    float-to-int v3, v3

    int-to-float v7, v3

    sget-object v3, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/keyboard/Key;->needsXScale()Z

    move-result v3

    if-eqz v3, :cond_1

    const/high16 v3, 0x3f800000

    move/from16 v0, v34

    int-to-float v4, v0

    const v5, 0x3f666666

    mul-float/2addr v4, v5

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lcom/android/inputmethod/keyboard/KeyboardView;->getLabelWidth(Ljava/lang/String;Landroid/graphics/Paint;)F

    move-result v5

    div-float/2addr v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setTextScaleX(F)V

    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/keyboard/Key;->isShiftedLetterActivated()Z

    move-result v3

    if-eqz v3, :cond_c

    move-object/from16 v0, p4

    iget v3, v0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyTextInactivatedColor:I

    :goto_1
    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/keyboard/Key;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_d

    move-object/from16 v0, p4

    iget v3, v0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mShadowRadius:F

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p4

    iget v6, v0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mShadowColor:I

    move-object/from16 v0, p3

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    :goto_2
    move-object/from16 v0, p4

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->blendAlpha(Landroid/graphics/Paint;)V

    const/4 v5, 0x0

    invoke-virtual/range {v35 .. v35}, Ljava/lang/String;->length()I

    move-result v6

    move-object/from16 v3, p2

    move-object/from16 v4, v35

    move-object/from16 v9, p3

    invoke-virtual/range {v3 .. v9}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;IIFFLandroid/graphics/Paint;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    const/high16 v3, 0x3f800000

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setTextScaleX(F)V

    if-eqz v10, :cond_2

    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v13

    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v14

    sub-int v3, v33, v14

    div-int/lit8 v12, v3, 0x2

    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/keyboard/Key;->hasLabelWithIconLeft()Z

    move-result v3

    if-eqz v3, :cond_e

    const/high16 v3, 0x40000000

    div-float v3, v39, v3

    sub-float v3, v29, v3

    float-to-int v11, v3

    move-object/from16 v9, p2

    invoke-static/range {v9 .. v14}, Lcom/android/inputmethod/keyboard/KeyboardView;->drawIcon(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;IIII)V

    :cond_2
    :goto_3
    sget-boolean v3, Lcom/android/inputmethod/latin/LatinImeLogger;->sVISUALDEBUG:Z

    if-eqz v3, :cond_3

    new-instance v27, Landroid/graphics/Paint;

    invoke-direct/range {v27 .. v27}, Landroid/graphics/Paint;-><init>()V

    move/from16 v0, v34

    int-to-float v3, v0

    const v4, -0x3fff8000

    move-object/from16 v0, p2

    move-object/from16 v1, v27

    invoke-static {v0, v8, v3, v4, v1}, Lcom/android/inputmethod/keyboard/KeyboardView;->drawHorizontalLine(Landroid/graphics/Canvas;FFILandroid/graphics/Paint;)V

    move/from16 v0, v33

    int-to-float v3, v0

    const v4, -0x3f7fff80

    move-object/from16 v0, p2

    move-object/from16 v1, v27

    invoke-static {v0, v7, v3, v4, v1}, Lcom/android/inputmethod/keyboard/KeyboardView;->drawVerticalLine(Landroid/graphics/Canvas;FFILandroid/graphics/Paint;)V

    :cond_3
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/inputmethod/keyboard/Key;->mHintLabel:Ljava/lang/String;

    if-eqz v3, :cond_4

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/Key;->mHintLabel:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/keyboard/Key;->hasHintLabel()Z

    move-result v3

    if-eqz v3, :cond_f

    move-object/from16 v0, p4

    iget v0, v0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyHintLabelColor:I

    move/from16 v31, v0

    move-object/from16 v0, p4

    iget v0, v0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyHintLabelSize:I

    move/from16 v32, v0

    sget-object v3, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    :goto_4
    move-object/from16 v0, p3

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    move-object/from16 v0, p4

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->blendAlpha(Landroid/graphics/Paint;)V

    move/from16 v0, v32

    int-to-float v3, v0

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/keyboard/Key;->hasHintLabel()Z

    move-result v3

    if-eqz v3, :cond_12

    sget-object v3, Lcom/android/inputmethod/keyboard/KeyboardView;->KEY_LABEL_REFERENCE_CHAR:[C

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v3, v1}, Lcom/android/inputmethod/keyboard/KeyboardView;->getCharWidth([CLandroid/graphics/Paint;)F

    move-result v3

    const/high16 v4, 0x40000000

    mul-float/2addr v3, v4

    add-float v19, v7, v3

    sget-object v3, Lcom/android/inputmethod/keyboard/KeyboardView;->KEY_LABEL_REFERENCE_CHAR:[C

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v3, v1}, Lcom/android/inputmethod/keyboard/KeyboardView;->getCharHeight([CLandroid/graphics/Paint;)F

    move-result v3

    const/high16 v4, 0x40000000

    div-float/2addr v3, v4

    add-float v20, v30, v3

    sget-object v3, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    :goto_5
    const/16 v17, 0x0

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v18

    move-object/from16 v15, p2

    move-object/from16 v21, p3

    invoke-virtual/range {v15 .. v21}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;IIFFLandroid/graphics/Paint;)V

    sget-boolean v3, Lcom/android/inputmethod/latin/LatinImeLogger;->sVISUALDEBUG:Z

    if-eqz v3, :cond_4

    new-instance v27, Landroid/graphics/Paint;

    invoke-direct/range {v27 .. v27}, Landroid/graphics/Paint;-><init>()V

    move/from16 v0, v20

    float-to-int v3, v0

    int-to-float v3, v3

    move/from16 v0, v34

    int-to-float v4, v0

    const v5, -0x3f7f8000

    move-object/from16 v0, p2

    move-object/from16 v1, v27

    invoke-static {v0, v3, v4, v5, v1}, Lcom/android/inputmethod/keyboard/KeyboardView;->drawHorizontalLine(Landroid/graphics/Canvas;FFILandroid/graphics/Paint;)V

    move/from16 v0, v19

    float-to-int v3, v0

    int-to-float v3, v3

    move/from16 v0, v33

    int-to-float v4, v0

    const v5, -0x3f7f8000

    move-object/from16 v0, p2

    move-object/from16 v1, v27

    invoke-static {v0, v3, v4, v5, v1}, Lcom/android/inputmethod/keyboard/KeyboardView;->drawVerticalLine(Landroid/graphics/Canvas;FFILandroid/graphics/Paint;)V

    :cond_4
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/inputmethod/keyboard/Key;->mLabel:Ljava/lang/String;

    if-nez v3, :cond_5

    if-eqz v10, :cond_5

    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v13

    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v14

    sub-int v3, v33, v14

    div-int/lit8 v12, v3, 0x2

    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/keyboard/Key;->isAlignLeft()Z

    move-result v3

    if-eqz v3, :cond_14

    move-object/from16 v0, p4

    iget v3, v0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyLabelHorizontalPadding:F

    float-to-int v11, v3

    move/from16 v28, v11

    :goto_6
    move-object/from16 v9, p2

    invoke-static/range {v9 .. v14}, Lcom/android/inputmethod/keyboard/KeyboardView;->drawIcon(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;IIII)V

    sget-boolean v3, Lcom/android/inputmethod/latin/LatinImeLogger;->sVISUALDEBUG:Z

    if-eqz v3, :cond_5

    new-instance v27, Landroid/graphics/Paint;

    invoke-direct/range {v27 .. v27}, Landroid/graphics/Paint;-><init>()V

    move/from16 v0, v28

    int-to-float v3, v0

    move/from16 v0, v33

    int-to-float v4, v0

    const v5, -0x3f7fff80

    move-object/from16 v0, p2

    move-object/from16 v1, v27

    invoke-static {v0, v3, v4, v5, v1}, Lcom/android/inputmethod/keyboard/KeyboardView;->drawVerticalLine(Landroid/graphics/Canvas;FFILandroid/graphics/Paint;)V

    int-to-float v0, v11

    move/from16 v22, v0

    int-to-float v0, v12

    move/from16 v23, v0

    int-to-float v0, v13

    move/from16 v24, v0

    int-to-float v0, v14

    move/from16 v25, v0

    const/high16 v26, -0x7f400000

    move-object/from16 v21, p2

    invoke-static/range {v21 .. v27}, Lcom/android/inputmethod/keyboard/KeyboardView;->drawRectangle(Landroid/graphics/Canvas;FFFFILandroid/graphics/Paint;)V

    :cond_5
    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/keyboard/Key;->hasPopupHint()Z

    move-result v3

    if-eqz v3, :cond_6

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/inputmethod/keyboard/Key;->mMoreKeys:[Lcom/android/inputmethod/keyboard/internal/KeySpecParser$MoreKeySpec;

    if-eqz v3, :cond_6

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/inputmethod/keyboard/Key;->mMoreKeys:[Lcom/android/inputmethod/keyboard/internal/KeySpecParser$MoreKeySpec;

    array-length v3, v3

    if-lez v3, :cond_6

    invoke-virtual/range {p0 .. p4}, Lcom/android/inputmethod/keyboard/KeyboardView;->drawKeyPopupHint(Lcom/android/inputmethod/keyboard/Key;Landroid/graphics/Canvas;Landroid/graphics/Paint;Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;)V

    :cond_6
    return-void

    :cond_7
    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/keyboard/Key;->isAlignRight()Z

    move-result v3

    if-eqz v3, :cond_8

    move-object/from16 v0, p4

    iget v3, v0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyLabelHorizontalPadding:F

    float-to-int v3, v3

    sub-int v3, v34, v3

    int-to-float v7, v3

    sget-object v3, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    goto/16 :goto_0

    :cond_8
    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/keyboard/Key;->isAlignLeftOfCenter()Z

    move-result v3

    if-eqz v3, :cond_9

    const/high16 v3, 0x40e00000

    mul-float v3, v3, v37

    const/high16 v4, 0x40800000

    div-float/2addr v3, v4

    sub-float v7, v29, v3

    sget-object v3, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    goto/16 :goto_0

    :cond_9
    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/keyboard/Key;->hasLabelWithIconLeft()Z

    move-result v3

    if-eqz v3, :cond_a

    if-eqz v10, :cond_a

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lcom/android/inputmethod/keyboard/KeyboardView;->getLabelWidth(Ljava/lang/String;Landroid/graphics/Paint;)F

    move-result v3

    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    const v4, 0x3d4ccccd

    move/from16 v0, v34

    int-to-float v5, v0

    mul-float/2addr v4, v5

    add-float v39, v3, v4

    const/high16 v3, 0x40000000

    div-float v3, v39, v3

    add-float v7, v29, v3

    sget-object v3, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    goto/16 :goto_0

    :cond_a
    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/keyboard/Key;->hasLabelWithIconRight()Z

    move-result v3

    if-eqz v3, :cond_b

    if-eqz v10, :cond_b

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lcom/android/inputmethod/keyboard/KeyboardView;->getLabelWidth(Ljava/lang/String;Landroid/graphics/Paint;)F

    move-result v3

    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    const v4, 0x3d4ccccd

    move/from16 v0, v34

    int-to-float v5, v0

    mul-float/2addr v4, v5

    add-float v39, v3, v4

    const/high16 v3, 0x40000000

    div-float v3, v39, v3

    sub-float v7, v29, v3

    sget-object v3, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    goto/16 :goto_0

    :cond_b
    move/from16 v7, v29

    sget-object v3, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    goto/16 :goto_0

    :cond_c
    move-object/from16 v0, p4

    iget v3, v0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyTextColor:I

    goto/16 :goto_1

    :cond_d
    const/4 v3, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    goto/16 :goto_2

    :cond_e
    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/keyboard/Key;->hasLabelWithIconRight()Z

    move-result v3

    if-eqz v3, :cond_2

    const/high16 v3, 0x40000000

    div-float v3, v39, v3

    add-float v3, v3, v29

    int-to-float v4, v13

    sub-float/2addr v3, v4

    float-to-int v11, v3

    move-object/from16 v9, p2

    invoke-static/range {v9 .. v14}, Lcom/android/inputmethod/keyboard/KeyboardView;->drawIcon(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;IIII)V

    goto/16 :goto_3

    :cond_f
    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/keyboard/Key;->hasShiftedLetterHint()Z

    move-result v3

    if-eqz v3, :cond_11

    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/keyboard/Key;->isShiftedLetterActivated()Z

    move-result v3

    if-eqz v3, :cond_10

    move-object/from16 v0, p4

    iget v0, v0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyShiftedLetterHintActivatedColor:I

    move/from16 v31, v0

    :goto_7
    move-object/from16 v0, p4

    iget v0, v0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyShiftedLetterHintSize:I

    move/from16 v32, v0

    goto/16 :goto_4

    :cond_10
    move-object/from16 v0, p4

    iget v0, v0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyShiftedLetterHintInactivatedColor:I

    move/from16 v31, v0

    goto :goto_7

    :cond_11
    move-object/from16 v0, p4

    iget v0, v0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyHintLetterColor:I

    move/from16 v31, v0

    move-object/from16 v0, p4

    iget v0, v0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyHintLetterSize:I

    move/from16 v32, v0

    goto/16 :goto_4

    :cond_12
    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/keyboard/Key;->hasShiftedLetterHint()Z

    move-result v3

    if-eqz v3, :cond_13

    move/from16 v0, v34

    int-to-float v3, v0

    move-object/from16 v0, p4

    iget v4, v0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyShiftedLetterHintPadding:F

    sub-float/2addr v3, v4

    sget-object v4, Lcom/android/inputmethod/keyboard/KeyboardView;->KEY_LABEL_REFERENCE_CHAR:[C

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v4, v1}, Lcom/android/inputmethod/keyboard/KeyboardView;->getCharWidth([CLandroid/graphics/Paint;)F

    move-result v4

    const/high16 v5, 0x40000000

    div-float/2addr v4, v5

    sub-float v19, v3, v4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mFontMetrics:Landroid/graphics/Paint$FontMetrics;

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->getFontMetrics(Landroid/graphics/Paint$FontMetrics;)F

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mFontMetrics:Landroid/graphics/Paint$FontMetrics;

    iget v3, v3, Landroid/graphics/Paint$FontMetrics;->top:F

    neg-float v0, v3

    move/from16 v20, v0

    sget-object v3, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    goto/16 :goto_5

    :cond_13
    move/from16 v0, v34

    int-to-float v3, v0

    move-object/from16 v0, p4

    iget v4, v0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyHintLetterPadding:F

    sub-float/2addr v3, v4

    sget-object v4, Lcom/android/inputmethod/keyboard/KeyboardView;->KEY_NUMERIC_HINT_LABEL_REFERENCE_CHAR:[C

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v4, v1}, Lcom/android/inputmethod/keyboard/KeyboardView;->getCharWidth([CLandroid/graphics/Paint;)F

    move-result v4

    const/high16 v5, 0x40000000

    div-float/2addr v4, v5

    sub-float v19, v3, v4

    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Paint;->ascent()F

    move-result v3

    neg-float v0, v3

    move/from16 v20, v0

    sget-object v3, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    goto/16 :goto_5

    :cond_14
    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/keyboard/Key;->isAlignRight()Z

    move-result v3

    if-eqz v3, :cond_15

    move-object/from16 v0, p4

    iget v3, v0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyLabelHorizontalPadding:F

    float-to-int v3, v3

    sub-int v3, v34, v3

    sub-int v11, v3, v13

    add-int v28, v11, v13

    goto/16 :goto_6

    :cond_15
    sub-int v3, v34, v13

    div-int/lit8 v11, v3, 0x2

    div-int/lit8 v3, v13, 0x2

    add-int v28, v11, v3

    goto/16 :goto_6
.end method

.method protected onMeasure(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyboard:Lcom/android/inputmethod/keyboard/Keyboard;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyboard:Lcom/android/inputmethod/keyboard/Keyboard;

    iget v1, v1, Lcom/android/inputmethod/keyboard/Keyboard;->mOccupiedHeight:I

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->getPaddingTop()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->getPaddingBottom()I

    move-result v2

    add-int v0, v1, v2

    invoke-virtual {p0, p1, v0}, Lcom/android/inputmethod/keyboard/KeyboardView;->setMeasuredDimension(II)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    goto :goto_0
.end method

.method public setKeyPreviewPopupEnabled(ZI)V
    .locals 0
    .param p1    # Z
    .param p2    # I

    iput-boolean p1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mShowKeyPreviewPopup:Z

    iput p2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mDelayAfterPreview:I

    return-void
.end method

.method public setKeyboard(Lcom/android/inputmethod/keyboard/Keyboard;)V
    .locals 3
    .param p1    # Lcom/android/inputmethod/keyboard/Keyboard;

    iput-object p1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyboard:Lcom/android/inputmethod/keyboard/Keyboard;

    invoke-static {p1}, Lcom/android/inputmethod/latin/LatinImeLogger;->onSetKeyboard(Lcom/android/inputmethod/keyboard/Keyboard;)V

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->requestLayout()V

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->invalidateAllKeys()V

    iget v1, p1, Lcom/android/inputmethod/keyboard/Keyboard;->mMostCommonKeyHeight:I

    iget v2, p1, Lcom/android/inputmethod/keyboard/Keyboard;->mVerticalGap:I

    sub-int v0, v1, v2

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyDrawParams:Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;

    invoke-virtual {v1, v0}, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->updateKeyHeight(I)V

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyPreviewDrawParams:Lcom/android/inputmethod/keyboard/KeyboardView$KeyPreviewDrawParams;

    invoke-virtual {v1, v0}, Lcom/android/inputmethod/keyboard/KeyboardView$KeyPreviewDrawParams;->updateKeyHeight(I)V

    return-void
.end method

.method public showKeyPreview(Lcom/android/inputmethod/keyboard/PointerTracker;)V
    .locals 14
    .param p1    # Lcom/android/inputmethod/keyboard/PointerTracker;

    const/4 v13, 0x1

    const/4 v12, -0x2

    const/4 v11, 0x0

    const/4 v10, 0x0

    iget-boolean v9, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mShowKeyPreviewPopup:Z

    if-nez v9, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/PointerTracker;->getKeyPreviewText()Landroid/widget/TextView;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v9

    if-nez v9, :cond_2

    invoke-direct {p0, v5}, Lcom/android/inputmethod/keyboard/KeyboardView;->addKeyPreview(Landroid/widget/TextView;)V

    :cond_2
    iget-object v9, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mDrawingHandler:Lcom/android/inputmethod/keyboard/KeyboardView$DrawingHandler;

    invoke-virtual {v9, p1}, Lcom/android/inputmethod/keyboard/KeyboardView$DrawingHandler;->cancelDismissKeyPreview(Lcom/android/inputmethod/keyboard/PointerTracker;)V

    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/PointerTracker;->getKey()Lcom/android/inputmethod/keyboard/Key;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyPreviewDrawParams:Lcom/android/inputmethod/keyboard/KeyboardView$KeyPreviewDrawParams;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/Key;->isShiftedLetterActivated()Z

    move-result v9

    if-eqz v9, :cond_4

    iget-object v2, v0, Lcom/android/inputmethod/keyboard/Key;->mHintLabel:Ljava/lang/String;

    :goto_1
    if-eqz v2, :cond_6

    invoke-virtual {v5, v10, v10, v10, v10}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    invoke-static {v2}, Lcom/android/inputmethod/latin/StringUtils;->codePointCount(Ljava/lang/String;)I

    move-result v9

    if-le v9, v13, :cond_5

    iget v9, v3, Lcom/android/inputmethod/keyboard/KeyboardView$KeyPreviewDrawParams;->mKeyLetterSize:I

    int-to-float v9, v9

    invoke-virtual {v5, v11, v9}, Landroid/widget/TextView;->setTextSize(IF)V

    sget-object v9, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    :goto_2
    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_3
    iget-object v9, v3, Lcom/android/inputmethod/keyboard/KeyboardView$KeyPreviewDrawParams;->mPreviewBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v5, v12, v12}, Landroid/widget/TextView;->measure(II)V

    iget v9, v0, Lcom/android/inputmethod/keyboard/Key;->mWidth:I

    iget v10, v0, Lcom/android/inputmethod/keyboard/Key;->mVisualInsetsLeft:I

    sub-int/2addr v9, v10

    iget v10, v0, Lcom/android/inputmethod/keyboard/Key;->mVisualInsetsRight:I

    sub-int v1, v9, v10

    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v6

    iget v4, v3, Lcom/android/inputmethod/keyboard/KeyboardView$KeyPreviewDrawParams;->mPreviewHeight:I

    invoke-virtual {v5}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v9

    sub-int v9, v6, v9

    invoke-virtual {v5}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v10

    sub-int/2addr v9, v10

    iput v9, v3, Lcom/android/inputmethod/keyboard/KeyboardView$KeyPreviewDrawParams;->mPreviewVisibleWidth:I

    invoke-virtual {v5}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v9

    sub-int v9, v4, v9

    invoke-virtual {v5}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v10

    sub-int/2addr v9, v10

    iput v9, v3, Lcom/android/inputmethod/keyboard/KeyboardView$KeyPreviewDrawParams;->mPreviewVisibleHeight:I

    iget v9, v3, Lcom/android/inputmethod/keyboard/KeyboardView$KeyPreviewDrawParams;->mPreviewOffset:I

    invoke-virtual {v5}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v10

    sub-int/2addr v9, v10

    iput v9, v3, Lcom/android/inputmethod/keyboard/KeyboardView$KeyPreviewDrawParams;->mPreviewVisibleOffset:I

    iget-object v9, v3, Lcom/android/inputmethod/keyboard/KeyboardView$KeyPreviewDrawParams;->mCoordinates:[I

    invoke-virtual {p0, v9}, Lcom/android/inputmethod/keyboard/KeyboardView;->getLocationInWindow([I)V

    iget v9, v0, Lcom/android/inputmethod/keyboard/Key;->mX:I

    iget v10, v0, Lcom/android/inputmethod/keyboard/Key;->mVisualInsetsLeft:I

    add-int/2addr v9, v10

    sub-int v10, v6, v1

    div-int/lit8 v10, v10, 0x2

    sub-int/2addr v9, v10

    iget-object v10, v3, Lcom/android/inputmethod/keyboard/KeyboardView$KeyPreviewDrawParams;->mCoordinates:[I

    aget v10, v10, v11

    add-int v7, v9, v10

    if-gez v7, :cond_7

    const/4 v7, 0x0

    iget-object v9, v3, Lcom/android/inputmethod/keyboard/KeyboardView$KeyPreviewDrawParams;->mPreviewLeftBackground:Landroid/graphics/drawable/Drawable;

    if-eqz v9, :cond_3

    iget-object v9, v3, Lcom/android/inputmethod/keyboard/KeyboardView$KeyPreviewDrawParams;->mPreviewLeftBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_3
    :goto_4
    iget v9, v0, Lcom/android/inputmethod/keyboard/Key;->mY:I

    sub-int/2addr v9, v4

    iget v10, v3, Lcom/android/inputmethod/keyboard/KeyboardView$KeyPreviewDrawParams;->mPreviewOffset:I

    add-int/2addr v9, v10

    iget-object v10, v3, Lcom/android/inputmethod/keyboard/KeyboardView$KeyPreviewDrawParams;->mCoordinates:[I

    aget v10, v10, v13

    add-int v8, v9, v10

    invoke-virtual {v5}, Landroid/widget/TextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v10

    iget-object v9, v0, Lcom/android/inputmethod/keyboard/Key;->mMoreKeys:[Lcom/android/inputmethod/keyboard/internal/KeySpecParser$MoreKeySpec;

    if-eqz v9, :cond_8

    sget-object v9, Lcom/android/inputmethod/keyboard/KeyboardView;->LONG_PRESSABLE_STATE_SET:[I

    :goto_5
    invoke-virtual {v10, v9}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    iget v9, v3, Lcom/android/inputmethod/keyboard/KeyboardView$KeyPreviewDrawParams;->mPreviewTextColor:I

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-static {v5, v7, v8, v6, v4}, Lcom/android/inputmethod/keyboard/ViewLayoutUtils;->placeViewAt(Landroid/view/View;IIII)V

    invoke-virtual {v5, v11}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_4
    iget-object v2, v0, Lcom/android/inputmethod/keyboard/Key;->mLabel:Ljava/lang/String;

    goto/16 :goto_1

    :cond_5
    iget v9, v3, Lcom/android/inputmethod/keyboard/KeyboardView$KeyPreviewDrawParams;->mPreviewTextSize:I

    int-to-float v9, v9

    invoke-virtual {v5, v11, v9}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v9, v3, Lcom/android/inputmethod/keyboard/KeyboardView$KeyPreviewDrawParams;->mKeyTextStyle:Landroid/graphics/Typeface;

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto/16 :goto_2

    :cond_6
    iget-object v9, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyboard:Lcom/android/inputmethod/keyboard/Keyboard;

    iget-object v9, v9, Lcom/android/inputmethod/keyboard/Keyboard;->mIconsSet:Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;

    invoke-virtual {v0, v9}, Lcom/android/inputmethod/keyboard/Key;->getPreviewIcon(Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v5, v10, v10, v10, v9}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    :cond_7
    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->getWidth()I

    move-result v9

    sub-int/2addr v9, v6

    if-le v7, v9, :cond_3

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->getWidth()I

    move-result v9

    sub-int v7, v9, v6

    iget-object v9, v3, Lcom/android/inputmethod/keyboard/KeyboardView$KeyPreviewDrawParams;->mPreviewRightBackground:Landroid/graphics/drawable/Drawable;

    if-eqz v9, :cond_3

    iget-object v9, v3, Lcom/android/inputmethod/keyboard/KeyboardView$KeyPreviewDrawParams;->mPreviewRightBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_4

    :cond_8
    sget-object v9, Lcom/android/inputmethod/keyboard/KeyboardView;->EMPTY_STATE_SET:[I

    goto :goto_5
.end method
