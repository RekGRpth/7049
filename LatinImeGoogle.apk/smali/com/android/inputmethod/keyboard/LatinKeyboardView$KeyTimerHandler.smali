.class Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;
.super Lcom/android/inputmethod/latin/StaticInnerHandlerWrapper;
.source "LatinKeyboardView.java"

# interfaces
.implements Lcom/android/inputmethod/keyboard/PointerTracker$TimerProxy;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/keyboard/LatinKeyboardView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "KeyTimerHandler"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/inputmethod/latin/StaticInnerHandlerWrapper",
        "<",
        "Lcom/android/inputmethod/keyboard/LatinKeyboardView;",
        ">;",
        "Lcom/android/inputmethod/keyboard/PointerTracker$TimerProxy;"
    }
.end annotation


# instance fields
.field private mInKeyRepeat:Z

.field private final mParams:Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerParams;


# direct methods
.method public constructor <init>(Lcom/android/inputmethod/keyboard/LatinKeyboardView;Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerParams;)V
    .locals 0
    .param p1    # Lcom/android/inputmethod/keyboard/LatinKeyboardView;
    .param p2    # Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerParams;

    invoke-direct {p0, p1}, Lcom/android/inputmethod/latin/StaticInnerHandlerWrapper;-><init>(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;->mParams:Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerParams;

    return-void
.end method

.method public static cancelAndStartAnimators(Landroid/animation/ObjectAnimator;Landroid/animation/ObjectAnimator;)V
    .locals 5
    .param p0    # Landroid/animation/ObjectAnimator;
    .param p1    # Landroid/animation/ObjectAnimator;

    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/animation/ObjectAnimator;->isStarted()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Landroid/animation/ObjectAnimator;->cancel()V

    const/high16 v3, 0x3f800000

    invoke-virtual {p0}, Landroid/animation/ObjectAnimator;->getAnimatedFraction()F

    move-result v4

    sub-float v0, v3, v4

    :cond_0
    invoke-virtual {p1}, Landroid/animation/ObjectAnimator;->getDuration()J

    move-result-wide v3

    long-to-float v3, v3

    mul-float/2addr v3, v0

    float-to-long v1, v3

    invoke-virtual {p1}, Landroid/animation/ObjectAnimator;->start()V

    invoke-virtual {p1, v1, v2}, Landroid/animation/ObjectAnimator;->setCurrentPlayTime(J)V

    return-void
.end method

.method private startKeyRepeatTimer(Lcom/android/inputmethod/keyboard/PointerTracker;J)V
    .locals 1
    .param p1    # Lcom/android/inputmethod/keyboard/PointerTracker;
    .param p2    # J

    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1}, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3}, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method


# virtual methods
.method public cancelAllMessages()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;->cancelKeyTimers()V

    return-void
.end method

.method public cancelDoubleTapTimer()V
    .locals 1

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;->removeMessages(I)V

    return-void
.end method

.method public cancelKeyRepeatTimer()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;->mInKeyRepeat:Z

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;->removeMessages(I)V

    return-void
.end method

.method public cancelKeyTimers()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;->cancelKeyRepeatTimer()V

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;->cancelLongPressTimer()V

    return-void
.end method

.method public cancelLongPressTimer()V
    .locals 1

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;->removeMessages(I)V

    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1    # Landroid/os/Message;

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;->getOuterInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/android/inputmethod/keyboard/PointerTracker;

    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/PointerTracker;->getKey()Lcom/android/inputmethod/keyboard/Key;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/inputmethod/keyboard/PointerTracker;->onRegisterKey(Lcom/android/inputmethod/keyboard/Key;)V

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;->mParams:Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerParams;

    iget v2, v2, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerParams;->mKeyRepeatInterval:I

    int-to-long v2, v2

    invoke-direct {p0, v1, v2, v3}, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;->startKeyRepeatTimer(Lcom/android/inputmethod/keyboard/PointerTracker;J)V

    goto :goto_0

    :pswitch_2
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/PointerTracker;->getKey()Lcom/android/inputmethod/keyboard/Key;

    move-result-object v2

    # invokes: Lcom/android/inputmethod/keyboard/LatinKeyboardView;->openMoreKeysKeyboardIfRequired(Lcom/android/inputmethod/keyboard/Key;Lcom/android/inputmethod/keyboard/PointerTracker;)Z
    invoke-static {v0, v2, v1}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->access$000(Lcom/android/inputmethod/keyboard/LatinKeyboardView;Lcom/android/inputmethod/keyboard/Key;Lcom/android/inputmethod/keyboard/PointerTracker;)Z

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getInstance()Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v2, v3}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->onLongPressTimeout(I)V

    goto :goto_0

    :pswitch_3
    # getter for: Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mAltCodeKeyWhileTypingFadeoutAnimator:Landroid/animation/ObjectAnimator;
    invoke-static {v0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->access$100(Lcom/android/inputmethod/keyboard/LatinKeyboardView;)Landroid/animation/ObjectAnimator;

    move-result-object v2

    # getter for: Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mAltCodeKeyWhileTypingFadeinAnimator:Landroid/animation/ObjectAnimator;
    invoke-static {v0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->access$200(Lcom/android/inputmethod/keyboard/LatinKeyboardView;)Landroid/animation/ObjectAnimator;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;->cancelAndStartAnimators(Landroid/animation/ObjectAnimator;Landroid/animation/ObjectAnimator;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public isInDoubleTapTimeout()Z
    .locals 1

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;->hasMessages(I)Z

    move-result v0

    return v0
.end method

.method public isInKeyRepeat()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;->mInKeyRepeat:Z

    return v0
.end method

.method public isTypingState()Z
    .locals 1

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;->hasMessages(I)Z

    move-result v0

    return v0
.end method

.method public startDoubleTapTimer()V
    .locals 3

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-static {}, Landroid/view/ViewConfiguration;->getDoubleTapTimeout()I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

.method public startKeyRepeatTimer(Lcom/android/inputmethod/keyboard/PointerTracker;)V
    .locals 2
    .param p1    # Lcom/android/inputmethod/keyboard/PointerTracker;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;->mInKeyRepeat:Z

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;->mParams:Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerParams;

    iget v0, v0, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerParams;->mKeyRepeatStartTimeout:I

    int-to-long v0, v0

    invoke-direct {p0, p1, v0, v1}, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;->startKeyRepeatTimer(Lcom/android/inputmethod/keyboard/PointerTracker;J)V

    return-void
.end method

.method public startLongPressTimer(I)V
    .locals 4
    .param p1    # I

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;->cancelLongPressTimer()V

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    if-lez v0, :cond_0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {p0, v1, p1, v2}, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    int-to-long v2, v0

    invoke-virtual {p0, v1, v2, v3}, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;->mParams:Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerParams;

    iget v0, v1, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerParams;->mLongPressShiftKeyTimeout:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch
.end method

.method public startLongPressTimer(Lcom/android/inputmethod/keyboard/PointerTracker;)V
    .locals 5
    .param p1    # Lcom/android/inputmethod/keyboard/PointerTracker;

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;->cancelLongPressTimer()V

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/PointerTracker;->getKey()Lcom/android/inputmethod/keyboard/Key;

    move-result-object v1

    iget v2, v1, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    packed-switch v2, :pswitch_data_0

    invoke-static {}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getInstance()Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->isInMomentarySwitchState()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;->mParams:Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerParams;

    iget v2, v2, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerParams;->mLongPressKeyTimeout:I

    mul-int/lit8 v0, v2, 0x3

    :goto_1
    if-lez v0, :cond_0

    const/4 v2, 0x2

    invoke-virtual {p0, v2, p1}, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    int-to-long v3, v0

    invoke-virtual {p0, v2, v3, v4}, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    :pswitch_0
    iget-object v2, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;->mParams:Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerParams;

    iget v0, v2, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerParams;->mLongPressShiftKeyTimeout:I

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;->mParams:Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerParams;

    iget v0, v2, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerParams;->mLongPressKeyTimeout:I

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch
.end method

.method public startTypingStateTimer()V
    .locals 5

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;->isTypingState()Z

    move-result v0

    invoke-virtual {p0, v2}, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;->removeMessages(I)V

    invoke-virtual {p0, v2}, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;->mParams:Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerParams;

    iget v3, v3, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerParams;->mIgnoreAltCodeKeyTimeout:I

    int-to-long v3, v3

    invoke-virtual {p0, v2, v3, v4}, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;->getOuterInstance()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    # getter for: Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mAltCodeKeyWhileTypingFadeinAnimator:Landroid/animation/ObjectAnimator;
    invoke-static {v1}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->access$200(Lcom/android/inputmethod/keyboard/LatinKeyboardView;)Landroid/animation/ObjectAnimator;

    move-result-object v2

    # getter for: Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mAltCodeKeyWhileTypingFadeoutAnimator:Landroid/animation/ObjectAnimator;
    invoke-static {v1}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->access$100(Lcom/android/inputmethod/keyboard/LatinKeyboardView;)Landroid/animation/ObjectAnimator;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;->cancelAndStartAnimators(Landroid/animation/ObjectAnimator;Landroid/animation/ObjectAnimator;)V

    goto :goto_0
.end method
