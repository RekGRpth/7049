.class Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerParams;
.super Ljava/lang/Object;
.source "LatinKeyboardView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/keyboard/LatinKeyboardView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "KeyTimerParams"
.end annotation


# instance fields
.field public final mIgnoreAltCodeKeyTimeout:I

.field public final mKeyRepeatInterval:I

.field public final mKeyRepeatStartTimeout:I

.field public final mLongPressKeyTimeout:I

.field public final mLongPressShiftKeyTimeout:I


# direct methods
.method public constructor <init>(Landroid/content/res/TypedArray;)V
    .locals 2
    .param p1    # Landroid/content/res/TypedArray;

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0xd

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerParams;->mKeyRepeatStartTimeout:I

    const/16 v0, 0xe

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerParams;->mKeyRepeatInterval:I

    const/16 v0, 0xf

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerParams;->mLongPressKeyTimeout:I

    const/16 v0, 0x10

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerParams;->mLongPressShiftKeyTimeout:I

    const/16 v0, 0x11

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerParams;->mIgnoreAltCodeKeyTimeout:I

    return-void
.end method
