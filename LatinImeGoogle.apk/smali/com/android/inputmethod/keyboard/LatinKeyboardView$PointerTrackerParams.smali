.class public Lcom/android/inputmethod/keyboard/LatinKeyboardView$PointerTrackerParams;
.super Ljava/lang/Object;
.source "LatinKeyboardView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/keyboard/LatinKeyboardView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PointerTrackerParams"
.end annotation


# static fields
.field public static final DEFAULT:Lcom/android/inputmethod/keyboard/LatinKeyboardView$PointerTrackerParams;


# instance fields
.field public final mSlidingKeyInputEnabled:Z

.field public final mTouchNoiseThresholdDistance:F

.field public final mTouchNoiseThresholdTime:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/inputmethod/keyboard/LatinKeyboardView$PointerTrackerParams;

    invoke-direct {v0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView$PointerTrackerParams;-><init>()V

    sput-object v0, Lcom/android/inputmethod/keyboard/LatinKeyboardView$PointerTrackerParams;->DEFAULT:Lcom/android/inputmethod/keyboard/LatinKeyboardView$PointerTrackerParams;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView$PointerTrackerParams;->mSlidingKeyInputEnabled:Z

    iput v0, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView$PointerTrackerParams;->mTouchNoiseThresholdTime:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView$PointerTrackerParams;->mTouchNoiseThresholdDistance:F

    return-void
.end method

.method public constructor <init>(Landroid/content/res/TypedArray;)V
    .locals 2
    .param p1    # Landroid/content/res/TypedArray;

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0xc

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView$PointerTrackerParams;->mSlidingKeyInputEnabled:Z

    const/16 v0, 0xa

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView$PointerTrackerParams;->mTouchNoiseThresholdTime:I

    const/16 v0, 0xb

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView$PointerTrackerParams;->mTouchNoiseThresholdDistance:F

    return-void
.end method
