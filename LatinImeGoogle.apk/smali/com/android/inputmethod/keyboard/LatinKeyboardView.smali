.class public Lcom/android/inputmethod/keyboard/LatinKeyboardView;
.super Lcom/android/inputmethod/keyboard/KeyboardView;
.source "LatinKeyboardView.java"

# interfaces
.implements Lcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;
.implements Lcom/android/inputmethod/keyboard/SuddenJumpingTouchEventHandler$ProcessMotionEvent;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerParams;,
        Lcom/android/inputmethod/keyboard/LatinKeyboardView$PointerTrackerParams;,
        Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;
    }
.end annotation


# static fields
.field private static final ENABLE_USABILITY_STUDY_LOG:Z

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAltCodeKeyWhileTypingAnimAlpha:I

.field private mAltCodeKeyWhileTypingFadeinAnimator:Landroid/animation/ObjectAnimator;

.field private mAltCodeKeyWhileTypingFadeoutAnimator:Landroid/animation/ObjectAnimator;

.field private final mAutoCorrectionSpacebarLedEnabled:Z

.field private final mAutoCorrectionSpacebarLedIcon:Landroid/graphics/drawable/Drawable;

.field private mAutoCorrectionSpacebarLedOn:Z

.field private final mConfigShowMoreKeysKeyboardAtTouchedPoint:Z

.field private mHasDistinctMultitouch:Z

.field private mHasMultipleEnabledIMEsOrSubtypes:Z

.field protected mKeyDetector:Lcom/android/inputmethod/keyboard/KeyDetector;

.field private final mKeyTimerHandler:Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;

.field private mKeyboardActionListener:Lcom/android/inputmethod/keyboard/KeyboardActionListener;

.field private mLanguageOnSpacebarAnimAlpha:I

.field private mLanguageOnSpacebarFadeoutAnimator:Landroid/animation/ObjectAnimator;

.field private final mLanguageOnSpacebarFinalAlpha:I

.field private mMoreKeysPanel:Lcom/android/inputmethod/keyboard/MoreKeysPanel;

.field private final mMoreKeysPanelCache:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Lcom/android/inputmethod/keyboard/Key;",
            "Lcom/android/inputmethod/keyboard/MoreKeysPanel;",
            ">;"
        }
    .end annotation
.end field

.field private mMoreKeysPanelPointerTrackerId:I

.field private mMoreKeysWindow:Landroid/widget/PopupWindow;

.field private mNeedsToDisplayLanguage:Z

.field private mOldKey:Lcom/android/inputmethod/keyboard/Key;

.field private mOldPointerCount:I

.field private final mPointerTrackerParams:Lcom/android/inputmethod/keyboard/LatinKeyboardView$PointerTrackerParams;

.field private mSpaceIcon:Landroid/graphics/drawable/Drawable;

.field private mSpaceKey:Lcom/android/inputmethod/keyboard/Key;

.field private final mSpacebarTextColor:I

.field private final mSpacebarTextRatio:F

.field private final mSpacebarTextShadowColor:I

.field private mSpacebarTextSize:F

.field private final mTouchScreenRegulator:Lcom/android/inputmethod/keyboard/SuddenJumpingTouchEventHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->TAG:Ljava/lang/String;

    sget-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sUsabilityStudy:Z

    sput-boolean v0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->ENABLE_USABILITY_STUDY_LOG:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const v0, 0x7f010003

    invoke-direct {p0, p1, p2, v0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 11
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/android/inputmethod/keyboard/KeyboardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/16 v7, 0xff

    iput v7, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mLanguageOnSpacebarAnimAlpha:I

    const/16 v7, 0xff

    iput v7, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mAltCodeKeyWhileTypingAnimAlpha:I

    new-instance v7, Ljava/util/WeakHashMap;

    invoke-direct {v7}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v7, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mMoreKeysPanelCache:Ljava/util/WeakHashMap;

    const/4 v7, 0x1

    iput v7, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mOldPointerCount:I

    new-instance v7, Lcom/android/inputmethod/keyboard/SuddenJumpingTouchEventHandler;

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-direct {v7, v8, p0}, Lcom/android/inputmethod/keyboard/SuddenJumpingTouchEventHandler;-><init>(Landroid/content/Context;Lcom/android/inputmethod/keyboard/SuddenJumpingTouchEventHandler$ProcessMotionEvent;)V

    iput-object v7, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mTouchScreenRegulator:Lcom/android/inputmethod/keyboard/SuddenJumpingTouchEventHandler;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    const-string v8, "android.hardware.touchscreen.multitouch.distinct"

    invoke-virtual {v7, v8}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v7

    iput-boolean v7, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mHasDistinctMultitouch:Z

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0c0012

    const-string v9, "false"

    invoke-static {v7, v8, v9}, Lcom/android/inputmethod/latin/Utils;->getDeviceOverrideValue(Landroid/content/res/Resources;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v6

    iget-boolean v7, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mHasDistinctMultitouch:Z

    invoke-static {v7, v6}, Lcom/android/inputmethod/keyboard/PointerTracker;->init(ZZ)V

    sget-object v7, Lcom/android/inputmethod/latin/R$styleable;->LatinKeyboardView:[I

    const v8, 0x7f0e0005

    invoke-virtual {p1, p2, v7, p3, v8}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v7

    iput-boolean v7, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mAutoCorrectionSpacebarLedEnabled:Z

    const/4 v7, 0x1

    invoke-virtual {v0, v7}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    iput-object v7, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mAutoCorrectionSpacebarLedIcon:Landroid/graphics/drawable/Drawable;

    const/4 v7, 0x2

    const/16 v8, 0x3e8

    const/16 v9, 0x3e8

    const/high16 v10, 0x3f800000

    invoke-virtual {v0, v7, v8, v9, v10}, Landroid/content/res/TypedArray;->getFraction(IIIF)F

    move-result v7

    const/high16 v8, 0x447a0000

    div-float/2addr v7, v8

    iput v7, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mSpacebarTextRatio:F

    const/4 v7, 0x3

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v7

    iput v7, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mSpacebarTextColor:I

    const/4 v7, 0x4

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v7

    iput v7, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mSpacebarTextShadowColor:I

    const/4 v7, 0x5

    const/16 v8, 0xff

    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v7

    iput v7, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mLanguageOnSpacebarFinalAlpha:I

    const/4 v7, 0x6

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v5

    const/4 v7, 0x7

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    const/16 v7, 0x8

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    new-instance v4, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerParams;

    invoke-direct {v4, v0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerParams;-><init>(Landroid/content/res/TypedArray;)V

    new-instance v7, Lcom/android/inputmethod/keyboard/LatinKeyboardView$PointerTrackerParams;

    invoke-direct {v7, v0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView$PointerTrackerParams;-><init>(Landroid/content/res/TypedArray;)V

    iput-object v7, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mPointerTrackerParams:Lcom/android/inputmethod/keyboard/LatinKeyboardView$PointerTrackerParams;

    const/16 v7, 0x9

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v3

    new-instance v7, Lcom/android/inputmethod/keyboard/KeyDetector;

    invoke-direct {v7, v3}, Lcom/android/inputmethod/keyboard/KeyDetector;-><init>(F)V

    iput-object v7, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mKeyDetector:Lcom/android/inputmethod/keyboard/KeyDetector;

    new-instance v7, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;

    invoke-direct {v7, p0, v4}, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;-><init>(Lcom/android/inputmethod/keyboard/LatinKeyboardView;Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerParams;)V

    iput-object v7, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mKeyTimerHandler:Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;

    const/16 v7, 0x12

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v7

    iput-boolean v7, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mConfigShowMoreKeysKeyboardAtTouchedPoint:Z

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    iget-object v7, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mPointerTrackerParams:Lcom/android/inputmethod/keyboard/LatinKeyboardView$PointerTrackerParams;

    invoke-static {v7}, Lcom/android/inputmethod/keyboard/PointerTracker;->setParameters(Lcom/android/inputmethod/keyboard/LatinKeyboardView$PointerTrackerParams;)V

    invoke-direct {p0, v5, p0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->loadObjectAnimator(ILjava/lang/Object;)Landroid/animation/ObjectAnimator;

    move-result-object v7

    iput-object v7, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mLanguageOnSpacebarFadeoutAnimator:Landroid/animation/ObjectAnimator;

    invoke-direct {p0, v2, p0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->loadObjectAnimator(ILjava/lang/Object;)Landroid/animation/ObjectAnimator;

    move-result-object v7

    iput-object v7, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mAltCodeKeyWhileTypingFadeoutAnimator:Landroid/animation/ObjectAnimator;

    invoke-direct {p0, v1, p0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->loadObjectAnimator(ILjava/lang/Object;)Landroid/animation/ObjectAnimator;

    move-result-object v7

    iput-object v7, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mAltCodeKeyWhileTypingFadeinAnimator:Landroid/animation/ObjectAnimator;

    return-void
.end method

.method static synthetic access$000(Lcom/android/inputmethod/keyboard/LatinKeyboardView;Lcom/android/inputmethod/keyboard/Key;Lcom/android/inputmethod/keyboard/PointerTracker;)Z
    .locals 1
    .param p0    # Lcom/android/inputmethod/keyboard/LatinKeyboardView;
    .param p1    # Lcom/android/inputmethod/keyboard/Key;
    .param p2    # Lcom/android/inputmethod/keyboard/PointerTracker;

    invoke-direct {p0, p1, p2}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->openMoreKeysKeyboardIfRequired(Lcom/android/inputmethod/keyboard/Key;Lcom/android/inputmethod/keyboard/PointerTracker;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/android/inputmethod/keyboard/LatinKeyboardView;)Landroid/animation/ObjectAnimator;
    .locals 1
    .param p0    # Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mAltCodeKeyWhileTypingFadeoutAnimator:Landroid/animation/ObjectAnimator;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/inputmethod/keyboard/LatinKeyboardView;)Landroid/animation/ObjectAnimator;
    .locals 1
    .param p0    # Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mAltCodeKeyWhileTypingFadeinAnimator:Landroid/animation/ObjectAnimator;

    return-object v0
.end method

.method private drawSpacebar(Lcom/android/inputmethod/keyboard/Key;Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 16
    .param p1    # Lcom/android/inputmethod/keyboard/Key;
    .param p2    # Landroid/graphics/Canvas;
    .param p3    # Landroid/graphics/Paint;

    move-object/from16 v0, p1

    iget v14, v0, Lcom/android/inputmethod/keyboard/Key;->mWidth:I

    move-object/from16 v0, p1

    iget v10, v0, Lcom/android/inputmethod/keyboard/Key;->mHeight:I

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mNeedsToDisplayLanguage:Z

    if-eqz v2, :cond_0

    sget-object v2, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mSpacebarTextSize:F

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    invoke-virtual/range {p0 .. p0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->getKeyboard()Lcom/android/inputmethod/keyboard/Keyboard;

    move-result-object v2

    iget-object v2, v2, Lcom/android/inputmethod/keyboard/Keyboard;->mId:Lcom/android/inputmethod/keyboard/KeyboardId;

    iget-object v12, v2, Lcom/android/inputmethod/keyboard/KeyboardId;->mSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v12, v14}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->layoutLanguageOnSpacebar(Landroid/graphics/Paint;Landroid/view/inputmethod/InputMethodSubtype;I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Paint;->descent()F

    move-result v9

    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Paint;->ascent()F

    move-result v2

    neg-float v2, v2

    add-float v13, v2, v9

    div-int/lit8 v2, v10, 0x2

    int-to-float v2, v2

    const/high16 v3, 0x40000000

    div-float v3, v13, v3

    add-float v8, v2, v3

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mSpacebarTextShadowColor:I

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mLanguageOnSpacebarAnimAlpha:I

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    div-int/lit8 v2, v14, 0x2

    int-to-float v2, v2

    sub-float v3, v8, v9

    const/high16 v15, 0x3f800000

    sub-float/2addr v3, v15

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v0, v11, v2, v3, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mSpacebarTextColor:I

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mLanguageOnSpacebarAnimAlpha:I

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    div-int/lit8 v2, v14, 0x2

    int-to-float v2, v2

    sub-float v3, v8, v9

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v0, v11, v2, v3, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :cond_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mAutoCorrectionSpacebarLedOn:Z

    if-eqz v2, :cond_2

    mul-int/lit8 v2, v14, 0x50

    div-int/lit8 v6, v2, 0x64

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mAutoCorrectionSpacebarLedIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v7

    sub-int v2, v14, v6

    div-int/lit8 v4, v2, 0x2

    sub-int v5, v10, v7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mAutoCorrectionSpacebarLedIcon:Landroid/graphics/drawable/Drawable;

    move-object/from16 v2, p2

    invoke-static/range {v2 .. v7}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->drawIcon(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;IIII)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mSpaceIcon:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mSpaceIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mSpaceIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v7

    sub-int v2, v14, v6

    div-int/lit8 v4, v2, 0x2

    sub-int v5, v10, v7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mSpaceIcon:Landroid/graphics/drawable/Drawable;

    move-object/from16 v2, p2

    invoke-static/range {v2 .. v7}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->drawIcon(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;IIII)V

    goto :goto_0
.end method

.method private fitsTextIntoWidth(ILjava/lang/String;Landroid/graphics/Paint;)Z
    .locals 6
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/graphics/Paint;

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000

    invoke-virtual {p3, v4}, Landroid/graphics/Paint;->setTextScaleX(F)V

    invoke-virtual {p0, p2, p3}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->getLabelWidth(Ljava/lang/String;Landroid/graphics/Paint;)F

    move-result v1

    int-to-float v4, p1

    cmpg-float v4, v1, v4

    if-gez v4, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    int-to-float v4, p1

    div-float v0, v4, v1

    const v4, 0x3f4ccccd

    cmpg-float v4, v0, v4

    if-gez v4, :cond_2

    move v2, v3

    goto :goto_0

    :cond_2
    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setTextScaleX(F)V

    invoke-virtual {p0, p2, p3}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->getLabelWidth(Ljava/lang/String;Landroid/graphics/Paint;)F

    move-result v4

    int-to-float v5, p1

    cmpg-float v4, v4, v5

    if-ltz v4, :cond_0

    move v2, v3

    goto :goto_0
.end method

.method static getFullDisplayName(Landroid/view/inputmethod/InputMethodSubtype;Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1
    .param p0    # Landroid/view/inputmethod/InputMethodSubtype;
    .param p1    # Landroid/content/res/Resources;

    invoke-static {p0}, Lcom/android/inputmethod/latin/SubtypeLocale;->isNoLanguage(Landroid/view/inputmethod/InputMethodSubtype;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/android/inputmethod/latin/SubtypeLocale;->getKeyboardLayoutSetDisplayName(Landroid/view/inputmethod/InputMethodSubtype;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p1}, Lcom/android/inputmethod/latin/SubtypeLocale;->getSubtypeDisplayName(Landroid/view/inputmethod/InputMethodSubtype;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static getMiddleDisplayName(Landroid/view/inputmethod/InputMethodSubtype;)Ljava/lang/String;
    .locals 2
    .param p0    # Landroid/view/inputmethod/InputMethodSubtype;

    invoke-static {p0}, Lcom/android/inputmethod/latin/SubtypeLocale;->isNoLanguage(Landroid/view/inputmethod/InputMethodSubtype;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/android/inputmethod/latin/SubtypeLocale;->getKeyboardLayoutSetDisplayName(Landroid/view/inputmethod/InputMethodSubtype;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    invoke-static {p0}, Lcom/android/inputmethod/latin/SubtypeLocale;->getSubtypeLocale(Landroid/view/inputmethod/InputMethodSubtype;)Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0, v0}, Ljava/util/Locale;->getDisplayLanguage(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/android/inputmethod/latin/StringUtils;->toTitleCase(Ljava/lang/String;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method static getShortDisplayName(Landroid/view/inputmethod/InputMethodSubtype;)Ljava/lang/String;
    .locals 2
    .param p0    # Landroid/view/inputmethod/InputMethodSubtype;

    invoke-static {p0}, Lcom/android/inputmethod/latin/SubtypeLocale;->isNoLanguage(Landroid/view/inputmethod/InputMethodSubtype;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, ""

    :goto_0
    return-object v1

    :cond_0
    invoke-static {p0}, Lcom/android/inputmethod/latin/SubtypeLocale;->getSubtypeLocale(Landroid/view/inputmethod/InputMethodSubtype;)Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/android/inputmethod/latin/StringUtils;->toTitleCase(Ljava/lang/String;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private invokeCodeInput(I)V
    .locals 2
    .param p1    # I

    const/4 v1, -0x1

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mKeyboardActionListener:Lcom/android/inputmethod/keyboard/KeyboardActionListener;

    invoke-interface {v0, p1, v1, v1}, Lcom/android/inputmethod/keyboard/KeyboardActionListener;->onCodeInput(III)V

    return-void
.end method

.method private invokeCustomRequest(I)Z
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mKeyboardActionListener:Lcom/android/inputmethod/keyboard/KeyboardActionListener;

    invoke-interface {v0, p1}, Lcom/android/inputmethod/keyboard/KeyboardActionListener;->onCustomRequest(I)Z

    move-result v0

    return v0
.end method

.method private invokeReleaseKey(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mKeyboardActionListener:Lcom/android/inputmethod/keyboard/KeyboardActionListener;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/android/inputmethod/keyboard/KeyboardActionListener;->onReleaseKey(IZ)V

    return-void
.end method

.method private layoutLanguageOnSpacebar(Landroid/graphics/Paint;Landroid/view/inputmethod/InputMethodSubtype;I)Ljava/lang/String;
    .locals 2
    .param p1    # Landroid/graphics/Paint;
    .param p2    # Landroid/view/inputmethod/InputMethodSubtype;
    .param p3    # I

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {p2, v1}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->getFullDisplayName(Landroid/view/inputmethod/InputMethodSubtype;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p3, v0, p1}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->fitsTextIntoWidth(ILjava/lang/String;Landroid/graphics/Paint;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v1, v0

    :goto_0
    return-object v1

    :cond_0
    invoke-static {p2}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->getMiddleDisplayName(Landroid/view/inputmethod/InputMethodSubtype;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p3, v0, p1}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->fitsTextIntoWidth(ILjava/lang/String;Landroid/graphics/Paint;)Z

    move-result v1

    if-eqz v1, :cond_1

    move-object v1, v0

    goto :goto_0

    :cond_1
    invoke-static {p2}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->getShortDisplayName(Landroid/view/inputmethod/InputMethodSubtype;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p3, v0, p1}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->fitsTextIntoWidth(ILjava/lang/String;Landroid/graphics/Paint;)Z

    move-result v1

    if-eqz v1, :cond_2

    move-object v1, v0

    goto :goto_0

    :cond_2
    const-string v1, ""

    goto :goto_0
.end method

.method private loadObjectAnimator(ILjava/lang/Object;)Landroid/animation/ObjectAnimator;
    .locals 2
    .param p1    # I
    .param p2    # Ljava/lang/Object;

    if-nez p1, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v0

    check-cast v0, Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private openMoreKeysKeyboardIfRequired(Lcom/android/inputmethod/keyboard/Key;Lcom/android/inputmethod/keyboard/PointerTracker;)Z
    .locals 2
    .param p1    # Lcom/android/inputmethod/keyboard/Key;
    .param p2    # Lcom/android/inputmethod/keyboard/PointerTracker;

    const/4 v0, 0x0

    iget v1, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mMoreKeysLayout:I

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mMoreKeysPanel:Lcom/android/inputmethod/keyboard/MoreKeysPanel;

    if-nez v1, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1, p2}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->onLongPress(Lcom/android/inputmethod/keyboard/Key;Lcom/android/inputmethod/keyboard/PointerTracker;)Z

    move-result v0

    goto :goto_0
.end method

.method private openMoreKeysPanel(Lcom/android/inputmethod/keyboard/Key;Lcom/android/inputmethod/keyboard/PointerTracker;)Z
    .locals 11
    .param p1    # Lcom/android/inputmethod/keyboard/Key;
    .param p2    # Lcom/android/inputmethod/keyboard/PointerTracker;

    const/4 v7, 0x0

    const/4 v10, 0x1

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mMoreKeysPanelCache:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/inputmethod/keyboard/MoreKeysPanel;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->onCreateMoreKeysPanel(Lcom/android/inputmethod/keyboard/Key;)Lcom/android/inputmethod/keyboard/MoreKeysPanel;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return v7

    :cond_0
    iget-object v1, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mMoreKeysPanelCache:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    iget-object v1, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mMoreKeysWindow:Landroid/widget/PopupWindow;

    if-nez v1, :cond_2

    new-instance v1, Landroid/widget/PopupWindow;

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mMoreKeysWindow:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mMoreKeysWindow:Landroid/widget/PopupWindow;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mMoreKeysWindow:Landroid/widget/PopupWindow;

    const v2, 0x7f0e0029

    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    :cond_2
    iput-object v0, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mMoreKeysPanel:Lcom/android/inputmethod/keyboard/MoreKeysPanel;

    iget v1, p2, Lcom/android/inputmethod/keyboard/PointerTracker;->mPointerId:I

    iput v1, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mMoreKeysPanelPointerTrackerId:I

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->isKeyPreviewPopupEnabled()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/Key;->noKeyPreview()Z

    move-result v1

    if-nez v1, :cond_3

    move v7, v10

    :cond_3
    iget-boolean v1, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mConfigShowMoreKeysKeyboardAtTouchedPoint:Z

    if-eqz v1, :cond_4

    if-nez v7, :cond_4

    invoke-virtual {p2}, Lcom/android/inputmethod/keyboard/PointerTracker;->getLastX()I

    move-result v3

    :goto_1
    iget v2, p1, Lcom/android/inputmethod/keyboard/Key;->mY:I

    if-eqz v7, :cond_5

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mKeyPreviewDrawParams:Lcom/android/inputmethod/keyboard/KeyboardView$KeyPreviewDrawParams;

    iget v1, v1, Lcom/android/inputmethod/keyboard/KeyboardView$KeyPreviewDrawParams;->mPreviewVisibleOffset:I

    :goto_2
    add-int v4, v2, v1

    iget-object v5, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mMoreKeysWindow:Landroid/widget/PopupWindow;

    iget-object v6, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mKeyboardActionListener:Lcom/android/inputmethod/keyboard/KeyboardActionListener;

    move-object v1, p0

    move-object v2, p0

    invoke-interface/range {v0 .. v6}, Lcom/android/inputmethod/keyboard/MoreKeysPanel;->showMoreKeysPanel(Landroid/view/View;Lcom/android/inputmethod/keyboard/MoreKeysPanel$Controller;IILandroid/widget/PopupWindow;Lcom/android/inputmethod/keyboard/KeyboardActionListener;)V

    invoke-virtual {p2}, Lcom/android/inputmethod/keyboard/PointerTracker;->getLastX()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/android/inputmethod/keyboard/MoreKeysPanel;->translateX(I)I

    move-result v8

    invoke-virtual {p2}, Lcom/android/inputmethod/keyboard/PointerTracker;->getLastY()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/android/inputmethod/keyboard/MoreKeysPanel;->translateY(I)I

    move-result v9

    invoke-virtual {p2, v8, v9, v0}, Lcom/android/inputmethod/keyboard/PointerTracker;->onShowMoreKeysPanel(IILcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;)V

    invoke-virtual {p0, v10}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->dimEntireKeyboard(Z)V

    move v7, v10

    goto :goto_0

    :cond_4
    iget v1, p1, Lcom/android/inputmethod/keyboard/Key;->mX:I

    iget v2, p1, Lcom/android/inputmethod/keyboard/Key;->mWidth:I

    div-int/lit8 v2, v2, 0x2

    add-int v3, v1, v2

    goto :goto_1

    :cond_5
    iget v1, p1, Lcom/android/inputmethod/keyboard/Key;->mVerticalGap:I

    neg-int v1, v1

    goto :goto_2
.end method

.method private updateAltCodeKeyWhileTyping()V
    .locals 5

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->getKeyboard()Lcom/android/inputmethod/keyboard/Keyboard;

    move-result-object v3

    if-nez v3, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, v3, Lcom/android/inputmethod/keyboard/Keyboard;->mAltCodeKeysWhileTyping:[Lcom/android/inputmethod/keyboard/Key;

    array-length v4, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v2, v0, v1

    invoke-virtual {p0, v2}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->invalidateKey(Lcom/android/inputmethod/keyboard/Key;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public cancelAllMessages()V
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mKeyTimerHandler:Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;->cancelAllMessages()V

    invoke-super {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->cancelAllMessages()V

    return-void
.end method

.method public closing()V
    .locals 1

    invoke-super {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->closing()V

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->dismissMoreKeysPanel()Z

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mMoreKeysPanelCache:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->clear()V

    return-void
.end method

.method public dismissMoreKeysPanel()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mMoreKeysWindow:Landroid/widget/PopupWindow;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mMoreKeysWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mMoreKeysWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->dismiss()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mMoreKeysPanel:Lcom/android/inputmethod/keyboard/MoreKeysPanel;

    const/4 v1, -0x1

    iput v1, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mMoreKeysPanelPointerTrackerId:I

    invoke-virtual {p0, v0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->dimEntireKeyboard(Z)V

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1    # Landroid/view/MotionEvent;

    const/4 v1, 0x0

    invoke-static {}, Lcom/android/inputmethod/accessibility/AccessibilityUtils;->getInstance()Lcom/android/inputmethod/accessibility/AccessibilityUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/inputmethod/accessibility/AccessibilityUtils;->isTouchExplorationEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v1, p0}, Lcom/android/inputmethod/keyboard/PointerTracker;->getPointerTracker(ILcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;)Lcom/android/inputmethod/keyboard/PointerTracker;

    move-result-object v0

    invoke-static {}, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->getInstance()Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->dispatchHoverEvent(Landroid/view/MotionEvent;Lcom/android/inputmethod/keyboard/PointerTracker;)Z

    move-result v1

    :cond_0
    return v1
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1    # Landroid/graphics/Canvas;

    invoke-static {}, Lcom/android/inputmethod/latin/Utils$GCUtils;->getInstance()Lcom/android/inputmethod/latin/Utils$GCUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/inputmethod/latin/Utils$GCUtils;->reset()V

    const/4 v2, 0x1

    const/4 v1, 0x0

    :goto_0
    const/4 v3, 0x5

    if-ge v1, v3, :cond_0

    if-eqz v2, :cond_0

    :try_start_0
    invoke-super {p0, p1}, Lcom/android/inputmethod/keyboard/KeyboardView;->draw(Landroid/graphics/Canvas;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v2, 0x0

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/android/inputmethod/latin/Utils$GCUtils;->getInstance()Lcom/android/inputmethod/latin/Utils$GCUtils;

    move-result-object v3

    sget-object v4, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->TAG:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Lcom/android/inputmethod/latin/Utils$GCUtils;->tryGCOrWait(Ljava/lang/String;Ljava/lang/Throwable;)Z

    move-result v2

    goto :goto_1

    :cond_0
    return-void
.end method

.method public getAltCodeKeyWhileTypingAnimAlpha()I
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mAltCodeKeyWhileTypingAnimAlpha:I

    return v0
.end method

.method public getDrawingProxy()Lcom/android/inputmethod/keyboard/PointerTracker$DrawingProxy;
    .locals 0

    return-object p0
.end method

.method public getKeyDetector()Lcom/android/inputmethod/keyboard/KeyDetector;
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mKeyDetector:Lcom/android/inputmethod/keyboard/KeyDetector;

    return-object v0
.end method

.method public getKeyboardActionListener()Lcom/android/inputmethod/keyboard/KeyboardActionListener;
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mKeyboardActionListener:Lcom/android/inputmethod/keyboard/KeyboardActionListener;

    return-object v0
.end method

.method public getLanguageOnSpacebarAnimAlpha()I
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mLanguageOnSpacebarAnimAlpha:I

    return v0
.end method

.method public getPointerCount()I
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mOldPointerCount:I

    return v0
.end method

.method public getTimerProxy()Lcom/android/inputmethod/keyboard/PointerTracker$TimerProxy;
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mKeyTimerHandler:Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;

    return-object v0
.end method

.method public isInSlidingKeyInput()Z
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mMoreKeysPanel:Lcom/android/inputmethod/keyboard/MoreKeysPanel;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-static {}, Lcom/android/inputmethod/keyboard/PointerTracker;->isAnyInSlidingKeyInput()Z

    move-result v0

    goto :goto_0
.end method

.method protected onCreateMoreKeysPanel(Lcom/android/inputmethod/keyboard/Key;)Lcom/android/inputmethod/keyboard/MoreKeysPanel;
    .locals 6
    .param p1    # Lcom/android/inputmethod/keyboard/Key;

    const/4 v2, 0x0

    const/4 v5, -0x2

    iget-object v3, p1, Lcom/android/inputmethod/keyboard/Key;->mMoreKeys:[Lcom/android/inputmethod/keyboard/internal/KeySpecParser$MoreKeySpec;

    if-nez v3, :cond_0

    :goto_0
    return-object v2

    :cond_0
    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    iget v4, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mMoreKeysLayout:I

    invoke-virtual {v3, v4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v3, Ljava/lang/NullPointerException;

    invoke-direct {v3}, Ljava/lang/NullPointerException;-><init>()V

    throw v3

    :cond_1
    const v3, 0x7f070042

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/inputmethod/keyboard/MoreKeysKeyboardView;

    new-instance v3, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder;

    invoke-direct {v3, v0, p1, p0}, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder;-><init>(Landroid/view/View;Lcom/android/inputmethod/keyboard/Key;Lcom/android/inputmethod/keyboard/KeyboardView;)V

    invoke-virtual {v3}, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder;->build()Lcom/android/inputmethod/keyboard/MoreKeysKeyboard;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/android/inputmethod/keyboard/MoreKeysKeyboardView;->setKeyboard(Lcom/android/inputmethod/keyboard/Keyboard;)V

    invoke-virtual {v0, v5, v5}, Landroid/view/View;->measure(II)V

    goto :goto_0
.end method

.method protected onDrawKeyTopVisuals(Lcom/android/inputmethod/keyboard/Key;Landroid/graphics/Canvas;Landroid/graphics/Paint;Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;)V
    .locals 2
    .param p1    # Lcom/android/inputmethod/keyboard/Key;
    .param p2    # Landroid/graphics/Canvas;
    .param p3    # Landroid/graphics/Paint;
    .param p4    # Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;

    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/Key;->altCodeWhileTyping()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/Key;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mAltCodeKeyWhileTypingAnimAlpha:I

    iput v0, p4, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mAnimAlpha:I

    :cond_0
    iget v0, p1, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    const/16 v1, 0x20

    if-ne v0, v1, :cond_2

    invoke-direct {p0, p1, p2, p3}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->drawSpacebar(Lcom/android/inputmethod/keyboard/Key;Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/Key;->isLongPressEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mHasMultipleEnabledIMEsOrSubtypes:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->drawKeyPopupHint(Lcom/android/inputmethod/keyboard/Key;Landroid/graphics/Canvas;Landroid/graphics/Paint;Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget v0, p1, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    const/16 v1, -0xa

    if-ne v0, v1, :cond_3

    invoke-super {p0, p1, p2, p3, p4}, Lcom/android/inputmethod/keyboard/KeyboardView;->onDrawKeyTopVisuals(Lcom/android/inputmethod/keyboard/Key;Landroid/graphics/Canvas;Landroid/graphics/Paint;Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;)V

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->drawKeyPopupHint(Lcom/android/inputmethod/keyboard/Key;Landroid/graphics/Canvas;Landroid/graphics/Paint;Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;)V

    goto :goto_0

    :cond_3
    invoke-super {p0, p1, p2, p3, p4}, Lcom/android/inputmethod/keyboard/KeyboardView;->onDrawKeyTopVisuals(Lcom/android/inputmethod/keyboard/Key;Landroid/graphics/Canvas;Landroid/graphics/Paint;Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;)V

    goto :goto_0
.end method

.method protected onLongPress(Lcom/android/inputmethod/keyboard/Key;Lcom/android/inputmethod/keyboard/PointerTracker;)Z
    .locals 5
    .param p1    # Lcom/android/inputmethod/keyboard/Key;
    .param p2    # Lcom/android/inputmethod/keyboard/PointerTracker;

    const/4 v2, 0x1

    iget v1, p1, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/Key;->hasEmbeddedMoreKey()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p1, Lcom/android/inputmethod/keyboard/Key;->mMoreKeys:[Lcom/android/inputmethod/keyboard/internal/KeySpecParser$MoreKeySpec;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    iget v0, v3, Lcom/android/inputmethod/keyboard/internal/KeySpecParser$MoreKeySpec;->mCode:I

    invoke-virtual {p2}, Lcom/android/inputmethod/keyboard/PointerTracker;->onLongPressed()V

    invoke-direct {p0, v0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->invokeCodeInput(I)V

    invoke-direct {p0, v1}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->invokeReleaseKey(I)V

    invoke-static {}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getInstance()Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->hapticAndAudioFeedback(I)V

    :goto_0
    return v2

    :cond_0
    const/16 v3, 0x20

    if-eq v1, v3, :cond_1

    const/16 v3, -0xa

    if-ne v1, v3, :cond_2

    :cond_1
    invoke-direct {p0, v2}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->invokeCustomRequest(I)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p2}, Lcom/android/inputmethod/keyboard/PointerTracker;->onLongPressed()V

    invoke-direct {p0, v1}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->invokeReleaseKey(I)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1, p2}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->openMoreKeysPanel(Lcom/android/inputmethod/keyboard/Key;Lcom/android/inputmethod/keyboard/PointerTracker;)Z

    move-result v2

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->getKeyboard()Lcom/android/inputmethod/keyboard/Keyboard;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mTouchScreenRegulator:Lcom/android/inputmethod/keyboard/SuddenJumpingTouchEventHandler;

    invoke-virtual {v0, p1}, Lcom/android/inputmethod/keyboard/SuddenJumpingTouchEventHandler;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public processMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 31
    .param p1    # Landroid/view/MotionEvent;

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mHasDistinctMultitouch:Z

    if-nez v7, :cond_0

    const/16 v21, 0x1

    :goto_0
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v8

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mOldPointerCount:I

    move/from16 v22, v0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mOldPointerCount:I

    if-eqz v21, :cond_1

    const/4 v7, 0x1

    move/from16 v0, v23

    if-le v0, v7, :cond_1

    const/4 v7, 0x1

    move/from16 v0, v22

    if-le v0, v7, :cond_1

    const/4 v7, 0x1

    :goto_1
    return v7

    :cond_0
    const/16 v21, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v5

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v17

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v16

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mMoreKeysPanel:Lcom/android/inputmethod/keyboard/MoreKeysPanel;

    if-eqz v7, :cond_5

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mMoreKeysPanelPointerTrackerId:I

    move/from16 v0, v16

    if-ne v0, v7, :cond_5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mMoreKeysPanel:Lcom/android/inputmethod/keyboard/MoreKeysPanel;

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v9

    float-to-int v9, v9

    invoke-interface {v7, v9}, Lcom/android/inputmethod/keyboard/MoreKeysPanel;->translateX(I)I

    move-result v3

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mMoreKeysPanel:Lcom/android/inputmethod/keyboard/MoreKeysPanel;

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v9

    float-to-int v9, v9

    invoke-interface {v7, v9}, Lcom/android/inputmethod/keyboard/MoreKeysPanel;->translateY(I)I

    move-result v4

    :goto_2
    sget-boolean v7, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->ENABLE_USABILITY_STUDY_LOG:Z

    if-eqz v7, :cond_2

    packed-switch v8, :pswitch_data_0

    :pswitch_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[Action"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, "]"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    :goto_3
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getSize(I)F

    move-result v30

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getPressure(I)F

    move-result v27

    invoke-static {}, Lcom/android/inputmethod/latin/Utils$UsabilityStudyLogUtils;->getInstance()Lcom/android/inputmethod/latin/Utils$UsabilityStudyLogUtils;

    move-result-object v7

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, v16

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, v30

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, v27

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Lcom/android/inputmethod/latin/Utils$UsabilityStudyLogUtils;->write(Ljava/lang/String;)V

    :cond_2
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mKeyTimerHandler:Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;

    invoke-virtual {v7}, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;->isInKeyRepeat()Z

    move-result v7

    if-eqz v7, :cond_3

    move/from16 v0, v16

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/android/inputmethod/keyboard/PointerTracker;->getPointerTracker(ILcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;)Lcom/android/inputmethod/keyboard/PointerTracker;

    move-result-object v2

    const/4 v7, 0x1

    move/from16 v0, v23

    if-le v0, v7, :cond_3

    invoke-virtual {v2}, Lcom/android/inputmethod/keyboard/PointerTracker;->isModifier()Z

    move-result v7

    if-nez v7, :cond_3

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mKeyTimerHandler:Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;

    invoke-virtual {v7}, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;->cancelKeyRepeatTimer()V

    :cond_3
    if-eqz v21, :cond_9

    const/4 v7, 0x0

    move-object/from16 v0, p0

    invoke-static {v7, v0}, Lcom/android/inputmethod/keyboard/PointerTracker;->getPointerTracker(ILcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;)Lcom/android/inputmethod/keyboard/PointerTracker;

    move-result-object v2

    const/4 v7, 0x1

    move/from16 v0, v23

    if-ne v0, v7, :cond_6

    const/4 v7, 0x2

    move/from16 v0, v22

    if-ne v0, v7, :cond_6

    invoke-virtual {v2, v3, v4}, Lcom/android/inputmethod/keyboard/PointerTracker;->getKeyOn(II)Lcom/android/inputmethod/keyboard/Key;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mOldKey:Lcom/android/inputmethod/keyboard/Key;

    move-object/from16 v0, v20

    if-eq v7, v0, :cond_4

    move-object/from16 v7, p0

    invoke-virtual/range {v2 .. v7}, Lcom/android/inputmethod/keyboard/PointerTracker;->onDownEvent(IIJLcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;)V

    const/4 v7, 0x1

    if-ne v8, v7, :cond_4

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/android/inputmethod/keyboard/PointerTracker;->onUpEvent(IIJ)V

    :cond_4
    :goto_4
    const/4 v7, 0x1

    goto/16 :goto_1

    :cond_5
    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v7

    float-to-int v3, v7

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v7

    float-to-int v4, v7

    goto/16 :goto_2

    :pswitch_1
    const-string v14, "[Up]"

    goto/16 :goto_3

    :pswitch_2
    const-string v14, "[Down]"

    goto/16 :goto_3

    :pswitch_3
    const-string v14, "[PointerUp]"

    goto/16 :goto_3

    :pswitch_4
    const-string v14, "[PointerDown]"

    goto/16 :goto_3

    :pswitch_5
    const-string v14, ""

    goto/16 :goto_3

    :cond_6
    const/4 v7, 0x2

    move/from16 v0, v23

    if-ne v0, v7, :cond_7

    const/4 v7, 0x1

    move/from16 v0, v22

    if-ne v0, v7, :cond_7

    invoke-virtual {v2}, Lcom/android/inputmethod/keyboard/PointerTracker;->getLastX()I

    move-result v18

    invoke-virtual {v2}, Lcom/android/inputmethod/keyboard/PointerTracker;->getLastY()I

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v2, v0, v1}, Lcom/android/inputmethod/keyboard/PointerTracker;->getKeyOn(II)Lcom/android/inputmethod/keyboard/Key;

    move-result-object v7

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mOldKey:Lcom/android/inputmethod/keyboard/Key;

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v2, v0, v1, v5, v6}, Lcom/android/inputmethod/keyboard/PointerTracker;->onUpEvent(IIJ)V

    goto :goto_4

    :cond_7
    const/4 v7, 0x1

    move/from16 v0, v23

    if-ne v0, v7, :cond_8

    const/4 v7, 0x1

    move/from16 v0, v22

    if-ne v0, v7, :cond_8

    move-object v7, v2

    move v9, v3

    move v10, v4

    move-wide v11, v5

    move-object/from16 v13, p0

    invoke-virtual/range {v7 .. v13}, Lcom/android/inputmethod/keyboard/PointerTracker;->processMotionEvent(IIIJLcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;)V

    goto :goto_4

    :cond_8
    sget-object v7, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Unknown touch panel behavior: pointer count is "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, v23

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " (old "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, v22

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ")"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    :cond_9
    const/4 v7, 0x2

    if-ne v8, v7, :cond_c

    const/4 v15, 0x0

    :goto_5
    move/from16 v0, v23

    if-ge v15, v0, :cond_d

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v24

    move/from16 v0, v24

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/android/inputmethod/keyboard/PointerTracker;->getPointerTracker(ILcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;)Lcom/android/inputmethod/keyboard/PointerTracker;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mMoreKeysPanel:Lcom/android/inputmethod/keyboard/MoreKeysPanel;

    if-eqz v7, :cond_b

    iget v7, v2, Lcom/android/inputmethod/keyboard/PointerTracker;->mPointerId:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mMoreKeysPanelPointerTrackerId:I

    if-ne v7, v9, :cond_b

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mMoreKeysPanel:Lcom/android/inputmethod/keyboard/MoreKeysPanel;

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/view/MotionEvent;->getX(I)F

    move-result v9

    float-to-int v9, v9

    invoke-interface {v7, v9}, Lcom/android/inputmethod/keyboard/MoreKeysPanel;->translateX(I)I

    move-result v28

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mMoreKeysPanel:Lcom/android/inputmethod/keyboard/MoreKeysPanel;

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/view/MotionEvent;->getY(I)F

    move-result v9

    float-to-int v9, v9

    invoke-interface {v7, v9}, Lcom/android/inputmethod/keyboard/MoreKeysPanel;->translateY(I)I

    move-result v29

    :goto_6
    move/from16 v0, v28

    move/from16 v1, v29

    invoke-virtual {v2, v0, v1, v5, v6}, Lcom/android/inputmethod/keyboard/PointerTracker;->onMoveEvent(IIJ)V

    sget-boolean v7, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->ENABLE_USABILITY_STUDY_LOG:Z

    if-eqz v7, :cond_a

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/view/MotionEvent;->getSize(I)F

    move-result v26

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/view/MotionEvent;->getPressure(I)F

    move-result v25

    invoke-static {}, Lcom/android/inputmethod/latin/Utils$UsabilityStudyLogUtils;->getInstance()Lcom/android/inputmethod/latin/Utils$UsabilityStudyLogUtils;

    move-result-object v7

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "[Move]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, v24

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, v28

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, v29

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, v26

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, v25

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Lcom/android/inputmethod/latin/Utils$UsabilityStudyLogUtils;->write(Ljava/lang/String;)V

    :cond_a
    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_5

    :cond_b
    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/view/MotionEvent;->getX(I)F

    move-result v7

    float-to-int v0, v7

    move/from16 v28, v0

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/view/MotionEvent;->getY(I)F

    move-result v7

    float-to-int v0, v7

    move/from16 v29, v0

    goto/16 :goto_6

    :cond_c
    move/from16 v0, v16

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/android/inputmethod/keyboard/PointerTracker;->getPointerTracker(ILcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;)Lcom/android/inputmethod/keyboard/PointerTracker;

    move-result-object v2

    move-object v7, v2

    move v9, v3

    move v10, v4

    move-wide v11, v5

    move-object/from16 v13, p0

    invoke-virtual/range {v7 .. v13}, Lcom/android/inputmethod/keyboard/PointerTracker;->processMotionEvent(IIIJLcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;)V

    :cond_d
    const/4 v7, 0x1

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public setAltCodeKeyWhileTypingAnimAlpha(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mAltCodeKeyWhileTypingAnimAlpha:I

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->updateAltCodeKeyWhileTyping()V

    return-void
.end method

.method public setDistinctMultitouch(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mHasDistinctMultitouch:Z

    return-void
.end method

.method public setKeyboard(Lcom/android/inputmethod/keyboard/Keyboard;)V
    .locals 5
    .param p1    # Lcom/android/inputmethod/keyboard/Keyboard;

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mKeyTimerHandler:Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/LatinKeyboardView$KeyTimerHandler;->cancelKeyTimers()V

    invoke-super {p0, p1}, Lcom/android/inputmethod/keyboard/KeyboardView;->setKeyboard(Lcom/android/inputmethod/keyboard/Keyboard;)V

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mKeyDetector:Lcom/android/inputmethod/keyboard/KeyDetector;

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->getPaddingLeft()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->getPaddingTop()I

    move-result v3

    neg-int v3, v3

    int-to-float v3, v3

    iget v4, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mVerticalCorrection:F

    add-float/2addr v3, v4

    invoke-virtual {v1, p1, v2, v3}, Lcom/android/inputmethod/keyboard/KeyDetector;->setKeyboard(Lcom/android/inputmethod/keyboard/Keyboard;FF)V

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mKeyDetector:Lcom/android/inputmethod/keyboard/KeyDetector;

    invoke-static {v1}, Lcom/android/inputmethod/keyboard/PointerTracker;->setKeyDetector(Lcom/android/inputmethod/keyboard/KeyDetector;)V

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mTouchScreenRegulator:Lcom/android/inputmethod/keyboard/SuddenJumpingTouchEventHandler;

    invoke-virtual {v1, p1}, Lcom/android/inputmethod/keyboard/SuddenJumpingTouchEventHandler;->setKeyboard(Lcom/android/inputmethod/keyboard/Keyboard;)V

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mMoreKeysPanelCache:Ljava/util/WeakHashMap;

    invoke-virtual {v1}, Ljava/util/WeakHashMap;->clear()V

    const/16 v1, 0x20

    invoke-virtual {p1, v1}, Lcom/android/inputmethod/keyboard/Keyboard;->getKey(I)Lcom/android/inputmethod/keyboard/Key;

    move-result-object v1

    iput-object v1, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mSpaceKey:Lcom/android/inputmethod/keyboard/Key;

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mSpaceKey:Lcom/android/inputmethod/keyboard/Key;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mSpaceKey:Lcom/android/inputmethod/keyboard/Key;

    iget-object v2, p1, Lcom/android/inputmethod/keyboard/Keyboard;->mIconsSet:Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;

    const/16 v3, 0xff

    invoke-virtual {v1, v2, v3}, Lcom/android/inputmethod/keyboard/Key;->getIcon(Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    :goto_0
    iput-object v1, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mSpaceIcon:Landroid/graphics/drawable/Drawable;

    iget v1, p1, Lcom/android/inputmethod/keyboard/Keyboard;->mMostCommonKeyHeight:I

    iget v2, p1, Lcom/android/inputmethod/keyboard/Keyboard;->mVerticalGap:I

    sub-int v0, v1, v2

    int-to-float v1, v0

    iget v2, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mSpacebarTextRatio:F

    mul-float/2addr v1, v2

    iput v1, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mSpacebarTextSize:F

    invoke-static {}, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->getInstance()Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->setKeyboard(Lcom/android/inputmethod/keyboard/Keyboard;)V

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setKeyboardActionListener(Lcom/android/inputmethod/keyboard/KeyboardActionListener;)V
    .locals 0
    .param p1    # Lcom/android/inputmethod/keyboard/KeyboardActionListener;

    iput-object p1, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mKeyboardActionListener:Lcom/android/inputmethod/keyboard/KeyboardActionListener;

    invoke-static {p1}, Lcom/android/inputmethod/keyboard/PointerTracker;->setKeyboardActionListener(Lcom/android/inputmethod/keyboard/KeyboardActionListener;)V

    return-void
.end method

.method public setLanguageOnSpacebarAnimAlpha(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mLanguageOnSpacebarAnimAlpha:I

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mSpaceKey:Lcom/android/inputmethod/keyboard/Key;

    invoke-virtual {p0, v0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->invalidateKey(Lcom/android/inputmethod/keyboard/Key;)V

    return-void
.end method

.method public setProximityCorrectionEnabled(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mKeyDetector:Lcom/android/inputmethod/keyboard/KeyDetector;

    invoke-virtual {v0, p1}, Lcom/android/inputmethod/keyboard/KeyDetector;->setProximityCorrectionEnabled(Z)V

    return-void
.end method

.method public startDisplayLanguageOnSpacebar(ZZZ)V
    .locals 2
    .param p1    # Z
    .param p2    # Z
    .param p3    # Z

    iput-boolean p2, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mNeedsToDisplayLanguage:Z

    iput-boolean p3, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mHasMultipleEnabledIMEsOrSubtypes:Z

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mLanguageOnSpacebarFadeoutAnimator:Landroid/animation/ObjectAnimator;

    if-nez v0, :cond_1

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mNeedsToDisplayLanguage:Z

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mSpaceKey:Lcom/android/inputmethod/keyboard/Key;

    invoke-virtual {p0, v1}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->invalidateKey(Lcom/android/inputmethod/keyboard/Key;)V

    return-void

    :cond_1
    if-eqz p1, :cond_3

    if-eqz p2, :cond_3

    const/16 v1, 0xff

    invoke-virtual {p0, v1}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->setLanguageOnSpacebarAnimAlpha(I)V

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->isStarted()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    :cond_2
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->isStarted()Z

    move-result v1

    if-nez v1, :cond_0

    iget v1, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mLanguageOnSpacebarFinalAlpha:I

    iput v1, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mLanguageOnSpacebarAnimAlpha:I

    goto :goto_0
.end method

.method public updateAutoCorrectionState(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mAutoCorrectionSpacebarLedEnabled:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean p1, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mAutoCorrectionSpacebarLedOn:Z

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->mSpaceKey:Lcom/android/inputmethod/keyboard/Key;

    invoke-virtual {p0, v0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->invalidateKey(Lcom/android/inputmethod/keyboard/Key;)V

    goto :goto_0
.end method

.method public updateShortcutKey(Z)V
    .locals 3
    .param p1    # Z

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->getKeyboard()Lcom/android/inputmethod/keyboard/Keyboard;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v2, -0x6

    invoke-virtual {v0, v2}, Lcom/android/inputmethod/keyboard/Keyboard;->getKey(I)Lcom/android/inputmethod/keyboard/Key;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1, p1}, Lcom/android/inputmethod/keyboard/Key;->setEnabled(Z)V

    invoke-virtual {p0, v1}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->invalidateKey(Lcom/android/inputmethod/keyboard/Key;)V

    goto :goto_0
.end method
