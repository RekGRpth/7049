.class public Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;
.super Lcom/android/inputmethod/keyboard/Keyboard$Params;
.source "MoreKeysKeyboard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MoreKeysKeyboardParams"
.end annotation


# instance fields
.field public mColumnWidth:I

.field public mDividerWidth:I

.field public mIsFixedOrder:Z

.field public mLeftKeys:I

.field public mNumColumns:I

.field public mNumRows:I

.field public mRightKeys:I

.field public mTopKeys:I

.field mTopRowAdjustment:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/Keyboard$Params;-><init>()V

    return-void
.end method

.method private getAutoOrderTopRowAdjustment()I
    .locals 3

    const/4 v2, 0x1

    iget v0, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mNumRows:I

    if-eq v0, v2, :cond_0

    iget v0, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mTopKeys:I

    if-eq v0, v2, :cond_0

    iget v0, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mNumColumns:I

    rem-int/lit8 v0, v0, 0x2

    iget v1, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mTopKeys:I

    rem-int/lit8 v1, v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mLeftKeys:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mRightKeys:I

    if-ne v0, v2, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private getAutomaticColumnPos(I)I
    .locals 8
    .param p1    # I

    iget v7, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mNumColumns:I

    rem-int v0, p1, v7

    iget v7, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mNumColumns:I

    div-int v6, p1, v7

    iget v3, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mLeftKeys:I

    invoke-direct {p0, v6}, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->isTopRow(I)Z

    move-result v7

    if-eqz v7, :cond_0

    iget v7, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mTopRowAdjustment:I

    add-int/2addr v3, v7

    :cond_0
    if-nez v0, :cond_2

    const/4 v4, 0x0

    :cond_1
    :goto_0
    return v4

    :cond_2
    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    :cond_3
    iget v7, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mRightKeys:I

    if-ge v5, v7, :cond_4

    move v4, v5

    add-int/lit8 v5, v5, 0x1

    add-int/lit8 v1, v1, 0x1

    :cond_4
    if-ge v1, v0, :cond_1

    if-ge v2, v3, :cond_5

    add-int/lit8 v2, v2, 0x1

    neg-int v4, v2

    add-int/lit8 v1, v1, 0x1

    :cond_5
    if-lt v1, v0, :cond_3

    goto :goto_0
.end method

.method private getFixedOrderColumnPos(I)I
    .locals 9
    .param p1    # I

    iget v7, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mNumColumns:I

    rem-int v0, p1, v7

    iget v7, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mNumColumns:I

    div-int v6, p1, v7

    invoke-direct {p0, v6}, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->isTopRow(I)Z

    move-result v7

    if-nez v7, :cond_1

    iget v7, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mLeftKeys:I

    sub-int v4, v0, v7

    :cond_0
    :goto_0
    return v4

    :cond_1
    iget v7, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mTopKeys:I

    div-int/lit8 v5, v7, 0x2

    iget v7, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mTopKeys:I

    add-int/lit8 v8, v5, 0x1

    sub-int v1, v7, v8

    sub-int v4, v0, v1

    iget v7, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mLeftKeys:I

    iget v8, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mTopRowAdjustment:I

    add-int v2, v7, v8

    iget v7, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mRightKeys:I

    add-int/lit8 v3, v7, -0x1

    if-lt v3, v5, :cond_2

    if-ge v2, v1, :cond_0

    :cond_2
    if-ge v3, v5, :cond_3

    sub-int v7, v5, v3

    sub-int/2addr v4, v7

    goto :goto_0

    :cond_3
    sub-int v7, v1, v2

    add-int/2addr v4, v7

    goto :goto_0
.end method

.method private getFixedOrderTopRowAdjustment()I
    .locals 3

    const/4 v2, 0x1

    iget v0, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mNumRows:I

    if-eq v0, v2, :cond_0

    iget v0, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mTopKeys:I

    rem-int/lit8 v0, v0, 0x2

    if-eq v0, v2, :cond_0

    iget v0, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mTopKeys:I

    iget v1, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mNumColumns:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mLeftKeys:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mRightKeys:I

    if-ne v0, v2, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private getOptimizedColumns(II)I
    .locals 3
    .param p1    # I
    .param p2    # I

    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    :goto_0
    invoke-static {p1, v0}, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->getTopRowEmptySlots(II)I

    move-result v1

    iget v2, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mNumRows:I

    if-lt v1, v2, :cond_0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    return v0
.end method

.method private static getTopRowEmptySlots(II)I
    .locals 2
    .param p0    # I
    .param p1    # I

    rem-int v0, p0, p1

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    sub-int v1, p1, v0

    goto :goto_0
.end method

.method private isTopRow(I)Z
    .locals 2
    .param p1    # I

    const/4 v0, 0x1

    iget v1, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mNumRows:I

    if-le v1, v0, :cond_0

    iget v1, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mNumRows:I

    add-int/lit8 v1, v1, -0x1

    if-ne p1, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method getColumnPos(I)I
    .locals 1
    .param p1    # I

    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mIsFixedOrder:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->getFixedOrderColumnPos(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->getAutomaticColumnPos(I)I

    move-result v0

    goto :goto_0
.end method

.method public getDefaultKeyCoordX()I
    .locals 2

    iget v0, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mLeftKeys:I

    iget v1, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mColumnWidth:I

    mul-int/2addr v0, v1

    return v0
.end method

.method public getX(II)I
    .locals 3
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0, p1}, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->getColumnPos(I)I

    move-result v1

    iget v2, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mColumnWidth:I

    mul-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->getDefaultKeyCoordX()I

    move-result v2

    add-int v0, v1, v2

    invoke-direct {p0, p2}, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->isTopRow(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mTopRowAdjustment:I

    iget v2, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mColumnWidth:I

    div-int/lit8 v2, v2, 0x2

    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    :cond_0
    return v0
.end method

.method public getY(I)I
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mNumRows:I

    add-int/lit8 v0, v0, -0x1

    sub-int/2addr v0, p1

    iget v1, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mDefaultRowHeight:I

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mTopPadding:I

    add-int/2addr v0, v1

    return v0
.end method

.method public markAsEdgeKey(Lcom/android/inputmethod/keyboard/Key;I)V
    .locals 1
    .param p1    # Lcom/android/inputmethod/keyboard/Key;
    .param p2    # I

    if-nez p2, :cond_0

    invoke-virtual {p1, p0}, Lcom/android/inputmethod/keyboard/Key;->markAsTopEdge(Lcom/android/inputmethod/keyboard/Keyboard$Params;)V

    :cond_0
    invoke-direct {p0, p2}, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->isTopRow(I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1, p0}, Lcom/android/inputmethod/keyboard/Key;->markAsBottomEdge(Lcom/android/inputmethod/keyboard/Keyboard$Params;)V

    :cond_1
    return-void
.end method

.method public setParameters(IIIIIIZI)V
    .locals 13
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # Z
    .param p8    # I

    move/from16 v0, p7

    iput-boolean v0, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mIsFixedOrder:Z

    div-int v10, p6, p3

    if-ge v10, p2, :cond_0

    new-instance v10, Ljava/lang/IllegalArgumentException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Keyboard is too small to hold more keys keyboard: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move/from16 v0, p6

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move/from16 v0, p3

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v10

    :cond_0
    move/from16 v0, p3

    iput v0, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mDefaultKeyWidth:I

    move/from16 v0, p4

    iput v0, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mDefaultRowHeight:I

    add-int v10, p1, p2

    add-int/lit8 v10, v10, -0x1

    div-int v7, v10, p2

    iput v7, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mNumRows:I

    iget-boolean v10, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mIsFixedOrder:Z

    if-eqz v10, :cond_4

    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result v4

    :goto_0
    iput v4, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mNumColumns:I

    rem-int v9, p1, v4

    if-nez v9, :cond_1

    move v9, v4

    :cond_1
    iput v9, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mTopKeys:I

    add-int/lit8 v10, v4, -0x1

    div-int/lit8 v5, v10, 0x2

    sub-int v6, v4, v5

    div-int v2, p5, p3

    sub-int v10, p6, p5

    div-int v3, v10, p3

    if-le v5, v2, :cond_5

    move v1, v2

    sub-int v8, v4, v1

    :goto_1
    if-ne v2, v1, :cond_2

    if-lez v1, :cond_2

    add-int/lit8 v1, v1, -0x1

    add-int/lit8 v8, v8, 0x1

    :cond_2
    add-int/lit8 v10, v8, -0x1

    if-ne v3, v10, :cond_3

    const/4 v10, 0x1

    if-le v8, v10, :cond_3

    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v8, v8, -0x1

    :cond_3
    iput v1, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mLeftKeys:I

    iput v8, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mRightKeys:I

    iget-boolean v10, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mIsFixedOrder:Z

    if-eqz v10, :cond_7

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->getFixedOrderTopRowAdjustment()I

    move-result v10

    :goto_2
    iput v10, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mTopRowAdjustment:I

    move/from16 v0, p8

    iput v0, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mDividerWidth:I

    iget v10, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mDefaultKeyWidth:I

    iget v11, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mDividerWidth:I

    add-int/2addr v10, v11

    iput v10, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mColumnWidth:I

    iget v10, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mNumColumns:I

    iget v11, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mColumnWidth:I

    mul-int/2addr v10, v11

    iget v11, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mDividerWidth:I

    sub-int/2addr v10, v11

    iput v10, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mOccupiedWidth:I

    iput v10, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mBaseWidth:I

    iget v10, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mNumRows:I

    iget v11, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mDefaultRowHeight:I

    mul-int/2addr v10, v11

    iget v11, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mVerticalGap:I

    sub-int/2addr v10, v11

    iget v11, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mTopPadding:I

    add-int/2addr v10, v11

    iget v11, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mBottomPadding:I

    add-int/2addr v10, v11

    iput v10, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mOccupiedHeight:I

    iput v10, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mBaseHeight:I

    return-void

    :cond_4
    invoke-direct {p0, p1, p2}, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->getOptimizedColumns(II)I

    move-result v4

    goto :goto_0

    :cond_5
    add-int/lit8 v10, v3, 0x1

    if-le v6, v10, :cond_6

    add-int/lit8 v8, v3, 0x1

    sub-int v1, v4, v8

    goto :goto_1

    :cond_6
    move v1, v5

    move v8, v6

    goto :goto_1

    :cond_7
    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->getAutoOrderTopRowAdjustment()I

    move-result v10

    goto :goto_2
.end method
