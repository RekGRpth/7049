.class public Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder;
.super Lcom/android/inputmethod/keyboard/Keyboard$Builder;
.source "MoreKeysKeyboard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/keyboard/MoreKeysKeyboard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeyDivider;,
        Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/inputmethod/keyboard/Keyboard$Builder",
        "<",
        "Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;",
        ">;"
    }
.end annotation


# instance fields
.field private final mDivider:Landroid/graphics/drawable/Drawable;

.field private final mParentKey:Lcom/android/inputmethod/keyboard/Key;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/android/inputmethod/keyboard/Key;Lcom/android/inputmethod/keyboard/KeyboardView;)V
    .locals 11
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/android/inputmethod/keyboard/Key;
    .param p3    # Lcom/android/inputmethod/keyboard/KeyboardView;

    const/4 v10, 0x1

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;

    invoke-direct {v1}, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;-><init>()V

    invoke-direct {p0, v0, v1}, Lcom/android/inputmethod/keyboard/Keyboard$Builder;-><init>(Landroid/content/Context;Lcom/android/inputmethod/keyboard/Keyboard$Params;)V

    invoke-virtual {p3}, Lcom/android/inputmethod/keyboard/KeyboardView;->getKeyboard()Lcom/android/inputmethod/keyboard/Keyboard;

    move-result-object v9

    iget v0, v9, Lcom/android/inputmethod/keyboard/Keyboard;->mMoreKeysTemplate:I

    iget-object v1, v9, Lcom/android/inputmethod/keyboard/Keyboard;->mId:Lcom/android/inputmethod/keyboard/KeyboardId;

    invoke-virtual {p0, v0, v1}, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder;->load(ILcom/android/inputmethod/keyboard/KeyboardId;)Lcom/android/inputmethod/keyboard/Keyboard$Builder;

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder;->mParams:Lcom/android/inputmethod/keyboard/Keyboard$Params;

    check-cast v0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;

    iget v1, v9, Lcom/android/inputmethod/keyboard/Keyboard;->mVerticalGap:I

    div-int/lit8 v1, v1, 0x2

    iput v1, v0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mVerticalGap:I

    iput-object p2, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder;->mParentKey:Lcom/android/inputmethod/keyboard/Key;

    invoke-virtual {p3}, Lcom/android/inputmethod/keyboard/KeyboardView;->isKeyPreviewPopupEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/android/inputmethod/keyboard/Key;->noKeyPreview()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p2, Lcom/android/inputmethod/keyboard/Key;->mMoreKeys:[Lcom/android/inputmethod/keyboard/internal/KeySpecParser$MoreKeySpec;

    array-length v0, v0

    if-ne v0, v10, :cond_0

    :goto_0
    if-eqz v10, :cond_1

    iget-object v0, p3, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyPreviewDrawParams:Lcom/android/inputmethod/keyboard/KeyboardView$KeyPreviewDrawParams;

    iget v3, v0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyPreviewDrawParams;->mPreviewVisibleWidth:I

    iget-object v0, p3, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyPreviewDrawParams:Lcom/android/inputmethod/keyboard/KeyboardView$KeyPreviewDrawParams;

    iget v1, v0, Lcom/android/inputmethod/keyboard/KeyboardView$KeyPreviewDrawParams;->mPreviewVisibleHeight:I

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder;->mParams:Lcom/android/inputmethod/keyboard/Keyboard$Params;

    check-cast v0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;

    iget v0, v0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mVerticalGap:I

    add-int v4, v1, v0

    :goto_1
    invoke-virtual {p2}, Lcom/android/inputmethod/keyboard/Key;->needsDividersInMoreKeys()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f020041

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder;->mDivider:Landroid/graphics/drawable/Drawable;

    int-to-float v0, v3

    const v1, 0x3e4ccccd

    mul-float/2addr v0, v1

    float-to-int v8, v0

    :goto_2
    iget-object v0, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder;->mParams:Lcom/android/inputmethod/keyboard/Keyboard$Params;

    check-cast v0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;

    iget-object v1, p2, Lcom/android/inputmethod/keyboard/Key;->mMoreKeys:[Lcom/android/inputmethod/keyboard/internal/KeySpecParser$MoreKeySpec;

    array-length v1, v1

    invoke-virtual {p2}, Lcom/android/inputmethod/keyboard/Key;->getMoreKeysColumn()I

    move-result v2

    iget v5, p2, Lcom/android/inputmethod/keyboard/Key;->mX:I

    iget v6, p2, Lcom/android/inputmethod/keyboard/Key;->mWidth:I

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    invoke-virtual {p3}, Lcom/android/inputmethod/keyboard/KeyboardView;->getMeasuredWidth()I

    move-result v6

    invoke-virtual {p2}, Lcom/android/inputmethod/keyboard/Key;->isFixedColumnOrderMoreKeys()Z

    move-result v7

    invoke-virtual/range {v0 .. v8}, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->setParameters(IIIIIIZI)V

    return-void

    :cond_0
    const/4 v10, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder;->mParams:Lcom/android/inputmethod/keyboard/Keyboard$Params;

    check-cast v0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;

    iget v0, v0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mDefaultKeyWidth:I

    invoke-static {p3, p2, v0}, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder;->getMaxKeyWidth(Lcom/android/inputmethod/keyboard/KeyboardView;Lcom/android/inputmethod/keyboard/Key;I)I

    move-result v3

    iget v4, v9, Lcom/android/inputmethod/keyboard/Keyboard;->mMostCommonKeyHeight:I

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder;->mDivider:Landroid/graphics/drawable/Drawable;

    const/4 v8, 0x0

    goto :goto_2
.end method

.method private static getMaxKeyWidth(Lcom/android/inputmethod/keyboard/KeyboardView;Lcom/android/inputmethod/keyboard/Key;I)I
    .locals 12
    .param p0    # Lcom/android/inputmethod/keyboard/KeyboardView;
    .param p1    # Lcom/android/inputmethod/keyboard/Key;
    .param p2    # I

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0b0004

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v10

    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/Key;->hasLabelsInMoreKeys()Z

    move-result v9

    if-eqz v9, :cond_1

    int-to-float v9, p2

    const v11, 0x3e4ccccd

    mul-float/2addr v9, v11

    :goto_0
    add-float/2addr v9, v10

    float-to-int v5, v9

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->newDefaultLabelPaint()Landroid/graphics/Paint;

    move-result-object v6

    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/Key;->hasLabelsInMoreKeys()Z

    move-result v9

    if-eqz v9, :cond_2

    iget-object v9, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyDrawParams:Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;

    iget v9, v9, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyLabelSize:I

    int-to-float v9, v9

    :goto_1
    invoke-virtual {v6, v9}, Landroid/graphics/Paint;->setTextSize(F)V

    move v4, p2

    iget-object v0, p1, Lcom/android/inputmethod/keyboard/Key;->mMoreKeys:[Lcom/android/inputmethod/keyboard/internal/KeySpecParser$MoreKeySpec;

    array-length v3, v0

    const/4 v1, 0x0

    :goto_2
    if-ge v1, v3, :cond_3

    aget-object v7, v0, v1

    iget-object v2, v7, Lcom/android/inputmethod/keyboard/internal/KeySpecParser$MoreKeySpec;->mLabel:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-static {v2}, Lcom/android/inputmethod/latin/StringUtils;->codePointCount(Ljava/lang/String;)I

    move-result v9

    const/4 v10, 0x1

    if-le v9, v10, :cond_0

    invoke-virtual {p0, v2, v6}, Lcom/android/inputmethod/keyboard/KeyboardView;->getLabelWidth(Ljava/lang/String;Landroid/graphics/Paint;)F

    move-result v9

    float-to-int v9, v9

    add-int v8, v9, v5

    if-ge v4, v8, :cond_0

    move v4, v8

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_1
    const/4 v9, 0x0

    goto :goto_0

    :cond_2
    iget-object v9, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyDrawParams:Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;

    iget v9, v9, Lcom/android/inputmethod/keyboard/KeyboardView$KeyDrawParams;->mKeyLetterSize:I

    int-to-float v9, v9

    goto :goto_1

    :cond_3
    return v4
.end method


# virtual methods
.method public bridge synthetic build()Lcom/android/inputmethod/keyboard/Keyboard;
    .locals 1

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder;->build()Lcom/android/inputmethod/keyboard/MoreKeysKeyboard;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/android/inputmethod/keyboard/MoreKeysKeyboard;
    .locals 14

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder;->mParams:Lcom/android/inputmethod/keyboard/Keyboard$Params;

    check-cast v1, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;

    iget-object v5, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder;->mParentKey:Lcom/android/inputmethod/keyboard/Key;

    invoke-virtual {v5}, Lcom/android/inputmethod/keyboard/Key;->getMoreKeyLabelFlags()I

    move-result v7

    iget-object v5, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder;->mParentKey:Lcom/android/inputmethod/keyboard/Key;

    iget-object v10, v5, Lcom/android/inputmethod/keyboard/Key;->mMoreKeys:[Lcom/android/inputmethod/keyboard/internal/KeySpecParser$MoreKeySpec;

    const/4 v11, 0x0

    :goto_0
    array-length v5, v10

    if-ge v11, v5, :cond_2

    aget-object v2, v10, v11

    iget v5, v1, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mNumColumns:I

    div-int v13, v11, v5

    invoke-virtual {v1, v11, v13}, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->getX(II)I

    move-result v3

    invoke-virtual {v1, v13}, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->getY(I)I

    move-result v4

    new-instance v0, Lcom/android/inputmethod/keyboard/Key;

    iget v5, v1, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mDefaultKeyWidth:I

    iget v6, v1, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mDefaultRowHeight:I

    invoke-direct/range {v0 .. v7}, Lcom/android/inputmethod/keyboard/Key;-><init>(Lcom/android/inputmethod/keyboard/Keyboard$Params;Lcom/android/inputmethod/keyboard/internal/KeySpecParser$MoreKeySpec;IIIII)V

    invoke-virtual {v1, v0, v13}, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->markAsEdgeKey(Lcom/android/inputmethod/keyboard/Key;I)V

    invoke-virtual {v1, v0}, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->onAddKey(Lcom/android/inputmethod/keyboard/Key;)V

    invoke-virtual {v1, v11}, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->getColumnPos(I)I

    move-result v12

    iget v5, v1, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mDividerWidth:I

    if-lez v5, :cond_0

    if-eqz v12, :cond_0

    if-lez v12, :cond_1

    iget v5, v1, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mDividerWidth:I

    sub-int v9, v3, v5

    :goto_1
    new-instance v8, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeyDivider;

    iget-object v5, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder;->mDivider:Landroid/graphics/drawable/Drawable;

    invoke-direct {v8, v1, v5, v9, v4}, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeyDivider;-><init>(Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;Landroid/graphics/drawable/Drawable;II)V

    invoke-virtual {v1, v8}, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->onAddKey(Lcom/android/inputmethod/keyboard/Key;)V

    :cond_0
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    :cond_1
    iget v5, v1, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;->mDefaultKeyWidth:I

    add-int v9, v3, v5

    goto :goto_1

    :cond_2
    new-instance v5, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard;

    invoke-direct {v5, v1}, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard;-><init>(Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder$MoreKeysKeyboardParams;)V

    return-object v5
.end method
