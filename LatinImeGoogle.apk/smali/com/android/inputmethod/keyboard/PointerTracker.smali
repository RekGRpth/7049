.class public Lcom/android/inputmethod/keyboard/PointerTracker;
.super Ljava/lang/Object;
.source "PointerTracker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/inputmethod/keyboard/PointerTracker$TimerProxy;,
        Lcom/android/inputmethod/keyboard/PointerTracker$DrawingProxy;,
        Lcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;
    }
.end annotation


# static fields
.field private static DEBUG_MODE:Z

.field private static final EMPTY_LISTENER:Lcom/android/inputmethod/keyboard/KeyboardActionListener;

.field private static final TAG:Ljava/lang/String;

.field private static sNeedsPhantomSuddenMoveEventHack:Z

.field private static sParams:Lcom/android/inputmethod/keyboard/LatinKeyboardView$PointerTrackerParams;

.field private static sPointerTrackerQueue:Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;

.field private static sTouchNoiseThresholdDistanceSquared:I

.field private static final sTrackers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/keyboard/PointerTracker;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mCurrentKey:Lcom/android/inputmethod/keyboard/Key;

.field private mDownTime:J

.field private mDrawingProxy:Lcom/android/inputmethod/keyboard/PointerTracker$DrawingProxy;

.field private mIgnoreModifierKey:Z

.field private mIsAllowedSlidingKeyInput:Z

.field mIsInSlidingKeyInput:Z

.field private mIsRepeatableKey:Z

.field private mIsShowingMoreKeysPanel:Z

.field private mKeyAlreadyProcessed:Z

.field private mKeyDetector:Lcom/android/inputmethod/keyboard/KeyDetector;

.field private final mKeyPreviewText:Landroid/widget/TextView;

.field private mKeyQuarterWidthSquared:I

.field private mKeyX:I

.field private mKeyY:I

.field private mKeyboard:Lcom/android/inputmethod/keyboard/Keyboard;

.field private mKeyboardLayoutHasBeenChanged:Z

.field private mLastX:I

.field private mLastY:I

.field private mListener:Lcom/android/inputmethod/keyboard/KeyboardActionListener;

.field public final mPointerId:I

.field private mTimerProxy:Lcom/android/inputmethod/keyboard/PointerTracker$TimerProxy;

.field private mUpTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/inputmethod/keyboard/PointerTracker;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/inputmethod/keyboard/PointerTracker;->TAG:Ljava/lang/String;

    sget-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sDBG:Z

    sput-boolean v0, Lcom/android/inputmethod/keyboard/PointerTracker;->DEBUG_MODE:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/inputmethod/keyboard/PointerTracker;->sTrackers:Ljava/util/ArrayList;

    new-instance v0, Lcom/android/inputmethod/keyboard/KeyboardActionListener$Adapter;

    invoke-direct {v0}, Lcom/android/inputmethod/keyboard/KeyboardActionListener$Adapter;-><init>()V

    sput-object v0, Lcom/android/inputmethod/keyboard/PointerTracker;->EMPTY_LISTENER:Lcom/android/inputmethod/keyboard/KeyboardActionListener;

    return-void
.end method

.method public constructor <init>(ILcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;)V
    .locals 1
    .param p1    # I
    .param p2    # Lcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/android/inputmethod/keyboard/PointerTracker;->EMPTY_LISTENER:Lcom/android/inputmethod/keyboard/KeyboardActionListener;

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mListener:Lcom/android/inputmethod/keyboard/KeyboardActionListener;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mCurrentKey:Lcom/android/inputmethod/keyboard/Key;

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput p1, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mPointerId:I

    invoke-interface {p2}, Lcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;->getKeyDetector()Lcom/android/inputmethod/keyboard/KeyDetector;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/inputmethod/keyboard/PointerTracker;->setKeyDetectorInner(Lcom/android/inputmethod/keyboard/KeyDetector;)V

    invoke-interface {p2}, Lcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;->getKeyboardActionListener()Lcom/android/inputmethod/keyboard/KeyboardActionListener;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mListener:Lcom/android/inputmethod/keyboard/KeyboardActionListener;

    invoke-interface {p2}, Lcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;->getDrawingProxy()Lcom/android/inputmethod/keyboard/PointerTracker$DrawingProxy;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mDrawingProxy:Lcom/android/inputmethod/keyboard/PointerTracker$DrawingProxy;

    invoke-interface {p2}, Lcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;->getTimerProxy()Lcom/android/inputmethod/keyboard/PointerTracker$TimerProxy;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mTimerProxy:Lcom/android/inputmethod/keyboard/PointerTracker$TimerProxy;

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mDrawingProxy:Lcom/android/inputmethod/keyboard/PointerTracker$DrawingProxy;

    invoke-interface {v0}, Lcom/android/inputmethod/keyboard/PointerTracker$DrawingProxy;->inflateKeyPreviewText()Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mKeyPreviewText:Landroid/widget/TextView;

    return-void
.end method

.method private callListenerOnCancelInput()V
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mListener:Lcom/android/inputmethod/keyboard/KeyboardActionListener;

    invoke-interface {v0}, Lcom/android/inputmethod/keyboard/KeyboardActionListener;->onCancelInput()V

    return-void
.end method

.method private callListenerOnCodeInput(Lcom/android/inputmethod/keyboard/Key;III)V
    .locals 6
    .param p1    # Lcom/android/inputmethod/keyboard/Key;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-boolean v5, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mIgnoreModifierKey:Z

    if-eqz v5, :cond_1

    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/Key;->isModifier()Z

    move-result v5

    if-eqz v5, :cond_1

    move v2, v3

    :goto_0
    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/Key;->altCodeWhileTyping()Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mTimerProxy:Lcom/android/inputmethod/keyboard/PointerTracker$TimerProxy;

    invoke-interface {v5}, Lcom/android/inputmethod/keyboard/PointerTracker$TimerProxy;->isTypingState()Z

    move-result v5

    if-eqz v5, :cond_2

    move v0, v3

    :goto_1
    if-eqz v0, :cond_3

    iget v1, p1, Lcom/android/inputmethod/keyboard/Key;->mAltCode:I

    :goto_2
    if-eqz v2, :cond_4

    :cond_0
    :goto_3
    return-void

    :cond_1
    move v2, v4

    goto :goto_0

    :cond_2
    move v0, v4

    goto :goto_1

    :cond_3
    move v1, p2

    goto :goto_2

    :cond_4
    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/Key;->isEnabled()Z

    move-result v3

    if-nez v3, :cond_5

    if-eqz v0, :cond_0

    :cond_5
    const/4 v3, -0x3

    if-ne v1, v3, :cond_6

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mListener:Lcom/android/inputmethod/keyboard/KeyboardActionListener;

    iget-object v4, p1, Lcom/android/inputmethod/keyboard/Key;->mOutputText:Ljava/lang/CharSequence;

    invoke-interface {v3, v4}, Lcom/android/inputmethod/keyboard/KeyboardActionListener;->onTextInput(Ljava/lang/CharSequence;)V

    goto :goto_3

    :cond_6
    const/16 v3, -0xb

    if-eq v1, v3, :cond_0

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mListener:Lcom/android/inputmethod/keyboard/KeyboardActionListener;

    invoke-interface {v3, v1, p3, p4}, Lcom/android/inputmethod/keyboard/KeyboardActionListener;->onCodeInput(III)V

    goto :goto_3
.end method

.method private callListenerOnPressAndCheckKeyboardLayoutChange(Lcom/android/inputmethod/keyboard/Key;)Z
    .locals 5
    .param p1    # Lcom/android/inputmethod/keyboard/Key;

    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mIgnoreModifierKey:Z

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/Key;->isModifier()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    move v1, v2

    :cond_0
    :goto_1
    return v1

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/Key;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mListener:Lcom/android/inputmethod/keyboard/KeyboardActionListener;

    iget v4, p1, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    invoke-interface {v3, v4}, Lcom/android/inputmethod/keyboard/KeyboardActionListener;->onPressKey(I)V

    iget-boolean v1, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mKeyboardLayoutHasBeenChanged:Z

    iput-boolean v2, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mKeyboardLayoutHasBeenChanged:Z

    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/Key;->altCodeWhileTyping()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/Key;->isModifier()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mTimerProxy:Lcom/android/inputmethod/keyboard/PointerTracker$TimerProxy;

    invoke-interface {v2}, Lcom/android/inputmethod/keyboard/PointerTracker$TimerProxy;->startTypingStateTimer()V

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method private callListenerOnRelease(Lcom/android/inputmethod/keyboard/Key;IZ)V
    .locals 2
    .param p1    # Lcom/android/inputmethod/keyboard/Key;
    .param p2    # I
    .param p3    # Z

    iget-boolean v1, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mIgnoreModifierKey:Z

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/Key;->isModifier()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/Key;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mListener:Lcom/android/inputmethod/keyboard/KeyboardActionListener;

    invoke-interface {v1, p2, p3}, Lcom/android/inputmethod/keyboard/KeyboardActionListener;->onReleaseKey(IZ)V

    goto :goto_1
.end method

.method private detectAndSendKey(Lcom/android/inputmethod/keyboard/Key;II)V
    .locals 2
    .param p1    # Lcom/android/inputmethod/keyboard/Key;
    .param p2    # I
    .param p3    # I

    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/PointerTracker;->callListenerOnCancelInput()V

    :goto_0
    return-void

    :cond_0
    iget v0, p1, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/android/inputmethod/keyboard/PointerTracker;->callListenerOnCodeInput(Lcom/android/inputmethod/keyboard/Key;III)V

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/android/inputmethod/keyboard/PointerTracker;->callListenerOnRelease(Lcom/android/inputmethod/keyboard/Key;IZ)V

    goto :goto_0
.end method

.method public static dismissAllKeyPreviews()V
    .locals 4

    sget-object v2, Lcom/android/inputmethod/keyboard/PointerTracker;->sTrackers:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/inputmethod/keyboard/PointerTracker;

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/PointerTracker;->getKeyPreviewText()Landroid/widget/TextView;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v2, v1, Lcom/android/inputmethod/keyboard/PointerTracker;->mCurrentKey:Lcom/android/inputmethod/keyboard/Key;

    invoke-direct {v1, v2}, Lcom/android/inputmethod/keyboard/PointerTracker;->setReleasedKeyGraphics(Lcom/android/inputmethod/keyboard/Key;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static getPointerTracker(ILcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;)Lcom/android/inputmethod/keyboard/PointerTracker;
    .locals 4
    .param p0    # I
    .param p1    # Lcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;

    sget-object v2, Lcom/android/inputmethod/keyboard/PointerTracker;->sTrackers:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    if-gt v0, p0, :cond_0

    new-instance v1, Lcom/android/inputmethod/keyboard/PointerTracker;

    invoke-direct {v1, v0, p1}, Lcom/android/inputmethod/keyboard/PointerTracker;-><init>(ILcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;)V

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v2, p0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/inputmethod/keyboard/PointerTracker;

    return-object v3
.end method

.method public static init(ZZ)V
    .locals 1
    .param p0    # Z
    .param p1    # Z

    if-eqz p0, :cond_0

    new-instance v0, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;

    invoke-direct {v0}, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;-><init>()V

    sput-object v0, Lcom/android/inputmethod/keyboard/PointerTracker;->sPointerTrackerQueue:Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;

    :goto_0
    sput-boolean p1, Lcom/android/inputmethod/keyboard/PointerTracker;->sNeedsPhantomSuddenMoveEventHack:Z

    sget-object v0, Lcom/android/inputmethod/keyboard/LatinKeyboardView$PointerTrackerParams;->DEFAULT:Lcom/android/inputmethod/keyboard/LatinKeyboardView$PointerTrackerParams;

    invoke-static {v0}, Lcom/android/inputmethod/keyboard/PointerTracker;->setParameters(Lcom/android/inputmethod/keyboard/LatinKeyboardView$PointerTrackerParams;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    sput-object v0, Lcom/android/inputmethod/keyboard/PointerTracker;->sPointerTrackerQueue:Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;

    goto :goto_0
.end method

.method public static isAnyInSlidingKeyInput()Z
    .locals 1

    sget-object v0, Lcom/android/inputmethod/keyboard/PointerTracker;->sPointerTrackerQueue:Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/inputmethod/keyboard/PointerTracker;->sPointerTrackerQueue:Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;->isAnyInSlidingKeyInput()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isMajorEnoughMoveToBeOnNewKey(IILcom/android/inputmethod/keyboard/Key;)Z
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # Lcom/android/inputmethod/keyboard/Key;

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mKeyDetector:Lcom/android/inputmethod/keyboard/KeyDetector;

    if-nez v3, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "keyboard and/or key detector not set"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v0, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mCurrentKey:Lcom/android/inputmethod/keyboard/Key;

    if-ne p3, v0, :cond_2

    move v1, v2

    :cond_1
    :goto_0
    return v1

    :cond_2
    if-eqz v0, :cond_1

    invoke-virtual {v0, p1, p2}, Lcom/android/inputmethod/keyboard/Key;->squaredDistanceToEdge(II)I

    move-result v3

    iget-object v4, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mKeyDetector:Lcom/android/inputmethod/keyboard/KeyDetector;

    invoke-virtual {v4}, Lcom/android/inputmethod/keyboard/KeyDetector;->getKeyHysteresisDistanceSquared()I

    move-result v4

    if-ge v3, v4, :cond_1

    move v1, v2

    goto :goto_0
.end method

.method private onCancelEventInternal()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mTimerProxy:Lcom/android/inputmethod/keyboard/PointerTracker$TimerProxy;

    invoke-interface {v0}, Lcom/android/inputmethod/keyboard/PointerTracker$TimerProxy;->cancelKeyTimers()V

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mCurrentKey:Lcom/android/inputmethod/keyboard/Key;

    invoke-direct {p0, v0}, Lcom/android/inputmethod/keyboard/PointerTracker;->setReleasedKeyGraphics(Lcom/android/inputmethod/keyboard/Key;)V

    iput-boolean v1, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mIsInSlidingKeyInput:Z

    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mIsShowingMoreKeysPanel:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mDrawingProxy:Lcom/android/inputmethod/keyboard/PointerTracker$DrawingProxy;

    invoke-interface {v0}, Lcom/android/inputmethod/keyboard/PointerTracker$DrawingProxy;->dismissMoreKeysPanel()Z

    iput-boolean v1, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mIsShowingMoreKeysPanel:Z

    :cond_0
    return-void
.end method

.method private onDownEventInternal(IIJ)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # J

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/inputmethod/keyboard/PointerTracker;->onDownKey(IIJ)Lcom/android/inputmethod/keyboard/Key;

    move-result-object v0

    sget-object v1, Lcom/android/inputmethod/keyboard/PointerTracker;->sParams:Lcom/android/inputmethod/keyboard/LatinKeyboardView$PointerTrackerParams;

    iget-boolean v1, v1, Lcom/android/inputmethod/keyboard/LatinKeyboardView$PointerTrackerParams;->mSlidingKeyInputEnabled:Z

    if-nez v1, :cond_1

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/Key;->isModifier()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mKeyDetector:Lcom/android/inputmethod/keyboard/KeyDetector;

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/KeyDetector;->alwaysAllowsSlidingInput()Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_1
    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mIsAllowedSlidingKeyInput:Z

    iput-boolean v2, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mKeyboardLayoutHasBeenChanged:Z

    iput-boolean v2, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mKeyAlreadyProcessed:Z

    iput-boolean v2, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mIsRepeatableKey:Z

    iput-boolean v2, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mIsInSlidingKeyInput:Z

    iput-boolean v2, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mIgnoreModifierKey:Z

    if-eqz v0, :cond_3

    invoke-direct {p0, v0}, Lcom/android/inputmethod/keyboard/PointerTracker;->callListenerOnPressAndCheckKeyboardLayoutChange(Lcom/android/inputmethod/keyboard/Key;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/inputmethod/keyboard/PointerTracker;->onDownKey(IIJ)Lcom/android/inputmethod/keyboard/Key;

    move-result-object v0

    :cond_2
    invoke-direct {p0, v0}, Lcom/android/inputmethod/keyboard/PointerTracker;->startRepeatKey(Lcom/android/inputmethod/keyboard/Key;)V

    invoke-direct {p0, v0}, Lcom/android/inputmethod/keyboard/PointerTracker;->startLongPressTimer(Lcom/android/inputmethod/keyboard/Key;)V

    invoke-direct {p0, v0}, Lcom/android/inputmethod/keyboard/PointerTracker;->setPressedKeyGraphics(Lcom/android/inputmethod/keyboard/Key;)V

    :cond_3
    return-void

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method private onDownKey(IIJ)Lcom/android/inputmethod/keyboard/Key;
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # J

    iput-wide p3, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mDownTime:J

    invoke-direct {p0, p1, p2}, Lcom/android/inputmethod/keyboard/PointerTracker;->onMoveKeyInternal(II)Lcom/android/inputmethod/keyboard/Key;

    move-result-object v0

    invoke-direct {p0, v0, p1, p2}, Lcom/android/inputmethod/keyboard/PointerTracker;->onMoveToNewKey(Lcom/android/inputmethod/keyboard/Key;II)Lcom/android/inputmethod/keyboard/Key;

    move-result-object v0

    return-object v0
.end method

.method private onMoveKey(II)Lcom/android/inputmethod/keyboard/Key;
    .locals 1
    .param p1    # I
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/android/inputmethod/keyboard/PointerTracker;->onMoveKeyInternal(II)Lcom/android/inputmethod/keyboard/Key;

    move-result-object v0

    return-object v0
.end method

.method private onMoveKeyInternal(II)Lcom/android/inputmethod/keyboard/Key;
    .locals 1
    .param p1    # I
    .param p2    # I

    iput p1, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mLastX:I

    iput p2, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mLastY:I

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mKeyDetector:Lcom/android/inputmethod/keyboard/KeyDetector;

    invoke-virtual {v0, p1, p2}, Lcom/android/inputmethod/keyboard/KeyDetector;->detectHitKey(II)Lcom/android/inputmethod/keyboard/Key;

    move-result-object v0

    return-object v0
.end method

.method private onMoveToNewKey(Lcom/android/inputmethod/keyboard/Key;II)Lcom/android/inputmethod/keyboard/Key;
    .locals 0
    .param p1    # Lcom/android/inputmethod/keyboard/Key;
    .param p2    # I
    .param p3    # I

    iput-object p1, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mCurrentKey:Lcom/android/inputmethod/keyboard/Key;

    iput p2, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mKeyX:I

    iput p3, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mKeyY:I

    return-object p1
.end method

.method private onUpEventInternal()V
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mTimerProxy:Lcom/android/inputmethod/keyboard/PointerTracker$TimerProxy;

    invoke-interface {v0}, Lcom/android/inputmethod/keyboard/PointerTracker$TimerProxy;->cancelKeyTimers()V

    iput-boolean v1, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mIsInSlidingKeyInput:Z

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mCurrentKey:Lcom/android/inputmethod/keyboard/Key;

    invoke-direct {p0, v0}, Lcom/android/inputmethod/keyboard/PointerTracker;->setReleasedKeyGraphics(Lcom/android/inputmethod/keyboard/Key;)V

    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mIsShowingMoreKeysPanel:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mDrawingProxy:Lcom/android/inputmethod/keyboard/PointerTracker$DrawingProxy;

    invoke-interface {v0}, Lcom/android/inputmethod/keyboard/PointerTracker$DrawingProxy;->dismissMoreKeysPanel()Z

    iput-boolean v1, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mIsShowingMoreKeysPanel:Z

    :cond_0
    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mKeyAlreadyProcessed:Z

    if-eqz v0, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mIsRepeatableKey:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mCurrentKey:Lcom/android/inputmethod/keyboard/Key;

    iget v1, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mKeyX:I

    iget v2, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mKeyY:I

    invoke-direct {p0, v0, v1, v2}, Lcom/android/inputmethod/keyboard/PointerTracker;->detectAndSendKey(Lcom/android/inputmethod/keyboard/Key;II)V

    goto :goto_0
.end method

.method public static setKeyDetector(Lcom/android/inputmethod/keyboard/KeyDetector;)V
    .locals 3
    .param p0    # Lcom/android/inputmethod/keyboard/KeyDetector;

    sget-object v2, Lcom/android/inputmethod/keyboard/PointerTracker;->sTrackers:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/inputmethod/keyboard/PointerTracker;

    invoke-direct {v1, p0}, Lcom/android/inputmethod/keyboard/PointerTracker;->setKeyDetectorInner(Lcom/android/inputmethod/keyboard/KeyDetector;)V

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/android/inputmethod/keyboard/PointerTracker;->mKeyboardLayoutHasBeenChanged:Z

    goto :goto_0

    :cond_0
    return-void
.end method

.method private setKeyDetectorInner(Lcom/android/inputmethod/keyboard/KeyDetector;)V
    .locals 2
    .param p1    # Lcom/android/inputmethod/keyboard/KeyDetector;

    iput-object p1, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mKeyDetector:Lcom/android/inputmethod/keyboard/KeyDetector;

    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/KeyDetector;->getKeyboard()Lcom/android/inputmethod/keyboard/Keyboard;

    move-result-object v1

    iput-object v1, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mKeyboard:Lcom/android/inputmethod/keyboard/Keyboard;

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mKeyboard:Lcom/android/inputmethod/keyboard/Keyboard;

    iget v1, v1, Lcom/android/inputmethod/keyboard/Keyboard;->mMostCommonKeyWidth:I

    div-int/lit8 v0, v1, 0x4

    mul-int v1, v0, v0

    iput v1, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mKeyQuarterWidthSquared:I

    return-void
.end method

.method public static setKeyboardActionListener(Lcom/android/inputmethod/keyboard/KeyboardActionListener;)V
    .locals 3
    .param p0    # Lcom/android/inputmethod/keyboard/KeyboardActionListener;

    sget-object v2, Lcom/android/inputmethod/keyboard/PointerTracker;->sTrackers:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/inputmethod/keyboard/PointerTracker;

    iput-object p0, v1, Lcom/android/inputmethod/keyboard/PointerTracker;->mListener:Lcom/android/inputmethod/keyboard/KeyboardActionListener;

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static setParameters(Lcom/android/inputmethod/keyboard/LatinKeyboardView$PointerTrackerParams;)V
    .locals 2
    .param p0    # Lcom/android/inputmethod/keyboard/LatinKeyboardView$PointerTrackerParams;

    sput-object p0, Lcom/android/inputmethod/keyboard/PointerTracker;->sParams:Lcom/android/inputmethod/keyboard/LatinKeyboardView$PointerTrackerParams;

    iget v0, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView$PointerTrackerParams;->mTouchNoiseThresholdDistance:F

    iget v1, p0, Lcom/android/inputmethod/keyboard/LatinKeyboardView$PointerTrackerParams;->mTouchNoiseThresholdDistance:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    sput v0, Lcom/android/inputmethod/keyboard/PointerTracker;->sTouchNoiseThresholdDistanceSquared:I

    return-void
.end method

.method private setPressedKeyGraphics(Lcom/android/inputmethod/keyboard/Key;)V
    .locals 12
    .param p1    # Lcom/android/inputmethod/keyboard/Key;

    const/4 v9, 0x1

    const/4 v10, 0x0

    if-nez p1, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/Key;->altCodeWhileTyping()Z

    move-result v11

    if-eqz v11, :cond_5

    iget-object v11, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mTimerProxy:Lcom/android/inputmethod/keyboard/PointerTracker$TimerProxy;

    invoke-interface {v11}, Lcom/android/inputmethod/keyboard/PointerTracker$TimerProxy;->isTypingState()Z

    move-result v11

    if-eqz v11, :cond_5

    move v2, v9

    :goto_0
    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/Key;->isEnabled()Z

    move-result v11

    if-nez v11, :cond_2

    if-eqz v2, :cond_6

    :cond_2
    move v7, v9

    :goto_1
    if-eqz v7, :cond_0

    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/Key;->noKeyPreview()Z

    move-result v9

    if-nez v9, :cond_3

    iget-object v9, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mDrawingProxy:Lcom/android/inputmethod/keyboard/PointerTracker$DrawingProxy;

    invoke-interface {v9, p0}, Lcom/android/inputmethod/keyboard/PointerTracker$DrawingProxy;->showKeyPreview(Lcom/android/inputmethod/keyboard/PointerTracker;)V

    :cond_3
    invoke-direct {p0, p1}, Lcom/android/inputmethod/keyboard/PointerTracker;->updatePressKeyGraphics(Lcom/android/inputmethod/keyboard/Key;)V

    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/Key;->isShift()Z

    move-result v9

    if-eqz v9, :cond_7

    iget-object v9, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mKeyboard:Lcom/android/inputmethod/keyboard/Keyboard;

    iget-object v3, v9, Lcom/android/inputmethod/keyboard/Keyboard;->mShiftKeys:[Lcom/android/inputmethod/keyboard/Key;

    array-length v6, v3

    const/4 v4, 0x0

    :goto_2
    if-ge v4, v6, :cond_7

    aget-object v8, v3, v4

    if-eq v8, p1, :cond_4

    invoke-direct {p0, v8}, Lcom/android/inputmethod/keyboard/PointerTracker;->updatePressKeyGraphics(Lcom/android/inputmethod/keyboard/Key;)V

    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_5
    move v2, v10

    goto :goto_0

    :cond_6
    move v7, v10

    goto :goto_1

    :cond_7
    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/Key;->altCodeWhileTyping()Z

    move-result v9

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mTimerProxy:Lcom/android/inputmethod/keyboard/PointerTracker$TimerProxy;

    invoke-interface {v9}, Lcom/android/inputmethod/keyboard/PointerTracker$TimerProxy;->isTypingState()Z

    move-result v9

    if-eqz v9, :cond_0

    iget v0, p1, Lcom/android/inputmethod/keyboard/Key;->mAltCode:I

    iget-object v9, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mKeyboard:Lcom/android/inputmethod/keyboard/Keyboard;

    invoke-virtual {v9, v0}, Lcom/android/inputmethod/keyboard/Keyboard;->getKey(I)Lcom/android/inputmethod/keyboard/Key;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-direct {p0, v1}, Lcom/android/inputmethod/keyboard/PointerTracker;->updatePressKeyGraphics(Lcom/android/inputmethod/keyboard/Key;)V

    :cond_8
    iget-object v9, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mKeyboard:Lcom/android/inputmethod/keyboard/Keyboard;

    iget-object v3, v9, Lcom/android/inputmethod/keyboard/Keyboard;->mAltCodeKeysWhileTyping:[Lcom/android/inputmethod/keyboard/Key;

    array-length v6, v3

    const/4 v4, 0x0

    :goto_3
    if-ge v4, v6, :cond_0

    aget-object v5, v3, v4

    if-eq v5, p1, :cond_9

    iget v9, v5, Lcom/android/inputmethod/keyboard/Key;->mAltCode:I

    if-ne v9, v0, :cond_9

    invoke-direct {p0, v5}, Lcom/android/inputmethod/keyboard/PointerTracker;->updatePressKeyGraphics(Lcom/android/inputmethod/keyboard/Key;)V

    :cond_9
    add-int/lit8 v4, v4, 0x1

    goto :goto_3
.end method

.method private setReleasedKeyGraphics(Lcom/android/inputmethod/keyboard/Key;)V
    .locals 8
    .param p1    # Lcom/android/inputmethod/keyboard/Key;

    iget-object v7, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mDrawingProxy:Lcom/android/inputmethod/keyboard/PointerTracker$DrawingProxy;

    invoke-interface {v7, p0}, Lcom/android/inputmethod/keyboard/PointerTracker$DrawingProxy;->dismissKeyPreview(Lcom/android/inputmethod/keyboard/PointerTracker;)V

    if-nez p1, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lcom/android/inputmethod/keyboard/PointerTracker;->updateReleaseKeyGraphics(Lcom/android/inputmethod/keyboard/Key;)V

    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/Key;->isShift()Z

    move-result v7

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mKeyboard:Lcom/android/inputmethod/keyboard/Keyboard;

    iget-object v2, v7, Lcom/android/inputmethod/keyboard/Keyboard;->mShiftKeys:[Lcom/android/inputmethod/keyboard/Key;

    array-length v5, v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v5, :cond_3

    aget-object v6, v2, v3

    if-eq v6, p1, :cond_2

    invoke-direct {p0, v6}, Lcom/android/inputmethod/keyboard/PointerTracker;->updateReleaseKeyGraphics(Lcom/android/inputmethod/keyboard/Key;)V

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/Key;->altCodeWhileTyping()Z

    move-result v7

    if-eqz v7, :cond_0

    iget v0, p1, Lcom/android/inputmethod/keyboard/Key;->mAltCode:I

    iget-object v7, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mKeyboard:Lcom/android/inputmethod/keyboard/Keyboard;

    invoke-virtual {v7, v0}, Lcom/android/inputmethod/keyboard/Keyboard;->getKey(I)Lcom/android/inputmethod/keyboard/Key;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-direct {p0, v1}, Lcom/android/inputmethod/keyboard/PointerTracker;->updateReleaseKeyGraphics(Lcom/android/inputmethod/keyboard/Key;)V

    :cond_4
    iget-object v7, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mKeyboard:Lcom/android/inputmethod/keyboard/Keyboard;

    iget-object v2, v7, Lcom/android/inputmethod/keyboard/Keyboard;->mAltCodeKeysWhileTyping:[Lcom/android/inputmethod/keyboard/Key;

    array-length v5, v2

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v5, :cond_0

    aget-object v4, v2, v3

    if-eq v4, p1, :cond_5

    iget v7, v4, Lcom/android/inputmethod/keyboard/Key;->mAltCode:I

    if-ne v7, v0, :cond_5

    invoke-direct {p0, v4}, Lcom/android/inputmethod/keyboard/PointerTracker;->updateReleaseKeyGraphics(Lcom/android/inputmethod/keyboard/Key;)V

    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method private startLongPressTimer(Lcom/android/inputmethod/keyboard/Key;)V
    .locals 1
    .param p1    # Lcom/android/inputmethod/keyboard/Key;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/Key;->isLongPressEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mTimerProxy:Lcom/android/inputmethod/keyboard/PointerTracker$TimerProxy;

    invoke-interface {v0, p0}, Lcom/android/inputmethod/keyboard/PointerTracker$TimerProxy;->startLongPressTimer(Lcom/android/inputmethod/keyboard/PointerTracker;)V

    :cond_0
    return-void
.end method

.method private startRepeatKey(Lcom/android/inputmethod/keyboard/Key;)V
    .locals 1
    .param p1    # Lcom/android/inputmethod/keyboard/Key;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/Key;->isRepeatable()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/android/inputmethod/keyboard/PointerTracker;->onRegisterKey(Lcom/android/inputmethod/keyboard/Key;)V

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mTimerProxy:Lcom/android/inputmethod/keyboard/PointerTracker$TimerProxy;

    invoke-interface {v0, p0}, Lcom/android/inputmethod/keyboard/PointerTracker$TimerProxy;->startKeyRepeatTimer(Lcom/android/inputmethod/keyboard/PointerTracker;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mIsRepeatableKey:Z

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mIsRepeatableKey:Z

    goto :goto_0
.end method

.method private startSlidingKeyInput(Lcom/android/inputmethod/keyboard/Key;)V
    .locals 1
    .param p1    # Lcom/android/inputmethod/keyboard/Key;

    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mIsInSlidingKeyInput:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/Key;->isModifier()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mIgnoreModifierKey:Z

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mIsInSlidingKeyInput:Z

    return-void
.end method

.method private updatePressKeyGraphics(Lcom/android/inputmethod/keyboard/Key;)V
    .locals 1
    .param p1    # Lcom/android/inputmethod/keyboard/Key;

    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/Key;->onPressed()V

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mDrawingProxy:Lcom/android/inputmethod/keyboard/PointerTracker$DrawingProxy;

    invoke-interface {v0, p1}, Lcom/android/inputmethod/keyboard/PointerTracker$DrawingProxy;->invalidateKey(Lcom/android/inputmethod/keyboard/Key;)V

    return-void
.end method

.method private updateReleaseKeyGraphics(Lcom/android/inputmethod/keyboard/Key;)V
    .locals 1
    .param p1    # Lcom/android/inputmethod/keyboard/Key;

    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/Key;->onReleased()V

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mDrawingProxy:Lcom/android/inputmethod/keyboard/PointerTracker$DrawingProxy;

    invoke-interface {v0, p1}, Lcom/android/inputmethod/keyboard/PointerTracker$DrawingProxy;->invalidateKey(Lcom/android/inputmethod/keyboard/Key;)V

    return-void
.end method


# virtual methods
.method public getKey()Lcom/android/inputmethod/keyboard/Key;
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mCurrentKey:Lcom/android/inputmethod/keyboard/Key;

    return-object v0
.end method

.method public getKeyOn(II)Lcom/android/inputmethod/keyboard/Key;
    .locals 1
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mKeyDetector:Lcom/android/inputmethod/keyboard/KeyDetector;

    invoke-virtual {v0, p1, p2}, Lcom/android/inputmethod/keyboard/KeyDetector;->detectHitKey(II)Lcom/android/inputmethod/keyboard/Key;

    move-result-object v0

    return-object v0
.end method

.method public getKeyPreviewText()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mKeyPreviewText:Landroid/widget/TextView;

    return-object v0
.end method

.method public getLastX()I
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mLastX:I

    return v0
.end method

.method public getLastY()I
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mLastY:I

    return v0
.end method

.method public isInSlidingKeyInput()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mIsInSlidingKeyInput:Z

    return v0
.end method

.method public isModifier()Z
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mCurrentKey:Lcom/android/inputmethod/keyboard/Key;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mCurrentKey:Lcom/android/inputmethod/keyboard/Key;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/Key;->isModifier()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCancelEvent(IIJ)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # J

    sget-object v0, Lcom/android/inputmethod/keyboard/PointerTracker;->sPointerTrackerQueue:Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0, p3, p4}, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;->releaseAllPointersExcept(Lcom/android/inputmethod/keyboard/PointerTracker;J)V

    invoke-virtual {v0, p0}, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;->remove(Lcom/android/inputmethod/keyboard/PointerTracker;)V

    :cond_0
    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/PointerTracker;->onCancelEventInternal()V

    return-void
.end method

.method public onDownEvent(IIJLcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;)V
    .locals 10
    .param p1    # I
    .param p2    # I
    .param p3    # J
    .param p5    # Lcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;

    invoke-interface {p5}, Lcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;->getDrawingProxy()Lcom/android/inputmethod/keyboard/PointerTracker$DrawingProxy;

    move-result-object v7

    iput-object v7, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mDrawingProxy:Lcom/android/inputmethod/keyboard/PointerTracker$DrawingProxy;

    invoke-interface {p5}, Lcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;->getTimerProxy()Lcom/android/inputmethod/keyboard/PointerTracker$TimerProxy;

    move-result-object v7

    iput-object v7, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mTimerProxy:Lcom/android/inputmethod/keyboard/PointerTracker$TimerProxy;

    invoke-interface {p5}, Lcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;->getKeyboardActionListener()Lcom/android/inputmethod/keyboard/KeyboardActionListener;

    move-result-object v7

    invoke-static {v7}, Lcom/android/inputmethod/keyboard/PointerTracker;->setKeyboardActionListener(Lcom/android/inputmethod/keyboard/KeyboardActionListener;)V

    invoke-interface {p5}, Lcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;->getKeyDetector()Lcom/android/inputmethod/keyboard/KeyDetector;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/android/inputmethod/keyboard/PointerTracker;->setKeyDetectorInner(Lcom/android/inputmethod/keyboard/KeyDetector;)V

    iget-wide v7, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mUpTime:J

    sub-long v0, p3, v7

    sget-object v7, Lcom/android/inputmethod/keyboard/PointerTracker;->sParams:Lcom/android/inputmethod/keyboard/LatinKeyboardView$PointerTrackerParams;

    iget v7, v7, Lcom/android/inputmethod/keyboard/LatinKeyboardView$PointerTrackerParams;->mTouchNoiseThresholdTime:I

    int-to-long v7, v7

    cmp-long v7, v0, v7

    if-gez v7, :cond_1

    iget v7, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mLastX:I

    sub-int v3, p1, v7

    iget v7, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mLastY:I

    sub-int v4, p2, v7

    mul-int v7, v3, v3

    mul-int v8, v4, v4

    add-int v2, v7, v8

    sget v7, Lcom/android/inputmethod/keyboard/PointerTracker;->sTouchNoiseThresholdDistanceSquared:I

    if-ge v2, v7, :cond_1

    sget-boolean v7, Lcom/android/inputmethod/keyboard/PointerTracker;->DEBUG_MODE:Z

    if-eqz v7, :cond_0

    sget-object v7, Lcom/android/inputmethod/keyboard/PointerTracker;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onDownEvent: ignore potential noise: time="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " distance="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mKeyAlreadyProcessed:Z

    :goto_0
    return-void

    :cond_1
    sget-object v6, Lcom/android/inputmethod/keyboard/PointerTracker;->sPointerTrackerQueue:Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;

    if-eqz v6, :cond_3

    invoke-virtual {p0, p1, p2}, Lcom/android/inputmethod/keyboard/PointerTracker;->getKeyOn(II)Lcom/android/inputmethod/keyboard/Key;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v5}, Lcom/android/inputmethod/keyboard/Key;->isModifier()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {v6, p3, p4}, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;->releaseAllPointers(J)V

    :cond_2
    invoke-virtual {v6, p0}, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;->add(Lcom/android/inputmethod/keyboard/PointerTracker;)V

    :cond_3
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/inputmethod/keyboard/PointerTracker;->onDownEventInternal(IIJ)V

    goto :goto_0
.end method

.method public onLongPressed()V
    .locals 2

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mKeyAlreadyProcessed:Z

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mCurrentKey:Lcom/android/inputmethod/keyboard/Key;

    invoke-direct {p0, v1}, Lcom/android/inputmethod/keyboard/PointerTracker;->setReleasedKeyGraphics(Lcom/android/inputmethod/keyboard/Key;)V

    sget-object v0, Lcom/android/inputmethod/keyboard/PointerTracker;->sPointerTrackerQueue:Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;->remove(Lcom/android/inputmethod/keyboard/PointerTracker;)V

    :cond_0
    return-void
.end method

.method public onMoveEvent(IIJ)V
    .locals 12
    .param p1    # I
    .param p2    # I
    .param p3    # J

    iget-boolean v7, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mKeyAlreadyProcessed:Z

    if-eqz v7, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v4, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mLastX:I

    iget v5, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mLastY:I

    iget-object v6, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mCurrentKey:Lcom/android/inputmethod/keyboard/Key;

    invoke-direct {p0, p1, p2}, Lcom/android/inputmethod/keyboard/PointerTracker;->onMoveKey(II)Lcom/android/inputmethod/keyboard/Key;

    move-result-object v2

    if-eqz v2, :cond_8

    if-nez v6, :cond_3

    invoke-direct {p0, v2}, Lcom/android/inputmethod/keyboard/PointerTracker;->callListenerOnPressAndCheckKeyboardLayoutChange(Lcom/android/inputmethod/keyboard/Key;)Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-direct {p0, p1, p2}, Lcom/android/inputmethod/keyboard/PointerTracker;->onMoveKey(II)Lcom/android/inputmethod/keyboard/Key;

    move-result-object v2

    :cond_2
    invoke-direct {p0, v2, p1, p2}, Lcom/android/inputmethod/keyboard/PointerTracker;->onMoveToNewKey(Lcom/android/inputmethod/keyboard/Key;II)Lcom/android/inputmethod/keyboard/Key;

    invoke-direct {p0, v2}, Lcom/android/inputmethod/keyboard/PointerTracker;->startLongPressTimer(Lcom/android/inputmethod/keyboard/Key;)V

    invoke-direct {p0, v2}, Lcom/android/inputmethod/keyboard/PointerTracker;->setPressedKeyGraphics(Lcom/android/inputmethod/keyboard/Key;)V

    goto :goto_0

    :cond_3
    invoke-direct {p0, p1, p2, v2}, Lcom/android/inputmethod/keyboard/PointerTracker;->isMajorEnoughMoveToBeOnNewKey(IILcom/android/inputmethod/keyboard/Key;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-direct {p0, v6}, Lcom/android/inputmethod/keyboard/PointerTracker;->setReleasedKeyGraphics(Lcom/android/inputmethod/keyboard/Key;)V

    iget v7, v6, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    const/4 v8, 0x1

    invoke-direct {p0, v6, v7, v8}, Lcom/android/inputmethod/keyboard/PointerTracker;->callListenerOnRelease(Lcom/android/inputmethod/keyboard/Key;IZ)V

    invoke-direct {p0, v6}, Lcom/android/inputmethod/keyboard/PointerTracker;->startSlidingKeyInput(Lcom/android/inputmethod/keyboard/Key;)V

    iget-object v7, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mTimerProxy:Lcom/android/inputmethod/keyboard/PointerTracker$TimerProxy;

    invoke-interface {v7}, Lcom/android/inputmethod/keyboard/PointerTracker$TimerProxy;->cancelKeyTimers()V

    invoke-direct {p0, v2}, Lcom/android/inputmethod/keyboard/PointerTracker;->startRepeatKey(Lcom/android/inputmethod/keyboard/Key;)V

    iget-boolean v7, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mIsAllowedSlidingKeyInput:Z

    if-eqz v7, :cond_5

    invoke-direct {p0, v2}, Lcom/android/inputmethod/keyboard/PointerTracker;->callListenerOnPressAndCheckKeyboardLayoutChange(Lcom/android/inputmethod/keyboard/Key;)Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-direct {p0, p1, p2}, Lcom/android/inputmethod/keyboard/PointerTracker;->onMoveKey(II)Lcom/android/inputmethod/keyboard/Key;

    move-result-object v2

    :cond_4
    invoke-direct {p0, v2, p1, p2}, Lcom/android/inputmethod/keyboard/PointerTracker;->onMoveToNewKey(Lcom/android/inputmethod/keyboard/Key;II)Lcom/android/inputmethod/keyboard/Key;

    invoke-direct {p0, v2}, Lcom/android/inputmethod/keyboard/PointerTracker;->startLongPressTimer(Lcom/android/inputmethod/keyboard/Key;)V

    invoke-direct {p0, v2}, Lcom/android/inputmethod/keyboard/PointerTracker;->setPressedKeyGraphics(Lcom/android/inputmethod/keyboard/Key;)V

    goto :goto_0

    :cond_5
    sub-int v0, p1, v4

    sub-int v1, p2, v5

    mul-int v7, v0, v0

    mul-int v8, v1, v1

    add-int v3, v7, v8

    sget-boolean v7, Lcom/android/inputmethod/keyboard/PointerTracker;->sNeedsPhantomSuddenMoveEventHack:Z

    if-eqz v7, :cond_7

    iget v7, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mKeyQuarterWidthSquared:I

    if-lt v3, v7, :cond_7

    sget-boolean v7, Lcom/android/inputmethod/keyboard/PointerTracker;->DEBUG_MODE:Z

    if-eqz v7, :cond_6

    sget-object v7, Lcom/android/inputmethod/keyboard/PointerTracker;->TAG:Ljava/lang/String;

    const-string v8, "onMoveEvent: phantom sudden move event is translated to up[%d,%d]/down[%d,%d] events"

    const/4 v9, 0x4

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/PointerTracker;->onUpEventInternal()V

    invoke-direct/range {p0 .. p4}, Lcom/android/inputmethod/keyboard/PointerTracker;->onDownEventInternal(IIJ)V

    goto/16 :goto_0

    :cond_7
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mKeyAlreadyProcessed:Z

    invoke-direct {p0, v6}, Lcom/android/inputmethod/keyboard/PointerTracker;->setReleasedKeyGraphics(Lcom/android/inputmethod/keyboard/Key;)V

    goto/16 :goto_0

    :cond_8
    if-eqz v6, :cond_0

    invoke-direct {p0, p1, p2, v2}, Lcom/android/inputmethod/keyboard/PointerTracker;->isMajorEnoughMoveToBeOnNewKey(IILcom/android/inputmethod/keyboard/Key;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-direct {p0, v6}, Lcom/android/inputmethod/keyboard/PointerTracker;->setReleasedKeyGraphics(Lcom/android/inputmethod/keyboard/Key;)V

    iget v7, v6, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    const/4 v8, 0x1

    invoke-direct {p0, v6, v7, v8}, Lcom/android/inputmethod/keyboard/PointerTracker;->callListenerOnRelease(Lcom/android/inputmethod/keyboard/Key;IZ)V

    invoke-direct {p0, v6}, Lcom/android/inputmethod/keyboard/PointerTracker;->startSlidingKeyInput(Lcom/android/inputmethod/keyboard/Key;)V

    iget-object v7, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mTimerProxy:Lcom/android/inputmethod/keyboard/PointerTracker$TimerProxy;

    invoke-interface {v7}, Lcom/android/inputmethod/keyboard/PointerTracker$TimerProxy;->cancelLongPressTimer()V

    iget-boolean v7, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mIsAllowedSlidingKeyInput:Z

    if-eqz v7, :cond_9

    invoke-direct {p0, v2, p1, p2}, Lcom/android/inputmethod/keyboard/PointerTracker;->onMoveToNewKey(Lcom/android/inputmethod/keyboard/Key;II)Lcom/android/inputmethod/keyboard/Key;

    goto/16 :goto_0

    :cond_9
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mKeyAlreadyProcessed:Z

    goto/16 :goto_0
.end method

.method public onPhantomUpEvent(IIJ)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # J

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/PointerTracker;->onUpEventInternal()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mKeyAlreadyProcessed:Z

    return-void
.end method

.method public onRegisterKey(Lcom/android/inputmethod/keyboard/Key;)V
    .locals 2
    .param p1    # Lcom/android/inputmethod/keyboard/Key;

    if-eqz p1, :cond_0

    iget v0, p1, Lcom/android/inputmethod/keyboard/Key;->mX:I

    iget v1, p1, Lcom/android/inputmethod/keyboard/Key;->mY:I

    invoke-direct {p0, p1, v0, v1}, Lcom/android/inputmethod/keyboard/PointerTracker;->detectAndSendKey(Lcom/android/inputmethod/keyboard/Key;II)V

    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/Key;->altCodeWhileTyping()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/Key;->isModifier()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mTimerProxy:Lcom/android/inputmethod/keyboard/PointerTracker$TimerProxy;

    invoke-interface {v0}, Lcom/android/inputmethod/keyboard/PointerTracker$TimerProxy;->startTypingStateTimer()V

    :cond_0
    return-void
.end method

.method public onShowMoreKeysPanel(IILcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;)V
    .locals 6
    .param p1    # I
    .param p2    # I
    .param p3    # Lcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/PointerTracker;->onLongPressed()V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/android/inputmethod/keyboard/PointerTracker;->onDownEvent(IIJLcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mIsShowingMoreKeysPanel:Z

    return-void
.end method

.method public onUpEvent(IIJ)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # J

    sget-object v0, Lcom/android/inputmethod/keyboard/PointerTracker;->sPointerTrackerQueue:Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mCurrentKey:Lcom/android/inputmethod/keyboard/Key;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/PointerTracker;->mCurrentKey:Lcom/android/inputmethod/keyboard/Key;

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/Key;->isModifier()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0, p0, p3, p4}, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;->releaseAllPointersExcept(Lcom/android/inputmethod/keyboard/PointerTracker;J)V

    :goto_0
    invoke-virtual {v0, p0}, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;->remove(Lcom/android/inputmethod/keyboard/PointerTracker;)V

    :cond_0
    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/PointerTracker;->onUpEventInternal()V

    return-void

    :cond_1
    invoke-virtual {v0, p0, p3, p4}, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;->releaseAllPointersOlderThan(Lcom/android/inputmethod/keyboard/PointerTracker;J)V

    goto :goto_0
.end method

.method public processMotionEvent(IIIJLcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;)V
    .locals 6
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # J
    .param p6    # Lcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    move-object v0, p0

    move v1, p2

    move v2, p3

    move-wide v3, p4

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Lcom/android/inputmethod/keyboard/PointerTracker;->onDownEvent(IIJLcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0, p2, p3, p4, p5}, Lcom/android/inputmethod/keyboard/PointerTracker;->onUpEvent(IIJ)V

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0, p2, p3, p4, p5}, Lcom/android/inputmethod/keyboard/PointerTracker;->onMoveEvent(IIJ)V

    goto :goto_0

    :pswitch_4
    invoke-virtual {p0, p2, p3, p4, p5}, Lcom/android/inputmethod/keyboard/PointerTracker;->onCancelEvent(IIJ)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
