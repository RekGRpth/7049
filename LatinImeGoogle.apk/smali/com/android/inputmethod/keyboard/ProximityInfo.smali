.class public Lcom/android/inputmethod/keyboard/ProximityInfo;
.super Ljava/lang/Object;
.source "ProximityInfo.java"


# static fields
.field private static final EMPTY_KEY_ARRAY:[Lcom/android/inputmethod/keyboard/Key;

.field private static SEARCH_DISTANCE:F


# instance fields
.field private final mCellHeight:I

.field private final mCellWidth:I

.field private final mGridHeight:I

.field private final mGridNeighbors:[[Lcom/android/inputmethod/keyboard/Key;

.field private final mGridSize:I

.field private final mGridWidth:I

.field private final mKeyHeight:I

.field private final mKeyboardHeight:I

.field private final mKeyboardMinWidth:I

.field private final mKeys:[Lcom/android/inputmethod/keyboard/Key;

.field private final mLocaleStr:Ljava/lang/String;

.field private final mMostCommonKeyWidth:I

.field private mNativeProximityInfo:J

.field private final mTouchPositionCorrection:Lcom/android/inputmethod/keyboard/Keyboard$Params$TouchPositionCorrection;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const v0, 0x3f99999a

    sput v0, Lcom/android/inputmethod/keyboard/ProximityInfo;->SEARCH_DISTANCE:F

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/android/inputmethod/keyboard/Key;

    sput-object v0, Lcom/android/inputmethod/keyboard/ProximityInfo;->EMPTY_KEY_ARRAY:[Lcom/android/inputmethod/keyboard/Key;

    invoke-static {}, Lcom/android/inputmethod/latin/JniUtils;->loadNativeLibrary()V

    return-void
.end method

.method public constructor <init>(Lcom/android/inputmethod/keyboard/ProximityInfo;)V
    .locals 2
    .param p1    # Lcom/android/inputmethod/keyboard/ProximityInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p1, Lcom/android/inputmethod/keyboard/ProximityInfo;->mLocaleStr:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mLocaleStr:Ljava/lang/String;

    iget v0, p1, Lcom/android/inputmethod/keyboard/ProximityInfo;->mGridWidth:I

    iput v0, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mGridWidth:I

    iget v0, p1, Lcom/android/inputmethod/keyboard/ProximityInfo;->mGridHeight:I

    iput v0, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mGridHeight:I

    iget v0, p1, Lcom/android/inputmethod/keyboard/ProximityInfo;->mGridSize:I

    iput v0, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mGridSize:I

    iget v0, p1, Lcom/android/inputmethod/keyboard/ProximityInfo;->mCellWidth:I

    iput v0, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mCellWidth:I

    iget v0, p1, Lcom/android/inputmethod/keyboard/ProximityInfo;->mCellHeight:I

    iput v0, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mCellHeight:I

    iget v0, p1, Lcom/android/inputmethod/keyboard/ProximityInfo;->mKeyboardMinWidth:I

    iput v0, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mKeyboardMinWidth:I

    iget v0, p1, Lcom/android/inputmethod/keyboard/ProximityInfo;->mKeyboardHeight:I

    iput v0, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mKeyboardHeight:I

    iget v0, p1, Lcom/android/inputmethod/keyboard/ProximityInfo;->mKeyHeight:I

    iput v0, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mKeyHeight:I

    iget v0, p1, Lcom/android/inputmethod/keyboard/ProximityInfo;->mMostCommonKeyWidth:I

    iput v0, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mMostCommonKeyWidth:I

    iget-object v0, p1, Lcom/android/inputmethod/keyboard/ProximityInfo;->mKeys:[Lcom/android/inputmethod/keyboard/Key;

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mKeys:[Lcom/android/inputmethod/keyboard/Key;

    iget-object v0, p1, Lcom/android/inputmethod/keyboard/ProximityInfo;->mTouchPositionCorrection:Lcom/android/inputmethod/keyboard/Keyboard$Params$TouchPositionCorrection;

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mTouchPositionCorrection:Lcom/android/inputmethod/keyboard/Keyboard$Params$TouchPositionCorrection;

    iget v0, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mGridSize:I

    new-array v0, v0, [[Lcom/android/inputmethod/keyboard/Key;

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mGridNeighbors:[[Lcom/android/inputmethod/keyboard/Key;

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/ProximityInfo;->computeNearestNeighbors()V

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/ProximityInfo;->createNativeProximityInfo()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mNativeProximityInfo:J

    return-void
.end method

.method constructor <init>(Ljava/lang/String;IIIIII[Lcom/android/inputmethod/keyboard/Key;Lcom/android/inputmethod/keyboard/Keyboard$Params$TouchPositionCorrection;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I
    .param p8    # [Lcom/android/inputmethod/keyboard/Key;
    .param p9    # Lcom/android/inputmethod/keyboard/Keyboard$Params$TouchPositionCorrection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, ""

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mLocaleStr:Ljava/lang/String;

    :goto_0
    iput p2, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mGridWidth:I

    iput p3, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mGridHeight:I

    iget v0, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mGridWidth:I

    iget v1, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mGridHeight:I

    mul-int/2addr v0, v1

    iput v0, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mGridSize:I

    iget v0, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mGridWidth:I

    add-int/2addr v0, p4

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mGridWidth:I

    div-int/2addr v0, v1

    iput v0, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mCellWidth:I

    iget v0, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mGridHeight:I

    add-int/2addr v0, p5

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mGridHeight:I

    div-int/2addr v0, v1

    iput v0, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mCellHeight:I

    iput p4, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mKeyboardMinWidth:I

    iput p5, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mKeyboardHeight:I

    iput p7, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mKeyHeight:I

    iput p6, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mMostCommonKeyWidth:I

    iput-object p8, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mKeys:[Lcom/android/inputmethod/keyboard/Key;

    iput-object p9, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mTouchPositionCorrection:Lcom/android/inputmethod/keyboard/Keyboard$Params$TouchPositionCorrection;

    iget v0, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mGridSize:I

    new-array v0, v0, [[Lcom/android/inputmethod/keyboard/Key;

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mGridNeighbors:[[Lcom/android/inputmethod/keyboard/Key;

    if-eqz p4, :cond_0

    if-nez p5, :cond_2

    :cond_0
    :goto_1
    return-void

    :cond_1
    iput-object p1, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mLocaleStr:Ljava/lang/String;

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/ProximityInfo;->computeNearestNeighbors()V

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/ProximityInfo;->createNativeProximityInfo()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mNativeProximityInfo:J

    goto :goto_1
.end method

.method private computeNearestNeighbors()V
    .locals 22

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mMostCommonKeyWidth:I

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mKeys:[Lcom/android/inputmethod/keyboard/Key;

    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    move-object v1, v12

    array-length v13, v1

    const/4 v9, 0x0

    :goto_0
    if-ge v9, v13, :cond_0

    aget-object v10, v1, v9

    iget v0, v10, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    move/from16 v19, v0

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v11, v0, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    :cond_0
    int-to-float v0, v6

    move/from16 v19, v0

    sget v20, Lcom/android/inputmethod/keyboard/ProximityInfo;->SEARCH_DISTANCE:F

    mul-float v19, v19, v20

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v16, v0

    mul-int v15, v16, v16

    array-length v0, v12

    move/from16 v19, v0

    move/from16 v0, v19

    new-array v14, v0, [Lcom/android/inputmethod/keyboard/Key;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mGridWidth:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mCellWidth:I

    move/from16 v20, v0

    mul-int v8, v19, v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mGridHeight:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mCellHeight:I

    move/from16 v20, v0

    mul-int v7, v19, v20

    const/16 v17, 0x0

    :goto_1
    move/from16 v0, v17

    if-ge v0, v8, :cond_4

    const/16 v18, 0x0

    :goto_2
    move/from16 v0, v18

    if-ge v0, v7, :cond_3

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mCellWidth:I

    move/from16 v19, v0

    div-int/lit8 v19, v19, 0x2

    add-int v2, v17, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mCellHeight:I

    move/from16 v19, v0

    div-int/lit8 v19, v19, 0x2

    add-int v3, v18, v19

    const/4 v4, 0x0

    move-object v1, v12

    array-length v13, v1

    const/4 v9, 0x0

    move v5, v4

    :goto_3
    if-ge v9, v13, :cond_2

    aget-object v10, v1, v9

    invoke-virtual {v10}, Lcom/android/inputmethod/keyboard/Key;->isSpacer()Z

    move-result v19

    if-eqz v19, :cond_1

    move v4, v5

    :goto_4
    add-int/lit8 v9, v9, 0x1

    move v5, v4

    goto :goto_3

    :cond_1
    invoke-virtual {v10, v2, v3}, Lcom/android/inputmethod/keyboard/Key;->squaredDistanceToEdge(II)I

    move-result v19

    move/from16 v0, v19

    if-ge v0, v15, :cond_5

    add-int/lit8 v4, v5, 0x1

    aput-object v10, v14, v5

    goto :goto_4

    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mGridNeighbors:[[Lcom/android/inputmethod/keyboard/Key;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mCellHeight:I

    move/from16 v19, v0

    div-int v19, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mGridWidth:I

    move/from16 v21, v0

    mul-int v19, v19, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mCellWidth:I

    move/from16 v21, v0

    div-int v21, v17, v21

    add-int v21, v21, v19

    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-static {v14, v0, v5}, Ljava/util/Arrays;->copyOfRange([Ljava/lang/Object;II)[Ljava/lang/Object;

    move-result-object v19

    check-cast v19, [Lcom/android/inputmethod/keyboard/Key;

    aput-object v19, v20, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mCellHeight:I

    move/from16 v19, v0

    add-int v18, v18, v19

    goto :goto_2

    :cond_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mCellWidth:I

    move/from16 v19, v0

    add-int v17, v17, v19

    goto :goto_1

    :cond_4
    return-void

    :cond_5
    move v4, v5

    goto :goto_4
.end method

.method public static createDummyProximityInfo()Lcom/android/inputmethod/keyboard/ProximityInfo;
    .locals 10

    const/4 v2, 0x1

    new-instance v0, Lcom/android/inputmethod/keyboard/ProximityInfo;

    const-string v1, ""

    sget-object v8, Lcom/android/inputmethod/keyboard/ProximityInfo;->EMPTY_KEY_ARRAY:[Lcom/android/inputmethod/keyboard/Key;

    const/4 v9, 0x0

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    invoke-direct/range {v0 .. v9}, Lcom/android/inputmethod/keyboard/ProximityInfo;-><init>(Ljava/lang/String;IIIIII[Lcom/android/inputmethod/keyboard/Key;Lcom/android/inputmethod/keyboard/Keyboard$Params$TouchPositionCorrection;)V

    return-object v0
.end method

.method private final createNativeProximityInfo()J
    .locals 34

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mGridNeighbors:[[Lcom/android/inputmethod/keyboard/Key;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mKeyboardMinWidth:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mKeyboardHeight:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mKeys:[Lcom/android/inputmethod/keyboard/Key;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mTouchPositionCorrection:Lcom/android/inputmethod/keyboard/Keyboard$Params$TouchPositionCorrection;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mGridSize:I

    mul-int/lit8 v2, v2, 0x10

    new-array v10, v2, [I

    const/4 v2, -0x1

    invoke-static {v10, v2}, Ljava/util/Arrays;->fill([II)V

    const/16 v24, 0x0

    :goto_0
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mGridSize:I

    move/from16 v0, v24

    if-ge v0, v2, :cond_1

    aget-object v2, v20, v24

    array-length v0, v2

    move/from16 v28, v0

    const/16 v25, 0x0

    :goto_1
    move/from16 v0, v25

    move/from16 v1, v28

    if-ge v0, v1, :cond_0

    mul-int/lit8 v2, v24, 0x10

    add-int v2, v2, v25

    aget-object v3, v20, v24

    aget-object v3, v3, v25

    iget v3, v3, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    aput v3, v10, v2

    add-int/lit8 v25, v25, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v24, v24, 0x1

    goto :goto_0

    :cond_1
    move-object/from16 v0, v27

    array-length v11, v0

    new-array v12, v11, [I

    new-array v13, v11, [I

    new-array v14, v11, [I

    new-array v15, v11, [I

    new-array v0, v11, [I

    move-object/from16 v16, v0

    const/16 v24, 0x0

    :goto_2
    move/from16 v0, v24

    if-ge v0, v11, :cond_2

    aget-object v26, v27, v24

    move-object/from16 v0, v26

    iget v2, v0, Lcom/android/inputmethod/keyboard/Key;->mX:I

    aput v2, v12, v24

    move-object/from16 v0, v26

    iget v2, v0, Lcom/android/inputmethod/keyboard/Key;->mY:I

    aput v2, v13, v24

    move-object/from16 v0, v26

    iget v2, v0, Lcom/android/inputmethod/keyboard/Key;->mWidth:I

    aput v2, v14, v24

    move-object/from16 v0, v26

    iget v2, v0, Lcom/android/inputmethod/keyboard/Key;->mHeight:I

    aput v2, v15, v24

    move-object/from16 v0, v26

    iget v2, v0, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    aput v2, v16, v24

    add-int/lit8 v24, v24, 0x1

    goto :goto_2

    :cond_2
    if-eqz v31, :cond_4

    invoke-virtual/range {v31 .. v31}, Lcom/android/inputmethod/keyboard/Keyboard$Params$TouchPositionCorrection;->isValid()Z

    move-result v2

    if-eqz v2, :cond_4

    new-array v0, v11, [F

    move-object/from16 v17, v0

    new-array v0, v11, [F

    move-object/from16 v18, v0

    new-array v0, v11, [F

    move-object/from16 v19, v0

    const/16 v24, 0x0

    :goto_3
    move/from16 v0, v24

    if-ge v0, v11, :cond_5

    aget-object v26, v27, v24

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/Key;->mHitBox:Landroid/graphics/Rect;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v2, v0, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mKeyHeight:I

    div-int v30, v2, v3

    move-object/from16 v0, v31

    iget-object v2, v0, Lcom/android/inputmethod/keyboard/Keyboard$Params$TouchPositionCorrection;->mRadii:[F

    array-length v2, v2

    move/from16 v0, v30

    if-ge v0, v2, :cond_3

    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Rect;->width()I

    move-result v23

    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Rect;->height()I

    move-result v22

    move-object/from16 v0, v31

    iget-object v2, v0, Lcom/android/inputmethod/keyboard/Keyboard$Params$TouchPositionCorrection;->mXs:[F

    aget v32, v2, v30

    move-object/from16 v0, v31

    iget-object v2, v0, Lcom/android/inputmethod/keyboard/Keyboard$Params$TouchPositionCorrection;->mYs:[F

    aget v33, v2, v30

    move-object/from16 v0, v31

    iget-object v2, v0, Lcom/android/inputmethod/keyboard/Keyboard$Params$TouchPositionCorrection;->mRadii:[F

    aget v29, v2, v30

    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v2

    move/from16 v0, v23

    int-to-float v3, v0

    mul-float v3, v3, v32

    add-float/2addr v2, v3

    aput v2, v17, v24

    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v2

    move/from16 v0, v22

    int-to-float v3, v0

    mul-float v3, v3, v33

    add-float/2addr v2, v3

    aput v2, v18, v24

    mul-int v2, v23, v23

    mul-int v3, v22, v22

    add-int/2addr v2, v3

    int-to-float v2, v2

    invoke-static {v2}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v2

    mul-float v2, v2, v29

    aput v2, v19, v24

    :cond_3
    add-int/lit8 v24, v24, 0x1

    goto :goto_3

    :cond_4
    const/16 v19, 0x0

    move-object/from16 v18, v19

    move-object/from16 v17, v19

    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mLocaleStr:Ljava/lang/String;

    const/16 v4, 0x10

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mGridWidth:I

    move-object/from16 v0, p0

    iget v8, v0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mGridHeight:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mMostCommonKeyWidth:I

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v19}, Lcom/android/inputmethod/keyboard/ProximityInfo;->setProximityInfoNative(Ljava/lang/String;IIIIII[II[I[I[I[I[I[F[F[F)J

    move-result-wide v2

    return-wide v2
.end method

.method public static createSpellCheckerProximityInfo([IIII)Lcom/android/inputmethod/keyboard/ProximityInfo;
    .locals 18
    .param p0    # [I
    .param p1    # I
    .param p2    # I
    .param p3    # I

    invoke-static {}, Lcom/android/inputmethod/keyboard/ProximityInfo;->createDummyProximityInfo()Lcom/android/inputmethod/keyboard/ProximityInfo;

    move-result-object v0

    const-string v1, ""

    const/4 v7, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v2, p1

    move/from16 v3, p2

    move/from16 v4, p3

    move/from16 v5, p2

    move/from16 v6, p3

    move-object/from16 v8, p0

    invoke-direct/range {v0 .. v17}, Lcom/android/inputmethod/keyboard/ProximityInfo;->setProximityInfoNative(Ljava/lang/String;IIIIII[II[I[I[I[I[I[F[F[F)J

    move-result-wide v1

    iput-wide v1, v0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mNativeProximityInfo:J

    return-object v0
.end method

.method private native releaseProximityInfoNative(J)V
.end method

.method private native setProximityInfoNative(Ljava/lang/String;IIIIII[II[I[I[I[I[I[F[F[F)J
.end method


# virtual methods
.method public fillArrayWithNearestKeyCodes(III[I)V
    .locals 11
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # [I

    const/16 v10, 0x20

    array-length v2, p4

    const/4 v9, 0x1

    if-ge v2, v9, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v4, 0x0

    if-le p3, v10, :cond_2

    add-int/lit8 v5, v4, 0x1

    aput p3, p4, v4

    move v4, v5

    :cond_2
    invoke-virtual {p0, p1, p2}, Lcom/android/inputmethod/keyboard/ProximityInfo;->getNearestKeys(II)[Lcom/android/inputmethod/keyboard/Key;

    move-result-object v8

    move-object v0, v8

    array-length v7, v0

    const/4 v3, 0x0

    move v5, v4

    :goto_1
    if-ge v3, v7, :cond_3

    aget-object v6, v0, v3

    if-lt v5, v2, :cond_4

    :cond_3
    if-ge v5, v2, :cond_0

    const/4 v9, -0x1

    aput v9, p4, v5

    goto :goto_0

    :cond_4
    iget v1, v6, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    if-le v1, v10, :cond_3

    add-int/lit8 v4, v5, 0x1

    aput v1, p4, v5

    add-int/lit8 v3, v3, 0x1

    move v5, v4

    goto :goto_1
.end method

.method protected finalize()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const-wide/16 v2, 0x0

    :try_start_0
    iget-wide v0, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mNativeProximityInfo:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mNativeProximityInfo:J

    invoke-direct {p0, v0, v1}, Lcom/android/inputmethod/keyboard/ProximityInfo;->releaseProximityInfoNative(J)V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mNativeProximityInfo:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    return-void

    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method public getNativeProximityInfo()J
    .locals 2

    iget-wide v0, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mNativeProximityInfo:J

    return-wide v0
.end method

.method public getNearestKeys(II)[Lcom/android/inputmethod/keyboard/Key;
    .locals 3
    .param p1    # I
    .param p2    # I

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mGridNeighbors:[[Lcom/android/inputmethod/keyboard/Key;

    if-nez v1, :cond_0

    sget-object v1, Lcom/android/inputmethod/keyboard/ProximityInfo;->EMPTY_KEY_ARRAY:[Lcom/android/inputmethod/keyboard/Key;

    :goto_0
    return-object v1

    :cond_0
    if-ltz p1, :cond_1

    iget v1, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mKeyboardMinWidth:I

    if-ge p1, v1, :cond_1

    if-ltz p2, :cond_1

    iget v1, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mKeyboardHeight:I

    if-ge p2, v1, :cond_1

    iget v1, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mCellHeight:I

    div-int v1, p2, v1

    iget v2, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mGridWidth:I

    mul-int/2addr v1, v2

    iget v2, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mCellWidth:I

    div-int v2, p1, v2

    add-int v0, v1, v2

    iget v1, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mGridSize:I

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/ProximityInfo;->mGridNeighbors:[[Lcom/android/inputmethod/keyboard/Key;

    aget-object v1, v1, v0

    goto :goto_0

    :cond_1
    sget-object v1, Lcom/android/inputmethod/keyboard/ProximityInfo;->EMPTY_KEY_ARRAY:[Lcom/android/inputmethod/keyboard/Key;

    goto :goto_0
.end method
