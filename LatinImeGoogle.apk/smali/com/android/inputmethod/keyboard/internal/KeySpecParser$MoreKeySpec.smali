.class public Lcom/android/inputmethod/keyboard/internal/KeySpecParser$MoreKeySpec;
.super Ljava/lang/Object;
.source "KeySpecParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/keyboard/internal/KeySpecParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MoreKeySpec"
.end annotation


# instance fields
.field public final mCode:I

.field public final mIconId:I

.field public final mLabel:Ljava/lang/String;

.field public final mOutputText:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;ZLjava/util/Locale;Lcom/android/inputmethod/keyboard/internal/KeyboardCodesSet;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Z
    .param p3    # Ljava/util/Locale;
    .param p4    # Lcom/android/inputmethod/keyboard/internal/KeyboardCodesSet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1, p4}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser;->getCode(Ljava/lang/String;Lcom/android/inputmethod/keyboard/internal/KeyboardCodesSet;)I

    move-result v0

    invoke-static {v0, p2, p3}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser;->toUpperCaseOfCodeForLocale(IZLjava/util/Locale;)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeySpecParser$MoreKeySpec;->mCode:I

    invoke-static {p1}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser;->getLabel(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2, p3}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser;->toUpperCaseOfStringForLocale(Ljava/lang/String;ZLjava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeySpecParser$MoreKeySpec;->mLabel:Ljava/lang/String;

    invoke-static {p1}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser;->getOutputText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2, p3}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser;->toUpperCaseOfStringForLocale(Ljava/lang/String;ZLjava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeySpecParser$MoreKeySpec;->mOutputText:Ljava/lang/String;

    invoke-static {p1}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser;->getIconId(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeySpecParser$MoreKeySpec;->mIconId:I

    return-void
.end method
