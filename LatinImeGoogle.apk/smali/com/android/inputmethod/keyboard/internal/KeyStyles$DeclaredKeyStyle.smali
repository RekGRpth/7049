.class Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;
.super Lcom/android/inputmethod/keyboard/internal/KeyStyles$KeyStyle;
.source "KeyStyles.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/keyboard/internal/KeyStyles;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DeclaredKeyStyle"
.end annotation


# instance fields
.field private final mParentStyleName:Ljava/lang/String;

.field private final mStyleAttributes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/android/inputmethod/keyboard/internal/KeyStyles;


# direct methods
.method public constructor <init>(Lcom/android/inputmethod/keyboard/internal/KeyStyles;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;->this$0:Lcom/android/inputmethod/keyboard/internal/KeyStyles;

    invoke-direct {p0, p1}, Lcom/android/inputmethod/keyboard/internal/KeyStyles$KeyStyle;-><init>(Lcom/android/inputmethod/keyboard/internal/KeyStyles;)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;->mStyleAttributes:Ljava/util/HashMap;

    iput-object p2, p0, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;->mParentStyleName:Ljava/lang/String;

    return-void
.end method

.method private readFlag(Landroid/content/res/TypedArray;I)V
    .locals 5
    .param p1    # Landroid/content/res/TypedArray;
    .param p2    # I

    const/4 v1, 0x0

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;->mStyleAttributes:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;->mStyleAttributes:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v4

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :cond_0
    or-int/2addr v1, v4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-void
.end method

.method private readInt(Landroid/content/res/TypedArray;I)V
    .locals 3
    .param p1    # Landroid/content/res/TypedArray;
    .param p2    # I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;->mStyleAttributes:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, p2, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method private readString(Landroid/content/res/TypedArray;I)V
    .locals 3
    .param p1    # Landroid/content/res/TypedArray;
    .param p2    # I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;->mStyleAttributes:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, p1, p2}, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;->parseString(Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method private readStringArray(Landroid/content/res/TypedArray;I)V
    .locals 3
    .param p1    # Landroid/content/res/TypedArray;
    .param p2    # I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;->mStyleAttributes:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, p1, p2}, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;->parseStringArray(Landroid/content/res/TypedArray;I)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method


# virtual methods
.method public getFlag(Landroid/content/res/TypedArray;I)I
    .locals 4
    .param p1    # Landroid/content/res/TypedArray;
    .param p2    # I

    const/4 v2, 0x0

    invoke-virtual {p1, p2, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;->mStyleAttributes:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;->mStyleAttributes:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    or-int/2addr v1, v2

    :cond_0
    iget-object v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;->this$0:Lcom/android/inputmethod/keyboard/internal/KeyStyles;

    iget-object v2, v2, Lcom/android/inputmethod/keyboard/internal/KeyStyles;->mStyles:Ljava/util/HashMap;

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;->mParentStyleName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/inputmethod/keyboard/internal/KeyStyles$KeyStyle;

    invoke-virtual {v0, p1, p2}, Lcom/android/inputmethod/keyboard/internal/KeyStyles$KeyStyle;->getFlag(Landroid/content/res/TypedArray;I)I

    move-result v2

    or-int/2addr v2, v1

    return v2
.end method

.method public getInt(Landroid/content/res/TypedArray;II)I
    .locals 3
    .param p1    # Landroid/content/res/TypedArray;
    .param p2    # I
    .param p3    # I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;->mStyleAttributes:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;->mStyleAttributes:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;->this$0:Lcom/android/inputmethod/keyboard/internal/KeyStyles;

    iget-object v1, v1, Lcom/android/inputmethod/keyboard/internal/KeyStyles;->mStyles:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;->mParentStyleName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/inputmethod/keyboard/internal/KeyStyles$KeyStyle;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/inputmethod/keyboard/internal/KeyStyles$KeyStyle;->getInt(Landroid/content/res/TypedArray;II)I

    move-result v1

    goto :goto_0
.end method

.method public getString(Landroid/content/res/TypedArray;I)Ljava/lang/String;
    .locals 3
    .param p1    # Landroid/content/res/TypedArray;
    .param p2    # I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, p1, p2}, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;->parseString(Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;->mStyleAttributes:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;->mStyleAttributes:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;->this$0:Lcom/android/inputmethod/keyboard/internal/KeyStyles;

    iget-object v1, v1, Lcom/android/inputmethod/keyboard/internal/KeyStyles;->mStyles:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;->mParentStyleName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/inputmethod/keyboard/internal/KeyStyles$KeyStyle;

    invoke-virtual {v0, p1, p2}, Lcom/android/inputmethod/keyboard/internal/KeyStyles$KeyStyle;->getString(Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getStringArray(Landroid/content/res/TypedArray;I)[Ljava/lang/String;
    .locals 3
    .param p1    # Landroid/content/res/TypedArray;
    .param p2    # I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, p1, p2}, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;->parseStringArray(Landroid/content/res/TypedArray;I)[Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;->mStyleAttributes:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;->mStyleAttributes:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    check-cast v1, [Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;->this$0:Lcom/android/inputmethod/keyboard/internal/KeyStyles;

    iget-object v1, v1, Lcom/android/inputmethod/keyboard/internal/KeyStyles;->mStyles:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;->mParentStyleName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/inputmethod/keyboard/internal/KeyStyles$KeyStyle;

    invoke-virtual {v0, p1, p2}, Lcom/android/inputmethod/keyboard/internal/KeyStyles$KeyStyle;->getStringArray(Landroid/content/res/TypedArray;I)[Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method readKeyAttributes(Landroid/content/res/TypedArray;)V
    .locals 1
    .param p1    # Landroid/content/res/TypedArray;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;->readString(Landroid/content/res/TypedArray;I)V

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;->readString(Landroid/content/res/TypedArray;I)V

    const/16 v0, 0x8

    invoke-direct {p0, p1, v0}, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;->readString(Landroid/content/res/TypedArray;I)V

    const/4 v0, 0x7

    invoke-direct {p0, p1, v0}, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;->readString(Landroid/content/res/TypedArray;I)V

    const/16 v0, 0x9

    invoke-direct {p0, p1, v0}, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;->readString(Landroid/content/res/TypedArray;I)V

    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;->readStringArray(Landroid/content/res/TypedArray;I)V

    const/4 v0, 0x3

    invoke-direct {p0, p1, v0}, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;->readStringArray(Landroid/content/res/TypedArray;I)V

    const/16 v0, 0xa

    invoke-direct {p0, p1, v0}, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;->readFlag(Landroid/content/res/TypedArray;I)V

    const/16 v0, 0xb

    invoke-direct {p0, p1, v0}, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;->readString(Landroid/content/res/TypedArray;I)V

    const/16 v0, 0xc

    invoke-direct {p0, p1, v0}, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;->readString(Landroid/content/res/TypedArray;I)V

    const/16 v0, 0xd

    invoke-direct {p0, p1, v0}, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;->readString(Landroid/content/res/TypedArray;I)V

    const/4 v0, 0x4

    invoke-direct {p0, p1, v0}, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;->readInt(Landroid/content/res/TypedArray;I)V

    const/4 v0, 0x5

    invoke-direct {p0, p1, v0}, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;->readInt(Landroid/content/res/TypedArray;I)V

    const/4 v0, 0x6

    invoke-direct {p0, p1, v0}, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;->readFlag(Landroid/content/res/TypedArray;I)V

    return-void
.end method
