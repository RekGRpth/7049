.class Lcom/android/inputmethod/keyboard/internal/KeyStyles$EmptyKeyStyle;
.super Lcom/android/inputmethod/keyboard/internal/KeyStyles$KeyStyle;
.source "KeyStyles.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/keyboard/internal/KeyStyles;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "EmptyKeyStyle"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/inputmethod/keyboard/internal/KeyStyles;


# direct methods
.method constructor <init>(Lcom/android/inputmethod/keyboard/internal/KeyStyles;)V
    .locals 0

    iput-object p1, p0, Lcom/android/inputmethod/keyboard/internal/KeyStyles$EmptyKeyStyle;->this$0:Lcom/android/inputmethod/keyboard/internal/KeyStyles;

    invoke-direct {p0, p1}, Lcom/android/inputmethod/keyboard/internal/KeyStyles$KeyStyle;-><init>(Lcom/android/inputmethod/keyboard/internal/KeyStyles;)V

    return-void
.end method


# virtual methods
.method public getFlag(Landroid/content/res/TypedArray;I)I
    .locals 1
    .param p1    # Landroid/content/res/TypedArray;
    .param p2    # I

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    return v0
.end method

.method public getInt(Landroid/content/res/TypedArray;II)I
    .locals 1
    .param p1    # Landroid/content/res/TypedArray;
    .param p2    # I
    .param p3    # I

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    return v0
.end method

.method public getString(Landroid/content/res/TypedArray;I)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/res/TypedArray;
    .param p2    # I

    invoke-virtual {p0, p1, p2}, Lcom/android/inputmethod/keyboard/internal/KeyStyles$EmptyKeyStyle;->parseString(Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getStringArray(Landroid/content/res/TypedArray;I)[Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/res/TypedArray;
    .param p2    # I

    invoke-virtual {p0, p1, p2}, Lcom/android/inputmethod/keyboard/internal/KeyStyles$EmptyKeyStyle;->parseStringArray(Landroid/content/res/TypedArray;I)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
