.class public abstract Lcom/android/inputmethod/keyboard/internal/KeyStyles$KeyStyle;
.super Ljava/lang/Object;
.source "KeyStyles.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/keyboard/internal/KeyStyles;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401
    name = "KeyStyle"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/inputmethod/keyboard/internal/KeyStyles;


# direct methods
.method public constructor <init>(Lcom/android/inputmethod/keyboard/internal/KeyStyles;)V
    .locals 0

    iput-object p1, p0, Lcom/android/inputmethod/keyboard/internal/KeyStyles$KeyStyle;->this$0:Lcom/android/inputmethod/keyboard/internal/KeyStyles;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getFlag(Landroid/content/res/TypedArray;I)I
.end method

.method public abstract getInt(Landroid/content/res/TypedArray;II)I
.end method

.method public abstract getString(Landroid/content/res/TypedArray;I)Ljava/lang/String;
.end method

.method public abstract getStringArray(Landroid/content/res/TypedArray;I)[Ljava/lang/String;
.end method

.method protected parseString(Landroid/content/res/TypedArray;I)Ljava/lang/String;
    .locals 2
    .param p1    # Landroid/content/res/TypedArray;
    .param p2    # I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyStyles$KeyStyle;->this$0:Lcom/android/inputmethod/keyboard/internal/KeyStyles;

    iget-object v1, v1, Lcom/android/inputmethod/keyboard/internal/KeyStyles;->mTextsSet:Lcom/android/inputmethod/keyboard/internal/KeyboardTextsSet;

    invoke-static {v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser;->resolveTextReference(Ljava/lang/String;Lcom/android/inputmethod/keyboard/internal/KeyboardTextsSet;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected parseStringArray(Landroid/content/res/TypedArray;I)[Ljava/lang/String;
    .locals 2
    .param p1    # Landroid/content/res/TypedArray;
    .param p2    # I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyStyles$KeyStyle;->this$0:Lcom/android/inputmethod/keyboard/internal/KeyStyles;

    iget-object v1, v1, Lcom/android/inputmethod/keyboard/internal/KeyStyles;->mTextsSet:Lcom/android/inputmethod/keyboard/internal/KeyboardTextsSet;

    invoke-static {v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser;->parseCsvString(Ljava/lang/String;Lcom/android/inputmethod/keyboard/internal/KeyboardTextsSet;)[Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
