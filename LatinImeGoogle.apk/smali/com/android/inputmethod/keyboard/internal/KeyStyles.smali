.class public Lcom/android/inputmethod/keyboard/internal/KeyStyles;
.super Ljava/lang/Object;
.source "KeyStyles.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;,
        Lcom/android/inputmethod/keyboard/internal/KeyStyles$EmptyKeyStyle;,
        Lcom/android/inputmethod/keyboard/internal/KeyStyles$KeyStyle;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mEmptyKeyStyle:Lcom/android/inputmethod/keyboard/internal/KeyStyles$KeyStyle;

.field final mStyles:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/inputmethod/keyboard/internal/KeyStyles$KeyStyle;",
            ">;"
        }
    .end annotation
.end field

.field final mTextsSet:Lcom/android/inputmethod/keyboard/internal/KeyboardTextsSet;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/inputmethod/keyboard/internal/KeyStyles;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/inputmethod/keyboard/internal/KeyStyles;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/android/inputmethod/keyboard/internal/KeyboardTextsSet;)V
    .locals 3
    .param p1    # Lcom/android/inputmethod/keyboard/internal/KeyboardTextsSet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyStyles;->mStyles:Ljava/util/HashMap;

    iput-object p1, p0, Lcom/android/inputmethod/keyboard/internal/KeyStyles;->mTextsSet:Lcom/android/inputmethod/keyboard/internal/KeyboardTextsSet;

    new-instance v0, Lcom/android/inputmethod/keyboard/internal/KeyStyles$EmptyKeyStyle;

    invoke-direct {v0, p0}, Lcom/android/inputmethod/keyboard/internal/KeyStyles$EmptyKeyStyle;-><init>(Lcom/android/inputmethod/keyboard/internal/KeyStyles;)V

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyStyles;->mEmptyKeyStyle:Lcom/android/inputmethod/keyboard/internal/KeyStyles$KeyStyle;

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyStyles;->mStyles:Ljava/util/HashMap;

    const-string v1, "<empty>"

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyStyles;->mEmptyKeyStyle:Lcom/android/inputmethod/keyboard/internal/KeyStyles$KeyStyle;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public getKeyStyle(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;)Lcom/android/inputmethod/keyboard/internal/KeyStyles$KeyStyle;
    .locals 4
    .param p1    # Landroid/content/res/TypedArray;
    .param p2    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/inputmethod/latin/XmlParseUtils$ParseException;
        }
    .end annotation

    const/16 v2, 0xe

    invoke-virtual {p1, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyStyles;->mEmptyKeyStyle:Lcom/android/inputmethod/keyboard/internal/KeyStyles$KeyStyle;

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p1, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyStyles;->mStyles:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Lcom/android/inputmethod/latin/XmlParseUtils$ParseException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown key style: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, p2}, Lcom/android/inputmethod/latin/XmlParseUtils$ParseException;-><init>(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)V

    throw v1

    :cond_1
    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyStyles;->mStyles:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/inputmethod/keyboard/internal/KeyStyles$KeyStyle;

    goto :goto_0
.end method

.method public parseKeyStyleAttributes(Landroid/content/res/TypedArray;Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 6
    .param p1    # Landroid/content/res/TypedArray;
    .param p2    # Landroid/content/res/TypedArray;
    .param p3    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v0, "<empty>"

    invoke-virtual {p1, v4}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p1, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/internal/KeyStyles;->mStyles:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v3, Lcom/android/inputmethod/latin/XmlParseUtils$ParseException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unknown parentStyle "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, p3}, Lcom/android/inputmethod/latin/XmlParseUtils$ParseException;-><init>(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)V

    throw v3

    :cond_0
    new-instance v1, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;

    invoke-direct {v1, p0, v0}, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;-><init>(Lcom/android/inputmethod/keyboard/internal/KeyStyles;Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Lcom/android/inputmethod/keyboard/internal/KeyStyles$DeclaredKeyStyle;->readKeyAttributes(Landroid/content/res/TypedArray;)V

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/internal/KeyStyles;->mStyles:Ljava/util/HashMap;

    invoke-virtual {v3, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
