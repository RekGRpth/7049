.class public Lcom/android/inputmethod/keyboard/internal/KeyboardState;
.super Ljava/lang/Object;
.source "KeyboardState.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/inputmethod/keyboard/internal/KeyboardState$SavedKeyboardState;,
        Lcom/android/inputmethod/keyboard/internal/KeyboardState$SwitchActions;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAlphabetShiftState:Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;

.field private mIsAlphabetMode:Z

.field private mIsInAlphabetUnshiftedFromShifted:Z

.field private mIsInDoubleTapShiftKey:Z

.field private mIsSymbolShifted:Z

.field private mLayoutSwitchBackSymbols:Ljava/lang/String;

.field private mLongPressShiftLockFired:Z

.field private mPrevMainKeyboardWasShiftLocked:Z

.field private mPrevSymbolsKeyboardWasShifted:Z

.field private final mSavedKeyboardState:Lcom/android/inputmethod/keyboard/internal/KeyboardState$SavedKeyboardState;

.field private mShiftKeyState:Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;

.field private final mSwitchActions:Lcom/android/inputmethod/keyboard/internal/KeyboardState$SwitchActions;

.field private mSwitchState:I

.field private mSymbolKeyState:Lcom/android/inputmethod/keyboard/internal/ModifierKeyState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/android/inputmethod/keyboard/internal/KeyboardState$SwitchActions;)V
    .locals 2
    .param p1    # Lcom/android/inputmethod/keyboard/internal/KeyboardState$SwitchActions;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;

    const-string v1, "Shift"

    invoke-direct {v0, v1}, Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mShiftKeyState:Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;

    new-instance v0, Lcom/android/inputmethod/keyboard/internal/ModifierKeyState;

    const-string v1, "Symbol"

    invoke-direct {v0, v1}, Lcom/android/inputmethod/keyboard/internal/ModifierKeyState;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSymbolKeyState:Lcom/android/inputmethod/keyboard/internal/ModifierKeyState;

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSwitchState:I

    new-instance v0, Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;

    invoke-direct {v0}, Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mAlphabetShiftState:Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;

    new-instance v0, Lcom/android/inputmethod/keyboard/internal/KeyboardState$SavedKeyboardState;

    invoke-direct {v0}, Lcom/android/inputmethod/keyboard/internal/KeyboardState$SavedKeyboardState;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSavedKeyboardState:Lcom/android/inputmethod/keyboard/internal/KeyboardState$SavedKeyboardState;

    iput-object p1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSwitchActions:Lcom/android/inputmethod/keyboard/internal/KeyboardState$SwitchActions;

    return-void
.end method

.method private isLayoutSwitchBackCharacter(I)Z
    .locals 2
    .param p1    # I

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mLayoutSwitchBackSymbols:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mLayoutSwitchBackSymbols:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static isSpaceCharacter(I)Z
    .locals 1
    .param p0    # I

    const/16 v0, 0x20

    if-eq p0, v0, :cond_0

    const/16 v0, 0xa

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onPressShift()V
    .locals 2

    const/4 v1, 0x1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mLongPressShiftLockFired:Z

    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mIsAlphabetMode:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSwitchActions:Lcom/android/inputmethod/keyboard/internal/KeyboardState$SwitchActions;

    invoke-interface {v0}, Lcom/android/inputmethod/keyboard/internal/KeyboardState$SwitchActions;->isInDoubleTapTimeout()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mIsInDoubleTapShiftKey:Z

    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mIsInDoubleTapShiftKey:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSwitchActions:Lcom/android/inputmethod/keyboard/internal/KeyboardState$SwitchActions;

    invoke-interface {v0}, Lcom/android/inputmethod/keyboard/internal/KeyboardState$SwitchActions;->startDoubleTapTimer()V

    :cond_0
    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mIsInDoubleTapShiftKey:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mAlphabetShiftState:Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;->isManualShifted()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mIsInAlphabetUnshiftedFromShifted:Z

    if-eqz v0, :cond_2

    :cond_1
    invoke-direct {p0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->setShiftLocked(Z)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mAlphabetShiftState:Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;->isShiftLocked()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->setShifted(I)V

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mShiftKeyState:Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;->onPress()V

    :goto_1
    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSwitchActions:Lcom/android/inputmethod/keyboard/internal/KeyboardState$SwitchActions;

    const/4 v1, -0x1

    invoke-interface {v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyboardState$SwitchActions;->startLongPressTimer(I)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mAlphabetShiftState:Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;->isAutomaticShifted()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-direct {p0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->setShifted(I)V

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mShiftKeyState:Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;->onPress()V

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mAlphabetShiftState:Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;->isShiftedOrShiftLocked()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mShiftKeyState:Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;->onPressOnShifted()V

    goto :goto_1

    :cond_6
    invoke-direct {p0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->setShifted(I)V

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mShiftKeyState:Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;->onPress()V

    goto :goto_1

    :cond_7
    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->toggleShiftInSymbols()V

    const/4 v0, 0x4

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSwitchState:I

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mShiftKeyState:Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;->onPress()V

    goto :goto_0
.end method

.method private onPressSymbol()V
    .locals 1

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->toggleAlphabetAndSymbols()V

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSymbolKeyState:Lcom/android/inputmethod/keyboard/internal/ModifierKeyState;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/internal/ModifierKeyState;->onPress()V

    const/4 v0, 0x3

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSwitchState:I

    return-void
.end method

.method private onReleaseShift(Z)V
    .locals 4
    .param p1    # Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mIsAlphabetMode:Z

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mAlphabetShiftState:Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;

    invoke-virtual {v3}, Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;->isShiftLocked()Z

    move-result v0

    iput-boolean v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mIsInAlphabetUnshiftedFromShifted:Z

    iget-boolean v3, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mIsInDoubleTapShiftKey:Z

    if-eqz v3, :cond_1

    iput-boolean v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mIsInDoubleTapShiftKey:Z

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mShiftKeyState:Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;->onRelease()V

    return-void

    :cond_1
    iget-boolean v3, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mLongPressShiftLockFired:Z

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mAlphabetShiftState:Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;

    invoke-virtual {v3}, Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;->isShiftLocked()Z

    move-result v3

    if-nez v3, :cond_2

    :goto_1
    invoke-direct {p0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->setShiftLocked(Z)V

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    iget-object v3, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mShiftKeyState:Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;

    invoke-virtual {v3}, Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;->isChording()Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mAlphabetShiftState:Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;

    invoke-virtual {v3}, Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;->isShiftLockShifted()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-direct {p0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->setShiftLocked(Z)V

    goto :goto_0

    :cond_4
    invoke-direct {p0, v2}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->setShifted(I)V

    goto :goto_0

    :cond_5
    iget-object v3, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mAlphabetShiftState:Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;

    invoke-virtual {v3}, Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;->isShiftLockShifted()Z

    move-result v3

    if-eqz v3, :cond_6

    if-eqz p1, :cond_6

    invoke-direct {p0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->setShiftLocked(Z)V

    goto :goto_0

    :cond_6
    if-eqz v0, :cond_8

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mAlphabetShiftState:Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;

    invoke-virtual {v3}, Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;->isShiftLockShifted()Z

    move-result v3

    if-nez v3, :cond_8

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mShiftKeyState:Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;

    invoke-virtual {v3}, Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;->isPressing()Z

    move-result v3

    if-nez v3, :cond_7

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mShiftKeyState:Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;

    invoke-virtual {v3}, Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;->isPressingOnShifted()Z

    move-result v3

    if-eqz v3, :cond_8

    :cond_7
    if-eqz p1, :cond_0

    :cond_8
    if-eqz v0, :cond_9

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mShiftKeyState:Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;

    invoke-virtual {v3}, Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;->isIgnoring()Z

    move-result v3

    if-nez v3, :cond_9

    if-nez p1, :cond_9

    invoke-direct {p0, v2}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->setShiftLocked(Z)V

    goto :goto_0

    :cond_9
    iget-object v3, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mAlphabetShiftState:Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;

    invoke-virtual {v3}, Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;->isShiftedOrShiftLocked()Z

    move-result v3

    if-eqz v3, :cond_a

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mShiftKeyState:Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;

    invoke-virtual {v3}, Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;->isPressingOnShifted()Z

    move-result v3

    if-eqz v3, :cond_a

    if-nez p1, :cond_a

    invoke-direct {p0, v2}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->setShifted(I)V

    iput-boolean v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mIsInAlphabetUnshiftedFromShifted:Z

    goto/16 :goto_0

    :cond_a
    iget-object v3, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mAlphabetShiftState:Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;

    invoke-virtual {v3}, Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;->isManualShiftedFromAutomaticShifted()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mShiftKeyState:Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;

    invoke-virtual {v3}, Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;->isPressing()Z

    move-result v3

    if-eqz v3, :cond_0

    if-nez p1, :cond_0

    invoke-direct {p0, v2}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->setShifted(I)V

    iput-boolean v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mIsInAlphabetUnshiftedFromShifted:Z

    goto/16 :goto_0

    :cond_b
    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mShiftKeyState:Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;->isChording()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->toggleShiftInSymbols()V

    goto/16 :goto_0
.end method

.method private onReleaseSymbol(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSymbolKeyState:Lcom/android/inputmethod/keyboard/internal/ModifierKeyState;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/internal/ModifierKeyState;->isChording()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->toggleAlphabetAndSymbols()V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSymbolKeyState:Lcom/android/inputmethod/keyboard/internal/ModifierKeyState;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/internal/ModifierKeyState;->onRelease()V

    return-void

    :cond_1
    if-nez p1, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mPrevSymbolsKeyboardWasShifted:Z

    goto :goto_0
.end method

.method private onRestoreKeyboardState()V
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSavedKeyboardState:Lcom/android/inputmethod/keyboard/internal/KeyboardState$SavedKeyboardState;

    iget-boolean v2, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardState$SavedKeyboardState;->mIsValid:Z

    if-eqz v2, :cond_0

    iget-boolean v2, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardState$SavedKeyboardState;->mIsAlphabetMode:Z

    if-eqz v2, :cond_2

    :cond_0
    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->setAlphabetKeyboard()V

    :goto_0
    iget-boolean v2, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardState$SavedKeyboardState;->mIsValid:Z

    if-nez v2, :cond_4

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-boolean v2, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardState$SavedKeyboardState;->mIsShifted:Z

    if-eqz v2, :cond_3

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->setSymbolsShiftedKeyboard()V

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->setSymbolsKeyboard()V

    goto :goto_0

    :cond_4
    iput-boolean v1, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardState$SavedKeyboardState;->mIsValid:Z

    iget-boolean v2, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardState$SavedKeyboardState;->mIsAlphabetMode:Z

    if-eqz v2, :cond_6

    iget-boolean v2, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardState$SavedKeyboardState;->mIsAlphabetShiftLocked:Z

    invoke-direct {p0, v2}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->setShiftLocked(Z)V

    iget-boolean v2, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardState$SavedKeyboardState;->mIsAlphabetShiftLocked:Z

    if-nez v2, :cond_1

    iget-boolean v2, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardState$SavedKeyboardState;->mIsShifted:Z

    if-eqz v2, :cond_5

    const/4 v1, 0x1

    :cond_5
    invoke-direct {p0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->setShifted(I)V

    goto :goto_1

    :cond_6
    iget-boolean v1, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardState$SavedKeyboardState;->mIsAlphabetShiftLocked:Z

    iput-boolean v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mPrevMainKeyboardWasShiftLocked:Z

    goto :goto_1
.end method

.method private setAlphabetKeyboard()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSwitchActions:Lcom/android/inputmethod/keyboard/internal/KeyboardState$SwitchActions;

    invoke-interface {v0}, Lcom/android/inputmethod/keyboard/internal/KeyboardState$SwitchActions;->setAlphabetKeyboard()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mIsAlphabetMode:Z

    iput-boolean v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mIsSymbolShifted:Z

    iput v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSwitchState:I

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSwitchActions:Lcom/android/inputmethod/keyboard/internal/KeyboardState$SwitchActions;

    invoke-interface {v0}, Lcom/android/inputmethod/keyboard/internal/KeyboardState$SwitchActions;->requestUpdatingShiftState()V

    return-void
.end method

.method private setShiftLocked(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mIsAlphabetMode:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mAlphabetShiftState:Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;->isShiftLocked()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mAlphabetShiftState:Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;->isShiftLockShifted()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSwitchActions:Lcom/android/inputmethod/keyboard/internal/KeyboardState$SwitchActions;

    invoke-interface {v0}, Lcom/android/inputmethod/keyboard/internal/KeyboardState$SwitchActions;->setAlphabetShiftLockedKeyboard()V

    :cond_2
    if-nez p1, :cond_3

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mAlphabetShiftState:Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;->isShiftLocked()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSwitchActions:Lcom/android/inputmethod/keyboard/internal/KeyboardState$SwitchActions;

    invoke-interface {v0}, Lcom/android/inputmethod/keyboard/internal/KeyboardState$SwitchActions;->setAlphabetKeyboard()V

    :cond_3
    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mAlphabetShiftState:Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;

    invoke-virtual {v0, p1}, Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;->setShiftLocked(Z)V

    goto :goto_0
.end method

.method private setShifted(I)V
    .locals 3
    .param p1    # I

    const/4 v2, 0x1

    iget-boolean v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mIsAlphabetMode:Z

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mAlphabetShiftState:Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;->isAutomaticShifted()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x2

    :goto_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mAlphabetShiftState:Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;->setShifted(Z)V

    if-eq p1, v0, :cond_0

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSwitchActions:Lcom/android/inputmethod/keyboard/internal/KeyboardState$SwitchActions;

    invoke-interface {v1}, Lcom/android/inputmethod/keyboard/internal/KeyboardState$SwitchActions;->setAlphabetKeyboard()V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mAlphabetShiftState:Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;->isManualShifted()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v0, 0x1

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :pswitch_1
    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mAlphabetShiftState:Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;->setAutomaticShifted()V

    if-eq p1, v0, :cond_0

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSwitchActions:Lcom/android/inputmethod/keyboard/internal/KeyboardState$SwitchActions;

    invoke-interface {v1}, Lcom/android/inputmethod/keyboard/internal/KeyboardState$SwitchActions;->setAlphabetAutomaticShiftedKeyboard()V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mAlphabetShiftState:Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;

    invoke-virtual {v1, v2}, Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;->setShifted(Z)V

    if-eq p1, v0, :cond_0

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSwitchActions:Lcom/android/inputmethod/keyboard/internal/KeyboardState$SwitchActions;

    invoke-interface {v1}, Lcom/android/inputmethod/keyboard/internal/KeyboardState$SwitchActions;->setAlphabetManualShiftedKeyboard()V

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mAlphabetShiftState:Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;

    invoke-virtual {v1, v2}, Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;->setShifted(Z)V

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSwitchActions:Lcom/android/inputmethod/keyboard/internal/KeyboardState$SwitchActions;

    invoke-interface {v1}, Lcom/android/inputmethod/keyboard/internal/KeyboardState$SwitchActions;->setAlphabetShiftLockShiftedKeyboard()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method private setSymbolsKeyboard()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSwitchActions:Lcom/android/inputmethod/keyboard/internal/KeyboardState$SwitchActions;

    invoke-interface {v0}, Lcom/android/inputmethod/keyboard/internal/KeyboardState$SwitchActions;->setSymbolsKeyboard()V

    iput-boolean v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mIsAlphabetMode:Z

    iput-boolean v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mIsSymbolShifted:Z

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mAlphabetShiftState:Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;->setShiftLocked(Z)V

    const/4 v0, 0x1

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSwitchState:I

    return-void
.end method

.method private setSymbolsShiftedKeyboard()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSwitchActions:Lcom/android/inputmethod/keyboard/internal/KeyboardState$SwitchActions;

    invoke-interface {v0}, Lcom/android/inputmethod/keyboard/internal/KeyboardState$SwitchActions;->setSymbolsShiftedKeyboard()V

    iput-boolean v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mIsAlphabetMode:Z

    iput-boolean v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mIsSymbolShifted:Z

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mAlphabetShiftState:Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;->setShiftLocked(Z)V

    iput v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSwitchState:I

    return-void
.end method

.method private static switchStateToString(I)Ljava/lang/String;
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "ALPHA"

    goto :goto_0

    :pswitch_1
    const-string v0, "SYMBOL-BEGIN"

    goto :goto_0

    :pswitch_2
    const-string v0, "SYMBOL"

    goto :goto_0

    :pswitch_3
    const-string v0, "MOMENTARY-ALPHA-SYMBOL"

    goto :goto_0

    :pswitch_4
    const-string v0, "MOMENTARY-SYMBOL-MORE"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private toggleAlphabetAndSymbols()V
    .locals 2

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mIsAlphabetMode:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mAlphabetShiftState:Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;->isShiftLocked()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mPrevMainKeyboardWasShiftLocked:Z

    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mPrevSymbolsKeyboardWasShifted:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->setSymbolsShiftedKeyboard()V

    :goto_0
    iput-boolean v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mPrevSymbolsKeyboardWasShifted:Z

    :goto_1
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->setSymbolsKeyboard()V

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mIsSymbolShifted:Z

    iput-boolean v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mPrevSymbolsKeyboardWasShifted:Z

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->setAlphabetKeyboard()V

    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mPrevMainKeyboardWasShiftLocked:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->setShiftLocked(Z)V

    :cond_2
    iput-boolean v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mPrevMainKeyboardWasShiftLocked:Z

    goto :goto_1
.end method

.method private toggleShiftInSymbols()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mIsSymbolShifted:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->setSymbolsKeyboard()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->setSymbolsShiftedKeyboard()V

    goto :goto_0
.end method

.method private updateAlphabetShiftState(I)V
    .locals 1
    .param p1    # I

    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mIsAlphabetMode:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mShiftKeyState:Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;->isReleasing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mAlphabetShiftState:Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;->isShiftLocked()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mShiftKeyState:Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;->isIgnoring()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mShiftKeyState:Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;->isReleasing()Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz p1, :cond_2

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->setShifted(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mShiftKeyState:Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;->isChording()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-direct {p0, v0}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->setShifted(I)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public isInMomentarySwitchState()Z
    .locals 2

    iget v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSwitchState:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSwitchState:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCancelInput(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSwitchState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->toggleAlphabetAndSymbols()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSwitchState:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->toggleShiftInSymbols()V

    goto :goto_0
.end method

.method public onCodeInput(IZI)V
    .locals 4
    .param p1    # I
    .param p2    # Z
    .param p3    # I

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSwitchState:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    invoke-static {p1}, Lcom/android/inputmethod/keyboard/Keyboard;->isLetterCode(I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p3}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->updateAlphabetShiftState(I)V

    :cond_1
    return-void

    :pswitch_0
    const/4 v0, -0x2

    if-ne p1, v0, :cond_3

    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mIsAlphabetMode:Z

    if-eqz v0, :cond_2

    iput v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSwitchState:I

    goto :goto_0

    :cond_2
    iput v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSwitchState:I

    goto :goto_0

    :cond_3
    if-eqz p2, :cond_0

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->toggleAlphabetAndSymbols()V

    goto :goto_0

    :pswitch_1
    const/4 v0, -0x1

    if-ne p1, v0, :cond_4

    iput v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSwitchState:I

    goto :goto_0

    :cond_4
    if-eqz p2, :cond_0

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->toggleShiftInSymbols()V

    iput v3, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSwitchState:I

    goto :goto_0

    :pswitch_2
    invoke-static {p1}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->isSpaceCharacter(I)Z

    move-result v0

    if-nez v0, :cond_6

    invoke-static {p1}, Lcom/android/inputmethod/keyboard/Keyboard;->isLetterCode(I)Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, -0x3

    if-ne p1, v0, :cond_6

    :cond_5
    iput v3, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSwitchState:I

    :cond_6
    invoke-direct {p0, p1}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->isLayoutSwitchBackCharacter(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->toggleAlphabetAndSymbols()V

    iput-boolean v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mPrevSymbolsKeyboardWasShifted:Z

    goto :goto_0

    :pswitch_3
    invoke-static {p1}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->isSpaceCharacter(I)Z

    move-result v0

    if-nez v0, :cond_7

    invoke-direct {p0, p1}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->isLayoutSwitchBackCharacter(I)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_7
    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->toggleAlphabetAndSymbols()V

    iput-boolean v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mPrevSymbolsKeyboardWasShifted:Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onLoadKeyboard(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mLayoutSwitchBackSymbols:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mAlphabetShiftState:Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;->setShiftLocked(Z)V

    iput-boolean v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mPrevMainKeyboardWasShiftLocked:Z

    iput-boolean v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mPrevSymbolsKeyboardWasShifted:Z

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mShiftKeyState:Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;->onRelease()V

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSymbolKeyState:Lcom/android/inputmethod/keyboard/internal/ModifierKeyState;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/internal/ModifierKeyState;->onRelease()V

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->onRestoreKeyboardState()V

    return-void
.end method

.method public onLongPressTimeout(I)V
    .locals 1
    .param p1    # I

    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mIsAlphabetMode:Z

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mLongPressShiftLockFired:Z

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSwitchActions:Lcom/android/inputmethod/keyboard/internal/KeyboardState$SwitchActions;

    invoke-interface {v0, p1}, Lcom/android/inputmethod/keyboard/internal/KeyboardState$SwitchActions;->hapticAndAudioFeedback(I)V

    :cond_0
    return-void
.end method

.method public onPressKey(IZI)V
    .locals 2
    .param p1    # I
    .param p2    # Z
    .param p3    # I

    const/4 v0, 0x0

    const/4 v1, -0x1

    if-ne p1, v1, :cond_1

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->onPressShift()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, -0x2

    if-ne p1, v1, :cond_2

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->onPressSymbol()V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSwitchActions:Lcom/android/inputmethod/keyboard/internal/KeyboardState$SwitchActions;

    invoke-interface {v1}, Lcom/android/inputmethod/keyboard/internal/KeyboardState$SwitchActions;->cancelDoubleTapTimer()V

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSwitchActions:Lcom/android/inputmethod/keyboard/internal/KeyboardState$SwitchActions;

    invoke-interface {v1}, Lcom/android/inputmethod/keyboard/internal/KeyboardState$SwitchActions;->cancelLongPressTimer()V

    iput-boolean v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mLongPressShiftLockFired:Z

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mShiftKeyState:Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;->onOtherKeyPressed()V

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSymbolKeyState:Lcom/android/inputmethod/keyboard/internal/ModifierKeyState;

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/internal/ModifierKeyState;->onOtherKeyPressed()V

    if-nez p2, :cond_0

    iget-boolean v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mIsAlphabetMode:Z

    if-eqz v1, :cond_0

    const/16 v1, 0x1000

    if-eq p3, v1, :cond_0

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mAlphabetShiftState:Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;->isAutomaticShifted()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mAlphabetShiftState:Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;->isManualShifted()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mShiftKeyState:Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;->isReleasing()Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    const/4 v0, 0x1

    :cond_4
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSwitchActions:Lcom/android/inputmethod/keyboard/internal/KeyboardState$SwitchActions;

    invoke-interface {v1}, Lcom/android/inputmethod/keyboard/internal/KeyboardState$SwitchActions;->setAlphabetKeyboard()V

    goto :goto_0
.end method

.method public onReleaseKey(IZ)V
    .locals 1
    .param p1    # I
    .param p2    # Z

    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    invoke-direct {p0, p2}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->onReleaseShift(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, -0x2

    if-ne p1, v0, :cond_0

    invoke-direct {p0, p2}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->onReleaseSymbol(Z)V

    goto :goto_0
.end method

.method public onSaveKeyboardState()V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSavedKeyboardState:Lcom/android/inputmethod/keyboard/internal/KeyboardState$SavedKeyboardState;

    iget-boolean v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mIsAlphabetMode:Z

    iput-boolean v1, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardState$SavedKeyboardState;->mIsAlphabetMode:Z

    iget-boolean v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mIsAlphabetMode:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mAlphabetShiftState:Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;->isShiftLocked()Z

    move-result v1

    iput-boolean v1, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardState$SavedKeyboardState;->mIsAlphabetShiftLocked:Z

    iget-boolean v1, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardState$SavedKeyboardState;->mIsAlphabetShiftLocked:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mAlphabetShiftState:Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;->isShiftedOrShiftLocked()Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    iput-boolean v1, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardState$SavedKeyboardState;->mIsShifted:Z

    :goto_1
    iput-boolean v2, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardState$SavedKeyboardState;->mIsValid:Z

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    iget-boolean v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mPrevMainKeyboardWasShiftLocked:Z

    iput-boolean v1, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardState$SavedKeyboardState;->mIsAlphabetShiftLocked:Z

    iget-boolean v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mIsSymbolShifted:Z

    iput-boolean v1, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardState$SavedKeyboardState;->mIsShifted:Z

    goto :goto_1
.end method

.method public onUpdateShiftState(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->updateAlphabetShiftState(I)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[keyboard="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mIsAlphabetMode:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mAlphabetShiftState:Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/internal/AlphabetShiftState;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " shift="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mShiftKeyState:Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " symbol="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSymbolKeyState:Lcom/android/inputmethod/keyboard/internal/ModifierKeyState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " switch="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mSwitchState:I

    invoke-static {v1}, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->switchStateToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardState;->mIsSymbolShifted:Z

    if-eqz v0, :cond_1

    const-string v0, "SYMBOLS_SHIFTED"

    goto :goto_0

    :cond_1
    const-string v0, "SYMBOLS"

    goto :goto_0
.end method
