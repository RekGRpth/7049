.class Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;
.super Lcom/android/inputmethod/keyboard/internal/ModifierKeyState;
.source "ShiftKeyState.java"


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/android/inputmethod/keyboard/internal/ModifierKeyState;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public isIgnoring()Z
    .locals 2

    iget v0, p0, Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;->mState:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPressingOnShifted()Z
    .locals 2

    iget v0, p0, Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;->mState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onOtherKeyPressed()V
    .locals 2

    iget v0, p0, Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;->mState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    const/4 v1, 0x2

    iput v1, p0, Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;->mState:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v1, 0x4

    iput v1, p0, Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;->mState:I

    goto :goto_0
.end method

.method public onPressOnShifted()V
    .locals 2

    iget v0, p0, Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;->mState:I

    const/4 v1, 0x3

    iput v1, p0, Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;->mState:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;->mState:I

    invoke-virtual {p0, v0}, Lcom/android/inputmethod/keyboard/internal/ShiftKeyState;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected toString(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1}, Lcom/android/inputmethod/keyboard/internal/ModifierKeyState;->toString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "PRESSING_ON_SHIFTED"

    goto :goto_0

    :pswitch_1
    const-string v0, "IGNORING"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
