.class Lcom/android/inputmethod/latin/AdditionalSubtypeSettings$SubtypeLocaleAdapter;
.super Landroid/widget/ArrayAdapter;
.source "AdditionalSubtypeSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/latin/AdditionalSubtypeSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "SubtypeLocaleAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/android/inputmethod/latin/AdditionalSubtypeSettings$SubtypeLocaleItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1    # Landroid/content/Context;

    const v5, 0x1090008

    invoke-direct {p0, p1, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    const v5, 0x1090009

    invoke-virtual {p0, v5}, Lcom/android/inputmethod/latin/AdditionalSubtypeSettings$SubtypeLocaleAdapter;->setDropDownViewResource(I)V

    new-instance v3, Ljava/util/TreeSet;

    invoke-direct {v3}, Ljava/util/TreeSet;-><init>()V

    invoke-static {p1}, Lcom/android/inputmethod/latin/ImfUtils;->getInputMethodInfoOfThisIme(Landroid/content/Context;)Landroid/view/inputmethod/InputMethodInfo;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/inputmethod/InputMethodInfo;->getSubtypeCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    invoke-virtual {v2, v1}, Landroid/view/inputmethod/InputMethodInfo;->getSubtypeAt(I)Landroid/view/inputmethod/InputMethodSubtype;

    move-result-object v4

    const-string v5, "AsciiCapable"

    invoke-virtual {v4, v5}, Landroid/view/inputmethod/InputMethodSubtype;->containsExtraValueKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v4}, Landroid/view/inputmethod/InputMethodSubtype;->getLocale()Ljava/lang/String;

    move-result-object v5

    invoke-static {p1, v5}, Lcom/android/inputmethod/latin/AdditionalSubtypeSettings$SubtypeLocaleAdapter;->createItem(Landroid/content/Context;Ljava/lang/String;)Lcom/android/inputmethod/latin/AdditionalSubtypeSettings$SubtypeLocaleItem;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v3}, Lcom/android/inputmethod/latin/AdditionalSubtypeSettings$SubtypeLocaleAdapter;->addAll(Ljava/util/Collection;)V

    return-void
.end method

.method public static createItem(Landroid/content/Context;Ljava/lang/String;)Lcom/android/inputmethod/latin/AdditionalSubtypeSettings$SubtypeLocaleItem;
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    const-string v1, "zz"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f0a008f

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/android/inputmethod/latin/AdditionalSubtypeSettings$SubtypeLocaleItem;

    invoke-direct {v1, p1, v0}, Lcom/android/inputmethod/latin/AdditionalSubtypeSettings$SubtypeLocaleItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/android/inputmethod/latin/AdditionalSubtypeSettings$SubtypeLocaleItem;

    invoke-direct {v1, p1}, Lcom/android/inputmethod/latin/AdditionalSubtypeSettings$SubtypeLocaleItem;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method
