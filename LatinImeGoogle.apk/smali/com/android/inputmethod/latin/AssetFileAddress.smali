.class Lcom/android/inputmethod/latin/AssetFileAddress;
.super Ljava/lang/Object;
.source "AssetFileAddress.java"


# instance fields
.field public final mFilename:Ljava/lang/String;

.field public final mLength:J

.field public final mOffset:J


# direct methods
.method public constructor <init>(Ljava/lang/String;JJ)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # J
    .param p4    # J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/inputmethod/latin/AssetFileAddress;->mFilename:Ljava/lang/String;

    iput-wide p2, p0, Lcom/android/inputmethod/latin/AssetFileAddress;->mOffset:J

    iput-wide p4, p0, Lcom/android/inputmethod/latin/AssetFileAddress;->mLength:J

    return-void
.end method

.method public static makeFromFileName(Ljava/lang/String;)Lcom/android/inputmethod/latin/AssetFileAddress;
    .locals 7
    .param p0    # Ljava/lang/String;

    const/4 v0, 0x0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->isFile()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/android/inputmethod/latin/AssetFileAddress;

    const-wide/16 v2, 0x0

    invoke-virtual {v6}, Ljava/io/File;->length()J

    move-result-wide v4

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/inputmethod/latin/AssetFileAddress;-><init>(Ljava/lang/String;JJ)V

    goto :goto_0
.end method

.method public static makeFromFileNameAndOffset(Ljava/lang/String;JJ)Lcom/android/inputmethod/latin/AssetFileAddress;
    .locals 7
    .param p0    # Ljava/lang/String;
    .param p1    # J
    .param p3    # J

    const/4 v0, 0x0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->isFile()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/android/inputmethod/latin/AssetFileAddress;

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/android/inputmethod/latin/AssetFileAddress;-><init>(Ljava/lang/String;JJ)V

    goto :goto_0
.end method
