.class public Lcom/android/inputmethod/latin/BinaryDictionary;
.super Lcom/android/inputmethod/latin/Dictionary;
.source "BinaryDictionary.java"


# instance fields
.field private final mBigramScores:[I

.field private mDicTypeId:I

.field private final mInputCodes:[I

.field private mNativeDict:J

.field private final mOutputChars:[C

.field private final mOutputChars_bigrams:[C

.field private final mScores:[I

.field private final mUseFullEditDistance:Z


# direct methods
.method static constructor <clinit>()V
    .locals 0

    invoke-static {}, Lcom/android/inputmethod/latin/JniUtils;->loadNativeLibrary()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;JJZLjava/util/Locale;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # J
    .param p5    # J
    .param p7    # Z
    .param p8    # Ljava/util/Locale;

    invoke-direct {p0}, Lcom/android/inputmethod/latin/Dictionary;-><init>()V

    const/16 v0, 0x30

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/inputmethod/latin/BinaryDictionary;->mInputCodes:[I

    const/16 v0, 0x360

    new-array v0, v0, [C

    iput-object v0, p0, Lcom/android/inputmethod/latin/BinaryDictionary;->mOutputChars:[C

    const/16 v0, 0xb40

    new-array v0, v0, [C

    iput-object v0, p0, Lcom/android/inputmethod/latin/BinaryDictionary;->mOutputChars_bigrams:[C

    const/16 v0, 0x12

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/inputmethod/latin/BinaryDictionary;->mScores:[I

    const/16 v0, 0x3c

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/inputmethod/latin/BinaryDictionary;->mBigramScores:[I

    const/4 v0, 0x1

    iput v0, p0, Lcom/android/inputmethod/latin/BinaryDictionary;->mDicTypeId:I

    iput-boolean p7, p0, Lcom/android/inputmethod/latin/BinaryDictionary;->mUseFullEditDistance:Z

    move-object v0, p0

    move-object v1, p2

    move-wide v2, p3

    move-wide v4, p5

    invoke-direct/range {v0 .. v5}, Lcom/android/inputmethod/latin/BinaryDictionary;->loadDictionary(Ljava/lang/String;JJ)V

    return-void
.end method

.method public static calcNormalizedScore(Ljava/lang/String;Ljava/lang/String;I)F
    .locals 4
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-static {v0, v1, v2, v3, p2}, Lcom/android/inputmethod/latin/BinaryDictionary;->calcNormalizedScoreNative([CI[CII)F

    move-result v0

    return v0
.end method

.method private static native calcNormalizedScoreNative([CI[CII)F
.end method

.method private closeInternal()V
    .locals 4

    const-wide/16 v2, 0x0

    iget-wide v0, p0, Lcom/android/inputmethod/latin/BinaryDictionary;->mNativeDict:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/android/inputmethod/latin/BinaryDictionary;->mNativeDict:J

    invoke-direct {p0, v0, v1}, Lcom/android/inputmethod/latin/BinaryDictionary;->closeNative(J)V

    iput-wide v2, p0, Lcom/android/inputmethod/latin/BinaryDictionary;->mNativeDict:J

    :cond_0
    return-void
.end method

.method private native closeNative(J)V
.end method

.method public static editDistance(Ljava/lang/String;Ljava/lang/String;)I
    .locals 4
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lcom/android/inputmethod/latin/BinaryDictionary;->editDistanceNative([CI[CI)I

    move-result v0

    return v0
.end method

.method private static native editDistanceNative([CI[CI)I
.end method

.method private native getBigramsNative(J[II[II[C[III)I
.end method

.method private native getFrequencyNative(J[II)I
.end method

.method private native getSuggestionsNative(JJ[I[I[II[IZ[C[I)I
.end method

.method private native isValidBigramNative(J[I[I)Z
.end method

.method private final loadDictionary(Ljava/lang/String;JJ)V
    .locals 10
    .param p1    # Ljava/lang/String;
    .param p2    # J
    .param p4    # J

    const/4 v6, 0x2

    const/16 v8, 0x30

    const/16 v9, 0x12

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    move v7, v6

    invoke-direct/range {v0 .. v9}, Lcom/android/inputmethod/latin/BinaryDictionary;->openNative(Ljava/lang/String;JJIIII)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/inputmethod/latin/BinaryDictionary;->mNativeDict:J

    return-void
.end method

.method private native openNative(Ljava/lang/String;JJIIII)J
.end method


# virtual methods
.method public declared-synchronized close()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/android/inputmethod/latin/BinaryDictionary;->closeInternal()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    :try_start_0
    invoke-direct {p0}, Lcom/android/inputmethod/latin/BinaryDictionary;->closeInternal()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    return-void

    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method public getBigrams(Lcom/android/inputmethod/latin/WordComposer;Ljava/lang/CharSequence;Lcom/android/inputmethod/latin/Dictionary$WordCallback;)V
    .locals 17
    .param p1    # Lcom/android/inputmethod/latin/WordComposer;
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # Lcom/android/inputmethod/latin/Dictionary$WordCallback;

    move-object/from16 v0, p0

    iget-wide v1, v0, Lcom/android/inputmethod/latin/BinaryDictionary;->mNativeDict:J

    const-wide/16 v5, 0x0

    cmp-long v1, v1, v5

    if-nez v1, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/inputmethod/latin/StringUtils;->toCodePointArray(Ljava/lang/String;)[I

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/inputmethod/latin/BinaryDictionary;->mOutputChars_bigrams:[C

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljava/util/Arrays;->fill([CC)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/inputmethod/latin/BinaryDictionary;->mBigramScores:[I

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljava/util/Arrays;->fill([II)V

    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/latin/WordComposer;->size()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/inputmethod/latin/BinaryDictionary;->mInputCodes:[I

    const/4 v2, -0x1

    invoke-static {v1, v2}, Ljava/util/Arrays;->fill([II)V

    if-lez v7, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/inputmethod/latin/BinaryDictionary;->mInputCodes:[I

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/android/inputmethod/latin/WordComposer;->getCodeAt(I)I

    move-result v3

    aput v3, v1, v2

    :cond_2
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/android/inputmethod/latin/BinaryDictionary;->mNativeDict:J

    array-length v5, v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/inputmethod/latin/BinaryDictionary;->mInputCodes:[I

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/inputmethod/latin/BinaryDictionary;->mOutputChars_bigrams:[C

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/inputmethod/latin/BinaryDictionary;->mBigramScores:[I

    const/16 v10, 0x30

    const/16 v11, 0x3c

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v11}, Lcom/android/inputmethod/latin/BinaryDictionary;->getBigramsNative(J[II[II[C[III)I

    move-result v15

    const/16 v1, 0x3c

    if-le v15, v1, :cond_3

    const/16 v15, 0x3c

    :cond_3
    const/16 v16, 0x0

    :goto_0
    move/from16 v0, v16

    if-ge v0, v15, :cond_0

    if-lez v7, :cond_4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/inputmethod/latin/BinaryDictionary;->mBigramScores:[I

    aget v1, v1, v16

    const/4 v2, 0x1

    if-lt v1, v2, :cond_0

    :cond_4
    mul-int/lit8 v10, v16, 0x30

    const/4 v11, 0x0

    :goto_1
    const/16 v1, 0x30

    if-ge v11, v1, :cond_5

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/inputmethod/latin/BinaryDictionary;->mOutputChars_bigrams:[C

    add-int v2, v10, v11

    aget-char v1, v1, v2

    if-eqz v1, :cond_5

    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    :cond_5
    if-lez v11, :cond_6

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/inputmethod/latin/BinaryDictionary;->mOutputChars_bigrams:[C

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/inputmethod/latin/BinaryDictionary;->mBigramScores:[I

    aget v12, v1, v16

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/inputmethod/latin/BinaryDictionary;->mDicTypeId:I

    const/4 v14, 0x1

    move-object/from16 v8, p3

    invoke-interface/range {v8 .. v14}, Lcom/android/inputmethod/latin/Dictionary$WordCallback;->addWord([CIIIII)Z

    :cond_6
    add-int/lit8 v16, v16, 0x1

    goto :goto_0
.end method

.method public getFrequency(Ljava/lang/CharSequence;)I
    .locals 4
    .param p1    # Ljava/lang/CharSequence;

    if-nez p1, :cond_0

    const/4 v1, -0x1

    :goto_0
    return v1

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/inputmethod/latin/StringUtils;->toCodePointArray(Ljava/lang/String;)[I

    move-result-object v0

    iget-wide v1, p0, Lcom/android/inputmethod/latin/BinaryDictionary;->mNativeDict:J

    array-length v3, v0

    invoke-direct {p0, v1, v2, v0, v3}, Lcom/android/inputmethod/latin/BinaryDictionary;->getFrequencyNative(J[II)I

    move-result v1

    goto :goto_0
.end method

.method getSuggestions(Lcom/android/inputmethod/latin/WordComposer;Ljava/lang/CharSequence;Lcom/android/inputmethod/keyboard/ProximityInfo;[C[I)I
    .locals 15
    .param p1    # Lcom/android/inputmethod/latin/WordComposer;
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # Lcom/android/inputmethod/keyboard/ProximityInfo;
    .param p4    # [C
    .param p5    # [I

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/BinaryDictionary;->isValidDictionary()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, -0x1

    :goto_0
    return v1

    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/latin/WordComposer;->size()I

    move-result v9

    const/16 v1, 0x2f

    if-le v9, v1, :cond_1

    const/4 v1, -0x1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/android/inputmethod/latin/BinaryDictionary;->mInputCodes:[I

    const/4 v2, -0x1

    invoke-static {v1, v2}, Ljava/util/Arrays;->fill([II)V

    const/4 v14, 0x0

    :goto_1
    if-ge v14, v9, :cond_2

    iget-object v1, p0, Lcom/android/inputmethod/latin/BinaryDictionary;->mInputCodes:[I

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/android/inputmethod/latin/WordComposer;->getCodeAt(I)I

    move-result v2

    aput v2, v1, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    move-object/from16 v0, p4

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([CC)V

    const/4 v1, 0x0

    move-object/from16 v0, p5

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    if-nez p2, :cond_3

    const/4 v10, 0x0

    :goto_2
    iget-wide v2, p0, Lcom/android/inputmethod/latin/BinaryDictionary;->mNativeDict:J

    invoke-virtual/range {p3 .. p3}, Lcom/android/inputmethod/keyboard/ProximityInfo;->getNativeProximityInfo()J

    move-result-wide v4

    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/latin/WordComposer;->getXCoordinates()[I

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/latin/WordComposer;->getYCoordinates()[I

    move-result-object v7

    iget-object v8, p0, Lcom/android/inputmethod/latin/BinaryDictionary;->mInputCodes:[I

    iget-boolean v11, p0, Lcom/android/inputmethod/latin/BinaryDictionary;->mUseFullEditDistance:Z

    move-object v1, p0

    move-object/from16 v12, p4

    move-object/from16 v13, p5

    invoke-direct/range {v1 .. v13}, Lcom/android/inputmethod/latin/BinaryDictionary;->getSuggestionsNative(JJ[I[I[II[IZ[C[I)I

    move-result v1

    goto :goto_0

    :cond_3
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/inputmethod/latin/StringUtils;->toCodePointArray(Ljava/lang/String;)[I

    move-result-object v10

    goto :goto_2
.end method

.method public getWords(Lcom/android/inputmethod/latin/WordComposer;Ljava/lang/CharSequence;Lcom/android/inputmethod/latin/Dictionary$WordCallback;Lcom/android/inputmethod/keyboard/ProximityInfo;)V
    .locals 9
    .param p1    # Lcom/android/inputmethod/latin/WordComposer;
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # Lcom/android/inputmethod/latin/Dictionary$WordCallback;
    .param p4    # Lcom/android/inputmethod/keyboard/ProximityInfo;

    iget-object v4, p0, Lcom/android/inputmethod/latin/BinaryDictionary;->mOutputChars:[C

    iget-object v5, p0, Lcom/android/inputmethod/latin/BinaryDictionary;->mScores:[I

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    invoke-virtual/range {v0 .. v5}, Lcom/android/inputmethod/latin/BinaryDictionary;->getSuggestions(Lcom/android/inputmethod/latin/WordComposer;Ljava/lang/CharSequence;Lcom/android/inputmethod/keyboard/ProximityInfo;[C[I)I

    move-result v7

    const/4 v8, 0x0

    :goto_0
    if-ge v8, v7, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/latin/BinaryDictionary;->mScores:[I

    aget v0, v0, v8

    const/4 v1, 0x1

    if-ge v0, v1, :cond_1

    :cond_0
    return-void

    :cond_1
    mul-int/lit8 v2, v8, 0x30

    const/4 v3, 0x0

    :goto_1
    const/16 v0, 0x30

    if-ge v3, v0, :cond_2

    iget-object v0, p0, Lcom/android/inputmethod/latin/BinaryDictionary;->mOutputChars:[C

    add-int v1, v2, v3

    aget-char v0, v0, v1

    if-eqz v0, :cond_2

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    if-lez v3, :cond_3

    iget-object v1, p0, Lcom/android/inputmethod/latin/BinaryDictionary;->mOutputChars:[C

    iget-object v0, p0, Lcom/android/inputmethod/latin/BinaryDictionary;->mScores:[I

    aget v4, v0, v8

    iget v5, p0, Lcom/android/inputmethod/latin/BinaryDictionary;->mDicTypeId:I

    const/4 v6, 0x0

    move-object v0, p3

    invoke-interface/range {v0 .. v6}, Lcom/android/inputmethod/latin/Dictionary$WordCallback;->addWord([CIIIII)Z

    :cond_3
    add-int/lit8 v8, v8, 0x1

    goto :goto_0
.end method

.method public isValidBigram(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    .locals 4
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Ljava/lang/CharSequence;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/inputmethod/latin/StringUtils;->toCodePointArray(Ljava/lang/String;)[I

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/inputmethod/latin/StringUtils;->toCodePointArray(Ljava/lang/String;)[I

    move-result-object v1

    iget-wide v2, p0, Lcom/android/inputmethod/latin/BinaryDictionary;->mNativeDict:J

    invoke-direct {p0, v2, v3, v0, v1}, Lcom/android/inputmethod/latin/BinaryDictionary;->isValidBigramNative(J[I[I)Z

    move-result v2

    goto :goto_0
.end method

.method isValidDictionary()Z
    .locals 4

    iget-wide v0, p0, Lcom/android/inputmethod/latin/BinaryDictionary;->mNativeDict:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isValidWord(Ljava/lang/CharSequence;)Z
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    invoke-virtual {p0, p1}, Lcom/android/inputmethod/latin/BinaryDictionary;->getFrequency(Ljava/lang/CharSequence;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
