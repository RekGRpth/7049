.class public abstract Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;
.super Lcom/android/inputmethod/latin/Dictionary;
.source "ExpandableBinaryDictionary.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$1;,
        Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;,
        Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$AsyncReloadDictionaryTask;
    }
.end annotation


# static fields
.field private static DEBUG:Z

.field private static final TAG:Ljava/lang/String;

.field private static final sSharedDictionaryControllers:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mBinaryDictionary:Lcom/android/inputmethod/latin/BinaryDictionary;

.field protected final mContext:Landroid/content/Context;

.field public final mDicTypeId:I

.field private final mFilename:Ljava/lang/String;

.field private mFusionDictionary:Lcom/android/inputmethod/latin/makedict/FusionDictionary;

.field private final mLocalDictionaryController:Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;

.field private final mSharedDictionaryController:Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->TAG:Ljava/lang/String;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->DEBUG:Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->sSharedDictionaryControllers:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/android/inputmethod/latin/Dictionary;-><init>()V

    new-instance v0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;

    invoke-direct {v0, v1}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;-><init>(Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$1;)V

    iput-object v0, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mLocalDictionaryController:Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;

    iput p3, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mDicTypeId:I

    iput-object p2, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mFilename:Ljava/lang/String;

    iput-object p1, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mContext:Landroid/content/Context;

    iput-object v1, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mBinaryDictionary:Lcom/android/inputmethod/latin/BinaryDictionary;

    invoke-static {p2}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->getSharedDictionaryController(Ljava/lang/String;)Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mSharedDictionaryController:Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->clearFusionDictionary()V

    return-void
.end method

.method static synthetic access$500(Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;)V
    .locals 0
    .param p0    # Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;

    invoke-direct {p0}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->syncReloadDictionaryInternal()V

    return-void
.end method

.method private dictionaryFileExists()Z
    .locals 3

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    iget-object v2, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mFilename:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    return v1
.end method

.method private generateBinaryDictionary()V
    .locals 10

    sget-boolean v6, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->DEBUG:Z

    if-eqz v6, :cond_0

    sget-object v6, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Generating binary dictionary: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mFilename:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " request="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mSharedDictionaryController:Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;

    # getter for: Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->mLastUpdateRequestTime:J
    invoke-static {v8}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->access$100(Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;)J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " update="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mSharedDictionaryController:Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;

    # getter for: Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->mLastUpdateTime:J
    invoke-static {v8}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->access$200(Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;)J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->loadDictionaryAsync()V

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mFilename:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".temp"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v1, Ljava/io/File;

    iget-object v6, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v6

    iget-object v7, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mFilename:Ljava/lang/String;

    invoke-direct {v1, v6, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v4, Ljava/io/File;

    iget-object v6, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v6

    invoke-direct {v4, v6, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const/4 v2, 0x0

    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/android/inputmethod/latin/makedict/UnsupportedFormatException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v6, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mFusionDictionary:Lcom/android/inputmethod/latin/makedict/FusionDictionary;

    const/4 v7, 0x1

    invoke-static {v3, v6, v7}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->writeDictionaryBinary(Ljava/io/OutputStream;Lcom/android/inputmethod/latin/makedict/FusionDictionary;I)V

    invoke-virtual {v3}, Ljava/io/FileOutputStream;->flush()V

    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V

    invoke-virtual {v4, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->clearFusionDictionary()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Lcom/android/inputmethod/latin/makedict/UnsupportedFormatException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v3, :cond_3

    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v2, v3

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v6

    move-object v2, v3

    goto :goto_0

    :catch_1
    move-exception v0

    :goto_1
    :try_start_3
    sget-object v6, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "IO exception while writing file: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v2, :cond_1

    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    :catch_2
    move-exception v6

    goto :goto_0

    :catch_3
    move-exception v0

    :goto_2
    :try_start_5
    sget-object v6, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unsupported format: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-eqz v2, :cond_1

    :try_start_6
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_0

    :catch_4
    move-exception v6

    goto :goto_0

    :catchall_0
    move-exception v6

    :goto_3
    if-eqz v2, :cond_2

    :try_start_7
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    :cond_2
    :goto_4
    throw v6

    :catch_5
    move-exception v7

    goto :goto_4

    :catchall_1
    move-exception v6

    move-object v2, v3

    goto :goto_3

    :catch_6
    move-exception v0

    move-object v2, v3

    goto :goto_2

    :catch_7
    move-exception v0

    move-object v2, v3

    goto :goto_1

    :cond_3
    move-object v2, v3

    goto :goto_0
.end method

.method protected static getFilenameWithLocale(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".dict"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static declared-synchronized getSharedDictionaryController(Ljava/lang/String;)Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;
    .locals 3
    .param p0    # Ljava/lang/String;

    const-class v2, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->sSharedDictionaryControllers:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;-><init>(Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$1;)V

    sget-object v1, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->sSharedDictionaryControllers:Ljava/util/HashMap;

    invoke-virtual {v1, p0, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v2

    return-object v0

    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method private isReloadRequired()Z
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mBinaryDictionary:Lcom/android/inputmethod/latin/BinaryDictionary;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mLocalDictionaryController:Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;

    # invokes: Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->isOutOfDate()Z
    invoke-static {v0}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->access$400(Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final syncReloadDictionaryInternal()V
    .locals 7

    iget-object v3, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mSharedDictionaryController:Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;

    invoke-virtual {v3}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->lock()V

    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    invoke-direct {p0}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->dictionaryFileExists()Z

    move-result v0

    iget-object v3, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mSharedDictionaryController:Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;

    # invokes: Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->isOutOfDate()Z
    invoke-static {v3}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->access$400(Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;)Z

    move-result v3

    if-nez v3, :cond_0

    if-nez v0, :cond_4

    :cond_0
    invoke-virtual {p0}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->hasContentChanged()Z

    move-result v3

    if-nez v3, :cond_1

    if-nez v0, :cond_3

    :cond_1
    iget-object v3, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mSharedDictionaryController:Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;

    # setter for: Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->mLastUpdateTime:J
    invoke-static {v3, v1, v2}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->access$202(Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;J)J

    invoke-direct {p0}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->generateBinaryDictionary()V

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->loadBinaryDictionary()V

    :cond_2
    :goto_0
    iget-object v3, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mLocalDictionaryController:Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;

    # setter for: Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->mLastUpdateTime:J
    invoke-static {v3, v1, v2}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->access$202(Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;J)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v3, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mSharedDictionaryController:Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;

    invoke-virtual {v3}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->unlock()V

    return-void

    :cond_3
    :try_start_1
    iget-object v3, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mSharedDictionaryController:Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;

    iget-object v4, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mSharedDictionaryController:Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;

    # getter for: Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->mLastUpdateTime:J
    invoke-static {v4}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->access$200(Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;)J

    move-result-wide v4

    # setter for: Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->mLastUpdateRequestTime:J
    invoke-static {v3, v4, v5}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->access$102(Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;J)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mSharedDictionaryController:Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;

    invoke-virtual {v4}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->unlock()V

    throw v3

    :cond_4
    :try_start_2
    iget-object v3, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mBinaryDictionary:Lcom/android/inputmethod/latin/BinaryDictionary;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mLocalDictionaryController:Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;

    # getter for: Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->mLastUpdateTime:J
    invoke-static {v3}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->access$200(Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;)J

    move-result-wide v3

    iget-object v5, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mSharedDictionaryController:Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;

    # getter for: Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->mLastUpdateTime:J
    invoke-static {v5}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->access$200(Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;)J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-gez v3, :cond_2

    :cond_5
    invoke-virtual {p0}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->loadBinaryDictionary()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method


# virtual methods
.method protected addWord(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    if-nez p2, :cond_0

    iget-object v1, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mFusionDictionary:Lcom/android/inputmethod/latin/makedict/FusionDictionary;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, p3, v2}, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->add(Ljava/lang/String;ILjava/util/ArrayList;)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;

    invoke-direct {v1, p2, p3}, Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mFusionDictionary:Lcom/android/inputmethod/latin/makedict/FusionDictionary;

    invoke-virtual {v1, p1, p3, v0}, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->add(Ljava/lang/String;ILjava/util/ArrayList;)V

    goto :goto_0
.end method

.method asyncReloadDictionaryIfRequired()V
    .locals 3

    invoke-direct {p0}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->isReloadRequired()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-boolean v0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->DEBUG:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Starting AsyncReloadDictionaryTask: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mFilename:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    new-instance v0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$AsyncReloadDictionaryTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$AsyncReloadDictionaryTask;-><init>(Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$1;)V

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$AsyncReloadDictionaryTask;->start()V

    goto :goto_0
.end method

.method public clearFusionDictionary()V
    .locals 5

    const/4 v4, 0x0

    new-instance v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary;

    new-instance v1, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    invoke-direct {v1}, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;-><init>()V

    new-instance v2, Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryOptions;

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    invoke-direct {v2, v3, v4, v4}, Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryOptions;-><init>(Ljava/util/HashMap;ZZ)V

    invoke-direct {v0, v1, v2}, Lcom/android/inputmethod/latin/makedict/FusionDictionary;-><init>(Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryOptions;)V

    iput-object v0, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mFusionDictionary:Lcom/android/inputmethod/latin/makedict/FusionDictionary;

    return-void
.end method

.method public close()V
    .locals 2

    iget-object v0, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mLocalDictionaryController:Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->lock()V

    :try_start_0
    iget-object v0, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mBinaryDictionary:Lcom/android/inputmethod/latin/BinaryDictionary;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mBinaryDictionary:Lcom/android/inputmethod/latin/BinaryDictionary;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/BinaryDictionary;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mBinaryDictionary:Lcom/android/inputmethod/latin/BinaryDictionary;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    iget-object v0, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mLocalDictionaryController:Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->unlock()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mLocalDictionaryController:Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->unlock()V

    throw v0
.end method

.method public getBigrams(Lcom/android/inputmethod/latin/WordComposer;Ljava/lang/CharSequence;Lcom/android/inputmethod/latin/Dictionary$WordCallback;)V
    .locals 0
    .param p1    # Lcom/android/inputmethod/latin/WordComposer;
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # Lcom/android/inputmethod/latin/Dictionary$WordCallback;

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->asyncReloadDictionaryIfRequired()V

    invoke-virtual {p0, p1, p2, p3}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->getBigramsInner(Lcom/android/inputmethod/latin/WordComposer;Ljava/lang/CharSequence;Lcom/android/inputmethod/latin/Dictionary$WordCallback;)V

    return-void
.end method

.method protected getBigramsInner(Lcom/android/inputmethod/latin/WordComposer;Ljava/lang/CharSequence;Lcom/android/inputmethod/latin/Dictionary$WordCallback;)V
    .locals 2
    .param p1    # Lcom/android/inputmethod/latin/WordComposer;
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # Lcom/android/inputmethod/latin/Dictionary$WordCallback;

    iget-object v0, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mLocalDictionaryController:Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mBinaryDictionary:Lcom/android/inputmethod/latin/BinaryDictionary;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mBinaryDictionary:Lcom/android/inputmethod/latin/BinaryDictionary;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/inputmethod/latin/BinaryDictionary;->getBigrams(Lcom/android/inputmethod/latin/WordComposer;Ljava/lang/CharSequence;Lcom/android/inputmethod/latin/Dictionary$WordCallback;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    iget-object v0, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mLocalDictionaryController:Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->unlock()V

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mLocalDictionaryController:Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->unlock()V

    throw v0
.end method

.method public getWords(Lcom/android/inputmethod/latin/WordComposer;Ljava/lang/CharSequence;Lcom/android/inputmethod/latin/Dictionary$WordCallback;Lcom/android/inputmethod/keyboard/ProximityInfo;)V
    .locals 0
    .param p1    # Lcom/android/inputmethod/latin/WordComposer;
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # Lcom/android/inputmethod/latin/Dictionary$WordCallback;
    .param p4    # Lcom/android/inputmethod/keyboard/ProximityInfo;

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->asyncReloadDictionaryIfRequired()V

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->getWordsInner(Lcom/android/inputmethod/latin/WordComposer;Ljava/lang/CharSequence;Lcom/android/inputmethod/latin/Dictionary$WordCallback;Lcom/android/inputmethod/keyboard/ProximityInfo;)V

    return-void
.end method

.method protected final getWordsInner(Lcom/android/inputmethod/latin/WordComposer;Ljava/lang/CharSequence;Lcom/android/inputmethod/latin/Dictionary$WordCallback;Lcom/android/inputmethod/keyboard/ProximityInfo;)V
    .locals 2
    .param p1    # Lcom/android/inputmethod/latin/WordComposer;
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # Lcom/android/inputmethod/latin/Dictionary$WordCallback;
    .param p4    # Lcom/android/inputmethod/keyboard/ProximityInfo;

    iget-object v0, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mLocalDictionaryController:Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mBinaryDictionary:Lcom/android/inputmethod/latin/BinaryDictionary;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mBinaryDictionary:Lcom/android/inputmethod/latin/BinaryDictionary;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/inputmethod/latin/BinaryDictionary;->getWords(Lcom/android/inputmethod/latin/WordComposer;Ljava/lang/CharSequence;Lcom/android/inputmethod/latin/Dictionary$WordCallback;Lcom/android/inputmethod/keyboard/ProximityInfo;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    iget-object v0, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mLocalDictionaryController:Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->unlock()V

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mLocalDictionaryController:Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->unlock()V

    throw v0
.end method

.method protected abstract hasContentChanged()Z
.end method

.method protected isValidBigramLocked(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mBinaryDictionary:Lcom/android/inputmethod/latin/BinaryDictionary;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mBinaryDictionary:Lcom/android/inputmethod/latin/BinaryDictionary;

    invoke-virtual {v0, p1, p2}, Lcom/android/inputmethod/latin/BinaryDictionary;->isValidBigram(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    goto :goto_0
.end method

.method public isValidWord(Ljava/lang/CharSequence;)Z
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->asyncReloadDictionaryIfRequired()V

    invoke-virtual {p0, p1}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->isValidWordInner(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method protected isValidWordInner(Ljava/lang/CharSequence;)Z
    .locals 2
    .param p1    # Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mLocalDictionaryController:Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->isValidWordLocked(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    iget-object v1, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mLocalDictionaryController:Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->unlock()V

    :goto_0
    return v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mLocalDictionaryController:Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->unlock()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isValidWordLocked(Ljava/lang/CharSequence;)Z
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mBinaryDictionary:Lcom/android/inputmethod/latin/BinaryDictionary;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mBinaryDictionary:Lcom/android/inputmethod/latin/BinaryDictionary;

    invoke-virtual {v0, p1}, Lcom/android/inputmethod/latin/BinaryDictionary;->isValidWord(Ljava/lang/CharSequence;)Z

    move-result v0

    goto :goto_0
.end method

.method protected loadBinaryDictionary()V
    .locals 11

    sget-boolean v1, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->DEBUG:Z

    if-eqz v1, :cond_0

    sget-object v1, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Loading binary dictionary: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mFilename:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " request="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mSharedDictionaryController:Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;

    # getter for: Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->mLastUpdateRequestTime:J
    invoke-static {v4}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->access$100(Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;)J

    move-result-wide v7

    invoke-virtual {v3, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " update="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mSharedDictionaryController:Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;

    # getter for: Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->mLastUpdateTime:J
    invoke-static {v4}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->access$200(Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;)J

    move-result-wide v7

    invoke-virtual {v3, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v9, Ljava/io/File;

    iget-object v1, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    iget-object v3, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mFilename:Ljava/lang/String;

    invoke-direct {v9, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v5

    new-instance v0, Lcom/android/inputmethod/latin/BinaryDictionary;

    iget-object v1, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mContext:Landroid/content/Context;

    const-wide/16 v3, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lcom/android/inputmethod/latin/BinaryDictionary;-><init>(Landroid/content/Context;Ljava/lang/String;JJZLjava/util/Locale;)V

    iget-object v1, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mBinaryDictionary:Lcom/android/inputmethod/latin/BinaryDictionary;

    if-eqz v1, :cond_1

    iget-object v10, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mBinaryDictionary:Lcom/android/inputmethod/latin/BinaryDictionary;

    iget-object v1, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mLocalDictionaryController:Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->lock()V

    iput-object v0, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mBinaryDictionary:Lcom/android/inputmethod/latin/BinaryDictionary;

    iget-object v1, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mLocalDictionaryController:Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->unlock()V

    invoke-virtual {v10}, Lcom/android/inputmethod/latin/BinaryDictionary;->close()V

    :goto_0
    return-void

    :cond_1
    iput-object v0, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mBinaryDictionary:Lcom/android/inputmethod/latin/BinaryDictionary;

    goto :goto_0
.end method

.method protected loadDictionary()V
    .locals 3

    iget-object v0, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mLocalDictionaryController:Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    # setter for: Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->mLastUpdateRequestTime:J
    invoke-static {v0, v1, v2}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->access$102(Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;J)J

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->asyncReloadDictionaryIfRequired()V

    return-void
.end method

.method protected abstract loadDictionaryAsync()V
.end method

.method protected setBigram(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    iget-object v0, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mFusionDictionary:Lcom/android/inputmethod/latin/makedict/FusionDictionary;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->setBigram(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method protected setRequiresReload(Z)V
    .locals 6
    .param p1    # Z

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mLocalDictionaryController:Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;

    # setter for: Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->mLastUpdateRequestTime:J
    invoke-static {v2, v0, v1}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->access$102(Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;J)J

    iget-object v2, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mSharedDictionaryController:Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;

    # setter for: Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->mLastUpdateRequestTime:J
    invoke-static {v2, v0, v1}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->access$102(Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;J)J

    sget-boolean v2, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->DEBUG:Z

    if-eqz v2, :cond_0

    sget-object v2, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Reload request: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mFilename:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": request="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " update="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->mSharedDictionaryController:Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;

    # getter for: Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->mLastUpdateTime:J
    invoke-static {v4}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;->access$200(Lcom/android/inputmethod/latin/ExpandableBinaryDictionary$DictionaryController;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method protected final syncReloadDictionaryIfRequired()V
    .locals 1

    invoke-direct {p0}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->isReloadRequired()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/android/inputmethod/latin/ExpandableBinaryDictionary;->syncReloadDictionaryInternal()V

    goto :goto_0
.end method
