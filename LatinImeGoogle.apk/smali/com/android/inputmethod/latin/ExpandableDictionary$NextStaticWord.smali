.class Lcom/android/inputmethod/latin/ExpandableDictionary$NextStaticWord;
.super Ljava/lang/Object;
.source "ExpandableDictionary.java"

# interfaces
.implements Lcom/android/inputmethod/latin/ExpandableDictionary$NextWord;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/latin/ExpandableDictionary;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "NextStaticWord"
.end annotation


# instance fields
.field private final mFrequency:I

.field public final mWord:Lcom/android/inputmethod/latin/ExpandableDictionary$Node;


# direct methods
.method public constructor <init>(Lcom/android/inputmethod/latin/ExpandableDictionary$Node;I)V
    .locals 0
    .param p1    # Lcom/android/inputmethod/latin/ExpandableDictionary$Node;
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/inputmethod/latin/ExpandableDictionary$NextStaticWord;->mWord:Lcom/android/inputmethod/latin/ExpandableDictionary$Node;

    iput p2, p0, Lcom/android/inputmethod/latin/ExpandableDictionary$NextStaticWord;->mFrequency:I

    return-void
.end method


# virtual methods
.method public getFcParams()Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getFrequency()I
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/latin/ExpandableDictionary$NextStaticWord;->mFrequency:I

    return v0
.end method

.method public getWordNode()Lcom/android/inputmethod/latin/ExpandableDictionary$Node;
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/ExpandableDictionary$NextStaticWord;->mWord:Lcom/android/inputmethod/latin/ExpandableDictionary$Node;

    return-object v0
.end method

.method public notifyTypedAgainAndGetFrequency()I
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/latin/ExpandableDictionary$NextStaticWord;->mFrequency:I

    return v0
.end method
