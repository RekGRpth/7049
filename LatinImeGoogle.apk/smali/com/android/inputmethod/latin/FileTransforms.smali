.class public Lcom/android/inputmethod/latin/FileTransforms;
.super Ljava/lang/Object;
.source "FileTransforms.java"


# static fields
.field private static final DECRYPT_CIPHER:Ljavax/crypto/Cipher;

.field private static final ENCRYPT_CIPHER:Ljavax/crypto/Cipher;

.field private static final KEY:Ljavax/crypto/spec/SecretKeySpec;

.field private static final KEYDATA:[B

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v2, 0x0

    const-class v0, Lcom/android/inputmethod/latin/FileTransforms;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/inputmethod/latin/FileTransforms;->TAG:Ljava/lang/String;

    const/16 v0, 0x20

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/inputmethod/latin/FileTransforms;->KEYDATA:[B

    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    sget-object v1, Lcom/android/inputmethod/latin/FileTransforms;->KEYDATA:[B

    const-string v3, "AES/ECB/PKCS5Padding"

    invoke-direct {v0, v1, v3}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    sput-object v0, Lcom/android/inputmethod/latin/FileTransforms;->KEY:Ljavax/crypto/spec/SecretKeySpec;

    :try_start_0
    const-string v0, "AES/ECB/PKCS5Padding"

    invoke-static {v0}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v1

    :try_start_1
    const-string v0, "AES/ECB/PKCS5Padding"

    invoke-static {v0}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/security/InvalidKeyException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v0

    const/4 v2, 0x1

    :try_start_2
    sget-object v3, Lcom/android/inputmethod/latin/FileTransforms;->KEY:Ljavax/crypto/spec/SecretKeySpec;

    invoke-virtual {v1, v2, v3}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    const/4 v2, 0x2

    sget-object v3, Lcom/android/inputmethod/latin/FileTransforms;->KEY:Ljavax/crypto/spec/SecretKeySpec;

    invoke-virtual {v0, v2, v3}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V
    :try_end_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_2} :catch_8
    .catch Ljava/security/InvalidKeyException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_2 .. :try_end_2} :catch_4

    :goto_0
    sput-object v1, Lcom/android/inputmethod/latin/FileTransforms;->ENCRYPT_CIPHER:Ljavax/crypto/Cipher;

    sput-object v0, Lcom/android/inputmethod/latin/FileTransforms;->DECRYPT_CIPHER:Ljavax/crypto/Cipher;

    return-void

    :catch_0
    move-exception v0

    move-object v1, v2

    move-object v6, v2

    move-object v2, v0

    move-object v0, v6

    :goto_1
    sget-object v3, Lcom/android/inputmethod/latin/FileTransforms;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Can\'t create the cipher: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v1, v2

    move-object v6, v2

    move-object v2, v0

    move-object v0, v6

    :goto_2
    sget-object v3, Lcom/android/inputmethod/latin/FileTransforms;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Can\'t create the cipher: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_2
    move-exception v0

    move-object v1, v2

    move-object v6, v2

    move-object v2, v0

    move-object v0, v6

    :goto_3
    sget-object v3, Lcom/android/inputmethod/latin/FileTransforms;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Can\'t create the cipher: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_3
    move-exception v0

    move-object v6, v0

    move-object v0, v2

    move-object v2, v6

    goto :goto_3

    :catch_4
    move-exception v2

    goto :goto_3

    :catch_5
    move-exception v0

    move-object v6, v0

    move-object v0, v2

    move-object v2, v6

    goto :goto_2

    :catch_6
    move-exception v2

    goto :goto_2

    :catch_7
    move-exception v0

    move-object v6, v0

    move-object v0, v2

    move-object v2, v6

    goto :goto_1

    :catch_8
    move-exception v2

    goto :goto_1

    nop

    :array_0
    .array-data 1
        0x4et
        0x3bt
        0x74t
        0x28t
        -0xdt
        -0x2at
        0x2bt
        -0x2at
        0x7t
        -0x3et
        -0x5t
        -0x43t
        -0x1bt
        -0x7dt
        0x58t
        0x20t
        0x6t
        -0x13t
        -0x5ct
        -0x7t
        -0x7dt
        -0x42t
        -0x6at
        -0x6t
        -0x1at
        0x3ft
        0x17t
        0x28t
        0x4at
        -0x1et
        -0x18t
        -0x17t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDecryptedStream(Ljava/io/InputStream;)Ljava/io/InputStream;
    .locals 2
    .param p0    # Ljava/io/InputStream;

    sget-object v0, Lcom/android/inputmethod/latin/FileTransforms;->DECRYPT_CIPHER:Ljavax/crypto/Cipher;

    if-nez v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Ljavax/crypto/CipherInputStream;

    sget-object v1, Lcom/android/inputmethod/latin/FileTransforms;->DECRYPT_CIPHER:Ljavax/crypto/Cipher;

    invoke-direct {v0, p0, v1}, Ljavax/crypto/CipherInputStream;-><init>(Ljava/io/InputStream;Ljavax/crypto/Cipher;)V

    move-object p0, v0

    goto :goto_0
.end method

.method public static getUncompressedStream(Ljava/io/InputStream;)Ljava/io/InputStream;
    .locals 1
    .param p0    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v0, p0}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    return-object v0
.end method
