.class public Lcom/android/inputmethod/latin/LastComposedWord;
.super Ljava/lang/Object;
.source "LastComposedWord.java"


# static fields
.field public static final NOT_A_COMPOSED_WORD:Lcom/android/inputmethod/latin/LastComposedWord;


# instance fields
.field private mActive:Z

.field public final mCommittedWord:Ljava/lang/String;

.field public final mPrevWord:Ljava/lang/CharSequence;

.field public final mPrimaryKeyCodes:[I

.field public final mSeparatorCode:I

.field public final mTypedWord:Ljava/lang/String;

.field public final mXCoordinates:[I

.field public final mYCoordinates:[I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v1, 0x0

    new-instance v0, Lcom/android/inputmethod/latin/LastComposedWord;

    const-string v4, ""

    const-string v5, ""

    const/4 v6, -0x1

    move-object v2, v1

    move-object v3, v1

    move-object v7, v1

    invoke-direct/range {v0 .. v7}, Lcom/android/inputmethod/latin/LastComposedWord;-><init>([I[I[ILjava/lang/String;Ljava/lang/String;ILjava/lang/CharSequence;)V

    sput-object v0, Lcom/android/inputmethod/latin/LastComposedWord;->NOT_A_COMPOSED_WORD:Lcom/android/inputmethod/latin/LastComposedWord;

    return-void
.end method

.method public constructor <init>([I[I[ILjava/lang/String;Ljava/lang/String;ILjava/lang/CharSequence;)V
    .locals 1
    .param p1    # [I
    .param p2    # [I
    .param p3    # [I
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # I
    .param p7    # Ljava/lang/CharSequence;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/inputmethod/latin/LastComposedWord;->mPrimaryKeyCodes:[I

    iput-object p2, p0, Lcom/android/inputmethod/latin/LastComposedWord;->mXCoordinates:[I

    iput-object p3, p0, Lcom/android/inputmethod/latin/LastComposedWord;->mYCoordinates:[I

    iput-object p4, p0, Lcom/android/inputmethod/latin/LastComposedWord;->mTypedWord:Ljava/lang/String;

    iput-object p5, p0, Lcom/android/inputmethod/latin/LastComposedWord;->mCommittedWord:Ljava/lang/String;

    iput p6, p0, Lcom/android/inputmethod/latin/LastComposedWord;->mSeparatorCode:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/inputmethod/latin/LastComposedWord;->mActive:Z

    iput-object p7, p0, Lcom/android/inputmethod/latin/LastComposedWord;->mPrevWord:Ljava/lang/CharSequence;

    return-void
.end method

.method public static getSeparatorLength(I)I
    .locals 1
    .param p0    # I

    const/4 v0, -0x1

    if-ne v0, p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public canRevertCommit()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/inputmethod/latin/LastComposedWord;->mActive:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/latin/LastComposedWord;->mCommittedWord:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public deactivate()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/inputmethod/latin/LastComposedWord;->mActive:Z

    return-void
.end method

.method public didCommitTypedWord()Z
    .locals 2

    iget-object v0, p0, Lcom/android/inputmethod/latin/LastComposedWord;->mTypedWord:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/inputmethod/latin/LastComposedWord;->mCommittedWord:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method
