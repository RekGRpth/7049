.class public Lcom/android/inputmethod/latin/LatinIME$UIHandler;
.super Lcom/android/inputmethod/latin/StaticInnerHandlerWrapper;
.source "LatinIME.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/latin/LatinIME;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UIHandler"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/inputmethod/latin/StaticInnerHandlerWrapper",
        "<",
        "Lcom/android/inputmethod/latin/LatinIME;",
        ">;"
    }
.end annotation


# instance fields
.field private mAppliedEditorInfo:Landroid/view/inputmethod/EditorInfo;

.field private mDelayUpdateShiftState:I

.field private mDelayUpdateSuggestions:I

.field private mDoubleSpacesTurnIntoPeriodTimeout:J

.field private mHasPendingFinishInput:Z

.field private mHasPendingFinishInputView:Z

.field private mHasPendingStartInput:Z

.field private mIsOrientationChanging:Z

.field private mPendingSuccessiveImsCallback:Z


# direct methods
.method public constructor <init>(Lcom/android/inputmethod/latin/LatinIME;)V
    .locals 0
    .param p1    # Lcom/android/inputmethod/latin/LatinIME;

    invoke-direct {p0, p1}, Lcom/android/inputmethod/latin/StaticInnerHandlerWrapper;-><init>(Ljava/lang/Object;)V

    return-void
.end method

.method private executePendingImsCallback(Lcom/android/inputmethod/latin/LatinIME;Landroid/view/inputmethod/EditorInfo;Z)V
    .locals 1
    .param p1    # Lcom/android/inputmethod/latin/LatinIME;
    .param p2    # Landroid/view/inputmethod/EditorInfo;
    .param p3    # Z

    iget-boolean v0, p0, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->mHasPendingFinishInputView:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->mHasPendingFinishInput:Z

    # invokes: Lcom/android/inputmethod/latin/LatinIME;->onFinishInputViewInternal(Z)V
    invoke-static {p1, v0}, Lcom/android/inputmethod/latin/LatinIME;->access$000(Lcom/android/inputmethod/latin/LatinIME;Z)V

    :cond_0
    iget-boolean v0, p0, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->mHasPendingFinishInput:Z

    if-eqz v0, :cond_1

    # invokes: Lcom/android/inputmethod/latin/LatinIME;->onFinishInputInternal()V
    invoke-static {p1}, Lcom/android/inputmethod/latin/LatinIME;->access$100(Lcom/android/inputmethod/latin/LatinIME;)V

    :cond_1
    iget-boolean v0, p0, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->mHasPendingStartInput:Z

    if-eqz v0, :cond_2

    # invokes: Lcom/android/inputmethod/latin/LatinIME;->onStartInputInternal(Landroid/view/inputmethod/EditorInfo;Z)V
    invoke-static {p1, p2, p3}, Lcom/android/inputmethod/latin/LatinIME;->access$200(Lcom/android/inputmethod/latin/LatinIME;Landroid/view/inputmethod/EditorInfo;Z)V

    :cond_2
    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->resetPendingImsCallback()V

    return-void
.end method

.method private resetPendingImsCallback()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->mHasPendingFinishInputView:Z

    iput-boolean v0, p0, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->mHasPendingFinishInput:Z

    iput-boolean v0, p0, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->mHasPendingStartInput:Z

    return-void
.end method


# virtual methods
.method public cancelDoubleSpacesTimer()V
    .locals 1

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->removeMessages(I)V

    return-void
.end method

.method public cancelUpdateBigramPredictions()V
    .locals 1

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->removeMessages(I)V

    return-void
.end method

.method public cancelUpdateSuggestions()V
    .locals 1

    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->removeMessages(I)V

    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1    # Landroid/os/Message;

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->getOuterInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/inputmethod/latin/LatinIME;

    iget-object v1, v0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    iget v2, p1, Landroid/os/Message;->what:I

    sparse-switch v2, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    invoke-virtual {v0}, Lcom/android/inputmethod/latin/LatinIME;->updateSuggestions()V

    goto :goto_0

    :sswitch_1
    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->updateShiftState()V

    goto :goto_0

    :sswitch_2
    invoke-virtual {v0}, Lcom/android/inputmethod/latin/LatinIME;->updateBigramPredictions()V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x5 -> :sswitch_2
        0x7 -> :sswitch_0
    .end sparse-switch
.end method

.method public hasPendingUpdateSuggestions()Z
    .locals 1

    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->hasMessages(I)Z

    move-result v0

    return v0
.end method

.method public isAcceptingDoubleSpaces()Z
    .locals 1

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->hasMessages(I)Z

    move-result v0

    return v0
.end method

.method public onCreate()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->getOuterInstance()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/inputmethod/latin/LatinIME;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/LatinIME;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f090000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->mDelayUpdateSuggestions:I

    const v1, 0x7f090002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->mDelayUpdateShiftState:I

    const v1, 0x7f090008

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v1, v1

    iput-wide v1, p0, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->mDoubleSpacesTurnIntoPeriodTimeout:J

    return-void
.end method

.method public onFinishInput()V
    .locals 3

    const/4 v1, 0x6

    invoke-virtual {p0, v1}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->mHasPendingFinishInput:Z

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->getOuterInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/inputmethod/latin/LatinIME;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->executePendingImsCallback(Lcom/android/inputmethod/latin/LatinIME;Landroid/view/inputmethod/EditorInfo;Z)V

    # invokes: Lcom/android/inputmethod/latin/LatinIME;->onFinishInputInternal()V
    invoke-static {v0}, Lcom/android/inputmethod/latin/LatinIME;->access$100(Lcom/android/inputmethod/latin/LatinIME;)V

    goto :goto_0
.end method

.method public onFinishInputView(Z)V
    .locals 2
    .param p1    # Z

    const/4 v1, 0x6

    invoke-virtual {p0, v1}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->mHasPendingFinishInputView:Z

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->getOuterInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/inputmethod/latin/LatinIME;

    # invokes: Lcom/android/inputmethod/latin/LatinIME;->onFinishInputViewInternal(Z)V
    invoke-static {v0, p1}, Lcom/android/inputmethod/latin/LatinIME;->access$000(Lcom/android/inputmethod/latin/LatinIME;Z)V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->mAppliedEditorInfo:Landroid/view/inputmethod/EditorInfo;

    goto :goto_0
.end method

.method public onStartInput(Landroid/view/inputmethod/EditorInfo;Z)V
    .locals 3
    .param p1    # Landroid/view/inputmethod/EditorInfo;
    .param p2    # Z

    const/4 v2, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v1}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iput-boolean v2, p0, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->mHasPendingStartInput:Z

    :goto_0
    return-void

    :cond_0
    iget-boolean v1, p0, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->mIsOrientationChanging:Z

    if-eqz v1, :cond_1

    if-eqz p2, :cond_1

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->mIsOrientationChanging:Z

    iput-boolean v2, p0, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->mPendingSuccessiveImsCallback:Z

    :cond_1
    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->getOuterInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/inputmethod/latin/LatinIME;

    invoke-direct {p0, v0, p1, p2}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->executePendingImsCallback(Lcom/android/inputmethod/latin/LatinIME;Landroid/view/inputmethod/EditorInfo;Z)V

    # invokes: Lcom/android/inputmethod/latin/LatinIME;->onStartInputInternal(Landroid/view/inputmethod/EditorInfo;Z)V
    invoke-static {v0, p1, p2}, Lcom/android/inputmethod/latin/LatinIME;->access$200(Lcom/android/inputmethod/latin/LatinIME;Landroid/view/inputmethod/EditorInfo;Z)V

    goto :goto_0
.end method

.method public onStartInputView(Landroid/view/inputmethod/EditorInfo;Z)V
    .locals 4
    .param p1    # Landroid/view/inputmethod/EditorInfo;
    .param p2    # Z

    const/4 v2, 0x6

    invoke-virtual {p0, v2}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->mAppliedEditorInfo:Landroid/view/inputmethod/EditorInfo;

    invoke-static {p1, v1}, Lcom/android/inputmethod/keyboard/KeyboardId;->equivalentEditorInfoForKeyboard(Landroid/view/inputmethod/EditorInfo;Landroid/view/inputmethod/EditorInfo;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->resetPendingImsCallback()V

    :goto_0
    return-void

    :cond_0
    iget-boolean v1, p0, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->mPendingSuccessiveImsCallback:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->mPendingSuccessiveImsCallback:Z

    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->resetPendingImsCallback()V

    invoke-virtual {p0, v2}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x320

    invoke-virtual {p0, v1, v2, v3}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_1
    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->getOuterInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/inputmethod/latin/LatinIME;

    invoke-direct {p0, v0, p1, p2}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->executePendingImsCallback(Lcom/android/inputmethod/latin/LatinIME;Landroid/view/inputmethod/EditorInfo;Z)V

    # invokes: Lcom/android/inputmethod/latin/LatinIME;->onStartInputViewInternal(Landroid/view/inputmethod/EditorInfo;Z)V
    invoke-static {v0, p1, p2}, Lcom/android/inputmethod/latin/LatinIME;->access$300(Lcom/android/inputmethod/latin/LatinIME;Landroid/view/inputmethod/EditorInfo;Z)V

    iput-object p1, p0, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->mAppliedEditorInfo:Landroid/view/inputmethod/EditorInfo;

    goto :goto_0
.end method

.method public postUpdateBigramPredictions()V
    .locals 3

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->removeMessages(I)V

    invoke-virtual {p0, v0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iget v1, p0, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->mDelayUpdateSuggestions:I

    int-to-long v1, v1

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

.method public postUpdateShiftState()V
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->removeMessages(I)V

    invoke-virtual {p0, v0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iget v1, p0, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->mDelayUpdateShiftState:I

    int-to-long v1, v1

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

.method public postUpdateSuggestions()V
    .locals 3

    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->removeMessages(I)V

    invoke-virtual {p0, v0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iget v1, p0, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->mDelayUpdateSuggestions:I

    int-to-long v1, v1

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

.method public startDoubleSpacesTimer()V
    .locals 3

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->removeMessages(I)V

    invoke-virtual {p0, v0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->mDoubleSpacesTurnIntoPeriodTimeout:J

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

.method public startOrientationChanging()V
    .locals 2

    const/4 v1, 0x6

    invoke-virtual {p0, v1}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->removeMessages(I)V

    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->resetPendingImsCallback()V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->mIsOrientationChanging:Z

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->getOuterInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/inputmethod/latin/LatinIME;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/LatinIME;->isInputViewShown()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->saveKeyboardState()V

    :cond_0
    return-void
.end method
