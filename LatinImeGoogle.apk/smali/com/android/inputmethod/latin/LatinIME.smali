.class public Lcom/android/inputmethod/latin/LatinIME;
.super Landroid/inputmethodservice/InputMethodService;
.source "LatinIME.java"

# interfaces
.implements Lcom/android/inputmethod/keyboard/KeyboardActionListener;
.implements Lcom/android/inputmethod/latin/TargetApplicationGetter$OnTargetApplicationKnownListener;
.implements Lcom/android/inputmethod/latin/suggestions/SuggestionsView$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/inputmethod/latin/LatinIME$UIHandler;
    }
.end annotation


# static fields
.field private static DEBUG:Z

.field private static final SUGGESTION_VISIBILITY_VALUE_ARRAY:[I

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mApplicationSpecifiedCompletions:[Landroid/view/inputmethod/CompletionInfo;

.field private mCorrectionMode:I

.field private mDeleteCount:I

.field private mDictionaryPackInstallReceiver:Landroid/content/BroadcastReceiver;

.field private mDisplayOrientation:I

.field private mEnteredText:Ljava/lang/CharSequence;

.field private mExpectingUpdateSelection:Z

.field private mExtractArea:Landroid/view/View;

.field private mFeedbackManager:Lcom/android/inputmethod/latin/AudioAndHapticFeedbackManager;

.field public final mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

.field private mImm:Lcom/android/inputmethod/compat/InputMethodManagerCompatWrapper;

.field private mInputAttributes:Lcom/android/inputmethod/latin/InputAttributes;

.field private mIsAutoCorrectionIndicatorOn:Z

.field private mIsMainDictionaryAvailable:Z

.field private mIsUserDictionaryAvailable:Z

.field private mKeyPreviewBackingView:Landroid/view/View;

.field final mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

.field private mLastComposedWord:Lcom/android/inputmethod/latin/LastComposedWord;

.field private mLastKeyTime:J

.field private mLastSelectionEnd:I

.field private mLastSelectionStart:I

.field private mOptionsDialog:Landroid/app/AlertDialog;

.field private mPrefs:Landroid/content/SharedPreferences;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mResources:Landroid/content/res/Resources;

.field private mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

.field private mShouldSwitchToLastSubtype:Z

.field private mSpaceState:I

.field private final mSubtypeSwitcher:Lcom/android/inputmethod/latin/SubtypeSwitcher;

.field mSuggest:Lcom/android/inputmethod/latin/Suggest;

.field private mSuggestionVisibility:I

.field private mSuggestionsContainer:Landroid/view/View;

.field private mSuggestionsView:Lcom/android/inputmethod/latin/suggestions/SuggestionsView;

.field private mTargetApplicationInfo:Landroid/content/pm/ApplicationInfo;

.field private mUserDictionary:Lcom/android/inputmethod/latin/Dictionary;

.field private mUserHistoryDictionary:Lcom/android/inputmethod/latin/UserHistoryDictionary;

.field private mWordComposer:Lcom/android/inputmethod/latin/WordComposer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/inputmethod/latin/LatinIME;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/inputmethod/latin/LatinIME;->TAG:Ljava/lang/String;

    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/inputmethod/latin/LatinIME;->SUGGESTION_VISIBILITY_VALUE_ARRAY:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f0a0009
        0x7f0a000a
        0x7f0a000b
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, -0x1

    invoke-direct {p0}, Landroid/inputmethodservice/InputMethodService;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mShouldSwitchToLastSubtype:Z

    sget-object v0, Lcom/android/inputmethod/latin/LastComposedWord;->NOT_A_COMPOSED_WORD:Lcom/android/inputmethod/latin/LastComposedWord;

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastComposedWord:Lcom/android/inputmethod/latin/LastComposedWord;

    new-instance v0, Lcom/android/inputmethod/latin/WordComposer;

    invoke-direct {v0}, Lcom/android/inputmethod/latin/WordComposer;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    iput v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastSelectionStart:I

    iput v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastSelectionEnd:I

    new-instance v0, Lcom/android/inputmethod/latin/DictionaryPackInstallBroadcastReceiver;

    invoke-direct {v0, p0}, Lcom/android/inputmethod/latin/DictionaryPackInstallBroadcastReceiver;-><init>(Lcom/android/inputmethod/latin/LatinIME;)V

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mDictionaryPackInstallReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-direct {v0, p0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;-><init>(Lcom/android/inputmethod/latin/LatinIME;)V

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    new-instance v0, Lcom/android/inputmethod/latin/LatinIME$2;

    invoke-direct {v0, p0}, Lcom/android/inputmethod/latin/LatinIME$2;-><init>(Lcom/android/inputmethod/latin/LatinIME;)V

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-static {}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->getInstance()Lcom/android/inputmethod/latin/SubtypeSwitcher;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSubtypeSwitcher:Lcom/android/inputmethod/latin/SubtypeSwitcher;

    invoke-static {}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getInstance()Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    return-void
.end method

.method static synthetic access$000(Lcom/android/inputmethod/latin/LatinIME;Z)V
    .locals 0
    .param p0    # Lcom/android/inputmethod/latin/LatinIME;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/android/inputmethod/latin/LatinIME;->onFinishInputViewInternal(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/android/inputmethod/latin/LatinIME;)V
    .locals 0
    .param p0    # Lcom/android/inputmethod/latin/LatinIME;

    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->onFinishInputInternal()V

    return-void
.end method

.method static synthetic access$200(Lcom/android/inputmethod/latin/LatinIME;Landroid/view/inputmethod/EditorInfo;Z)V
    .locals 0
    .param p0    # Lcom/android/inputmethod/latin/LatinIME;
    .param p1    # Landroid/view/inputmethod/EditorInfo;
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/android/inputmethod/latin/LatinIME;->onStartInputInternal(Landroid/view/inputmethod/EditorInfo;Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/android/inputmethod/latin/LatinIME;Landroid/view/inputmethod/EditorInfo;Z)V
    .locals 0
    .param p0    # Lcom/android/inputmethod/latin/LatinIME;
    .param p1    # Landroid/view/inputmethod/EditorInfo;
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/android/inputmethod/latin/LatinIME;->onStartInputViewInternal(Landroid/view/inputmethod/EditorInfo;Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/android/inputmethod/latin/LatinIME;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0    # Lcom/android/inputmethod/latin/LatinIME;

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mPrefs:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/inputmethod/latin/LatinIME;)Lcom/android/inputmethod/latin/SubtypeSwitcher;
    .locals 1
    .param p0    # Lcom/android/inputmethod/latin/LatinIME;

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSubtypeSwitcher:Lcom/android/inputmethod/latin/SubtypeSwitcher;

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/inputmethod/latin/LatinIME;)Lcom/android/inputmethod/latin/AudioAndHapticFeedbackManager;
    .locals 1
    .param p0    # Lcom/android/inputmethod/latin/LatinIME;

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mFeedbackManager:Lcom/android/inputmethod/latin/AudioAndHapticFeedbackManager;

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/inputmethod/latin/LatinIME;)V
    .locals 0
    .param p0    # Lcom/android/inputmethod/latin/LatinIME;

    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->launchSettings()V

    return-void
.end method

.method private addToUserHistoryDictionary(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 8
    .param p1    # Ljava/lang/CharSequence;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    move-object v2, v5

    :goto_0
    return-object v2

    :cond_0
    iget v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mCorrectionMode:I

    if-eq v6, v4, :cond_1

    iget v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mCorrectionMode:I

    const/4 v7, 0x2

    if-eq v6, v7, :cond_1

    move-object v2, v5

    goto :goto_0

    :cond_1
    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mUserHistoryDictionary:Lcom/android/inputmethod/latin/UserHistoryDictionary;

    if-eqz v6, :cond_7

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    iget-object v6, v6, Lcom/android/inputmethod/latin/SettingsValues;->mWordSeparators:Ljava/lang/String;

    invoke-static {v0, v6}, Lcom/android/inputmethod/latin/EditingUtils;->getPreviousWord(Landroid/view/inputmethod/InputConnection;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    :goto_1
    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v6}, Lcom/android/inputmethod/latin/WordComposer;->isAutoCapitalized()Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v6}, Lcom/android/inputmethod/latin/WordComposer;->isMostlyCaps()Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mSubtypeSwitcher:Lcom/android/inputmethod/latin/SubtypeSwitcher;

    invoke-virtual {v7}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->getCurrentSubtypeLocale()Ljava/util/Locale;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    :goto_2
    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    invoke-virtual {v6}, Lcom/android/inputmethod/latin/Suggest;->getUnigramDictionaries()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v6

    invoke-static {v6, p1}, Lcom/android/inputmethod/latin/AutoCorrection;->getMaxFrequency(Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/CharSequence;)I

    move-result v1

    if-nez v1, :cond_4

    move-object v2, v5

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    :cond_4
    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mUserHistoryDictionary:Lcom/android/inputmethod/latin/UserHistoryDictionary;

    if-nez v2, :cond_5

    :goto_3
    if-lez v1, :cond_6

    :goto_4
    invoke-virtual {v6, v5, v3, v4}, Lcom/android/inputmethod/latin/UserHistoryDictionary;->addToUserHistory(Ljava/lang/String;Ljava/lang/String;Z)I

    goto :goto_0

    :cond_5
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_3

    :cond_6
    const/4 v4, 0x0

    goto :goto_4

    :cond_7
    move-object v2, v5

    goto :goto_0
.end method

.method private static canBeFollowedByPeriod(I)Z
    .locals 1
    .param p0    # I

    invoke-static {p0}, Ljava/lang/Character;->isLetterOrDigit(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x27

    if-eq p0, v0, :cond_0

    const/16 v0, 0x22

    if-eq p0, v0, :cond_0

    const/16 v0, 0x29

    if-eq p0, v0, :cond_0

    const/16 v0, 0x5d

    if-eq p0, v0, :cond_0

    const/16 v0, 0x7d

    if-eq p0, v0, :cond_0

    const/16 v0, 0x3e

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private commitChosenWord(Ljava/lang/CharSequence;II)V
    .locals 5
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    iget-boolean v3, v3, Lcom/android/inputmethod/latin/SettingsValues;->mEnableSuggestionSpanInsertion:Z

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionsView:Lcom/android/inputmethod/latin/suggestions/SuggestionsView;

    invoke-virtual {v3}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->getSuggestions()Lcom/android/inputmethod/latin/SuggestedWords;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mIsMainDictionaryAvailable:Z

    invoke-static {p0, p1, v2, v3}, Lcom/android/inputmethod/compat/SuggestionSpanUtils;->getTextWithSuggestionSpan(Landroid/content/Context;Ljava/lang/CharSequence;Lcom/android/inputmethod/latin/SuggestedWords;Z)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v0, v3, v4}, Landroid/view/inputmethod/InputConnection;->commitText(Ljava/lang/CharSequence;I)Z

    :cond_0
    :goto_0
    invoke-direct {p0, p1}, Lcom/android/inputmethod/latin/LatinIME;->addToUserHistoryDictionary(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, p2, v4, p3, v1}, Lcom/android/inputmethod/latin/WordComposer;->commitWord(ILjava/lang/String;ILjava/lang/CharSequence;)Lcom/android/inputmethod/latin/LastComposedWord;

    move-result-object v3

    iput-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastComposedWord:Lcom/android/inputmethod/latin/LastComposedWord;

    return-void

    :cond_1
    invoke-interface {v0, p1, v4}, Landroid/view/inputmethod/InputConnection;->commitText(Ljava/lang/CharSequence;I)Z

    goto :goto_0
.end method

.method private commitCurrentAutoCorrection(ILandroid/view/inputmethod/InputConnection;)V
    .locals 5

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->hasPendingUpdateSuggestions()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->cancelUpdateSuggestions()V

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->updateSuggestions()V

    :cond_0
    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/WordComposer;->getAutoCorrectionOrNull()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/WordComposer;->getTypedWord()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "We have an auto-correction but the typed word is empty? Impossible! I must commit suicide."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, p1}, Lcom/android/inputmethod/latin/Utils$Stats;->onAutoCorrection(Ljava/lang/String;Ljava/lang/String;I)V

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mExpectingUpdateSelection:Z

    const/4 v2, 0x2

    invoke-direct {p0, v0, v2, p1}, Lcom/android/inputmethod/latin/LatinIME;->commitChosenWord(Ljava/lang/CharSequence;II)V

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    if-eqz p2, :cond_2

    new-instance v2, Landroid/view/inputmethod/CorrectionInfo;

    iget v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastSelectionEnd:I

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-direct {v2, v3, v1, v0}, Landroid/view/inputmethod/CorrectionInfo;-><init>(ILjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    invoke-interface {p2, v2}, Landroid/view/inputmethod/InputConnection;->commitCorrection(Landroid/view/inputmethod/CorrectionInfo;)Z

    :cond_2
    return-void
.end method

.method private static getActionId(Lcom/android/inputmethod/keyboard/Keyboard;)I
    .locals 1
    .param p0    # Lcom/android/inputmethod/keyboard/Keyboard;

    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mId:Lcom/android/inputmethod/keyboard/KeyboardId;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/KeyboardId;->imeActionId()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private getAdjustedBackingViewHeight()I
    .locals 10

    iget-object v9, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyPreviewBackingView:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getHeight()I

    move-result v0

    if-lez v0, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v9, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v9}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getKeyboardView()Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    move-result-object v3

    if-nez v3, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {v3}, Lcom/android/inputmethod/keyboard/KeyboardView;->getHeight()I

    move-result v2

    iget-object v9, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionsContainer:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getHeight()I

    move-result v8

    iget-object v9, p0, Lcom/android/inputmethod/latin/LatinIME;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    iget v1, v9, Landroid/util/DisplayMetrics;->heightPixels:I

    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    iget-object v9, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyPreviewBackingView:Landroid/view/View;

    invoke-virtual {v9, v6}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    iget v4, v6, Landroid/graphics/Rect;->top:I

    sub-int v9, v1, v4

    sub-int/2addr v9, v8

    sub-int v7, v9, v2

    iget-object v9, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyPreviewBackingView:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    iget-object v9, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionsView:Lcom/android/inputmethod/latin/suggestions/SuggestionsView;

    invoke-virtual {v9, v7}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->setMoreSuggestionsHeight(I)I

    move-result v9

    iput v9, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v9, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyPreviewBackingView:Landroid/view/View;

    invoke-virtual {v9, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget v0, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_0
.end method

.method private getTextWithUnderline(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    iget-boolean v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mIsAutoCorrectionIndicatorOn:Z

    if-eqz v0, :cond_0

    invoke-static {p0, p1}, Lcom/android/inputmethod/compat/SuggestionSpanUtils;->getTextWithAutoCorrectionIndicatorUnderline(Landroid/content/Context;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method private handleBackspace(I)V
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-interface {v0}, Landroid/view/inputmethod/InputConnection;->beginBatchEdit()Z

    invoke-direct {p0, p1, v0}, Lcom/android/inputmethod/latin/LatinIME;->handleBackspaceWhileInBatchEdit(ILandroid/view/inputmethod/InputConnection;)V

    invoke-interface {v0}, Landroid/view/inputmethod/InputConnection;->endBatchEdit()Z

    goto :goto_0
.end method

.method private handleBackspaceWhileInBatchEdit(ILandroid/view/inputmethod/InputConnection;)V
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->postUpdateShiftState()V

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mEnteredText:Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mEnteredText:Ljava/lang/CharSequence;

    invoke-static {p2, v0}, Lcom/android/inputmethod/latin/LatinIME;->sameAsTextBeforeCursor(Landroid/view/inputmethod/InputConnection;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mEnteredText:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    invoke-interface {p2, v0, v3}, Landroid/view/inputmethod/InputConnection;->deleteSurroundingText(II)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/WordComposer;->isComposingWord()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/WordComposer;->size()I

    move-result v0

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/WordComposer;->deleteLast()V

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/WordComposer;->getTypedWord()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/inputmethod/latin/LatinIME;->getTextWithUnderline(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {p2, v0, v2}, Landroid/view/inputmethod/InputConnection;->setComposingText(Ljava/lang/CharSequence;I)Z

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/WordComposer;->isComposingWord()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->postUpdateBigramPredictions()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->postUpdateSuggestions()V

    goto :goto_0

    :cond_3
    invoke-interface {p2, v2, v3}, Landroid/view/inputmethod/InputConnection;->deleteSurroundingText(II)Z

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastComposedWord:Lcom/android/inputmethod/latin/LastComposedWord;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/LastComposedWord;->canRevertCommit()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {}, Lcom/android/inputmethod/latin/Utils$Stats;->onAutoCorrectionCancellation()V

    invoke-direct {p0, p2}, Lcom/android/inputmethod/latin/LatinIME;->revertCommit(Landroid/view/inputmethod/InputConnection;)V

    goto :goto_0

    :cond_5
    if-ne v2, p1, :cond_8

    invoke-direct {p0, p2}, Lcom/android/inputmethod/latin/LatinIME;->revertDoubleSpaceWhileInBatchEdit(Landroid/view/inputmethod/InputConnection;)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_6
    iget v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastSelectionStart:I

    iget v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastSelectionEnd:I

    if-eq v0, v1, :cond_9

    iget v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastSelectionEnd:I

    iget v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastSelectionStart:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastSelectionEnd:I

    iget v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastSelectionEnd:I

    invoke-interface {p2, v1, v2}, Landroid/view/inputmethod/InputConnection;->setSelection(II)Z

    invoke-interface {p2, v0, v3}, Landroid/view/inputmethod/InputConnection;->deleteSurroundingText(II)Z

    :cond_7
    :goto_1
    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->isSuggestionsRequested()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p2}, Lcom/android/inputmethod/latin/LatinIME;->restartSuggestionsOnWordBeforeCursorIfAtEndOfWord(Landroid/view/inputmethod/InputConnection;)V

    goto :goto_0

    :cond_8
    const/4 v0, 0x2

    if-ne v0, p1, :cond_6

    invoke-static {p2}, Lcom/android/inputmethod/latin/LatinIME;->revertSwapPunctuation(Landroid/view/inputmethod/InputConnection;)Z

    move-result v0

    if-eqz v0, :cond_6

    goto :goto_0

    :cond_9
    const/4 v0, -0x1

    iget v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastSelectionEnd:I

    if-ne v0, v1, :cond_a

    sget-object v0, Lcom/android/inputmethod/latin/LatinIME;->TAG:Ljava/lang/String;

    const-string v1, "Backspace when we don\'t know the selection position"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mTargetApplicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mTargetApplicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_b

    const/16 v0, 0x43

    invoke-static {v0, p2}, Lcom/android/inputmethod/latin/LatinIME;->sendUpDownEnterOrBackspace(ILandroid/view/inputmethod/InputConnection;)V

    :goto_2
    iget v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mDeleteCount:I

    const/16 v1, 0x14

    if-le v0, v1, :cond_7

    invoke-interface {p2, v2, v3}, Landroid/view/inputmethod/InputConnection;->deleteSurroundingText(II)Z

    goto :goto_1

    :cond_b
    invoke-interface {p2, v2, v3}, Landroid/view/inputmethod/InputConnection;->deleteSurroundingText(II)Z

    goto :goto_2
.end method

.method private handleCharacter(IIII)V
    .locals 6
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-interface {v5}, Landroid/view/inputmethod/InputConnection;->beginBatchEdit()Z

    :cond_0
    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/android/inputmethod/latin/LatinIME;->handleCharacterWhileInBatchEdit(IIIILandroid/view/inputmethod/InputConnection;)V

    if-eqz v5, :cond_1

    invoke-interface {v5}, Landroid/view/inputmethod/InputConnection;->endBatchEdit()Z

    :cond_1
    return-void
.end method

.method private handleCharacterWhileInBatchEdit(IIIILandroid/view/inputmethod/InputConnection;)V
    .locals 4

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/WordComposer;->isComposingWord()Z

    move-result v0

    const/4 v3, 0x4

    if-ne v3, p4, :cond_1

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-virtual {v3, p1}, Lcom/android/inputmethod/latin/SettingsValues;->isSymbolExcludedFromWordSeparators(I)Z

    move-result v3

    if-nez v3, :cond_1

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Should not be composing here"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/16 v3, 0x20

    invoke-direct {p0, v3}, Lcom/android/inputmethod/latin/LatinIME;->sendKeyCodePoint(I)V

    :cond_1
    if-nez v0, :cond_3

    invoke-static {p1}, Lcom/android/inputmethod/latin/LatinIME;->isAlphabet(I)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-virtual {v3, p1}, Lcom/android/inputmethod/latin/SettingsValues;->isSymbolExcludedFromWordSeparators(I)Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_2
    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->isSuggestionsRequested()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->isCursorTouchingWord()Z

    move-result v3

    if-nez v3, :cond_3

    const/16 v0, 0x27

    if-eq v0, p1, :cond_8

    move v0, v1

    :goto_0
    invoke-direct {p0, v2}, Lcom/android/inputmethod/latin/LatinIME;->resetComposingState(Z)V

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->clearSuggestions()V

    :cond_3
    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v3}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getKeyboardView()Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->getKeyDetector()Lcom/android/inputmethod/keyboard/KeyDetector;

    move-result-object v3

    invoke-virtual {v0, p1, p2, p3, v3}, Lcom/android/inputmethod/latin/WordComposer;->add(IIILcom/android/inputmethod/keyboard/KeyDetector;)V

    if-eqz p5, :cond_6

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/WordComposer;->size()I

    move-result v0

    if-ne v0, v1, :cond_5

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->getCurrentAutoCapsState()I

    move-result v3

    if-eqz v3, :cond_4

    move v2, v1

    :cond_4
    invoke-virtual {v0, v2}, Lcom/android/inputmethod/latin/WordComposer;->setAutoCapitalized(Z)V

    :cond_5
    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/WordComposer;->getTypedWord()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/inputmethod/latin/LatinIME;->getTextWithUnderline(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {p5, v0, v1}, Landroid/view/inputmethod/InputConnection;->setComposingText(Ljava/lang/CharSequence;I)Z

    :cond_6
    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->postUpdateSuggestions()V

    :cond_7
    :goto_1
    int-to-char v0, p1

    invoke-static {v0, p2, p3}, Lcom/android/inputmethod/latin/Utils$Stats;->onNonSeparator(CII)V

    return-void

    :cond_8
    move v0, v2

    goto :goto_0

    :cond_9
    const/4 v0, -0x2

    if-ne v0, p2, :cond_b

    :goto_2
    invoke-direct {p0, p5, p1, p4, v1}, Lcom/android/inputmethod/latin/LatinIME;->maybeStripSpaceWhileInBatchEdit(Landroid/view/inputmethod/InputConnection;IIZ)Z

    move-result v0

    invoke-direct {p0, p1}, Lcom/android/inputmethod/latin/LatinIME;->sendKeyCodePoint(I)V

    if-eqz v0, :cond_a

    invoke-direct {p0, p5}, Lcom/android/inputmethod/latin/LatinIME;->swapSwapperAndSpaceWhileInBatchEdit(Landroid/view/inputmethod/InputConnection;)V

    const/4 v0, 0x3

    iput v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSpaceState:I

    :cond_a
    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionsView:Lcom/android/inputmethod/latin/suggestions/SuggestionsView;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionsView:Lcom/android/inputmethod/latin/suggestions/SuggestionsView;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->dismissAddToDictionaryHint()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->postUpdateBigramPredictions()V

    goto :goto_1

    :cond_b
    move v1, v2

    goto :goto_2
.end method

.method private handleClose()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {p0, v1, v2}, Lcom/android/inputmethod/latin/LatinIME;->commitTyped(Landroid/view/inputmethod/InputConnection;I)V

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/android/inputmethod/latin/LatinIME;->requestHideSelf(I)V

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getKeyboardView()Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->closing()V

    :cond_0
    return-void
.end method

.method private handleLanguageSwitchKey()V
    .locals 7

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    iget-boolean v0, v6, Lcom/android/inputmethod/latin/SettingsValues;->mIncludesOtherImesInLanguageSwitchList:Z

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->getWindow()Landroid/app/Dialog;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v6

    iget-object v3, v6, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    iget-boolean v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mShouldSwitchToLastSubtype:Z

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mImm:Lcom/android/inputmethod/compat/InputMethodManagerCompatWrapper;

    invoke-virtual {v6}, Lcom/android/inputmethod/compat/InputMethodManagerCompatWrapper;->getLastInputMethodSubtype()Landroid/view/inputmethod/InputMethodSubtype;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/android/inputmethod/latin/ImfUtils;->checkIfSubtypeBelongsToThisImeAndEnabled(Landroid/content/Context;Landroid/view/inputmethod/InputMethodSubtype;)Z

    move-result v2

    if-nez v0, :cond_0

    if-eqz v2, :cond_1

    :cond_0
    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mImm:Lcom/android/inputmethod/compat/InputMethodManagerCompatWrapper;

    invoke-virtual {v6, v3}, Lcom/android/inputmethod/compat/InputMethodManagerCompatWrapper;->switchToLastInputMethod(Landroid/os/IBinder;)Z

    move-result v6

    if-eqz v6, :cond_1

    iput-boolean v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mShouldSwitchToLastSubtype:Z

    :goto_0
    return-void

    :cond_1
    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mImm:Lcom/android/inputmethod/compat/InputMethodManagerCompatWrapper;

    if-nez v0, :cond_2

    move v4, v5

    :cond_2
    invoke-virtual {v6, v3, v4}, Lcom/android/inputmethod/compat/InputMethodManagerCompatWrapper;->switchToNextInputMethod(Landroid/os/IBinder;Z)Z

    iput-boolean v5, p0, Lcom/android/inputmethod/latin/LatinIME;->mShouldSwitchToLastSubtype:Z

    goto :goto_0

    :cond_3
    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mImm:Lcom/android/inputmethod/compat/InputMethodManagerCompatWrapper;

    if-nez v0, :cond_4

    :goto_1
    invoke-virtual {v6, v3, v5}, Lcom/android/inputmethod/compat/InputMethodManagerCompatWrapper;->switchToNextInputMethod(Landroid/os/IBinder;Z)Z

    goto :goto_0

    :cond_4
    move v5, v4

    goto :goto_1
.end method

.method private handleSeparator(IIII)Z
    .locals 9
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/16 v8, 0x20

    const/4 v4, 0x0

    const/4 v7, 0x4

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionsView:Lcom/android/inputmethod/latin/suggestions/SuggestionsView;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionsView:Lcom/android/inputmethod/latin/suggestions/SuggestionsView;

    invoke-virtual {v6}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->dismissAddToDictionaryHint()Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v6}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->cancelUpdateBigramPredictions()V

    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v6}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->postUpdateSuggestions()V

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/view/inputmethod/InputConnection;->beginBatchEdit()Z

    :cond_1
    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v6}, Lcom/android/inputmethod/latin/WordComposer;->isComposingWord()Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    iget-boolean v6, v6, Lcom/android/inputmethod/latin/SettingsValues;->mAutoCorrectEnabled:Z

    if-eqz v6, :cond_8

    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mInputAttributes:Lcom/android/inputmethod/latin/InputAttributes;

    iget-boolean v6, v6, Lcom/android/inputmethod/latin/InputAttributes;->mInputTypeNoAutoCorrect:Z

    if-nez v6, :cond_8

    move v2, v5

    :goto_0
    if-eqz v2, :cond_9

    const/16 v6, 0x27

    if-eq p1, v6, :cond_9

    invoke-direct {p0, p1, v1}, Lcom/android/inputmethod/latin/LatinIME;->commitCurrentAutoCorrection(ILandroid/view/inputmethod/InputConnection;)V

    const/4 v0, 0x1

    :cond_2
    :goto_1
    const/4 v6, -0x2

    if-ne v6, p2, :cond_3

    move v4, v5

    :cond_3
    invoke-direct {p0, v1, p1, p4, v4}, Lcom/android/inputmethod/latin/LatinIME;->maybeStripSpaceWhileInBatchEdit(Landroid/view/inputmethod/InputConnection;IIZ)Z

    move-result v3

    if-ne v7, p4, :cond_4

    iget-object v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-virtual {v4, p1}, Lcom/android/inputmethod/latin/SettingsValues;->isPhantomSpacePromotingSymbol(I)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-direct {p0, v8}, Lcom/android/inputmethod/latin/LatinIME;->sendKeyCodePoint(I)V

    :cond_4
    invoke-direct {p0, p1}, Lcom/android/inputmethod/latin/LatinIME;->sendKeyCodePoint(I)V

    if-ne v8, p1, :cond_b

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->isSuggestionsRequested()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-direct {p0, v1}, Lcom/android/inputmethod/latin/LatinIME;->maybeDoubleSpaceWhileInBatchEdit(Landroid/view/inputmethod/InputConnection;)Z

    move-result v4

    if-eqz v4, :cond_a

    iput v5, p0, Lcom/android/inputmethod/latin/LatinIME;->mSpaceState:I

    :cond_5
    :goto_2
    iget-object v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v4}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->startDoubleSpacesTimer()V

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->isCursorTouchingWord()Z

    move-result v4

    if-nez v4, :cond_6

    iget-object v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v4}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->cancelUpdateSuggestions()V

    iget-object v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v4}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->postUpdateBigramPredictions()V

    :cond_6
    :goto_3
    int-to-char v4, p1

    invoke-static {v4, p2, p3}, Lcom/android/inputmethod/latin/Utils$Stats;->onSeparator(III)V

    if-eqz v1, :cond_7

    invoke-interface {v1}, Landroid/view/inputmethod/InputConnection;->endBatchEdit()Z

    :cond_7
    return v0

    :cond_8
    move v2, v4

    goto :goto_0

    :cond_9
    invoke-virtual {p0, v1, p1}, Lcom/android/inputmethod/latin/LatinIME;->commitTyped(Landroid/view/inputmethod/InputConnection;I)V

    goto :goto_1

    :cond_a
    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->isShowingPunctuationList()Z

    move-result v4

    if-nez v4, :cond_5

    const/4 v4, 0x3

    iput v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mSpaceState:I

    goto :goto_2

    :cond_b
    if-eqz v3, :cond_d

    invoke-direct {p0, v1}, Lcom/android/inputmethod/latin/LatinIME;->swapSwapperAndSpaceWhileInBatchEdit(Landroid/view/inputmethod/InputConnection;)V

    const/4 v4, 0x2

    iput v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mSpaceState:I

    :cond_c
    :goto_4
    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->setPunctuationSuggestions()V

    goto :goto_3

    :cond_d
    if-ne v7, p4, :cond_c

    iput v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mSpaceState:I

    goto :goto_4
.end method

.method private initSuggest()V
    .locals 5

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mSubtypeSwitcher:Lcom/android/inputmethod/latin/SubtypeSwitcher;

    invoke-virtual {v3}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->getCurrentSubtypeLocale()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    invoke-virtual {v3}, Lcom/android/inputmethod/latin/Suggest;->getContactsDictionary()Lcom/android/inputmethod/latin/Dictionary;

    move-result-object v1

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    invoke-virtual {v3}, Lcom/android/inputmethod/latin/Suggest;->close()V

    :goto_0
    new-instance v3, Lcom/android/inputmethod/latin/Suggest;

    invoke-direct {v3, p0, v2}, Lcom/android/inputmethod/latin/Suggest;-><init>(Landroid/content/Context;Ljava/util/Locale;)V

    iput-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    iget-boolean v3, v3, Lcom/android/inputmethod/latin/SettingsValues;->mAutoCorrectEnabled:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    iget-object v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    iget v4, v4, Lcom/android/inputmethod/latin/SettingsValues;->mAutoCorrectionThreshold:F

    invoke-virtual {v3, v4}, Lcom/android/inputmethod/latin/Suggest;->setAutoCorrectionThreshold(F)V

    :cond_0
    invoke-static {p0, v2}, Lcom/android/inputmethod/latin/DictionaryFactory;->isDictionaryAvailable(Landroid/content/Context;Ljava/util/Locale;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mIsMainDictionaryAvailable:Z

    new-instance v3, Lcom/android/inputmethod/latin/UserBinaryDictionary;

    invoke-direct {v3, p0, v0}, Lcom/android/inputmethod/latin/UserBinaryDictionary;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mUserDictionary:Lcom/android/inputmethod/latin/Dictionary;

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mUserDictionary:Lcom/android/inputmethod/latin/Dictionary;

    check-cast v3, Lcom/android/inputmethod/latin/UserBinaryDictionary;

    invoke-virtual {v3}, Lcom/android/inputmethod/latin/UserBinaryDictionary;->isEnabled()Z

    move-result v3

    iput-boolean v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mIsUserDictionaryAvailable:Z

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    iget-object v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mUserDictionary:Lcom/android/inputmethod/latin/Dictionary;

    invoke-virtual {v3, v4}, Lcom/android/inputmethod/latin/Suggest;->setUserDictionary(Lcom/android/inputmethod/latin/Dictionary;)V

    invoke-direct {p0, v1}, Lcom/android/inputmethod/latin/LatinIME;->resetContactsDictionary(Lcom/android/inputmethod/latin/Dictionary;)V

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mPrefs:Landroid/content/SharedPreferences;

    if-nez v3, :cond_1

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    iput-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mPrefs:Landroid/content/SharedPreferences;

    :cond_1
    const/4 v3, 0x3

    iget-object v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mPrefs:Landroid/content/SharedPreferences;

    invoke-static {p0, v0, v3, v4}, Lcom/android/inputmethod/latin/UserHistoryDictionary;->getInstance(Landroid/content/Context;Ljava/lang/String;ILandroid/content/SharedPreferences;)Lcom/android/inputmethod/latin/UserHistoryDictionary;

    move-result-object v3

    iput-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mUserHistoryDictionary:Lcom/android/inputmethod/latin/UserHistoryDictionary;

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    iget-object v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mUserHistoryDictionary:Lcom/android/inputmethod/latin/UserHistoryDictionary;

    invoke-virtual {v3, v4}, Lcom/android/inputmethod/latin/Suggest;->setUserHistoryDictionary(Lcom/android/inputmethod/latin/Dictionary;)V

    return-void

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static isAlphabet(I)Z
    .locals 1
    .param p0    # I

    invoke-static {p0}, Ljava/lang/Character;->isLetter(I)Z

    move-result v0

    return v0
.end method

.method private isShowingOptionDialog()Z
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mOptionsDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mOptionsDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private launchSettings()V
    .locals 1

    const-class v0, Lcom/android/inputmethod/latin/SettingsActivity;

    invoke-direct {p0, v0}, Lcom/android/inputmethod/latin/LatinIME;->launchSettingsClass(Ljava/lang/Class;)V

    return-void
.end method

.method private launchSettingsClass(Ljava/lang/Class;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Landroid/preference/PreferenceActivity;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->handleClose()V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v0, p0, p1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/android/inputmethod/latin/LatinIME;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private maybeDoubleSpaceWhileInBatchEdit(Landroid/view/inputmethod/InputConnection;)Z
    .locals 7

    const/16 v6, 0x20

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mCorrectionMode:I

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_0

    invoke-interface {p1, v5, v0}, Landroid/view/inputmethod/InputConnection;->getTextBeforeCursor(II)Ljava/lang/CharSequence;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-ne v3, v5, :cond_0

    invoke-interface {v2, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    invoke-static {v3}, Lcom/android/inputmethod/latin/LatinIME;->canBeFollowedByPeriod(I)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    if-ne v3, v6, :cond_0

    invoke-interface {v2, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-ne v2, v6, :cond_0

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->isAcceptingDoubleSpaces()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->cancelDoubleSpacesTimer()V

    invoke-interface {p1, v4, v0}, Landroid/view/inputmethod/InputConnection;->deleteSurroundingText(II)Z

    const-string v0, ". "

    invoke-interface {p1, v0, v1}, Landroid/view/inputmethod/InputConnection;->commitText(Ljava/lang/CharSequence;I)Z

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->updateShiftState()V

    move v0, v1

    goto :goto_0
.end method

.method private maybeStripSpaceWhileInBatchEdit(Landroid/view/inputmethod/InputConnection;IIZ)Z
    .locals 3
    .param p1    # Landroid/view/inputmethod/InputConnection;
    .param p2    # I
    .param p3    # I
    .param p4    # Z

    const/4 v2, 0x2

    const/4 v0, 0x0

    const/16 v1, 0xa

    if-ne v1, p2, :cond_1

    if-ne v2, p3, :cond_1

    invoke-static {p1}, Lcom/android/inputmethod/latin/LatinIME;->removeTrailingSpaceWhileInBatchEdit(Landroid/view/inputmethod/InputConnection;)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v1, 0x3

    if-eq v1, p3, :cond_2

    if-ne v2, p3, :cond_0

    :cond_2
    if-eqz p4, :cond_0

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-virtual {v1, p2}, Lcom/android/inputmethod/latin/SettingsValues;->isWeakSpaceSwapper(I)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-virtual {v1, p2}, Lcom/android/inputmethod/latin/SettingsValues;->isWeakSpaceStripper(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p1}, Lcom/android/inputmethod/latin/LatinIME;->removeTrailingSpaceWhileInBatchEdit(Landroid/view/inputmethod/InputConnection;)V

    goto :goto_0
.end method

.method private onFinishInputInternal()V
    .locals 2

    invoke-super {p0}, Landroid/inputmethodservice/InputMethodService;->onFinishInput()V

    invoke-static {}, Lcom/android/inputmethod/latin/LatinImeLogger;->commit()V

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getKeyboardView()Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/KeyboardView;->closing()V

    :cond_0
    return-void
.end method

.method private onFinishInputViewInternal(Z)V
    .locals 2
    .param p1    # Z

    invoke-super {p0, p1}, Landroid/inputmethodservice/InputMethodService;->onFinishInputView(Z)V

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->onFinishInputView()V

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getKeyboardView()Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/KeyboardView;->cancelAllMessages()V

    :cond_0
    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->cancelUpdateSuggestions()V

    return-void
.end method

.method private onSettingsKeyPressed()V
    .locals 1

    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->isShowingOptionDialog()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->showSubtypeSelectorAndSettings()V

    goto :goto_0
.end method

.method private onStartInputInternal(Landroid/view/inputmethod/EditorInfo;Z)V
    .locals 0
    .param p1    # Landroid/view/inputmethod/EditorInfo;
    .param p2    # Z

    invoke-super {p0, p1, p2}, Landroid/inputmethodservice/InputMethodService;->onStartInput(Landroid/view/inputmethod/EditorInfo;Z)V

    return-void
.end method

.method private onStartInputViewInternal(Landroid/view/inputmethod/EditorInfo;Z)V
    .locals 11
    .param p1    # Landroid/view/inputmethod/EditorInfo;
    .param p2    # Z

    const/4 v10, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-super {p0, p1, p2}, Landroid/inputmethodservice/InputMethodService;->onStartInputView(Landroid/view/inputmethod/EditorInfo;Z)V

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v2}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getKeyboardView()Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    move-result-object v1

    if-nez p1, :cond_0

    sget-object v3, Lcom/android/inputmethod/latin/LatinIME;->TAG:Ljava/lang/String;

    const-string v4, "Null EditorInfo in onStartInputView()"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-boolean v3, Lcom/android/inputmethod/latin/LatinImeLogger;->sDBG:Z

    if-eqz v3, :cond_5

    new-instance v3, Ljava/lang/NullPointerException;

    const-string v4, "Null EditorInfo in onStartInputView()"

    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    sget-boolean v3, Lcom/android/inputmethod/latin/LatinIME;->DEBUG:Z

    if-eqz v3, :cond_1

    sget-object v3, Lcom/android/inputmethod/latin/LatinIME;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onStartInputView: editorInfo:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "inputType=0x%08x imeOptions=0x%08x"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    iget v9, p1, Landroid/view/inputmethod/EditorInfo;->inputType:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v5

    iget v9, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v4

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v6, Lcom/android/inputmethod/latin/LatinIME;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "All caps = "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v3, p1, Landroid/view/inputmethod/EditorInfo;->inputType:I

    and-int/lit16 v3, v3, 0x1000

    if-eqz v3, :cond_6

    move v3, v4

    :goto_0
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, ", sentence caps = "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v3, p1, Landroid/view/inputmethod/EditorInfo;->inputType:I

    and-int/lit16 v3, v3, 0x4000

    if-eqz v3, :cond_7

    move v3, v4

    :goto_1
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, ", word caps = "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v3, p1, Landroid/view/inputmethod/EditorInfo;->inputType:I

    and-int/lit16 v3, v3, 0x2000

    if-eqz v3, :cond_8

    move v3, v4

    :goto_2
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v6, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const-string v3, "nm"

    invoke-static {v10, v3, p1}, Lcom/android/inputmethod/latin/InputAttributes;->inPrivateImeOptions(Ljava/lang/String;Ljava/lang/String;Landroid/view/inputmethod/EditorInfo;)Z

    move-result v3

    if-eqz v3, :cond_2

    sget-object v3, Lcom/android/inputmethod/latin/LatinIME;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Deprecated private IME option specified: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p1, Landroid/view/inputmethod/EditorInfo;->privateImeOptions:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v3, Lcom/android/inputmethod/latin/LatinIME;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Use "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "noMicrophoneKey"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " instead"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-string v6, "forceAscii"

    invoke-static {v3, v6, p1}, Lcom/android/inputmethod/latin/InputAttributes;->inPrivateImeOptions(Ljava/lang/String;Ljava/lang/String;Landroid/view/inputmethod/EditorInfo;)Z

    move-result v3

    if-eqz v3, :cond_3

    sget-object v3, Lcom/android/inputmethod/latin/LatinIME;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Deprecated private IME option specified: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p1, Landroid/view/inputmethod/EditorInfo;->privateImeOptions:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v3, Lcom/android/inputmethod/latin/LatinIME;->TAG:Ljava/lang/String;

    const-string v6, "Use EditorInfo.IME_FLAG_FORCE_ASCII flag instead"

    invoke-static {v3, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-object v3, p1, Landroid/view/inputmethod/EditorInfo;->packageName:Ljava/lang/String;

    invoke-static {v3}, Lcom/android/inputmethod/latin/TargetApplicationGetter;->getCachedApplicationInfo(Ljava/lang/String;)Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    iput-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mTargetApplicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mTargetApplicationInfo:Landroid/content/pm/ApplicationInfo;

    if-nez v3, :cond_4

    new-instance v3, Lcom/android/inputmethod/latin/TargetApplicationGetter;

    invoke-direct {v3, p0, p0}, Lcom/android/inputmethod/latin/TargetApplicationGetter;-><init>(Landroid/content/Context;Lcom/android/inputmethod/latin/TargetApplicationGetter$OnTargetApplicationKnownListener;)V

    new-array v6, v4, [Ljava/lang/String;

    iget-object v7, p1, Landroid/view/inputmethod/EditorInfo;->packageName:Ljava/lang/String;

    aput-object v7, v6, v5

    invoke-virtual {v3, v6}, Lcom/android/inputmethod/latin/TargetApplicationGetter;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_4
    invoke-static {p1}, Lcom/android/inputmethod/latin/LatinImeLogger;->onStartInputView(Landroid/view/inputmethod/EditorInfo;)V

    if-nez v1, :cond_9

    :cond_5
    :goto_3
    return-void

    :cond_6
    move v3, v5

    goto/16 :goto_0

    :cond_7
    move v3, v5

    goto/16 :goto_1

    :cond_8
    move v3, v5

    goto/16 :goto_2

    :cond_9
    invoke-static {}, Lcom/android/inputmethod/accessibility/AccessibilityUtils;->getInstance()Lcom/android/inputmethod/accessibility/AccessibilityUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/inputmethod/accessibility/AccessibilityUtils;->isTouchExplorationEnabled()Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-virtual {v0, p1, p2}, Lcom/android/inputmethod/accessibility/AccessibilityUtils;->onStartInputViewInternal(Landroid/view/inputmethod/EditorInfo;Z)V

    :cond_a
    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mSubtypeSwitcher:Lcom/android/inputmethod/latin/SubtypeSwitcher;

    invoke-virtual {v3}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->updateParametersOnStartInputView()V

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->updateFullscreenMode()V

    iget v3, p1, Landroid/view/inputmethod/EditorInfo;->initialSelStart:I

    iput v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastSelectionStart:I

    iget v3, p1, Landroid/view/inputmethod/EditorInfo;->initialSelEnd:I

    iput v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastSelectionEnd:I

    new-instance v3, Lcom/android/inputmethod/latin/InputAttributes;

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->isFullscreenMode()Z

    move-result v6

    invoke-direct {v3, p1, v6}, Lcom/android/inputmethod/latin/InputAttributes;-><init>(Landroid/view/inputmethod/EditorInfo;Z)V

    iput-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mInputAttributes:Lcom/android/inputmethod/latin/InputAttributes;

    iput-object v10, p0, Lcom/android/inputmethod/latin/LatinIME;->mApplicationSpecifiedCompletions:[Landroid/view/inputmethod/CompletionInfo;

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->closing()V

    iput-object v10, p0, Lcom/android/inputmethod/latin/LatinIME;->mEnteredText:Ljava/lang/CharSequence;

    invoke-direct {p0, v4}, Lcom/android/inputmethod/latin/LatinIME;->resetComposingState(Z)V

    iput v5, p0, Lcom/android/inputmethod/latin/LatinIME;->mDeleteCount:I

    iput v5, p0, Lcom/android/inputmethod/latin/LatinIME;->mSpaceState:I

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->loadSettings()V

    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->updateCorrectionMode()V

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mResources:Landroid/content/res/Resources;

    invoke-direct {p0, v3}, Lcom/android/inputmethod/latin/LatinIME;->updateSuggestionVisibility(Landroid/content/res/Resources;)V

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    iget-boolean v3, v3, Lcom/android/inputmethod/latin/SettingsValues;->mAutoCorrectEnabled:Z

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    iget v6, v6, Lcom/android/inputmethod/latin/SettingsValues;->mAutoCorrectionThreshold:F

    invoke-virtual {v3, v6}, Lcom/android/inputmethod/latin/Suggest;->setAutoCorrectionThreshold(F)V

    :cond_b
    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-virtual {v2, p1, v3}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->loadKeyboard(Landroid/view/inputmethod/EditorInfo;Lcom/android/inputmethod/latin/SettingsValues;)V

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionsView:Lcom/android/inputmethod/latin/suggestions/SuggestionsView;

    if-eqz v3, :cond_c

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionsView:Lcom/android/inputmethod/latin/suggestions/SuggestionsView;

    invoke-virtual {v3}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->clear()V

    :cond_c
    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->isSuggestionsStripVisible()Z

    move-result v3

    invoke-direct {p0, v3, v5}, Lcom/android/inputmethod/latin/LatinIME;->setSuggestionStripShownInternal(ZZ)V

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v3}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->postUpdateSuggestions()V

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v3}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->cancelDoubleSpacesTimer()V

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    iget-boolean v3, v3, Lcom/android/inputmethod/latin/SettingsValues;->mKeyPreviewPopupOn:Z

    iget-object v5, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    iget v5, v5, Lcom/android/inputmethod/latin/SettingsValues;->mKeyPreviewPopupDismissDelay:I

    invoke-virtual {v1, v3, v5}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->setKeyPreviewPopupEnabled(ZI)V

    invoke-virtual {v1, v4}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->setProximityCorrectionEnabled(Z)V

    goto/16 :goto_3
.end method

.method private performEditorAction(I)V
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Landroid/view/inputmethod/InputConnection;->performEditorAction(I)Z

    :cond_0
    return-void
.end method

.method private static removeTrailingSpaceWhileInBatchEdit(Landroid/view/inputmethod/InputConnection;)V
    .locals 5
    .param p0    # Landroid/view/inputmethod/InputConnection;

    const/4 v4, 0x1

    const/4 v3, 0x0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {p0, v4, v3}, Landroid/view/inputmethod/InputConnection;->getTextBeforeCursor(II)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-ne v1, v4, :cond_0

    invoke-interface {v0, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    const/16 v2, 0x20

    if-ne v1, v2, :cond_0

    invoke-interface {p0, v4, v3}, Landroid/view/inputmethod/InputConnection;->deleteSurroundingText(II)Z

    goto :goto_0
.end method

.method private resetComposingState(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/WordComposer;->reset()V

    if-eqz p1, :cond_0

    sget-object v0, Lcom/android/inputmethod/latin/LastComposedWord;->NOT_A_COMPOSED_WORD:Lcom/android/inputmethod/latin/LastComposedWord;

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastComposedWord:Lcom/android/inputmethod/latin/LastComposedWord;

    :cond_0
    return-void
.end method

.method private resetContactsDictionary(Lcom/android/inputmethod/latin/Dictionary;)V
    .locals 4
    .param p1    # Lcom/android/inputmethod/latin/Dictionary;

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    iget-boolean v2, v2, Lcom/android/inputmethod/latin/SettingsValues;->mUseContactsDict:Z

    if-eqz v2, :cond_2

    const/4 v1, 0x1

    :goto_0
    if-nez v1, :cond_3

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/android/inputmethod/latin/Dictionary;->close()V

    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    invoke-virtual {v2, v0}, Lcom/android/inputmethod/latin/Suggest;->setContactsDictionary(Lcom/android/inputmethod/latin/Dictionary;)V

    :cond_1
    return-void

    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    :cond_3
    if-eqz p1, :cond_4

    move-object v2, p1

    check-cast v2, Lcom/android/inputmethod/latin/ContactsBinaryDictionary;

    invoke-virtual {v2, p0}, Lcom/android/inputmethod/latin/ContactsBinaryDictionary;->reopen(Landroid/content/Context;)V

    move-object v0, p1

    goto :goto_1

    :cond_4
    new-instance v0, Lcom/android/inputmethod/latin/ContactsBinaryDictionary;

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mSubtypeSwitcher:Lcom/android/inputmethod/latin/SubtypeSwitcher;

    invoke-virtual {v3}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->getCurrentSubtypeLocale()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v0, p0, v2, v3}, Lcom/android/inputmethod/latin/ContactsBinaryDictionary;-><init>(Landroid/content/Context;ILjava/util/Locale;)V

    goto :goto_1
.end method

.method private resetEntireInputState()V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/android/inputmethod/latin/LatinIME;->resetComposingState(Z)V

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->updateSuggestions()V

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/view/inputmethod/InputConnection;->finishComposingText()Z

    :cond_0
    return-void
.end method

.method private restartSuggestionsOnWordBeforeCursor(Landroid/view/inputmethod/InputConnection;Ljava/lang/CharSequence;)V
    .locals 3
    .param p1    # Landroid/view/inputmethod/InputConnection;
    .param p2    # Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v2}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getKeyboard()Lcom/android/inputmethod/keyboard/Keyboard;

    move-result-object v2

    invoke-virtual {v1, p2, v2}, Lcom/android/inputmethod/latin/WordComposer;->setComposingWord(Ljava/lang/CharSequence;Lcom/android/inputmethod/keyboard/Keyboard;)V

    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Landroid/view/inputmethod/InputConnection;->deleteSurroundingText(II)Z

    const/4 v1, 0x1

    invoke-interface {p1, p2, v1}, Landroid/view/inputmethod/InputConnection;->setComposingText(Ljava/lang/CharSequence;I)Z

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->postUpdateSuggestions()V

    return-void
.end method

.method private restartSuggestionsOnWordBeforeCursorIfAtEndOfWord(Landroid/view/inputmethod/InputConnection;)V
    .locals 8
    .param p1    # Landroid/view/inputmethod/InputConnection;

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-interface {p1, v7, v6}, Landroid/view/inputmethod/InputConnection;->getTextBeforeCursor(II)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-interface {v2, v6}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    invoke-virtual {v4, v5}, Lcom/android/inputmethod/latin/SettingsValues;->isWordSeparator(I)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {p1, v7, v6}, Landroid/view/inputmethod/InputConnection;->getTextAfterCursor(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-interface {v1, v6}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    invoke-virtual {v4, v5}, Lcom/android/inputmethod/latin/SettingsValues;->isWordSeparator(I)Z

    move-result v4

    if-eqz v4, :cond_0

    :cond_2
    iget-object v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    iget-object v4, v4, Lcom/android/inputmethod/latin/SettingsValues;->mWordSeparators:Ljava/lang/String;

    invoke-static {p1, v4}, Lcom/android/inputmethod/latin/EditingUtils;->getWordAtCursor(Landroid/view/inputmethod/InputConnection;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :goto_1
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    const/16 v4, 0x27

    invoke-interface {v3, v6}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    if-ne v4, v5, :cond_3

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v4

    invoke-interface {v3, v7, v4}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v3

    goto :goto_1

    :cond_3
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-interface {v3, v6}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-ne v4, v7, :cond_4

    invoke-static {v0}, Ljava/lang/Character;->isLetter(C)Z

    move-result v4

    if-eqz v4, :cond_0

    :cond_4
    invoke-static {v0}, Lcom/android/inputmethod/latin/LatinIME;->isAlphabet(I)Z

    move-result v4

    if-nez v4, :cond_5

    iget-object v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-virtual {v4, v0}, Lcom/android/inputmethod/latin/SettingsValues;->isSymbolExcludedFromWordSeparators(I)Z

    move-result v4

    if-eqz v4, :cond_0

    :cond_5
    invoke-direct {p0, p1, v3}, Lcom/android/inputmethod/latin/LatinIME;->restartSuggestionsOnWordBeforeCursor(Landroid/view/inputmethod/InputConnection;Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private revertCommit(Landroid/view/inputmethod/InputConnection;)V
    .locals 10

    const/4 v9, 0x1

    const/4 v8, -0x1

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastComposedWord:Lcom/android/inputmethod/latin/LastComposedWord;

    iget-object v0, v0, Lcom/android/inputmethod/latin/LastComposedWord;->mPrevWord:Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastComposedWord:Lcom/android/inputmethod/latin/LastComposedWord;

    iget-object v1, v1, Lcom/android/inputmethod/latin/LastComposedWord;->mTypedWord:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastComposedWord:Lcom/android/inputmethod/latin/LastComposedWord;

    iget-object v2, v2, Lcom/android/inputmethod/latin/LastComposedWord;->mCommittedWord:Ljava/lang/String;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v3

    iget-object v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastComposedWord:Lcom/android/inputmethod/latin/LastComposedWord;

    iget v4, v4, Lcom/android/inputmethod/latin/LastComposedWord;->mSeparatorCode:I

    invoke-static {v4}, Lcom/android/inputmethod/latin/LastComposedWord;->getSeparatorLength(I)I

    move-result v4

    add-int v5, v3, v4

    sget-boolean v6, Lcom/android/inputmethod/latin/LatinIME;->DEBUG:Z

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v6}, Lcom/android/inputmethod/latin/WordComposer;->isComposingWord()Z

    move-result v6

    if-eqz v6, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "revertCommit, but we are composing a word"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-interface {p1, v5, v7}, Landroid/view/inputmethod/InputConnection;->getTextBeforeCursor(II)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6, v7, v3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "revertCommit check failed: we thought we were reverting \""

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\", but before the cursor we found \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-interface {p1, v5, v7}, Landroid/view/inputmethod/InputConnection;->deleteSurroundingText(II)Z

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mUserHistoryDictionary:Lcom/android/inputmethod/latin/UserHistoryDictionary;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v0, v2}, Lcom/android/inputmethod/latin/UserHistoryDictionary;->cancelAddingUserHistory(Ljava/lang/String;Ljava/lang/String;)Z

    :cond_2
    if-eqz v4, :cond_3

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastComposedWord:Lcom/android/inputmethod/latin/LastComposedWord;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/LastComposedWord;->didCommitTypedWord()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastComposedWord:Lcom/android/inputmethod/latin/LastComposedWord;

    invoke-virtual {v0, v2}, Lcom/android/inputmethod/latin/WordComposer;->resumeSuggestionOnLastComposedWord(Lcom/android/inputmethod/latin/LastComposedWord;)V

    invoke-interface {p1, v1, v9}, Landroid/view/inputmethod/InputConnection;->setComposingText(Ljava/lang/CharSequence;I)Z

    :goto_0
    sget-object v0, Lcom/android/inputmethod/latin/LastComposedWord;->NOT_A_COMPOSED_WORD:Lcom/android/inputmethod/latin/LastComposedWord;

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastComposedWord:Lcom/android/inputmethod/latin/LastComposedWord;

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->cancelUpdateBigramPredictions()V

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->postUpdateSuggestions()V

    return-void

    :cond_4
    invoke-interface {p1, v1, v9}, Landroid/view/inputmethod/InputConnection;->commitText(Ljava/lang/CharSequence;I)Z

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastComposedWord:Lcom/android/inputmethod/latin/LastComposedWord;

    iget v0, v0, Lcom/android/inputmethod/latin/LastComposedWord;->mSeparatorCode:I

    invoke-direct {p0, v0}, Lcom/android/inputmethod/latin/LatinIME;->sendKeyCodePoint(I)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastComposedWord:Lcom/android/inputmethod/latin/LastComposedWord;

    iget v0, v0, Lcom/android/inputmethod/latin/LastComposedWord;->mSeparatorCode:I

    invoke-static {v0, v8, v8}, Lcom/android/inputmethod/latin/Utils$Stats;->onSeparator(III)V

    goto :goto_0
.end method

.method private revertDoubleSpaceWhileInBatchEdit(Landroid/view/inputmethod/InputConnection;)Z
    .locals 5

    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->cancelDoubleSpacesTimer()V

    invoke-interface {p1, v4, v0}, Landroid/view/inputmethod/InputConnection;->getTextBeforeCursor(II)Ljava/lang/CharSequence;

    move-result-object v2

    const-string v3, ". "

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v1, Lcom/android/inputmethod/latin/LatinIME;->TAG:Ljava/lang/String;

    const-string v2, "Tried to revert double-space combo but we didn\'t find \". \" just before the cursor."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_0
    invoke-interface {p1, v4, v0}, Landroid/view/inputmethod/InputConnection;->deleteSurroundingText(II)Z

    const-string v0, "  "

    invoke-interface {p1, v0, v1}, Landroid/view/inputmethod/InputConnection;->commitText(Ljava/lang/CharSequence;I)Z

    move v0, v1

    goto :goto_0
.end method

.method private static revertSwapPunctuation(Landroid/view/inputmethod/InputConnection;)Z
    .locals 6

    const/4 v5, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-interface {p0, v5, v1}, Landroid/view/inputmethod/InputConnection;->getTextBeforeCursor(II)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    const/16 v3, 0x20

    invoke-interface {v2, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    if-eq v3, v4, :cond_1

    :cond_0
    sget-object v0, Lcom/android/inputmethod/latin/LatinIME;->TAG:Ljava/lang/String;

    const-string v2, "Tried to revert a swap of punctuation but we didn\'t find a space just before the cursor."

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    :goto_0
    return v0

    :cond_1
    invoke-interface {p0}, Landroid/view/inputmethod/InputConnection;->beginBatchEdit()Z

    invoke-interface {p0, v5, v1}, Landroid/view/inputmethod/InputConnection;->deleteSurroundingText(II)Z

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v2, v1, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v1, v0}, Landroid/view/inputmethod/InputConnection;->commitText(Ljava/lang/CharSequence;I)Z

    invoke-interface {p0}, Landroid/view/inputmethod/InputConnection;->endBatchEdit()Z

    goto :goto_0
.end method

.method private static sameAsTextBeforeCursor(Landroid/view/inputmethod/InputConnection;Ljava/lang/CharSequence;)Z
    .locals 3
    .param p0    # Landroid/view/inputmethod/InputConnection;
    .param p1    # Ljava/lang/CharSequence;

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    const/4 v2, 0x0

    invoke-interface {p0, v1, v2}, Landroid/view/inputmethod/InputConnection;->getTextBeforeCursor(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    return v1
.end method

.method private sendKeyCodePoint(I)V
    .locals 6
    .param p1    # I

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/16 v2, 0x30

    if-lt p1, v2, :cond_1

    const/16 v2, 0x39

    if-gt p1, v2, :cond_1

    int-to-char v2, p1

    invoke-super {p0, v2}, Landroid/inputmethodservice/InputMethodService;->sendKeyChar(C)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v2, 0xa

    if-ne v2, p1, :cond_2

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mTargetApplicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mTargetApplicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    const/16 v3, 0x10

    if-ge v2, v3, :cond_2

    const/16 v2, 0x42

    invoke-static {v2, v0}, Lcom/android/inputmethod/latin/LatinIME;->sendUpDownEnterOrBackspace(ILandroid/view/inputmethod/InputConnection;)V

    goto :goto_0

    :cond_2
    new-instance v1, Ljava/lang/String;

    new-array v2, v5, [I

    aput p1, v2, v4

    invoke-direct {v1, v2, v4, v5}, Ljava/lang/String;-><init>([III)V

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/view/inputmethod/InputConnection;->commitText(Ljava/lang/CharSequence;I)Z

    goto :goto_0
.end method

.method private static sendUpDownEnterOrBackspace(ILandroid/view/inputmethod/InputConnection;)V
    .locals 16
    .param p0    # I
    .param p1    # Landroid/view/inputmethod/InputConnection;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    new-instance v1, Landroid/view/KeyEvent;

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, -0x1

    const/4 v11, 0x0

    const/4 v12, 0x6

    move-wide v4, v2

    move/from16 v7, p0

    invoke-direct/range {v1 .. v12}, Landroid/view/KeyEvent;-><init>(JJIIIIIII)V

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/view/inputmethod/InputConnection;->sendKeyEvent(Landroid/view/KeyEvent;)Z

    new-instance v4, Landroid/view/KeyEvent;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v5

    const/4 v9, 0x1

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, -0x1

    const/4 v14, 0x0

    const/4 v15, 0x6

    move-wide v7, v2

    move/from16 v10, p0

    invoke-direct/range {v4 .. v15}, Landroid/view/KeyEvent;-><init>(JJIIIIIII)V

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/view/inputmethod/InputConnection;->sendKeyEvent(Landroid/view/KeyEvent;)Z

    return-void
.end method

.method private setAutoCorrectionIndicator(Z)V
    .locals 3
    .param p1    # Z

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mIsAutoCorrectionIndicatorOn:Z

    if-eq v2, p1, :cond_0

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/WordComposer;->isComposingWord()Z

    move-result v2

    if-eqz v2, :cond_0

    iput-boolean p1, p0, Lcom/android/inputmethod/latin/LatinIME;->mIsAutoCorrectionIndicatorOn:Z

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/WordComposer;->getTypedWord()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/inputmethod/latin/LatinIME;->getTextWithUnderline(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/view/inputmethod/InputConnection;->setComposingText(Ljava/lang/CharSequence;I)Z

    goto :goto_0
.end method

.method private setSuggestionStripShown(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/android/inputmethod/latin/LatinIME;->setSuggestionStripShownInternal(ZZ)V

    return-void
.end method

.method private setSuggestionStripShownInternal(ZZ)V
    .locals 5
    .param p1    # Z
    .param p2    # Z

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->onEvaluateInputViewShown()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionsContainer:Landroid/view/View;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v4}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getKeyboardView()Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->isShown()Z

    move-result v0

    :goto_0
    if-eqz p1, :cond_3

    if-eqz p2, :cond_0

    if-eqz v0, :cond_3

    :cond_0
    const/4 v2, 0x1

    :goto_1
    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->isFullscreenMode()Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionsContainer:Landroid/view/View;

    if-eqz v2, :cond_4

    :goto_2
    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    :goto_3
    return-void

    :cond_2
    move v0, v3

    goto :goto_0

    :cond_3
    move v2, v3

    goto :goto_1

    :cond_4
    const/16 v3, 0x8

    goto :goto_2

    :cond_5
    iget-object v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionsContainer:Landroid/view/View;

    if-eqz v2, :cond_6

    :goto_4
    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    :cond_6
    const/4 v3, 0x4

    goto :goto_4
.end method

.method private setSuggestions(Lcom/android/inputmethod/latin/SuggestedWords;Z)V
    .locals 1
    .param p1    # Lcom/android/inputmethod/latin/SuggestedWords;
    .param p2    # Z

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionsView:Lcom/android/inputmethod/latin/suggestions/SuggestionsView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionsView:Lcom/android/inputmethod/latin/suggestions/SuggestionsView;

    invoke-virtual {v0, p1}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->setSuggestions(Lcom/android/inputmethod/latin/SuggestedWords;)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v0, p2}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->onAutoCorrectionStateChanged(Z)V

    :cond_0
    return-void
.end method

.method private showOptionDialogInternal(Landroid/app/AlertDialog;)V
    .locals 3

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getKeyboardView()Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1, v1}, Landroid/app/AlertDialog;->setCancelable(Z)V

    invoke-virtual {p1, v1}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    invoke-virtual {p1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    iput-object v0, v2, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    const/16 v0, 0x3eb

    iput v0, v2, Landroid/view/WindowManager$LayoutParams;->type:I

    invoke-virtual {v1, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    const/high16 v0, 0x20000

    invoke-virtual {v1, v0}, Landroid/view/Window;->addFlags(I)V

    iput-object p1, p0, Lcom/android/inputmethod/latin/LatinIME;->mOptionsDialog:Landroid/app/AlertDialog;

    invoke-virtual {p1}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method private showSubtypeSelectorAndSettings()V
    .locals 4

    const v0, 0x7f0a002d

    invoke-virtual {p0, v0}, Lcom/android/inputmethod/latin/LatinIME;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/CharSequence;

    const/4 v2, 0x0

    const v3, 0x7f0a0084

    invoke-virtual {p0, v3}, Lcom/android/inputmethod/latin/LatinIME;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const v3, 0x7f0a002c

    invoke-virtual {p0, v3}, Lcom/android/inputmethod/latin/LatinIME;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    new-instance v2, Lcom/android/inputmethod/latin/LatinIME$3;

    invoke-direct {v2, p0, p0}, Lcom/android/inputmethod/latin/LatinIME$3;-><init>(Lcom/android/inputmethod/latin/LatinIME;Landroid/content/Context;)V

    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v1, v2}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/inputmethod/latin/LatinIME;->showOptionDialogInternal(Landroid/app/AlertDialog;)V

    return-void
.end method

.method private specificTldProcessingOnTextInput(Landroid/view/inputmethod/InputConnection;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 5
    .param p1    # Landroid/view/inputmethod/InputConnection;
    .param p2    # Ljava/lang/CharSequence;

    const/16 v4, 0x2e

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-le v1, v2, :cond_0

    invoke-interface {p2, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    if-ne v1, v4, :cond_0

    invoke-interface {p2, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isLetter(C)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object p2

    :cond_1
    iput v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mSpaceState:I

    invoke-interface {p1, v2, v3}, Landroid/view/inputmethod/InputConnection;->getTextBeforeCursor(II)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-ne v1, v2, :cond_0

    invoke-interface {v0, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    if-ne v1, v4, :cond_0

    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-interface {p2, v2, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object p2

    goto :goto_0
.end method

.method private swapSwapperAndSpaceWhileInBatchEdit(Landroid/view/inputmethod/InputConnection;)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x2

    const/4 v3, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {p1, v4, v3}, Landroid/view/inputmethod/InputConnection;->getTextBeforeCursor(II)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-ne v1, v4, :cond_0

    invoke-interface {v0, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    const/16 v2, 0x20

    if-ne v1, v2, :cond_0

    invoke-interface {p1, v4, v3}, Landroid/view/inputmethod/InputConnection;->deleteSurroundingText(II)Z

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v0, v5}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0, v5}, Landroid/view/inputmethod/InputConnection;->commitText(Ljava/lang/CharSequence;I)Z

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->updateShiftState()V

    goto :goto_0
.end method

.method private updateCorrectionMode()V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    iget-boolean v3, v3, Lcom/android/inputmethod/latin/SettingsValues;->mAutoCorrectEnabled:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mInputAttributes:Lcom/android/inputmethod/latin/InputAttributes;

    iget-boolean v3, v3, Lcom/android/inputmethod/latin/InputAttributes;->mInputTypeNoAutoCorrect:Z

    if-nez v3, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    :goto_1
    iput v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mCorrectionMode:I

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    iget-boolean v1, v1, Lcom/android/inputmethod/latin/SettingsValues;->mBigramSuggestionEnabled:Z

    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    const/4 v1, 0x2

    :goto_2
    iput v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mCorrectionMode:I

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1

    :cond_2
    iget v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mCorrectionMode:I

    goto :goto_2
.end method

.method private updateSuggestionVisibility(Landroid/content/res/Resources;)V
    .locals 6
    .param p1    # Landroid/content/res/Resources;

    iget-object v5, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    iget-object v3, v5, Lcom/android/inputmethod/latin/SettingsValues;->mShowSuggestionsSetting:Ljava/lang/String;

    sget-object v0, Lcom/android/inputmethod/latin/LatinIME;->SUGGESTION_VISIBILITY_VALUE_ARRAY:[I

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget v4, v0, v1

    invoke-virtual {p1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    iput v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionVisibility:I

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public addWordToDictionary(Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mUserDictionary:Lcom/android/inputmethod/latin/Dictionary;

    check-cast v0, Lcom/android/inputmethod/latin/UserBinaryDictionary;

    const/16 v1, 0x80

    invoke-virtual {v0, p1, v1}, Lcom/android/inputmethod/latin/UserBinaryDictionary;->addWordToUserDictionary(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->postUpdateSuggestions()V

    const/4 v0, 0x1

    return v0
.end method

.method public clearSuggestions()V
    .locals 2

    const/4 v1, 0x0

    sget-object v0, Lcom/android/inputmethod/latin/SuggestedWords;->EMPTY:Lcom/android/inputmethod/latin/SuggestedWords;

    invoke-direct {p0, v0, v1}, Lcom/android/inputmethod/latin/LatinIME;->setSuggestions(Lcom/android/inputmethod/latin/SuggestedWords;Z)V

    invoke-direct {p0, v1}, Lcom/android/inputmethod/latin/LatinIME;->setAutoCorrectionIndicator(Z)V

    return-void
.end method

.method public commitTyped(Landroid/view/inputmethod/InputConnection;I)V
    .locals 5
    .param p1    # Landroid/view/inputmethod/InputConnection;
    .param p2    # I

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/WordComposer;->isComposingWord()Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/WordComposer;->getTypedWord()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-lez v2, :cond_2

    if-eqz p1, :cond_1

    const/4 v2, 0x1

    invoke-interface {p1, v1, v2}, Landroid/view/inputmethod/InputConnection;->commitText(Ljava/lang/CharSequence;I)Z

    :cond_1
    invoke-direct {p0, v1}, Lcom/android/inputmethod/latin/LatinIME;->addToUserHistoryDictionary(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    const/4 v3, 0x0

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, p2, v0}, Lcom/android/inputmethod/latin/WordComposer;->commitWord(ILjava/lang/String;ILjava/lang/CharSequence;)Lcom/android/inputmethod/latin/LastComposedWord;

    move-result-object v2

    iput-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastComposedWord:Lcom/android/inputmethod/latin/LastComposedWord;

    :cond_2
    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->updateSuggestions()V

    goto :goto_0
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 4

    invoke-super {p0, p1, p2, p3}, Landroid/inputmethodservice/InputMethodService;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    new-instance v1, Landroid/util/PrintWriterPrinter;

    invoke-direct {v1, p2}, Landroid/util/PrintWriterPrinter;-><init>(Ljava/io/PrintWriter;)V

    const-string v0, "LatinIME state :"

    invoke-interface {v1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getKeyboard()Lcom/android/inputmethod/keyboard/Keyboard;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/Keyboard;->mId:Lcom/android/inputmethod/keyboard/KeyboardId;

    iget v0, v0, Lcom/android/inputmethod/keyboard/KeyboardId;->mMode:I

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "  Keyboard mode = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  mIsSuggestionsRequested="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mInputAttributes:Lcom/android/inputmethod/latin/InputAttributes;

    iget-boolean v2, v2, Lcom/android/inputmethod/latin/InputAttributes;->mIsSettingsSuggestionStripOn:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  mCorrectionMode="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mCorrectionMode:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  isComposingWord="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/WordComposer;->isComposingWord()Z

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  mAutoCorrectEnabled="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    iget-boolean v2, v2, Lcom/android/inputmethod/latin/SettingsValues;->mAutoCorrectEnabled:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  mSoundOn="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    iget-boolean v2, v2, Lcom/android/inputmethod/latin/SettingsValues;->mSoundOn:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  mVibrateOn="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    iget-boolean v2, v2, Lcom/android/inputmethod/latin/SettingsValues;->mVibrateOn:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  mKeyPreviewPopupOn="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    iget-boolean v2, v2, Lcom/android/inputmethod/latin/SettingsValues;->mKeyPreviewPopupOn:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  mInputAttributes="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mInputAttributes:Lcom/android/inputmethod/latin/InputAttributes;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/InputAttributes;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, -0x1

    goto/16 :goto_0
.end method

.method public getCurrentAutoCapsState()I
    .locals 6

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    iget-boolean v5, v5, Lcom/android/inputmethod/latin/SettingsValues;->mAutoCap:Z

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->getCurrentInputEditorInfo()Landroid/view/inputmethod/EditorInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v2, v0, Landroid/view/inputmethod/EditorInfo;->inputType:I

    and-int/lit16 v5, v2, 0x1000

    if-eqz v5, :cond_2

    const/16 v4, 0x1000

    goto :goto_0

    :cond_2
    and-int/lit16 v5, v2, 0x6000

    if-nez v5, :cond_3

    const/4 v3, 0x1

    :goto_1
    if-nez v3, :cond_0

    iget-object v5, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v5}, Lcom/android/inputmethod/latin/WordComposer;->isComposingWord()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1, v2}, Landroid/view/inputmethod/InputConnection;->getCursorCapsMode(I)I

    move-result v4

    goto :goto_0

    :cond_3
    move v3, v4

    goto :goto_1
.end method

.method public hapticAndAudioFeedback(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mFeedbackManager:Lcom/android/inputmethod/latin/AudioAndHapticFeedbackManager;

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getKeyboardView()Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/android/inputmethod/latin/AudioAndHapticFeedbackManager;->hapticAndAudioFeedback(ILandroid/view/View;)V

    return-void
.end method

.method public hideWindow()V
    .locals 1

    invoke-static {}, Lcom/android/inputmethod/latin/LatinImeLogger;->commit()V

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->onHideWindow()V

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mOptionsDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mOptionsDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mOptionsDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mOptionsDialog:Landroid/app/AlertDialog;

    :cond_0
    invoke-super {p0}, Landroid/inputmethodservice/InputMethodService;->hideWindow()V

    return-void
.end method

.method public isCursorTouchingWord()Z
    .locals 7

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    invoke-interface {v2, v4, v3}, Landroid/view/inputmethod/InputConnection;->getTextBeforeCursor(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v2, v4, v3}, Landroid/view/inputmethod/InputConnection;->getTextAfterCursor(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-interface {v1, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v6

    invoke-virtual {v5, v6}, Lcom/android/inputmethod/latin/SettingsValues;->isWordSeparator(I)Z

    move-result v5

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-interface {v1, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v6

    invoke-virtual {v5, v6}, Lcom/android/inputmethod/latin/SettingsValues;->isSymbolExcludedFromWordSeparators(I)Z

    move-result v5

    if-nez v5, :cond_2

    move v3, v4

    goto :goto_0

    :cond_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-interface {v0, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v6

    invoke-virtual {v5, v6}, Lcom/android/inputmethod/latin/SettingsValues;->isWordSeparator(I)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-interface {v0, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v6

    invoke-virtual {v5, v6}, Lcom/android/inputmethod/latin/SettingsValues;->isSymbolExcludedFromWordSeparators(I)Z

    move-result v5

    if-nez v5, :cond_0

    move v3, v4

    goto :goto_0
.end method

.method public isShowingPunctuationList()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionsView:Lcom/android/inputmethod/latin/suggestions/SuggestionsView;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    iget-object v1, v1, Lcom/android/inputmethod/latin/SettingsValues;->mSuggestPuncList:Lcom/android/inputmethod/latin/SuggestedWords;

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionsView:Lcom/android/inputmethod/latin/suggestions/SuggestionsView;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->getSuggestions()Lcom/android/inputmethod/latin/SuggestedWords;

    move-result-object v2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isShowingSuggestionsStrip()Z
    .locals 3

    const/4 v0, 0x1

    iget v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionVisibility:I

    const v2, 0x7f0a0009

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionVisibility:I

    const v2, 0x7f0a000a

    if-ne v1, v2, :cond_1

    iget v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mDisplayOrientation:I

    if-ne v1, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSuggestionsRequested()Z
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mInputAttributes:Lcom/android/inputmethod/latin/InputAttributes;

    iget-boolean v0, v0, Lcom/android/inputmethod/latin/InputAttributes;->mIsSettingsSuggestionStripOn:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mCorrectionMode:I

    if-gtz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->isShowingSuggestionsStrip()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSuggestionsStripVisible()Z
    .locals 3

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionsView:Lcom/android/inputmethod/latin/suggestions/SuggestionsView;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionsView:Lcom/android/inputmethod/latin/suggestions/SuggestionsView;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->isShowingAddToDictionaryHint()Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->isShowingSuggestionsStrip()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mInputAttributes:Lcom/android/inputmethod/latin/InputAttributes;

    iget-boolean v0, v0, Lcom/android/inputmethod/latin/InputAttributes;->mApplicationSpecifiedCompletionOn:Z

    if-eqz v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->isSuggestionsRequested()Z

    move-result v0

    goto :goto_0
.end method

.method public isWordSeparator(I)Z
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-virtual {v0, p1}, Lcom/android/inputmethod/latin/SettingsValues;->isWordSeparator(I)Z

    move-result v0

    return v0
.end method

.method public launchDebugSettings()V
    .locals 1

    const-class v0, Lcom/android/inputmethod/latin/DebugSettingsActivity;

    invoke-direct {p0, v0}, Lcom/android/inputmethod/latin/LatinIME;->launchSettingsClass(Ljava/lang/Class;)V

    return-void
.end method

.method loadSettings()V
    .locals 3

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mPrefs:Landroid/content/SharedPreferences;

    if-nez v1, :cond_0

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mPrefs:Landroid/content/SharedPreferences;

    :cond_0
    new-instance v0, Lcom/android/inputmethod/latin/LatinIME$1;

    invoke-direct {v0, p0}, Lcom/android/inputmethod/latin/LatinIME$1;-><init>(Lcom/android/inputmethod/latin/LatinIME;)V

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mResources:Landroid/content/res/Resources;

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mSubtypeSwitcher:Lcom/android/inputmethod/latin/SubtypeSwitcher;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->getCurrentSubtypeLocale()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/android/inputmethod/latin/LocaleUtils$RunInLocale;->runInLocale(Landroid/content/res/Resources;Ljava/util/Locale;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/inputmethod/latin/SettingsValues;

    iput-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    new-instance v1, Lcom/android/inputmethod/latin/AudioAndHapticFeedbackManager;

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-direct {v1, p0, v2}, Lcom/android/inputmethod/latin/AudioAndHapticFeedbackManager;-><init>(Lcom/android/inputmethod/latin/LatinIME;Lcom/android/inputmethod/latin/SettingsValues;)V

    iput-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mFeedbackManager:Lcom/android/inputmethod/latin/AudioAndHapticFeedbackManager;

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    if-nez v1, :cond_1

    const/4 v1, 0x0

    :goto_0
    invoke-direct {p0, v1}, Lcom/android/inputmethod/latin/LatinIME;->resetContactsDictionary(Lcom/android/inputmethod/latin/Dictionary;)V

    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/Suggest;->getContactsDictionary()Lcom/android/inputmethod/latin/Dictionary;

    move-result-object v1

    goto :goto_0
.end method

.method public onCancelInput()V
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->onCancelInput()V

    return-void
.end method

.method public onCodeInput(III)V
    .locals 10
    .param p1    # I
    .param p2    # I
    .param p3    # I

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    const/4 v6, -0x4

    if-ne p1, v6, :cond_0

    iget-wide v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastKeyTime:J

    const-wide/16 v8, 0xc8

    add-long/2addr v6, v8

    cmp-long v6, v4, v6

    if-lez v6, :cond_1

    :cond_0
    const/4 v6, 0x0

    iput v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mDeleteCount:I

    :cond_1
    iput-wide v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastKeyTime:J

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    iget v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mSpaceState:I

    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v6}, Lcom/android/inputmethod/latin/WordComposer;->isComposingWord()Z

    move-result v6

    if-nez v6, :cond_2

    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mIsAutoCorrectionIndicatorOn:Z

    :cond_2
    const/16 v6, 0x20

    if-eq p1, v6, :cond_3

    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v6}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->cancelDoubleSpacesTimer()V

    :cond_3
    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const/16 v6, 0x9

    if-ne p1, v6, :cond_5

    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mInputAttributes:Lcom/android/inputmethod/latin/InputAttributes;

    iget v6, v6, Lcom/android/inputmethod/latin/InputAttributes;->mEditorAction:I

    const/4 v7, 0x5

    if-ne v6, v7, :cond_5

    const/4 v6, 0x5

    invoke-direct {p0, v6}, Lcom/android/inputmethod/latin/LatinIME;->performEditorAction(I)V

    :goto_0
    :pswitch_1
    invoke-virtual {v3, p1}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->onCodeInput(I)V

    if-nez v0, :cond_4

    const/4 v6, -0x1

    if-eq p1, v6, :cond_4

    const/4 v6, -0x2

    if-eq p1, v6, :cond_4

    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastComposedWord:Lcom/android/inputmethod/latin/LastComposedWord;

    invoke-virtual {v6}, Lcom/android/inputmethod/latin/LastComposedWord;->deactivate()V

    :cond_4
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mEnteredText:Ljava/lang/CharSequence;

    return-void

    :pswitch_2
    const/4 v6, 0x0

    iput v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mSpaceState:I

    invoke-direct {p0, v2}, Lcom/android/inputmethod/latin/LatinIME;->handleBackspace(I)V

    iget v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mDeleteCount:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mDeleteCount:I

    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mExpectingUpdateSelection:Z

    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mShouldSwitchToLastSubtype:Z

    invoke-static {p2, p3}, Lcom/android/inputmethod/latin/LatinImeLogger;->logOnDelete(II)V

    goto :goto_0

    :pswitch_3
    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->onSettingsKeyPressed()V

    goto :goto_0

    :pswitch_4
    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mSubtypeSwitcher:Lcom/android/inputmethod/latin/SubtypeSwitcher;

    invoke-virtual {v6}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->switchToShortcutIME()V

    goto :goto_0

    :pswitch_5
    invoke-virtual {v3}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getKeyboard()Lcom/android/inputmethod/keyboard/Keyboard;

    move-result-object v6

    invoke-static {v6}, Lcom/android/inputmethod/latin/LatinIME;->getActionId(Lcom/android/inputmethod/keyboard/Keyboard;)I

    move-result v6

    invoke-direct {p0, v6}, Lcom/android/inputmethod/latin/LatinIME;->performEditorAction(I)V

    goto :goto_0

    :pswitch_6
    const/4 v6, 0x5

    invoke-direct {p0, v6}, Lcom/android/inputmethod/latin/LatinIME;->performEditorAction(I)V

    goto :goto_0

    :pswitch_7
    const/4 v6, 0x7

    invoke-direct {p0, v6}, Lcom/android/inputmethod/latin/LatinIME;->performEditorAction(I)V

    goto :goto_0

    :pswitch_8
    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->handleLanguageSwitchKey()V

    goto :goto_0

    :cond_5
    const/4 v6, 0x0

    iput v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mSpaceState:I

    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-virtual {v6, p1}, Lcom/android/inputmethod/latin/SettingsValues;->isWordSeparator(I)Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-direct {p0, p1, p2, p3, v2}, Lcom/android/inputmethod/latin/LatinIME;->handleSeparator(IIII)Z

    move-result v0

    :goto_1
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mExpectingUpdateSelection:Z

    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mShouldSwitchToLastSubtype:Z

    goto :goto_0

    :cond_6
    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v6}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getKeyboard()Lcom/android/inputmethod/keyboard/Keyboard;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-virtual {v1, p1}, Lcom/android/inputmethod/keyboard/Keyboard;->hasProximityCharsCorrection(I)Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-direct {p0, p1, p2, p3, v2}, Lcom/android/inputmethod/latin/LatinIME;->handleCharacter(IIII)V

    goto :goto_1

    :cond_7
    const/4 v6, -0x1

    const/4 v7, -0x1

    invoke-direct {p0, p1, v6, v7, v2}, Lcom/android/inputmethod/latin/LatinIME;->handleCharacter(IIII)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch -0xa
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onComputeInsets(Landroid/inputmethodservice/InputMethodService$Insets;)V
    .locals 14
    .param p1    # Landroid/inputmethodservice/InputMethodService$Insets;

    const/16 v13, 0x8

    const/4 v11, 0x0

    invoke-super {p0, p1}, Landroid/inputmethodservice/InputMethodService;->onComputeInsets(Landroid/inputmethodservice/InputMethodService$Insets;)V

    iget-object v12, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v12}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getKeyboardView()Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v12, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionsContainer:Landroid/view/View;

    if-nez v12, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->getAdjustedBackingViewHeight()I

    move-result v0

    iget-object v12, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyPreviewBackingView:Landroid/view/View;

    invoke-virtual {v12}, Landroid/view/View;->getVisibility()I

    move-result v12

    if-ne v12, v13, :cond_4

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_5

    move v2, v11

    :goto_2
    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->isFullscreenMode()Z

    move-result v12

    if-eqz v12, :cond_6

    iget-object v12, p0, Lcom/android/inputmethod/latin/LatinIME;->mExtractArea:Landroid/view/View;

    invoke-virtual {v12}, Landroid/view/View;->getHeight()I

    move-result v4

    :goto_3
    iget-object v12, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionsContainer:Landroid/view/View;

    invoke-virtual {v12}, Landroid/view/View;->getVisibility()I

    move-result v12

    if-ne v12, v13, :cond_7

    move v7, v11

    :goto_4
    add-int v12, v4, v2

    add-int v3, v12, v7

    move v10, v3

    iget-object v12, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v12}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getKeyboardView()Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    move-result-object v6

    if-eqz v6, :cond_3

    invoke-virtual {v6}, Lcom/android/inputmethod/keyboard/LatinKeyboardView;->isShown()Z

    move-result v12

    if-eqz v12, :cond_3

    iget-object v12, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionsContainer:Landroid/view/View;

    invoke-virtual {v12}, Landroid/view/View;->getVisibility()I

    move-result v12

    if-nez v12, :cond_2

    sub-int/2addr v10, v7

    :cond_2
    invoke-virtual {v5}, Lcom/android/inputmethod/keyboard/KeyboardView;->getWidth()I

    move-result v9

    invoke-virtual {v5}, Lcom/android/inputmethod/keyboard/KeyboardView;->getHeight()I

    move-result v12

    add-int/2addr v12, v3

    add-int/lit8 v8, v12, 0x64

    const/4 v12, 0x3

    iput v12, p1, Landroid/inputmethodservice/InputMethodService$Insets;->touchableInsets:I

    iget-object v12, p1, Landroid/inputmethodservice/InputMethodService$Insets;->touchableRegion:Landroid/graphics/Region;

    invoke-virtual {v12, v11, v10, v9, v8}, Landroid/graphics/Region;->set(IIII)Z

    :cond_3
    iput v10, p1, Landroid/inputmethodservice/InputMethodService$Insets;->contentTopInsets:I

    iput v10, p1, Landroid/inputmethodservice/InputMethodService$Insets;->visibleTopInsets:I

    goto :goto_0

    :cond_4
    move v1, v11

    goto :goto_1

    :cond_5
    move v2, v0

    goto :goto_2

    :cond_6
    move v4, v11

    goto :goto_3

    :cond_7
    iget-object v12, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionsContainer:Landroid/view/View;

    invoke-virtual {v12}, Landroid/view/View;->getHeight()I

    move-result v7

    goto :goto_4
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1    # Landroid/content/res/Configuration;

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mSubtypeSwitcher:Lcom/android/inputmethod/latin/SubtypeSwitcher;

    invoke-virtual {v1, p1}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mDisplayOrientation:I

    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    if-eq v1, v2, :cond_1

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    iput v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mDisplayOrientation:I

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->startOrientationChanging()V

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/android/inputmethod/latin/LatinIME;->commitTyped(Landroid/view/inputmethod/InputConnection;I)V

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/view/inputmethod/InputConnection;->finishComposingText()Z

    :cond_0
    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->isShowingOptionDialog()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mOptionsDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    :cond_1
    invoke-super {p0, p1}, Landroid/inputmethodservice/InputMethodService;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method public onCreate()V
    .locals 11

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v5

    iput-object v5, p0, Lcom/android/inputmethod/latin/LatinIME;->mPrefs:Landroid/content/SharedPreferences;

    invoke-static {p0, v5}, Lcom/android/inputmethod/latin/LatinImeLogger;->init(Lcom/android/inputmethod/latin/LatinIME;Landroid/content/SharedPreferences;)V

    invoke-static {p0}, Lcom/android/inputmethod/compat/InputMethodManagerCompatWrapper;->init(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->init(Lcom/android/inputmethod/latin/LatinIME;)V

    invoke-static {p0, v5}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->init(Lcom/android/inputmethod/latin/LatinIME;Landroid/content/SharedPreferences;)V

    invoke-static {p0}, Lcom/android/inputmethod/accessibility/AccessibilityUtils;->init(Landroid/inputmethodservice/InputMethodService;)V

    invoke-super {p0}, Landroid/inputmethodservice/InputMethodService;->onCreate()V

    invoke-static {}, Lcom/android/inputmethod/compat/InputMethodManagerCompatWrapper;->getInstance()Lcom/android/inputmethod/compat/InputMethodManagerCompatWrapper;

    move-result-object v8

    iput-object v8, p0, Lcom/android/inputmethod/latin/LatinIME;->mImm:Lcom/android/inputmethod/compat/InputMethodManagerCompatWrapper;

    iget-object v8, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v8}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->onCreate()V

    sget-boolean v8, Lcom/android/inputmethod/latin/LatinImeLogger;->sDBG:Z

    sput-boolean v8, Lcom/android/inputmethod/latin/LatinIME;->DEBUG:Z

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    iput-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mResources:Landroid/content/res/Resources;

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->loadSettings()V

    iget-object v8, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-virtual {v8}, Lcom/android/inputmethod/latin/SettingsValues;->getAdditionalSubtypes()[Landroid/view/inputmethod/InputMethodSubtype;

    move-result-object v8

    invoke-static {p0, v8}, Lcom/android/inputmethod/latin/ImfUtils;->setAdditionalInputMethodSubtypes(Landroid/content/Context;[Landroid/view/inputmethod/InputMethodSubtype;)V

    new-instance v8, Lcom/android/inputmethod/latin/InputAttributes;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-direct {v8, v9, v10}, Lcom/android/inputmethod/latin/InputAttributes;-><init>(Landroid/view/inputmethod/EditorInfo;Z)V

    iput-object v8, p0, Lcom/android/inputmethod/latin/LatinIME;->mInputAttributes:Lcom/android/inputmethod/latin/InputAttributes;

    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->updateCorrectionMode()V

    invoke-static {}, Lcom/android/inputmethod/latin/Utils$GCUtils;->getInstance()Lcom/android/inputmethod/latin/Utils$GCUtils;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/inputmethod/latin/Utils$GCUtils;->reset()V

    const/4 v7, 0x1

    const/4 v2, 0x0

    :goto_0
    const/4 v8, 0x5

    if-ge v2, v8, :cond_0

    if-eqz v7, :cond_0

    :try_start_0
    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->initSuggest()V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v7, 0x0

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/android/inputmethod/latin/Utils$GCUtils;->getInstance()Lcom/android/inputmethod/latin/Utils$GCUtils;

    move-result-object v8

    const-string v9, "InitSuggest"

    invoke-virtual {v8, v9, v0}, Lcom/android/inputmethod/latin/Utils$GCUtils;->tryGCOrWait(Ljava/lang/String;Ljava/lang/Throwable;)Z

    move-result v7

    goto :goto_1

    :cond_0
    invoke-virtual {v6}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v8

    iget v8, v8, Landroid/content/res/Configuration;->orientation:I

    iput v8, p0, Lcom/android/inputmethod/latin/LatinIME;->mDisplayOrientation:I

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v8, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v1, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v8, "android.media.RINGER_MODE_CHANGED"

    invoke-virtual {v1, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/android/inputmethod/latin/LatinIME;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v8, v1}, Lcom/android/inputmethod/latin/LatinIME;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v4, Landroid/content/IntentFilter;

    invoke-direct {v4}, Landroid/content/IntentFilter;-><init>()V

    const-string v8, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v4, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v8, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v4, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v8, "package"

    invoke-virtual {v4, v8}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/android/inputmethod/latin/LatinIME;->mDictionaryPackInstallReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v8, v4}, Lcom/android/inputmethod/latin/LatinIME;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    const-string v8, "com.android.inputmethod.latin.dictionarypack.newdict"

    invoke-virtual {v3, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/android/inputmethod/latin/LatinIME;->mDictionaryPackInstallReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v8, v3}, Lcom/android/inputmethod/latin/LatinIME;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public onCreateInputView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->onCreateInputView()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onCurrentInputMethodSubtypeChanged(Landroid/view/inputmethod/InputMethodSubtype;)V
    .locals 1
    .param p1    # Landroid/view/inputmethod/InputMethodSubtype;

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSubtypeSwitcher:Lcom/android/inputmethod/latin/SubtypeSwitcher;

    invoke-virtual {v0, p1}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->updateSubtype(Landroid/view/inputmethod/InputMethodSubtype;)V

    return-void
.end method

.method public onCustomRequest(I)Z
    .locals 3
    .param p1    # I

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->isShowingOptionDialog()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {p0, v1}, Lcom/android/inputmethod/latin/ImfUtils;->hasMultipleEnabledIMEsOrSubtypes(Landroid/content/Context;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mImm:Lcom/android/inputmethod/compat/InputMethodManagerCompatWrapper;

    invoke-virtual {v0}, Lcom/android/inputmethod/compat/InputMethodManagerCompatWrapper;->showInputMethodPicker()V

    move v0, v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/Suggest;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    :cond_0
    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/android/inputmethod/latin/LatinIME;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mDictionaryPackInstallReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/android/inputmethod/latin/LatinIME;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-static {}, Lcom/android/inputmethod/latin/LatinImeLogger;->commit()V

    invoke-static {}, Lcom/android/inputmethod/latin/LatinImeLogger;->onDestroy()V

    invoke-super {p0}, Landroid/inputmethodservice/InputMethodService;->onDestroy()V

    return-void
.end method

.method public onDisplayCompletions([Landroid/view/inputmethod/CompletionInfo;)V
    .locals 10
    .param p1    # [Landroid/view/inputmethod/CompletionInfo;

    const/4 v2, 0x0

    sget-boolean v3, Lcom/android/inputmethod/latin/LatinIME;->DEBUG:Z

    if-eqz v3, :cond_0

    sget-object v3, Lcom/android/inputmethod/latin/LatinIME;->TAG:Ljava/lang/String;

    const-string v4, "Received completions:"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_0

    const/4 v8, 0x0

    :goto_0
    array-length v3, p1

    if-ge v8, v3, :cond_0

    sget-object v3, Lcom/android/inputmethod/latin/LatinIME;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "  #"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, p1, v8

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mInputAttributes:Lcom/android/inputmethod/latin/InputAttributes;

    iget-boolean v3, v3, Lcom/android/inputmethod/latin/InputAttributes;->mApplicationSpecifiedCompletionOn:Z

    if-eqz v3, :cond_1

    iput-object p1, p0, Lcom/android/inputmethod/latin/LatinIME;->mApplicationSpecifiedCompletions:[Landroid/view/inputmethod/CompletionInfo;

    if-nez p1, :cond_2

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->clearSuggestions()V

    :cond_1
    :goto_1
    return-void

    :cond_2
    invoke-static {p1}, Lcom/android/inputmethod/latin/SuggestedWords;->getFromApplicationSpecifiedCompletions([Landroid/view/inputmethod/CompletionInfo;)Ljava/util/ArrayList;

    move-result-object v1

    new-instance v0, Lcom/android/inputmethod/latin/SuggestedWords;

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    invoke-direct/range {v0 .. v7}, Lcom/android/inputmethod/latin/SuggestedWords;-><init>(Ljava/util/ArrayList;ZZZZZZ)V

    const/4 v9, 0x0

    invoke-direct {p0, v0, v2}, Lcom/android/inputmethod/latin/LatinIME;->setSuggestions(Lcom/android/inputmethod/latin/SuggestedWords;Z)V

    invoke-direct {p0, v2}, Lcom/android/inputmethod/latin/LatinIME;->setAutoCorrectionIndicator(Z)V

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v3}, Lcom/android/inputmethod/latin/WordComposer;->getTypedWord()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/inputmethod/latin/WordComposer;->setAutoCorrection(Ljava/lang/CharSequence;)V

    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/android/inputmethod/latin/LatinIME;->setSuggestionStripShown(Z)V

    goto :goto_1
.end method

.method public onEvaluateFullscreenMode()Z
    .locals 3

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/inputmethod/latin/SettingsValues;->isFullscreenModeAllowed(Landroid/content/res/Resources;)Z

    move-result v0

    invoke-super {p0}, Landroid/inputmethodservice/InputMethodService;->onEvaluateFullscreenMode()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onExtractedCursorMovement(II)V
    .locals 1
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->isSuggestionsRequested()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/inputmethodservice/InputMethodService;->onExtractedCursorMovement(II)V

    goto :goto_0
.end method

.method public onExtractedTextClicked()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->isSuggestionsRequested()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Landroid/inputmethodservice/InputMethodService;->onExtractedTextClicked()V

    goto :goto_0
.end method

.method public onFinishInput()V
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->onFinishInput()V

    return-void
.end method

.method public onFinishInputView(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v0, p1}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->onFinishInputView(Z)V

    return-void
.end method

.method public onPressKey(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v0, p1}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->onPressKey(I)V

    return-void
.end method

.method public onRefreshKeyboard()V
    .locals 3

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getKeyboardView()Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->getCurrentInputEditorInfo()Landroid/view/inputmethod/EditorInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-virtual {v0, v1, v2}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->loadKeyboard(Landroid/view/inputmethod/EditorInfo;Lcom/android/inputmethod/latin/SettingsValues;)V

    :cond_0
    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->initSuggest()V

    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->updateCorrectionMode()V

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->loadSettings()V

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->isCursorTouchingWord()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->postUpdateSuggestions()V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->postUpdateBigramPredictions()V

    goto :goto_0
.end method

.method public onReleaseKey(IZ)V
    .locals 5
    .param p1    # I
    .param p2    # Z

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v2, p1, p2}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->onReleaseKey(IZ)V

    invoke-static {}, Lcom/android/inputmethod/accessibility/AccessibilityUtils;->getInstance()Lcom/android/inputmethod/accessibility/AccessibilityUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/inputmethod/accessibility/AccessibilityUtils;->isTouchExplorationEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    const/4 v2, -0x4

    if-ne v2, p1, :cond_1

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0, v4, v3}, Landroid/view/inputmethod/InputConnection;->getTextBeforeCursor(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-interface {v1, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->isHighSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0, v4, v3}, Landroid/view/inputmethod/InputConnection;->deleteSurroundingText(II)Z

    :cond_1
    return-void

    :pswitch_0
    invoke-static {}, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->getInstance()Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->notifyShiftState()V

    goto :goto_0

    :pswitch_1
    invoke-static {}, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->getInstance()Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->notifySymbolsState()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onStartInput(Landroid/view/inputmethod/EditorInfo;Z)V
    .locals 1
    .param p1    # Landroid/view/inputmethod/EditorInfo;
    .param p2    # Z

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v0, p1, p2}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->onStartInput(Landroid/view/inputmethod/EditorInfo;Z)V

    return-void
.end method

.method public onStartInputView(Landroid/view/inputmethod/EditorInfo;Z)V
    .locals 1
    .param p1    # Landroid/view/inputmethod/EditorInfo;
    .param p2    # Z

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v0, p1, p2}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->onStartInputView(Landroid/view/inputmethod/EditorInfo;Z)V

    return-void
.end method

.method public onTargetApplicationKnown(Landroid/content/pm/ApplicationInfo;)V
    .locals 0
    .param p1    # Landroid/content/pm/ApplicationInfo;

    iput-object p1, p0, Lcom/android/inputmethod/latin/LatinIME;->mTargetApplicationInfo:Landroid/content/pm/ApplicationInfo;

    return-void
.end method

.method public onTextInput(Ljava/lang/CharSequence;)V
    .locals 4
    .param p1    # Ljava/lang/CharSequence;

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-interface {v0}, Landroid/view/inputmethod/InputConnection;->beginBatchEdit()Z

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/android/inputmethod/latin/LatinIME;->commitTyped(Landroid/view/inputmethod/InputConnection;I)V

    invoke-direct {p0, v0, p1}, Lcom/android/inputmethod/latin/LatinIME;->specificTldProcessingOnTextInput(Landroid/view/inputmethod/InputConnection;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    const/4 v1, 0x4

    iget v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mSpaceState:I

    if-ne v1, v2, :cond_1

    const/16 v1, 0x20

    invoke-direct {p0, v1}, Lcom/android/inputmethod/latin/LatinIME;->sendKeyCodePoint(I)V

    :cond_1
    invoke-interface {v0, p1, v3}, Landroid/view/inputmethod/InputConnection;->commitText(Ljava/lang/CharSequence;I)Z

    invoke-interface {v0}, Landroid/view/inputmethod/InputConnection;->endBatchEdit()Z

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->updateShiftState()V

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    const/4 v2, -0x3

    invoke-virtual {v1, v2}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->onCodeInput(I)V

    const/4 v1, 0x0

    iput v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mSpaceState:I

    iput-object p1, p0, Lcom/android/inputmethod/latin/LatinIME;->mEnteredText:Ljava/lang/CharSequence;

    invoke-direct {p0, v3}, Lcom/android/inputmethod/latin/LatinIME;->resetComposingState(Z)V

    goto :goto_0
.end method

.method public onUpdateSelection(IIIIII)V
    .locals 7
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I

    const/4 v0, 0x1

    const/4 v6, -0x1

    const/4 v2, 0x0

    invoke-super/range {p0 .. p6}, Landroid/inputmethodservice/InputMethodService;->onUpdateSelection(IIIIII)V

    sget-boolean v3, Lcom/android/inputmethod/latin/LatinIME;->DEBUG:Z

    if-eqz v3, :cond_0

    sget-object v3, Lcom/android/inputmethod/latin/LatinIME;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onUpdateSelection: oss="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", ose="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", lss="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastSelectionStart:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", lse="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastSelectionEnd:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", nss="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", nse="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", cs="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", ce="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-ne p3, p6, :cond_1

    if-eq p4, p6, :cond_5

    :cond_1
    iget v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastSelectionStart:I

    if-eq v3, p3, :cond_5

    move v1, v0

    :goto_0
    if-ne p5, v6, :cond_6

    if-ne p6, v6, :cond_6

    :goto_1
    iget-boolean v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mExpectingUpdateSelection:Z

    if-nez v3, :cond_4

    iput v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mSpaceState:I

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v3}, Lcom/android/inputmethod/latin/WordComposer;->isComposingWord()Z

    move-result v3

    if-eqz v3, :cond_2

    if-nez v1, :cond_2

    if-eqz v0, :cond_3

    :cond_2
    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->resetEntireInputState()V

    :cond_3
    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v3}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->postUpdateShiftState()V

    :cond_4
    iput-boolean v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mExpectingUpdateSelection:Z

    iput p3, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastSelectionStart:I

    iput p4, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastSelectionEnd:I

    return-void

    :cond_5
    move v1, v2

    goto :goto_0

    :cond_6
    move v0, v2

    goto :goto_1
.end method

.method public onWindowHidden()V
    .locals 2

    invoke-super {p0}, Landroid/inputmethodservice/InputMethodService;->onWindowHidden()V

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getKeyboardView()Lcom/android/inputmethod/keyboard/LatinKeyboardView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/KeyboardView;->closing()V

    :cond_0
    return-void
.end method

.method public pickSuggestionManually(ILjava/lang/CharSequence;II)V
    .locals 6
    .param p1    # I
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # I
    .param p4    # I

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-interface {v5}, Landroid/view/inputmethod/InputConnection;->beginBatchEdit()Z

    :cond_0
    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/android/inputmethod/latin/LatinIME;->pickSuggestionManuallyWhileInBatchEdit(ILjava/lang/CharSequence;IILandroid/view/inputmethod/InputConnection;)V

    if-eqz v5, :cond_1

    invoke-interface {v5}, Landroid/view/inputmethod/InputConnection;->endBatchEdit()Z

    :cond_1
    return-void
.end method

.method public pickSuggestionManuallyWhileInBatchEdit(ILjava/lang/CharSequence;IILandroid/view/inputmethod/InputConnection;)V
    .locals 7

    const/4 v6, 0x4

    const/4 v4, -0x2

    const/4 v1, 0x0

    const/4 v5, -0x1

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionsView:Lcom/android/inputmethod/latin/suggestions/SuggestionsView;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->getSuggestions()Lcom/android/inputmethod/latin/SuggestedWords;

    move-result-object v2

    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-ne v3, v0, :cond_1

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->isShowingPunctuationList()Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v0, ""

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3, p1, v2}, Lcom/android/inputmethod/latin/LatinImeLogger;->logOnManualSuggestion(Ljava/lang/String;Ljava/lang/String;ILcom/android/inputmethod/latin/SuggestedWords;)V

    invoke-interface {p2, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    invoke-virtual {p0, v0, v4, v4}, Lcom/android/inputmethod/latin/LatinIME;->onCodeInput(III)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mSpaceState:I

    if-ne v6, v3, :cond_2

    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-lez v3, :cond_2

    invoke-static {p2, v1}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v3

    iget-object v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-virtual {v4, v3}, Lcom/android/inputmethod/latin/SettingsValues;->isWeakSpaceStripper(I)Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-virtual {v4, v3}, Lcom/android/inputmethod/latin/SettingsValues;->isWeakSpaceSwapper(I)Z

    move-result v3

    if-nez v3, :cond_2

    const/16 v3, 0x20

    invoke-direct {p0, v3}, Lcom/android/inputmethod/latin/LatinIME;->sendKeyCodePoint(I)V

    :cond_2
    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mInputAttributes:Lcom/android/inputmethod/latin/InputAttributes;

    iget-boolean v3, v3, Lcom/android/inputmethod/latin/InputAttributes;->mApplicationSpecifiedCompletionOn:Z

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mApplicationSpecifiedCompletions:[Landroid/view/inputmethod/CompletionInfo;

    if-eqz v3, :cond_4

    if-ltz p1, :cond_4

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mApplicationSpecifiedCompletions:[Landroid/view/inputmethod/CompletionInfo;

    array-length v3, v3

    if-ge p1, v3, :cond_4

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionsView:Lcom/android/inputmethod/latin/suggestions/SuggestionsView;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionsView:Lcom/android/inputmethod/latin/suggestions/SuggestionsView;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->clear()V

    :cond_3
    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->updateShiftState()V

    invoke-direct {p0, v0}, Lcom/android/inputmethod/latin/LatinIME;->resetComposingState(Z)V

    if-eqz p5, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mApplicationSpecifiedCompletions:[Landroid/view/inputmethod/CompletionInfo;

    aget-object v0, v0, p1

    invoke-interface {p5, v0}, Landroid/view/inputmethod/InputConnection;->commitCompletion(Landroid/view/inputmethod/CompletionInfo;)Z

    goto :goto_0

    :cond_4
    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v3}, Lcom/android/inputmethod/latin/WordComposer;->getTypedWord()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, p1, v2}, Lcom/android/inputmethod/latin/LatinImeLogger;->logOnManualSuggestion(Ljava/lang/String;Ljava/lang/String;ILcom/android/inputmethod/latin/SuggestedWords;)V

    iput-boolean v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mExpectingUpdateSelection:Z

    invoke-direct {p0, p2, v0, v5}, Lcom/android/inputmethod/latin/LatinIME;->commitChosenWord(Ljava/lang/CharSequence;II)V

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastComposedWord:Lcom/android/inputmethod/latin/LastComposedWord;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/LastComposedWord;->deactivate()V

    iput v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mSpaceState:I

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v2}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->updateShiftState()V

    if-nez p1, :cond_6

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/Suggest;->hasMainDictionary()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/Suggest;->getUnigramDictionaries()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v2

    invoke-static {v2, p2, v0}, Lcom/android/inputmethod/latin/AutoCorrection;->isValidWord(Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/CharSequence;Z)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_5
    :goto_1
    const/16 v1, 0x20

    invoke-static {v1, v5, v5}, Lcom/android/inputmethod/latin/Utils$Stats;->onSeparator(III)V

    if-nez v0, :cond_7

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->updateBigramPredictions()V

    goto/16 :goto_0

    :cond_6
    move v0, v1

    goto :goto_1

    :cond_7
    iget-boolean v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mIsUserDictionaryAvailable:Z

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionsView:Lcom/android/inputmethod/latin/suggestions/SuggestionsView;

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    iget-object v1, v1, Lcom/android/inputmethod/latin/SettingsValues;->mHintToSaveText:Ljava/lang/CharSequence;

    invoke-virtual {v0, p2, v1}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->showAddToDictionaryHint(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_8
    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->postUpdateSuggestions()V

    goto/16 :goto_0
.end method

.method resetSuggestMainDict()V
    .locals 2

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mSubtypeSwitcher:Lcom/android/inputmethod/latin/SubtypeSwitcher;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->getCurrentSubtypeLocale()Ljava/util/Locale;

    move-result-object v0

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    invoke-virtual {v1, p0, v0}, Lcom/android/inputmethod/latin/Suggest;->resetMainDict(Landroid/content/Context;Ljava/util/Locale;)V

    invoke-static {p0, v0}, Lcom/android/inputmethod/latin/DictionaryFactory;->isDictionaryAvailable(Landroid/content/Context;Ljava/util/Locale;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mIsMainDictionaryAvailable:Z

    return-void
.end method

.method public setCandidatesView(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    return-void
.end method

.method public setInputView(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-super {p0, p1}, Landroid/inputmethodservice/InputMethodService;->setInputView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->getWindow()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const v1, 0x102001c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mExtractArea:Landroid/view/View;

    const v0, 0x7f07003e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyPreviewBackingView:Landroid/view/View;

    const v0, 0x7f07003f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionsContainer:Landroid/view/View;

    const v0, 0x7f070040

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionsView:Lcom/android/inputmethod/latin/suggestions/SuggestionsView;

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionsView:Lcom/android/inputmethod/latin/suggestions/SuggestionsView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionsView:Lcom/android/inputmethod/latin/suggestions/SuggestionsView;

    invoke-virtual {v0, p0, p1}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->setListener(Lcom/android/inputmethod/latin/suggestions/SuggestionsView$Listener;Landroid/view/View;)V

    :cond_0
    sget-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sVISUALDEBUG:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyPreviewBackingView:Landroid/view/View;

    const/high16 v1, 0x10ff0000

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    :cond_1
    return-void
.end method

.method public setPunctuationSuggestions()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    iget-boolean v0, v0, Lcom/android/inputmethod/latin/SettingsValues;->mBigramPredictionEnabled:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->clearSuggestions()V

    :goto_0
    invoke-direct {p0, v1}, Lcom/android/inputmethod/latin/LatinIME;->setAutoCorrectionIndicator(Z)V

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->isSuggestionsStripVisible()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/inputmethod/latin/LatinIME;->setSuggestionStripShown(Z)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    iget-object v0, v0, Lcom/android/inputmethod/latin/SettingsValues;->mSuggestPuncList:Lcom/android/inputmethod/latin/SuggestedWords;

    invoke-direct {p0, v0, v1}, Lcom/android/inputmethod/latin/LatinIME;->setSuggestions(Lcom/android/inputmethod/latin/SuggestedWords;Z)V

    goto :goto_0
.end method

.method public showSuggestions(Lcom/android/inputmethod/latin/SuggestedWords;Ljava/lang/CharSequence;)V
    .locals 3
    .param p1    # Lcom/android/inputmethod/latin/SuggestedWords;
    .param p2    # Ljava/lang/CharSequence;

    invoke-virtual {p1}, Lcom/android/inputmethod/latin/SuggestedWords;->size()I

    move-result v2

    if-lez v2, :cond_1

    invoke-virtual {p1}, Lcom/android/inputmethod/latin/SuggestedWords;->hasAutoCorrectionWord()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Lcom/android/inputmethod/latin/SuggestedWords;->getWord(I)Ljava/lang/CharSequence;

    move-result-object v0

    :goto_0
    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v2, v0}, Lcom/android/inputmethod/latin/WordComposer;->setAutoCorrection(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Lcom/android/inputmethod/latin/SuggestedWords;->willAutoCorrect()Z

    move-result v1

    invoke-direct {p0, p1, v1}, Lcom/android/inputmethod/latin/LatinIME;->setSuggestions(Lcom/android/inputmethod/latin/SuggestedWords;Z)V

    invoke-direct {p0, v1}, Lcom/android/inputmethod/latin/LatinIME;->setAutoCorrectionIndicator(Z)V

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->isSuggestionsStripVisible()Z

    move-result v2

    invoke-direct {p0, v2}, Lcom/android/inputmethod/latin/LatinIME;->setSuggestionStripShown(Z)V

    return-void

    :cond_0
    move-object v0, p2

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public updateBigramPredictions()V
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->isSuggestionsRequested()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    iget-boolean v1, v1, Lcom/android/inputmethod/latin/SettingsValues;->mBigramPredictionEnabled:Z

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->setPunctuationSuggestions()V

    goto :goto_0

    :cond_2
    iget v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mCorrectionMode:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v1

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    iget-object v2, v2, Lcom/android/inputmethod/latin/SettingsValues;->mWordSeparators:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/android/inputmethod/latin/EditingUtils;->getThisWord(Landroid/view/inputmethod/InputConnection;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/Suggest;->getBigramPredictions(Ljava/lang/CharSequence;)Lcom/android/inputmethod/latin/SuggestedWords;

    move-result-object v0

    :cond_3
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/SuggestedWords;->size()I

    move-result v1

    if-lez v1, :cond_4

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/android/inputmethod/latin/LatinIME;->showSuggestions(Lcom/android/inputmethod/latin/SuggestedWords;Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->clearSuggestions()V

    goto :goto_0
.end method

.method public updateFullscreenMode()V
    .locals 2

    invoke-super {p0}, Landroid/inputmethodservice/InputMethodService;->updateFullscreenMode()V

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyPreviewBackingView:Landroid/view/View;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyPreviewBackingView:Landroid/view/View;

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->isFullscreenMode()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x8

    :goto_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public updateSuggestions()V
    .locals 9

    const/4 v6, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->isSuggestionsRequested()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/WordComposer;->isComposingWord()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/android/inputmethod/latin/LatinIME;->TAG:Ljava/lang/String;

    const-string v1, "Called updateSuggestions but suggestions were not requested!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/WordComposer;->getTypedWord()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/WordComposer;->setAutoCorrection(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->cancelUpdateSuggestions()V

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->cancelUpdateBigramPredictions()V

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/WordComposer;->isComposingWord()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->setPunctuationSuggestions()V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    if-nez v0, :cond_5

    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/WordComposer;->getTypedWord()Ljava/lang/String;

    move-result-object v8

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    iget-object v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v4}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getKeyboard()Lcom/android/inputmethod/keyboard/Keyboard;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/inputmethod/keyboard/Keyboard;->getProximityInfo()Lcom/android/inputmethod/keyboard/ProximityInfo;

    move-result-object v4

    iget v5, p0, Lcom/android/inputmethod/latin/LatinIME;->mCorrectionMode:I

    invoke-virtual {v1, v3, v0, v4, v5}, Lcom/android/inputmethod/latin/Suggest;->getSuggestedWords(Lcom/android/inputmethod/latin/WordComposer;Ljava/lang/CharSequence;Lcom/android/inputmethod/keyboard/ProximityInfo;I)Lcom/android/inputmethod/latin/SuggestedWords;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/SuggestedWords;->size()I

    move-result v1

    if-gt v1, v6, :cond_4

    invoke-interface {v8}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-eq v1, v6, :cond_4

    iget-boolean v1, v0, Lcom/android/inputmethod/latin/SuggestedWords;->mAllowsToBeAutoCorrected:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionsView:Lcom/android/inputmethod/latin/suggestions/SuggestionsView;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->isShowingAddToDictionaryHint()Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_4
    invoke-virtual {p0, v0, v8}, Lcom/android/inputmethod/latin/LatinIME;->showSuggestions(Lcom/android/inputmethod/latin/SuggestedWords;Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    iget-object v1, v1, Lcom/android/inputmethod/latin/SettingsValues;->mWordSeparators:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/android/inputmethod/latin/EditingUtils;->getPreviousWord(Landroid/view/inputmethod/InputConnection;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionsView:Lcom/android/inputmethod/latin/suggestions/SuggestionsView;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->getSuggestions()Lcom/android/inputmethod/latin/SuggestedWords;

    move-result-object v0

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    iget-object v1, v1, Lcom/android/inputmethod/latin/SettingsValues;->mSuggestPuncList:Lcom/android/inputmethod/latin/SuggestedWords;

    if-ne v0, v1, :cond_7

    sget-object v0, Lcom/android/inputmethod/latin/SuggestedWords;->EMPTY:Lcom/android/inputmethod/latin/SuggestedWords;

    :cond_7
    invoke-static {v8, v0}, Lcom/android/inputmethod/latin/SuggestedWords;->getTypedWordAndPreviousSuggestions(Ljava/lang/CharSequence;Lcom/android/inputmethod/latin/SuggestedWords;)Ljava/util/ArrayList;

    move-result-object v1

    new-instance v0, Lcom/android/inputmethod/latin/SuggestedWords;

    move v3, v2

    move v4, v2

    move v5, v2

    move v7, v2

    invoke-direct/range {v0 .. v7}, Lcom/android/inputmethod/latin/SuggestedWords;-><init>(Ljava/util/ArrayList;ZZZZZZ)V

    invoke-virtual {p0, v0, v8}, Lcom/android/inputmethod/latin/LatinIME;->showSuggestions(Lcom/android/inputmethod/latin/SuggestedWords;Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
