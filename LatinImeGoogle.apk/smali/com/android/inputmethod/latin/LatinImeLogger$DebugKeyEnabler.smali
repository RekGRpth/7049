.class Lcom/android/inputmethod/latin/LatinImeLogger$DebugKeyEnabler;
.super Ljava/lang/Object;
.source "LatinImeLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/latin/LatinImeLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DebugKeyEnabler"
.end annotation


# instance fields
.field private mCounter:I

.field private mLastTime:J


# direct methods
.method private constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger$DebugKeyEnabler;->mCounter:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger$DebugKeyEnabler;->mLastTime:J

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/inputmethod/latin/LatinImeLogger$1;)V
    .locals 0
    .param p1    # Lcom/android/inputmethod/latin/LatinImeLogger$1;

    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinImeLogger$DebugKeyEnabler;-><init>()V

    return-void
.end method


# virtual methods
.method public check()Z
    .locals 5

    const/4 v0, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/android/inputmethod/latin/LatinImeLogger$DebugKeyEnabler;->mLastTime:J

    sub-long/2addr v1, v3

    const-wide/16 v3, 0x2710

    cmp-long v1, v1, v3

    if-lez v1, :cond_1

    iput v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger$DebugKeyEnabler;->mCounter:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/inputmethod/latin/LatinImeLogger$DebugKeyEnabler;->mLastTime:J

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lcom/android/inputmethod/latin/LatinImeLogger$DebugKeyEnabler;->mCounter:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/inputmethod/latin/LatinImeLogger$DebugKeyEnabler;->mCounter:I

    const/16 v2, 0x9

    if-lt v1, v2, :cond_0

    iput v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger$DebugKeyEnabler;->mCounter:I

    const/4 v0, 0x1

    goto :goto_0
.end method
