.class public Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;
.super Ljava/lang/Object;
.source "LatinImeLogger.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/latin/LatinImeLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LogEntry"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;",
        ">;"
    }
.end annotation


# instance fields
.field private final mData:[Ljava/lang/String;

.field public final mTag:I

.field public mTime:J


# direct methods
.method public constructor <init>(JI[Ljava/lang/String;)V
    .locals 0
    .param p1    # J
    .param p3    # I
    .param p4    # [Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p3, p0, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;->mTag:I

    iput-wide p1, p0, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;->mTime:J

    iput-object p4, p0, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;->mData:[Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public compareTo(Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;)I
    .locals 6
    .param p1    # Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;

    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;->getLogStrings()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;->getLogStrings()[Ljava/lang/String;

    move-result-object v1

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    if-nez v1, :cond_2

    move v2, v3

    goto :goto_0

    :cond_2
    array-length v5, v0

    if-nez v5, :cond_3

    array-length v5, v1

    if-nez v5, :cond_3

    move v2, v4

    goto :goto_0

    :cond_3
    array-length v5, v0

    if-nez v5, :cond_4

    move v2, v3

    goto :goto_0

    :cond_4
    array-length v3, v1

    if-eqz v3, :cond_0

    aget-object v2, v1, v4

    aget-object v3, v0, v4

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;

    invoke-virtual {p0, p1}, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;->compareTo(Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;)I

    move-result v0

    return v0
.end method

.method public getLogStrings()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;->mData:[Ljava/lang/String;

    return-object v0
.end method
