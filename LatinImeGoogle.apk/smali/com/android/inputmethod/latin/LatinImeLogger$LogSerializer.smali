.class Lcom/android/inputmethod/latin/LatinImeLogger$LogSerializer;
.super Ljava/lang/Object;
.source "LatinImeLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/latin/LatinImeLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LogSerializer"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static appendLogEntry(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 5
    .param p0    # Ljava/lang/StringBuffer;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;

    array-length v4, p3

    if-lez v4, :cond_0

    array-length v4, p3

    add-int/lit8 v4, v4, 0x2

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4}, Lcom/android/inputmethod/latin/LatinImeLogger$LogSerializer;->appendWithLength(Ljava/lang/StringBuffer;Ljava/lang/String;)V

    invoke-static {p0, p1}, Lcom/android/inputmethod/latin/LatinImeLogger$LogSerializer;->appendWithLength(Ljava/lang/StringBuffer;Ljava/lang/String;)V

    invoke-static {p0, p2}, Lcom/android/inputmethod/latin/LatinImeLogger$LogSerializer;->appendWithLength(Ljava/lang/StringBuffer;Ljava/lang/String;)V

    move-object v0, p3

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    invoke-static {p0, v3}, Lcom/android/inputmethod/latin/LatinImeLogger$LogSerializer;->appendWithLength(Ljava/lang/StringBuffer;Ljava/lang/String;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static appendWithLength(Ljava/lang/StringBuffer;Ljava/lang/String;)V
    .locals 2
    .param p0    # Ljava/lang/StringBuffer;
    .param p1    # Ljava/lang/String;

    const/16 v1, 0x3b

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    invoke-virtual {p0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {p0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    return-void
.end method

.method public static createStringFromEntries(Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;

    iget-wide v3, v1, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;->mTime:J

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    iget v4, v1, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;->mTag:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;->getLogStrings()[Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v3, v4, v5}, Lcom/android/inputmethod/latin/LatinImeLogger$LogSerializer;->appendLogEntry(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method
