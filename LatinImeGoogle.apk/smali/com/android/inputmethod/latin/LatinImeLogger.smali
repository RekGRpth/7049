.class public Lcom/android/inputmethod/latin/LatinImeLogger;
.super Ljava/lang/Object;
.source "LatinImeLogger.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/inputmethod/latin/LatinImeLogger$1;,
        Lcom/android/inputmethod/latin/LatinImeLogger$DebugKeyEnabler;,
        Lcom/android/inputmethod/latin/LatinImeLogger$LogSerializer;,
        Lcom/android/inputmethod/latin/LatinImeLogger$AddTextToDropBoxTask;,
        Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;
    }
.end annotation


# static fields
.field private static sConfigDefaultKeyboardThemeIndex:Ljava/lang/String;

.field public static sDBG:Z

.field private static sDebugKeyEnabler:Lcom/android/inputmethod/latin/LatinImeLogger$DebugKeyEnabler;

.field private static sKeyboardHeight:I

.field private static sKeyboardWidth:I

.field static sLastAutoCorrectionAfter:Ljava/lang/String;

.field static sLastAutoCorrectionBefore:Ljava/lang/String;

.field private static sLastAutoCorrectionDataType:I

.field private static sLastAutoCorrectionDicTypeId:I

.field static sLastAutoCorrectionSeparator:Ljava/lang/String;

.field private static sLastAutoCorrectionXCoordinates:[I

.field private static sLastAutoCorrectionYCoordinates:[I

.field static final sLatinImeLogger:Lcom/android/inputmethod/latin/LatinImeLogger;

.field static sLogEnabled:Z

.field private static sPRINTLOGGING:Z

.field private static sPreviousWords:[Ljava/lang/String;

.field private static sSuggestDicMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field public static sUsabilityStudy:Z

.field public static sVISUALDEBUG:Z


# instance fields
.field private mActualCharCount:I

.field private mAddTextToDropBoxTask:Lcom/android/inputmethod/latin/LatinImeLogger$AddTextToDropBoxTask;

.field private mAutoCancelledCountPerDic:[I

.field private mAutoCorrectionCountPerDic:[I

.field private final mCancelledEntries:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/latin/LogEntries$AutoCorrectionCancelledEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentLanguage:Ljava/lang/String;

.field private mDeleteCount:I

.field private mDropBox:Landroid/os/DropBoxManager;

.field private mInputCount:I

.field private mLastTimeActive:J

.field private mLastTimeCountEntry:J

.field private mLastTimeSend:J

.field private mLatinIME:Lcom/android/inputmethod/latin/LatinIME;

.field private mLogBuffer:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mManualSuggestCountPerDic:[I

.field private mPrefs:Landroid/content/SharedPreferences;

.field private mPrivacyLogBuffer:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;",
            ">;"
        }
    .end annotation
.end field

.field mRingCharBuffer:Lcom/android/inputmethod/latin/Utils$RingCharBuffer;

.field private mSelectedLanguages:Ljava/lang/String;

.field private mThemeId:Ljava/lang/String;

.field private mWordCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    sput-boolean v2, Lcom/android/inputmethod/latin/LatinImeLogger;->sDBG:Z

    sput-boolean v2, Lcom/android/inputmethod/latin/LatinImeLogger;->sVISUALDEBUG:Z

    sput-boolean v2, Lcom/android/inputmethod/latin/LatinImeLogger;->sUsabilityStudy:Z

    sput-boolean v2, Lcom/android/inputmethod/latin/LatinImeLogger;->sPRINTLOGGING:Z

    sput-boolean v2, Lcom/android/inputmethod/latin/LatinImeLogger;->sLogEnabled:Z

    new-instance v0, Lcom/android/inputmethod/latin/LatinImeLogger;

    invoke-direct {v0}, Lcom/android/inputmethod/latin/LatinImeLogger;-><init>()V

    sput-object v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sLatinImeLogger:Lcom/android/inputmethod/latin/LatinImeLogger;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sSuggestDicMap:Ljava/util/HashMap;

    new-instance v0, Lcom/android/inputmethod/latin/LatinImeLogger$DebugKeyEnabler;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/android/inputmethod/latin/LatinImeLogger$DebugKeyEnabler;-><init>(Lcom/android/inputmethod/latin/LatinImeLogger$1;)V

    sput-object v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sDebugKeyEnabler:Lcom/android/inputmethod/latin/LatinImeLogger$DebugKeyEnabler;

    sput v2, Lcom/android/inputmethod/latin/LatinImeLogger;->sKeyboardWidth:I

    sput v2, Lcom/android/inputmethod/latin/LatinImeLogger;->sKeyboardHeight:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x7

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLogBuffer:Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mPrivacyLogBuffer:Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mRingCharBuffer:Lcom/android/inputmethod/latin/Utils$RingCharBuffer;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mCancelledEntries:Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLatinIME:Lcom/android/inputmethod/latin/LatinIME;

    iput-object v1, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mPrefs:Landroid/content/SharedPreferences;

    iput-object v1, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mDropBox:Landroid/os/DropBoxManager;

    new-array v0, v2, [I

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mAutoCorrectionCountPerDic:[I

    new-array v0, v2, [I

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mManualSuggestCountPerDic:[I

    new-array v0, v2, [I

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mAutoCancelledCountPerDic:[I

    return-void
.end method

.method static synthetic access$100()Z
    .locals 1

    sget-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sPRINTLOGGING:Z

    return v0
.end method

.method static synthetic access$202(Lcom/android/inputmethod/latin/LatinImeLogger;J)J
    .locals 0
    .param p0    # Lcom/android/inputmethod/latin/LatinImeLogger;
    .param p1    # J

    iput-wide p1, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLastTimeSend:J

    return-wide p1
.end method

.method private addCountEntry(J)V
    .locals 7
    .param p1    # J

    const/4 v6, 0x1

    const/4 v5, 0x0

    sget-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sPRINTLOGGING:Z

    if-eqz v0, :cond_0

    const-string v0, "LatinIMELogs"

    const-string v1, "Log counts. (4)"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLogBuffer:Ljava/util/ArrayList;

    new-instance v1, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;

    const/4 v2, 0x4

    new-array v3, v6, [Ljava/lang/String;

    iget v4, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mDeleteCount:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-direct {v1, p1, p2, v2, v3}, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;-><init>(JI[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLogBuffer:Ljava/util/ArrayList;

    new-instance v1, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;

    const/4 v2, 0x3

    new-array v3, v6, [Ljava/lang/String;

    iget v4, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mInputCount:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-direct {v1, p1, p2, v2, v3}, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;-><init>(JI[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLogBuffer:Ljava/util/ArrayList;

    new-instance v1, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;

    const/4 v2, 0x5

    new-array v3, v6, [Ljava/lang/String;

    iget v4, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mWordCount:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-direct {v1, p1, p2, v2, v3}, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;-><init>(JI[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLogBuffer:Ljava/util/ArrayList;

    new-instance v1, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;

    const/4 v2, 0x6

    new-array v3, v6, [Ljava/lang/String;

    iget v4, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mActualCharCount:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-direct {v1, p1, p2, v2, v3}, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;-><init>(JI[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput v5, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mDeleteCount:I

    iput v5, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mInputCount:I

    iput v5, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mWordCount:I

    iput v5, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mActualCharCount:I

    iput-wide p1, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLastTimeCountEntry:J

    return-void
.end method

.method private addData(ILjava/lang/Object;)V
    .locals 7
    .param p1    # I
    .param p2    # Ljava/lang/Object;

    const-wide/16 v5, 0x4e20

    const/4 v4, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x1

    sparse-switch p1, :sswitch_data_0

    sget-boolean v1, Lcom/android/inputmethod/latin/LatinImeLogger;->sDBG:Z

    if-eqz v1, :cond_0

    const-string v1, "LatinIMELogs"

    const-string v2, "Log Tag is not entried."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    iget-wide v1, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLastTimeActive:J

    iget-wide v3, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLastTimeCountEntry:J

    sub-long/2addr v1, v3

    cmp-long v1, v1, v5

    if-gtz v1, :cond_1

    iget v1, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mDeleteCount:I

    if-nez v1, :cond_2

    iget v1, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mInputCount:I

    if-nez v1, :cond_2

    :cond_1
    iget-wide v1, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLastTimeActive:J

    invoke-direct {p0, v1, v2}, Lcom/android/inputmethod/latin/LatinImeLogger;->addCountEntry(J)V

    :cond_2
    iget v1, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mDeleteCount:I

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mDeleteCount:I

    goto :goto_0

    :sswitch_1
    iget-wide v1, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLastTimeActive:J

    iget-wide v3, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLastTimeCountEntry:J

    sub-long/2addr v1, v3

    cmp-long v1, v1, v5

    if-gtz v1, :cond_3

    iget v1, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mDeleteCount:I

    if-nez v1, :cond_4

    iget v1, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mInputCount:I

    if-nez v1, :cond_4

    :cond_3
    iget-wide v1, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLastTimeActive:J

    invoke-direct {p0, v1, v2}, Lcom/android/inputmethod/latin/LatinImeLogger;->addCountEntry(J)V

    :cond_4
    iget v1, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mInputCount:I

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mInputCount:I

    goto :goto_0

    :sswitch_2
    iget v1, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mWordCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mWordCount:I

    check-cast p2, [Ljava/lang/String;

    move-object v0, p2

    check-cast v0, [Ljava/lang/String;

    array-length v1, v0

    if-ge v1, v2, :cond_5

    sget-boolean v1, Lcom/android/inputmethod/latin/LatinImeLogger;->sDBG:Z

    if-eqz v1, :cond_0

    const-string v1, "LatinIMELogs"

    const-string v2, "The length of logged string array is invalid."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_5
    iget v1, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mActualCharCount:I

    aget-object v2, v0, v3

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mActualCharCount:I

    aget-object v1, v0, v4

    invoke-static {v1}, Lcom/android/inputmethod/latin/LatinImeLogger;->checkStringDataSafe(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    aget-object v1, v0, v3

    invoke-static {v1}, Lcom/android/inputmethod/latin/LatinImeLogger;->checkStringDataSafe(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mPrivacyLogBuffer:Ljava/util/ArrayList;

    new-instance v2, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-direct {v2, v3, v4, p1, v0}, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;-><init>(JI[Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_6
    sget-boolean v1, Lcom/android/inputmethod/latin/LatinImeLogger;->sDBG:Z

    if-eqz v1, :cond_0

    const-string v1, "LatinIMELogs"

    const-string v2, "Skipped to add an entry because data is unsafe."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :sswitch_3
    iget v1, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mWordCount:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mWordCount:I

    check-cast p2, [Ljava/lang/String;

    move-object v0, p2

    check-cast v0, [Ljava/lang/String;

    array-length v1, v0

    if-ge v1, v2, :cond_7

    sget-boolean v1, Lcom/android/inputmethod/latin/LatinImeLogger;->sDBG:Z

    if-eqz v1, :cond_0

    const-string v1, "LatinIMELogs"

    const-string v2, "The length of logged string array is invalid."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_7
    iget v1, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mActualCharCount:I

    aget-object v2, v0, v3

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mActualCharCount:I

    aget-object v1, v0, v4

    invoke-static {v1}, Lcom/android/inputmethod/latin/LatinImeLogger;->checkStringDataSafe(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    aget-object v1, v0, v3

    invoke-static {v1}, Lcom/android/inputmethod/latin/LatinImeLogger;->checkStringDataSafe(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mPrivacyLogBuffer:Ljava/util/ArrayList;

    new-instance v2, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-direct {v2, v3, v4, p1, v0}, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;-><init>(JI[Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_8
    sget-boolean v1, Lcom/android/inputmethod/latin/LatinImeLogger;->sDBG:Z

    if-eqz v1, :cond_0

    const-string v1, "LatinIMELogs"

    const-string v2, "Skipped to add an entry because data is unsafe."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :sswitch_4
    check-cast p2, [Ljava/lang/String;

    move-object v0, p2

    check-cast v0, [Ljava/lang/String;

    array-length v1, v0

    if-ge v1, v2, :cond_9

    sget-boolean v1, Lcom/android/inputmethod/latin/LatinImeLogger;->sDBG:Z

    if-eqz v1, :cond_0

    const-string v1, "LatinIMELogs"

    const-string v2, "The length of logged string array is invalid."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_9
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-direct {p0, v1, v2, v0}, Lcom/android/inputmethod/latin/LatinImeLogger;->addExceptionEntry(J[Ljava/lang/String;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_1
        0x4 -> :sswitch_0
        0xa -> :sswitch_4
        0xf -> :sswitch_2
        0x11 -> :sswitch_2
        0x13 -> :sswitch_3
    .end sparse-switch
.end method

.method private addDeviceInfoEntry(J)V
    .locals 2
    .param p1    # J

    sget-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sPRINTLOGGING:Z

    if-eqz v0, :cond_0

    const-string v0, "LatinIMELogs"

    const-string v1, "Log device info entry."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLogBuffer:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLatinIME:Lcom/android/inputmethod/latin/LatinIME;

    invoke-static {p1, p2, v1}, Lcom/android/inputmethod/latin/LogEntries$DeviceInfoEntry;->getInstance(JLandroid/content/Context;)Lcom/android/inputmethod/latin/LogEntries$DeviceInfoEntry;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private addExceptionEntry(J[Ljava/lang/String;)V
    .locals 3
    .param p1    # J
    .param p3    # [Ljava/lang/String;

    sget-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sPRINTLOGGING:Z

    if-eqz v0, :cond_0

    const-string v0, "LatinIMELogs"

    const-string v1, "Log Exception. (1)"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLogBuffer:Ljava/util/ArrayList;

    new-instance v1, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;

    const/16 v2, 0xa

    invoke-direct {v1, p1, p2, v2, p3}, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;-><init>(JI[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private addLanguagesEntry(J)V
    .locals 6
    .param p1    # J

    sget-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sPRINTLOGGING:Z

    if-eqz v0, :cond_0

    const-string v0, "LatinIMELogs"

    const-string v1, "Log language settings. (1)"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mCurrentLanguage:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLatinIME:Lcom/android/inputmethod/latin/LatinIME;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/LatinIME;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mCurrentLanguage:Ljava/lang/String;

    :cond_1
    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLogBuffer:Ljava/util/ArrayList;

    new-instance v1, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;

    const/16 v2, 0xe

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mCurrentLanguage:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mSelectedLanguages:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-direct {v1, p1, p2, v2, v3}, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;-><init>(JI[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private addSettingsEntry(J)V
    .locals 7
    .param p1    # J

    sget-boolean v2, Lcom/android/inputmethod/latin/LatinImeLogger;->sPRINTLOGGING:Z

    if-eqz v2, :cond_0

    const-string v2, "LatinIMELogs"

    const-string v3, "Log settings. (1)"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLatinIME:Lcom/android/inputmethod/latin/LatinIME;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/LatinIME;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a000e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mPrefs:Landroid/content/SharedPreferences;

    const-string v3, "auto_correction_threshold"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLogBuffer:Ljava/util/ArrayList;

    new-instance v3, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;

    const/16 v4, 0x8

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-direct {v3, p1, p2, v4, v5}, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;-><init>(JI[Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private addSuggestionCountEntry(J)V
    .locals 6
    .param p1    # J

    const/4 v5, 0x0

    sget-boolean v2, Lcom/android/inputmethod/latin/LatinImeLogger;->sPRINTLOGGING:Z

    if-eqz v2, :cond_0

    const-string v2, "LatinIMELogs"

    const-string v3, "log suggest counts. (1)"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mAutoCorrectionCountPerDic:[I

    array-length v2, v2

    new-array v1, v2, [Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mAutoCorrectionCountPerDic:[I

    aget v2, v2, v0

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLogBuffer:Ljava/util/ArrayList;

    new-instance v3, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;

    const/16 v4, 0xd

    invoke-direct {v3, p1, p2, v4, v1}, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;-><init>(JI[Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mAutoCancelledCountPerDic:[I

    array-length v2, v2

    new-array v1, v2, [Ljava/lang/String;

    const/4 v0, 0x0

    :goto_1
    array-length v2, v1

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mAutoCancelledCountPerDic:[I

    aget v2, v2, v0

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLogBuffer:Ljava/util/ArrayList;

    new-instance v3, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;

    const/16 v4, 0xc

    invoke-direct {v3, p1, p2, v4, v1}, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;-><init>(JI[Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mManualSuggestCountPerDic:[I

    array-length v2, v2

    new-array v1, v2, [Ljava/lang/String;

    const/4 v0, 0x0

    :goto_2
    array-length v2, v1

    if-ge v0, v2, :cond_3

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mManualSuggestCountPerDic:[I

    aget v2, v2, v0

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLogBuffer:Ljava/util/ArrayList;

    new-instance v3, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;

    const/16 v4, 0xb

    invoke-direct {v3, p1, p2, v4, v1}, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;-><init>(JI[Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mAutoCorrectionCountPerDic:[I

    invoke-static {v2, v5}, Ljava/util/Arrays;->fill([II)V

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mManualSuggestCountPerDic:[I

    invoke-static {v2, v5}, Ljava/util/Arrays;->fill([II)V

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mAutoCancelledCountPerDic:[I

    invoke-static {v2, v5}, Ljava/util/Arrays;->fill([II)V

    return-void
.end method

.method private addThemeIdEntry(J)V
    .locals 6
    .param p1    # J

    sget-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sPRINTLOGGING:Z

    if-eqz v0, :cond_0

    const-string v0, "LatinIMELogs"

    const-string v1, "Log theme Id. (1)"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mThemeId:Ljava/lang/String;

    const-string v1, "2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "6"

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mThemeId:Ljava/lang/String;

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLogBuffer:Ljava/util/ArrayList;

    new-instance v1, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;

    const/4 v2, 0x7

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mThemeId:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-direct {v1, p1, p2, v2, v3}, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;-><init>(JI[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mThemeId:Ljava/lang/String;

    const-string v1, "3"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "7"

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mThemeId:Ljava/lang/String;

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mThemeId:Ljava/lang/String;

    const-string v1, "4"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "8"

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mThemeId:Ljava/lang/String;

    goto :goto_0
.end method

.method private addVersionNameEntry(J)V
    .locals 8
    .param p1    # J

    sget-boolean v2, Lcom/android/inputmethod/latin/LatinImeLogger;->sPRINTLOGGING:Z

    if-eqz v2, :cond_0

    const-string v2, "LatinIMELogs"

    const-string v3, "Log Version. (1)"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLatinIME:Lcom/android/inputmethod/latin/LatinIME;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/LatinIME;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLatinIME:Lcom/android/inputmethod/latin/LatinIME;

    invoke-virtual {v3}, Lcom/android/inputmethod/latin/LatinIME;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLogBuffer:Ljava/util/ArrayList;

    new-instance v3, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;

    const/16 v4, 0x9

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    iget v7, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget-object v7, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-direct {v3, p1, p2, v4, v5}, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;-><init>(JI[Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "LatinIMELogs"

    const-string v3, "Could not find version name."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static checkStringDataSafe(Ljava/lang/String;)Z
    .locals 4
    .param p0    # Ljava/lang/String;

    sget-boolean v1, Lcom/android/inputmethod/latin/LatinImeLogger;->sDBG:Z

    if-eqz v1, :cond_0

    const-string v1, "LatinIMELogs"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Check String safety: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_2

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isDigit(C)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    :goto_1
    return v1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public static commit()V
    .locals 4

    sget-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sLogEnabled:Z

    if-eqz v0, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sget-object v2, Lcom/android/inputmethod/latin/LatinImeLogger;->sLatinImeLogger:Lcom/android/inputmethod/latin/LatinImeLogger;

    iget-wide v2, v2, Lcom/android/inputmethod/latin/LatinImeLogger;->mLastTimeActive:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x4e20

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    sget-object v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sLatinImeLogger:Lcom/android/inputmethod/latin/LatinImeLogger;

    iget-object v0, v0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLogBuffer:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    sget-object v1, Lcom/android/inputmethod/latin/LatinImeLogger;->sLatinImeLogger:Lcom/android/inputmethod/latin/LatinImeLogger;

    iget-object v1, v1, Lcom/android/inputmethod/latin/LatinImeLogger;->mPrivacyLogBuffer:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v0, v1

    int-to-long v0, v0

    const-wide/16 v2, 0x28

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    :cond_0
    sget-object v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sLatinImeLogger:Lcom/android/inputmethod/latin/LatinImeLogger;

    invoke-direct {v0}, Lcom/android/inputmethod/latin/LatinImeLogger;->commitInternal()V

    :cond_1
    return-void
.end method

.method private commitInternal()V
    .locals 6

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLogBuffer:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mAddTextToDropBoxTask:Lcom/android/inputmethod/latin/LatinImeLogger$AddTextToDropBoxTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mAddTextToDropBoxTask:Lcom/android/inputmethod/latin/LatinImeLogger$AddTextToDropBoxTask;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/LatinImeLogger$AddTextToDropBoxTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_2

    :cond_0
    sget-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sPRINTLOGGING:Z

    if-eqz v0, :cond_1

    const-string v0, "LatinIMELogs"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Commit ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLogBuffer:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinImeLogger;->flushPrivacyLogSafely()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-direct {p0, v3, v4}, Lcom/android/inputmethod/latin/LatinImeLogger;->addCountEntry(J)V

    invoke-direct {p0, v3, v4}, Lcom/android/inputmethod/latin/LatinImeLogger;->addThemeIdEntry(J)V

    invoke-direct {p0, v3, v4}, Lcom/android/inputmethod/latin/LatinImeLogger;->addLanguagesEntry(J)V

    invoke-direct {p0, v3, v4}, Lcom/android/inputmethod/latin/LatinImeLogger;->addSettingsEntry(J)V

    invoke-direct {p0, v3, v4}, Lcom/android/inputmethod/latin/LatinImeLogger;->addVersionNameEntry(J)V

    invoke-direct {p0, v3, v4}, Lcom/android/inputmethod/latin/LatinImeLogger;->addSuggestionCountEntry(J)V

    invoke-direct {p0, v3, v4}, Lcom/android/inputmethod/latin/LatinImeLogger;->addDeviceInfoEntry(J)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLogBuffer:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/android/inputmethod/latin/LatinImeLogger$LogSerializer;->createStringFromEntries(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinImeLogger;->reset()V

    new-instance v0, Lcom/android/inputmethod/latin/LatinImeLogger$AddTextToDropBoxTask;

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mDropBox:Landroid/os/DropBoxManager;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/inputmethod/latin/LatinImeLogger$AddTextToDropBoxTask;-><init>(Lcom/android/inputmethod/latin/LatinImeLogger;Landroid/os/DropBoxManager;JLjava/lang/String;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/LatinImeLogger$AddTextToDropBoxTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v0

    check-cast v0, Lcom/android/inputmethod/latin/LatinImeLogger$AddTextToDropBoxTask;

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mAddTextToDropBoxTask:Lcom/android/inputmethod/latin/LatinImeLogger$AddTextToDropBoxTask;

    :cond_2
    return-void
.end method

.method private commitInternalAndStopSelf()V
    .locals 2

    sget-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sDBG:Z

    if-eqz v0, :cond_0

    const-string v0, "LatinIMELogs"

    const-string v1, "Exception was thrown and let\'s die."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinImeLogger;->commitInternal()V

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLatinIME:Lcom/android/inputmethod/latin/LatinIME;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/LatinIME;->hideWindow()V

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLatinIME:Lcom/android/inputmethod/latin/LatinIME;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/LatinIME;->stopSelf()V

    return-void
.end method

.method private flushAutoCorrectionCancelledEntries(Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLogBuffer:Ljava/util/ArrayList;

    monitor-enter v3

    :try_start_0
    sget-boolean v2, Lcom/android/inputmethod/latin/LatinImeLogger;->sDBG:Z

    if-eqz v2, :cond_0

    const-string v2, "LatinIMELogs"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Preferable word = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mCancelledEntries:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_1

    monitor-exit v3

    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mCancelledEntries:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mCancelledEntries:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/inputmethod/latin/LogEntries$AutoCorrectionCancelledEntry;

    invoke-virtual {v2, p1}, Lcom/android/inputmethod/latin/LogEntries$AutoCorrectionCancelledEntry;->setPreferableWord(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mCancelledEntries:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/inputmethod/latin/LogEntries$AutoCorrectionCancelledEntry;

    iget v2, v1, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;->mTag:I

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;->getLogStrings()[Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v2, v4}, Lcom/android/inputmethod/latin/LatinImeLogger;->sendLogToDropBox(ILjava/lang/Object;)V

    goto :goto_1

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_2
    :try_start_1
    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mCancelledEntries:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private flushPrivacyLogSafely()V
    .locals 7

    sget-boolean v4, Lcom/android/inputmethod/latin/LatinImeLogger;->sPRINTLOGGING:Z

    if-eqz v4, :cond_0

    const-string v4, "LatinIMELogs"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Log obfuscated data. ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mPrivacyLogBuffer:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v4, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mPrivacyLogBuffer:Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    iget-object v4, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mPrivacyLogBuffer:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;

    iput-wide v2, v1, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;->mTime:J

    iget-object v4, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLogBuffer:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mPrivacyLogBuffer:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method public static init(Lcom/android/inputmethod/latin/LatinIME;Landroid/content/SharedPreferences;)V
    .locals 1
    .param p0    # Lcom/android/inputmethod/latin/LatinIME;
    .param p1    # Landroid/content/SharedPreferences;

    sget-object v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sLatinImeLogger:Lcom/android/inputmethod/latin/LatinImeLogger;

    invoke-direct {v0, p0, p1}, Lcom/android/inputmethod/latin/LatinImeLogger;->initInternal(Lcom/android/inputmethod/latin/LatinIME;Landroid/content/SharedPreferences;)V

    return-void
.end method

.method private initInternal(Lcom/android/inputmethod/latin/LatinIME;Landroid/content/SharedPreferences;)V
    .locals 3
    .param p1    # Lcom/android/inputmethod/latin/LatinIME;
    .param p2    # Landroid/content/SharedPreferences;

    const/4 v2, 0x0

    iput-object p1, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLatinIME:Lcom/android/inputmethod/latin/LatinIME;

    iput-object p2, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLatinIME:Lcom/android/inputmethod/latin/LatinIME;

    const-string v1, "dropbox"

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/LatinIME;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/DropBoxManager;

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mDropBox:Landroid/os/DropBoxManager;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLastTimeSend:J

    iget-wide v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLastTimeSend:J

    iput-wide v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLastTimeActive:J

    iget-wide v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLastTimeSend:J

    iput-wide v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLastTimeCountEntry:J

    iput v2, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mDeleteCount:I

    iput v2, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mInputCount:I

    iput v2, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mWordCount:I

    iput v2, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mActualCharCount:I

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mAutoCorrectionCountPerDic:[I

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([II)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mManualSuggestCountPerDic:[I

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([II)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mAutoCancelledCountPerDic:[I

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([II)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLogBuffer:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mPrivacyLogBuffer:Ljava/util/ArrayList;

    sput-boolean v2, Lcom/android/inputmethod/latin/LatinImeLogger;->sUsabilityStudy:Z

    sput-boolean v2, Lcom/android/inputmethod/latin/LatinImeLogger;->sLogEnabled:Z

    const/high16 v0, 0x7f0a0000

    invoke-virtual {p1, v0}, Lcom/android/inputmethod/latin/LatinIME;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sConfigDefaultKeyboardThemeIndex:Ljava/lang/String;

    const-string v0, "pref_keyboard_layout_20110916"

    sget-object v1, Lcom/android/inputmethod/latin/LatinImeLogger;->sConfigDefaultKeyboardThemeIndex:Ljava/lang/String;

    invoke-interface {p2, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mThemeId:Ljava/lang/String;

    const-string v0, "selected_languages"

    const-string v1, ""

    invoke-interface {p2, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mSelectedLanguages:Ljava/lang/String;

    const-string v0, "input_language"

    const-string v1, ""

    invoke-interface {p2, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mCurrentLanguage:Ljava/lang/String;

    const-string v0, "debug_mode"

    sget-boolean v1, Lcom/android/inputmethod/latin/LatinImeLogger;->sPRINTLOGGING:Z

    invoke-interface {p2, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sPRINTLOGGING:Z

    sget-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sPRINTLOGGING:Z

    sput-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sDBG:Z

    sget-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sLogEnabled:Z

    sget-boolean v1, Lcom/android/inputmethod/latin/LatinImeLogger;->sUsabilityStudy:Z

    invoke-static {p1, v0, v1}, Lcom/android/inputmethod/latin/Utils$RingCharBuffer;->init(Landroid/inputmethodservice/InputMethodService;ZZ)Lcom/android/inputmethod/latin/Utils$RingCharBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mRingCharBuffer:Lcom/android/inputmethod/latin/Utils$RingCharBuffer;

    invoke-interface {p2, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    return-void
.end method

.method public static logOnAutoCorrection(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 10

    const/4 v6, 0x1

    const/4 v1, 0x0

    sget-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sLogEnabled:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sLatinImeLogger:Lcom/android/inputmethod/latin/LatinImeLogger;

    invoke-direct {v0, p1}, Lcom/android/inputmethod/latin/LatinImeLogger;->flushAutoCorrectionCancelledEntries(Ljava/lang/String;)V

    sget-object v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sSuggestDicMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sDBG:Z

    if-eqz v0, :cond_0

    const-string v0, "LatinIMELogs"

    const-string v1, "logOnAutoCorrection was cancelled: from unknown dic."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    sget-object v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sSuggestDicMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_1
    return-void

    :cond_2
    int-to-char v0, p2

    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v3

    sget-object v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sSuggestDicMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sLastAutoCorrectionDicTypeId:I

    sget-object v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sSuggestDicMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sLastAutoCorrectionDataType:I

    sget-object v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sLatinImeLogger:Lcom/android/inputmethod/latin/LatinImeLogger;

    iget-object v0, v0, Lcom/android/inputmethod/latin/LatinImeLogger;->mAutoCorrectionCountPerDic:[I

    sget v2, Lcom/android/inputmethod/latin/LatinImeLogger;->sLastAutoCorrectionDicTypeId:I

    aget v4, v0, v2

    add-int/lit8 v4, v4, 0x1

    aput v4, v0, v2

    sget-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sUsabilityStudy:Z

    if-eqz v0, :cond_3

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-static {}, Lcom/android/inputmethod/latin/Utils$UsabilityStudyLogUtils;->getInstance()Lcom/android/inputmethod/latin/Utils$UsabilityStudyLogUtils;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[----]"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\t\t"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/android/inputmethod/latin/Utils$UsabilityStudyLogUtils;->write(Ljava/lang/String;)V

    :cond_3
    :goto_1
    sget v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sLastAutoCorrectionDicTypeId:I

    if-eq v0, v6, :cond_5

    sget-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sDBG:Z

    if-eqz v0, :cond_4

    const-string v0, "LatinIMELogs"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "logOnAutoCorrection was cancelled: not from main dic.:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v4, Lcom/android/inputmethod/latin/LatinImeLogger;->sLastAutoCorrectionDicTypeId:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    const-string p0, ""

    const-string p1, ""

    const/4 v0, 0x0

    sput-object v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sPreviousWords:[Ljava/lang/String;

    :cond_5
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    sget-object v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sPreviousWords:[Ljava/lang/String;

    if-nez v0, :cond_7

    move v0, v1

    :goto_2
    mul-int/lit8 v2, v4, 0x2

    add-int/lit8 v2, v2, 0x6

    add-int/2addr v2, v0

    new-array v5, v2, [Ljava/lang/String;

    new-array v2, v4, [I

    sput-object v2, Lcom/android/inputmethod/latin/LatinImeLogger;->sLastAutoCorrectionXCoordinates:[I

    new-array v2, v4, [I

    sput-object v2, Lcom/android/inputmethod/latin/LatinImeLogger;->sLastAutoCorrectionYCoordinates:[I

    aput-object p0, v5, v1

    aput-object p1, v5, v6

    const/4 v2, 0x2

    aput-object v3, v5, v2

    const/4 v2, 0x3

    sget v6, Lcom/android/inputmethod/latin/LatinImeLogger;->sLastAutoCorrectionDataType:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    const/4 v2, 0x4

    sget v6, Lcom/android/inputmethod/latin/LatinImeLogger;->sKeyboardWidth:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    const/4 v2, 0x5

    sget v6, Lcom/android/inputmethod/latin/LatinImeLogger;->sKeyboardHeight:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_8

    sub-int v6, v4, v2

    add-int/lit8 v6, v6, -0x2

    sget-object v7, Lcom/android/inputmethod/latin/LatinImeLogger;->sLatinImeLogger:Lcom/android/inputmethod/latin/LatinImeLogger;

    iget-object v7, v7, Lcom/android/inputmethod/latin/LatinImeLogger;->mRingCharBuffer:Lcom/android/inputmethod/latin/Utils$RingCharBuffer;

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v8

    invoke-virtual {v7, v8, v6}, Lcom/android/inputmethod/latin/Utils$RingCharBuffer;->getPreviousX(CI)I

    move-result v7

    sget-object v8, Lcom/android/inputmethod/latin/LatinImeLogger;->sLatinImeLogger:Lcom/android/inputmethod/latin/LatinImeLogger;

    iget-object v8, v8, Lcom/android/inputmethod/latin/LatinImeLogger;->mRingCharBuffer:Lcom/android/inputmethod/latin/Utils$RingCharBuffer;

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v9

    invoke-virtual {v8, v9, v6}, Lcom/android/inputmethod/latin/Utils$RingCharBuffer;->getPreviousY(CI)I

    move-result v6

    mul-int/lit8 v8, v2, 0x2

    add-int/lit8 v8, v8, 0x6

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v5, v8

    mul-int/lit8 v8, v2, 0x2

    add-int/lit8 v8, v8, 0x6

    add-int/lit8 v8, v8, 0x1

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v5, v8

    sget-object v8, Lcom/android/inputmethod/latin/LatinImeLogger;->sLastAutoCorrectionXCoordinates:[I

    aput v7, v8, v2

    sget-object v7, Lcom/android/inputmethod/latin/LatinImeLogger;->sLastAutoCorrectionYCoordinates:[I

    aput v6, v7, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_6
    invoke-static {}, Lcom/android/inputmethod/latin/Utils$UsabilityStudyLogUtils;->getInstance()Lcom/android/inputmethod/latin/Utils$UsabilityStudyLogUtils;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[Auto]"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ","

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\t\t"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/android/inputmethod/latin/Utils$UsabilityStudyLogUtils;->write(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_7
    sget-object v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sPreviousWords:[Ljava/lang/String;

    array-length v0, v0

    goto/16 :goto_2

    :cond_8
    :goto_4
    if-ge v1, v0, :cond_9

    mul-int/lit8 v2, v4, 0x2

    add-int/lit8 v2, v2, 0x6

    add-int/2addr v2, v1

    sget-object v6, Lcom/android/inputmethod/latin/LatinImeLogger;->sPreviousWords:[Ljava/lang/String;

    aget-object v6, v6, v1

    aput-object v6, v5, v2

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_9
    sget-object v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sLatinImeLogger:Lcom/android/inputmethod/latin/LatinImeLogger;

    const/16 v1, 0x11

    invoke-direct {v0, v1, v5}, Lcom/android/inputmethod/latin/LatinImeLogger;->sendLogToDropBox(ILjava/lang/Object;)V

    const-class v1, Lcom/android/inputmethod/latin/LatinImeLogger;

    monitor-enter v1

    :try_start_0
    sput-object p0, Lcom/android/inputmethod/latin/LatinImeLogger;->sLastAutoCorrectionBefore:Ljava/lang/String;

    sput-object p1, Lcom/android/inputmethod/latin/LatinImeLogger;->sLastAutoCorrectionAfter:Ljava/lang/String;

    sput-object v3, Lcom/android/inputmethod/latin/LatinImeLogger;->sLastAutoCorrectionSeparator:Ljava/lang/String;

    monitor-exit v1

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static logOnAutoCorrectionCancelled()V
    .locals 9

    sget-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sLogEnabled:Z

    if-eqz v0, :cond_3

    sget-object v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sLatinImeLogger:Lcom/android/inputmethod/latin/LatinImeLogger;

    iget-object v0, v0, Lcom/android/inputmethod/latin/LatinImeLogger;->mAutoCancelledCountPerDic:[I

    sget v1, Lcom/android/inputmethod/latin/LatinImeLogger;->sLastAutoCorrectionDicTypeId:I

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sLastAutoCorrectionBefore:Ljava/lang/String;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sLastAutoCorrectionAfter:Ljava/lang/String;

    if-eqz v0, :cond_2

    sget-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sUsabilityStudy:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sLastAutoCorrectionBefore:Ljava/lang/String;

    sget-object v1, Lcom/android/inputmethod/latin/LatinImeLogger;->sLastAutoCorrectionAfter:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/android/inputmethod/latin/Utils$UsabilityStudyLogUtils;->getInstance()Lcom/android/inputmethod/latin/Utils$UsabilityStudyLogUtils;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Cancel]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/android/inputmethod/latin/LatinImeLogger;->sLastAutoCorrectionBefore:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/android/inputmethod/latin/LatinImeLogger;->sLastAutoCorrectionAfter:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\t\t"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/Utils$UsabilityStudyLogUtils;->write(Ljava/lang/String;)V

    :cond_0
    sget-boolean v0, Lcom/android/inputmethod/latin/ResearchLogger;->sIsLogging:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sLastAutoCorrectionBefore:Ljava/lang/String;

    sget-object v1, Lcom/android/inputmethod/latin/LatinImeLogger;->sLastAutoCorrectionAfter:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/android/inputmethod/latin/ResearchLogger;->getInstance()Lcom/android/inputmethod/latin/ResearchLogger;

    move-result-object v0

    const-string v1, "[Cancel]"

    sget-object v2, Lcom/android/inputmethod/latin/LatinImeLogger;->sLastAutoCorrectionBefore:Ljava/lang/String;

    sget-object v3, Lcom/android/inputmethod/latin/LatinImeLogger;->sLastAutoCorrectionAfter:Ljava/lang/String;

    const/4 v4, -0x1

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/inputmethod/latin/ResearchLogger;->logCorrection(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    :cond_1
    new-instance v0, Lcom/android/inputmethod/latin/LogEntries$AutoCorrectionCancelledEntry;

    sget-object v1, Lcom/android/inputmethod/latin/LatinImeLogger;->sLastAutoCorrectionBefore:Ljava/lang/String;

    sget-object v2, Lcom/android/inputmethod/latin/LatinImeLogger;->sLastAutoCorrectionAfter:Ljava/lang/String;

    sget-object v3, Lcom/android/inputmethod/latin/LatinImeLogger;->sLastAutoCorrectionSeparator:Ljava/lang/String;

    sget v4, Lcom/android/inputmethod/latin/LatinImeLogger;->sLastAutoCorrectionDataType:I

    sget v5, Lcom/android/inputmethod/latin/LatinImeLogger;->sKeyboardWidth:I

    sget v6, Lcom/android/inputmethod/latin/LatinImeLogger;->sKeyboardHeight:I

    sget-object v7, Lcom/android/inputmethod/latin/LatinImeLogger;->sLastAutoCorrectionXCoordinates:[I

    sget-object v8, Lcom/android/inputmethod/latin/LatinImeLogger;->sLastAutoCorrectionYCoordinates:[I

    invoke-direct/range {v0 .. v8}, Lcom/android/inputmethod/latin/LogEntries$AutoCorrectionCancelledEntry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III[I[I)V

    sget-object v1, Lcom/android/inputmethod/latin/LatinImeLogger;->sLatinImeLogger:Lcom/android/inputmethod/latin/LatinImeLogger;

    iget-object v1, v1, Lcom/android/inputmethod/latin/LatinImeLogger;->mCancelledEntries:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    const-class v1, Lcom/android/inputmethod/latin/LatinImeLogger;

    monitor-enter v1

    :try_start_0
    const-string v0, ""

    sput-object v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sLastAutoCorrectionBefore:Ljava/lang/String;

    const-string v0, ""

    sput-object v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sLastAutoCorrectionAfter:Ljava/lang/String;

    const-string v0, ""

    sput-object v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sLastAutoCorrectionSeparator:Ljava/lang/String;

    monitor-exit v1

    :cond_3
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static logOnDelete(II)V
    .locals 4
    .param p0    # I
    .param p1    # I

    sget-boolean v1, Lcom/android/inputmethod/latin/LatinImeLogger;->sLogEnabled:Z

    if-eqz v1, :cond_1

    sget-object v1, Lcom/android/inputmethod/latin/LatinImeLogger;->sLatinImeLogger:Lcom/android/inputmethod/latin/LatinImeLogger;

    iget-object v1, v1, Lcom/android/inputmethod/latin/LatinImeLogger;->mRingCharBuffer:Lcom/android/inputmethod/latin/Utils$RingCharBuffer;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/inputmethod/latin/Utils$RingCharBuffer;->getLastWord(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/android/inputmethod/latin/LatinImeLogger;->sLastAutoCorrectionBefore:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/android/inputmethod/latin/LatinImeLogger;->logOnAutoCorrectionCancelled()V

    :cond_0
    sget-object v1, Lcom/android/inputmethod/latin/LatinImeLogger;->sLatinImeLogger:Lcom/android/inputmethod/latin/LatinImeLogger;

    iget-object v1, v1, Lcom/android/inputmethod/latin/LatinImeLogger;->mRingCharBuffer:Lcom/android/inputmethod/latin/Utils$RingCharBuffer;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/Utils$RingCharBuffer;->pop()C

    sget-object v1, Lcom/android/inputmethod/latin/LatinImeLogger;->sLatinImeLogger:Lcom/android/inputmethod/latin/LatinImeLogger;

    const/4 v2, 0x4

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/android/inputmethod/latin/LatinImeLogger;->sendLogToDropBox(ILjava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public static logOnException(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 8
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/Throwable;

    const/4 v7, 0x0

    sget-boolean v3, Lcom/android/inputmethod/latin/LatinImeLogger;->sLogEnabled:Z

    if-eqz v3, :cond_1

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v2, Ljava/io/PrintStream;

    invoke-direct {v2, v0}, Ljava/io/PrintStream;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {p1, v2}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintStream;)V

    new-instance v3, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    const/16 v5, 0x190

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-direct {v3, v4, v7, v5}, Ljava/lang/String;-><init>([BII)V

    invoke-static {v3}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/android/inputmethod/latin/LatinImeLogger;->sLatinImeLogger:Lcom/android/inputmethod/latin/LatinImeLogger;

    const/16 v4, 0xa

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    aput-object p0, v5, v7

    const/4 v6, 0x1

    aput-object v1, v5, v6

    invoke-direct {v3, v4, v5}, Lcom/android/inputmethod/latin/LatinImeLogger;->sendLogToDropBox(ILjava/lang/Object;)V

    sget-boolean v3, Lcom/android/inputmethod/latin/LatinImeLogger;->sDBG:Z

    if-eqz v3, :cond_0

    const-string v3, "LatinIMELogs"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    sget-object v3, Lcom/android/inputmethod/latin/LatinImeLogger;->sLatinImeLogger:Lcom/android/inputmethod/latin/LatinImeLogger;

    invoke-direct {v3}, Lcom/android/inputmethod/latin/LatinImeLogger;->commitInternalAndStopSelf()V

    :cond_1
    return-void
.end method

.method public static logOnInputChar()V
    .locals 4

    const/4 v3, 0x1

    sget-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sLogEnabled:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sLatinImeLogger:Lcom/android/inputmethod/latin/LatinImeLogger;

    const/4 v1, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/android/inputmethod/latin/LatinImeLogger;->sendLogToDropBox(ILjava/lang/Object;)V

    sget-object v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sLatinImeLogger:Lcom/android/inputmethod/latin/LatinImeLogger;

    iget-object v0, v0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLatinIME:Lcom/android/inputmethod/latin/LatinIME;

    sget-object v1, Lcom/android/inputmethod/latin/LatinImeLogger;->sLatinImeLogger:Lcom/android/inputmethod/latin/LatinImeLogger;

    iget-object v1, v1, Lcom/android/inputmethod/latin/LatinImeLogger;->mRingCharBuffer:Lcom/android/inputmethod/latin/Utils$RingCharBuffer;

    invoke-virtual {v1, v3}, Lcom/android/inputmethod/latin/Utils$RingCharBuffer;->getBackwardNthChar(I)C

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/LatinIME;->isWordSeparator(I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sLatinImeLogger:Lcom/android/inputmethod/latin/LatinImeLogger;

    sget-object v1, Lcom/android/inputmethod/latin/LatinImeLogger;->sLatinImeLogger:Lcom/android/inputmethod/latin/LatinImeLogger;

    iget-object v1, v1, Lcom/android/inputmethod/latin/LatinImeLogger;->mRingCharBuffer:Lcom/android/inputmethod/latin/Utils$RingCharBuffer;

    invoke-virtual {v1, v3}, Lcom/android/inputmethod/latin/Utils$RingCharBuffer;->getLastWord(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/inputmethod/latin/LatinImeLogger;->flushAutoCorrectionCancelledEntries(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static logOnInputSeparator()V
    .locals 3

    sget-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sLogEnabled:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sLatinImeLogger:Lcom/android/inputmethod/latin/LatinImeLogger;

    const/4 v1, 0x3

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/android/inputmethod/latin/LatinImeLogger;->sendLogToDropBox(ILjava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public static logOnManualSuggestion(Ljava/lang/String;Ljava/lang/String;ILcom/android/inputmethod/latin/SuggestedWords;)V
    .locals 22
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Lcom/android/inputmethod/latin/SuggestedWords;

    sget-boolean v17, Lcom/android/inputmethod/latin/LatinImeLogger;->sLogEnabled:Z

    if-eqz v17, :cond_1

    sget-object v17, Lcom/android/inputmethod/latin/LatinImeLogger;->sLatinImeLogger:Lcom/android/inputmethod/latin/LatinImeLogger;

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/android/inputmethod/latin/LatinImeLogger;->flushAutoCorrectionCancelledEntries(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v17

    if-nez v17, :cond_2

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v17

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_2

    sget-object v17, Lcom/android/inputmethod/latin/LatinImeLogger;->sLatinImeLogger:Lcom/android/inputmethod/latin/LatinImeLogger;

    const/16 v18, 0xf

    const/16 v19, 0x4

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    aput-object p0, v19, v20

    const/16 v20, 0x1

    aput-object p1, v19, v20

    const/16 v20, 0x2

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v21

    aput-object v21, v19, v20

    const/16 v20, 0x3

    const-string v21, ""

    aput-object v21, v19, v20

    invoke-direct/range {v17 .. v19}, Lcom/android/inputmethod/latin/LatinImeLogger;->sendLogToDropBox(ILjava/lang/Object;)V

    :cond_0
    :goto_0
    sget-object v17, Lcom/android/inputmethod/latin/LatinImeLogger;->sSuggestDicMap:Ljava/util/HashMap;

    invoke-virtual/range {v17 .. v17}, Ljava/util/HashMap;->clear()V

    :cond_1
    return-void

    :cond_2
    sget-object v17, Lcom/android/inputmethod/latin/LatinImeLogger;->sSuggestDicMap:Ljava/util/HashMap;

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_3

    sget-boolean v17, Lcom/android/inputmethod/latin/LatinImeLogger;->sDBG:Z

    if-eqz v17, :cond_0

    const-string v17, "LatinIMELogs"

    const-string v18, "logOnManualSuggestion was cancelled: from unknown dic."

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    sget-object v17, Lcom/android/inputmethod/latin/LatinImeLogger;->sSuggestDicMap:Ljava/util/HashMap;

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Landroid/util/Pair;

    move-object/from16 v0, v17

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v17, v0

    check-cast v17, Ljava/lang/Integer;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v11

    sget-object v17, Lcom/android/inputmethod/latin/LatinImeLogger;->sLatinImeLogger:Lcom/android/inputmethod/latin/LatinImeLogger;

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/android/inputmethod/latin/LatinImeLogger;->mManualSuggestCountPerDic:[I

    move-object/from16 v17, v0

    aget v18, v17, v11

    add-int/lit8 v18, v18, 0x1

    aput v18, v17, v11

    sget-boolean v17, Lcom/android/inputmethod/latin/LatinImeLogger;->sUsabilityStudy:Z

    if-eqz v17, :cond_4

    invoke-static {}, Lcom/android/inputmethod/latin/Utils$UsabilityStudyLogUtils;->getInstance()Lcom/android/inputmethod/latin/Utils$UsabilityStudyLogUtils;

    move-result-object v17

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "[Manual]"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "\t\t"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lcom/android/inputmethod/latin/Utils$UsabilityStudyLogUtils;->write(Ljava/lang/String;)V

    :cond_4
    sget-boolean v17, Lcom/android/inputmethod/latin/ResearchLogger;->sIsLogging:Z

    if-eqz v17, :cond_5

    invoke-static {}, Lcom/android/inputmethod/latin/ResearchLogger;->getInstance()Lcom/android/inputmethod/latin/ResearchLogger;

    move-result-object v17

    const-string v18, "[Manual]"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move/from16 v4, p2

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/inputmethod/latin/ResearchLogger;->logCorrection(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    :cond_5
    const/16 v17, 0x1

    move/from16 v0, v17

    if-eq v11, v0, :cond_7

    sget-boolean v17, Lcom/android/inputmethod/latin/LatinImeLogger;->sDBG:Z

    if-eqz v17, :cond_6

    const-string v17, "LatinIMELogs"

    const-string v18, "logOnManualSuggestion was cancelled: not from main dic."

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    const-string p0, ""

    const-string p1, ""

    const/16 v17, 0x0

    sput-object v17, Lcom/android/inputmethod/latin/LatinImeLogger;->sPreviousWords:[Ljava/lang/String;

    :cond_7
    sget-object v17, Lcom/android/inputmethod/latin/LatinImeLogger;->sPreviousWords:[Ljava/lang/String;

    if-nez v17, :cond_8

    const/4 v13, 0x0

    :goto_1
    invoke-virtual/range {p3 .. p3}, Lcom/android/inputmethod/latin/SuggestedWords;->size()I

    move-result v16

    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v8, 0x2

    const/4 v10, 0x3

    const/4 v9, 0x4

    const/4 v5, 0x5

    mul-int/lit8 v17, v16, 0x2

    add-int/lit8 v17, v17, 0x5

    add-int v17, v17, v13

    move/from16 v0, v17

    new-array v15, v0, [Ljava/lang/String;

    const/16 v17, 0x0

    aput-object p0, v15, v17

    const/16 v17, 0x1

    aput-object p1, v15, v17

    const/16 v17, 0x2

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v18

    aput-object v18, v15, v17

    const/16 v17, 0x3

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v18

    aput-object v18, v15, v17

    const/16 v17, 0x4

    invoke-static {v13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v18

    aput-object v18, v15, v17

    const/4 v12, 0x0

    :goto_2
    move/from16 v0, v16

    if-ge v12, v0, :cond_a

    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Lcom/android/inputmethod/latin/SuggestedWords;->getWord(I)Ljava/lang/CharSequence;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v14

    sget-object v17, Lcom/android/inputmethod/latin/LatinImeLogger;->sSuggestDicMap:Ljava/util/HashMap;

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_9

    add-int/lit8 v17, v12, 0x5

    aput-object v14, v15, v17

    add-int/lit8 v17, v16, 0x5

    add-int v18, v17, v12

    sget-object v17, Lcom/android/inputmethod/latin/LatinImeLogger;->sSuggestDicMap:Ljava/util/HashMap;

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Landroid/util/Pair;

    move-object/from16 v0, v17

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 v17, v0

    check-cast v17, Ljava/lang/Integer;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v18

    :goto_3
    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    :cond_8
    sget-object v17, Lcom/android/inputmethod/latin/LatinImeLogger;->sPreviousWords:[Ljava/lang/String;

    move-object/from16 v0, v17

    array-length v13, v0

    goto :goto_1

    :cond_9
    add-int/lit8 v17, v12, 0x5

    const-string v18, ""

    aput-object v18, v15, v17

    add-int/lit8 v17, v16, 0x5

    add-int v17, v17, v12

    const-string v18, ""

    aput-object v18, v15, v17

    goto :goto_3

    :cond_a
    const/4 v12, 0x0

    :goto_4
    if-ge v12, v13, :cond_b

    mul-int/lit8 v17, v16, 0x2

    add-int/lit8 v17, v17, 0x5

    add-int v17, v17, v12

    sget-object v18, Lcom/android/inputmethod/latin/LatinImeLogger;->sPreviousWords:[Ljava/lang/String;

    aget-object v18, v18, v12

    aput-object v18, v15, v17

    add-int/lit8 v12, v12, 0x1

    goto :goto_4

    :cond_b
    sget-object v17, Lcom/android/inputmethod/latin/LatinImeLogger;->sLatinImeLogger:Lcom/android/inputmethod/latin/LatinImeLogger;

    const/16 v18, 0xf

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-direct {v0, v1, v15}, Lcom/android/inputmethod/latin/LatinImeLogger;->sendLogToDropBox(ILjava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public static onAddSuggestedWord(Ljava/lang/String;II)V
    .locals 4
    .param p0    # Ljava/lang/String;
    .param p1    # I
    .param p2    # I

    sget-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sLogEnabled:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sSuggestDicMap:Ljava/util/HashMap;

    new-instance v1, Landroid/util/Pair;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public static onDestroy()V
    .locals 2

    sget-object v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sLatinImeLogger:Lcom/android/inputmethod/latin/LatinImeLogger;

    const-string v1, ""

    invoke-direct {v0, v1}, Lcom/android/inputmethod/latin/LatinImeLogger;->flushAutoCorrectionCancelledEntries(Ljava/lang/String;)V

    sget-object v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sLatinImeLogger:Lcom/android/inputmethod/latin/LatinImeLogger;

    invoke-direct {v0}, Lcom/android/inputmethod/latin/LatinImeLogger;->commitInternal()V

    sget-object v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sLatinImeLogger:Lcom/android/inputmethod/latin/LatinImeLogger;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/LatinImeLogger;->destroy()V

    return-void
.end method

.method public static onSetKeyboard(Lcom/android/inputmethod/keyboard/Keyboard;)V
    .locals 1
    .param p0    # Lcom/android/inputmethod/keyboard/Keyboard;

    sget-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sLogEnabled:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mOccupiedWidth:I

    sput v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sKeyboardWidth:I

    iget v0, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mOccupiedHeight:I

    sput v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sKeyboardHeight:I

    :cond_0
    return-void
.end method

.method public static onStartInputView(Landroid/view/inputmethod/EditorInfo;)V
    .locals 3
    .param p0    # Landroid/view/inputmethod/EditorInfo;

    sget-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sUsabilityStudy:Z

    if-eqz v0, :cond_0

    if-eqz p0, :cond_0

    invoke-static {}, Lcom/android/inputmethod/latin/Utils$UsabilityStudyLogUtils;->getInstance()Lcom/android/inputmethod/latin/Utils$UsabilityStudyLogUtils;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[onStartInputView]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/view/inputmethod/EditorInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Landroid/view/inputmethod/EditorInfo;->fieldId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Landroid/view/inputmethod/EditorInfo;->inputType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/Utils$UsabilityStudyLogUtils;->write(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static onStartSuggestion(Ljava/lang/CharSequence;)V
    .locals 3
    .param p0    # Ljava/lang/CharSequence;

    sget-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sLogEnabled:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sSuggestDicMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    if-nez p0, :cond_1

    const-string v0, ""

    :goto_0
    aput-object v0, v1, v2

    sput-object v1, Lcom/android/inputmethod/latin/LatinImeLogger;->sPreviousWords:[Ljava/lang/String;

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private reset()V
    .locals 2

    const/4 v1, 0x0

    iput v1, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mDeleteCount:I

    iput v1, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mInputCount:I

    iput v1, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mWordCount:I

    iput v1, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mActualCharCount:I

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mAutoCorrectionCountPerDic:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mManualSuggestCountPerDic:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mAutoCancelledCountPerDic:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLogBuffer:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mPrivacyLogBuffer:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method private sendLogToDropBox(ILjava/lang/Object;)V
    .locals 15
    .param p1    # I
    .param p2    # Ljava/lang/Object;

    iget-object v9, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLogBuffer:Ljava/util/ArrayList;

    monitor-enter v9

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sget-boolean v8, Lcom/android/inputmethod/latin/LatinImeLogger;->sDBG:Z

    if-eqz v8, :cond_2

    const-string v6, ""

    move-object/from16 v0, p2

    instance-of v8, v0, [Ljava/lang/String;

    if-eqz v8, :cond_0

    move-object/from16 v0, p2

    check-cast v0, [Ljava/lang/String;

    move-object v8, v0

    move-object v0, v8

    check-cast v0, [Ljava/lang/String;

    move-object v1, v0

    array-length v3, v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v7, v1, v2

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, ","

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    move-object/from16 v0, p2

    instance-of v8, v0, Ljava/lang/Integer;

    if-eqz v8, :cond_1

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p2

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    :cond_1
    const-string v8, "LatinIMELogs"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "SendLog: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, p1

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ";"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " -> will be sent after "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-wide v11, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLastTimeSend:J

    sub-long v11, v4, v11

    const-wide/32 v13, 0x493e0

    sub-long/2addr v11, v13

    neg-long v11, v11

    const-wide/16 v13, 0x3e8

    div-long/2addr v11, v13

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " sec."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-wide v10, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLastTimeActive:J

    sub-long v10, v4, v10

    const-wide/32 v12, 0x493e0

    cmp-long v8, v10, v12

    if-lez v8, :cond_3

    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinImeLogger;->commitInternal()V

    invoke-direct/range {p0 .. p2}, Lcom/android/inputmethod/latin/LatinImeLogger;->addData(ILjava/lang/Object;)V

    :goto_1
    iput-wide v4, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLastTimeActive:J

    monitor-exit v9

    return-void

    :cond_3
    iget-wide v10, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLastTimeSend:J

    sub-long v10, v4, v10

    const-wide/32 v12, 0x493e0

    cmp-long v8, v10, v12

    if-lez v8, :cond_4

    invoke-direct/range {p0 .. p2}, Lcom/android/inputmethod/latin/LatinImeLogger;->addData(ILjava/lang/Object;)V

    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinImeLogger;->commitInternal()V

    goto :goto_1

    :catchall_0
    move-exception v8

    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v8

    :cond_4
    :try_start_1
    invoke-direct/range {p0 .. p2}, Lcom/android/inputmethod/latin/LatinImeLogger;->addData(ILjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method


# virtual methods
.method public destroy()V
    .locals 2

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mAddTextToDropBoxTask:Lcom/android/inputmethod/latin/LatinImeLogger$AddTextToDropBoxTask;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/inputmethod/latin/Utils;->cancelTask(Landroid/os/AsyncTask;Z)V

    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 3
    .param p1    # Landroid/content/SharedPreferences;
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x1

    const/4 v1, 0x0

    const-string v2, "enable_logging"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {p1, p2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    sget-boolean v2, Lcom/android/inputmethod/latin/LatinImeLogger;->sUsabilityStudy:Z

    if-eqz v2, :cond_3

    :cond_0
    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLatinIME:Lcom/android/inputmethod/latin/LatinIME;

    if-eqz v2, :cond_2

    :goto_0
    sput-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sLogEnabled:Z

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLatinIME:Lcom/android/inputmethod/latin/LatinIME;

    sget-boolean v1, Lcom/android/inputmethod/latin/LatinImeLogger;->sLogEnabled:Z

    sget-boolean v2, Lcom/android/inputmethod/latin/LatinImeLogger;->sUsabilityStudy:Z

    invoke-static {v0, v1, v2}, Lcom/android/inputmethod/latin/Utils$RingCharBuffer;->init(Landroid/inputmethodservice/InputMethodService;ZZ)Lcom/android/inputmethod/latin/Utils$RingCharBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mRingCharBuffer:Lcom/android/inputmethod/latin/Utils$RingCharBuffer;

    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    sput-boolean v1, Lcom/android/inputmethod/latin/LatinImeLogger;->sLogEnabled:Z

    goto :goto_1

    :cond_4
    const-string v2, "pref_keyboard_layout_20110916"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v0, "pref_keyboard_layout_20110916"

    sget-object v1, Lcom/android/inputmethod/latin/LatinImeLogger;->sConfigDefaultKeyboardThemeIndex:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mThemeId:Ljava/lang/String;

    iget-wide v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLastTimeActive:J

    invoke-direct {p0, v0, v1}, Lcom/android/inputmethod/latin/LatinImeLogger;->addThemeIdEntry(J)V

    goto :goto_1

    :cond_5
    const-string v2, "debug_mode"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v0, "debug_mode"

    sget-boolean v1, Lcom/android/inputmethod/latin/LatinImeLogger;->sPRINTLOGGING:Z

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sPRINTLOGGING:Z

    sget-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sPRINTLOGGING:Z

    sput-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sDBG:Z

    goto :goto_1

    :cond_6
    const-string v2, "input_language"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    const-string v0, "input_language"

    const-string v1, ""

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mCurrentLanguage:Ljava/lang/String;

    iget-wide v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLastTimeActive:J

    invoke-direct {p0, v0, v1}, Lcom/android/inputmethod/latin/LatinImeLogger;->addLanguagesEntry(J)V

    goto :goto_1

    :cond_7
    const-string v2, "input_language"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    const-string v0, "selected_languages"

    const-string v1, ""

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mSelectedLanguages:Ljava/lang/String;

    goto :goto_1

    :cond_8
    const-string v2, "auto_cap"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    sget-object v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sDebugKeyEnabler:Lcom/android/inputmethod/latin/LatinImeLogger$DebugKeyEnabler;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/LatinImeLogger$DebugKeyEnabler;->check()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLatinIME:Lcom/android/inputmethod/latin/LatinIME;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/LatinIME;->launchDebugSettings()V

    goto :goto_1

    :cond_9
    const-string v2, "usability_study_mode"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "usability_study_mode"

    invoke-interface {p1, v2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    sput-boolean v2, Lcom/android/inputmethod/latin/LatinImeLogger;->sUsabilityStudy:Z

    invoke-interface {p1, p2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_a

    sget-boolean v2, Lcom/android/inputmethod/latin/LatinImeLogger;->sUsabilityStudy:Z

    if-eqz v2, :cond_b

    :cond_a
    move v1, v0

    :cond_b
    sput-boolean v1, Lcom/android/inputmethod/latin/LatinImeLogger;->sLogEnabled:Z

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mLatinIME:Lcom/android/inputmethod/latin/LatinIME;

    sget-boolean v1, Lcom/android/inputmethod/latin/LatinImeLogger;->sLogEnabled:Z

    sget-boolean v2, Lcom/android/inputmethod/latin/LatinImeLogger;->sUsabilityStudy:Z

    invoke-static {v0, v1, v2}, Lcom/android/inputmethod/latin/Utils$RingCharBuffer;->init(Landroid/inputmethodservice/InputMethodService;ZZ)Lcom/android/inputmethod/latin/Utils$RingCharBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinImeLogger;->mRingCharBuffer:Lcom/android/inputmethod/latin/Utils$RingCharBuffer;

    goto/16 :goto_1
.end method
