.class public Lcom/android/inputmethod/latin/LogEntries$AutoCorrectionCancelledEntry;
.super Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;
.source "LogEntries.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/latin/LogEntries;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AutoCorrectionCancelledEntry"
.end annotation


# instance fields
.field private final mAfter:Ljava/lang/String;

.field private final mBefore:Ljava/lang/String;

.field private final mDatatype:I

.field private final mHeight:I

.field private mPreferableWord:Ljava/lang/String;

.field private final mSeparator:Ljava/lang/String;

.field private final mWidth:I

.field private final mXCoordinates:[I

.field private final mYCoordinates:[I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III[I[I)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # [I
    .param p8    # [I

    const/4 v6, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const/16 v4, 0x13

    const/4 v5, 0x0

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;-><init>(JI[Ljava/lang/String;)V

    const-string v2, ""

    iput-object v2, p0, Lcom/android/inputmethod/latin/LogEntries$AutoCorrectionCancelledEntry;->mPreferableWord:Ljava/lang/String;

    sget-boolean v2, Lcom/android/inputmethod/latin/LatinImeLogger;->sDBG:Z

    if-eqz v2, :cond_0

    # getter for: Lcom/android/inputmethod/latin/LogEntries;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/inputmethod/latin/LogEntries;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "AutoCorrectionCancelledEntry:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iput-object p1, p0, Lcom/android/inputmethod/latin/LogEntries$AutoCorrectionCancelledEntry;->mBefore:Ljava/lang/String;

    iput-object p2, p0, Lcom/android/inputmethod/latin/LogEntries$AutoCorrectionCancelledEntry;->mAfter:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/inputmethod/latin/LogEntries$AutoCorrectionCancelledEntry;->mSeparator:Ljava/lang/String;

    iput p4, p0, Lcom/android/inputmethod/latin/LogEntries$AutoCorrectionCancelledEntry;->mDatatype:I

    iput p5, p0, Lcom/android/inputmethod/latin/LogEntries$AutoCorrectionCancelledEntry;->mWidth:I

    iput p6, p0, Lcom/android/inputmethod/latin/LogEntries$AutoCorrectionCancelledEntry;->mHeight:I

    array-length v0, p7

    array-length v1, p8

    new-array v2, v0, [I

    iput-object v2, p0, Lcom/android/inputmethod/latin/LogEntries$AutoCorrectionCancelledEntry;->mXCoordinates:[I

    new-array v2, v1, [I

    iput-object v2, p0, Lcom/android/inputmethod/latin/LogEntries$AutoCorrectionCancelledEntry;->mYCoordinates:[I

    iget-object v2, p0, Lcom/android/inputmethod/latin/LogEntries$AutoCorrectionCancelledEntry;->mXCoordinates:[I

    invoke-static {p7, v6, v2, v6, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/android/inputmethod/latin/LogEntries$AutoCorrectionCancelledEntry;->mYCoordinates:[I

    invoke-static {p8, v6, v2, v6, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void
.end method


# virtual methods
.method public getLogStrings()[Ljava/lang/String;
    .locals 13

    const/4 v11, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v7, 0x2

    const/4 v3, 0x3

    const/4 v5, 0x4

    const/4 v4, 0x5

    const/4 v6, 0x6

    const/4 v0, 0x7

    iget-object v12, p0, Lcom/android/inputmethod/latin/LogEntries$AutoCorrectionCancelledEntry;->mXCoordinates:[I

    if-nez v12, :cond_0

    move v10, v11

    :goto_0
    mul-int/lit8 v12, v10, 0x2

    add-int/lit8 v12, v12, 0x7

    new-array v9, v12, [Ljava/lang/String;

    iget-object v12, p0, Lcom/android/inputmethod/latin/LogEntries$AutoCorrectionCancelledEntry;->mBefore:Ljava/lang/String;

    aput-object v12, v9, v11

    const/4 v11, 0x1

    iget-object v12, p0, Lcom/android/inputmethod/latin/LogEntries$AutoCorrectionCancelledEntry;->mAfter:Ljava/lang/String;

    aput-object v12, v9, v11

    const/4 v11, 0x2

    iget-object v12, p0, Lcom/android/inputmethod/latin/LogEntries$AutoCorrectionCancelledEntry;->mSeparator:Ljava/lang/String;

    aput-object v12, v9, v11

    const/4 v11, 0x3

    iget v12, p0, Lcom/android/inputmethod/latin/LogEntries$AutoCorrectionCancelledEntry;->mDatatype:I

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v9, v11

    const/4 v11, 0x4

    iget v12, p0, Lcom/android/inputmethod/latin/LogEntries$AutoCorrectionCancelledEntry;->mWidth:I

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v9, v11

    const/4 v11, 0x5

    iget v12, p0, Lcom/android/inputmethod/latin/LogEntries$AutoCorrectionCancelledEntry;->mHeight:I

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v9, v11

    const/4 v11, 0x6

    iget-object v12, p0, Lcom/android/inputmethod/latin/LogEntries$AutoCorrectionCancelledEntry;->mPreferableWord:Ljava/lang/String;

    aput-object v12, v9, v11

    const/4 v8, 0x0

    :goto_1
    if-ge v8, v10, :cond_1

    mul-int/lit8 v11, v8, 0x2

    add-int/lit8 v11, v11, 0x7

    iget-object v12, p0, Lcom/android/inputmethod/latin/LogEntries$AutoCorrectionCancelledEntry;->mXCoordinates:[I

    aget v12, v12, v8

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v9, v11

    mul-int/lit8 v11, v8, 0x2

    add-int/lit8 v11, v11, 0x7

    add-int/lit8 v11, v11, 0x1

    iget-object v12, p0, Lcom/android/inputmethod/latin/LogEntries$AutoCorrectionCancelledEntry;->mYCoordinates:[I

    aget v12, v12, v8

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v9, v11

    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_0
    iget-object v12, p0, Lcom/android/inputmethod/latin/LogEntries$AutoCorrectionCancelledEntry;->mXCoordinates:[I

    array-length v10, v12

    goto :goto_0

    :cond_1
    return-object v9
.end method

.method public setPreferableWord(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/inputmethod/latin/LogEntries$AutoCorrectionCancelledEntry;->mPreferableWord:Ljava/lang/String;

    return-void
.end method
