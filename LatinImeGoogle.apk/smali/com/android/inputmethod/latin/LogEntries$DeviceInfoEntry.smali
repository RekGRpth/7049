.class public Lcom/android/inputmethod/latin/LogEntries$DeviceInfoEntry;
.super Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;
.source "LogEntries.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/latin/LogEntries;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DeviceInfoEntry"
.end annotation


# static fields
.field private static final MODEL:Ljava/lang/String;

.field private static sInstance:Lcom/android/inputmethod/latin/LogEntries$DeviceInfoEntry;


# instance fields
.field private final mDensity:F

.field private final mDensityDpi:I

.field private final mHeightPixels:I

.field private final mScaledDensity:F

.field private final mScreenLayout:I

.field private final mScreenMetrics:I

.field private final mWidthPixels:I

.field private final mXdpi:F

.field private final mYdpi:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    sput-object v0, Lcom/android/inputmethod/latin/LogEntries$DeviceInfoEntry;->MODEL:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(JLandroid/content/Context;)V
    .locals 3
    .param p1    # J
    .param p3    # Landroid/content/Context;

    const/16 v1, 0x14

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, v1, v2}, Lcom/android/inputmethod/latin/LatinImeLogger$LogEntry;-><init>(JI[Ljava/lang/String;)V

    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    const-string v1, "window"

    invoke-virtual {p3, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v1, v0, Landroid/util/DisplayMetrics;->density:F

    iput v1, p0, Lcom/android/inputmethod/latin/LogEntries$DeviceInfoEntry;->mDensity:F

    iget v1, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    iput v1, p0, Lcom/android/inputmethod/latin/LogEntries$DeviceInfoEntry;->mDensityDpi:I

    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v1, p0, Lcom/android/inputmethod/latin/LogEntries$DeviceInfoEntry;->mHeightPixels:I

    iget v1, v0, Landroid/util/DisplayMetrics;->scaledDensity:F

    iput v1, p0, Lcom/android/inputmethod/latin/LogEntries$DeviceInfoEntry;->mScaledDensity:F

    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v1, p0, Lcom/android/inputmethod/latin/LogEntries$DeviceInfoEntry;->mWidthPixels:I

    iget v1, v0, Landroid/util/DisplayMetrics;->xdpi:F

    iput v1, p0, Lcom/android/inputmethod/latin/LogEntries$DeviceInfoEntry;->mXdpi:F

    iget v1, v0, Landroid/util/DisplayMetrics;->ydpi:F

    iput v1, p0, Lcom/android/inputmethod/latin/LogEntries$DeviceInfoEntry;->mYdpi:F

    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090011

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/android/inputmethod/latin/LogEntries$DeviceInfoEntry;->mScreenMetrics:I

    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->screenLayout:I

    iput v1, p0, Lcom/android/inputmethod/latin/LogEntries$DeviceInfoEntry;->mScreenLayout:I

    return-void
.end method

.method public static declared-synchronized getInstance(JLandroid/content/Context;)Lcom/android/inputmethod/latin/LogEntries$DeviceInfoEntry;
    .locals 2
    .param p0    # J
    .param p2    # Landroid/content/Context;

    const-class v1, Lcom/android/inputmethod/latin/LogEntries$DeviceInfoEntry;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/inputmethod/latin/LogEntries$DeviceInfoEntry;->sInstance:Lcom/android/inputmethod/latin/LogEntries$DeviceInfoEntry;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/inputmethod/latin/LogEntries$DeviceInfoEntry;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/inputmethod/latin/LogEntries$DeviceInfoEntry;-><init>(JLandroid/content/Context;)V

    sput-object v0, Lcom/android/inputmethod/latin/LogEntries$DeviceInfoEntry;->sInstance:Lcom/android/inputmethod/latin/LogEntries$DeviceInfoEntry;

    :cond_0
    sget-object v0, Lcom/android/inputmethod/latin/LogEntries$DeviceInfoEntry;->sInstance:Lcom/android/inputmethod/latin/LogEntries$DeviceInfoEntry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public getLogStrings()[Ljava/lang/String;
    .locals 8

    const/16 v5, 0xa

    new-array v4, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    sget-object v6, Lcom/android/inputmethod/latin/LogEntries$DeviceInfoEntry;->MODEL:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget v6, p0, Lcom/android/inputmethod/latin/LogEntries$DeviceInfoEntry;->mDensity:F

    invoke-static {v6}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    iget v6, p0, Lcom/android/inputmethod/latin/LogEntries$DeviceInfoEntry;->mDensityDpi:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    iget v6, p0, Lcom/android/inputmethod/latin/LogEntries$DeviceInfoEntry;->mHeightPixels:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x4

    iget v6, p0, Lcom/android/inputmethod/latin/LogEntries$DeviceInfoEntry;->mScaledDensity:F

    invoke-static {v6}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x5

    iget v6, p0, Lcom/android/inputmethod/latin/LogEntries$DeviceInfoEntry;->mWidthPixels:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x6

    iget v6, p0, Lcom/android/inputmethod/latin/LogEntries$DeviceInfoEntry;->mXdpi:F

    invoke-static {v6}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x7

    iget v6, p0, Lcom/android/inputmethod/latin/LogEntries$DeviceInfoEntry;->mYdpi:F

    invoke-static {v6}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0x8

    iget v6, p0, Lcom/android/inputmethod/latin/LogEntries$DeviceInfoEntry;->mScreenMetrics:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0x9

    iget v6, p0, Lcom/android/inputmethod/latin/LogEntries$DeviceInfoEntry;->mScreenLayout:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    sget-boolean v5, Lcom/android/inputmethod/latin/LatinImeLogger;->sDBG:Z

    if-eqz v5, :cond_0

    move-object v0, v4

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    # getter for: Lcom/android/inputmethod/latin/LogEntries;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/inputmethod/latin/LogEntries;->access$000()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "DeviceInfo: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v4
.end method
