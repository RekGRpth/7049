.class public Lcom/android/inputmethod/latin/LogEntries;
.super Ljava/lang/Object;
.source "LogEntries.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/inputmethod/latin/LogEntries$DeviceInfoEntry;,
        Lcom/android/inputmethod/latin/LogEntries$AutoCorrectionCancelledEntry;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/inputmethod/latin/LogEntries;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/inputmethod/latin/LogEntries;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/inputmethod/latin/LogEntries;->TAG:Ljava/lang/String;

    return-object v0
.end method
