.class public final Lcom/android/inputmethod/latin/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/latin/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final Keyboard:[I

.field public static final KeyboardLayoutSet_Element:[I

.field public static final KeyboardTheme:[I

.field public static final KeyboardView:[I

.field public static final Keyboard_Case:[I

.field public static final Keyboard_Include:[I

.field public static final Keyboard_Key:[I

.field public static final Keyboard_KeyStyle:[I

.field public static final LatinKeyboardView:[I

.field public static final SuggestionsView:[I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/16 v4, 0x13

    const/16 v3, 0xe

    const/16 v0, 0x1c

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/inputmethod/latin/R$styleable;->Keyboard:[I

    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/inputmethod/latin/R$styleable;->KeyboardLayoutSet_Element:[I

    const/16 v0, 0xc

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/android/inputmethod/latin/R$styleable;->KeyboardTheme:[I

    const/16 v0, 0x23

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/android/inputmethod/latin/R$styleable;->KeyboardView:[I

    new-array v0, v3, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/android/inputmethod/latin/R$styleable;->Keyboard_Case:[I

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f01007f

    aput v2, v0, v1

    sput-object v0, Lcom/android/inputmethod/latin/R$styleable;->Keyboard_Include:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/android/inputmethod/latin/R$styleable;->Keyboard_Key:[I

    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_6

    sput-object v0, Lcom/android/inputmethod/latin/R$styleable;->Keyboard_KeyStyle:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_7

    sput-object v0, Lcom/android/inputmethod/latin/R$styleable;->LatinKeyboardView:[I

    new-array v0, v3, [I

    fill-array-data v0, :array_8

    sput-object v0, Lcom/android/inputmethod/latin/R$styleable;->SuggestionsView:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f010050
        0x7f010051
        0x7f010052
        0x7f010053
        0x7f010054
        0x7f010055
        0x7f010056
        0x7f010057
        0x7f010058
        0x7f010059
        0x7f01005a
        0x7f01005b
        0x7f01005c
        0x7f01005d
        0x7f01005e
        0x7f01005f
        0x7f010060
        0x7f010061
        0x7f010062
        0x7f010063
        0x7f010064
        0x7f010065
        0x7f010066
        0x7f010067
        0x7f010068
        0x7f010069
        0x7f01006a
        0x7f01006b
    .end array-data

    :array_1
    .array-data 4
        0x7f010090
        0x7f010091
        0x7f010092
    .end array-data

    :array_2
    .array-data 4
        0x7f010000
        0x7f010001
        0x7f010002
        0x7f010003
        0x7f010004
        0x7f010005
        0x7f010006
        0x7f010007
        0x7f010008
        0x7f010009
        0x7f01000a
        0x7f01000b
    .end array-data

    :array_3
    .array-data 4
        0x7f01000c
        0x7f01000d
        0x7f01000e
        0x7f01000f
        0x7f010010
        0x7f010011
        0x7f010012
        0x7f010013
        0x7f010014
        0x7f010015
        0x7f010016
        0x7f010017
        0x7f010018
        0x7f010019
        0x7f01001a
        0x7f01001b
        0x7f01001c
        0x7f01001d
        0x7f01001e
        0x7f01001f
        0x7f010020
        0x7f010021
        0x7f010022
        0x7f010023
        0x7f010024
        0x7f010025
        0x7f010026
        0x7f010027
        0x7f010028
        0x7f010029
        0x7f01002a
        0x7f01002b
        0x7f01002c
        0x7f01002d
        0x7f01002e
    .end array-data

    :array_4
    .array-data 4
        0x7f010080
        0x7f010081
        0x7f010082
        0x7f010083
        0x7f010084
        0x7f010085
        0x7f010086
        0x7f010087
        0x7f010088
        0x7f010089
        0x7f01008a
        0x7f01008b
        0x7f01008c
        0x7f01008d
    .end array-data

    :array_5
    .array-data 4
        0x7f01006c
        0x7f01006d
        0x7f01006e
        0x7f01006f
        0x7f010070
        0x7f010071
        0x7f010072
        0x7f010073
        0x7f010074
        0x7f010075
        0x7f010076
        0x7f010077
        0x7f010078
        0x7f010079
        0x7f01007a
        0x7f01007b
        0x7f01007c
        0x7f01007d
        0x7f01007e
    .end array-data

    :array_6
    .array-data 4
        0x7f01008e
        0x7f01008f
    .end array-data

    :array_7
    .array-data 4
        0x7f01002f
        0x7f010030
        0x7f010031
        0x7f010032
        0x7f010033
        0x7f010034
        0x7f010035
        0x7f010036
        0x7f010037
        0x7f010038
        0x7f010039
        0x7f01003a
        0x7f01003b
        0x7f01003c
        0x7f01003d
        0x7f01003e
        0x7f01003f
        0x7f010040
        0x7f010041
    .end array-data

    :array_8
    .array-data 4
        0x7f010042
        0x7f010043
        0x7f010044
        0x7f010045
        0x7f010046
        0x7f010047
        0x7f010048
        0x7f010049
        0x7f01004a
        0x7f01004b
        0x7f01004c
        0x7f01004d
        0x7f01004e
        0x7f01004f
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
