.class Lcom/android/inputmethod/latin/ResearchLogger$1;
.super Ljava/lang/Object;
.source "ResearchLogger.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/inputmethod/latin/ResearchLogger;->write(Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/inputmethod/latin/ResearchLogger;

.field final synthetic val$log:Ljava/lang/String;

.field final synthetic val$logGroup:Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;


# direct methods
.method constructor <init>(Lcom/android/inputmethod/latin/ResearchLogger;Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/inputmethod/latin/ResearchLogger$1;->this$0:Lcom/android/inputmethod/latin/ResearchLogger;

    iput-object p2, p0, Lcom/android/inputmethod/latin/ResearchLogger$1;->val$logGroup:Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;

    iput-object p3, p0, Lcom/android/inputmethod/latin/ResearchLogger$1;->val$log:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    const/16 v8, 0x9

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v7, p0, Lcom/android/inputmethod/latin/ResearchLogger$1;->val$logGroup:Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;

    # getter for: Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;->mLogString:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;->access$100(Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v7, p0, Lcom/android/inputmethod/latin/ResearchLogger$1;->val$log:Ljava/lang/String;

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v7, 0xa

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v7, p0, Lcom/android/inputmethod/latin/ResearchLogger$1;->this$0:Lcom/android/inputmethod/latin/ResearchLogger;

    iget-object v7, v7, Lcom/android/inputmethod/latin/ResearchLogger;->mLogFileManager:Lcom/android/inputmethod/latin/ResearchLogger$LogFileManager;

    invoke-virtual {v7, v4}, Lcom/android/inputmethod/latin/ResearchLogger$LogFileManager;->append(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v7, p0, Lcom/android/inputmethod/latin/ResearchLogger$1;->this$0:Lcom/android/inputmethod/latin/ResearchLogger;

    iget-object v7, v7, Lcom/android/inputmethod/latin/ResearchLogger;->mLogFileManager:Lcom/android/inputmethod/latin/ResearchLogger$LogFileManager;

    iget-object v8, p0, Lcom/android/inputmethod/latin/ResearchLogger$1;->this$0:Lcom/android/inputmethod/latin/ResearchLogger;

    # getter for: Lcom/android/inputmethod/latin/ResearchLogger;->mIms:Landroid/inputmethodservice/InputMethodService;
    invoke-static {v8}, Lcom/android/inputmethod/latin/ResearchLogger;->access$200(Lcom/android/inputmethod/latin/ResearchLogger;)Landroid/inputmethodservice/InputMethodService;

    move-result-object v8

    invoke-static {v8}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/android/inputmethod/latin/ResearchLogger$LogFileManager;->createLogFile(Landroid/content/SharedPreferences;)V

    iget-object v7, p0, Lcom/android/inputmethod/latin/ResearchLogger$1;->this$0:Lcom/android/inputmethod/latin/ResearchLogger;

    iget-object v7, v7, Lcom/android/inputmethod/latin/ResearchLogger;->mLogFileManager:Lcom/android/inputmethod/latin/ResearchLogger$LogFileManager;

    invoke-virtual {v7, v4}, Lcom/android/inputmethod/latin/ResearchLogger$LogFileManager;->append(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method
