.class final enum Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;
.super Ljava/lang/Enum;
.source "ResearchLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/latin/ResearchLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "LogGroup"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;

.field public static final enum CORRECTION:Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;

.field public static final enum KEY:Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;

.field public static final enum MOTION_EVENT:Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;

.field public static final enum STATE_CHANGE:Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;

.field public static final enum UNSTRUCTURED:Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;


# instance fields
.field private final mLogString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;

    const-string v1, "MOTION_EVENT"

    const-string v2, "m"

    invoke-direct {v0, v1, v3, v2}, Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;->MOTION_EVENT:Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;

    new-instance v0, Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;

    const-string v1, "KEY"

    const-string v2, "k"

    invoke-direct {v0, v1, v4, v2}, Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;->KEY:Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;

    new-instance v0, Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;

    const-string v1, "CORRECTION"

    const-string v2, "c"

    invoke-direct {v0, v1, v5, v2}, Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;->CORRECTION:Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;

    new-instance v0, Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;

    const-string v1, "STATE_CHANGE"

    const-string v2, "s"

    invoke-direct {v0, v1, v6, v2}, Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;->STATE_CHANGE:Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;

    new-instance v0, Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;

    const-string v1, "UNSTRUCTURED"

    const-string v2, "u"

    invoke-direct {v0, v1, v7, v2}, Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;->UNSTRUCTURED:Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;

    sget-object v1, Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;->MOTION_EVENT:Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;->KEY:Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;->CORRECTION:Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;

    aput-object v1, v0, v5

    sget-object v1, Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;->STATE_CHANGE:Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;

    aput-object v1, v0, v6

    sget-object v1, Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;->UNSTRUCTURED:Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;

    aput-object v1, v0, v7

    sput-object v0, Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;->$VALUES:[Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;->mLogString:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$100(Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;

    iget-object v0, p0, Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;->mLogString:Ljava/lang/String;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;
    .locals 1

    const-class v0, Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;

    return-object v0
.end method

.method public static values()[Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;
    .locals 1

    sget-object v0, Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;->$VALUES:[Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;

    invoke-virtual {v0}, [Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;

    return-object v0
.end method
