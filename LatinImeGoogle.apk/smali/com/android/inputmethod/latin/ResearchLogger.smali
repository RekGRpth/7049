.class public Lcom/android/inputmethod/latin/ResearchLogger;
.super Ljava/lang/Object;
.source "ResearchLogger.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;,
        Lcom/android/inputmethod/latin/ResearchLogger$LogFileManager;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static final sInstance:Lcom/android/inputmethod/latin/ResearchLogger;

.field public static sIsLogging:Z


# instance fields
.field private mIms:Landroid/inputmethodservice/InputMethodService;

.field mLogFileManager:Lcom/android/inputmethod/latin/ResearchLogger$LogFileManager;

.field final mLoggingHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lcom/android/inputmethod/latin/ResearchLogger;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/inputmethod/latin/ResearchLogger;->TAG:Ljava/lang/String;

    new-instance v0, Lcom/android/inputmethod/latin/ResearchLogger;

    new-instance v1, Lcom/android/inputmethod/latin/ResearchLogger$LogFileManager;

    invoke-direct {v1}, Lcom/android/inputmethod/latin/ResearchLogger$LogFileManager;-><init>()V

    invoke-direct {v0, v1}, Lcom/android/inputmethod/latin/ResearchLogger;-><init>(Lcom/android/inputmethod/latin/ResearchLogger$LogFileManager;)V

    sput-object v0, Lcom/android/inputmethod/latin/ResearchLogger;->sInstance:Lcom/android/inputmethod/latin/ResearchLogger;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/inputmethod/latin/ResearchLogger;->sIsLogging:Z

    return-void
.end method

.method private constructor <init>(Lcom/android/inputmethod/latin/ResearchLogger$LogFileManager;)V
    .locals 3
    .param p1    # Lcom/android/inputmethod/latin/ResearchLogger$LogFileManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "ResearchLogger logging task"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/android/inputmethod/latin/ResearchLogger;->mLoggingHandler:Landroid/os/Handler;

    iput-object p1, p0, Lcom/android/inputmethod/latin/ResearchLogger;->mLogFileManager:Lcom/android/inputmethod/latin/ResearchLogger$LogFileManager;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/inputmethod/latin/ResearchLogger;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/inputmethod/latin/ResearchLogger;)Landroid/inputmethodservice/InputMethodService;
    .locals 1
    .param p0    # Lcom/android/inputmethod/latin/ResearchLogger;

    iget-object v0, p0, Lcom/android/inputmethod/latin/ResearchLogger;->mIms:Landroid/inputmethodservice/InputMethodService;

    return-object v0
.end method

.method public static getInstance()Lcom/android/inputmethod/latin/ResearchLogger;
    .locals 1

    sget-object v0, Lcom/android/inputmethod/latin/ResearchLogger;->sInstance:Lcom/android/inputmethod/latin/ResearchLogger;

    return-object v0
.end method

.method private write(Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;Ljava/lang/String;)V
    .locals 2
    .param p1    # Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/inputmethod/latin/ResearchLogger;->mLoggingHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/inputmethod/latin/ResearchLogger$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/inputmethod/latin/ResearchLogger$1;-><init>(Lcom/android/inputmethod/latin/ResearchLogger;Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method


# virtual methods
.method public clearAll()V
    .locals 2

    iget-object v0, p0, Lcom/android/inputmethod/latin/ResearchLogger;->mLoggingHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/inputmethod/latin/ResearchLogger$2;

    invoke-direct {v1, p0}, Lcom/android/inputmethod/latin/ResearchLogger$2;-><init>(Lcom/android/inputmethod/latin/ResearchLogger;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method getLogFileManager()Lcom/android/inputmethod/latin/ResearchLogger$LogFileManager;
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/ResearchLogger;->mLogFileManager:Lcom/android/inputmethod/latin/ResearchLogger$LogFileManager;

    return-object v0
.end method

.method public logCorrection(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # I

    const/16 v1, 0x9

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    sget-object v1, Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;->CORRECTION:Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/android/inputmethod/latin/ResearchLogger;->write(Lcom/android/inputmethod/latin/ResearchLogger$LogGroup;Ljava/lang/String;)V

    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/content/SharedPreferences;
    .param p2    # Ljava/lang/String;

    if-eqz p2, :cond_0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "usability_study_mode"

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/inputmethod/latin/ResearchLogger;->sIsLogging:Z

    goto :goto_0
.end method
