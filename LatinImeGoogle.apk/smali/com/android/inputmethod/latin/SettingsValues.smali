.class public Lcom/android/inputmethod/latin/SettingsValues;
.super Ljava/lang/Object;
.source "SettingsValues.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mAdditionalSubtypes:[Landroid/view/inputmethod/InputMethodSubtype;

.field public final mAutoCap:Z

.field public final mAutoCorrectEnabled:Z

.field public final mAutoCorrectionThreshold:F

.field private final mAutoCorrectionThresholdRawValue:Ljava/lang/String;

.field public final mBigramPredictionEnabled:Z

.field public final mBigramSuggestionEnabled:Z

.field public final mDelayUpdateOldSuggestions:I

.field public final mEnableSuggestionSpanInsertion:Z

.field public final mFxVolume:F

.field public final mHintToSaveText:Ljava/lang/CharSequence;

.field public final mIncludesOtherImesInLanguageSwitchList:Z

.field public final mIsLanguageSwitchKeySuppressed:Z

.field public final mKeyPreviewPopupDismissDelay:I

.field private final mKeyPreviewPopupDismissDelayRawValue:Ljava/lang/String;

.field public final mKeyPreviewPopupOn:Z

.field private final mKeypressSoundVolumeRawValue:F

.field public final mKeypressVibrationDuration:I

.field private final mPhantomSpacePromotingSymbols:Ljava/lang/String;

.field public final mShowSuggestionsSetting:Ljava/lang/String;

.field public final mSoundOn:Z

.field public final mSuggestPuncList:Lcom/android/inputmethod/latin/SuggestedWords;

.field private final mSymbolsExcludedFromWordSeparators:Ljava/lang/String;

.field private final mUsabilityStudyMode:Z

.field public final mUseContactsDict:Z

.field public final mVibrateOn:Z

.field private final mVibrationDurationSettingsRawValue:I

.field private final mVoiceKeyEnabled:Z

.field private final mVoiceKeyOnMain:Z

.field private final mVoiceMode:Ljava/lang/String;

.field public final mWeakSpaceStrippers:Ljava/lang/String;

.field public final mWeakSpaceSwappers:Ljava/lang/String;

.field public final mWordSeparators:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/inputmethod/latin/SettingsValues;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/inputmethod/latin/SettingsValues;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/SharedPreferences;Landroid/content/Context;)V
    .locals 11
    .param p1    # Landroid/content/SharedPreferences;
    .param p2    # Landroid/content/Context;

    const/4 v8, 0x0

    const/4 v7, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v6, 0x7f090001

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v6

    iput v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mDelayUpdateOldSuggestions:I

    const v6, 0x7f0a0005

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mWeakSpaceStrippers:Ljava/lang/String;

    const v6, 0x7f0a0004

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mWeakSpaceSwappers:Ljava/lang/String;

    const v6, 0x7f0a0006

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mPhantomSpacePromotingSymbols:Ljava/lang/String;

    sget-boolean v6, Lcom/android/inputmethod/latin/LatinImeLogger;->sDBG:Z

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mWeakSpaceStrippers:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    iget-object v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mWeakSpaceStrippers:Ljava/lang/String;

    invoke-virtual {v6, v0}, Ljava/lang/String;->codePointAt(I)I

    move-result v6

    invoke-virtual {p0, v6}, Lcom/android/inputmethod/latin/SettingsValues;->isWeakSpaceSwapper(I)Z

    move-result v6

    if-eqz v6, :cond_0

    new-instance v6, Ljava/lang/RuntimeException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Char code "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/inputmethod/latin/SettingsValues;->mWeakSpaceStrippers:Ljava/lang/String;

    invoke-virtual {v8, v0}, Ljava/lang/String;->codePointAt(I)I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " is both a weak space swapper and stripper."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_0
    iget-object v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mWeakSpaceStrippers:Ljava/lang/String;

    invoke-virtual {v6, v0, v7}, Ljava/lang/String;->offsetByCodePoints(II)I

    move-result v0

    goto :goto_0

    :cond_1
    const v6, 0x7f0a0003

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v9, 0x0

    invoke-static {v6, v9}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser;->parseCsvString(Ljava/lang/String;Lcom/android/inputmethod/keyboard/internal/KeyboardTextsSet;)[Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/inputmethod/latin/SettingsValues;->createSuggestPuncList([Ljava/lang/String;)Lcom/android/inputmethod/latin/SuggestedWords;

    move-result-object v6

    iput-object v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mSuggestPuncList:Lcom/android/inputmethod/latin/SuggestedWords;

    const v6, 0x7f0a0007

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mSymbolsExcludedFromWordSeparators:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mWeakSpaceStrippers:Ljava/lang/String;

    iget-object v9, p0, Lcom/android/inputmethod/latin/SettingsValues;->mWeakSpaceSwappers:Ljava/lang/String;

    iget-object v10, p0, Lcom/android/inputmethod/latin/SettingsValues;->mSymbolsExcludedFromWordSeparators:Ljava/lang/String;

    invoke-static {v6, v9, v10, v2}, Lcom/android/inputmethod/latin/SettingsValues;->createWordSeparators(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mWordSeparators:Ljava/lang/String;

    const v6, 0x7f0a0086

    invoke-virtual {p2, v6}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    iput-object v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mHintToSaveText:Ljava/lang/CharSequence;

    const-string v6, "auto_cap"

    invoke-interface {p1, v6, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    iput-boolean v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mAutoCap:Z

    invoke-static {p2, p1, v2}, Lcom/android/inputmethod/latin/SettingsValues;->isVibrateOn(Landroid/content/Context;Landroid/content/SharedPreferences;Landroid/content/res/Resources;)Z

    move-result v6

    iput-boolean v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mVibrateOn:Z

    const-string v6, "sound_on"

    const v9, 0x7f080009

    invoke-virtual {v2, v9}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v9

    invoke-interface {p1, v6, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    iput-boolean v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mSoundOn:Z

    invoke-static {p1, v2}, Lcom/android/inputmethod/latin/SettingsValues;->isKeyPreviewPopupEnabled(Landroid/content/SharedPreferences;Landroid/content/res/Resources;)Z

    move-result v6

    iput-boolean v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mKeyPreviewPopupOn:Z

    const v6, 0x7f0a0011

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v6, 0x7f0a0013

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "voice_mode"

    invoke-interface {p1, v6, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mVoiceMode:Ljava/lang/String;

    const-string v6, "auto_correction_threshold"

    const v9, 0x7f0a000e

    invoke-virtual {v2, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-interface {p1, v6, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mAutoCorrectionThresholdRawValue:Ljava/lang/String;

    const-string v6, "show_suggestions_setting"

    const v9, 0x7f0a000c

    invoke-virtual {v2, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-interface {p1, v6, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mShowSuggestionsSetting:Ljava/lang/String;

    invoke-static {p1}, Lcom/android/inputmethod/latin/SettingsValues;->getUsabilityStudyMode(Landroid/content/SharedPreferences;)Z

    move-result v6

    iput-boolean v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mUsabilityStudyMode:Z

    const-string v6, "pref_include_other_imes_in_language_switch_list"

    invoke-interface {p1, v6, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    iput-boolean v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mIncludesOtherImesInLanguageSwitchList:Z

    invoke-static {p1}, Lcom/android/inputmethod/latin/SettingsValues;->isLanguageSwitchKeySupressed(Landroid/content/SharedPreferences;)Z

    move-result v6

    iput-boolean v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mIsLanguageSwitchKeySuppressed:Z

    const-string v6, "pref_key_preview_popup_dismiss_delay"

    const v9, 0x7f09000a

    invoke-virtual {v2, v9}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-interface {p1, v6, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mKeyPreviewPopupDismissDelayRawValue:Ljava/lang/String;

    const-string v6, "pref_key_use_contacts_dict"

    invoke-interface {p1, v6, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    iput-boolean v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mUseContactsDict:Z

    iget-object v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mAutoCorrectionThresholdRawValue:Ljava/lang/String;

    invoke-static {v2, v6}, Lcom/android/inputmethod/latin/SettingsValues;->isAutoCorrectEnabled(Landroid/content/res/Resources;Ljava/lang/String;)Z

    move-result v6

    iput-boolean v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mAutoCorrectEnabled:Z

    iget-boolean v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mAutoCorrectEnabled:Z

    if-eqz v6, :cond_2

    iget-boolean v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mAutoCorrectEnabled:Z

    invoke-static {p1, v2, v6}, Lcom/android/inputmethod/latin/SettingsValues;->isBigramSuggestionEnabled(Landroid/content/SharedPreferences;Landroid/content/res/Resources;Z)Z

    move-result v6

    if-eqz v6, :cond_2

    move v6, v7

    :goto_1
    iput-boolean v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mBigramSuggestionEnabled:Z

    iget-boolean v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mBigramSuggestionEnabled:Z

    if-eqz v6, :cond_3

    invoke-static {p1, v2}, Lcom/android/inputmethod/latin/SettingsValues;->isBigramPredictionEnabled(Landroid/content/SharedPreferences;Landroid/content/res/Resources;)Z

    move-result v6

    if-eqz v6, :cond_3

    move v6, v7

    :goto_2
    iput-boolean v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mBigramPredictionEnabled:Z

    iput-boolean v7, p0, Lcom/android/inputmethod/latin/SettingsValues;->mEnableSuggestionSpanInsertion:Z

    const-string v6, "pref_vibration_duration_settings"

    const/4 v9, -0x1

    invoke-interface {p1, v6, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v6

    iput v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mVibrationDurationSettingsRawValue:I

    const-string v6, "pref_keypress_sound_volume"

    const/high16 v9, -0x40800000

    invoke-interface {p1, v6, v9}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v6

    iput v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mKeypressSoundVolumeRawValue:F

    invoke-static {p1, v2}, Lcom/android/inputmethod/latin/SettingsValues;->getCurrentVibrationDuration(Landroid/content/SharedPreferences;Landroid/content/res/Resources;)I

    move-result v6

    iput v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mKeypressVibrationDuration:I

    invoke-static {p1, v2}, Lcom/android/inputmethod/latin/SettingsValues;->getCurrentKeypressSoundVolume(Landroid/content/SharedPreferences;Landroid/content/res/Resources;)F

    move-result v6

    iput v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mFxVolume:F

    invoke-static {p1, v2}, Lcom/android/inputmethod/latin/SettingsValues;->getKeyPreviewPopupDismissDelay(Landroid/content/SharedPreferences;Landroid/content/res/Resources;)I

    move-result v6

    iput v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mKeyPreviewPopupDismissDelay:I

    iget-object v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mAutoCorrectionThresholdRawValue:Ljava/lang/String;

    invoke-static {v2, v6}, Lcom/android/inputmethod/latin/SettingsValues;->getAutoCorrectionThreshold(Landroid/content/res/Resources;Ljava/lang/String;)F

    move-result v6

    iput v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mAutoCorrectionThreshold:F

    iget-object v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mVoiceMode:Ljava/lang/String;

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mVoiceMode:Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    move v6, v7

    :goto_3
    iput-boolean v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mVoiceKeyEnabled:Z

    iget-object v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mVoiceMode:Ljava/lang/String;

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mVoiceMode:Ljava/lang/String;

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    :goto_4
    iput-boolean v7, p0, Lcom/android/inputmethod/latin/SettingsValues;->mVoiceKeyOnMain:Z

    invoke-static {p1, v2}, Lcom/android/inputmethod/latin/SettingsValues;->getPrefAdditionalSubtypes(Landroid/content/SharedPreferences;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/android/inputmethod/latin/AdditionalSubtype;->createAdditionalSubtypesArray(Ljava/lang/String;)[Landroid/view/inputmethod/InputMethodSubtype;

    move-result-object v6

    iput-object v6, p0, Lcom/android/inputmethod/latin/SettingsValues;->mAdditionalSubtypes:[Landroid/view/inputmethod/InputMethodSubtype;

    return-void

    :cond_2
    move v6, v8

    goto :goto_1

    :cond_3
    move v6, v8

    goto :goto_2

    :cond_4
    move v6, v8

    goto :goto_3

    :cond_5
    move v7, v8

    goto :goto_4
.end method

.method private static createSuggestPuncList([Ljava/lang/String;)Lcom/android/inputmethod/latin/SuggestedWords;
    .locals 12
    .param p0    # [Ljava/lang/String;

    const/4 v2, 0x0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    if-eqz p0, :cond_0

    move-object v8, p0

    array-length v10, v8

    const/4 v9, 0x0

    :goto_0
    if-ge v9, v10, :cond_0

    aget-object v11, v8, v9

    new-instance v0, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;

    invoke-static {v11}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser;->getLabel(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7fffffff

    invoke-direct {v0, v3, v4}, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;-><init>(Ljava/lang/CharSequence;I)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/android/inputmethod/latin/SuggestedWords;

    const/4 v5, 0x1

    move v3, v2

    move v4, v2

    move v6, v2

    move v7, v2

    invoke-direct/range {v0 .. v7}, Lcom/android/inputmethod/latin/SuggestedWords;-><init>(Ljava/util/ArrayList;ZZZZZZ)V

    return-object v0
.end method

.method private static createWordSeparators(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 4
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/content/res/Resources;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f0a0006

    invoke-virtual {p3, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    :goto_0
    if-ltz v0, :cond_0

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p2, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private static getAutoCorrectionThreshold(Landroid/content/res/Resources;Ljava/lang/String;)F
    .locals 7
    .param p0    # Landroid/content/res/Resources;
    .param p1    # Ljava/lang/String;

    const/high16 v4, 0x7f0c0000

    invoke-virtual {p0, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    const v1, 0x7f7fffff

    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_0

    array-length v4, v2

    if-ge v0, v4, :cond_0

    aget-object v4, v2, v0

    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v3

    const v1, 0x7f7fffff

    sget-object v4, Lcom/android/inputmethod/latin/SettingsValues;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Cannot load auto correction threshold setting. currentAutoCorrectionSetting: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", autoCorrectionThresholdValues: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getCurrentKeypressSoundVolume(Landroid/content/SharedPreferences;Landroid/content/res/Resources;)F
    .locals 3
    .param p0    # Landroid/content/SharedPreferences;
    .param p1    # Landroid/content/res/Resources;

    const-string v1, "pref_keypress_sound_volume"

    const/high16 v2, -0x40800000

    invoke-interface {p0, v1, v2}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const v1, 0x7f0c0011

    const-string v2, "-1.0f"

    invoke-static {p1, v1, v2}, Lcom/android/inputmethod/latin/Utils;->getDeviceOverrideValue(Landroid/content/res/Resources;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    goto :goto_0
.end method

.method public static getCurrentVibrationDuration(Landroid/content/SharedPreferences;Landroid/content/res/Resources;)I
    .locals 3
    .param p0    # Landroid/content/SharedPreferences;
    .param p1    # Landroid/content/res/Resources;

    const-string v1, "pref_vibration_duration_settings"

    const/4 v2, -0x1

    invoke-interface {p0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-ltz v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const v1, 0x7f0c0010

    const-string v2, "-1"

    invoke-static {p1, v1, v2}, Lcom/android/inputmethod/latin/Utils;->getDeviceOverrideValue(Landroid/content/res/Resources;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public static getKeyPreviewPopupDismissDelay(Landroid/content/SharedPreferences;Landroid/content/res/Resources;)I
    .locals 2
    .param p0    # Landroid/content/SharedPreferences;
    .param p1    # Landroid/content/res/Resources;

    const-string v0, "pref_key_preview_popup_dismiss_delay"

    const v1, 0x7f09000a

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static getLastUserHistoryWriteTime(Landroid/content/SharedPreferences;Ljava/lang/String;)J
    .locals 4
    .param p0    # Landroid/content/SharedPreferences;
    .param p1    # Ljava/lang/String;

    const-string v2, "last_user_dictionary_write_time"

    const-string v3, ""

    invoke-interface {p0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/inputmethod/latin/Utils;->localeAndTimeStrToHashMap(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    :goto_0
    return-wide v2

    :cond_0
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method public static getPrefAdditionalSubtypes(Landroid/content/SharedPreferences;Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 5
    .param p0    # Landroid/content/SharedPreferences;
    .param p1    # Landroid/content/res/Resources;

    const v1, 0x7f0a0029

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, ""

    aput-object v4, v2, v3

    invoke-virtual {p1, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "custom_input_styles"

    invoke-interface {p0, v1, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getUsabilityStudyMode(Landroid/content/SharedPreferences;)Z
    .locals 2
    .param p0    # Landroid/content/SharedPreferences;

    const-string v0, "usability_study_mode"

    const/4 v1, 0x1

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private static isAutoCorrectEnabled(Landroid/content/res/Resources;Ljava/lang/String;)Z
    .locals 2
    .param p0    # Landroid/content/res/Resources;
    .param p1    # Ljava/lang/String;

    const v1, 0x7f0a000d

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static isBigramPredictionEnabled(Landroid/content/SharedPreferences;Landroid/content/res/Resources;)Z
    .locals 2
    .param p0    # Landroid/content/SharedPreferences;
    .param p1    # Landroid/content/res/Resources;

    const-string v0, "next_word_prediction"

    const v1, 0x7f080008

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private static isBigramSuggestionEnabled(Landroid/content/SharedPreferences;Landroid/content/res/Resources;Z)Z
    .locals 1
    .param p0    # Landroid/content/SharedPreferences;
    .param p1    # Landroid/content/res/Resources;
    .param p2    # Z

    const/4 v0, 0x1

    return v0
.end method

.method public static isKeyPreviewPopupEnabled(Landroid/content/SharedPreferences;Landroid/content/res/Resources;)Z
    .locals 3
    .param p0    # Landroid/content/SharedPreferences;
    .param p1    # Landroid/content/res/Resources;

    const v2, 0x7f080006

    const v1, 0x7f080003

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    :goto_0
    return v1

    :cond_0
    const-string v1, "popup_on"

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    invoke-interface {p0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    goto :goto_0
.end method

.method public static isLanguageSwitchKeySupressed(Landroid/content/SharedPreferences;)Z
    .locals 2
    .param p0    # Landroid/content/SharedPreferences;

    const-string v0, "pref_suppress_language_switch_key"

    const/4 v1, 0x0

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private static isVibrateOn(Landroid/content/Context;Landroid/content/SharedPreferences;Landroid/content/res/Resources;)Z
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/content/SharedPreferences;
    .param p2    # Landroid/content/res/Resources;

    invoke-static {p0}, Lcom/android/inputmethod/latin/VibratorUtils;->getInstance(Landroid/content/Context;)Lcom/android/inputmethod/latin/VibratorUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/VibratorUtils;->hasVibrator()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v1, "vibrate_on"

    const v2, 0x7f08000a

    invoke-virtual {p2, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    invoke-interface {p1, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static setLastUserHistoryWriteTime(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 5
    .param p0    # Landroid/content/SharedPreferences;
    .param p1    # Ljava/lang/String;

    const-string v3, "last_user_dictionary_write_time"

    const-string v4, ""

    invoke-interface {p0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/inputmethod/latin/Utils;->localeAndTimeStrToHashMap(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, p1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Lcom/android/inputmethod/latin/Utils;->localeAndTimeHashMapToStr(Ljava/util/HashMap;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "last_user_dictionary_write_time"

    invoke-interface {v3, v4, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method


# virtual methods
.method public getAdditionalSubtypes()[Landroid/view/inputmethod/InputMethodSubtype;
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/SettingsValues;->mAdditionalSubtypes:[Landroid/view/inputmethod/InputMethodSubtype;

    return-object v0
.end method

.method public isFullscreenModeAllowed(Landroid/content/res/Resources;)Z
    .locals 1
    .param p1    # Landroid/content/res/Resources;

    const v0, 0x7f080001

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public isLanguageSwitchKeyEnabled(Landroid/content/Context;)Z
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/android/inputmethod/latin/SettingsValues;->mIsLanguageSwitchKeySuppressed:Z

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-boolean v1, p0, Lcom/android/inputmethod/latin/SettingsValues;->mIncludesOtherImesInLanguageSwitchList:Z

    if-eqz v1, :cond_1

    invoke-static {p1, v0}, Lcom/android/inputmethod/latin/ImfUtils;->hasMultipleEnabledIMEsOrSubtypes(Landroid/content/Context;Z)Z

    move-result v0

    goto :goto_0

    :cond_1
    invoke-static {p1, v0}, Lcom/android/inputmethod/latin/ImfUtils;->hasMultipleEnabledSubtypesInThisIme(Landroid/content/Context;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public isPhantomSpacePromotingSymbol(I)Z
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/android/inputmethod/latin/SettingsValues;->mPhantomSpacePromotingSymbols:Ljava/lang/String;

    int-to-char v1, p1

    invoke-static {v1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public isSymbolExcludedFromWordSeparators(I)Z
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/android/inputmethod/latin/SettingsValues;->mSymbolsExcludedFromWordSeparators:Ljava/lang/String;

    int-to-char v1, p1

    invoke-static {v1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public isVoiceKeyEnabled(Landroid/view/inputmethod/EditorInfo;)Z
    .locals 4
    .param p1    # Landroid/view/inputmethod/EditorInfo;

    const/4 v2, 0x0

    invoke-static {}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->getInstance()Lcom/android/inputmethod/latin/SubtypeSwitcher;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->isShortcutImeEnabled()Z

    move-result v1

    if-eqz p1, :cond_1

    iget v0, p1, Landroid/view/inputmethod/EditorInfo;->inputType:I

    :goto_0
    if-eqz v1, :cond_0

    iget-boolean v3, p0, Lcom/android/inputmethod/latin/SettingsValues;->mVoiceKeyEnabled:Z

    if-eqz v3, :cond_0

    invoke-static {v0}, Lcom/android/inputmethod/latin/InputTypeUtils;->isPasswordInputType(I)Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v2, 0x1

    :cond_0
    return v2

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method public isVoiceKeyOnMain()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/inputmethod/latin/SettingsValues;->mVoiceKeyOnMain:Z

    return v0
.end method

.method public isWeakSpaceStripper(I)Z
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/android/inputmethod/latin/SettingsValues;->mWeakSpaceStrippers:Ljava/lang/String;

    int-to-char v1, p1

    invoke-static {v1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public isWeakSpaceSwapper(I)Z
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/android/inputmethod/latin/SettingsValues;->mWeakSpaceSwappers:Ljava/lang/String;

    int-to-char v1, p1

    invoke-static {v1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public isWordSeparator(I)Z
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/android/inputmethod/latin/SettingsValues;->mWordSeparators:Ljava/lang/String;

    int-to-char v1, p1

    invoke-static {v1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method
