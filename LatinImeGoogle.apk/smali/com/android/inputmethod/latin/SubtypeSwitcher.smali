.class public Lcom/android/inputmethod/latin/SubtypeSwitcher;
.super Ljava/lang/Object;
.source "SubtypeSwitcher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/inputmethod/latin/SubtypeSwitcher$NeedsToDisplayLanguage;
    }
.end annotation


# static fields
.field private static DBG:Z

.field private static final TAG:Ljava/lang/String;

.field private static final sInstance:Lcom/android/inputmethod/latin/SubtypeSwitcher;


# instance fields
.field private mConnectivityManager:Landroid/net/ConnectivityManager;

.field private mCurrentSubtype:Landroid/view/inputmethod/InputMethodSubtype;

.field private mCurrentSystemLocale:Ljava/util/Locale;

.field private mImm:Landroid/view/inputmethod/InputMethodManager;

.field private mIsNetworkConnected:Z

.field private mNeedsToDisplayLanguage:Lcom/android/inputmethod/latin/SubtypeSwitcher$NeedsToDisplayLanguage;

.field private mNoLanguageSubtype:Landroid/view/inputmethod/InputMethodSubtype;

.field private mResources:Landroid/content/res/Resources;

.field private mService:Lcom/android/inputmethod/latin/LatinIME;

.field private mShortcutInputMethodInfo:Landroid/view/inputmethod/InputMethodInfo;

.field private mShortcutSubtype:Landroid/view/inputmethod/InputMethodSubtype;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sDBG:Z

    sput-boolean v0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->DBG:Z

    const-class v0, Lcom/android/inputmethod/latin/SubtypeSwitcher;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->TAG:Ljava/lang/String;

    new-instance v0, Lcom/android/inputmethod/latin/SubtypeSwitcher;

    invoke-direct {v0}, Lcom/android/inputmethod/latin/SubtypeSwitcher;-><init>()V

    sput-object v0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->sInstance:Lcom/android/inputmethod/latin/SubtypeSwitcher;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/android/inputmethod/latin/SubtypeSwitcher$NeedsToDisplayLanguage;

    invoke-direct {v0}, Lcom/android/inputmethod/latin/SubtypeSwitcher$NeedsToDisplayLanguage;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mNeedsToDisplayLanguage:Lcom/android/inputmethod/latin/SubtypeSwitcher$NeedsToDisplayLanguage;

    return-void
.end method

.method public static getInstance()Lcom/android/inputmethod/latin/SubtypeSwitcher;
    .locals 1

    sget-object v0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->sInstance:Lcom/android/inputmethod/latin/SubtypeSwitcher;

    return-object v0
.end method

.method public static init(Lcom/android/inputmethod/latin/LatinIME;)V
    .locals 1
    .param p0    # Lcom/android/inputmethod/latin/LatinIME;

    invoke-static {p0}, Lcom/android/inputmethod/latin/SubtypeLocale;->init(Landroid/content/Context;)V

    sget-object v0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->sInstance:Lcom/android/inputmethod/latin/SubtypeSwitcher;

    invoke-direct {v0, p0}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->initialize(Lcom/android/inputmethod/latin/LatinIME;)V

    sget-object v0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->sInstance:Lcom/android/inputmethod/latin/SubtypeSwitcher;

    invoke-direct {v0}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->updateAllParameters()V

    return-void
.end method

.method private initialize(Lcom/android/inputmethod/latin/LatinIME;)V
    .locals 3
    .param p1    # Lcom/android/inputmethod/latin/LatinIME;

    iput-object p1, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mService:Lcom/android/inputmethod/latin/LatinIME;

    invoke-virtual {p1}, Lcom/android/inputmethod/latin/LatinIME;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iput-object v1, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mResources:Landroid/content/res/Resources;

    invoke-static {p1}, Lcom/android/inputmethod/latin/ImfUtils;->getInputMethodManager(Landroid/content/Context;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v1

    iput-object v1, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mImm:Landroid/view/inputmethod/InputMethodManager;

    const-string v1, "connectivity"

    invoke-virtual {p1, v1}, Lcom/android/inputmethod/latin/LatinIME;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    iput-object v1, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mConnectivityManager:Landroid/net/ConnectivityManager;

    iget-object v1, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    iput-object v1, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mCurrentSystemLocale:Ljava/util/Locale;

    iget-object v1, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mImm:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodManager;->getCurrentInputMethodSubtype()Landroid/view/inputmethod/InputMethodSubtype;

    move-result-object v1

    iput-object v1, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mCurrentSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    const-string v1, "zz"

    const-string v2, "qwerty"

    invoke-static {p1, v1, v2}, Lcom/android/inputmethod/latin/ImfUtils;->findSubtypeByLocaleAndKeyboardLayoutSet(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/view/inputmethod/InputMethodSubtype;

    move-result-object v1

    iput-object v1, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mNoLanguageSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    iget-object v1, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mNoLanguageSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Can\'t find no lanugage with QWERTY subtype"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v1, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mIsNetworkConnected:Z

    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private switchToTargetIME(Ljava/lang/String;Landroid/view/inputmethod/InputMethodSubtype;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/view/inputmethod/InputMethodSubtype;

    iget-object v0, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mService:Lcom/android/inputmethod/latin/LatinIME;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/LatinIME;->getWindow()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget-object v3, v0, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    if-nez v3, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mImm:Landroid/view/inputmethod/InputMethodManager;

    new-instance v0, Lcom/android/inputmethod/latin/SubtypeSwitcher$1;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/android/inputmethod/latin/SubtypeSwitcher$1;-><init>(Lcom/android/inputmethod/latin/SubtypeSwitcher;Landroid/view/inputmethod/InputMethodManager;Landroid/os/IBinder;Ljava/lang/String;Landroid/view/inputmethod/InputMethodSubtype;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v4}, Lcom/android/inputmethod/latin/SubtypeSwitcher$1;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private updateAllParameters()V
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    iput-object v0, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mCurrentSystemLocale:Ljava/util/Locale;

    iget-object v0, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mImm:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->getCurrentInputMethodSubtype()Landroid/view/inputmethod/InputMethodSubtype;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->updateSubtype(Landroid/view/inputmethod/InputMethodSubtype;)V

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->updateParametersOnStartInputView()V

    return-void
.end method

.method private updateEnabledSubtypes()V
    .locals 8

    iget-object v0, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mCurrentSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    const/4 v2, 0x1

    iget-object v5, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mImm:Landroid/view/inputmethod/InputMethodManager;

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v7}, Landroid/view/inputmethod/InputMethodManager;->getEnabledInputMethodSubtypeList(Landroid/view/inputmethod/InputMethodInfo;Z)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/inputmethod/InputMethodSubtype;

    invoke-virtual {v4, v0}, Landroid/view/inputmethod/InputMethodSubtype;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    iget-object v5, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mNeedsToDisplayLanguage:Lcom/android/inputmethod/latin/SubtypeSwitcher$NeedsToDisplayLanguage;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/android/inputmethod/latin/SubtypeSwitcher$NeedsToDisplayLanguage;->updateEnabledSubtypeCount(I)V

    if-eqz v2, :cond_3

    sget-boolean v5, Lcom/android/inputmethod/latin/SubtypeSwitcher;->DBG:Z

    if-eqz v5, :cond_2

    sget-object v5, Lcom/android/inputmethod/latin/SubtypeSwitcher;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Last subtype: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodSubtype;->getLocale()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodSubtype;->getExtraValue()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v5, Lcom/android/inputmethod/latin/SubtypeSwitcher;->TAG:Ljava/lang/String;

    const-string v6, "Last subtype was disabled. Update to the current one."

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v5, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mImm:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v5}, Landroid/view/inputmethod/InputMethodManager;->getCurrentInputMethodSubtype()Landroid/view/inputmethod/InputMethodSubtype;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->updateSubtype(Landroid/view/inputmethod/InputMethodSubtype;)V

    :cond_3
    return-void
.end method

.method private updateShortcutIME()V
    .locals 9

    const/4 v5, 0x0

    sget-boolean v4, Lcom/android/inputmethod/latin/SubtypeSwitcher;->DBG:Z

    if-eqz v4, :cond_0

    sget-object v6, Lcom/android/inputmethod/latin/SubtypeSwitcher;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Update shortcut IME from : "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v4, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mShortcutInputMethodInfo:Landroid/view/inputmethod/InputMethodInfo;

    if-nez v4, :cond_3

    const-string v4, "<null>"

    :goto_0
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ", "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v4, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mShortcutSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    if-nez v4, :cond_4

    const-string v4, "<null>"

    :goto_1
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v4, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mImm:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v4}, Landroid/view/inputmethod/InputMethodManager;->getShortcutInputMethodsAndSubtypes()Ljava/util/Map;

    move-result-object v2

    iput-object v5, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mShortcutInputMethodInfo:Landroid/view/inputmethod/InputMethodInfo;

    iput-object v5, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mShortcutSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodInfo;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    iput-object v1, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mShortcutInputMethodInfo:Landroid/view/inputmethod/InputMethodInfo;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_5

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/inputmethod/InputMethodSubtype;

    :goto_2
    iput-object v4, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mShortcutSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    :cond_1
    sget-boolean v4, Lcom/android/inputmethod/latin/SubtypeSwitcher;->DBG:Z

    if-eqz v4, :cond_2

    sget-object v5, Lcom/android/inputmethod/latin/SubtypeSwitcher;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Update shortcut IME to : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v4, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mShortcutInputMethodInfo:Landroid/view/inputmethod/InputMethodInfo;

    if-nez v4, :cond_6

    const-string v4, "<null>"

    :goto_3
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ", "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v4, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mShortcutSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    if-nez v4, :cond_7

    const-string v4, "<null>"

    :goto_4
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void

    :cond_3
    iget-object v4, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mShortcutInputMethodInfo:Landroid/view/inputmethod/InputMethodInfo;

    invoke-virtual {v4}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    :cond_4
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mShortcutSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    invoke-virtual {v8}, Landroid/view/inputmethod/InputMethodSubtype;->getLocale()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, ", "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v8, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mShortcutSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    invoke-virtual {v8}, Landroid/view/inputmethod/InputMethodSubtype;->getMode()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    :cond_5
    move-object v4, v5

    goto :goto_2

    :cond_6
    iget-object v4, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mShortcutInputMethodInfo:Landroid/view/inputmethod/InputMethodInfo;

    invoke-virtual {v4}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;

    move-result-object v4

    goto :goto_3

    :cond_7
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mShortcutSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    invoke-virtual {v7}, Landroid/view/inputmethod/InputMethodSubtype;->getLocale()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ", "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v7, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mShortcutSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    invoke-virtual {v7}, Landroid/view/inputmethod/InputMethodSubtype;->getMode()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_4
.end method


# virtual methods
.method public getCurrentSubtype()Landroid/view/inputmethod/InputMethodSubtype;
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mCurrentSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    return-object v0
.end method

.method public getCurrentSubtypeLocale()Ljava/util/Locale;
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mCurrentSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    invoke-static {v0}, Lcom/android/inputmethod/latin/SubtypeLocale;->getSubtypeLocale(Landroid/view/inputmethod/InputMethodSubtype;)Ljava/util/Locale;

    move-result-object v0

    return-object v0
.end method

.method public getNoLanguageSubtype()Landroid/view/inputmethod/InputMethodSubtype;
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mNoLanguageSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    return-object v0
.end method

.method public isShortcutImeEnabled()Z
    .locals 7

    const/4 v3, 0x0

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mShortcutInputMethodInfo:Landroid/view/inputmethod/InputMethodInfo;

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    iget-object v5, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mShortcutSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    if-nez v5, :cond_2

    move v3, v4

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    iget-object v5, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mImm:Landroid/view/inputmethod/InputMethodManager;

    iget-object v6, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mShortcutInputMethodInfo:Landroid/view/inputmethod/InputMethodInfo;

    invoke-virtual {v5, v6, v4}, Landroid/view/inputmethod/InputMethodManager;->getEnabledInputMethodSubtypeList(Landroid/view/inputmethod/InputMethodInfo;Z)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodSubtype;

    iget-object v5, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mShortcutSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    invoke-virtual {v1, v5}, Landroid/view/inputmethod/InputMethodSubtype;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    move v3, v4

    goto :goto_0
.end method

.method public isShortcutImeReady()Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mShortcutInputMethodInfo:Landroid/view/inputmethod/InputMethodInfo;

    if-nez v1, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mShortcutSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mShortcutSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    const-string v2, "requireNetworkConnectivity"

    invoke-virtual {v1, v2}, Landroid/view/inputmethod/InputMethodSubtype;->containsExtraValueKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v0, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mIsNetworkConnected:Z

    goto :goto_0
.end method

.method public needsToDisplayLanguage(Ljava/util/Locale;)Z
    .locals 2
    .param p1    # Ljava/util/Locale;

    invoke-virtual {p1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "zz"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->getCurrentSubtypeLocale()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mNeedsToDisplayLanguage:Lcom/android/inputmethod/latin/SubtypeSwitcher$NeedsToDisplayLanguage;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/SubtypeSwitcher$NeedsToDisplayLanguage;->getValue()Z

    move-result v0

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1    # Landroid/content/res/Configuration;

    iget-object v0, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    iget-object v1, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mCurrentSystemLocale:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->updateAllParameters()V

    :cond_0
    return-void
.end method

.method public onNetworkStateChanged(Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Intent;

    const/4 v1, 0x0

    const-string v2, "noConnectivity"

    invoke-virtual {p1, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    iput-boolean v1, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mIsNetworkConnected:Z

    invoke-static {}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getInstance()Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->onNetworkStateChanged()V

    return-void
.end method

.method public switchToShortcutIME()V
    .locals 2

    iget-object v1, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mShortcutInputMethodInfo:Landroid/view/inputmethod/InputMethodInfo;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mShortcutInputMethodInfo:Landroid/view/inputmethod/InputMethodInfo;

    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mShortcutSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    invoke-direct {p0, v0, v1}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->switchToTargetIME(Ljava/lang/String;Landroid/view/inputmethod/InputMethodSubtype;)V

    goto :goto_0
.end method

.method public updateParametersOnStartInputView()V
    .locals 0

    invoke-direct {p0}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->updateEnabledSubtypes()V

    invoke-direct {p0}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->updateShortcutIME()V

    return-void
.end method

.method public updateSubtype(Landroid/view/inputmethod/InputMethodSubtype;)V
    .locals 4
    .param p1    # Landroid/view/inputmethod/InputMethodSubtype;

    sget-boolean v1, Lcom/android/inputmethod/latin/SubtypeSwitcher;->DBG:Z

    if-eqz v1, :cond_0

    sget-object v1, Lcom/android/inputmethod/latin/SubtypeSwitcher;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCurrentInputMethodSubtypeChanged: to: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/view/inputmethod/InputMethodSubtype;->getLocale()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/view/inputmethod/InputMethodSubtype;->getExtraValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", from: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mCurrentSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    invoke-virtual {v3}, Landroid/view/inputmethod/InputMethodSubtype;->getLocale()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mCurrentSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    invoke-virtual {v3}, Landroid/view/inputmethod/InputMethodSubtype;->getExtraValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p1}, Lcom/android/inputmethod/latin/SubtypeLocale;->getSubtypeLocale(Landroid/view/inputmethod/InputMethodSubtype;)Ljava/util/Locale;

    move-result-object v0

    iget-object v1, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mNeedsToDisplayLanguage:Lcom/android/inputmethod/latin/SubtypeSwitcher$NeedsToDisplayLanguage;

    iget-object v2, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mCurrentSystemLocale:Ljava/util/Locale;

    invoke-virtual {v2, v0}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/inputmethod/latin/SubtypeSwitcher$NeedsToDisplayLanguage;->updateIsSystemLanguageSameAsInputLanguage(Z)V

    iget-object v1, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mCurrentSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    invoke-virtual {p1, v1}, Landroid/view/inputmethod/InputMethodSubtype;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mCurrentSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    invoke-direct {p0}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->updateShortcutIME()V

    iget-object v1, p0, Lcom/android/inputmethod/latin/SubtypeSwitcher;->mService:Lcom/android/inputmethod/latin/LatinIME;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/LatinIME;->onRefreshKeyboard()V

    goto :goto_0
.end method
