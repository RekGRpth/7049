.class Lcom/android/inputmethod/latin/Suggest$1;
.super Ljava/lang/Thread;
.source "Suggest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/inputmethod/latin/Suggest;->resetMainDict(Landroid/content/Context;Ljava/util/Locale;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/inputmethod/latin/Suggest;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$locale:Ljava/util/Locale;


# direct methods
.method constructor <init>(Lcom/android/inputmethod/latin/Suggest;Ljava/lang/String;Landroid/content/Context;Ljava/util/Locale;)V
    .locals 0
    .param p2    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/inputmethod/latin/Suggest$1;->this$0:Lcom/android/inputmethod/latin/Suggest;

    iput-object p3, p0, Lcom/android/inputmethod/latin/Suggest$1;->val$context:Landroid/content/Context;

    iput-object p4, p0, Lcom/android/inputmethod/latin/Suggest$1;->val$locale:Ljava/util/Locale;

    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v1, p0, Lcom/android/inputmethod/latin/Suggest$1;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/inputmethod/latin/Suggest$1;->val$locale:Ljava/util/Locale;

    invoke-static {v1, v2}, Lcom/android/inputmethod/latin/DictionaryFactory;->createMainDictionaryFromManager(Landroid/content/Context;Ljava/util/Locale;)Lcom/android/inputmethod/latin/DictionaryCollection;

    move-result-object v0

    iget-object v2, p0, Lcom/android/inputmethod/latin/Suggest$1;->this$0:Lcom/android/inputmethod/latin/Suggest;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/DictionaryCollection;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    # setter for: Lcom/android/inputmethod/latin/Suggest;->mHasMainDictionary:Z
    invoke-static {v2, v1}, Lcom/android/inputmethod/latin/Suggest;->access$002(Lcom/android/inputmethod/latin/Suggest;Z)Z

    iget-object v1, p0, Lcom/android/inputmethod/latin/Suggest$1;->this$0:Lcom/android/inputmethod/latin/Suggest;

    # getter for: Lcom/android/inputmethod/latin/Suggest;->mUnigramDictionaries:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v1}, Lcom/android/inputmethod/latin/Suggest;->access$100(Lcom/android/inputmethod/latin/Suggest;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v1

    const-string v2, "main"

    # invokes: Lcom/android/inputmethod/latin/Suggest;->addOrReplaceDictionary(Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/String;Lcom/android/inputmethod/latin/Dictionary;)V
    invoke-static {v1, v2, v0}, Lcom/android/inputmethod/latin/Suggest;->access$200(Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/String;Lcom/android/inputmethod/latin/Dictionary;)V

    iget-object v1, p0, Lcom/android/inputmethod/latin/Suggest$1;->this$0:Lcom/android/inputmethod/latin/Suggest;

    # getter for: Lcom/android/inputmethod/latin/Suggest;->mBigramDictionaries:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v1}, Lcom/android/inputmethod/latin/Suggest;->access$300(Lcom/android/inputmethod/latin/Suggest;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v1

    const-string v2, "main"

    # invokes: Lcom/android/inputmethod/latin/Suggest;->addOrReplaceDictionary(Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/String;Lcom/android/inputmethod/latin/Dictionary;)V
    invoke-static {v1, v2, v0}, Lcom/android/inputmethod/latin/Suggest;->access$200(Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/String;Lcom/android/inputmethod/latin/Dictionary;)V

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
