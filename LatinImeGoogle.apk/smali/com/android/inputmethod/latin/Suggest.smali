.class public Lcom/android/inputmethod/latin/Suggest;
.super Ljava/lang/Object;
.source "Suggest.java"

# interfaces
.implements Lcom/android/inputmethod/latin/Dictionary$WordCallback;


# static fields
.field private static final DBG:Z

.field public static final TAG:Ljava/lang/String;

.field private static final sEmptyWordComposer:Lcom/android/inputmethod/latin/WordComposer;


# instance fields
.field private mAutoCorrectionThreshold:F

.field private final mBigramDictionaries:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/inputmethod/latin/Dictionary;",
            ">;"
        }
    .end annotation
.end field

.field private mBigramSuggestions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mConsideredWord:Ljava/lang/CharSequence;

.field private mContactsDict:Lcom/android/inputmethod/latin/Dictionary;

.field private mHasMainDictionary:Z

.field private mIsAllUpperCase:Z

.field private mIsFirstCharCapitalized:Z

.field private mPrefMaxSuggestions:I

.field private mSuggestions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mTrailingSingleQuotesCount:I

.field private final mUnigramDictionaries:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/inputmethod/latin/Dictionary;",
            ">;"
        }
    .end annotation
.end field

.field private mWhiteListDictionary:Lcom/android/inputmethod/latin/WhitelistDictionary;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/inputmethod/latin/Suggest;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/inputmethod/latin/Suggest;->TAG:Ljava/lang/String;

    sget-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sDBG:Z

    sput-boolean v0, Lcom/android/inputmethod/latin/Suggest;->DBG:Z

    new-instance v0, Lcom/android/inputmethod/latin/WordComposer;

    invoke-direct {v0}, Lcom/android/inputmethod/latin/WordComposer;-><init>()V

    sput-object v0, Lcom/android/inputmethod/latin/Suggest;->sEmptyWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljava/io/File;JJLjava/util/Locale;)V
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/io/File;
    .param p3    # J
    .param p5    # J
    .param p7    # Ljava/util/Locale;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v1, p0, Lcom/android/inputmethod/latin/Suggest;->mUnigramDictionaries:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v1, p0, Lcom/android/inputmethod/latin/Suggest;->mBigramDictionaries:Ljava/util/concurrent/ConcurrentHashMap;

    const/16 v1, 0x12

    iput v1, p0, Lcom/android/inputmethod/latin/Suggest;->mPrefMaxSuggestions:I

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/inputmethod/latin/Suggest;->mSuggestions:Ljava/util/ArrayList;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/inputmethod/latin/Suggest;->mBigramSuggestions:Ljava/util/ArrayList;

    const/4 v7, 0x0

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    move-wide v5, p5

    move-object/from16 v8, p7

    invoke-static/range {v1 .. v8}, Lcom/android/inputmethod/latin/DictionaryFactory;->createDictionaryForTest(Landroid/content/Context;Ljava/io/File;JJZLjava/util/Locale;)Lcom/android/inputmethod/latin/Dictionary;

    move-result-object v9

    if-eqz v9, :cond_0

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/android/inputmethod/latin/Suggest;->mHasMainDictionary:Z

    iget-object v1, p0, Lcom/android/inputmethod/latin/Suggest;->mUnigramDictionaries:Ljava/util/concurrent/ConcurrentHashMap;

    const-string v2, "main"

    invoke-static {v1, v2, v9}, Lcom/android/inputmethod/latin/Suggest;->addOrReplaceDictionary(Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/String;Lcom/android/inputmethod/latin/Dictionary;)V

    iget-object v1, p0, Lcom/android/inputmethod/latin/Suggest;->mBigramDictionaries:Ljava/util/concurrent/ConcurrentHashMap;

    const-string v2, "main"

    invoke-static {v1, v2, v9}, Lcom/android/inputmethod/latin/Suggest;->addOrReplaceDictionary(Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/String;Lcom/android/inputmethod/latin/Dictionary;)V

    move-object/from16 v0, p7

    invoke-direct {p0, p1, v0}, Lcom/android/inputmethod/latin/Suggest;->initWhitelistAndAutocorrectAndPool(Landroid/content/Context;Ljava/util/Locale;)V

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/Locale;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/util/Locale;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/latin/Suggest;->mUnigramDictionaries:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/latin/Suggest;->mBigramDictionaries:Ljava/util/concurrent/ConcurrentHashMap;

    const/16 v0, 0x12

    iput v0, p0, Lcom/android/inputmethod/latin/Suggest;->mPrefMaxSuggestions:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/latin/Suggest;->mSuggestions:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/latin/Suggest;->mBigramSuggestions:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/android/inputmethod/latin/Suggest;->initAsynchronously(Landroid/content/Context;Ljava/util/Locale;)V

    return-void
.end method

.method static synthetic access$002(Lcom/android/inputmethod/latin/Suggest;Z)Z
    .locals 0
    .param p0    # Lcom/android/inputmethod/latin/Suggest;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/inputmethod/latin/Suggest;->mHasMainDictionary:Z

    return p1
.end method

.method static synthetic access$100(Lcom/android/inputmethod/latin/Suggest;)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1
    .param p0    # Lcom/android/inputmethod/latin/Suggest;

    iget-object v0, p0, Lcom/android/inputmethod/latin/Suggest;->mUnigramDictionaries:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method

.method static synthetic access$200(Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/String;Lcom/android/inputmethod/latin/Dictionary;)V
    .locals 0
    .param p0    # Ljava/util/concurrent/ConcurrentHashMap;
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/android/inputmethod/latin/Dictionary;

    invoke-static {p0, p1, p2}, Lcom/android/inputmethod/latin/Suggest;->addOrReplaceDictionary(Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/String;Lcom/android/inputmethod/latin/Dictionary;)V

    return-void
.end method

.method static synthetic access$300(Lcom/android/inputmethod/latin/Suggest;)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1
    .param p0    # Lcom/android/inputmethod/latin/Suggest;

    iget-object v0, p0, Lcom/android/inputmethod/latin/Suggest;->mBigramDictionaries:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method

.method private static addOrReplaceDictionary(Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/String;Lcom/android/inputmethod/latin/Dictionary;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/android/inputmethod/latin/Dictionary;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/inputmethod/latin/Dictionary;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/android/inputmethod/latin/Dictionary;",
            ")V"
        }
    .end annotation

    if-nez p2, :cond_1

    invoke-virtual {p0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/inputmethod/latin/Dictionary;

    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_0

    if-eq p2, v0, :cond_0

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/Dictionary;->close()V

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0, p1, p2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/inputmethod/latin/Dictionary;

    move-object v0, v1

    goto :goto_0
.end method

.method private static capitalizeWord(ZZLjava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 4
    .param p0    # Z
    .param p1    # Z
    .param p2    # Ljava/lang/CharSequence;

    const/4 v3, 0x1

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    if-nez p0, :cond_2

    if-nez p1, :cond_2

    :cond_0
    move-object v0, p2

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/android/inputmethod/latin/Suggest;->getApproxMaxWordLength()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    if-eqz p0, :cond_3

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_3
    if-eqz p1, :cond_1

    const/4 v2, 0x0

    invoke-interface {p2, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    if-le v1, v3, :cond_1

    invoke-interface {p2, v3, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private getAllBigrams(Ljava/lang/CharSequence;Lcom/android/inputmethod/latin/WordComposer;)V
    .locals 4
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Lcom/android/inputmethod/latin/WordComposer;

    invoke-static {p1}, Lcom/android/inputmethod/latin/StringUtils;->hasUpperCase(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/android/inputmethod/latin/Suggest;->mBigramDictionaries:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/inputmethod/latin/Dictionary;

    invoke-virtual {v0, p2, v2, p0}, Lcom/android/inputmethod/latin/Dictionary;->getBigrams(Lcom/android/inputmethod/latin/WordComposer;Ljava/lang/CharSequence;Lcom/android/inputmethod/latin/Dictionary$WordCallback;)V

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/android/inputmethod/latin/Suggest;->mBigramDictionaries:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/inputmethod/latin/Dictionary;

    invoke-virtual {v0, p2, p1, p0}, Lcom/android/inputmethod/latin/Dictionary;->getBigrams(Lcom/android/inputmethod/latin/WordComposer;Ljava/lang/CharSequence;Lcom/android/inputmethod/latin/Dictionary$WordCallback;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public static getApproxMaxWordLength()I
    .locals 1

    const/16 v0, 0x20

    return v0
.end method

.method private static getSuggestionsInfoListWithDebugInfo(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 12
    .param p0    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;",
            ">;"
        }
    .end annotation

    const/4 v11, 0x0

    invoke-virtual {p1, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;

    const-string v7, "+"

    invoke-virtual {v6, v7}, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;->setDebugString(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x0

    :goto_0
    add-int/lit8 v7, v5, -0x1

    if-ge v1, v7, :cond_1

    add-int/lit8 v7, v1, 0x1

    invoke-virtual {p1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;->toString()Ljava/lang/String;

    move-result-object v7

    iget v8, v0, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;->mScore:I

    invoke-static {p0, v7, v8}, Lcom/android/inputmethod/latin/BinaryDictionary;->calcNormalizedScore(Ljava/lang/String;Ljava/lang/String;I)F

    move-result v2

    const/4 v7, 0x0

    cmpl-float v7, v2, v7

    if-lez v7, :cond_0

    const-string v7, "%d (%4.2f)"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    iget v9, v0, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;->mScore:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v11

    const/4 v9, 0x1

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    :goto_1
    invoke-virtual {v0, v3}, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;->setDebugString(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget v7, v0, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;->mScore:I

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_1
    return-object v4
.end method

.method private initAsynchronously(Landroid/content/Context;Ljava/util/Locale;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/util/Locale;

    invoke-virtual {p0, p1, p2}, Lcom/android/inputmethod/latin/Suggest;->resetMainDict(Landroid/content/Context;Ljava/util/Locale;)V

    invoke-direct {p0, p1, p2}, Lcom/android/inputmethod/latin/Suggest;->initWhitelistAndAutocorrectAndPool(Landroid/content/Context;Ljava/util/Locale;)V

    return-void
.end method

.method private initWhitelistAndAutocorrectAndPool(Landroid/content/Context;Ljava/util/Locale;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/util/Locale;

    new-instance v0, Lcom/android/inputmethod/latin/WhitelistDictionary;

    invoke-direct {v0, p1, p2}, Lcom/android/inputmethod/latin/WhitelistDictionary;-><init>(Landroid/content/Context;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/android/inputmethod/latin/Suggest;->mWhiteListDictionary:Lcom/android/inputmethod/latin/WhitelistDictionary;

    iget-object v0, p0, Lcom/android/inputmethod/latin/Suggest;->mUnigramDictionaries:Ljava/util/concurrent/ConcurrentHashMap;

    const-string v1, "whitelist"

    iget-object v2, p0, Lcom/android/inputmethod/latin/Suggest;->mWhiteListDictionary:Lcom/android/inputmethod/latin/WhitelistDictionary;

    invoke-static {v0, v1, v2}, Lcom/android/inputmethod/latin/Suggest;->addOrReplaceDictionary(Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/String;Lcom/android/inputmethod/latin/Dictionary;)V

    return-void
.end method

.method public static shouldBlockAutoCorrectionBySafetyNet(Ljava/lang/String;Ljava/lang/CharSequence;)Z
    .locals 7
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/CharSequence;

    const/4 v4, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x4

    if-ge v2, v3, :cond_0

    move v3, v4

    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x5

    if-ge v2, v3, :cond_3

    const/4 v3, 0x2

    :goto_1
    add-int/lit8 v1, v3, 0x1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/android/inputmethod/latin/BinaryDictionary;->editDistance(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    sget-boolean v3, Lcom/android/inputmethod/latin/Suggest;->DBG:Z

    if-eqz v3, :cond_1

    sget-object v3, Lcom/android/inputmethod/latin/Suggest;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Autocorrected edit distance = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    if-le v0, v1, :cond_4

    sget-boolean v3, Lcom/android/inputmethod/latin/Suggest;->DBG:Z

    if-eqz v3, :cond_2

    sget-object v3, Lcom/android/inputmethod/latin/Suggest;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Safety net: before = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", after = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v3, Lcom/android/inputmethod/latin/Suggest;->TAG:Ljava/lang/String;

    const-string v4, "(Error) The edit distance of this correction exceeds limit. Turning off auto-correction."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const/4 v3, 0x1

    goto :goto_0

    :cond_3
    div-int/lit8 v3, v2, 0x2

    goto :goto_1

    :cond_4
    move v3, v4

    goto :goto_0
.end method


# virtual methods
.method protected addBigramToSuggestions(Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;)V
    .locals 1
    .param p1    # Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;

    iget-object v0, p0, Lcom/android/inputmethod/latin/Suggest;->mSuggestions:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addWord([CIIIII)Z
    .locals 11
    .param p1    # [C
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I

    move/from16 v3, p6

    const/4 v9, 0x1

    move/from16 v0, p6

    if-ne v0, v9, :cond_1

    iget-object v8, p0, Lcom/android/inputmethod/latin/Suggest;->mBigramSuggestions:Ljava/util/ArrayList;

    const/16 v6, 0x3c

    :goto_0
    const/4 v5, 0x0

    iget-object v9, p0, Lcom/android/inputmethod/latin/Suggest;->mConsideredWord:Ljava/lang/CharSequence;

    invoke-static {v9, p1, p2, p3}, Lcom/android/inputmethod/latin/StringUtils;->equalsIgnoreCase(Ljava/lang/CharSequence;[CII)Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lez v9, :cond_0

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;

    iget-object v9, v2, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;->mWord:Ljava/lang/CharSequence;

    invoke-static {v9, p1, p2, p3}, Lcom/android/inputmethod/latin/StringUtils;->equalsIgnoreCase(Ljava/lang/CharSequence;[CII)Z

    move-result v9

    if-eqz v9, :cond_0

    iget v9, v2, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;->mScore:I

    if-gt p4, v9, :cond_0

    const/4 v5, 0x1

    :cond_0
    :goto_1
    if-lt v5, v6, :cond_5

    const/4 v9, 0x1

    :goto_2
    return v9

    :cond_1
    iget-object v8, p0, Lcom/android/inputmethod/latin/Suggest;->mSuggestions:Ljava/util/ArrayList;

    iget v6, p0, Lcom/android/inputmethod/latin/Suggest;->mPrefMaxSuggestions:I

    goto :goto_0

    :cond_2
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lt v9, v6, :cond_4

    add-int/lit8 v9, v6, -0x1

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;

    iget v9, v9, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;->mScore:I

    if-lt v9, p4, :cond_4

    const/4 v9, 0x1

    goto :goto_2

    :cond_3
    add-int/lit8 v5, v5, 0x1

    :cond_4
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v5, v9, :cond_0

    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;

    iget v1, v9, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;->mScore:I

    if-lt v1, p4, :cond_0

    if-ne v1, p4, :cond_3

    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;

    invoke-virtual {v9}, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;->codePointCount()I

    move-result v9

    if-ge p3, v9, :cond_3

    goto :goto_1

    :cond_5
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/android/inputmethod/latin/Suggest;->getApproxMaxWordLength()I

    move-result v9

    invoke-direct {v7, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    iget-boolean v9, p0, Lcom/android/inputmethod/latin/Suggest;->mIsAllUpperCase:Z

    if-eqz v9, :cond_7

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, p1, p2, p3}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v9}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_6
    :goto_3
    iget v9, p0, Lcom/android/inputmethod/latin/Suggest;->mTrailingSingleQuotesCount:I

    add-int/lit8 v4, v9, -0x1

    :goto_4
    if-ltz v4, :cond_9

    const/16 v9, 0x27

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->appendCodePoint(I)Ljava/lang/StringBuilder;

    add-int/lit8 v4, v4, -0x1

    goto :goto_4

    :cond_7
    iget-boolean v9, p0, Lcom/android/inputmethod/latin/Suggest;->mIsFirstCharCapitalized:Z

    if-eqz v9, :cond_8

    aget-char v9, p1, p2

    invoke-static {v9}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v9, 0x1

    if-le p3, v9, :cond_6

    add-int/lit8 v9, p2, 0x1

    add-int/lit8 v10, p3, -0x1

    invoke-virtual {v7, p1, v9, v10}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    goto :goto_3

    :cond_8
    invoke-virtual {v7, p1, p2, p3}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    goto :goto_3

    :cond_9
    new-instance v9, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;

    invoke-direct {v9, v7, p4}, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;-><init>(Ljava/lang/CharSequence;I)V

    invoke-virtual {v8, v5, v9}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-le v9, v6, :cond_a

    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :goto_5
    const/4 v9, 0x1

    goto/16 :goto_2

    :cond_a
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    move/from16 v0, p5

    invoke-static {v9, v0, v3}, Lcom/android/inputmethod/latin/LatinImeLogger;->onAddSuggestedWord(Ljava/lang/String;II)V

    goto :goto_5
.end method

.method public close()V
    .locals 4

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iget-object v3, p0, Lcom/android/inputmethod/latin/Suggest;->mUnigramDictionaries:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    iget-object v3, p0, Lcom/android/inputmethod/latin/Suggest;->mBigramDictionaries:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/inputmethod/latin/Dictionary;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/Dictionary;->close()V

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/android/inputmethod/latin/Suggest;->mHasMainDictionary:Z

    return-void
.end method

.method public getBigramPredictions(Ljava/lang/CharSequence;)Lcom/android/inputmethod/latin/SuggestedWords;
    .locals 10
    .param p1    # Ljava/lang/CharSequence;

    const/4 v2, 0x0

    invoke-static {p1}, Lcom/android/inputmethod/latin/LatinImeLogger;->onStartSuggestion(Ljava/lang/CharSequence;)V

    iput-boolean v2, p0, Lcom/android/inputmethod/latin/Suggest;->mIsFirstCharCapitalized:Z

    iput-boolean v2, p0, Lcom/android/inputmethod/latin/Suggest;->mIsAllUpperCase:Z

    iput v2, p0, Lcom/android/inputmethod/latin/Suggest;->mTrailingSingleQuotesCount:I

    new-instance v0, Ljava/util/ArrayList;

    iget v1, p0, Lcom/android/inputmethod/latin/Suggest;->mPrefMaxSuggestions:I

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/android/inputmethod/latin/Suggest;->mSuggestions:Ljava/util/ArrayList;

    const-string v0, ""

    invoke-static {v0, v2, v2}, Lcom/android/inputmethod/latin/LatinImeLogger;->onAddSuggestedWord(Ljava/lang/String;II)V

    const-string v0, ""

    iput-object v0, p0, Lcom/android/inputmethod/latin/Suggest;->mConsideredWord:Ljava/lang/CharSequence;

    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x3c

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/android/inputmethod/latin/Suggest;->mBigramSuggestions:Ljava/util/ArrayList;

    sget-object v0, Lcom/android/inputmethod/latin/Suggest;->sEmptyWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-direct {p0, p1, v0}, Lcom/android/inputmethod/latin/Suggest;->getAllBigrams(Ljava/lang/CharSequence;Lcom/android/inputmethod/latin/WordComposer;)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/Suggest;->mBigramSuggestions:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/android/inputmethod/latin/Suggest;->mPrefMaxSuggestions:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v9

    const/4 v8, 0x0

    :goto_0
    if-ge v8, v9, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/latin/Suggest;->mBigramSuggestions:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;

    invoke-virtual {p0, v0}, Lcom/android/inputmethod/latin/Suggest;->addBigramToSuggestions(Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;)V

    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/inputmethod/latin/Suggest;->mSuggestions:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;->removeDups(Ljava/util/ArrayList;)V

    new-instance v0, Lcom/android/inputmethod/latin/SuggestedWords;

    iget-object v1, p0, Lcom/android/inputmethod/latin/Suggest;->mSuggestions:Ljava/util/ArrayList;

    const/4 v7, 0x1

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    invoke-direct/range {v0 .. v7}, Lcom/android/inputmethod/latin/SuggestedWords;-><init>(Ljava/util/ArrayList;ZZZZZZ)V

    return-object v0
.end method

.method public getContactsDictionary()Lcom/android/inputmethod/latin/Dictionary;
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/Suggest;->mContactsDict:Lcom/android/inputmethod/latin/Dictionary;

    return-object v0
.end method

.method public getSuggestedWords(Lcom/android/inputmethod/latin/WordComposer;Ljava/lang/CharSequence;Lcom/android/inputmethod/keyboard/ProximityInfo;I)Lcom/android/inputmethod/latin/SuggestedWords;
    .locals 35
    .param p1    # Lcom/android/inputmethod/latin/WordComposer;
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # Lcom/android/inputmethod/keyboard/ProximityInfo;
    .param p4    # I

    invoke-static/range {p2 .. p2}, Lcom/android/inputmethod/latin/LatinImeLogger;->onStartSuggestion(Ljava/lang/CharSequence;)V

    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/latin/WordComposer;->isFirstCharCapitalized()Z

    move-result v5

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/android/inputmethod/latin/Suggest;->mIsFirstCharCapitalized:Z

    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/latin/WordComposer;->isAllUpperCase()Z

    move-result v5

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/android/inputmethod/latin/Suggest;->mIsAllUpperCase:Z

    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/latin/WordComposer;->trailingSingleQuotesCount()I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/inputmethod/latin/Suggest;->mTrailingSingleQuotesCount:I

    new-instance v5, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/inputmethod/latin/Suggest;->mPrefMaxSuggestions:I

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/inputmethod/latin/Suggest;->mSuggestions:Ljava/util/ArrayList;

    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/latin/WordComposer;->getTypedWord()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/inputmethod/latin/Suggest;->mTrailingSingleQuotesCount:I

    if-lez v5, :cond_0

    const/4 v5, 0x0

    invoke-virtual/range {v33 .. v33}, Ljava/lang/String;->length()I

    move-result v6

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/inputmethod/latin/Suggest;->mTrailingSingleQuotesCount:I

    sub-int/2addr v6, v7

    move-object/from16 v0, v33

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    :goto_0
    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, v33

    invoke-static {v0, v5, v6}, Lcom/android/inputmethod/latin/LatinImeLogger;->onAddSuggestedWord(Ljava/lang/String;II)V

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/android/inputmethod/latin/Suggest;->mConsideredWord:Ljava/lang/CharSequence;

    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/latin/WordComposer;->size()I

    move-result v5

    const/4 v6, 0x1

    if-gt v5, v6, :cond_6

    const/4 v5, 0x2

    move/from16 v0, p4

    if-ne v0, v5, :cond_6

    new-instance v5, Ljava/util/ArrayList;

    const/16 v6, 0x3c

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/inputmethod/latin/Suggest;->mBigramSuggestions:Ljava/util/ArrayList;

    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/android/inputmethod/latin/Suggest;->getAllBigrams(Ljava/lang/CharSequence;Lcom/android/inputmethod/latin/WordComposer;)V

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/inputmethod/latin/Suggest;->mBigramSuggestions:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/inputmethod/latin/Suggest;->mPrefMaxSuggestions:I

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v30

    const/16 v28, 0x0

    :goto_1
    move/from16 v0, v28

    move/from16 v1, v30

    if-ge v0, v1, :cond_3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/inputmethod/latin/Suggest;->mBigramSuggestions:Ljava/util/ArrayList;

    move/from16 v0, v28

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/inputmethod/latin/Suggest;->addBigramToSuggestions(Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;)V

    add-int/lit8 v28, v28, 0x1

    goto :goto_1

    :cond_0
    move-object/from16 v8, v33

    goto :goto_0

    :cond_1
    const/4 v5, 0x0

    invoke-virtual {v8, v5}, Ljava/lang/String;->charAt(I)C

    move-result v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v25

    const/16 v23, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/inputmethod/latin/Suggest;->mBigramSuggestions:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v22

    const/16 v28, 0x0

    :goto_2
    move/from16 v0, v28

    move/from16 v1, v22

    if-ge v0, v1, :cond_3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/inputmethod/latin/Suggest;->mBigramSuggestions:Ljava/util/ArrayList;

    move/from16 v0, v28

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;

    const/4 v5, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;->codePointAt(I)I

    move-result v5

    int-to-char v0, v5

    move/from16 v21, v0

    move/from16 v0, v21

    move/from16 v1, v24

    if-eq v0, v1, :cond_2

    move/from16 v0, v21

    move/from16 v1, v25

    if-ne v0, v1, :cond_5

    :cond_2
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/Suggest;->addBigramToSuggestions(Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;)V

    add-int/lit8 v23, v23, 0x1

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/inputmethod/latin/Suggest;->mPrefMaxSuggestions:I

    move/from16 v0, v23

    if-le v0, v5, :cond_5

    :cond_3
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/android/inputmethod/latin/Suggest;->mIsAllUpperCase:Z

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/android/inputmethod/latin/Suggest;->mIsFirstCharCapitalized:Z

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/inputmethod/latin/Suggest;->mWhiteListDictionary:Lcom/android/inputmethod/latin/WhitelistDictionary;

    invoke-virtual {v7, v8}, Lcom/android/inputmethod/latin/WhitelistDictionary;->getWhitelistedWord(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/android/inputmethod/latin/Suggest;->capitalizeWord(ZZLjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v10

    const/4 v5, 0x1

    move/from16 v0, p4

    if-eq v5, v0, :cond_4

    const/4 v5, 0x2

    move/from16 v0, p4

    if-ne v5, v0, :cond_b

    :cond_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/inputmethod/latin/Suggest;->mUnigramDictionaries:Ljava/util/concurrent/ConcurrentHashMap;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/inputmethod/latin/Suggest;->mSuggestions:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/inputmethod/latin/Suggest;->mAutoCorrectionThreshold:F

    move-object/from16 v6, p1

    invoke-static/range {v5 .. v10}, Lcom/android/inputmethod/latin/AutoCorrection;->computeAutoCorrectionWord(Ljava/util/concurrent/ConcurrentHashMap;Lcom/android/inputmethod/latin/WordComposer;Ljava/util/ArrayList;Ljava/lang/CharSequence;FLjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v19

    if-eqz v19, :cond_a

    const/16 v27, 0x1

    :goto_3
    if-eqz v10, :cond_d

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/inputmethod/latin/Suggest;->mTrailingSingleQuotesCount:I

    if-lez v5, :cond_11

    new-instance v32, Ljava/lang/StringBuilder;

    move-object/from16 v0, v32

    invoke-direct {v0, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/inputmethod/latin/Suggest;->mTrailingSingleQuotesCount:I

    add-int/lit8 v28, v5, -0x1

    :goto_4
    if-ltz v28, :cond_c

    const/16 v5, 0x27

    move-object/from16 v0, v32

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->appendCodePoint(I)Ljava/lang/StringBuilder;

    add-int/lit8 v28, v28, -0x1

    goto :goto_4

    :cond_5
    add-int/lit8 v28, v28, 0x1

    goto/16 :goto_2

    :cond_6
    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/latin/WordComposer;->size()I

    move-result v5

    const/4 v6, 0x1

    if-le v5, v6, :cond_3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/inputmethod/latin/Suggest;->mTrailingSingleQuotesCount:I

    if-lez v5, :cond_7

    new-instance v34, Lcom/android/inputmethod/latin/WordComposer;

    move-object/from16 v0, v34

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/android/inputmethod/latin/WordComposer;-><init>(Lcom/android/inputmethod/latin/WordComposer;)V

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/inputmethod/latin/Suggest;->mTrailingSingleQuotesCount:I

    add-int/lit8 v28, v5, -0x1

    :goto_5
    if-ltz v28, :cond_8

    invoke-virtual/range {v34 .. v34}, Lcom/android/inputmethod/latin/WordComposer;->deleteLast()V

    add-int/lit8 v28, v28, -0x1

    goto :goto_5

    :cond_7
    move-object/from16 v34, p1

    :cond_8
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/inputmethod/latin/Suggest;->mUnigramDictionaries:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v5}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v29

    :cond_9
    :goto_6
    invoke-interface/range {v29 .. v29}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface/range {v29 .. v29}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Ljava/lang/String;

    const-string v5, "history_unigram"

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_9

    const-string v5, "whitelist"

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_9

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/inputmethod/latin/Suggest;->mUnigramDictionaries:Ljava/util/concurrent/ConcurrentHashMap;

    move-object/from16 v0, v31

    invoke-virtual {v5, v0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/android/inputmethod/latin/Dictionary;

    move-object/from16 v0, v26

    move-object/from16 v1, v34

    move-object/from16 v2, p2

    move-object/from16 v3, p0

    move-object/from16 v4, p3

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/inputmethod/latin/Dictionary;->getWords(Lcom/android/inputmethod/latin/WordComposer;Ljava/lang/CharSequence;Lcom/android/inputmethod/latin/Dictionary$WordCallback;Lcom/android/inputmethod/keyboard/ProximityInfo;)V

    goto :goto_6

    :cond_a
    const/16 v27, 0x0

    goto/16 :goto_3

    :cond_b
    const/16 v27, 0x0

    goto/16 :goto_3

    :cond_c
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/inputmethod/latin/Suggest;->mSuggestions:Ljava/util/ArrayList;

    const/4 v6, 0x0

    new-instance v7, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const v11, 0x7fffffff

    invoke-direct {v7, v9, v11}, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;-><init>(Ljava/lang/CharSequence;I)V

    invoke-virtual {v5, v6, v7}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    :cond_d
    :goto_7
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/inputmethod/latin/Suggest;->mSuggestions:Ljava/util/ArrayList;

    const/4 v6, 0x0

    new-instance v7, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;

    const v9, 0x7fffffff

    move-object/from16 v0, v33

    invoke-direct {v7, v0, v9}, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;-><init>(Ljava/lang/CharSequence;I)V

    invoke-virtual {v5, v6, v7}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/inputmethod/latin/Suggest;->mSuggestions:Ljava/util/ArrayList;

    invoke-static {v5}, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;->removeDups(Ljava/util/ArrayList;)V

    sget-boolean v5, Lcom/android/inputmethod/latin/Suggest;->DBG:Z

    if-eqz v5, :cond_12

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/inputmethod/latin/Suggest;->mSuggestions:Ljava/util/ArrayList;

    move-object/from16 v0, v33

    invoke-static {v0, v5}, Lcom/android/inputmethod/latin/Suggest;->getSuggestionsInfoListWithDebugInfo(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v12

    :goto_8
    invoke-virtual/range {p0 .. p0}, Lcom/android/inputmethod/latin/Suggest;->getUnigramDictionaries()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/latin/WordComposer;->isFirstCharCapitalized()Z

    move-result v6

    invoke-static {v5, v8, v6}, Lcom/android/inputmethod/latin/AutoCorrection;->allowsToBeAutoCorrected(Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/CharSequence;Z)Z

    move-result v5

    if-eqz v5, :cond_13

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/android/inputmethod/latin/Suggest;->mHasMainDictionary:Z

    if-eqz v5, :cond_13

    const/4 v15, 0x1

    :goto_9
    move/from16 v14, v27

    const/4 v5, 0x1

    move/from16 v0, p4

    if-eq v0, v5, :cond_e

    const/4 v5, 0x2

    move/from16 v0, p4

    if-ne v0, v5, :cond_f

    :cond_e
    if-nez v15, :cond_14

    const/4 v5, 0x1

    :goto_a
    or-int/2addr v14, v5

    :cond_f
    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/latin/WordComposer;->isMostlyCaps()Z

    move-result v5

    if-nez v5, :cond_15

    const/4 v5, 0x1

    :goto_b
    and-int/2addr v14, v5

    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/latin/WordComposer;->isResumed()Z

    move-result v5

    if-nez v5, :cond_16

    const/4 v5, 0x1

    :goto_c
    and-int/2addr v14, v5

    if-eqz v15, :cond_10

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v6, 0x1

    if-le v5, v6, :cond_10

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/inputmethod/latin/Suggest;->mAutoCorrectionThreshold:F

    const/4 v6, 0x0

    cmpl-float v5, v5, v6

    if-lez v5, :cond_10

    const/4 v5, 0x1

    invoke-virtual {v12, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;

    iget-object v5, v5, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;->mWord:Ljava/lang/CharSequence;

    move-object/from16 v0, v33

    invoke-static {v0, v5}, Lcom/android/inputmethod/latin/Suggest;->shouldBlockAutoCorrectionBySafetyNet(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_10

    const/4 v14, 0x0

    :cond_10
    new-instance v11, Lcom/android/inputmethod/latin/SuggestedWords;

    if-nez v15, :cond_17

    const/4 v13, 0x1

    :goto_d
    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    invoke-direct/range {v11 .. v18}, Lcom/android/inputmethod/latin/SuggestedWords;-><init>(Ljava/util/ArrayList;ZZZZZZ)V

    return-object v11

    :cond_11
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/inputmethod/latin/Suggest;->mSuggestions:Ljava/util/ArrayList;

    const/4 v6, 0x0

    new-instance v7, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;

    const v9, 0x7fffffff

    invoke-direct {v7, v10, v9}, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;-><init>(Ljava/lang/CharSequence;I)V

    invoke-virtual {v5, v6, v7}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto/16 :goto_7

    :cond_12
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/inputmethod/latin/Suggest;->mSuggestions:Ljava/util/ArrayList;

    goto :goto_8

    :cond_13
    const/4 v15, 0x0

    goto :goto_9

    :cond_14
    const/4 v5, 0x0

    goto :goto_a

    :cond_15
    const/4 v5, 0x0

    goto :goto_b

    :cond_16
    const/4 v5, 0x0

    goto :goto_c

    :cond_17
    const/4 v13, 0x0

    goto :goto_d
.end method

.method public getUnigramDictionaries()Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/inputmethod/latin/Dictionary;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/inputmethod/latin/Suggest;->mUnigramDictionaries:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method

.method public hasMainDictionary()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/inputmethod/latin/Suggest;->mHasMainDictionary:Z

    return v0
.end method

.method public resetMainDict(Landroid/content/Context;Ljava/util/Locale;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/util/Locale;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/inputmethod/latin/Suggest;->mHasMainDictionary:Z

    new-instance v0, Lcom/android/inputmethod/latin/Suggest$1;

    const-string v1, "InitializeBinaryDictionary"

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/android/inputmethod/latin/Suggest$1;-><init>(Lcom/android/inputmethod/latin/Suggest;Ljava/lang/String;Landroid/content/Context;Ljava/util/Locale;)V

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/Suggest$1;->start()V

    return-void
.end method

.method public setAutoCorrectionThreshold(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/android/inputmethod/latin/Suggest;->mAutoCorrectionThreshold:F

    return-void
.end method

.method public setContactsDictionary(Lcom/android/inputmethod/latin/Dictionary;)V
    .locals 2
    .param p1    # Lcom/android/inputmethod/latin/Dictionary;

    iput-object p1, p0, Lcom/android/inputmethod/latin/Suggest;->mContactsDict:Lcom/android/inputmethod/latin/Dictionary;

    iget-object v0, p0, Lcom/android/inputmethod/latin/Suggest;->mUnigramDictionaries:Ljava/util/concurrent/ConcurrentHashMap;

    const-string v1, "contacts"

    invoke-static {v0, v1, p1}, Lcom/android/inputmethod/latin/Suggest;->addOrReplaceDictionary(Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/String;Lcom/android/inputmethod/latin/Dictionary;)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/Suggest;->mBigramDictionaries:Ljava/util/concurrent/ConcurrentHashMap;

    const-string v1, "contacts"

    invoke-static {v0, v1, p1}, Lcom/android/inputmethod/latin/Suggest;->addOrReplaceDictionary(Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/String;Lcom/android/inputmethod/latin/Dictionary;)V

    return-void
.end method

.method public setUserDictionary(Lcom/android/inputmethod/latin/Dictionary;)V
    .locals 2
    .param p1    # Lcom/android/inputmethod/latin/Dictionary;

    iget-object v0, p0, Lcom/android/inputmethod/latin/Suggest;->mUnigramDictionaries:Ljava/util/concurrent/ConcurrentHashMap;

    const-string v1, "user"

    invoke-static {v0, v1, p1}, Lcom/android/inputmethod/latin/Suggest;->addOrReplaceDictionary(Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/String;Lcom/android/inputmethod/latin/Dictionary;)V

    return-void
.end method

.method public setUserHistoryDictionary(Lcom/android/inputmethod/latin/Dictionary;)V
    .locals 2
    .param p1    # Lcom/android/inputmethod/latin/Dictionary;

    iget-object v0, p0, Lcom/android/inputmethod/latin/Suggest;->mUnigramDictionaries:Ljava/util/concurrent/ConcurrentHashMap;

    const-string v1, "history_unigram"

    invoke-static {v0, v1, p1}, Lcom/android/inputmethod/latin/Suggest;->addOrReplaceDictionary(Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/String;Lcom/android/inputmethod/latin/Dictionary;)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/Suggest;->mBigramDictionaries:Ljava/util/concurrent/ConcurrentHashMap;

    const-string v1, "history_bigram"

    invoke-static {v0, v1, p1}, Lcom/android/inputmethod/latin/Suggest;->addOrReplaceDictionary(Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/String;Lcom/android/inputmethod/latin/Dictionary;)V

    return-void
.end method
