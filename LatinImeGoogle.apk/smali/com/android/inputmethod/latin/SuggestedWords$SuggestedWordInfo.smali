.class public Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;
.super Ljava/lang/Object;
.source "SuggestedWords.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/latin/SuggestedWords;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SuggestedWordInfo"
.end annotation


# instance fields
.field public final mCodePointCount:I

.field private mDebugString:Ljava/lang/String;

.field public final mScore:I

.field public final mWord:Ljava/lang/CharSequence;

.field private final mWordStr:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;I)V
    .locals 3
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;->mDebugString:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;->mWordStr:Ljava/lang/String;

    iput-object p1, p0, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;->mWord:Ljava/lang/CharSequence;

    iput p2, p0, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;->mScore:I

    iget-object v0, p0, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;->mWordStr:Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;->mWordStr:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->codePointCount(II)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;->mCodePointCount:I

    return-void
.end method

.method public static removeDups(Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v5, 0x1

    if-gt v4, v5, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v1, 0x1

    :goto_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_0

    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v1, :cond_3

    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;

    iget-object v4, v0, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;->mWord:Ljava/lang/CharSequence;

    iget-object v5, v3, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;->mWord:Ljava/lang/CharSequence;

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    iget v4, v0, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;->mScore:I

    iget v5, v3, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;->mScore:I

    if-ge v4, v5, :cond_2

    move v2, v1

    :cond_2
    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    add-int/lit8 v1, v1, -0x1

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method


# virtual methods
.method public codePointAt(I)I
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;->mWordStr:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    return v0
.end method

.method public codePointCount()I
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;->mCodePointCount:I

    return v0
.end method

.method public getDebugString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;->mDebugString:Ljava/lang/String;

    return-object v0
.end method

.method public setDebugString(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Debug info is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;->mDebugString:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;->mDebugString:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;->mWordStr:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;->mWordStr:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;->mDebugString:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
