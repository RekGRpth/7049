.class public Lcom/android/inputmethod/latin/SynchronouslyLoadedContactsBinaryDictionary;
.super Lcom/android/inputmethod/latin/ContactsBinaryDictionary;
.source "SynchronouslyLoadedContactsBinaryDictionary.java"


# instance fields
.field private mClosed:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/Locale;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/util/Locale;

    const/4 v0, 0x4

    invoke-direct {p0, p1, v0, p2}, Lcom/android/inputmethod/latin/ContactsBinaryDictionary;-><init>(Landroid/content/Context;ILjava/util/Locale;)V

    return-void
.end method


# virtual methods
.method public declared-synchronized close()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/android/inputmethod/latin/SynchronouslyLoadedContactsBinaryDictionary;->mClosed:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/android/inputmethod/latin/SynchronouslyLoadedContactsBinaryDictionary;->mClosed:Z

    invoke-super {p0}, Lcom/android/inputmethod/latin/ContactsBinaryDictionary;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getWords(Lcom/android/inputmethod/latin/WordComposer;Ljava/lang/CharSequence;Lcom/android/inputmethod/latin/Dictionary$WordCallback;Lcom/android/inputmethod/keyboard/ProximityInfo;)V
    .locals 1
    .param p1    # Lcom/android/inputmethod/latin/WordComposer;
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # Lcom/android/inputmethod/latin/Dictionary$WordCallback;
    .param p4    # Lcom/android/inputmethod/keyboard/ProximityInfo;

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/android/inputmethod/latin/SynchronouslyLoadedContactsBinaryDictionary;->syncReloadDictionaryIfRequired()V

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/android/inputmethod/latin/SynchronouslyLoadedContactsBinaryDictionary;->getWordsInner(Lcom/android/inputmethod/latin/WordComposer;Ljava/lang/CharSequence;Lcom/android/inputmethod/latin/Dictionary$WordCallback;Lcom/android/inputmethod/keyboard/ProximityInfo;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isValidWord(Ljava/lang/CharSequence;)Z
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/android/inputmethod/latin/SynchronouslyLoadedContactsBinaryDictionary;->syncReloadDictionaryIfRequired()V

    invoke-virtual {p0, p1}, Lcom/android/inputmethod/latin/SynchronouslyLoadedContactsBinaryDictionary;->isValidWordInner(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
