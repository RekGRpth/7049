.class Lcom/android/inputmethod/latin/UserHistoryDictionary$UpdateDbTask;
.super Landroid/os/AsyncTask;
.source "UserHistoryDictionary.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/latin/UserHistoryDictionary;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "UpdateDbTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final mBigramList:Lcom/android/inputmethod/latin/UserHistoryDictionaryBigramList;

.field private final mDbHelper:Lcom/android/inputmethod/latin/UserHistoryDictionary$DatabaseHelper;

.field private final mLocale:Ljava/lang/String;

.field private final mPrefs:Landroid/content/SharedPreferences;

.field private final mUserHistoryDictionary:Lcom/android/inputmethod/latin/UserHistoryDictionary;


# direct methods
.method public constructor <init>(Lcom/android/inputmethod/latin/UserHistoryDictionary$DatabaseHelper;Lcom/android/inputmethod/latin/UserHistoryDictionaryBigramList;Ljava/lang/String;Lcom/android/inputmethod/latin/UserHistoryDictionary;Landroid/content/SharedPreferences;)V
    .locals 0
    .param p1    # Lcom/android/inputmethod/latin/UserHistoryDictionary$DatabaseHelper;
    .param p2    # Lcom/android/inputmethod/latin/UserHistoryDictionaryBigramList;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/android/inputmethod/latin/UserHistoryDictionary;
    .param p5    # Landroid/content/SharedPreferences;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p2, p0, Lcom/android/inputmethod/latin/UserHistoryDictionary$UpdateDbTask;->mBigramList:Lcom/android/inputmethod/latin/UserHistoryDictionaryBigramList;

    iput-object p3, p0, Lcom/android/inputmethod/latin/UserHistoryDictionary$UpdateDbTask;->mLocale:Ljava/lang/String;

    iput-object p1, p0, Lcom/android/inputmethod/latin/UserHistoryDictionary$UpdateDbTask;->mDbHelper:Lcom/android/inputmethod/latin/UserHistoryDictionary$DatabaseHelper;

    iput-object p4, p0, Lcom/android/inputmethod/latin/UserHistoryDictionary$UpdateDbTask;->mUserHistoryDictionary:Lcom/android/inputmethod/latin/UserHistoryDictionary;

    iput-object p5, p0, Lcom/android/inputmethod/latin/UserHistoryDictionary$UpdateDbTask;->mPrefs:Landroid/content/SharedPreferences;

    return-void
.end method

.method private static checkPruneData(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 14
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;

    const/4 v2, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    const-string v0, "PRAGMA foreign_keys = ON;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "frequency"

    new-array v2, v2, [Ljava/lang/String;

    const-string v0, "pair_id"

    aput-object v0, v2, v4

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v13

    # getter for: Lcom/android/inputmethod/latin/UserHistoryDictionary;->sMaxHistoryBigrams:I
    invoke-static {}, Lcom/android/inputmethod/latin/UserHistoryDictionary;->access$000()I

    move-result v0

    if-le v13, v0, :cond_0

    # getter for: Lcom/android/inputmethod/latin/UserHistoryDictionary;->sMaxHistoryBigrams:I
    invoke-static {}, Lcom/android/inputmethod/latin/UserHistoryDictionary;->access$000()I

    move-result v0

    sub-int v0, v13, v0

    # getter for: Lcom/android/inputmethod/latin/UserHistoryDictionary;->sDeleteHistoryBigrams:I
    invoke-static {}, Lcom/android/inputmethod/latin/UserHistoryDictionary;->access$100()I

    move-result v1

    add-int v10, v0, v1

    const-string v0, "pair_id"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    const/4 v9, 0x0

    :goto_0
    if-ge v9, v10, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v8, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    const-string v0, "main"

    const-string v1, "_id=?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v11, v2, v3

    invoke-virtual {p0, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    return-void

    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private doLoadTaskLocked(Landroid/database/sqlite/SQLiteDatabase;)Ljava/lang/Void;
    .locals 32
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;

    sget-boolean v2, Lcom/android/inputmethod/latin/UserHistoryDictionary;->PROFILE_SAVE_RESTORE:Z

    if-eqz v2, :cond_6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    :goto_0
    const/16 v28, 0x0

    const/16 v27, 0x0

    const/16 v26, 0x0

    const-string v2, "PRAGMA foreign_keys = ON;"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/inputmethod/latin/UserHistoryDictionary$UpdateDbTask;->mBigramList:Lcom/android/inputmethod/latin/UserHistoryDictionaryBigramList;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/UserHistoryDictionaryBigramList;->size()I

    move-result v2

    # getter for: Lcom/android/inputmethod/latin/UserHistoryDictionary;->sMaxHistoryBigrams:I
    invoke-static {}, Lcom/android/inputmethod/latin/UserHistoryDictionary;->access$000()I

    move-result v3

    if-gt v2, v3, :cond_7

    const/4 v10, 0x1

    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/inputmethod/latin/UserHistoryDictionary$UpdateDbTask;->mBigramList:Lcom/android/inputmethod/latin/UserHistoryDictionaryBigramList;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/UserHistoryDictionaryBigramList;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :cond_0
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_f

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/inputmethod/latin/UserHistoryDictionary$UpdateDbTask;->mBigramList:Lcom/android/inputmethod/latin/UserHistoryDictionaryBigramList;

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Lcom/android/inputmethod/latin/UserHistoryDictionaryBigramList;->getBigrams(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :cond_1
    :goto_2
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Ljava/lang/String;

    sget-boolean v2, Lcom/android/inputmethod/latin/UserHistoryDictionary;->PROFILE_SAVE_RESTORE:Z

    if-eqz v2, :cond_2

    add-int/lit8 v28, v28, 0x1

    :cond_2
    if-nez v29, :cond_8

    const/16 v16, 0x2

    invoke-virtual/range {v30 .. v31}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Byte;

    invoke-virtual {v2}, Ljava/lang/Byte;->byteValue()B

    move-result v25

    const/4 v2, 0x2

    move/from16 v0, v25

    if-eq v0, v2, :cond_1

    :goto_3
    const/4 v11, 0x0

    if-eqz v29, :cond_c

    :try_start_0
    const-string v3, "main"

    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v5, "_id"

    aput-object v5, v4, v2

    const-string v5, "word1=? AND word2=? AND locale=?"

    const/4 v2, 0x3

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v29, v6, v2

    const/4 v2, 0x1

    aput-object v31, v6, v2

    const/4 v2, 0x2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/inputmethod/latin/UserHistoryDictionary$UpdateDbTask;->mLocale:Ljava/lang/String;

    aput-object v7, v6, v2

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    :goto_4
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_d

    sget-boolean v2, Lcom/android/inputmethod/latin/UserHistoryDictionary;->PROFILE_SAVE_RESTORE:Z

    if-eqz v2, :cond_3

    add-int/lit8 v26, v26, 0x1

    :cond_3
    const-string v2, "_id"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v23

    const-string v2, "frequency"

    const-string v3, "pair_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    :goto_5
    if-lez v16, :cond_5

    sget-boolean v2, Lcom/android/inputmethod/latin/UserHistoryDictionary;->PROFILE_SAVE_RESTORE:Z

    if-eqz v2, :cond_4

    add-int/lit8 v27, v27, 0x1

    :cond_4
    const-string v2, "frequency"

    const/4 v3, 0x0

    move/from16 v0, v23

    move/from16 v1, v16

    invoke-static {v0, v1}, Lcom/android/inputmethod/latin/UserHistoryDictionary$UpdateDbTask;->getFrequencyContentValues(II)Landroid/content/ContentValues;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/inputmethod/latin/UserHistoryDictionary$UpdateDbTask;->mBigramList:Lcom/android/inputmethod/latin/UserHistoryDictionaryBigramList;

    move/from16 v0, v16

    int-to-byte v3, v0

    move-object/from16 v0, v29

    move-object/from16 v1, v31

    invoke-virtual {v2, v0, v1, v3}, Lcom/android/inputmethod/latin/UserHistoryDictionaryBigramList;->updateBigram(Ljava/lang/String;Ljava/lang/String;B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_5
    if-eqz v11, :cond_1

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    goto/16 :goto_2

    :cond_6
    const-wide/16 v20, 0x0

    goto/16 :goto_0

    :cond_7
    const/4 v10, 0x0

    goto/16 :goto_1

    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/inputmethod/latin/UserHistoryDictionary$UpdateDbTask;->mUserHistoryDictionary:Lcom/android/inputmethod/latin/UserHistoryDictionary;

    move-object/from16 v0, v29

    move-object/from16 v1, v31

    invoke-virtual {v2, v0, v1}, Lcom/android/inputmethod/latin/UserHistoryDictionary;->getBigramWord(Ljava/lang/String;Ljava/lang/String;)Lcom/android/inputmethod/latin/ExpandableDictionary$NextWord;

    move-result-object v22

    if-eqz v22, :cond_b

    invoke-interface/range {v22 .. v22}, Lcom/android/inputmethod/latin/ExpandableDictionary$NextWord;->getFcParams()Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;

    move-result-object v15

    invoke-virtual/range {v30 .. v31}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Byte;

    invoke-virtual {v2}, Ljava/lang/Byte;->byteValue()B

    move-result v25

    invoke-virtual {v15}, Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;->getFc()B

    move-result v14

    invoke-virtual {v15}, Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;->isValid()Z

    move-result v19

    if-lez v25, :cond_9

    move/from16 v0, v25

    if-eq v0, v14, :cond_1

    :cond_9
    move/from16 v0, v19

    invoke-static {v14, v0, v10}, Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils;->needsToSave(BZZ)Z

    move-result v2

    if-eqz v2, :cond_a

    move/from16 v16, v14

    goto/16 :goto_3

    :cond_a
    const/16 v16, -0x1

    goto/16 :goto_3

    :cond_b
    const/16 v16, -0x1

    goto/16 :goto_3

    :cond_c
    :try_start_1
    const-string v3, "main"

    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v5, "_id"

    aput-object v5, v4, v2

    const-string v5, "word1 IS NULL AND word2=? AND locale=?"

    const/4 v2, 0x2

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v31, v6, v2

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/inputmethod/latin/UserHistoryDictionary$UpdateDbTask;->mLocale:Ljava/lang/String;

    aput-object v7, v6, v2

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    goto/16 :goto_4

    :cond_d
    const-string v2, "main"

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/inputmethod/latin/UserHistoryDictionary$UpdateDbTask;->mLocale:Ljava/lang/String;

    move-object/from16 v0, v29

    move-object/from16 v1, v31

    invoke-static {v0, v1, v4}, Lcom/android/inputmethod/latin/UserHistoryDictionary$UpdateDbTask;->getContentValues(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Long;->intValue()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v23

    goto/16 :goto_5

    :catchall_0
    move-exception v2

    if-eqz v11, :cond_e

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_e
    throw v2

    :cond_f
    invoke-static/range {p1 .. p1}, Lcom/android/inputmethod/latin/UserHistoryDictionary$UpdateDbTask;->checkPruneData(Landroid/database/sqlite/SQLiteDatabase;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/inputmethod/latin/UserHistoryDictionary$UpdateDbTask;->mPrefs:Landroid/content/SharedPreferences;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/inputmethod/latin/UserHistoryDictionary$UpdateDbTask;->mLocale:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/android/inputmethod/latin/SettingsValues;->setLastUserHistoryWriteTime(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    sget-boolean v2, Lcom/android/inputmethod/latin/UserHistoryDictionary;->PROFILE_SAVE_RESTORE:Z

    if-eqz v2, :cond_10

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v12, v2, v20

    const-string v2, "UserHistoryDictionary"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "PROF: Write User HistoryDictionary: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/inputmethod/latin/UserHistoryDictionary$UpdateDbTask;->mLocale:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "ms. Total: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v28

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ". Insert: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ". Delete: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_10
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    const/4 v2, 0x0

    return-object v2
.end method

.method private static getContentValues(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;
    .locals 2
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v0, Landroid/content/ContentValues;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Landroid/content/ContentValues;-><init>(I)V

    const-string v1, "word1"

    invoke-virtual {v0, v1, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "word2"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "locale"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private static getFrequencyContentValues(II)Landroid/content/ContentValues;
    .locals 3
    .param p0    # I
    .param p1    # I

    new-instance v0, Landroid/content/ContentValues;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/content/ContentValues;-><init>(I)V

    const-string v1, "pair_id"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "freq"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    return-object v0
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/inputmethod/latin/UserHistoryDictionary$UpdateDbTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 3
    .param p1    # [Ljava/lang/Void;

    const/4 v1, 0x0

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/android/inputmethod/latin/UserHistoryDictionary$UpdateDbTask;->mUserHistoryDictionary:Lcom/android/inputmethod/latin/UserHistoryDictionary;

    # getter for: Lcom/android/inputmethod/latin/UserHistoryDictionary;->mBigramListLock:Ljava/util/concurrent/locks/ReentrantLock;
    invoke-static {v2}, Lcom/android/inputmethod/latin/UserHistoryDictionary;->access$200(Lcom/android/inputmethod/latin/UserHistoryDictionary;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->tryLock()Z

    move-result v2

    if-eqz v2, :cond_1

    :try_start_0
    iget-object v2, p0, Lcom/android/inputmethod/latin/UserHistoryDictionary$UpdateDbTask;->mDbHelper:Lcom/android/inputmethod/latin/UserHistoryDictionary$DatabaseHelper;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/UserHistoryDictionary$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteCantOpenDatabaseException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_0
    if-nez v0, :cond_2

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :cond_0
    iget-object v2, p0, Lcom/android/inputmethod/latin/UserHistoryDictionary$UpdateDbTask;->mUserHistoryDictionary:Lcom/android/inputmethod/latin/UserHistoryDictionary;

    # getter for: Lcom/android/inputmethod/latin/UserHistoryDictionary;->mBigramListLock:Ljava/util/concurrent/locks/ReentrantLock;
    invoke-static {v2}, Lcom/android/inputmethod/latin/UserHistoryDictionary;->access$200(Lcom/android/inputmethod/latin/UserHistoryDictionary;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    :cond_1
    :goto_1
    return-object v1

    :cond_2
    :try_start_1
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    invoke-direct {p0, v0}, Lcom/android/inputmethod/latin/UserHistoryDictionary$UpdateDbTask;->doLoadTaskLocked(Landroid/database/sqlite/SQLiteDatabase;)Ljava/lang/Void;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :cond_3
    iget-object v2, p0, Lcom/android/inputmethod/latin/UserHistoryDictionary$UpdateDbTask;->mUserHistoryDictionary:Lcom/android/inputmethod/latin/UserHistoryDictionary;

    # getter for: Lcom/android/inputmethod/latin/UserHistoryDictionary;->mBigramListLock:Ljava/util/concurrent/locks/ReentrantLock;
    invoke-static {v2}, Lcom/android/inputmethod/latin/UserHistoryDictionary;->access$200(Lcom/android/inputmethod/latin/UserHistoryDictionary;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_1

    :catchall_0
    move-exception v1

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :cond_4
    iget-object v2, p0, Lcom/android/inputmethod/latin/UserHistoryDictionary$UpdateDbTask;->mUserHistoryDictionary:Lcom/android/inputmethod/latin/UserHistoryDictionary;

    # getter for: Lcom/android/inputmethod/latin/UserHistoryDictionary;->mBigramListLock:Ljava/util/concurrent/locks/ReentrantLock;
    invoke-static {v2}, Lcom/android/inputmethod/latin/UserHistoryDictionary;->access$200(Lcom/android/inputmethod/latin/UserHistoryDictionary;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v1

    :catch_0
    move-exception v2

    goto :goto_0
.end method
