.class public Lcom/android/inputmethod/latin/UserHistoryDictionary;
.super Lcom/android/inputmethod/latin/ExpandableDictionary;
.source "UserHistoryDictionary.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/inputmethod/latin/UserHistoryDictionary$UpdateDbTask;,
        Lcom/android/inputmethod/latin/UserHistoryDictionary$DatabaseHelper;
    }
.end annotation


# static fields
.field public static final PROFILE_SAVE_RESTORE:Z

.field private static sDeleteHistoryBigrams:I

.field private static final sDictProjectionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final sLangDictCache:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/ref/SoftReference",
            "<",
            "Lcom/android/inputmethod/latin/UserHistoryDictionary;",
            ">;>;"
        }
    .end annotation
.end field

.field private static sMaxHistoryBigrams:I

.field private static sOpenHelper:Lcom/android/inputmethod/latin/UserHistoryDictionary$DatabaseHelper;


# instance fields
.field private final mBigramList:Lcom/android/inputmethod/latin/UserHistoryDictionaryBigramList;

.field private final mBigramListLock:Ljava/util/concurrent/locks/ReentrantLock;

.field private final mLocale:Ljava/lang/String;

.field private final mPrefs:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    sget-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sDBG:Z

    sput-boolean v0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->PROFILE_SAVE_RESTORE:Z

    const/16 v0, 0x2710

    sput v0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->sMaxHistoryBigrams:I

    const/16 v0, 0x3e8

    sput v0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->sDeleteHistoryBigrams:I

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->sLangDictCache:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->sDictProjectionMap:Ljava/util/HashMap;

    sget-object v0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->sDictProjectionMap:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->sDictProjectionMap:Ljava/util/HashMap;

    const-string v1, "word1"

    const-string v2, "word1"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->sDictProjectionMap:Ljava/util/HashMap;

    const-string v1, "word2"

    const-string v2, "word2"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->sDictProjectionMap:Ljava/util/HashMap;

    const-string v1, "locale"

    const-string v2, "locale"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->sDictProjectionMap:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->sDictProjectionMap:Ljava/util/HashMap;

    const-string v1, "pair_id"

    const-string v2, "pair_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->sDictProjectionMap:Ljava/util/HashMap;

    const-string v1, "freq"

    const-string v2, "freq"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x0

    sput-object v0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->sOpenHelper:Lcom/android/inputmethod/latin/UserHistoryDictionary$DatabaseHelper;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;ILandroid/content/SharedPreferences;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # Landroid/content/SharedPreferences;

    invoke-direct {p0, p1, p3}, Lcom/android/inputmethod/latin/ExpandableDictionary;-><init>(Landroid/content/Context;I)V

    new-instance v0, Lcom/android/inputmethod/latin/UserHistoryDictionaryBigramList;

    invoke-direct {v0}, Lcom/android/inputmethod/latin/UserHistoryDictionaryBigramList;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->mBigramList:Lcom/android/inputmethod/latin/UserHistoryDictionaryBigramList;

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->mBigramListLock:Ljava/util/concurrent/locks/ReentrantLock;

    iput-object p2, p0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->mLocale:Ljava/lang/String;

    iput-object p4, p0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->mPrefs:Landroid/content/SharedPreferences;

    sget-object v0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->sOpenHelper:Lcom/android/inputmethod/latin/UserHistoryDictionary$DatabaseHelper;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/inputmethod/latin/UserHistoryDictionary$DatabaseHelper;

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/UserHistoryDictionary;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/inputmethod/latin/UserHistoryDictionary$DatabaseHelper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->sOpenHelper:Lcom/android/inputmethod/latin/UserHistoryDictionary$DatabaseHelper;

    :cond_0
    iget-object v0, p0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->mLocale:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->mLocale:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/UserHistoryDictionary;->loadDictionary()V

    :cond_1
    return-void
.end method

.method static synthetic access$000()I
    .locals 1

    sget v0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->sMaxHistoryBigrams:I

    return v0
.end method

.method static synthetic access$100()I
    .locals 1

    sget v0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->sDeleteHistoryBigrams:I

    return v0
.end method

.method static synthetic access$200(Lcom/android/inputmethod/latin/UserHistoryDictionary;)Ljava/util/concurrent/locks/ReentrantLock;
    .locals 1
    .param p0    # Lcom/android/inputmethod/latin/UserHistoryDictionary;

    iget-object v0, p0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->mBigramListLock:Ljava/util/concurrent/locks/ReentrantLock;

    return-object v0
.end method

.method private flushPendingWrites()V
    .locals 6

    iget-object v0, p0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->mBigramListLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->isLocked()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/android/inputmethod/latin/UserHistoryDictionary$UpdateDbTask;

    sget-object v1, Lcom/android/inputmethod/latin/UserHistoryDictionary;->sOpenHelper:Lcom/android/inputmethod/latin/UserHistoryDictionary$DatabaseHelper;

    iget-object v2, p0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->mBigramList:Lcom/android/inputmethod/latin/UserHistoryDictionaryBigramList;

    iget-object v3, p0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->mLocale:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->mPrefs:Landroid/content/SharedPreferences;

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/inputmethod/latin/UserHistoryDictionary$UpdateDbTask;-><init>(Lcom/android/inputmethod/latin/UserHistoryDictionary$DatabaseHelper;Lcom/android/inputmethod/latin/UserHistoryDictionaryBigramList;Ljava/lang/String;Lcom/android/inputmethod/latin/UserHistoryDictionary;Landroid/content/SharedPreferences;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/UserHistoryDictionary$UpdateDbTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;Ljava/lang/String;ILandroid/content/SharedPreferences;)Lcom/android/inputmethod/latin/UserHistoryDictionary;
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Landroid/content/SharedPreferences;

    const-class v3, Lcom/android/inputmethod/latin/UserHistoryDictionary;

    monitor-enter v3

    :try_start_0
    sget-object v2, Lcom/android/inputmethod/latin/UserHistoryDictionary;->sLangDictCache:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, Lcom/android/inputmethod/latin/UserHistoryDictionary;->sLangDictCache:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/SoftReference;

    if-nez v1, :cond_1

    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    sget-boolean v2, Lcom/android/inputmethod/latin/UserHistoryDictionary;->PROFILE_SAVE_RESTORE:Z

    if-eqz v2, :cond_0

    const-string v2, "UserHistoryDictionary"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Use cached UserHistoryDictionary for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_1
    monitor-exit v3

    return-object v0

    :cond_1
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/inputmethod/latin/UserHistoryDictionary;

    move-object v0, v2

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/android/inputmethod/latin/UserHistoryDictionary;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/android/inputmethod/latin/UserHistoryDictionary;-><init>(Landroid/content/Context;Ljava/lang/String;ILandroid/content/SharedPreferences;)V

    sget-object v2, Lcom/android/inputmethod/latin/UserHistoryDictionary;->sLangDictCache:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v4, Ljava/lang/ref/SoftReference;

    invoke-direct {v4, v0}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v2, p1, v4}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method private loadDictionaryAsyncLocked()V
    .locals 19

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->mPrefs:Landroid/content/SharedPreferences;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->mLocale:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-static {v1, v0}, Lcom/android/inputmethod/latin/SettingsValues;->getLastUserHistoryWriteTime(Landroid/content/SharedPreferences;Ljava/lang/String;)J

    move-result-wide v5

    const-wide/16 v16, 0x0

    cmp-long v1, v5, v16

    if-nez v1, :cond_1

    const/4 v11, 0x1

    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const-string v1, "locale=?"

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->mLocale:Ljava/lang/String;

    move-object/from16 v18, v0

    aput-object v18, v16, v17

    move-object/from16 v0, v16

    invoke-static {v1, v0}, Lcom/android/inputmethod/latin/UserHistoryDictionary;->query(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    if-nez v7, :cond_2

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/4 v11, 0x0

    goto :goto_0

    :cond_2
    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_7

    const-string v1, "word1"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    const-string v1, "word2"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    const-string v1, "freq"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    :goto_2
    invoke-interface {v7}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-nez v1, :cond_7

    invoke-interface {v7, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-interface {v7, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-interface {v7, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-nez v12, :cond_5

    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-super {v0, v14, v1, v2}, Lcom/android/inputmethod/latin/ExpandableDictionary;->addWord(Ljava/lang/String;Ljava/lang/String;I)V

    :cond_3
    :goto_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->mBigramList:Lcom/android/inputmethod/latin/UserHistoryDictionaryBigramList;

    int-to-byte v0, v2

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v1, v12, v14, v0}, Lcom/android/inputmethod/latin/UserHistoryDictionaryBigramList;->addBigram(Ljava/lang/String;Ljava/lang/String;B)V

    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v1

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    sget-boolean v16, Lcom/android/inputmethod/latin/UserHistoryDictionary;->PROFILE_SAVE_RESTORE:Z

    if-eqz v16, :cond_4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    sub-long v8, v16, v3

    const-string v16, "UserHistoryDictionary"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "PROF: Load User HistoryDictionary: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->mLocale:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, ", "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "ms."

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    throw v1

    :cond_5
    :try_start_1
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v16, 0x30

    move/from16 v0, v16

    if-ge v1, v0, :cond_3

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v16, 0x30

    move/from16 v0, v16

    if-ge v1, v0, :cond_3

    if-eqz v11, :cond_6

    new-instance v1, Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;

    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-direct {v1, v0}, Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;-><init>(Z)V

    :goto_4
    move-object/from16 v0, p0

    invoke-super {v0, v12, v14, v1}, Lcom/android/inputmethod/latin/ExpandableDictionary;->setBigramAndGetFrequency(Ljava/lang/String;Ljava/lang/String;Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;)I

    goto :goto_3

    :cond_6
    new-instance v1, Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;

    invoke-direct/range {v1 .. v6}, Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;-><init>(IJJ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_4

    :cond_7
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    sget-boolean v1, Lcom/android/inputmethod/latin/UserHistoryDictionary;->PROFILE_SAVE_RESTORE:Z

    if-eqz v1, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    sub-long v8, v16, v3

    const-string v1, "UserHistoryDictionary"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "PROF: Load User HistoryDictionary: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->mLocale:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "ms."

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method private static query(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10
    .param p0    # Ljava/lang/String;
    .param p1    # [Ljava/lang/String;

    const/4 v8, 0x0

    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    const-string v2, "main INNER JOIN frequency ON (main._id=frequency.pair_id)"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    sget-object v2, Lcom/android/inputmethod/latin/UserHistoryDictionary;->sDictProjectionMap:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    :try_start_0
    sget-object v2, Lcom/android/inputmethod/latin/UserHistoryDictionary;->sOpenHelper:Lcom/android/inputmethod/latin/UserHistoryDictionary$DatabaseHelper;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/UserHistoryDictionary$DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "word1"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "word2"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "freq"

    aput-object v4, v2, v3

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v3, p0

    move-object v4, p1

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteCantOpenDatabaseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    :goto_0
    return-object v8

    :catch_0
    move-exception v9

    goto :goto_0
.end method


# virtual methods
.method public addToUserHistory(Ljava/lang/String;Ljava/lang/String;Z)I
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    iget-object v1, p0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->mBigramListLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->tryLock()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    const/4 v2, 0x2

    :try_start_0
    invoke-super {p0, p2, v1, v2}, Lcom/android/inputmethod/latin/ExpandableDictionary;->addWord(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->mBigramListLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    :goto_0
    return v0

    :cond_0
    if-nez p1, :cond_1

    const/4 v0, 0x2

    :goto_1
    :try_start_1
    iget-object v1, p0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->mBigramList:Lcom/android/inputmethod/latin/UserHistoryDictionaryBigramList;

    invoke-virtual {v1, p1, p2}, Lcom/android/inputmethod/latin/UserHistoryDictionaryBigramList;->addBigram(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v1, p0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->mBigramListLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    :cond_1
    :try_start_2
    new-instance v1, Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;

    invoke-direct {v1, p3}, Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;-><init>(Z)V

    invoke-super {p0, p1, p2, v1}, Lcom/android/inputmethod/latin/ExpandableDictionary;->setBigramAndGetFrequency(Ljava/lang/String;Ljava/lang/String;Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    goto :goto_1

    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->mBigramListLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v1

    :cond_2
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public cancelAddingUserHistory(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->mBigramListLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->mBigramList:Lcom/android/inputmethod/latin/UserHistoryDictionaryBigramList;

    invoke-virtual {v0, p1, p2}, Lcom/android/inputmethod/latin/UserHistoryDictionaryBigramList;->removeBigram(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0, p1, p2}, Lcom/android/inputmethod/latin/ExpandableDictionary;->removeBigram(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    iget-object v1, p0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->mBigramListLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->mBigramListLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->mBigramListLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public close()V
    .locals 0

    invoke-direct {p0}, Lcom/android/inputmethod/latin/UserHistoryDictionary;->flushPendingWrites()V

    return-void
.end method

.method public declared-synchronized isValidWord(Ljava/lang/CharSequence;)Z
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    monitor-enter p0

    const/4 v0, 0x0

    monitor-exit p0

    return v0
.end method

.method public loadDictionaryAsync()V
    .locals 2

    iget-object v0, p0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->mBigramListLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    :try_start_0
    invoke-direct {p0}, Lcom/android/inputmethod/latin/UserHistoryDictionary;->loadDictionaryAsyncLocked()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->mBigramListLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/android/inputmethod/latin/UserHistoryDictionary;->mBigramListLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method
