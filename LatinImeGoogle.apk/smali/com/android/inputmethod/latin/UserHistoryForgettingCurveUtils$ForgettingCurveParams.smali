.class public Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;
.super Ljava/lang/Object;
.source "UserHistoryForgettingCurveUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ForgettingCurveParams"
.end annotation


# instance fields
.field private mFc:B

.field private final mIsValid:Z

.field mLastTouchedTime:J


# direct methods
.method public constructor <init>(IJJ)V
    .locals 7
    .param p1    # I
    .param p2    # J
    .param p4    # J

    int-to-byte v0, p1

    invoke-static {v0}, Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils;->fcToLevel(B)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v6, 0x1

    :goto_0
    move-object v0, p0

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    invoke-direct/range {v0 .. v6}, Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;-><init>(IJJZ)V

    return-void

    :cond_0
    const/4 v6, 0x0

    goto :goto_0
.end method

.method private constructor <init>(IJJZ)V
    .locals 2
    .param p1    # I
    .param p2    # J
    .param p4    # J
    .param p6    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;->mLastTouchedTime:J

    iput-boolean p6, p0, Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;->mIsValid:Z

    int-to-byte v0, p1

    iput-byte v0, p0, Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;->mFc:B

    iput-wide p4, p0, Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;->mLastTouchedTime:J

    invoke-direct {p0, p2, p3}, Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;->updateElapsedTime(J)V

    return-void
.end method

.method private constructor <init>(JZ)V
    .locals 7
    .param p1    # J
    .param p3    # Z

    const/4 v0, 0x0

    invoke-static {v0, p3}, Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils;->pushCount(BZ)B

    move-result v1

    move-object v0, p0

    move-wide v2, p1

    move-wide v4, p1

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;-><init>(IJJZ)V

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 2
    .param p1    # Z

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-direct {p0, v0, v1, p1}, Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;-><init>(JZ)V

    return-void
.end method

.method private updateElapsedTime(J)V
    .locals 6
    .param p1    # J

    const-wide/32 v4, 0x1499700

    iget-wide v2, p0, Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;->mLastTouchedTime:J

    sub-long v2, p1, v2

    div-long/2addr v2, v4

    long-to-int v0, v2

    if-gtz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v2, 0x40

    if-lt v0, v2, :cond_2

    iput-wide p1, p0, Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;->mLastTouchedTime:J

    const/4 v2, 0x0

    iput-byte v2, p0, Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;->mFc:B

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v0, :cond_0

    iget-wide v2, p0, Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;->mLastTouchedTime:J

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;->mLastTouchedTime:J

    iget-byte v2, p0, Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;->mFc:B

    invoke-static {v2}, Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils;->pushElapsedTime(B)B

    move-result v2

    iput-byte v2, p0, Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;->mFc:B

    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private updateLastTouchedTime()V
    .locals 2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;->mLastTouchedTime:J

    return-void
.end method


# virtual methods
.method public getFc()B
    .locals 2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;->updateElapsedTime(J)V

    iget-byte v0, p0, Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;->mFc:B

    return v0
.end method

.method public getFrequency()I
    .locals 2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;->updateElapsedTime(J)V

    iget-byte v0, p0, Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;->mFc:B

    invoke-static {v0}, Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils;->fcToFreq(B)I

    move-result v0

    return v0
.end method

.method public isValid()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;->mIsValid:Z

    return v0
.end method

.method public notifyTypedAgainAndGetFrequency()I
    .locals 2

    invoke-direct {p0}, Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;->updateLastTouchedTime()V

    iget-byte v0, p0, Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;->mFc:B

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils;->pushCount(BZ)B

    move-result v0

    iput-byte v0, p0, Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;->mFc:B

    iget-byte v0, p0, Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;->mFc:B

    invoke-static {v0}, Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils;->fcToFreq(B)I

    move-result v0

    return v0
.end method
