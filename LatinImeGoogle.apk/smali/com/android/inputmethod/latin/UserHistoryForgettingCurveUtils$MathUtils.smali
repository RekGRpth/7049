.class Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$MathUtils;
.super Ljava/lang/Object;
.source "UserHistoryForgettingCurveUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MathUtils"
.end annotation


# static fields
.field public static final SCORE_TABLE:[[I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    const/4 v12, 0x3

    const/16 v9, 0x10

    filled-new-array {v12, v9}, [I

    move-result-object v9

    sget-object v10, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v10, v9}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [[I

    sput-object v9, Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$MathUtils;->SCORE_TABLE:[[I

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v12, :cond_3

    const/4 v9, 0x2

    if-lt v4, v9, :cond_0

    const-wide v5, 0x405fc00000000000L

    :goto_1
    const/4 v8, 0x0

    :goto_2
    const/16 v9, 0xf

    if-ge v8, v9, :cond_2

    mul-int/lit8 v9, v8, 0x6

    int-to-double v0, v9

    const-wide/high16 v9, 0x4048000000000000L

    div-double v9, v0, v9

    invoke-static {v5, v6, v9, v10}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v9

    mul-double v2, v5, v9

    const/16 v9, 0x7f

    const/4 v10, 0x0

    double-to-int v11, v2

    invoke-static {v10, v11}, Ljava/lang/Math;->max(II)I

    move-result v10

    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    move-result v7

    sget-object v9, Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$MathUtils;->SCORE_TABLE:[[I

    aget-object v9, v9, v4

    aput v7, v9, v8

    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    :cond_0
    const/4 v9, 0x1

    if-ne v4, v9, :cond_1

    const-wide v5, 0x404fc00000000000L

    goto :goto_1

    :cond_1
    if-nez v4, :cond_2

    const-wide v5, 0x403fc00000000000L

    goto :goto_1

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
