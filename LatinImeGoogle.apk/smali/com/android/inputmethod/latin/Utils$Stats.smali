.class public Lcom/android/inputmethod/latin/Utils$Stats;
.super Ljava/lang/Object;
.source "Utils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/latin/Utils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Stats"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static onAutoCorrection(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {p0, p1, p2}, Lcom/android/inputmethod/latin/LatinImeLogger;->logOnAutoCorrection(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public static onAutoCorrectionCancellation()V
    .locals 0

    invoke-static {}, Lcom/android/inputmethod/latin/LatinImeLogger;->logOnAutoCorrectionCancelled()V

    return-void
.end method

.method public static onNonSeparator(CII)V
    .locals 1
    .param p0    # C
    .param p1    # I
    .param p2    # I

    invoke-static {}, Lcom/android/inputmethod/latin/Utils$RingCharBuffer;->getInstance()Lcom/android/inputmethod/latin/Utils$RingCharBuffer;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2}, Lcom/android/inputmethod/latin/Utils$RingCharBuffer;->push(CII)V

    invoke-static {}, Lcom/android/inputmethod/latin/LatinImeLogger;->logOnInputChar()V

    return-void
.end method

.method public static onSeparator(III)V
    .locals 2
    .param p0    # I
    .param p1    # I
    .param p2    # I

    invoke-static {}, Lcom/android/inputmethod/latin/Utils$RingCharBuffer;->getInstance()Lcom/android/inputmethod/latin/Utils$RingCharBuffer;

    move-result-object v0

    int-to-char v1, p0

    invoke-virtual {v0, v1, p1, p2}, Lcom/android/inputmethod/latin/Utils$RingCharBuffer;->push(CII)V

    invoke-static {}, Lcom/android/inputmethod/latin/LatinImeLogger;->logOnInputSeparator()V

    return-void
.end method
