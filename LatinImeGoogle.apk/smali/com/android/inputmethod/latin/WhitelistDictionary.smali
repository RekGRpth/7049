.class public Lcom/android/inputmethod/latin/WhitelistDictionary;
.super Lcom/android/inputmethod/latin/ExpandableDictionary;
.source "WhitelistDictionary.java"


# static fields
.field private static final DBG:Z

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mWhitelistWords:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sDBG:Z

    sput-boolean v0, Lcom/android/inputmethod/latin/WhitelistDictionary;->DBG:Z

    const-class v0, Lcom/android/inputmethod/latin/WhitelistDictionary;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/inputmethod/latin/WhitelistDictionary;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/Locale;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/util/Locale;

    const/4 v1, 0x6

    invoke-direct {p0, p1, v1}, Lcom/android/inputmethod/latin/ExpandableDictionary;-><init>(Landroid/content/Context;I)V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/android/inputmethod/latin/WhitelistDictionary;->mWhitelistWords:Ljava/util/HashMap;

    new-instance v0, Lcom/android/inputmethod/latin/WhitelistDictionary$1;

    invoke-direct {v0, p0}, Lcom/android/inputmethod/latin/WhitelistDictionary$1;-><init>(Lcom/android/inputmethod/latin/WhitelistDictionary;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/android/inputmethod/latin/LocaleUtils$RunInLocale;->runInLocale(Landroid/content/res/Resources;Ljava/util/Locale;)Ljava/lang/Object;

    return-void
.end method

.method static synthetic access$000(Lcom/android/inputmethod/latin/WhitelistDictionary;[Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/android/inputmethod/latin/WhitelistDictionary;
    .param p1    # [Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/android/inputmethod/latin/WhitelistDictionary;->initWordlist([Ljava/lang/String;)V

    return-void
.end method

.method private initWordlist([Ljava/lang/String;)V
    .locals 10
    .param p1    # [Ljava/lang/String;

    iget-object v6, p0, Lcom/android/inputmethod/latin/WhitelistDictionary;->mWhitelistWords:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->clear()V

    array-length v0, p1

    rem-int/lit8 v6, v0, 0x3

    if-eqz v6, :cond_1

    sget-boolean v6, Lcom/android/inputmethod/latin/WhitelistDictionary;->DBG:Z

    if-eqz v6, :cond_0

    sget-object v6, Lcom/android/inputmethod/latin/WhitelistDictionary;->TAG:Ljava/lang/String;

    const-string v7, "The number of the whitelist is invalid."

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v4, 0x0

    :goto_1
    if-ge v4, v0, :cond_0

    :try_start_0
    aget-object v6, p1, v4

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v5

    add-int/lit8 v6, v4, 0x1

    aget-object v2, p1, v6

    add-int/lit8 v6, v4, 0x2

    aget-object v1, p1, v6

    if-eqz v2, :cond_2

    if-eqz v1, :cond_2

    iget-object v6, p0, Lcom/android/inputmethod/latin/WhitelistDictionary;->mWhitelistWords:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Landroid/util/Pair;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-direct {v8, v9, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {p0, v1, v6, v5}, Lcom/android/inputmethod/latin/WhitelistDictionary;->addWord(Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    add-int/lit8 v4, v4, 0x3

    goto :goto_1

    :catch_0
    move-exception v3

    sget-boolean v6, Lcom/android/inputmethod/latin/WhitelistDictionary;->DBG:Z

    if-eqz v6, :cond_0

    sget-object v6, Lcom/android/inputmethod/latin/WhitelistDictionary;->TAG:Ljava/lang/String;

    const-string v7, "The score of the word is invalid."

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public getWhitelistedWord(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/android/inputmethod/latin/WhitelistDictionary;->mWhitelistWords:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-boolean v1, Lcom/android/inputmethod/latin/WhitelistDictionary;->DBG:Z

    if-eqz v1, :cond_2

    sget-object v1, Lcom/android/inputmethod/latin/WhitelistDictionary;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "--- found whitelistedWord: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v1, p0, Lcom/android/inputmethod/latin/WhitelistDictionary;->mWhitelistWords:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    goto :goto_0
.end method

.method public shouldForciblyAutoCorrectFrom(Ljava/lang/CharSequence;)Z
    .locals 3
    .param p1    # Ljava/lang/CharSequence;

    const/4 v1, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/android/inputmethod/latin/WhitelistDictionary;->getWhitelistedWord(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method
