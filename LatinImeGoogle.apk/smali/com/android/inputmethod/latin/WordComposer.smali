.class public Lcom/android/inputmethod/latin/WordComposer;
.super Ljava/lang/Object;
.source "WordComposer.java"


# instance fields
.field private mAutoCapitalized:Z

.field private mAutoCorrection:Ljava/lang/CharSequence;

.field private mCapsCount:I

.field private mCodePointSize:I

.field private mIsFirstCharCapitalized:Z

.field private mIsResumed:Z

.field private mPrimaryKeyCodes:[I

.field private mTrailingSingleQuotesCount:I

.field private mTypedWord:Ljava/lang/StringBuilder;

.field private mXCoordinates:[I

.field private mYCoordinates:[I


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/16 v1, 0x30

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v0, v1, [I

    iput-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mPrimaryKeyCodes:[I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mTypedWord:Ljava/lang/StringBuilder;

    new-array v0, v1, [I

    iput-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mXCoordinates:[I

    new-array v0, v1, [I

    iput-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mYCoordinates:[I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mAutoCorrection:Ljava/lang/CharSequence;

    iput v2, p0, Lcom/android/inputmethod/latin/WordComposer;->mTrailingSingleQuotesCount:I

    iput-boolean v2, p0, Lcom/android/inputmethod/latin/WordComposer;->mIsResumed:Z

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/WordComposer;->refreshSize()V

    return-void
.end method

.method public constructor <init>(Lcom/android/inputmethod/latin/WordComposer;)V
    .locals 0
    .param p1    # Lcom/android/inputmethod/latin/WordComposer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0, p1}, Lcom/android/inputmethod/latin/WordComposer;->init(Lcom/android/inputmethod/latin/WordComposer;)V

    return-void
.end method

.method private add(III)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # I

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/WordComposer;->size()I

    move-result v0

    iget-object v1, p0, Lcom/android/inputmethod/latin/WordComposer;->mTypedWord:Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->appendCodePoint(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/WordComposer;->refreshSize()V

    const/16 v1, 0x30

    if-ge v0, v1, :cond_0

    iget-object v2, p0, Lcom/android/inputmethod/latin/WordComposer;->mPrimaryKeyCodes:[I

    const/16 v1, 0x20

    if-lt p1, v1, :cond_2

    invoke-static {p1}, Ljava/lang/Character;->toLowerCase(I)I

    move-result v1

    :goto_0
    aput v1, v2, v0

    iget-object v1, p0, Lcom/android/inputmethod/latin/WordComposer;->mXCoordinates:[I

    aput p2, v1, v0

    iget-object v1, p0, Lcom/android/inputmethod/latin/WordComposer;->mYCoordinates:[I

    aput p3, v1, v0

    :cond_0
    iget-boolean v1, p0, Lcom/android/inputmethod/latin/WordComposer;->mIsFirstCharCapitalized:Z

    invoke-static {v0, p1, v1}, Lcom/android/inputmethod/latin/WordComposer;->isFirstCharCapitalized(IIZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/inputmethod/latin/WordComposer;->mIsFirstCharCapitalized:Z

    invoke-static {p1}, Ljava/lang/Character;->isUpperCase(I)Z

    move-result v1

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/android/inputmethod/latin/WordComposer;->mCapsCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/inputmethod/latin/WordComposer;->mCapsCount:I

    :cond_1
    const/16 v1, 0x27

    if-ne v1, p1, :cond_3

    iget v1, p0, Lcom/android/inputmethod/latin/WordComposer;->mTrailingSingleQuotesCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/inputmethod/latin/WordComposer;->mTrailingSingleQuotesCount:I

    :goto_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/inputmethod/latin/WordComposer;->mAutoCorrection:Ljava/lang/CharSequence;

    return-void

    :cond_2
    move v1, p1

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    iput v1, p0, Lcom/android/inputmethod/latin/WordComposer;->mTrailingSingleQuotesCount:I

    goto :goto_1
.end method

.method private addKeyInfo(ILcom/android/inputmethod/keyboard/Keyboard;)V
    .locals 8
    .param p1    # I
    .param p2    # Lcom/android/inputmethod/keyboard/Keyboard;

    const/4 v7, -0x1

    iget-object v0, p2, Lcom/android/inputmethod/keyboard/Keyboard;->mKeys:[Lcom/android/inputmethod/keyboard/Key;

    array-length v3, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v2, v0, v1

    iget v6, v2, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    if-ne v6, p1, :cond_0

    iget v6, v2, Lcom/android/inputmethod/keyboard/Key;->mX:I

    iget v7, v2, Lcom/android/inputmethod/keyboard/Key;->mWidth:I

    div-int/lit8 v7, v7, 0x2

    add-int v4, v6, v7

    iget v6, v2, Lcom/android/inputmethod/keyboard/Key;->mY:I

    iget v7, v2, Lcom/android/inputmethod/keyboard/Key;->mHeight:I

    div-int/lit8 v7, v7, 0x2

    add-int v5, v6, v7

    invoke-direct {p0, p1, v4, v5}, Lcom/android/inputmethod/latin/WordComposer;->add(III)V

    :goto_1
    return-void

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1, v7, v7}, Lcom/android/inputmethod/latin/WordComposer;->add(III)V

    goto :goto_1
.end method

.method private static isFirstCharCapitalized(IIZ)Z
    .locals 1
    .param p0    # I
    .param p1    # I
    .param p2    # Z

    if-nez p0, :cond_0

    invoke-static {p1}, Ljava/lang/Character;->isUpperCase(I)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    if-eqz p2, :cond_1

    invoke-static {p1}, Ljava/lang/Character;->isUpperCase(I)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public add(IIILcom/android/inputmethod/keyboard/KeyDetector;)V
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Lcom/android/inputmethod/keyboard/KeyDetector;

    const/4 v3, -0x1

    const/4 v2, -0x2

    if-eqz p4, :cond_0

    if-eq p2, v2, :cond_0

    if-eq p3, v2, :cond_0

    if-eq p2, v3, :cond_0

    if-ne p3, v3, :cond_1

    :cond_0
    move v0, p2

    move v1, p3

    :goto_0
    invoke-direct {p0, p1, v0, v1}, Lcom/android/inputmethod/latin/WordComposer;->add(III)V

    return-void

    :cond_1
    invoke-virtual {p4, p2}, Lcom/android/inputmethod/keyboard/KeyDetector;->getTouchX(I)I

    move-result v0

    invoke-virtual {p4, p3}, Lcom/android/inputmethod/keyboard/KeyDetector;->getTouchY(I)I

    move-result v1

    goto :goto_0
.end method

.method public commitWord(ILjava/lang/String;ILjava/lang/CharSequence;)Lcom/android/inputmethod/latin/LastComposedWord;
    .locals 9
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # Ljava/lang/CharSequence;

    const/4 v8, 0x0

    const/16 v5, 0x30

    iget-object v1, p0, Lcom/android/inputmethod/latin/WordComposer;->mPrimaryKeyCodes:[I

    iget-object v2, p0, Lcom/android/inputmethod/latin/WordComposer;->mXCoordinates:[I

    iget-object v3, p0, Lcom/android/inputmethod/latin/WordComposer;->mYCoordinates:[I

    new-array v4, v5, [I

    iput-object v4, p0, Lcom/android/inputmethod/latin/WordComposer;->mPrimaryKeyCodes:[I

    new-array v4, v5, [I

    iput-object v4, p0, Lcom/android/inputmethod/latin/WordComposer;->mXCoordinates:[I

    new-array v4, v5, [I

    iput-object v4, p0, Lcom/android/inputmethod/latin/WordComposer;->mYCoordinates:[I

    new-instance v0, Lcom/android/inputmethod/latin/LastComposedWord;

    iget-object v4, p0, Lcom/android/inputmethod/latin/WordComposer;->mTypedWord:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v5, p2

    move v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/android/inputmethod/latin/LastComposedWord;-><init>([I[I[ILjava/lang/String;Ljava/lang/String;ILjava/lang/CharSequence;)V

    const/4 v4, 0x2

    if-eq p1, v4, :cond_0

    const/4 v4, 0x1

    if-eq p1, v4, :cond_0

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/LastComposedWord;->deactivate()V

    :cond_0
    iget-object v4, p0, Lcom/android/inputmethod/latin/WordComposer;->mTypedWord:Ljava/lang/StringBuilder;

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->setLength(I)V

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/WordComposer;->refreshSize()V

    const/4 v4, 0x0

    iput-object v4, p0, Lcom/android/inputmethod/latin/WordComposer;->mAutoCorrection:Ljava/lang/CharSequence;

    iput-boolean v8, p0, Lcom/android/inputmethod/latin/WordComposer;->mIsResumed:Z

    return-object v0
.end method

.method public deleteLast()V
    .locals 6

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/WordComposer;->size()I

    move-result v2

    if-lez v2, :cond_2

    iget-object v4, p0, Lcom/android/inputmethod/latin/WordComposer;->mTypedWord:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-ge v3, v2, :cond_0

    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "In WordComposer: mCodes and mTypedWords have non-matching lengths"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_0
    iget-object v4, p0, Lcom/android/inputmethod/latin/WordComposer;->mTypedWord:Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->codePointBefore(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isSupplementaryCodePoint(I)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/android/inputmethod/latin/WordComposer;->mTypedWord:Ljava/lang/StringBuilder;

    add-int/lit8 v5, v3, -0x2

    invoke-virtual {v4, v5, v3}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    :goto_0
    invoke-static {v1}, Ljava/lang/Character;->isUpperCase(I)Z

    move-result v4

    if-eqz v4, :cond_1

    iget v4, p0, Lcom/android/inputmethod/latin/WordComposer;->mCapsCount:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/android/inputmethod/latin/WordComposer;->mCapsCount:I

    :cond_1
    invoke-virtual {p0}, Lcom/android/inputmethod/latin/WordComposer;->refreshSize()V

    :cond_2
    invoke-virtual {p0}, Lcom/android/inputmethod/latin/WordComposer;->size()I

    move-result v4

    if-nez v4, :cond_3

    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/android/inputmethod/latin/WordComposer;->mIsFirstCharCapitalized:Z

    :cond_3
    iget v4, p0, Lcom/android/inputmethod/latin/WordComposer;->mTrailingSingleQuotesCount:I

    if-lez v4, :cond_6

    iget v4, p0, Lcom/android/inputmethod/latin/WordComposer;->mTrailingSingleQuotesCount:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/android/inputmethod/latin/WordComposer;->mTrailingSingleQuotesCount:I

    :cond_4
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/android/inputmethod/latin/WordComposer;->mAutoCorrection:Ljava/lang/CharSequence;

    return-void

    :cond_5
    iget-object v4, p0, Lcom/android/inputmethod/latin/WordComposer;->mTypedWord:Ljava/lang/StringBuilder;

    add-int/lit8 v5, v3, -0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_6
    iget-object v4, p0, Lcom/android/inputmethod/latin/WordComposer;->mTypedWord:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    :goto_1
    if-lez v0, :cond_4

    iget-object v4, p0, Lcom/android/inputmethod/latin/WordComposer;->mTypedWord:Ljava/lang/StringBuilder;

    const/4 v5, -0x1

    invoke-virtual {v4, v0, v5}, Ljava/lang/StringBuilder;->offsetByCodePoints(II)I

    move-result v0

    const/16 v4, 0x27

    iget-object v5, p0, Lcom/android/inputmethod/latin/WordComposer;->mTypedWord:Ljava/lang/StringBuilder;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->codePointAt(I)I

    move-result v5

    if-ne v4, v5, :cond_4

    iget v4, p0, Lcom/android/inputmethod/latin/WordComposer;->mTrailingSingleQuotesCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/android/inputmethod/latin/WordComposer;->mTrailingSingleQuotesCount:I

    goto :goto_1
.end method

.method public getAutoCorrectionOrNull()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mAutoCorrection:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getCodeAt(I)I
    .locals 1
    .param p1    # I

    const/16 v0, 0x30

    if-lt p1, v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mPrimaryKeyCodes:[I

    aget v0, v0, p1

    goto :goto_0
.end method

.method public getTypedWord()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mTypedWord:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getXCoordinates()[I
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mXCoordinates:[I

    return-object v0
.end method

.method public getYCoordinates()[I
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mYCoordinates:[I

    return-object v0
.end method

.method public init(Lcom/android/inputmethod/latin/WordComposer;)V
    .locals 2
    .param p1    # Lcom/android/inputmethod/latin/WordComposer;

    iget-object v0, p1, Lcom/android/inputmethod/latin/WordComposer;->mPrimaryKeyCodes:[I

    iget-object v1, p1, Lcom/android/inputmethod/latin/WordComposer;->mPrimaryKeyCodes:[I

    array-length v1, v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mPrimaryKeyCodes:[I

    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/android/inputmethod/latin/WordComposer;->mTypedWord:Ljava/lang/StringBuilder;

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/CharSequence;)V

    iput-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mTypedWord:Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/android/inputmethod/latin/WordComposer;->mXCoordinates:[I

    iget-object v1, p1, Lcom/android/inputmethod/latin/WordComposer;->mXCoordinates:[I

    array-length v1, v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mXCoordinates:[I

    iget-object v0, p1, Lcom/android/inputmethod/latin/WordComposer;->mYCoordinates:[I

    iget-object v1, p1, Lcom/android/inputmethod/latin/WordComposer;->mYCoordinates:[I

    array-length v1, v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mYCoordinates:[I

    iget v0, p1, Lcom/android/inputmethod/latin/WordComposer;->mCapsCount:I

    iput v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mCapsCount:I

    iget-boolean v0, p1, Lcom/android/inputmethod/latin/WordComposer;->mIsFirstCharCapitalized:Z

    iput-boolean v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mIsFirstCharCapitalized:Z

    iget-boolean v0, p1, Lcom/android/inputmethod/latin/WordComposer;->mAutoCapitalized:Z

    iput-boolean v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mAutoCapitalized:Z

    iget v0, p1, Lcom/android/inputmethod/latin/WordComposer;->mTrailingSingleQuotesCount:I

    iput v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mTrailingSingleQuotesCount:I

    iget-boolean v0, p1, Lcom/android/inputmethod/latin/WordComposer;->mIsResumed:Z

    iput-boolean v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mIsResumed:Z

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/WordComposer;->refreshSize()V

    return-void
.end method

.method public isAllUpperCase()Z
    .locals 2

    iget v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mCapsCount:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mCapsCount:I

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/WordComposer;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isAutoCapitalized()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mAutoCapitalized:Z

    return v0
.end method

.method public final isComposingWord()Z
    .locals 1

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/WordComposer;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFirstCharCapitalized()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mIsFirstCharCapitalized:Z

    return v0
.end method

.method public isMostlyCaps()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/android/inputmethod/latin/WordComposer;->mCapsCount:I

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isResumed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mIsResumed:Z

    return v0
.end method

.method public final refreshSize()V
    .locals 3

    iget-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mTypedWord:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/inputmethod/latin/WordComposer;->mTypedWord:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->codePointCount(II)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mCodePointSize:I

    return-void
.end method

.method public reset()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mTypedWord:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mAutoCorrection:Ljava/lang/CharSequence;

    iput v1, p0, Lcom/android/inputmethod/latin/WordComposer;->mCapsCount:I

    iput-boolean v1, p0, Lcom/android/inputmethod/latin/WordComposer;->mIsFirstCharCapitalized:Z

    iput v1, p0, Lcom/android/inputmethod/latin/WordComposer;->mTrailingSingleQuotesCount:I

    iput-boolean v1, p0, Lcom/android/inputmethod/latin/WordComposer;->mIsResumed:Z

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/WordComposer;->refreshSize()V

    return-void
.end method

.method public resumeSuggestionOnLastComposedWord(Lcom/android/inputmethod/latin/LastComposedWord;)V
    .locals 2
    .param p1    # Lcom/android/inputmethod/latin/LastComposedWord;

    iget-object v0, p1, Lcom/android/inputmethod/latin/LastComposedWord;->mPrimaryKeyCodes:[I

    iput-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mPrimaryKeyCodes:[I

    iget-object v0, p1, Lcom/android/inputmethod/latin/LastComposedWord;->mXCoordinates:[I

    iput-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mXCoordinates:[I

    iget-object v0, p1, Lcom/android/inputmethod/latin/LastComposedWord;->mYCoordinates:[I

    iput-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mYCoordinates:[I

    iget-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mTypedWord:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mTypedWord:Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/android/inputmethod/latin/LastComposedWord;->mTypedWord:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/WordComposer;->refreshSize()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mAutoCorrection:Ljava/lang/CharSequence;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mIsResumed:Z

    return-void
.end method

.method public setAutoCapitalized(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/inputmethod/latin/WordComposer;->mAutoCapitalized:Z

    return-void
.end method

.method public setAutoCorrection(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;

    iput-object p1, p0, Lcom/android/inputmethod/latin/WordComposer;->mAutoCorrection:Ljava/lang/CharSequence;

    return-void
.end method

.method public setComposingWord(Ljava/lang/CharSequence;Lcom/android/inputmethod/keyboard/Keyboard;)V
    .locals 4
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Lcom/android/inputmethod/keyboard/Keyboard;

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/WordComposer;->reset()V

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-static {p1, v1}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v0

    invoke-direct {p0, v0, p2}, Lcom/android/inputmethod/latin/WordComposer;->addKeyInfo(ILcom/android/inputmethod/keyboard/Keyboard;)V

    invoke-static {p1, v1, v3}, Ljava/lang/Character;->offsetByCodePoints(Ljava/lang/CharSequence;II)I

    move-result v1

    goto :goto_0

    :cond_0
    iput-boolean v3, p0, Lcom/android/inputmethod/latin/WordComposer;->mIsResumed:Z

    return-void
.end method

.method public final size()I
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mCodePointSize:I

    return v0
.end method

.method public trailingSingleQuotesCount()I
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mTrailingSingleQuotesCount:I

    return v0
.end method
