.class Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$CharEncoding;
.super Ljava/lang/Object;
.source "BinaryDictInputOutput.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CharEncoding"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000([I)I
    .locals 1
    .param p0    # [I

    invoke-static {p0}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$CharEncoding;->getCharArraySize([I)I

    move-result v0

    return v0
.end method

.method static synthetic access$100(I)I
    .locals 1
    .param p0    # I

    invoke-static {p0}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$CharEncoding;->getCharSize(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$200([I[BI)I
    .locals 1
    .param p0    # [I
    .param p1    # [B
    .param p2    # I

    invoke-static {p0, p1, p2}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$CharEncoding;->writeCharArray([I[BI)I

    move-result v0

    return v0
.end method

.method static synthetic access$300([BILjava/lang/String;)I
    .locals 1
    .param p0    # [B
    .param p1    # I
    .param p2    # Ljava/lang/String;

    invoke-static {p0, p1, p2}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$CharEncoding;->writeString([BILjava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic access$400(Ljava/io/ByteArrayOutputStream;Ljava/lang/String;)V
    .locals 0
    .param p0    # Ljava/io/ByteArrayOutputStream;
    .param p1    # Ljava/lang/String;

    invoke-static {p0, p1}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$CharEncoding;->writeString(Ljava/io/ByteArrayOutputStream;Ljava/lang/String;)V

    return-void
.end method

.method private static fitsOnOneByte(I)Z
    .locals 1
    .param p0    # I

    const/16 v0, 0x20

    if-lt p0, v0, :cond_0

    const/16 v0, 0xff

    if-gt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getCharArraySize([I)I
    .locals 6
    .param p0    # [I

    const/4 v4, 0x0

    move-object v0, p0

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget v1, v0, v2

    invoke-static {v1}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$CharEncoding;->getCharSize(I)I

    move-result v5

    add-int/2addr v4, v5

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return v4
.end method

.method private static getCharSize(I)I
    .locals 2
    .param p0    # I

    const/4 v0, 0x1

    invoke-static {p0}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$CharEncoding;->fitsOnOneByte(I)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v1, -0x1

    if-eq v1, p0, :cond_0

    const/4 v0, 0x3

    goto :goto_0
.end method

.method private static writeCharArray([I[BI)I
    .locals 7
    .param p0    # [I
    .param p1    # [B
    .param p2    # I

    move-object v0, p0

    array-length v4, v0

    const/4 v2, 0x0

    move v3, p2

    :goto_0
    if-ge v2, v4, :cond_1

    aget v1, v0, v2

    const/4 v5, 0x1

    invoke-static {v1}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$CharEncoding;->getCharSize(I)I

    move-result v6

    if-ne v5, v6, :cond_0

    add-int/lit8 p2, v3, 0x1

    int-to-byte v5, v1

    aput-byte v5, p1, v3

    :goto_1
    add-int/lit8 v2, v2, 0x1

    move v3, p2

    goto :goto_0

    :cond_0
    add-int/lit8 p2, v3, 0x1

    shr-int/lit8 v5, v1, 0x10

    and-int/lit16 v5, v5, 0xff

    int-to-byte v5, v5

    aput-byte v5, p1, v3

    add-int/lit8 v3, p2, 0x1

    shr-int/lit8 v5, v1, 0x8

    and-int/lit16 v5, v5, 0xff

    int-to-byte v5, v5

    aput-byte v5, p1, p2

    add-int/lit8 p2, v3, 0x1

    and-int/lit16 v5, v1, 0xff

    int-to-byte v5, v5

    aput-byte v5, p1, v3

    goto :goto_1

    :cond_1
    return v3
.end method

.method private static writeString([BILjava/lang/String;)I
    .locals 7
    .param p0    # [B
    .param p1    # I
    .param p2    # Ljava/lang/String;

    const/4 v6, 0x1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v4

    move v2, p1

    const/4 v1, 0x0

    move v3, v2

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {p2, v1}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    invoke-static {v0}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$CharEncoding;->getCharSize(I)I

    move-result v5

    if-ne v6, v5, :cond_0

    add-int/lit8 v2, v3, 0x1

    int-to-byte v5, v0

    aput-byte v5, p0, v3

    :goto_1
    invoke-virtual {p2, v1, v6}, Ljava/lang/String;->offsetByCodePoints(II)I

    move-result v1

    move v3, v2

    goto :goto_0

    :cond_0
    add-int/lit8 v2, v3, 0x1

    shr-int/lit8 v5, v0, 0x10

    and-int/lit16 v5, v5, 0xff

    int-to-byte v5, v5

    aput-byte v5, p0, v3

    add-int/lit8 v3, v2, 0x1

    shr-int/lit8 v5, v0, 0x8

    and-int/lit16 v5, v5, 0xff

    int-to-byte v5, v5

    aput-byte v5, p0, v2

    add-int/lit8 v2, v3, 0x1

    and-int/lit16 v5, v0, 0xff

    int-to-byte v5, v5

    aput-byte v5, p0, v3

    goto :goto_1

    :cond_1
    add-int/lit8 v2, v3, 0x1

    const/16 v5, 0x1f

    aput-byte v5, p0, v3

    sub-int v5, v2, p1

    return v5
.end method

.method private static writeString(Ljava/io/ByteArrayOutputStream;Ljava/lang/String;)V
    .locals 5
    .param p0    # Ljava/io/ByteArrayOutputStream;
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-virtual {p1, v1}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    invoke-static {v0}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$CharEncoding;->getCharSize(I)I

    move-result v3

    if-ne v4, v3, :cond_0

    int-to-byte v3, v0

    invoke-virtual {p0, v3}, Ljava/io/ByteArrayOutputStream;->write(I)V

    :goto_1
    invoke-virtual {p1, v1, v4}, Ljava/lang/String;->offsetByCodePoints(II)I

    move-result v1

    goto :goto_0

    :cond_0
    shr-int/lit8 v3, v0, 0x10

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    invoke-virtual {p0, v3}, Ljava/io/ByteArrayOutputStream;->write(I)V

    shr-int/lit8 v3, v0, 0x8

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    invoke-virtual {p0, v3}, Ljava/io/ByteArrayOutputStream;->write(I)V

    and-int/lit16 v3, v0, 0xff

    int-to-byte v3, v3

    invoke-virtual {p0, v3}, Ljava/io/ByteArrayOutputStream;->write(I)V

    goto :goto_1

    :cond_1
    const/16 v3, 0x1f

    invoke-virtual {p0, v3}, Ljava/io/ByteArrayOutputStream;->write(I)V

    return-void
.end method
