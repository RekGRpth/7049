.class public Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;
.super Ljava/lang/Object;
.source "BinaryDictInputOutput.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$CharEncoding;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final characterBuffer:[I

.field private static wordCache:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->$assertionsDisabled:Z

    const/16 v0, 0x30

    new-array v0, v0, [I

    sput-object v0, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->characterBuffer:[I

    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    sput-object v0, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->wordCache:Ljava/util/TreeMap;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static computeActualNodeSize(Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;Lcom/android/inputmethod/latin/makedict/FusionDictionary;)Z
    .locals 12
    .param p0    # Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;
    .param p1    # Lcom/android/inputmethod/latin/makedict/FusionDictionary;

    const/4 v2, 0x0

    invoke-static {p0}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->getGroupCountSize(Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;)I

    move-result v9

    iget-object v10, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mData:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;

    iget v10, v3, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mCachedAddress:I

    iget v11, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mCachedAddress:I

    add-int/2addr v11, v9

    if-eq v10, v11, :cond_0

    const/4 v2, 0x1

    iget v10, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mCachedAddress:I

    add-int/2addr v10, v9

    iput v10, v3, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mCachedAddress:I

    :cond_0
    invoke-static {v3}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->getGroupCharactersSize(Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;)I

    move-result v10

    add-int/lit8 v4, v10, 0x1

    invoke-virtual {v3}, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->isTerminal()Z

    move-result v10

    if-eqz v10, :cond_1

    add-int/lit8 v4, v4, 0x1

    :cond_1
    iget-object v10, v3, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mChildren:Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    if-eqz v10, :cond_2

    iget v10, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mCachedAddress:I

    add-int/2addr v10, v4

    add-int v8, v10, v9

    iget-object v10, v3, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mChildren:Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    iget v10, v10, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mCachedAddress:I

    sub-int v7, v10, v8

    invoke-static {v7}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->getByteSize(I)I

    move-result v10

    add-int/2addr v4, v10

    :cond_2
    iget-object v10, v3, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mShortcutTargets:Ljava/util/ArrayList;

    invoke-static {v10}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->getShortcutListSize(Ljava/util/ArrayList;)I

    move-result v10

    add-int/2addr v4, v10

    iget-object v10, v3, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mBigrams:Ljava/util/ArrayList;

    if-eqz v10, :cond_3

    iget-object v10, v3, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mBigrams:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;

    iget v10, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mCachedAddress:I

    add-int/2addr v10, v4

    add-int/2addr v10, v9

    add-int/lit8 v8, v10, 0x1

    iget-object v10, v1, Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;->mWord:Ljava/lang/String;

    invoke-static {p1, v10}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->findAddressOfWord(Lcom/android/inputmethod/latin/makedict/FusionDictionary;Ljava/lang/String;)I

    move-result v0

    sub-int v7, v0, v8

    invoke-static {v7}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->getByteSize(I)I

    move-result v10

    add-int/lit8 v10, v10, 0x1

    add-int/2addr v4, v10

    goto :goto_1

    :cond_3
    iput v4, v3, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mCachedSize:I

    add-int/2addr v9, v4

    goto :goto_0

    :cond_4
    iget v10, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mCachedSize:I

    if-eq v10, v9, :cond_5

    iput v9, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mCachedSize:I

    const/4 v2, 0x1

    :cond_5
    return v2
.end method

.method private static computeAddresses(Lcom/android/inputmethod/latin/makedict/FusionDictionary;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 12
    .param p0    # Lcom/android/inputmethod/latin/makedict/FusionDictionary;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/inputmethod/latin/makedict/FusionDictionary;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;",
            ">;"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    invoke-static {v4}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->setNodeMaximumSize(Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;)V

    goto :goto_0

    :cond_0
    invoke-static {p1}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->stackNodes(Ljava/util/ArrayList;)I

    move-result v6

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Compressing the array addresses. Original size : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/android/inputmethod/latin/makedict/MakedictLog;->i(Ljava/lang/String;)V

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "(Recursively seen size : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ")"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/android/inputmethod/latin/makedict/MakedictLog;->i(Ljava/lang/String;)V

    const/4 v8, 0x0

    const/4 v1, 0x0

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    iget v7, v4, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mCachedSize:I

    invoke-static {v4, p0}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->computeActualNodeSize(Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;Lcom/android/inputmethod/latin/makedict/FusionDictionary;)Z

    move-result v0

    iget v5, v4, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mCachedSize:I

    if-ge v7, v5, :cond_2

    new-instance v9, Ljava/lang/RuntimeException;

    const-string v10, "Increased size ?!"

    invoke-direct {v9, v10}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v9

    :cond_2
    or-int/2addr v1, v0

    goto :goto_1

    :cond_3
    invoke-static {p1}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->stackNodes(Ljava/util/ArrayList;)I

    add-int/lit8 v8, v8, 0x1

    const/16 v9, 0x18

    if-le v8, v9, :cond_4

    new-instance v9, Ljava/lang/RuntimeException;

    const-string v10, "Too many passes - probably a bug"

    invoke-direct {v9, v10}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v9

    :cond_4
    if-nez v1, :cond_1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {p1, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Compression complete in "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " passes."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/android/inputmethod/latin/makedict/MakedictLog;->i(Ljava/lang/String;)V

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "After address compression : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v3, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mCachedAddress:I

    iget v11, v3, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mCachedSize:I

    add-int/2addr v10, v11

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/android/inputmethod/latin/makedict/MakedictLog;->i(Ljava/lang/String;)V

    return-object p1
.end method

.method private static findAddressOfWord(Lcom/android/inputmethod/latin/makedict/FusionDictionary;Ljava/lang/String;)I
    .locals 1
    .param p0    # Lcom/android/inputmethod/latin/makedict/FusionDictionary;
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->mRoot:Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    invoke-static {v0, p1}, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->findWordInTree(Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;Ljava/lang/String;)Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;

    move-result-object v0

    iget v0, v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mCachedAddress:I

    return v0
.end method

.method static flattenTree(Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;)Ljava/util/ArrayList;
    .locals 4
    .param p0    # Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;",
            ">;"
        }
    .end annotation

    invoke-static {p0}, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->countCharGroups(Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;)I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Counted nodes : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/inputmethod/latin/makedict/MakedictLog;->i(Ljava/lang/String;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static {v0, p0}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->flattenTreeInner(Ljava/util/ArrayList;Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;)Ljava/util/ArrayList;

    move-result-object v2

    return-object v2
.end method

.method private static flattenTreeInner(Ljava/util/ArrayList;Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;)Ljava/util/ArrayList;
    .locals 5
    .param p1    # Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;",
            ">;",
            "Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p1, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;

    iget-object v4, v1, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mChildren:Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    if-eqz v4, :cond_0

    iget-object v4, v1, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mChildren:Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    invoke-static {p0, v4}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->flattenTreeInner(Ljava/util/ArrayList;Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;)Ljava/util/ArrayList;

    goto :goto_0

    :cond_1
    return-object p0
.end method

.method private static getByteSize(I)I
    .locals 2
    .param p0    # I

    sget-boolean v0, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    const/high16 v0, 0x1000000

    if-lt p0, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    invoke-static {p0}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->hasChildrenAddress(I)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    invoke-static {p0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/16 v1, 0x100

    if-ge v0, v1, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    invoke-static {p0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/high16 v1, 0x10000

    if-ge v0, v1, :cond_3

    const/4 v0, 0x2

    goto :goto_0

    :cond_3
    const/4 v0, 0x3

    goto :goto_0
.end method

.method private static getCharGroupMaximumSize(Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;)I
    .locals 2
    .param p0    # Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;

    invoke-static {p0}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->getGroupCharactersSize(Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;)I

    move-result v1

    add-int/lit8 v0, v1, 0x1

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->isTerminal()Z

    move-result v1

    if-eqz v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    add-int/lit8 v0, v0, 0x3

    iget-object v1, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mShortcutTargets:Ljava/util/ArrayList;

    invoke-static {v1}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->getShortcutListSize(Ljava/util/ArrayList;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mBigrams:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mBigrams:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    :cond_1
    return v0
.end method

.method private static getGroupCharactersSize(Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;)I
    .locals 2
    .param p0    # Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;

    iget-object v1, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mChars:[I

    # invokes: Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$CharEncoding;->getCharArraySize([I)I
    invoke-static {v1}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$CharEncoding;->access$000([I)I

    move-result v0

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->hasSeveralChars()Z

    move-result v1

    if-eqz v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    return v0
.end method

.method private static getGroupCountSize(I)I
    .locals 3
    .param p0    # I

    const/16 v0, 0x7f

    if-lt v0, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x7fff

    if-lt v0, p0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t have more than 32767 groups in a node (found "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static getGroupCountSize(Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;)I
    .locals 1
    .param p0    # Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    iget-object v0, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-static {v0}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->getGroupCountSize(I)I

    move-result v0

    return v0
.end method

.method private static getShortcutListSize(Ljava/util/ArrayList;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;",
            ">;)I"
        }
    .end annotation

    if-nez p0, :cond_1

    const/4 v2, 0x0

    :cond_0
    return v2

    :cond_1
    const/4 v2, 0x2

    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;

    invoke-static {v1}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->getShortcutSize(Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0
.end method

.method private static getShortcutSize(Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;)I
    .locals 6
    .param p0    # Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;->mWord:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-virtual {v4, v1}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    # invokes: Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$CharEncoding;->getCharSize(I)I
    invoke-static {v0}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$CharEncoding;->access$100(I)I

    move-result v5

    add-int/2addr v3, v5

    const/4 v5, 0x1

    invoke-virtual {v4, v1, v5}, Ljava/lang/String;->offsetByCodePoints(II)I

    move-result v1

    goto :goto_0

    :cond_0
    add-int/lit8 v3, v3, 0x1

    return v3
.end method

.method private static hasChildrenAddress(I)Z
    .locals 1
    .param p0    # I

    const/high16 v0, -0x80000000

    if-eq v0, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static final makeBigramFlags(ZIIILjava/lang/String;)I
    .locals 11
    .param p0    # Z
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/String;

    if-eqz p0, :cond_0

    const/16 v7, 0x80

    move v8, v7

    :goto_0
    if-gez p1, :cond_1

    const/16 v7, 0x40

    :goto_1
    add-int v0, v8, v7

    invoke-static {p1}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->getByteSize(I)I

    move-result v7

    packed-switch v7, :pswitch_data_0

    new-instance v7, Ljava/lang/RuntimeException;

    const-string v8, "Strange offset size"

    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_0
    const/4 v7, 0x0

    move v8, v7

    goto :goto_0

    :cond_1
    const/4 v7, 0x0

    goto :goto_1

    :pswitch_0
    or-int/lit8 v0, v0, 0x10

    :goto_2
    if-le p3, p2, :cond_2

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unigram freq is superior to bigram freq for \""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\". Bigram freq is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", unigram freq for "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/android/inputmethod/latin/makedict/MakedictLog;->e(Ljava/lang/String;)V

    move p2, p3

    :cond_2
    rsub-int v7, p3, 0xff

    int-to-double v7, v7

    const-wide v9, 0x4030800000000000L

    div-double v5, v7, v9

    add-int/lit8 v7, p3, 0x1

    int-to-double v7, v7

    const-wide/high16 v9, 0x4000000000000000L

    div-double v9, v5, v9

    add-double v3, v7, v9

    int-to-double v7, p2

    sub-double/2addr v7, v3

    div-double/2addr v7, v5

    double-to-int v1, v7

    if-lez v1, :cond_3

    move v2, v1

    :goto_3
    and-int/lit8 v7, v2, 0xf

    add-int/2addr v0, v7

    return v0

    :pswitch_1
    or-int/lit8 v0, v0, 0x20

    goto :goto_2

    :pswitch_2
    or-int/lit8 v0, v0, 0x30

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static makeCharGroupFlags(Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;II)B
    .locals 3
    .param p0    # Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;
    .param p1    # I
    .param p2    # I

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mChars:[I

    array-length v1, v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    const/16 v1, 0x20

    int-to-byte v0, v1

    :cond_0
    iget v1, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mFrequency:I

    if-ltz v1, :cond_1

    or-int/lit8 v1, v0, 0x10

    int-to-byte v0, v1

    :cond_1
    iget-object v1, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mChildren:Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    if-eqz v1, :cond_2

    invoke-static {p2}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->getByteSize(I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Node with a strange address"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    or-int/lit8 v1, v0, 0x40

    int-to-byte v0, v1

    :cond_2
    :goto_0
    iget-object v1, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mShortcutTargets:Ljava/util/ArrayList;

    if-eqz v1, :cond_3

    or-int/lit8 v1, v0, 0x8

    int-to-byte v0, v1

    :cond_3
    iget-object v1, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mBigrams:Ljava/util/ArrayList;

    if-eqz v1, :cond_4

    or-int/lit8 v1, v0, 0x4

    int-to-byte v0, v1

    :cond_4
    return v0

    :pswitch_1
    or-int/lit16 v1, v0, 0x80

    int-to-byte v0, v1

    goto :goto_0

    :pswitch_2
    or-int/lit16 v1, v0, 0xc0

    int-to-byte v0, v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static final makeOptionsValue(Lcom/android/inputmethod/latin/makedict/FusionDictionary;)I
    .locals 5
    .param p0    # Lcom/android/inputmethod/latin/makedict/FusionDictionary;

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->mOptions:Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryOptions;

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->hasBigrams()Z

    move-result v0

    iget-boolean v2, v1, Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryOptions;->mFrenchLigatureProcessing:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x4

    :goto_0
    iget-boolean v4, v1, Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryOptions;->mGermanUmlautProcessing:Z

    if-eqz v4, :cond_2

    const/4 v4, 0x1

    :goto_1
    add-int/2addr v2, v4

    if-eqz v0, :cond_0

    const/16 v3, 0x8

    :cond_0
    add-int/2addr v2, v3

    return v2

    :cond_1
    move v2, v3

    goto :goto_0

    :cond_2
    move v4, v3

    goto :goto_1
.end method

.method private static final makeShortcutFlags(ZI)I
    .locals 2
    .param p0    # Z
    .param p1    # I

    if-eqz p0, :cond_0

    const/16 v0, 0x80

    :goto_0
    and-int/lit8 v1, p1, 0xf

    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static setNodeMaximumSize(Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;)V
    .locals 5
    .param p0    # Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    invoke-static {p0}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->getGroupCountSize(Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;)I

    move-result v3

    iget-object v4, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mData:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;

    invoke-static {v0}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->getCharGroupMaximumSize(Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;)I

    move-result v1

    iput v1, v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mCachedSize:I

    add-int/2addr v3, v1

    goto :goto_0

    :cond_0
    iput v3, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mCachedSize:I

    return-void
.end method

.method private static stackNodes(Ljava/util/ArrayList;)I
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;",
            ">;)I"
        }
    .end annotation

    const/4 v6, 0x0

    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    iput v6, v5, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mCachedAddress:I

    invoke-static {v5}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->getGroupCountSize(Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;)I

    move-result v1

    const/4 v2, 0x0

    iget-object v7, v5, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mData:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;

    add-int v7, v1, v6

    add-int/2addr v7, v2

    iput v7, v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mCachedAddress:I

    iget v7, v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mCachedSize:I

    add-int/2addr v2, v7

    goto :goto_1

    :cond_0
    add-int v7, v2, v1

    iget v8, v5, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mCachedSize:I

    if-eq v7, v8, :cond_1

    new-instance v7, Ljava/lang/RuntimeException;

    const-string v8, "Bug : Stored and computed node size differ"

    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_1
    iget v7, v5, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mCachedSize:I

    add-int/2addr v6, v7

    goto :goto_0

    :cond_2
    return v6
.end method

.method public static writeDictionaryBinary(Ljava/io/OutputStream;Lcom/android/inputmethod/latin/makedict/FusionDictionary;I)V
    .locals 21
    .param p0    # Ljava/io/OutputStream;
    .param p1    # Lcom/android/inputmethod/latin/makedict/FusionDictionary;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/inputmethod/latin/makedict/UnsupportedFormatException;
        }
    .end annotation

    const/16 v18, 0x1

    move/from16 v0, p2

    move/from16 v1, v18

    if-lt v0, v1, :cond_0

    const/16 v18, 0x2

    move/from16 v0, p2

    move/from16 v1, v18

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v18, Lcom/android/inputmethod/latin/makedict/UnsupportedFormatException;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Requested file format version "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ", but this implementation only supports versions "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const/16 v20, 0x1

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " through "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const/16 v20, 0x2

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Lcom/android/inputmethod/latin/makedict/UnsupportedFormatException;-><init>(Ljava/lang/String;)V

    throw v18

    :cond_1
    new-instance v7, Ljava/io/ByteArrayOutputStream;

    const/16 v18, 0x100

    move/from16 v0, v18

    invoke-direct {v7, v0}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    const/16 v18, 0x2

    move/from16 v0, p2

    move/from16 v1, v18

    if-lt v0, v1, :cond_2

    const/16 v18, -0x65

    move/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    const/16 v18, -0x3f

    move/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    const/16 v18, 0x3a

    move/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    const/16 v18, -0x2

    move/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    shr-int/lit8 v18, p2, 0x8

    move/from16 v0, v18

    and-int/lit16 v0, v0, 0xff

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    move/from16 v0, p2

    and-int/lit16 v0, v0, 0xff

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    :goto_0
    invoke-static/range {p1 .. p1}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->makeOptionsValue(Lcom/android/inputmethod/latin/makedict/FusionDictionary;)I

    move-result v15

    shr-int/lit8 v18, v15, 0x8

    move/from16 v0, v18

    and-int/lit16 v0, v0, 0xff

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    and-int/lit16 v0, v15, 0xff

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    const/16 v18, 0x2

    move/from16 v0, p2

    move/from16 v1, v18

    if-lt v0, v1, :cond_5

    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v8

    const/4 v9, 0x0

    :goto_1
    const/16 v18, 0x4

    move/from16 v0, v18

    if-ge v9, v0, :cond_3

    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    :cond_2
    const/16 v18, 0x78

    move/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    const/16 v18, -0x4f

    move/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    move/from16 v0, p2

    and-int/lit16 v0, v0, 0xff

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    goto :goto_0

    :cond_3
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->mOptions:Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryOptions;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryOptions;->mAttributes:Ljava/util/HashMap;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_4

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->mOptions:Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryOptions;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryOptions;->mAttributes:Ljava/util/HashMap;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    # invokes: Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$CharEncoding;->writeString(Ljava/io/ByteArrayOutputStream;Ljava/lang/String;)V
    invoke-static {v7, v12}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$CharEncoding;->access$400(Ljava/io/ByteArrayOutputStream;Ljava/lang/String;)V

    move-object/from16 v0, v17

    # invokes: Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$CharEncoding;->writeString(Ljava/io/ByteArrayOutputStream;Ljava/lang/String;)V
    invoke-static {v7, v0}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$CharEncoding;->access$400(Ljava/io/ByteArrayOutputStream;Ljava/lang/String;)V

    goto :goto_2

    :cond_4
    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v16

    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    shr-int/lit8 v18, v16, 0x18

    move/from16 v0, v18

    and-int/lit16 v0, v0, 0xff

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    aput-byte v18, v4, v8

    add-int/lit8 v18, v8, 0x1

    shr-int/lit8 v19, v16, 0x10

    move/from16 v0, v19

    and-int/lit16 v0, v0, 0xff

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-byte v0, v0

    move/from16 v19, v0

    aput-byte v19, v4, v18

    add-int/lit8 v18, v8, 0x2

    shr-int/lit8 v19, v16, 0x8

    move/from16 v0, v19

    and-int/lit16 v0, v0, 0xff

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-byte v0, v0

    move/from16 v19, v0

    aput-byte v19, v4, v18

    add-int/lit8 v18, v8, 0x3

    shr-int/lit8 v19, v16, 0x0

    move/from16 v0, v19

    and-int/lit16 v0, v0, 0xff

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-byte v0, v0

    move/from16 v19, v0

    aput-byte v19, v4, v18

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Ljava/io/OutputStream;->write([B)V

    :goto_3
    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->close()V

    const-string v18, "Flattening the tree..."

    invoke-static/range {v18 .. v18}, Lcom/android/inputmethod/latin/makedict/MakedictLog;->i(Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->mRoot:Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->flattenTree(Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;)Ljava/util/ArrayList;

    move-result-object v6

    const-string v18, "Computing addresses..."

    invoke-static/range {v18 .. v18}, Lcom/android/inputmethod/latin/makedict/MakedictLog;->i(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-static {v0, v6}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->computeAddresses(Lcom/android/inputmethod/latin/makedict/FusionDictionary;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    const-string v18, "Checking array..."

    invoke-static/range {v18 .. v18}, Lcom/android/inputmethod/latin/makedict/MakedictLog;->i(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v18

    add-int/lit8 v18, v18, -0x1

    move/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    iget v0, v13, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mCachedAddress:I

    move/from16 v18, v0

    iget v0, v13, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mCachedSize:I

    move/from16 v19, v0

    add-int v3, v18, v19

    new-array v2, v3, [B

    const/4 v11, 0x0

    const-string v18, "Writing file..."

    invoke-static/range {v18 .. v18}, Lcom/android/inputmethod/latin/makedict/MakedictLog;->i(Ljava/lang/String;)V

    const/4 v5, 0x0

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_4
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_6

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    move-object/from16 v0, p1

    invoke-static {v0, v2, v14}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->writePlacedNode(Lcom/android/inputmethod/latin/makedict/FusionDictionary;[BLcom/android/inputmethod/latin/makedict/FusionDictionary$Node;)I

    move-result v5

    goto :goto_4

    :cond_5
    move-object/from16 v0, p0

    invoke-virtual {v7, v0}, Ljava/io/ByteArrayOutputStream;->writeTo(Ljava/io/OutputStream;)V

    goto :goto_3

    :cond_6
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1, v5}, Ljava/io/OutputStream;->write([BII)V

    invoke-virtual/range {p0 .. p0}, Ljava/io/OutputStream;->close()V

    const-string v18, "Done"

    invoke-static/range {v18 .. v18}, Lcom/android/inputmethod/latin/makedict/MakedictLog;->i(Ljava/lang/String;)V

    return-void
.end method

.method private static writePlacedNode(Lcom/android/inputmethod/latin/makedict/FusionDictionary;[BLcom/android/inputmethod/latin/makedict/FusionDictionary$Node;)I
    .locals 31
    .param p0    # Lcom/android/inputmethod/latin/makedict/FusionDictionary;
    .param p1    # [B
    .param p2    # Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mCachedAddress:I

    move/from16 v17, v0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mData:Ljava/util/ArrayList;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Ljava/util/ArrayList;->size()I

    move-result v15

    invoke-static/range {p2 .. p2}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->getGroupCountSize(Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;)I

    move-result v11

    const/16 v28, 0x1

    move/from16 v0, v28

    if-ne v0, v11, :cond_0

    add-int/lit8 v18, v17, 0x1

    int-to-byte v0, v15

    move/from16 v28, v0

    aput-byte v28, p1, v17

    move/from16 v17, v18

    :goto_0
    move/from16 v14, v17

    const/16 v16, 0x0

    move/from16 v18, v17

    :goto_1
    move/from16 v0, v16

    if-ge v0, v15, :cond_b

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mData:Ljava/util/ArrayList;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;

    iget v0, v13, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mCachedAddress:I

    move/from16 v28, v0

    move/from16 v0, v18

    move/from16 v1, v28

    if-eq v0, v1, :cond_2

    new-instance v28, Ljava/lang/RuntimeException;

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "Bug: write index is not the same as the cached address of the group : "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, " <> "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    iget v0, v13, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mCachedAddress:I

    move/from16 v30, v0

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-direct/range {v28 .. v29}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v28

    :cond_0
    const/16 v28, 0x2

    move/from16 v0, v28

    if-ne v0, v11, :cond_1

    add-int/lit8 v18, v17, 0x1

    shr-int/lit8 v28, v15, 0x8

    move/from16 v0, v28

    or-int/lit16 v0, v0, 0x80

    move/from16 v28, v0

    move/from16 v0, v28

    int-to-byte v0, v0

    move/from16 v28, v0

    aput-byte v28, p1, v17

    add-int/lit8 v17, v18, 0x1

    and-int/lit16 v0, v15, 0xff

    move/from16 v28, v0

    move/from16 v0, v28

    int-to-byte v0, v0

    move/from16 v28, v0

    aput-byte v28, p1, v18

    goto :goto_0

    :cond_1
    new-instance v28, Ljava/lang/RuntimeException;

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "Strange size from getGroupCountSize : "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-direct/range {v28 .. v29}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v28

    :cond_2
    invoke-static {v13}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->getGroupCharactersSize(Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;)I

    move-result v28

    add-int/lit8 v28, v28, 0x1

    add-int v14, v14, v28

    iget v0, v13, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mFrequency:I

    move/from16 v28, v0

    if-ltz v28, :cond_3

    add-int/lit8 v14, v14, 0x1

    :cond_3
    iget-object v0, v13, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mChildren:Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    move-object/from16 v28, v0

    if-nez v28, :cond_6

    const/high16 v10, -0x80000000

    :goto_2
    invoke-static {v13, v14, v10}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->makeCharGroupFlags(Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;II)B

    move-result v12

    add-int/lit8 v17, v18, 0x1

    aput-byte v12, p1, v18

    iget-object v0, v13, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mChars:[I

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move-object/from16 v1, p1

    move/from16 v2, v17

    # invokes: Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$CharEncoding;->writeCharArray([I[BI)I
    invoke-static {v0, v1, v2}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$CharEncoding;->access$200([I[BI)I

    move-result v17

    invoke-virtual {v13}, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->hasSeveralChars()Z

    move-result v28

    if-eqz v28, :cond_4

    add-int/lit8 v18, v17, 0x1

    const/16 v28, 0x1f

    aput-byte v28, p1, v17

    move/from16 v17, v18

    :cond_4
    iget v0, v13, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mFrequency:I

    move/from16 v28, v0

    if-ltz v28, :cond_5

    add-int/lit8 v18, v17, 0x1

    iget v0, v13, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mFrequency:I

    move/from16 v28, v0

    move/from16 v0, v28

    int-to-byte v0, v0

    move/from16 v28, v0

    aput-byte v28, p1, v17

    move/from16 v17, v18

    :cond_5
    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-static {v0, v1, v10}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->writeVariableAddress([BII)I

    move-result v21

    add-int v17, v17, v21

    add-int v14, v14, v21

    iget-object v0, v13, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mShortcutTargets:Ljava/util/ArrayList;

    move-object/from16 v28, v0

    if-eqz v28, :cond_9

    move/from16 v19, v17

    add-int/lit8 v17, v17, 0x2

    add-int/lit8 v14, v14, 0x2

    iget-object v0, v13, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mShortcutTargets:Ljava/util/ArrayList;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v24

    :goto_3
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    move-result v28

    if-eqz v28, :cond_7

    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;

    add-int/lit8 v14, v14, 0x1

    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    move-result v28

    move-object/from16 v0, v26

    iget v0, v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;->mFrequency:I

    move/from16 v29, v0

    invoke-static/range {v28 .. v29}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->makeShortcutFlags(ZI)I

    move-result v23

    add-int/lit8 v18, v17, 0x1

    move/from16 v0, v23

    int-to-byte v0, v0

    move/from16 v28, v0

    aput-byte v28, p1, v17

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;->mWord:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move-object/from16 v2, v28

    # invokes: Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$CharEncoding;->writeString([BILjava/lang/String;)I
    invoke-static {v0, v1, v2}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$CharEncoding;->access$300([BILjava/lang/String;)I

    move-result v25

    add-int v17, v18, v25

    add-int v14, v14, v25

    goto :goto_3

    :cond_6
    iget-object v0, v13, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mChildren:Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mCachedAddress:I

    move/from16 v28, v0

    sub-int v10, v28, v14

    goto/16 :goto_2

    :cond_7
    sub-int v22, v17, v19

    const v28, 0xffff

    move/from16 v0, v22

    move/from16 v1, v28

    if-le v0, v1, :cond_8

    new-instance v28, Ljava/lang/RuntimeException;

    const-string v29, "Shortcut list too large"

    invoke-direct/range {v28 .. v29}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v28

    :cond_8
    shr-int/lit8 v28, v22, 0x8

    move/from16 v0, v28

    int-to-byte v0, v0

    move/from16 v28, v0

    aput-byte v28, p1, v19

    add-int/lit8 v28, v19, 0x1

    move/from16 v0, v22

    and-int/lit16 v0, v0, 0xff

    move/from16 v29, v0

    move/from16 v0, v29

    int-to-byte v0, v0

    move/from16 v29, v0

    aput-byte v29, p1, v28

    :cond_9
    iget-object v0, v13, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mBigrams:Ljava/util/ArrayList;

    move-object/from16 v28, v0

    if-eqz v28, :cond_a

    iget-object v0, v13, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mBigrams:Ljava/util/ArrayList;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v28

    if-eqz v28, :cond_a

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->mRoot:Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    move-object/from16 v28, v0

    iget-object v0, v6, Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;->mWord:Ljava/lang/String;

    move-object/from16 v29, v0

    invoke-static/range {v28 .. v29}, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->findWordInTree(Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;Ljava/lang/String;)Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;

    move-result-object v26

    move-object/from16 v0, v26

    iget v5, v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mCachedAddress:I

    move-object/from16 v0, v26

    iget v0, v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mFrequency:I

    move/from16 v27, v0

    add-int/lit8 v14, v14, 0x1

    sub-int v20, v5, v14

    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v28

    iget v0, v6, Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;->mFrequency:I

    move/from16 v29, v0

    iget-object v0, v6, Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;->mWord:Ljava/lang/String;

    move-object/from16 v30, v0

    move/from16 v0, v28

    move/from16 v1, v20

    move/from16 v2, v29

    move/from16 v3, v27

    move-object/from16 v4, v30

    invoke-static {v0, v1, v2, v3, v4}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->makeBigramFlags(ZIIILjava/lang/String;)I

    move-result v7

    add-int/lit8 v18, v17, 0x1

    int-to-byte v0, v7

    move/from16 v28, v0

    aput-byte v28, p1, v17

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(I)I

    move-result v28

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v28

    invoke-static {v0, v1, v2}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->writeVariableAddress([BII)I

    move-result v9

    add-int v17, v18, v9

    add-int/2addr v14, v9

    goto :goto_4

    :cond_a
    add-int/lit8 v16, v16, 0x1

    move/from16 v18, v17

    goto/16 :goto_1

    :cond_b
    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mCachedAddress:I

    move/from16 v28, v0

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mCachedSize:I

    move/from16 v29, v0

    add-int v28, v28, v29

    move/from16 v0, v18

    move/from16 v1, v28

    if-eq v0, v1, :cond_c

    new-instance v28, Ljava/lang/RuntimeException;

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "Not the same size : written "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mCachedAddress:I

    move/from16 v30, v0

    sub-int v30, v18, v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, " bytes out of a node that should have "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mCachedSize:I

    move/from16 v30, v0

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, " bytes"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-direct/range {v28 .. v29}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v28

    :cond_c
    return v18
.end method

.method private static writeVariableAddress([BII)I
    .locals 4
    .param p0    # [B
    .param p1    # I
    .param p2    # I

    invoke-static {p2}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->getByteSize(I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Address "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " has a strange size"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    add-int/lit8 v0, p1, 0x1

    int-to-byte v1, p2

    aput-byte v1, p0, p1

    const/4 v1, 0x1

    move p1, v0

    :goto_0
    return v1

    :pswitch_1
    add-int/lit8 v0, p1, 0x1

    shr-int/lit8 v1, p2, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p0, p1

    add-int/lit8 p1, v0, 0x1

    and-int/lit16 v1, p2, 0xff

    int-to-byte v1, v1

    aput-byte v1, p0, v0

    const/4 v1, 0x2

    goto :goto_0

    :pswitch_2
    add-int/lit8 v0, p1, 0x1

    shr-int/lit8 v1, p2, 0x10

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p0, p1

    add-int/lit8 p1, v0, 0x1

    shr-int/lit8 v1, p2, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p0, v0

    add-int/lit8 v0, p1, 0x1

    and-int/lit16 v1, p2, 0xff

    int-to-byte v1, v1

    aput-byte v1, p0, p1

    const/4 v1, 0x3

    move p1, v0

    goto :goto_0

    :pswitch_3
    const/4 v1, 0x0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
