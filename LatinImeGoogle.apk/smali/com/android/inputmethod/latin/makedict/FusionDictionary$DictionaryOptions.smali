.class public Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryOptions;
.super Ljava/lang/Object;
.source "FusionDictionary.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/latin/makedict/FusionDictionary;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DictionaryOptions"
.end annotation


# instance fields
.field public final mAttributes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final mFrenchLigatureProcessing:Z

.field public final mGermanUmlautProcessing:Z


# direct methods
.method public constructor <init>(Ljava/util/HashMap;ZZ)V
    .locals 0
    .param p2    # Z
    .param p3    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;ZZ)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryOptions;->mAttributes:Ljava/util/HashMap;

    iput-boolean p2, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryOptions;->mGermanUmlautProcessing:Z

    iput-boolean p3, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryOptions;->mFrenchLigatureProcessing:Z

    return-void
.end method
