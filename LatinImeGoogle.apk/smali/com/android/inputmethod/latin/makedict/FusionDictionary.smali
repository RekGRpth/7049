.class public Lcom/android/inputmethod/latin/makedict/FusionDictionary;
.super Ljava/lang/Object;
.source "FusionDictionary.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/inputmethod/latin/makedict/FusionDictionary$1;,
        Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryIterator;,
        Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroupComparator;,
        Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryOptions;,
        Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;,
        Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;,
        Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "Lcom/android/inputmethod/latin/makedict/Word;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static ARRAYS_ARE_EQUAL:I

.field private static CHARACTER_NOT_FOUND:I

.field private static final CHARGROUP_COMPARATOR:Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroupComparator;


# instance fields
.field public final mOptions:Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryOptions;

.field public final mRoot:Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    const-class v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->$assertionsDisabled:Z

    sput v1, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->ARRAYS_ARE_EQUAL:I

    new-instance v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroupComparator;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroupComparator;-><init>(Lcom/android/inputmethod/latin/makedict/FusionDictionary$1;)V

    sput-object v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->CHARGROUP_COMPARATOR:Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroupComparator;

    const/4 v0, -0x1

    sput v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->CHARACTER_NOT_FOUND:I

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public constructor <init>(Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryOptions;)V
    .locals 0
    .param p1    # Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;
    .param p2    # Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryOptions;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->mRoot:Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    iput-object p2, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->mOptions:Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryOptions;

    return-void
.end method

.method private add([IILjava/util/ArrayList;)V
    .locals 20
    .param p1    # [I
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([II",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;",
            ">;)V"
        }
    .end annotation

    sget-boolean v5, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->$assertionsDisabled:Z

    if-nez v5, :cond_1

    if-ltz p2, :cond_0

    const/16 v5, 0xff

    move/from16 v0, p2

    if-le v0, v5, :cond_1

    :cond_0
    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    :cond_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->mRoot:Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->mRoot:Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    aget v6, p1, v11

    invoke-static {v5, v6}, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->findIndexOfChar(Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;I)I

    move-result v19

    :goto_0
    sget v5, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->CHARACTER_NOT_FOUND:I

    move/from16 v0, v19

    if-eq v5, v0, :cond_2

    iget-object v5, v13, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mData:Ljava/util/ArrayList;

    move/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;

    iget-object v5, v12, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mChars:[I

    move-object/from16 v0, p1

    invoke-static {v5, v0, v11}, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->compareArrays([I[II)I

    move-result v14

    sget v5, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->ARRAYS_ARE_EQUAL:I

    if-eq v5, v14, :cond_3

    iget-object v5, v12, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mChars:[I

    array-length v5, v5

    if-ge v14, v5, :cond_3

    :cond_2
    const/4 v5, -0x1

    move/from16 v0, v19

    if-ne v5, v0, :cond_4

    aget v5, p1, v11

    invoke-static {v13, v5}, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->findInsertionIndex(Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;I)I

    move-result v15

    new-instance v16, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;

    move-object/from16 v0, p1

    array-length v5, v0

    move-object/from16 v0, p1

    invoke-static {v0, v11, v5}, Ljava/util/Arrays;->copyOfRange([III)[I

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, p3

    move/from16 v2, p2

    invoke-direct {v0, v5, v1, v6, v2}, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;-><init>([ILjava/util/ArrayList;Ljava/util/ArrayList;I)V

    iget-object v5, v13, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mData:Ljava/util/ArrayList;

    move-object/from16 v0, v16

    invoke-virtual {v5, v15, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    :goto_1
    return-void

    :cond_3
    iget-object v5, v12, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mChildren:Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    if-eqz v5, :cond_2

    iget-object v5, v12, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mChars:[I

    array-length v5, v5

    add-int/2addr v11, v5

    move-object/from16 v0, p1

    array-length v5, v0

    if-ge v11, v5, :cond_2

    iget-object v13, v12, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mChildren:Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    aget v5, p1, v11

    invoke-static {v13, v5}, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->findIndexOfChar(Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;I)I

    move-result v19

    goto :goto_0

    :cond_4
    iget-object v5, v12, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mChars:[I

    array-length v5, v5

    if-ne v14, v5, :cond_6

    add-int v5, v11, v14

    move-object/from16 v0, p1

    array-length v6, v0

    if-lt v5, v6, :cond_5

    const/4 v5, 0x0

    move/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v12, v0, v1, v5}, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->update(ILjava/util/ArrayList;Ljava/util/ArrayList;)V

    goto :goto_1

    :cond_5
    new-instance v17, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;

    add-int v5, v11, v14

    move-object/from16 v0, p1

    array-length v6, v0

    move-object/from16 v0, p1

    invoke-static {v0, v5, v6}, Ljava/util/Arrays;->copyOfRange([III)[I

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, p3

    move/from16 v2, p2

    invoke-direct {v0, v5, v1, v6, v2}, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;-><init>([ILjava/util/ArrayList;Ljava/util/ArrayList;I)V

    new-instance v5, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    invoke-direct {v5}, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;-><init>()V

    iput-object v5, v12, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mChildren:Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    iget-object v5, v12, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mChildren:Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    iget-object v5, v5, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mData:Ljava/util/ArrayList;

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_6
    if-nez v14, :cond_7

    const/4 v5, 0x0

    move/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v12, v0, v1, v5}, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->update(ILjava/util/ArrayList;Ljava/util/ArrayList;)V

    goto :goto_1

    :cond_7
    new-instance v9, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    invoke-direct {v9}, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;-><init>()V

    new-instance v3, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;

    iget-object v5, v12, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mChars:[I

    iget-object v6, v12, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mChars:[I

    array-length v6, v6

    invoke-static {v5, v14, v6}, Ljava/util/Arrays;->copyOfRange([III)[I

    move-result-object v4

    iget-object v5, v12, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mShortcutTargets:Ljava/util/ArrayList;

    iget-object v6, v12, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mBigrams:Ljava/util/ArrayList;

    iget v7, v12, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mFrequency:I

    iget-object v8, v12, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mChildren:Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    invoke-direct/range {v3 .. v8}, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;-><init>([ILjava/util/ArrayList;Ljava/util/ArrayList;ILcom/android/inputmethod/latin/makedict/FusionDictionary$Node;)V

    iget-object v5, v9, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mData:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int v5, v11, v14

    move-object/from16 v0, p1

    array-length v6, v0

    if-lt v5, v6, :cond_8

    new-instance v4, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;

    iget-object v5, v12, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mChars:[I

    const/4 v6, 0x0

    invoke-static {v5, v6, v14}, Ljava/util/Arrays;->copyOfRange([III)[I

    move-result-object v5

    const/4 v7, 0x0

    move-object/from16 v6, p3

    move/from16 v8, p2

    invoke-direct/range {v4 .. v9}, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;-><init>([ILjava/util/ArrayList;Ljava/util/ArrayList;ILcom/android/inputmethod/latin/makedict/FusionDictionary$Node;)V

    :goto_2
    iget-object v5, v13, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mData:Ljava/util/ArrayList;

    move/from16 v0, v19

    invoke-virtual {v5, v0, v4}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    :cond_8
    new-instance v4, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;

    iget-object v5, v12, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mChars:[I

    const/4 v6, 0x0

    invoke-static {v5, v6, v14}, Ljava/util/Arrays;->copyOfRange([III)[I

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, -0x1

    invoke-direct/range {v4 .. v9}, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;-><init>([ILjava/util/ArrayList;Ljava/util/ArrayList;ILcom/android/inputmethod/latin/makedict/FusionDictionary$Node;)V

    new-instance v18, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;

    add-int v5, v11, v14

    move-object/from16 v0, p1

    array-length v6, v0

    move-object/from16 v0, p1

    invoke-static {v0, v5, v6}, Ljava/util/Arrays;->copyOfRange([III)[I

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p3

    move/from16 v2, p2

    invoke-direct {v0, v5, v1, v6, v2}, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;-><init>([ILjava/util/ArrayList;Ljava/util/ArrayList;I)V

    add-int v5, v11, v14

    aget v5, p1, v5

    iget-object v6, v12, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mChars:[I

    aget v6, v6, v14

    if-le v5, v6, :cond_9

    const/4 v10, 0x1

    :goto_3
    iget-object v5, v9, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mData:Ljava/util/ArrayList;

    move-object/from16 v0, v18

    invoke-virtual {v5, v10, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_2

    :cond_9
    const/4 v10, 0x0

    goto :goto_3
.end method

.method private static compareArrays([I[II)I
    .locals 3
    .param p0    # [I
    .param p1    # [I
    .param p2    # I

    const/4 v0, 0x1

    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_2

    add-int v1, p2, v0

    array-length v2, p1

    if-lt v1, v2, :cond_1

    :cond_0
    :goto_1
    return v0

    :cond_1
    aget v1, p0, v0

    add-int v2, p2, v0

    aget v2, p1, v2

    if-ne v1, v2, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    array-length v1, p1

    array-length v2, p0

    if-le v1, v2, :cond_3

    array-length v0, p0

    goto :goto_1

    :cond_3
    sget v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->ARRAYS_ARE_EQUAL:I

    goto :goto_1
.end method

.method public static countCharGroups(Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;)I
    .locals 5
    .param p0    # Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    iget-object v4, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mData:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v3, v2

    add-int/lit8 v1, v2, -0x1

    :goto_0
    if-ltz v1, :cond_1

    iget-object v4, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mData:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;

    iget-object v4, v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mChildren:Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    if-eqz v4, :cond_0

    iget-object v4, v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mChildren:Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    invoke-static {v4}, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->countCharGroups(Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_1
    return v3
.end method

.method private static findIndexOfChar(Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;I)I
    .locals 3
    .param p0    # Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;
    .param p1    # I

    invoke-static {p0, p1}, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->findInsertionIndex(Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;I)I

    move-result v0

    iget-object v1, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mData:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gt v1, v0, :cond_1

    sget v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->CHARACTER_NOT_FOUND:I

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mData:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;

    iget-object v1, v1, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mChars:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    if-eq p1, v1, :cond_0

    sget v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->CHARACTER_NOT_FOUND:I

    goto :goto_0
.end method

.method private static findInsertionIndex(Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;I)I
    .locals 6
    .param p0    # Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;
    .param p1    # I

    const/4 v5, 0x0

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mData:Ljava/util/ArrayList;

    new-instance v1, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;

    const/4 v3, 0x1

    new-array v3, v3, [I

    aput p1, v3, v4

    invoke-direct {v1, v3, v5, v5, v4}, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;-><init>([ILjava/util/ArrayList;Ljava/util/ArrayList;I)V

    sget-object v3, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->CHARGROUP_COMPARATOR:Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroupComparator;

    invoke-static {v0, v1, v3}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;)I

    move-result v2

    if-ltz v2, :cond_0

    :goto_0
    return v2

    :cond_0
    neg-int v3, v2

    add-int/lit8 v2, v3, -0x1

    goto :goto_0
.end method

.method public static findWordInTree(Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;Ljava/lang/String;)Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;
    .locals 6
    .param p0    # Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v2, 0x0

    move-object v0, v4

    check-cast v0, Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {p1, v2}, Ljava/lang/String;->codePointAt(I)I

    move-result v5

    invoke-static {p0, v5}, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->findIndexOfChar(Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;I)I

    move-result v3

    sget v5, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->CHARACTER_NOT_FOUND:I

    if-ne v5, v3, :cond_1

    :goto_0
    return-object v4

    :cond_1
    iget-object v5, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mData:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;

    iget-object v5, v1, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mChars:[I

    array-length v5, v5

    add-int/2addr v2, v5

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v2, v5, :cond_2

    iget-object p0, v1, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mChildren:Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    :cond_2
    if-eqz p0, :cond_3

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    if-lt v2, v5, :cond_0

    :cond_3
    move-object v4, v1

    goto :goto_0
.end method

.method private static getCodePoints(Ljava/lang/String;)[I
    .locals 8
    .param p0    # Ljava/lang/String;

    const/4 v7, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v4, v0

    invoke-static {v0, v7, v4}, Ljava/lang/Character;->codePointCount([CII)I

    move-result v6

    new-array v2, v6, [I

    invoke-static {v0, v7}, Ljava/lang/Character;->codePointAt([CI)I

    move-result v1

    const/4 v3, 0x0

    invoke-static {v1}, Ljava/lang/Character;->charCount(I)I

    move-result v5

    :goto_0
    if-ge v5, v4, :cond_0

    aput v1, v2, v3

    invoke-static {v0, v5}, Ljava/lang/Character;->codePointAt([CI)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->charCount(I)I

    move-result v6

    add-int/2addr v5, v6

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    aput v1, v2, v3

    return-object v2
.end method

.method private static hasBigramsInternal(Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;)Z
    .locals 5
    .param p0    # Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    const/4 v3, 0x1

    const/4 v2, 0x0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v4, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mData:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v1, v4, -0x1

    :goto_1
    if-ltz v1, :cond_0

    iget-object v4, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mData:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;

    iget-object v4, v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mBigrams:Ljava/util/ArrayList;

    if-eqz v4, :cond_2

    move v2, v3

    goto :goto_0

    :cond_2
    iget-object v4, v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mChildren:Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    invoke-static {v4}, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->hasBigramsInternal(Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;)Z

    move-result v4

    if-eqz v4, :cond_3

    move v2, v3

    goto :goto_0

    :cond_3
    add-int/lit8 v1, v1, -0x1

    goto :goto_1
.end method


# virtual methods
.method public add(Ljava/lang/String;ILjava/util/ArrayList;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;",
            ">;)V"
        }
    .end annotation

    invoke-static {p1}, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->getCodePoints(Ljava/lang/String;)[I

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->add([IILjava/util/ArrayList;)V

    return-void
.end method

.method public hasBigrams()Z
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->mRoot:Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    invoke-static {v0}, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->hasBigramsInternal(Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;)Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lcom/android/inputmethod/latin/makedict/Word;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryIterator;

    iget-object v1, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->mRoot:Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    iget-object v1, v1, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mData:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryIterator;-><init>(Ljava/util/ArrayList;)V

    return-object v0
.end method

.method public setBigram(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    iget-object v2, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->mRoot:Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    invoke-static {v2, p1}, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->findWordInTree(Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;Ljava/lang/String;)Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->mRoot:Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    invoke-static {v2, p2}, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->findWordInTree(Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;Ljava/lang/String;)Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {p2}, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->getCodePoints(Ljava/lang/String;)[I

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {p0, v2, v3, v4}, Lcom/android/inputmethod/latin/makedict/FusionDictionary;->add([IILjava/util/ArrayList;)V

    :cond_0
    invoke-virtual {v0, p2, p3}, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->addBigram(Ljava/lang/String;I)V

    return-void

    :cond_1
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "First word of bigram not found"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
.end method
