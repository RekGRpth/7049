.class Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$1;
.super Ljava/lang/Thread;
.source "AndroidSpellCheckerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->closeAllDictionaries()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;

.field final synthetic val$oldPools:Ljava/util/Map;

.field final synthetic val$oldUserDictionaries:Ljava/util/Map;

.field final synthetic val$oldWhitelistDictionaries:Ljava/util/Map;


# direct methods
.method constructor <init>(Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V
    .locals 0
    .param p2    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$1;->this$0:Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;

    iput-object p3, p0, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$1;->val$oldPools:Ljava/util/Map;

    iput-object p4, p0, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$1;->val$oldUserDictionaries:Ljava/util/Map;

    iput-object p5, p0, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$1;->val$oldWhitelistDictionaries:Ljava/util/Map;

    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    iget-object v4, p0, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$1;->val$oldPools:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/inputmethod/latin/spellcheck/DictionaryPool;

    invoke-virtual {v3}, Lcom/android/inputmethod/latin/spellcheck/DictionaryPool;->close()V

    goto :goto_0

    :cond_0
    iget-object v4, p0, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$1;->val$oldUserDictionaries:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/inputmethod/latin/Dictionary;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/Dictionary;->close()V

    goto :goto_1

    :cond_1
    iget-object v4, p0, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$1;->val$oldWhitelistDictionaries:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/inputmethod/latin/Dictionary;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/Dictionary;->close()V

    goto :goto_2

    :cond_2
    iget-object v4, p0, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$1;->this$0:Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;

    # getter for: Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->mUseContactsLock:Ljava/lang/Object;
    invoke-static {v4}, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->access$100(Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;)Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    :try_start_0
    iget-object v4, p0, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$1;->this$0:Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;

    # getter for: Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->mContactsDictionary:Lcom/android/inputmethod/latin/Dictionary;
    invoke-static {v4}, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->access$200(Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;)Lcom/android/inputmethod/latin/Dictionary;

    move-result-object v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$1;->this$0:Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;

    # getter for: Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->mContactsDictionary:Lcom/android/inputmethod/latin/Dictionary;
    invoke-static {v4}, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->access$200(Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;)Lcom/android/inputmethod/latin/Dictionary;

    move-result-object v1

    iget-object v4, p0, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$1;->this$0:Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;

    const/4 v6, 0x0

    # setter for: Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->mContactsDictionary:Lcom/android/inputmethod/latin/Dictionary;
    invoke-static {v4, v6}, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->access$202(Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;Lcom/android/inputmethod/latin/Dictionary;)Lcom/android/inputmethod/latin/Dictionary;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/Dictionary;->close()V

    :cond_3
    monitor-exit v5

    return-void

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4
.end method
