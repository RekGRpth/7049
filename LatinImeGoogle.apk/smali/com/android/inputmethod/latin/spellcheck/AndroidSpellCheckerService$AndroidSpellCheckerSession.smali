.class Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession;
.super Landroid/service/textservice/SpellCheckerService$Session;
.source "AndroidSpellCheckerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AndroidSpellCheckerSession"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession$SuggestionsCache;,
        Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession$SuggestionsParams;
    }
.end annotation


# instance fields
.field private mDictionaryPool:Lcom/android/inputmethod/latin/spellcheck/DictionaryPool;

.field private mLocale:Ljava/util/Locale;

.field private final mObserver:Landroid/database/ContentObserver;

.field private mScript:I

.field private final mService:Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;

.field private final mSuggestionsCache:Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession$SuggestionsCache;


# direct methods
.method constructor <init>(Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;)V
    .locals 4
    .param p1    # Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/service/textservice/SpellCheckerService$Session;-><init>()V

    new-instance v1, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession$SuggestionsCache;

    invoke-direct {v1, v2}, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession$SuggestionsCache;-><init>(Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$1;)V

    iput-object v1, p0, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession;->mSuggestionsCache:Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession$SuggestionsCache;

    iput-object p1, p0, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession;->mService:Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;

    invoke-virtual {p1}, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession$1;

    invoke-direct {v1, p0, v2}, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession$1;-><init>(Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession;->mObserver:Landroid/database/ContentObserver;

    sget-object v1, Landroid/provider/UserDictionary$Words;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession;->mObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    return-void
.end method

.method static synthetic access$400(Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession;)Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession$SuggestionsCache;
    .locals 1
    .param p0    # Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession;

    iget-object v0, p0, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession;->mSuggestionsCache:Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession$SuggestionsCache;

    return-object v0
.end method

.method private fixWronglyInvalidatedWordWithSingleQuote(Landroid/view/textservice/TextInfo;Landroid/view/textservice/SentenceSuggestionsInfo;)Landroid/view/textservice/SentenceSuggestionsInfo;
    .locals 28
    .param p1    # Landroid/view/textservice/TextInfo;
    .param p2    # Landroid/view/textservice/SentenceSuggestionsInfo;

    invoke-virtual/range {p1 .. p1}, Landroid/view/textservice/TextInfo;->getText()Ljava/lang/String;

    move-result-object v25

    const-string v26, "\'"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_0

    const/16 v26, 0x0

    :goto_0
    return-object v26

    :cond_0
    invoke-virtual/range {p2 .. p2}, Landroid/view/textservice/SentenceSuggestionsInfo;->getSuggestionsCount()I

    move-result v3

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    const/4 v9, 0x0

    :goto_1
    if-ge v9, v3, :cond_5

    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Landroid/view/textservice/SentenceSuggestionsInfo;->getSuggestionsInfoAt(I)Landroid/view/textservice/SuggestionsInfo;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/view/textservice/SuggestionsInfo;->getSuggestionsAttributes()I

    move-result v8

    and-int/lit8 v26, v8, 0x1

    if-nez v26, :cond_2

    :cond_1
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    :cond_2
    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Landroid/view/textservice/SentenceSuggestionsInfo;->getOffsetAt(I)I

    move-result v18

    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Landroid/view/textservice/SentenceSuggestionsInfo;->getLengthAt(I)I

    move-result v11

    add-int v26, v18, v11

    move-object/from16 v0, v25

    move/from16 v1, v18

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v23

    const-string v26, "\'"

    move-object/from16 v0, v23

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v26

    if-eqz v26, :cond_1

    const-string v26, "\'"

    const/16 v27, -0x1

    move-object/from16 v0, v23

    move-object/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v22

    if-eqz v22, :cond_1

    move-object/from16 v0, v22

    array-length v0, v0

    move/from16 v26, v0

    const/16 v27, 0x1

    move/from16 v0, v26

    move/from16 v1, v27

    if-le v0, v1, :cond_1

    move-object/from16 v0, v22

    array-length v0, v0

    move/from16 v20, v0

    const/4 v10, 0x0

    :goto_2
    move/from16 v0, v20

    if-ge v10, v0, :cond_1

    aget-object v21, v22, v10

    invoke-static/range {v21 .. v21}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v26

    if-eqz v26, :cond_4

    :cond_3
    :goto_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession;->mSuggestionsCache:Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession$SuggestionsCache;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession$SuggestionsCache;->getSuggestionsFromCache(Ljava/lang/String;)Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession$SuggestionsParams;

    move-result-object v26

    if-eqz v26, :cond_3

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v13

    const/4 v12, 0x0

    new-instance v16, Landroid/view/textservice/SuggestionsInfo;

    const/16 v26, 0x0

    # getter for: Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->EMPTY_STRING_ARRAY:[Ljava/lang/String;
    invoke-static {}, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->access$000()[Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v16

    move/from16 v1, v26

    move-object/from16 v2, v27

    invoke-direct {v0, v1, v2}, Landroid/view/textservice/SuggestionsInfo;-><init>(I[Ljava/lang/String;)V

    invoke-virtual/range {v19 .. v19}, Landroid/view/textservice/SuggestionsInfo;->getCookie()I

    move-result v26

    invoke-virtual/range {v19 .. v19}, Landroid/view/textservice/SuggestionsInfo;->getSequence()I

    move-result v27

    move-object/from16 v0, v16

    move/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/view/textservice/SuggestionsInfo;->setCookieAndSequence(II)V

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, v16

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_5
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-gtz v6, :cond_6

    const/16 v26, 0x0

    goto/16 :goto_0

    :cond_6
    add-int v24, v3, v6

    move/from16 v0, v24

    new-array v15, v0, [I

    move/from16 v0, v24

    new-array v14, v0, [I

    move/from16 v0, v24

    new-array v0, v0, [Landroid/view/textservice/SuggestionsInfo;

    move-object/from16 v17, v0

    const/4 v9, 0x0

    :goto_4
    if-ge v9, v3, :cond_7

    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Landroid/view/textservice/SentenceSuggestionsInfo;->getOffsetAt(I)I

    move-result v26

    aput v26, v15, v9

    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Landroid/view/textservice/SentenceSuggestionsInfo;->getLengthAt(I)I

    move-result v26

    aput v26, v14, v9

    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Landroid/view/textservice/SentenceSuggestionsInfo;->getSuggestionsInfoAt(I)Landroid/view/textservice/SuggestionsInfo;

    move-result-object v26

    aput-object v26, v17, v9

    add-int/lit8 v9, v9, 0x1

    goto :goto_4

    :cond_7
    :goto_5
    move/from16 v0, v24

    if-ge v9, v0, :cond_8

    sub-int v26, v9, v3

    move/from16 v0, v26

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/Integer;

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Integer;->intValue()I

    move-result v26

    aput v26, v15, v9

    sub-int v26, v9, v3

    move/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/Integer;

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Integer;->intValue()I

    move-result v26

    aput v26, v14, v9

    sub-int v26, v9, v3

    move/from16 v0, v26

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/textservice/SuggestionsInfo;

    aput-object v26, v17, v9

    add-int/lit8 v9, v9, 0x1

    goto :goto_5

    :cond_8
    new-instance v26, Landroid/view/textservice/SentenceSuggestionsInfo;

    move-object/from16 v0, v26

    move-object/from16 v1, v17

    invoke-direct {v0, v1, v15, v14}, Landroid/view/textservice/SentenceSuggestionsInfo;-><init>([Landroid/view/textservice/SuggestionsInfo;[I[I)V

    goto/16 :goto_0
.end method

.method private static isLetterCheckableByLanguage(II)Z
    .locals 3
    .param p0    # I
    .param p1    # I

    const/4 v0, 0x1

    const/4 v1, 0x0

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Impossible value of script: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const/16 v2, 0x2af

    if-gt p0, v2, :cond_1

    invoke-static {p0}, Ljava/lang/Character;->isLetter(I)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :pswitch_1
    const/16 v2, 0x400

    if-lt p0, v2, :cond_2

    const/16 v2, 0x52f

    if-gt p0, v2, :cond_2

    invoke-static {p0}, Ljava/lang/Character;->isLetter(I)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private onGetSuggestions(Landroid/view/textservice/TextInfo;Ljava/lang/String;I)Landroid/view/textservice/SuggestionsInfo;
    .locals 24
    .param p1    # Landroid/view/textservice/TextInfo;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/view/textservice/TextInfo;->getText()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession;->mSuggestionsCache:Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession$SuggestionsCache;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v13}, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession$SuggestionsCache;->getSuggestionsFromCache(Ljava/lang/String;)Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession$SuggestionsParams;

    move-result-object v5

    if-eqz v5, :cond_1

    new-instance v21, Landroid/view/textservice/SuggestionsInfo;

    iget v0, v5, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession$SuggestionsParams;->mFlags:I

    move/from16 v22, v0

    iget-object v0, v5, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession$SuggestionsParams;->mSuggestions:[Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-direct/range {v21 .. v23}, Landroid/view/textservice/SuggestionsInfo;-><init>(I[Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-object v21

    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession;->mScript:I

    move/from16 v21, v0

    move/from16 v0, v21

    invoke-static {v13, v0}, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession;->shouldFilterOut(Ljava/lang/String;I)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v21

    if-eqz v21, :cond_5

    const/4 v9, 0x0

    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession;->mDictionaryPool:Lcom/android/inputmethod/latin/spellcheck/DictionaryPool;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/android/inputmethod/latin/spellcheck/DictionaryPool;->takeOrGetNull()Lcom/android/inputmethod/latin/spellcheck/DictAndProximity;

    move-result-object v9

    if-nez v9, :cond_2

    # invokes: Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->getNotInDictEmptySuggestions()Landroid/view/textservice/SuggestionsInfo;
    invoke-static {}, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->access$700()Landroid/view/textservice/SuggestionsInfo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v21

    if-eqz v9, :cond_0

    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession;->mDictionaryPool:Lcom/android/inputmethod/latin/spellcheck/DictionaryPool;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v9}, Lcom/android/inputmethod/latin/spellcheck/DictionaryPool;->offer(Lcom/android/inputmethod/latin/spellcheck/DictAndProximity;)Z

    move-result v22

    if-nez v22, :cond_0

    # getter for: Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->access$900()Ljava/lang/String;

    move-result-object v22

    const-string v23, "Can\'t re-insert a dictionary into its pool"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v10

    # getter for: Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->access$900()Ljava/lang/String;

    move-result-object v21

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Exception while spellcheking: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    # invokes: Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->getNotInDictEmptySuggestions()Landroid/view/textservice/SuggestionsInfo;
    invoke-static {}, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->access$700()Landroid/view/textservice/SuggestionsInfo;

    move-result-object v21

    goto :goto_0

    :cond_2
    :try_start_3
    iget-object v0, v9, Lcom/android/inputmethod/latin/spellcheck/DictAndProximity;->mDictionary:Lcom/android/inputmethod/latin/Dictionary;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v13}, Lcom/android/inputmethod/latin/Dictionary;->isValidWord(Ljava/lang/CharSequence;)Z

    move-result v21

    if-eqz v21, :cond_3

    # invokes: Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->getInDictEmptySuggestions()Landroid/view/textservice/SuggestionsInfo;
    invoke-static {}, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->access$800()Landroid/view/textservice/SuggestionsInfo;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v21

    :goto_1
    if-eqz v9, :cond_0

    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession;->mDictionaryPool:Lcom/android/inputmethod/latin/spellcheck/DictionaryPool;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v9}, Lcom/android/inputmethod/latin/spellcheck/DictionaryPool;->offer(Lcom/android/inputmethod/latin/spellcheck/DictAndProximity;)Z

    move-result v22

    if-nez v22, :cond_0

    # getter for: Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->access$900()Ljava/lang/String;

    move-result-object v22

    const-string v23, "Can\'t re-insert a dictionary into its pool"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_0

    :cond_3
    :try_start_5
    # invokes: Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->getNotInDictEmptySuggestions()Landroid/view/textservice/SuggestionsInfo;
    invoke-static {}, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->access$700()Landroid/view/textservice/SuggestionsInfo;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v21

    goto :goto_1

    :catchall_0
    move-exception v21

    if-eqz v9, :cond_4

    :try_start_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession;->mDictionaryPool:Lcom/android/inputmethod/latin/spellcheck/DictionaryPool;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v9}, Lcom/android/inputmethod/latin/spellcheck/DictionaryPool;->offer(Lcom/android/inputmethod/latin/spellcheck/DictAndProximity;)Z

    move-result v22

    if-nez v22, :cond_4

    # getter for: Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->access$900()Ljava/lang/String;

    move-result-object v22

    const-string v23, "Can\'t re-insert a dictionary into its pool"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    throw v21

    :cond_5
    const-string v21, "\u2019"

    const-string v22, "\'"

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    new-instance v18, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$SuggestionsGatherer;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession;->mService:Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;

    move-object/from16 v21, v0

    # getter for: Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->mSuggestionThreshold:F
    invoke-static/range {v21 .. v21}, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->access$1000(Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;)F

    move-result v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession;->mService:Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;

    move-object/from16 v22, v0

    # getter for: Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->mRecommendedThreshold:F
    invoke-static/range {v22 .. v22}, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->access$1100(Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;)F

    move-result v22

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, p3

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$SuggestionsGatherer;-><init>(Ljava/lang/String;FFI)V

    new-instance v8, Lcom/android/inputmethod/latin/WordComposer;

    invoke-direct {v8}, Lcom/android/inputmethod/latin/WordComposer;-><init>()V

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v15

    const/4 v12, 0x0

    :goto_2
    if-ge v12, v15, :cond_7

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/lang/String;->codePointAt(I)I

    move-result v7

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession;->mScript:I

    move/from16 v21, v0

    move/from16 v0, v21

    invoke-static {v7, v0}, Lcom/android/inputmethod/latin/spellcheck/SpellCheckerProximityInfo;->getXYForCodePointAndScript(II)I

    move-result v20

    const/16 v21, -0x1

    move/from16 v0, v21

    move/from16 v1, v20

    if-ne v0, v1, :cond_6

    const/16 v21, -0x1

    const/16 v22, -0x1

    const/16 v23, 0x0

    move/from16 v0, v21

    move/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v8, v7, v0, v1, v2}, Lcom/android/inputmethod/latin/WordComposer;->add(IIILcom/android/inputmethod/keyboard/KeyDetector;)V

    :goto_3
    const/16 v21, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v12, v1}, Ljava/lang/String;->offsetByCodePoints(II)I

    move-result v12

    goto :goto_2

    :cond_6
    const v21, 0xffff

    and-int v21, v21, v20

    shr-int/lit8 v22, v20, 0x10

    const/16 v23, 0x0

    move/from16 v0, v21

    move/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v8, v7, v0, v1, v2}, Lcom/android/inputmethod/latin/WordComposer;->add(IIILcom/android/inputmethod/keyboard/KeyDetector;)V

    goto :goto_3

    :cond_7
    # invokes: Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->getCapitalizationType(Ljava/lang/String;)I
    invoke-static/range {v19 .. v19}, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->access$1200(Ljava/lang/String;)I
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_0

    move-result v6

    const/4 v14, 0x1

    const/4 v9, 0x0

    :try_start_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession;->mDictionaryPool:Lcom/android/inputmethod/latin/spellcheck/DictionaryPool;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/android/inputmethod/latin/spellcheck/DictionaryPool;->takeOrGetNull()Lcom/android/inputmethod/latin/spellcheck/DictAndProximity;

    move-result-object v9

    if-nez v9, :cond_8

    # invokes: Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->getNotInDictEmptySuggestions()Landroid/view/textservice/SuggestionsInfo;
    invoke-static {}, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->access$700()Landroid/view/textservice/SuggestionsInfo;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move-result-object v21

    if-eqz v9, :cond_0

    :try_start_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession;->mDictionaryPool:Lcom/android/inputmethod/latin/spellcheck/DictionaryPool;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v9}, Lcom/android/inputmethod/latin/spellcheck/DictionaryPool;->offer(Lcom/android/inputmethod/latin/spellcheck/DictAndProximity;)Z

    move-result v22

    if-nez v22, :cond_0

    # getter for: Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->access$900()Ljava/lang/String;

    move-result-object v22

    const-string v23, "Can\'t re-insert a dictionary into its pool"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catch Ljava/lang/RuntimeException; {:try_start_8 .. :try_end_8} :catch_0

    goto/16 :goto_0

    :cond_8
    :try_start_9
    iget-object v0, v9, Lcom/android/inputmethod/latin/spellcheck/DictAndProximity;->mDictionary:Lcom/android/inputmethod/latin/Dictionary;

    move-object/from16 v21, v0

    iget-object v0, v9, Lcom/android/inputmethod/latin/spellcheck/DictAndProximity;->mProximityInfo:Lcom/android/inputmethod/keyboard/ProximityInfo;

    move-object/from16 v22, v0

    move-object/from16 v0, v21

    move-object/from16 v1, p2

    move-object/from16 v2, v18

    move-object/from16 v3, v22

    invoke-virtual {v0, v8, v1, v2, v3}, Lcom/android/inputmethod/latin/Dictionary;->getWords(Lcom/android/inputmethod/latin/WordComposer;Ljava/lang/CharSequence;Lcom/android/inputmethod/latin/Dictionary$WordCallback;Lcom/android/inputmethod/keyboard/ProximityInfo;)V

    iget-object v0, v9, Lcom/android/inputmethod/latin/spellcheck/DictAndProximity;->mDictionary:Lcom/android/inputmethod/latin/Dictionary;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/Dictionary;->isValidWord(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_9

    if-eqz v6, :cond_9

    iget-object v0, v9, Lcom/android/inputmethod/latin/spellcheck/DictAndProximity;->mDictionary:Lcom/android/inputmethod/latin/Dictionary;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession;->mLocale:Ljava/util/Locale;

    move-object/from16 v22, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Lcom/android/inputmethod/latin/Dictionary;->isValidWord(Ljava/lang/CharSequence;)Z
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    move-result v14

    :cond_9
    if-eqz v9, :cond_a

    :try_start_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession;->mDictionaryPool:Lcom/android/inputmethod/latin/spellcheck/DictionaryPool;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v9}, Lcom/android/inputmethod/latin/spellcheck/DictionaryPool;->offer(Lcom/android/inputmethod/latin/spellcheck/DictAndProximity;)Z

    move-result v21

    if-nez v21, :cond_a

    # getter for: Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->access$900()Ljava/lang/String;

    move-result-object v21

    const-string v22, "Can\'t re-insert a dictionary into its pool"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession;->mLocale:Ljava/util/Locale;

    move-object/from16 v21, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v6, v1}, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$SuggestionsGatherer;->getResults(ILjava/util/Locale;)Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$SuggestionsGatherer$Result;

    move-result-object v16

    if-eqz v14, :cond_c

    const/16 v21, 0x1

    move/from16 v22, v21

    :goto_4
    move-object/from16 v0, v16

    iget-boolean v0, v0, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$SuggestionsGatherer$Result;->mHasRecommendedSuggestions:Z

    move/from16 v21, v0

    if-eqz v21, :cond_d

    invoke-static {}, Lcom/android/inputmethod/compat/SuggestionsInfoCompatUtils;->getValueOf_RESULT_ATTR_HAS_RECOMMENDED_SUGGESTIONS()I

    move-result v21

    :goto_5
    or-int v11, v22, v21

    new-instance v17, Landroid/view/textservice/SuggestionsInfo;

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$SuggestionsGatherer$Result;->mSuggestions:[Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-direct {v0, v11, v1}, Landroid/view/textservice/SuggestionsInfo;-><init>(I[Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession;->mSuggestionsCache:Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession$SuggestionsCache;

    move-object/from16 v21, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$SuggestionsGatherer$Result;->mSuggestions:[Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2, v11}, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession$SuggestionsCache;->putSuggestionsToCache(Ljava/lang/String;[Ljava/lang/String;I)V

    move-object/from16 v21, v17

    goto/16 :goto_0

    :catchall_1
    move-exception v21

    if-eqz v9, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession;->mDictionaryPool:Lcom/android/inputmethod/latin/spellcheck/DictionaryPool;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v9}, Lcom/android/inputmethod/latin/spellcheck/DictionaryPool;->offer(Lcom/android/inputmethod/latin/spellcheck/DictAndProximity;)Z

    move-result v22

    if-nez v22, :cond_b

    # getter for: Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->access$900()Ljava/lang/String;

    move-result-object v22

    const-string v23, "Can\'t re-insert a dictionary into its pool"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_b
    throw v21
    :try_end_a
    .catch Ljava/lang/RuntimeException; {:try_start_a .. :try_end_a} :catch_0

    :cond_c
    const/16 v21, 0x2

    move/from16 v22, v21

    goto :goto_4

    :cond_d
    const/16 v21, 0x0

    goto :goto_5
.end method

.method private static shouldFilterOut(Ljava/lang/String;I)Z
    .locals 9
    .param p0    # Ljava/lang/String;
    .param p1    # I

    const/4 v6, 0x0

    const/4 v5, 0x1

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v7

    if-gt v7, v5, :cond_1

    :cond_0
    :goto_0
    return v5

    :cond_1
    invoke-virtual {p0, v6}, Ljava/lang/String;->codePointAt(I)I

    move-result v1

    invoke-static {v1, p1}, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession;->isLetterCheckableByLanguage(II)Z

    move-result v7

    if-nez v7, :cond_2

    const/16 v7, 0x27

    if-ne v7, v1, :cond_0

    :cond_2
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_4

    invoke-virtual {p0, v2}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    const/16 v7, 0x40

    if-eq v7, v0, :cond_0

    const/16 v7, 0x2f

    if-eq v7, v0, :cond_0

    invoke-static {v0, p1}, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession;->isLetterCheckableByLanguage(II)Z

    move-result v7

    if-eqz v7, :cond_3

    add-int/lit8 v4, v4, 0x1

    :cond_3
    invoke-virtual {p0, v2, v5}, Ljava/lang/String;->offsetByCodePoints(II)I

    move-result v2

    goto :goto_1

    :cond_4
    mul-int/lit8 v7, v4, 0x4

    mul-int/lit8 v8, v3, 0x3

    if-lt v7, v8, :cond_0

    move v5, v6

    goto :goto_0
.end method


# virtual methods
.method public onClose()V
    .locals 2

    iget-object v1, p0, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession;->mService:Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession;->mObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    return-void
.end method

.method public onCreate()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession;->getLocale()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession;->mService:Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;

    # invokes: Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->getDictionaryPool(Ljava/lang/String;)Lcom/android/inputmethod/latin/spellcheck/DictionaryPool;
    invoke-static {v1, v0}, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->access$500(Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;Ljava/lang/String;)Lcom/android/inputmethod/latin/spellcheck/DictionaryPool;

    move-result-object v1

    iput-object v1, p0, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession;->mDictionaryPool:Lcom/android/inputmethod/latin/spellcheck/DictionaryPool;

    invoke-static {v0}, Lcom/android/inputmethod/latin/LocaleUtils;->constructLocaleFromString(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v1

    iput-object v1, p0, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession;->mLocale:Ljava/util/Locale;

    iget-object v1, p0, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession;->mLocale:Ljava/util/Locale;

    # invokes: Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->getScriptFromLocale(Ljava/util/Locale;)I
    invoke-static {v1}, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->access$600(Ljava/util/Locale;)I

    move-result v1

    iput v1, p0, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession;->mScript:I

    return-void
.end method

.method public onGetSentenceSuggestionsMultiple([Landroid/view/textservice/TextInfo;I)[Landroid/view/textservice/SentenceSuggestionsInfo;
    .locals 5
    .param p1    # [Landroid/view/textservice/TextInfo;
    .param p2    # I

    invoke-super {p0, p1, p2}, Landroid/service/textservice/SpellCheckerService$Session;->onGetSentenceSuggestionsMultiple([Landroid/view/textservice/TextInfo;I)[Landroid/view/textservice/SentenceSuggestionsInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    array-length v3, v1

    array-length v4, p1

    if-eq v3, v4, :cond_1

    :cond_0
    return-object v1

    :cond_1
    const/4 v0, 0x0

    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_0

    aget-object v3, p1, v0

    aget-object v4, v1, v0

    invoke-direct {p0, v3, v4}, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession;->fixWronglyInvalidatedWordWithSingleQuote(Landroid/view/textservice/TextInfo;Landroid/view/textservice/SentenceSuggestionsInfo;)Landroid/view/textservice/SentenceSuggestionsInfo;

    move-result-object v2

    if-eqz v2, :cond_2

    aput-object v2, v1, v0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public onGetSuggestions(Landroid/view/textservice/TextInfo;I)Landroid/view/textservice/SuggestionsInfo;
    .locals 1
    .param p1    # Landroid/view/textservice/TextInfo;
    .param p2    # I

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession;->onGetSuggestions(Landroid/view/textservice/TextInfo;Ljava/lang/String;I)Landroid/view/textservice/SuggestionsInfo;

    move-result-object v0

    return-object v0
.end method

.method public onGetSuggestionsMultiple([Landroid/view/textservice/TextInfo;IZ)[Landroid/view/textservice/SuggestionsInfo;
    .locals 8
    .param p1    # [Landroid/view/textservice/TextInfo;
    .param p2    # I
    .param p3    # Z

    array-length v1, p1

    new-array v4, v1, [Landroid/view/textservice/SuggestionsInfo;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_2

    if-eqz p3, :cond_1

    if-lez v0, :cond_1

    add-int/lit8 v5, v0, -0x1

    aget-object v5, p1, v5

    invoke-virtual {v5}, Landroid/view/textservice/TextInfo;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v2, 0x0

    :goto_1
    aget-object v5, p1, v0

    invoke-direct {p0, v5, v2, p2}, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService$AndroidSpellCheckerSession;->onGetSuggestions(Landroid/view/textservice/TextInfo;Ljava/lang/String;I)Landroid/view/textservice/SuggestionsInfo;

    move-result-object v5

    aput-object v5, v4, v0

    aget-object v5, v4, v0

    aget-object v6, p1, v0

    invoke-virtual {v6}, Landroid/view/textservice/TextInfo;->getCookie()I

    move-result v6

    aget-object v7, p1, v0

    invoke-virtual {v7}, Landroid/view/textservice/TextInfo;->getSequence()I

    move-result v7

    invoke-virtual {v5, v6, v7}, Landroid/view/textservice/SuggestionsInfo;->setCookieAndSequence(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move-object v2, v3

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    :cond_2
    return-object v4
.end method
