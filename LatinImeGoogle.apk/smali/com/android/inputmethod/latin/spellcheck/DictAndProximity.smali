.class public Lcom/android/inputmethod/latin/spellcheck/DictAndProximity;
.super Ljava/lang/Object;
.source "DictAndProximity.java"


# instance fields
.field public final mDictionary:Lcom/android/inputmethod/latin/Dictionary;

.field public final mProximityInfo:Lcom/android/inputmethod/keyboard/ProximityInfo;


# direct methods
.method public constructor <init>(Lcom/android/inputmethod/latin/Dictionary;Lcom/android/inputmethod/keyboard/ProximityInfo;)V
    .locals 0
    .param p1    # Lcom/android/inputmethod/latin/Dictionary;
    .param p2    # Lcom/android/inputmethod/keyboard/ProximityInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/inputmethod/latin/spellcheck/DictAndProximity;->mDictionary:Lcom/android/inputmethod/latin/Dictionary;

    iput-object p2, p0, Lcom/android/inputmethod/latin/spellcheck/DictAndProximity;->mProximityInfo:Lcom/android/inputmethod/keyboard/ProximityInfo;

    return-void
.end method
