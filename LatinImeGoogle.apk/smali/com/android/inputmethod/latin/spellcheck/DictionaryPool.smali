.class public Lcom/android/inputmethod/latin/spellcheck/DictionaryPool;
.super Ljava/util/concurrent/LinkedBlockingQueue;
.source "DictionaryPool.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/concurrent/LinkedBlockingQueue",
        "<",
        "Lcom/android/inputmethod/latin/spellcheck/DictAndProximity;",
        ">;"
    }
.end annotation


# instance fields
.field private volatile mClosed:Z

.field private final mLocale:Ljava/util/Locale;

.field private final mMaxSize:I

.field private final mService:Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;

.field private mSize:I


# direct methods
.method public constructor <init>(ILcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;Ljava/util/Locale;)V
    .locals 1
    .param p1    # I
    .param p2    # Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;
    .param p3    # Ljava/util/Locale;

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput p1, p0, Lcom/android/inputmethod/latin/spellcheck/DictionaryPool;->mMaxSize:I

    iput-object p2, p0, Lcom/android/inputmethod/latin/spellcheck/DictionaryPool;->mService:Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;

    iput-object p3, p0, Lcom/android/inputmethod/latin/spellcheck/DictionaryPool;->mLocale:Ljava/util/Locale;

    iput v0, p0, Lcom/android/inputmethod/latin/spellcheck/DictionaryPool;->mSize:I

    iput-boolean v0, p0, Lcom/android/inputmethod/latin/spellcheck/DictionaryPool;->mClosed:Z

    return-void
.end method


# virtual methods
.method public close()V
    .locals 3

    monitor-enter p0

    const/4 v2, 0x1

    :try_start_0
    iput-boolean v2, p0, Lcom/android/inputmethod/latin/spellcheck/DictionaryPool;->mClosed:Z

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/spellcheck/DictionaryPool;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/inputmethod/latin/spellcheck/DictAndProximity;

    iget-object v2, v0, Lcom/android/inputmethod/latin/spellcheck/DictAndProximity;->mDictionary:Lcom/android/inputmethod/latin/Dictionary;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/Dictionary;->close()V

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/android/inputmethod/latin/spellcheck/DictionaryPool;->clear()V

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public offer(Lcom/android/inputmethod/latin/spellcheck/DictAndProximity;)Z
    .locals 1
    .param p1    # Lcom/android/inputmethod/latin/spellcheck/DictAndProximity;

    iget-boolean v0, p0, Lcom/android/inputmethod/latin/spellcheck/DictionaryPool;->mClosed:Z

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/android/inputmethod/latin/spellcheck/DictAndProximity;->mDictionary:Lcom/android/inputmethod/latin/Dictionary;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/Dictionary;->close()V

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Ljava/util/concurrent/LinkedBlockingQueue;->offer(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public bridge synthetic offer(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/android/inputmethod/latin/spellcheck/DictAndProximity;

    invoke-virtual {p0, p1}, Lcom/android/inputmethod/latin/spellcheck/DictionaryPool;->offer(Lcom/android/inputmethod/latin/spellcheck/DictAndProximity;)Z

    move-result v0

    return v0
.end method

.method public take()Lcom/android/inputmethod/latin/spellcheck/DictAndProximity;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/spellcheck/DictionaryPool;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/inputmethod/latin/spellcheck/DictAndProximity;

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    monitor-enter p0

    :try_start_0
    iget v1, p0, Lcom/android/inputmethod/latin/spellcheck/DictionaryPool;->mSize:I

    iget v2, p0, Lcom/android/inputmethod/latin/spellcheck/DictionaryPool;->mMaxSize:I

    if-lt v1, v2, :cond_1

    invoke-super {p0}, Ljava/util/concurrent/LinkedBlockingQueue;->take()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/inputmethod/latin/spellcheck/DictAndProximity;

    monitor-exit p0

    move-object v0, v1

    goto :goto_0

    :cond_1
    iget v1, p0, Lcom/android/inputmethod/latin/spellcheck/DictionaryPool;->mSize:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/inputmethod/latin/spellcheck/DictionaryPool;->mSize:I

    iget-object v1, p0, Lcom/android/inputmethod/latin/spellcheck/DictionaryPool;->mService:Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;

    iget-object v2, p0, Lcom/android/inputmethod/latin/spellcheck/DictionaryPool;->mLocale:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Lcom/android/inputmethod/latin/spellcheck/AndroidSpellCheckerService;->createDictAndProximity(Ljava/util/Locale;)Lcom/android/inputmethod/latin/spellcheck/DictAndProximity;

    move-result-object v0

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public bridge synthetic take()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/spellcheck/DictionaryPool;->take()Lcom/android/inputmethod/latin/spellcheck/DictAndProximity;

    move-result-object v0

    return-object v0
.end method

.method public takeOrGetNull()Lcom/android/inputmethod/latin/spellcheck/DictAndProximity;
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcom/android/inputmethod/latin/spellcheck/DictionaryPool;->take()Lcom/android/inputmethod/latin/spellcheck/DictAndProximity;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    goto :goto_0
.end method
