.class public Lcom/android/inputmethod/latin/spellcheck/SpellCheckerSettingsFragment;
.super Landroid/preference/PreferenceFragment;
.source "SpellCheckerSettingsFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onActivityCreated(Landroid/os/Bundle;)V

    const v0, 0x7f0500b7

    invoke-virtual {p0, v0}, Lcom/android/inputmethod/latin/spellcheck/SpellCheckerSettingsFragment;->addPreferencesFromResource(I)V

    return-void
.end method
