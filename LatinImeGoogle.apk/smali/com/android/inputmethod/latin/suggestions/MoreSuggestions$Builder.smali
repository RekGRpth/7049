.class public Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder;
.super Lcom/android/inputmethod/keyboard/Keyboard$Builder;
.source "MoreSuggestions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/latin/suggestions/MoreSuggestions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder$Divider;,
        Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder$MoreSuggestionsParam;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/inputmethod/keyboard/Keyboard$Builder",
        "<",
        "Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder$MoreSuggestionsParam;",
        ">;"
    }
.end annotation


# instance fields
.field private mFromPos:I

.field private final mPaneView:Lcom/android/inputmethod/latin/suggestions/MoreSuggestionsView;

.field private mSuggestions:Lcom/android/inputmethod/latin/SuggestedWords;

.field private mToPos:I


# direct methods
.method public constructor <init>(Lcom/android/inputmethod/latin/suggestions/MoreSuggestionsView;)V
    .locals 2
    .param p1    # Lcom/android/inputmethod/latin/suggestions/MoreSuggestionsView;

    invoke-virtual {p1}, Lcom/android/inputmethod/latin/suggestions/MoreSuggestionsView;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder$MoreSuggestionsParam;

    invoke-direct {v1}, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder$MoreSuggestionsParam;-><init>()V

    invoke-direct {p0, v0, v1}, Lcom/android/inputmethod/keyboard/Keyboard$Builder;-><init>(Landroid/content/Context;Lcom/android/inputmethod/keyboard/Keyboard$Params;)V

    iput-object p1, p0, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder;->mPaneView:Lcom/android/inputmethod/latin/suggestions/MoreSuggestionsView;

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/android/inputmethod/keyboard/Keyboard;
    .locals 1

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder;->build()Lcom/android/inputmethod/latin/suggestions/MoreSuggestions;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/android/inputmethod/latin/suggestions/MoreSuggestions;
    .locals 21

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder;->mParams:Lcom/android/inputmethod/keyboard/Keyboard$Params;

    check-cast v2, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder$MoreSuggestionsParam;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder;->mFromPos:I

    move/from16 v20, v0

    :goto_0
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder;->mToPos:I

    move/from16 v0, v20

    if-ge v0, v5, :cond_1

    move/from16 v0, v20

    invoke-virtual {v2, v0}, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder$MoreSuggestionsParam;->getX(I)I

    move-result v8

    move/from16 v0, v20

    invoke-virtual {v2, v0}, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder$MoreSuggestionsParam;->getY(I)I

    move-result v9

    move/from16 v0, v20

    invoke-virtual {v2, v0}, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder$MoreSuggestionsParam;->getWidth(I)I

    move-result v10

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder;->mSuggestions:Lcom/android/inputmethod/latin/SuggestedWords;

    move/from16 v0, v20

    invoke-virtual {v5, v0}, Lcom/android/inputmethod/latin/SuggestedWords;->getWord(I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder;->mSuggestions:Lcom/android/inputmethod/latin/SuggestedWords;

    move/from16 v0, v20

    invoke-static {v5, v0}, Lcom/android/inputmethod/latin/Utils;->getDebugInfo(Lcom/android/inputmethod/latin/SuggestedWords;I)Ljava/lang/String;

    move-result-object v4

    move/from16 v0, v20

    add-int/lit16 v6, v0, 0x400

    new-instance v1, Lcom/android/inputmethod/keyboard/Key;

    const/4 v5, 0x0

    const/4 v7, 0x0

    iget v11, v2, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder$MoreSuggestionsParam;->mDefaultRowHeight:I

    const/4 v12, 0x0

    invoke-direct/range {v1 .. v12}, Lcom/android/inputmethod/keyboard/Key;-><init>(Lcom/android/inputmethod/keyboard/Keyboard$Params;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;IIIII)V

    move/from16 v0, v20

    invoke-virtual {v2, v1, v0}, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder$MoreSuggestionsParam;->markAsEdgeKey(Lcom/android/inputmethod/keyboard/Key;I)V

    invoke-virtual {v2, v1}, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder$MoreSuggestionsParam;->onAddKey(Lcom/android/inputmethod/keyboard/Key;)V

    move/from16 v0, v20

    invoke-virtual {v2, v0}, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder$MoreSuggestionsParam;->getColumnNumber(I)I

    move-result v18

    move/from16 v0, v20

    invoke-virtual {v2, v0}, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder$MoreSuggestionsParam;->getNumColumnInRow(I)I

    move-result v19

    add-int/lit8 v5, v19, -0x1

    move/from16 v0, v18

    if-ge v0, v5, :cond_0

    new-instance v11, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder$Divider;

    iget-object v13, v2, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder$MoreSuggestionsParam;->mDivider:Landroid/graphics/drawable/Drawable;

    add-int v14, v8, v10

    iget v0, v2, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder$MoreSuggestionsParam;->mDividerWidth:I

    move/from16 v16, v0

    iget v0, v2, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder$MoreSuggestionsParam;->mDefaultRowHeight:I

    move/from16 v17, v0

    move-object v12, v2

    move v15, v9

    invoke-direct/range {v11 .. v17}, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder$Divider;-><init>(Lcom/android/inputmethod/keyboard/Keyboard$Params;Landroid/graphics/drawable/Drawable;IIII)V

    invoke-virtual {v2, v11}, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder$MoreSuggestionsParam;->onAddKey(Lcom/android/inputmethod/keyboard/Key;)V

    :cond_0
    add-int/lit8 v20, v20, 0x1

    goto :goto_0

    :cond_1
    new-instance v5, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions;

    invoke-direct {v5, v2}, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions;-><init>(Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder$MoreSuggestionsParam;)V

    return-object v5
.end method

.method public layout(Lcom/android/inputmethod/latin/SuggestedWords;IIII)Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder;
    .locals 10
    .param p1    # Lcom/android/inputmethod/latin/SuggestedWords;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-static {}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getInstance()Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getKeyboard()Lcom/android/inputmethod/keyboard/Keyboard;

    move-result-object v8

    const v9, 0x7f05001a

    const v0, 0x7f05001a

    iget-object v1, v8, Lcom/android/inputmethod/keyboard/Keyboard;->mId:Lcom/android/inputmethod/keyboard/KeyboardId;

    invoke-virtual {p0, v0, v1}, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder;->load(ILcom/android/inputmethod/keyboard/KeyboardId;)Lcom/android/inputmethod/keyboard/Keyboard$Builder;

    iget-object v0, p0, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder;->mParams:Lcom/android/inputmethod/keyboard/Keyboard$Params;

    check-cast v0, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder$MoreSuggestionsParam;

    iget-object v1, p0, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder;->mParams:Lcom/android/inputmethod/keyboard/Keyboard$Params;

    check-cast v1, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder$MoreSuggestionsParam;

    iget v2, v8, Lcom/android/inputmethod/keyboard/Keyboard;->mVerticalGap:I

    div-int/lit8 v2, v2, 0x2

    iput v2, v1, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder$MoreSuggestionsParam;->mTopPadding:I

    iput v2, v0, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder$MoreSuggestionsParam;->mVerticalGap:I

    iget-object v0, p0, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder;->mParams:Lcom/android/inputmethod/keyboard/Keyboard$Params;

    check-cast v0, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder$MoreSuggestionsParam;

    iget-object v6, p0, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder;->mPaneView:Lcom/android/inputmethod/latin/suggestions/MoreSuggestionsView;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder$MoreSuggestionsParam;->layout(Lcom/android/inputmethod/latin/SuggestedWords;IIIILcom/android/inputmethod/latin/suggestions/MoreSuggestionsView;)I

    move-result v7

    iput p2, p0, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder;->mFromPos:I

    add-int v0, p2, v7

    iput v0, p0, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder;->mToPos:I

    iput-object p1, p0, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder;->mSuggestions:Lcom/android/inputmethod/latin/SuggestedWords;

    return-object p0
.end method
