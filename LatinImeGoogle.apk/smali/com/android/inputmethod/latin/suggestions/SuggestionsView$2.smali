.class Lcom/android/inputmethod/latin/suggestions/SuggestionsView$2;
.super Lcom/android/inputmethod/keyboard/KeyboardActionListener$Adapter;
.source "SuggestionsView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/latin/suggestions/SuggestionsView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/inputmethod/latin/suggestions/SuggestionsView;


# direct methods
.method constructor <init>(Lcom/android/inputmethod/latin/suggestions/SuggestionsView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$2;->this$0:Lcom/android/inputmethod/latin/suggestions/SuggestionsView;

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/KeyboardActionListener$Adapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancelInput()V
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$2;->this$0:Lcom/android/inputmethod/latin/suggestions/SuggestionsView;

    # invokes: Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->dismissMoreSuggestions()Z
    invoke-static {v0}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->access$400(Lcom/android/inputmethod/latin/suggestions/SuggestionsView;)Z

    return-void
.end method

.method public onCustomRequest(I)Z
    .locals 4
    .param p1    # I

    const/4 v3, -0x1

    move v0, p1

    iget-object v2, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$2;->this$0:Lcom/android/inputmethod/latin/suggestions/SuggestionsView;

    # getter for: Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mSuggestedWords:Lcom/android/inputmethod/latin/SuggestedWords;
    invoke-static {v2}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->access$200(Lcom/android/inputmethod/latin/suggestions/SuggestionsView;)Lcom/android/inputmethod/latin/SuggestedWords;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/android/inputmethod/latin/SuggestedWords;->getWord(I)Ljava/lang/CharSequence;

    move-result-object v1

    iget-object v2, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$2;->this$0:Lcom/android/inputmethod/latin/suggestions/SuggestionsView;

    # getter for: Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mListener:Lcom/android/inputmethod/latin/suggestions/SuggestionsView$Listener;
    invoke-static {v2}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->access$300(Lcom/android/inputmethod/latin/suggestions/SuggestionsView;)Lcom/android/inputmethod/latin/suggestions/SuggestionsView$Listener;

    move-result-object v2

    invoke-interface {v2, v0, v1, v3, v3}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$Listener;->pickSuggestionManually(ILjava/lang/CharSequence;II)V

    iget-object v2, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$2;->this$0:Lcom/android/inputmethod/latin/suggestions/SuggestionsView;

    # invokes: Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->dismissMoreSuggestions()Z
    invoke-static {v2}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->access$400(Lcom/android/inputmethod/latin/suggestions/SuggestionsView;)Z

    const/4 v2, 0x1

    return v2
.end method
