.class Lcom/android/inputmethod/latin/suggestions/SuggestionsView$4;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "SuggestionsView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/latin/suggestions/SuggestionsView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/inputmethod/latin/suggestions/SuggestionsView;


# direct methods
.method constructor <init>(Lcom/android/inputmethod/latin/suggestions/SuggestionsView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$4;->this$0:Lcom/android/inputmethod/latin/suggestions/SuggestionsView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 4
    .param p1    # Landroid/view/MotionEvent;
    .param p2    # Landroid/view/MotionEvent;
    .param p3    # F
    .param p4    # F

    const/4 v3, 0x0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    sub-float v0, v1, v2

    cmpl-float v1, p4, v3

    if-lez v1, :cond_0

    cmpg-float v1, v0, v3

    if-gez v1, :cond_0

    iget-object v1, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$4;->this$0:Lcom/android/inputmethod/latin/suggestions/SuggestionsView;

    # invokes: Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->showMoreSuggestions()Z
    invoke-static {v1}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->access$500(Lcom/android/inputmethod/latin/suggestions/SuggestionsView;)Z

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
