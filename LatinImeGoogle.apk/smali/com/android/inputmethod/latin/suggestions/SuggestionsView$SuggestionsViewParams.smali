.class Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;
.super Ljava/lang/Object;
.source "SuggestionsView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/latin/suggestions/SuggestionsView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SuggestionsViewParams"
.end annotation


# static fields
.field private static final BOLD_SPAN:Landroid/text/style/CharacterStyle;

.field private static final UNDERLINE_SPAN:Landroid/text/style/CharacterStyle;


# instance fields
.field private final mAlphaObsoleted:F

.field private final mCenterSuggestionIndex:I

.field private final mCenterSuggestionWeight:F

.field private final mColorAutoCorrect:I

.field private final mColorSuggested:I

.field private final mColorTypedWord:I

.field private final mColorValidTypedWord:I

.field public final mDividerWidth:I

.field private final mDividers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final mHintToSaveView:Landroid/widget/TextView;

.field private final mInfos:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field private final mLeftwardsArrowView:Landroid/widget/TextView;

.field private mMaxMoreSuggestionsRow:I

.field public final mMinMoreSuggestionsWidth:F

.field public mMoreSuggestionsAvailable:Z

.field public final mMoreSuggestionsBottomGap:I

.field private final mMoreSuggestionsHint:Landroid/graphics/drawable/Drawable;

.field public final mMoreSuggestionsRowHeight:I

.field public final mPadding:I

.field private final mSuggestionStripOption:I

.field public final mSuggestionsCountInStrip:I

.field public final mSuggestionsStripHeight:I

.field private final mTexts:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private final mWordToSaveView:Landroid/widget/TextView;

.field private final mWords:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/text/style/StyleSpan;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/style/StyleSpan;-><init>(I)V

    sput-object v0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->BOLD_SPAN:Landroid/text/style/CharacterStyle;

    new-instance v0, Landroid/text/style/UnderlineSpan;

    invoke-direct {v0}, Landroid/text/style/UnderlineSpan;-><init>()V

    sput-object v0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->UNDERLINE_SPAN:Landroid/text/style/CharacterStyle;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILjava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 12
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/util/AttributeSet;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/TextView;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/TextView;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    iput-object v10, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mTexts:Ljava/util/ArrayList;

    move-object/from16 v0, p4

    iput-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mWords:Ljava/util/ArrayList;

    move-object/from16 v0, p5

    iput-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mDividers:Ljava/util/ArrayList;

    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mInfos:Ljava/util/ArrayList;

    const/4 v10, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    const/4 v10, 0x0

    move-object/from16 v0, p5

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/View;

    invoke-virtual {v9}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    move-result v10

    invoke-virtual {v9}, Landroid/widget/TextView;->getCompoundPaddingRight()I

    move-result v11

    add-int/2addr v10, v11

    iput v10, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mPadding:I

    const/4 v10, -0x1

    const/4 v11, -0x1

    invoke-virtual {v6, v10, v11}, Landroid/view/View;->measure(II)V

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v10

    iput v10, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mDividerWidth:I

    invoke-virtual {v9}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v10, 0x7f0b0011

    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    iput v10, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mSuggestionsStripHeight:I

    sget-object v10, Lcom/android/inputmethod/latin/R$styleable;->SuggestionsView:[I

    const v11, 0x7f0e000a

    invoke-virtual {p1, p2, v10, p3, v11}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v1, v10, v11}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v10

    iput v10, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mSuggestionStripOption:I

    const/4 v10, 0x5

    const/16 v11, 0x64

    invoke-static {v1, v10, v11}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->getPercent(Landroid/content/res/TypedArray;II)F

    move-result v5

    const/4 v10, 0x6

    const/16 v11, 0x64

    invoke-static {v1, v10, v11}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->getPercent(Landroid/content/res/TypedArray;II)F

    move-result v4

    const/4 v10, 0x7

    const/16 v11, 0x64

    invoke-static {v1, v10, v11}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->getPercent(Landroid/content/res/TypedArray;II)F

    move-result v2

    const/16 v10, 0x8

    const/16 v11, 0x64

    invoke-static {v1, v10, v11}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->getPercent(Landroid/content/res/TypedArray;II)F

    move-result v3

    const/16 v10, 0x8

    const/16 v11, 0x64

    invoke-static {v1, v10, v11}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->getPercent(Landroid/content/res/TypedArray;II)F

    move-result v10

    iput v10, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mAlphaObsoleted:F

    const/4 v10, 0x1

    const/4 v11, 0x0

    invoke-virtual {v1, v10, v11}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v10

    invoke-static {v10, v5}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->applyAlpha(IF)I

    move-result v10

    iput v10, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mColorValidTypedWord:I

    const/4 v10, 0x2

    const/4 v11, 0x0

    invoke-virtual {v1, v10, v11}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v10

    invoke-static {v10, v4}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->applyAlpha(IF)I

    move-result v10

    iput v10, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mColorTypedWord:I

    const/4 v10, 0x3

    const/4 v11, 0x0

    invoke-virtual {v1, v10, v11}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v10

    invoke-static {v10, v2}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->applyAlpha(IF)I

    move-result v10

    iput v10, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mColorAutoCorrect:I

    const/4 v10, 0x4

    const/4 v11, 0x0

    invoke-virtual {v1, v10, v11}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v10

    invoke-static {v10, v3}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->applyAlpha(IF)I

    move-result v10

    iput v10, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mColorSuggested:I

    const/16 v10, 0xa

    const/4 v11, 0x3

    invoke-virtual {v1, v10, v11}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v10

    iput v10, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mSuggestionsCountInStrip:I

    const/16 v10, 0xb

    const/16 v11, 0x28

    invoke-static {v1, v10, v11}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->getPercent(Landroid/content/res/TypedArray;II)F

    move-result v10

    iput v10, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mCenterSuggestionWeight:F

    const/16 v10, 0xc

    const/4 v11, 0x2

    invoke-virtual {v1, v10, v11}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v10

    iput v10, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mMaxMoreSuggestionsRow:I

    const/16 v10, 0xd

    invoke-static {v1, v10}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->getRatio(Landroid/content/res/TypedArray;I)F

    move-result v10

    iput v10, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mMinMoreSuggestionsWidth:F

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    const v10, 0x7f0b001b

    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v10

    iget v11, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mColorAutoCorrect:I

    invoke-static {v8, v10, v11}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->getMoreSuggestionsHint(Landroid/content/res/Resources;FI)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    iput-object v10, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mMoreSuggestionsHint:Landroid/graphics/drawable/Drawable;

    iget v10, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mSuggestionsCountInStrip:I

    div-int/lit8 v10, v10, 0x2

    iput v10, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mCenterSuggestionIndex:I

    const v10, 0x7f0b0014

    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v10

    iput v10, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mMoreSuggestionsBottomGap:I

    const v10, 0x7f0b0013

    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    iput v10, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mMoreSuggestionsRowHeight:I

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v7

    const v10, 0x7f03000a

    const/4 v11, 0x0

    invoke-virtual {v7, v10, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    iput-object v10, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mWordToSaveView:Landroid/widget/TextView;

    const v10, 0x7f030001

    const/4 v11, 0x0

    invoke-virtual {v7, v10, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    iput-object v10, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mLeftwardsArrowView:Landroid/widget/TextView;

    const v10, 0x7f030001

    const/4 v11, 0x0

    invoke-virtual {v7, v10, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    iput-object v10, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mHintToSaveView:Landroid/widget/TextView;

    return-void
.end method

.method private static addDivider(Landroid/view/ViewGroup;Landroid/view/View;)V
    .locals 2
    .param p0    # Landroid/view/ViewGroup;
    .param p1    # Landroid/view/View;

    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v1, 0x11

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    return-void
.end method

.method private static applyAlpha(IF)I
    .locals 4
    .param p0    # I
    .param p1    # F

    invoke-static {p0}, Landroid/graphics/Color;->alpha(I)I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v0, v1

    invoke-static {p0}, Landroid/graphics/Color;->red(I)I

    move-result v1

    invoke-static {p0}, Landroid/graphics/Color;->green(I)I

    move-result v2

    invoke-static {p0}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    return v1
.end method

.method private static getEllipsizedText(Ljava/lang/CharSequence;ILandroid/text/TextPaint;)Ljava/lang/CharSequence;
    .locals 6
    .param p0    # Ljava/lang/CharSequence;
    .param p1    # I
    .param p2    # Landroid/text/TextPaint;

    const v5, 0x3f333333

    if-nez p0, :cond_1

    const/4 p0, 0x0

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    const/high16 v3, 0x3f800000

    invoke-virtual {p2, v3}, Landroid/text/TextPaint;->setTextScaleX(F)V

    invoke-static {p0, p2}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->getTextWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)I

    move-result v2

    if-le v2, p1, :cond_0

    int-to-float v3, p1

    int-to-float v4, v2

    div-float v1, v3, v4

    cmpl-float v3, v1, v5

    if-ltz v3, :cond_2

    invoke-virtual {p2, v1}, Landroid/text/TextPaint;->setTextScaleX(F)V

    goto :goto_0

    :cond_2
    int-to-float v3, p1

    div-float/2addr v3, v5

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->MIDDLE:Landroid/text/TextUtils$TruncateAt;

    invoke-static {p0, p2, v3, v4}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v5}, Landroid/text/TextPaint;->setTextScaleX(F)V

    move-object p0, v0

    goto :goto_0
.end method

.method private getMoreSuggestionsHeight()I
    .locals 2

    iget v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mMaxMoreSuggestionsRow:I

    iget v1, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mMoreSuggestionsRowHeight:I

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mMoreSuggestionsBottomGap:I

    add-int/2addr v0, v1

    return v0
.end method

.method private static getMoreSuggestionsHint(Landroid/content/res/Resources;FI)Landroid/graphics/drawable/Drawable;
    .locals 10
    .param p0    # Landroid/content/res/Resources;
    .param p1    # F
    .param p2    # I

    const/high16 v9, 0x3f000000

    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    sget-object v6, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    invoke-virtual {v4, p1}, Landroid/graphics/Paint;->setTextSize(F)V

    invoke-virtual {v4, p2}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    const-string v6, "\u2026"

    const/4 v7, 0x0

    const-string v8, "\u2026"

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v4, v6, v7, v8, v0}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v6, v9

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v5

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v6, v9

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v3

    mul-int/lit8 v6, v3, 0x3

    div-int/lit8 v6, v6, 0x2

    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v6, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    const-string v6, "\u2026"

    div-int/lit8 v7, v5, 0x2

    int-to-float v7, v7

    int-to-float v8, v3

    invoke-virtual {v2, v6, v7, v8, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    new-instance v6, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v6, p0, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    return-object v6
.end method

.method private static getPercent(Landroid/content/res/TypedArray;II)F
    .locals 2
    .param p0    # Landroid/content/res/TypedArray;
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0, p1, p2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x42c80000

    div-float/2addr v0, v1

    return v0
.end method

.method private static getRatio(Landroid/content/res/TypedArray;I)F
    .locals 2
    .param p0    # Landroid/content/res/TypedArray;
    .param p1    # I

    const/16 v1, 0x3e8

    const/high16 v0, 0x3f800000

    invoke-virtual {p0, p1, v1, v1, v0}, Landroid/content/res/TypedArray;->getFraction(IIIF)F

    move-result v0

    const/high16 v1, 0x447a0000

    div-float/2addr v0, v1

    return v0
.end method

.method private getStyledSuggestionWord(Lcom/android/inputmethod/latin/SuggestedWords;I)Ljava/lang/CharSequence;
    .locals 9
    .param p1    # Lcom/android/inputmethod/latin/SuggestedWords;
    .param p2    # I

    const/16 v8, 0x11

    const/4 v1, 0x1

    const/4 v6, 0x0

    invoke-virtual {p1, p2}, Lcom/android/inputmethod/latin/SuggestedWords;->getWord(I)Ljava/lang/CharSequence;

    move-result-object v5

    if-ne p2, v1, :cond_0

    invoke-virtual {p1}, Lcom/android/inputmethod/latin/SuggestedWords;->willAutoCorrect()Z

    move-result v7

    if-eqz v7, :cond_0

    move v0, v1

    :goto_0
    if-nez p2, :cond_1

    iget-boolean v7, p1, Lcom/android/inputmethod/latin/SuggestedWords;->mTypedWordValid:Z

    if-eqz v7, :cond_1

    :goto_1
    if-nez v0, :cond_2

    if-nez v1, :cond_2

    :goto_2
    return-object v5

    :cond_0
    move v0, v6

    goto :goto_0

    :cond_1
    move v1, v6

    goto :goto_1

    :cond_2
    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    move-result v2

    new-instance v4, Landroid/text/SpannableString;

    invoke-direct {v4, v5}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    iget v3, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mSuggestionStripOption:I

    if-eqz v0, :cond_3

    and-int/lit8 v7, v3, 0x1

    if-nez v7, :cond_4

    :cond_3
    if-eqz v1, :cond_5

    and-int/lit8 v7, v3, 0x4

    if-eqz v7, :cond_5

    :cond_4
    sget-object v7, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->BOLD_SPAN:Landroid/text/style/CharacterStyle;

    invoke-interface {v4, v7, v6, v2, v8}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    :cond_5
    if-eqz v0, :cond_6

    and-int/lit8 v7, v3, 0x2

    if-eqz v7, :cond_6

    sget-object v7, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->UNDERLINE_SPAN:Landroid/text/style/CharacterStyle;

    invoke-interface {v4, v7, v6, v2, v8}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    :cond_6
    move-object v5, v4

    goto :goto_2
.end method

.method private getSuggestionTextColor(ILcom/android/inputmethod/latin/SuggestedWords;I)I
    .locals 5
    .param p1    # I
    .param p2    # Lcom/android/inputmethod/latin/SuggestedWords;
    .param p3    # I

    const/4 v3, 0x0

    const/4 v2, 0x1

    if-eqz p3, :cond_1

    move v1, v2

    :goto_0
    iget v4, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mCenterSuggestionIndex:I

    if-ne p1, v4, :cond_2

    invoke-virtual {p2}, Lcom/android/inputmethod/latin/SuggestedWords;->willAutoCorrect()Z

    move-result v4

    if-eqz v4, :cond_2

    iget v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mColorAutoCorrect:I

    :goto_1
    sget-boolean v4, Lcom/android/inputmethod/latin/LatinImeLogger;->sDBG:Z

    if-eqz v4, :cond_5

    invoke-virtual {p2}, Lcom/android/inputmethod/latin/SuggestedWords;->size()I

    move-result v4

    if-le v4, v2, :cond_5

    iget v4, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mCenterSuggestionIndex:I

    if-ne p1, v4, :cond_5

    iget-boolean v4, p2, Lcom/android/inputmethod/latin/SuggestedWords;->mHasAutoCorrectionCandidate:Z

    if-eqz v4, :cond_5

    invoke-virtual {p2, v2}, Lcom/android/inputmethod/latin/SuggestedWords;->getWord(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v3}, Lcom/android/inputmethod/latin/SuggestedWords;->getWord(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/inputmethod/latin/Suggest;->shouldBlockAutoCorrectionBySafetyNet(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    const/high16 v0, -0x10000

    :cond_0
    :goto_2
    return v0

    :cond_1
    move v1, v3

    goto :goto_0

    :cond_2
    iget v4, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mCenterSuggestionIndex:I

    if-ne p1, v4, :cond_3

    iget-boolean v4, p2, Lcom/android/inputmethod/latin/SuggestedWords;->mTypedWordValid:Z

    if-eqz v4, :cond_3

    iget v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mColorValidTypedWord:I

    goto :goto_1

    :cond_3
    if-eqz v1, :cond_4

    iget v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mColorSuggested:I

    goto :goto_1

    :cond_4
    iget v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mColorTypedWord:I

    goto :goto_1

    :cond_5
    iget-boolean v2, p2, Lcom/android/inputmethod/latin/SuggestedWords;->mIsObsoleteSuggestions:Z

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    iget v2, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mAlphaObsoleted:F

    invoke-static {v0, v2}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->applyAlpha(IF)I

    move-result v0

    goto :goto_2
.end method

.method private getSuggestionWeight(I)F
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mCenterSuggestionIndex:I

    if-ne p1, v0, :cond_0

    iget v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mCenterSuggestionWeight:F

    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x3f800000

    iget v1, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mCenterSuggestionWeight:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mSuggestionsCountInStrip:I

    add-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    div-float/2addr v0, v1

    goto :goto_0
.end method

.method private getSuggestionWidth(II)I
    .locals 5
    .param p1    # I
    .param p2    # I

    iget v3, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mPadding:I

    iget v4, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mSuggestionsCountInStrip:I

    mul-int v2, v3, v4

    iget v3, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mDividerWidth:I

    iget v4, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mSuggestionsCountInStrip:I

    add-int/lit8 v4, v4, -0x1

    mul-int v1, v3, v4

    sub-int v3, p2, v2

    sub-int v0, v3, v1

    int-to-float v3, v0

    invoke-direct {p0, p1}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->getSuggestionWeight(I)F

    move-result v4

    mul-float/2addr v3, v4

    float-to-int v3, v3

    return v3
.end method

.method private static getTextScaleX(Ljava/lang/CharSequence;ILandroid/text/TextPaint;)F
    .locals 3
    .param p0    # Ljava/lang/CharSequence;
    .param p1    # I
    .param p2    # Landroid/text/TextPaint;

    const/high16 v1, 0x3f800000

    invoke-virtual {p2, v1}, Landroid/text/TextPaint;->setTextScaleX(F)V

    invoke-static {p0, p2}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->getTextWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)I

    move-result v0

    if-gt v0, p1, :cond_0

    :goto_0
    return v1

    :cond_0
    int-to-float v1, p1

    int-to-float v2, v0

    div-float/2addr v1, v2

    goto :goto_0
.end method

.method private static getTextTypeface(Ljava/lang/CharSequence;)Landroid/graphics/Typeface;
    .locals 4

    const/4 v3, 0x0

    instance-of v0, p0, Landroid/text/SpannableString;

    if-nez v0, :cond_0

    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    :goto_0
    return-object v0

    :cond_0
    move-object v0, p0

    check-cast v0, Landroid/text/SpannableString;

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    const-class v2, Landroid/text/style/StyleSpan;

    invoke-virtual {v0, v3, v1, v2}, Landroid/text/SpannableString;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/StyleSpan;

    array-length v1, v0

    if-nez v1, :cond_1

    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    goto :goto_0

    :cond_1
    aget-object v0, v0, v3

    invoke-virtual {v0}, Landroid/text/style/StyleSpan;->getStyle()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    goto :goto_0

    :pswitch_0
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private static getTextWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)I
    .locals 8
    .param p0    # Ljava/lang/CharSequence;
    .param p1    # Landroid/text/TextPaint;

    const/4 v4, 0x0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    :goto_0
    return v4

    :cond_0
    invoke-virtual {p1}, Landroid/text/TextPaint;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v3

    invoke-static {p0}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->getTextTypeface(Ljava/lang/CharSequence;)Landroid/graphics/Typeface;

    move-result-object v6

    invoke-virtual {p1, v6}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    new-array v5, v2, [F

    invoke-virtual {p1, p0, v4, v2, v5}, Landroid/text/TextPaint;->getTextWidths(Ljava/lang/CharSequence;II[F)I

    move-result v0

    const/4 v4, 0x0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v0, :cond_1

    aget v6, v5, v1

    const/high16 v7, 0x3f000000

    add-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    add-int/2addr v4, v6

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {p1, v3}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    goto :goto_0
.end method

.method private getWordPosition(ILcom/android/inputmethod/latin/SuggestedWords;)I
    .locals 2
    .param p1    # I
    .param p2    # Lcom/android/inputmethod/latin/SuggestedWords;

    invoke-virtual {p2}, Lcom/android/inputmethod/latin/SuggestedWords;->willAutoCorrect()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget v1, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mCenterSuggestionIndex:I

    if-ne p1, v1, :cond_1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    if-ne p1, v0, :cond_2

    iget v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mCenterSuggestionIndex:I

    goto :goto_1

    :cond_2
    move v0, p1

    goto :goto_1
.end method

.method private layoutPunctuationSuggestions(Lcom/android/inputmethod/latin/SuggestedWords;Landroid/view/ViewGroup;)V
    .locals 8
    .param p1    # Lcom/android/inputmethod/latin/SuggestedWords;
    .param p2    # Landroid/view/ViewGroup;

    const/high16 v7, 0x3f800000

    const/4 v6, 0x0

    invoke-virtual {p1}, Lcom/android/inputmethod/latin/SuggestedWords;->size()I

    move-result v4

    const/4 v5, 0x5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    if-eqz v1, :cond_0

    iget-object v4, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mDividers:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    invoke-static {p2, v4}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->addDivider(Landroid/view/ViewGroup;Landroid/view/View;)V

    :cond_0
    iget-object v4, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mWords:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    iget v4, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mColorAutoCorrect:I

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {p1, v1}, Lcom/android/inputmethod/latin/SuggestedWords;->getWord(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setTextScaleX(F)V

    invoke-virtual {v3, v6, v6, v6, v6}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget v4, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mSuggestionsStripHeight:I

    invoke-static {v3, v7, v4}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->setLayoutWeight(Landroid/view/View;FI)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mMoreSuggestionsAvailable:Z

    return-void
.end method

.method private static setLayoutWeight(Landroid/view/View;FI)V
    .locals 3
    .param p0    # Landroid/view/View;
    .param p1    # F
    .param p2    # I

    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    instance-of v2, v1, Landroid/widget/LinearLayout$LayoutParams;

    if-eqz v2, :cond_0

    move-object v0, v1

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iput p1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    const/4 v2, 0x0

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    iput p2, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    :cond_0
    return-void
.end method

.method private setupTexts(Lcom/android/inputmethod/latin/SuggestedWords;I)V
    .locals 5
    .param p1    # Lcom/android/inputmethod/latin/SuggestedWords;
    .param p2    # I

    iget-object v3, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mTexts:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    invoke-virtual {p1}, Lcom/android/inputmethod/latin/SuggestedWords;->size()I

    move-result v3

    invoke-static {v3, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    invoke-direct {p0, p1, v1}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->getStyledSuggestionWord(Lcom/android/inputmethod/latin/SuggestedWords;I)Ljava/lang/CharSequence;

    move-result-object v2

    iget-object v3, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mTexts:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    move v1, v0

    :goto_1
    if-ge v1, p2, :cond_1

    iget-object v3, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mTexts:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method


# virtual methods
.method public getAddToDictionaryWord()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mWordToSaveView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getMaxMoreSuggestionsRow()I
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mMaxMoreSuggestionsRow:I

    return v0
.end method

.method public isAddToDictionaryShowing(Landroid/view/View;)Z
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mWordToSaveView:Landroid/widget/TextView;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mHintToSaveView:Landroid/widget/TextView;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mLeftwardsArrowView:Landroid/widget/TextView;

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public layout(Lcom/android/inputmethod/latin/SuggestedWords;Landroid/view/ViewGroup;Landroid/view/ViewGroup;I)V
    .locals 22
    .param p1    # Lcom/android/inputmethod/latin/SuggestedWords;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # I

    move-object/from16 v0, p1

    iget-boolean v0, v0, Lcom/android/inputmethod/latin/SuggestedWords;->mIsPunctuationSuggestions:Z

    move/from16 v18, v0

    if-eqz v18, :cond_1

    invoke-direct/range {p0 .. p2}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->layoutPunctuationSuggestions(Lcom/android/inputmethod/latin/SuggestedWords;Landroid/view/ViewGroup;)V

    :cond_0
    return-void

    :cond_1
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mSuggestionsCountInStrip:I

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->setupTexts(Lcom/android/inputmethod/latin/SuggestedWords;I)V

    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/latin/SuggestedWords;->size()I

    move-result v18

    move/from16 v0, v18

    if-le v0, v4, :cond_4

    const/16 v18, 0x1

    :goto_0
    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mMoreSuggestionsAvailable:Z

    const/16 v16, 0x0

    const/4 v7, 0x0

    :goto_1
    if-ge v7, v4, :cond_0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v7, v1}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->getWordPosition(ILcom/android/inputmethod/latin/SuggestedWords;)I

    move-result v10

    if-eqz v7, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mDividers:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/View;

    move-object/from16 v0, p2

    invoke-static {v0, v6}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->addDivider(Landroid/view/ViewGroup;Landroid/view/View;)V

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v18

    add-int v16, v16, v18

    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mTexts:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/CharSequence;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mWords:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mCenterSuggestionIndex:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-ne v7, v0, :cond_5

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mMoreSuggestionsAvailable:Z

    move/from16 v18, v0

    if-eqz v18, :cond_5

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mMoreSuggestionsHint:Landroid/graphics/drawable/Drawable;

    move-object/from16 v21, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    move-object/from16 v3, v21

    invoke-virtual {v15, v0, v1, v2, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mMoreSuggestionsHint:Landroid/graphics/drawable/Drawable;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v18

    move/from16 v0, v18

    neg-int v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    :goto_2
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_6

    const/16 v18, 0x1

    :goto_3
    move/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v7, v1, v10}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->getSuggestionTextColor(ILcom/android/inputmethod/latin/SuggestedWords;I)I

    move-result v18

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/widget/TextView;->setTextColor(I)V

    move-object/from16 v0, p0

    move/from16 v1, p4

    invoke-direct {v0, v7, v1}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->getSuggestionWidth(II)I

    move-result v14

    invoke-virtual {v15}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v12, v14, v0}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->getEllipsizedText(Ljava/lang/CharSequence;ILandroid/text/TextPaint;)Ljava/lang/CharSequence;

    move-result-object v13

    invoke-virtual {v15}, Landroid/widget/TextView;->getTextScaleX()F

    move-result v11

    invoke-virtual {v15, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v15, v11}, Landroid/widget/TextView;->setTextScaleX(F)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->getSuggestionWeight(I)F

    move-result v18

    const/16 v19, -0x1

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-static {v15, v0, v1}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->setLayoutWeight(Landroid/view/View;FI)V

    invoke-virtual {v15}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v18

    add-int v16, v16, v18

    sget-boolean v18, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->DBG:Z

    if-eqz v18, :cond_3

    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/latin/SuggestedWords;->size()I

    move-result v18

    move/from16 v0, v18

    if-ge v10, v0, :cond_3

    move-object/from16 v0, p1

    invoke-static {v0, v10}, Lcom/android/inputmethod/latin/Utils;->getDebugInfo(Lcom/android/inputmethod/latin/SuggestedWords;I)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mInfos:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    invoke-virtual {v8, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p3

    invoke-virtual {v0, v8}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    const/16 v18, -0x2

    const/16 v19, -0x2

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v8, v0, v1}, Landroid/widget/TextView;->measure(II)V

    invoke-virtual {v8}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v9

    invoke-virtual {v8}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v17

    sub-int v18, v16, v9

    invoke-virtual {v8}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v17

    move/from16 v2, v19

    invoke-static {v8, v0, v1, v9, v2}, Lcom/android/inputmethod/keyboard/ViewLayoutUtils;->placeViewAt(Landroid/view/View;IIII)V

    :cond_3
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_1

    :cond_4
    const/16 v18, 0x0

    goto/16 :goto_0

    :cond_5
    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    move-object/from16 v3, v21

    invoke-virtual {v15, v0, v1, v2, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    :cond_6
    const/16 v18, 0x0

    goto/16 :goto_3
.end method

.method public layoutAddToDictionaryHint(Ljava/lang/CharSequence;Landroid/view/ViewGroup;ILjava/lang/CharSequence;Landroid/view/View$OnClickListener;)V
    .locals 12
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # I
    .param p4    # Ljava/lang/CharSequence;
    .param p5    # Landroid/view/View$OnClickListener;

    iget v10, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mDividerWidth:I

    sub-int v10, p3, v10

    iget v11, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mPadding:I

    mul-int/lit8 v11, v11, 0x2

    sub-int v6, v10, v11

    iget-object v8, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mWordToSaveView:Landroid/widget/TextView;

    iget v10, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mColorTypedWord:I

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setTextColor(I)V

    int-to-float v10, v6

    iget v11, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mCenterSuggestionWeight:F

    mul-float/2addr v10, v11

    float-to-int v9, v10

    invoke-virtual {v8}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v10

    invoke-static {p1, v9, v10}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->getEllipsizedText(Ljava/lang/CharSequence;ILandroid/text/TextPaint;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v8}, Landroid/widget/TextView;->getTextScaleX()F

    move-result v7

    invoke-virtual {v8, p1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v8, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setTextScaleX(F)V

    invoke-virtual {p2, v8}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget v10, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mCenterSuggestionWeight:F

    const/4 v11, -0x1

    invoke-static {v8, v10, v11}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->setLayoutWeight(Landroid/view/View;FI)V

    iget-object v10, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mDividers:Ljava/util/ArrayList;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/view/View;

    invoke-virtual {p2, v10}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v4, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mLeftwardsArrowView:Landroid/widget/TextView;

    iget v10, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mColorAutoCorrect:I

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setTextColor(I)V

    const-string v10, "\u2190"

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p2, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v2, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mHintToSaveView:Landroid/widget/TextView;

    const/16 v10, 0x13

    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setGravity(I)V

    iget v10, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mColorAutoCorrect:I

    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setTextColor(I)V

    sub-int v10, v6, v9

    invoke-virtual {v4}, Landroid/widget/TextView;->getWidth()I

    move-result v11

    sub-int v3, v10, v11

    invoke-virtual {v2}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v10

    move-object/from16 v0, p4

    invoke-static {v0, v3, v10}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->getTextScaleX(Ljava/lang/CharSequence;ILandroid/text/TextPaint;)F

    move-result v1

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setTextScaleX(F)V

    invoke-virtual {p2, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    const/high16 v10, 0x3f800000

    iget v11, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mCenterSuggestionWeight:F

    sub-float/2addr v10, v11

    const/4 v11, -0x1

    invoke-static {v2, v10, v11}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->setLayoutWeight(Landroid/view/View;FI)V

    move-object/from16 v0, p5

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p5

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p5

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setMoreSuggestionsHeight(I)I
    .locals 4
    .param p1    # I

    invoke-direct {p0}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->getMoreSuggestionsHeight()I

    move-result v0

    if-gt v0, p1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget v2, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mMoreSuggestionsBottomGap:I

    sub-int v2, p1, v2

    iget v3, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mMoreSuggestionsRowHeight:I

    div-int/2addr v2, v3

    iput v2, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mMaxMoreSuggestionsRow:I

    invoke-direct {p0}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->getMoreSuggestionsHeight()I

    move-result v1

    move v0, v1

    goto :goto_0
.end method
