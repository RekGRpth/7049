.class public Lcom/android/inputmethod/latin/suggestions/SuggestionsView;
.super Landroid/widget/RelativeLayout;
.source "SuggestionsView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;,
        Lcom/android/inputmethod/latin/suggestions/SuggestionsView$UiHandler;,
        Lcom/android/inputmethod/latin/suggestions/SuggestionsView$Listener;
    }
.end annotation


# static fields
.field static final DBG:Z


# instance fields
.field private final mDividers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final mHandler:Lcom/android/inputmethod/latin/suggestions/SuggestionsView$UiHandler;

.field private final mInfos:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field private mKeyboardView:Lcom/android/inputmethod/keyboard/KeyboardView;

.field private mLastX:I

.field private mLastY:I

.field private mListener:Lcom/android/inputmethod/latin/suggestions/SuggestionsView$Listener;

.field private final mMoreSuggestionsBuilder:Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder;

.field private final mMoreSuggestionsContainer:Landroid/view/View;

.field private final mMoreSuggestionsController:Lcom/android/inputmethod/keyboard/MoreKeysPanel$Controller;

.field private final mMoreSuggestionsListener:Lcom/android/inputmethod/keyboard/KeyboardActionListener;

.field private final mMoreSuggestionsModalTolerance:I

.field private mMoreSuggestionsMode:I

.field private final mMoreSuggestionsSlidingDetector:Landroid/view/GestureDetector;

.field private final mMoreSuggestionsSlidingListener:Landroid/view/GestureDetector$OnGestureListener;

.field private final mMoreSuggestionsView:Lcom/android/inputmethod/latin/suggestions/MoreSuggestionsView;

.field private final mMoreSuggestionsWindow:Landroid/widget/PopupWindow;

.field private mOriginX:I

.field private mOriginY:I

.field private final mParams:Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;

.field private final mPreviewPopup:Landroid/widget/PopupWindow;

.field private final mPreviewText:Landroid/widget/TextView;

.field private mSuggestedWords:Lcom/android/inputmethod/latin/SuggestedWords;

.field private final mSuggestionsStrip:Landroid/view/ViewGroup;

.field private final mWords:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sDBG:Z

    sput-boolean v0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->DBG:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const v0, 0x7f010008

    invoke-direct {p0, p1, p2, v0}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 13
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct/range {p0 .. p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mWords:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mInfos:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mDividers:Ljava/util/ArrayList;

    sget-object v0, Lcom/android/inputmethod/latin/SuggestedWords;->EMPTY:Lcom/android/inputmethod/latin/SuggestedWords;

    iput-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mSuggestedWords:Lcom/android/inputmethod/latin/SuggestedWords;

    new-instance v0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$UiHandler;

    invoke-direct {v0, p0}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$UiHandler;-><init>(Lcom/android/inputmethod/latin/suggestions/SuggestionsView;)V

    iput-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mHandler:Lcom/android/inputmethod/latin/suggestions/SuggestionsView$UiHandler;

    new-instance v0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$2;

    invoke-direct {v0, p0}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$2;-><init>(Lcom/android/inputmethod/latin/suggestions/SuggestionsView;)V

    iput-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mMoreSuggestionsListener:Lcom/android/inputmethod/keyboard/KeyboardActionListener;

    new-instance v0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$3;

    invoke-direct {v0, p0}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$3;-><init>(Lcom/android/inputmethod/latin/suggestions/SuggestionsView;)V

    iput-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mMoreSuggestionsController:Lcom/android/inputmethod/keyboard/MoreKeysPanel$Controller;

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mMoreSuggestionsMode:I

    new-instance v0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$4;

    invoke-direct {v0, p0}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$4;-><init>(Lcom/android/inputmethod/latin/suggestions/SuggestionsView;)V

    iput-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mMoreSuggestionsSlidingListener:Landroid/view/GestureDetector$OnGestureListener;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v8

    const v0, 0x7f03000b

    invoke-virtual {v8, v0, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    new-instance v0, Landroid/widget/PopupWindow;

    invoke-direct {v0, p1}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mPreviewPopup:Landroid/widget/PopupWindow;

    const v0, 0x7f030009

    const/4 v1, 0x0

    invoke-virtual {v8, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mPreviewText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mPreviewPopup:Landroid/widget/PopupWindow;

    const/4 v1, -0x2

    const/4 v2, -0x2

    invoke-virtual {v0, v1, v2}, Landroid/widget/PopupWindow;->setWindowLayoutMode(II)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mPreviewPopup:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mPreviewText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mPreviewPopup:Landroid/widget/PopupWindow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const v0, 0x7f070046

    invoke-virtual {p0, v0}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mSuggestionsStrip:Landroid/view/ViewGroup;

    const/4 v10, 0x0

    :goto_0
    const/16 v0, 0x12

    if-ge v10, v0, :cond_0

    const v0, 0x7f03000a

    const/4 v1, 0x0

    invoke-virtual {v8, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v12, v0}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v12, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v12, p0}, Landroid/widget/TextView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mWords:Ljava/util/ArrayList;

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const v0, 0x7f030007

    const/4 v1, 0x0

    invoke-virtual {v8, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v7, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mDividers:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mInfos:Ljava/util/ArrayList;

    const v0, 0x7f030008

    const/4 v2, 0x0

    invoke-virtual {v8, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;

    iget-object v4, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mWords:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mDividers:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mInfos:Ljava/util/ArrayList;

    move-object v1, p1

    move-object v2, p2

    move/from16 v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILjava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mParams:Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;

    const v0, 0x7f030005

    const/4 v1, 0x0

    invoke-virtual {v8, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mMoreSuggestionsContainer:Landroid/view/View;

    iget-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mMoreSuggestionsContainer:Landroid/view/View;

    const v1, 0x7f070043

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/inputmethod/latin/suggestions/MoreSuggestionsView;

    iput-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mMoreSuggestionsView:Lcom/android/inputmethod/latin/suggestions/MoreSuggestionsView;

    new-instance v0, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder;

    iget-object v1, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mMoreSuggestionsView:Lcom/android/inputmethod/latin/suggestions/MoreSuggestionsView;

    invoke-direct {v0, v1}, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder;-><init>(Lcom/android/inputmethod/latin/suggestions/MoreSuggestionsView;)V

    iput-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mMoreSuggestionsBuilder:Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder;

    new-instance v9, Landroid/widget/PopupWindow;

    invoke-direct {v9, p1}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    const/4 v0, -0x2

    const/4 v1, -0x2

    invoke-virtual {v9, v0, v1}, Landroid/widget/PopupWindow;->setWindowLayoutMode(II)V

    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const v1, 0x106000d

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v9, v0}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 v0, 0x2

    invoke-virtual {v9, v0}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    const/4 v0, 0x1

    invoke-virtual {v9, v0}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    const/4 v0, 0x1

    invoke-virtual {v9, v0}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    new-instance v0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$1;

    invoke-direct {v0, p0}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$1;-><init>(Lcom/android/inputmethod/latin/suggestions/SuggestionsView;)V

    invoke-virtual {v9, v0}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    iput-object v9, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mMoreSuggestionsWindow:Landroid/widget/PopupWindow;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v0, 0x7f0b0015

    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mMoreSuggestionsModalTolerance:I

    new-instance v0, Landroid/view/GestureDetector;

    iget-object v1, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mMoreSuggestionsSlidingListener:Landroid/view/GestureDetector$OnGestureListener;

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mMoreSuggestionsSlidingDetector:Landroid/view/GestureDetector;

    return-void
.end method

.method static synthetic access$000(Lcom/android/inputmethod/latin/suggestions/SuggestionsView;)V
    .locals 0
    .param p0    # Lcom/android/inputmethod/latin/suggestions/SuggestionsView;

    invoke-direct {p0}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->hidePreview()V

    return-void
.end method

.method static synthetic access$100(Lcom/android/inputmethod/latin/suggestions/SuggestionsView;)Lcom/android/inputmethod/keyboard/KeyboardView;
    .locals 1
    .param p0    # Lcom/android/inputmethod/latin/suggestions/SuggestionsView;

    iget-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mKeyboardView:Lcom/android/inputmethod/keyboard/KeyboardView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/inputmethod/latin/suggestions/SuggestionsView;)Lcom/android/inputmethod/latin/SuggestedWords;
    .locals 1
    .param p0    # Lcom/android/inputmethod/latin/suggestions/SuggestionsView;

    iget-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mSuggestedWords:Lcom/android/inputmethod/latin/SuggestedWords;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/inputmethod/latin/suggestions/SuggestionsView;)Lcom/android/inputmethod/latin/suggestions/SuggestionsView$Listener;
    .locals 1
    .param p0    # Lcom/android/inputmethod/latin/suggestions/SuggestionsView;

    iget-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mListener:Lcom/android/inputmethod/latin/suggestions/SuggestionsView$Listener;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/inputmethod/latin/suggestions/SuggestionsView;)Z
    .locals 1
    .param p0    # Lcom/android/inputmethod/latin/suggestions/SuggestionsView;

    invoke-direct {p0}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->dismissMoreSuggestions()Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/android/inputmethod/latin/suggestions/SuggestionsView;)Z
    .locals 1
    .param p0    # Lcom/android/inputmethod/latin/suggestions/SuggestionsView;

    invoke-direct {p0}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->showMoreSuggestions()Z

    move-result v0

    return v0
.end method

.method private addToDictionary(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1    # Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mListener:Lcom/android/inputmethod/latin/suggestions/SuggestionsView$Listener;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$Listener;->addWordToDictionary(Ljava/lang/String;)Z

    return-void
.end method

.method private dismissMoreSuggestions()Z
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mMoreSuggestionsWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mMoreSuggestionsWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private hidePreview()V
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mPreviewPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    return-void
.end method

.method private showMoreSuggestions()Z
    .locals 15

    iget-object v13, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mParams:Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;

    iget-boolean v1, v13, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mMoreSuggestionsAvailable:Z

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->getWidth()I

    move-result v14

    iget-object v11, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mMoreSuggestionsContainer:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    sub-int v1, v14, v1

    invoke-virtual {v11}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    sub-int v3, v1, v2

    iget-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mMoreSuggestionsBuilder:Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder;

    iget-object v1, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mSuggestedWords:Lcom/android/inputmethod/latin/SuggestedWords;

    iget v2, v13, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mSuggestionsCountInStrip:I

    int-to-float v5, v3

    iget v6, v13, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mMinMoreSuggestionsWidth:F

    mul-float/2addr v5, v6

    float-to-int v4, v5

    invoke-virtual {v13}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->getMaxMoreSuggestionsRow()I

    move-result v5

    invoke-virtual/range {v0 .. v5}, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder;->layout(Lcom/android/inputmethod/latin/SuggestedWords;IIII)Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder;

    iget-object v1, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mMoreSuggestionsView:Lcom/android/inputmethod/latin/suggestions/MoreSuggestionsView;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/suggestions/MoreSuggestions$Builder;->build()Lcom/android/inputmethod/latin/suggestions/MoreSuggestions;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/inputmethod/latin/suggestions/MoreSuggestionsView;->setKeyboard(Lcom/android/inputmethod/keyboard/Keyboard;)V

    const/4 v1, -0x2

    const/4 v2, -0x2

    invoke-virtual {v11, v1, v2}, Landroid/view/View;->measure(II)V

    iget-object v4, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mMoreSuggestionsView:Lcom/android/inputmethod/latin/suggestions/MoreSuggestionsView;

    div-int/lit8 v7, v14, 0x2

    iget v1, v13, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mMoreSuggestionsBottomGap:I

    neg-int v8, v1

    iget-object v6, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mMoreSuggestionsController:Lcom/android/inputmethod/keyboard/MoreKeysPanel$Controller;

    iget-object v9, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mMoreSuggestionsWindow:Landroid/widget/PopupWindow;

    iget-object v10, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mMoreSuggestionsListener:Lcom/android/inputmethod/keyboard/KeyboardActionListener;

    move-object v5, p0

    invoke-interface/range {v4 .. v10}, Lcom/android/inputmethod/keyboard/MoreKeysPanel;->showMoreKeysPanel(Landroid/view/View;Lcom/android/inputmethod/keyboard/MoreKeysPanel$Controller;IILandroid/widget/PopupWindow;Lcom/android/inputmethod/keyboard/KeyboardActionListener;)V

    const/4 v1, 0x1

    iput v1, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mMoreSuggestionsMode:I

    iget v1, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mLastX:I

    iput v1, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mOriginX:I

    iget v1, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mLastY:I

    iput v1, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mOriginY:I

    iget-object v1, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mKeyboardView:Lcom/android/inputmethod/keyboard/KeyboardView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/inputmethod/keyboard/KeyboardView;->dimEntireKeyboard(Z)V

    const/4 v12, 0x0

    :goto_0
    iget v1, v13, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->mSuggestionsCountInStrip:I

    if-ge v12, v1, :cond_0

    iget-object v1, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mWords:Ljava/util/ArrayList;

    invoke-virtual {v1, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setPressed(Z)V

    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    :goto_1
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method


# virtual methods
.method public clear()V
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mSuggestionsStrip:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->removeAllViews()V

    iget-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mSuggestionsStrip:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->addView(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->dismissMoreSuggestions()Z

    return-void
.end method

.method public dismissAddToDictionaryHint()Z
    .locals 1

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->isShowingAddToDictionaryHint()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->clear()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 14
    .param p1    # Landroid/view/MotionEvent;

    const/4 v11, 0x1

    iget-object v12, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mMoreSuggestionsWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v12}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v12

    if-eqz v12, :cond_0

    iget v12, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mMoreSuggestionsMode:I

    if-nez v12, :cond_3

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v12

    float-to-int v12, v12

    iput v12, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mLastX:I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v12

    float-to-int v12, v12

    iput v12, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mLastY:I

    iget-object v12, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mMoreSuggestionsSlidingDetector:Landroid/view/GestureDetector;

    invoke-virtual {v12, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v12

    if-eqz v12, :cond_2

    :cond_1
    :goto_0
    return v11

    :cond_2
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v11

    goto :goto_0

    :cond_3
    iget-object v6, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mMoreSuggestionsView:Lcom/android/inputmethod/latin/suggestions/MoreSuggestionsView;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v8

    invoke-virtual {p1, v8}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v7

    invoke-static {v7, v6}, Lcom/android/inputmethod/keyboard/PointerTracker;->getPointerTracker(ILcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;)Lcom/android/inputmethod/keyboard/PointerTracker;

    move-result-object v0

    invoke-virtual {p1, v8}, Landroid/view/MotionEvent;->getX(I)F

    move-result v12

    float-to-int v9, v12

    invoke-virtual {p1, v8}, Landroid/view/MotionEvent;->getY(I)F

    move-result v12

    float-to-int v10, v12

    invoke-interface {v6, v9}, Lcom/android/inputmethod/keyboard/MoreKeysPanel;->translateX(I)I

    move-result v2

    invoke-interface {v6, v10}, Lcom/android/inputmethod/keyboard/MoreKeysPanel;->translateY(I)I

    move-result v3

    iget v12, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mMoreSuggestionsMode:I

    if-ne v12, v11, :cond_7

    iget v12, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mOriginX:I

    sub-int v12, v9, v12

    invoke-static {v12}, Ljava/lang/Math;->abs(I)I

    move-result v12

    iget v13, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mMoreSuggestionsModalTolerance:I

    if-ge v12, v13, :cond_4

    iget v12, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mOriginY:I

    sub-int/2addr v12, v10

    iget v13, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mMoreSuggestionsModalTolerance:I

    if-lt v12, v13, :cond_5

    :cond_4
    const/4 v12, 0x2

    iput v12, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mMoreSuggestionsMode:I

    invoke-virtual {v0, v2, v3, v6}, Lcom/android/inputmethod/keyboard/PointerTracker;->onShowMoreKeysPanel(IILcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;)V

    goto :goto_0

    :cond_5
    if-eq v1, v11, :cond_6

    const/4 v12, 0x6

    if-ne v1, v12, :cond_1

    :cond_6
    const/4 v12, 0x0

    iput v12, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mMoreSuggestionsMode:I

    goto :goto_0

    :cond_7
    invoke-virtual/range {v0 .. v6}, Lcom/android/inputmethod/keyboard/PointerTracker;->processMotionEvent(IIIJLcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;)V

    goto :goto_0
.end method

.method public getSuggestions()Lcom/android/inputmethod/latin/SuggestedWords;
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mSuggestedWords:Lcom/android/inputmethod/latin/SuggestedWords;

    return-object v0
.end method

.method public isShowingAddToDictionaryHint()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mSuggestionsStrip:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mParams:Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;

    iget-object v2, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mSuggestionsStrip:Landroid/view/ViewGroup;

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->isAddToDictionaryShowing(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    iget-object v3, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mParams:Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;

    invoke-virtual {v3, p1}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->isAddToDictionaryShowing(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mParams:Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;

    invoke-virtual {v3}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->getAddToDictionaryWord()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->addToDictionary(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->clear()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    instance-of v3, v1, Ljava/lang/Integer;

    if-eqz v3, :cond_0

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v3, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mSuggestedWords:Lcom/android/inputmethod/latin/SuggestedWords;

    invoke-virtual {v3}, Lcom/android/inputmethod/latin/SuggestedWords;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    iget-object v3, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mSuggestedWords:Lcom/android/inputmethod/latin/SuggestedWords;

    invoke-virtual {v3, v0}, Lcom/android/inputmethod/latin/SuggestedWords;->getWord(I)Ljava/lang/CharSequence;

    move-result-object v2

    iget-object v3, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mListener:Lcom/android/inputmethod/latin/suggestions/SuggestionsView$Listener;

    iget v4, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mLastX:I

    iget v5, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mLastY:I

    invoke-interface {v3, v0, v2, v4, v5}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$Listener;->pickSuggestionManually(ILjava/lang/CharSequence;II)V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    iget-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mHandler:Lcom/android/inputmethod/latin/suggestions/SuggestionsView$UiHandler;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$UiHandler;->cancelAllMessages()V

    invoke-direct {p0}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->hidePreview()V

    invoke-direct {p0}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->dismissMoreSuggestions()Z

    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-direct {p0}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->showMoreSuggestions()Z

    move-result v0

    return v0
.end method

.method public setListener(Lcom/android/inputmethod/latin/suggestions/SuggestionsView$Listener;Landroid/view/View;)V
    .locals 1
    .param p1    # Lcom/android/inputmethod/latin/suggestions/SuggestionsView$Listener;
    .param p2    # Landroid/view/View;

    iput-object p1, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mListener:Lcom/android/inputmethod/latin/suggestions/SuggestionsView$Listener;

    const v0, 0x7f070041

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/inputmethod/keyboard/KeyboardView;

    iput-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mKeyboardView:Lcom/android/inputmethod/keyboard/KeyboardView;

    return-void
.end method

.method public setMoreSuggestionsHeight(I)I
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mParams:Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;

    invoke-virtual {v0, p1}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->setMoreSuggestionsHeight(I)I

    move-result v0

    return v0
.end method

.method public setSuggestions(Lcom/android/inputmethod/latin/SuggestedWords;)V
    .locals 4
    .param p1    # Lcom/android/inputmethod/latin/SuggestedWords;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->clear()V

    iput-object p1, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mSuggestedWords:Lcom/android/inputmethod/latin/SuggestedWords;

    iget-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mParams:Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;

    iget-object v1, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mSuggestedWords:Lcom/android/inputmethod/latin/SuggestedWords;

    iget-object v2, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mSuggestionsStrip:Landroid/view/ViewGroup;

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->getWidth()I

    move-result v3

    invoke-virtual {v0, v1, v2, p0, v3}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->layout(Lcom/android/inputmethod/latin/SuggestedWords;Landroid/view/ViewGroup;Landroid/view/ViewGroup;I)V

    goto :goto_0
.end method

.method public showAddToDictionaryHint(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 6
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Ljava/lang/CharSequence;

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->clear()V

    iget-object v0, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mParams:Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;

    iget-object v2, p0, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->mSuggestionsStrip:Landroid/view/ViewGroup;

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView;->getWidth()I

    move-result v3

    move-object v1, p1

    move-object v4, p2

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Lcom/android/inputmethod/latin/suggestions/SuggestionsView$SuggestionsViewParams;->layoutAddToDictionaryHint(Ljava/lang/CharSequence;Landroid/view/ViewGroup;ILjava/lang/CharSequence;Landroid/view/View$OnClickListener;)V

    return-void
.end method
