.class public final Lcom/android/launcher/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/launcher/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final all_apps_button_vertical_padding:I = 0x7f0a0074

.field public static final app_icon_drawable_padding:I = 0x7f0a0019

.field public static final app_icon_drawable_padding_land:I = 0x7f0a001a

.field public static final app_icon_padding_top:I = 0x7f0a001b

.field public static final app_icon_size:I = 0x7f0a0022

.field public static final app_widget_preview_label_margin_left:I = 0x7f0a0038

.field public static final app_widget_preview_label_margin_right:I = 0x7f0a0039

.field public static final app_widget_preview_label_margin_top:I = 0x7f0a0037

.field public static final app_widget_preview_padding_left:I = 0x7f0a0034

.field public static final app_widget_preview_padding_right:I = 0x7f0a0035

.field public static final app_widget_preview_padding_top:I = 0x7f0a0036

.field public static final apps_customize_cell_height:I = 0x7f0a0024

.field public static final apps_customize_cell_width:I = 0x7f0a0023

.field public static final apps_customize_max_gap:I = 0x7f0a0025

.field public static final apps_customize_pageLayoutHeightGap:I = 0x7f0a006b

.field public static final apps_customize_pageLayoutPaddingBottom:I = 0x7f0a006d

.field public static final apps_customize_pageLayoutPaddingLeft:I = 0x7f0a006e

.field public static final apps_customize_pageLayoutPaddingRight:I = 0x7f0a006f

.field public static final apps_customize_pageLayoutPaddingTop:I = 0x7f0a006c

.field public static final apps_customize_pageLayoutWidthGap:I = 0x7f0a006a

.field public static final apps_customize_tab_bar_height:I = 0x7f0a0020

.field public static final apps_customize_tab_bar_margin_top:I = 0x7f0a0021

.field public static final apps_customize_widget_cell_height_gap:I = 0x7f0a0027

.field public static final apps_customize_widget_cell_width_gap:I = 0x7f0a0026

.field public static final button_bar_height:I = 0x7f0a0028

.field public static final button_bar_height_bottom_padding:I = 0x7f0a002a

.field public static final button_bar_height_plus_padding:I = 0x7f0a002d

.field public static final button_bar_height_top_padding:I = 0x7f0a0029

.field public static final button_bar_width_left_padding:I = 0x7f0a002b

.field public static final button_bar_width_right_padding:I = 0x7f0a002c

.field public static final cell_layout_bottom_padding:I = 0x7f0a0065

.field public static final cell_layout_bottom_padding_land:I = 0x7f0a0049

.field public static final cell_layout_bottom_padding_port:I = 0x7f0a0048

.field public static final cell_layout_left_padding:I = 0x7f0a0062

.field public static final cell_layout_left_padding_land:I = 0x7f0a0043

.field public static final cell_layout_left_padding_port:I = 0x7f0a0042

.field public static final cell_layout_right_padding:I = 0x7f0a0063

.field public static final cell_layout_right_padding_land:I = 0x7f0a0045

.field public static final cell_layout_right_padding_port:I = 0x7f0a0044

.field public static final cell_layout_top_padding:I = 0x7f0a0064

.field public static final cell_layout_top_padding_land:I = 0x7f0a0047

.field public static final cell_layout_top_padding_port:I = 0x7f0a0046

.field public static final clingPunchThroughGraphicCenterRadius:I = 0x7f0a0000

.field public static final cling_text_block_offset_x:I = 0x7f0a0002

.field public static final cling_text_block_offset_y:I = 0x7f0a0003

.field public static final dragViewOffsetX:I = 0x7f0a0031

.field public static final dragViewOffsetY:I = 0x7f0a0032

.field public static final dragViewScale:I = 0x7f0a0033

.field public static final drop_target_drag_padding:I = 0x7f0a002e

.field public static final folderClingMarginTop:I = 0x7f0a0001

.field public static final folder_cell_height:I = 0x7f0a000c

.field public static final folder_cell_width:I = 0x7f0a000b

.field public static final folder_height_gap:I = 0x7f0a0041

.field public static final folder_icon_padding_top:I = 0x7f0a000d

.field public static final folder_name_padding:I = 0x7f0a003f

.field public static final folder_preview_padding:I = 0x7f0a003e

.field public static final folder_preview_size:I = 0x7f0a003d

.field public static final folder_unread_margin_right:I = 0x7f0a005f

.field public static final folder_width_gap:I = 0x7f0a0040

.field public static final hotseat_cell_height:I = 0x7f0a0013

.field public static final hotseat_cell_width:I = 0x7f0a0012

.field public static final hotseat_height_gap:I = 0x7f0a0015

.field public static final hotseat_unread_margin_right:I = 0x7f0a005e

.field public static final hotseat_width_gap:I = 0x7f0a0014

.field public static final preview_padding_bottom:I = 0x7f0a005d

.field public static final preview_padding_left:I = 0x7f0a005a

.field public static final preview_padding_right:I = 0x7f0a005b

.field public static final preview_padding_top:I = 0x7f0a005c

.field public static final qsb_bar_height:I = 0x7f0a0006

.field public static final qsb_bar_height_inset:I = 0x7f0a0005

.field public static final qsb_padding_left:I = 0x7f0a0007

.field public static final qsb_padding_right:I = 0x7f0a0008

.field public static final reveal_radius:I = 0x7f0a0004

.field public static final scroll_zone:I = 0x7f0a0030

.field public static final search_bar_height:I = 0x7f0a0009

.field public static final shortcut_preview_padding_left:I = 0x7f0a003a

.field public static final shortcut_preview_padding_right:I = 0x7f0a003b

.field public static final shortcut_preview_padding_top:I = 0x7f0a003c

.field public static final status_bar_height:I = 0x7f0a002f

.field public static final toolbar_button_horizontal_padding:I = 0x7f0a001d

.field public static final toolbar_button_vertical_padding:I = 0x7f0a001c

.field public static final toolbar_external_icon_height:I = 0x7f0a001f

.field public static final toolbar_external_icon_width:I = 0x7f0a001e

.field public static final wallpaper_chooser_grid_height:I = 0x7f0a0076

.field public static final wallpaper_chooser_grid_width:I = 0x7f0a0075

.field public static final workspace_bottom_padding:I = 0x7f0a0069

.field public static final workspace_bottom_padding_land:I = 0x7f0a0051

.field public static final workspace_bottom_padding_port:I = 0x7f0a0050

.field public static final workspace_cell_height:I = 0x7f0a0060

.field public static final workspace_cell_height_land:I = 0x7f0a0054

.field public static final workspace_cell_height_port:I = 0x7f0a0055

.field public static final workspace_cell_width:I = 0x7f0a0070

.field public static final workspace_cell_width_land:I = 0x7f0a0052

.field public static final workspace_cell_width_port:I = 0x7f0a0053

.field public static final workspace_content_large_only_top_margin:I = 0x7f0a0073

.field public static final workspace_divider_padding_bottom:I = 0x7f0a0011

.field public static final workspace_divider_padding_left:I = 0x7f0a000e

.field public static final workspace_divider_padding_right:I = 0x7f0a000f

.field public static final workspace_divider_padding_top:I = 0x7f0a0010

.field public static final workspace_height_gap:I = 0x7f0a0072

.field public static final workspace_height_gap_land:I = 0x7f0a0058

.field public static final workspace_height_gap_port:I = 0x7f0a0059

.field public static final workspace_icon_text_size:I = 0x7f0a0017

.field public static final workspace_left_padding:I = 0x7f0a0066

.field public static final workspace_left_padding_land:I = 0x7f0a004b

.field public static final workspace_left_padding_port:I = 0x7f0a004a

.field public static final workspace_max_gap:I = 0x7f0a000a

.field public static final workspace_overscroll_drawable_padding:I = 0x7f0a0016

.field public static final workspace_page_spacing:I = 0x7f0a0061

.field public static final workspace_right_padding:I = 0x7f0a0067

.field public static final workspace_right_padding_land:I = 0x7f0a004d

.field public static final workspace_right_padding_port:I = 0x7f0a004c

.field public static final workspace_spring_loaded_page_spacing:I = 0x7f0a0018

.field public static final workspace_top_padding:I = 0x7f0a0068

.field public static final workspace_top_padding_land:I = 0x7f0a004f

.field public static final workspace_top_padding_port:I = 0x7f0a004e

.field public static final workspace_width_gap:I = 0x7f0a0071

.field public static final workspace_width_gap_land:I = 0x7f0a0056

.field public static final workspace_width_gap_port:I = 0x7f0a0057


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
