.class public final Lcom/android/launcher/R$integer;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/launcher/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "integer"
.end annotation


# static fields
.field public static final apps_customize_cling_focused_x:I = 0x7f090025

.field public static final apps_customize_cling_focused_y:I = 0x7f090026

.field public static final apps_customize_maxCellCountX:I = 0x7f090021

.field public static final apps_customize_maxCellCountY:I = 0x7f090022

.field public static final apps_customize_widget_cell_count_x:I = 0x7f090023

.field public static final apps_customize_widget_cell_count_y:I = 0x7f090024

.field public static final cell_count_x:I = 0x7f09001d

.field public static final cell_count_y:I = 0x7f09001e

.field public static final config_allAppsBatchLoadDelay:I = 0x7f09000e

.field public static final config_allAppsBatchSize:I = 0x7f09000f

.field public static final config_appsCustomizeDragSlopeThreshold:I = 0x7f09000d

.field public static final config_appsCustomizeFadeInTime:I = 0x7f090007

.field public static final config_appsCustomizeFadeOutTime:I = 0x7f090008

.field public static final config_appsCustomizeSpringLoadedBgAlpha:I = 0x7f090001

.field public static final config_appsCustomizeWorkspaceAnimationStagger:I = 0x7f09000a

.field public static final config_appsCustomizeWorkspaceShrinkTime:I = 0x7f090009

.field public static final config_appsCustomizeZoomInTime:I = 0x7f090004

.field public static final config_appsCustomizeZoomOutTime:I = 0x7f090005

.field public static final config_appsCustomizeZoomScaleFactor:I = 0x7f090006

.field public static final config_cameraDistance:I = 0x7f090019

.field public static final config_dragFadeOutAlpha:I = 0x7f090017

.field public static final config_dragFadeOutDuration:I = 0x7f090018

.field public static final config_dragOutlineFadeTime:I = 0x7f090011

.field public static final config_dragOutlineMaxAlpha:I = 0x7f090012

.field public static final config_dropAnimMaxDist:I = 0x7f090016

.field public static final config_dropAnimMaxDuration:I = 0x7f090014

.field public static final config_dropAnimMinDuration:I = 0x7f090013

.field public static final config_dropTargetBgTransitionDuration:I = 0x7f090010

.field public static final config_flingToDeleteMinVelocity:I = 0x7f090000

.field public static final config_folderAnimDuration:I = 0x7f090015

.field public static final config_tabTransitionDuration:I = 0x7f09000c

.field public static final config_workspaceAppsCustomizeAnimationStagger:I = 0x7f09000b

.field public static final config_workspaceSpringLoadShrinkPercentage:I = 0x7f090003

.field public static final config_workspaceUnshrinkTime:I = 0x7f090002

.field public static final folder_max_count_x:I = 0x7f09001a

.field public static final folder_max_count_y:I = 0x7f09001b

.field public static final folder_max_num_items:I = 0x7f09001c

.field public static final hotseat_all_apps_index:I = 0x7f090020

.field public static final hotseat_cell_count:I = 0x7f09001f


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
