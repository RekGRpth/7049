.class public final Lcom/android/launcher/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/launcher/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final accessibility_all_apps_button:I = 0x7f0c002d

.field public static final accessibility_delete_button:I = 0x7f0c002e

.field public static final accessibility_search_button:I = 0x7f0c002b

.field public static final accessibility_voice_search_button:I = 0x7f0c002c

.field public static final activity_not_found:I = 0x7f0c000a

.field public static final all_apps_button_label:I = 0x7f0c0024

.field public static final all_apps_cling_add_item:I = 0x7f0c0053

.field public static final all_apps_cling_title:I = 0x7f0c0052

.field public static final all_apps_home_button_label:I = 0x7f0c0025

.field public static final application_name:I = 0x7f0c0003

.field public static final apps_customize_apps_scroll_format:I = 0x7f0c004d

.field public static final apps_customize_widgets_scroll_format:I = 0x7f0c004e

.field public static final cab_app_selection_text:I = 0x7f0c003a

.field public static final cab_folder_selection_text:I = 0x7f0c003c

.field public static final cab_menu_app_info:I = 0x7f0c0039

.field public static final cab_menu_delete_app:I = 0x7f0c0038

.field public static final cab_shortcut_selection_text:I = 0x7f0c003d

.field public static final cab_widget_selection_text:I = 0x7f0c003b

.field public static final cancel_action:I = 0x7f0c0015

.field public static final chooser_wallpaper:I = 0x7f0c0007

.field public static final cling_dismiss:I = 0x7f0c0057

.field public static final completely_out_of_space:I = 0x7f0c001b

.field public static final custom_workspace_cling_description_1:I = 0x7f0c005f

.field public static final custom_workspace_cling_description_2:I = 0x7f0c0061

.field public static final custom_workspace_cling_title_1:I = 0x7f0c005e

.field public static final custom_workspace_cling_title_2:I = 0x7f0c0060

.field public static final default_scroll_format:I = 0x7f0c004b

.field public static final delete_target_label:I = 0x7f0c0028

.field public static final delete_target_uninstall_label:I = 0x7f0c0029

.field public static final delete_zone_label_all_apps:I = 0x7f0c0027

.field public static final delete_zone_label_all_apps_system_app:I = 0x7f0c002f

.field public static final delete_zone_label_workspace:I = 0x7f0c0026

.field public static final dream_name:I = 0x7f0c0048

.field public static final external_drop_widget_error:I = 0x7f0c0010

.field public static final external_drop_widget_pick_format:I = 0x7f0c000f

.field public static final external_drop_widget_pick_title:I = 0x7f0c0011

.field public static final folder_cling_create_folder:I = 0x7f0c0056

.field public static final folder_cling_move_item:I = 0x7f0c0055

.field public static final folder_cling_title:I = 0x7f0c0054

.field public static final folder_closed:I = 0x7f0c005b

.field public static final folder_hint_text:I = 0x7f0c0049

.field public static final folder_name:I = 0x7f0c0006

.field public static final folder_name_format:I = 0x7f0c005d

.field public static final folder_opened:I = 0x7f0c0058

.field public static final folder_renamed:I = 0x7f0c005c

.field public static final folder_tap_to_close:I = 0x7f0c0059

.field public static final folder_tap_to_rename:I = 0x7f0c005a

.field public static final gadget_error_text:I = 0x7f0c0046

.field public static final group_applications:I = 0x7f0c0017

.field public static final group_shortcuts:I = 0x7f0c0018

.field public static final group_wallpapers:I = 0x7f0c001a

.field public static final group_widgets:I = 0x7f0c0019

.field public static final help_url:I = 0x7f0c0037

.field public static final home:I = 0x7f0c0004

.field public static final hotseat_out_of_space:I = 0x7f0c001d

.field public static final info_target_label:I = 0x7f0c002a

.field public static final invalid_hotseat_item:I = 0x7f0c001e

.field public static final long_press_widget_to_add:I = 0x7f0c000c

.field public static final market:I = 0x7f0c000d

.field public static final menu_add:I = 0x7f0c0030

.field public static final menu_help:I = 0x7f0c0036

.field public static final menu_item_add_item:I = 0x7f0c0016

.field public static final menu_manage_apps:I = 0x7f0c0031

.field public static final menu_notifications:I = 0x7f0c0034

.field public static final menu_search:I = 0x7f0c0033

.field public static final menu_settings:I = 0x7f0c0035

.field public static final menu_wallpaper:I = 0x7f0c0032

.field public static final one_video_widget:I = 0x7f0c0000

.field public static final orientation:I = 0x7f0c0001

.field public static final out_of_space:I = 0x7f0c001c

.field public static final permdesc_install_shortcut:I = 0x7f0c003f

.field public static final permdesc_read_settings:I = 0x7f0c0043

.field public static final permdesc_uninstall_shortcut:I = 0x7f0c0041

.field public static final permdesc_write_settings:I = 0x7f0c0045

.field public static final permlab_install_shortcut:I = 0x7f0c003e

.field public static final permlab_read_settings:I = 0x7f0c0042

.field public static final permlab_uninstall_shortcut:I = 0x7f0c0040

.field public static final permlab_write_settings:I = 0x7f0c0044

.field public static final pick_wallpaper:I = 0x7f0c0009

.field public static final rename_action:I = 0x7f0c0014

.field public static final rename_folder_label:I = 0x7f0c0012

.field public static final rename_folder_title:I = 0x7f0c0013

.field public static final scene:I = 0x7f0c0002

.field public static final shortcut_duplicate:I = 0x7f0c0021

.field public static final shortcut_installed:I = 0x7f0c001f

.field public static final shortcut_uninstalled:I = 0x7f0c0020

.field public static final title_select_application:I = 0x7f0c0023

.field public static final title_select_shortcut:I = 0x7f0c0022

.field public static final uid_name:I = 0x7f0c0005

.field public static final uninstall_system_app_text:I = 0x7f0c0047

.field public static final wallpaper_instructions:I = 0x7f0c0008

.field public static final widget_dims_format:I = 0x7f0c000e

.field public static final widgets_tab_label:I = 0x7f0c000b

.field public static final workspace_cling_move_item:I = 0x7f0c0050

.field public static final workspace_cling_open_all_apps:I = 0x7f0c0051

.field public static final workspace_cling_title:I = 0x7f0c004f

.field public static final workspace_description_format:I = 0x7f0c004a

.field public static final workspace_scroll_format:I = 0x7f0c004c


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
