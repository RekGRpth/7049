.class public final Lcom/android/launcher/R$style;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/launcher/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "style"
.end annotation


# static fields
.field public static final ClingButton:I = 0x7f0d0002

.field public static final ClingText:I = 0x7f0d0004

.field public static final ClingTitleText:I = 0x7f0d0003

.field public static final CustomClingText:I = 0x7f0d0016

.field public static final CustomClingTitleText:I = 0x7f0d0015

.field public static final DropTargetButton:I = 0x7f0d0011

.field public static final DropTargetButtonContainer:I = 0x7f0d0010

.field public static final MarketButton:I = 0x7f0d0014

.field public static final QSBBar:I = 0x7f0d000c

.field public static final SearchButton:I = 0x7f0d000e

.field public static final SearchButtonDivider:I = 0x7f0d000f

.field public static final SearchButton_Voice:I = 0x7f0d0018

.field public static final SearchDropTargetBar:I = 0x7f0d000d

.field public static final TabIndicator:I = 0x7f0d0012

.field public static final TabIndicator_AppsCustomize:I = 0x7f0d0013

.field public static final Theme:I = 0x7f0d0001

.field public static final Theme_WallpaperPicker:I = 0x7f0d0000

.field public static final UnreadNumAppearance:I = 0x7f0d0017

.field public static final WorkspaceIcon:I = 0x7f0d0005

.field public static final WorkspaceIcon_Landscape:I = 0x7f0d0007

.field public static final WorkspaceIcon_Landscape_AppsCustomize:I = 0x7f0d000b

.field public static final WorkspaceIcon_Landscape_Folder:I = 0x7f0d0009

.field public static final WorkspaceIcon_Portrait:I = 0x7f0d0006

.field public static final WorkspaceIcon_Portrait_AppsCustomize:I = 0x7f0d000a

.field public static final WorkspaceIcon_Portrait_Folder:I = 0x7f0d0008


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
