.class public Lcom/android/launcher2/MTKAppIcon;
.super Landroid/widget/RelativeLayout;
.source "MTKAppIcon.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MTKAppIcon"


# instance fields
.field private mAlpha:I

.field mAppIcon:Lcom/android/launcher2/PagedViewIcon;

.field private mHolographicAlpha:I

.field private mInfo:Lcom/android/launcher2/ApplicationInfo;

.field mUnread:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    const/16 v0, 0xff

    iput v0, p0, Lcom/android/launcher2/MTKAppIcon;->mAlpha:I

    invoke-direct {p0, p1}, Lcom/android/launcher2/MTKAppIcon;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/16 v0, 0xff

    iput v0, p0, Lcom/android/launcher2/MTKAppIcon;->mAlpha:I

    invoke-direct {p0, p1}, Lcom/android/launcher2/MTKAppIcon;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/16 v0, 0xff

    iput v0, p0, Lcom/android/launcher2/MTKAppIcon;->mAlpha:I

    invoke-direct {p0, p1}, Lcom/android/launcher2/MTKAppIcon;->init(Landroid/content/Context;)V

    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public applyFromApplicationInfo(Lcom/android/launcher2/ApplicationInfo;ZLcom/android/launcher2/PagedViewIcon$PressedCallback;)V
    .locals 1
    .param p1    # Lcom/android/launcher2/ApplicationInfo;
    .param p2    # Z
    .param p3    # Lcom/android/launcher2/PagedViewIcon$PressedCallback;

    iget-object v0, p0, Lcom/android/launcher2/MTKAppIcon;->mAppIcon:Lcom/android/launcher2/PagedViewIcon;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/launcher2/PagedViewIcon;->applyFromApplicationInfo(Lcom/android/launcher2/ApplicationInfo;ZLcom/android/launcher2/PagedViewIcon$PressedCallback;)V

    invoke-virtual {p0, p1}, Lcom/android/launcher2/MTKAppIcon;->setTag(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/android/launcher2/MTKAppIcon;->updateUnreadNum()V

    return-void
.end method

.method public invalidateCheckedImage()V
    .locals 1

    iget-object v0, p0, Lcom/android/launcher2/MTKAppIcon;->mAppIcon:Lcom/android/launcher2/PagedViewIcon;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/view/View;->onFinishInflate()V

    const v0, 0x7f06002d

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/PagedViewIcon;

    iput-object v0, p0, Lcom/android/launcher2/MTKAppIcon;->mAppIcon:Lcom/android/launcher2/PagedViewIcon;

    const v0, 0x7f06002e

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/launcher2/MTKAppIcon;->mUnread:Landroid/widget/TextView;

    return-void
.end method

.method public setAlpha(F)V
    .locals 6
    .param p1    # F

    const/high16 v5, 0x437f0000

    invoke-static {p1}, Lcom/android/launcher2/HolographicOutlineHelper;->viewAlphaInterpolator(F)F

    move-result v3

    invoke-static {p1}, Lcom/android/launcher2/HolographicOutlineHelper;->highlightAlphaInterpolator(F)F

    move-result v0

    mul-float v4, v3, v5

    float-to-int v2, v4

    mul-float v4, v0, v5

    float-to-int v1, v4

    iget v4, p0, Lcom/android/launcher2/MTKAppIcon;->mAlpha:I

    if-ne v4, v2, :cond_0

    iget v4, p0, Lcom/android/launcher2/MTKAppIcon;->mHolographicAlpha:I

    if-eq v4, v1, :cond_1

    :cond_0
    iput v2, p0, Lcom/android/launcher2/MTKAppIcon;->mAlpha:I

    iput v1, p0, Lcom/android/launcher2/MTKAppIcon;->mHolographicAlpha:I

    invoke-super {p0, v3}, Landroid/view/View;->setAlpha(F)V

    :cond_1
    return-void
.end method

.method public setTag(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;

    invoke-super {p0, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/launcher2/MTKAppIcon;->mAppIcon:Lcom/android/launcher2/PagedViewIcon;

    invoke-virtual {v0, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/launcher2/MTKAppIcon;->mUnread:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    check-cast p1, Lcom/android/launcher2/ApplicationInfo;

    iput-object p1, p0, Lcom/android/launcher2/MTKAppIcon;->mInfo:Lcom/android/launcher2/ApplicationInfo;

    return-void
.end method

.method public updateUnreadNum()V
    .locals 2

    iget-object v0, p0, Lcom/android/launcher2/MTKAppIcon;->mInfo:Lcom/android/launcher2/ApplicationInfo;

    iget v0, v0, Lcom/android/launcher2/ItemInfo;->unreadNum:I

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/android/launcher2/MTKAppIcon;->mUnread:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/launcher2/MTKAppIcon;->mUnread:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/launcher2/MTKAppIcon;->mInfo:Lcom/android/launcher2/ApplicationInfo;

    iget v0, v0, Lcom/android/launcher2/ItemInfo;->unreadNum:I

    const/16 v1, 0x63

    if-le v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/launcher2/MTKAppIcon;->mUnread:Landroid/widget/TextView;

    invoke-static {}, Lcom/android/launcher2/MTKUnreadLoader;->getExceedText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/launcher2/MTKAppIcon;->mUnread:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/launcher2/MTKAppIcon;->mInfo:Lcom/android/launcher2/ApplicationInfo;

    iget v1, v1, Lcom/android/launcher2/ItemInfo;->unreadNum:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public updateUnreadNum(I)V
    .locals 2
    .param p1    # I

    const/4 v1, 0x0

    if-gtz p1, :cond_0

    iget-object v0, p0, Lcom/android/launcher2/MTKAppIcon;->mInfo:Lcom/android/launcher2/ApplicationInfo;

    iput v1, v0, Lcom/android/launcher2/ItemInfo;->unreadNum:I

    iget-object v0, p0, Lcom/android/launcher2/MTKAppIcon;->mUnread:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    iget-object v0, p0, Lcom/android/launcher2/MTKAppIcon;->mInfo:Lcom/android/launcher2/ApplicationInfo;

    invoke-virtual {p0, v0}, Lcom/android/launcher2/MTKAppIcon;->setTag(Ljava/lang/Object;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/launcher2/MTKAppIcon;->mInfo:Lcom/android/launcher2/ApplicationInfo;

    iput p1, v0, Lcom/android/launcher2/ItemInfo;->unreadNum:I

    iget-object v0, p0, Lcom/android/launcher2/MTKAppIcon;->mUnread:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const/16 v0, 0x63

    if-le p1, v0, :cond_1

    iget-object v0, p0, Lcom/android/launcher2/MTKAppIcon;->mUnread:Landroid/widget/TextView;

    invoke-static {}, Lcom/android/launcher2/MTKUnreadLoader;->getExceedText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/launcher2/MTKAppIcon;->mUnread:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
