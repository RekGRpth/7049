.class public Lcom/android/launcher2/MTKShortcut;
.super Landroid/widget/RelativeLayout;
.source "MTKShortcut.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MTKShortcut"


# instance fields
.field mFavorite:Lcom/android/launcher2/BubbleTextView;

.field mInfo:Lcom/android/launcher2/ShortcutInfo;

.field mUnread:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1}, Lcom/android/launcher2/MTKShortcut;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0, p1}, Lcom/android/launcher2/MTKShortcut;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0, p1}, Lcom/android/launcher2/MTKShortcut;->init(Landroid/content/Context;)V

    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public applyFromShortcutInfo(Lcom/android/launcher2/ShortcutInfo;Lcom/android/launcher2/IconCache;)V
    .locals 1
    .param p1    # Lcom/android/launcher2/ShortcutInfo;
    .param p2    # Lcom/android/launcher2/IconCache;

    iget-object v0, p0, Lcom/android/launcher2/MTKShortcut;->mFavorite:Lcom/android/launcher2/BubbleTextView;

    invoke-virtual {v0, p1, p2}, Lcom/android/launcher2/BubbleTextView;->applyFromShortcutInfo(Lcom/android/launcher2/ShortcutInfo;Lcom/android/launcher2/IconCache;)V

    invoke-virtual {p0, p1}, Lcom/android/launcher2/MTKShortcut;->setTag(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/android/launcher2/MTKShortcut;->updateShortcutUnreadNum()V

    return-void
.end method

.method public getFavoriteCompoundDrawable()Landroid/graphics/drawable/Drawable;
    .locals 2

    iget-object v0, p0, Lcom/android/launcher2/MTKShortcut;->mFavorite:Lcom/android/launcher2/BubbleTextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/4 v1, 0x1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/android/launcher2/MTKShortcut;->mFavorite:Lcom/android/launcher2/BubbleTextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getUnreadText()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/android/launcher2/MTKShortcut;->mUnread:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/launcher2/MTKShortcut;->mUnread:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "0"

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/android/launcher2/MTKShortcut;->mUnread:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method public getUnreadVisibility()I
    .locals 1

    iget-object v0, p0, Lcom/android/launcher2/MTKShortcut;->mUnread:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/launcher2/MTKShortcut;->mUnread:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/view/View;->onFinishInflate()V

    const v0, 0x7f06002b

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/BubbleTextView;

    iput-object v0, p0, Lcom/android/launcher2/MTKShortcut;->mFavorite:Lcom/android/launcher2/BubbleTextView;

    const v0, 0x7f06002c

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/launcher2/MTKShortcut;->mUnread:Landroid/widget/TextView;

    return-void
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 2
    .param p1    # Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/launcher2/MTKShortcut;->mFavorite:Lcom/android/launcher2/BubbleTextView;

    invoke-virtual {v0, v1, p1, v1, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method setShortcutUnreadMarginRight(I)V
    .locals 4
    .param p1    # I

    iget-object v1, p0, Lcom/android/launcher2/MTKShortcut;->mUnread:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    invoke-virtual {v0, v1, v2, p1, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    iget-object v1, p0, Lcom/android/launcher2/MTKShortcut;->mUnread:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/android/launcher2/MTKShortcut;->mUnread:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/view/View;->requestLayout()V

    return-void
.end method

.method public setTag(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;

    invoke-super {p0, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/launcher2/MTKShortcut;->mFavorite:Lcom/android/launcher2/BubbleTextView;

    invoke-virtual {v0, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/launcher2/MTKShortcut;->mUnread:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    check-cast p1, Lcom/android/launcher2/ShortcutInfo;

    iput-object p1, p0, Lcom/android/launcher2/MTKShortcut;->mInfo:Lcom/android/launcher2/ShortcutInfo;

    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/android/launcher2/MTKShortcut;->mFavorite:Lcom/android/launcher2/BubbleTextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public updateShortcutUnreadNum()V
    .locals 2

    iget-object v0, p0, Lcom/android/launcher2/MTKShortcut;->mInfo:Lcom/android/launcher2/ShortcutInfo;

    iget v0, v0, Lcom/android/launcher2/ItemInfo;->unreadNum:I

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/android/launcher2/MTKShortcut;->mUnread:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/launcher2/MTKShortcut;->mUnread:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/launcher2/MTKShortcut;->mInfo:Lcom/android/launcher2/ShortcutInfo;

    iget v0, v0, Lcom/android/launcher2/ItemInfo;->unreadNum:I

    const/16 v1, 0x63

    if-le v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/launcher2/MTKShortcut;->mUnread:Landroid/widget/TextView;

    invoke-static {}, Lcom/android/launcher2/MTKUnreadLoader;->getExceedText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/launcher2/MTKShortcut;->mUnread:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/launcher2/MTKShortcut;->mInfo:Lcom/android/launcher2/ShortcutInfo;

    iget v1, v1, Lcom/android/launcher2/ItemInfo;->unreadNum:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public updateShortcutUnreadNum(I)V
    .locals 2
    .param p1    # I

    const/4 v1, 0x0

    if-gtz p1, :cond_0

    iget-object v0, p0, Lcom/android/launcher2/MTKShortcut;->mInfo:Lcom/android/launcher2/ShortcutInfo;

    iput v1, v0, Lcom/android/launcher2/ItemInfo;->unreadNum:I

    iget-object v0, p0, Lcom/android/launcher2/MTKShortcut;->mUnread:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    iget-object v0, p0, Lcom/android/launcher2/MTKShortcut;->mInfo:Lcom/android/launcher2/ShortcutInfo;

    invoke-virtual {p0, v0}, Lcom/android/launcher2/MTKShortcut;->setTag(Ljava/lang/Object;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/launcher2/MTKShortcut;->mInfo:Lcom/android/launcher2/ShortcutInfo;

    iput p1, v0, Lcom/android/launcher2/ItemInfo;->unreadNum:I

    iget-object v0, p0, Lcom/android/launcher2/MTKShortcut;->mUnread:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const/16 v0, 0x63

    if-le p1, v0, :cond_1

    iget-object v0, p0, Lcom/android/launcher2/MTKShortcut;->mUnread:Landroid/widget/TextView;

    invoke-static {}, Lcom/android/launcher2/MTKUnreadLoader;->getExceedText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/launcher2/MTKShortcut;->mUnread:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
