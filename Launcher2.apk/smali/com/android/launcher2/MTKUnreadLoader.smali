.class public Lcom/android/launcher2/MTKUnreadLoader;
.super Landroid/content/BroadcastReceiver;
.source "MTKUnreadLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/launcher2/MTKUnreadLoader$UnreadCallbacks;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MTKUnreadLoader"

.field private static final TAG_UNREADSHORTCUTS:Ljava/lang/String; = "unreadshortcuts"

.field private static final sExceedString:Landroid/text/SpannableStringBuilder;

.field private static final sLogLock:Ljava/lang/Object;

.field private static final sUnreadSupportShortcuts:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/launcher2/UnreadSupportShortcut;",
            ">;"
        }
    .end annotation
.end field

.field private static sUnreadSupportShortcutsNum:I


# instance fields
.field private mCallbacks:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/android/launcher2/MTKUnreadLoader$UnreadCallbacks;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/16 v5, 0x21

    const/4 v4, 0x3

    const/4 v3, 0x2

    new-instance v0, Landroid/text/SpannableStringBuilder;

    const-string v1, "99+"

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lcom/android/launcher2/MTKUnreadLoader;->sExceedString:Landroid/text/SpannableStringBuilder;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/launcher2/MTKUnreadLoader;->sUnreadSupportShortcuts:Ljava/util/ArrayList;

    const/4 v0, 0x0

    sput v0, Lcom/android/launcher2/MTKUnreadLoader;->sUnreadSupportShortcutsNum:I

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/launcher2/MTKUnreadLoader;->sLogLock:Ljava/lang/Object;

    sget-object v0, Lcom/android/launcher2/MTKUnreadLoader;->sExceedString:Landroid/text/SpannableStringBuilder;

    new-instance v1, Landroid/text/style/SuperscriptSpan;

    invoke-direct {v1}, Landroid/text/style/SuperscriptSpan;-><init>()V

    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    sget-object v0, Lcom/android/launcher2/MTKUnreadLoader;->sExceedString:Landroid/text/SpannableStringBuilder;

    new-instance v1, Landroid/text/style/AbsoluteSizeSpan;

    const/16 v2, 0xa

    invoke-direct {v1, v2}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    iput-object p1, p0, Lcom/android/launcher2/MTKUnreadLoader;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$000(Lcom/android/launcher2/MTKUnreadLoader;)V
    .locals 0
    .param p0    # Lcom/android/launcher2/MTKUnreadLoader;

    invoke-direct {p0}, Lcom/android/launcher2/MTKUnreadLoader;->loadUnreadSupportShortcuts()V

    return-void
.end method

.method static synthetic access$100(Lcom/android/launcher2/MTKUnreadLoader;)V
    .locals 0
    .param p0    # Lcom/android/launcher2/MTKUnreadLoader;

    invoke-direct {p0}, Lcom/android/launcher2/MTKUnreadLoader;->initUnreadNumberFromSystem()V

    return-void
.end method

.method static synthetic access$200(Lcom/android/launcher2/MTKUnreadLoader;)Ljava/lang/ref/WeakReference;
    .locals 1
    .param p0    # Lcom/android/launcher2/MTKUnreadLoader;

    iget-object v0, p0, Lcom/android/launcher2/MTKUnreadLoader;->mCallbacks:Ljava/lang/ref/WeakReference;

    return-object v0
.end method

.method static getExceedText()Ljava/lang/CharSequence;
    .locals 1

    sget-object v0, Lcom/android/launcher2/MTKUnreadLoader;->sExceedString:Landroid/text/SpannableStringBuilder;

    return-object v0
.end method

.method static declared-synchronized getUnreadNumberAt(I)I
    .locals 2
    .param p0    # I

    const-class v1, Lcom/android/launcher2/MTKUnreadLoader;

    monitor-enter v1

    if-ltz p0, :cond_0

    :try_start_0
    sget v0, Lcom/android/launcher2/MTKUnreadLoader;->sUnreadSupportShortcutsNum:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lt p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    monitor-exit v1

    return v0

    :cond_1
    :try_start_1
    sget-object v0, Lcom/android/launcher2/MTKUnreadLoader;->sUnreadSupportShortcuts:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/UnreadSupportShortcut;

    iget v0, v0, Lcom/android/launcher2/UnreadSupportShortcut;->unreadNum:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static getUnreadNumberOfComponent(Landroid/content/ComponentName;)I
    .locals 2
    .param p0    # Landroid/content/ComponentName;

    invoke-static {p0}, Lcom/android/launcher2/MTKUnreadLoader;->supportUnreadFeature(Landroid/content/ComponentName;)I

    move-result v0

    invoke-static {v0}, Lcom/android/launcher2/MTKUnreadLoader;->getUnreadNumberAt(I)I

    move-result v1

    return v1
.end method

.method private static getUnreadSupportShortcutInfo()Ljava/lang/String;
    .locals 4

    const-string v0, " Unread support shortcuts are "

    sget-object v2, Lcom/android/launcher2/MTKUnreadLoader;->sLogLock:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Lcom/android/launcher2/MTKUnreadLoader;->sUnreadSupportShortcuts:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/AbstractCollection;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    monitor-exit v2

    return-object v0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private initUnreadNumberFromSystem()V
    .locals 8

    iget-object v5, p0, Lcom/android/launcher2/MTKUnreadLoader;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget v4, Lcom/android/launcher2/MTKUnreadLoader;->sUnreadSupportShortcutsNum:I

    const/4 v3, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_0

    sget-object v5, Lcom/android/launcher2/MTKUnreadLoader;->sUnreadSupportShortcuts:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/launcher2/UnreadSupportShortcut;

    :try_start_0
    iget-object v5, v3, Lcom/android/launcher2/UnreadSupportShortcut;->key:Ljava/lang/String;

    invoke-static {v0, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v5

    iput v5, v3, Lcom/android/launcher2/UnreadSupportShortcut;->unreadNum:I

    const-string v5, "MTKUnreadLoader"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "initUnreadNumberFromSystem: key = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v3, Lcom/android/launcher2/UnreadSupportShortcut;->key:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", unreadNum = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v3, Lcom/android/launcher2/UnreadSupportShortcut;->unreadNum:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/launcher2/LauncherLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v5, "MTKUnreadLoader"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "initUnreadNumberFromSystem SettingNotFoundException key = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v3, Lcom/android/launcher2/UnreadSupportShortcut;->key:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", e = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/launcher2/LauncherLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_0
    return-void
.end method

.method private loadUnreadSupportShortcuts()V
    .locals 17

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const-string v9, "MTKUnreadLoader"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "loadUnreadSupportShortcuts begin: start = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/android/launcher2/LauncherLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/launcher2/MTKUnreadLoader;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f050002

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v5

    invoke-static {v5}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v2

    const-string v9, "unreadshortcuts"

    invoke-static {v5, v9}, Lcom/android/internal/util/XmlUtils;->beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    invoke-interface {v5}, Landroid/content/res/XmlResourceParser;->getDepth()I

    move-result v3

    const/4 v8, -0x1

    :cond_0
    :goto_0
    invoke-interface {v5}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v8

    const/4 v9, 0x3

    if-ne v8, v9, :cond_1

    invoke-interface {v5}, Landroid/content/res/XmlResourceParser;->getDepth()I

    move-result v9

    if-le v9, v3, :cond_2

    :cond_1
    const/4 v9, 0x1

    if-eq v8, v9, :cond_2

    const/4 v9, 0x2

    if-ne v8, v9, :cond_0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/launcher2/MTKUnreadLoader;->mContext:Landroid/content/Context;

    sget-object v10, Lcom/android/launcher/R$styleable;->UnreadShortcut:[I

    invoke-virtual {v9, v2, v10}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    sget-object v10, Lcom/android/launcher2/MTKUnreadLoader;->sLogLock:Ljava/lang/Object;

    monitor-enter v10
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2

    :try_start_1
    sget-object v9, Lcom/android/launcher2/MTKUnreadLoader;->sUnreadSupportShortcuts:Ljava/util/ArrayList;

    new-instance v11, Lcom/android/launcher2/UnreadSupportShortcut;

    const/4 v12, 0x0

    invoke-virtual {v1, v12}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x1

    invoke-virtual {v1, v13}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x2

    invoke-virtual {v1, v14}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x3

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v1, v15, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v15

    invoke-direct {v11, v12, v13, v14, v15}, Lcom/android/launcher2/UnreadSupportShortcut;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V
    :try_end_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    :catch_0
    move-exception v4

    const-string v9, "MTKUnreadLoader"

    const-string v10, "Got XmlPullParserException while parsing unread shortcuts."

    invoke-static {v9, v10, v4}, Lcom/android/launcher2/LauncherLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_2
    :goto_1
    sget-object v9, Lcom/android/launcher2/MTKUnreadLoader;->sUnreadSupportShortcuts:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    sput v9, Lcom/android/launcher2/MTKUnreadLoader;->sUnreadSupportShortcutsNum:I

    const-string v9, "MTKUnreadLoader"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "loadUnreadSupportShortcuts end: time used = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    sub-long/2addr v11, v6

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ",sUnreadSupportShortcuts = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-object v11, Lcom/android/launcher2/MTKUnreadLoader;->sUnreadSupportShortcuts:Ljava/util/ArrayList;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {}, Lcom/android/launcher2/MTKUnreadLoader;->getUnreadSupportShortcutInfo()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/android/launcher2/LauncherLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :catchall_0
    move-exception v9

    :try_start_3
    monitor-exit v10
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v9
    :try_end_4
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_2

    :catch_1
    move-exception v4

    const-string v9, "MTKUnreadLoader"

    const-string v10, "Got IOException while parsing unread shortcuts."

    invoke-static {v9, v10, v4}, Lcom/android/launcher2/LauncherLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :catch_2
    move-exception v4

    const-string v9, "MTKUnreadLoader"

    const-string v10, "Got RuntimeException while parsing unread shortcuts."

    invoke-static {v9, v10, v4}, Lcom/android/launcher2/LauncherLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method static declared-synchronized setUnreadNumberAt(II)Z
    .locals 2
    .param p0    # I
    .param p1    # I

    const-class v1, Lcom/android/launcher2/MTKUnreadLoader;

    monitor-enter v1

    if-gez p0, :cond_0

    :try_start_0
    sget v0, Lcom/android/launcher2/MTKUnreadLoader;->sUnreadSupportShortcutsNum:I

    if-ge p0, v0, :cond_1

    :cond_0
    sget-object v0, Lcom/android/launcher2/MTKUnreadLoader;->sUnreadSupportShortcuts:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/UnreadSupportShortcut;

    iget v0, v0, Lcom/android/launcher2/UnreadSupportShortcut;->unreadNum:I

    if-eq v0, p1, :cond_1

    sget-object v0, Lcom/android/launcher2/MTKUnreadLoader;->sUnreadSupportShortcuts:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/UnreadSupportShortcut;

    iput p1, v0, Lcom/android/launcher2/UnreadSupportShortcut;->unreadNum:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static supportUnreadFeature(Landroid/content/ComponentName;)I
    .locals 5
    .param p0    # Landroid/content/ComponentName;

    const/4 v4, -0x1

    if-nez p0, :cond_1

    move v0, v4

    :cond_0
    :goto_0
    return v0

    :cond_1
    sget-object v3, Lcom/android/launcher2/MTKUnreadLoader;->sUnreadSupportShortcuts:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v0, 0x0

    move v2, v1

    :goto_1
    if-ge v0, v2, :cond_2

    sget-object v3, Lcom/android/launcher2/MTKUnreadLoader;->sUnreadSupportShortcuts:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/launcher2/UnreadSupportShortcut;

    iget-object v3, v3, Lcom/android/launcher2/UnreadSupportShortcut;->component:Landroid/content/ComponentName;

    invoke-virtual {v3, p0}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v0, v4

    goto :goto_0
.end method


# virtual methods
.method public initialize(Lcom/android/launcher2/MTKUnreadLoader$UnreadCallbacks;)V
    .locals 1
    .param p1    # Lcom/android/launcher2/MTKUnreadLoader$UnreadCallbacks;

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/launcher2/MTKUnreadLoader;->mCallbacks:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method loadAndInitUnreadShortcuts()V
    .locals 2

    new-instance v0, Lcom/android/launcher2/MTKUnreadLoader$1;

    invoke-direct {v0, p0}, Lcom/android/launcher2/MTKUnreadLoader$1;-><init>(Lcom/android/launcher2/MTKUnreadLoader;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v9, -0x1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v6, "com.mediatek.action.UNREAD_CHANGED"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const-string v6, "com.mediatek.intent.extra.UNREAD_COMPONENT"

    invoke-virtual {p2, v6}, Landroid/content/Intent;->getExtra(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ComponentName;

    const-string v6, "com.mediatek.intent.extra.UNREAD_NUMBER"

    invoke-virtual {p2, v6, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    const-string v6, "MTKUnreadLoader"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Receive unread broadcast: componentName = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", unreadNum = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", mCallbacks = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/launcher2/MTKUnreadLoader;->mCallbacks:Ljava/lang/ref/WeakReference;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Lcom/android/launcher2/MTKUnreadLoader;->getUnreadSupportShortcutInfo()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/launcher2/LauncherLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v6, p0, Lcom/android/launcher2/MTKUnreadLoader;->mCallbacks:Ljava/lang/ref/WeakReference;

    if-eqz v6, :cond_0

    if-eqz v2, :cond_0

    if-eq v5, v9, :cond_0

    invoke-static {v2}, Lcom/android/launcher2/MTKUnreadLoader;->supportUnreadFeature(Landroid/content/ComponentName;)I

    move-result v3

    if-ltz v3, :cond_0

    invoke-static {v3, v5}, Lcom/android/launcher2/MTKUnreadLoader;->setUnreadNumberAt(II)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v6, p0, Lcom/android/launcher2/MTKUnreadLoader;->mCallbacks:Ljava/lang/ref/WeakReference;

    invoke-virtual {v6}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/MTKUnreadLoader$UnreadCallbacks;

    if-eqz v1, :cond_0

    invoke-interface {v1, v2, v5}, Lcom/android/launcher2/MTKUnreadLoader$UnreadCallbacks;->bindComponentUnreadChanged(Landroid/content/ComponentName;I)V

    :cond_0
    return-void
.end method
