.class Lcom/android/launcher2/PendingAddWidgetInfo;
.super Lcom/android/launcher2/PendingAddItemInfo;
.source "PendingAddItemInfo.java"


# instance fields
.field boundWidget:Landroid/appwidget/AppWidgetHostView;

.field configurationData:Landroid/os/Parcelable;

.field icon:I

.field info:Landroid/appwidget/AppWidgetProviderInfo;

.field mimeType:Ljava/lang/String;

.field minHeight:I

.field minResizeHeight:I

.field minResizeWidth:I

.field minWidth:I

.field previewImage:I


# direct methods
.method public constructor <init>(Landroid/appwidget/AppWidgetProviderInfo;Ljava/lang/String;Landroid/os/Parcelable;)V
    .locals 1
    .param p1    # Landroid/appwidget/AppWidgetProviderInfo;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/os/Parcelable;

    invoke-direct {p0}, Lcom/android/launcher2/PendingAddItemInfo;-><init>()V

    const/4 v0, 0x4

    iput v0, p0, Lcom/android/launcher2/ItemInfo;->itemType:I

    iput-object p1, p0, Lcom/android/launcher2/PendingAddWidgetInfo;->info:Landroid/appwidget/AppWidgetProviderInfo;

    iget-object v0, p1, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    iput-object v0, p0, Lcom/android/launcher2/PendingAddItemInfo;->componentName:Landroid/content/ComponentName;

    iget v0, p1, Landroid/appwidget/AppWidgetProviderInfo;->minWidth:I

    iput v0, p0, Lcom/android/launcher2/PendingAddWidgetInfo;->minWidth:I

    iget v0, p1, Landroid/appwidget/AppWidgetProviderInfo;->minHeight:I

    iput v0, p0, Lcom/android/launcher2/PendingAddWidgetInfo;->minHeight:I

    iget v0, p1, Landroid/appwidget/AppWidgetProviderInfo;->minResizeWidth:I

    iput v0, p0, Lcom/android/launcher2/PendingAddWidgetInfo;->minResizeWidth:I

    iget v0, p1, Landroid/appwidget/AppWidgetProviderInfo;->minResizeHeight:I

    iput v0, p0, Lcom/android/launcher2/PendingAddWidgetInfo;->minResizeHeight:I

    iget v0, p1, Landroid/appwidget/AppWidgetProviderInfo;->previewImage:I

    iput v0, p0, Lcom/android/launcher2/PendingAddWidgetInfo;->previewImage:I

    iget v0, p1, Landroid/appwidget/AppWidgetProviderInfo;->icon:I

    iput v0, p0, Lcom/android/launcher2/PendingAddWidgetInfo;->icon:I

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    iput-object p2, p0, Lcom/android/launcher2/PendingAddWidgetInfo;->mimeType:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/launcher2/PendingAddWidgetInfo;->configurationData:Landroid/os/Parcelable;

    :cond_0
    return-void
.end method

.method public constructor <init>(Lcom/android/launcher2/PendingAddWidgetInfo;)V
    .locals 1
    .param p1    # Lcom/android/launcher2/PendingAddWidgetInfo;

    invoke-direct {p0}, Lcom/android/launcher2/PendingAddItemInfo;-><init>()V

    iget v0, p1, Lcom/android/launcher2/PendingAddWidgetInfo;->minWidth:I

    iput v0, p0, Lcom/android/launcher2/PendingAddWidgetInfo;->minWidth:I

    iget v0, p1, Lcom/android/launcher2/PendingAddWidgetInfo;->minHeight:I

    iput v0, p0, Lcom/android/launcher2/PendingAddWidgetInfo;->minHeight:I

    iget v0, p1, Lcom/android/launcher2/PendingAddWidgetInfo;->minResizeWidth:I

    iput v0, p0, Lcom/android/launcher2/PendingAddWidgetInfo;->minResizeWidth:I

    iget v0, p1, Lcom/android/launcher2/PendingAddWidgetInfo;->minResizeHeight:I

    iput v0, p0, Lcom/android/launcher2/PendingAddWidgetInfo;->minResizeHeight:I

    iget v0, p1, Lcom/android/launcher2/PendingAddWidgetInfo;->previewImage:I

    iput v0, p0, Lcom/android/launcher2/PendingAddWidgetInfo;->previewImage:I

    iget v0, p1, Lcom/android/launcher2/PendingAddWidgetInfo;->icon:I

    iput v0, p0, Lcom/android/launcher2/PendingAddWidgetInfo;->icon:I

    iget-object v0, p1, Lcom/android/launcher2/PendingAddWidgetInfo;->info:Landroid/appwidget/AppWidgetProviderInfo;

    iput-object v0, p0, Lcom/android/launcher2/PendingAddWidgetInfo;->info:Landroid/appwidget/AppWidgetProviderInfo;

    iget-object v0, p1, Lcom/android/launcher2/PendingAddWidgetInfo;->boundWidget:Landroid/appwidget/AppWidgetHostView;

    iput-object v0, p0, Lcom/android/launcher2/PendingAddWidgetInfo;->boundWidget:Landroid/appwidget/AppWidgetHostView;

    iget-object v0, p1, Lcom/android/launcher2/PendingAddWidgetInfo;->mimeType:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/launcher2/PendingAddWidgetInfo;->mimeType:Ljava/lang/String;

    iget-object v0, p1, Lcom/android/launcher2/PendingAddWidgetInfo;->configurationData:Landroid/os/Parcelable;

    iput-object v0, p0, Lcom/android/launcher2/PendingAddWidgetInfo;->configurationData:Landroid/os/Parcelable;

    iget-object v0, p1, Lcom/android/launcher2/PendingAddItemInfo;->componentName:Landroid/content/ComponentName;

    iput-object v0, p0, Lcom/android/launcher2/PendingAddItemInfo;->componentName:Landroid/content/ComponentName;

    iget v0, p1, Lcom/android/launcher2/ItemInfo;->itemType:I

    iput v0, p0, Lcom/android/launcher2/ItemInfo;->itemType:I

    iget v0, p1, Lcom/android/launcher2/ItemInfo;->spanX:I

    iput v0, p0, Lcom/android/launcher2/ItemInfo;->spanX:I

    iget v0, p1, Lcom/android/launcher2/ItemInfo;->spanY:I

    iput v0, p0, Lcom/android/launcher2/ItemInfo;->spanY:I

    iget v0, p1, Lcom/android/launcher2/ItemInfo;->minSpanX:I

    iput v0, p0, Lcom/android/launcher2/ItemInfo;->minSpanX:I

    iget v0, p1, Lcom/android/launcher2/ItemInfo;->minSpanY:I

    iput v0, p0, Lcom/android/launcher2/ItemInfo;->minSpanY:I

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Widget: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher2/PendingAddItemInfo;->componentName:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
