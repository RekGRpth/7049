.class public Lcom/android/launcher2/RocketLauncher$Board$FlyingStar;
.super Lcom/android/launcher2/RocketLauncher$Board$FlyingIcon;
.source "RocketLauncher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/launcher2/RocketLauncher$Board;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "FlyingStar"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/launcher2/RocketLauncher$Board;


# direct methods
.method public constructor <init>(Lcom/android/launcher2/RocketLauncher$Board;Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/util/AttributeSet;

    iput-object p1, p0, Lcom/android/launcher2/RocketLauncher$Board$FlyingStar;->this$0:Lcom/android/launcher2/RocketLauncher$Board;

    invoke-direct {p0, p1, p2, p3}, Lcom/android/launcher2/RocketLauncher$Board$FlyingIcon;-><init>(Lcom/android/launcher2/RocketLauncher$Board;Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public randomize()V
    .locals 2

    invoke-super {p0}, Lcom/android/launcher2/RocketLauncher$Board$FlyingIcon;->randomize()V

    const v0, 0x443b8000

    const/high16 v1, 0x44fa0000

    invoke-static {v0, v1}, Lcom/android/launcher2/RocketLauncher$Board;->randfrange(FF)F

    move-result v0

    iput v0, p0, Lcom/android/launcher2/RocketLauncher$Board$FlyingIcon;->v:F

    const/high16 v0, 0x3f800000

    const/high16 v1, 0x40000000

    invoke-static {v0, v1}, Lcom/android/launcher2/RocketLauncher$Board;->randfrange(FF)F

    move-result v0

    iput v0, p0, Lcom/android/launcher2/RocketLauncher$Board$FlyingIcon;->endscale:F

    return-void
.end method

.method public randomizeIcon()V
    .locals 1

    const v0, 0x7f020091

    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    return-void
.end method
