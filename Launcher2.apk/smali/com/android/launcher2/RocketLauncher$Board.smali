.class public Lcom/android/launcher2/RocketLauncher$Board;
.super Landroid/widget/FrameLayout;
.source "RocketLauncher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/launcher2/RocketLauncher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Board"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/launcher2/RocketLauncher$Board$FlyingStar;,
        Lcom/android/launcher2/RocketLauncher$Board$FlyingIcon;
    }
.end annotation


# static fields
.field public static final FIXED_STARS:Z = true

.field public static final FLYING_STARS:Z = true

.field public static final LAUNCH_ZOOM_TIME:I = 0x190

.field public static final MANEUVERING_THRUST_SCALE:F = 0.1f

.field public static final NUM_ICONS:I = 0x14

.field static sRNG:Ljava/util/Random;


# instance fields
.field mAnim:Landroid/animation/TimeAnimator;

.field mComponentNames:[Landroid/content/ComponentName;

.field final mEngageWarp:Ljava/lang/Runnable;

.field mIcons:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/content/ComponentName;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mManeuveringThrusters:Z

.field private mSpeedScale:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lcom/android/launcher2/RocketLauncher$Board;->sRNG:Ljava/util/Random;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/launcher2/RocketLauncher$Board;->mManeuveringThrusters:Z

    const/high16 v1, 0x3f800000

    iput v1, p0, Lcom/android/launcher2/RocketLauncher$Board;->mSpeedScale:F

    new-instance v1, Lcom/android/launcher2/RocketLauncher$Board$2;

    invoke-direct {v1, p0}, Lcom/android/launcher2/RocketLauncher$Board$2;-><init>(Lcom/android/launcher2/RocketLauncher$Board;)V

    iput-object v1, p0, Lcom/android/launcher2/RocketLauncher$Board;->mEngageWarp:Ljava/lang/Runnable;

    const/high16 v1, -0x1000000

    invoke-virtual {p0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/LauncherApplication;

    invoke-virtual {v0}, Lcom/android/launcher2/LauncherApplication;->getIconCache()Lcom/android/launcher2/IconCache;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/launcher2/IconCache;->getAllIcons()Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, Lcom/android/launcher2/RocketLauncher$Board;->mIcons:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/android/launcher2/RocketLauncher$Board;->mIcons:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    new-array v1, v1, [Landroid/content/ComponentName;

    iput-object v1, p0, Lcom/android/launcher2/RocketLauncher$Board;->mComponentNames:[Landroid/content/ComponentName;

    iget-object v1, p0, Lcom/android/launcher2/RocketLauncher$Board;->mIcons:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    iget-object v2, p0, Lcom/android/launcher2/RocketLauncher$Board;->mComponentNames:[Landroid/content/ComponentName;

    invoke-interface {v1, v2}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/content/ComponentName;

    iput-object v1, p0, Lcom/android/launcher2/RocketLauncher$Board;->mComponentNames:[Landroid/content/ComponentName;

    return-void
.end method

.method static synthetic access$000(Lcom/android/launcher2/RocketLauncher$Board;)Z
    .locals 1
    .param p0    # Lcom/android/launcher2/RocketLauncher$Board;

    iget-boolean v0, p0, Lcom/android/launcher2/RocketLauncher$Board;->mManeuveringThrusters:Z

    return v0
.end method

.method static synthetic access$002(Lcom/android/launcher2/RocketLauncher$Board;Z)Z
    .locals 0
    .param p0    # Lcom/android/launcher2/RocketLauncher$Board;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/launcher2/RocketLauncher$Board;->mManeuveringThrusters:Z

    return p1
.end method

.method static synthetic access$100(Lcom/android/launcher2/RocketLauncher$Board;)F
    .locals 1
    .param p0    # Lcom/android/launcher2/RocketLauncher$Board;

    iget v0, p0, Lcom/android/launcher2/RocketLauncher$Board;->mSpeedScale:F

    return v0
.end method

.method static synthetic access$102(Lcom/android/launcher2/RocketLauncher$Board;F)F
    .locals 0
    .param p0    # Lcom/android/launcher2/RocketLauncher$Board;
    .param p1    # F

    iput p1, p0, Lcom/android/launcher2/RocketLauncher$Board;->mSpeedScale:F

    return p1
.end method

.method static synthetic access$116(Lcom/android/launcher2/RocketLauncher$Board;F)F
    .locals 1
    .param p0    # Lcom/android/launcher2/RocketLauncher$Board;
    .param p1    # F

    iget v0, p0, Lcom/android/launcher2/RocketLauncher$Board;->mSpeedScale:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/android/launcher2/RocketLauncher$Board;->mSpeedScale:F

    return v0
.end method

.method static synthetic access$124(Lcom/android/launcher2/RocketLauncher$Board;F)F
    .locals 1
    .param p0    # Lcom/android/launcher2/RocketLauncher$Board;
    .param p1    # F

    iget v0, p0, Lcom/android/launcher2/RocketLauncher$Board;->mSpeedScale:F

    sub-float/2addr v0, p1

    iput v0, p0, Lcom/android/launcher2/RocketLauncher$Board;->mSpeedScale:F

    return v0
.end method

.method static lerp(FFF)F
    .locals 1
    .param p0    # F
    .param p1    # F
    .param p2    # F

    sub-float v0, p1, p0

    mul-float/2addr v0, p2

    add-float/2addr v0, p0

    return v0
.end method

.method static pick([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">([TE;)TE;"
        }
    .end annotation

    array-length v0, p0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/android/launcher2/RocketLauncher$Board;->sRNG:Ljava/util/Random;

    array-length v1, p0

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    aget-object v0, p0, v0

    goto :goto_0
.end method

.method static randfrange(FF)F
    .locals 1
    .param p0    # F
    .param p1    # F

    sget-object v0, Lcom/android/launcher2/RocketLauncher$Board;->sRNG:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextFloat()F

    move-result v0

    invoke-static {p0, p1, v0}, Lcom/android/launcher2/RocketLauncher$Board;->lerp(FFF)F

    move-result v0

    return v0
.end method

.method static randsign()I
    .locals 1

    sget-object v0, Lcom/android/launcher2/RocketLauncher$Board;->sRNG:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextBoolean()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private reset()V
    .locals 10

    const/16 v9, 0x14

    const/4 v5, -0x2

    const/high16 v8, 0x3f400000

    const/4 v7, 0x0

    const/4 v6, 0x0

    invoke-virtual {p0}, Landroid/view/ViewGroup;->removeAllViews()V

    new-instance v4, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v4, v5, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v9, :cond_0

    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v0, v5, v6}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const v5, 0x7f020091

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    const/high16 v5, 0x3e800000

    invoke-static {v5, v8}, Lcom/android/launcher2/RocketLauncher$Board;->randfrange(FF)F

    move-result v3

    invoke-virtual {v0, v3}, Landroid/view/View;->setScaleX(F)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setScaleY(F)V

    invoke-virtual {v0, v8}, Landroid/view/View;->setAlpha(F)V

    invoke-virtual {p0, v0, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v5

    int-to-float v5, v5

    invoke-static {v7, v5}, Lcom/android/launcher2/RocketLauncher$Board;->randfrange(FF)F

    move-result v5

    invoke-virtual {v0, v5}, Landroid/view/View;->setX(F)V

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v5

    int-to-float v5, v5

    invoke-static {v7, v5}, Lcom/android/launcher2/RocketLauncher$Board;->randfrange(FF)F

    move-result v5

    invoke-virtual {v0, v5}, Landroid/view/View;->setY(F)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_1
    const/16 v5, 0x28

    if-ge v1, v5, :cond_2

    if-ge v1, v9, :cond_1

    new-instance v2, Lcom/android/launcher2/RocketLauncher$Board$FlyingStar;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v2, p0, v5, v6}, Lcom/android/launcher2/RocketLauncher$Board$FlyingStar;-><init>(Lcom/android/launcher2/RocketLauncher$Board;Landroid/content/Context;Landroid/util/AttributeSet;)V

    :goto_2
    invoke-virtual {p0, v2, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v2}, Lcom/android/launcher2/RocketLauncher$Board$FlyingIcon;->reset()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    new-instance v2, Lcom/android/launcher2/RocketLauncher$Board$FlyingIcon;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v2, p0, v5, v6}, Lcom/android/launcher2/RocketLauncher$Board$FlyingIcon;-><init>(Lcom/android/launcher2/RocketLauncher$Board;Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto :goto_2

    :cond_2
    new-instance v5, Landroid/animation/TimeAnimator;

    invoke-direct {v5}, Landroid/animation/TimeAnimator;-><init>()V

    iput-object v5, p0, Lcom/android/launcher2/RocketLauncher$Board;->mAnim:Landroid/animation/TimeAnimator;

    iget-object v5, p0, Lcom/android/launcher2/RocketLauncher$Board;->mAnim:Landroid/animation/TimeAnimator;

    new-instance v6, Lcom/android/launcher2/RocketLauncher$Board$1;

    invoke-direct {v6, p0}, Lcom/android/launcher2/RocketLauncher$Board$1;-><init>(Lcom/android/launcher2/RocketLauncher$Board;)V

    invoke-virtual {v5, v6}, Landroid/animation/TimeAnimator;->setTimeListener(Landroid/animation/TimeAnimator$TimeListener;)V

    return-void
.end method


# virtual methods
.method public isOpaque()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/view/View;->setSystemUiVisibility(I)V

    invoke-direct {p0}, Lcom/android/launcher2/RocketLauncher$Board;->reset()V

    iget-object v0, p0, Lcom/android/launcher2/RocketLauncher$Board;->mAnim:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    iget-object v0, p0, Lcom/android/launcher2/RocketLauncher$Board;->mAnim:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-boolean v0, p0, Lcom/android/launcher2/RocketLauncher$Board;->mManeuveringThrusters:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    iget-object v0, p0, Lcom/android/launcher2/RocketLauncher$Board;->mAnim:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    invoke-direct {p0}, Lcom/android/launcher2/RocketLauncher$Board;->reset()V

    iget-object v0, p0, Lcom/android/launcher2/RocketLauncher$Board;->mAnim:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/android/launcher2/RocketLauncher$Board;->mManeuveringThrusters:Z

    if-nez v1, :cond_0

    iput-boolean v0, p0, Lcom/android/launcher2/RocketLauncher$Board;->mManeuveringThrusters:Z

    invoke-virtual {p0}, Lcom/android/launcher2/RocketLauncher$Board;->resetWarpTimer()V

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public resetWarpTimer()V
    .locals 4

    invoke-virtual {p0}, Landroid/view/View;->getHandler()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher2/RocketLauncher$Board;->mEngageWarp:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v1, p0, Lcom/android/launcher2/RocketLauncher$Board;->mEngageWarp:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method
