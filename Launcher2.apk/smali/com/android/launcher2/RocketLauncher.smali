.class public Lcom/android/launcher2/RocketLauncher;
.super Landroid/support/v13/dreams/BasicDream;
.source "RocketLauncher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/launcher2/RocketLauncher$Board;
    }
.end annotation


# static fields
.field public static final ROCKET_LAUNCHER:Z = true


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v13/dreams/BasicDream;-><init>()V

    return-void
.end method


# virtual methods
.method public onStart()V
    .locals 5

    invoke-super {p0}, Landroid/support/v13/dreams/BasicDream;->onStart()V

    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v3, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v4, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    if-le v3, v4, :cond_0

    iget v1, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    :goto_0
    new-instance v0, Lcom/android/launcher2/RocketLauncher$Board;

    const/4 v3, 0x0

    invoke-direct {v0, p0, v3}, Lcom/android/launcher2/RocketLauncher$Board;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v3, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v3}, Landroid/app/Activity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget v3, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    sub-int/2addr v3, v1

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {v0, v3}, Landroid/view/View;->setX(F)V

    iget v3, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    sub-int/2addr v3, v1

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {v0, v3}, Landroid/view/View;->setY(F)V

    return-void

    :cond_0
    iget v1, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    goto :goto_0
.end method

.method public onUserInteraction()V
    .locals 0

    return-void
.end method
