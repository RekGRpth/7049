.class Lcom/android/launcher2/SceneChooserDialogFragment$ImageAdapter;
.super Landroid/widget/BaseAdapter;
.source "SceneChooserDialogFragment.java"

# interfaces
.implements Landroid/widget/SpinnerAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/launcher2/SceneChooserDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ImageAdapter"
.end annotation


# instance fields
.field private mLayoutInflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/android/launcher2/SceneChooserDialogFragment;


# direct methods
.method constructor <init>(Lcom/android/launcher2/SceneChooserDialogFragment;Landroid/content/Context;)V
    .locals 1
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/android/launcher2/SceneChooserDialogFragment$ImageAdapter;->this$0:Lcom/android/launcher2/SceneChooserDialogFragment;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/SceneChooserDialogFragment$ImageAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/launcher2/SceneChooserDialogFragment$ImageAdapter;->this$0:Lcom/android/launcher2/SceneChooserDialogFragment;

    invoke-static {v0}, Lcom/android/launcher2/SceneChooserDialogFragment;->access$100(Lcom/android/launcher2/SceneChooserDialogFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/launcher2/SceneChooserDialogFragment$ImageAdapter;->this$0:Lcom/android/launcher2/SceneChooserDialogFragment;

    invoke-static {v0}, Lcom/android/launcher2/SceneChooserDialogFragment;->access$100(Lcom/android/launcher2/SceneChooserDialogFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    if-nez p2, :cond_1

    new-instance v1, Lcom/android/launcher2/SceneChooserDialogFragment$ViewHolder;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/android/launcher2/SceneChooserDialogFragment$ViewHolder;-><init>(Lcom/android/launcher2/SceneChooserDialogFragment$1;)V

    iget-object v2, p0, Lcom/android/launcher2/SceneChooserDialogFragment$ImageAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f030015

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    const v2, 0x7f060036

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v1, Lcom/android/launcher2/SceneChooserDialogFragment$ViewHolder;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    iget-object v3, v1, Lcom/android/launcher2/SceneChooserDialogFragment$ViewHolder;->mImageView:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/android/launcher2/SceneChooserDialogFragment$ImageAdapter;->this$0:Lcom/android/launcher2/SceneChooserDialogFragment;

    invoke-static {v2}, Lcom/android/launcher2/SceneChooserDialogFragment;->access$100(Lcom/android/launcher2/SceneChooserDialogFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/launcher2/SceneChooserDialogFragment$SceneData;

    iget-object v2, v2, Lcom/android/launcher2/SceneChooserDialogFragment$SceneData;->mScenePreviewImage:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v2, v1, Lcom/android/launcher2/SceneChooserDialogFragment$ViewHolder;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setDither(Z)V

    :cond_0
    return-object p2

    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/SceneChooserDialogFragment$ViewHolder;

    goto :goto_0
.end method
