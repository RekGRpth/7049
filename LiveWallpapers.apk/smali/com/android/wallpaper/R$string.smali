.class public final Lcom/android/wallpaper/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/wallpaper/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final clock_settings:I = 0x7f090013

.field public static final palette:I = 0x7f090016

.field public static final palette_black_c:I = 0x7f09001b

.field public static final palette_gray:I = 0x7f090017

.field public static final palette_halloween:I = 0x7f09001c

.field public static final palette_matrix:I = 0x7f090019

.field public static final palette_oceanic:I = 0x7f09001e

.field public static final palette_violet:I = 0x7f090018

.field public static final palette_white_c:I = 0x7f09001a

.field public static final palette_zenburn:I = 0x7f09001d

.field public static final show_seconds:I = 0x7f090014

.field public static final variable_line_width:I = 0x7f090015

.field public static final wallpaper_clock:I = 0x7f09000d

.field public static final wallpaper_clock_author:I = 0x7f09000e

.field public static final wallpaper_clock_desc:I = 0x7f09000f

.field public static final wallpaper_fall:I = 0x7f09000a

.field public static final wallpaper_fall_author:I = 0x7f09000b

.field public static final wallpaper_fall_desc:I = 0x7f09000c

.field public static final wallpaper_galaxy:I = 0x7f090007

.field public static final wallpaper_galaxy_author:I = 0x7f090008

.field public static final wallpaper_galaxy_desc:I = 0x7f090009

.field public static final wallpaper_grass:I = 0x7f090004

.field public static final wallpaper_grass_author:I = 0x7f090005

.field public static final wallpaper_grass_desc:I = 0x7f090006

.field public static final wallpaper_nexus:I = 0x7f090010

.field public static final wallpaper_nexus_author:I = 0x7f090011

.field public static final wallpaper_nexus_desc:I = 0x7f090012

.field public static final wallpaper_walkaround:I = 0x7f090001

.field public static final wallpaper_walkaround_author:I = 0x7f090002

.field public static final wallpaper_walkaround_desc:I = 0x7f090003

.field public static final wallpapers:I = 0x7f090000


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
