.class public Lcom/android/wallpaper/fall/Fall;
.super Landroid/app/Activity;
.source "Fall.java"


# instance fields
.field private mView:Lcom/android/wallpaper/fall/FallView;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Lcom/android/wallpaper/fall/FallView;

    invoke-direct {v0, p0}, Lcom/android/wallpaper/fall/FallView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/wallpaper/fall/Fall;->mView:Lcom/android/wallpaper/fall/FallView;

    iget-object v0, p0, Lcom/android/wallpaper/fall/Fall;->mView:Lcom/android/wallpaper/fall/FallView;

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(Landroid/view/View;)V

    return-void
.end method

.method protected onPause()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    iget-object v0, p0, Lcom/android/wallpaper/fall/Fall;->mView:Lcom/android/wallpaper/fall/FallView;

    invoke-virtual {v0}, Landroid/renderscript/RSSurfaceView;->pause()V

    return-void
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget-object v0, p0, Lcom/android/wallpaper/fall/Fall;->mView:Lcom/android/wallpaper/fall/FallView;

    invoke-virtual {v0}, Landroid/renderscript/RSSurfaceView;->resume()V

    return-void
.end method
