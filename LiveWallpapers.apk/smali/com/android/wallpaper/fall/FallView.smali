.class Lcom/android/wallpaper/fall/FallView;
.super Landroid/renderscript/RSSurfaceView;
.source "FallView.java"


# instance fields
.field private mRender:Lcom/android/wallpaper/fall/FallRS;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x1

    invoke-direct {p0, p1}, Landroid/renderscript/RSSurfaceView;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Landroid/view/View;->setFocusable(Z)V

    invoke-virtual {p0, v0}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    return-void
.end method


# virtual methods
.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    const/4 v0, 0x1

    return v0

    :pswitch_0
    iget-object v0, p0, Lcom/android/wallpaper/fall/FallView;->mRender:Lcom/android/wallpaper/fall/FallRS;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/android/wallpaper/fall/FallRS;->addDrop(FF)V

    const-wide/16 v0, 0x10

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 5
    .param p1    # Landroid/view/SurfaceHolder;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-super {p0, p1, p2, p3, p4}, Landroid/renderscript/RSSurfaceView;->surfaceChanged(Landroid/view/SurfaceHolder;III)V

    new-instance v1, Landroid/renderscript/RenderScriptGL$SurfaceConfig;

    invoke-direct {v1}, Landroid/renderscript/RenderScriptGL$SurfaceConfig;-><init>()V

    invoke-virtual {p0, v1}, Landroid/renderscript/RSSurfaceView;->createRenderScriptGL(Landroid/renderscript/RenderScriptGL$SurfaceConfig;)Landroid/renderscript/RenderScriptGL;

    move-result-object v0

    new-instance v2, Lcom/android/wallpaper/fall/FallRS;

    invoke-direct {v2, p3, p4}, Lcom/android/wallpaper/fall/FallRS;-><init>(II)V

    iput-object v2, p0, Lcom/android/wallpaper/fall/FallView;->mRender:Lcom/android/wallpaper/fall/FallRS;

    iget-object v2, p0, Lcom/android/wallpaper/fall/FallView;->mRender:Lcom/android/wallpaper/fall/FallRS;

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v0, v3, v4}, Lcom/android/wallpaper/RenderScriptScene;->init(Landroid/renderscript/RenderScriptGL;Landroid/content/res/Resources;Z)V

    iget-object v2, p0, Lcom/android/wallpaper/fall/FallView;->mRender:Lcom/android/wallpaper/fall/FallRS;

    invoke-virtual {v2}, Lcom/android/wallpaper/fall/FallRS;->start()V

    return-void
.end method
