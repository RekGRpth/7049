.class public Lcom/android/wallpaper/fall/ScriptField_Constants$Item;
.super Ljava/lang/Object;
.source "ScriptField_Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/wallpaper/fall/ScriptField_Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Item"
.end annotation


# static fields
.field public static final sizeof:I = 0xc0


# instance fields
.field Drop01:Landroid/renderscript/Float4;

.field Drop02:Landroid/renderscript/Float4;

.field Drop03:Landroid/renderscript/Float4;

.field Drop04:Landroid/renderscript/Float4;

.field Drop05:Landroid/renderscript/Float4;

.field Drop06:Landroid/renderscript/Float4;

.field Drop07:Landroid/renderscript/Float4;

.field Drop08:Landroid/renderscript/Float4;

.field Drop09:Landroid/renderscript/Float4;

.field Drop10:Landroid/renderscript/Float4;

.field Offset:Landroid/renderscript/Float4;

.field Rotate:F


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/renderscript/Float4;

    invoke-direct {v0}, Landroid/renderscript/Float4;-><init>()V

    iput-object v0, p0, Lcom/android/wallpaper/fall/ScriptField_Constants$Item;->Drop01:Landroid/renderscript/Float4;

    new-instance v0, Landroid/renderscript/Float4;

    invoke-direct {v0}, Landroid/renderscript/Float4;-><init>()V

    iput-object v0, p0, Lcom/android/wallpaper/fall/ScriptField_Constants$Item;->Drop02:Landroid/renderscript/Float4;

    new-instance v0, Landroid/renderscript/Float4;

    invoke-direct {v0}, Landroid/renderscript/Float4;-><init>()V

    iput-object v0, p0, Lcom/android/wallpaper/fall/ScriptField_Constants$Item;->Drop03:Landroid/renderscript/Float4;

    new-instance v0, Landroid/renderscript/Float4;

    invoke-direct {v0}, Landroid/renderscript/Float4;-><init>()V

    iput-object v0, p0, Lcom/android/wallpaper/fall/ScriptField_Constants$Item;->Drop04:Landroid/renderscript/Float4;

    new-instance v0, Landroid/renderscript/Float4;

    invoke-direct {v0}, Landroid/renderscript/Float4;-><init>()V

    iput-object v0, p0, Lcom/android/wallpaper/fall/ScriptField_Constants$Item;->Drop05:Landroid/renderscript/Float4;

    new-instance v0, Landroid/renderscript/Float4;

    invoke-direct {v0}, Landroid/renderscript/Float4;-><init>()V

    iput-object v0, p0, Lcom/android/wallpaper/fall/ScriptField_Constants$Item;->Drop06:Landroid/renderscript/Float4;

    new-instance v0, Landroid/renderscript/Float4;

    invoke-direct {v0}, Landroid/renderscript/Float4;-><init>()V

    iput-object v0, p0, Lcom/android/wallpaper/fall/ScriptField_Constants$Item;->Drop07:Landroid/renderscript/Float4;

    new-instance v0, Landroid/renderscript/Float4;

    invoke-direct {v0}, Landroid/renderscript/Float4;-><init>()V

    iput-object v0, p0, Lcom/android/wallpaper/fall/ScriptField_Constants$Item;->Drop08:Landroid/renderscript/Float4;

    new-instance v0, Landroid/renderscript/Float4;

    invoke-direct {v0}, Landroid/renderscript/Float4;-><init>()V

    iput-object v0, p0, Lcom/android/wallpaper/fall/ScriptField_Constants$Item;->Drop09:Landroid/renderscript/Float4;

    new-instance v0, Landroid/renderscript/Float4;

    invoke-direct {v0}, Landroid/renderscript/Float4;-><init>()V

    iput-object v0, p0, Lcom/android/wallpaper/fall/ScriptField_Constants$Item;->Drop10:Landroid/renderscript/Float4;

    new-instance v0, Landroid/renderscript/Float4;

    invoke-direct {v0}, Landroid/renderscript/Float4;-><init>()V

    iput-object v0, p0, Lcom/android/wallpaper/fall/ScriptField_Constants$Item;->Offset:Landroid/renderscript/Float4;

    return-void
.end method
