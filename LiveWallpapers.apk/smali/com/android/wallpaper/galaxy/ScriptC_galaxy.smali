.class public Lcom/android/wallpaper/galaxy/ScriptC_galaxy;
.super Landroid/renderscript/ScriptC;
.source "ScriptC_galaxy.java"


# static fields
.field private static final mExportFuncIdx_initParticles:I = 0x0

.field private static final mExportVarIdx_Particles:I = 0xb

.field private static final mExportVarIdx_gIsPreview:I = 0x1

.field private static final mExportVarIdx_gPFBackground:I = 0x2

.field private static final mExportVarIdx_gPFStars:I = 0x3

.field private static final mExportVarIdx_gPSLights:I = 0x6

.field private static final mExportVarIdx_gPVBkProj:I = 0x5

.field private static final mExportVarIdx_gPVStars:I = 0x4

.field private static final mExportVarIdx_gParticlesMesh:I = 0xa

.field private static final mExportVarIdx_gTFlares:I = 0x8

.field private static final mExportVarIdx_gTLight1:I = 0x9

.field private static final mExportVarIdx_gTSpace:I = 0x7

.field private static final mExportVarIdx_gXOffset:I = 0x0

.field private static final mExportVarIdx_vpConstants:I = 0xc


# instance fields
.field private __ALLOCATION:Landroid/renderscript/Element;

.field private __F32:Landroid/renderscript/Element;

.field private __I32:Landroid/renderscript/Element;

.field private __MESH:Landroid/renderscript/Element;

.field private __PROGRAM_FRAGMENT:Landroid/renderscript/Element;

.field private __PROGRAM_STORE:Landroid/renderscript/Element;

.field private __PROGRAM_VERTEX:Landroid/renderscript/Element;

.field private mExportVar_Particles:Lcom/android/wallpaper/galaxy/ScriptField_Particle;

.field private mExportVar_gIsPreview:I

.field private mExportVar_gPFBackground:Landroid/renderscript/ProgramFragment;

.field private mExportVar_gPFStars:Landroid/renderscript/ProgramFragment;

.field private mExportVar_gPSLights:Landroid/renderscript/ProgramStore;

.field private mExportVar_gPVBkProj:Landroid/renderscript/ProgramVertex;

.field private mExportVar_gPVStars:Landroid/renderscript/ProgramVertex;

.field private mExportVar_gParticlesMesh:Landroid/renderscript/Mesh;

.field private mExportVar_gTFlares:Landroid/renderscript/Allocation;

.field private mExportVar_gTLight1:Landroid/renderscript/Allocation;

.field private mExportVar_gTSpace:Landroid/renderscript/Allocation;

.field private mExportVar_gXOffset:F

.field private mExportVar_vpConstants:Lcom/android/wallpaper/galaxy/ScriptField_VpConsts;


# direct methods
.method public constructor <init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V
    .locals 1
    .param p1    # Landroid/renderscript/RenderScript;
    .param p2    # Landroid/content/res/Resources;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/renderscript/ScriptC;-><init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V

    invoke-static {p1}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->__F32:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->I32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->__I32:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_FRAGMENT(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->__PROGRAM_FRAGMENT:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_VERTEX(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->__PROGRAM_VERTEX:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_STORE(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->__PROGRAM_STORE:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->ALLOCATION(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->__ALLOCATION:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->MESH(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->__MESH:Landroid/renderscript/Element;

    return-void
.end method


# virtual methods
.method public bind_Particles(Lcom/android/wallpaper/galaxy/ScriptField_Particle;)V
    .locals 2
    .param p1    # Lcom/android/wallpaper/galaxy/ScriptField_Particle;

    const/16 v1, 0xb

    iput-object p1, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->mExportVar_Particles:Lcom/android/wallpaper/galaxy/ScriptField_Particle;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/renderscript/Script;->bindAllocation(Landroid/renderscript/Allocation;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/renderscript/Script$FieldBase;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Landroid/renderscript/Script;->bindAllocation(Landroid/renderscript/Allocation;I)V

    goto :goto_0
.end method

.method public bind_vpConstants(Lcom/android/wallpaper/galaxy/ScriptField_VpConsts;)V
    .locals 2
    .param p1    # Lcom/android/wallpaper/galaxy/ScriptField_VpConsts;

    const/16 v1, 0xc

    iput-object p1, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->mExportVar_vpConstants:Lcom/android/wallpaper/galaxy/ScriptField_VpConsts;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/renderscript/Script;->bindAllocation(Landroid/renderscript/Allocation;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/renderscript/Script$FieldBase;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Landroid/renderscript/Script;->bindAllocation(Landroid/renderscript/Allocation;I)V

    goto :goto_0
.end method

.method public get_Particles()Lcom/android/wallpaper/galaxy/ScriptField_Particle;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->mExportVar_Particles:Lcom/android/wallpaper/galaxy/ScriptField_Particle;

    return-object v0
.end method

.method public get_gIsPreview()I
    .locals 1

    iget v0, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->mExportVar_gIsPreview:I

    return v0
.end method

.method public get_gPFBackground()Landroid/renderscript/ProgramFragment;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->mExportVar_gPFBackground:Landroid/renderscript/ProgramFragment;

    return-object v0
.end method

.method public get_gPFStars()Landroid/renderscript/ProgramFragment;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->mExportVar_gPFStars:Landroid/renderscript/ProgramFragment;

    return-object v0
.end method

.method public get_gPSLights()Landroid/renderscript/ProgramStore;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->mExportVar_gPSLights:Landroid/renderscript/ProgramStore;

    return-object v0
.end method

.method public get_gPVBkProj()Landroid/renderscript/ProgramVertex;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->mExportVar_gPVBkProj:Landroid/renderscript/ProgramVertex;

    return-object v0
.end method

.method public get_gPVStars()Landroid/renderscript/ProgramVertex;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->mExportVar_gPVStars:Landroid/renderscript/ProgramVertex;

    return-object v0
.end method

.method public get_gParticlesMesh()Landroid/renderscript/Mesh;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->mExportVar_gParticlesMesh:Landroid/renderscript/Mesh;

    return-object v0
.end method

.method public get_gTFlares()Landroid/renderscript/Allocation;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->mExportVar_gTFlares:Landroid/renderscript/Allocation;

    return-object v0
.end method

.method public get_gTLight1()Landroid/renderscript/Allocation;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->mExportVar_gTLight1:Landroid/renderscript/Allocation;

    return-object v0
.end method

.method public get_gTSpace()Landroid/renderscript/Allocation;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->mExportVar_gTSpace:Landroid/renderscript/Allocation;

    return-object v0
.end method

.method public get_gXOffset()F
    .locals 1

    iget v0, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->mExportVar_gXOffset:F

    return v0
.end method

.method public get_vpConstants()Lcom/android/wallpaper/galaxy/ScriptField_VpConsts;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->mExportVar_vpConstants:Lcom/android/wallpaper/galaxy/ScriptField_VpConsts;

    return-object v0
.end method

.method public invoke_initParticles()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/renderscript/Script;->invoke(I)V

    return-void
.end method

.method public set_gIsPreview(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->mExportVar_gIsPreview:I

    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(II)V

    return-void
.end method

.method public set_gPFBackground(Landroid/renderscript/ProgramFragment;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramFragment;

    iput-object p1, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->mExportVar_gPFBackground:Landroid/renderscript/ProgramFragment;

    const/4 v0, 0x2

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gPFStars(Landroid/renderscript/ProgramFragment;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramFragment;

    iput-object p1, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->mExportVar_gPFStars:Landroid/renderscript/ProgramFragment;

    const/4 v0, 0x3

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gPSLights(Landroid/renderscript/ProgramStore;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramStore;

    iput-object p1, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->mExportVar_gPSLights:Landroid/renderscript/ProgramStore;

    const/4 v0, 0x6

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gPVBkProj(Landroid/renderscript/ProgramVertex;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramVertex;

    iput-object p1, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->mExportVar_gPVBkProj:Landroid/renderscript/ProgramVertex;

    const/4 v0, 0x5

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gPVStars(Landroid/renderscript/ProgramVertex;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramVertex;

    iput-object p1, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->mExportVar_gPVStars:Landroid/renderscript/ProgramVertex;

    const/4 v0, 0x4

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gParticlesMesh(Landroid/renderscript/Mesh;)V
    .locals 1
    .param p1    # Landroid/renderscript/Mesh;

    iput-object p1, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->mExportVar_gParticlesMesh:Landroid/renderscript/Mesh;

    const/16 v0, 0xa

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gTFlares(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    iput-object p1, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->mExportVar_gTFlares:Landroid/renderscript/Allocation;

    const/16 v0, 0x8

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gTLight1(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    iput-object p1, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->mExportVar_gTLight1:Landroid/renderscript/Allocation;

    const/16 v0, 0x9

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gTSpace(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    iput-object p1, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->mExportVar_gTSpace:Landroid/renderscript/Allocation;

    const/4 v0, 0x7

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gXOffset(F)V
    .locals 1
    .param p1    # F

    iput p1, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->mExportVar_gXOffset:F

    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(IF)V

    return-void
.end method
