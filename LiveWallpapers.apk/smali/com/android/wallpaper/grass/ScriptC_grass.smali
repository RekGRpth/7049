.class public Lcom/android/wallpaper/grass/ScriptC_grass;
.super Landroid/renderscript/ScriptC;
.source "ScriptC_grass.java"


# static fields
.field private static final mExportFuncIdx_updateBlades:I = 0x0

.field private static final mExportVarIdx_Blades:I = 0x14

.field private static final mExportVarIdx_Verticies:I = 0x15

.field private static final mExportVarIdx_gAfternoon:I = 0x7

.field private static final mExportVarIdx_gBladesCount:I = 0x0

.field private static final mExportVarIdx_gBladesMesh:I = 0x13

.field private static final mExportVarIdx_gDawn:I = 0x5

.field private static final mExportVarIdx_gDusk:I = 0x8

.field private static final mExportVarIdx_gHeight:I = 0x3

.field private static final mExportVarIdx_gIndexCount:I = 0x1

.field private static final mExportVarIdx_gIsPreview:I = 0x9

.field private static final mExportVarIdx_gMorning:I = 0x6

.field private static final mExportVarIdx_gPFBackground:I = 0xb

.field private static final mExportVarIdx_gPFGrass:I = 0xc

.field private static final mExportVarIdx_gPSBackground:I = 0xd

.field private static final mExportVarIdx_gPVBackground:I = 0xa

.field private static final mExportVarIdx_gTAa:I = 0x12

.field private static final mExportVarIdx_gTNight:I = 0xe

.field private static final mExportVarIdx_gTSky:I = 0x11

.field private static final mExportVarIdx_gTSunrise:I = 0x10

.field private static final mExportVarIdx_gTSunset:I = 0xf

.field private static final mExportVarIdx_gWidth:I = 0x2

.field private static final mExportVarIdx_gXOffset:I = 0x4


# instance fields
.field private __ALLOCATION:Landroid/renderscript/Element;

.field private __F32:Landroid/renderscript/Element;

.field private __I32:Landroid/renderscript/Element;

.field private __MESH:Landroid/renderscript/Element;

.field private __PROGRAM_FRAGMENT:Landroid/renderscript/Element;

.field private __PROGRAM_STORE:Landroid/renderscript/Element;

.field private __PROGRAM_VERTEX:Landroid/renderscript/Element;

.field private mExportVar_Blades:Lcom/android/wallpaper/grass/ScriptField_Blade;

.field private mExportVar_Verticies:Lcom/android/wallpaper/grass/ScriptField_Vertex;

.field private mExportVar_gAfternoon:F

.field private mExportVar_gBladesCount:I

.field private mExportVar_gBladesMesh:Landroid/renderscript/Mesh;

.field private mExportVar_gDawn:F

.field private mExportVar_gDusk:F

.field private mExportVar_gHeight:I

.field private mExportVar_gIndexCount:I

.field private mExportVar_gIsPreview:I

.field private mExportVar_gMorning:F

.field private mExportVar_gPFBackground:Landroid/renderscript/ProgramFragment;

.field private mExportVar_gPFGrass:Landroid/renderscript/ProgramFragment;

.field private mExportVar_gPSBackground:Landroid/renderscript/ProgramStore;

.field private mExportVar_gPVBackground:Landroid/renderscript/ProgramVertex;

.field private mExportVar_gTAa:Landroid/renderscript/Allocation;

.field private mExportVar_gTNight:Landroid/renderscript/Allocation;

.field private mExportVar_gTSky:Landroid/renderscript/Allocation;

.field private mExportVar_gTSunrise:Landroid/renderscript/Allocation;

.field private mExportVar_gTSunset:Landroid/renderscript/Allocation;

.field private mExportVar_gWidth:I

.field private mExportVar_gXOffset:F


# direct methods
.method public constructor <init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V
    .locals 1
    .param p1    # Landroid/renderscript/RenderScript;
    .param p2    # Landroid/content/res/Resources;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/renderscript/ScriptC;-><init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V

    invoke-static {p1}, Landroid/renderscript/Element;->I32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->__I32:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->__F32:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_VERTEX(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->__PROGRAM_VERTEX:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_FRAGMENT(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->__PROGRAM_FRAGMENT:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_STORE(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->__PROGRAM_STORE:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->ALLOCATION(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->__ALLOCATION:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->MESH(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->__MESH:Landroid/renderscript/Element;

    return-void
.end method


# virtual methods
.method public bind_Blades(Lcom/android/wallpaper/grass/ScriptField_Blade;)V
    .locals 2
    .param p1    # Lcom/android/wallpaper/grass/ScriptField_Blade;

    const/16 v1, 0x14

    iput-object p1, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_Blades:Lcom/android/wallpaper/grass/ScriptField_Blade;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/renderscript/Script;->bindAllocation(Landroid/renderscript/Allocation;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/renderscript/Script$FieldBase;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Landroid/renderscript/Script;->bindAllocation(Landroid/renderscript/Allocation;I)V

    goto :goto_0
.end method

.method public bind_Verticies(Lcom/android/wallpaper/grass/ScriptField_Vertex;)V
    .locals 2
    .param p1    # Lcom/android/wallpaper/grass/ScriptField_Vertex;

    const/16 v1, 0x15

    iput-object p1, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_Verticies:Lcom/android/wallpaper/grass/ScriptField_Vertex;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/renderscript/Script;->bindAllocation(Landroid/renderscript/Allocation;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/renderscript/Script$FieldBase;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Landroid/renderscript/Script;->bindAllocation(Landroid/renderscript/Allocation;I)V

    goto :goto_0
.end method

.method public get_Blades()Lcom/android/wallpaper/grass/ScriptField_Blade;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_Blades:Lcom/android/wallpaper/grass/ScriptField_Blade;

    return-object v0
.end method

.method public get_Verticies()Lcom/android/wallpaper/grass/ScriptField_Vertex;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_Verticies:Lcom/android/wallpaper/grass/ScriptField_Vertex;

    return-object v0
.end method

.method public get_gAfternoon()F
    .locals 1

    iget v0, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_gAfternoon:F

    return v0
.end method

.method public get_gBladesCount()I
    .locals 1

    iget v0, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_gBladesCount:I

    return v0
.end method

.method public get_gBladesMesh()Landroid/renderscript/Mesh;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_gBladesMesh:Landroid/renderscript/Mesh;

    return-object v0
.end method

.method public get_gDawn()F
    .locals 1

    iget v0, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_gDawn:F

    return v0
.end method

.method public get_gDusk()F
    .locals 1

    iget v0, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_gDusk:F

    return v0
.end method

.method public get_gHeight()I
    .locals 1

    iget v0, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_gHeight:I

    return v0
.end method

.method public get_gIndexCount()I
    .locals 1

    iget v0, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_gIndexCount:I

    return v0
.end method

.method public get_gIsPreview()I
    .locals 1

    iget v0, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_gIsPreview:I

    return v0
.end method

.method public get_gMorning()F
    .locals 1

    iget v0, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_gMorning:F

    return v0
.end method

.method public get_gPFBackground()Landroid/renderscript/ProgramFragment;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_gPFBackground:Landroid/renderscript/ProgramFragment;

    return-object v0
.end method

.method public get_gPFGrass()Landroid/renderscript/ProgramFragment;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_gPFGrass:Landroid/renderscript/ProgramFragment;

    return-object v0
.end method

.method public get_gPSBackground()Landroid/renderscript/ProgramStore;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_gPSBackground:Landroid/renderscript/ProgramStore;

    return-object v0
.end method

.method public get_gPVBackground()Landroid/renderscript/ProgramVertex;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_gPVBackground:Landroid/renderscript/ProgramVertex;

    return-object v0
.end method

.method public get_gTAa()Landroid/renderscript/Allocation;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_gTAa:Landroid/renderscript/Allocation;

    return-object v0
.end method

.method public get_gTNight()Landroid/renderscript/Allocation;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_gTNight:Landroid/renderscript/Allocation;

    return-object v0
.end method

.method public get_gTSky()Landroid/renderscript/Allocation;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_gTSky:Landroid/renderscript/Allocation;

    return-object v0
.end method

.method public get_gTSunrise()Landroid/renderscript/Allocation;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_gTSunrise:Landroid/renderscript/Allocation;

    return-object v0
.end method

.method public get_gTSunset()Landroid/renderscript/Allocation;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_gTSunset:Landroid/renderscript/Allocation;

    return-object v0
.end method

.method public get_gWidth()I
    .locals 1

    iget v0, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_gWidth:I

    return v0
.end method

.method public get_gXOffset()F
    .locals 1

    iget v0, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_gXOffset:F

    return v0
.end method

.method public invoke_updateBlades()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/renderscript/Script;->invoke(I)V

    return-void
.end method

.method public set_gAfternoon(F)V
    .locals 1
    .param p1    # F

    iput p1, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_gAfternoon:F

    const/4 v0, 0x7

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(IF)V

    return-void
.end method

.method public set_gBladesCount(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_gBladesCount:I

    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(II)V

    return-void
.end method

.method public set_gBladesMesh(Landroid/renderscript/Mesh;)V
    .locals 1
    .param p1    # Landroid/renderscript/Mesh;

    iput-object p1, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_gBladesMesh:Landroid/renderscript/Mesh;

    const/16 v0, 0x13

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gDawn(F)V
    .locals 1
    .param p1    # F

    iput p1, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_gDawn:F

    const/4 v0, 0x5

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(IF)V

    return-void
.end method

.method public set_gDusk(F)V
    .locals 1
    .param p1    # F

    iput p1, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_gDusk:F

    const/16 v0, 0x8

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(IF)V

    return-void
.end method

.method public set_gHeight(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_gHeight:I

    const/4 v0, 0x3

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(II)V

    return-void
.end method

.method public set_gIndexCount(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_gIndexCount:I

    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(II)V

    return-void
.end method

.method public set_gIsPreview(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_gIsPreview:I

    const/16 v0, 0x9

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(II)V

    return-void
.end method

.method public set_gMorning(F)V
    .locals 1
    .param p1    # F

    iput p1, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_gMorning:F

    const/4 v0, 0x6

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(IF)V

    return-void
.end method

.method public set_gPFBackground(Landroid/renderscript/ProgramFragment;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramFragment;

    iput-object p1, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_gPFBackground:Landroid/renderscript/ProgramFragment;

    const/16 v0, 0xb

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gPFGrass(Landroid/renderscript/ProgramFragment;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramFragment;

    iput-object p1, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_gPFGrass:Landroid/renderscript/ProgramFragment;

    const/16 v0, 0xc

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gPSBackground(Landroid/renderscript/ProgramStore;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramStore;

    iput-object p1, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_gPSBackground:Landroid/renderscript/ProgramStore;

    const/16 v0, 0xd

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gPVBackground(Landroid/renderscript/ProgramVertex;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramVertex;

    iput-object p1, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_gPVBackground:Landroid/renderscript/ProgramVertex;

    const/16 v0, 0xa

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gTAa(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    iput-object p1, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_gTAa:Landroid/renderscript/Allocation;

    const/16 v0, 0x12

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gTNight(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    iput-object p1, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_gTNight:Landroid/renderscript/Allocation;

    const/16 v0, 0xe

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gTSky(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    iput-object p1, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_gTSky:Landroid/renderscript/Allocation;

    const/16 v0, 0x11

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gTSunrise(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    iput-object p1, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_gTSunrise:Landroid/renderscript/Allocation;

    const/16 v0, 0x10

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gTSunset(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    iput-object p1, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_gTSunset:Landroid/renderscript/Allocation;

    const/16 v0, 0xf

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gWidth(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_gWidth:I

    const/4 v0, 0x2

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(II)V

    return-void
.end method

.method public set_gXOffset(F)V
    .locals 1
    .param p1    # F

    iput p1, p0, Lcom/android/wallpaper/grass/ScriptC_grass;->mExportVar_gXOffset:F

    const/4 v0, 0x4

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(IF)V

    return-void
.end method
