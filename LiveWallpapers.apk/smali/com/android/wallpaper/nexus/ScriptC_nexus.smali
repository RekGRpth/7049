.class public Lcom/android/wallpaper/nexus/ScriptC_nexus;
.super Landroid/renderscript/ScriptC;
.source "ScriptC_nexus.java"


# static fields
.field private static final mExportFuncIdx_addTap:I = 0x1

.field private static final mExportFuncIdx_initPulses:I = 0x0

.field private static final mExportVarIdx_gIsPreview:I = 0x1

.field private static final mExportVarIdx_gMode:I = 0x2

.field private static final mExportVarIdx_gPFTexture:I = 0x3

.field private static final mExportVarIdx_gPFTexture565:I = 0x5

.field private static final mExportVarIdx_gPSBlend:I = 0x4

.field private static final mExportVarIdx_gTBackground:I = 0x6

.field private static final mExportVarIdx_gTGlow:I = 0x8

.field private static final mExportVarIdx_gTPulse:I = 0x7

.field private static final mExportVarIdx_gXOffset:I


# instance fields
.field private __ALLOCATION:Landroid/renderscript/Element;

.field private __F32:Landroid/renderscript/Element;

.field private __I32:Landroid/renderscript/Element;

.field private __PROGRAM_FRAGMENT:Landroid/renderscript/Element;

.field private __PROGRAM_STORE:Landroid/renderscript/Element;

.field private mExportVar_gIsPreview:I

.field private mExportVar_gMode:I

.field private mExportVar_gPFTexture:Landroid/renderscript/ProgramFragment;

.field private mExportVar_gPFTexture565:Landroid/renderscript/ProgramFragment;

.field private mExportVar_gPSBlend:Landroid/renderscript/ProgramStore;

.field private mExportVar_gTBackground:Landroid/renderscript/Allocation;

.field private mExportVar_gTGlow:Landroid/renderscript/Allocation;

.field private mExportVar_gTPulse:Landroid/renderscript/Allocation;

.field private mExportVar_gXOffset:F


# direct methods
.method public constructor <init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V
    .locals 1
    .param p1    # Landroid/renderscript/RenderScript;
    .param p2    # Landroid/content/res/Resources;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/renderscript/ScriptC;-><init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V

    invoke-static {p1}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/nexus/ScriptC_nexus;->__F32:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->I32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/nexus/ScriptC_nexus;->__I32:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_FRAGMENT(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/nexus/ScriptC_nexus;->__PROGRAM_FRAGMENT:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_STORE(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/nexus/ScriptC_nexus;->__PROGRAM_STORE:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->ALLOCATION(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/nexus/ScriptC_nexus;->__ALLOCATION:Landroid/renderscript/Element;

    return-void
.end method


# virtual methods
.method public get_gIsPreview()I
    .locals 1

    iget v0, p0, Lcom/android/wallpaper/nexus/ScriptC_nexus;->mExportVar_gIsPreview:I

    return v0
.end method

.method public get_gMode()I
    .locals 1

    iget v0, p0, Lcom/android/wallpaper/nexus/ScriptC_nexus;->mExportVar_gMode:I

    return v0
.end method

.method public get_gPFTexture()Landroid/renderscript/ProgramFragment;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/nexus/ScriptC_nexus;->mExportVar_gPFTexture:Landroid/renderscript/ProgramFragment;

    return-object v0
.end method

.method public get_gPFTexture565()Landroid/renderscript/ProgramFragment;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/nexus/ScriptC_nexus;->mExportVar_gPFTexture565:Landroid/renderscript/ProgramFragment;

    return-object v0
.end method

.method public get_gPSBlend()Landroid/renderscript/ProgramStore;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/nexus/ScriptC_nexus;->mExportVar_gPSBlend:Landroid/renderscript/ProgramStore;

    return-object v0
.end method

.method public get_gTBackground()Landroid/renderscript/Allocation;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/nexus/ScriptC_nexus;->mExportVar_gTBackground:Landroid/renderscript/Allocation;

    return-object v0
.end method

.method public get_gTGlow()Landroid/renderscript/Allocation;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/nexus/ScriptC_nexus;->mExportVar_gTGlow:Landroid/renderscript/Allocation;

    return-object v0
.end method

.method public get_gTPulse()Landroid/renderscript/Allocation;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/nexus/ScriptC_nexus;->mExportVar_gTPulse:Landroid/renderscript/Allocation;

    return-object v0
.end method

.method public get_gXOffset()F
    .locals 1

    iget v0, p0, Lcom/android/wallpaper/nexus/ScriptC_nexus;->mExportVar_gXOffset:F

    return v0
.end method

.method public invoke_addTap(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    new-instance v0, Landroid/renderscript/FieldPacker;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Landroid/renderscript/FieldPacker;-><init>(I)V

    invoke-virtual {v0, p1}, Landroid/renderscript/FieldPacker;->addI32(I)V

    invoke-virtual {v0, p2}, Landroid/renderscript/FieldPacker;->addI32(I)V

    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, Landroid/renderscript/Script;->invoke(ILandroid/renderscript/FieldPacker;)V

    return-void
.end method

.method public invoke_initPulses()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/renderscript/Script;->invoke(I)V

    return-void
.end method

.method public set_gIsPreview(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/android/wallpaper/nexus/ScriptC_nexus;->mExportVar_gIsPreview:I

    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(II)V

    return-void
.end method

.method public set_gMode(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/android/wallpaper/nexus/ScriptC_nexus;->mExportVar_gMode:I

    const/4 v0, 0x2

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(II)V

    return-void
.end method

.method public set_gPFTexture(Landroid/renderscript/ProgramFragment;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramFragment;

    iput-object p1, p0, Lcom/android/wallpaper/nexus/ScriptC_nexus;->mExportVar_gPFTexture:Landroid/renderscript/ProgramFragment;

    const/4 v0, 0x3

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gPFTexture565(Landroid/renderscript/ProgramFragment;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramFragment;

    iput-object p1, p0, Lcom/android/wallpaper/nexus/ScriptC_nexus;->mExportVar_gPFTexture565:Landroid/renderscript/ProgramFragment;

    const/4 v0, 0x5

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gPSBlend(Landroid/renderscript/ProgramStore;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramStore;

    iput-object p1, p0, Lcom/android/wallpaper/nexus/ScriptC_nexus;->mExportVar_gPSBlend:Landroid/renderscript/ProgramStore;

    const/4 v0, 0x4

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gTBackground(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    iput-object p1, p0, Lcom/android/wallpaper/nexus/ScriptC_nexus;->mExportVar_gTBackground:Landroid/renderscript/Allocation;

    const/4 v0, 0x6

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gTGlow(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    iput-object p1, p0, Lcom/android/wallpaper/nexus/ScriptC_nexus;->mExportVar_gTGlow:Landroid/renderscript/Allocation;

    const/16 v0, 0x8

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gTPulse(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    iput-object p1, p0, Lcom/android/wallpaper/nexus/ScriptC_nexus;->mExportVar_gTPulse:Landroid/renderscript/Allocation;

    const/4 v0, 0x7

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gXOffset(F)V
    .locals 1
    .param p1    # F

    iput p1, p0, Lcom/android/wallpaper/nexus/ScriptC_nexus;->mExportVar_gXOffset:F

    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(IF)V

    return-void
.end method
