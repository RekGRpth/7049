.class Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;
.super Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockPalette;
.source "PolarClockWallpaper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/wallpaper/polarclock/PolarClockWallpaper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "CyclingClockPalette"
.end annotation


# static fields
.field private static final COLORS_CACHE_COUNT:I = 0x2d0

.field private static sFallbackPalette:Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;


# instance fields
.field protected mBackgroundColor:I

.field protected mBrightness:F

.field private final mColors:[I

.field protected mId:Ljava/lang/String;

.field protected mSaturation:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;->sFallbackPalette:Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockPalette;-><init>()V

    const/16 v0, 0x2d0

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;->mColors:[I

    return-void
.end method

.method private computeIntermediateColors()V
    .locals 7

    iget-object v0, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;->mColors:[I

    array-length v1, v0

    const v3, 0x3ab60b61

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    int-to-float v4, v2

    mul-float/2addr v4, v3

    iget v5, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;->mSaturation:F

    iget v6, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;->mBrightness:F

    invoke-static {v4, v5, v6}, Landroid/graphics/Color;->HSBtoColor(FFF)I

    move-result v4

    aput v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static getFallback()Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;
    .locals 2

    sget-object v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;->sFallbackPalette:Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;

    invoke-direct {v0}, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;-><init>()V

    sput-object v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;->sFallbackPalette:Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;

    sget-object v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;->sFallbackPalette:Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;

    const-string v1, "default_c"

    iput-object v1, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;->mId:Ljava/lang/String;

    sget-object v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;->sFallbackPalette:Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;

    const/4 v1, -0x1

    iput v1, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;->mBackgroundColor:I

    sget-object v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;->sFallbackPalette:Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;

    const v1, 0x3f4ccccd

    iput v1, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;->mSaturation:F

    sget-object v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;->sFallbackPalette:Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;

    const v1, 0x3f666666

    iput v1, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;->mBrightness:F

    sget-object v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;->sFallbackPalette:Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;

    invoke-direct {v0}, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;->computeIntermediateColors()V

    :cond_0
    sget-object v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;->sFallbackPalette:Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;

    return-object v0
.end method

.method public static parseXmlPaletteTag(Landroid/content/res/XmlResourceParser;)Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockPalette;
    .locals 4
    .param p0    # Landroid/content/res/XmlResourceParser;

    const/4 v2, 0x0

    new-instance v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;

    invoke-direct {v0}, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;-><init>()V

    const-string v3, "id"

    invoke-interface {p0, v2, v3}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;->mId:Ljava/lang/String;

    const-string v3, "background"

    invoke-interface {p0, v2, v3}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    iput v3, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;->mBackgroundColor:I

    :cond_0
    const-string v3, "saturation"

    invoke-interface {p0, v2, v3}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    iput v3, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;->mSaturation:F

    :cond_1
    const-string v3, "brightness"

    invoke-interface {p0, v2, v3}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    iput v3, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;->mBrightness:F

    :cond_2
    iget-object v3, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;->mId:Ljava/lang/String;

    if-nez v3, :cond_3

    move-object v0, v2

    :goto_0
    return-object v0

    :cond_3
    invoke-direct {v0}, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;->computeIntermediateColors()V

    goto :goto_0
.end method


# virtual methods
.method public getBackgroundColor()I
    .locals 1

    iget v0, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;->mBackgroundColor:I

    return v0
.end method

.method public getDayColor(F)I
    .locals 2
    .param p1    # F

    const/high16 v0, 0x3f800000

    cmpl-float v0, p1, v0

    if-gez v0, :cond_0

    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_1

    :cond_0
    const/4 p1, 0x0

    :cond_1
    iget-object v0, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;->mColors:[I

    const/high16 v1, 0x44340000

    mul-float/2addr v1, p1

    float-to-int v1, v1

    aget v0, v0, v1

    return v0
.end method

.method public getHourColor(F)I
    .locals 2
    .param p1    # F

    const/high16 v0, 0x3f800000

    cmpl-float v0, p1, v0

    if-gez v0, :cond_0

    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_1

    :cond_0
    const/4 p1, 0x0

    :cond_1
    iget-object v0, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;->mColors:[I

    const/high16 v1, 0x44340000

    mul-float/2addr v1, p1

    float-to-int v1, v1

    aget v0, v0, v1

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public getMinuteColor(F)I
    .locals 2
    .param p1    # F

    const/high16 v0, 0x3f800000

    cmpl-float v0, p1, v0

    if-gez v0, :cond_0

    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_1

    :cond_0
    const/4 p1, 0x0

    :cond_1
    iget-object v0, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;->mColors:[I

    const/high16 v1, 0x44340000

    mul-float/2addr v1, p1

    float-to-int v1, v1

    aget v0, v0, v1

    return v0
.end method

.method public getMonthColor(F)I
    .locals 2
    .param p1    # F

    const/high16 v0, 0x3f800000

    cmpl-float v0, p1, v0

    if-gez v0, :cond_0

    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_1

    :cond_0
    const/4 p1, 0x0

    :cond_1
    iget-object v0, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;->mColors:[I

    const/high16 v1, 0x44340000

    mul-float/2addr v1, p1

    float-to-int v1, v1

    aget v0, v0, v1

    return v0
.end method

.method public getSecondColor(F)I
    .locals 2
    .param p1    # F

    const/high16 v0, 0x3f800000

    cmpl-float v0, p1, v0

    if-gez v0, :cond_0

    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_1

    :cond_0
    const/4 p1, 0x0

    :cond_1
    iget-object v0, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;->mColors:[I

    const/high16 v1, 0x44340000

    mul-float/2addr v1, p1

    float-to-int v1, v1

    aget v0, v0, v1

    return v0
.end method
