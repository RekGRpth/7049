.class Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;
.super Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockPalette;
.source "PolarClockWallpaper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/wallpaper/polarclock/PolarClockWallpaper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "FixedClockPalette"
.end annotation


# static fields
.field private static sFallbackPalette:Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;


# instance fields
.field protected mBackgroundColor:I

.field protected mDayColor:I

.field protected mHourColor:I

.field protected mId:Ljava/lang/String;

.field protected mMinuteColor:I

.field protected mMonthColor:I

.field protected mSecondColor:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;->sFallbackPalette:Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockPalette;-><init>()V

    return-void
.end method

.method public static getFallback()Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;
    .locals 6

    sget-object v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;->sFallbackPalette:Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;

    invoke-direct {v0}, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;-><init>()V

    sput-object v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;->sFallbackPalette:Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;

    sget-object v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;->sFallbackPalette:Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;

    const-string v1, "default"

    iput-object v1, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;->mId:Ljava/lang/String;

    sget-object v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;->sFallbackPalette:Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;

    const/4 v1, -0x1

    iput v1, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;->mBackgroundColor:I

    sget-object v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;->sFallbackPalette:Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;

    sget-object v1, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;->sFallbackPalette:Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;

    sget-object v2, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;->sFallbackPalette:Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;

    sget-object v3, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;->sFallbackPalette:Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;

    sget-object v4, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;->sFallbackPalette:Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;

    const/high16 v5, -0x1000000

    iput v5, v4, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;->mMonthColor:I

    iput v5, v3, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;->mDayColor:I

    iput v5, v2, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;->mHourColor:I

    iput v5, v1, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;->mMinuteColor:I

    iput v5, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;->mSecondColor:I

    :cond_0
    sget-object v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;->sFallbackPalette:Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;

    return-object v0
.end method

.method public static parseXmlPaletteTag(Landroid/content/res/XmlResourceParser;)Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockPalette;
    .locals 4
    .param p0    # Landroid/content/res/XmlResourceParser;

    const/4 v2, 0x0

    new-instance v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;

    invoke-direct {v0}, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;-><init>()V

    const-string v3, "id"

    invoke-interface {p0, v2, v3}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;->mId:Ljava/lang/String;

    const-string v3, "background"

    invoke-interface {p0, v2, v3}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    iput v3, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;->mBackgroundColor:I

    :cond_0
    const-string v3, "second"

    invoke-interface {p0, v2, v3}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    iput v3, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;->mSecondColor:I

    :cond_1
    const-string v3, "minute"

    invoke-interface {p0, v2, v3}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    iput v3, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;->mMinuteColor:I

    :cond_2
    const-string v3, "hour"

    invoke-interface {p0, v2, v3}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    iput v3, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;->mHourColor:I

    :cond_3
    const-string v3, "day"

    invoke-interface {p0, v2, v3}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    iput v3, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;->mDayColor:I

    :cond_4
    const-string v3, "month"

    invoke-interface {p0, v2, v3}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    iput v3, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;->mMonthColor:I

    :cond_5
    iget-object v3, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;->mId:Ljava/lang/String;

    if-nez v3, :cond_6

    move-object v0, v2

    :cond_6
    return-object v0
.end method


# virtual methods
.method public getBackgroundColor()I
    .locals 1

    iget v0, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;->mBackgroundColor:I

    return v0
.end method

.method public getDayColor(F)I
    .locals 1
    .param p1    # F

    iget v0, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;->mDayColor:I

    return v0
.end method

.method public getHourColor(F)I
    .locals 1
    .param p1    # F

    iget v0, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;->mHourColor:I

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public getMinuteColor(F)I
    .locals 1
    .param p1    # F

    iget v0, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;->mMinuteColor:I

    return v0
.end method

.method public getMonthColor(F)I
    .locals 1
    .param p1    # F

    iget v0, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;->mMonthColor:I

    return v0
.end method

.method public getSecondColor(F)I
    .locals 1
    .param p1    # F

    iget v0, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;->mSecondColor:I

    return v0
.end method
