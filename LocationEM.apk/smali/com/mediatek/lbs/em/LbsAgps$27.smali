.class Lcom/mediatek/lbs/em/LbsAgps$27;
.super Ljava/lang/Object;
.source "LbsAgps.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/lbs/em/LbsAgps;->initWidget()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/lbs/em/LbsAgps;


# direct methods
.method constructor <init>(Lcom/mediatek/lbs/em/LbsAgps;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/lbs/em/LbsAgps$27;->this$0:Lcom/mediatek/lbs/em/LbsAgps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps$27;->this$0:Lcom/mediatek/lbs/em/LbsAgps;

    # getter for: Lcom/mediatek/lbs/em/LbsAgps;->mIgnoreFirstNiDialogSpinner:Z
    invoke-static {v1}, Lcom/mediatek/lbs/em/LbsAgps;->access$2100(Lcom/mediatek/lbs/em/LbsAgps;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps$27;->this$0:Lcom/mediatek/lbs/em/LbsAgps;

    const/4 v2, 0x0

    # setter for: Lcom/mediatek/lbs/em/LbsAgps;->mIgnoreFirstNiDialogSpinner:Z
    invoke-static {v1, v2}, Lcom/mediatek/lbs/em/LbsAgps;->access$2102(Lcom/mediatek/lbs/em/LbsAgps;Z)Z

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps$27;->this$0:Lcom/mediatek/lbs/em/LbsAgps;

    # getter for: Lcom/mediatek/lbs/em/LbsAgps;->mSpinnerNiDialogTest:Landroid/widget/Spinner;
    invoke-static {v1}, Lcom/mediatek/lbs/em/LbsAgps;->access$2200(Lcom/mediatek/lbs/em/LbsAgps;)Landroid/widget/Spinner;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItemId()J

    move-result-wide v1

    long-to-int v0, v1

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps$27;->this$0:Lcom/mediatek/lbs/em/LbsAgps;

    # invokes: Lcom/mediatek/lbs/em/LbsAgps;->handleNiDialogTest(I)V
    invoke-static {v1, v0}, Lcom/mediatek/lbs/em/LbsAgps;->access$2300(Lcom/mediatek/lbs/em/LbsAgps;I)V

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    return-void
.end method
