.class public Lcom/mediatek/lbs/em/LbsAgps;
.super Landroid/app/Activity;
.source "LbsAgps.java"


# instance fields
.field private final NI_DIALOG_TEST_LIST:[Ljava/lang/String;

.field private final TIMEOUT_LIST:[Ljava/lang/String;

.field private mAccUnitFlag:I

.field private mAgpsMgr:Lcom/mediatek/common/agps/MtkAgpsManager;

.field private mButtonDelay:Landroid/widget/Button;

.field private mButtonHAcc:Landroid/widget/Button;

.field private mButtonLoadProfile:Landroid/widget/Button;

.field private mButtonLocationAge:Landroid/widget/Button;

.field private mButtonReset:Landroid/widget/Button;

.field private mButtonReset2Default:Landroid/widget/Button;

.field private mButtonSetProfile:Landroid/widget/Button;

.field private mButtonSlpAddr:Landroid/widget/Button;

.field private mButtonSlpPort:Landroid/widget/Button;

.field private mButtonTest1:Landroid/widget/Button;

.field private mButtonTest2:Landroid/widget/Button;

.field private mButtonTest3:Landroid/widget/Button;

.field private mButtonTest4:Landroid/widget/Button;

.field private mButtonTest5:Landroid/widget/Button;

.field private mButtonTest6:Landroid/widget/Button;

.field private mButtonTest7:Landroid/widget/Button;

.field private mButtonTest8:Landroid/widget/Button;

.field private mButtonVAcc:Landroid/widget/Button;

.field private mCheckBoxCerVerify:Landroid/widget/CheckBox;

.field private mCheckBoxEcidEnable:Landroid/widget/CheckBox;

.field private mCheckBoxEmAllow:Landroid/widget/CheckBox;

.field private mCheckBoxEnableAgps:Landroid/widget/CheckBox;

.field private mCheckBoxEnableNiTimer:Landroid/widget/CheckBox;

.field private mCheckBoxExternalAddr:Landroid/widget/CheckBox;

.field private mCheckBoxLabPerformance:Landroid/widget/CheckBox;

.field private mCheckBoxMlcNumber:Landroid/widget/CheckBox;

.field private mCheckBoxNiAllow:Landroid/widget/CheckBox;

.field private mCheckBoxRoamingAllow:Landroid/widget/CheckBox;

.field private mCheckBoxSupl2File:Landroid/widget/CheckBox;

.field private mCheckBoxTls:Landroid/widget/CheckBox;

.field private mCheckEnableIot:Landroid/widget/CheckBox;

.field private mCurrentProfile:Lcom/mediatek/common/agps/MtkAgpsProfile;

.field private mIgnoreFirstNiDialogSpinner:Z

.field private mIgnoreFirstNotifySpinner:Z

.field private mIgnoreFirstSlpSpinner:Z

.field private mIgnoreFirstVerifySpinner:Z

.field private mProfileManager:Lcom/mediatek/common/agps/MtkAgpsProfileManager;

.field private mRadioButtonAssistanceData:Landroid/widget/RadioButton;

.field private mRadioButtonCp:Landroid/widget/RadioButton;

.field private mRadioButtonImsi:Landroid/widget/RadioButton;

.field private mRadioButtonIpv4:Landroid/widget/RadioButton;

.field private mRadioButtonKValue:Landroid/widget/RadioButton;

.field private mRadioButtonLocationEstimate:Landroid/widget/RadioButton;

.field private mRadioButtonMeter:Landroid/widget/RadioButton;

.field private mRadioButtonMsa:Landroid/widget/RadioButton;

.field private mRadioButtonMsb:Landroid/widget/RadioButton;

.field private mRadioButtonPreferSim1:Landroid/widget/RadioButton;

.field private mRadioButtonPreferSim2:Landroid/widget/RadioButton;

.field private mRadioButtonRrc:Landroid/widget/RadioButton;

.field private mRadioButtonRrlp:Landroid/widget/RadioButton;

.field private mRadioButtonRrlpRrc:Landroid/widget/RadioButton;

.field private mRadioButtonStandalone:Landroid/widget/RadioButton;

.field private mRadioButtonUp:Landroid/widget/RadioButton;

.field private mSlpPopup:Landroid/widget/PopupMenu;

.field private mSpinnerNiDialogTest:Landroid/widget/Spinner;

.field private mSpinnerNotificationTimeout:Landroid/widget/Spinner;

.field private mSpinnerVerificationTimeout:Landroid/widget/Spinner;

.field private mTextViewDelay:Landroid/widget/TextView;

.field private mTextViewExternalAddr:Landroid/widget/TextView;

.field private mTextViewHAcc:Landroid/widget/TextView;

.field private mTextViewLocationAge:Landroid/widget/TextView;

.field private mTextViewMlcNumber:Landroid/widget/TextView;

.field private mTextViewSlpAddr:Landroid/widget/TextView;

.field private mTextViewSlpPort:Landroid/widget/TextView;

.field private mTextViewVAcc:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "0"

    aput-object v1, v0, v4

    const-string v1, "1"

    aput-object v1, v0, v3

    const-string v1, "2"

    aput-object v1, v0, v5

    const-string v1, "3"

    aput-object v1, v0, v6

    const-string v1, "4"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "5"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "6"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "7"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "8"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->TIMEOUT_LIST:[Ljava/lang/String;

    const/16 v0, 0x14

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "EM - SI request received"

    aput-object v1, v0, v4

    const-string v1, "EM - Position Fixed"

    aput-object v1, v0, v3

    const-string v1, "NOTIFY - Notification only"

    aput-object v1, v0, v5

    const-string v1, "NOTIFY - Allow no answer"

    aput-object v1, v0, v6

    const-string v1, "NOTIFY - Deny no answer"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "ERR - Network create fail"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "ERR - Incorrect push content"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "ERR - Unsupport operation"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "ERR - Request not accepted"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "ERR - No Resource to handle new process"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "ERR - Network connection is down"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "ERR - Remote side abort the session"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "ERR - Timer expiry"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "ERR - Receive incorrect message context"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "ERR - User agree on confirmation"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "ERR - User deny on confirmation"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "ERR - Only for no position"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "ERR - TLS authentication fail"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "ERR - waiting verification timeout"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "ERR - Modem reset happens"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->NI_DIALOG_TEST_LIST:[Ljava/lang/String;

    iput-boolean v3, p0, Lcom/mediatek/lbs/em/LbsAgps;->mIgnoreFirstSlpSpinner:Z

    iput-boolean v3, p0, Lcom/mediatek/lbs/em/LbsAgps;->mIgnoreFirstNiDialogSpinner:Z

    iput-boolean v3, p0, Lcom/mediatek/lbs/em/LbsAgps;->mIgnoreFirstNotifySpinner:Z

    iput-boolean v3, p0, Lcom/mediatek/lbs/em/LbsAgps;->mIgnoreFirstVerifySpinner:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mProfileManager:Lcom/mediatek/common/agps/MtkAgpsProfileManager;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mAgpsMgr:Lcom/mediatek/common/agps/MtkAgpsManager;

    iput v4, p0, Lcom/mediatek/lbs/em/LbsAgps;->mAccUnitFlag:I

    new-instance v0, Lcom/mediatek/common/agps/MtkAgpsProfile;

    invoke-direct {v0}, Lcom/mediatek/common/agps/MtkAgpsProfile;-><init>()V

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCurrentProfile:Lcom/mediatek/common/agps/MtkAgpsProfile;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/lbs/em/LbsAgps;)Lcom/mediatek/common/agps/MtkAgpsProfileManager;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mProfileManager:Lcom/mediatek/common/agps/MtkAgpsProfileManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/lbs/em/LbsAgps;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewSlpAddr:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/mediatek/lbs/em/LbsAgps;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxNiAllow:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/mediatek/lbs/em/LbsAgps;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxEmAllow:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/mediatek/lbs/em/LbsAgps;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxRoamingAllow:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/mediatek/lbs/em/LbsAgps;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxSupl2File:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/mediatek/lbs/em/LbsAgps;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxEcidEnable:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/mediatek/lbs/em/LbsAgps;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxLabPerformance:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/mediatek/lbs/em/LbsAgps;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxEnableNiTimer:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/mediatek/lbs/em/LbsAgps;)Z
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    iget-boolean v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mIgnoreFirstNotifySpinner:Z

    return v0
.end method

.method static synthetic access$1702(Lcom/mediatek/lbs/em/LbsAgps;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mIgnoreFirstNotifySpinner:Z

    return p1
.end method

.method static synthetic access$1800(Lcom/mediatek/lbs/em/LbsAgps;)Landroid/widget/Spinner;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mSpinnerNotificationTimeout:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/mediatek/lbs/em/LbsAgps;)Z
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    iget-boolean v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mIgnoreFirstVerifySpinner:Z

    return v0
.end method

.method static synthetic access$1902(Lcom/mediatek/lbs/em/LbsAgps;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mIgnoreFirstVerifySpinner:Z

    return p1
.end method

.method static synthetic access$200(Lcom/mediatek/lbs/em/LbsAgps;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewSlpPort:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/mediatek/lbs/em/LbsAgps;)Landroid/widget/Spinner;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mSpinnerVerificationTimeout:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/mediatek/lbs/em/LbsAgps;)Z
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    iget-boolean v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mIgnoreFirstNiDialogSpinner:Z

    return v0
.end method

.method static synthetic access$2102(Lcom/mediatek/lbs/em/LbsAgps;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mIgnoreFirstNiDialogSpinner:Z

    return p1
.end method

.method static synthetic access$2200(Lcom/mediatek/lbs/em/LbsAgps;)Landroid/widget/Spinner;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mSpinnerNiDialogTest:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/mediatek/lbs/em/LbsAgps;I)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/lbs/em/LbsAgps;->handleNiDialogTest(I)V

    return-void
.end method

.method static synthetic access$2400(Lcom/mediatek/lbs/em/LbsAgps;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxExternalAddr:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/mediatek/lbs/em/LbsAgps;)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    invoke-direct {p0}, Lcom/mediatek/lbs/em/LbsAgps;->editExternalAddr()V

    return-void
.end method

.method static synthetic access$2600(Lcom/mediatek/lbs/em/LbsAgps;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxMlcNumber:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/mediatek/lbs/em/LbsAgps;)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    invoke-direct {p0}, Lcom/mediatek/lbs/em/LbsAgps;->editMlcNumber()V

    return-void
.end method

.method static synthetic access$2800(Lcom/mediatek/lbs/em/LbsAgps;)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    invoke-direct {p0}, Lcom/mediatek/lbs/em/LbsAgps;->editSlpAddr()V

    return-void
.end method

.method static synthetic access$2900(Lcom/mediatek/lbs/em/LbsAgps;)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    invoke-direct {p0}, Lcom/mediatek/lbs/em/LbsAgps;->editSlpPort()V

    return-void
.end method

.method static synthetic access$300(Lcom/mediatek/lbs/em/LbsAgps;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxTls:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/mediatek/lbs/em/LbsAgps;)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    invoke-direct {p0}, Lcom/mediatek/lbs/em/LbsAgps;->handleKValueAccuracyUnit()V

    return-void
.end method

.method static synthetic access$3100(Lcom/mediatek/lbs/em/LbsAgps;)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    invoke-direct {p0}, Lcom/mediatek/lbs/em/LbsAgps;->handleMeterAccuracyUnit()V

    return-void
.end method

.method static synthetic access$3200(Lcom/mediatek/lbs/em/LbsAgps;)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    invoke-direct {p0}, Lcom/mediatek/lbs/em/LbsAgps;->editQopHorizontalAcc()V

    return-void
.end method

.method static synthetic access$3300(Lcom/mediatek/lbs/em/LbsAgps;)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    invoke-direct {p0}, Lcom/mediatek/lbs/em/LbsAgps;->editQopVerticalAcc()V

    return-void
.end method

.method static synthetic access$3400(Lcom/mediatek/lbs/em/LbsAgps;)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    invoke-direct {p0}, Lcom/mediatek/lbs/em/LbsAgps;->editQopLocationAge()V

    return-void
.end method

.method static synthetic access$3500(Lcom/mediatek/lbs/em/LbsAgps;)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    invoke-direct {p0}, Lcom/mediatek/lbs/em/LbsAgps;->editQopDelay()V

    return-void
.end method

.method static synthetic access$3600(Lcom/mediatek/lbs/em/LbsAgps;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxCerVerify:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/mediatek/lbs/em/LbsAgps;)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    invoke-direct {p0}, Lcom/mediatek/lbs/em/LbsAgps;->loadConfigFromAgpsFramework()V

    return-void
.end method

.method static synthetic access$3800(Lcom/mediatek/lbs/em/LbsAgps;)I
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    iget v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mAccUnitFlag:I

    return v0
.end method

.method static synthetic access$3900(Lcom/mediatek/lbs/em/LbsAgps;I)I
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/lbs/em/LbsAgps;->calcKFromMeter(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/mediatek/lbs/em/LbsAgps;)Lcom/mediatek/common/agps/MtkAgpsManager;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mAgpsMgr:Lcom/mediatek/common/agps/MtkAgpsManager;

    return-object v0
.end method

.method static synthetic access$4000(Lcom/mediatek/lbs/em/LbsAgps;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewHAcc:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$4100(Lcom/mediatek/lbs/em/LbsAgps;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewVAcc:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$4200(Lcom/mediatek/lbs/em/LbsAgps;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewLocationAge:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$4300(Lcom/mediatek/lbs/em/LbsAgps;)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    invoke-direct {p0}, Lcom/mediatek/lbs/em/LbsAgps;->showSpecialWidget()V

    return-void
.end method

.method static synthetic access$4400(Lcom/mediatek/lbs/em/LbsAgps;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewDelay:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$4500(Lcom/mediatek/lbs/em/LbsAgps;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewExternalAddr:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$4600(Lcom/mediatek/lbs/em/LbsAgps;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewMlcNumber:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$502(Lcom/mediatek/lbs/em/LbsAgps;Lcom/mediatek/common/agps/MtkAgpsProfile;)Lcom/mediatek/common/agps/MtkAgpsProfile;
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;
    .param p1    # Lcom/mediatek/common/agps/MtkAgpsProfile;

    iput-object p1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCurrentProfile:Lcom/mediatek/common/agps/MtkAgpsProfile;

    return-object p1
.end method

.method static synthetic access$600(Lcom/mediatek/lbs/em/LbsAgps;)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    invoke-direct {p0}, Lcom/mediatek/lbs/em/LbsAgps;->handleLoadProfile()V

    return-void
.end method

.method static synthetic access$700(Lcom/mediatek/lbs/em/LbsAgps;)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    invoke-direct {p0}, Lcom/mediatek/lbs/em/LbsAgps;->handleSetProfile()V

    return-void
.end method

.method static synthetic access$800(Lcom/mediatek/lbs/em/LbsAgps;)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    invoke-direct {p0}, Lcom/mediatek/lbs/em/LbsAgps;->handleEnableAgps()V

    return-void
.end method

.method static synthetic access$900(Lcom/mediatek/lbs/em/LbsAgps;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsAgps;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckEnableIot:Landroid/widget/CheckBox;

    return-object v0
.end method

.method private calcKFromMeter(I)I
    .locals 6
    .param p1    # I

    int-to-double v2, p1

    const-wide/high16 v4, 0x4024000000000000L

    div-double/2addr v2, v4

    const-wide/high16 v4, 0x3ff0000000000000L

    add-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    const-wide v4, 0x3ff199999999999aL

    invoke-static {v4, v5}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    div-double v0, v2, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v2, v2

    return v2
.end method

.method private calcMeterFromK(I)I
    .locals 6
    .param p1    # I

    const/4 v2, 0x1

    if-ne p1, v2, :cond_0

    :goto_0
    return v2

    :cond_0
    const-wide v2, 0x3ff199999999999aL

    int-to-double v4, p1

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    const-wide/high16 v4, 0x3ff0000000000000L

    sub-double/2addr v2, v4

    const-wide/high16 v4, 0x4024000000000000L

    mul-double v0, v2, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    goto :goto_0
.end method

.method private editExternalAddr()V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v4, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxExternalAddr:Landroid/widget/CheckBox;

    invoke-virtual {v4}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    if-ne v4, v6, :cond_0

    iget-object v4, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewExternalAddr:Landroid/widget/TextView;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setEnabled(Z)V

    new-instance v2, Landroid/app/Dialog;

    invoke-direct {v2, p0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    const-string v4, "Edit External Address"

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    const v4, 0x7f030002

    invoke-virtual {v2, v4}, Landroid/app/Dialog;->setContentView(I)V

    invoke-virtual {v2}, Landroid/app/Dialog;->show()V

    const v4, 0x7f060044

    invoke-virtual {v2, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    iget-object v4, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewExternalAddr:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    const v4, 0x7f060045

    invoke-virtual {v2, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    const v4, 0x7f060046

    invoke-virtual {v2, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v4, Lcom/mediatek/lbs/em/LbsAgps$64;

    invoke-direct {v4, p0, v3, v2}, Lcom/mediatek/lbs/em/LbsAgps$64;-><init>(Lcom/mediatek/lbs/em/LbsAgps;Landroid/widget/EditText;Landroid/app/Dialog;)V

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v4, Lcom/mediatek/lbs/em/LbsAgps$65;

    invoke-direct {v4, p0, v2}, Lcom/mediatek/lbs/em/LbsAgps$65;-><init>(Lcom/mediatek/lbs/em/LbsAgps;Landroid/app/Dialog;)V

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    return-void

    :cond_0
    iget-object v4, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewExternalAddr:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0
.end method

.method private editMlcNumber()V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v4, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxMlcNumber:Landroid/widget/CheckBox;

    invoke-virtual {v4}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    if-ne v4, v6, :cond_0

    iget-object v4, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewMlcNumber:Landroid/widget/TextView;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setEnabled(Z)V

    new-instance v2, Landroid/app/Dialog;

    invoke-direct {v2, p0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    const-string v4, "Edit MLC Number"

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    const v4, 0x7f030002

    invoke-virtual {v2, v4}, Landroid/app/Dialog;->setContentView(I)V

    invoke-virtual {v2}, Landroid/app/Dialog;->show()V

    const v4, 0x7f060044

    invoke-virtual {v2, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    iget-object v4, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewMlcNumber:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    const v4, 0x7f060045

    invoke-virtual {v2, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    const v4, 0x7f060046

    invoke-virtual {v2, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v4, Lcom/mediatek/lbs/em/LbsAgps$66;

    invoke-direct {v4, p0, v3, v2}, Lcom/mediatek/lbs/em/LbsAgps$66;-><init>(Lcom/mediatek/lbs/em/LbsAgps;Landroid/widget/EditText;Landroid/app/Dialog;)V

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v4, Lcom/mediatek/lbs/em/LbsAgps$67;

    invoke-direct {v4, p0, v2}, Lcom/mediatek/lbs/em/LbsAgps$67;-><init>(Lcom/mediatek/lbs/em/LbsAgps;Landroid/app/Dialog;)V

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    return-void

    :cond_0
    iget-object v4, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewMlcNumber:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0
.end method

.method private editQopDelay()V
    .locals 8

    const/4 v7, 0x0

    new-instance v3, Landroid/app/Dialog;

    invoke-direct {v3, p0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    const-string v5, "Edit Delay"

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    const v5, 0x7f030002

    invoke-virtual {v3, v5}, Landroid/app/Dialog;->setContentView(I)V

    invoke-virtual {v3}, Landroid/app/Dialog;->show()V

    const v5, 0x7f060044

    invoke-virtual {v3, v5}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    const/4 v5, 0x1

    new-array v0, v5, [Landroid/text/InputFilter;

    new-instance v5, Landroid/text/InputFilter$LengthFilter;

    const/4 v6, 0x5

    invoke-direct {v5, v6}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v5, v0, v7

    invoke-virtual {v4, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setInputType(I)V

    iget-object v5, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewDelay:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    const v5, 0x7f060045

    invoke-virtual {v3, v5}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    const v5, 0x7f060046

    invoke-virtual {v3, v5}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    new-instance v5, Lcom/mediatek/lbs/em/LbsAgps$62;

    invoke-direct {v5, p0, v4, v3}, Lcom/mediatek/lbs/em/LbsAgps$62;-><init>(Lcom/mediatek/lbs/em/LbsAgps;Landroid/widget/EditText;Landroid/app/Dialog;)V

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v5, Lcom/mediatek/lbs/em/LbsAgps$63;

    invoke-direct {v5, p0, v3}, Lcom/mediatek/lbs/em/LbsAgps$63;-><init>(Lcom/mediatek/lbs/em/LbsAgps;Landroid/app/Dialog;)V

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private editQopHorizontalAcc()V
    .locals 8

    const/4 v7, 0x0

    new-instance v3, Landroid/app/Dialog;

    invoke-direct {v3, p0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    const-string v5, "Edit Horizontal Accuracy"

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    const v5, 0x7f030002

    invoke-virtual {v3, v5}, Landroid/app/Dialog;->setContentView(I)V

    invoke-virtual {v3}, Landroid/app/Dialog;->show()V

    const v5, 0x7f060044

    invoke-virtual {v3, v5}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    const/4 v5, 0x1

    new-array v0, v5, [Landroid/text/InputFilter;

    new-instance v5, Landroid/text/InputFilter$LengthFilter;

    const/4 v6, 0x5

    invoke-direct {v5, v6}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v5, v0, v7

    invoke-virtual {v4, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setInputType(I)V

    iget-object v5, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewHAcc:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    const v5, 0x7f060045

    invoke-virtual {v3, v5}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    const v5, 0x7f060046

    invoke-virtual {v3, v5}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    new-instance v5, Lcom/mediatek/lbs/em/LbsAgps$56;

    invoke-direct {v5, p0, v4, v3}, Lcom/mediatek/lbs/em/LbsAgps$56;-><init>(Lcom/mediatek/lbs/em/LbsAgps;Landroid/widget/EditText;Landroid/app/Dialog;)V

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v5, Lcom/mediatek/lbs/em/LbsAgps$57;

    invoke-direct {v5, p0, v3}, Lcom/mediatek/lbs/em/LbsAgps$57;-><init>(Lcom/mediatek/lbs/em/LbsAgps;Landroid/app/Dialog;)V

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private editQopLocationAge()V
    .locals 8

    const/4 v7, 0x0

    new-instance v3, Landroid/app/Dialog;

    invoke-direct {v3, p0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    const-string v5, "Edit Location Age"

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    const v5, 0x7f030002

    invoke-virtual {v3, v5}, Landroid/app/Dialog;->setContentView(I)V

    invoke-virtual {v3}, Landroid/app/Dialog;->show()V

    const v5, 0x7f060044

    invoke-virtual {v3, v5}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    const/4 v5, 0x1

    new-array v0, v5, [Landroid/text/InputFilter;

    new-instance v5, Landroid/text/InputFilter$LengthFilter;

    const/4 v6, 0x5

    invoke-direct {v5, v6}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v5, v0, v7

    invoke-virtual {v4, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setInputType(I)V

    iget-object v5, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewLocationAge:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    const v5, 0x7f060045

    invoke-virtual {v3, v5}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    const v5, 0x7f060046

    invoke-virtual {v3, v5}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    new-instance v5, Lcom/mediatek/lbs/em/LbsAgps$60;

    invoke-direct {v5, p0, v4, v3}, Lcom/mediatek/lbs/em/LbsAgps$60;-><init>(Lcom/mediatek/lbs/em/LbsAgps;Landroid/widget/EditText;Landroid/app/Dialog;)V

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v5, Lcom/mediatek/lbs/em/LbsAgps$61;

    invoke-direct {v5, p0, v3}, Lcom/mediatek/lbs/em/LbsAgps$61;-><init>(Lcom/mediatek/lbs/em/LbsAgps;Landroid/app/Dialog;)V

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private editQopVerticalAcc()V
    .locals 8

    const/4 v7, 0x0

    new-instance v3, Landroid/app/Dialog;

    invoke-direct {v3, p0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    const-string v5, "Edit Vertical Accuracy"

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    const v5, 0x7f030002

    invoke-virtual {v3, v5}, Landroid/app/Dialog;->setContentView(I)V

    invoke-virtual {v3}, Landroid/app/Dialog;->show()V

    const v5, 0x7f060044

    invoke-virtual {v3, v5}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    const/4 v5, 0x1

    new-array v0, v5, [Landroid/text/InputFilter;

    new-instance v5, Landroid/text/InputFilter$LengthFilter;

    const/4 v6, 0x5

    invoke-direct {v5, v6}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v5, v0, v7

    invoke-virtual {v4, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setInputType(I)V

    iget-object v5, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewVAcc:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    const v5, 0x7f060045

    invoke-virtual {v3, v5}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    const v5, 0x7f060046

    invoke-virtual {v3, v5}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    new-instance v5, Lcom/mediatek/lbs/em/LbsAgps$58;

    invoke-direct {v5, p0, v4, v3}, Lcom/mediatek/lbs/em/LbsAgps$58;-><init>(Lcom/mediatek/lbs/em/LbsAgps;Landroid/widget/EditText;Landroid/app/Dialog;)V

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v5, Lcom/mediatek/lbs/em/LbsAgps$59;

    invoke-direct {v5, p0, v3}, Lcom/mediatek/lbs/em/LbsAgps$59;-><init>(Lcom/mediatek/lbs/em/LbsAgps;Landroid/app/Dialog;)V

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private editSlpAddr()V
    .locals 8

    const/4 v7, 0x0

    new-instance v3, Landroid/app/Dialog;

    invoke-direct {v3, p0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    const-string v5, "Edit SLP Address"

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    const v5, 0x7f030002

    invoke-virtual {v3, v5}, Landroid/app/Dialog;->setContentView(I)V

    invoke-virtual {v3}, Landroid/app/Dialog;->show()V

    const v5, 0x7f060044

    invoke-virtual {v3, v5}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    const/4 v5, 0x1

    new-array v0, v5, [Landroid/text/InputFilter;

    new-instance v5, Landroid/text/InputFilter$LengthFilter;

    const/16 v6, 0x1e

    invoke-direct {v5, v6}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v5, v0, v7

    invoke-virtual {v4, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    iget-object v5, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewSlpAddr:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    const v5, 0x7f060045

    invoke-virtual {v3, v5}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    const v5, 0x7f060046

    invoke-virtual {v3, v5}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    new-instance v5, Lcom/mediatek/lbs/em/LbsAgps$52;

    invoke-direct {v5, p0, v4, v3}, Lcom/mediatek/lbs/em/LbsAgps$52;-><init>(Lcom/mediatek/lbs/em/LbsAgps;Landroid/widget/EditText;Landroid/app/Dialog;)V

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v5, Lcom/mediatek/lbs/em/LbsAgps$53;

    invoke-direct {v5, p0, v3}, Lcom/mediatek/lbs/em/LbsAgps$53;-><init>(Lcom/mediatek/lbs/em/LbsAgps;Landroid/app/Dialog;)V

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private editSlpPort()V
    .locals 8

    const/4 v7, 0x0

    new-instance v3, Landroid/app/Dialog;

    invoke-direct {v3, p0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    const-string v5, "Edit SLP Port"

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    const v5, 0x7f030002

    invoke-virtual {v3, v5}, Landroid/app/Dialog;->setContentView(I)V

    invoke-virtual {v3}, Landroid/app/Dialog;->show()V

    const v5, 0x7f060044

    invoke-virtual {v3, v5}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    const/4 v5, 0x1

    new-array v0, v5, [Landroid/text/InputFilter;

    new-instance v5, Landroid/text/InputFilter$LengthFilter;

    const/4 v6, 0x5

    invoke-direct {v5, v6}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v5, v0, v7

    invoke-virtual {v4, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setInputType(I)V

    iget-object v5, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewSlpPort:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    const v5, 0x7f060045

    invoke-virtual {v3, v5}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    const v5, 0x7f060046

    invoke-virtual {v3, v5}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    new-instance v5, Lcom/mediatek/lbs/em/LbsAgps$54;

    invoke-direct {v5, p0, v4, v3}, Lcom/mediatek/lbs/em/LbsAgps$54;-><init>(Lcom/mediatek/lbs/em/LbsAgps;Landroid/widget/EditText;Landroid/app/Dialog;)V

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v5, Lcom/mediatek/lbs/em/LbsAgps$55;

    invoke-direct {v5, p0, v3}, Lcom/mediatek/lbs/em/LbsAgps$55;-><init>(Lcom/mediatek/lbs/em/LbsAgps;Landroid/app/Dialog;)V

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private handleEnableAgps()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxEnableAgps:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mAgpsMgr:Lcom/mediatek/common/agps/MtkAgpsManager;

    invoke-interface {v0}, Lcom/mediatek/common/agps/MtkAgpsManager;->enable()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mAgpsMgr:Lcom/mediatek/common/agps/MtkAgpsManager;

    invoke-interface {v0}, Lcom/mediatek/common/agps/MtkAgpsManager;->disable()V

    goto :goto_0
.end method

.method private handleKValueAccuracyUnit()V
    .locals 4

    iget v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mAccUnitFlag:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    const/4 v1, 0x0

    iput v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mAccUnitFlag:I

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewHAcc:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->calcKFromMeter(I)I

    move-result v0

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewHAcc:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewVAcc:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->calcKFromMeter(I)I

    move-result v0

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewVAcc:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method private handleLoadProfile()V
    .locals 42

    const-string v40, "agps_profile"

    const/16 v41, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/lbs/em/LbsAgps;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v27

    const-string v40, "agps_enable"

    const/16 v41, 0x1

    move-object/from16 v0, v27

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    const-string v40, "iot"

    const/16 v41, 0x0

    move-object/from16 v0, v27

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v18

    const-string v40, "mode"

    const/16 v41, 0x1

    move-object/from16 v0, v27

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v22

    const-string v40, "cp_up"

    const/16 v41, 0x0

    move-object/from16 v0, v27

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v11

    const-string v40, "slp_code"

    const-string v41, "GOOGLE"

    move-object/from16 v0, v27

    move-object/from16 v1, v40

    move-object/from16 v2, v41

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v32

    const-string v40, "slp_addr"

    const-string v41, "supl.google.com"

    move-object/from16 v0, v27

    move-object/from16 v1, v40

    move-object/from16 v2, v41

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    const-string v40, "slp_port"

    const/16 v41, 0x1c6b

    move-object/from16 v0, v27

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v33

    const-string v40, "slp_tls"

    const/16 v41, 0x1

    move-object/from16 v0, v27

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v34

    const-string v40, "type"

    const/16 v41, 0x0

    move-object/from16 v0, v27

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v37

    const-string v40, "set_id"

    const/16 v41, 0x0

    move-object/from16 v0, v27

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v29

    const-string v40, "acc_unit"

    const/16 v41, 0x0

    move-object/from16 v0, v27

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    const-string v40, "h_acc"

    const/16 v41, 0xa

    move-object/from16 v0, v27

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v16

    const-string v40, "v_acc"

    const/16 v41, 0x10

    move-object/from16 v0, v27

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v38

    const-string v40, "age"

    const/16 v41, 0x0

    move-object/from16 v0, v27

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    const-string v40, "delay"

    const/16 v41, 0x0

    move-object/from16 v0, v27

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v12

    const-string v40, "cert_ver"

    const/16 v41, 0x0

    move-object/from16 v0, v27

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    const-string v40, "molr_pos_method"

    const/16 v41, 0x0

    move-object/from16 v0, v27

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v23

    const-string v40, "ext_addr_enable"

    const/16 v41, 0x0

    move-object/from16 v0, v27

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v15

    const-string v40, "ext_addr"

    const-string v41, "0123456789*#+"

    move-object/from16 v0, v27

    move-object/from16 v1, v40

    move-object/from16 v2, v41

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    const-string v40, "mlc_num_enable"

    const/16 v41, 0x0

    move-object/from16 v0, v27

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v21

    const-string v40, "mlc_num"

    const-string v41, "0123456789*#+"

    move-object/from16 v0, v27

    move-object/from16 v1, v40

    move-object/from16 v2, v41

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    const-string v40, "sim_select"

    const/16 v41, 0x1

    move-object/from16 v0, v27

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v30

    const-string v40, "allow_ni"

    const/16 v41, 0x1

    move-object/from16 v0, v27

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    const-string v40, "allow_em"

    const/16 v41, 0x1

    move-object/from16 v0, v27

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    const-string v40, "allow_roaming"

    const/16 v41, 0x1

    move-object/from16 v0, v27

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    const-string v40, "supl_to_file"

    const/16 v41, 0x0

    move-object/from16 v0, v27

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v35

    const-string v40, "ecid"

    const/16 v41, 0x0

    move-object/from16 v0, v27

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v13

    const-string v40, "lab_performance"

    const/16 v41, 0x0

    move-object/from16 v0, v27

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v19

    const-string v40, "enable_ni_timer"

    const/16 v41, 0x0

    move-object/from16 v0, v27

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v25

    const-string v40, "notification_timeout"

    const/16 v41, 0x5

    move-object/from16 v0, v27

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v24

    const-string v40, "verification_timeout"

    const/16 v41, 0x5

    move-object/from16 v0, v27

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v39

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxEnableAgps:Landroid/widget/CheckBox;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckEnableIot:Landroid/widget/CheckBox;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    if-nez v22, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonMsa:Landroid/widget/RadioButton;

    move-object/from16 v40, v0

    const/16 v41, 0x1

    invoke-virtual/range {v40 .. v41}, Landroid/widget/RadioButton;->setChecked(Z)V

    :goto_0
    const/16 v40, 0x1

    move/from16 v0, v40

    if-ne v11, v0, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonCp:Landroid/widget/RadioButton;

    move-object/from16 v40, v0

    const/16 v41, 0x1

    invoke-virtual/range {v40 .. v41}, Landroid/widget/RadioButton;->setChecked(Z)V

    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewSlpAddr:Landroid/widget/TextView;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewSlpPort:Landroid/widget/TextView;

    move-object/from16 v40, v0

    invoke-static/range {v33 .. v33}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v41

    invoke-virtual/range {v40 .. v41}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxTls:Landroid/widget/CheckBox;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    if-nez v37, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonRrlp:Landroid/widget/RadioButton;

    move-object/from16 v40, v0

    const/16 v41, 0x1

    invoke-virtual/range {v40 .. v41}, Landroid/widget/RadioButton;->setChecked(Z)V

    :goto_2
    const/16 v40, 0x1

    move/from16 v0, v29

    move/from16 v1, v40

    if-ne v0, v1, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonIpv4:Landroid/widget/RadioButton;

    move-object/from16 v40, v0

    const/16 v41, 0x1

    invoke-virtual/range {v40 .. v41}, Landroid/widget/RadioButton;->setChecked(Z)V

    :goto_3
    if-nez v3, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonKValue:Landroid/widget/RadioButton;

    move-object/from16 v40, v0

    const/16 v41, 0x1

    invoke-virtual/range {v40 .. v41}, Landroid/widget/RadioButton;->setChecked(Z)V

    const/16 v40, 0x0

    move/from16 v0, v40

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/lbs/em/LbsAgps;->mAccUnitFlag:I

    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewHAcc:Landroid/widget/TextView;

    move-object/from16 v40, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v41

    invoke-virtual/range {v40 .. v41}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewVAcc:Landroid/widget/TextView;

    move-object/from16 v40, v0

    invoke-static/range {v38 .. v38}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v41

    invoke-virtual/range {v40 .. v41}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewLocationAge:Landroid/widget/TextView;

    move-object/from16 v40, v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v41

    invoke-virtual/range {v40 .. v41}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewDelay:Landroid/widget/TextView;

    move-object/from16 v40, v0

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v41

    invoke-virtual/range {v40 .. v41}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxCerVerify:Landroid/widget/CheckBox;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    invoke-virtual {v0, v9}, Landroid/widget/CheckBox;->setChecked(Z)V

    if-nez v23, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonLocationEstimate:Landroid/widget/RadioButton;

    move-object/from16 v40, v0

    const/16 v41, 0x1

    invoke-virtual/range {v40 .. v41}, Landroid/widget/RadioButton;->setChecked(Z)V

    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxExternalAddr:Landroid/widget/CheckBox;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    invoke-virtual {v0, v15}, Landroid/widget/CheckBox;->setChecked(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewExternalAddr:Landroid/widget/TextView;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    invoke-virtual {v0, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxMlcNumber:Landroid/widget/CheckBox;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewMlcNumber:Landroid/widget/TextView;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 v40, 0x1

    move/from16 v0, v30

    move/from16 v1, v40

    if-ne v0, v1, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonPreferSim1:Landroid/widget/RadioButton;

    move-object/from16 v40, v0

    const/16 v41, 0x1

    invoke-virtual/range {v40 .. v41}, Landroid/widget/RadioButton;->setChecked(Z)V

    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxNiAllow:Landroid/widget/CheckBox;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    invoke-virtual {v0, v7}, Landroid/widget/CheckBox;->setChecked(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxEmAllow:Landroid/widget/CheckBox;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxRoamingAllow:Landroid/widget/CheckBox;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    invoke-virtual {v0, v8}, Landroid/widget/CheckBox;->setChecked(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxSupl2File:Landroid/widget/CheckBox;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxEcidEnable:Landroid/widget/CheckBox;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    invoke-virtual {v0, v13}, Landroid/widget/CheckBox;->setChecked(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxLabPerformance:Landroid/widget/CheckBox;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxEnableNiTimer:Landroid/widget/CheckBox;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mSpinnerNotificationTimeout:Landroid/widget/Spinner;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mSpinnerVerificationTimeout:Landroid/widget/Spinner;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mProfileManager:Lcom/mediatek/common/agps/MtkAgpsProfileManager;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->getDefaultProfile()Lcom/mediatek/common/agps/MtkAgpsProfile;

    move-result-object v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mProfileManager:Lcom/mediatek/common/agps/MtkAgpsProfileManager;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->getAllProfile()Ljava/util/List;

    move-result-object v40

    invoke-interface/range {v40 .. v40}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :cond_0
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v40

    if-eqz v40, :cond_1

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v36

    check-cast v36, Lcom/mediatek/common/agps/MtkAgpsProfile;

    move-object/from16 v0, v36

    iget-object v0, v0, Lcom/mediatek/common/agps/MtkAgpsProfile;->code:Ljava/lang/String;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_0

    move-object/from16 v28, v36

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mProfileManager:Lcom/mediatek/common/agps/MtkAgpsProfileManager;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->getAllProfile()Ljava/util/List;

    move-result-object v40

    const/16 v41, 0x0

    invoke-interface/range {v40 .. v41}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/mediatek/common/agps/MtkAgpsProfile;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewSlpAddr:Landroid/widget/TextView;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v40

    move-object/from16 v0, v40

    move-object/from16 v1, v28

    iput-object v0, v1, Lcom/mediatek/common/agps/MtkAgpsProfile;->addr:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewSlpPort:Landroid/widget/TextView;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v40 .. v40}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/Integer;->intValue()I

    move-result v40

    move/from16 v0, v40

    move-object/from16 v1, v28

    iput v0, v1, Lcom/mediatek/common/agps/MtkAgpsProfile;->port:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxTls:Landroid/widget/CheckBox;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v40

    if-eqz v40, :cond_b

    const/16 v40, 0x1

    :goto_7
    move/from16 v0, v40

    move-object/from16 v1, v28

    iput v0, v1, Lcom/mediatek/common/agps/MtkAgpsProfile;->tls:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mAgpsMgr:Lcom/mediatek/common/agps/MtkAgpsManager;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Lcom/mediatek/common/agps/MtkAgpsManager;->setProfile(Lcom/mediatek/common/agps/MtkAgpsProfile;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mAgpsMgr:Lcom/mediatek/common/agps/MtkAgpsManager;

    move-object/from16 v40, v0

    invoke-interface/range {v40 .. v40}, Lcom/mediatek/common/agps/MtkAgpsManager;->getConfig()Lcom/mediatek/common/agps/MtkAgpsConfig;

    move-result-object v10

    move/from16 v0, v22

    iput v0, v10, Lcom/mediatek/common/agps/MtkAgpsConfig;->siMode:I

    move/from16 v0, v29

    iput v0, v10, Lcom/mediatek/common/agps/MtkAgpsConfig;->setId:I

    const/16 v40, 0x1

    move/from16 v0, v40

    if-ne v3, v0, :cond_c

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->calcKFromMeter(I)I

    move-result v40

    move/from16 v0, v40

    iput v0, v10, Lcom/mediatek/common/agps/MtkAgpsConfig;->qopHacc:I

    move-object/from16 v0, p0

    move/from16 v1, v38

    invoke-direct {v0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->calcKFromMeter(I)I

    move-result v40

    move/from16 v0, v40

    iput v0, v10, Lcom/mediatek/common/agps/MtkAgpsConfig;->qopVacc:I

    :goto_8
    iput v4, v10, Lcom/mediatek/common/agps/MtkAgpsConfig;->qopAge:I

    iput v12, v10, Lcom/mediatek/common/agps/MtkAgpsConfig;->qopDelay:I

    move/from16 v0, v24

    iput v0, v10, Lcom/mediatek/common/agps/MtkAgpsConfig;->notifyTimeout:I

    move/from16 v0, v39

    iput v0, v10, Lcom/mediatek/common/agps/MtkAgpsConfig;->verifyTimeout:I

    if-eqz v7, :cond_d

    const/16 v40, 0x1

    :goto_9
    move/from16 v0, v40

    iput v0, v10, Lcom/mediatek/common/agps/MtkAgpsConfig;->niEnable:I

    iput v11, v10, Lcom/mediatek/common/agps/MtkAgpsConfig;->molrPositionType:I

    iput-object v14, v10, Lcom/mediatek/common/agps/MtkAgpsConfig;->extAddress:Ljava/lang/String;

    if-eqz v15, :cond_e

    const/16 v40, 0x1

    :goto_a
    move/from16 v0, v40

    iput v0, v10, Lcom/mediatek/common/agps/MtkAgpsConfig;->extAddressEnable:I

    move-object/from16 v0, v20

    iput-object v0, v10, Lcom/mediatek/common/agps/MtkAgpsConfig;->mlcNum:Ljava/lang/String;

    if-eqz v21, :cond_f

    const/16 v40, 0x1

    :goto_b
    move/from16 v0, v40

    iput v0, v10, Lcom/mediatek/common/agps/MtkAgpsConfig;->mlcNumEnable:I

    move/from16 v0, v37

    iput v0, v10, Lcom/mediatek/common/agps/MtkAgpsConfig;->suplCapabilityType:I

    move/from16 v0, v23

    iput v0, v10, Lcom/mediatek/common/agps/MtkAgpsConfig;->molrType:I

    if-eqz v35, :cond_10

    const/16 v40, 0x1

    :goto_c
    move/from16 v0, v40

    iput v0, v10, Lcom/mediatek/common/agps/MtkAgpsConfig;->supl2file:I

    if-eqz v18, :cond_11

    const/16 v40, 0x1

    :goto_d
    move/from16 v0, v40

    iput v0, v10, Lcom/mediatek/common/agps/MtkAgpsConfig;->niIot:I

    move/from16 v0, v30

    iput v0, v10, Lcom/mediatek/common/agps/MtkAgpsConfig;->simIdPref:I

    if-eqz v8, :cond_12

    const/16 v40, 0x1

    :goto_e
    move/from16 v0, v40

    iput v0, v10, Lcom/mediatek/common/agps/MtkAgpsConfig;->roaming:I

    if-eqz v9, :cond_13

    const/16 v40, 0x1

    :goto_f
    move/from16 v0, v40

    iput v0, v10, Lcom/mediatek/common/agps/MtkAgpsConfig;->caEnable:I

    if-eqz v6, :cond_14

    const/16 v40, 0x1

    :goto_10
    move/from16 v0, v40

    iput v0, v10, Lcom/mediatek/common/agps/MtkAgpsConfig;->emEnable:I

    if-eqz v25, :cond_15

    const/16 v40, 0x1

    :goto_11
    move/from16 v0, v40

    iput v0, v10, Lcom/mediatek/common/agps/MtkAgpsConfig;->niTimer:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mAgpsMgr:Lcom/mediatek/common/agps/MtkAgpsManager;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    invoke-interface {v0, v10}, Lcom/mediatek/common/agps/MtkAgpsManager;->setConfig(Lcom/mediatek/common/agps/MtkAgpsConfig;)V

    if-eqz v5, :cond_16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mAgpsMgr:Lcom/mediatek/common/agps/MtkAgpsManager;

    move-object/from16 v40, v0

    invoke-interface/range {v40 .. v40}, Lcom/mediatek/common/agps/MtkAgpsManager;->enable()V

    :goto_12
    return-void

    :cond_2
    const/16 v40, 0x1

    move/from16 v0, v22

    move/from16 v1, v40

    if-ne v0, v1, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonMsb:Landroid/widget/RadioButton;

    move-object/from16 v40, v0

    const/16 v41, 0x1

    invoke-virtual/range {v40 .. v41}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto/16 :goto_0

    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonStandalone:Landroid/widget/RadioButton;

    move-object/from16 v40, v0

    const/16 v41, 0x1

    invoke-virtual/range {v40 .. v41}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto/16 :goto_0

    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonUp:Landroid/widget/RadioButton;

    move-object/from16 v40, v0

    const/16 v41, 0x1

    invoke-virtual/range {v40 .. v41}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto/16 :goto_1

    :cond_5
    const/16 v40, 0x1

    move/from16 v0, v37

    move/from16 v1, v40

    if-ne v0, v1, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonRrc:Landroid/widget/RadioButton;

    move-object/from16 v40, v0

    const/16 v41, 0x1

    invoke-virtual/range {v40 .. v41}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto/16 :goto_2

    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonRrlpRrc:Landroid/widget/RadioButton;

    move-object/from16 v40, v0

    const/16 v41, 0x1

    invoke-virtual/range {v40 .. v41}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto/16 :goto_2

    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonImsi:Landroid/widget/RadioButton;

    move-object/from16 v40, v0

    const/16 v41, 0x1

    invoke-virtual/range {v40 .. v41}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto/16 :goto_3

    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonMeter:Landroid/widget/RadioButton;

    move-object/from16 v40, v0

    const/16 v41, 0x1

    invoke-virtual/range {v40 .. v41}, Landroid/widget/RadioButton;->setChecked(Z)V

    const/16 v40, 0x1

    move/from16 v0, v40

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/lbs/em/LbsAgps;->mAccUnitFlag:I

    goto/16 :goto_4

    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonAssistanceData:Landroid/widget/RadioButton;

    move-object/from16 v40, v0

    const/16 v41, 0x1

    invoke-virtual/range {v40 .. v41}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto/16 :goto_5

    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonPreferSim2:Landroid/widget/RadioButton;

    move-object/from16 v40, v0

    const/16 v41, 0x1

    invoke-virtual/range {v40 .. v41}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto/16 :goto_6

    :cond_b
    const/16 v40, 0x0

    goto/16 :goto_7

    :cond_c
    move/from16 v0, v16

    iput v0, v10, Lcom/mediatek/common/agps/MtkAgpsConfig;->qopHacc:I

    move/from16 v0, v38

    iput v0, v10, Lcom/mediatek/common/agps/MtkAgpsConfig;->qopVacc:I

    goto/16 :goto_8

    :cond_d
    const/16 v40, 0x0

    goto/16 :goto_9

    :cond_e
    const/16 v40, 0x0

    goto/16 :goto_a

    :cond_f
    const/16 v40, 0x0

    goto/16 :goto_b

    :cond_10
    const/16 v40, 0x0

    goto/16 :goto_c

    :cond_11
    const/16 v40, 0x0

    goto/16 :goto_d

    :cond_12
    const/16 v40, 0x0

    goto/16 :goto_e

    :cond_13
    const/16 v40, 0x0

    goto/16 :goto_f

    :cond_14
    const/16 v40, 0x0

    goto/16 :goto_10

    :cond_15
    const/16 v40, 0x0

    goto/16 :goto_11

    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mAgpsMgr:Lcom/mediatek/common/agps/MtkAgpsManager;

    move-object/from16 v40, v0

    invoke-interface/range {v40 .. v40}, Lcom/mediatek/common/agps/MtkAgpsManager;->disable()V

    goto/16 :goto_12
.end method

.method private handleMeterAccuracyUnit()V
    .locals 4

    iget v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mAccUnitFlag:I

    if-nez v1, :cond_0

    const/4 v1, 0x1

    iput v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mAccUnitFlag:I

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewHAcc:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->calcMeterFromK(I)I

    move-result v0

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewHAcc:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewVAcc:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->calcMeterFromK(I)I

    move-result v0

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewVAcc:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method private handleNiDialogTest(I)V
    .locals 6
    .param p1    # I

    const/16 v5, 0xd05

    const/4 v1, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/16 v2, 0x115c

    if-nez p1, :cond_1

    const-string v0, "com.mediatek.agps.NOTIFY_ACTION"

    const/16 v1, 0x457

    invoke-direct {p0, v0, v1, v3}, Lcom/mediatek/lbs/em/LbsAgps;->sendIntent(Ljava/lang/String;II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-ne p1, v3, :cond_2

    const-string v0, "com.mediatek.agps.NOTIFY_ACTION"

    const/16 v1, 0x457

    invoke-direct {p0, v0, v1, v4}, Lcom/mediatek/lbs/em/LbsAgps;->sendIntent(Ljava/lang/String;II)V

    goto :goto_0

    :cond_2
    if-ne p1, v4, :cond_3

    const-string v0, "com.mediatek.agps.VERIFY_ACTION"

    invoke-direct {p0, v0, v5, v3}, Lcom/mediatek/lbs/em/LbsAgps;->sendIntent(Ljava/lang/String;II)V

    goto :goto_0

    :cond_3
    if-ne p1, v1, :cond_4

    const-string v0, "com.mediatek.agps.VERIFY_ACTION"

    invoke-direct {p0, v0, v5, v4}, Lcom/mediatek/lbs/em/LbsAgps;->sendIntent(Ljava/lang/String;II)V

    goto :goto_0

    :cond_4
    const/4 v0, 0x4

    if-ne p1, v0, :cond_5

    const-string v0, "com.mediatek.agps.VERIFY_ACTION"

    invoke-direct {p0, v0, v5, v1}, Lcom/mediatek/lbs/em/LbsAgps;->sendIntent(Ljava/lang/String;II)V

    goto :goto_0

    :cond_5
    const/4 v0, 0x5

    if-ne p1, v0, :cond_6

    const-string v0, "com.mediatek.agps.ERROR_ACTION"

    invoke-direct {p0, v0, v2, v3}, Lcom/mediatek/lbs/em/LbsAgps;->sendIntent(Ljava/lang/String;II)V

    goto :goto_0

    :cond_6
    const/4 v0, 0x6

    if-ne p1, v0, :cond_7

    const-string v0, "com.mediatek.agps.ERROR_ACTION"

    invoke-direct {p0, v0, v2, v4}, Lcom/mediatek/lbs/em/LbsAgps;->sendIntent(Ljava/lang/String;II)V

    goto :goto_0

    :cond_7
    const/4 v0, 0x7

    if-ne p1, v0, :cond_8

    const-string v0, "com.mediatek.agps.ERROR_ACTION"

    invoke-direct {p0, v0, v2, v1}, Lcom/mediatek/lbs/em/LbsAgps;->sendIntent(Ljava/lang/String;II)V

    goto :goto_0

    :cond_8
    const/16 v0, 0x8

    if-ne p1, v0, :cond_9

    const-string v0, "com.mediatek.agps.ERROR_ACTION"

    const/4 v1, 0x4

    invoke-direct {p0, v0, v2, v1}, Lcom/mediatek/lbs/em/LbsAgps;->sendIntent(Ljava/lang/String;II)V

    goto :goto_0

    :cond_9
    const/16 v0, 0x9

    if-ne p1, v0, :cond_a

    const-string v0, "com.mediatek.agps.ERROR_ACTION"

    const/4 v1, 0x5

    invoke-direct {p0, v0, v2, v1}, Lcom/mediatek/lbs/em/LbsAgps;->sendIntent(Ljava/lang/String;II)V

    goto :goto_0

    :cond_a
    const/16 v0, 0xa

    if-ne p1, v0, :cond_b

    const-string v0, "com.mediatek.agps.ERROR_ACTION"

    const/4 v1, 0x6

    invoke-direct {p0, v0, v2, v1}, Lcom/mediatek/lbs/em/LbsAgps;->sendIntent(Ljava/lang/String;II)V

    goto :goto_0

    :cond_b
    const/16 v0, 0xb

    if-ne p1, v0, :cond_c

    const-string v0, "com.mediatek.agps.ERROR_ACTION"

    const/4 v1, 0x7

    invoke-direct {p0, v0, v2, v1}, Lcom/mediatek/lbs/em/LbsAgps;->sendIntent(Ljava/lang/String;II)V

    goto :goto_0

    :cond_c
    const/16 v0, 0xc

    if-ne p1, v0, :cond_d

    const-string v0, "com.mediatek.agps.ERROR_ACTION"

    const/16 v1, 0x8

    invoke-direct {p0, v0, v2, v1}, Lcom/mediatek/lbs/em/LbsAgps;->sendIntent(Ljava/lang/String;II)V

    goto :goto_0

    :cond_d
    const/16 v0, 0xd

    if-ne p1, v0, :cond_e

    const-string v0, "com.mediatek.agps.ERROR_ACTION"

    const/16 v1, 0x9

    invoke-direct {p0, v0, v2, v1}, Lcom/mediatek/lbs/em/LbsAgps;->sendIntent(Ljava/lang/String;II)V

    goto/16 :goto_0

    :cond_e
    const/16 v0, 0xe

    if-ne p1, v0, :cond_f

    const-string v0, "com.mediatek.agps.ERROR_ACTION"

    const/16 v1, 0xa

    invoke-direct {p0, v0, v2, v1}, Lcom/mediatek/lbs/em/LbsAgps;->sendIntent(Ljava/lang/String;II)V

    goto/16 :goto_0

    :cond_f
    const/16 v0, 0xf

    if-ne p1, v0, :cond_10

    const-string v0, "com.mediatek.agps.ERROR_ACTION"

    const/16 v1, 0xb

    invoke-direct {p0, v0, v2, v1}, Lcom/mediatek/lbs/em/LbsAgps;->sendIntent(Ljava/lang/String;II)V

    goto/16 :goto_0

    :cond_10
    const/16 v0, 0x10

    if-ne p1, v0, :cond_11

    const-string v0, "com.mediatek.agps.ERROR_ACTION"

    const/16 v1, 0xc

    invoke-direct {p0, v0, v2, v1}, Lcom/mediatek/lbs/em/LbsAgps;->sendIntent(Ljava/lang/String;II)V

    goto/16 :goto_0

    :cond_11
    const/16 v0, 0x11

    if-ne p1, v0, :cond_12

    const-string v0, "com.mediatek.agps.ERROR_ACTION"

    const/16 v1, 0xd

    invoke-direct {p0, v0, v2, v1}, Lcom/mediatek/lbs/em/LbsAgps;->sendIntent(Ljava/lang/String;II)V

    goto/16 :goto_0

    :cond_12
    const/16 v0, 0x12

    if-ne p1, v0, :cond_13

    const-string v0, "com.mediatek.agps.ERROR_ACTION"

    const/16 v1, 0xe

    invoke-direct {p0, v0, v2, v1}, Lcom/mediatek/lbs/em/LbsAgps;->sendIntent(Ljava/lang/String;II)V

    goto/16 :goto_0

    :cond_13
    const/16 v0, 0x13

    if-ne p1, v0, :cond_0

    const-string v0, "com.mediatek.agps.ERROR_ACTION"

    const/16 v1, 0xf

    invoke-direct {p0, v0, v2, v1}, Lcom/mediatek/lbs/em/LbsAgps;->sendIntent(Ljava/lang/String;II)V

    goto/16 :goto_0
.end method

.method private handleSetProfile()V
    .locals 38

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxEnableAgps:Landroid/widget/CheckBox;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckEnableIot:Landroid/widget/CheckBox;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonStandalone:Landroid/widget/RadioButton;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v35

    if-eqz v35, :cond_0

    const/16 v20, 0x2

    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonCp:Landroid/widget/RadioButton;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v35

    if-eqz v35, :cond_2

    const/4 v10, 0x1

    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mCurrentProfile:Lcom/mediatek/common/agps/MtkAgpsProfile;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    iget-object v0, v0, Lcom/mediatek/common/agps/MtkAgpsProfile;->code:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewSlpAddr:Landroid/widget/TextView;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewSlpPort:Landroid/widget/TextView;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v35 .. v35}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/Integer;->intValue()I

    move-result v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxTls:Landroid/widget/CheckBox;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonRrlp:Landroid/widget/RadioButton;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v35

    if-eqz v35, :cond_3

    const/16 v32, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonIpv4:Landroid/widget/RadioButton;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v35

    if-eqz v35, :cond_5

    const/16 v25, 0x1

    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonKValue:Landroid/widget/RadioButton;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v35

    if-eqz v35, :cond_6

    const/4 v3, 0x0

    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewHAcc:Landroid/widget/TextView;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v35 .. v35}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/Integer;->intValue()I

    move-result v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewVAcc:Landroid/widget/TextView;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v35 .. v35}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/Integer;->intValue()I

    move-result v33

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewLocationAge:Landroid/widget/TextView;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v35 .. v35}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/Integer;->intValue()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewDelay:Landroid/widget/TextView;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v35 .. v35}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/Integer;->intValue()I

    move-result v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxCerVerify:Landroid/widget/CheckBox;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonLocationEstimate:Landroid/widget/RadioButton;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v35

    if-eqz v35, :cond_7

    const/16 v21, 0x0

    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxExternalAddr:Landroid/widget/CheckBox;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewExternalAddr:Landroid/widget/TextView;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxMlcNumber:Landroid/widget/CheckBox;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewMlcNumber:Landroid/widget/TextView;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonPreferSim1:Landroid/widget/RadioButton;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v35

    if-eqz v35, :cond_8

    const/16 v26, 0x1

    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxNiAllow:Landroid/widget/CheckBox;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxEmAllow:Landroid/widget/CheckBox;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxRoamingAllow:Landroid/widget/CheckBox;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxSupl2File:Landroid/widget/CheckBox;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxEcidEnable:Landroid/widget/CheckBox;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxLabPerformance:Landroid/widget/CheckBox;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxEnableNiTimer:Landroid/widget/CheckBox;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mSpinnerNotificationTimeout:Landroid/widget/Spinner;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mSpinnerVerificationTimeout:Landroid/widget/Spinner;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v34

    const-string v35, "agps_profile"

    const/16 v36, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/lbs/em/LbsAgps;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v35

    const-string v36, "agps_enable"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v35

    const-string v36, "iot"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    move/from16 v2, v16

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v35

    const-string v36, "mode"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    move/from16 v2, v20

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v35

    const-string v36, "cp_up"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    invoke-interface {v0, v1, v10}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v35

    const-string v36, "slp_code"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    move-object/from16 v2, v28

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v35

    const-string v36, "slp_addr"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    move-object/from16 v2, v27

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v35

    const-string v36, "slp_port"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    move/from16 v2, v29

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v35

    const-string v36, "slp_tls"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    move/from16 v2, v30

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v35

    const-string v36, "type"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    move/from16 v2, v32

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v35

    const-string v36, "set_id"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    move/from16 v2, v25

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v35

    const-string v36, "acc_unit"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v35

    const-string v36, "h_acc"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    invoke-interface {v0, v1, v15}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v35

    const-string v36, "v_acc"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    move/from16 v2, v33

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v35

    const-string v36, "age"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v35

    const-string v36, "delay"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    invoke-interface {v0, v1, v11}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v35

    const-string v36, "cert_ver"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    invoke-interface {v0, v1, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v35

    const-string v36, "molr_pos_method"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    move/from16 v2, v21

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v35

    const-string v36, "ext_addr_enable"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    invoke-interface {v0, v1, v14}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v35

    const-string v36, "ext_addr"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    invoke-interface {v0, v1, v13}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v35

    const-string v36, "mlc_num_enable"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    move/from16 v2, v19

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v35

    const-string v36, "mlc_num"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    move-object/from16 v2, v18

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v35

    const-string v36, "sim_select"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    move/from16 v2, v26

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v35

    const-string v36, "allow_ni"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    invoke-interface {v0, v1, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v35

    const-string v36, "allow_em"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v35

    const-string v36, "allow_roaming"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    invoke-interface {v0, v1, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v35

    const-string v36, "supl_to_file"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    move/from16 v2, v31

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v35

    const-string v36, "ecid"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    invoke-interface {v0, v1, v12}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v35

    const-string v36, "lab_performance"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    move/from16 v2, v17

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v35

    const-string v36, "enable_ni_timer"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    move/from16 v2, v23

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v35

    const-string v36, "notification_timeout"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    move/from16 v2, v22

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v35

    const-string v36, "verification_timeout"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    move/from16 v2, v34

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v35

    invoke-interface/range {v35 .. v35}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/lbs/em/LbsAgps;->getApplicationContext()Landroid/content/Context;

    move-result-object v35

    const-string v36, "Saved"

    const/16 v37, 0x0

    invoke-static/range {v35 .. v37}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Landroid/widget/Toast;->show()V

    return-void

    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonMsb:Landroid/widget/RadioButton;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v35

    if-eqz v35, :cond_1

    const/16 v20, 0x1

    goto/16 :goto_0

    :cond_1
    const/16 v20, 0x0

    goto/16 :goto_0

    :cond_2
    const/4 v10, 0x0

    goto/16 :goto_1

    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonRrc:Landroid/widget/RadioButton;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v35

    if-eqz v35, :cond_4

    const/16 v32, 0x1

    goto/16 :goto_2

    :cond_4
    const/16 v32, 0x2

    goto/16 :goto_2

    :cond_5
    const/16 v25, 0x0

    goto/16 :goto_3

    :cond_6
    const/4 v3, 0x1

    goto/16 :goto_4

    :cond_7
    const/16 v21, 0x1

    goto/16 :goto_5

    :cond_8
    const/16 v26, 0x2

    goto/16 :goto_6
.end method

.method private hideSpecialWidget()V
    .locals 4

    const/16 v3, 0x8

    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/data/data/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/mediatek/lbs/em/LbsAgps;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/show"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonReset:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonTest1:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonTest2:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonTest3:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonTest4:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonTest5:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonTest6:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonTest7:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonTest8:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private initWidget()V
    .locals 4

    const v3, 0x1090009

    const v2, 0x1090008

    const/high16 v1, 0x7f060000

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonLoadProfile:Landroid/widget/Button;

    const v1, 0x7f060001

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonSetProfile:Landroid/widget/Button;

    const v1, 0x7f060002

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonReset:Landroid/widget/Button;

    const v1, 0x7f060003

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonTest1:Landroid/widget/Button;

    const v1, 0x7f060004

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonTest2:Landroid/widget/Button;

    const v1, 0x7f060005

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonTest3:Landroid/widget/Button;

    const v1, 0x7f060006

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonTest4:Landroid/widget/Button;

    const v1, 0x7f060007

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonTest5:Landroid/widget/Button;

    const v1, 0x7f060008

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonTest6:Landroid/widget/Button;

    const v1, 0x7f060009

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonTest7:Landroid/widget/Button;

    const v1, 0x7f06000a

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonTest8:Landroid/widget/Button;

    const v1, 0x7f06000b

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxEnableAgps:Landroid/widget/CheckBox;

    const v1, 0x7f06000c

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckEnableIot:Landroid/widget/CheckBox;

    const v1, 0x7f06000d

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonStandalone:Landroid/widget/RadioButton;

    const v1, 0x7f06000e

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonMsb:Landroid/widget/RadioButton;

    const v1, 0x7f06000f

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonMsa:Landroid/widget/RadioButton;

    const v1, 0x7f060010

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonCp:Landroid/widget/RadioButton;

    const v1, 0x7f060011

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonUp:Landroid/widget/RadioButton;

    const v1, 0x7f060012

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewSlpAddr:Landroid/widget/TextView;

    const v1, 0x7f060013

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonSlpAddr:Landroid/widget/Button;

    const v1, 0x7f060014

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewSlpPort:Landroid/widget/TextView;

    const v1, 0x7f060015

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonSlpPort:Landroid/widget/Button;

    const v1, 0x7f060016

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxTls:Landroid/widget/CheckBox;

    const v1, 0x7f060017

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonRrlp:Landroid/widget/RadioButton;

    const v1, 0x7f060018

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonRrc:Landroid/widget/RadioButton;

    const v1, 0x7f060019

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonRrlpRrc:Landroid/widget/RadioButton;

    const v1, 0x7f06001a

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonIpv4:Landroid/widget/RadioButton;

    const v1, 0x7f06001b

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonImsi:Landroid/widget/RadioButton;

    const v1, 0x7f06001c

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonKValue:Landroid/widget/RadioButton;

    const v1, 0x7f06001d

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonMeter:Landroid/widget/RadioButton;

    const v1, 0x7f06001e

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewHAcc:Landroid/widget/TextView;

    const v1, 0x7f06001f

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonHAcc:Landroid/widget/Button;

    const v1, 0x7f060020

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewVAcc:Landroid/widget/TextView;

    const v1, 0x7f060021

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonVAcc:Landroid/widget/Button;

    const v1, 0x7f060022

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewLocationAge:Landroid/widget/TextView;

    const v1, 0x7f060023

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonLocationAge:Landroid/widget/Button;

    const v1, 0x7f060024

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewDelay:Landroid/widget/TextView;

    const v1, 0x7f060025

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonDelay:Landroid/widget/Button;

    const v1, 0x7f060026

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxCerVerify:Landroid/widget/CheckBox;

    const v1, 0x7f060027

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonLocationEstimate:Landroid/widget/RadioButton;

    const v1, 0x7f060028

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonAssistanceData:Landroid/widget/RadioButton;

    const v1, 0x7f060029

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxExternalAddr:Landroid/widget/CheckBox;

    const v1, 0x7f06002a

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewExternalAddr:Landroid/widget/TextView;

    const v1, 0x7f06002b

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxMlcNumber:Landroid/widget/CheckBox;

    const v1, 0x7f06002c

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewMlcNumber:Landroid/widget/TextView;

    const v1, 0x7f06002f

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonPreferSim1:Landroid/widget/RadioButton;

    const v1, 0x7f060030

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonPreferSim2:Landroid/widget/RadioButton;

    const v1, 0x7f060031

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxNiAllow:Landroid/widget/CheckBox;

    const v1, 0x7f060032

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxEmAllow:Landroid/widget/CheckBox;

    const v1, 0x7f060033

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxRoamingAllow:Landroid/widget/CheckBox;

    const v1, 0x7f060035

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxSupl2File:Landroid/widget/CheckBox;

    const v1, 0x7f060034

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxEcidEnable:Landroid/widget/CheckBox;

    const v1, 0x7f06002d

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxLabPerformance:Landroid/widget/CheckBox;

    const v1, 0x7f060036

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxEnableNiTimer:Landroid/widget/CheckBox;

    const v1, 0x7f060037

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mSpinnerNotificationTimeout:Landroid/widget/Spinner;

    const v1, 0x7f060038

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mSpinnerVerificationTimeout:Landroid/widget/Spinner;

    const v1, 0x7f06003a

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mSpinnerNiDialogTest:Landroid/widget/Spinner;

    const v1, 0x7f060039

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonReset2Default:Landroid/widget/Button;

    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->TIMEOUT_LIST:[Ljava/lang/String;

    invoke-direct {v0, p0, v2, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v0, v3}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mSpinnerNotificationTimeout:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->TIMEOUT_LIST:[Ljava/lang/String;

    invoke-direct {v0, p0, v2, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v0, v3}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mSpinnerVerificationTimeout:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->NI_DIALOG_TEST_LIST:[Ljava/lang/String;

    invoke-direct {v0, p0, v2, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v0, v3}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mSpinnerNiDialogTest:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonLoadProfile:Landroid/widget/Button;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$2;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$2;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonSetProfile:Landroid/widget/Button;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$3;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$3;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonReset:Landroid/widget/Button;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$4;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$4;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonTest1:Landroid/widget/Button;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$5;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$5;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonTest2:Landroid/widget/Button;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$6;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$6;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonTest3:Landroid/widget/Button;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$7;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$7;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonTest4:Landroid/widget/Button;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$8;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$8;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonTest5:Landroid/widget/Button;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$9;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$9;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonTest6:Landroid/widget/Button;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$10;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$10;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonTest7:Landroid/widget/Button;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$11;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$11;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonTest8:Landroid/widget/Button;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$12;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$12;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxEnableAgps:Landroid/widget/CheckBox;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$13;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$13;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonStandalone:Landroid/widget/RadioButton;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$14;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$14;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonMsb:Landroid/widget/RadioButton;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$15;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$15;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonMsa:Landroid/widget/RadioButton;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$16;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$16;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckEnableIot:Landroid/widget/CheckBox;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$17;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$17;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxNiAllow:Landroid/widget/CheckBox;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$18;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$18;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxEmAllow:Landroid/widget/CheckBox;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$19;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$19;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxRoamingAllow:Landroid/widget/CheckBox;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$20;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$20;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxSupl2File:Landroid/widget/CheckBox;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$21;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$21;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxEcidEnable:Landroid/widget/CheckBox;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$22;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$22;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxLabPerformance:Landroid/widget/CheckBox;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$23;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$23;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxEnableNiTimer:Landroid/widget/CheckBox;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$24;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$24;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mSpinnerNotificationTimeout:Landroid/widget/Spinner;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$25;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$25;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mSpinnerVerificationTimeout:Landroid/widget/Spinner;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$26;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$26;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mSpinnerNiDialogTest:Landroid/widget/Spinner;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$27;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$27;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonCp:Landroid/widget/RadioButton;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$28;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$28;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonUp:Landroid/widget/RadioButton;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$29;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$29;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonLocationEstimate:Landroid/widget/RadioButton;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$30;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$30;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonAssistanceData:Landroid/widget/RadioButton;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$31;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$31;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxExternalAddr:Landroid/widget/CheckBox;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$32;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$32;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxMlcNumber:Landroid/widget/CheckBox;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$33;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$33;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonPreferSim1:Landroid/widget/RadioButton;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$34;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$34;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonPreferSim2:Landroid/widget/RadioButton;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$35;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$35;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonSlpAddr:Landroid/widget/Button;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$36;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$36;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonSlpPort:Landroid/widget/Button;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$37;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$37;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxTls:Landroid/widget/CheckBox;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$38;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$38;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonRrlp:Landroid/widget/RadioButton;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$39;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$39;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonRrc:Landroid/widget/RadioButton;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$40;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$40;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonRrlpRrc:Landroid/widget/RadioButton;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$41;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$41;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonIpv4:Landroid/widget/RadioButton;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$42;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$42;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonImsi:Landroid/widget/RadioButton;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$43;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$43;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonKValue:Landroid/widget/RadioButton;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$44;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$44;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonMeter:Landroid/widget/RadioButton;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$45;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$45;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonHAcc:Landroid/widget/Button;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$46;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$46;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonVAcc:Landroid/widget/Button;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$47;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$47;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonLocationAge:Landroid/widget/Button;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$48;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$48;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonDelay:Landroid/widget/Button;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$49;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$49;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxCerVerify:Landroid/widget/CheckBox;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$50;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$50;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonReset2Default:Landroid/widget/Button;

    new-instance v2, Lcom/mediatek/lbs/em/LbsAgps$51;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsAgps$51;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private loadConfigFromAgpsFramework()V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x1

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mAgpsMgr:Lcom/mediatek/common/agps/MtkAgpsManager;

    invoke-interface {v1}, Lcom/mediatek/common/agps/MtkAgpsManager;->getConfig()Lcom/mediatek/common/agps/MtkAgpsConfig;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mAgpsMgr:Lcom/mediatek/common/agps/MtkAgpsManager;

    invoke-interface {v1}, Lcom/mediatek/common/agps/MtkAgpsManager;->getProfile()Lcom/mediatek/common/agps/MtkAgpsProfile;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCurrentProfile:Lcom/mediatek/common/agps/MtkAgpsProfile;

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxEnableAgps:Landroid/widget/CheckBox;

    iget-object v4, p0, Lcom/mediatek/lbs/em/LbsAgps;->mAgpsMgr:Lcom/mediatek/common/agps/MtkAgpsManager;

    invoke-interface {v4}, Lcom/mediatek/common/agps/MtkAgpsManager;->getStatus()Z

    move-result v4

    invoke-virtual {v1, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v4, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckEnableIot:Landroid/widget/CheckBox;

    iget v1, v0, Lcom/mediatek/common/agps/MtkAgpsConfig;->niIot:I

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    invoke-virtual {v4, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget v1, v0, Lcom/mediatek/common/agps/MtkAgpsConfig;->siMode:I

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonMsa:Landroid/widget/RadioButton;

    invoke-virtual {v1, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    :goto_1
    iget v1, v0, Lcom/mediatek/common/agps/MtkAgpsConfig;->molrPositionType:I

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonUp:Landroid/widget/RadioButton;

    invoke-virtual {v1, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    :goto_2
    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewSlpAddr:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCurrentProfile:Lcom/mediatek/common/agps/MtkAgpsProfile;

    iget-object v4, v4, Lcom/mediatek/common/agps/MtkAgpsProfile;->addr:Ljava/lang/String;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewSlpPort:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCurrentProfile:Lcom/mediatek/common/agps/MtkAgpsProfile;

    iget v5, v5, Lcom/mediatek/common/agps/MtkAgpsProfile;->port:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxTls:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCurrentProfile:Lcom/mediatek/common/agps/MtkAgpsProfile;

    iget v1, v1, Lcom/mediatek/common/agps/MtkAgpsProfile;->tls:I

    if-nez v1, :cond_4

    move v1, v2

    :goto_3
    invoke-virtual {v4, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget v1, v0, Lcom/mediatek/common/agps/MtkAgpsConfig;->suplCapabilityType:I

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonRrlp:Landroid/widget/RadioButton;

    invoke-virtual {v1, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    :goto_4
    iget v1, v0, Lcom/mediatek/common/agps/MtkAgpsConfig;->setId:I

    if-nez v1, :cond_7

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonImsi:Landroid/widget/RadioButton;

    invoke-virtual {v1, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    :goto_5
    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonKValue:Landroid/widget/RadioButton;

    invoke-virtual {v1, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewHAcc:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v0, Lcom/mediatek/common/agps/MtkAgpsConfig;->qopHacc:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewVAcc:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v0, Lcom/mediatek/common/agps/MtkAgpsConfig;->qopVacc:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewLocationAge:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v0, Lcom/mediatek/common/agps/MtkAgpsConfig;->qopAge:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewDelay:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v0, Lcom/mediatek/common/agps/MtkAgpsConfig;->qopDelay:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxCerVerify:Landroid/widget/CheckBox;

    iget v1, v0, Lcom/mediatek/common/agps/MtkAgpsConfig;->caEnable:I

    if-nez v1, :cond_8

    move v1, v2

    :goto_6
    invoke-virtual {v4, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget v1, v0, Lcom/mediatek/common/agps/MtkAgpsConfig;->molrType:I

    if-nez v1, :cond_9

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonLocationEstimate:Landroid/widget/RadioButton;

    invoke-virtual {v1, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    :goto_7
    iget-object v4, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxExternalAddr:Landroid/widget/CheckBox;

    iget v1, v0, Lcom/mediatek/common/agps/MtkAgpsConfig;->extAddressEnable:I

    if-nez v1, :cond_a

    move v1, v2

    :goto_8
    invoke-virtual {v4, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewExternalAddr:Landroid/widget/TextView;

    iget-object v4, v0, Lcom/mediatek/common/agps/MtkAgpsConfig;->extAddress:Ljava/lang/String;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxMlcNumber:Landroid/widget/CheckBox;

    iget v1, v0, Lcom/mediatek/common/agps/MtkAgpsConfig;->mlcNumEnable:I

    if-nez v1, :cond_b

    move v1, v2

    :goto_9
    invoke-virtual {v4, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mTextViewMlcNumber:Landroid/widget/TextView;

    iget-object v4, v0, Lcom/mediatek/common/agps/MtkAgpsConfig;->mlcNum:Ljava/lang/String;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v1, v0, Lcom/mediatek/common/agps/MtkAgpsConfig;->simIdPref:I

    if-ne v1, v3, :cond_c

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonPreferSim1:Landroid/widget/RadioButton;

    invoke-virtual {v1, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    :goto_a
    iget-object v4, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxNiAllow:Landroid/widget/CheckBox;

    iget v1, v0, Lcom/mediatek/common/agps/MtkAgpsConfig;->niEnable:I

    if-nez v1, :cond_d

    move v1, v2

    :goto_b
    invoke-virtual {v4, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v4, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxEmAllow:Landroid/widget/CheckBox;

    iget v1, v0, Lcom/mediatek/common/agps/MtkAgpsConfig;->emEnable:I

    if-nez v1, :cond_e

    move v1, v2

    :goto_c
    invoke-virtual {v4, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v4, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxRoamingAllow:Landroid/widget/CheckBox;

    iget v1, v0, Lcom/mediatek/common/agps/MtkAgpsConfig;->roaming:I

    if-nez v1, :cond_f

    move v1, v2

    :goto_d
    invoke-virtual {v4, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v4, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxSupl2File:Landroid/widget/CheckBox;

    iget v1, v0, Lcom/mediatek/common/agps/MtkAgpsConfig;->supl2file:I

    if-nez v1, :cond_10

    move v1, v2

    :goto_e
    invoke-virtual {v4, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v4, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxEcidEnable:Landroid/widget/CheckBox;

    iget v1, v0, Lcom/mediatek/common/agps/MtkAgpsConfig;->eCidEnable:I

    if-nez v1, :cond_11

    move v1, v2

    :goto_f
    invoke-virtual {v4, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v4, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxLabPerformance:Landroid/widget/CheckBox;

    iget v1, v0, Lcom/mediatek/common/agps/MtkAgpsConfig;->labPerformance:I

    if-nez v1, :cond_12

    move v1, v2

    :goto_10
    invoke-virtual {v4, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mCheckBoxEnableNiTimer:Landroid/widget/CheckBox;

    iget v4, v0, Lcom/mediatek/common/agps/MtkAgpsConfig;->niTimer:I

    if-nez v4, :cond_13

    :goto_11
    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mSpinnerNotificationTimeout:Landroid/widget/Spinner;

    iget v2, v0, Lcom/mediatek/common/agps/MtkAgpsConfig;->notifyTimeout:I

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setSelection(I)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mSpinnerVerificationTimeout:Landroid/widget/Spinner;

    iget v2, v0, Lcom/mediatek/common/agps/MtkAgpsConfig;->verifyTimeout:I

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setSelection(I)V

    return-void

    :cond_0
    move v1, v3

    goto/16 :goto_0

    :cond_1
    iget v1, v0, Lcom/mediatek/common/agps/MtkAgpsConfig;->siMode:I

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonMsb:Landroid/widget/RadioButton;

    invoke-virtual {v1, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto/16 :goto_1

    :cond_2
    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonStandalone:Landroid/widget/RadioButton;

    invoke-virtual {v1, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto/16 :goto_1

    :cond_3
    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonCp:Landroid/widget/RadioButton;

    invoke-virtual {v1, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto/16 :goto_2

    :cond_4
    move v1, v3

    goto/16 :goto_3

    :cond_5
    iget v1, v0, Lcom/mediatek/common/agps/MtkAgpsConfig;->suplCapabilityType:I

    if-ne v1, v3, :cond_6

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonRrc:Landroid/widget/RadioButton;

    invoke-virtual {v1, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto/16 :goto_4

    :cond_6
    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonRrlpRrc:Landroid/widget/RadioButton;

    invoke-virtual {v1, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto/16 :goto_4

    :cond_7
    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonIpv4:Landroid/widget/RadioButton;

    invoke-virtual {v1, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto/16 :goto_5

    :cond_8
    move v1, v3

    goto/16 :goto_6

    :cond_9
    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonAssistanceData:Landroid/widget/RadioButton;

    invoke-virtual {v1, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto/16 :goto_7

    :cond_a
    move v1, v3

    goto/16 :goto_8

    :cond_b
    move v1, v3

    goto/16 :goto_9

    :cond_c
    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsAgps;->mRadioButtonPreferSim2:Landroid/widget/RadioButton;

    invoke-virtual {v1, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto/16 :goto_a

    :cond_d
    move v1, v3

    goto/16 :goto_b

    :cond_e
    move v1, v3

    goto/16 :goto_c

    :cond_f
    move v1, v3

    goto/16 :goto_d

    :cond_10
    move v1, v3

    goto/16 :goto_e

    :cond_11
    move v1, v3

    goto/16 :goto_f

    :cond_12
    move v1, v3

    goto :goto_10

    :cond_13
    move v2, v3

    goto :goto_11
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "LocationEM"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private msleep(J)V
    .locals 1
    .param p1    # J

    :try_start_0
    invoke-static {p1, p2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method private sendIntent(Ljava/lang/String;II)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "msg_type"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "msg_id"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "request_id"

    const-string v3, "test request id"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "client_name"

    const-string v3, "test client name"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    invoke-virtual {v1, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsAgps;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method private showSpecialWidget()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonReset:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonTest1:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonTest2:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonTest3:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonTest4:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonTest5:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonTest6:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonTest7:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mButtonTest8:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/high16 v0, 0x7f030000

    invoke-virtual {p0, v0}, Lcom/mediatek/lbs/em/LbsAgps;->setContentView(I)V

    new-instance v0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;

    invoke-direct {v0}, Lcom/mediatek/common/agps/MtkAgpsProfileManager;-><init>()V

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mProfileManager:Lcom/mediatek/common/agps/MtkAgpsProfileManager;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mProfileManager:Lcom/mediatek/common/agps/MtkAgpsProfileManager;

    const-string v1, "/etc/agps_profiles_conf.xml"

    invoke-virtual {v0, v1}, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->updateAgpsProfile(Ljava/lang/String;)V

    const-string v0, "mtk-agps"

    invoke-virtual {p0, v0}, Lcom/mediatek/lbs/em/LbsAgps;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/common/agps/MtkAgpsManager;

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mAgpsMgr:Lcom/mediatek/common/agps/MtkAgpsManager;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsAgps;->mAgpsMgr:Lcom/mediatek/common/agps/MtkAgpsManager;

    if-nez v0, :cond_0

    const-string v0, "ERR: getSystemService MtkAgpsManager failed"

    invoke-direct {p0, v0}, Lcom/mediatek/lbs/em/LbsAgps;->log(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/lbs/em/LbsAgps;->initWidget()V

    invoke-direct {p0}, Lcom/mediatek/lbs/em/LbsAgps;->hideSpecialWidget()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onPause()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method public onPopupButtonClick(Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/view/View;

    const/4 v6, 0x0

    iget-object v4, p0, Lcom/mediatek/lbs/em/LbsAgps;->mSlpPopup:Landroid/widget/PopupMenu;

    if-nez v4, :cond_0

    const/4 v1, 0x0

    new-instance v4, Landroid/widget/PopupMenu;

    invoke-direct {v4, p0, p1}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    iput-object v4, p0, Lcom/mediatek/lbs/em/LbsAgps;->mSlpPopup:Landroid/widget/PopupMenu;

    iget-object v4, p0, Lcom/mediatek/lbs/em/LbsAgps;->mProfileManager:Lcom/mediatek/common/agps/MtkAgpsProfileManager;

    invoke-virtual {v4}, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->getAllProfile()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/common/agps/MtkAgpsProfile;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v2, Lcom/mediatek/common/agps/MtkAgpsProfile;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v2, Lcom/mediatek/common/agps/MtkAgpsProfile;->code:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/lbs/em/LbsAgps;->mSlpPopup:Landroid/widget/PopupMenu;

    invoke-virtual {v4}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v4

    invoke-interface {v4, v6, v1, v6, v3}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v4, p0, Lcom/mediatek/lbs/em/LbsAgps;->mSlpPopup:Landroid/widget/PopupMenu;

    new-instance v5, Lcom/mediatek/lbs/em/LbsAgps$1;

    invoke-direct {v5, p0}, Lcom/mediatek/lbs/em/LbsAgps$1;-><init>(Lcom/mediatek/lbs/em/LbsAgps;)V

    invoke-virtual {v4, v5}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    iget-object v4, p0, Lcom/mediatek/lbs/em/LbsAgps;->mSlpPopup:Landroid/widget/PopupMenu;

    invoke-virtual {v4}, Landroid/widget/PopupMenu;->show()V

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    invoke-direct {p0}, Lcom/mediatek/lbs/em/LbsAgps;->loadConfigFromAgpsFramework()V

    return-void
.end method
