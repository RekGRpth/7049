.class Lcom/mediatek/lbs/em/LbsEpo$StressTest;
.super Ljava/lang/Thread;
.source "LbsEpo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/lbs/em/LbsEpo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "StressTest"
.end annotation


# instance fields
.field private delay1:I

.field private delay2:I

.field private enable:Z

.field private numOfLoop:I

.field final synthetic this$0:Lcom/mediatek/lbs/em/LbsEpo;


# direct methods
.method public constructor <init>(Lcom/mediatek/lbs/em/LbsEpo;)V
    .locals 3

    const/16 v0, 0xa

    const/4 v1, 0x3

    const/16 v2, 0xf

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/mediatek/lbs/em/LbsEpo$StressTest;-><init>(Lcom/mediatek/lbs/em/LbsEpo;III)V

    return-void
.end method

.method public constructor <init>(Lcom/mediatek/lbs/em/LbsEpo;III)V
    .locals 1
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iput-object p1, p0, Lcom/mediatek/lbs/em/LbsEpo$StressTest;->this$0:Lcom/mediatek/lbs/em/LbsEpo;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/lbs/em/LbsEpo$StressTest;->enable:Z

    iput p2, p0, Lcom/mediatek/lbs/em/LbsEpo$StressTest;->numOfLoop:I

    iput p3, p0, Lcom/mediatek/lbs/em/LbsEpo$StressTest;->delay1:I

    iput p4, p0, Lcom/mediatek/lbs/em/LbsEpo$StressTest;->delay2:I

    return-void
.end method

.method private oneSession()V
    .locals 6

    const/4 v5, 0x0

    :try_start_0
    iget v2, p0, Lcom/mediatek/lbs/em/LbsEpo$StressTest;->delay1:I

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v2, p0, Lcom/mediatek/lbs/em/LbsEpo$StressTest;->this$0:Lcom/mediatek/lbs/em/LbsEpo;

    const-string v3, "startDownload"

    # invokes: Lcom/mediatek/lbs/em/LbsEpo;->log(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/mediatek/lbs/em/LbsEpo;->access$1300(Lcom/mediatek/lbs/em/LbsEpo;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/lbs/em/LbsEpo$StressTest;->this$0:Lcom/mediatek/lbs/em/LbsEpo;

    # getter for: Lcom/mediatek/lbs/em/LbsEpo;->mEpoMgr:Lcom/mediatek/common/epo/MtkEpoClientManager;
    invoke-static {v2}, Lcom/mediatek/lbs/em/LbsEpo;->access$1400(Lcom/mediatek/lbs/em/LbsEpo;)Lcom/mediatek/common/epo/MtkEpoClientManager;

    move-result-object v2

    invoke-interface {v2}, Lcom/mediatek/common/epo/MtkEpoClientManager;->startDownload()I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/mediatek/lbs/em/LbsEpo$StressTest;->this$0:Lcom/mediatek/lbs/em/LbsEpo;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "startDownload result="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/lbs/em/LbsEpo$StressTest;->this$0:Lcom/mediatek/lbs/em/LbsEpo;

    # invokes: Lcom/mediatek/lbs/em/LbsEpo;->getStartResultString(I)Ljava/lang/String;
    invoke-static {v4, v1}, Lcom/mediatek/lbs/em/LbsEpo;->access$1500(Lcom/mediatek/lbs/em/LbsEpo;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v5, v5, v3}, Lcom/mediatek/lbs/em/LbsEpo;->sendMessage(IILjava/lang/String;)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_0
    :try_start_1
    iget v2, p0, Lcom/mediatek/lbs/em/LbsEpo$StressTest;->delay2:I

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_2

    iget-object v2, p0, Lcom/mediatek/lbs/em/LbsEpo$StressTest;->this$0:Lcom/mediatek/lbs/em/LbsEpo;

    # getter for: Lcom/mediatek/lbs/em/LbsEpo;->mEpoMgr:Lcom/mediatek/common/epo/MtkEpoClientManager;
    invoke-static {v2}, Lcom/mediatek/lbs/em/LbsEpo;->access$1400(Lcom/mediatek/lbs/em/LbsEpo;)Lcom/mediatek/common/epo/MtkEpoClientManager;

    move-result-object v2

    invoke-interface {v2}, Lcom/mediatek/common/epo/MtkEpoClientManager;->stopDownload()V

    iget-object v2, p0, Lcom/mediatek/lbs/em/LbsEpo$StressTest;->this$0:Lcom/mediatek/lbs/em/LbsEpo;

    const-string v3, "ERR: session timeout"

    # invokes: Lcom/mediatek/lbs/em/LbsEpo;->log(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/mediatek/lbs/em/LbsEpo;->access$1300(Lcom/mediatek/lbs/em/LbsEpo;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/lbs/em/LbsEpo$StressTest;->this$0:Lcom/mediatek/lbs/em/LbsEpo;

    const-string v3, "ERR: session timeout"

    invoke-virtual {v2, v5, v5, v3}, Lcom/mediatek/lbs/em/LbsEpo;->sendMessage(IILjava/lang/String;)V

    const-wide/32 v2, 0x186a0

    :try_start_2
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    iget-object v2, p0, Lcom/mediatek/lbs/em/LbsEpo$StressTest;->this$0:Lcom/mediatek/lbs/em/LbsEpo;

    const-string v3, "ERR: cannot stop the session within 100 seconds"

    # invokes: Lcom/mediatek/lbs/em/LbsEpo;->log(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/mediatek/lbs/em/LbsEpo;->access$1300(Lcom/mediatek/lbs/em/LbsEpo;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/lbs/em/LbsEpo$StressTest;->this$0:Lcom/mediatek/lbs/em/LbsEpo;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "cannot stop the session within 100 seconds"

    invoke-virtual {v2, v3, v4, v5}, Lcom/mediatek/lbs/em/LbsEpo;->sendMessage(IILjava/lang/String;)V

    invoke-virtual {p0}, Lcom/mediatek/lbs/em/LbsEpo$StressTest;->stopStress()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    :catch_1
    move-exception v2

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 5

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsEpo$StressTest;->this$0:Lcom/mediatek/lbs/em/LbsEpo;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "numOfLoop="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/lbs/em/LbsEpo$StressTest;->numOfLoop:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " delay1="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/lbs/em/LbsEpo$StressTest;->delay1:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " delay2="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/lbs/em/LbsEpo$StressTest;->delay2:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/mediatek/lbs/em/LbsEpo;->log(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/mediatek/lbs/em/LbsEpo;->access$1300(Lcom/mediatek/lbs/em/LbsEpo;Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/mediatek/lbs/em/LbsEpo$StressTest;->numOfLoop:I

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsEpo$StressTest;->this$0:Lcom/mediatek/lbs/em/LbsEpo;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "=== stress test loop="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "==="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/mediatek/lbs/em/LbsEpo;->log(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/mediatek/lbs/em/LbsEpo;->access$1300(Lcom/mediatek/lbs/em/LbsEpo;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsEpo$StressTest;->this$0:Lcom/mediatek/lbs/em/LbsEpo;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "=== stress test loop="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ==="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v4, v4, v2}, Lcom/mediatek/lbs/em/LbsEpo;->sendMessage(IILjava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/lbs/em/LbsEpo$StressTest;->oneSession()V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsEpo$StressTest;->this$0:Lcom/mediatek/lbs/em/LbsEpo;

    const-string v2, "stopDownload"

    # invokes: Lcom/mediatek/lbs/em/LbsEpo;->log(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/mediatek/lbs/em/LbsEpo;->access$1300(Lcom/mediatek/lbs/em/LbsEpo;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsEpo$StressTest;->this$0:Lcom/mediatek/lbs/em/LbsEpo;

    # getter for: Lcom/mediatek/lbs/em/LbsEpo;->mEpoMgr:Lcom/mediatek/common/epo/MtkEpoClientManager;
    invoke-static {v1}, Lcom/mediatek/lbs/em/LbsEpo;->access$1400(Lcom/mediatek/lbs/em/LbsEpo;)Lcom/mediatek/common/epo/MtkEpoClientManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/mediatek/common/epo/MtkEpoClientManager;->stopDownload()V

    iget-boolean v1, p0, Lcom/mediatek/lbs/em/LbsEpo$StressTest;->enable:Z

    if-nez v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsEpo$StressTest;->this$0:Lcom/mediatek/lbs/em/LbsEpo;

    const-string v2, "=== end of stress test ==="

    invoke-virtual {v1, v4, v4, v2}, Lcom/mediatek/lbs/em/LbsEpo;->sendMessage(IILjava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsEpo$StressTest;->this$0:Lcom/mediatek/lbs/em/LbsEpo;

    const-string v2, "end of stress test"

    # invokes: Lcom/mediatek/lbs/em/LbsEpo;->log(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/mediatek/lbs/em/LbsEpo;->access$1300(Lcom/mediatek/lbs/em/LbsEpo;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsEpo$StressTest;->this$0:Lcom/mediatek/lbs/em/LbsEpo;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v4, v3}, Lcom/mediatek/lbs/em/LbsEpo;->sendMessage(IILjava/lang/String;)V

    return-void

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public sessionDone()V
    .locals 0

    invoke-virtual {p0}, Lcom/mediatek/lbs/em/LbsEpo$StressTest;->interrupt()V

    return-void
.end method

.method public startStress()V
    .locals 0

    invoke-virtual {p0}, Lcom/mediatek/lbs/em/LbsEpo$StressTest;->start()V

    return-void
.end method

.method public stopStress()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/lbs/em/LbsEpo$StressTest;->enable:Z

    invoke-virtual {p0}, Lcom/mediatek/lbs/em/LbsEpo$StressTest;->interrupt()V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "StressTest  numOfLoop="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/mediatek/lbs/em/LbsEpo$StressTest;->numOfLoop:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " delay1="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/mediatek/lbs/em/LbsEpo$StressTest;->delay1:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " delay2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/mediatek/lbs/em/LbsEpo$StressTest;->delay2:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
