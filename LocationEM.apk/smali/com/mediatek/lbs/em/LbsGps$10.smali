.class Lcom/mediatek/lbs/em/LbsGps$10;
.super Ljava/lang/Object;
.source "LbsGps.java"

# interfaces
.implements Landroid/location/GpsStatus$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/lbs/em/LbsGps;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/lbs/em/LbsGps;


# direct methods
.method constructor <init>(Lcom/mediatek/lbs/em/LbsGps;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/lbs/em/LbsGps$10;->this$0:Lcom/mediatek/lbs/em/LbsGps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGpsStatusChanged(I)V
    .locals 8
    .param p1    # I

    const/4 v5, 0x4

    if-eq p1, v5, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v5, p0, Lcom/mediatek/lbs/em/LbsGps$10;->this$0:Lcom/mediatek/lbs/em/LbsGps;

    const/4 v6, 0x0

    # setter for: Lcom/mediatek/lbs/em/LbsGps;->mSatNum:I
    invoke-static {v5, v6}, Lcom/mediatek/lbs/em/LbsGps;->access$1702(Lcom/mediatek/lbs/em/LbsGps;I)I

    new-instance v4, Ljava/lang/String;

    invoke-direct {v4}, Ljava/lang/String;-><init>()V

    iget-object v5, p0, Lcom/mediatek/lbs/em/LbsGps$10;->this$0:Lcom/mediatek/lbs/em/LbsGps;

    # getter for: Lcom/mediatek/lbs/em/LbsGps;->mLocationManager:Landroid/location/LocationManager;
    invoke-static {v5}, Lcom/mediatek/lbs/em/LbsGps;->access$1800(Lcom/mediatek/lbs/em/LbsGps;)Landroid/location/LocationManager;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/location/LocationManager;->getGpsStatus(Landroid/location/GpsStatus;)Landroid/location/GpsStatus;

    move-result-object v3

    invoke-virtual {v3}, Landroid/location/GpsStatus;->getSatellites()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/location/GpsSatellite;

    iget-object v5, p0, Lcom/mediatek/lbs/em/LbsGps$10;->this$0:Lcom/mediatek/lbs/em/LbsGps;

    # operator++ for: Lcom/mediatek/lbs/em/LbsGps;->mSatNum:I
    invoke-static {v5}, Lcom/mediatek/lbs/em/LbsGps;->access$1708(Lcom/mediatek/lbs/em/LbsGps;)I

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " PRN="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Landroid/location/GpsSatellite;->getPrn()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " SNR="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Landroid/location/GpsSatellite;->getSnr()F

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " AZI="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Landroid/location/GpsSatellite;->getAzimuth()F

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ELE="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Landroid/location/GpsSatellite;->getElevation()F

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Landroid/location/GpsSatellite;->usedInFix()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :cond_1
    iget-object v5, p0, Lcom/mediatek/lbs/em/LbsGps$10;->this$0:Lcom/mediatek/lbs/em/LbsGps;

    # operator++ for: Lcom/mediatek/lbs/em/LbsGps;->mSatCount:I
    invoke-static {v5}, Lcom/mediatek/lbs/em/LbsGps;->access$1908(Lcom/mediatek/lbs/em/LbsGps;)I

    iget-object v5, p0, Lcom/mediatek/lbs/em/LbsGps$10;->this$0:Lcom/mediatek/lbs/em/LbsGps;

    # getter for: Lcom/mediatek/lbs/em/LbsGps;->mTextViewSatellite:Landroid/widget/TextView;
    invoke-static {v5}, Lcom/mediatek/lbs/em/LbsGps;->access$2000(Lcom/mediatek/lbs/em/LbsGps;)Landroid/widget/TextView;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Satellite Count="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/mediatek/lbs/em/LbsGps$10;->this$0:Lcom/mediatek/lbs/em/LbsGps;

    # getter for: Lcom/mediatek/lbs/em/LbsGps;->mSatCount:I
    invoke-static {v7}, Lcom/mediatek/lbs/em/LbsGps;->access$1900(Lcom/mediatek/lbs/em/LbsGps;)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "Satellite Num="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/mediatek/lbs/em/LbsGps$10;->this$0:Lcom/mediatek/lbs/em/LbsGps;

    # getter for: Lcom/mediatek/lbs/em/LbsGps;->mSatNum:I
    invoke-static {v7}, Lcom/mediatek/lbs/em/LbsGps;->access$1700(Lcom/mediatek/lbs/em/LbsGps;)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method
