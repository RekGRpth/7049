.class Lcom/mediatek/lbs/em/LbsGps$11;
.super Ljava/lang/Object;
.source "LbsGps.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/lbs/em/LbsGps;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/lbs/em/LbsGps;


# direct methods
.method constructor <init>(Lcom/mediatek/lbs/em/LbsGps;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/lbs/em/LbsGps$11;->this$0:Lcom/mediatek/lbs/em/LbsGps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getTimeString(J)Ljava/lang/String;
    .locals 4
    .param p1    # J

    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p1, p2}, Ljava/util/Date;-><init>(J)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/util/Date;->getYear()I

    move-result v3

    add-int/lit16 v3, v3, 0x76c

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/Date;->getMonth()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/Date;->getDate()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/Date;->getHours()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/Date;->getMinutes()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/Date;->getSeconds()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;)V
    .locals 14
    .param p1    # Landroid/location/Location;

    const/4 v13, 0x2

    const/4 v12, 0x1

    const/4 v11, 0x0

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps$11;->this$0:Lcom/mediatek/lbs/em/LbsGps;

    const-string v1, "ERR: onLocationChanged get null"

    # invokes: Lcom/mediatek/lbs/em/LbsGps;->log(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/mediatek/lbs/em/LbsGps;->access$2100(Lcom/mediatek/lbs/em/LbsGps;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps$11;->this$0:Lcom/mediatek/lbs/em/LbsGps;

    # operator++ for: Lcom/mediatek/lbs/em/LbsGps;->mFixCount:I
    invoke-static {v0}, Lcom/mediatek/lbs/em/LbsGps;->access$2208(Lcom/mediatek/lbs/em/LbsGps;)I

    new-array v8, v13, [F

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    iget-object v4, p0, Lcom/mediatek/lbs/em/LbsGps$11;->this$0:Lcom/mediatek/lbs/em/LbsGps;

    # getter for: Lcom/mediatek/lbs/em/LbsGps;->mRefLat:D
    invoke-static {v4}, Lcom/mediatek/lbs/em/LbsGps;->access$2300(Lcom/mediatek/lbs/em/LbsGps;)D

    move-result-wide v4

    iget-object v6, p0, Lcom/mediatek/lbs/em/LbsGps$11;->this$0:Lcom/mediatek/lbs/em/LbsGps;

    # getter for: Lcom/mediatek/lbs/em/LbsGps;->mRefLng:D
    invoke-static {v6}, Lcom/mediatek/lbs/em/LbsGps;->access$2400(Lcom/mediatek/lbs/em/LbsGps;)D

    move-result-wide v6

    invoke-static/range {v0 .. v8}, Landroid/location/Location;->distanceBetween(DDDD[F)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps$11;->this$0:Lcom/mediatek/lbs/em/LbsGps;

    aget v1, v8, v11

    # setter for: Lcom/mediatek/lbs/em/LbsGps;->mDistance:F
    invoke-static {v0, v1}, Lcom/mediatek/lbs/em/LbsGps;->access$2502(Lcom/mediatek/lbs/em/LbsGps;F)F

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps$11;->this$0:Lcom/mediatek/lbs/em/LbsGps;

    # getter for: Lcom/mediatek/lbs/em/LbsGps;->mGotFix:Z
    invoke-static {v0}, Lcom/mediatek/lbs/em/LbsGps;->access$2600(Lcom/mediatek/lbs/em/LbsGps;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps$11;->this$0:Lcom/mediatek/lbs/em/LbsGps;

    # setter for: Lcom/mediatek/lbs/em/LbsGps;->mGotFix:Z
    invoke-static {v0, v12}, Lcom/mediatek/lbs/em/LbsGps;->access$2602(Lcom/mediatek/lbs/em/LbsGps;Z)Z

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps$11;->this$0:Lcom/mediatek/lbs/em/LbsGps;

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsGps$11;->this$0:Lcom/mediatek/lbs/em/LbsGps;

    # getter for: Lcom/mediatek/lbs/em/LbsGps;->mTimer:Lcom/mediatek/lbs/em/LbsGps$GpsTimer;
    invoke-static {v1}, Lcom/mediatek/lbs/em/LbsGps;->access$2800(Lcom/mediatek/lbs/em/LbsGps;)Lcom/mediatek/lbs/em/LbsGps$GpsTimer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/lbs/em/LbsGps$GpsTimer;->getTimer()I

    move-result v1

    # setter for: Lcom/mediatek/lbs/em/LbsGps;->mTTFF:I
    invoke-static {v0, v1}, Lcom/mediatek/lbs/em/LbsGps;->access$2702(Lcom/mediatek/lbs/em/LbsGps;I)I

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps$11;->this$0:Lcom/mediatek/lbs/em/LbsGps;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    # setter for: Lcom/mediatek/lbs/em/LbsGps;->mFirstLat:D
    invoke-static {v0, v1, v2}, Lcom/mediatek/lbs/em/LbsGps;->access$2902(Lcom/mediatek/lbs/em/LbsGps;D)D

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps$11;->this$0:Lcom/mediatek/lbs/em/LbsGps;

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v1

    # setter for: Lcom/mediatek/lbs/em/LbsGps;->mFirstLng:D
    invoke-static {v0, v1, v2}, Lcom/mediatek/lbs/em/LbsGps;->access$3002(Lcom/mediatek/lbs/em/LbsGps;D)D

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps$11;->this$0:Lcom/mediatek/lbs/em/LbsGps;

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsGps$11;->this$0:Lcom/mediatek/lbs/em/LbsGps;

    # getter for: Lcom/mediatek/lbs/em/LbsGps;->mDistance:F
    invoke-static {v1}, Lcom/mediatek/lbs/em/LbsGps;->access$2500(Lcom/mediatek/lbs/em/LbsGps;)F

    move-result v1

    # setter for: Lcom/mediatek/lbs/em/LbsGps;->mFirstDistance:F
    invoke-static {v0, v1}, Lcom/mediatek/lbs/em/LbsGps;->access$3102(Lcom/mediatek/lbs/em/LbsGps;F)F

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps$11;->this$0:Lcom/mediatek/lbs/em/LbsGps;

    # getter for: Lcom/mediatek/lbs/em/LbsGps;->mStressTest:Lcom/mediatek/lbs/em/LbsGps$StressTest;
    invoke-static {v0}, Lcom/mediatek/lbs/em/LbsGps;->access$3200(Lcom/mediatek/lbs/em/LbsGps;)Lcom/mediatek/lbs/em/LbsGps$StressTest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/lbs/em/LbsGps$StressTest;->gotFixNotify()V

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Location Count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsGps$11;->this$0:Lcom/mediatek/lbs/em/LbsGps;

    # getter for: Lcom/mediatek/lbs/em/LbsGps;->mFixCount:I
    invoke-static {v1}, Lcom/mediatek/lbs/em/LbsGps;->access$2200(Lcom/mediatek/lbs/em/LbsGps;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Lat=%.06f Lng=%.06f\n"

    new-array v2, v13, [Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v2, v11

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v2, v12

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Time tick="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "UTC time="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v1

    invoke-direct {p0, v1, v2}, Lcom/mediatek/lbs/em/LbsGps$11;->getTimeString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Altitude="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/location/Location;->getAltitude()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/location/Location;->hasAltitude()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Accuracy="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/location/Location;->hasAccuracy()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Bearing="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/location/Location;->getBearing()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/location/Location;->hasBearing()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Speed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/location/Location;->getSpeed()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/location/Location;->hasSpeed()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Distance="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsGps$11;->this$0:Lcom/mediatek/lbs/em/LbsGps;

    # getter for: Lcom/mediatek/lbs/em/LbsGps;->mDistance:F
    invoke-static {v1}, Lcom/mediatek/lbs/em/LbsGps;->access$2500(Lcom/mediatek/lbs/em/LbsGps;)F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "TTFF="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsGps$11;->this$0:Lcom/mediatek/lbs/em/LbsGps;

    # getter for: Lcom/mediatek/lbs/em/LbsGps;->mTTFF:I
    invoke-static {v1}, Lcom/mediatek/lbs/em/LbsGps;->access$2700(Lcom/mediatek/lbs/em/LbsGps;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "First Lat=%.06f Lng=%.06f\n"

    new-array v2, v13, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/mediatek/lbs/em/LbsGps$11;->this$0:Lcom/mediatek/lbs/em/LbsGps;

    # getter for: Lcom/mediatek/lbs/em/LbsGps;->mFirstLat:D
    invoke-static {v3}, Lcom/mediatek/lbs/em/LbsGps;->access$2900(Lcom/mediatek/lbs/em/LbsGps;)D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v2, v11

    iget-object v3, p0, Lcom/mediatek/lbs/em/LbsGps$11;->this$0:Lcom/mediatek/lbs/em/LbsGps;

    # getter for: Lcom/mediatek/lbs/em/LbsGps;->mFirstLng:D
    invoke-static {v3}, Lcom/mediatek/lbs/em/LbsGps;->access$3000(Lcom/mediatek/lbs/em/LbsGps;)D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v2, v12

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "First distance="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsGps$11;->this$0:Lcom/mediatek/lbs/em/LbsGps;

    # getter for: Lcom/mediatek/lbs/em/LbsGps;->mFirstDistance:F
    invoke-static {v1}, Lcom/mediatek/lbs/em/LbsGps;->access$3100(Lcom/mediatek/lbs/em/LbsGps;)F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps$11;->this$0:Lcom/mediatek/lbs/em/LbsGps;

    # getter for: Lcom/mediatek/lbs/em/LbsGps;->mTextViewLocation:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/mediatek/lbs/em/LbsGps;->access$3300(Lcom/mediatek/lbs/em/LbsGps;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v9, Landroid/content/Intent;

    const-string v0, "com.mediatek.lbs.em.fix"

    invoke-direct {v9, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v0, "lat"

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    invoke-virtual {v9, v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    const-string v0, "lng"

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v1

    invoke-virtual {v9, v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    const-string v0, "hasAcc"

    invoke-virtual {p1}, Landroid/location/Location;->hasAccuracy()Z

    move-result v1

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "acc"

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps$11;->this$0:Lcom/mediatek/lbs/em/LbsGps;

    invoke-virtual {v0, v9}, Lcom/mediatek/lbs/em/LbsGps;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Landroid/os/Bundle;

    return-void
.end method
