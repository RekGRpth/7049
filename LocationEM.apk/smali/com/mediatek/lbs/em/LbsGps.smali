.class public Lcom/mediatek/lbs/em/LbsGps;
.super Landroid/app/Activity;
.source "LbsGps.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/lbs/em/LbsGps$GpsTimer;,
        Lcom/mediatek/lbs/em/LbsGps$StressTest;
    }
.end annotation


# static fields
.field private static final STRESS_DELETE_DATA:I = 0x5

.field private static final STRESS_END:I = 0x1

.field private static final STRESS_LOOP_UPDATE:I = 0x2

.field private static final STRESS_START_GPS:I = 0x3

.field private static final STRESS_STOP_GPS:I = 0x4

.field private static final TIMER_UPDATE:I


# instance fields
.field private mButtonChangeRefLatLng:Landroid/widget/Button;

.field private mButtonDelete:Landroid/widget/Button;

.field private mButtonGps:Landroid/widget/ToggleButton;

.field private mButtonStress:Landroid/widget/ToggleButton;

.field private mCheckBoxLogNmea:Landroid/widget/CheckBox;

.field private mCheckBoxLogToSdcard:Landroid/widget/CheckBox;

.field private mDelay1:I

.field private mDelay2:I

.field private mDelay3:I

.field private mDelay4:I

.field private mDistance:F

.field private mEditTextDelay1:Landroid/widget/EditText;

.field private mEditTextDelay2:Landroid/widget/EditText;

.field private mEditTextDelay3:Landroid/widget/EditText;

.field private mEditTextDelay4:Landroid/widget/EditText;

.field private mEditTextNumOfLoop:Landroid/widget/EditText;

.field private mFirstDistance:F

.field private mFirstLat:D

.field private mFirstLng:D

.field private mFixCount:I

.field private mGotFix:Z

.field private mHandler:Landroid/os/Handler;

.field private mLocationListener:Landroid/location/LocationListener;

.field private mLocationManager:Landroid/location/LocationManager;

.field private mLogToSdcard:Z

.field private mNmeaCount:I

.field private mNmeaFileName:Ljava/lang/String;

.field private mNmeaListener:Landroid/location/GpsStatus$NmeaListener;

.field private mNumOfLoop:I

.field private mRadioButtonCold:Landroid/widget/RadioButton;

.field private mRadioButtonFull:Landroid/widget/RadioButton;

.field private mRadioButtonHot:Landroid/widget/RadioButton;

.field private mRadioButtonWarm:Landroid/widget/RadioButton;

.field private mRefLat:D

.field private mRefLng:D

.field private mSatCount:I

.field private mSatNum:I

.field private mStatusListener:Landroid/location/GpsStatus$Listener;

.field private mStressTest:Lcom/mediatek/lbs/em/LbsGps$StressTest;

.field private mStringList:Lcom/mediatek/lbs/em/UtilityStringList;

.field private mTTFF:I

.field private mTextView5:Landroid/widget/TextView;

.field private mTextViewLocation:Landroid/widget/TextView;

.field private mTextViewLoop:Landroid/widget/TextView;

.field private mTextViewNmea:Landroid/widget/TextView;

.field private mTextViewRefLatLng:Landroid/widget/TextView;

.field private mTextViewSatellite:Landroid/widget/TextView;

.field private mTextViewTimer:Landroid/widget/TextView;

.field private mTimer:Lcom/mediatek/lbs/em/LbsGps$GpsTimer;


# direct methods
.method public constructor <init>()V
    .locals 12

    const/4 v11, 0x0

    const/4 v10, 0x0

    const-wide/16 v8, 0x0

    const/4 v7, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/16 v0, 0x64

    iput v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mNumOfLoop:I

    const/16 v0, 0x14

    iput v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mDelay1:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mDelay2:I

    const/16 v0, 0x258

    iput v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mDelay3:I

    iput v7, p0, Lcom/mediatek/lbs/em/LbsGps;->mDelay4:I

    new-instance v0, Lcom/mediatek/lbs/em/LbsGps$StressTest;

    iget v2, p0, Lcom/mediatek/lbs/em/LbsGps;->mNumOfLoop:I

    iget v3, p0, Lcom/mediatek/lbs/em/LbsGps;->mDelay1:I

    iget v4, p0, Lcom/mediatek/lbs/em/LbsGps;->mDelay2:I

    iget v5, p0, Lcom/mediatek/lbs/em/LbsGps;->mDelay3:I

    iget v6, p0, Lcom/mediatek/lbs/em/LbsGps;->mDelay4:I

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/mediatek/lbs/em/LbsGps$StressTest;-><init>(Lcom/mediatek/lbs/em/LbsGps;IIIII)V

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mStressTest:Lcom/mediatek/lbs/em/LbsGps$StressTest;

    iput v7, p0, Lcom/mediatek/lbs/em/LbsGps;->mFixCount:I

    iput v7, p0, Lcom/mediatek/lbs/em/LbsGps;->mSatCount:I

    iput v7, p0, Lcom/mediatek/lbs/em/LbsGps;->mSatNum:I

    iput v7, p0, Lcom/mediatek/lbs/em/LbsGps;->mNmeaCount:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mTTFF:I

    iput-boolean v7, p0, Lcom/mediatek/lbs/em/LbsGps;->mGotFix:Z

    iput-wide v8, p0, Lcom/mediatek/lbs/em/LbsGps;->mFirstLat:D

    iput-wide v8, p0, Lcom/mediatek/lbs/em/LbsGps;->mFirstLng:D

    iput v10, p0, Lcom/mediatek/lbs/em/LbsGps;->mFirstDistance:F

    iput v10, p0, Lcom/mediatek/lbs/em/LbsGps;->mDistance:F

    iput-wide v8, p0, Lcom/mediatek/lbs/em/LbsGps;->mRefLat:D

    iput-wide v8, p0, Lcom/mediatek/lbs/em/LbsGps;->mRefLng:D

    iput-boolean v7, p0, Lcom/mediatek/lbs/em/LbsGps;->mLogToSdcard:Z

    iput-object v11, p0, Lcom/mediatek/lbs/em/LbsGps;->mNmeaFileName:Ljava/lang/String;

    iput-object v11, p0, Lcom/mediatek/lbs/em/LbsGps;->mLocationManager:Landroid/location/LocationManager;

    new-instance v0, Lcom/mediatek/lbs/em/UtilityStringList;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Lcom/mediatek/lbs/em/UtilityStringList;-><init>(I)V

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mStringList:Lcom/mediatek/lbs/em/UtilityStringList;

    new-instance v0, Lcom/mediatek/lbs/em/LbsGps$GpsTimer;

    invoke-direct {v0, p0}, Lcom/mediatek/lbs/em/LbsGps$GpsTimer;-><init>(Lcom/mediatek/lbs/em/LbsGps;)V

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mTimer:Lcom/mediatek/lbs/em/LbsGps$GpsTimer;

    new-instance v0, Lcom/mediatek/lbs/em/LbsGps$9;

    invoke-direct {v0, p0}, Lcom/mediatek/lbs/em/LbsGps$9;-><init>(Lcom/mediatek/lbs/em/LbsGps;)V

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mNmeaListener:Landroid/location/GpsStatus$NmeaListener;

    new-instance v0, Lcom/mediatek/lbs/em/LbsGps$10;

    invoke-direct {v0, p0}, Lcom/mediatek/lbs/em/LbsGps$10;-><init>(Lcom/mediatek/lbs/em/LbsGps;)V

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mStatusListener:Landroid/location/GpsStatus$Listener;

    new-instance v0, Lcom/mediatek/lbs/em/LbsGps$11;

    invoke-direct {v0, p0}, Lcom/mediatek/lbs/em/LbsGps$11;-><init>(Lcom/mediatek/lbs/em/LbsGps;)V

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mLocationListener:Landroid/location/LocationListener;

    new-instance v0, Lcom/mediatek/lbs/em/LbsGps$12;

    invoke-direct {v0, p0}, Lcom/mediatek/lbs/em/LbsGps$12;-><init>(Lcom/mediatek/lbs/em/LbsGps;)V

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/lbs/em/LbsGps;)Landroid/widget/ToggleButton;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mButtonGps:Landroid/widget/ToggleButton;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/lbs/em/LbsGps;)Z
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;

    invoke-direct {p0}, Lcom/mediatek/lbs/em/LbsGps;->isGPSProviderEnable()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1000(Lcom/mediatek/lbs/em/LbsGps;J)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;
    .param p1    # J

    invoke-direct {p0, p1, p2}, Lcom/mediatek/lbs/em/LbsGps;->getTimeString2(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Lcom/mediatek/lbs/em/LbsGps;)Z
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;

    iget-boolean v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mLogToSdcard:Z

    return v0
.end method

.method static synthetic access$1102(Lcom/mediatek/lbs/em/LbsGps;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/lbs/em/LbsGps;->mLogToSdcard:Z

    return p1
.end method

.method static synthetic access$1200(Lcom/mediatek/lbs/em/LbsGps;)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;

    invoke-direct {p0}, Lcom/mediatek/lbs/em/LbsGps;->startStressGps()V

    return-void
.end method

.method static synthetic access$1300(Lcom/mediatek/lbs/em/LbsGps;)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;

    invoke-direct {p0}, Lcom/mediatek/lbs/em/LbsGps;->stopStressGps()V

    return-void
.end method

.method static synthetic access$1400(Lcom/mediatek/lbs/em/LbsGps;)Lcom/mediatek/lbs/em/UtilityStringList;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mStringList:Lcom/mediatek/lbs/em/UtilityStringList;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/mediatek/lbs/em/LbsGps;)I
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;

    iget v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mNmeaCount:I

    return v0
.end method

.method static synthetic access$1508(Lcom/mediatek/lbs/em/LbsGps;)I
    .locals 2
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;

    iget v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mNmeaCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/mediatek/lbs/em/LbsGps;->mNmeaCount:I

    return v0
.end method

.method static synthetic access$1600(Lcom/mediatek/lbs/em/LbsGps;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mTextViewNmea:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/mediatek/lbs/em/LbsGps;)I
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;

    iget v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mSatNum:I

    return v0
.end method

.method static synthetic access$1702(Lcom/mediatek/lbs/em/LbsGps;I)I
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/lbs/em/LbsGps;->mSatNum:I

    return p1
.end method

.method static synthetic access$1708(Lcom/mediatek/lbs/em/LbsGps;)I
    .locals 2
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;

    iget v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mSatNum:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/mediatek/lbs/em/LbsGps;->mSatNum:I

    return v0
.end method

.method static synthetic access$1800(Lcom/mediatek/lbs/em/LbsGps;)Landroid/location/LocationManager;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mLocationManager:Landroid/location/LocationManager;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/mediatek/lbs/em/LbsGps;)I
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;

    iget v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mSatCount:I

    return v0
.end method

.method static synthetic access$1908(Lcom/mediatek/lbs/em/LbsGps;)I
    .locals 2
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;

    iget v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mSatCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/mediatek/lbs/em/LbsGps;->mSatCount:I

    return v0
.end method

.method static synthetic access$200(Lcom/mediatek/lbs/em/LbsGps;)Landroid/widget/ToggleButton;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mButtonStress:Landroid/widget/ToggleButton;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/mediatek/lbs/em/LbsGps;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mTextViewSatellite:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/mediatek/lbs/em/LbsGps;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/lbs/em/LbsGps;->log(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2200(Lcom/mediatek/lbs/em/LbsGps;)I
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;

    iget v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mFixCount:I

    return v0
.end method

.method static synthetic access$2208(Lcom/mediatek/lbs/em/LbsGps;)I
    .locals 2
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;

    iget v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mFixCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/mediatek/lbs/em/LbsGps;->mFixCount:I

    return v0
.end method

.method static synthetic access$2300(Lcom/mediatek/lbs/em/LbsGps;)D
    .locals 2
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;

    iget-wide v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mRefLat:D

    return-wide v0
.end method

.method static synthetic access$2302(Lcom/mediatek/lbs/em/LbsGps;D)D
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;
    .param p1    # D

    iput-wide p1, p0, Lcom/mediatek/lbs/em/LbsGps;->mRefLat:D

    return-wide p1
.end method

.method static synthetic access$2400(Lcom/mediatek/lbs/em/LbsGps;)D
    .locals 2
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;

    iget-wide v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mRefLng:D

    return-wide v0
.end method

.method static synthetic access$2402(Lcom/mediatek/lbs/em/LbsGps;D)D
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;
    .param p1    # D

    iput-wide p1, p0, Lcom/mediatek/lbs/em/LbsGps;->mRefLng:D

    return-wide p1
.end method

.method static synthetic access$2500(Lcom/mediatek/lbs/em/LbsGps;)F
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;

    iget v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mDistance:F

    return v0
.end method

.method static synthetic access$2502(Lcom/mediatek/lbs/em/LbsGps;F)F
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;
    .param p1    # F

    iput p1, p0, Lcom/mediatek/lbs/em/LbsGps;->mDistance:F

    return p1
.end method

.method static synthetic access$2600(Lcom/mediatek/lbs/em/LbsGps;)Z
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;

    iget-boolean v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mGotFix:Z

    return v0
.end method

.method static synthetic access$2602(Lcom/mediatek/lbs/em/LbsGps;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/lbs/em/LbsGps;->mGotFix:Z

    return p1
.end method

.method static synthetic access$2700(Lcom/mediatek/lbs/em/LbsGps;)I
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;

    iget v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mTTFF:I

    return v0
.end method

.method static synthetic access$2702(Lcom/mediatek/lbs/em/LbsGps;I)I
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/lbs/em/LbsGps;->mTTFF:I

    return p1
.end method

.method static synthetic access$2800(Lcom/mediatek/lbs/em/LbsGps;)Lcom/mediatek/lbs/em/LbsGps$GpsTimer;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mTimer:Lcom/mediatek/lbs/em/LbsGps$GpsTimer;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/mediatek/lbs/em/LbsGps;)D
    .locals 2
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;

    iget-wide v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mFirstLat:D

    return-wide v0
.end method

.method static synthetic access$2902(Lcom/mediatek/lbs/em/LbsGps;D)D
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;
    .param p1    # D

    iput-wide p1, p0, Lcom/mediatek/lbs/em/LbsGps;->mFirstLat:D

    return-wide p1
.end method

.method static synthetic access$300(Lcom/mediatek/lbs/em/LbsGps;)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;

    invoke-direct {p0}, Lcom/mediatek/lbs/em/LbsGps;->startGps()V

    return-void
.end method

.method static synthetic access$3000(Lcom/mediatek/lbs/em/LbsGps;)D
    .locals 2
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;

    iget-wide v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mFirstLng:D

    return-wide v0
.end method

.method static synthetic access$3002(Lcom/mediatek/lbs/em/LbsGps;D)D
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;
    .param p1    # D

    iput-wide p1, p0, Lcom/mediatek/lbs/em/LbsGps;->mFirstLng:D

    return-wide p1
.end method

.method static synthetic access$3100(Lcom/mediatek/lbs/em/LbsGps;)F
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;

    iget v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mFirstDistance:F

    return v0
.end method

.method static synthetic access$3102(Lcom/mediatek/lbs/em/LbsGps;F)F
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;
    .param p1    # F

    iput p1, p0, Lcom/mediatek/lbs/em/LbsGps;->mFirstDistance:F

    return p1
.end method

.method static synthetic access$3200(Lcom/mediatek/lbs/em/LbsGps;)Lcom/mediatek/lbs/em/LbsGps$StressTest;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mStressTest:Lcom/mediatek/lbs/em/LbsGps$StressTest;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/mediatek/lbs/em/LbsGps;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mTextViewLocation:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/mediatek/lbs/em/LbsGps;II)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;
    .param p1    # I
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/mediatek/lbs/em/LbsGps;->sendMessage(II)V

    return-void
.end method

.method static synthetic access$3500(Lcom/mediatek/lbs/em/LbsGps;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mTextViewTimer:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/mediatek/lbs/em/LbsGps;)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;

    invoke-direct {p0}, Lcom/mediatek/lbs/em/LbsGps;->enableWidgetAfterStress()V

    return-void
.end method

.method static synthetic access$3700(Lcom/mediatek/lbs/em/LbsGps;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mTextViewLoop:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$3800(Lcom/mediatek/lbs/em/LbsGps;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mTextViewRefLatLng:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$3900(Lcom/mediatek/lbs/em/LbsGps;)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;

    invoke-direct {p0}, Lcom/mediatek/lbs/em/LbsGps;->updateRefLatLng()V

    return-void
.end method

.method static synthetic access$400(Lcom/mediatek/lbs/em/LbsGps;)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;

    invoke-direct {p0}, Lcom/mediatek/lbs/em/LbsGps;->stopGps()V

    return-void
.end method

.method static synthetic access$500(Lcom/mediatek/lbs/em/LbsGps;)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;

    invoke-direct {p0}, Lcom/mediatek/lbs/em/LbsGps;->startDeletingAidingData()V

    return-void
.end method

.method static synthetic access$600(Lcom/mediatek/lbs/em/LbsGps;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/lbs/em/LbsGps;->openDialogLatlng(Z)V

    return-void
.end method

.method static synthetic access$700(Lcom/mediatek/lbs/em/LbsGps;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mCheckBoxLogNmea:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$800(Lcom/mediatek/lbs/em/LbsGps;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mCheckBoxLogToSdcard:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$900(Lcom/mediatek/lbs/em/LbsGps;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mNmeaFileName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$902(Lcom/mediatek/lbs/em/LbsGps;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsGps;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/lbs/em/LbsGps;->mNmeaFileName:Ljava/lang/String;

    return-object p1
.end method

.method private enableWidgetAfterStress()V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mStressTest:Lcom/mediatek/lbs/em/LbsGps$StressTest;

    invoke-virtual {v0}, Lcom/mediatek/lbs/em/LbsGps$StressTest;->stopStress()V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mButtonGps:Landroid/widget/ToggleButton;

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mEditTextNumOfLoop:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mEditTextDelay1:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mEditTextDelay2:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mEditTextDelay3:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mEditTextDelay4:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mButtonDelete:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mRadioButtonHot:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mRadioButtonWarm:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mRadioButtonCold:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mRadioButtonFull:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mButtonStress:Landroid/widget/ToggleButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setChecked(Z)V

    return-void
.end method

.method private getTimeString2(J)Ljava/lang/String;
    .locals 6
    .param p1    # J

    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p1, p2}, Ljava/util/Date;-><init>(J)V

    const-string v2, "%04d%02d%02d_%02d%02d%02d"

    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/util/Date;->getYear()I

    move-result v5

    add-int/lit16 v5, v5, 0x76c

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v0}, Ljava/util/Date;->getMonth()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-virtual {v0}, Ljava/util/Date;->getDate()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    invoke-virtual {v0}, Ljava/util/Date;->getHours()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    invoke-virtual {v0}, Ljava/util/Date;->getMinutes()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x5

    invoke-virtual {v0}, Ljava/util/Date;->getSeconds()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private initWidget()V
    .locals 6

    const v0, 0x7f06005d

    invoke-virtual {p0, v0}, Lcom/mediatek/lbs/em/LbsGps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mButtonGps:Landroid/widget/ToggleButton;

    const v0, 0x7f06005e

    invoke-virtual {p0, v0}, Lcom/mediatek/lbs/em/LbsGps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mButtonDelete:Landroid/widget/Button;

    const v0, 0x7f06005f

    invoke-virtual {p0, v0}, Lcom/mediatek/lbs/em/LbsGps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mButtonChangeRefLatLng:Landroid/widget/Button;

    const v0, 0x7f060063

    invoke-virtual {p0, v0}, Lcom/mediatek/lbs/em/LbsGps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mCheckBoxLogNmea:Landroid/widget/CheckBox;

    const v0, 0x7f060064

    invoke-virtual {p0, v0}, Lcom/mediatek/lbs/em/LbsGps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mCheckBoxLogToSdcard:Landroid/widget/CheckBox;

    const v0, 0x7f060060

    invoke-virtual {p0, v0}, Lcom/mediatek/lbs/em/LbsGps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mButtonStress:Landroid/widget/ToggleButton;

    const v0, 0x7f060066

    invoke-virtual {p0, v0}, Lcom/mediatek/lbs/em/LbsGps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mRadioButtonHot:Landroid/widget/RadioButton;

    const v0, 0x7f060067

    invoke-virtual {p0, v0}, Lcom/mediatek/lbs/em/LbsGps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mRadioButtonWarm:Landroid/widget/RadioButton;

    const v0, 0x7f060068

    invoke-virtual {p0, v0}, Lcom/mediatek/lbs/em/LbsGps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mRadioButtonCold:Landroid/widget/RadioButton;

    const v0, 0x7f060069

    invoke-virtual {p0, v0}, Lcom/mediatek/lbs/em/LbsGps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mRadioButtonFull:Landroid/widget/RadioButton;

    const v0, 0x7f06006a

    invoke-virtual {p0, v0}, Lcom/mediatek/lbs/em/LbsGps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mTextViewRefLatLng:Landroid/widget/TextView;

    const v0, 0x7f06006b

    invoke-virtual {p0, v0}, Lcom/mediatek/lbs/em/LbsGps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mTextViewTimer:Landroid/widget/TextView;

    const v0, 0x7f06006c

    invoke-virtual {p0, v0}, Lcom/mediatek/lbs/em/LbsGps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mTextViewLocation:Landroid/widget/TextView;

    const v0, 0x7f06006d

    invoke-virtual {p0, v0}, Lcom/mediatek/lbs/em/LbsGps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mTextViewSatellite:Landroid/widget/TextView;

    const v0, 0x7f06006e

    invoke-virtual {p0, v0}, Lcom/mediatek/lbs/em/LbsGps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mTextViewNmea:Landroid/widget/TextView;

    const v0, 0x7f06006f

    invoke-virtual {p0, v0}, Lcom/mediatek/lbs/em/LbsGps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mTextView5:Landroid/widget/TextView;

    const v0, 0x7f060061

    invoke-virtual {p0, v0}, Lcom/mediatek/lbs/em/LbsGps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mTextViewLoop:Landroid/widget/TextView;

    const v0, 0x7f060071

    invoke-virtual {p0, v0}, Lcom/mediatek/lbs/em/LbsGps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mEditTextNumOfLoop:Landroid/widget/EditText;

    const v0, 0x7f060073

    invoke-virtual {p0, v0}, Lcom/mediatek/lbs/em/LbsGps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mEditTextDelay1:Landroid/widget/EditText;

    const v0, 0x7f060075

    invoke-virtual {p0, v0}, Lcom/mediatek/lbs/em/LbsGps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mEditTextDelay2:Landroid/widget/EditText;

    const v0, 0x7f060077

    invoke-virtual {p0, v0}, Lcom/mediatek/lbs/em/LbsGps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mEditTextDelay3:Landroid/widget/EditText;

    const v0, 0x7f060079

    invoke-virtual {p0, v0}, Lcom/mediatek/lbs/em/LbsGps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mEditTextDelay4:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mTextView5:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mTextViewNmea:Landroid/widget/TextView;

    const/high16 v1, 0x41300000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mEditTextNumOfLoop:Landroid/widget/EditText;

    iget v1, p0, Lcom/mediatek/lbs/em/LbsGps;->mNumOfLoop:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mEditTextDelay1:Landroid/widget/EditText;

    iget v1, p0, Lcom/mediatek/lbs/em/LbsGps;->mDelay1:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mEditTextDelay2:Landroid/widget/EditText;

    iget v1, p0, Lcom/mediatek/lbs/em/LbsGps;->mDelay2:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mEditTextDelay3:Landroid/widget/EditText;

    iget v1, p0, Lcom/mediatek/lbs/em/LbsGps;->mDelay3:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mEditTextDelay4:Landroid/widget/EditText;

    iget v1, p0, Lcom/mediatek/lbs/em/LbsGps;->mDelay4:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mCheckBoxLogToSdcard:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsGps;->mCheckBoxLogNmea:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mTextViewRefLatLng:Landroid/widget/TextView;

    const-string v1, "Reference Lat=%.06f Lng=%.06f"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-wide v4, p0, Lcom/mediatek/lbs/em/LbsGps;->mRefLat:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-wide v4, p0, Lcom/mediatek/lbs/em/LbsGps;->mRefLng:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mButtonGps:Landroid/widget/ToggleButton;

    new-instance v1, Lcom/mediatek/lbs/em/LbsGps$3;

    invoke-direct {v1, p0}, Lcom/mediatek/lbs/em/LbsGps$3;-><init>(Lcom/mediatek/lbs/em/LbsGps;)V

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mButtonDelete:Landroid/widget/Button;

    new-instance v1, Lcom/mediatek/lbs/em/LbsGps$4;

    invoke-direct {v1, p0}, Lcom/mediatek/lbs/em/LbsGps$4;-><init>(Lcom/mediatek/lbs/em/LbsGps;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mButtonChangeRefLatLng:Landroid/widget/Button;

    new-instance v1, Lcom/mediatek/lbs/em/LbsGps$5;

    invoke-direct {v1, p0}, Lcom/mediatek/lbs/em/LbsGps$5;-><init>(Lcom/mediatek/lbs/em/LbsGps;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mCheckBoxLogNmea:Landroid/widget/CheckBox;

    new-instance v1, Lcom/mediatek/lbs/em/LbsGps$6;

    invoke-direct {v1, p0}, Lcom/mediatek/lbs/em/LbsGps$6;-><init>(Lcom/mediatek/lbs/em/LbsGps;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mCheckBoxLogToSdcard:Landroid/widget/CheckBox;

    new-instance v1, Lcom/mediatek/lbs/em/LbsGps$7;

    invoke-direct {v1, p0}, Lcom/mediatek/lbs/em/LbsGps$7;-><init>(Lcom/mediatek/lbs/em/LbsGps;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mButtonStress:Landroid/widget/ToggleButton;

    new-instance v1, Lcom/mediatek/lbs/em/LbsGps$8;

    invoke-direct {v1, p0}, Lcom/mediatek/lbs/em/LbsGps$8;-><init>(Lcom/mediatek/lbs/em/LbsGps;)V

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private isGPSProviderEnable()Z
    .locals 3

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mLocationManager:Landroid/location/LocationManager;

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v1, "GPS is disabled now, Do you want to enable it?"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "No"

    new-instance v2, Lcom/mediatek/lbs/em/LbsGps$2;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsGps$2;-><init>(Lcom/mediatek/lbs/em/LbsGps;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "Yes"

    new-instance v2, Lcom/mediatek/lbs/em/LbsGps$1;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsGps$1;-><init>(Lcom/mediatek/lbs/em/LbsGps;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "LocationEM"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private msleep(J)V
    .locals 1
    .param p1    # J

    :try_start_0
    invoke-static {p1, p2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method private openDialogLatlng(Z)V
    .locals 10
    .param p1    # Z

    new-instance v4, Landroid/app/Dialog;

    invoke-direct {v4, p0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    const-string v8, "Change Ref Position"

    invoke-virtual {v4, v8}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    const v8, 0x7f030007

    invoke-virtual {v4, v8}, Landroid/app/Dialog;->setContentView(I)V

    const v8, 0x7f06007a

    invoke-virtual {v4, v8}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/EditText;

    const v8, 0x7f06007b

    invoke-virtual {v4, v8}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/EditText;

    const v8, 0x7f06007c

    invoke-virtual {v4, v8}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    const v8, 0x7f06007d

    invoke-virtual {v4, v8}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    const v8, 0x7f06007e

    invoke-virtual {v4, v8}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iget-wide v8, p0, Lcom/mediatek/lbs/em/LbsGps;->mRefLat:D

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    const-string v8, "0123456789."

    invoke-static {v8}, Landroid/text/method/DigitsKeyListener;->getInstance(Ljava/lang/String;)Landroid/text/method/DigitsKeyListener;

    move-result-object v8

    invoke-virtual {v6, v8}, Landroid/widget/EditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    iget-wide v8, p0, Lcom/mediatek/lbs/em/LbsGps;->mRefLng:D

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    const-string v8, "0123456789."

    invoke-static {v8}, Landroid/text/method/DigitsKeyListener;->getInstance(Ljava/lang/String;)Landroid/text/method/DigitsKeyListener;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    new-instance v8, Lcom/mediatek/lbs/em/LbsGps$13;

    invoke-direct {v8, p0, v6, v7, v4}, Lcom/mediatek/lbs/em/LbsGps$13;-><init>(Lcom/mediatek/lbs/em/LbsGps;Landroid/widget/EditText;Landroid/widget/EditText;Landroid/app/Dialog;)V

    invoke-virtual {v3, v8}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v8, Lcom/mediatek/lbs/em/LbsGps$14;

    invoke-direct {v8, p0, v4}, Lcom/mediatek/lbs/em/LbsGps$14;-><init>(Lcom/mediatek/lbs/em/LbsGps;Landroid/app/Dialog;)V

    invoke-virtual {v2, v8}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v8, Lcom/mediatek/lbs/em/LbsGps$15;

    invoke-direct {v8, p0, v4}, Lcom/mediatek/lbs/em/LbsGps$15;-><init>(Lcom/mediatek/lbs/em/LbsGps;Landroid/app/Dialog;)V

    invoke-virtual {v1, v8}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v4}, Landroid/app/Dialog;->show()V

    const/4 v8, 0x1

    if-ne p1, v8, :cond_0

    new-instance v0, Landroid/widget/Button;

    invoke-direct {v0, p0}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    new-instance v5, Landroid/app/Dialog;

    invoke-direct {v5, p0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    const-string v8, "OK"

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    const-string v8, "Input lat/lng are incorrect"

    invoke-virtual {v5, v8}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v5, v0}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    invoke-virtual {v5}, Landroid/app/Dialog;->show()V

    new-instance v8, Lcom/mediatek/lbs/em/LbsGps$16;

    invoke-direct {v8, p0, v5}, Lcom/mediatek/lbs/em/LbsGps$16;-><init>(Lcom/mediatek/lbs/em/LbsGps;Landroid/app/Dialog;)V

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method private sendMessage(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    iput p1, v0, Landroid/os/Message;->what:I

    iput p2, v0, Landroid/os/Message;->arg1:I

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsGps;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method private setGpsMode(I)V
    .locals 4
    .param p1    # I

    const/4 v2, 0x1

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    if-nez p1, :cond_0

    const-string v1, "Hot Start"

    invoke-direct {p0, v1}, Lcom/mediatek/lbs/em/LbsGps;->log(Ljava/lang/String;)V

    const-string v1, "rti"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :goto_0
    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsGps;->mLocationManager:Landroid/location/LocationManager;

    const-string v2, "gps"

    const-string v3, "delete_aiding_data"

    invoke-virtual {v1, v2, v3, v0}, Landroid/location/LocationManager;->sendExtraCommand(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Z

    :goto_1
    return-void

    :cond_0
    if-ne p1, v2, :cond_1

    const-string v1, "Warm Start"

    invoke-direct {p0, v1}, Lcom/mediatek/lbs/em/LbsGps;->log(Ljava/lang/String;)V

    const-string v1, "ephemeris"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x2

    if-ne p1, v1, :cond_2

    const-string v1, "Cold Start"

    invoke-direct {p0, v1}, Lcom/mediatek/lbs/em/LbsGps;->log(Ljava/lang/String;)V

    const-string v1, "ephemeris"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "position"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "time"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "iono"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "utc"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "health"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_2
    const/4 v1, 0x3

    if-ne p1, v1, :cond_3

    const-string v1, "Full Start"

    invoke-direct {p0, v1}, Lcom/mediatek/lbs/em/LbsGps;->log(Ljava/lang/String;)V

    const-string v1, "all"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_3
    const-string v1, "WARNING: unknown reset type"

    invoke-direct {p0, v1}, Lcom/mediatek/lbs/em/LbsGps;->log(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private startDeletingAidingData()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mRadioButtonHot:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mediatek/lbs/em/LbsGps;->setGpsMode(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mRadioButtonWarm:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/mediatek/lbs/em/LbsGps;->setGpsMode(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mRadioButtonCold:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/mediatek/lbs/em/LbsGps;->setGpsMode(I)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mRadioButtonFull:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/mediatek/lbs/em/LbsGps;->setGpsMode(I)V

    goto :goto_0
.end method

.method private startGps()V
    .locals 6

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    const/4 v1, 0x0

    const-string v0, "startGps"

    invoke-direct {p0, v0}, Lcom/mediatek/lbs/em/LbsGps;->log(Ljava/lang/String;)V

    iput v1, p0, Lcom/mediatek/lbs/em/LbsGps;->mFixCount:I

    iput v1, p0, Lcom/mediatek/lbs/em/LbsGps;->mSatCount:I

    iput v1, p0, Lcom/mediatek/lbs/em/LbsGps;->mSatNum:I

    iput v1, p0, Lcom/mediatek/lbs/em/LbsGps;->mNmeaCount:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mTTFF:I

    iput-boolean v1, p0, Lcom/mediatek/lbs/em/LbsGps;->mGotFix:Z

    iput-wide v2, p0, Lcom/mediatek/lbs/em/LbsGps;->mFirstLat:D

    iput-wide v2, p0, Lcom/mediatek/lbs/em/LbsGps;->mFirstLng:D

    iput v4, p0, Lcom/mediatek/lbs/em/LbsGps;->mFirstDistance:F

    iput v4, p0, Lcom/mediatek/lbs/em/LbsGps;->mDistance:F

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mStringList:Lcom/mediatek/lbs/em/UtilityStringList;

    invoke-virtual {v0}, Lcom/mediatek/lbs/em/UtilityStringList;->clear()V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mTextViewTimer:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mTextViewLocation:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mTextViewSatellite:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mTextViewNmea:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v0, Lcom/mediatek/lbs/em/LbsGps$GpsTimer;

    invoke-direct {v0, p0}, Lcom/mediatek/lbs/em/LbsGps$GpsTimer;-><init>(Lcom/mediatek/lbs/em/LbsGps;)V

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mTimer:Lcom/mediatek/lbs/em/LbsGps$GpsTimer;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mTimer:Lcom/mediatek/lbs/em/LbsGps$GpsTimer;

    invoke-virtual {v0}, Lcom/mediatek/lbs/em/LbsGps$GpsTimer;->startTimer()V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mLocationManager:Landroid/location/LocationManager;

    const-string v1, "gps"

    const-wide/16 v2, 0x3e8

    iget-object v5, p0, Lcom/mediatek/lbs/em/LbsGps;->mLocationListener:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    return-void
.end method

.method private startStressGps()V
    .locals 9

    const/4 v8, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mEditTextDelay3:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v7

    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    invoke-virtual {p0}, Lcom/mediatek/lbs/em/LbsGps;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "ERR: Parameters error!!"

    invoke-static {v0, v1, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mButtonStress:Landroid/widget/ToggleButton;

    invoke-virtual {v0, v8}, Landroid/widget/ToggleButton;->setChecked(Z)V

    :goto_0
    return-void

    :cond_0
    :try_start_1
    new-instance v0, Lcom/mediatek/lbs/em/LbsGps$StressTest;

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsGps;->mEditTextNumOfLoop:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsGps;->mEditTextDelay1:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsGps;->mEditTextDelay2:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsGps;->mEditTextDelay3:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsGps;->mEditTextDelay4:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v6

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/mediatek/lbs/em/LbsGps$StressTest;-><init>(Lcom/mediatek/lbs/em/LbsGps;IIIII)V

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mStressTest:Lcom/mediatek/lbs/em/LbsGps$StressTest;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mStressTest:Lcom/mediatek/lbs/em/LbsGps$StressTest;

    invoke-virtual {v0}, Lcom/mediatek/lbs/em/LbsGps$StressTest;->startStress()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mButtonGps:Landroid/widget/ToggleButton;

    invoke-virtual {v0, v8}, Landroid/widget/ToggleButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mEditTextNumOfLoop:Landroid/widget/EditText;

    invoke-virtual {v0, v8}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mEditTextDelay1:Landroid/widget/EditText;

    invoke-virtual {v0, v8}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mEditTextDelay2:Landroid/widget/EditText;

    invoke-virtual {v0, v8}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mEditTextDelay3:Landroid/widget/EditText;

    invoke-virtual {v0, v8}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mEditTextDelay4:Landroid/widget/EditText;

    invoke-virtual {v0, v8}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mButtonDelete:Landroid/widget/Button;

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mRadioButtonHot:Landroid/widget/RadioButton;

    invoke-virtual {v0, v8}, Landroid/widget/RadioButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mRadioButtonWarm:Landroid/widget/RadioButton;

    invoke-virtual {v0, v8}, Landroid/widget/RadioButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mRadioButtonCold:Landroid/widget/RadioButton;

    invoke-virtual {v0, v8}, Landroid/widget/RadioButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mRadioButtonFull:Landroid/widget/RadioButton;

    invoke-virtual {v0, v8}, Landroid/widget/RadioButton;->setEnabled(Z)V

    goto/16 :goto_0
.end method

.method private stopGps()V
    .locals 2

    const-string v0, "stopGps"

    invoke-direct {p0, v0}, Lcom/mediatek/lbs/em/LbsGps;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mTimer:Lcom/mediatek/lbs/em/LbsGps$GpsTimer;

    invoke-virtual {v0}, Lcom/mediatek/lbs/em/LbsGps$GpsTimer;->stopTimer()V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mLocationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsGps;->mLocationListener:Landroid/location/LocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    return-void
.end method

.method private stopStressGps()V
    .locals 0

    invoke-direct {p0}, Lcom/mediatek/lbs/em/LbsGps;->enableWidgetAfterStress()V

    return-void
.end method

.method private updateRefLatLng()V
    .locals 7

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsGps;->mLocationManager:Landroid/location/LocationManager;

    const-string v2, "gps"

    invoke-virtual {v1, v2}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    iput-wide v1, p0, Lcom/mediatek/lbs/em/LbsGps;->mRefLat:D

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v1

    iput-wide v1, p0, Lcom/mediatek/lbs/em/LbsGps;->mRefLng:D

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsGps;->mTextViewRefLatLng:Landroid/widget/TextView;

    const-string v2, "Reference Lat=%.06f Lng=%.06f"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-wide v5, p0, Lcom/mediatek/lbs/em/LbsGps;->mRefLat:D

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-wide v5, p0, Lcom/mediatek/lbs/em/LbsGps;->mRefLng:D

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030006

    invoke-virtual {p0, v0}, Lcom/mediatek/lbs/em/LbsGps;->setContentView(I)V

    const-string v0, "location"

    invoke-virtual {p0, v0}, Lcom/mediatek/lbs/em/LbsGps;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mLocationManager:Landroid/location/LocationManager;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mLocationManager:Landroid/location/LocationManager;

    if-nez v0, :cond_0

    const-string v0, "ERR: mLocationManager is null"

    invoke-direct {p0, v0}, Lcom/mediatek/lbs/em/LbsGps;->log(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mLocationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsGps;->mNmeaListener:Landroid/location/GpsStatus$NmeaListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->addNmeaListener(Landroid/location/GpsStatus$NmeaListener;)Z

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mLocationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsGps;->mStatusListener:Landroid/location/GpsStatus$Listener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->addGpsStatusListener(Landroid/location/GpsStatus$Listener;)Z

    invoke-direct {p0}, Lcom/mediatek/lbs/em/LbsGps;->initWidget()V

    invoke-direct {p0}, Lcom/mediatek/lbs/em/LbsGps;->updateRefLatLng()V

    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/mediatek/lbs/em/LbsGps;->getTimeString2(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mNmeaFileName:Ljava/lang/String;

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mLocationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsGps;->mNmeaListener:Landroid/location/GpsStatus$NmeaListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeNmeaListener(Landroid/location/GpsStatus$NmeaListener;)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mLocationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsGps;->mLocationListener:Landroid/location/LocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mLocationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsGps;->mStatusListener:Landroid/location/GpsStatus$Listener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeGpsStatusListener(Landroid/location/GpsStatus$Listener;)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mTimer:Lcom/mediatek/lbs/em/LbsGps$GpsTimer;

    invoke-virtual {v0}, Lcom/mediatek/lbs/em/LbsGps$GpsTimer;->stopTimer()V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsGps;->mStressTest:Lcom/mediatek/lbs/em/LbsGps$StressTest;

    invoke-virtual {v0}, Lcom/mediatek/lbs/em/LbsGps$StressTest;->stopStress()V

    return-void
.end method

.method public write2File(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Z
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Z
    .param p5    # Z

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-eqz p5, :cond_0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "/sdcard/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->mkdir()Z

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    if-eqz p4, :cond_1

    :try_start_0
    new-instance v0, Ljava/io/DataOutputStream;

    new-instance v6, Ljava/io/FileOutputStream;

    const/4 v7, 0x1

    invoke-direct {v6, v3, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;Z)V

    invoke-direct {v0, v6}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    :goto_1
    invoke-virtual {v0, p3}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move v4, v5

    :goto_2
    return v4

    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "/data/data/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/mediatek/lbs/em/LbsGps;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_1
    :try_start_1
    new-instance v0, Ljava/io/DataOutputStream;

    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v6}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_2

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2
.end method
