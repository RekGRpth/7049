.class public Lcom/mediatek/lbs/em/LbsMisc;
.super Landroid/app/Activity;
.source "LbsMisc.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/lbs/em/LbsMisc$CellStateListener;,
        Lcom/mediatek/lbs/em/LbsMisc$ConnectionThread;
    }
.end annotation


# instance fields
.field SERVER_IP:[Ljava/lang/String;

.field SERVER_NAME:[Ljava/lang/String;

.field SERVER_PORT:[Ljava/lang/String;

.field private mButtonConnect:Landroid/widget/ToggleButton;

.field private mButton_AGPS:Landroid/widget/Button;

.field private mButton_APN:Landroid/widget/Button;

.field private mButton_GPS:Landroid/widget/Button;

.field private mButton_TIME:Landroid/widget/Button;

.field private mButton_WIFI:Landroid/widget/Button;

.field private mButton_YGPS:Landroid/widget/Button;

.field private mConnectionThread:Lcom/mediatek/lbs/em/LbsMisc$ConnectionThread;

.field private mEditTextIp:Landroid/widget/EditText;

.field private mEditTextPort:Landroid/widget/EditText;

.field private mHandler:Landroid/os/Handler;

.field private mSessionId:I

.field private mSpinnerServer:Landroid/widget/Spinner;

.field private mStringList:Lcom/mediatek/lbs/em/UtilityStringList;

.field private mTelephonyMgr:Landroid/telephony/TelephonyManager;

.field private mTextViewCellInfo:Landroid/widget/TextView;

.field private mTextViewResult:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "LOCAL"

    aput-object v1, v0, v2

    const-string v1, "CMCC"

    aput-object v1, v0, v3

    const-string v1, "GOOGLE"

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsMisc;->SERVER_NAME:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "127.0.0.1"

    aput-object v1, v0, v2

    const-string v1, "221.176.0.55"

    aput-object v1, v0, v3

    const-string v1, "supl.google.com"

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsMisc;->SERVER_IP:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "7276"

    aput-object v1, v0, v2

    const-string v1, "7275"

    aput-object v1, v0, v3

    const-string v1, "7275"

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsMisc;->SERVER_PORT:[Ljava/lang/String;

    iput v2, p0, Lcom/mediatek/lbs/em/LbsMisc;->mSessionId:I

    new-instance v0, Lcom/mediatek/lbs/em/UtilityStringList;

    const/16 v1, 0xc

    invoke-direct {v0, v1}, Lcom/mediatek/lbs/em/UtilityStringList;-><init>(I)V

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsMisc;->mStringList:Lcom/mediatek/lbs/em/UtilityStringList;

    new-instance v0, Lcom/mediatek/lbs/em/LbsMisc$9;

    invoke-direct {v0, p0}, Lcom/mediatek/lbs/em/LbsMisc$9;-><init>(Lcom/mediatek/lbs/em/LbsMisc;)V

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsMisc;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/lbs/em/LbsMisc;)Landroid/widget/Spinner;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsMisc;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsMisc;->mSpinnerServer:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/lbs/em/LbsMisc;)Landroid/widget/EditText;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsMisc;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsMisc;->mEditTextIp:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/mediatek/lbs/em/LbsMisc;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsMisc;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsMisc;->mTextViewResult:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/mediatek/lbs/em/LbsMisc;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsMisc;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/lbs/em/LbsMisc;->log(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/mediatek/lbs/em/LbsMisc;)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsMisc;

    invoke-direct {p0}, Lcom/mediatek/lbs/em/LbsMisc;->updateCellInfo()V

    return-void
.end method

.method static synthetic access$200(Lcom/mediatek/lbs/em/LbsMisc;)Landroid/widget/EditText;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsMisc;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsMisc;->mEditTextPort:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/lbs/em/LbsMisc;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsMisc;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/mediatek/lbs/em/LbsMisc;->startComponent(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/mediatek/lbs/em/LbsMisc;)Landroid/widget/ToggleButton;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsMisc;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsMisc;->mButtonConnect:Landroid/widget/ToggleButton;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mediatek/lbs/em/LbsMisc;)Lcom/mediatek/lbs/em/LbsMisc$ConnectionThread;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsMisc;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsMisc;->mConnectionThread:Lcom/mediatek/lbs/em/LbsMisc$ConnectionThread;

    return-object v0
.end method

.method static synthetic access$502(Lcom/mediatek/lbs/em/LbsMisc;Lcom/mediatek/lbs/em/LbsMisc$ConnectionThread;)Lcom/mediatek/lbs/em/LbsMisc$ConnectionThread;
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsMisc;
    .param p1    # Lcom/mediatek/lbs/em/LbsMisc$ConnectionThread;

    iput-object p1, p0, Lcom/mediatek/lbs/em/LbsMisc;->mConnectionThread:Lcom/mediatek/lbs/em/LbsMisc$ConnectionThread;

    return-object p1
.end method

.method static synthetic access$600(Lcom/mediatek/lbs/em/LbsMisc;)I
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsMisc;

    iget v0, p0, Lcom/mediatek/lbs/em/LbsMisc;->mSessionId:I

    return v0
.end method

.method static synthetic access$608(Lcom/mediatek/lbs/em/LbsMisc;)I
    .locals 2
    .param p0    # Lcom/mediatek/lbs/em/LbsMisc;

    iget v0, p0, Lcom/mediatek/lbs/em/LbsMisc;->mSessionId:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/mediatek/lbs/em/LbsMisc;->mSessionId:I

    return v0
.end method

.method static synthetic access$800(Lcom/mediatek/lbs/em/LbsMisc;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsMisc;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsMisc;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$900(Lcom/mediatek/lbs/em/LbsMisc;)Lcom/mediatek/lbs/em/UtilityStringList;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsMisc;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsMisc;->mStringList:Lcom/mediatek/lbs/em/UtilityStringList;

    return-object v0
.end method

.method private getLocalIpAddress()Ljava/lang/String;
    .locals 8

    :try_start_0
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/net/NetworkInterface;

    invoke-virtual {v4}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/net/InetAddress;

    invoke-virtual {v3}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {v3}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    const/16 v7, 0x10

    if-gt v6, v7, :cond_1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IP="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/mediatek/lbs/em/LbsMisc;->log(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v5

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_2
    const/4 v5, 0x0

    goto :goto_0
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "LocationEM"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private startComponent(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    :try_start_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    new-instance v2, Landroid/content/ComponentName;

    invoke-direct {v2, p1, p2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsMisc;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ERR: startComponent failed fullActivityName="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/lbs/em/LbsMisc;->log(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mediatek/lbs/em/LbsMisc;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "ERR: startComponent failed!!"

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private updateCellInfo()V
    .locals 8

    iget-object v5, p0, Lcom/mediatek/lbs/em/LbsMisc;->mTelephonyMgr:Landroid/telephony/TelephonyManager;

    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/lbs/em/LbsMisc;->mTelephonyMgr:Landroid/telephony/TelephonyManager;

    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getCellLocation()Landroid/telephony/CellLocation;

    move-result-object v0

    check-cast v0, Landroid/telephony/gsm/GsmCellLocation;

    const/4 v1, -0x1

    const/4 v3, -0x1

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/telephony/gsm/GsmCellLocation;->getCid()I

    move-result v1

    invoke-virtual {v0}, Landroid/telephony/gsm/GsmCellLocation;->getLac()I

    move-result v3

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/lbs/em/LbsMisc;->getLocalIpAddress()Ljava/lang/String;

    move-result-object v2

    iget-object v5, p0, Lcom/mediatek/lbs/em/LbsMisc;->mTextViewCellInfo:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mccMnc=["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] lac=["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] cid=["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "IP="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v1, 0x7f03000c

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsMisc;->setContentView(I)V

    const v1, 0x7f060084

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsMisc;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsMisc;->mButton_YGPS:Landroid/widget/Button;

    const v1, 0x7f060087

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsMisc;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsMisc;->mButton_AGPS:Landroid/widget/Button;

    const v1, 0x7f060085

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsMisc;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsMisc;->mButton_TIME:Landroid/widget/Button;

    const v1, 0x7f060083

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsMisc;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsMisc;->mButton_GPS:Landroid/widget/Button;

    const v1, 0x7f060086

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsMisc;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsMisc;->mButton_APN:Landroid/widget/Button;

    const v1, 0x7f060088

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsMisc;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsMisc;->mButton_WIFI:Landroid/widget/Button;

    const v1, 0x7f060089

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsMisc;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsMisc;->mTextViewCellInfo:Landroid/widget/TextView;

    const v1, 0x7f06008a

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsMisc;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsMisc;->mSpinnerServer:Landroid/widget/Spinner;

    const v1, 0x7f06008b

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsMisc;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsMisc;->mEditTextIp:Landroid/widget/EditText;

    const v1, 0x7f06008c

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsMisc;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsMisc;->mEditTextPort:Landroid/widget/EditText;

    const v1, 0x7f06008d

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsMisc;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ToggleButton;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsMisc;->mButtonConnect:Landroid/widget/ToggleButton;

    const v1, 0x7f06008e

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsMisc;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsMisc;->mTextViewResult:Landroid/widget/TextView;

    new-instance v0, Landroid/widget/ArrayAdapter;

    const v1, 0x1090008

    iget-object v2, p0, Lcom/mediatek/lbs/em/LbsMisc;->SERVER_NAME:[Ljava/lang/String;

    invoke-direct {v0, p0, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    const v1, 0x1090009

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsMisc;->mSpinnerServer:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    const-string v1, "phone"

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsMisc;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsMisc;->mTelephonyMgr:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsMisc;->mTelephonyMgr:Landroid/telephony/TelephonyManager;

    new-instance v2, Lcom/mediatek/lbs/em/LbsMisc$CellStateListener;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsMisc$CellStateListener;-><init>(Lcom/mediatek/lbs/em/LbsMisc;)V

    const/16 v3, 0x71

    invoke-virtual {v1, v2, v3}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    invoke-direct {p0}, Lcom/mediatek/lbs/em/LbsMisc;->updateCellInfo()V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsMisc;->mSpinnerServer:Landroid/widget/Spinner;

    new-instance v2, Lcom/mediatek/lbs/em/LbsMisc$1;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsMisc$1;-><init>(Lcom/mediatek/lbs/em/LbsMisc;)V

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsMisc;->mButton_YGPS:Landroid/widget/Button;

    new-instance v2, Lcom/mediatek/lbs/em/LbsMisc$2;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsMisc$2;-><init>(Lcom/mediatek/lbs/em/LbsMisc;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsMisc;->mButton_AGPS:Landroid/widget/Button;

    new-instance v2, Lcom/mediatek/lbs/em/LbsMisc$3;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsMisc$3;-><init>(Lcom/mediatek/lbs/em/LbsMisc;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsMisc;->mButton_TIME:Landroid/widget/Button;

    new-instance v2, Lcom/mediatek/lbs/em/LbsMisc$4;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsMisc$4;-><init>(Lcom/mediatek/lbs/em/LbsMisc;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsMisc;->mButton_GPS:Landroid/widget/Button;

    new-instance v2, Lcom/mediatek/lbs/em/LbsMisc$5;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsMisc$5;-><init>(Lcom/mediatek/lbs/em/LbsMisc;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsMisc;->mButton_APN:Landroid/widget/Button;

    new-instance v2, Lcom/mediatek/lbs/em/LbsMisc$6;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsMisc$6;-><init>(Lcom/mediatek/lbs/em/LbsMisc;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsMisc;->mButton_WIFI:Landroid/widget/Button;

    new-instance v2, Lcom/mediatek/lbs/em/LbsMisc$7;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsMisc$7;-><init>(Lcom/mediatek/lbs/em/LbsMisc;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsMisc;->mButtonConnect:Landroid/widget/ToggleButton;

    new-instance v2, Lcom/mediatek/lbs/em/LbsMisc$8;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/LbsMisc$8;-><init>(Lcom/mediatek/lbs/em/LbsMisc;)V

    invoke-virtual {v1, v2}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method
