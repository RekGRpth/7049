.class public Lcom/mediatek/lbs/em/LbsView;
.super Landroid/app/Activity;
.source "LbsView.java"


# instance fields
.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mLocationManager:Landroid/location/LocationManager;

.field private mStatusListener:Landroid/location/GpsStatus$Listener;

.field private mViewGps:Lcom/mediatek/lbs/em/ViewGps;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsView;->mLocationManager:Landroid/location/LocationManager;

    new-instance v0, Lcom/mediatek/lbs/em/LbsView$1;

    invoke-direct {v0, p0}, Lcom/mediatek/lbs/em/LbsView$1;-><init>(Lcom/mediatek/lbs/em/LbsView;)V

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsView;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/mediatek/lbs/em/LbsView$2;

    invoke-direct {v0, p0}, Lcom/mediatek/lbs/em/LbsView$2;-><init>(Lcom/mediatek/lbs/em/LbsView;)V

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsView;->mStatusListener:Landroid/location/GpsStatus$Listener;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/lbs/em/LbsView;)Lcom/mediatek/lbs/em/ViewGps;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsView;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsView;->mViewGps:Lcom/mediatek/lbs/em/ViewGps;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/lbs/em/LbsView;)Landroid/location/LocationManager;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsView;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsView;->mLocationManager:Landroid/location/LocationManager;

    return-object v0
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "LocationEM"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v1, 0x7f03000e

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsView;->setContentView(I)V

    const v1, 0x7f06008f

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/mediatek/lbs/em/ViewGps;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsView;->mViewGps:Lcom/mediatek/lbs/em/ViewGps;

    const-string v1, "location"

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/LbsView;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/location/LocationManager;

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsView;->mLocationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsView;->mLocationManager:Landroid/location/LocationManager;

    if-nez v1, :cond_0

    const-string v1, "ERR: mLocationManager is null"

    invoke-direct {p0, v1}, Lcom/mediatek/lbs/em/LbsView;->log(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsView;->mLocationManager:Landroid/location/LocationManager;

    iget-object v2, p0, Lcom/mediatek/lbs/em/LbsView;->mStatusListener:Landroid/location/GpsStatus$Listener;

    invoke-virtual {v1, v2}, Landroid/location/LocationManager;->addGpsStatusListener(Landroid/location/GpsStatus$Listener;)Z

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.location.GPS_ENABLED_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsView;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/mediatek/lbs/em/LbsView;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsView;->mLocationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsView;->mStatusListener:Landroid/location/GpsStatus$Listener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeGpsStatusListener(Landroid/location/GpsStatus$Listener;)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsView;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/mediatek/lbs/em/LbsView;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method
