.class Lcom/mediatek/lbs/em/NetworkLocationEM$1;
.super Ljava/lang/Object;
.source "NetworkLocationEM.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/lbs/em/NetworkLocationEM;->initializeQueryView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;


# direct methods
.method constructor <init>(Lcom/mediatek/lbs/em/NetworkLocationEM;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$1;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$1;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    const/4 v2, 0x4

    # invokes: Lcom/mediatek/lbs/em/NetworkLocationEM;->changeTab(I)V
    invoke-static {v1, v2}, Lcom/mediatek/lbs/em/NetworkLocationEM;->access$500(Lcom/mediatek/lbs/em/NetworkLocationEM;I)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.mediatek.lbs.em.fix"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "lat"

    iget-object v2, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$1;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    # getter for: Lcom/mediatek/lbs/em/NetworkLocationEM;->mLocation:Landroid/location/Location;
    invoke-static {v2}, Lcom/mediatek/lbs/em/NetworkLocationEM;->access$600(Lcom/mediatek/lbs/em/NetworkLocationEM;)Landroid/location/Location;

    move-result-object v2

    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    const-string v1, "lng"

    iget-object v2, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$1;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    # getter for: Lcom/mediatek/lbs/em/NetworkLocationEM;->mLocation:Landroid/location/Location;
    invoke-static {v2}, Lcom/mediatek/lbs/em/NetworkLocationEM;->access$600(Lcom/mediatek/lbs/em/NetworkLocationEM;)Landroid/location/Location;

    move-result-object v2

    invoke-virtual {v2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    const-string v1, "hasAcc"

    iget-object v2, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$1;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    # getter for: Lcom/mediatek/lbs/em/NetworkLocationEM;->mLocation:Landroid/location/Location;
    invoke-static {v2}, Lcom/mediatek/lbs/em/NetworkLocationEM;->access$600(Lcom/mediatek/lbs/em/NetworkLocationEM;)Landroid/location/Location;

    move-result-object v2

    invoke-virtual {v2}, Landroid/location/Location;->hasAccuracy()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "acc"

    iget-object v2, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$1;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    # getter for: Lcom/mediatek/lbs/em/NetworkLocationEM;->mLocation:Landroid/location/Location;
    invoke-static {v2}, Lcom/mediatek/lbs/em/NetworkLocationEM;->access$600(Lcom/mediatek/lbs/em/NetworkLocationEM;)Landroid/location/Location;

    move-result-object v2

    invoke-virtual {v2}, Landroid/location/Location;->getAccuracy()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    const-string v1, "needGoTo"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$1;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    invoke-virtual {v1, v0}, Lcom/mediatek/lbs/em/NetworkLocationEM;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method
