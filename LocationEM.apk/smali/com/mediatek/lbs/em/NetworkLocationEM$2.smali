.class Lcom/mediatek/lbs/em/NetworkLocationEM$2;
.super Landroid/os/Handler;
.source "NetworkLocationEM.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/lbs/em/NetworkLocationEM;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;


# direct methods
.method constructor <init>(Lcom/mediatek/lbs/em/NetworkLocationEM;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$2;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1    # Landroid/os/Message;

    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$2;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    # invokes: Lcom/mediatek/lbs/em/NetworkLocationEM;->handleGeocodingSucessfully(Ljava/util/List;)V
    invoke-static {v1, v0}, Lcom/mediatek/lbs/em/NetworkLocationEM;->access$700(Lcom/mediatek/lbs/em/NetworkLocationEM;Ljava/util/List;)V

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$2;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    # invokes: Lcom/mediatek/lbs/em/NetworkLocationEM;->handleGeocodingFailed()V
    invoke-static {v0}, Lcom/mediatek/lbs/em/NetworkLocationEM;->access$800(Lcom/mediatek/lbs/em/NetworkLocationEM;)V

    goto :goto_0

    :sswitch_2
    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$2;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    # invokes: Lcom/mediatek/lbs/em/NetworkLocationEM;->handleSendSucessfully(Ljava/lang/String;)V
    invoke-static {v1, v0}, Lcom/mediatek/lbs/em/NetworkLocationEM;->access$900(Lcom/mediatek/lbs/em/NetworkLocationEM;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_3
    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$2;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    # invokes: Lcom/mediatek/lbs/em/NetworkLocationEM;->handleSendFailed(Ljava/lang/String;)V
    invoke-static {v1, v0}, Lcom/mediatek/lbs/em/NetworkLocationEM;->access$1000(Lcom/mediatek/lbs/em/NetworkLocationEM;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_2
        0x5 -> :sswitch_3
        0xf -> :sswitch_0
        0x10 -> :sswitch_1
    .end sparse-switch
.end method
