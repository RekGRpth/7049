.class Lcom/mediatek/lbs/em/NetworkLocationEM$3;
.super Ljava/lang/Object;
.source "NetworkLocationEM.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/lbs/em/NetworkLocationEM;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;


# direct methods
.method constructor <init>(Lcom/mediatek/lbs/em/NetworkLocationEM;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$3;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$3;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    # getter for: Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnCellQuery:Landroid/widget/Button;
    invoke-static {v1}, Lcom/mediatek/lbs/em/NetworkLocationEM;->access$1100(Lcom/mediatek/lbs/em/NetworkLocationEM;)Landroid/widget/Button;

    move-result-object v1

    if-ne p1, v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$3;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    const-string v2, "connectivity"

    invoke-virtual {v1, v2}, Lcom/mediatek/lbs/em/NetworkLocationEM;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    if-nez v1, :cond_1

    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$3;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v2, "Notice"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const-string v2, "Please enable network connection(WIFI/GPRS) before using this service"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const-string v2, "Yes"

    new-instance v3, Lcom/mediatek/lbs/em/NetworkLocationEM$3$1;

    invoke-direct {v3, p0}, Lcom/mediatek/lbs/em/NetworkLocationEM$3$1;-><init>(Lcom/mediatek/lbs/em/NetworkLocationEM$3;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$3;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    # invokes: Lcom/mediatek/lbs/em/NetworkLocationEM;->getCellInfo()V
    invoke-static {v1}, Lcom/mediatek/lbs/em/NetworkLocationEM;->access$1200(Lcom/mediatek/lbs/em/NetworkLocationEM;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$3;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    # getter for: Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnSIM1Info:Landroid/widget/Button;
    invoke-static {v1}, Lcom/mediatek/lbs/em/NetworkLocationEM;->access$1300(Lcom/mediatek/lbs/em/NetworkLocationEM;)Landroid/widget/Button;

    move-result-object v1

    if-ne p1, v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$3;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    const/4 v2, 0x0

    # invokes: Lcom/mediatek/lbs/em/NetworkLocationEM;->getSIMInfo(I)V
    invoke-static {v1, v2}, Lcom/mediatek/lbs/em/NetworkLocationEM;->access$1400(Lcom/mediatek/lbs/em/NetworkLocationEM;I)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$3;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    # getter for: Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnSIM2Info:Landroid/widget/Button;
    invoke-static {v1}, Lcom/mediatek/lbs/em/NetworkLocationEM;->access$1500(Lcom/mediatek/lbs/em/NetworkLocationEM;)Landroid/widget/Button;

    move-result-object v1

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$3;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    const/4 v2, 0x1

    # invokes: Lcom/mediatek/lbs/em/NetworkLocationEM;->getSIMInfo(I)V
    invoke-static {v1, v2}, Lcom/mediatek/lbs/em/NetworkLocationEM;->access$1400(Lcom/mediatek/lbs/em/NetworkLocationEM;I)V

    goto :goto_0
.end method
