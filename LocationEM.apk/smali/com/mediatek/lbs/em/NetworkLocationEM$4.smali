.class Lcom/mediatek/lbs/em/NetworkLocationEM$4;
.super Ljava/lang/Thread;
.source "NetworkLocationEM.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/lbs/em/NetworkLocationEM;->handleSendSucessfully(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

.field final synthetic val$innnerLocation:Landroid/location/Location;


# direct methods
.method constructor <init>(Lcom/mediatek/lbs/em/NetworkLocationEM;Landroid/location/Location;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$4;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    iput-object p2, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$4;->val$innnerLocation:Landroid/location/Location;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    const/4 v10, 0x2

    const/4 v9, 0x0

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    const-string v0, "NetworkLocationEM"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "innnerLocation is: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$4;->val$innnerLocation:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$4;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    # getter for: Lcom/mediatek/lbs/em/NetworkLocationEM;->mGeo:Landroid/location/Geocoder;
    invoke-static {v0}, Lcom/mediatek/lbs/em/NetworkLocationEM;->access$1600(Lcom/mediatek/lbs/em/NetworkLocationEM;)Landroid/location/Geocoder;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$4;->val$innnerLocation:Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    iget-object v3, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$4;->val$innnerLocation:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    const/4 v5, 0x5

    invoke-virtual/range {v0 .. v5}, Landroid/location/Geocoder;->getFromLocation(DDI)Ljava/util/List;

    move-result-object v6

    const-string v1, "NetworkLocationEM"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addressList is: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz v6, :cond_0

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v6, :cond_1

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v8

    const/16 v0, 0xf

    iput v0, v8, Landroid/os/Message;->what:I

    iput-object v6, v8, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v0, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$4;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    # getter for: Lcom/mediatek/lbs/em/NetworkLocationEM;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/mediatek/lbs/em/NetworkLocationEM;->access$1700(Lcom/mediatek/lbs/em/NetworkLocationEM;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :goto_1
    return-void

    :cond_0
    const-string v0, "null"

    goto :goto_0

    :cond_1
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v8

    const/16 v0, 0x10

    iput v0, v8, Landroid/os/Message;->what:I

    const/4 v0, 0x0

    iput-object v0, v8, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v0, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$4;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    # getter for: Lcom/mediatek/lbs/em/NetworkLocationEM;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/mediatek/lbs/em/NetworkLocationEM;->access$1700(Lcom/mediatek/lbs/em/NetworkLocationEM;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    :catch_0
    move-exception v7

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v8

    iput v10, v8, Landroid/os/Message;->what:I

    iput-object v9, v8, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v0, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$4;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    # getter for: Lcom/mediatek/lbs/em/NetworkLocationEM;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/mediatek/lbs/em/NetworkLocationEM;->access$1700(Lcom/mediatek/lbs/em/NetworkLocationEM;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    :catch_1
    move-exception v7

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v8

    iput v10, v8, Landroid/os/Message;->what:I

    iput-object v9, v8, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v0, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$4;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    # getter for: Lcom/mediatek/lbs/em/NetworkLocationEM;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/mediatek/lbs/em/NetworkLocationEM;->access$1700(Lcom/mediatek/lbs/em/NetworkLocationEM;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1
.end method
