.class public Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;
.super Landroid/widget/BaseAdapter;
.source "NetworkLocationEM.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/lbs/em/NetworkLocationEM;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MyAdapter"
.end annotation


# instance fields
.field public mInflater:Landroid/view/LayoutInflater;

.field private mListData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;


# direct methods
.method public constructor <init>(Lcom/mediatek/lbs/em/NetworkLocationEM;Landroid/content/Context;)V
    .locals 1
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    # getter for: Lcom/mediatek/lbs/em/NetworkLocationEM;->mListData:Ljava/util/List;
    invoke-static {p1}, Lcom/mediatek/lbs/em/NetworkLocationEM;->access$000(Lcom/mediatek/lbs/em/NetworkLocationEM;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->mListData:Ljava/util/List;

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->mInflater:Landroid/view/LayoutInflater;

    return-void
.end method

.method static synthetic access$200(Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;I)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->showOnListItemClick(I)V

    return-void
.end method

.method static synthetic access$400(Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;II[Ljava/lang/String;[I)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;
    .param p1    # I
    .param p2    # I
    .param p3    # [Ljava/lang/String;
    .param p4    # [I

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->performActionAfterClick(II[Ljava/lang/String;[I)V

    return-void
.end method

.method private getItemInfo(ILjava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[I)I
    .locals 8
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;
    .param p4    # [Ljava/lang/String;
    .param p5    # [I

    const/4 v4, -0x1

    if-nez p1, :cond_2

    const-string v5, "NetworkLocationEM"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "showOnListItemClick has been called: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    invoke-virtual {v5}, Lcom/mediatek/lbs/em/NetworkLocationEM;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f050085

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    iget-object v5, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    invoke-virtual {v5}, Lcom/mediatek/lbs/em/NetworkLocationEM;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const/high16 v6, 0x7f040000

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object p3

    iget-object v5, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    # getter for: Lcom/mediatek/lbs/em/NetworkLocationEM;->mNetProfileManager:Lcom/mediatek/lbs/em/NetworkLocationProfileManager;
    invoke-static {v5}, Lcom/mediatek/lbs/em/NetworkLocationEM;->access$300(Lcom/mediatek/lbs/em/NetworkLocationEM;)Lcom/mediatek/lbs/em/NetworkLocationProfileManager;

    move-result-object v5

    const-string v6, "NetworkServiceName"

    invoke-virtual {v5, v6}, Lcom/mediatek/lbs/em/NetworkLocationProfileManager;->getSettingContent(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    array-length v5, p3

    if-ge v0, v5, :cond_0

    aget-object v5, p3, v0

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    move v4, v0

    :cond_0
    :goto_1
    return v4

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v5, 0x1

    if-ne v5, p1, :cond_4

    iget-object v5, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    invoke-virtual {v5}, Lcom/mediatek/lbs/em/NetworkLocationEM;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f05008b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    iget-object v5, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    invoke-virtual {v5}, Lcom/mediatek/lbs/em/NetworkLocationEM;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f040001

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object p3

    iget-object v5, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    # getter for: Lcom/mediatek/lbs/em/NetworkLocationEM;->mNetProfileManager:Lcom/mediatek/lbs/em/NetworkLocationProfileManager;
    invoke-static {v5}, Lcom/mediatek/lbs/em/NetworkLocationEM;->access$300(Lcom/mediatek/lbs/em/NetworkLocationEM;)Lcom/mediatek/lbs/em/NetworkLocationProfileManager;

    move-result-object v5

    const-string v6, "GeocoderServiceName"

    invoke-virtual {v5, v6}, Lcom/mediatek/lbs/em/NetworkLocationProfileManager;->getSettingContent(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const/4 v0, 0x0

    :goto_2
    array-length v5, p3

    if-ge v0, v5, :cond_0

    aget-object v5, p3, v0

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    move v4, v0

    goto :goto_1

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    const/4 v5, 0x2

    if-ne v5, p1, :cond_0

    iget-object v5, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    invoke-virtual {v5}, Lcom/mediatek/lbs/em/NetworkLocationEM;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f050093

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    iget-object v5, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    invoke-virtual {v5}, Lcom/mediatek/lbs/em/NetworkLocationEM;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f040002

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object p5

    array-length v5, p5

    new-array p3, v5, [Ljava/lang/String;

    const/4 v0, 0x0

    :goto_3
    array-length v5, p3

    if-ge v0, v5, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    aget v5, p5, v0

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "seconds"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, p3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_5
    iget-object v5, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    # getter for: Lcom/mediatek/lbs/em/NetworkLocationEM;->mNetProfileManager:Lcom/mediatek/lbs/em/NetworkLocationProfileManager;
    invoke-static {v5}, Lcom/mediatek/lbs/em/NetworkLocationEM;->access$300(Lcom/mediatek/lbs/em/NetworkLocationEM;)Lcom/mediatek/lbs/em/NetworkLocationProfileManager;

    move-result-object v5

    const-string v6, "LocationFrequency"

    invoke-virtual {v5, v6}, Lcom/mediatek/lbs/em/NetworkLocationProfileManager;->getSettingContent(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v0, 0x0

    :goto_4
    array-length v5, p5

    if-ge v0, v5, :cond_0

    aget v5, p5, v0

    if-ne v5, v2, :cond_6

    move v4, v0

    goto/16 :goto_1

    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_4
.end method

.method private performActionAfterClick(II[Ljava/lang/String;[I)V
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # [Ljava/lang/String;
    .param p4    # [I

    const/4 v4, 0x0

    if-nez p1, :cond_1

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    # getter for: Lcom/mediatek/lbs/em/NetworkLocationEM;->mNetProfileManager:Lcom/mediatek/lbs/em/NetworkLocationProfileManager;
    invoke-static {v1}, Lcom/mediatek/lbs/em/NetworkLocationEM;->access$300(Lcom/mediatek/lbs/em/NetworkLocationEM;)Lcom/mediatek/lbs/em/NetworkLocationProfileManager;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    # getter for: Lcom/mediatek/lbs/em/NetworkLocationEM;->mNetProfileManager:Lcom/mediatek/lbs/em/NetworkLocationProfileManager;
    invoke-static {v2}, Lcom/mediatek/lbs/em/NetworkLocationEM;->access$300(Lcom/mediatek/lbs/em/NetworkLocationEM;)Lcom/mediatek/lbs/em/NetworkLocationProfileManager;

    const-string v2, "NetworkServiceName"

    aget-object v3, p3, p2

    invoke-virtual {v1, v2, v3}, Lcom/mediatek/lbs/em/NetworkLocationProfileManager;->storeDataBack(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    # getter for: Lcom/mediatek/lbs/em/NetworkLocationEM;->mNetProfileManager:Lcom/mediatek/lbs/em/NetworkLocationProfileManager;
    invoke-static {v1}, Lcom/mediatek/lbs/em/NetworkLocationEM;->access$300(Lcom/mediatek/lbs/em/NetworkLocationEM;)Lcom/mediatek/lbs/em/NetworkLocationProfileManager;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    # getter for: Lcom/mediatek/lbs/em/NetworkLocationEM;->mNetProfileManager:Lcom/mediatek/lbs/em/NetworkLocationProfileManager;
    invoke-static {v2}, Lcom/mediatek/lbs/em/NetworkLocationEM;->access$300(Lcom/mediatek/lbs/em/NetworkLocationEM;)Lcom/mediatek/lbs/em/NetworkLocationProfileManager;

    const-string v2, "NetworkServiceName"

    aget-object v3, p3, p2

    invoke-virtual {v1, v2, v3}, Lcom/mediatek/lbs/em/NetworkLocationProfileManager;->storeToTheDataBase(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    invoke-virtual {v1}, Lcom/mediatek/lbs/em/NetworkLocationEM;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    # getter for: Lcom/mediatek/lbs/em/NetworkLocationEM;->mNetProfileManager:Lcom/mediatek/lbs/em/NetworkLocationProfileManager;
    invoke-static {v2}, Lcom/mediatek/lbs/em/NetworkLocationEM;->access$300(Lcom/mediatek/lbs/em/NetworkLocationEM;)Lcom/mediatek/lbs/em/NetworkLocationProfileManager;

    move-result-object v2

    aget-object v3, p3, p2

    invoke-virtual {v2, v3}, Lcom/mediatek/lbs/em/NetworkLocationProfileManager;->getNetworkLocationToastString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x1

    if-ne v1, p1, :cond_2

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    # getter for: Lcom/mediatek/lbs/em/NetworkLocationEM;->mNetProfileManager:Lcom/mediatek/lbs/em/NetworkLocationProfileManager;
    invoke-static {v1}, Lcom/mediatek/lbs/em/NetworkLocationEM;->access$300(Lcom/mediatek/lbs/em/NetworkLocationEM;)Lcom/mediatek/lbs/em/NetworkLocationProfileManager;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    # getter for: Lcom/mediatek/lbs/em/NetworkLocationEM;->mNetProfileManager:Lcom/mediatek/lbs/em/NetworkLocationProfileManager;
    invoke-static {v2}, Lcom/mediatek/lbs/em/NetworkLocationEM;->access$300(Lcom/mediatek/lbs/em/NetworkLocationEM;)Lcom/mediatek/lbs/em/NetworkLocationProfileManager;

    const-string v2, "GeocoderServiceName"

    aget-object v3, p3, p2

    invoke-virtual {v1, v2, v3}, Lcom/mediatek/lbs/em/NetworkLocationProfileManager;->storeDataBack(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    # getter for: Lcom/mediatek/lbs/em/NetworkLocationEM;->mNetProfileManager:Lcom/mediatek/lbs/em/NetworkLocationProfileManager;
    invoke-static {v1}, Lcom/mediatek/lbs/em/NetworkLocationEM;->access$300(Lcom/mediatek/lbs/em/NetworkLocationEM;)Lcom/mediatek/lbs/em/NetworkLocationProfileManager;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    # getter for: Lcom/mediatek/lbs/em/NetworkLocationEM;->mNetProfileManager:Lcom/mediatek/lbs/em/NetworkLocationProfileManager;
    invoke-static {v2}, Lcom/mediatek/lbs/em/NetworkLocationEM;->access$300(Lcom/mediatek/lbs/em/NetworkLocationEM;)Lcom/mediatek/lbs/em/NetworkLocationProfileManager;

    const-string v2, "GeocoderServiceName"

    aget-object v3, p3, p2

    invoke-virtual {v1, v2, v3}, Lcom/mediatek/lbs/em/NetworkLocationProfileManager;->storeToTheDataBase(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    invoke-virtual {v1}, Lcom/mediatek/lbs/em/NetworkLocationEM;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    # getter for: Lcom/mediatek/lbs/em/NetworkLocationEM;->mNetProfileManager:Lcom/mediatek/lbs/em/NetworkLocationProfileManager;
    invoke-static {v2}, Lcom/mediatek/lbs/em/NetworkLocationEM;->access$300(Lcom/mediatek/lbs/em/NetworkLocationEM;)Lcom/mediatek/lbs/em/NetworkLocationProfileManager;

    move-result-object v2

    aget-object v3, p3, p2

    invoke-virtual {v2, v3}, Lcom/mediatek/lbs/em/NetworkLocationProfileManager;->getGeocoderServiceToastString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    const/4 v1, 0x2

    if-ne v1, p1, :cond_0

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    # getter for: Lcom/mediatek/lbs/em/NetworkLocationEM;->mNetProfileManager:Lcom/mediatek/lbs/em/NetworkLocationProfileManager;
    invoke-static {v1}, Lcom/mediatek/lbs/em/NetworkLocationEM;->access$300(Lcom/mediatek/lbs/em/NetworkLocationEM;)Lcom/mediatek/lbs/em/NetworkLocationProfileManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/lbs/em/NetworkLocationProfileManager;->isMediaTekLocationServiceBinding()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    # getter for: Lcom/mediatek/lbs/em/NetworkLocationEM;->mNetProfileManager:Lcom/mediatek/lbs/em/NetworkLocationProfileManager;
    invoke-static {v1}, Lcom/mediatek/lbs/em/NetworkLocationEM;->access$300(Lcom/mediatek/lbs/em/NetworkLocationEM;)Lcom/mediatek/lbs/em/NetworkLocationProfileManager;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    # getter for: Lcom/mediatek/lbs/em/NetworkLocationEM;->mNetProfileManager:Lcom/mediatek/lbs/em/NetworkLocationProfileManager;
    invoke-static {v2}, Lcom/mediatek/lbs/em/NetworkLocationEM;->access$300(Lcom/mediatek/lbs/em/NetworkLocationEM;)Lcom/mediatek/lbs/em/NetworkLocationProfileManager;

    const-string v2, "LocationFrequency"

    aget v3, p4, p2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/mediatek/lbs/em/NetworkLocationProfileManager;->storeDataBack(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->notifyDataSetChanged()V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.mediatek.locationem.locationfrequency"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    # getter for: Lcom/mediatek/lbs/em/NetworkLocationEM;->mNetProfileManager:Lcom/mediatek/lbs/em/NetworkLocationProfileManager;
    invoke-static {v1}, Lcom/mediatek/lbs/em/NetworkLocationEM;->access$300(Lcom/mediatek/lbs/em/NetworkLocationEM;)Lcom/mediatek/lbs/em/NetworkLocationProfileManager;

    const-string v1, "LocationFrequency"

    aget v2, p4, p2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    invoke-virtual {v1, v0}, Lcom/mediatek/lbs/em/NetworkLocationEM;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method private showOnListItemClick(I)V
    .locals 13
    .param p1    # I

    const/4 v11, 0x2

    const/4 v12, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v10, -0x1

    move-object v0, p0

    move v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->getItemInfo(ILjava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[I)I

    move-result v10

    if-eqz p1, :cond_0

    if-eq v12, p1, :cond_0

    if-ne v11, p1, :cond_2

    iget-object v0, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    # getter for: Lcom/mediatek/lbs/em/NetworkLocationEM;->mNetProfileManager:Lcom/mediatek/lbs/em/NetworkLocationProfileManager;
    invoke-static {v0}, Lcom/mediatek/lbs/em/NetworkLocationEM;->access$300(Lcom/mediatek/lbs/em/NetworkLocationEM;)Lcom/mediatek/lbs/em/NetworkLocationProfileManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/lbs/em/NetworkLocationProfileManager;->isMediaTekLocationServiceBinding()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const-string v0, "NetworkLocationEM"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "showOnListItemClick inter has been called: "

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v11, "items:"

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v11, 0x0

    aget-object v11, v3, v11

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-object v11, v3, v12

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v11, "selectpos:"

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v7, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    invoke-direct {v7, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v7, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-object v9, v3

    move-object v8, v5

    new-instance v0, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter$2;

    invoke-direct {v0, p0, p1, v9, v8}, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter$2;-><init>(Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;I[Ljava/lang/String;[I)V

    invoke-virtual {v7, v3, v10, v0}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v7}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog;->show()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/4 v0, 0x3

    if-eq v0, p1, :cond_3

    if-ne v11, p1, :cond_1

    iget-object v0, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    # getter for: Lcom/mediatek/lbs/em/NetworkLocationEM;->mNetProfileManager:Lcom/mediatek/lbs/em/NetworkLocationProfileManager;
    invoke-static {v0}, Lcom/mediatek/lbs/em/NetworkLocationEM;->access$300(Lcom/mediatek/lbs/em/NetworkLocationEM;)Lcom/mediatek/lbs/em/NetworkLocationProfileManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/lbs/em/NetworkLocationProfileManager;->isMediaTekLocationServiceBinding()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_3
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f05009b

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f05009c

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "Understand"

    new-instance v11, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter$3;

    invoke-direct {v11, p0}, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter$3;-><init>(Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;)V

    invoke-virtual {v0, v1, v11}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->mListData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const v9, 0x1020015

    const v8, 0x1020014

    const-string v5, "NetworkLocationEM"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getView position:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->this$0:Lcom/mediatek/lbs/em/NetworkLocationEM;

    # invokes: Lcom/mediatek/lbs/em/NetworkLocationEM;->getListData()V
    invoke-static {v5}, Lcom/mediatek/lbs/em/NetworkLocationEM;->access$100(Lcom/mediatek/lbs/em/NetworkLocationEM;)V

    const/4 v2, 0x0

    const/4 v1, 0x0

    const-string v4, "Title"

    const-string v3, "Summary"

    if-nez p2, :cond_1

    iget-object v5, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v6, 0x1090004

    const/4 v7, 0x0

    invoke-virtual {v5, v6, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    :goto_0
    if-eqz p2, :cond_0

    iget-object v5, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->mListData:Ljava/util/List;

    invoke-interface {v5, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map;

    invoke-interface {v5, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->mListData:Ljava/util/List;

    invoke-interface {v5, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map;

    invoke-interface {v5, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    move v0, p1

    new-instance v5, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter$1;

    invoke-direct {v5, p0, v0}, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter$1;-><init>(Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;I)V

    invoke-virtual {p2, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object p2

    :cond_1
    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    goto :goto_0
.end method
