.class public Lcom/mediatek/lbs/em/NetworkLocationEM;
.super Landroid/app/Activity;
.source "NetworkLocationEM.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;
    }
.end annotation


# static fields
.field private static final GEO_FAIL:I = 0x10

.field private static final GEO_SUCESS:I = 0xf

.field private static final LOCATIONEM_LOCATIONFREQUENCY:Ljava/lang/String; = "com.android.mediatek.locationem.locationfrequency"

.field private static final NET_FAIL:I = 0x5

.field private static final NET_SUCESS:I = 0x4

.field private static final TAG:Ljava/lang/String; = "NetworkLocationEM"


# instance fields
.field private mBtnCellQuery:Landroid/widget/Button;

.field private mBtnGoToMap:Landroid/widget/Button;

.field public final mBtnQueryClick:Landroid/view/View$OnClickListener;

.field private mBtnSIM1Info:Landroid/widget/Button;

.field private mBtnSIM2Info:Landroid/widget/Button;

.field private mCarrier:Ljava/lang/String;

.field private mCellCarrier:Landroid/widget/EditText;

.field private mCellIDEdit:Landroid/widget/EditText;

.field private mCellInfo:Lcom/android/location/provider/mediatek/utility/CellInfo;

.field private mCellLACEdit:Landroid/widget/EditText;

.field private mCellMCCEdit:Landroid/widget/EditText;

.field private mCellMNCEdit:Landroid/widget/EditText;

.field private mDialog:Landroid/app/ProgressDialog;

.field private mGeminiSupport:Z

.field private mGeo:Landroid/location/Geocoder;

.field private mGeocodingShowText:Landroid/widget/TextView;

.field private mHandler:Landroid/os/Handler;

.field private mListAdapter:Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;

.field private mListData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private mListNetSettings:Landroid/widget/ListView;

.field private mLocation:Landroid/location/Location;

.field private mLocationShowText:Landroid/widget/TextView;

.field private mNetAccessorInterface:Lcom/android/location/provider/mediatek/serviceaccessor/NetworkLocationContentAccessorInterface;

.field private mNetProfileManager:Lcom/mediatek/lbs/em/NetworkLocationProfileManager;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mListNetSettings:Landroid/widget/ListView;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mListData:Ljava/util/List;

    iput-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mListAdapter:Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;

    iput-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mNetProfileManager:Lcom/mediatek/lbs/em/NetworkLocationProfileManager;

    iput-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnCellQuery:Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnSIM1Info:Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnSIM2Info:Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCellIDEdit:Landroid/widget/EditText;

    iput-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCellLACEdit:Landroid/widget/EditText;

    iput-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCellMNCEdit:Landroid/widget/EditText;

    iput-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCellMCCEdit:Landroid/widget/EditText;

    iput-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCellCarrier:Landroid/widget/EditText;

    iput-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCellInfo:Lcom/android/location/provider/mediatek/utility/CellInfo;

    iput-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mLocationShowText:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mGeocodingShowText:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnGoToMap:Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCarrier:Ljava/lang/String;

    iput-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mGeo:Landroid/location/Geocoder;

    iput-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mNetAccessorInterface:Lcom/android/location/provider/mediatek/serviceaccessor/NetworkLocationContentAccessorInterface;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mGeminiSupport:Z

    new-instance v0, Lcom/mediatek/lbs/em/NetworkLocationEM$2;

    invoke-direct {v0, p0}, Lcom/mediatek/lbs/em/NetworkLocationEM$2;-><init>(Lcom/mediatek/lbs/em/NetworkLocationEM;)V

    iput-object v0, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/mediatek/lbs/em/NetworkLocationEM$3;

    invoke-direct {v0, p0}, Lcom/mediatek/lbs/em/NetworkLocationEM$3;-><init>(Lcom/mediatek/lbs/em/NetworkLocationEM;)V

    iput-object v0, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnQueryClick:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/lbs/em/NetworkLocationEM;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/NetworkLocationEM;

    iget-object v0, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mListData:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/lbs/em/NetworkLocationEM;)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/NetworkLocationEM;

    invoke-direct {p0}, Lcom/mediatek/lbs/em/NetworkLocationEM;->getListData()V

    return-void
.end method

.method static synthetic access$1000(Lcom/mediatek/lbs/em/NetworkLocationEM;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/NetworkLocationEM;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/lbs/em/NetworkLocationEM;->handleSendFailed(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/mediatek/lbs/em/NetworkLocationEM;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/NetworkLocationEM;

    iget-object v0, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnCellQuery:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/mediatek/lbs/em/NetworkLocationEM;)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/NetworkLocationEM;

    invoke-direct {p0}, Lcom/mediatek/lbs/em/NetworkLocationEM;->getCellInfo()V

    return-void
.end method

.method static synthetic access$1300(Lcom/mediatek/lbs/em/NetworkLocationEM;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/NetworkLocationEM;

    iget-object v0, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnSIM1Info:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/mediatek/lbs/em/NetworkLocationEM;I)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/NetworkLocationEM;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/lbs/em/NetworkLocationEM;->getSIMInfo(I)V

    return-void
.end method

.method static synthetic access$1500(Lcom/mediatek/lbs/em/NetworkLocationEM;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/NetworkLocationEM;

    iget-object v0, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnSIM2Info:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/mediatek/lbs/em/NetworkLocationEM;)Landroid/location/Geocoder;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/NetworkLocationEM;

    iget-object v0, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mGeo:Landroid/location/Geocoder;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/mediatek/lbs/em/NetworkLocationEM;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/NetworkLocationEM;

    iget-object v0, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/lbs/em/NetworkLocationEM;)Lcom/mediatek/lbs/em/NetworkLocationProfileManager;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/NetworkLocationEM;

    iget-object v0, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mNetProfileManager:Lcom/mediatek/lbs/em/NetworkLocationProfileManager;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mediatek/lbs/em/NetworkLocationEM;I)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/NetworkLocationEM;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/lbs/em/NetworkLocationEM;->changeTab(I)V

    return-void
.end method

.method static synthetic access$600(Lcom/mediatek/lbs/em/NetworkLocationEM;)Landroid/location/Location;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/NetworkLocationEM;

    iget-object v0, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mLocation:Landroid/location/Location;

    return-object v0
.end method

.method static synthetic access$700(Lcom/mediatek/lbs/em/NetworkLocationEM;Ljava/util/List;)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/NetworkLocationEM;
    .param p1    # Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/mediatek/lbs/em/NetworkLocationEM;->handleGeocodingSucessfully(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$800(Lcom/mediatek/lbs/em/NetworkLocationEM;)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/NetworkLocationEM;

    invoke-direct {p0}, Lcom/mediatek/lbs/em/NetworkLocationEM;->handleGeocodingFailed()V

    return-void
.end method

.method static synthetic access$900(Lcom/mediatek/lbs/em/NetworkLocationEM;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/NetworkLocationEM;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/lbs/em/NetworkLocationEM;->handleSendSucessfully(Ljava/lang/String;)V

    return-void
.end method

.method private changeTab(I)V
    .locals 2
    .param p1    # I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.mediatek.lbs.em.changetab"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "tab"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/mediatek/lbs/em/NetworkLocationEM;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method private checkIsInputValid()Z
    .locals 4

    const/4 v2, 0x0

    const-string v3, "[0-9]{1,10}"

    invoke-static {v3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    iget-object v3, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCellIDEdit:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "CELL_ID is invalid"

    invoke-static {p0, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    :goto_0
    return v2

    :cond_0
    iget-object v3, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCellLACEdit:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "LAC is invalid"

    invoke-static {p0, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCellMNCEdit:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "MNC is invalid"

    invoke-static {p0, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCellMCCEdit:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "MCC is invalid"

    invoke-static {p0, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_3
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private getCellInfo()V
    .locals 4

    iget-object v2, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mLocationShowText:Landroid/widget/TextView;

    const-string v3, "Querying the cell location"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mGeocodingShowText:Landroid/widget/TextView;

    const-string v3, "Geocoding will be performed after locating sucessfully"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/mediatek/lbs/em/NetworkLocationEM;->checkIsInputValid()Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Lcom/android/location/provider/mediatek/utility/CellInfo;

    invoke-direct {v2}, Lcom/android/location/provider/mediatek/utility/CellInfo;-><init>()V

    iput-object v2, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCellInfo:Lcom/android/location/provider/mediatek/utility/CellInfo;

    iget-object v2, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCellInfo:Lcom/android/location/provider/mediatek/utility/CellInfo;

    iget-object v2, v2, Lcom/android/location/provider/mediatek/utility/CellInfo;->mMainCell:Lcom/android/location/provider/mediatek/utility/MainCell;

    iget-object v3, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCellIDEdit:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/android/location/provider/mediatek/utility/MainCell;->mCellID:I

    iget-object v2, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCellInfo:Lcom/android/location/provider/mediatek/utility/CellInfo;

    iget-object v2, v2, Lcom/android/location/provider/mediatek/utility/CellInfo;->mMainCell:Lcom/android/location/provider/mediatek/utility/MainCell;

    iget-object v3, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCellLACEdit:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/android/location/provider/mediatek/utility/MainCell;->mLocalAreaCode:I

    iget-object v2, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCellInfo:Lcom/android/location/provider/mediatek/utility/CellInfo;

    iget-object v2, v2, Lcom/android/location/provider/mediatek/utility/CellInfo;->mMainCell:Lcom/android/location/provider/mediatek/utility/MainCell;

    iget-object v3, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCellMCCEdit:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/android/location/provider/mediatek/utility/MainCell;->mMobileCountryCode:I

    iget-object v2, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCellInfo:Lcom/android/location/provider/mediatek/utility/CellInfo;

    iget-object v2, v2, Lcom/android/location/provider/mediatek/utility/CellInfo;->mMainCell:Lcom/android/location/provider/mediatek/utility/MainCell;

    iget-object v3, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCellMNCEdit:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/android/location/provider/mediatek/utility/MainCell;->mMobileNetworkCode:I

    iget-object v2, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCellInfo:Lcom/android/location/provider/mediatek/utility/CellInfo;

    iget-object v2, v2, Lcom/android/location/provider/mediatek/utility/CellInfo;->mMainCell:Lcom/android/location/provider/mediatek/utility/MainCell;

    iget-object v3, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCarrier:Ljava/lang/String;

    iput-object v3, v2, Lcom/android/location/provider/mediatek/utility/MainCell;->mCarrier:Ljava/lang/String;

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCarrier:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    iget-object v2, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCellInfo:Lcom/android/location/provider/mediatek/utility/CellInfo;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mNetAccessorInterface:Lcom/android/location/provider/mediatek/serviceaccessor/NetworkLocationContentAccessorInterface;

    invoke-virtual {v2, v1, v0}, Lcom/android/location/provider/mediatek/serviceaccessor/NetworkLocationContentAccessorInterface;->startGetLocationByCellIdAndWifi(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    invoke-direct {p0}, Lcom/mediatek/lbs/em/NetworkLocationEM;->notifyUsrIsLocating()V

    :cond_0
    return-void
.end method

.method private getListData()V
    .locals 5

    const-string v3, "NetworkLocationEM"

    const-string v4, "getListData() is called"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mListData:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v2, "Title"

    const-string v1, "Summary"

    invoke-virtual {p0}, Lcom/mediatek/lbs/em/NetworkLocationEM;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f050083

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mNetProfileManager:Lcom/mediatek/lbs/em/NetworkLocationProfileManager;

    invoke-virtual {v3}, Lcom/mediatek/lbs/em/NetworkLocationProfileManager;->getNetBindAbstract()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mListData:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mNetProfileManager:Lcom/mediatek/lbs/em/NetworkLocationProfileManager;

    invoke-virtual {v4}, Lcom/mediatek/lbs/em/NetworkLocationProfileManager;->isGeocodingServiceShowing()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ne v3, v4, :cond_1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p0}, Lcom/mediatek/lbs/em/NetworkLocationEM;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f05008b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mNetProfileManager:Lcom/mediatek/lbs/em/NetworkLocationProfileManager;

    invoke-virtual {v3}, Lcom/mediatek/lbs/em/NetworkLocationProfileManager;->getGeocoderServiceBindAbstract()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mListData:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mNetProfileManager:Lcom/mediatek/lbs/em/NetworkLocationProfileManager;

    invoke-virtual {v3}, Lcom/mediatek/lbs/em/NetworkLocationProfileManager;->isMediaTekLocationServiceBinding()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p0}, Lcom/mediatek/lbs/em/NetworkLocationEM;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f050093

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "NetworkLocationEM"

    iget-object v4, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mNetProfileManager:Lcom/mediatek/lbs/em/NetworkLocationProfileManager;

    invoke-virtual {v4}, Lcom/mediatek/lbs/em/NetworkLocationProfileManager;->getFrequencyAbstract()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mNetProfileManager:Lcom/mediatek/lbs/em/NetworkLocationProfileManager;

    invoke-virtual {v3}, Lcom/mediatek/lbs/em/NetworkLocationProfileManager;->getFrequencyAbstract()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mListData:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p0}, Lcom/mediatek/lbs/em/NetworkLocationEM;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f050099

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/mediatek/lbs/em/NetworkLocationEM;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f05009a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mListData:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    return-void
.end method

.method private getNetworkType(I)Lcom/android/location/provider/mediatek/connectivity/Network_Type;
    .locals 4
    .param p1    # I

    const/4 v3, 0x0

    const/4 v1, 0x0

    sget-object v0, Lcom/android/location/provider/mediatek/connectivity/Network_Type;->Network_2G:Lcom/android/location/provider/mediatek/connectivity/Network_Type;

    if-nez p1, :cond_2

    const-string v2, "gsm.cs.network.type"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v1

    :cond_0
    :goto_0
    const/4 v2, 0x3

    if-lt v1, v2, :cond_1

    sget-object v0, Lcom/android/location/provider/mediatek/connectivity/Network_Type;->Network_3G:Lcom/android/location/provider/mediatek/connectivity/Network_Type;

    :cond_1
    return-object v0

    :cond_2
    const/4 v2, 0x1

    if-ne p1, v2, :cond_0

    const-string v2, "gsm.cs.network.type.2"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v1

    goto :goto_0
.end method

.method private getSIMInfo(I)V
    .locals 11
    .param p1    # I

    const/4 v10, 0x5

    const/4 v9, 0x3

    const/4 v8, 0x0

    const/4 v7, 0x1

    const-string v5, "phone"

    invoke-virtual {p0, v5}, Lcom/mediatek/lbs/em/NetworkLocationEM;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telephony/TelephonyManager;

    const/4 v0, 0x0

    iget-boolean v5, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mGeminiSupport:Z

    if-eqz v5, :cond_4

    invoke-virtual {v3, p1}, Landroid/telephony/TelephonyManager;->getSimStateGemini(I)I

    move-result v4

    if-ne v4, v10, :cond_3

    invoke-virtual {v3, p1}, Landroid/telephony/TelephonyManager;->getNetworkOperatorGemini(I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v3, p1}, Landroid/telephony/TelephonyManager;->getCellLocationGemini(I)Landroid/telephony/CellLocation;

    move-result-object v0

    check-cast v0, Landroid/telephony/gsm/GsmCellLocation;

    if-eqz v0, :cond_0

    iget-object v5, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCellIDEdit:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/telephony/gsm/GsmCellLocation;->getCid()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, p1}, Lcom/mediatek/lbs/em/NetworkLocationEM;->getNetworkType(I)Lcom/android/location/provider/mediatek/connectivity/Network_Type;

    move-result-object v2

    sget-object v5, Lcom/android/location/provider/mediatek/connectivity/Network_Type;->Network_2G:Lcom/android/location/provider/mediatek/connectivity/Network_Type;

    if-ne v2, v5, :cond_1

    iget-object v5, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCellLACEdit:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/telephony/gsm/GsmCellLocation;->getLac()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v5, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCellMCCEdit:Landroid/widget/EditText;

    invoke-virtual {v1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCellMNCEdit:Landroid/widget/EditText;

    invoke-virtual {v1, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v3, p1}, Landroid/telephony/TelephonyManager;->getNetworkOperatorNameGemini(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCarrier:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SIM"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    add-int/lit8 v6, p1, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " information updated"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget-object v5, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCellLACEdit:Landroid/widget/EditText;

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SIM"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    add-int/lit8 v6, p1, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is not available"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_1

    :cond_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SIM"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    add-int/lit8 v6, p1, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is not available"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_1

    :cond_4
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v4

    if-ne v4, v10, :cond_7

    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_6

    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getCellLocation()Landroid/telephony/CellLocation;

    move-result-object v0

    check-cast v0, Landroid/telephony/gsm/GsmCellLocation;

    if-eqz v0, :cond_0

    iget-object v5, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCellIDEdit:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/telephony/gsm/GsmCellLocation;->getCid()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, p1}, Lcom/mediatek/lbs/em/NetworkLocationEM;->getNetworkType(I)Lcom/android/location/provider/mediatek/connectivity/Network_Type;

    move-result-object v2

    sget-object v5, Lcom/android/location/provider/mediatek/connectivity/Network_Type;->Network_2G:Lcom/android/location/provider/mediatek/connectivity/Network_Type;

    if-ne v2, v5, :cond_5

    iget-object v5, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCellLACEdit:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/telephony/gsm/GsmCellLocation;->getLac()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    iget-object v5, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCellMCCEdit:Landroid/widget/EditText;

    invoke-virtual {v1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCellMNCEdit:Landroid/widget/EditText;

    invoke-virtual {v1, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCarrier:Ljava/lang/String;

    const-string v5, "SIM information updated"

    invoke-static {p0, v5, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    :cond_5
    iget-object v5, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCellLACEdit:Landroid/widget/EditText;

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_6
    const-string v5, "SIM is not available"

    invoke-static {p0, v5, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    :cond_7
    const-string v5, "SIM is not available"

    invoke-static {p0, v5, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1
.end method

.method private handleGeocodingFailed()V
    .locals 2

    invoke-static {}, Landroid/location/Geocoder;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mGeocodingShowText:Landroid/widget/TextView;

    const-string v1, "Geocoding Failed"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v0, "Geocoding Failed"

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mGeocodingShowText:Landroid/widget/TextView;

    const-string v1, "Please install GMS package to use Geocoding function"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private handleGeocodingSucessfully(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/location/Address;",
            ">;)V"
        }
    .end annotation

    const/4 v4, 0x0

    const-string v2, "Geocoding Sucessfully"

    const/4 v3, 0x1

    invoke-static {p0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Address;

    if-eqz v0, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Country:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Landroid/location/Address;->getCountryCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\r\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "Address:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v4}, Landroid/location/Address;->getAddressLine(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mGeocodingShowText:Landroid/widget/TextView;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method private handleSendFailed(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mDialog:Landroid/app/ProgressDialog;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mLocationShowText:Landroid/widget/TextView;

    const-string v1, "Can not get the cell location"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mGeocodingShowText:Landroid/widget/TextView;

    const-string v1, "Geocoding is not performed because the locating failed"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnGoToMap:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method

.method private handleSendSucessfully(Ljava/lang/String;)V
    .locals 8
    .param p1    # Ljava/lang/String;

    const/4 v7, 0x0

    iget-object v4, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mDialog:Landroid/app/ProgressDialog;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v4}, Landroid/app/ProgressDialog;->dismiss()V

    iput-object v7, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mDialog:Landroid/app/ProgressDialog;

    :cond_0
    const/4 v2, 0x0

    iget-object v4, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mNetAccessorInterface:Lcom/android/location/provider/mediatek/serviceaccessor/NetworkLocationContentAccessorInterface;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mNetAccessorInterface:Lcom/android/location/provider/mediatek/serviceaccessor/NetworkLocationContentAccessorInterface;

    invoke-virtual {v4, p1}, Lcom/android/location/provider/mediatek/serviceaccessor/NetworkLocationContentAccessorInterface;->finishGetLocationByCellIdAndWifi(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v2

    :cond_1
    if-eqz v2, :cond_2

    iget-object v4, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnGoToMap:Landroid/widget/Button;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setEnabled(Z)V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "lat="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "lng="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "alt="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Landroid/location/Location;->getAltitude()D

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "acc="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Landroid/location/Location;->getAccuracy()F

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v2, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mLocation:Landroid/location/Location;

    iget-object v4, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mLocationShowText:Landroid/widget/TextView;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v1, v2

    invoke-static {}, Landroid/location/Geocoder;->isPresent()Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mGeocodingShowText:Landroid/widget/TextView;

    const-string v5, "Geocoding is performing based on the current cell location"

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v4, Lcom/mediatek/lbs/em/NetworkLocationEM$4;

    invoke-direct {v4, p0, v1}, Lcom/mediatek/lbs/em/NetworkLocationEM$4;-><init>(Lcom/mediatek/lbs/em/NetworkLocationEM;Landroid/location/Location;)V

    invoke-virtual {v4}, Lcom/mediatek/lbs/em/NetworkLocationEM$4;->start()V

    :cond_2
    :goto_0
    return-void

    :cond_3
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v3

    const/16 v4, 0x10

    iput v4, v3, Landroid/os/Message;->what:I

    iput-object v7, v3, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v4, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private initializeListView()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mNetProfileManager:Lcom/mediatek/lbs/em/NetworkLocationProfileManager;

    invoke-virtual {v0}, Lcom/mediatek/lbs/em/NetworkLocationProfileManager;->init()V

    invoke-direct {p0}, Lcom/mediatek/lbs/em/NetworkLocationEM;->getListData()V

    new-instance v0, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;

    invoke-direct {v0, p0, p0}, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;-><init>(Lcom/mediatek/lbs/em/NetworkLocationEM;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mListAdapter:Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;

    const v0, 0x7f060043

    invoke-virtual {p0, v0}, Lcom/mediatek/lbs/em/NetworkLocationEM;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mListNetSettings:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mListNetSettings:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mListAdapter:Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mListNetSettings:Landroid/widget/ListView;

    invoke-direct {p0, v0}, Lcom/mediatek/lbs/em/NetworkLocationEM;->setListViewHeightBasedOnChildren(Landroid/widget/ListView;)V

    return-void
.end method

.method private initializeQueryView()V
    .locals 6

    const/4 v5, 0x0

    const/high16 v4, 0x3f800000

    const/4 v3, -0x2

    const v1, 0x7f06003f

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/NetworkLocationEM;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iget-boolean v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mGeminiSupport:Z

    if-eqz v1, :cond_2

    new-instance v1, Landroid/widget/Button;

    invoke-direct {v1, p0}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnSIM1Info:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnSIM1Info:Landroid/widget/Button;

    const-string v2, "SIM1"

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnSIM1Info:Landroid/widget/Button;

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v3, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-direct {p0, v5}, Lcom/mediatek/lbs/em/NetworkLocationEM;->isSimReady(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnSIM1Info:Landroid/widget/Button;

    iget-object v2, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnQueryClick:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    new-instance v1, Landroid/widget/Button;

    invoke-direct {v1, p0}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnSIM2Info:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnSIM2Info:Landroid/widget/Button;

    const-string v2, "SIM2"

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnSIM2Info:Landroid/widget/Button;

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v3, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/mediatek/lbs/em/NetworkLocationEM;->isSimReady(I)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnSIM2Info:Landroid/widget/Button;

    iget-object v2, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnQueryClick:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_1
    new-instance v1, Landroid/widget/Button;

    invoke-direct {v1, p0}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnCellQuery:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnCellQuery:Landroid/widget/Button;

    const-string v2, "Query"

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnCellQuery:Landroid/widget/Button;

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v3, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnCellQuery:Landroid/widget/Button;

    iget-object v2, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnQueryClick:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnSIM1Info:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnCellQuery:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnSIM2Info:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    :goto_2
    const v1, 0x7f06003b

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/NetworkLocationEM;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCellIDEdit:Landroid/widget/EditText;

    const v1, 0x7f06003c

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/NetworkLocationEM;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCellLACEdit:Landroid/widget/EditText;

    const v1, 0x7f06003e

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/NetworkLocationEM;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCellMNCEdit:Landroid/widget/EditText;

    const v1, 0x7f06003d

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/NetworkLocationEM;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCellMCCEdit:Landroid/widget/EditText;

    const v1, 0x7f060042

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/NetworkLocationEM;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnGoToMap:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnGoToMap:Landroid/widget/Button;

    new-instance v2, Lcom/mediatek/lbs/em/NetworkLocationEM$1;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/em/NetworkLocationEM$1;-><init>(Lcom/mediatek/lbs/em/NetworkLocationEM;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnGoToMap:Landroid/widget/Button;

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCellIDEdit:Landroid/widget/EditText;

    const-string v2, "11111"

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCellLACEdit:Landroid/widget/EditText;

    const-string v2, "11001"

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCellMNCEdit:Landroid/widget/EditText;

    const-string v2, "03"

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mCellMCCEdit:Landroid/widget/EditText;

    const-string v2, "460"

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f060040

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/NetworkLocationEM;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mLocationShowText:Landroid/widget/TextView;

    const v1, 0x7f060041

    invoke-virtual {p0, v1}, Lcom/mediatek/lbs/em/NetworkLocationEM;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mGeocodingShowText:Landroid/widget/TextView;

    return-void

    :cond_0
    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnSIM1Info:Landroid/widget/Button;

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setEnabled(Z)V

    goto/16 :goto_0

    :cond_1
    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnSIM2Info:Landroid/widget/Button;

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setEnabled(Z)V

    goto/16 :goto_1

    :cond_2
    new-instance v1, Landroid/widget/Button;

    invoke-direct {v1, p0}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnSIM1Info:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnSIM1Info:Landroid/widget/Button;

    const-string v2, "SIM"

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnSIM1Info:Landroid/widget/Button;

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v3, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v1, -0x1

    invoke-direct {p0, v1}, Lcom/mediatek/lbs/em/NetworkLocationEM;->isSimReady(I)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnSIM1Info:Landroid/widget/Button;

    iget-object v2, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnQueryClick:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_3
    new-instance v1, Landroid/widget/Button;

    invoke-direct {v1, p0}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnCellQuery:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnCellQuery:Landroid/widget/Button;

    const-string v2, "Query"

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnCellQuery:Landroid/widget/Button;

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v3, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnCellQuery:Landroid/widget/Button;

    iget-object v2, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnQueryClick:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnSIM1Info:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnCellQuery:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_2

    :cond_3
    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mBtnSIM1Info:Landroid/widget/Button;

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_3
.end method

.method private isSimReady(I)Z
    .locals 3
    .param p1    # I

    const/4 v1, 0x0

    const-string v2, "phone"

    invoke-virtual {p0, v2}, Lcom/mediatek/lbs/em/NetworkLocationEM;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iget-boolean v2, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mGeminiSupport:Z

    if-nez v2, :cond_0

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    return v2

    :cond_0
    invoke-virtual {v0, p1}, Landroid/telephony/TelephonyManager;->getNetworkOperatorGemini(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "hugo_app"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private notifyUsrIsLocating()V
    .locals 3

    const-string v0, ""

    const-string v1, "Cell Location Querying"

    const/4 v2, 0x1

    invoke-static {p0, v0, v1, v2}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mDialog:Landroid/app/ProgressDialog;

    return-void
.end method

.method private setListViewHeightBasedOnChildren(Landroid/widget/ListView;)V
    .locals 7
    .param p1    # Landroid/widget/ListView;

    const/4 v6, 0x0

    invoke-virtual {p1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    check-cast v1, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v4, 0x0

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1}, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->getCount()I

    move-result v5

    if-ge v0, v5, :cond_1

    const/4 v5, 0x0

    invoke-virtual {v1, v0, v5, p1}, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v6, v6}, Landroid/view/View;->measure(II)V

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v4, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    invoke-virtual {p1}, Landroid/widget/ListView;->getDividerHeight()I

    move-result v5

    invoke-virtual {v1}, Lcom/mediatek/lbs/em/NetworkLocationEM$MyAdapter;->getCount()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    mul-int/2addr v5, v6

    add-int/2addr v5, v4

    iput v5, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget v5, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    add-int/lit8 v5, v5, 0x32

    iput v5, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {p1, v3}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mGeminiSupport:Z

    new-instance v0, Lcom/mediatek/lbs/em/NetworkLocationProfileManager;

    invoke-direct {v0, p0}, Lcom/mediatek/lbs/em/NetworkLocationProfileManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mNetProfileManager:Lcom/mediatek/lbs/em/NetworkLocationProfileManager;

    new-instance v0, Lcom/android/location/provider/mediatek/serviceaccessor/google/GoogleNetworkLocationContentAccessor;

    iget-object v1, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/android/location/provider/mediatek/serviceaccessor/google/GoogleNetworkLocationContentAccessor;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mNetAccessorInterface:Lcom/android/location/provider/mediatek/serviceaccessor/NetworkLocationContentAccessorInterface;

    new-instance v0, Landroid/location/Geocoder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/mediatek/lbs/em/NetworkLocationEM;->mGeo:Landroid/location/Geocoder;

    const v0, 0x7f030001

    invoke-virtual {p0, v0}, Lcom/mediatek/lbs/em/NetworkLocationEM;->setContentView(I)V

    invoke-direct {p0}, Lcom/mediatek/lbs/em/NetworkLocationEM;->initializeListView()V

    invoke-direct {p0}, Lcom/mediatek/lbs/em/NetworkLocationEM;->initializeQueryView()V

    return-void
.end method
