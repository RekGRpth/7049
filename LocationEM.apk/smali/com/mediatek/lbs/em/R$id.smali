.class public final Lcom/mediatek/lbs/em/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/lbs/em/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final ButtonMenu:I = 0x7f060080

.field public static final Button_AGPS:I = 0x7f060087

.field public static final Button_APN:I = 0x7f060086

.field public static final Button_ChangeRefLatLng:I = 0x7f06005f

.field public static final Button_Delay:I = 0x7f060025

.field public static final Button_EditButtonCancel:I = 0x7f060046

.field public static final Button_EditButtonOK:I = 0x7f060045

.field public static final Button_EpoStress:I = 0x7f06004a

.field public static final Button_GPS:I = 0x7f060083

.field public static final Button_GoToMap:I = 0x7f060042

.field public static final Button_HAcc:I = 0x7f06001f

.field public static final Button_LoadProfile:I = 0x7f060000

.field public static final Button_LocationAge:I = 0x7f060023

.field public static final Button_Manual:I = 0x7f060049

.field public static final Button_Once:I = 0x7f06005e

.field public static final Button_Reset:I = 0x7f060002

.field public static final Button_Reset2Default:I = 0x7f060039

.field public static final Button_SetProfile:I = 0x7f060001

.field public static final Button_SlpAddr:I = 0x7f060013

.field public static final Button_SlpPort:I = 0x7f060015

.field public static final Button_StartGPS:I = 0x7f06005d

.field public static final Button_Stress:I = 0x7f060060

.field public static final Button_TIME:I = 0x7f060085

.field public static final Button_Test1:I = 0x7f060003

.field public static final Button_Test2:I = 0x7f060004

.field public static final Button_Test3:I = 0x7f060005

.field public static final Button_Test4:I = 0x7f060006

.field public static final Button_Test5:I = 0x7f060007

.field public static final Button_Test6:I = 0x7f060008

.field public static final Button_Test7:I = 0x7f060009

.field public static final Button_Test8:I = 0x7f06000a

.field public static final Button_UpdatePeriod:I = 0x7f06004e

.field public static final Button_VAcc:I = 0x7f060021

.field public static final Button_WIFI:I = 0x7f060088

.field public static final Button_YGPS:I = 0x7f060084

.field public static final Button_apply:I = 0x7f06007e

.field public static final Button_cancel:I = 0x7f06007d

.field public static final Button_ok:I = 0x7f06007c

.field public static final Button_serverConnect:I = 0x7f06008d

.field public static final CheckBox_CerVerify:I = 0x7f060026

.field public static final CheckBox_EcidEnable:I = 0x7f060034

.field public static final CheckBox_EmAllow:I = 0x7f060032

.field public static final CheckBox_EnableAgps:I = 0x7f06000b

.field public static final CheckBox_EnableAuto:I = 0x7f060048

.field public static final CheckBox_EnableEpo:I = 0x7f060047

.field public static final CheckBox_EnableIot:I = 0x7f06000c

.field public static final CheckBox_EnableNiTimer:I = 0x7f060036

.field public static final CheckBox_ExternalAddr:I = 0x7f060029

.field public static final CheckBox_LabPerformance:I = 0x7f06002d

.field public static final CheckBox_LogNmea:I = 0x7f060063

.field public static final CheckBox_LogToSdcard:I = 0x7f060064

.field public static final CheckBox_MlcNumber:I = 0x7f06002b

.field public static final CheckBox_NiAllow:I = 0x7f060031

.field public static final CheckBox_RoamingAllow:I = 0x7f060033

.field public static final CheckBox_Supl2File:I = 0x7f060035

.field public static final CheckBox_Tls:I = 0x7f060016

.field public static final EditText_Delay1:I = 0x7f060073

.field public static final EditText_Delay2:I = 0x7f060075

.field public static final EditText_Delay3:I = 0x7f060077

.field public static final EditText_Delay4:I = 0x7f060079

.field public static final EditText_EditButton:I = 0x7f060044

.field public static final EditText_EpoDelay1:I = 0x7f060052

.field public static final EditText_EpoDelay2:I = 0x7f060054

.field public static final EditText_EpoNumOfLoop:I = 0x7f060050

.field public static final EditText_NumOfLoop:I = 0x7f060071

.field public static final EditText_UpdatePeriod:I = 0x7f06004d

.field public static final EditText_addr:I = 0x7f060081

.field public static final EditText_lat:I = 0x7f06007a

.field public static final EditText_lng:I = 0x7f06007b

.field public static final EditText_serverIp:I = 0x7f06008b

.field public static final EditText_serverPort:I = 0x7f06008c

.field public static final ImageView_fileRow:I = 0x7f06005a

.field public static final NetSettingListView:I = 0x7f060043

.field public static final RadioButton_AssistanceData:I = 0x7f060028

.field public static final RadioButton_Cp:I = 0x7f060010

.field public static final RadioButton_Imsi:I = 0x7f06001b

.field public static final RadioButton_Ipv4:I = 0x7f06001a

.field public static final RadioButton_KValue:I = 0x7f06001c

.field public static final RadioButton_LocationEstimate:I = 0x7f060027

.field public static final RadioButton_Meter:I = 0x7f06001d

.field public static final RadioButton_Msa:I = 0x7f06000f

.field public static final RadioButton_Msb:I = 0x7f06000e

.field public static final RadioButton_PreferSim1:I = 0x7f06002f

.field public static final RadioButton_PreferSim2:I = 0x7f060030

.field public static final RadioButton_Rrc:I = 0x7f060018

.field public static final RadioButton_Rrlp:I = 0x7f060017

.field public static final RadioButton_RrlpRrc:I = 0x7f060019

.field public static final RadioButton_Standalone:I = 0x7f06000d

.field public static final RadioButton_Up:I = 0x7f060011

.field public static final RadioButton_cold:I = 0x7f060068

.field public static final RadioButton_full:I = 0x7f060069

.field public static final RadioButton_hot:I = 0x7f060066

.field public static final RadioButton_warm:I = 0x7f060067

.field public static final RadioGroup_SimPrefer:I = 0x7f06002e

.field public static final Spinner_NiDialogTest:I = 0x7f06003a

.field public static final Spinner_NotificationTimeout:I = 0x7f060037

.field public static final Spinner_ServerPrfoile:I = 0x7f06008a

.field public static final Spinner_VerificationTimeout:I = 0x7f060038

.field public static final TextView_0:I = 0x7f06006a

.field public static final TextView_1:I = 0x7f06006b

.field public static final TextView_2:I = 0x7f06006c

.field public static final TextView_3:I = 0x7f06006d

.field public static final TextView_4:I = 0x7f06006e

.field public static final TextView_5:I = 0x7f06006f

.field public static final TextView_CellInfo:I = 0x7f060089

.field public static final TextView_Delay:I = 0x7f060024

.field public static final TextView_Delay1:I = 0x7f060072

.field public static final TextView_Delay2:I = 0x7f060074

.field public static final TextView_Delay3:I = 0x7f060076

.field public static final TextView_Delay4:I = 0x7f060078

.field public static final TextView_EpoDelay1:I = 0x7f060051

.field public static final TextView_EpoDelay2:I = 0x7f060053

.field public static final TextView_EpoNumOfLoop:I = 0x7f06004f

.field public static final TextView_ExternalAddr:I = 0x7f06002a

.field public static final TextView_FileInfo:I = 0x7f06004c

.field public static final TextView_FilePath:I = 0x7f060059

.field public static final TextView_HAcc:I = 0x7f06001e

.field public static final TextView_LocationAge:I = 0x7f060022

.field public static final TextView_Loop:I = 0x7f060061

.field public static final TextView_MlcNumber:I = 0x7f06002c

.field public static final TextView_NumOfLoop:I = 0x7f060070

.field public static final TextView_Porgress:I = 0x7f06004b

.field public static final TextView_ServerResult:I = 0x7f06008e

.field public static final TextView_SlpAddr:I = 0x7f060012

.field public static final TextView_SlpPort:I = 0x7f060014

.field public static final TextView_VAcc:I = 0x7f060020

.field public static final TextView_fileRow:I = 0x7f06005b

.field public static final TextView_fileRow2:I = 0x7f06005c

.field public static final button_LocationEm:I = 0x7f060057

.field public static final button_Sdcard:I = 0x7f060056

.field public static final button_layout:I = 0x7f06003f

.field public static final entry_cell_id:I = 0x7f06003b

.field public static final entry_cell_lac:I = 0x7f06003c

.field public static final entry_cell_mcc:I = 0x7f06003d

.field public static final entry_cell_mnc:I = 0x7f06003e

.field public static final linearLayout1:I = 0x7f060058

.field public static final linearLayout2:I = 0x7f060055

.field public static final linearLayout3:I = 0x7f060062

.field public static final linearLayout4:I = 0x7f060065

.field public static final spinner_locList:I = 0x7f060082

.field public static final textview_id_geocoding_result:I = 0x7f060041

.field public static final textview_id_query_result:I = 0x7f060040

.field public static final viewgps:I = 0x7f06008f

.field public static final webview2:I = 0x7f06007f


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
