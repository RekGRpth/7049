.class public final Lcom/mediatek/lbs/em/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/lbs/em/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final DO_QUERY:I = 0x7f0500a6

.field public static final SIM1_Info:I = 0x7f0500a5

.field public static final SIM2_Info:I = 0x7f0500a4

.field public static final accuracy_unit:I = 0x7f05004f

.field public static final agps_mode:I = 0x7f050028

.field public static final agps_onoff:I = 0x7f050026

.field public static final allow_em_notification:I = 0x7f05002f

.field public static final allow_ni:I = 0x7f05002e

.field public static final allow_roaming:I = 0x7f050030

.field public static final apn:I = 0x7f050079

.field public static final app_name:I = 0x7f050000

.field public static final apply_last_fix:I = 0x7f050005

.field public static final assistance_data:I = 0x7f05003c

.field public static final both:I = 0x7f05004b

.field public static final cancel:I = 0x7f050002

.field public static final cell_info:I = 0x7f050081

.field public static final certificate_verification:I = 0x7f050057

.field public static final cold:I = 0x7f050012

.field public static final connect:I = 0x7f05007c

.field public static final connectionTest:I = 0x7f05007b

.field public static final connection_off:I = 0x7f05007f

.field public static final connection_on:I = 0x7f050080

.field public static final control_plane:I = 0x7f050038

.field public static final cp_number:I = 0x7f05003e

.field public static final cp_settings:I = 0x7f05003a

.field public static final cp_up_switch:I = 0x7f050037

.field public static final delay:I = 0x7f050055

.field public static final delay_before_deleting_data:I = 0x7f050017

.field public static final delay_before_starting_epo:I = 0x7f05006c

.field public static final delay_before_starting_gps:I = 0x7f050016

.field public static final delay_before_stopping_gps:I = 0x7f050019

.field public static final delete:I = 0x7f05000c

.field public static final dsd:I = 0x7f050076

.field public static final ecid_enable:I = 0x7f050032

.field public static final edit:I = 0x7f050046

.field public static final enable_agps:I = 0x7f050027

.field public static final enable_iot:I = 0x7f05002d

.field public static final enable_ni_timer:I = 0x7f050034

.field public static final epo_auto_download:I = 0x7f050062

.field public static final epo_disabled:I = 0x7f050061

.field public static final epo_download_timeout:I = 0x7f05006d

.field public static final epo_downloading:I = 0x7f050063

.field public static final epo_enable:I = 0x7f050060

.field public static final epo_file_info:I = 0x7f050069

.field public static final epo_functionality:I = 0x7f05005f

.field public static final epo_idle:I = 0x7f050064

.field public static final epo_log:I = 0x7f050068

.field public static final epo_num_of_loop:I = 0x7f05006b

.field public static final epo_period_setting:I = 0x7f050067

.field public static final epo_stress:I = 0x7f05006e

.field public static final epo_stress_off:I = 0x7f050070

.field public static final epo_stress_on:I = 0x7f05006f

.field public static final epo_stress_test_settings:I = 0x7f05006a

.field public static final epo_update:I = 0x7f050066

.field public static final external_addr:I = 0x7f05003d

.field public static final feature_enabler:I = 0x7f05002c

.field public static final file_location_em:I = 0x7f050073

.field public static final file_sdcard:I = 0x7f050072

.field public static final full:I = 0x7f050013

.field public static final geo_bindservice:I = 0x7f05008a

.field public static final geo_bindservice_listtitle:I = 0x7f05008d

.field public static final geo_bindservice_summary:I = 0x7f05008c

.field public static final geo_bindservice_title:I = 0x7f05008b

.field public static final geo_failed:I = 0x7f05008f

.field public static final geo_now:I = 0x7f050091

.field public static final geo_reboot:I = 0x7f050090

.field public static final geo_sucess:I = 0x7f05008e

.field public static final geo_use_limit:I = 0x7f050098

.field public static final geo_use_limit_detail_description:I = 0x7f05009c

.field public static final geo_use_limit_detail_title:I = 0x7f05009b

.field public static final geo_use_limit_summary:I = 0x7f05009a

.field public static final geo_use_limit_title:I = 0x7f050099

.field public static final go_to_map:I = 0x7f0500a7

.field public static final gps:I = 0x7f050077

.field public static final gps_off:I = 0x7f050009

.field public static final gps_on:I = 0x7f050008

.field public static final horizontal_accuracy:I = 0x7f050052

.field public static final hot:I = 0x7f050010

.field public static final imsi:I = 0x7f05004e

.field public static final ipv4:I = 0x7f05004d

.field public static final k_value:I = 0x7f050050

.field public static final lab_performance:I = 0x7f05005e

.field public static final latitude:I = 0x7f050003

.field public static final load_profile:I = 0x7f050024

.field public static final locale:I = 0x7f050006

.field public static final location_age:I = 0x7f050054

.field public static final location_estimate:I = 0x7f05003b

.field public static final log_nmea:I = 0x7f05000e

.field public static final log_supl_to_file:I = 0x7f050031

.field public static final log_to_sdcard:I = 0x7f05000f

.field public static final longitude:I = 0x7f050004

.field public static final menu:I = 0x7f050071

.field public static final meter:I = 0x7f050051

.field public static final minute:I = 0x7f050065

.field public static final mlc_number:I = 0x7f05003f

.field public static final molr_position_method:I = 0x7f05005b

.field public static final msa:I = 0x7f05002b

.field public static final msb:I = 0x7f05002a

.field public static final msic_goto:I = 0x7f050074

.field public static final net_bindservice:I = 0x7f050082

.field public static final net_bindservice_listtitle:I = 0x7f050085

.field public static final net_bindservice_summary:I = 0x7f050084

.field public static final net_bindservice_title:I = 0x7f050083

.field public static final net_failed:I = 0x7f050087

.field public static final net_now:I = 0x7f050088

.field public static final net_reboot:I = 0x7f050089

.field public static final net_reportfrequency:I = 0x7f050092

.field public static final net_reportfrequency_entriesDefaultValues:I = 0x7f050096

.field public static final net_reportfrequency_listtitle:I = 0x7f050095

.field public static final net_reportfrequency_summary:I = 0x7f050094

.field public static final net_reportfrequency_title:I = 0x7f050093

.field public static final net_reportfrequency_value:I = 0x7f050097

.field public static final net_sucess:I = 0x7f050086

.field public static final ni_dialog_custom:I = 0x7f050033

.field public static final ni_dialog_test:I = 0x7f050023

.field public static final notification_timeout:I = 0x7f050035

.field public static final num_0:I = 0x7f05005a

.field public static final num_10:I = 0x7f050058

.field public static final num_16:I = 0x7f050059

.field public static final num_of_fix:I = 0x7f050014

.field public static final ok:I = 0x7f050001

.field public static final query_result_geocoding_text:I = 0x7f0500a3

.field public static final query_result_init_text:I = 0x7f0500a2

.field public static final ref_latlng:I = 0x7f05000d

.field public static final reset_agpsd:I = 0x7f050022

.field public static final reset_to_default:I = 0x7f050056

.field public static final rrc:I = 0x7f05004a

.field public static final rrlp:I = 0x7f050049

.field public static final select:I = 0x7f050007

.field public static final server_addr:I = 0x7f05007d

.field public static final server_port:I = 0x7f05007e

.field public static final session_timeout:I = 0x7f050018

.field public static final set_id:I = 0x7f05004c

.field public static final set_profile:I = 0x7f050025

.field public static final sim1_preferred:I = 0x7f050040

.field public static final sim2_preferred:I = 0x7f050041

.field public static final sim_selection:I = 0x7f05005c

.field public static final slp_addr:I = 0x7f050044

.field public static final slp_port:I = 0x7f050045

.field public static final slp_profile:I = 0x7f050043

.field public static final slp_template:I = 0x7f05005d

.field public static final standalone:I = 0x7f050029

.field public static final stress_off:I = 0x7f05000b

.field public static final stress_on:I = 0x7f05000a

.field public static final stress_test_settings:I = 0x7f050015

.field public static final table_layout_cell_carrier:I = 0x7f0500a1

.field public static final table_layout_cell_id:I = 0x7f05009d

.field public static final table_layout_cell_lac:I = 0x7f05009e

.field public static final table_layout_cell_mcc:I = 0x7f05009f

.field public static final table_layout_cell_mnc:I = 0x7f0500a0

.field public static final test1:I = 0x7f05001a

.field public static final test2:I = 0x7f05001b

.field public static final test3:I = 0x7f05001c

.field public static final test4:I = 0x7f05001d

.field public static final test5:I = 0x7f05001e

.field public static final test6:I = 0x7f05001f

.field public static final test7:I = 0x7f050020

.field public static final test8:I = 0x7f050021

.field public static final time:I = 0x7f050078

.field public static final tls_enable:I = 0x7f050047

.field public static final type:I = 0x7f050048

.field public static final up_settings:I = 0x7f050042

.field public static final user_plane:I = 0x7f050039

.field public static final verification_timeout:I = 0x7f050036

.field public static final vertical_accuracy:I = 0x7f050053

.field public static final warm:I = 0x7f050011

.field public static final wifi:I = 0x7f05007a

.field public static final ygps:I = 0x7f050075


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
