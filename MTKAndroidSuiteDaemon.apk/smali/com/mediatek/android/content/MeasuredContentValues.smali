.class public Lcom/mediatek/android/content/MeasuredContentValues;
.super Ljava/lang/Object;
.source "MeasuredContentValues.java"


# instance fields
.field private final mContentValues:Landroid/content/ContentValues;

.field private mParcelSize:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/mediatek/android/content/MeasuredContentValues;-><init>(I)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0, p1}, Landroid/content/ContentValues;-><init>(I)V

    iput-object v0, p0, Lcom/mediatek/android/content/MeasuredContentValues;->mContentValues:Landroid/content/ContentValues;

    const/4 v0, 0x4

    iput v0, p0, Lcom/mediatek/android/content/MeasuredContentValues;->mParcelSize:I

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/android/content/MeasuredContentValues;->mContentValues:Landroid/content/ContentValues;

    invoke-virtual {v0}, Landroid/content/ContentValues;->clear()V

    const/4 v0, 0x4

    iput v0, p0, Lcom/mediatek/android/content/MeasuredContentValues;->mParcelSize:I

    return-void
.end method

.method public getValues()Landroid/content/ContentValues;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/android/content/MeasuredContentValues;->mContentValues:Landroid/content/ContentValues;

    return-object v0
.end method

.method public measure()I
    .locals 1

    iget v0, p0, Lcom/mediatek/android/content/MeasuredContentValues;->mParcelSize:I

    return v0
.end method

.method public measureValue(Ljava/lang/Integer;)I
    .locals 1
    .param p1    # Ljava/lang/Integer;

    if-nez p1, :cond_0

    const/4 v0, 0x4

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public measureValue(Ljava/lang/Long;)I
    .locals 1
    .param p1    # Ljava/lang/Long;

    if-nez p1, :cond_0

    const/4 v0, 0x4

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0xc

    goto :goto_0
.end method

.method public measureValue(Ljava/lang/String;)I
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    const/4 v0, 0x4

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x3

    mul-int/lit8 v0, v1, 0x4

    goto :goto_0
.end method

.method public measureValue([B)I
    .locals 2
    .param p1    # [B

    if-nez p1, :cond_0

    const/4 v0, 0x4

    :goto_0
    return v0

    :cond_0
    array-length v1, p1

    add-int/lit8 v0, v1, 0x8

    goto :goto_0
.end method

.method public put(Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Integer;

    iget-object v0, p0, Lcom/mediatek/android/content/MeasuredContentValues;->mContentValues:Landroid/content/ContentValues;

    invoke-virtual {v0, p1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/mediatek/android/content/MeasuredContentValues;->mParcelSize:I

    invoke-virtual {p0, p1}, Lcom/mediatek/android/content/MeasuredContentValues;->measureValue(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, p2}, Lcom/mediatek/android/content/MeasuredContentValues;->measureValue(Ljava/lang/Integer;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/mediatek/android/content/MeasuredContentValues;->mParcelSize:I

    :cond_0
    iget-object v0, p0, Lcom/mediatek/android/content/MeasuredContentValues;->mContentValues:Landroid/content/ContentValues;

    invoke-virtual {v0, p1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    return-void
.end method

.method public put(Ljava/lang/String;Ljava/lang/Long;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Long;

    iget-object v0, p0, Lcom/mediatek/android/content/MeasuredContentValues;->mContentValues:Landroid/content/ContentValues;

    invoke-virtual {v0, p1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/mediatek/android/content/MeasuredContentValues;->mParcelSize:I

    invoke-virtual {p0, p1}, Lcom/mediatek/android/content/MeasuredContentValues;->measureValue(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, p2}, Lcom/mediatek/android/content/MeasuredContentValues;->measureValue(Ljava/lang/Long;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/mediatek/android/content/MeasuredContentValues;->mParcelSize:I

    :cond_0
    iget-object v0, p0, Lcom/mediatek/android/content/MeasuredContentValues;->mContentValues:Landroid/content/ContentValues;

    invoke-virtual {v0, p1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    return-void
.end method

.method public put(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/android/content/MeasuredContentValues;->mContentValues:Landroid/content/ContentValues;

    invoke-virtual {v1, p1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/android/content/MeasuredContentValues;->mContentValues:Landroid/content/ContentValues;

    invoke-virtual {v1, p1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/mediatek/android/content/MeasuredContentValues;->mParcelSize:I

    invoke-virtual {p0, p2}, Lcom/mediatek/android/content/MeasuredContentValues;->measureValue(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p0, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->measureValue(Ljava/lang/String;)I

    move-result v3

    sub-int/2addr v2, v3

    add-int/2addr v1, v2

    iput v1, p0, Lcom/mediatek/android/content/MeasuredContentValues;->mParcelSize:I

    :goto_0
    iget-object v1, p0, Lcom/mediatek/android/content/MeasuredContentValues;->mContentValues:Landroid/content/ContentValues;

    invoke-virtual {v1, p1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    iget v1, p0, Lcom/mediatek/android/content/MeasuredContentValues;->mParcelSize:I

    invoke-virtual {p0, p1}, Lcom/mediatek/android/content/MeasuredContentValues;->measureValue(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p0, p2}, Lcom/mediatek/android/content/MeasuredContentValues;->measureValue(Ljava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    iput v1, p0, Lcom/mediatek/android/content/MeasuredContentValues;->mParcelSize:I

    goto :goto_0
.end method

.method public put(Ljava/lang/String;[B)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # [B

    iget-object v1, p0, Lcom/mediatek/android/content/MeasuredContentValues;->mContentValues:Landroid/content/ContentValues;

    invoke-virtual {v1, p1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/android/content/MeasuredContentValues;->mContentValues:Landroid/content/ContentValues;

    invoke-virtual {v1, p1}, Landroid/content/ContentValues;->getAsByteArray(Ljava/lang/String;)[B

    move-result-object v0

    iget v1, p0, Lcom/mediatek/android/content/MeasuredContentValues;->mParcelSize:I

    invoke-virtual {p0, p2}, Lcom/mediatek/android/content/MeasuredContentValues;->measureValue([B)I

    move-result v2

    invoke-virtual {p0, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->measureValue([B)I

    move-result v3

    sub-int/2addr v2, v3

    add-int/2addr v1, v2

    iput v1, p0, Lcom/mediatek/android/content/MeasuredContentValues;->mParcelSize:I

    :goto_0
    iget-object v1, p0, Lcom/mediatek/android/content/MeasuredContentValues;->mContentValues:Landroid/content/ContentValues;

    invoke-virtual {v1, p1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    return-void

    :cond_0
    iget v1, p0, Lcom/mediatek/android/content/MeasuredContentValues;->mParcelSize:I

    invoke-virtual {p0, p1}, Lcom/mediatek/android/content/MeasuredContentValues;->measureValue(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p0, p2}, Lcom/mediatek/android/content/MeasuredContentValues;->measureValue([B)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    iput v1, p0, Lcom/mediatek/android/content/MeasuredContentValues;->mParcelSize:I

    goto :goto_0
.end method
