.class public abstract Lcom/mediatek/android/content/OperationBatchHelper;
.super Ljava/lang/Object;
.source "OperationBatchHelper.java"


# instance fields
.field private final mOpBatch:Lcom/mediatek/android/content/ContentProviderOperationBatch;


# direct methods
.method public constructor <init>(Lcom/mediatek/android/content/ContentProviderOperationBatch;)V
    .locals 0
    .param p1    # Lcom/mediatek/android/content/ContentProviderOperationBatch;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mediatek/android/content/OperationBatchHelper;->mOpBatch:Lcom/mediatek/android/content/ContentProviderOperationBatch;

    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "OperationBatchHelper"

    return-object v0
.end method

.method public abstract onAppend(Lcom/mediatek/android/content/ContentProviderOperationBatch;I)V
.end method

.method public abstract onApply(Lcom/mediatek/android/content/ContentProviderOperationBatch;)[Landroid/content/ContentProviderResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Landroid/content/OperationApplicationException;
        }
    .end annotation
.end method

.method public abstract onOperationResult(Landroid/content/ContentProviderResult;I)V
.end method

.method public run(I)V
    .locals 13
    .param p1    # I

    const/4 v3, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v3, p1, :cond_1

    iget-object v8, p0, Lcom/mediatek/android/content/OperationBatchHelper;->mOpBatch:Lcom/mediatek/android/content/ContentProviderOperationBatch;

    invoke-virtual {p0, v8, v3}, Lcom/mediatek/android/content/OperationBatchHelper;->onAppend(Lcom/mediatek/android/content/ContentProviderOperationBatch;I)V

    iget-object v8, p0, Lcom/mediatek/android/content/OperationBatchHelper;->mOpBatch:Lcom/mediatek/android/content/ContentProviderOperationBatch;

    invoke-virtual {v8}, Lcom/mediatek/android/content/ContentProviderOperationBatch;->isFull()Z

    move-result v8

    if-nez v8, :cond_0

    add-int/lit8 v8, p1, -0x1

    if-ne v3, v8, :cond_2

    :cond_0
    :try_start_0
    iget-object v8, p0, Lcom/mediatek/android/content/OperationBatchHelper;->mOpBatch:Lcom/mediatek/android/content/ContentProviderOperationBatch;

    invoke-virtual {p0, v8}, Lcom/mediatek/android/content/OperationBatchHelper;->onApply(Lcom/mediatek/android/content/ContentProviderOperationBatch;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    iget-object v8, p0, Lcom/mediatek/android/content/OperationBatchHelper;->mOpBatch:Lcom/mediatek/android/content/ContentProviderOperationBatch;

    invoke-virtual {v8}, Lcom/mediatek/android/content/ContentProviderOperationBatch;->clear()V

    if-eqz v6, :cond_2

    move-object v0, v6

    array-length v5, v0

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v5, :cond_2

    aget-object v7, v0, v4

    invoke-virtual {p0, v7, v2}, Lcom/mediatek/android/content/OperationBatchHelper;->onOperationResult(Landroid/content/ContentProviderResult;I)V

    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :catch_0
    move-exception v1

    :try_start_1
    invoke-virtual {p0}, Lcom/mediatek/android/content/OperationBatchHelper;->getName()Ljava/lang/String;

    move-result-object v8

    const-string v9, "run"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Exception occurs in onApply("

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/mediatek/android/content/OperationBatchHelper;->mOpBatch:Lcom/mediatek/android/content/ContentProviderOperationBatch;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ")"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v8, v9, v10, v11, v1}, Lcom/mediatek/apst/target/util/Debugger;->logE(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v8, p0, Lcom/mediatek/android/content/OperationBatchHelper;->mOpBatch:Lcom/mediatek/android/content/ContentProviderOperationBatch;

    invoke-virtual {v8}, Lcom/mediatek/android/content/ContentProviderOperationBatch;->clear()V

    :cond_1
    :goto_2
    return-void

    :catch_1
    move-exception v1

    :try_start_2
    invoke-virtual {p0}, Lcom/mediatek/android/content/OperationBatchHelper;->getName()Ljava/lang/String;

    move-result-object v8

    const-string v9, "run"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Exception occurs in onApply("

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/mediatek/android/content/OperationBatchHelper;->mOpBatch:Lcom/mediatek/android/content/ContentProviderOperationBatch;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ")"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v8, v9, v10, v11, v1}, Lcom/mediatek/apst/target/util/Debugger;->logE(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v8, p0, Lcom/mediatek/android/content/OperationBatchHelper;->mOpBatch:Lcom/mediatek/android/content/ContentProviderOperationBatch;

    invoke-virtual {v8}, Lcom/mediatek/android/content/ContentProviderOperationBatch;->clear()V

    goto :goto_2

    :catchall_0
    move-exception v8

    iget-object v9, p0, Lcom/mediatek/android/content/OperationBatchHelper;->mOpBatch:Lcom/mediatek/android/content/ContentProviderOperationBatch;

    invoke-virtual {v9}, Lcom/mediatek/android/content/ContentProviderOperationBatch;->clear()V

    throw v8

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0
.end method
