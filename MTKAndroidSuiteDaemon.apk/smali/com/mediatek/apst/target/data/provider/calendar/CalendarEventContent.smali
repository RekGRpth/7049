.class public abstract Lcom/mediatek/apst/target/data/provider/calendar/CalendarEventContent;
.super Ljava/lang/Object;
.source "CalendarEventContent.java"


# static fields
.field public static final ACCESS_CONFIDENTIAL:I = 0x1

.field public static final ACCESS_DEFAULT:I = 0x0

.field public static final ACCESS_LEVEL:Ljava/lang/String; = "accessLevel"

.field public static final ACCESS_PRIVATE:I = 0x2

.field public static final ACCESS_PUBLIC:I = 0x3

.field public static final ALL_DAY:Ljava/lang/String; = "allDay"

.field public static final AVAILABILITY:Ljava/lang/String; = "availability"

.field public static final AVAILABILITY_BUSY:I = 0x0

.field public static final AVAILABILITY_FREE:I = 0x1

.field public static final CALENDAR_ID:Ljava/lang/String; = "calendar_id"

.field public static final CAN_INVITE_OTHERS:Ljava/lang/String; = "canInviteOthers"

.field public static final COLUMN_ID:Ljava/lang/String; = "_id"

.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final CREATE_TIME:Ljava/lang/String; = "createTime"

.field public static final DELETED:Ljava/lang/String; = "deleted"

.field public static final DESCRIPTION:Ljava/lang/String; = "description"

.field public static final DTEND:Ljava/lang/String; = "dtend"

.field public static final DTSTART:Ljava/lang/String; = "dtstart"

.field public static final DURATION:Ljava/lang/String; = "duration"

.field public static final EVENT_COLOR:Ljava/lang/String; = "eventColor"

.field public static final EVENT_END_TIMEZONE:Ljava/lang/String; = "eventEndTimezone"

.field public static final EVENT_LOCATION:Ljava/lang/String; = "eventLocation"

.field public static final EVENT_TIMEZONE:Ljava/lang/String; = "eventTimezone"

.field public static final EXDATE:Ljava/lang/String; = "exdate"

.field public static final EXRULE:Ljava/lang/String; = "exrule"

.field public static final GUESTS_CAN_INVITE_OTHERS:Ljava/lang/String; = "guestsCanInviteOthers"

.field public static final GUESTS_CAN_MODIFY:Ljava/lang/String; = "guestsCanModify"

.field public static final GUESTS_CAN_SEE_GUESTS:Ljava/lang/String; = "guestsCanSeeGuests"

.field public static final HAS_ALARM:Ljava/lang/String; = "hasAlarm"

.field public static final HAS_ATTENDEE_DATA:Ljava/lang/String; = "hasAttendeeData"

.field public static final HAS_EXTENDED_PROPERTIES:Ljava/lang/String; = "hasExtendedProperties"

.field public static final LAST_DATE:Ljava/lang/String; = "lastDate"

.field public static final LAST_SYNCED:Ljava/lang/String; = "lastSynced"

.field public static final MODIFY_TIME:Ljava/lang/String; = "modifyTime"

.field public static final ORGANIZER:Ljava/lang/String; = "organizer"

.field public static final ORIGINAL_ALL_DAY:Ljava/lang/String; = "originalAllDay"

.field public static final ORIGINAL_ID:Ljava/lang/String; = "original_id"

.field public static final ORIGINAL_INSTANCE_TIME:Ljava/lang/String; = "originalInstanceTime"

.field public static final ORIGINAL_SYNC_ID:Ljava/lang/String; = "original_sync_id"

.field public static final RDATE:Ljava/lang/String; = "rdate"

.field public static final RRULE:Ljava/lang/String; = "rrule"

.field public static final SELF_ATTENDEE_STATUS:Ljava/lang/String; = "selfAttendeeStatus"

.field public static final STATUS:Ljava/lang/String; = "eventStatus"

.field public static final STATUS_CANCELED:I = 0x2

.field public static final STATUS_CONFIRMED:I = 0x1

.field public static final STATUS_TENTATIVE:I = 0x0

.field public static final SYNC_DATA1:Ljava/lang/String; = "sync_data1"

.field public static final SYNC_DATA10:Ljava/lang/String; = "sync_data10"

.field public static final SYNC_DATA2:Ljava/lang/String; = "sync_data2"

.field public static final SYNC_DATA3:Ljava/lang/String; = "sync_data3"

.field public static final SYNC_DATA4:Ljava/lang/String; = "sync_data4"

.field public static final SYNC_DATA5:Ljava/lang/String; = "sync_data5"

.field public static final SYNC_DATA6:Ljava/lang/String; = "sync_data6"

.field public static final SYNC_DATA7:Ljava/lang/String; = "sync_data7"

.field public static final SYNC_DATA8:Ljava/lang/String; = "sync_data8"

.field public static final SYNC_DATA9:Ljava/lang/String; = "sync_data9"

.field public static final TITLE:Ljava/lang/String; = "title"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "content://com.android.calendar/events"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/mediatek/apst/target/data/provider/calendar/CalendarEventContent;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cursorToEvent(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;
    .locals 10
    .param p0    # Landroid/database/Cursor;

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v9, -0x1

    if-eqz p0, :cond_0

    invoke-interface {p0}, Landroid/database/Cursor;->getPosition()I

    move-result v7

    if-eq v7, v9, :cond_0

    invoke-interface {p0}, Landroid/database/Cursor;->getPosition()I

    move-result v7

    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v8

    if-ne v7, v8, :cond_2

    :cond_0
    move-object v2, v4

    :cond_1
    :goto_0
    return-object v2

    :cond_2
    new-instance v2, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;

    invoke-direct {v2}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;-><init>()V

    :try_start_0
    const-string v7, "_id"

    invoke-interface {p0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v9, v0, :cond_3

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    invoke-virtual {v2, v7, v8}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->setId(J)V

    :cond_3
    const-string v7, "calendar_id"

    invoke-interface {p0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v9, v0, :cond_4

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    invoke-virtual {v2, v7, v8}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->setCalendarId(J)V

    :cond_4
    const-string v7, "title"

    invoke-interface {p0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v9, v0, :cond_5

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->setTitle(Ljava/lang/String;)V

    :cond_5
    const-string v7, "dtstart"

    invoke-interface {p0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v9, v0, :cond_6

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    invoke-virtual {v2, v7, v8}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->setTimeFrom(J)V

    :cond_6
    const-string v7, "dtend"

    invoke-interface {p0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v9, v0, :cond_7

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    invoke-virtual {v2, v7, v8}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->setTimeTo(J)V

    :cond_7
    const-string v7, "allDay"

    invoke-interface {p0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v9, v0, :cond_8

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    if-ne v7, v5, :cond_12

    move v3, v5

    :goto_1
    invoke-virtual {v2, v3}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->setAllDay(Z)V

    :cond_8
    const-string v7, "eventLocation"

    invoke-interface {p0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v9, v0, :cond_9

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->setEventLocation(Ljava/lang/String;)V

    :cond_9
    const-string v7, "description"

    invoke-interface {p0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v9, v0, :cond_a

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->setDescription(Ljava/lang/String;)V

    :cond_a
    const-string v7, "organizer"

    invoke-interface {p0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v9, v0, :cond_b

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->setCalendarOwner(Ljava/lang/String;)V

    :cond_b
    const-string v7, "rrule"

    invoke-interface {p0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v9, v0, :cond_c

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->setRepetition(Ljava/lang/String;)V

    :cond_c
    const-string v7, "modifyTime"

    invoke-interface {p0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v9, v0, :cond_d

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    invoke-virtual {v2, v7, v8}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->setModifyTime(J)V

    :cond_d
    const-string v7, "createTime"

    invoke-interface {p0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v9, v0, :cond_e

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    invoke-virtual {v2, v7, v8}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->setCreateTime(J)V

    :cond_e
    const-string v7, "duration"

    invoke-interface {p0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v9, v0, :cond_f

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->setDuration(Ljava/lang/String;)V

    :cond_f
    const-string v7, "eventTimezone"

    invoke-interface {p0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v9, v0, :cond_10

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->setTimeZone(Ljava/lang/String;)V

    :cond_10
    const-string v7, "availability"

    invoke-interface {p0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v9, v0, :cond_11

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-virtual {v2, v7}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->setAvailability(I)V

    :cond_11
    const-string v7, "accessLevel"

    invoke-interface {p0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v9, v0, :cond_1

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-virtual {v2, v7}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->setAccessLevel(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v1

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p0, v5, v6

    invoke-static {v5, v4, v1}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :cond_12
    move v3, v6

    goto/16 :goto_1
.end method

.method public static cursorToRaw(Landroid/database/Cursor;Ljava/nio/ByteBuffer;)I
    .locals 10
    .param p0    # Landroid/database/Cursor;
    .param p1    # Ljava/nio/ByteBuffer;

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v9, 0x0

    const/4 v3, 0x0

    const/4 v8, -0x1

    if-nez p0, :cond_0

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p0, v5, v3

    aput-object p1, v5, v4

    const-string v4, "Cursor is null."

    invoke-static {v5, v4}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return v3

    :cond_0
    invoke-interface {p0}, Landroid/database/Cursor;->getPosition()I

    move-result v6

    if-eq v6, v8, :cond_1

    invoke-interface {p0}, Landroid/database/Cursor;->getPosition()I

    move-result v6

    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v7

    if-ne v6, v7, :cond_2

    :cond_1
    new-array v5, v5, [Ljava/lang/Object;

    aput-object p0, v5, v3

    aput-object p1, v5, v4

    const-string v4, "Cursor has moved to the end."

    invoke-static {v5, v4}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    if-nez p1, :cond_3

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p0, v5, v3

    aput-object p1, v5, v4

    const-string v4, "Buffer is null."

    invoke-static {v5, v4}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Ljava/nio/Buffer;->mark()Ljava/nio/Buffer;

    :try_start_0
    const-string v6, "_id"

    invoke-interface {p0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v8, v0, :cond_4

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-virtual {p1, v6, v7}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    :goto_1
    const-string v6, "calendar_id"

    invoke-interface {p0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v8, v0, :cond_5

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-virtual {p1, v6, v7}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    :goto_2
    const-string v6, "title"

    invoke-interface {p0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v8, v0, :cond_6

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {p1, v6}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_3
    const-string v6, "dtstart"

    invoke-interface {p0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v8, v0, :cond_7

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-virtual {p1, v6, v7}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    :goto_4
    const-string v6, "dtend"

    invoke-interface {p0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v8, v0, :cond_8

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-virtual {p1, v6, v7}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    :goto_5
    const-string v6, "allDay"

    invoke-interface {p0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v8, v0, :cond_a

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    if-ne v6, v4, :cond_9

    move v2, v4

    :goto_6
    invoke-static {p1, v2}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putBoolean(Ljava/nio/ByteBuffer;Z)V

    :goto_7
    const-string v6, "eventLocation"

    invoke-interface {p0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v8, v0, :cond_b

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {p1, v6}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_8
    const-string v6, "description"

    invoke-interface {p0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v8, v0, :cond_c

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {p1, v6}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_9
    const-string v6, "organizer"

    invoke-interface {p0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v8, v0, :cond_d

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {p1, v6}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_a
    const/4 v6, 0x0

    invoke-virtual {p1, v6}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    const-string v6, "rrule"

    invoke-interface {p0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v8, v0, :cond_e

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {p1, v6}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_b
    const/4 v6, 0x0

    invoke-virtual {p1, v6}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    const-string v6, "modifyTime"

    invoke-interface {p0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v8, v0, :cond_f

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-virtual {p1, v6, v7}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    :goto_c
    const-string v6, "createTime"

    invoke-interface {p0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v8, v0, :cond_10

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-virtual {p1, v6, v7}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    :goto_d
    const-string v6, "duration"

    invoke-interface {p0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v8, v0, :cond_11

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {p1, v6}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_e
    const-string v6, "eventTimezone"

    invoke-interface {p0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v8, v0, :cond_12

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {p1, v6}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_f
    const-string v6, "availability"

    invoke-interface {p0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v8, v0, :cond_13

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-virtual {p1, v6}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    :goto_10
    const-string v6, "accessLevel"

    invoke-interface {p0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v8, v0, :cond_14

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-virtual {p1, v6}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    :goto_11
    move v3, v4

    goto/16 :goto_0

    :cond_4
    const-wide/16 v6, -0x1

    invoke-virtual {p1, v6, v7}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/nio/BufferOverflowException; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_1

    :catch_0
    move-exception v1

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p0, v5, v3

    aput-object p1, v5, v4

    invoke-static {v5, v9, v1}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {p1}, Ljava/nio/Buffer;->reset()Ljava/nio/Buffer;

    goto/16 :goto_0

    :cond_5
    const-wide/16 v6, 0x0

    :try_start_1
    invoke-virtual {p1, v6, v7}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/nio/BufferOverflowException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_2

    :catch_1
    move-exception v1

    invoke-virtual {p1}, Ljava/nio/Buffer;->reset()Ljava/nio/Buffer;

    move v3, v5

    goto/16 :goto_0

    :cond_6
    const/4 v6, 0x0

    :try_start_2
    invoke-static {p1, v6}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_7
    const-wide/16 v6, 0x0

    invoke-virtual {p1, v6, v7}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    goto/16 :goto_4

    :cond_8
    const-wide/16 v6, 0x0

    invoke-virtual {p1, v6, v7}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    goto/16 :goto_5

    :cond_9
    move v2, v3

    goto/16 :goto_6

    :cond_a
    const/4 v6, 0x0

    invoke-static {p1, v6}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putBoolean(Ljava/nio/ByteBuffer;Z)V

    goto/16 :goto_7

    :cond_b
    const/4 v6, 0x0

    invoke-static {p1, v6}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    goto/16 :goto_8

    :cond_c
    const/4 v6, 0x0

    invoke-static {p1, v6}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    goto/16 :goto_9

    :cond_d
    const/4 v6, 0x0

    invoke-static {p1, v6}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    goto/16 :goto_a

    :cond_e
    const/4 v6, 0x0

    invoke-static {p1, v6}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    goto/16 :goto_b

    :cond_f
    const-wide/16 v6, 0x0

    invoke-virtual {p1, v6, v7}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    goto/16 :goto_c

    :cond_10
    const-wide/16 v6, 0x0

    invoke-virtual {p1, v6, v7}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    goto/16 :goto_d

    :cond_11
    const/4 v6, 0x0

    invoke-static {p1, v6}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    goto/16 :goto_e

    :cond_12
    const/4 v6, 0x0

    invoke-static {p1, v6}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    goto/16 :goto_f

    :cond_13
    const/4 v6, 0x0

    invoke-interface {p0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-virtual {p1, v6}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto/16 :goto_10

    :cond_14
    const/4 v6, 0x0

    invoke-interface {p0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-virtual {p1, v6}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/nio/BufferOverflowException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_11
.end method
