.class public abstract Lcom/mediatek/apst/target/data/provider/contacts/ContactDataContent;
.super Ljava/lang/Object;
.source "ContactDataContent.java"


# static fields
.field public static final COLUMN_BINDING_SIM_ID:Ljava/lang/String; = "sim_id"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createContentValues(Lcom/mediatek/apst/util/entity/contacts/ContactData;Z)Landroid/content/ContentValues;
    .locals 18
    .param p0    # Lcom/mediatek/apst/util/entity/contacts/ContactData;
    .param p1    # Z

    if-nez p0, :cond_0

    const/4 v13, 0x0

    :goto_0
    return-object v13

    :cond_0
    if-eqz p1, :cond_2

    const/4 v1, 0x2

    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/apst/util/entity/contacts/ContactData;->getMimeType()I

    move-result v5

    move-object/from16 v0, p0

    instance-of v15, v0, Lcom/mediatek/apst/util/entity/contacts/StructuredName;

    if-eqz v15, :cond_3

    move-object/from16 v6, p0

    check-cast v6, Lcom/mediatek/apst/util/entity/contacts/StructuredName;

    new-instance v13, Landroid/content/ContentValues;

    add-int/lit8 v15, v1, 0x9

    invoke-direct {v13, v15}, Landroid/content/ContentValues;-><init>(I)V

    const-string v15, "mimetype"

    const-string v16, "vnd.android.cursor.item/name"

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data2"

    invoke-virtual {v6}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->getGivenName()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data3"

    invoke-virtual {v6}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->getFamilyName()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data5"

    invoke-virtual {v6}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->getMiddleName()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data4"

    invoke-virtual {v6}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->getPrefix()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data6"

    invoke-virtual {v6}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->getSuffix()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data9"

    invoke-virtual {v6}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->getPhoneticFamilyName()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data7"

    invoke-virtual {v6}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->getPhoneticGivenName()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data8"

    invoke-virtual {v6}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->getPhoneticMiddleName()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    if-eqz p1, :cond_1

    const-string v15, "_id"

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->getId()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_1
    const-string v15, "raw_contact_id"

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/apst/util/entity/contacts/ContactData;->getRawContactId()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto/16 :goto_0

    :cond_2
    const/4 v1, 0x1

    goto/16 :goto_1

    :cond_3
    move-object/from16 v0, p0

    instance-of v15, v0, Lcom/mediatek/apst/util/entity/contacts/Phone;

    if-eqz v15, :cond_4

    move-object/from16 v10, p0

    check-cast v10, Lcom/mediatek/apst/util/entity/contacts/Phone;

    new-instance v13, Landroid/content/ContentValues;

    add-int/lit8 v15, v1, 0x4

    invoke-direct {v13, v15}, Landroid/content/ContentValues;-><init>(I)V

    const-string v15, "mimetype"

    const-string v16, "vnd.android.cursor.item/phone_v2"

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data1"

    invoke-virtual {v10}, Lcom/mediatek/apst/util/entity/contacts/Phone;->getNumber()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data2"

    invoke-virtual {v10}, Lcom/mediatek/apst/util/entity/contacts/Phone;->getType()I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v15, "data3"

    invoke-virtual {v10}, Lcom/mediatek/apst/util/entity/contacts/Phone;->getLabel()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "sim_id"

    invoke-virtual {v10}, Lcom/mediatek/apst/util/entity/contacts/Phone;->getBindingSimId()I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_2

    :cond_4
    move-object/from16 v0, p0

    instance-of v15, v0, Lcom/mediatek/apst/util/entity/contacts/Photo;

    if-eqz v15, :cond_5

    move-object/from16 v11, p0

    check-cast v11, Lcom/mediatek/apst/util/entity/contacts/Photo;

    new-instance v13, Landroid/content/ContentValues;

    add-int/lit8 v15, v1, 0x2

    invoke-direct {v13, v15}, Landroid/content/ContentValues;-><init>(I)V

    const-string v15, "mimetype"

    const-string v16, "vnd.android.cursor.item/photo"

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data15"

    invoke-virtual {v11}, Lcom/mediatek/apst/util/entity/contacts/Photo;->getPhotoBytes()[B

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    goto/16 :goto_2

    :cond_5
    move-object/from16 v0, p0

    instance-of v15, v0, Lcom/mediatek/apst/util/entity/contacts/Im;

    if-eqz v15, :cond_6

    move-object/from16 v4, p0

    check-cast v4, Lcom/mediatek/apst/util/entity/contacts/Im;

    new-instance v13, Landroid/content/ContentValues;

    add-int/lit8 v15, v1, 0x6

    invoke-direct {v13, v15}, Landroid/content/ContentValues;-><init>(I)V

    const-string v15, "mimetype"

    const-string v16, "vnd.android.cursor.item/im"

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data1"

    invoke-virtual {v4}, Lcom/mediatek/apst/util/entity/contacts/Im;->getData()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data2"

    invoke-virtual {v4}, Lcom/mediatek/apst/util/entity/contacts/Im;->getType()I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v15, "data3"

    invoke-virtual {v4}, Lcom/mediatek/apst/util/entity/contacts/Im;->getLabel()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data5"

    invoke-virtual {v4}, Lcom/mediatek/apst/util/entity/contacts/Im;->getProtocol()I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v15, "data6"

    invoke-virtual {v4}, Lcom/mediatek/apst/util/entity/contacts/Im;->getCustomProtocol()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_6
    move-object/from16 v0, p0

    instance-of v15, v0, Lcom/mediatek/apst/util/entity/contacts/Email;

    if-eqz v15, :cond_7

    move-object/from16 v2, p0

    check-cast v2, Lcom/mediatek/apst/util/entity/contacts/Email;

    new-instance v13, Landroid/content/ContentValues;

    add-int/lit8 v15, v1, 0x4

    invoke-direct {v13, v15}, Landroid/content/ContentValues;-><init>(I)V

    const-string v15, "mimetype"

    const-string v16, "vnd.android.cursor.item/email_v2"

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data1"

    invoke-virtual {v2}, Lcom/mediatek/apst/util/entity/contacts/Email;->getData()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data2"

    invoke-virtual {v2}, Lcom/mediatek/apst/util/entity/contacts/Email;->getType()I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v15, "data3"

    invoke-virtual {v2}, Lcom/mediatek/apst/util/entity/contacts/Email;->getLabel()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_7
    move-object/from16 v0, p0

    instance-of v15, v0, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;

    if-eqz v15, :cond_8

    move-object/from16 v12, p0

    check-cast v12, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;

    new-instance v13, Landroid/content/ContentValues;

    add-int/lit8 v15, v1, 0xb

    invoke-direct {v13, v15}, Landroid/content/ContentValues;-><init>(I)V

    const-string v15, "mimetype"

    const-string v16, "vnd.android.cursor.item/postal-address_v2"

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data1"

    invoke-virtual {v12}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->getFormattedAddress()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data2"

    invoke-virtual {v12}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->getType()I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v15, "data3"

    invoke-virtual {v12}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->getLabel()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data4"

    invoke-virtual {v12}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->getStreet()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data5"

    invoke-virtual {v12}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->getPobox()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data6"

    invoke-virtual {v12}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->getNeighborhood()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data7"

    invoke-virtual {v12}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->getCity()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data8"

    invoke-virtual {v12}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->getRegion()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data9"

    invoke-virtual {v12}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->getPostcode()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data10"

    invoke-virtual {v12}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->getCountry()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_8
    move-object/from16 v0, p0

    instance-of v15, v0, Lcom/mediatek/apst/util/entity/contacts/Organization;

    if-eqz v15, :cond_9

    move-object/from16 v9, p0

    check-cast v9, Lcom/mediatek/apst/util/entity/contacts/Organization;

    new-instance v13, Landroid/content/ContentValues;

    add-int/lit8 v15, v1, 0xa

    invoke-direct {v13, v15}, Landroid/content/ContentValues;-><init>(I)V

    const-string v15, "mimetype"

    const-string v16, "vnd.android.cursor.item/organization"

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data1"

    invoke-virtual {v9}, Lcom/mediatek/apst/util/entity/contacts/Organization;->getCompany()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data2"

    invoke-virtual {v9}, Lcom/mediatek/apst/util/entity/contacts/Organization;->getType()I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v15, "data3"

    invoke-virtual {v9}, Lcom/mediatek/apst/util/entity/contacts/Organization;->getLabel()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data4"

    invoke-virtual {v9}, Lcom/mediatek/apst/util/entity/contacts/Organization;->getTitle()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data5"

    invoke-virtual {v9}, Lcom/mediatek/apst/util/entity/contacts/Organization;->getTitle()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data6"

    invoke-virtual {v9}, Lcom/mediatek/apst/util/entity/contacts/Organization;->getJobDescription()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data7"

    invoke-virtual {v9}, Lcom/mediatek/apst/util/entity/contacts/Organization;->getSymbol()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data8"

    invoke-virtual {v9}, Lcom/mediatek/apst/util/entity/contacts/Organization;->getPhoneticName()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data9"

    invoke-virtual {v9}, Lcom/mediatek/apst/util/entity/contacts/Organization;->getOfficeLocation()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_9
    move-object/from16 v0, p0

    instance-of v15, v0, Lcom/mediatek/apst/util/entity/contacts/Nickname;

    if-eqz v15, :cond_a

    move-object/from16 v7, p0

    check-cast v7, Lcom/mediatek/apst/util/entity/contacts/Nickname;

    new-instance v13, Landroid/content/ContentValues;

    add-int/lit8 v15, v1, 0x4

    invoke-direct {v13, v15}, Landroid/content/ContentValues;-><init>(I)V

    const-string v15, "mimetype"

    const-string v16, "vnd.android.cursor.item/nickname"

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data1"

    invoke-virtual {v7}, Lcom/mediatek/apst/util/entity/contacts/Nickname;->getName()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data2"

    invoke-virtual {v7}, Lcom/mediatek/apst/util/entity/contacts/Nickname;->getType()I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v15, "data3"

    invoke-virtual {v7}, Lcom/mediatek/apst/util/entity/contacts/Nickname;->getLabel()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_a
    move-object/from16 v0, p0

    instance-of v15, v0, Lcom/mediatek/apst/util/entity/contacts/Website;

    if-eqz v15, :cond_b

    move-object/from16 v14, p0

    check-cast v14, Lcom/mediatek/apst/util/entity/contacts/Website;

    new-instance v13, Landroid/content/ContentValues;

    add-int/lit8 v15, v1, 0x4

    invoke-direct {v13, v15}, Landroid/content/ContentValues;-><init>(I)V

    const-string v15, "mimetype"

    const-string v16, "vnd.android.cursor.item/website"

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data1"

    invoke-virtual {v14}, Lcom/mediatek/apst/util/entity/contacts/Website;->getUrl()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data2"

    invoke-virtual {v14}, Lcom/mediatek/apst/util/entity/contacts/Website;->getType()I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v15, "data3"

    invoke-virtual {v14}, Lcom/mediatek/apst/util/entity/contacts/Website;->getLabel()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_b
    move-object/from16 v0, p0

    instance-of v15, v0, Lcom/mediatek/apst/util/entity/contacts/Note;

    if-eqz v15, :cond_c

    move-object/from16 v8, p0

    check-cast v8, Lcom/mediatek/apst/util/entity/contacts/Note;

    new-instance v13, Landroid/content/ContentValues;

    add-int/lit8 v15, v1, 0x2

    invoke-direct {v13, v15}, Landroid/content/ContentValues;-><init>(I)V

    const-string v15, "mimetype"

    const-string v16, "vnd.android.cursor.item/note"

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data1"

    invoke-virtual {v8}, Lcom/mediatek/apst/util/entity/contacts/Note;->getNote()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_c
    move-object/from16 v0, p0

    instance-of v15, v0, Lcom/mediatek/apst/util/entity/contacts/GroupMembership;

    if-eqz v15, :cond_d

    move-object/from16 v3, p0

    check-cast v3, Lcom/mediatek/apst/util/entity/contacts/GroupMembership;

    new-instance v13, Landroid/content/ContentValues;

    add-int/lit8 v15, v1, 0x2

    invoke-direct {v13, v15}, Landroid/content/ContentValues;-><init>(I)V

    const-string v15, "mimetype"

    const-string v16, "vnd.android.cursor.item/group_membership"

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data1"

    invoke-virtual {v3}, Lcom/mediatek/apst/util/entity/contacts/GroupMembership;->getGroupId()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto/16 :goto_2

    :cond_d
    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object p0, v15, v16

    const/16 v16, 0x1

    invoke-static/range {p1 .. p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v17

    aput-object v17, v15, v16

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Illegal mime type: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v13, 0x0

    goto/16 :goto_0
.end method

.method public static createContentValuesArray(Lcom/mediatek/apst/util/entity/contacts/RawContact;Z)[Landroid/content/ContentValues;
    .locals 21
    .param p0    # Lcom/mediatek/apst/util/entity/contacts/RawContact;
    .param p1    # Z

    if-nez p0, :cond_0

    const/4 v15, 0x0

    :goto_0
    return-object v15

    :cond_0
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    if-eqz p1, :cond_2

    const/4 v2, 0x2

    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/apst/util/entity/contacts/RawContact;->getNames()Ljava/util/List;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/mediatek/apst/util/entity/contacts/StructuredName;

    new-instance v14, Landroid/content/ContentValues;

    add-int/lit8 v18, v2, 0x9

    move/from16 v0, v18

    invoke-direct {v14, v0}, Landroid/content/ContentValues;-><init>(I)V

    if-eqz p1, :cond_1

    const-string v18, "_id"

    invoke-virtual {v7}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->getId()J

    move-result-wide v19

    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_1
    const-string v18, "raw_contact_id"

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->getId()J

    move-result-wide v19

    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v18, "mimetype"

    const-string v19, "vnd.android.cursor.item/name"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "data2"

    invoke-virtual {v7}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->getGivenName()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "data3"

    invoke-virtual {v7}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->getFamilyName()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "data5"

    invoke-virtual {v7}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->getMiddleName()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "data4"

    invoke-virtual {v7}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->getPrefix()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "data6"

    invoke-virtual {v7}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->getSuffix()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "data9"

    invoke-virtual {v7}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->getPhoneticFamilyName()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "data7"

    invoke-virtual {v7}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->getPhoneticGivenName()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "data8"

    invoke-virtual {v7}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->getPhoneticMiddleName()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    :cond_2
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/apst/util/entity/contacts/RawContact;->getPhones()Ljava/util/List;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/mediatek/apst/util/entity/contacts/Phone;

    new-instance v14, Landroid/content/ContentValues;

    add-int/lit8 v18, v2, 0x4

    move/from16 v0, v18

    invoke-direct {v14, v0}, Landroid/content/ContentValues;-><init>(I)V

    if-eqz p1, :cond_4

    const-string v18, "_id"

    invoke-virtual {v11}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->getId()J

    move-result-wide v19

    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_4
    const-string v18, "raw_contact_id"

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->getId()J

    move-result-wide v19

    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v18, "mimetype"

    const-string v19, "vnd.android.cursor.item/phone_v2"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "data1"

    invoke-virtual {v11}, Lcom/mediatek/apst/util/entity/contacts/Phone;->getNumber()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "data2"

    invoke-virtual {v11}, Lcom/mediatek/apst/util/entity/contacts/Phone;->getType()I

    move-result v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v18, "data3"

    invoke-virtual {v11}, Lcom/mediatek/apst/util/entity/contacts/Phone;->getLabel()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "sim_id"

    invoke-virtual {v11}, Lcom/mediatek/apst/util/entity/contacts/Phone;->getBindingSimId()I

    move-result v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/apst/util/entity/contacts/RawContact;->getPhotos()Ljava/util/List;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/mediatek/apst/util/entity/contacts/Photo;

    new-instance v14, Landroid/content/ContentValues;

    add-int/lit8 v18, v2, 0x2

    move/from16 v0, v18

    invoke-direct {v14, v0}, Landroid/content/ContentValues;-><init>(I)V

    if-eqz p1, :cond_6

    const-string v18, "_id"

    invoke-virtual {v12}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->getId()J

    move-result-wide v19

    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_6
    const-string v18, "raw_contact_id"

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->getId()J

    move-result-wide v19

    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v18, "mimetype"

    const-string v19, "vnd.android.cursor.item/photo"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "data15"

    invoke-virtual {v12}, Lcom/mediatek/apst/util/entity/contacts/Photo;->getPhotoBytes()[B

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    move-object/from16 v0, v16

    invoke-interface {v0, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/apst/util/entity/contacts/RawContact;->getIms()Ljava/util/List;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_9

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/mediatek/apst/util/entity/contacts/Im;

    new-instance v14, Landroid/content/ContentValues;

    add-int/lit8 v18, v2, 0x6

    move/from16 v0, v18

    invoke-direct {v14, v0}, Landroid/content/ContentValues;-><init>(I)V

    if-eqz p1, :cond_8

    const-string v18, "_id"

    invoke-virtual {v6}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->getId()J

    move-result-wide v19

    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_8
    const-string v18, "raw_contact_id"

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->getId()J

    move-result-wide v19

    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v18, "mimetype"

    const-string v19, "vnd.android.cursor.item/im"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "data1"

    invoke-virtual {v6}, Lcom/mediatek/apst/util/entity/contacts/Im;->getData()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "data2"

    invoke-virtual {v6}, Lcom/mediatek/apst/util/entity/contacts/Im;->getType()I

    move-result v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v18, "data3"

    invoke-virtual {v6}, Lcom/mediatek/apst/util/entity/contacts/Im;->getLabel()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "data5"

    invoke-virtual {v6}, Lcom/mediatek/apst/util/entity/contacts/Im;->getProtocol()I

    move-result v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v18, "data6"

    invoke-virtual {v6}, Lcom/mediatek/apst/util/entity/contacts/Im;->getCustomProtocol()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_5

    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/apst/util/entity/contacts/RawContact;->getEmails()Ljava/util/List;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_6
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_b

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/apst/util/entity/contacts/Email;

    new-instance v14, Landroid/content/ContentValues;

    add-int/lit8 v18, v2, 0x4

    move/from16 v0, v18

    invoke-direct {v14, v0}, Landroid/content/ContentValues;-><init>(I)V

    if-eqz p1, :cond_a

    const-string v18, "_id"

    invoke-virtual {v3}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->getId()J

    move-result-wide v19

    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_a
    const-string v18, "raw_contact_id"

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->getId()J

    move-result-wide v19

    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v18, "mimetype"

    const-string v19, "vnd.android.cursor.item/email_v2"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "data1"

    invoke-virtual {v3}, Lcom/mediatek/apst/util/entity/contacts/Email;->getData()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "data2"

    invoke-virtual {v3}, Lcom/mediatek/apst/util/entity/contacts/Email;->getType()I

    move-result v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v18, "data3"

    invoke-virtual {v3}, Lcom/mediatek/apst/util/entity/contacts/Email;->getLabel()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    :cond_b
    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/apst/util/entity/contacts/RawContact;->getPostals()Ljava/util/List;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_7
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_d

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;

    new-instance v14, Landroid/content/ContentValues;

    add-int/lit8 v18, v2, 0xb

    move/from16 v0, v18

    invoke-direct {v14, v0}, Landroid/content/ContentValues;-><init>(I)V

    if-eqz p1, :cond_c

    const-string v18, "_id"

    invoke-virtual {v13}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->getId()J

    move-result-wide v19

    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_c
    const-string v18, "raw_contact_id"

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->getId()J

    move-result-wide v19

    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v18, "mimetype"

    const-string v19, "vnd.android.cursor.item/postal-address_v2"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "data1"

    invoke-virtual {v13}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->getFormattedAddress()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "data2"

    invoke-virtual {v13}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->getType()I

    move-result v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v18, "data3"

    invoke-virtual {v13}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->getLabel()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "data4"

    invoke-virtual {v13}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->getStreet()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "data5"

    invoke-virtual {v13}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->getPobox()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "data6"

    invoke-virtual {v13}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->getNeighborhood()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "data7"

    invoke-virtual {v13}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->getCity()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "data8"

    invoke-virtual {v13}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->getRegion()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "data9"

    invoke-virtual {v13}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->getPostcode()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "data10"

    invoke-virtual {v13}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->getCountry()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_7

    :cond_d
    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/apst/util/entity/contacts/RawContact;->getOrganizations()Ljava/util/List;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_8
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_f

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/mediatek/apst/util/entity/contacts/Organization;

    new-instance v14, Landroid/content/ContentValues;

    add-int/lit8 v18, v2, 0xa

    move/from16 v0, v18

    invoke-direct {v14, v0}, Landroid/content/ContentValues;-><init>(I)V

    if-eqz p1, :cond_e

    const-string v18, "_id"

    invoke-virtual {v10}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->getId()J

    move-result-wide v19

    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_e
    const-string v18, "raw_contact_id"

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->getId()J

    move-result-wide v19

    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v18, "mimetype"

    const-string v19, "vnd.android.cursor.item/organization"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "data1"

    invoke-virtual {v10}, Lcom/mediatek/apst/util/entity/contacts/Organization;->getCompany()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "data2"

    invoke-virtual {v10}, Lcom/mediatek/apst/util/entity/contacts/Organization;->getType()I

    move-result v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v18, "data3"

    invoke-virtual {v10}, Lcom/mediatek/apst/util/entity/contacts/Organization;->getLabel()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "data4"

    invoke-virtual {v10}, Lcom/mediatek/apst/util/entity/contacts/Organization;->getTitle()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "data5"

    invoke-virtual {v10}, Lcom/mediatek/apst/util/entity/contacts/Organization;->getTitle()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "data6"

    invoke-virtual {v10}, Lcom/mediatek/apst/util/entity/contacts/Organization;->getJobDescription()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "data7"

    invoke-virtual {v10}, Lcom/mediatek/apst/util/entity/contacts/Organization;->getSymbol()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "data8"

    invoke-virtual {v10}, Lcom/mediatek/apst/util/entity/contacts/Organization;->getPhoneticName()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "data9"

    invoke-virtual {v10}, Lcom/mediatek/apst/util/entity/contacts/Organization;->getOfficeLocation()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_8

    :cond_f
    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/apst/util/entity/contacts/RawContact;->getNicknames()Ljava/util/List;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_9
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_11

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/mediatek/apst/util/entity/contacts/Nickname;

    new-instance v14, Landroid/content/ContentValues;

    add-int/lit8 v18, v2, 0x4

    move/from16 v0, v18

    invoke-direct {v14, v0}, Landroid/content/ContentValues;-><init>(I)V

    if-eqz p1, :cond_10

    const-string v18, "_id"

    invoke-virtual {v8}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->getId()J

    move-result-wide v19

    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_10
    const-string v18, "raw_contact_id"

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->getId()J

    move-result-wide v19

    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v18, "mimetype"

    const-string v19, "vnd.android.cursor.item/nickname"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "data1"

    invoke-virtual {v8}, Lcom/mediatek/apst/util/entity/contacts/Nickname;->getName()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "data2"

    invoke-virtual {v8}, Lcom/mediatek/apst/util/entity/contacts/Nickname;->getType()I

    move-result v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v18, "data3"

    invoke-virtual {v8}, Lcom/mediatek/apst/util/entity/contacts/Nickname;->getLabel()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_9

    :cond_11
    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/apst/util/entity/contacts/RawContact;->getWebsites()Ljava/util/List;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_a
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_13

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/mediatek/apst/util/entity/contacts/Website;

    new-instance v14, Landroid/content/ContentValues;

    add-int/lit8 v18, v2, 0x4

    move/from16 v0, v18

    invoke-direct {v14, v0}, Landroid/content/ContentValues;-><init>(I)V

    if-eqz p1, :cond_12

    const-string v18, "_id"

    invoke-virtual/range {v17 .. v17}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->getId()J

    move-result-wide v19

    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_12
    const-string v18, "raw_contact_id"

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->getId()J

    move-result-wide v19

    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v18, "mimetype"

    const-string v19, "vnd.android.cursor.item/website"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "data1"

    invoke-virtual/range {v17 .. v17}, Lcom/mediatek/apst/util/entity/contacts/Website;->getUrl()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "data2"

    invoke-virtual/range {v17 .. v17}, Lcom/mediatek/apst/util/entity/contacts/Website;->getType()I

    move-result v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v18, "data3"

    invoke-virtual/range {v17 .. v17}, Lcom/mediatek/apst/util/entity/contacts/Website;->getLabel()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_a

    :cond_13
    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/apst/util/entity/contacts/RawContact;->getNotes()Ljava/util/List;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_b
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_15

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/mediatek/apst/util/entity/contacts/Note;

    new-instance v14, Landroid/content/ContentValues;

    add-int/lit8 v18, v2, 0x2

    move/from16 v0, v18

    invoke-direct {v14, v0}, Landroid/content/ContentValues;-><init>(I)V

    if-eqz p1, :cond_14

    const-string v18, "_id"

    invoke-virtual {v9}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->getId()J

    move-result-wide v19

    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_14
    const-string v18, "raw_contact_id"

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->getId()J

    move-result-wide v19

    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v18, "mimetype"

    const-string v19, "vnd.android.cursor.item/note"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "data1"

    invoke-virtual {v9}, Lcom/mediatek/apst/util/entity/contacts/Note;->getNote()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_b

    :cond_15
    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->getGroupMemberships()Ljava/util/List;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_c
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_17

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/apst/util/entity/contacts/GroupMembership;

    new-instance v14, Landroid/content/ContentValues;

    add-int/lit8 v18, v2, 0x2

    move/from16 v0, v18

    invoke-direct {v14, v0}, Landroid/content/ContentValues;-><init>(I)V

    if-eqz p1, :cond_16

    const-string v18, "_id"

    invoke-virtual {v4}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->getId()J

    move-result-wide v19

    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_16
    const-string v18, "raw_contact_id"

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->getId()J

    move-result-wide v19

    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v18, "mimetype"

    const-string v19, "vnd.android.cursor.item/group_membership"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "data1"

    invoke-virtual {v4}, Lcom/mediatek/apst/util/entity/contacts/GroupMembership;->getGroupId()J

    move-result-wide v19

    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_c

    :cond_17
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v18

    move/from16 v0, v18

    new-array v15, v0, [Landroid/content/ContentValues;

    move-object/from16 v0, v16

    invoke-interface {v0, v15}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    goto/16 :goto_0
.end method

.method public static createMeasuredContentValues(Lcom/mediatek/apst/util/entity/contacts/ContactData;Z)Lcom/mediatek/android/content/MeasuredContentValues;
    .locals 18
    .param p0    # Lcom/mediatek/apst/util/entity/contacts/ContactData;
    .param p1    # Z

    if-nez p0, :cond_0

    const/4 v13, 0x0

    :goto_0
    return-object v13

    :cond_0
    if-eqz p1, :cond_2

    const/4 v1, 0x2

    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/apst/util/entity/contacts/ContactData;->getMimeType()I

    move-result v5

    move-object/from16 v0, p0

    instance-of v15, v0, Lcom/mediatek/apst/util/entity/contacts/StructuredName;

    if-eqz v15, :cond_3

    move-object/from16 v6, p0

    check-cast v6, Lcom/mediatek/apst/util/entity/contacts/StructuredName;

    new-instance v13, Lcom/mediatek/android/content/MeasuredContentValues;

    add-int/lit8 v15, v1, 0x9

    invoke-direct {v13, v15}, Lcom/mediatek/android/content/MeasuredContentValues;-><init>(I)V

    const-string v15, "mimetype"

    const-string v16, "vnd.android.cursor.item/name"

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data2"

    invoke-virtual {v6}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->getGivenName()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data3"

    invoke-virtual {v6}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->getFamilyName()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data5"

    invoke-virtual {v6}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->getMiddleName()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data4"

    invoke-virtual {v6}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->getPrefix()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data6"

    invoke-virtual {v6}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->getSuffix()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data9"

    invoke-virtual {v6}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->getPhoneticFamilyName()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data7"

    invoke-virtual {v6}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->getPhoneticGivenName()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data8"

    invoke-virtual {v6}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->getPhoneticMiddleName()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    if-eqz p1, :cond_1

    const-string v15, "_id"

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->getId()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_1
    const-string v15, "raw_contact_id"

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/apst/util/entity/contacts/ContactData;->getRawContactId()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto/16 :goto_0

    :cond_2
    const/4 v1, 0x1

    goto/16 :goto_1

    :cond_3
    move-object/from16 v0, p0

    instance-of v15, v0, Lcom/mediatek/apst/util/entity/contacts/Phone;

    if-eqz v15, :cond_4

    move-object/from16 v10, p0

    check-cast v10, Lcom/mediatek/apst/util/entity/contacts/Phone;

    new-instance v13, Lcom/mediatek/android/content/MeasuredContentValues;

    add-int/lit8 v15, v1, 0x4

    invoke-direct {v13, v15}, Lcom/mediatek/android/content/MeasuredContentValues;-><init>(I)V

    const-string v15, "mimetype"

    const-string v16, "vnd.android.cursor.item/phone_v2"

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data1"

    invoke-virtual {v10}, Lcom/mediatek/apst/util/entity/contacts/Phone;->getNumber()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data2"

    invoke-virtual {v10}, Lcom/mediatek/apst/util/entity/contacts/Phone;->getType()I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v15, "data3"

    invoke-virtual {v10}, Lcom/mediatek/apst/util/entity/contacts/Phone;->getLabel()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "sim_id"

    invoke-virtual {v10}, Lcom/mediatek/apst/util/entity/contacts/Phone;->getBindingSimId()I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_2

    :cond_4
    move-object/from16 v0, p0

    instance-of v15, v0, Lcom/mediatek/apst/util/entity/contacts/Photo;

    if-eqz v15, :cond_5

    move-object/from16 v11, p0

    check-cast v11, Lcom/mediatek/apst/util/entity/contacts/Photo;

    new-instance v13, Lcom/mediatek/android/content/MeasuredContentValues;

    add-int/lit8 v15, v1, 0x2

    invoke-direct {v13, v15}, Lcom/mediatek/android/content/MeasuredContentValues;-><init>(I)V

    const-string v15, "mimetype"

    const-string v16, "vnd.android.cursor.item/photo"

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data15"

    invoke-virtual {v11}, Lcom/mediatek/apst/util/entity/contacts/Photo;->getPhotoBytes()[B

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;[B)V

    goto/16 :goto_2

    :cond_5
    move-object/from16 v0, p0

    instance-of v15, v0, Lcom/mediatek/apst/util/entity/contacts/Im;

    if-eqz v15, :cond_6

    move-object/from16 v4, p0

    check-cast v4, Lcom/mediatek/apst/util/entity/contacts/Im;

    new-instance v13, Lcom/mediatek/android/content/MeasuredContentValues;

    add-int/lit8 v15, v1, 0x6

    invoke-direct {v13, v15}, Lcom/mediatek/android/content/MeasuredContentValues;-><init>(I)V

    const-string v15, "mimetype"

    const-string v16, "vnd.android.cursor.item/im"

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data1"

    invoke-virtual {v4}, Lcom/mediatek/apst/util/entity/contacts/Im;->getData()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data2"

    invoke-virtual {v4}, Lcom/mediatek/apst/util/entity/contacts/Im;->getType()I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v15, "data3"

    invoke-virtual {v4}, Lcom/mediatek/apst/util/entity/contacts/Im;->getLabel()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data5"

    invoke-virtual {v4}, Lcom/mediatek/apst/util/entity/contacts/Im;->getProtocol()I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v15, "data6"

    invoke-virtual {v4}, Lcom/mediatek/apst/util/entity/contacts/Im;->getCustomProtocol()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_6
    move-object/from16 v0, p0

    instance-of v15, v0, Lcom/mediatek/apst/util/entity/contacts/Email;

    if-eqz v15, :cond_7

    move-object/from16 v2, p0

    check-cast v2, Lcom/mediatek/apst/util/entity/contacts/Email;

    new-instance v13, Lcom/mediatek/android/content/MeasuredContentValues;

    add-int/lit8 v15, v1, 0x4

    invoke-direct {v13, v15}, Lcom/mediatek/android/content/MeasuredContentValues;-><init>(I)V

    const-string v15, "mimetype"

    const-string v16, "vnd.android.cursor.item/email_v2"

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data1"

    invoke-virtual {v2}, Lcom/mediatek/apst/util/entity/contacts/Email;->getData()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data2"

    invoke-virtual {v2}, Lcom/mediatek/apst/util/entity/contacts/Email;->getType()I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v15, "data3"

    invoke-virtual {v2}, Lcom/mediatek/apst/util/entity/contacts/Email;->getLabel()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_7
    move-object/from16 v0, p0

    instance-of v15, v0, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;

    if-eqz v15, :cond_8

    move-object/from16 v12, p0

    check-cast v12, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;

    new-instance v13, Lcom/mediatek/android/content/MeasuredContentValues;

    add-int/lit8 v15, v1, 0xb

    invoke-direct {v13, v15}, Lcom/mediatek/android/content/MeasuredContentValues;-><init>(I)V

    const-string v15, "mimetype"

    const-string v16, "vnd.android.cursor.item/postal-address_v2"

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data1"

    invoke-virtual {v12}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->getFormattedAddress()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data2"

    invoke-virtual {v12}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->getType()I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v15, "data3"

    invoke-virtual {v12}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->getLabel()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data4"

    invoke-virtual {v12}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->getStreet()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data5"

    invoke-virtual {v12}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->getPobox()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data6"

    invoke-virtual {v12}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->getNeighborhood()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data7"

    invoke-virtual {v12}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->getCity()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data8"

    invoke-virtual {v12}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->getRegion()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data9"

    invoke-virtual {v12}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->getPostcode()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data10"

    invoke-virtual {v12}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->getCountry()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_8
    move-object/from16 v0, p0

    instance-of v15, v0, Lcom/mediatek/apst/util/entity/contacts/Organization;

    if-eqz v15, :cond_9

    move-object/from16 v9, p0

    check-cast v9, Lcom/mediatek/apst/util/entity/contacts/Organization;

    new-instance v13, Lcom/mediatek/android/content/MeasuredContentValues;

    add-int/lit8 v15, v1, 0xa

    invoke-direct {v13, v15}, Lcom/mediatek/android/content/MeasuredContentValues;-><init>(I)V

    const-string v15, "mimetype"

    const-string v16, "vnd.android.cursor.item/organization"

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data1"

    invoke-virtual {v9}, Lcom/mediatek/apst/util/entity/contacts/Organization;->getCompany()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data2"

    invoke-virtual {v9}, Lcom/mediatek/apst/util/entity/contacts/Organization;->getType()I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v15, "data3"

    invoke-virtual {v9}, Lcom/mediatek/apst/util/entity/contacts/Organization;->getLabel()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data4"

    invoke-virtual {v9}, Lcom/mediatek/apst/util/entity/contacts/Organization;->getTitle()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data5"

    invoke-virtual {v9}, Lcom/mediatek/apst/util/entity/contacts/Organization;->getTitle()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data6"

    invoke-virtual {v9}, Lcom/mediatek/apst/util/entity/contacts/Organization;->getJobDescription()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data7"

    invoke-virtual {v9}, Lcom/mediatek/apst/util/entity/contacts/Organization;->getSymbol()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data8"

    invoke-virtual {v9}, Lcom/mediatek/apst/util/entity/contacts/Organization;->getPhoneticName()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data9"

    invoke-virtual {v9}, Lcom/mediatek/apst/util/entity/contacts/Organization;->getOfficeLocation()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_9
    move-object/from16 v0, p0

    instance-of v15, v0, Lcom/mediatek/apst/util/entity/contacts/Nickname;

    if-eqz v15, :cond_a

    move-object/from16 v7, p0

    check-cast v7, Lcom/mediatek/apst/util/entity/contacts/Nickname;

    new-instance v13, Lcom/mediatek/android/content/MeasuredContentValues;

    add-int/lit8 v15, v1, 0x4

    invoke-direct {v13, v15}, Lcom/mediatek/android/content/MeasuredContentValues;-><init>(I)V

    const-string v15, "mimetype"

    const-string v16, "vnd.android.cursor.item/nickname"

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data1"

    invoke-virtual {v7}, Lcom/mediatek/apst/util/entity/contacts/Nickname;->getName()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data2"

    invoke-virtual {v7}, Lcom/mediatek/apst/util/entity/contacts/Nickname;->getType()I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v15, "data3"

    invoke-virtual {v7}, Lcom/mediatek/apst/util/entity/contacts/Nickname;->getLabel()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_a
    move-object/from16 v0, p0

    instance-of v15, v0, Lcom/mediatek/apst/util/entity/contacts/Website;

    if-eqz v15, :cond_b

    move-object/from16 v14, p0

    check-cast v14, Lcom/mediatek/apst/util/entity/contacts/Website;

    new-instance v13, Lcom/mediatek/android/content/MeasuredContentValues;

    add-int/lit8 v15, v1, 0x4

    invoke-direct {v13, v15}, Lcom/mediatek/android/content/MeasuredContentValues;-><init>(I)V

    const-string v15, "mimetype"

    const-string v16, "vnd.android.cursor.item/website"

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data1"

    invoke-virtual {v14}, Lcom/mediatek/apst/util/entity/contacts/Website;->getUrl()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data2"

    invoke-virtual {v14}, Lcom/mediatek/apst/util/entity/contacts/Website;->getType()I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v15, "data3"

    invoke-virtual {v14}, Lcom/mediatek/apst/util/entity/contacts/Website;->getLabel()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_b
    move-object/from16 v0, p0

    instance-of v15, v0, Lcom/mediatek/apst/util/entity/contacts/Note;

    if-eqz v15, :cond_c

    move-object/from16 v8, p0

    check-cast v8, Lcom/mediatek/apst/util/entity/contacts/Note;

    new-instance v13, Lcom/mediatek/android/content/MeasuredContentValues;

    add-int/lit8 v15, v1, 0x2

    invoke-direct {v13, v15}, Lcom/mediatek/android/content/MeasuredContentValues;-><init>(I)V

    const-string v15, "mimetype"

    const-string v16, "vnd.android.cursor.item/note"

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data1"

    invoke-virtual {v8}, Lcom/mediatek/apst/util/entity/contacts/Note;->getNote()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_c
    move-object/from16 v0, p0

    instance-of v15, v0, Lcom/mediatek/apst/util/entity/contacts/GroupMembership;

    if-eqz v15, :cond_d

    move-object/from16 v3, p0

    check-cast v3, Lcom/mediatek/apst/util/entity/contacts/GroupMembership;

    new-instance v13, Lcom/mediatek/android/content/MeasuredContentValues;

    add-int/lit8 v15, v1, 0x2

    invoke-direct {v13, v15}, Lcom/mediatek/android/content/MeasuredContentValues;-><init>(I)V

    const-string v15, "mimetype"

    const-string v16, "vnd.android.cursor.item/group_membership"

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "data1"

    invoke-virtual {v3}, Lcom/mediatek/apst/util/entity/contacts/GroupMembership;->getGroupId()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto/16 :goto_2

    :cond_d
    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object p0, v15, v16

    const/16 v16, 0x1

    invoke-static/range {p1 .. p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v17

    aput-object v17, v15, v16

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Illegal mime type: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v13, 0x0

    goto/16 :goto_0
.end method

.method public static cursorToContactData(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/ContactData;
    .locals 13
    .param p0    # Landroid/database/Cursor;

    if-eqz p0, :cond_0

    invoke-interface {p0}, Landroid/database/Cursor;->getPosition()I

    move-result v10

    const/4 v11, -0x1

    if-eq v10, v11, :cond_0

    invoke-interface {p0}, Landroid/database/Cursor;->getPosition()I

    move-result v10

    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v11

    if-ne v10, v11, :cond_1

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    const-wide/16 v3, -0x1

    const-wide/16 v6, -0x1

    const/4 v5, 0x0

    const/4 v9, 0x0

    const/4 v8, 0x0

    :try_start_0
    const-string v10, "_id"

    invoke-interface {p0, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    const/4 v10, -0x1

    if-eq v10, v0, :cond_2

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    :cond_2
    const-string v10, "raw_contact_id"

    invoke-interface {p0, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    const/4 v10, -0x1

    if-eq v10, v0, :cond_3

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    :cond_3
    const-string v10, "is_primary"

    invoke-interface {p0, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    const/4 v10, -0x1

    if-eq v10, v0, :cond_4

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_7

    const/4 v5, 0x1

    :cond_4
    :goto_1
    const-string v10, "is_super_primary"

    invoke-interface {p0, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    const/4 v10, -0x1

    if-eq v10, v0, :cond_5

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_8

    const/4 v9, 0x1

    :cond_5
    :goto_2
    const-string v10, "mimetype"

    invoke-interface {p0, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    const/4 v10, -0x1

    if-eq v10, v0, :cond_6

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    :cond_6
    if-nez v8, :cond_9

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object p0, v10, v11

    const-string v11, "mimeType is absent in cursor."

    invoke-static {v10, v11}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    goto :goto_0

    :cond_7
    const/4 v5, 0x0

    goto :goto_1

    :cond_8
    const/4 v9, 0x0

    goto :goto_2

    :cond_9
    const/16 v10, 0x9

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a

    invoke-static {p0}, Lcom/mediatek/apst/target/data/provider/contacts/ContactDataContent;->cursorToStructuredName(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/StructuredName;

    move-result-object v1

    :goto_3
    invoke-virtual {v1, v3, v4}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->setId(J)V

    invoke-virtual {v1, v6, v7}, Lcom/mediatek/apst/util/entity/contacts/ContactData;->setRawContactId(J)V

    invoke-virtual {v1, v5}, Lcom/mediatek/apst/util/entity/contacts/ContactData;->setPrimary(Z)V

    invoke-virtual {v1, v9}, Lcom/mediatek/apst/util/entity/contacts/ContactData;->setSuperPrimary(Z)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v2

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object p0, v10, v11

    const/4 v11, 0x0

    invoke-static {v10, v11, v2}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_a
    const/4 v10, 0x7

    :try_start_1
    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_b

    invoke-static {p0}, Lcom/mediatek/apst/target/data/provider/contacts/ContactDataContent;->cursorToPhone(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/Phone;

    move-result-object v1

    goto :goto_3

    :cond_b
    const/16 v10, 0xa

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_c

    invoke-static {p0}, Lcom/mediatek/apst/target/data/provider/contacts/ContactDataContent;->cursorToPhoto(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/Photo;

    move-result-object v1

    goto :goto_3

    :cond_c
    const/4 v10, 0x2

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_d

    invoke-static {p0}, Lcom/mediatek/apst/target/data/provider/contacts/ContactDataContent;->cursorToIm(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/Im;

    move-result-object v1

    goto :goto_3

    :cond_d
    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_e

    invoke-static {p0}, Lcom/mediatek/apst/target/data/provider/contacts/ContactDataContent;->cursorToEmail(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/Email;

    move-result-object v1

    goto :goto_3

    :cond_e
    const/4 v10, 0x3

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_f

    invoke-static {p0}, Lcom/mediatek/apst/target/data/provider/contacts/ContactDataContent;->cursorToStructuredPostal(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;

    move-result-object v1

    goto :goto_3

    :cond_f
    const/16 v10, 0x8

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_10

    invoke-static {p0}, Lcom/mediatek/apst/target/data/provider/contacts/ContactDataContent;->cursorToOrganization(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/Organization;

    move-result-object v1

    goto :goto_3

    :cond_10
    const/4 v10, 0x4

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_11

    invoke-static {p0}, Lcom/mediatek/apst/target/data/provider/contacts/ContactDataContent;->cursorToNickname(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/Nickname;

    move-result-object v1

    goto/16 :goto_3

    :cond_11
    const/4 v10, 0x5

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_12

    invoke-static {p0}, Lcom/mediatek/apst/target/data/provider/contacts/ContactDataContent;->cursorToWebsite(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/Website;

    move-result-object v1

    goto/16 :goto_3

    :cond_12
    const/4 v10, 0x6

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_13

    invoke-static {p0}, Lcom/mediatek/apst/target/data/provider/contacts/ContactDataContent;->cursorToNote(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/Note;

    move-result-object v1

    goto/16 :goto_3

    :cond_13
    const/16 v10, 0xb

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_14

    invoke-static {p0}, Lcom/mediatek/apst/target/data/provider/contacts/ContactDataContent;->cursorToGroupMembership(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/GroupMembership;

    move-result-object v1

    goto/16 :goto_3

    :cond_14
    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object p0, v10, v11

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Ignored unknown mimeType: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method private static cursorToEmail(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/Email;
    .locals 6
    .param p0    # Landroid/database/Cursor;

    const/4 v3, 0x0

    const/4 v5, -0x1

    new-instance v1, Lcom/mediatek/apst/util/entity/contacts/Email;

    invoke-direct {v1}, Lcom/mediatek/apst/util/entity/contacts/Email;-><init>()V

    :try_start_0
    const-string v4, "data1"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Email;->setData(Ljava/lang/String;)V

    :cond_0
    const-string v4, "data2"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_1

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Email;->setType(I)V

    :cond_1
    const-string v4, "data3"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_2

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Email;->setLabel(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_0
    return-object v1

    :catch_0
    move-exception v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-static {v4, v3, v2}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v3

    goto :goto_0
.end method

.method private static cursorToEmailRaw(Landroid/database/Cursor;Ljava/nio/ByteBuffer;)I
    .locals 7
    .param p0    # Landroid/database/Cursor;
    .param p1    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v2, 0x1

    const/4 v5, -0x1

    :try_start_0
    const-string v4, "data1"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_0
    const-string v4, "data2"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_1

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    :goto_1
    const-string v4, "data3"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_2

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_2
    return v2

    :cond_0
    const/4 v4, 0x0

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p0, v4, v3

    aput-object p1, v4, v2

    invoke-static {v4, v6, v1}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v2, v3

    goto :goto_2

    :cond_1
    const/4 v4, 0x1

    :try_start_1
    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method private static cursorToGroupMembership(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/GroupMembership;
    .locals 6
    .param p0    # Landroid/database/Cursor;

    const/4 v3, 0x0

    new-instance v1, Lcom/mediatek/apst/util/entity/contacts/GroupMembership;

    invoke-direct {v1}, Lcom/mediatek/apst/util/entity/contacts/GroupMembership;-><init>()V

    :try_start_0
    const-string v4, "data1"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    const/4 v4, -0x1

    if-eq v4, v0, :cond_0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lcom/mediatek/apst/util/entity/contacts/GroupMembership;->setGroupId(J)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-object v1

    :catch_0
    move-exception v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-static {v4, v3, v2}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v3

    goto :goto_0
.end method

.method private static cursorToGroupMembershipRaw(Landroid/database/Cursor;Ljava/nio/ByteBuffer;)I
    .locals 6
    .param p0    # Landroid/database/Cursor;
    .param p1    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    const/4 v2, 0x1

    const/4 v3, 0x0

    :try_start_0
    const-string v4, "data1"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    const/4 v4, -0x1

    if-eq v4, v0, :cond_0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {p1, v4, v5}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    :goto_0
    return v2

    :cond_0
    const-wide/16 v4, -0x1

    invoke-virtual {p1, v4, v5}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p0, v4, v3

    aput-object p1, v4, v2

    const/4 v2, 0x0

    invoke-static {v4, v2, v1}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v2, v3

    goto :goto_0
.end method

.method private static cursorToIm(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/Im;
    .locals 6
    .param p0    # Landroid/database/Cursor;

    const/4 v3, 0x0

    const/4 v5, -0x1

    new-instance v1, Lcom/mediatek/apst/util/entity/contacts/Im;

    invoke-direct {v1}, Lcom/mediatek/apst/util/entity/contacts/Im;-><init>()V

    :try_start_0
    const-string v4, "data1"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Im;->setData(Ljava/lang/String;)V

    :cond_0
    const-string v4, "data2"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_1

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Im;->setType(I)V

    :cond_1
    const-string v4, "data3"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_2

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Im;->setLabel(Ljava/lang/String;)V

    :cond_2
    const-string v4, "data5"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_3

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Im;->setProtocol(I)V

    :cond_3
    const-string v4, "data6"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_4

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Im;->setCustomProtocol(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_4
    :goto_0
    return-object v1

    :catch_0
    move-exception v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-static {v4, v3, v2}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v3

    goto :goto_0
.end method

.method private static cursorToImRaw(Landroid/database/Cursor;Ljava/nio/ByteBuffer;)I
    .locals 7
    .param p0    # Landroid/database/Cursor;
    .param p1    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v5, -0x1

    :try_start_0
    const-string v4, "data1"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_0
    const-string v4, "data2"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_1

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    :goto_1
    const-string v4, "data3"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_2

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_2
    const-string v4, "data5"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_3

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    :goto_3
    const-string v4, "data6"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_4

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_4
    return v2

    :cond_0
    const/4 v4, 0x0

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p0, v4, v3

    aput-object p1, v4, v2

    invoke-static {v4, v6, v1}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v2, v3

    goto :goto_4

    :cond_1
    const/4 v4, 0x1

    :try_start_1
    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto :goto_3

    :cond_4
    const/4 v4, 0x0

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4
.end method

.method private static cursorToNickname(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/Nickname;
    .locals 6
    .param p0    # Landroid/database/Cursor;

    const/4 v3, 0x0

    const/4 v5, -0x1

    new-instance v1, Lcom/mediatek/apst/util/entity/contacts/Nickname;

    invoke-direct {v1}, Lcom/mediatek/apst/util/entity/contacts/Nickname;-><init>()V

    :try_start_0
    const-string v4, "data1"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Nickname;->setName(Ljava/lang/String;)V

    :cond_0
    const-string v4, "data2"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_1

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Nickname;->setType(I)V

    :cond_1
    const-string v4, "data3"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_2

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Nickname;->setLabel(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_0
    return-object v1

    :catch_0
    move-exception v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-static {v4, v3, v2}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v3

    goto :goto_0
.end method

.method private static cursorToNicknameRaw(Landroid/database/Cursor;Ljava/nio/ByteBuffer;)I
    .locals 7
    .param p0    # Landroid/database/Cursor;
    .param p1    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v2, 0x1

    const/4 v5, -0x1

    :try_start_0
    const-string v4, "data1"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_0
    const-string v4, "data2"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_1

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    :goto_1
    const-string v4, "data3"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_2

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_2
    return v2

    :cond_0
    const/4 v4, 0x0

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p0, v4, v3

    aput-object p1, v4, v2

    invoke-static {v4, v6, v1}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v2, v3

    goto :goto_2

    :cond_1
    const/4 v4, 0x1

    :try_start_1
    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method private static cursorToNote(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/Note;
    .locals 6
    .param p0    # Landroid/database/Cursor;

    const/4 v3, 0x0

    new-instance v1, Lcom/mediatek/apst/util/entity/contacts/Note;

    invoke-direct {v1}, Lcom/mediatek/apst/util/entity/contacts/Note;-><init>()V

    :try_start_0
    const-string v4, "data1"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    const/4 v4, -0x1

    if-eq v4, v0, :cond_0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Note;->setNote(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-object v1

    :catch_0
    move-exception v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-static {v4, v3, v2}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v3

    goto :goto_0
.end method

.method private static cursorToNoteRaw(Landroid/database/Cursor;Ljava/nio/ByteBuffer;)I
    .locals 6
    .param p0    # Landroid/database/Cursor;
    .param p1    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    const/4 v5, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    :try_start_0
    const-string v4, "data1"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    const/4 v4, -0x1

    if-eq v4, v0, :cond_0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_0
    return v2

    :cond_0
    const/4 v4, 0x0

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p0, v4, v3

    aput-object p1, v4, v2

    invoke-static {v4, v5, v1}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v2, v3

    goto :goto_0
.end method

.method private static cursorToOrganization(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/Organization;
    .locals 6
    .param p0    # Landroid/database/Cursor;

    const/4 v3, 0x0

    const/4 v5, -0x1

    new-instance v1, Lcom/mediatek/apst/util/entity/contacts/Organization;

    invoke-direct {v1}, Lcom/mediatek/apst/util/entity/contacts/Organization;-><init>()V

    :try_start_0
    const-string v4, "data1"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Organization;->setCompany(Ljava/lang/String;)V

    :cond_0
    const-string v4, "data2"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_1

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Organization;->setType(I)V

    :cond_1
    const-string v4, "data3"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_2

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Organization;->setLabel(Ljava/lang/String;)V

    :cond_2
    const-string v4, "data4"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_3

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Organization;->setTitle(Ljava/lang/String;)V

    :cond_3
    const-string v4, "data5"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_4

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Organization;->setDepartment(Ljava/lang/String;)V

    :cond_4
    const-string v4, "data6"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_5

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Organization;->setJobDescription(Ljava/lang/String;)V

    :cond_5
    const-string v4, "data7"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_6

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Organization;->setSymbol(Ljava/lang/String;)V

    :cond_6
    const-string v4, "data8"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_7

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Organization;->setPhoneticName(Ljava/lang/String;)V

    :cond_7
    const-string v4, "data9"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_8

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Organization;->setOfficeLocation(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_8
    :goto_0
    return-object v1

    :catch_0
    move-exception v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-static {v4, v3, v2}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v3

    goto :goto_0
.end method

.method private static cursorToOrganizationRaw(Landroid/database/Cursor;Ljava/nio/ByteBuffer;)I
    .locals 7
    .param p0    # Landroid/database/Cursor;
    .param p1    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v6, 0x0

    const/4 v5, -0x1

    :try_start_0
    const-string v4, "data1"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_0
    const-string v4, "data2"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_1

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    :goto_1
    const-string v4, "data3"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_2

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_2
    const-string v4, "data4"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_3

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_3
    const-string v4, "data5"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_4

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_4
    const-string v4, "data6"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_5

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_5
    const-string v4, "data7"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_6

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_6
    const-string v4, "data8"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_7

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_7
    const-string v4, "data9"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_8

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_8
    return v2

    :cond_0
    const/4 v4, 0x0

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p0, v4, v3

    aput-object p1, v4, v2

    invoke-static {v4, v6, v1}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v2, v3

    goto :goto_8

    :cond_1
    const/4 v4, 0x1

    :try_start_1
    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    const/4 v4, 0x0

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    goto :goto_3

    :cond_4
    const/4 v4, 0x0

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    goto :goto_4

    :cond_5
    const/4 v4, 0x0

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    goto :goto_5

    :cond_6
    const/4 v4, 0x0

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    goto :goto_6

    :cond_7
    const/4 v4, 0x0

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    goto :goto_7

    :cond_8
    const/4 v4, 0x0

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_8
.end method

.method private static cursorToPhone(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/Phone;
    .locals 6
    .param p0    # Landroid/database/Cursor;

    const/4 v3, 0x0

    const/4 v5, -0x1

    new-instance v1, Lcom/mediatek/apst/util/entity/contacts/Phone;

    invoke-direct {v1}, Lcom/mediatek/apst/util/entity/contacts/Phone;-><init>()V

    :try_start_0
    const-string v4, "data1"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Phone;->setNumber(Ljava/lang/String;)V

    :cond_0
    const-string v4, "data2"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_1

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Phone;->setType(I)V

    :cond_1
    const-string v4, "data3"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_2

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Phone;->setLabel(Ljava/lang/String;)V

    :cond_2
    const-string v4, "sim_id"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_3

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Phone;->setBindingSimId(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_0
    return-object v1

    :catch_0
    move-exception v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-static {v4, v3, v2}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v3

    goto :goto_0
.end method

.method private static cursorToPhoneRaw(Landroid/database/Cursor;Ljava/nio/ByteBuffer;)I
    .locals 7
    .param p0    # Landroid/database/Cursor;
    .param p1    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v2, 0x1

    const/4 v5, -0x1

    :try_start_0
    const-string v4, "data1"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_0
    const-string v4, "data2"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_1

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    :goto_1
    const-string v4, "data3"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_2

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_2
    const-string v4, "sim_id"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_3

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    :goto_3
    return v2

    :cond_0
    const/4 v4, 0x0

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p0, v4, v3

    aput-object p1, v4, v2

    invoke-static {v4, v6, v1}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v2, v3

    goto :goto_3

    :cond_1
    const/4 v4, 0x1

    :try_start_1
    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    const/4 v4, -0x1

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3
.end method

.method private static cursorToPhoto(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/Photo;
    .locals 6
    .param p0    # Landroid/database/Cursor;

    const/4 v3, 0x0

    new-instance v1, Lcom/mediatek/apst/util/entity/contacts/Photo;

    invoke-direct {v1}, Lcom/mediatek/apst/util/entity/contacts/Photo;-><init>()V

    :try_start_0
    const-string v4, "data15"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    const/4 v4, -0x1

    if-eq v4, v0, :cond_0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Photo;->setPhotoBytes([B)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-object v1

    :catch_0
    move-exception v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-static {v4, v3, v2}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v3

    goto :goto_0
.end method

.method private static cursorToPhotoRaw(Landroid/database/Cursor;Ljava/nio/ByteBuffer;)I
    .locals 6
    .param p0    # Landroid/database/Cursor;
    .param p1    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    const/4 v5, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    :try_start_0
    const-string v4, "data15"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    const/4 v4, -0x1

    if-eq v4, v0, :cond_0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v4

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putBytes(Ljava/nio/ByteBuffer;[B)V

    :goto_0
    return v2

    :cond_0
    const/4 v4, 0x0

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putBytes(Ljava/nio/ByteBuffer;[B)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p0, v4, v3

    aput-object p1, v4, v2

    invoke-static {v4, v5, v1}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v2, v3

    goto :goto_0
.end method

.method public static cursorToRaw(Landroid/database/Cursor;Ljava/nio/ByteBuffer;)I
    .locals 10
    .param p0    # Landroid/database/Cursor;
    .param p1    # Ljava/nio/ByteBuffer;

    const/4 v7, -0x1

    const/4 v9, 0x1

    const/4 v8, 0x2

    const/4 v4, 0x0

    if-nez p0, :cond_1

    new-array v5, v8, [Ljava/lang/Object;

    aput-object p0, v5, v4

    aput-object p1, v5, v9

    const-string v6, "Cursor is null."

    invoke-static {v5, v6}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    move v2, v4

    :cond_0
    :goto_0
    return v2

    :cond_1
    invoke-interface {p0}, Landroid/database/Cursor;->getPosition()I

    move-result v5

    if-eq v5, v7, :cond_2

    invoke-interface {p0}, Landroid/database/Cursor;->getPosition()I

    move-result v5

    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v6

    if-ne v5, v6, :cond_3

    :cond_2
    new-array v5, v8, [Ljava/lang/Object;

    aput-object p0, v5, v4

    aput-object p1, v5, v9

    const-string v6, "Cursor has moved to the end."

    invoke-static {v5, v6}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    move v2, v4

    goto :goto_0

    :cond_3
    if-nez p1, :cond_4

    new-array v5, v8, [Ljava/lang/Object;

    aput-object p0, v5, v4

    aput-object p1, v5, v9

    const-string v6, "Buffer is null."

    invoke-static {v5, v6}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    move v2, v4

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Ljava/nio/Buffer;->mark()Ljava/nio/Buffer;

    invoke-virtual {p1}, Ljava/nio/Buffer;->position()I

    const/4 v2, 0x0

    const/4 v3, 0x0

    :try_start_0
    const-string v5, "_id"

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v7, v0, :cond_6

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-virtual {p1, v5, v6}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    :goto_1
    const-string v5, "raw_contact_id"

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v7, v0, :cond_7

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-virtual {p1, v5, v6}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    :goto_2
    const-string v5, "mimetype"

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v7, v0, :cond_5

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    :cond_5
    if-nez v3, :cond_8

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p0, v5, v6

    const/4 v6, 0x1

    aput-object p1, v5, v6

    const-string v6, "mimeType is absent in cursor."

    invoke-static {v5, v6}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/nio/BufferOverflowException; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v2, 0x0

    :goto_3
    if-ne v2, v8, :cond_14

    invoke-virtual {p1}, Ljava/nio/Buffer;->reset()Ljava/nio/Buffer;

    goto :goto_0

    :cond_6
    const-wide/16 v5, -0x1

    :try_start_1
    invoke-virtual {p1, v5, v6}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/nio/BufferOverflowException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_0
    move-exception v1

    new-array v5, v8, [Ljava/lang/Object;

    aput-object p0, v5, v4

    aput-object p1, v5, v9

    const/4 v4, 0x0

    invoke-static {v5, v4, v1}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v2, 0x0

    goto :goto_3

    :cond_7
    const-wide/16 v5, -0x1

    :try_start_2
    invoke-virtual {p1, v5, v6}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    goto :goto_2

    :catch_1
    move-exception v1

    const/4 v2, 0x2

    goto :goto_3

    :cond_8
    const-string v5, "vnd.android.cursor.item/name"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    const/16 v5, 0x9

    invoke-virtual {p1, v5}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    invoke-static {p0, p1}, Lcom/mediatek/apst/target/data/provider/contacts/ContactDataContent;->cursorToStructuredNameRaw(Landroid/database/Cursor;Ljava/nio/ByteBuffer;)I

    move-result v2

    goto :goto_3

    :cond_9
    const-string v5, "vnd.android.cursor.item/phone_v2"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    const/4 v5, 0x7

    invoke-virtual {p1, v5}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    invoke-static {p0, p1}, Lcom/mediatek/apst/target/data/provider/contacts/ContactDataContent;->cursorToPhoneRaw(Landroid/database/Cursor;Ljava/nio/ByteBuffer;)I

    move-result v2

    goto :goto_3

    :cond_a
    const-string v5, "vnd.android.cursor.item/photo"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    const/16 v5, 0xa

    invoke-virtual {p1, v5}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    invoke-static {p0, p1}, Lcom/mediatek/apst/target/data/provider/contacts/ContactDataContent;->cursorToPhotoRaw(Landroid/database/Cursor;Ljava/nio/ByteBuffer;)I

    move-result v2

    goto :goto_3

    :cond_b
    const-string v5, "vnd.android.cursor.item/im"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_c

    const/4 v5, 0x2

    invoke-virtual {p1, v5}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    invoke-static {p0, p1}, Lcom/mediatek/apst/target/data/provider/contacts/ContactDataContent;->cursorToImRaw(Landroid/database/Cursor;Ljava/nio/ByteBuffer;)I

    move-result v2

    goto :goto_3

    :cond_c
    const-string v5, "vnd.android.cursor.item/email_v2"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_d

    const/4 v5, 0x1

    invoke-virtual {p1, v5}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    invoke-static {p0, p1}, Lcom/mediatek/apst/target/data/provider/contacts/ContactDataContent;->cursorToEmailRaw(Landroid/database/Cursor;Ljava/nio/ByteBuffer;)I

    move-result v2

    goto :goto_3

    :cond_d
    const-string v5, "vnd.android.cursor.item/postal-address_v2"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_e

    const/4 v5, 0x3

    invoke-virtual {p1, v5}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    invoke-static {p0, p1}, Lcom/mediatek/apst/target/data/provider/contacts/ContactDataContent;->cursorToStructuredPostalRaw(Landroid/database/Cursor;Ljava/nio/ByteBuffer;)I

    move-result v2

    goto/16 :goto_3

    :cond_e
    const-string v5, "vnd.android.cursor.item/organization"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_f

    const/16 v5, 0x8

    invoke-virtual {p1, v5}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    invoke-static {p0, p1}, Lcom/mediatek/apst/target/data/provider/contacts/ContactDataContent;->cursorToOrganizationRaw(Landroid/database/Cursor;Ljava/nio/ByteBuffer;)I

    move-result v2

    goto/16 :goto_3

    :cond_f
    const-string v5, "vnd.android.cursor.item/nickname"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_10

    const/4 v5, 0x4

    invoke-virtual {p1, v5}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    invoke-static {p0, p1}, Lcom/mediatek/apst/target/data/provider/contacts/ContactDataContent;->cursorToNicknameRaw(Landroid/database/Cursor;Ljava/nio/ByteBuffer;)I

    move-result v2

    goto/16 :goto_3

    :cond_10
    const-string v5, "vnd.android.cursor.item/website"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_11

    const/4 v5, 0x5

    invoke-virtual {p1, v5}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    invoke-static {p0, p1}, Lcom/mediatek/apst/target/data/provider/contacts/ContactDataContent;->cursorToWebsiteRaw(Landroid/database/Cursor;Ljava/nio/ByteBuffer;)I

    move-result v2

    goto/16 :goto_3

    :cond_11
    const-string v5, "vnd.android.cursor.item/note"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_12

    const/4 v5, 0x6

    invoke-virtual {p1, v5}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    invoke-static {p0, p1}, Lcom/mediatek/apst/target/data/provider/contacts/ContactDataContent;->cursorToNoteRaw(Landroid/database/Cursor;Ljava/nio/ByteBuffer;)I

    move-result v2

    goto/16 :goto_3

    :cond_12
    const-string v5, "vnd.android.cursor.item/group_membership"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_13

    const/16 v5, 0xb

    invoke-virtual {p1, v5}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    invoke-static {p0, p1}, Lcom/mediatek/apst/target/data/provider/contacts/ContactDataContent;->cursorToGroupMembershipRaw(Landroid/database/Cursor;Ljava/nio/ByteBuffer;)I

    move-result v2

    goto/16 :goto_3

    :cond_13
    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p0, v5, v6

    const/4 v6, 0x1

    aput-object p1, v5, v6

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Ignored unknown mimeType: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/nio/BufferOverflowException; {:try_start_2 .. :try_end_2} :catch_1

    const/4 v2, 0x0

    goto/16 :goto_3

    :cond_14
    if-nez v2, :cond_0

    invoke-virtual {p1}, Ljava/nio/Buffer;->reset()Ljava/nio/Buffer;

    goto/16 :goto_0
.end method

.method private static cursorToStructuredName(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/StructuredName;
    .locals 6
    .param p0    # Landroid/database/Cursor;

    const/4 v3, 0x0

    const/4 v5, -0x1

    new-instance v1, Lcom/mediatek/apst/util/entity/contacts/StructuredName;

    invoke-direct {v1}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;-><init>()V

    :try_start_0
    const-string v4, "data1"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->setDisplayName(Ljava/lang/String;)V

    :cond_0
    const-string v4, "data2"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_1

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->setGivenName(Ljava/lang/String;)V

    :cond_1
    const-string v4, "data3"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_2

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->setFamilyName(Ljava/lang/String;)V

    :cond_2
    const-string v4, "data4"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_3

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->setPrefix(Ljava/lang/String;)V

    :cond_3
    const-string v4, "data5"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_4

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->setMiddleName(Ljava/lang/String;)V

    :cond_4
    const-string v4, "data6"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_5

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->setSuffix(Ljava/lang/String;)V

    :cond_5
    const-string v4, "data7"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_6

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->setPhoneticGivenName(Ljava/lang/String;)V

    :cond_6
    const-string v4, "data8"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_7

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->setPhoneticMiddleName(Ljava/lang/String;)V

    :cond_7
    const-string v4, "data9"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_8

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->setPhoneticFamilyName(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_8
    :goto_0
    return-object v1

    :catch_0
    move-exception v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-static {v4, v3, v2}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v3

    goto :goto_0
.end method

.method private static cursorToStructuredNameRaw(Landroid/database/Cursor;Ljava/nio/ByteBuffer;)I
    .locals 7
    .param p0    # Landroid/database/Cursor;
    .param p1    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v6, -0x1

    const/4 v5, 0x0

    :try_start_0
    const-string v4, "data1"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v6, v0, :cond_0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_0
    const-string v4, "data2"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v6, v0, :cond_1

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_1
    const-string v4, "data5"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v6, v0, :cond_2

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_2
    const-string v4, "data3"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v6, v0, :cond_3

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_3
    const-string v4, "data4"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v6, v0, :cond_4

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_4
    const-string v4, "data6"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v6, v0, :cond_5

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_5
    const-string v4, "data7"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v6, v0, :cond_6

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_6
    const-string v4, "data8"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v6, v0, :cond_7

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_7
    const-string v4, "data9"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v6, v0, :cond_8

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_8
    return v2

    :cond_0
    const/4 v4, 0x0

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p0, v4, v3

    aput-object p1, v4, v2

    invoke-static {v4, v5, v1}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v2, v3

    goto :goto_8

    :cond_1
    const/4 v4, 0x0

    :try_start_1
    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    const/4 v4, 0x0

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    goto :goto_3

    :cond_4
    const/4 v4, 0x0

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    goto :goto_4

    :cond_5
    const/4 v4, 0x0

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    goto :goto_5

    :cond_6
    const/4 v4, 0x0

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    goto :goto_6

    :cond_7
    const/4 v4, 0x0

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    goto :goto_7

    :cond_8
    const/4 v4, 0x0

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_8
.end method

.method private static cursorToStructuredPostal(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;
    .locals 6
    .param p0    # Landroid/database/Cursor;

    const/4 v3, 0x0

    const/4 v5, -0x1

    new-instance v1, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;

    invoke-direct {v1}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;-><init>()V

    :try_start_0
    const-string v4, "data1"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->setFormattedAddress(Ljava/lang/String;)V

    :cond_0
    const-string v4, "data2"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_1

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->setType(I)V

    :cond_1
    const-string v4, "data3"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_2

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->setLabel(Ljava/lang/String;)V

    :cond_2
    const-string v4, "data4"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_3

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->setStreet(Ljava/lang/String;)V

    :cond_3
    const-string v4, "data5"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_4

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->setPobox(Ljava/lang/String;)V

    :cond_4
    const-string v4, "data6"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_5

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->setNeighborhood(Ljava/lang/String;)V

    :cond_5
    const-string v4, "data7"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_6

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->setCity(Ljava/lang/String;)V

    :cond_6
    const-string v4, "data8"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_7

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->setRegion(Ljava/lang/String;)V

    :cond_7
    const-string v4, "data9"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_8

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->setPostcode(Ljava/lang/String;)V

    :cond_8
    const-string v4, "data10"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_9

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->setCountry(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_9
    :goto_0
    return-object v1

    :catch_0
    move-exception v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-static {v4, v3, v2}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v3

    goto :goto_0
.end method

.method private static cursorToStructuredPostalRaw(Landroid/database/Cursor;Ljava/nio/ByteBuffer;)I
    .locals 7
    .param p0    # Landroid/database/Cursor;
    .param p1    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v6, 0x0

    const/4 v5, -0x1

    :try_start_0
    const-string v4, "data1"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_0
    const-string v4, "data2"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_1

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    :goto_1
    const-string v4, "data3"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_2

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_2
    const-string v4, "data4"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_3

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_3
    const-string v4, "data5"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_4

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_4
    const-string v4, "data6"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_5

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_5
    const-string v4, "data7"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_6

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_6
    const-string v4, "data8"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_7

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_7
    const-string v4, "data9"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_8

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_8
    const-string v4, "data10"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_9

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_9
    return v2

    :cond_0
    const/4 v4, 0x0

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v1

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p0, v4, v3

    aput-object p1, v4, v2

    invoke-static {v4, v6, v1}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v2, v3

    goto :goto_9

    :cond_1
    const/4 v4, 0x1

    :try_start_1
    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto/16 :goto_1

    :cond_2
    const/4 v4, 0x0

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_3
    const/4 v4, 0x0

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    goto :goto_3

    :cond_4
    const/4 v4, 0x0

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    goto :goto_4

    :cond_5
    const/4 v4, 0x0

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    goto :goto_5

    :cond_6
    const/4 v4, 0x0

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    goto :goto_6

    :cond_7
    const/4 v4, 0x0

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    goto :goto_7

    :cond_8
    const/4 v4, 0x0

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    goto :goto_8

    :cond_9
    const/4 v4, 0x0

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_9
.end method

.method private static cursorToWebsite(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/Website;
    .locals 6
    .param p0    # Landroid/database/Cursor;

    const/4 v3, 0x0

    const/4 v5, -0x1

    new-instance v1, Lcom/mediatek/apst/util/entity/contacts/Website;

    invoke-direct {v1}, Lcom/mediatek/apst/util/entity/contacts/Website;-><init>()V

    :try_start_0
    const-string v4, "data1"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Website;->setUrl(Ljava/lang/String;)V

    :cond_0
    const-string v4, "data2"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_1

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Website;->setType(I)V

    :cond_1
    const-string v4, "data3"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_2

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Website;->setLabel(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_0
    return-object v1

    :catch_0
    move-exception v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-static {v4, v3, v2}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v3

    goto :goto_0
.end method

.method private static cursorToWebsiteRaw(Landroid/database/Cursor;Ljava/nio/ByteBuffer;)I
    .locals 7
    .param p0    # Landroid/database/Cursor;
    .param p1    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v2, 0x1

    const/4 v5, -0x1

    :try_start_0
    const-string v4, "data1"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_0
    const-string v4, "data2"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_1

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    :goto_1
    const-string v4, "data3"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_2

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :goto_2
    return v2

    :cond_0
    const/4 v4, 0x0

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p0, v4, v3

    aput-object p1, v4, v2

    invoke-static {v4, v6, v1}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v2, v3

    goto :goto_2

    :cond_1
    const/4 v4, 0x1

    :try_start_1
    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    invoke-static {p1, v4}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method
