.class public abstract Lcom/mediatek/apst/target/data/provider/contacts/RawContactsEntityContent;
.super Ljava/lang/Object;
.source "RawContactsEntityContent.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cursorToContactData(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/ContactData;
    .locals 14
    .param p0    # Landroid/database/Cursor;

    if-eqz p0, :cond_0

    invoke-interface {p0}, Landroid/database/Cursor;->getPosition()I

    move-result v11

    const/4 v12, -0x1

    if-eq v11, v12, :cond_0

    invoke-interface {p0}, Landroid/database/Cursor;->getPosition()I

    move-result v11

    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v12

    if-ne v11, v12, :cond_1

    :cond_0
    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_1
    const/4 v1, 0x0

    const-wide/16 v4, -0x1

    const-wide/16 v7, -0x1

    const/4 v6, 0x0

    const/4 v10, 0x0

    const/4 v9, 0x0

    :try_start_0
    const-string v11, "_id"

    invoke-interface {p0, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    const/4 v11, -0x1

    if-eq v11, v0, :cond_2

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    :cond_2
    const-string v11, "data_id"

    invoke-interface {p0, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    const/4 v11, -0x1

    if-eq v11, v0, :cond_4

    invoke-interface {p0, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v11

    if-eqz v11, :cond_3

    new-instance v2, Lcom/mediatek/apst/util/entity/contacts/ContactData;

    const-wide/16 v11, -0x1

    const/4 v13, -0x1

    invoke-direct {v2, v11, v12, v13}, Lcom/mediatek/apst/util/entity/contacts/ContactData;-><init>(JI)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    invoke-virtual {v2, v7, v8}, Lcom/mediatek/apst/util/entity/contacts/ContactData;->setRawContactId(J)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v3

    move-object v1, v2

    :goto_1
    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object p0, v11, v12

    const/4 v12, 0x0

    invoke-static {v11, v12, v3}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v2, 0x0

    goto :goto_0

    :cond_3
    :try_start_2
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    :cond_4
    const-string v11, "is_primary"

    invoke-interface {p0, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    const/4 v11, -0x1

    if-eq v11, v0, :cond_5

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    const/4 v12, 0x1

    if-ne v11, v12, :cond_8

    const/4 v6, 0x1

    :cond_5
    :goto_2
    const-string v11, "is_super_primary"

    invoke-interface {p0, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    const/4 v11, -0x1

    if-eq v11, v0, :cond_6

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    const/4 v12, 0x1

    if-ne v11, v12, :cond_9

    const/4 v10, 0x1

    :cond_6
    :goto_3
    const-string v11, "mimetype"

    invoke-interface {p0, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    const/4 v11, -0x1

    if-eq v11, v0, :cond_7

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    :cond_7
    if-nez v9, :cond_a

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object p0, v11, v12

    const-string v12, "mimeType is absent in cursor."

    invoke-static {v11, v12}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    goto :goto_0

    :cond_8
    const/4 v6, 0x0

    goto :goto_2

    :cond_9
    const/4 v10, 0x0

    goto :goto_3

    :cond_a
    const-string v11, "vnd.android.cursor.item/name"

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_b

    invoke-static {p0}, Lcom/mediatek/apst/target/data/provider/contacts/RawContactsEntityContent;->cursorToStructuredName(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/StructuredName;

    move-result-object v1

    :goto_4
    invoke-virtual {v1, v4, v5}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->setId(J)V

    invoke-virtual {v1, v7, v8}, Lcom/mediatek/apst/util/entity/contacts/ContactData;->setRawContactId(J)V

    invoke-virtual {v1, v6}, Lcom/mediatek/apst/util/entity/contacts/ContactData;->setPrimary(Z)V

    invoke-virtual {v1, v10}, Lcom/mediatek/apst/util/entity/contacts/ContactData;->setSuperPrimary(Z)V

    move-object v2, v1

    goto/16 :goto_0

    :cond_b
    const-string v11, "vnd.android.cursor.item/phone_v2"

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_c

    invoke-static {p0}, Lcom/mediatek/apst/target/data/provider/contacts/RawContactsEntityContent;->cursorToPhone(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/Phone;

    move-result-object v1

    goto :goto_4

    :cond_c
    const-string v11, "vnd.android.cursor.item/photo"

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_d

    invoke-static {p0}, Lcom/mediatek/apst/target/data/provider/contacts/RawContactsEntityContent;->cursorToPhoto(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/Photo;

    move-result-object v1

    goto :goto_4

    :cond_d
    const-string v11, "vnd.android.cursor.item/im"

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_e

    invoke-static {p0}, Lcom/mediatek/apst/target/data/provider/contacts/RawContactsEntityContent;->cursorToIm(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/Im;

    move-result-object v1

    goto :goto_4

    :cond_e
    const-string v11, "vnd.android.cursor.item/email_v2"

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_f

    invoke-static {p0}, Lcom/mediatek/apst/target/data/provider/contacts/RawContactsEntityContent;->cursorToEmail(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/Email;

    move-result-object v1

    goto :goto_4

    :cond_f
    const-string v11, "vnd.android.cursor.item/postal-address_v2"

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_10

    invoke-static {p0}, Lcom/mediatek/apst/target/data/provider/contacts/RawContactsEntityContent;->cursorToStructuredPostal(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;

    move-result-object v1

    goto :goto_4

    :cond_10
    const-string v11, "vnd.android.cursor.item/organization"

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_11

    invoke-static {p0}, Lcom/mediatek/apst/target/data/provider/contacts/RawContactsEntityContent;->cursorToOrganization(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/Organization;

    move-result-object v1

    goto :goto_4

    :cond_11
    const-string v11, "vnd.android.cursor.item/nickname"

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_12

    invoke-static {p0}, Lcom/mediatek/apst/target/data/provider/contacts/RawContactsEntityContent;->cursorToNickname(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/Nickname;

    move-result-object v1

    goto :goto_4

    :cond_12
    const-string v11, "vnd.android.cursor.item/website"

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_13

    invoke-static {p0}, Lcom/mediatek/apst/target/data/provider/contacts/RawContactsEntityContent;->cursorToWebsite(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/Website;

    move-result-object v1

    goto :goto_4

    :cond_13
    const-string v11, "vnd.android.cursor.item/note"

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_14

    invoke-static {p0}, Lcom/mediatek/apst/target/data/provider/contacts/RawContactsEntityContent;->cursorToNote(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/Note;

    move-result-object v1

    goto/16 :goto_4

    :cond_14
    const-string v11, "vnd.android.cursor.item/group_membership"

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_15

    invoke-static {p0}, Lcom/mediatek/apst/target/data/provider/contacts/RawContactsEntityContent;->cursorToGroupMembership(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/GroupMembership;

    move-result-object v1

    goto/16 :goto_4

    :cond_15
    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object p0, v11, v12

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Ignored unknown mimeType: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    const/4 v2, 0x0

    goto/16 :goto_0

    :catch_1
    move-exception v3

    goto/16 :goto_1
.end method

.method private static cursorToEmail(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/Email;
    .locals 6
    .param p0    # Landroid/database/Cursor;

    const/4 v3, 0x0

    const/4 v5, -0x1

    new-instance v1, Lcom/mediatek/apst/util/entity/contacts/Email;

    invoke-direct {v1}, Lcom/mediatek/apst/util/entity/contacts/Email;-><init>()V

    :try_start_0
    const-string v4, "data1"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Email;->setData(Ljava/lang/String;)V

    :cond_0
    const-string v4, "data2"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_1

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Email;->setType(I)V

    :cond_1
    const-string v4, "data3"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_2

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Email;->setLabel(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_0
    return-object v1

    :catch_0
    move-exception v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-static {v4, v3, v2}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v3

    goto :goto_0
.end method

.method private static cursorToGroupMembership(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/GroupMembership;
    .locals 6
    .param p0    # Landroid/database/Cursor;

    const/4 v3, 0x0

    new-instance v1, Lcom/mediatek/apst/util/entity/contacts/GroupMembership;

    invoke-direct {v1}, Lcom/mediatek/apst/util/entity/contacts/GroupMembership;-><init>()V

    :try_start_0
    const-string v4, "data1"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    const/4 v4, -0x1

    if-eq v4, v0, :cond_0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lcom/mediatek/apst/util/entity/contacts/GroupMembership;->setGroupId(J)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-object v1

    :catch_0
    move-exception v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-static {v4, v3, v2}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v3

    goto :goto_0
.end method

.method private static cursorToIm(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/Im;
    .locals 6
    .param p0    # Landroid/database/Cursor;

    const/4 v3, 0x0

    const/4 v5, -0x1

    new-instance v1, Lcom/mediatek/apst/util/entity/contacts/Im;

    invoke-direct {v1}, Lcom/mediatek/apst/util/entity/contacts/Im;-><init>()V

    :try_start_0
    const-string v4, "data1"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Im;->setData(Ljava/lang/String;)V

    :cond_0
    const-string v4, "data2"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_1

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Im;->setType(I)V

    :cond_1
    const-string v4, "data3"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_2

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Im;->setLabel(Ljava/lang/String;)V

    :cond_2
    const-string v4, "data5"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_3

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Im;->setProtocol(I)V

    :cond_3
    const-string v4, "data6"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_4

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Im;->setCustomProtocol(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_4
    :goto_0
    return-object v1

    :catch_0
    move-exception v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-static {v4, v3, v2}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v3

    goto :goto_0
.end method

.method private static cursorToNickname(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/Nickname;
    .locals 6
    .param p0    # Landroid/database/Cursor;

    const/4 v3, 0x0

    const/4 v5, -0x1

    new-instance v1, Lcom/mediatek/apst/util/entity/contacts/Nickname;

    invoke-direct {v1}, Lcom/mediatek/apst/util/entity/contacts/Nickname;-><init>()V

    :try_start_0
    const-string v4, "data1"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Nickname;->setName(Ljava/lang/String;)V

    :cond_0
    const-string v4, "data2"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_1

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Nickname;->setType(I)V

    :cond_1
    const-string v4, "data3"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_2

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Nickname;->setLabel(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_0
    return-object v1

    :catch_0
    move-exception v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-static {v4, v3, v2}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v3

    goto :goto_0
.end method

.method private static cursorToNote(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/Note;
    .locals 6
    .param p0    # Landroid/database/Cursor;

    const/4 v3, 0x0

    new-instance v1, Lcom/mediatek/apst/util/entity/contacts/Note;

    invoke-direct {v1}, Lcom/mediatek/apst/util/entity/contacts/Note;-><init>()V

    :try_start_0
    const-string v4, "data1"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    const/4 v4, -0x1

    if-eq v4, v0, :cond_0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Note;->setNote(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-object v1

    :catch_0
    move-exception v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-static {v4, v3, v2}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v3

    goto :goto_0
.end method

.method private static cursorToOrganization(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/Organization;
    .locals 6
    .param p0    # Landroid/database/Cursor;

    const/4 v3, 0x0

    const/4 v5, -0x1

    new-instance v1, Lcom/mediatek/apst/util/entity/contacts/Organization;

    invoke-direct {v1}, Lcom/mediatek/apst/util/entity/contacts/Organization;-><init>()V

    :try_start_0
    const-string v4, "data1"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Organization;->setCompany(Ljava/lang/String;)V

    :cond_0
    const-string v4, "data2"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_1

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Organization;->setType(I)V

    :cond_1
    const-string v4, "data3"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_2

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Organization;->setLabel(Ljava/lang/String;)V

    :cond_2
    const-string v4, "data4"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_3

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Organization;->setTitle(Ljava/lang/String;)V

    :cond_3
    const-string v4, "data5"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_4

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Organization;->setDepartment(Ljava/lang/String;)V

    :cond_4
    const-string v4, "data6"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_5

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Organization;->setJobDescription(Ljava/lang/String;)V

    :cond_5
    const-string v4, "data7"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_6

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Organization;->setSymbol(Ljava/lang/String;)V

    :cond_6
    const-string v4, "data8"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_7

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Organization;->setPhoneticName(Ljava/lang/String;)V

    :cond_7
    const-string v4, "data9"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_8

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Organization;->setOfficeLocation(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_8
    :goto_0
    return-object v1

    :catch_0
    move-exception v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-static {v4, v3, v2}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v3

    goto :goto_0
.end method

.method private static cursorToPhone(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/Phone;
    .locals 6
    .param p0    # Landroid/database/Cursor;

    const/4 v3, 0x0

    const/4 v5, -0x1

    new-instance v1, Lcom/mediatek/apst/util/entity/contacts/Phone;

    invoke-direct {v1}, Lcom/mediatek/apst/util/entity/contacts/Phone;-><init>()V

    :try_start_0
    const-string v4, "data1"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Phone;->setNumber(Ljava/lang/String;)V

    :cond_0
    const-string v4, "data2"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_1

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Phone;->setType(I)V

    :cond_1
    const-string v4, "data3"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_2

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Phone;->setLabel(Ljava/lang/String;)V

    :cond_2
    const-string v4, "sim_id"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_3

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Phone;->setBindingSimId(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_0
    return-object v1

    :catch_0
    move-exception v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-static {v4, v3, v2}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v3

    goto :goto_0
.end method

.method private static cursorToPhoto(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/Photo;
    .locals 6
    .param p0    # Landroid/database/Cursor;

    const/4 v3, 0x0

    new-instance v1, Lcom/mediatek/apst/util/entity/contacts/Photo;

    invoke-direct {v1}, Lcom/mediatek/apst/util/entity/contacts/Photo;-><init>()V

    :try_start_0
    const-string v4, "data15"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    const/4 v4, -0x1

    if-eq v4, v0, :cond_0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Photo;->setPhotoBytes([B)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-object v1

    :catch_0
    move-exception v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-static {v4, v3, v2}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v3

    goto :goto_0
.end method

.method private static cursorToStructuredName(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/StructuredName;
    .locals 6
    .param p0    # Landroid/database/Cursor;

    const/4 v3, 0x0

    const/4 v5, -0x1

    new-instance v1, Lcom/mediatek/apst/util/entity/contacts/StructuredName;

    invoke-direct {v1}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;-><init>()V

    :try_start_0
    const-string v4, "data1"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->setDisplayName(Ljava/lang/String;)V

    :cond_0
    const-string v4, "data2"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_1

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->setGivenName(Ljava/lang/String;)V

    :cond_1
    const-string v4, "data3"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_2

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->setFamilyName(Ljava/lang/String;)V

    :cond_2
    const-string v4, "data4"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_3

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->setPrefix(Ljava/lang/String;)V

    :cond_3
    const-string v4, "data5"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_4

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->setMiddleName(Ljava/lang/String;)V

    :cond_4
    const-string v4, "data6"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_5

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->setSuffix(Ljava/lang/String;)V

    :cond_5
    const-string v4, "data7"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_6

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->setPhoneticGivenName(Ljava/lang/String;)V

    :cond_6
    const-string v4, "data8"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_7

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->setPhoneticMiddleName(Ljava/lang/String;)V

    :cond_7
    const-string v4, "data9"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_8

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->setPhoneticFamilyName(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_8
    :goto_0
    return-object v1

    :catch_0
    move-exception v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-static {v4, v3, v2}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v3

    goto :goto_0
.end method

.method private static cursorToStructuredPostal(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;
    .locals 6
    .param p0    # Landroid/database/Cursor;

    const/4 v3, 0x0

    const/4 v5, -0x1

    new-instance v1, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;

    invoke-direct {v1}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;-><init>()V

    :try_start_0
    const-string v4, "data1"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->setFormattedAddress(Ljava/lang/String;)V

    :cond_0
    const-string v4, "data2"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_1

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->setType(I)V

    :cond_1
    const-string v4, "data3"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_2

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->setLabel(Ljava/lang/String;)V

    :cond_2
    const-string v4, "data4"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_3

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->setStreet(Ljava/lang/String;)V

    :cond_3
    const-string v4, "data5"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_4

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->setPobox(Ljava/lang/String;)V

    :cond_4
    const-string v4, "data6"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_5

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->setNeighborhood(Ljava/lang/String;)V

    :cond_5
    const-string v4, "data7"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_6

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->setCity(Ljava/lang/String;)V

    :cond_6
    const-string v4, "data8"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_7

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->setRegion(Ljava/lang/String;)V

    :cond_7
    const-string v4, "data9"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_8

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->setPostcode(Ljava/lang/String;)V

    :cond_8
    const-string v4, "data10"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_9

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->setCountry(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_9
    :goto_0
    return-object v1

    :catch_0
    move-exception v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-static {v4, v3, v2}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v3

    goto :goto_0
.end method

.method private static cursorToWebsite(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/contacts/Website;
    .locals 6
    .param p0    # Landroid/database/Cursor;

    const/4 v3, 0x0

    const/4 v5, -0x1

    new-instance v1, Lcom/mediatek/apst/util/entity/contacts/Website;

    invoke-direct {v1}, Lcom/mediatek/apst/util/entity/contacts/Website;-><init>()V

    :try_start_0
    const-string v4, "data1"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Website;->setUrl(Ljava/lang/String;)V

    :cond_0
    const-string v4, "data2"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_1

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Website;->setType(I)V

    :cond_1
    const-string v4, "data3"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_2

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/entity/contacts/Website;->setLabel(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_0
    return-object v1

    :catch_0
    move-exception v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-static {v4, v3, v2}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v3

    goto :goto_0
.end method
