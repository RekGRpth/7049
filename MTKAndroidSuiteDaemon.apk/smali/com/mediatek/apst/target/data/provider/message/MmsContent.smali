.class public abstract Lcom/mediatek/apst/target/data/provider/message/MmsContent;
.super Ljava/lang/Object;
.source "MmsContent.java"


# static fields
.field public static final ADDR_CHARSET_VALUE:I = 0x6a

.field public static final ADDR_TYPE_RECEIVE:I = 0x89

.field public static final ADDR_TYPE_SENT:I = 0x97

.field public static final COLUMN_ADDR_ADDRESS:Ljava/lang/String; = "address"

.field public static final COLUMN_ADDR_CHARSET:Ljava/lang/String; = "charset"

.field public static final COLUMN_ADDR_MSG_ID:Ljava/lang/String; = "msg_id"

.field public static final COLUMN_ADDR_TYPE:Ljava/lang/String; = "type"

.field public static final COLUMN_CT_CLS:Ljava/lang/String; = "ct_cls"

.field public static final COLUMN_CT_L:Ljava/lang/String; = "ct_l"

.field public static final COLUMN_CT_T:Ljava/lang/String; = "ct_t"

.field public static final COLUMN_DATE:Ljava/lang/String; = "date"

.field public static final COLUMN_DATE_SENT:Ljava/lang/String; = "date_sent"

.field public static final COLUMN_D_RPT:Ljava/lang/String; = "d_rpt"

.field public static final COLUMN_D_TM:Ljava/lang/String; = "d_tm"

.field public static final COLUMN_EXP:Ljava/lang/String; = "exp"

.field public static final COLUMN_ID:Ljava/lang/String; = "_id"

.field public static final COLUMN_LOCKED:Ljava/lang/String; = "locked"

.field public static final COLUMN_MSG_BOX:Ljava/lang/String; = "msg_box"

.field public static final COLUMN_M_CLS:Ljava/lang/String; = "m_cls"

.field public static final COLUMN_M_ID:Ljava/lang/String; = "m_id"

.field public static final COLUMN_M_SIZE:Ljava/lang/String; = "m_size"

.field public static final COLUMN_M_TYPE:Ljava/lang/String; = "m_type"

.field public static final COLUMN_PART_CHARSET:Ljava/lang/String; = "chset"

.field public static final COLUMN_PART_CID:Ljava/lang/String; = "cid"

.field public static final COLUMN_PART_CL:Ljava/lang/String; = "cl"

.field public static final COLUMN_PART_CONTENTTYPE:Ljava/lang/String; = "ct"

.field public static final COLUMN_PART_DATAPATH:Ljava/lang/String; = "_data"

.field public static final COLUMN_PART_ID:Ljava/lang/String; = "_id"

.field public static final COLUMN_PART_MID:Ljava/lang/String; = "mid"

.field public static final COLUMN_PART_NAME:Ljava/lang/String; = "name"

.field public static final COLUMN_PART_SEQ:Ljava/lang/String; = "seq"

.field public static final COLUMN_PART_TEXT:Ljava/lang/String; = "text"

.field public static final COLUMN_PRI:Ljava/lang/String; = "pri"

.field public static final COLUMN_READ:Ljava/lang/String; = "read"

.field public static final COLUMN_READ_STATUS:Ljava/lang/String; = "read_status"

.field public static final COLUMN_RESP_ST:Ljava/lang/String; = "resp_st"

.field public static final COLUMN_RESP_TXT:Ljava/lang/String; = "resp_txt"

.field public static final COLUMN_RETR_ST:Ljava/lang/String; = "retr_st"

.field public static final COLUMN_RETR_TXT:Ljava/lang/String; = "retr_txt"

.field public static final COLUMN_RETR_TXT_CS:Ljava/lang/String; = "retr_txt_cs"

.field public static final COLUMN_RPT_A:Ljava/lang/String; = "rpt_a"

.field public static final COLUMN_RR:Ljava/lang/String; = "rr"

.field public static final COLUMN_SEEN:Ljava/lang/String; = "seen"

.field public static final COLUMN_SIM_ID:Ljava/lang/String; = "sim_id"

.field public static final COLUMN_ST:Ljava/lang/String; = "st"

.field public static final COLUMN_SUBJECT:Ljava/lang/String; = "sub"

.field public static final COLUMN_SUBJECT_CHAR_SET:Ljava/lang/String; = "sub_cs"

.field public static final COLUMN_THREAD_ID:Ljava/lang/String; = "thread_id"

.field public static final COLUMN_TR_ID:Ljava/lang/String; = "tr_id"

.field public static final COLUMN_V:Ljava/lang/String; = "v"

.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final CONTENT_URI_ADDR:Landroid/net/Uri;

.field public static final CONTENT_URI_CONVERSATIONS:Landroid/net/Uri;

.field public static final CONTENT_URI_DRAFT:Landroid/net/Uri;

.field public static final CONTENT_URI_INBOX:Landroid/net/Uri;

.field public static final CONTENT_URI_OB:Landroid/net/Uri;

.field public static final CONTENT_URI_PART:Landroid/net/Uri;

.field public static final CONTENT_URI_SENT:Landroid/net/Uri;

.field public static final NOTIFY_MMS:Ljava/lang/String; = "130"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "content://mms"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/mediatek/apst/target/data/provider/message/MmsContent;->CONTENT_URI:Landroid/net/Uri;

    const-string v0, "content://mms/part"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/mediatek/apst/target/data/provider/message/MmsContent;->CONTENT_URI_PART:Landroid/net/Uri;

    const-string v0, "content://mms/addr"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/mediatek/apst/target/data/provider/message/MmsContent;->CONTENT_URI_ADDR:Landroid/net/Uri;

    const-string v0, "content://mms-sms/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/mediatek/apst/target/data/provider/message/MmsContent;->CONTENT_URI_OB:Landroid/net/Uri;

    const-string v0, "content://mms/inbox"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/mediatek/apst/target/data/provider/message/MmsContent;->CONTENT_URI_INBOX:Landroid/net/Uri;

    const-string v0, "content://mms/sent"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/mediatek/apst/target/data/provider/message/MmsContent;->CONTENT_URI_SENT:Landroid/net/Uri;

    const-string v0, "content://mms/draft"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/mediatek/apst/target/data/provider/message/MmsContent;->CONTENT_URI_DRAFT:Landroid/net/Uri;

    const-string v0, "content://mms-mms/conversations"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/mediatek/apst/target/data/provider/message/MmsContent;->CONTENT_URI_CONVERSATIONS:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cursorToMms(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/message/Mms;
    .locals 12
    .param p0    # Landroid/database/Cursor;

    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v11, -0x1

    if-eqz p0, :cond_0

    invoke-interface {p0}, Landroid/database/Cursor;->getPosition()I

    move-result v4

    if-eq v4, v11, :cond_0

    invoke-interface {p0}, Landroid/database/Cursor;->getPosition()I

    move-result v4

    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v7

    if-ne v4, v7, :cond_2

    :cond_0
    move-object v2, v3

    :cond_1
    :goto_0
    return-object v2

    :cond_2
    new-instance v2, Lcom/mediatek/apst/util/entity/message/Mms;

    invoke-direct {v2}, Lcom/mediatek/apst/util/entity/message/Mms;-><init>()V

    :try_start_0
    const-string v4, "_id"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v11, v0, :cond_3

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    invoke-virtual {v2, v7, v8}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->setId(J)V

    :cond_3
    const-string v4, "thread_id"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v11, v0, :cond_4

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    invoke-virtual {v2, v7, v8}, Lcom/mediatek/apst/util/entity/message/Message;->setThreadId(J)V

    :cond_4
    const-string v4, "date"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v11, v0, :cond_5

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    const-wide/16 v9, 0x3e8

    mul-long/2addr v7, v9

    invoke-virtual {v2, v7, v8}, Lcom/mediatek/apst/util/entity/message/Message;->setDate(J)V

    :cond_5
    const-string v4, "msg_box"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v11, v0, :cond_6

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v2, v4}, Lcom/mediatek/apst/util/entity/message/Message;->setBox(I)V

    :cond_6
    const-string v4, "read"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v11, v0, :cond_7

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-ne v4, v5, :cond_15

    move v4, v5

    :goto_1
    invoke-virtual {v2, v4}, Lcom/mediatek/apst/util/entity/message/Message;->setRead(Z)V

    :cond_7
    const-string v4, "locked"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v11, v0, :cond_8

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-ne v4, v5, :cond_16

    move v4, v5

    :goto_2
    invoke-virtual {v2, v4}, Lcom/mediatek/apst/util/entity/message/Message;->setLocked(Z)V

    :cond_8
    const-string v4, "sub"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v11, v0, :cond_9

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/mediatek/apst/util/entity/message/Message;->setSubject(Ljava/lang/String;)V

    :cond_9
    const-string v4, "ct_t"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v11, v0, :cond_a

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/mediatek/apst/util/entity/message/Mms;->setContentType(Ljava/lang/String;)V

    :cond_a
    const-string v4, "m_id"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v11, v0, :cond_b

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/mediatek/apst/util/entity/message/Mms;->setM_id(Ljava/lang/String;)V

    :cond_b
    const-string v4, "sub_cs"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v11, v0, :cond_c

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/mediatek/apst/util/entity/message/Mms;->setSub_cs(Ljava/lang/String;)V

    :cond_c
    const-string v4, "m_cls"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v11, v0, :cond_d

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/mediatek/apst/util/entity/message/Mms;->setM_cls(Ljava/lang/String;)V

    :cond_d
    const-string v4, "m_type"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v11, v0, :cond_e

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/mediatek/apst/util/entity/message/Mms;->setM_type(Ljava/lang/String;)V

    :cond_e
    const-string v4, "v"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v11, v0, :cond_f

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/mediatek/apst/util/entity/message/Mms;->setV(Ljava/lang/String;)V

    :cond_f
    const-string v4, "m_size"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v11, v0, :cond_10

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/mediatek/apst/util/entity/message/Mms;->setM_size(Ljava/lang/String;)V

    :cond_10
    const-string v4, "tr_id"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v11, v0, :cond_11

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/mediatek/apst/util/entity/message/Mms;->setTr_id(Ljava/lang/String;)V

    :cond_11
    const-string v4, "d_rpt"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v11, v0, :cond_12

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/mediatek/apst/util/entity/message/Mms;->setD_rpt(Ljava/lang/String;)V

    :cond_12
    const-string v4, "seen"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v11, v0, :cond_13

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/mediatek/apst/util/entity/message/Mms;->setSeen(Ljava/lang/String;)V

    :cond_13
    const-string v4, "date_sent"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v11, v0, :cond_14

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v2, v4}, Lcom/mediatek/apst/util/entity/message/Message;->setDate_sent(I)V

    :cond_14
    const-string v4, "sim_id"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v11, v0, :cond_1

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v2, v4}, Lcom/mediatek/apst/util/entity/message/Message;->setSimId(I)V

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Lcom/mediatek/apst/target/util/Global;->getSimName(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/mediatek/apst/util/entity/message/Message;->setSimName(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v1

    new-array v4, v5, [Ljava/lang/Object;

    aput-object p0, v4, v6

    invoke-static {v4, v3, v1}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :cond_15
    move v4, v6

    goto/16 :goto_1

    :cond_16
    move v4, v6

    goto/16 :goto_2
.end method

.method public static cursorToMmsPart(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/message/MmsPart;
    .locals 7
    .param p0    # Landroid/database/Cursor;

    const/4 v3, 0x0

    const/4 v6, -0x1

    if-eqz p0, :cond_0

    invoke-interface {p0}, Landroid/database/Cursor;->getPosition()I

    move-result v4

    if-eq v4, v6, :cond_0

    invoke-interface {p0}, Landroid/database/Cursor;->getPosition()I

    move-result v4

    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-ne v4, v5, :cond_2

    :cond_0
    move-object v2, v3

    :cond_1
    :goto_0
    return-object v2

    :cond_2
    new-instance v2, Lcom/mediatek/apst/util/entity/message/MmsPart;

    invoke-direct {v2}, Lcom/mediatek/apst/util/entity/message/MmsPart;-><init>()V

    :try_start_0
    const-string v4, "_id"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v6, v0, :cond_3

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->setId(J)V

    :cond_3
    const-string v4, "mid"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v6, v0, :cond_4

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/mediatek/apst/util/entity/message/MmsPart;->setMmsId(J)V

    :cond_4
    const-string v4, "seq"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v6, v0, :cond_5

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v2, v4}, Lcom/mediatek/apst/util/entity/message/MmsPart;->setSequence(I)V

    :cond_5
    const-string v4, "ct"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v6, v0, :cond_6

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/mediatek/apst/util/entity/message/MmsPart;->setContentType(Ljava/lang/String;)V

    :cond_6
    const-string v4, "name"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v6, v0, :cond_7

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/mediatek/apst/util/entity/message/MmsPart;->setName(Ljava/lang/String;)V

    :cond_7
    const-string v4, "chset"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v6, v0, :cond_8

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/mediatek/apst/util/entity/message/MmsPart;->setCharset(Ljava/lang/String;)V

    :cond_8
    const-string v4, "cid"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v6, v0, :cond_9

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/mediatek/apst/util/entity/message/MmsPart;->setCid(Ljava/lang/String;)V

    :cond_9
    const-string v4, "cl"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v6, v0, :cond_a

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/mediatek/apst/util/entity/message/MmsPart;->setCl(Ljava/lang/String;)V

    :cond_a
    const-string v4, "_data"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v6, v0, :cond_b

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/mediatek/apst/util/entity/message/MmsPart;->setDataPath(Ljava/lang/String;)V

    :cond_b
    const-string v4, "text"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v6, v0, :cond_1

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/mediatek/apst/util/entity/message/MmsPart;->setText(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v1

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-static {v4, v3, v1}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0
.end method

.method public static extractAddrSpec(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;

    sget-object v1, Lcom/mediatek/apst/target/util/StringUtils;->NAME_ADDR_EMAIL_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object p0

    :cond_0
    return-object p0
.end method

.method public static getParts(Ljava/util/List;Ljava/lang/Long;)Ljava/util/List;
    .locals 7
    .param p1    # Ljava/lang/Long;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/apst/util/entity/message/MmsPart;",
            ">;",
            "Ljava/lang/Long;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/apst/util/entity/message/MmsPart;",
            ">;"
        }
    .end annotation

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/apst/util/entity/message/MmsPart;

    invoke-virtual {v1}, Lcom/mediatek/apst/util/entity/message/MmsPart;->getMmsId()J

    move-result-wide v3

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-nez v3, :cond_0

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v2
.end method

.method public static isEmailAddress(Ljava/lang/String;)Z
    .locals 4
    .param p0    # Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    invoke-static {p0}, Lcom/mediatek/apst/target/data/provider/message/MmsContent;->extractAddrSpec(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    sget-object v2, Lcom/mediatek/apst/target/util/StringUtils;->EMAIL_ADDRESS_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    goto :goto_0
.end method


# virtual methods
.method public mmsToContentValues(Lcom/mediatek/apst/util/entity/message/Mms;)Landroid/content/ContentValues;
    .locals 8
    .param p1    # Lcom/mediatek/apst/util/entity/message/Mms;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz p1, :cond_2

    new-array v1, v2, [Ljava/lang/Object;

    aput-object p1, v1, v3

    const-string v4, ">>insertMms begin"

    invoke-static {v1, v4}, Lcom/mediatek/apst/target/util/Debugger;->logI([Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Landroid/content/ContentValues;

    const/16 v1, 0x13

    invoke-direct {v0, v1}, Landroid/content/ContentValues;-><init>(I)V

    const-string v1, "_id"

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->getId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "thread_id"

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/message/Message;->getThreadId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "sub"

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/message/Message;->getSubject()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "ct_t"

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/message/Mms;->getContentType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "msg_box"

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/message/Message;->getBox()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "date"

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/message/Message;->getDate()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v4, "read"

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/message/Message;->isRead()Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "locked"

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/message/Message;->isLocked()Z

    move-result v4

    if-eqz v4, :cond_1

    :goto_1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "m_id"

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/message/Mms;->getM_id()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "sub_cs"

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/message/Mms;->getSub_cs()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "m_cls"

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/message/Mms;->getM_cls()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "m_type"

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/message/Mms;->getM_type()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "v"

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/message/Mms;->getV()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "m_size"

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/message/Mms;->getM_size()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "tr_id"

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/message/Mms;->getTr_id()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "d_rpt"

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/message/Mms;->getD_rpt()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "seen"

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/message/Mms;->getSeen()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "sim_id"

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/message/Message;->getSimId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "date_sent"

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/message/Message;->getDate_sent()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :goto_2
    return-object v0

    :cond_0
    move v1, v3

    goto/16 :goto_0

    :cond_1
    move v2, v3

    goto :goto_1

    :cond_2
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    goto :goto_2
.end method
