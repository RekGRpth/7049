.class public Lcom/mediatek/apst/target/data/proxy/contacts/FastSimContactsCursorParser;
.super Lcom/mediatek/apst/target/data/proxy/FastCursorParser;
.source "FastSimContactsCursorParser.java"


# instance fields
.field private mSimId:I


# direct methods
.method public constructor <init>(Landroid/database/Cursor;Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;)V
    .locals 1
    .param p1    # Landroid/database/Cursor;
    .param p2    # Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;

    invoke-direct {p0, p1, p2}, Lcom/mediatek/apst/target/data/proxy/FastCursorParser;-><init>(Landroid/database/Cursor;Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/apst/target/data/proxy/contacts/FastSimContactsCursorParser;->mSimId:I

    return-void
.end method

.method public constructor <init>(Landroid/database/Cursor;Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;I)V
    .locals 0
    .param p1    # Landroid/database/Cursor;
    .param p2    # Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;
    .param p3    # I

    invoke-direct {p0, p1, p2}, Lcom/mediatek/apst/target/data/proxy/FastCursorParser;-><init>(Landroid/database/Cursor;Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;)V

    iput p3, p0, Lcom/mediatek/apst/target/data/proxy/contacts/FastSimContactsCursorParser;->mSimId:I

    return-void
.end method

.method public constructor <init>(Landroid/database/Cursor;Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V
    .locals 1
    .param p1    # Landroid/database/Cursor;
    .param p2    # Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;
    .param p3    # Ljava/nio/ByteBuffer;

    invoke-direct {p0, p1, p2, p3}, Lcom/mediatek/apst/target/data/proxy/FastCursorParser;-><init>(Landroid/database/Cursor;Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/apst/target/data/proxy/contacts/FastSimContactsCursorParser;->mSimId:I

    return-void
.end method

.method public constructor <init>(Landroid/database/Cursor;Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;I)V
    .locals 0
    .param p1    # Landroid/database/Cursor;
    .param p2    # Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;
    .param p3    # Ljava/nio/ByteBuffer;
    .param p4    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/mediatek/apst/target/data/proxy/FastCursorParser;-><init>(Landroid/database/Cursor;Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    iput p4, p0, Lcom/mediatek/apst/target/data/proxy/contacts/FastSimContactsCursorParser;->mSimId:I

    return-void
.end method


# virtual methods
.method public onParseCursorToRaw(Landroid/database/Cursor;Ljava/nio/ByteBuffer;)I
    .locals 1
    .param p1    # Landroid/database/Cursor;
    .param p2    # Ljava/nio/ByteBuffer;

    iget v0, p0, Lcom/mediatek/apst/target/data/proxy/contacts/FastSimContactsCursorParser;->mSimId:I

    invoke-static {p1, p2, v0}, Lcom/mediatek/apst/target/data/provider/contacts/SimContactsContent;->cursorToRaw(Landroid/database/Cursor;Ljava/nio/ByteBuffer;I)I

    move-result v0

    return v0
.end method
