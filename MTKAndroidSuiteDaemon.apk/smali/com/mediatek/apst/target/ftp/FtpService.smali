.class public Lcom/mediatek/apst/target/ftp/FtpService;
.super Landroid/app/Service;
.source "FtpService.java"


# instance fields
.field private mFtpServer:Lcom/mediatek/apst/target/ftp/FtpServer;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    new-instance v0, Lcom/mediatek/apst/target/ftp/FtpServer;

    invoke-direct {v0}, Lcom/mediatek/apst/target/ftp/FtpServer;-><init>()V

    iput-object v0, p0, Lcom/mediatek/apst/target/ftp/FtpService;->mFtpServer:Lcom/mediatek/apst/target/ftp/FtpServer;

    iget-object v0, p0, Lcom/mediatek/apst/target/ftp/FtpService;->mFtpServer:Lcom/mediatek/apst/target/ftp/FtpServer;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/target/ftp/FtpService;->mFtpServer:Lcom/mediatek/apst/target/ftp/FtpServer;

    invoke-virtual {v0}, Lcom/mediatek/apst/target/ftp/FtpServer;->destroy()V

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method
