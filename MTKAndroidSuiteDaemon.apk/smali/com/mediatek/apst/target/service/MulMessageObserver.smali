.class public Lcom/mediatek/apst/target/service/MulMessageObserver;
.super Landroid/database/ContentObserver;
.source "MulMessageObserver.java"

# interfaces
.implements Lcom/mediatek/apst/target/data/proxy/ISelfChangeObserver;


# instance fields
.field private mMaxMmsId:J

.field private mObservering:Z

.field mProxy:Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

.field private mSelfChangingContent:Z


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;)V
    .locals 2
    .param p1    # Landroid/os/Handler;
    .param p2    # Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    iput-boolean v0, p0, Lcom/mediatek/apst/target/service/MulMessageObserver;->mSelfChangingContent:Z

    iput-boolean v0, p0, Lcom/mediatek/apst/target/service/MulMessageObserver;->mObservering:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mediatek/apst/target/service/MulMessageObserver;->mMaxMmsId:J

    iput-object p2, p0, Lcom/mediatek/apst/target/service/MulMessageObserver;->mProxy:Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    return-void
.end method


# virtual methods
.method public deliverSelfNotifications()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isSelfChangingContent()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/apst/target/service/MulMessageObserver;->mSelfChangingContent:Z

    return v0
.end method

.method public onChange(Z)V
    .locals 11
    .param p1    # Z

    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    iget-boolean v6, p0, Lcom/mediatek/apst/target/service/MulMessageObserver;->mObservering:Z

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/mediatek/apst/target/service/MulMessageObserver;->mProxy:Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    invoke-virtual {v6}, Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;->getMaxMmsId()J

    move-result-wide v3

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/mediatek/apst/target/service/MulMessageObserver;->isSelfChangingContent()Z

    move-result v6

    if-nez v6, :cond_1

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v6, v7

    const-string v7, "Message content is changed by other applications!"

    invoke-static {v6, v7}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    iget-wide v6, p0, Lcom/mediatek/apst/target/service/MulMessageObserver;->mMaxMmsId:J

    sub-long v0, v3, v6

    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-lez v6, :cond_2

    const/4 v2, 0x0

    :goto_0
    int-to-long v6, v2

    cmp-long v6, v6, v0

    if-gez v6, :cond_2

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v6, v7

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "New MMS insert, id="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, p0, Lcom/mediatek/apst/target/service/MulMessageObserver;->mMaxMmsId:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/apst/target/util/Debugger;->logI([Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v6, p0, Lcom/mediatek/apst/target/service/MulMessageObserver;->mProxy:Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    iget-wide v7, p0, Lcom/mediatek/apst/target/service/MulMessageObserver;->mMaxMmsId:J

    int-to-long v9, v2

    add-long/2addr v7, v9

    const-wide/16 v9, 0x1

    add-long/2addr v7, v9

    invoke-virtual {v6, v7, v8}, Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;->getMms(J)Lcom/mediatek/apst/util/entity/message/Mms;

    move-result-object v5

    if-eqz v5, :cond_0

    new-instance v6, Lcom/mediatek/apst/target/event/Event;

    invoke-direct {v6}, Lcom/mediatek/apst/target/event/Event;-><init>()V

    const-string v7, "by_self"

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/mediatek/apst/target/event/Event;->put(Ljava/lang/String;Ljava/lang/Object;)Lcom/mediatek/apst/target/event/Event;

    move-result-object v6

    const-string v7, "mms"

    invoke-virtual {v6, v7, v5}, Lcom/mediatek/apst/target/event/Event;->put(Ljava/lang/String;Ljava/lang/Object;)Lcom/mediatek/apst/target/event/Event;

    move-result-object v6

    invoke-static {v6}, Lcom/mediatek/apst/target/event/EventDispatcher;->dispatchMmsInsertedEvent(Lcom/mediatek/apst/target/event/Event;)V

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v6, v7

    const-string v7, "New Mms is null"

    invoke-static {v6, v7}, Lcom/mediatek/apst/target/util/Debugger;->logI([Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    :catchall_0
    move-exception v6

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6

    :cond_1
    const/4 v6, 0x1

    :try_start_1
    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v6, v7

    const-string v7, "Message content is changed by self."

    invoke-static {v6, v7}, Lcom/mediatek/apst/target/util/Debugger;->logI([Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    :cond_2
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iput-wide v3, p0, Lcom/mediatek/apst/target/service/MulMessageObserver;->mMaxMmsId:J

    :cond_3
    return-void
.end method

.method public onSelfChangeDone()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/apst/target/service/MulMessageObserver;->mSelfChangingContent:Z

    return-void
.end method

.method public onSelfChangeStart()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/apst/target/service/MulMessageObserver;->mSelfChangingContent:Z

    return-void
.end method

.method public start()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MulMessageObserver;->mProxy:Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    if-nez v0, :cond_1

    const-string v0, "Proxy is null."

    invoke-static {v0}, Lcom/mediatek/apst/target/util/Debugger;->logE(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/mediatek/apst/target/service/MulMessageObserver;->mObservering:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MulMessageObserver;->mProxy:Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    invoke-virtual {v0}, Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;->getMaxMmsId()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mediatek/apst/target/service/MulMessageObserver;->mMaxMmsId:J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/apst/target/service/MulMessageObserver;->mObservering:Z

    const-string v0, "Start observering mms content."

    invoke-static {v0}, Lcom/mediatek/apst/target/util/Debugger;->logE(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public stop()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/apst/target/service/MulMessageObserver;->mObservering:Z

    const-string v0, "Stop observering mms content."

    invoke-static {v0}, Lcom/mediatek/apst/target/util/Debugger;->logI(Ljava/lang/String;)V

    return-void
.end method
