.class public abstract Lcom/mediatek/apst/target/service/NewSmsFinder;
.super Ljava/lang/Thread;
.source "NewSmsFinder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/apst/target/service/NewSmsFinder$Clue;
    }
.end annotation


# instance fields
.field private mFindList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/apst/target/service/NewSmsFinder$Clue;",
            ">;"
        }
    .end annotation
.end field

.field private mLastSmsTimeStamp:J

.field private mShouldTerminate:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/apst/target/service/NewSmsFinder;->mFindList:Ljava/util/List;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/apst/target/service/NewSmsFinder;->mShouldTerminate:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mediatek/apst/target/service/NewSmsFinder;->mLastSmsTimeStamp:J

    return-void
.end method


# virtual methods
.method public declared-synchronized appendTask(Lcom/mediatek/apst/target/service/NewSmsFinder$Clue;)V
    .locals 1
    .param p1    # Lcom/mediatek/apst/target/service/NewSmsFinder$Clue;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mediatek/apst/target/service/NewSmsFinder;->mFindList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public abstract findSms(JLjava/lang/String;Ljava/lang/String;I)Lcom/mediatek/apst/util/entity/message/Sms;
.end method

.method public getClassName()Ljava/lang/String;
    .locals 1

    const-string v0, "NewSmsFinder"

    return-object v0
.end method

.method public isShouldTerminate()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/apst/target/service/NewSmsFinder;->mShouldTerminate:Z

    return v0
.end method

.method public run()V
    .locals 12

    const/4 v11, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/mediatek/apst/target/service/NewSmsFinder;->isShouldTerminate()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/mediatek/apst/target/service/NewSmsFinder;->mFindList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/apst/target/service/NewSmsFinder;->mFindList:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/mediatek/apst/target/service/NewSmsFinder$Clue;

    if-eqz v10, :cond_2

    const/16 v9, 0x1e

    const/4 v7, 0x0

    :goto_1
    if-lez v9, :cond_1

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v10}, Lcom/mediatek/apst/target/service/NewSmsFinder$Clue;->getDate()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/mediatek/apst/target/service/NewSmsFinder;->mLastSmsTimeStamp:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    iget-wide v0, p0, Lcom/mediatek/apst/target/service/NewSmsFinder;->mLastSmsTimeStamp:J

    invoke-virtual {v10, v0, v1}, Lcom/mediatek/apst/target/service/NewSmsFinder$Clue;->setDate(J)V

    :cond_0
    invoke-virtual {v10}, Lcom/mediatek/apst/target/service/NewSmsFinder$Clue;->getDate()J

    move-result-wide v1

    invoke-virtual {v10}, Lcom/mediatek/apst/target/service/NewSmsFinder$Clue;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10}, Lcom/mediatek/apst/target/service/NewSmsFinder$Clue;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v10}, Lcom/mediatek/apst/target/service/NewSmsFinder$Clue;->getBox()I

    move-result v5

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/mediatek/apst/target/service/NewSmsFinder;->findSms(JLjava/lang/String;Ljava/lang/String;I)Lcom/mediatek/apst/util/entity/message/Sms;

    move-result-object v8

    if-eqz v8, :cond_3

    invoke-virtual {v8}, Lcom/mediatek/apst/util/entity/message/Message;->getDate()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mediatek/apst/target/service/NewSmsFinder;->mLastSmsTimeStamp:J

    const/4 v7, 0x1

    :cond_1
    if-nez v7, :cond_2

    invoke-virtual {p0}, Lcom/mediatek/apst/target/service/NewSmsFinder;->getClassName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "run"

    const-string v2, "Fail to find the new SMS."

    invoke-static {v0, v1, v11, v2}, Lcom/mediatek/apst/target/util/Debugger;->logW(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/String;)V

    :cond_2
    const-wide/16 v0, 0x7d0

    :try_start_0
    invoke-static {v0, v1}, Lcom/mediatek/apst/target/service/NewSmsFinder;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v6

    invoke-virtual {p0}, Lcom/mediatek/apst/target/service/NewSmsFinder;->getClassName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "run"

    invoke-static {v0, v1, v11, v11, v6}, Lcom/mediatek/apst/target/util/Debugger;->logE(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_3
    const-wide/16 v0, 0x64

    :try_start_1
    invoke-static {v0, v1}, Lcom/mediatek/apst/target/service/NewSmsFinder;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v6

    invoke-virtual {p0}, Lcom/mediatek/apst/target/service/NewSmsFinder;->getClassName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "run"

    invoke-static {v0, v1, v11, v11, v6}, Lcom/mediatek/apst/target/util/Debugger;->logE(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_4
    return-void
.end method

.method public terminate()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/apst/target/service/NewSmsFinder;->mShouldTerminate:Z

    return-void
.end method
