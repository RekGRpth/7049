.class public Lcom/mediatek/apst/target/service/NotifyService;
.super Landroid/app/IntentService;
.source "NotifyService.java"


# static fields
.field public static final SIM_ID:Ljava/lang/String; = "simid"


# instance fields
.field private mSim1OK:Ljava/lang/Boolean;

.field private mSim2OK:Ljava/lang/Boolean;

.field private mSimOK:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "NotifyService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method private onSimStatusChange(Landroid/content/Intent;)V
    .locals 21
    .param p1    # Landroid/content/Intent;

    const-string v18, "Action"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    if-eqz v17, :cond_0

    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput-object p1, v18, v19

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "strAction:"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/mediatek/apst/target/util/Debugger;->logI([Ljava/lang/Object;Ljava/lang/String;)V

    :cond_0
    const/4 v6, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v18, "android.intent.action.SIM_SETTING_INFO_CHANGED"

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_1

    const-string v18, "simid"

    const-wide/16 v19, -0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move-wide/from16 v2, v19

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v13

    long-to-int v0, v13

    move/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/mediatek/apst/target/util/Global;->getSimInfoById(I)Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

    move-result-object v15

    if-eqz v15, :cond_1

    invoke-virtual {v15}, Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;->getSlotId()I

    move-result v16

    if-nez v16, :cond_6

    const/4 v4, 0x1

    :cond_1
    :goto_0
    const/16 v18, 0x0

    invoke-static/range {v18 .. v18}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->getSimState(I)I

    move-result v9

    const/16 v18, 0x1

    invoke-static/range {v18 .. v18}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->getSimState(I)I

    move-result v12

    invoke-static {v9}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->isSimAccessible(I)Z

    move-result v8

    invoke-static {v12}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->isSimAccessible(I)Z

    move-result v11

    const/16 v18, 0x0

    invoke-static/range {v18 .. v18}, Lcom/mediatek/apst/target/util/Global;->getSimInfoBySlot(I)Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

    move-result-object v7

    const/16 v18, 0x1

    invoke-static/range {v18 .. v18}, Lcom/mediatek/apst/target/util/Global;->getSimInfoBySlot(I)Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/NotifyService;->mSim1OK:Ljava/lang/Boolean;

    move-object/from16 v18, v0

    if-eqz v18, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/NotifyService;->mSim1OK:Ljava/lang/Boolean;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v18

    move/from16 v0, v18

    if-ne v0, v8, :cond_2

    if-eqz v4, :cond_3

    :cond_2
    new-instance v18, Lcom/mediatek/apst/target/event/Event;

    invoke-direct/range {v18 .. v18}, Lcom/mediatek/apst/target/event/Event;-><init>()V

    const-string v19, "state"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    invoke-virtual/range {v18 .. v20}, Lcom/mediatek/apst/target/event/Event;->put(Ljava/lang/String;Ljava/lang/Object;)Lcom/mediatek/apst/target/event/Event;

    move-result-object v18

    const-string v19, "sim_id"

    const/16 v20, 0x0

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    invoke-virtual/range {v18 .. v20}, Lcom/mediatek/apst/target/event/Event;->put(Ljava/lang/String;Ljava/lang/Object;)Lcom/mediatek/apst/target/event/Event;

    move-result-object v18

    const-string v19, "sim_info"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v7}, Lcom/mediatek/apst/target/event/Event;->put(Ljava/lang/String;Ljava/lang/Object;)Lcom/mediatek/apst/target/event/Event;

    move-result-object v18

    const-string v19, "sim_info_flag"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v20

    invoke-virtual/range {v18 .. v20}, Lcom/mediatek/apst/target/event/Event;->put(Ljava/lang/String;Ljava/lang/Object;)Lcom/mediatek/apst/target/event/Event;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/mediatek/apst/target/event/EventDispatcher;->dispatchSimStateChangedEvent(Lcom/mediatek/apst/target/event/Event;)V

    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/NotifyService;->mSim2OK:Ljava/lang/Boolean;

    move-object/from16 v18, v0

    if-eqz v18, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/NotifyService;->mSim2OK:Ljava/lang/Boolean;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v18

    move/from16 v0, v18

    if-ne v0, v11, :cond_4

    if-eqz v5, :cond_5

    :cond_4
    new-instance v18, Lcom/mediatek/apst/target/event/Event;

    invoke-direct/range {v18 .. v18}, Lcom/mediatek/apst/target/event/Event;-><init>()V

    const-string v19, "state"

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    invoke-virtual/range {v18 .. v20}, Lcom/mediatek/apst/target/event/Event;->put(Ljava/lang/String;Ljava/lang/Object;)Lcom/mediatek/apst/target/event/Event;

    move-result-object v18

    const-string v19, "sim_id"

    const/16 v20, 0x1

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    invoke-virtual/range {v18 .. v20}, Lcom/mediatek/apst/target/event/Event;->put(Ljava/lang/String;Ljava/lang/Object;)Lcom/mediatek/apst/target/event/Event;

    move-result-object v18

    const-string v19, "sim_info"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v10}, Lcom/mediatek/apst/target/event/Event;->put(Ljava/lang/String;Ljava/lang/Object;)Lcom/mediatek/apst/target/event/Event;

    move-result-object v18

    const-string v19, "sim_info_flag"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v20

    invoke-virtual/range {v18 .. v20}, Lcom/mediatek/apst/target/event/Event;->put(Ljava/lang/String;Ljava/lang/Object;)Lcom/mediatek/apst/target/event/Event;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/mediatek/apst/target/event/EventDispatcher;->dispatchSimStateChangedEvent(Lcom/mediatek/apst/target/event/Event;)V

    :cond_5
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/apst/target/service/NotifyService;->mSim1OK:Ljava/lang/Boolean;

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/apst/target/service/NotifyService;->mSim2OK:Ljava/lang/Boolean;

    return-void

    :cond_6
    const/16 v18, 0x1

    move/from16 v0, v18

    move/from16 v1, v16

    if-ne v0, v1, :cond_1

    const/4 v5, 0x1

    goto/16 :goto_0
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Intent;

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v3, [Ljava/lang/Object;

    aput-object p1, v0, v2

    const-string v1, "NotifyService --> onHandleIntent"

    invoke-static {v0, v1}, Lcom/mediatek/apst/target/util/Debugger;->logI([Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    invoke-direct {p0, p1}, Lcom/mediatek/apst/target/service/NotifyService;->onSimStatusChange(Landroid/content/Intent;)V

    :goto_0
    new-array v0, v3, [Ljava/lang/Object;

    aput-object p1, v0, v2

    const-string v1, "NotifyService --> onHandleIntent End"

    invoke-static {v0, v1}, Lcom/mediatek/apst/target/util/Debugger;->logI([Ljava/lang/Object;Ljava/lang/String;)V

    return-void

    :cond_0
    new-array v0, v3, [Ljava/lang/Object;

    aput-object p1, v0, v2

    const-string v1, "intent is null"

    invoke-static {v0, v1}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method
