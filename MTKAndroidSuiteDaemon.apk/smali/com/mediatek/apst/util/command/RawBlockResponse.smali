.class public Lcom/mediatek/apst/util/command/RawBlockResponse;
.super Lcom/mediatek/apst/util/command/ResponseCommand;
.source "RawBlockResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/apst/util/command/RawBlockResponse$Builder;
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x2L


# instance fields
.field private progress:I

.field private raw:[B

.field private total:I


# direct methods
.method public constructor <init>(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/mediatek/apst/util/command/ResponseCommand;-><init>(II)V

    return-void
.end method

.method public static builder(I)Lcom/mediatek/apst/util/command/RawBlockResponse$Builder;
    .locals 1
    .param p0    # I

    new-instance v0, Lcom/mediatek/apst/util/command/RawBlockResponse$Builder;

    invoke-direct {v0, p0}, Lcom/mediatek/apst/util/command/RawBlockResponse$Builder;-><init>(I)V

    return-object v0
.end method

.method public static builder(II)Lcom/mediatek/apst/util/command/RawBlockResponse$Builder;
    .locals 1
    .param p0    # I
    .param p1    # I

    new-instance v0, Lcom/mediatek/apst/util/command/RawBlockResponse$Builder;

    invoke-direct {v0, p0, p1}, Lcom/mediatek/apst/util/command/RawBlockResponse$Builder;-><init>(II)V

    return-object v0
.end method


# virtual methods
.method public getProgress()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/util/command/RawBlockResponse;->progress:I

    return v0
.end method

.method public getRaw()[B
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/RawBlockResponse;->raw:[B

    return-object v0
.end method

.method public getTotal()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/util/command/RawBlockResponse;->total:I

    return v0
.end method

.method public setProgress(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/util/command/RawBlockResponse;->progress:I

    return-void
.end method

.method public setRaw([B)V
    .locals 1
    .param p1    # [B

    if-nez p1, :cond_0

    const/4 v0, 0x4

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/mediatek/apst/util/command/RawBlockResponse;->raw:[B

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/mediatek/apst/util/command/RawBlockResponse;->raw:[B

    goto :goto_0
.end method

.method public setTotal(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/util/command/RawBlockResponse;->total:I

    return-void
.end method
