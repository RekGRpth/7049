.class public Lcom/mediatek/apst/util/command/backup/GetAllBookmarkForBackupRsp;
.super Lcom/mediatek/apst/util/command/RawBlockResponse;
.source "GetAllBookmarkForBackupRsp.java"


# static fields
.field private static final serialVersionUID:J = 0x2L


# instance fields
.field private mBookmarkDataList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/apst/util/entity/bookmark/BookmarkData;",
            ">;"
        }
    .end annotation
.end field

.field private mBookmarkFolderList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/apst/util/entity/bookmark/BookmarkFolder;",
            ">;"
        }
    .end annotation
.end field

.field private mTest:Ljava/lang/String;


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1    # I

    const/high16 v0, 0x1110000

    invoke-direct {p0, v0, p1}, Lcom/mediatek/apst/util/command/RawBlockResponse;-><init>(II)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/apst/util/command/backup/GetAllBookmarkForBackupRsp;->mBookmarkDataList:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public getGetAllBookmarkInfoResults()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/backup/GetAllBookmarkForBackupRsp;->mTest:Ljava/lang/String;

    return-object v0
.end method

.method public getmBookmarkDataList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/apst/util/entity/bookmark/BookmarkData;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/apst/util/command/backup/GetAllBookmarkForBackupRsp;->mBookmarkDataList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getmBookmarkFolderList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/apst/util/entity/bookmark/BookmarkFolder;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/apst/util/command/backup/GetAllBookmarkForBackupRsp;->mBookmarkFolderList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public setGetAllBookmarkInfoResults(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/command/backup/GetAllBookmarkForBackupRsp;->mTest:Ljava/lang/String;

    return-void
.end method

.method public setmBookmarkDataList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/apst/util/entity/bookmark/BookmarkData;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/mediatek/apst/util/command/backup/GetAllBookmarkForBackupRsp;->mBookmarkDataList:Ljava/util/ArrayList;

    return-void
.end method

.method public setmBookmarkFolderList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/apst/util/entity/bookmark/BookmarkFolder;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/mediatek/apst/util/command/backup/GetAllBookmarkForBackupRsp;->mBookmarkFolderList:Ljava/util/ArrayList;

    return-void
.end method
