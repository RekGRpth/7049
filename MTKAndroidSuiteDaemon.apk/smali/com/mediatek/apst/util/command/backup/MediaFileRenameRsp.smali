.class public Lcom/mediatek/apst/util/command/backup/MediaFileRenameRsp;
.super Lcom/mediatek/apst/util/command/ResponseCommand;
.source "MediaFileRenameRsp.java"


# static fields
.field private static final serialVersionUID:J = 0x2L


# instance fields
.field private results:[Z


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1    # I

    const/high16 v0, 0x1110000

    invoke-direct {p0, v0, p1}, Lcom/mediatek/apst/util/command/ResponseCommand;-><init>(II)V

    return-void
.end method


# virtual methods
.method public getResults()[Z
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/backup/MediaFileRenameRsp;->results:[Z

    return-object v0
.end method

.method public setResults([Z)V
    .locals 0
    .param p1    # [Z

    iput-object p1, p0, Lcom/mediatek/apst/util/command/backup/MediaFileRenameRsp;->results:[Z

    return-void
.end method
