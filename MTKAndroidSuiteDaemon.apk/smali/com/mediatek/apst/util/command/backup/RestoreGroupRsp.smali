.class public Lcom/mediatek/apst/util/command/backup/RestoreGroupRsp;
.super Lcom/mediatek/apst/util/command/ResponseCommand;
.source "RestoreGroupRsp.java"


# static fields
.field private static final serialVersionUID:J = 0x2L


# instance fields
.field private mGroupCount:I


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1    # I

    const/high16 v0, 0x1110000

    invoke-direct {p0, v0, p1}, Lcom/mediatek/apst/util/command/ResponseCommand;-><init>(II)V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/util/command/backup/RestoreGroupRsp;->mGroupCount:I

    return v0
.end method

.method public setCount(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/util/command/backup/RestoreGroupRsp;->mGroupCount:I

    return-void
.end method
