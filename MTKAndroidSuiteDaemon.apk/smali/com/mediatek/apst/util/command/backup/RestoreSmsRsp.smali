.class public Lcom/mediatek/apst/util/command/backup/RestoreSmsRsp;
.super Lcom/mediatek/apst/util/command/ResponseCommand;
.source "RestoreSmsRsp.java"


# static fields
.field private static final serialVersionUID:J = 0x2L


# instance fields
.field private insertedIds:[J

.field private threadIds:[J


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1    # I

    const/high16 v0, 0x1110000

    invoke-direct {p0, v0, p1}, Lcom/mediatek/apst/util/command/ResponseCommand;-><init>(II)V

    return-void
.end method


# virtual methods
.method public getInsertedIds()[J
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/backup/RestoreSmsRsp;->insertedIds:[J

    return-object v0
.end method

.method public getThreadIds()[J
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/backup/RestoreSmsRsp;->threadIds:[J

    return-object v0
.end method

.method public setInsertedIds([J)V
    .locals 0
    .param p1    # [J

    iput-object p1, p0, Lcom/mediatek/apst/util/command/backup/RestoreSmsRsp;->insertedIds:[J

    return-void
.end method

.method public setThreadIds([J)V
    .locals 0
    .param p1    # [J

    iput-object p1, p0, Lcom/mediatek/apst/util/command/backup/RestoreSmsRsp;->threadIds:[J

    return-void
.end method
