.class public Lcom/mediatek/apst/util/command/calendar/UpdateEventReq;
.super Lcom/mediatek/apst/util/command/RequestCommand;
.source "UpdateEventReq.java"


# static fields
.field private static final serialVersionUID:J = 0x2L


# instance fields
.field private newOne:Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;

.field private updateId:J


# direct methods
.method public constructor <init>()V
    .locals 1

    const/high16 v0, 0x1000000

    invoke-direct {p0, v0}, Lcom/mediatek/apst/util/command/RequestCommand;-><init>(I)V

    return-void
.end method


# virtual methods
.method public getNewOne()Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/calendar/UpdateEventReq;->newOne:Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;

    return-object v0
.end method

.method public getUpdateId()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/command/calendar/UpdateEventReq;->updateId:J

    return-wide v0
.end method

.method public setNewOne(Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;)V
    .locals 0
    .param p1    # Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;

    iput-object p1, p0, Lcom/mediatek/apst/util/command/calendar/UpdateEventReq;->newOne:Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;

    return-void
.end method

.method public setUpdateId(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/command/calendar/UpdateEventReq;->updateId:J

    return-void
.end method
