.class public Lcom/mediatek/apst/util/command/contacts/AddGroupMembershipReq;
.super Lcom/mediatek/apst/util/command/RequestCommand;
.source "AddGroupMembershipReq.java"


# static fields
.field private static final serialVersionUID:J = 0x2L


# instance fields
.field private contactIds:[J

.field private groupEntry:Lcom/mediatek/apst/util/entity/contacts/Group;

.field private groupId:J

.field private mSimIndexes:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/mediatek/apst/util/command/RequestCommand;-><init>(I)V

    return-void
.end method


# virtual methods
.method public getContactIds()[J
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/contacts/AddGroupMembershipReq;->contactIds:[J

    return-object v0
.end method

.method public getGroupEntry()Lcom/mediatek/apst/util/entity/contacts/Group;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/contacts/AddGroupMembershipReq;->groupEntry:Lcom/mediatek/apst/util/entity/contacts/Group;

    return-object v0
.end method

.method public getGroupId()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/command/contacts/AddGroupMembershipReq;->groupId:J

    return-wide v0
.end method

.method public getSimIndexes()[I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/contacts/AddGroupMembershipReq;->mSimIndexes:[I

    return-object v0
.end method

.method public setContactIds([J)V
    .locals 0
    .param p1    # [J

    iput-object p1, p0, Lcom/mediatek/apst/util/command/contacts/AddGroupMembershipReq;->contactIds:[J

    return-void
.end method

.method public setGroupEntry(Lcom/mediatek/apst/util/entity/contacts/Group;)V
    .locals 0
    .param p1    # Lcom/mediatek/apst/util/entity/contacts/Group;

    iput-object p1, p0, Lcom/mediatek/apst/util/command/contacts/AddGroupMembershipReq;->groupEntry:Lcom/mediatek/apst/util/entity/contacts/Group;

    return-void
.end method

.method public setGroupId(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/command/contacts/AddGroupMembershipReq;->groupId:J

    return-void
.end method

.method public setSimIndexes([I)V
    .locals 0
    .param p1    # [I

    iput-object p1, p0, Lcom/mediatek/apst/util/command/contacts/AddGroupMembershipReq;->mSimIndexes:[I

    return-void
.end method
