.class public Lcom/mediatek/apst/util/command/contacts/AddGroupRsp;
.super Lcom/mediatek/apst/util/command/ResponseCommand;
.source "AddGroupRsp.java"


# static fields
.field private static final serialVersionUID:J = 0x2L


# instance fields
.field private insertedId:J

.field private result:Lcom/mediatek/apst/util/entity/contacts/Group;


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1    # I

    const/16 v0, 0x10

    invoke-direct {p0, v0, p1}, Lcom/mediatek/apst/util/command/ResponseCommand;-><init>(II)V

    return-void
.end method


# virtual methods
.method public getInsertedId()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/command/contacts/AddGroupRsp;->insertedId:J

    return-wide v0
.end method

.method public getResult()Lcom/mediatek/apst/util/entity/contacts/Group;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/contacts/AddGroupRsp;->result:Lcom/mediatek/apst/util/entity/contacts/Group;

    return-object v0
.end method

.method public setInsertedId(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/command/contacts/AddGroupRsp;->insertedId:J

    return-void
.end method

.method public setResult(Lcom/mediatek/apst/util/entity/contacts/Group;)V
    .locals 0
    .param p1    # Lcom/mediatek/apst/util/entity/contacts/Group;

    iput-object p1, p0, Lcom/mediatek/apst/util/command/contacts/AddGroupRsp;->result:Lcom/mediatek/apst/util/entity/contacts/Group;

    return-void
.end method
