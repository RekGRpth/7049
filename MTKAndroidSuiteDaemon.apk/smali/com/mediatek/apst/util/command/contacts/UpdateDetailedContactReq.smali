.class public Lcom/mediatek/apst/util/command/contacts/UpdateDetailedContactReq;
.super Lcom/mediatek/apst/util/command/RequestCommand;
.source "UpdateDetailedContactReq.java"


# static fields
.field private static final serialVersionUID:J = 0x2L


# instance fields
.field private newOne:Lcom/mediatek/apst/util/entity/contacts/RawContact;

.field private simEmail:Ljava/lang/String;

.field private simName:Ljava/lang/String;

.field private simNumber:Ljava/lang/String;

.field private sourceLocation:I

.field private updateId:J


# direct methods
.method public constructor <init>()V
    .locals 1

    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/mediatek/apst/util/command/RequestCommand;-><init>(I)V

    return-void
.end method


# virtual methods
.method public getNewOne()Lcom/mediatek/apst/util/entity/contacts/RawContact;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/contacts/UpdateDetailedContactReq;->newOne:Lcom/mediatek/apst/util/entity/contacts/RawContact;

    return-object v0
.end method

.method public getSimEmail()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/contacts/UpdateDetailedContactReq;->simEmail:Ljava/lang/String;

    return-object v0
.end method

.method public getSimName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/contacts/UpdateDetailedContactReq;->simName:Ljava/lang/String;

    return-object v0
.end method

.method public getSimNumber()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/contacts/UpdateDetailedContactReq;->simNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getSourceLocation()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/util/command/contacts/UpdateDetailedContactReq;->sourceLocation:I

    return v0
.end method

.method public getUpdateId()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/command/contacts/UpdateDetailedContactReq;->updateId:J

    return-wide v0
.end method

.method public setNewOne(Lcom/mediatek/apst/util/entity/contacts/RawContact;)V
    .locals 0
    .param p1    # Lcom/mediatek/apst/util/entity/contacts/RawContact;

    iput-object p1, p0, Lcom/mediatek/apst/util/command/contacts/UpdateDetailedContactReq;->newOne:Lcom/mediatek/apst/util/entity/contacts/RawContact;

    return-void
.end method

.method public setSimEmail(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/command/contacts/UpdateDetailedContactReq;->simEmail:Ljava/lang/String;

    return-void
.end method

.method public setSimName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/command/contacts/UpdateDetailedContactReq;->simName:Ljava/lang/String;

    return-void
.end method

.method public setSimNumber(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/command/contacts/UpdateDetailedContactReq;->simNumber:Ljava/lang/String;

    return-void
.end method

.method public setSourceLocation(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/util/command/contacts/UpdateDetailedContactReq;->sourceLocation:I

    return-void
.end method

.method public setUpdateId(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/command/contacts/UpdateDetailedContactReq;->updateId:J

    return-void
.end method
