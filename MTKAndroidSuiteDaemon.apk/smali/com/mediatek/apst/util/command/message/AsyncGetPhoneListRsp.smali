.class public Lcom/mediatek/apst/util/command/message/AsyncGetPhoneListRsp;
.super Lcom/mediatek/apst/util/command/RawBlockResponse;
.source "AsyncGetPhoneListRsp.java"


# static fields
.field private static final serialVersionUID:J = 0x2L


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1    # I

    const/16 v0, 0x100

    invoke-direct {p0, v0, p1}, Lcom/mediatek/apst/util/command/RawBlockResponse;-><init>(II)V

    return-void
.end method


# virtual methods
.method public getAll()Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/apst/util/entity/message/TargetAddress;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/mediatek/apst/util/command/RawBlockResponse;->getRaw()[B

    move-result-object v5

    invoke-static {v5}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v1}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v1, :cond_0

    return-object v4

    :cond_0
    new-instance v3, Lcom/mediatek/apst/util/entity/message/TargetAddress;

    invoke-direct {v3}, Lcom/mediatek/apst/util/entity/message/TargetAddress;-><init>()V

    invoke-virtual {v3, v0}, Lcom/mediatek/apst/util/entity/message/TargetAddress;->readRaw(Ljava/nio/ByteBuffer;)V

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
