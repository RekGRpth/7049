.class public Lcom/mediatek/apst/util/command/message/DeleteMessageReq;
.super Lcom/mediatek/apst/util/command/RequestCommand;
.source "DeleteMessageReq.java"


# static fields
.field private static final serialVersionUID:J = 0x2L


# instance fields
.field private deleteMmsDates:[J

.field private deleteMmsIds:[J

.field private deleteSmsDates:[J

.field private deleteSmsIds:[J


# direct methods
.method public constructor <init>()V
    .locals 1

    const/16 v0, 0x100

    invoke-direct {p0, v0}, Lcom/mediatek/apst/util/command/RequestCommand;-><init>(I)V

    return-void
.end method


# virtual methods
.method public getDeleteMmsDates()[J
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/message/DeleteMessageReq;->deleteMmsDates:[J

    return-object v0
.end method

.method public getDeleteMmsIds()[J
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/message/DeleteMessageReq;->deleteMmsIds:[J

    return-object v0
.end method

.method public getDeleteSmsDates()[J
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/message/DeleteMessageReq;->deleteSmsDates:[J

    return-object v0
.end method

.method public getDeleteSmsIds()[J
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/message/DeleteMessageReq;->deleteSmsIds:[J

    return-object v0
.end method

.method public setDeleteMmsDates([J)V
    .locals 0
    .param p1    # [J

    iput-object p1, p0, Lcom/mediatek/apst/util/command/message/DeleteMessageReq;->deleteMmsDates:[J

    return-void
.end method

.method public setDeleteMmsIds([J)V
    .locals 0
    .param p1    # [J

    iput-object p1, p0, Lcom/mediatek/apst/util/command/message/DeleteMessageReq;->deleteMmsIds:[J

    return-void
.end method

.method public setDeleteSmsDates([J)V
    .locals 0
    .param p1    # [J

    iput-object p1, p0, Lcom/mediatek/apst/util/command/message/DeleteMessageReq;->deleteSmsDates:[J

    return-void
.end method

.method public setDeleteSmsIds([J)V
    .locals 0
    .param p1    # [J

    iput-object p1, p0, Lcom/mediatek/apst/util/command/message/DeleteMessageReq;->deleteSmsIds:[J

    return-void
.end method
