.class public Lcom/mediatek/apst/util/command/message/MoveMessageToBoxReq;
.super Lcom/mediatek/apst/util/command/RequestCommand;
.source "MoveMessageToBoxReq.java"


# static fields
.field private static final serialVersionUID:J = 0x2L


# instance fields
.field private box:I

.field private updateMmsDates:[J

.field private updateMmsIds:[J

.field private updateSmsDates:[J

.field private updateSmsIds:[J


# direct methods
.method public constructor <init>()V
    .locals 1

    const/16 v0, 0x100

    invoke-direct {p0, v0}, Lcom/mediatek/apst/util/command/RequestCommand;-><init>(I)V

    return-void
.end method


# virtual methods
.method public getBox()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/util/command/message/MoveMessageToBoxReq;->box:I

    return v0
.end method

.method public getUpdateMmsDates()[J
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/message/MoveMessageToBoxReq;->updateMmsDates:[J

    return-object v0
.end method

.method public getUpdateMmsIds()[J
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/message/MoveMessageToBoxReq;->updateMmsIds:[J

    return-object v0
.end method

.method public getUpdateSmsDates()[J
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/message/MoveMessageToBoxReq;->updateSmsDates:[J

    return-object v0
.end method

.method public getUpdateSmsIds()[J
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/message/MoveMessageToBoxReq;->updateSmsIds:[J

    return-object v0
.end method

.method public setBox(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/util/command/message/MoveMessageToBoxReq;->box:I

    return-void
.end method

.method public setUpdateMmsDates([J)V
    .locals 0
    .param p1    # [J

    iput-object p1, p0, Lcom/mediatek/apst/util/command/message/MoveMessageToBoxReq;->updateMmsDates:[J

    return-void
.end method

.method public setUpdateMmsIds([J)V
    .locals 0
    .param p1    # [J

    iput-object p1, p0, Lcom/mediatek/apst/util/command/message/MoveMessageToBoxReq;->updateMmsIds:[J

    return-void
.end method

.method public setUpdateSmsDates([J)V
    .locals 0
    .param p1    # [J

    iput-object p1, p0, Lcom/mediatek/apst/util/command/message/MoveMessageToBoxReq;->updateSmsDates:[J

    return-void
.end method

.method public setUpdateSmsIds([J)V
    .locals 0
    .param p1    # [J

    iput-object p1, p0, Lcom/mediatek/apst/util/command/message/MoveMessageToBoxReq;->updateSmsIds:[J

    return-void
.end method
