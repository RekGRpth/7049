.class public Lcom/mediatek/apst/util/command/message/SendSmsRsp;
.super Lcom/mediatek/apst/util/command/ResponseCommand;
.source "SendSmsRsp.java"


# static fields
.field private static final serialVersionUID:J = 0x2L


# instance fields
.field private dates:[J

.field private insertedIds:[J

.field private simId:I

.field private threadId:J


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1    # I

    const/16 v0, 0x100

    invoke-direct {p0, v0, p1}, Lcom/mediatek/apst/util/command/ResponseCommand;-><init>(II)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/apst/util/command/message/SendSmsRsp;->simId:I

    return-void
.end method


# virtual methods
.method public getDates()[J
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/message/SendSmsRsp;->dates:[J

    return-object v0
.end method

.method public getInsertedIds()[J
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/message/SendSmsRsp;->insertedIds:[J

    return-object v0
.end method

.method public getSimId()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/util/command/message/SendSmsRsp;->simId:I

    return v0
.end method

.method public getThreadId()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/command/message/SendSmsRsp;->threadId:J

    return-wide v0
.end method

.method public setDates([J)V
    .locals 0
    .param p1    # [J

    iput-object p1, p0, Lcom/mediatek/apst/util/command/message/SendSmsRsp;->dates:[J

    return-void
.end method

.method public setInsertedIds([J)V
    .locals 0
    .param p1    # [J

    iput-object p1, p0, Lcom/mediatek/apst/util/command/message/SendSmsRsp;->insertedIds:[J

    return-void
.end method

.method public setSimId(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/util/command/message/SendSmsRsp;->simId:I

    return-void
.end method

.method public setThreadId(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/command/message/SendSmsRsp;->threadId:J

    return-void
.end method
