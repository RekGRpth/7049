.class public Lcom/mediatek/apst/util/command/sync/CalendarFastSyncGetRemindersReq;
.super Lcom/mediatek/apst/util/command/RequestCommand;
.source "CalendarFastSyncGetRemindersReq.java"


# static fields
.field private static final serialVersionUID:J = 0x2L


# instance fields
.field private requestedEventIds:[J


# direct methods
.method public constructor <init>()V
    .locals 1

    const v0, 0x11000

    invoke-direct {p0, v0}, Lcom/mediatek/apst/util/command/RequestCommand;-><init>(I)V

    return-void
.end method


# virtual methods
.method public getRequestedEventIds()[J
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/sync/CalendarFastSyncGetRemindersReq;->requestedEventIds:[J

    return-object v0
.end method

.method public setRequestedEventIds([J)V
    .locals 0
    .param p1    # [J

    iput-object p1, p0, Lcom/mediatek/apst/util/command/sync/CalendarFastSyncGetRemindersReq;->requestedEventIds:[J

    return-void
.end method
