.class public Lcom/mediatek/apst/util/command/sync/CalendarSyncStartRsp;
.super Lcom/mediatek/apst/util/command/ResponseCommand;
.source "CalendarSyncStartRsp.java"


# static fields
.field private static final serialVersionUID:J = 0x2L


# instance fields
.field private isSyncAble:Z

.field private lastSyncDate:J

.field private localAccountId:J

.field private syncNeedReinit:Z


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1    # I

    const v0, 0x11000

    invoke-direct {p0, v0, p1}, Lcom/mediatek/apst/util/command/ResponseCommand;-><init>(II)V

    return-void
.end method


# virtual methods
.method public getLastSyncDate()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/command/sync/CalendarSyncStartRsp;->lastSyncDate:J

    return-wide v0
.end method

.method public getLocalAccountId()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/command/sync/CalendarSyncStartRsp;->localAccountId:J

    return-wide v0
.end method

.method public isSyncAble()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/apst/util/command/sync/CalendarSyncStartRsp;->isSyncAble:Z

    return v0
.end method

.method public isSyncNeedReinit()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/apst/util/command/sync/CalendarSyncStartRsp;->syncNeedReinit:Z

    return v0
.end method

.method public setLastSyncDate(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/command/sync/CalendarSyncStartRsp;->lastSyncDate:J

    return-void
.end method

.method public setLocalAccountId(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/command/sync/CalendarSyncStartRsp;->localAccountId:J

    return-void
.end method

.method public setSyncAble(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/apst/util/command/sync/CalendarSyncStartRsp;->isSyncAble:Z

    return-void
.end method

.method public setSyncNeedReinit(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/apst/util/command/sync/CalendarSyncStartRsp;->syncNeedReinit:Z

    return-void
.end method
