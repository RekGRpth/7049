.class public Lcom/mediatek/apst/util/command/sync/ContactsSlowSyncGetAllContactDataRsp;
.super Lcom/mediatek/apst/util/command/RawBlockResponse;
.source "ContactsSlowSyncGetAllContactDataRsp.java"


# static fields
.field private static final serialVersionUID:J = 0x2L


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1    # I

    const/high16 v0, 0x10000

    invoke-direct {p0, v0, p1}, Lcom/mediatek/apst/util/command/RawBlockResponse;-><init>(II)V

    return-void
.end method


# virtual methods
.method public getAll(I)[Lcom/mediatek/apst/util/entity/contacts/ContactData;
    .locals 5
    .param p1    # I

    invoke-virtual {p0}, Lcom/mediatek/apst/util/command/RawBlockResponse;->getRaw()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    new-array v3, v1, [Lcom/mediatek/apst/util/entity/contacts/ContactData;

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v1, :cond_0

    return-object v3

    :cond_0
    invoke-static {v0, p1}, Lcom/mediatek/apst/util/entity/contacts/ContactDataAdapter;->readRaw(Ljava/nio/ByteBuffer;I)Lcom/mediatek/apst/util/entity/contacts/ContactData;

    move-result-object v4

    aput-object v4, v3, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
