.class public Lcom/mediatek/apst/util/command/sysinfo/GetStorageRsp;
.super Lcom/mediatek/apst/util/command/ResponseCommand;
.source "GetStorageRsp.java"


# static fields
.field private static final serialVersionUID:J = 0x2L


# instance fields
.field private internalAvailableSpace:J

.field private internalTotalSpace:J

.field private sdCardAvailableSpace:J

.field private sdCardPath:Ljava/lang/String;

.field private sdCardTotalSpace:J

.field private sdMounted:Z

.field private sdWriteable:Z


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lcom/mediatek/apst/util/command/ResponseCommand;-><init>(II)V

    return-void
.end method


# virtual methods
.method public getInternalAvailableSpace()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/command/sysinfo/GetStorageRsp;->internalAvailableSpace:J

    return-wide v0
.end method

.method public getInternalTotalSpace()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/command/sysinfo/GetStorageRsp;->internalTotalSpace:J

    return-wide v0
.end method

.method public getSdCardAvailableSpace()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/command/sysinfo/GetStorageRsp;->sdCardAvailableSpace:J

    return-wide v0
.end method

.method public getSdCardPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/sysinfo/GetStorageRsp;->sdCardPath:Ljava/lang/String;

    return-object v0
.end method

.method public getSdCardTotalSpace()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/command/sysinfo/GetStorageRsp;->sdCardTotalSpace:J

    return-wide v0
.end method

.method public isSdMounted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/apst/util/command/sysinfo/GetStorageRsp;->sdMounted:Z

    return v0
.end method

.method public isSdWriteable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/apst/util/command/sysinfo/GetStorageRsp;->sdWriteable:Z

    return v0
.end method

.method public setInternalAvailableSpace(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/command/sysinfo/GetStorageRsp;->internalAvailableSpace:J

    return-void
.end method

.method public setInternalTotalSpace(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/command/sysinfo/GetStorageRsp;->internalTotalSpace:J

    return-void
.end method

.method public setSdCardAvailableSpace(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/command/sysinfo/GetStorageRsp;->sdCardAvailableSpace:J

    return-void
.end method

.method public setSdCardPath(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/command/sysinfo/GetStorageRsp;->sdCardPath:Ljava/lang/String;

    return-void
.end method

.method public setSdCardTotalSpace(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/command/sysinfo/GetStorageRsp;->sdCardTotalSpace:J

    return-void
.end method

.method public setSdMounted(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/apst/util/command/sysinfo/GetStorageRsp;->sdMounted:Z

    return-void
.end method

.method public setSdWriteable(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/apst/util/command/sysinfo/GetStorageRsp;->sdWriteable:Z

    return-void
.end method
