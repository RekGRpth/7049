.class public Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;
.super Lcom/mediatek/apst/util/command/ResponseCommand;
.source "GetSysInfoRsp.java"


# static fields
.field private static final serialVersionUID:J = 0x2L


# instance fields
.field private SDCardAndEmmcState:[Z

.field private applicationsCount:I

.field private batteryLevel:I

.field private batteryScale:I

.field private contactsCount:I

.field private device:Ljava/lang/String;

.field private firmwareVersion:Ljava/lang/String;

.field private gemini:Z

.field private internalAvailableSpace:J

.field private internalTotalSpace:J

.field private mFeatureOptionList:[I

.field private manufacturer:Ljava/lang/String;

.field private messagesCount:I

.field private model:Ljava/lang/String;

.field private sdCardAvailableSpace:J

.field private sdCardPath:Ljava/lang/String;

.field private sdCardTotalSpace:J

.field private sdMounted:Z

.field private sdWriteable:Z

.field private sim1Accessible:Z

.field private sim1Info:Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

.field private sim2Accessible:Z

.field private sim2Info:Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

.field private simAccessible:Z

.field private simContactsCount:I

.field private simInfo:Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

.field private simInfoList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lcom/mediatek/apst/util/command/ResponseCommand;-><init>(II)V

    return-void
.end method


# virtual methods
.method public getApplicationsCount()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->applicationsCount:I

    return v0
.end method

.method public getBatteryLevel()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->batteryLevel:I

    return v0
.end method

.method public getBatteryScale()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->batteryScale:I

    return v0
.end method

.method public getContactsCount()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->contactsCount:I

    return v0
.end method

.method public getDevice()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->device:Ljava/lang/String;

    return-object v0
.end method

.method public getFeatureOptionList()[I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->mFeatureOptionList:[I

    return-object v0
.end method

.method public getFirmwareVersion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->firmwareVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getInternalAvailableSpace()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->internalAvailableSpace:J

    return-wide v0
.end method

.method public getInternalTotalSpace()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->internalTotalSpace:J

    return-wide v0
.end method

.method public getManufacturer()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->manufacturer:Ljava/lang/String;

    return-object v0
.end method

.method public getMessagesCount()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->messagesCount:I

    return v0
.end method

.method public getModel()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->model:Ljava/lang/String;

    return-object v0
.end method

.method public getSDCardAndEmmcState()[Z
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->SDCardAndEmmcState:[Z

    return-object v0
.end method

.method public getSdCardAvailableSpace()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->sdCardAvailableSpace:J

    return-wide v0
.end method

.method public getSdCardPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->sdCardPath:Ljava/lang/String;

    return-object v0
.end method

.method public getSdCardTotalSpace()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->sdCardTotalSpace:J

    return-wide v0
.end method

.method public getSim1Info()Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->sim1Info:Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

    return-object v0
.end method

.method public getSim2Info()Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->sim2Info:Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

    return-object v0
.end method

.method public getSimContactsCount()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->simContactsCount:I

    return v0
.end method

.method public getSimInfo()Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->simInfo:Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

    return-object v0
.end method

.method public getSimInfoList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->simInfoList:Ljava/util/List;

    return-object v0
.end method

.method public isGemini()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->gemini:Z

    return v0
.end method

.method public isSdMounted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->sdMounted:Z

    return v0
.end method

.method public isSdWriteable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->sdWriteable:Z

    return v0
.end method

.method public isSim1Accessible()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->sim1Accessible:Z

    return v0
.end method

.method public isSim2Accessible()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->sim2Accessible:Z

    return v0
.end method

.method public isSimAccessible()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->simAccessible:Z

    return v0
.end method

.method public setApplicationsCount(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->applicationsCount:I

    return-void
.end method

.method public setBatteryLevel(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->batteryLevel:I

    return-void
.end method

.method public setBatteryScale(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->batteryScale:I

    return-void
.end method

.method public setContactsCount(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->contactsCount:I

    return-void
.end method

.method public setDevice(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->device:Ljava/lang/String;

    return-void
.end method

.method public setFeatureOptionList([I)V
    .locals 0
    .param p1    # [I

    iput-object p1, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->mFeatureOptionList:[I

    return-void
.end method

.method public setFirmwareVersion(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->firmwareVersion:Ljava/lang/String;

    return-void
.end method

.method public setGemini(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->gemini:Z

    return-void
.end method

.method public setInternalAvailableSpace(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->internalAvailableSpace:J

    return-void
.end method

.method public setInternalTotalSpace(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->internalTotalSpace:J

    return-void
.end method

.method public setManufacturer(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->manufacturer:Ljava/lang/String;

    return-void
.end method

.method public setMessagesCount(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->messagesCount:I

    return-void
.end method

.method public setModel(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->model:Ljava/lang/String;

    return-void
.end method

.method public setSDCardAndEmmcState([Z)V
    .locals 0
    .param p1    # [Z

    iput-object p1, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->SDCardAndEmmcState:[Z

    return-void
.end method

.method public setSdCardAvailableSpace(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->sdCardAvailableSpace:J

    return-void
.end method

.method public setSdCardPath(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->sdCardPath:Ljava/lang/String;

    return-void
.end method

.method public setSdCardTotalSpace(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->sdCardTotalSpace:J

    return-void
.end method

.method public setSdMounted(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->sdMounted:Z

    return-void
.end method

.method public setSdWriteable(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->sdWriteable:Z

    return-void
.end method

.method public setSim1Accessible(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->sim1Accessible:Z

    return-void
.end method

.method public setSim1Info(Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;)V
    .locals 0
    .param p1    # Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

    iput-object p1, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->sim1Info:Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

    return-void
.end method

.method public setSim2Accessible(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->sim2Accessible:Z

    return-void
.end method

.method public setSim2Info(Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;)V
    .locals 0
    .param p1    # Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

    iput-object p1, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->sim2Info:Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

    return-void
.end method

.method public setSimAccessible(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->simAccessible:Z

    return-void
.end method

.method public setSimContactsCount(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->simContactsCount:I

    return-void
.end method

.method public setSimInfo(Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;)V
    .locals 0
    .param p1    # Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

    iput-object p1, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->simInfo:Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

    return-void
.end method

.method public setSimInfoList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->simInfoList:Ljava/util/List;

    return-void
.end method
