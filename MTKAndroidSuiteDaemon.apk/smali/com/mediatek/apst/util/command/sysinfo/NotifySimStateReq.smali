.class public Lcom/mediatek/apst/util/command/sysinfo/NotifySimStateReq;
.super Lcom/mediatek/apst/util/command/RequestCommand;
.source "NotifySimStateReq.java"


# static fields
.field private static final serialVersionUID:J = 0x2L


# instance fields
.field private bInfoChanged:Z

.field private contactsCount:I

.field private sim1Accessible:Z

.field private sim1DetailInfo:Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

.field private sim2Accessible:Z

.field private sim2DetailInfo:Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

.field private simAccessible:Z

.field private simDetailInfo:Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/mediatek/apst/util/command/RequestCommand;-><init>(I)V

    return-void
.end method


# virtual methods
.method public getContactsCount()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/util/command/sysinfo/NotifySimStateReq;->contactsCount:I

    return v0
.end method

.method public getSim1DetailInfo()Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/sysinfo/NotifySimStateReq;->sim1DetailInfo:Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

    return-object v0
.end method

.method public getSim2DetailInfo()Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/sysinfo/NotifySimStateReq;->sim2DetailInfo:Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

    return-object v0
.end method

.method public getSimDetailInfo()Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/sysinfo/NotifySimStateReq;->simDetailInfo:Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

    return-object v0
.end method

.method public isInfoChanged()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/apst/util/command/sysinfo/NotifySimStateReq;->bInfoChanged:Z

    return v0
.end method

.method public isSim1Accessible()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/apst/util/command/sysinfo/NotifySimStateReq;->sim1Accessible:Z

    return v0
.end method

.method public isSim2Accessible()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/apst/util/command/sysinfo/NotifySimStateReq;->sim2Accessible:Z

    return v0
.end method

.method public isSimAccessible()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/apst/util/command/sysinfo/NotifySimStateReq;->simAccessible:Z

    return v0
.end method

.method public setContactsCount(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/util/command/sysinfo/NotifySimStateReq;->contactsCount:I

    return-void
.end method

.method public setInfoChanged(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/apst/util/command/sysinfo/NotifySimStateReq;->bInfoChanged:Z

    return-void
.end method

.method public setSim1Accessible(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/apst/util/command/sysinfo/NotifySimStateReq;->sim1Accessible:Z

    return-void
.end method

.method public setSim1DetailInfo(Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;)V
    .locals 0
    .param p1    # Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

    iput-object p1, p0, Lcom/mediatek/apst/util/command/sysinfo/NotifySimStateReq;->sim1DetailInfo:Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

    return-void
.end method

.method public setSim2Accessible(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/apst/util/command/sysinfo/NotifySimStateReq;->sim2Accessible:Z

    return-void
.end method

.method public setSim2DetailInfo(Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;)V
    .locals 0
    .param p1    # Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

    iput-object p1, p0, Lcom/mediatek/apst/util/command/sysinfo/NotifySimStateReq;->sim2DetailInfo:Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

    return-void
.end method

.method public setSimAccessible(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/apst/util/command/sysinfo/NotifySimStateReq;->simAccessible:Z

    return-void
.end method

.method public setSimDetailInfo(Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;)V
    .locals 0
    .param p1    # Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

    iput-object p1, p0, Lcom/mediatek/apst/util/command/sysinfo/NotifySimStateReq;->simDetailInfo:Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

    return-void
.end method
