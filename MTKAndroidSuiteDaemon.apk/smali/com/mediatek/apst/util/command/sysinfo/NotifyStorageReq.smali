.class public Lcom/mediatek/apst/util/command/sysinfo/NotifyStorageReq;
.super Lcom/mediatek/apst/util/command/RequestCommand;
.source "NotifyStorageReq.java"


# static fields
.field public static final EXTERNAL:I = 0x2

.field public static final INTERNAL:I = 0x1

.field private static final serialVersionUID:J = 0x2L


# instance fields
.field private availableSpace:J

.field private storageType:I

.field private totalSpace:J


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/mediatek/apst/util/command/RequestCommand;-><init>(I)V

    return-void
.end method


# virtual methods
.method public getAvailableSpace()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/command/sysinfo/NotifyStorageReq;->availableSpace:J

    return-wide v0
.end method

.method public getStorageType()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/util/command/sysinfo/NotifyStorageReq;->storageType:I

    return v0
.end method

.method public getTotalSpace()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/command/sysinfo/NotifyStorageReq;->totalSpace:J

    return-wide v0
.end method

.method public setAvailableSpace(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/command/sysinfo/NotifyStorageReq;->availableSpace:J

    return-void
.end method

.method public setStorageType(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/util/command/sysinfo/NotifyStorageReq;->storageType:I

    return-void
.end method

.method public setTotalSpace(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/command/sysinfo/NotifyStorageReq;->totalSpace:J

    return-void
.end method
