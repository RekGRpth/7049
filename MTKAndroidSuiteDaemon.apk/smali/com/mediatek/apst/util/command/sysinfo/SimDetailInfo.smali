.class public Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;
.super Ljava/lang/Object;
.source "SimDetailInfo.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final SIM_TYPE_SIM:I = 0x2

.field public static final SIM_TYPE_USIM:I = 0x3

.field public static final SLOT_ID_ONE:I = 0x0

.field public static final SLOT_ID_SINGLE:I = 0x0

.field public static final SLOT_ID_TWO:I = 0x1

.field private static final serialVersionUID:J = 0x2L

.field public static final versionCode:I = 0x1


# instance fields
.field private mColor:I

.field private mDisplayName:Ljava/lang/String;

.field private mICCId:Ljava/lang/String;

.field private mNumber:Ljava/lang/String;

.field private mSIMType:I

.field private mSimId:I

.field private mSlotId:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;->mDisplayName:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;->mNumber:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;->mICCId:Ljava/lang/String;

    const/16 v0, -0xff

    iput v0, p0, Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;->mSlotId:I

    return-void
.end method


# virtual methods
.method public getColor()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;->mColor:I

    return v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;->mDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method public getICCId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;->mICCId:Ljava/lang/String;

    return-object v0
.end method

.method public getNumber()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;->mNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getSIMType()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;->mSIMType:I

    return v0
.end method

.method public getSimId()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;->mSimId:I

    return v0
.end method

.method public getSlotId()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;->mSlotId:I

    return v0
.end method

.method public setColor(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;->mColor:I

    return-void
.end method

.method public setDisplayName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;->mDisplayName:Ljava/lang/String;

    return-void
.end method

.method public setICCId(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;->mICCId:Ljava/lang/String;

    return-void
.end method

.method public setNumber(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;->mNumber:Ljava/lang/String;

    return-void
.end method

.method public setSIMType(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;->mSIMType:I

    return-void
.end method

.method public setSimId(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;->mSimId:I

    return-void
.end method

.method public setSlotId(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;->mSlotId:I

    return-void
.end method
