.class public Lcom/mediatek/apst/util/communication/common/PollingThr;
.super Ljava/lang/Object;
.source "PollingThr.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private bRunning:Z

.field private mDispatcher:Lcom/mediatek/apst/util/communication/common/Dispatcher;

.field private mOis:Ljava/io/ObjectInputStream;

.field private mOos:Ljava/io/ObjectOutputStream;

.field private mRole:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/mediatek/apst/util/communication/common/PollingThr;->mOis:Ljava/io/ObjectInputStream;

    iput-object v0, p0, Lcom/mediatek/apst/util/communication/common/PollingThr;->mDispatcher:Lcom/mediatek/apst/util/communication/common/Dispatcher;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/apst/util/communication/common/PollingThr;->bRunning:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/apst/util/communication/common/PollingThr;->mRole:I

    return-void
.end method

.method public constructor <init>(Ljava/io/ObjectInputStream;Ljava/io/ObjectOutputStream;Lcom/mediatek/apst/util/communication/common/Dispatcher;I)V
    .locals 1
    .param p1    # Ljava/io/ObjectInputStream;
    .param p2    # Ljava/io/ObjectOutputStream;
    .param p3    # Lcom/mediatek/apst/util/communication/common/Dispatcher;
    .param p4    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mediatek/apst/util/communication/common/PollingThr;->mOis:Ljava/io/ObjectInputStream;

    iput-object p2, p0, Lcom/mediatek/apst/util/communication/common/PollingThr;->mOos:Ljava/io/ObjectOutputStream;

    iput-object p3, p0, Lcom/mediatek/apst/util/communication/common/PollingThr;->mDispatcher:Lcom/mediatek/apst/util/communication/common/Dispatcher;

    iput p4, p0, Lcom/mediatek/apst/util/communication/common/PollingThr;->mRole:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/apst/util/communication/common/PollingThr;->bRunning:Z

    return-void
.end method


# virtual methods
.method public isRunning()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/apst/util/communication/common/PollingThr;->bRunning:Z

    return v0
.end method

.method public run()V
    .locals 7

    const/4 v4, 0x1

    const/4 v6, 0x0

    iget-object v3, p0, Lcom/mediatek/apst/util/communication/common/PollingThr;->mDispatcher:Lcom/mediatek/apst/util/communication/common/Dispatcher;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/apst/util/communication/common/PollingThr;->mOis:Ljava/io/ObjectInputStream;

    if-nez v3, :cond_2

    :cond_0
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "[PollingThr]The dispatcher or stream object is null!"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iput-boolean v4, p0, Lcom/mediatek/apst/util/communication/common/PollingThr;->bRunning:Z

    :goto_1
    iget-boolean v3, p0, Lcom/mediatek/apst/util/communication/common/PollingThr;->bRunning:Z

    if-eqz v3, :cond_1

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "[PollingThr]waiting for data..."

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :try_start_0
    iget-object v3, p0, Lcom/mediatek/apst/util/communication/common/PollingThr;->mOis:Ljava/io/ObjectInputStream;

    invoke-virtual {v3}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_6

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "[PollingThr:info]Recieved data"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    instance-of v3, v2, Ljava/lang/String;

    if-eqz v3, :cond_5

    const-string v3, "DISC"

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "[PollingThr]Get DISC Symbol! The polling thread will exit!"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget v3, p0, Lcom/mediatek/apst/util/communication/common/PollingThr;->mRole:I

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/mediatek/apst/util/communication/common/PollingThr;->mOos:Ljava/io/ObjectOutputStream;

    const-string v4, "DISC"

    invoke-virtual {v3, v4}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    :cond_3
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "[PollingThr]Stop normally!"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget v3, p0, Lcom/mediatek/apst/util/communication/common/PollingThr;->mRole:I

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/mediatek/apst/util/communication/common/PollingThr;->mDispatcher:Lcom/mediatek/apst/util/communication/common/Dispatcher;

    new-instance v4, Lcom/mediatek/apst/util/communication/connManager/ConnDisconnectInfo;

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Lcom/mediatek/apst/util/communication/connManager/ConnDisconnectInfo;-><init>(I)V

    invoke-virtual {v3, v4}, Lcom/mediatek/apst/util/communication/common/Dispatcher;->dispatch(Ljava/lang/Object;)Z

    :cond_4
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/mediatek/apst/util/communication/common/PollingThr;->bRunning:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "[PollingThr:error]The polling thread get IO Exception, and the thread is stoped!"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/apst/util/communication/common/PollingThr;->mDispatcher:Lcom/mediatek/apst/util/communication/common/Dispatcher;

    new-instance v4, Lcom/mediatek/apst/util/communication/connManager/ConnDisconnectExpt;

    invoke-direct {v4, v0}, Lcom/mediatek/apst/util/communication/connManager/ConnDisconnectExpt;-><init>(Ljava/io/IOException;)V

    invoke-virtual {v3, v4}, Lcom/mediatek/apst/util/communication/common/Dispatcher;->dispatch(Ljava/lang/Object;)Z

    iput-boolean v6, p0, Lcom/mediatek/apst/util/communication/common/PollingThr;->bRunning:Z

    goto :goto_0

    :cond_5
    :try_start_1
    iget-object v3, p0, Lcom/mediatek/apst/util/communication/common/PollingThr;->mDispatcher:Lcom/mediatek/apst/util/communication/common/Dispatcher;

    invoke-virtual {v3, v2}, Lcom/mediatek/apst/util/communication/common/Dispatcher;->dispatch(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    iput-boolean v6, p0, Lcom/mediatek/apst/util/communication/common/PollingThr;->bRunning:Z

    goto :goto_0

    :cond_6
    :try_start_2
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "[PollingThr:warning]Get NULL! We will ignore it!"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    const-wide/16 v3, 0x3e8

    :try_start_3
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_1

    :catch_3
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto/16 :goto_1
.end method

.method public setRunning(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/apst/util/communication/common/PollingThr;->bRunning:Z

    return-void
.end method
