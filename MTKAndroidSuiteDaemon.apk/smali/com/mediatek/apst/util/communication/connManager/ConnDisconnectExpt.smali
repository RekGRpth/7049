.class public Lcom/mediatek/apst/util/communication/connManager/ConnDisconnectExpt;
.super Lcom/mediatek/apst/util/communication/connManager/ConnManageEntity;
.source "ConnDisconnectExpt.java"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private exceptionObj:Ljava/io/IOException;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, -0x1

    invoke-direct {p0, v0, v1}, Lcom/mediatek/apst/util/communication/connManager/ConnManageEntity;-><init>(II)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/apst/util/communication/connManager/ConnDisconnectExpt;->exceptionObj:Ljava/io/IOException;

    return-void
.end method

.method public constructor <init>(Ljava/io/IOException;)V
    .locals 2
    .param p1    # Ljava/io/IOException;

    const/4 v0, 0x1

    const/4 v1, -0x1

    invoke-direct {p0, v0, v1}, Lcom/mediatek/apst/util/communication/connManager/ConnManageEntity;-><init>(II)V

    iput-object p1, p0, Lcom/mediatek/apst/util/communication/connManager/ConnDisconnectExpt;->exceptionObj:Ljava/io/IOException;

    return-void
.end method


# virtual methods
.method public getExceptionObj()Ljava/io/IOException;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/communication/connManager/ConnDisconnectExpt;->exceptionObj:Ljava/io/IOException;

    return-object v0
.end method
