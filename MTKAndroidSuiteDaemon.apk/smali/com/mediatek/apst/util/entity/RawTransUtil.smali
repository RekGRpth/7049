.class public Lcom/mediatek/apst/util/entity/RawTransUtil;
.super Ljava/lang/Object;
.source "RawTransUtil.java"


# static fields
.field public static final BOOLEAN:I = 0x1

.field public static final BYTE:I = 0x1

.field public static final CHAR:I = 0x2

.field public static final DEFAULT_BUFFER_SIZE:I = 0xc3500

.field public static final DOUBLE:I = 0x8

.field public static final FLOAT:I = 0x4

.field public static final INT:I = 0x4

.field public static final LENGTH_NULL:I = -0x1

.field public static final LONG:I = 0x8

.field public static final SHORT:I = 0x2

.field public static final UTF8:Ljava/lang/String; = "UTF-8"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static allocateDefaultBuffer()Ljava/nio/ByteBuffer;
    .locals 1

    const v0, 0xc3500

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0
.end method

.method public static final getBoolean(Ljava/nio/ByteBuffer;)Z
    .locals 1
    .param p0    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;,
            Ljava/nio/BufferUnderflowException;
        }
    .end annotation

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->get()B

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static final getBytes(Ljava/nio/ByteBuffer;)[B
    .locals 2
    .param p0    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;,
            Ljava/nio/BufferUnderflowException;
        }
    .end annotation

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    if-ltz v1, :cond_0

    new-array v0, v1, [B

    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;
    .locals 5
    .param p0    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;,
            Ljava/nio/BufferUnderflowException;
        }
    .end annotation

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v2

    if-ltz v2, :cond_0

    new-array v0, v2, [C

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v2, :cond_1

    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v0}, Ljava/lang/String;-><init>([C)V

    :cond_0
    return-object v3

    :cond_1
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getChar()C

    move-result v4

    aput-char v4, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static final getStringArray(Ljava/nio/ByteBuffer;)[Ljava/lang/String;
    .locals 4
    .param p0    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;,
            Ljava/nio/BufferUnderflowException;
        }
    .end annotation

    const/4 v2, 0x0

    check-cast v2, [Ljava/lang/String;

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    if-ltz v1, :cond_0

    new-array v2, v1, [Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    if-lt v0, v1, :cond_1

    :cond_0
    return-object v2

    :cond_1
    invoke-static {p0}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static final putBoolean(Ljava/nio/ByteBuffer;Z)V
    .locals 1
    .param p0    # Ljava/nio/ByteBuffer;
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;,
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final putBytes(Ljava/nio/ByteBuffer;[B)V
    .locals 1
    .param p0    # Ljava/nio/ByteBuffer;
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;,
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    if-eqz p1, :cond_0

    array-length v0, p1

    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    invoke-virtual {p0, p1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    :goto_0
    return-void

    :cond_0
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto :goto_0
.end method

.method public static final putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V
    .locals 3
    .param p0    # Ljava/nio/ByteBuffer;
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;,
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v2, v0

    invoke-virtual {p0, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    const/4 v1, 0x0

    :goto_0
    array-length v2, v0

    if-lt v1, v2, :cond_0

    :goto_1
    return-void

    :cond_0
    aget-char v2, v0, v1

    invoke-virtual {p0, v2}, Ljava/nio/ByteBuffer;->putChar(C)Ljava/nio/ByteBuffer;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, -0x1

    invoke-virtual {p0, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto :goto_1
.end method

.method public static final putStringArray(Ljava/nio/ByteBuffer;[Ljava/lang/String;)V
    .locals 2
    .param p0    # Ljava/nio/ByteBuffer;
    .param p1    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;,
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    if-eqz p1, :cond_1

    array-length v1, p1

    invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-lt v0, v1, :cond_0

    :goto_1
    return-void

    :cond_0
    aget-object v1, p1, v0

    invoke-static {p0, v1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, -0x1

    invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto :goto_1
.end method

.method public static final sizeOfBytes([B)I
    .locals 2
    .param p0    # [B

    const/4 v0, 0x4

    if-eqz p0, :cond_0

    array-length v1, p0

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_0
    return v0
.end method

.method public static final sizeOfString(Ljava/lang/String;)I
    .locals 2
    .param p0    # Ljava/lang/String;

    const/4 v0, 0x4

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    :cond_0
    return v0
.end method
