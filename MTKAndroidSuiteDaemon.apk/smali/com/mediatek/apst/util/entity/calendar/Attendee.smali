.class public Lcom/mediatek/apst/util/entity/calendar/Attendee;
.super Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;
.source "Attendee.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field private static final serialVersionUID:J = 0x2L


# instance fields
.field private attendeeEmail:Ljava/lang/String;

.field private attendeeName:Ljava/lang/String;

.field private attendeeRelationShip:I

.field private attendeeStatus:I

.field private attendeeType:I

.field private eventId:J


# direct methods
.method public constructor <init>()V
    .locals 2

    const-wide/16 v0, -0x1

    invoke-direct {p0, v0, v1}, Lcom/mediatek/apst/util/entity/calendar/Attendee;-><init>(J)V

    return-void
.end method

.method public constructor <init>(J)V
    .locals 3
    .param p1    # J

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;-><init>(J)V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/mediatek/apst/util/entity/calendar/Attendee;->eventId:J

    iput-object v2, p0, Lcom/mediatek/apst/util/entity/calendar/Attendee;->attendeeName:Ljava/lang/String;

    iput-object v2, p0, Lcom/mediatek/apst/util/entity/calendar/Attendee;->attendeeEmail:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getAttendeeEmail()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/calendar/Attendee;->attendeeEmail:Ljava/lang/String;

    return-object v0
.end method

.method public getAttendeeName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/calendar/Attendee;->attendeeName:Ljava/lang/String;

    return-object v0
.end method

.method public getAttendeeRelationShip()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/util/entity/calendar/Attendee;->attendeeRelationShip:I

    return v0
.end method

.method public getAttendeeStatus()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/util/entity/calendar/Attendee;->attendeeStatus:I

    return v0
.end method

.method public getAttendeeType()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/util/entity/calendar/Attendee;->attendeeType:I

    return v0
.end method

.method public getEventId()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/entity/calendar/Attendee;->eventId:J

    return-wide v0
.end method

.method public readRawWithVersion(Ljava/nio/ByteBuffer;I)V
    .locals 2
    .param p1    # Ljava/nio/ByteBuffer;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;,
            Ljava/nio/BufferUnderflowException;
        }
    .end annotation

    invoke-super {p0, p1, p2}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->readRawWithVersion(Ljava/nio/ByteBuffer;I)V

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mediatek/apst/util/entity/calendar/Attendee;->eventId:J

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/apst/util/entity/calendar/Attendee;->attendeeName:Ljava/lang/String;

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/apst/util/entity/calendar/Attendee;->attendeeEmail:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    iput v0, p0, Lcom/mediatek/apst/util/entity/calendar/Attendee;->attendeeStatus:I

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    iput v0, p0, Lcom/mediatek/apst/util/entity/calendar/Attendee;->attendeeRelationShip:I

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    iput v0, p0, Lcom/mediatek/apst/util/entity/calendar/Attendee;->attendeeType:I

    return-void
.end method

.method public setAttendeeEmail(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/calendar/Attendee;->attendeeEmail:Ljava/lang/String;

    return-void
.end method

.method public setAttendeeName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/calendar/Attendee;->attendeeName:Ljava/lang/String;

    return-void
.end method

.method public setAttendeeRelationShip(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/util/entity/calendar/Attendee;->attendeeRelationShip:I

    return-void
.end method

.method public setAttendeeStatus(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/util/entity/calendar/Attendee;->attendeeStatus:I

    return-void
.end method

.method public setAttendeeType(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/util/entity/calendar/Attendee;->attendeeType:I

    return-void
.end method

.method public setEventId(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/entity/calendar/Attendee;->eventId:J

    return-void
.end method

.method public writeRawWithVersion(Ljava/nio/ByteBuffer;I)V
    .locals 2
    .param p1    # Ljava/nio/ByteBuffer;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;,
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    invoke-super {p0, p1, p2}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->writeRawWithVersion(Ljava/nio/ByteBuffer;I)V

    iget-wide v0, p0, Lcom/mediatek/apst/util/entity/calendar/Attendee;->eventId:J

    invoke-virtual {p1, v0, v1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/calendar/Attendee;->attendeeName:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/calendar/Attendee;->attendeeEmail:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    iget v0, p0, Lcom/mediatek/apst/util/entity/calendar/Attendee;->attendeeStatus:I

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    iget v0, p0, Lcom/mediatek/apst/util/entity/calendar/Attendee;->attendeeRelationShip:I

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    iget v0, p0, Lcom/mediatek/apst/util/entity/calendar/Attendee;->attendeeType:I

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    return-void
.end method
