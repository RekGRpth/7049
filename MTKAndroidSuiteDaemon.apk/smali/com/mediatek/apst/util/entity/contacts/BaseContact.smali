.class public Lcom/mediatek/apst/util/entity/contacts/BaseContact;
.super Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;
.source "BaseContact.java"

# interfaces
.implements Lcom/mediatek/apst/util/entity/DataStoreLocations;
.implements Ljava/lang/Cloneable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private displayName:Ljava/lang/String;

.field private groupMemberships:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/GroupMembership;",
            ">;"
        }
    .end annotation
.end field

.field private modifyTime:J

.field private primaryNumber:Ljava/lang/String;

.field private simName:Ljava/lang/String;

.field private storeLocation:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const-wide/16 v0, -0x1

    invoke-direct {p0, v0, v1}, Lcom/mediatek/apst/util/entity/contacts/BaseContact;-><init>(J)V

    return-void
.end method

.method public constructor <init>(J)V
    .locals 2
    .param p1    # J

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;-><init>(J)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->storeLocation:I

    iput-object v1, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->displayName:Ljava/lang/String;

    iput-object v1, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->primaryNumber:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->groupMemberships:Ljava/util/List;

    iput-object v1, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->simName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public bridge synthetic clone()Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->clone()Lcom/mediatek/apst/util/entity/contacts/BaseContact;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/mediatek/apst/util/entity/contacts/BaseContact;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-super {p0}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->clone()Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;

    move-result-object v0

    check-cast v0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->groupMemberships:Ljava/util/List;

    iget-object v2, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->groupMemberships:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    return-object v0

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/apst/util/entity/contacts/GroupMembership;

    iget-object v3, v0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->groupMemberships:Ljava/util/List;

    invoke-virtual {v1}, Lcom/mediatek/apst/util/entity/contacts/GroupMembership;->clone()Lcom/mediatek/apst/util/entity/contacts/GroupMembership;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public getGroupMemberships()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/GroupMembership;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->groupMemberships:Ljava/util/List;

    return-object v0
.end method

.method public getModifyTime()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->modifyTime:J

    return-wide v0
.end method

.method public getPrimaryNumber()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->primaryNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getSimName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->simName:Ljava/lang/String;

    return-object v0
.end method

.method public getStoreLocation()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->storeLocation:I

    return v0
.end method

.method public readRaw(Ljava/nio/ByteBuffer;)V
    .locals 4
    .param p1    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation

    invoke-super {p0, p1}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->readRaw(Ljava/nio/ByteBuffer;)V

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    iput v3, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->storeLocation:I

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->displayName:Ljava/lang/String;

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->primaryNumber:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v2

    if-ltz v2, :cond_1

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v3, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->groupMemberships:Ljava/util/List;

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v2, :cond_0

    :goto_1
    return-void

    :cond_0
    new-instance v0, Lcom/mediatek/apst/util/entity/contacts/GroupMembership;

    invoke-direct {v0}, Lcom/mediatek/apst/util/entity/contacts/GroupMembership;-><init>()V

    invoke-virtual {v0, p1}, Lcom/mediatek/apst/util/entity/contacts/GroupMembership;->readRaw(Ljava/nio/ByteBuffer;)V

    iget-object v3, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->groupMemberships:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->groupMemberships:Ljava/util/List;

    goto :goto_1
.end method

.method public readRawWithVersion(Ljava/nio/ByteBuffer;I)V
    .locals 5
    .param p1    # Ljava/nio/ByteBuffer;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;,
            Ljava/nio/BufferUnderflowException;
        }
    .end annotation

    invoke-super {p0, p1, p2}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->readRawWithVersion(Ljava/nio/ByteBuffer;I)V

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    iput v3, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->storeLocation:I

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->displayName:Ljava/lang/String;

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->primaryNumber:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v2

    if-ltz v2, :cond_2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v3, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->groupMemberships:Ljava/util/List;

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v2, :cond_1

    :goto_1
    const/4 v3, 0x2

    if-lt p2, v3, :cond_0

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->simName:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->modifyTime:J

    :cond_0
    return-void

    :cond_1
    new-instance v0, Lcom/mediatek/apst/util/entity/contacts/GroupMembership;

    invoke-direct {v0}, Lcom/mediatek/apst/util/entity/contacts/GroupMembership;-><init>()V

    invoke-virtual {v0, p1}, Lcom/mediatek/apst/util/entity/contacts/GroupMembership;->readRaw(Ljava/nio/ByteBuffer;)V

    iget-object v3, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->groupMemberships:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->groupMemberships:Ljava/util/List;

    goto :goto_1
.end method

.method public setDisplayName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->displayName:Ljava/lang/String;

    return-void
.end method

.method public setGroupMemberships(Ljava/util/ArrayList;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/GroupMembership;",
            ">;)Z"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iput-object p1, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->groupMemberships:Ljava/util/List;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setGroupMemberships(Ljava/util/Vector;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/GroupMembership;",
            ">;)Z"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iput-object p1, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->groupMemberships:Ljava/util/List;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setModifyTime(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->modifyTime:J

    return-void
.end method

.method public setPrimaryNumber(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->primaryNumber:Ljava/lang/String;

    return-void
.end method

.method public setSimName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->simName:Ljava/lang/String;

    return-void
.end method

.method public setStoreLocation(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->storeLocation:I

    return-void
.end method

.method public shallowClone()Lcom/mediatek/apst/util/entity/contacts/BaseContact;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-super {p0}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->clone()Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;

    move-result-object v0

    check-cast v0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->groupMemberships:Ljava/util/List;

    iget-object v2, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->groupMemberships:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    return-object v0

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/apst/util/entity/contacts/GroupMembership;

    iget-object v3, v0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->groupMemberships:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public writeRaw(Ljava/nio/ByteBuffer;)V
    .locals 3
    .param p1    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation

    invoke-super {p0, p1}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->writeRaw(Ljava/nio/ByteBuffer;)V

    iget v1, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->storeLocation:I

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->displayName:Ljava/lang/String;

    invoke-static {p1, v1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->primaryNumber:Ljava/lang/String;

    invoke-static {p1, v1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->groupMemberships:Ljava/util/List;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->groupMemberships:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->groupMemberships:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    return-void

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/apst/util/entity/contacts/GroupMembership;

    invoke-virtual {v0, p1}, Lcom/mediatek/apst/util/entity/contacts/GroupMembership;->writeRaw(Ljava/nio/ByteBuffer;)V

    goto :goto_0

    :cond_1
    const/4 v1, -0x1

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto :goto_1
.end method

.method public writeRawWithVersion(Ljava/nio/ByteBuffer;I)V
    .locals 3
    .param p1    # Ljava/nio/ByteBuffer;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;,
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    invoke-super {p0, p1, p2}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->writeRawWithVersion(Ljava/nio/ByteBuffer;I)V

    iget v1, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->storeLocation:I

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->displayName:Ljava/lang/String;

    invoke-static {p1, v1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->primaryNumber:Ljava/lang/String;

    invoke-static {p1, v1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->groupMemberships:Ljava/util/List;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->groupMemberships:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->groupMemberships:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    :goto_1
    const/4 v1, 0x2

    if-lt p2, v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->simName:Ljava/lang/String;

    invoke-static {p1, v1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    iget-wide v1, p0, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->modifyTime:J

    invoke-virtual {p1, v1, v2}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    :cond_0
    return-void

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/apst/util/entity/contacts/GroupMembership;

    invoke-virtual {v0, p1}, Lcom/mediatek/apst/util/entity/contacts/GroupMembership;->writeRaw(Ljava/nio/ByteBuffer;)V

    goto :goto_0

    :cond_2
    const/4 v1, -0x1

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto :goto_1
.end method
