.class public Lcom/mediatek/apst/util/entity/contacts/Contact;
.super Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;
.source "Contact.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private customRingtone:Ljava/lang/String;

.field private displayName:Ljava/lang/String;

.field private hasPhoneNumber:Z

.field private inVisibleGroup:Z

.field private lastTimeContacted:J

.field private lookup:Ljava/lang/String;

.field private photoId:J

.field private rawContacts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/RawContact;",
            ">;"
        }
    .end annotation
.end field

.field private sendToVoicemail:Z

.field private starred:Z

.field private threadSafe:Z

.field private timesContacted:I


# direct methods
.method public constructor <init>(J)V
    .locals 1
    .param p1    # J

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/mediatek/apst/util/entity/contacts/Contact;-><init>(JZ)V

    return-void
.end method

.method public constructor <init>(JZ)V
    .locals 3
    .param p1    # J
    .param p3    # Z

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;-><init>(J)V

    iput-boolean v1, p0, Lcom/mediatek/apst/util/entity/contacts/Contact;->sendToVoicemail:Z

    iput v1, p0, Lcom/mediatek/apst/util/entity/contacts/Contact;->timesContacted:I

    iput-boolean v1, p0, Lcom/mediatek/apst/util/entity/contacts/Contact;->starred:Z

    iput-boolean v2, p0, Lcom/mediatek/apst/util/entity/contacts/Contact;->inVisibleGroup:Z

    iput-boolean v1, p0, Lcom/mediatek/apst/util/entity/contacts/Contact;->hasPhoneNumber:Z

    if-eqz p3, :cond_0

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/mediatek/apst/util/entity/contacts/Contact;->rawContacts:Ljava/util/List;

    iput-boolean v2, p0, Lcom/mediatek/apst/util/entity/contacts/Contact;->threadSafe:Z

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/apst/util/entity/contacts/Contact;->rawContacts:Ljava/util/List;

    iput-boolean v1, p0, Lcom/mediatek/apst/util/entity/contacts/Contact;->threadSafe:Z

    goto :goto_0
.end method


# virtual methods
.method public addAll(Ljava/util/List;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/RawContact;",
            ">;)Z"
        }
    .end annotation

    invoke-interface {p1, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public addRawContact(Lcom/mediatek/apst/util/entity/contacts/RawContact;)Z
    .locals 1
    .param p1    # Lcom/mediatek/apst/util/entity/contacts/RawContact;

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/contacts/Contact;->rawContacts:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public clear()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/contacts/Contact;->rawContacts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method public bridge synthetic clone()Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/mediatek/apst/util/entity/contacts/Contact;->clone()Lcom/mediatek/apst/util/entity/contacts/Contact;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/mediatek/apst/util/entity/contacts/Contact;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-super {p0}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->clone()Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;

    move-result-object v0

    check-cast v0, Lcom/mediatek/apst/util/entity/contacts/Contact;

    iget-boolean v2, v0, Lcom/mediatek/apst/util/entity/contacts/Contact;->threadSafe:Z

    if-eqz v2, :cond_0

    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    iput-object v2, v0, Lcom/mediatek/apst/util/entity/contacts/Contact;->rawContacts:Ljava/util/List;

    :goto_0
    iget-object v2, p0, Lcom/mediatek/apst/util/entity/contacts/Contact;->rawContacts:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    return-object v0

    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v0, Lcom/mediatek/apst/util/entity/contacts/Contact;->rawContacts:Ljava/util/List;

    goto :goto_0

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/apst/util/entity/contacts/RawContact;

    iget-object v3, v0, Lcom/mediatek/apst/util/entity/contacts/Contact;->rawContacts:Ljava/util/List;

    invoke-virtual {v1}, Lcom/mediatek/apst/util/entity/contacts/RawContact;->clone()Lcom/mediatek/apst/util/entity/contacts/RawContact;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public getCustomRingtone()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/contacts/Contact;->customRingtone:Ljava/lang/String;

    return-object v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/contacts/Contact;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public getLastTimeContacted()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/entity/contacts/Contact;->lastTimeContacted:J

    return-wide v0
.end method

.method public getLookup()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/contacts/Contact;->lookup:Ljava/lang/String;

    return-object v0
.end method

.method public getPhotoId()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/entity/contacts/Contact;->photoId:J

    return-wide v0
.end method

.method public getRawContacts()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/RawContact;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/contacts/Contact;->rawContacts:Ljava/util/List;

    return-object v0
.end method

.method public getRawContactsCount()I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/contacts/Contact;->rawContacts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getTimesContacted()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/util/entity/contacts/Contact;->timesContacted:I

    return v0
.end method

.method public isHasPhoneNumber()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/apst/util/entity/contacts/Contact;->hasPhoneNumber:Z

    return v0
.end method

.method public isInVisibleGroup()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/apst/util/entity/contacts/Contact;->inVisibleGroup:Z

    return v0
.end method

.method public isSendToVoicemail()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/apst/util/entity/contacts/Contact;->sendToVoicemail:Z

    return v0
.end method

.method public isStarred()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/apst/util/entity/contacts/Contact;->starred:Z

    return v0
.end method

.method public isThreadSafe()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/apst/util/entity/contacts/Contact;->threadSafe:Z

    return v0
.end method

.method public removeAll(Ljava/util/List;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/RawContact;",
            ">;)Z"
        }
    .end annotation

    invoke-interface {p1, p1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public removeRawContact(I)Lcom/mediatek/apst/util/entity/contacts/RawContact;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/contacts/Contact;->rawContacts:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/apst/util/entity/contacts/RawContact;

    return-object v0
.end method

.method public removeRawContact(Lcom/mediatek/apst/util/entity/contacts/RawContact;)Z
    .locals 1
    .param p1    # Lcom/mediatek/apst/util/entity/contacts/RawContact;

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/contacts/Contact;->rawContacts:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public setCustomRingtone(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/contacts/Contact;->customRingtone:Ljava/lang/String;

    return-void
.end method

.method public setDisplayName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/contacts/Contact;->displayName:Ljava/lang/String;

    return-void
.end method

.method public setHasPhoneNumber(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/apst/util/entity/contacts/Contact;->hasPhoneNumber:Z

    return-void
.end method

.method public setInVisibleGroup(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/apst/util/entity/contacts/Contact;->inVisibleGroup:Z

    return-void
.end method

.method public setLastTimeContacted(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/entity/contacts/Contact;->lastTimeContacted:J

    return-void
.end method

.method public setLookup(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/contacts/Contact;->lookup:Ljava/lang/String;

    return-void
.end method

.method public setPhotoId(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/entity/contacts/Contact;->photoId:J

    return-void
.end method

.method public setRawContacts(Ljava/util/ArrayList;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/RawContact;",
            ">;)Z"
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-boolean v1, p0, Lcom/mediatek/apst/util/entity/contacts/Contact;->threadSafe:Z

    if-nez v1, :cond_0

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/contacts/Contact;->rawContacts:Ljava/util/List;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setRawContacts(Ljava/util/Vector;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/RawContact;",
            ">;)Z"
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-boolean v1, p0, Lcom/mediatek/apst/util/entity/contacts/Contact;->threadSafe:Z

    if-eqz v1, :cond_0

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/contacts/Contact;->rawContacts:Ljava/util/List;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setSendToVoicemail(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/apst/util/entity/contacts/Contact;->sendToVoicemail:Z

    return-void
.end method

.method public setStarred(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/apst/util/entity/contacts/Contact;->starred:Z

    return-void
.end method

.method public setTimesContacted(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/util/entity/contacts/Contact;->timesContacted:I

    return-void
.end method

.method public shallowClone()Lcom/mediatek/apst/util/entity/contacts/Contact;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-super {p0}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->clone()Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;

    move-result-object v0

    check-cast v0, Lcom/mediatek/apst/util/entity/contacts/Contact;

    iget-boolean v2, v0, Lcom/mediatek/apst/util/entity/contacts/Contact;->threadSafe:Z

    if-eqz v2, :cond_0

    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    iput-object v2, v0, Lcom/mediatek/apst/util/entity/contacts/Contact;->rawContacts:Ljava/util/List;

    :goto_0
    iget-object v2, p0, Lcom/mediatek/apst/util/entity/contacts/Contact;->rawContacts:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    return-object v0

    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v0, Lcom/mediatek/apst/util/entity/contacts/Contact;->rawContacts:Ljava/util/List;

    goto :goto_0

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/apst/util/entity/contacts/RawContact;

    iget-object v3, v0, Lcom/mediatek/apst/util/entity/contacts/Contact;->rawContacts:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method
