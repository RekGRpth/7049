.class public Lcom/mediatek/apst/util/entity/contacts/RawContact;
.super Lcom/mediatek/apst/util/entity/contacts/BaseContact;
.source "RawContact.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/apst/util/entity/contacts/RawContact$UnknownContactDataTypeException;
    }
.end annotation


# static fields
.field public static final SOURCE_NONE:I = -0xff

.field public static final SOURCE_PHONE:I = -0x1

.field public static final SOURCE_SIM:I = 0x0

.field public static final SOURCE_SIM1:I = 0x1

.field public static final SOURCE_SIM2:I = 0x2

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private accountId:I

.field private contactId:J

.field private customRingtone:Ljava/lang/String;

.field private dirty:Z

.field private emails:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/Email;",
            ">;"
        }
    .end annotation
.end field

.field private ims:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/Im;",
            ">;"
        }
    .end annotation
.end field

.field private lastTimeContacted:J

.field private names:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/StructuredName;",
            ">;"
        }
    .end annotation
.end field

.field private nicknames:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/Nickname;",
            ">;"
        }
    .end annotation
.end field

.field private notes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/Note;",
            ">;"
        }
    .end annotation
.end field

.field private organizations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/Organization;",
            ">;"
        }
    .end annotation
.end field

.field private phones:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/Phone;",
            ">;"
        }
    .end annotation
.end field

.field private photos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/Photo;",
            ">;"
        }
    .end annotation
.end field

.field private postals:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;",
            ">;"
        }
    .end annotation
.end field

.field private sendToVoicemail:Z

.field private simId:I

.field private simIndex:I

.field private sourceLocation:I

.field private starred:Z

.field private timesContacted:I

.field private version:I

.field private websites:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/Website;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    const-wide/16 v0, -0x1

    invoke-direct {p0, v0, v1}, Lcom/mediatek/apst/util/entity/contacts/RawContact;-><init>(J)V

    return-void
.end method

.method public constructor <init>(J)V
    .locals 4
    .param p1    # J

    const/4 v3, -0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2}, Lcom/mediatek/apst/util/entity/contacts/BaseContact;-><init>(J)V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->contactId:J

    iput v2, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->timesContacted:I

    iput-boolean v2, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->starred:Z

    iput-boolean v2, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->sendToVoicemail:Z

    iput v3, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->version:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->names:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->phones:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->photos:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->emails:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->ims:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->postals:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->organizations:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->notes:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->nicknames:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->websites:Ljava/util/List;

    iput v3, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->sourceLocation:I

    return-void
.end method

.method public constructor <init>(Lcom/mediatek/apst/util/entity/contacts/BaseContact;)V
    .locals 2
    .param p1    # Lcom/mediatek/apst/util/entity/contacts/BaseContact;

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->getId()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/mediatek/apst/util/entity/contacts/RawContact;-><init>(J)V

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->setDisplayName(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->getPrimaryNumber()Ljava/lang/String;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->setPrimaryNumber(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->getStoreLocation()I

    move-result v0

    invoke-super {p0, v0}, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->setStoreLocation(I)V

    invoke-super {p0}, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->getGroupMemberships()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->getGroupMemberships()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-void
.end method


# virtual methods
.method public addContactData(Lcom/mediatek/apst/util/entity/contacts/ContactData;)V
    .locals 3
    .param p1    # Lcom/mediatek/apst/util/entity/contacts/ContactData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mediatek/apst/util/entity/contacts/RawContact$UnknownContactDataTypeException;
        }
    .end annotation

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    instance-of v0, p1, Lcom/mediatek/apst/util/entity/contacts/StructuredName;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/mediatek/apst/util/entity/contacts/RawContact;->getNames()Ljava/util/List;

    move-result-object v0

    check-cast p1, Lcom/mediatek/apst/util/entity/contacts/StructuredName;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    instance-of v0, p1, Lcom/mediatek/apst/util/entity/contacts/Phone;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/mediatek/apst/util/entity/contacts/RawContact;->getPhones()Ljava/util/List;

    move-result-object v0

    check-cast p1, Lcom/mediatek/apst/util/entity/contacts/Phone;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    instance-of v0, p1, Lcom/mediatek/apst/util/entity/contacts/Photo;

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/mediatek/apst/util/entity/contacts/RawContact;->getPhotos()Ljava/util/List;

    move-result-object v0

    check-cast p1, Lcom/mediatek/apst/util/entity/contacts/Photo;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    instance-of v0, p1, Lcom/mediatek/apst/util/entity/contacts/Im;

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/mediatek/apst/util/entity/contacts/RawContact;->getIms()Ljava/util/List;

    move-result-object v0

    check-cast p1, Lcom/mediatek/apst/util/entity/contacts/Im;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    instance-of v0, p1, Lcom/mediatek/apst/util/entity/contacts/Email;

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/mediatek/apst/util/entity/contacts/RawContact;->getEmails()Ljava/util/List;

    move-result-object v0

    check-cast p1, Lcom/mediatek/apst/util/entity/contacts/Email;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_5
    instance-of v0, p1, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/mediatek/apst/util/entity/contacts/RawContact;->getPostals()Ljava/util/List;

    move-result-object v0

    check-cast p1, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_6
    instance-of v0, p1, Lcom/mediatek/apst/util/entity/contacts/Organization;

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lcom/mediatek/apst/util/entity/contacts/RawContact;->getOrganizations()Ljava/util/List;

    move-result-object v0

    check-cast p1, Lcom/mediatek/apst/util/entity/contacts/Organization;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_7
    instance-of v0, p1, Lcom/mediatek/apst/util/entity/contacts/Nickname;

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Lcom/mediatek/apst/util/entity/contacts/RawContact;->getNicknames()Ljava/util/List;

    move-result-object v0

    check-cast p1, Lcom/mediatek/apst/util/entity/contacts/Nickname;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_8
    instance-of v0, p1, Lcom/mediatek/apst/util/entity/contacts/Website;

    if-eqz v0, :cond_9

    invoke-virtual {p0}, Lcom/mediatek/apst/util/entity/contacts/RawContact;->getWebsites()Ljava/util/List;

    move-result-object v0

    check-cast p1, Lcom/mediatek/apst/util/entity/contacts/Website;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_9
    instance-of v0, p1, Lcom/mediatek/apst/util/entity/contacts/Note;

    if-eqz v0, :cond_a

    invoke-virtual {p0}, Lcom/mediatek/apst/util/entity/contacts/RawContact;->getNotes()Ljava/util/List;

    move-result-object v0

    check-cast p1, Lcom/mediatek/apst/util/entity/contacts/Note;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_a
    instance-of v0, p1, Lcom/mediatek/apst/util/entity/contacts/GroupMembership;

    if-eqz v0, :cond_b

    invoke-virtual {p0}, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->getGroupMemberships()Ljava/util/List;

    move-result-object v0

    check-cast p1, Lcom/mediatek/apst/util/entity/contacts/GroupMembership;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_b
    new-instance v0, Lcom/mediatek/apst/util/entity/contacts/RawContact$UnknownContactDataTypeException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/contacts/ContactData;->getMimeType()I

    move-result v2

    invoke-direct {v0, p0, v1, v2}, Lcom/mediatek/apst/util/entity/contacts/RawContact$UnknownContactDataTypeException;-><init>(Lcom/mediatek/apst/util/entity/contacts/RawContact;Ljava/lang/String;I)V

    throw v0
.end method

.method public clearAllData()V
    .locals 1

    invoke-virtual {p0}, Lcom/mediatek/apst/util/entity/contacts/RawContact;->getNames()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    invoke-virtual {p0}, Lcom/mediatek/apst/util/entity/contacts/RawContact;->getPhones()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    invoke-virtual {p0}, Lcom/mediatek/apst/util/entity/contacts/RawContact;->getPhotos()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    invoke-virtual {p0}, Lcom/mediatek/apst/util/entity/contacts/RawContact;->getIms()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    invoke-virtual {p0}, Lcom/mediatek/apst/util/entity/contacts/RawContact;->getEmails()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    invoke-virtual {p0}, Lcom/mediatek/apst/util/entity/contacts/RawContact;->getPostals()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    invoke-virtual {p0}, Lcom/mediatek/apst/util/entity/contacts/RawContact;->getOrganizations()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    invoke-virtual {p0}, Lcom/mediatek/apst/util/entity/contacts/RawContact;->getNicknames()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    invoke-virtual {p0}, Lcom/mediatek/apst/util/entity/contacts/RawContact;->getWebsites()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    invoke-virtual {p0}, Lcom/mediatek/apst/util/entity/contacts/RawContact;->getNotes()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    invoke-virtual {p0}, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->getGroupMemberships()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method public bridge synthetic clone()Lcom/mediatek/apst/util/entity/contacts/BaseContact;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/mediatek/apst/util/entity/contacts/RawContact;->clone()Lcom/mediatek/apst/util/entity/contacts/RawContact;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/mediatek/apst/util/entity/contacts/RawContact;
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-super {p0}, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->clone()Lcom/mediatek/apst/util/entity/contacts/BaseContact;

    move-result-object v0

    check-cast v0, Lcom/mediatek/apst/util/entity/contacts/RawContact;

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    iput-object v11, v0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->names:Ljava/util/List;

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    iput-object v11, v0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->phones:Ljava/util/List;

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    iput-object v11, v0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->photos:Ljava/util/List;

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    iput-object v11, v0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->emails:Ljava/util/List;

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    iput-object v11, v0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->ims:Ljava/util/List;

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    iput-object v11, v0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->postals:Ljava/util/List;

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    iput-object v11, v0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->organizations:Ljava/util/List;

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    iput-object v11, v0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->notes:Ljava/util/List;

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    iput-object v11, v0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->nicknames:Ljava/util/List;

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    iput-object v11, v0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->websites:Ljava/util/List;

    iget-object v11, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->names:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_0

    iget-object v11, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->phones:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_1

    iget-object v11, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->photos:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_2

    iget-object v11, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->emails:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_3
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_3

    iget-object v11, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->ims:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_4
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_4

    iget-object v11, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->postals:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_5
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_5

    iget-object v11, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->organizations:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_6
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_6

    iget-object v11, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->notes:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_7
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_7

    iget-object v11, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->nicknames:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_8
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_8

    iget-object v11, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->websites:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_9
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_9

    return-object v0

    :cond_0
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/apst/util/entity/contacts/StructuredName;

    iget-object v12, v0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->names:Ljava/util/List;

    invoke-virtual {v3}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->clone()Lcom/mediatek/apst/util/entity/contacts/StructuredName;

    move-result-object v13

    invoke-interface {v12, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_1
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/mediatek/apst/util/entity/contacts/Phone;

    iget-object v12, v0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->phones:Ljava/util/List;

    invoke-virtual {v7}, Lcom/mediatek/apst/util/entity/contacts/Phone;->clone()Lcom/mediatek/apst/util/entity/contacts/Phone;

    move-result-object v13

    invoke-interface {v12, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_2
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/mediatek/apst/util/entity/contacts/Photo;

    iget-object v12, v0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->photos:Ljava/util/List;

    invoke-virtual {v8}, Lcom/mediatek/apst/util/entity/contacts/Photo;->clone()Lcom/mediatek/apst/util/entity/contacts/Photo;

    move-result-object v13

    invoke-interface {v12, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    :cond_3
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/apst/util/entity/contacts/Email;

    iget-object v12, v0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->emails:Ljava/util/List;

    invoke-virtual {v1}, Lcom/mediatek/apst/util/entity/contacts/Email;->clone()Lcom/mediatek/apst/util/entity/contacts/Email;

    move-result-object v13

    invoke-interface {v12, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    :cond_4
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/apst/util/entity/contacts/Im;

    iget-object v12, v0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->ims:Ljava/util/List;

    invoke-virtual {v2}, Lcom/mediatek/apst/util/entity/contacts/Im;->clone()Lcom/mediatek/apst/util/entity/contacts/Im;

    move-result-object v13

    invoke-interface {v12, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    :cond_5
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;

    iget-object v12, v0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->postals:Ljava/util/List;

    invoke-virtual {v9}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->clone()Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;

    move-result-object v13

    invoke-interface {v12, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_5

    :cond_6
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/mediatek/apst/util/entity/contacts/Organization;

    iget-object v12, v0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->organizations:Ljava/util/List;

    invoke-virtual {v6}, Lcom/mediatek/apst/util/entity/contacts/Organization;->clone()Lcom/mediatek/apst/util/entity/contacts/Organization;

    move-result-object v13

    invoke-interface {v12, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_6

    :cond_7
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/mediatek/apst/util/entity/contacts/Note;

    iget-object v12, v0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->notes:Ljava/util/List;

    invoke-virtual {v5}, Lcom/mediatek/apst/util/entity/contacts/Note;->clone()Lcom/mediatek/apst/util/entity/contacts/Note;

    move-result-object v13

    invoke-interface {v12, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_7

    :cond_8
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/apst/util/entity/contacts/Nickname;

    iget-object v12, v0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->nicknames:Ljava/util/List;

    invoke-virtual {v4}, Lcom/mediatek/apst/util/entity/contacts/Nickname;->clone()Lcom/mediatek/apst/util/entity/contacts/Nickname;

    move-result-object v13

    invoke-interface {v12, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_8

    :cond_9
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/mediatek/apst/util/entity/contacts/Website;

    iget-object v12, v0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->websites:Ljava/util/List;

    invoke-virtual {v10}, Lcom/mediatek/apst/util/entity/contacts/Website;->clone()Lcom/mediatek/apst/util/entity/contacts/Website;

    move-result-object v13

    invoke-interface {v12, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_9
.end method

.method public getAllContactData()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/ContactData;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->names:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->phones:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->photos:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->emails:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->ims:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->postals:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->organizations:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->notes:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->nicknames:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->websites:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {p0}, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->getGroupMemberships()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-object v0
.end method

.method public getContactDataCount()I
    .locals 2

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->names:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->phones:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->photos:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->emails:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->ims:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->postals:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->organizations:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->notes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->nicknames:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->websites:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->getGroupMemberships()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getContactId()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->contactId:J

    return-wide v0
.end method

.method public getCustomRingtone()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->customRingtone:Ljava/lang/String;

    return-object v0
.end method

.method public getEmails()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/Email;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->emails:Ljava/util/List;

    return-object v0
.end method

.method public getIms()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/Im;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->ims:Ljava/util/List;

    return-object v0
.end method

.method public getLastTimeContacted()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->lastTimeContacted:J

    return-wide v0
.end method

.method public getNames()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/StructuredName;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->names:Ljava/util/List;

    return-object v0
.end method

.method public getNicknames()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/Nickname;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->nicknames:Ljava/util/List;

    return-object v0
.end method

.method public getNotes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/Note;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->notes:Ljava/util/List;

    return-object v0
.end method

.method public getOrganizations()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/Organization;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->organizations:Ljava/util/List;

    return-object v0
.end method

.method public getPhones()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/Phone;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->phones:Ljava/util/List;

    return-object v0
.end method

.method public getPhotos()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/Photo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->photos:Ljava/util/List;

    return-object v0
.end method

.method public getPostals()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->postals:Ljava/util/List;

    return-object v0
.end method

.method public getSimId()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->simId:I

    return v0
.end method

.method public getSimIndex()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->simIndex:I

    return v0
.end method

.method public getSourceLocation()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->sourceLocation:I

    return v0
.end method

.method public getTimesContacted()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->timesContacted:I

    return v0
.end method

.method public getVersion()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->version:I

    return v0
.end method

.method public getWebsites()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/Website;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->websites:Ljava/util/List;

    return-object v0
.end method

.method public isDirty()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->dirty:Z

    return v0
.end method

.method public isSendToVoicemail()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->sendToVoicemail:Z

    return v0
.end method

.method public isStarred()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->starred:Z

    return v0
.end method

.method public readRaw(Ljava/nio/ByteBuffer;)V
    .locals 14
    .param p1    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation

    const/4 v13, 0x0

    invoke-super {p0, p1}, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->readRaw(Ljava/nio/ByteBuffer;)V

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getBoolean(Ljava/nio/ByteBuffer;)Z

    move-result v12

    iput-boolean v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->starred:Z

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getBoolean(Ljava/nio/ByteBuffer;)Z

    move-result v12

    iput-boolean v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->sendToVoicemail:Z

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v12

    iput v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->version:I

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v10

    if-ltz v10, :cond_1

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12, v10}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->names:Ljava/util/List;

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v10, :cond_0

    :goto_1
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v10

    if-ltz v10, :cond_3

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12, v10}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->phones:Ljava/util/List;

    const/4 v1, 0x0

    :goto_2
    if-lt v1, v10, :cond_2

    :goto_3
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v10

    if-ltz v10, :cond_5

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12, v10}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->photos:Ljava/util/List;

    const/4 v1, 0x0

    :goto_4
    if-lt v1, v10, :cond_4

    :goto_5
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v10

    if-ltz v10, :cond_7

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12, v10}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->emails:Ljava/util/List;

    const/4 v1, 0x0

    :goto_6
    if-lt v1, v10, :cond_6

    :goto_7
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v10

    if-ltz v10, :cond_9

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12, v10}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->ims:Ljava/util/List;

    const/4 v1, 0x0

    :goto_8
    if-lt v1, v10, :cond_8

    :goto_9
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v10

    if-ltz v10, :cond_b

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12, v10}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->postals:Ljava/util/List;

    const/4 v1, 0x0

    :goto_a
    if-lt v1, v10, :cond_a

    :goto_b
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v10

    if-ltz v10, :cond_d

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12, v10}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->organizations:Ljava/util/List;

    const/4 v1, 0x0

    :goto_c
    if-lt v1, v10, :cond_c

    :goto_d
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v10

    if-ltz v10, :cond_f

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12, v10}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->notes:Ljava/util/List;

    const/4 v1, 0x0

    :goto_e
    if-lt v1, v10, :cond_e

    :goto_f
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v10

    if-ltz v10, :cond_11

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12, v10}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->nicknames:Ljava/util/List;

    const/4 v1, 0x0

    :goto_10
    if-lt v1, v10, :cond_10

    :goto_11
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v10

    if-ltz v10, :cond_13

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12, v10}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->websites:Ljava/util/List;

    const/4 v1, 0x0

    :goto_12
    if-lt v1, v10, :cond_12

    :goto_13
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v12

    iput v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->sourceLocation:I

    return-void

    :cond_0
    new-instance v3, Lcom/mediatek/apst/util/entity/contacts/StructuredName;

    invoke-direct {v3}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;-><init>()V

    invoke-virtual {v3, p1}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->readRaw(Ljava/nio/ByteBuffer;)V

    iget-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->names:Ljava/util/List;

    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    :cond_1
    iput-object v13, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->names:Ljava/util/List;

    goto/16 :goto_1

    :cond_2
    new-instance v7, Lcom/mediatek/apst/util/entity/contacts/Phone;

    invoke-direct {v7}, Lcom/mediatek/apst/util/entity/contacts/Phone;-><init>()V

    invoke-virtual {v7, p1}, Lcom/mediatek/apst/util/entity/contacts/Phone;->readRaw(Ljava/nio/ByteBuffer;)V

    iget-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->phones:Ljava/util/List;

    invoke-interface {v12, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_2

    :cond_3
    iput-object v13, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->phones:Ljava/util/List;

    goto/16 :goto_3

    :cond_4
    new-instance v8, Lcom/mediatek/apst/util/entity/contacts/Photo;

    invoke-direct {v8}, Lcom/mediatek/apst/util/entity/contacts/Photo;-><init>()V

    invoke-virtual {v8, p1}, Lcom/mediatek/apst/util/entity/contacts/Photo;->readRaw(Ljava/nio/ByteBuffer;)V

    iget-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->photos:Ljava/util/List;

    invoke-interface {v12, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_4

    :cond_5
    iput-object v13, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->photos:Ljava/util/List;

    goto/16 :goto_5

    :cond_6
    new-instance v0, Lcom/mediatek/apst/util/entity/contacts/Email;

    invoke-direct {v0}, Lcom/mediatek/apst/util/entity/contacts/Email;-><init>()V

    invoke-virtual {v0, p1}, Lcom/mediatek/apst/util/entity/contacts/Email;->readRaw(Ljava/nio/ByteBuffer;)V

    iget-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->emails:Ljava/util/List;

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_6

    :cond_7
    iput-object v13, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->emails:Ljava/util/List;

    goto/16 :goto_7

    :cond_8
    new-instance v2, Lcom/mediatek/apst/util/entity/contacts/Im;

    invoke-direct {v2}, Lcom/mediatek/apst/util/entity/contacts/Im;-><init>()V

    invoke-virtual {v2, p1}, Lcom/mediatek/apst/util/entity/contacts/Im;->readRaw(Ljava/nio/ByteBuffer;)V

    iget-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->ims:Ljava/util/List;

    invoke-interface {v12, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_8

    :cond_9
    iput-object v13, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->ims:Ljava/util/List;

    goto/16 :goto_9

    :cond_a
    new-instance v9, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;

    invoke-direct {v9}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;-><init>()V

    invoke-virtual {v9, p1}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->readRaw(Ljava/nio/ByteBuffer;)V

    iget-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->postals:Ljava/util/List;

    invoke-interface {v12, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_a

    :cond_b
    iput-object v13, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->postals:Ljava/util/List;

    goto/16 :goto_b

    :cond_c
    new-instance v6, Lcom/mediatek/apst/util/entity/contacts/Organization;

    invoke-direct {v6}, Lcom/mediatek/apst/util/entity/contacts/Organization;-><init>()V

    invoke-virtual {v6, p1}, Lcom/mediatek/apst/util/entity/contacts/Organization;->readRaw(Ljava/nio/ByteBuffer;)V

    iget-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->organizations:Ljava/util/List;

    invoke-interface {v12, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_c

    :cond_d
    iput-object v13, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->organizations:Ljava/util/List;

    goto/16 :goto_d

    :cond_e
    new-instance v5, Lcom/mediatek/apst/util/entity/contacts/Note;

    invoke-direct {v5}, Lcom/mediatek/apst/util/entity/contacts/Note;-><init>()V

    invoke-virtual {v5, p1}, Lcom/mediatek/apst/util/entity/contacts/Note;->readRaw(Ljava/nio/ByteBuffer;)V

    iget-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->notes:Ljava/util/List;

    invoke-interface {v12, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_e

    :cond_f
    iput-object v13, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->notes:Ljava/util/List;

    goto/16 :goto_f

    :cond_10
    new-instance v4, Lcom/mediatek/apst/util/entity/contacts/Nickname;

    invoke-direct {v4}, Lcom/mediatek/apst/util/entity/contacts/Nickname;-><init>()V

    invoke-virtual {v4, p1}, Lcom/mediatek/apst/util/entity/contacts/Nickname;->readRaw(Ljava/nio/ByteBuffer;)V

    iget-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->nicknames:Ljava/util/List;

    invoke-interface {v12, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_10

    :cond_11
    iput-object v13, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->nicknames:Ljava/util/List;

    goto/16 :goto_11

    :cond_12
    new-instance v11, Lcom/mediatek/apst/util/entity/contacts/Website;

    invoke-direct {v11}, Lcom/mediatek/apst/util/entity/contacts/Website;-><init>()V

    invoke-virtual {v11, p1}, Lcom/mediatek/apst/util/entity/contacts/Website;->readRaw(Ljava/nio/ByteBuffer;)V

    iget-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->websites:Ljava/util/List;

    invoke-interface {v12, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_12

    :cond_13
    iput-object v13, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->websites:Ljava/util/List;

    goto/16 :goto_13
.end method

.method public readRawWithVersion(Ljava/nio/ByteBuffer;I)V
    .locals 13
    .param p1    # Ljava/nio/ByteBuffer;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;,
            Ljava/nio/BufferUnderflowException;
        }
    .end annotation

    invoke-super {p0, p1, p2}, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->readRawWithVersion(Ljava/nio/ByteBuffer;I)V

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getBoolean(Ljava/nio/ByteBuffer;)Z

    move-result v12

    iput-boolean v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->starred:Z

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getBoolean(Ljava/nio/ByteBuffer;)Z

    move-result v12

    iput-boolean v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->sendToVoicemail:Z

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v12

    iput v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->version:I

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v10

    if-ltz v10, :cond_2

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12, v10}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->names:Ljava/util/List;

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v10, :cond_1

    :goto_1
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v10

    if-ltz v10, :cond_4

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12, v10}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->phones:Ljava/util/List;

    const/4 v1, 0x0

    :goto_2
    if-lt v1, v10, :cond_3

    :goto_3
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v10

    if-ltz v10, :cond_6

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12, v10}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->photos:Ljava/util/List;

    const/4 v1, 0x0

    :goto_4
    if-lt v1, v10, :cond_5

    :goto_5
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v10

    if-ltz v10, :cond_8

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12, v10}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->emails:Ljava/util/List;

    const/4 v1, 0x0

    :goto_6
    if-lt v1, v10, :cond_7

    :goto_7
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v10

    if-ltz v10, :cond_a

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12, v10}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->ims:Ljava/util/List;

    const/4 v1, 0x0

    :goto_8
    if-lt v1, v10, :cond_9

    :goto_9
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v10

    if-ltz v10, :cond_c

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12, v10}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->postals:Ljava/util/List;

    const/4 v1, 0x0

    :goto_a
    if-lt v1, v10, :cond_b

    :goto_b
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v10

    if-ltz v10, :cond_e

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12, v10}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->organizations:Ljava/util/List;

    const/4 v1, 0x0

    :goto_c
    if-lt v1, v10, :cond_d

    :goto_d
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v10

    if-ltz v10, :cond_10

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12, v10}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->notes:Ljava/util/List;

    const/4 v1, 0x0

    :goto_e
    if-lt v1, v10, :cond_f

    :goto_f
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v10

    if-ltz v10, :cond_12

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12, v10}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->nicknames:Ljava/util/List;

    const/4 v1, 0x0

    :goto_10
    if-lt v1, v10, :cond_11

    :goto_11
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v10

    if-ltz v10, :cond_14

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12, v10}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->websites:Ljava/util/List;

    const/4 v1, 0x0

    :goto_12
    if-lt v1, v10, :cond_13

    :goto_13
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v12

    iput v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->sourceLocation:I

    const/4 v12, 0x2

    if-lt p2, v12, :cond_0

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v12

    iput v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->simId:I

    :cond_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v12

    iput v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->simIndex:I

    return-void

    :cond_1
    new-instance v3, Lcom/mediatek/apst/util/entity/contacts/StructuredName;

    invoke-direct {v3}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;-><init>()V

    invoke-virtual {v3, p1}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->readRaw(Ljava/nio/ByteBuffer;)V

    iget-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->names:Ljava/util/List;

    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    :cond_2
    const/4 v12, 0x0

    iput-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->names:Ljava/util/List;

    goto/16 :goto_1

    :cond_3
    new-instance v7, Lcom/mediatek/apst/util/entity/contacts/Phone;

    invoke-direct {v7}, Lcom/mediatek/apst/util/entity/contacts/Phone;-><init>()V

    invoke-virtual {v7, p1, p2}, Lcom/mediatek/apst/util/entity/contacts/Phone;->readRawWithVersion(Ljava/nio/ByteBuffer;I)V

    iget-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->phones:Ljava/util/List;

    invoke-interface {v12, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_2

    :cond_4
    const/4 v12, 0x0

    iput-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->phones:Ljava/util/List;

    goto/16 :goto_3

    :cond_5
    new-instance v8, Lcom/mediatek/apst/util/entity/contacts/Photo;

    invoke-direct {v8}, Lcom/mediatek/apst/util/entity/contacts/Photo;-><init>()V

    invoke-virtual {v8, p1}, Lcom/mediatek/apst/util/entity/contacts/Photo;->readRaw(Ljava/nio/ByteBuffer;)V

    iget-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->photos:Ljava/util/List;

    invoke-interface {v12, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_4

    :cond_6
    const/4 v12, 0x0

    iput-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->photos:Ljava/util/List;

    goto/16 :goto_5

    :cond_7
    new-instance v0, Lcom/mediatek/apst/util/entity/contacts/Email;

    invoke-direct {v0}, Lcom/mediatek/apst/util/entity/contacts/Email;-><init>()V

    invoke-virtual {v0, p1}, Lcom/mediatek/apst/util/entity/contacts/Email;->readRaw(Ljava/nio/ByteBuffer;)V

    iget-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->emails:Ljava/util/List;

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_6

    :cond_8
    const/4 v12, 0x0

    iput-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->emails:Ljava/util/List;

    goto/16 :goto_7

    :cond_9
    new-instance v2, Lcom/mediatek/apst/util/entity/contacts/Im;

    invoke-direct {v2}, Lcom/mediatek/apst/util/entity/contacts/Im;-><init>()V

    invoke-virtual {v2, p1}, Lcom/mediatek/apst/util/entity/contacts/Im;->readRaw(Ljava/nio/ByteBuffer;)V

    iget-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->ims:Ljava/util/List;

    invoke-interface {v12, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_8

    :cond_a
    const/4 v12, 0x0

    iput-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->ims:Ljava/util/List;

    goto/16 :goto_9

    :cond_b
    new-instance v9, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;

    invoke-direct {v9}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;-><init>()V

    invoke-virtual {v9, p1}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->readRaw(Ljava/nio/ByteBuffer;)V

    iget-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->postals:Ljava/util/List;

    invoke-interface {v12, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_a

    :cond_c
    const/4 v12, 0x0

    iput-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->postals:Ljava/util/List;

    goto/16 :goto_b

    :cond_d
    new-instance v6, Lcom/mediatek/apst/util/entity/contacts/Organization;

    invoke-direct {v6}, Lcom/mediatek/apst/util/entity/contacts/Organization;-><init>()V

    invoke-virtual {v6, p1}, Lcom/mediatek/apst/util/entity/contacts/Organization;->readRaw(Ljava/nio/ByteBuffer;)V

    iget-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->organizations:Ljava/util/List;

    invoke-interface {v12, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_c

    :cond_e
    const/4 v12, 0x0

    iput-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->organizations:Ljava/util/List;

    goto/16 :goto_d

    :cond_f
    new-instance v5, Lcom/mediatek/apst/util/entity/contacts/Note;

    invoke-direct {v5}, Lcom/mediatek/apst/util/entity/contacts/Note;-><init>()V

    invoke-virtual {v5, p1}, Lcom/mediatek/apst/util/entity/contacts/Note;->readRaw(Ljava/nio/ByteBuffer;)V

    iget-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->notes:Ljava/util/List;

    invoke-interface {v12, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_e

    :cond_10
    const/4 v12, 0x0

    iput-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->notes:Ljava/util/List;

    goto/16 :goto_f

    :cond_11
    new-instance v4, Lcom/mediatek/apst/util/entity/contacts/Nickname;

    invoke-direct {v4}, Lcom/mediatek/apst/util/entity/contacts/Nickname;-><init>()V

    invoke-virtual {v4, p1}, Lcom/mediatek/apst/util/entity/contacts/Nickname;->readRaw(Ljava/nio/ByteBuffer;)V

    iget-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->nicknames:Ljava/util/List;

    invoke-interface {v12, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_10

    :cond_12
    const/4 v12, 0x0

    iput-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->nicknames:Ljava/util/List;

    goto/16 :goto_11

    :cond_13
    new-instance v11, Lcom/mediatek/apst/util/entity/contacts/Website;

    invoke-direct {v11}, Lcom/mediatek/apst/util/entity/contacts/Website;-><init>()V

    invoke-virtual {v11, p1}, Lcom/mediatek/apst/util/entity/contacts/Website;->readRaw(Ljava/nio/ByteBuffer;)V

    iget-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->websites:Ljava/util/List;

    invoke-interface {v12, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_12

    :cond_14
    const/4 v12, 0x0

    iput-object v12, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->websites:Ljava/util/List;

    goto/16 :goto_13
.end method

.method public setContactId(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->contactId:J

    return-void
.end method

.method public setCustomRingtone(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->customRingtone:Ljava/lang/String;

    return-void
.end method

.method public setDirty(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->dirty:Z

    return-void
.end method

.method public setEmails(Ljava/util/ArrayList;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/Email;",
            ">;)Z"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iput-object p1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->emails:Ljava/util/List;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setEmails(Ljava/util/Vector;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/Email;",
            ">;)Z"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iput-object p1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->emails:Ljava/util/List;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setIms(Ljava/util/ArrayList;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/Im;",
            ">;)Z"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iput-object p1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->ims:Ljava/util/List;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setIms(Ljava/util/Vector;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/Im;",
            ">;)Z"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iput-object p1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->ims:Ljava/util/List;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setLastTimeContacted(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->lastTimeContacted:J

    return-void
.end method

.method public setNames(Ljava/util/ArrayList;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/StructuredName;",
            ">;)Z"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iput-object p1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->names:Ljava/util/List;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setNames(Ljava/util/Vector;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/StructuredName;",
            ">;)Z"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iput-object p1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->names:Ljava/util/List;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setNicknames(Ljava/util/ArrayList;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/Nickname;",
            ">;)Z"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iput-object p1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->nicknames:Ljava/util/List;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setNicknames(Ljava/util/Vector;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/Nickname;",
            ">;)Z"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iput-object p1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->nicknames:Ljava/util/List;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setNotes(Ljava/util/ArrayList;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/Note;",
            ">;)Z"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iput-object p1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->notes:Ljava/util/List;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setNotes(Ljava/util/Vector;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/Note;",
            ">;)Z"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iput-object p1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->notes:Ljava/util/List;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setOrganizations(Ljava/util/ArrayList;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/Organization;",
            ">;)Z"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iput-object p1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->organizations:Ljava/util/List;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setOrganizations(Ljava/util/Vector;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/Organization;",
            ">;)Z"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iput-object p1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->organizations:Ljava/util/List;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setPhones(Ljava/util/ArrayList;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/Phone;",
            ">;)Z"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iput-object p1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->phones:Ljava/util/List;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setPhones(Ljava/util/Vector;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/Phone;",
            ">;)Z"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iput-object p1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->phones:Ljava/util/List;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setPhotos(Ljava/util/ArrayList;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/Photo;",
            ">;)Z"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iput-object p1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->photos:Ljava/util/List;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setPhotos(Ljava/util/Vector;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/Photo;",
            ">;)Z"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iput-object p1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->photos:Ljava/util/List;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setPostals(Ljava/util/ArrayList;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;",
            ">;)Z"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iput-object p1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->postals:Ljava/util/List;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setPostals(Ljava/util/Vector;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;",
            ">;)Z"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iput-object p1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->postals:Ljava/util/List;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setSendToVoicemail(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->sendToVoicemail:Z

    return-void
.end method

.method public setSimId(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->simId:I

    return-void
.end method

.method public setSimIndex(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->simIndex:I

    return-void
.end method

.method public setSourceLocation(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->sourceLocation:I

    return-void
.end method

.method public setStarred(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->starred:Z

    return-void
.end method

.method public setTimesContacted(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->timesContacted:I

    return-void
.end method

.method public setVersion(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->version:I

    return-void
.end method

.method public setWebsites(Ljava/util/ArrayList;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/Website;",
            ">;)Z"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iput-object p1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->websites:Ljava/util/List;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setWebsites(Ljava/util/Vector;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector",
            "<",
            "Lcom/mediatek/apst/util/entity/contacts/Website;",
            ">;)Z"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iput-object p1, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->websites:Ljava/util/List;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic shallowClone()Lcom/mediatek/apst/util/entity/contacts/BaseContact;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/mediatek/apst/util/entity/contacts/RawContact;->shallowClone()Lcom/mediatek/apst/util/entity/contacts/RawContact;

    move-result-object v0

    return-object v0
.end method

.method public shallowClone()Lcom/mediatek/apst/util/entity/contacts/RawContact;
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-super {p0}, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->clone()Lcom/mediatek/apst/util/entity/contacts/BaseContact;

    move-result-object v0

    check-cast v0, Lcom/mediatek/apst/util/entity/contacts/RawContact;

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    iput-object v11, v0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->names:Ljava/util/List;

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    iput-object v11, v0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->phones:Ljava/util/List;

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    iput-object v11, v0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->photos:Ljava/util/List;

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    iput-object v11, v0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->emails:Ljava/util/List;

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    iput-object v11, v0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->ims:Ljava/util/List;

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    iput-object v11, v0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->postals:Ljava/util/List;

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    iput-object v11, v0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->organizations:Ljava/util/List;

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    iput-object v11, v0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->notes:Ljava/util/List;

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    iput-object v11, v0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->nicknames:Ljava/util/List;

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    iput-object v11, v0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->websites:Ljava/util/List;

    iget-object v11, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->names:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_0

    iget-object v11, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->phones:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_1

    iget-object v11, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->photos:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_2

    iget-object v11, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->emails:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_3
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_3

    iget-object v11, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->ims:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_4
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_4

    iget-object v11, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->postals:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_5
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_5

    iget-object v11, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->organizations:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_6
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_6

    iget-object v11, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->notes:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_7
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_7

    iget-object v11, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->nicknames:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_8
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_8

    iget-object v11, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->websites:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_9
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_9

    return-object v0

    :cond_0
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/apst/util/entity/contacts/StructuredName;

    iget-object v12, v0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->names:Ljava/util/List;

    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/mediatek/apst/util/entity/contacts/Phone;

    iget-object v12, v0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->phones:Ljava/util/List;

    invoke-interface {v12, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/mediatek/apst/util/entity/contacts/Photo;

    iget-object v12, v0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->photos:Ljava/util/List;

    invoke-interface {v12, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/apst/util/entity/contacts/Email;

    iget-object v12, v0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->emails:Ljava/util/List;

    invoke-interface {v12, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_4
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/apst/util/entity/contacts/Im;

    iget-object v12, v0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->ims:Ljava/util/List;

    invoke-interface {v12, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_5
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;

    iget-object v12, v0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->postals:Ljava/util/List;

    invoke-interface {v12, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_6
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/mediatek/apst/util/entity/contacts/Organization;

    iget-object v12, v0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->organizations:Ljava/util/List;

    invoke-interface {v12, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    :cond_7
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/mediatek/apst/util/entity/contacts/Note;

    iget-object v12, v0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->notes:Ljava/util/List;

    invoke-interface {v12, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_7

    :cond_8
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/apst/util/entity/contacts/Nickname;

    iget-object v12, v0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->nicknames:Ljava/util/List;

    invoke-interface {v12, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_8

    :cond_9
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/mediatek/apst/util/entity/contacts/Website;

    iget-object v12, v0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->websites:Ljava/util/List;

    invoke-interface {v12, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_9
.end method

.method public writeRaw(Ljava/nio/ByteBuffer;)V
    .locals 13
    .param p1    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation

    const/4 v12, -0x1

    invoke-super {p0, p1}, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->writeRaw(Ljava/nio/ByteBuffer;)V

    iget-boolean v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->starred:Z

    invoke-static {p1, v10}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putBoolean(Ljava/nio/ByteBuffer;Z)V

    iget-boolean v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->sendToVoicemail:Z

    invoke-static {p1, v10}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putBoolean(Ljava/nio/ByteBuffer;Z)V

    iget v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->version:I

    invoke-virtual {p1, v10}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->names:Ljava/util/List;

    if-eqz v10, :cond_1

    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->names:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    invoke-virtual {p1, v10}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->names:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_0

    :goto_1
    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->phones:Ljava/util/List;

    if-eqz v10, :cond_3

    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->phones:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    invoke-virtual {p1, v10}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->phones:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_2

    :goto_3
    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->photos:Ljava/util/List;

    if-eqz v10, :cond_5

    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->photos:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    invoke-virtual {p1, v10}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->photos:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_4
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_4

    :goto_5
    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->emails:Ljava/util/List;

    if-eqz v10, :cond_7

    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->emails:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    invoke-virtual {p1, v10}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->emails:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_6
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_6

    :goto_7
    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->ims:Ljava/util/List;

    if-eqz v10, :cond_9

    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->ims:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    invoke-virtual {p1, v10}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->ims:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_8
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_8

    :goto_9
    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->postals:Ljava/util/List;

    if-eqz v10, :cond_b

    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->postals:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    invoke-virtual {p1, v10}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->postals:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_a
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_a

    :goto_b
    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->organizations:Ljava/util/List;

    if-eqz v10, :cond_d

    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->organizations:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    invoke-virtual {p1, v10}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->organizations:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_c
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_c

    :goto_d
    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->notes:Ljava/util/List;

    if-eqz v10, :cond_f

    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->notes:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    invoke-virtual {p1, v10}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->notes:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_e
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_e

    :goto_f
    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->nicknames:Ljava/util/List;

    if-eqz v10, :cond_11

    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->nicknames:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    invoke-virtual {p1, v10}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->nicknames:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_10
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_10

    :goto_11
    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->websites:Ljava/util/List;

    if-eqz v10, :cond_13

    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->websites:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    invoke-virtual {p1, v10}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->websites:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_12
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_12

    :goto_13
    iget v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->sourceLocation:I

    invoke-virtual {p1, v10}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    return-void

    :cond_0
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/apst/util/entity/contacts/StructuredName;

    invoke-virtual {v2, p1}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->writeRaw(Ljava/nio/ByteBuffer;)V

    goto/16 :goto_0

    :cond_1
    invoke-virtual {p1, v12}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto/16 :goto_1

    :cond_2
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/mediatek/apst/util/entity/contacts/Phone;

    invoke-virtual {v6, p1}, Lcom/mediatek/apst/util/entity/contacts/Phone;->writeRaw(Ljava/nio/ByteBuffer;)V

    goto/16 :goto_2

    :cond_3
    invoke-virtual {p1, v12}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto/16 :goto_3

    :cond_4
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/mediatek/apst/util/entity/contacts/Photo;

    invoke-virtual {v7, p1}, Lcom/mediatek/apst/util/entity/contacts/Photo;->writeRaw(Ljava/nio/ByteBuffer;)V

    goto/16 :goto_4

    :cond_5
    invoke-virtual {p1, v12}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto/16 :goto_5

    :cond_6
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/apst/util/entity/contacts/Email;

    invoke-virtual {v0, p1}, Lcom/mediatek/apst/util/entity/contacts/Email;->writeRaw(Ljava/nio/ByteBuffer;)V

    goto/16 :goto_6

    :cond_7
    invoke-virtual {p1, v12}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto/16 :goto_7

    :cond_8
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/apst/util/entity/contacts/Im;

    invoke-virtual {v1, p1}, Lcom/mediatek/apst/util/entity/contacts/Im;->writeRaw(Ljava/nio/ByteBuffer;)V

    goto/16 :goto_8

    :cond_9
    invoke-virtual {p1, v12}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto/16 :goto_9

    :cond_a
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;

    invoke-virtual {v8, p1}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->writeRaw(Ljava/nio/ByteBuffer;)V

    goto/16 :goto_a

    :cond_b
    invoke-virtual {p1, v12}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto/16 :goto_b

    :cond_c
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/mediatek/apst/util/entity/contacts/Organization;

    invoke-virtual {v5, p1}, Lcom/mediatek/apst/util/entity/contacts/Organization;->writeRaw(Ljava/nio/ByteBuffer;)V

    goto/16 :goto_c

    :cond_d
    invoke-virtual {p1, v12}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto/16 :goto_d

    :cond_e
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/apst/util/entity/contacts/Note;

    invoke-virtual {v4, p1}, Lcom/mediatek/apst/util/entity/contacts/Note;->writeRaw(Ljava/nio/ByteBuffer;)V

    goto/16 :goto_e

    :cond_f
    invoke-virtual {p1, v12}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto/16 :goto_f

    :cond_10
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/apst/util/entity/contacts/Nickname;

    invoke-virtual {v3, p1}, Lcom/mediatek/apst/util/entity/contacts/Nickname;->writeRaw(Ljava/nio/ByteBuffer;)V

    goto/16 :goto_10

    :cond_11
    invoke-virtual {p1, v12}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto/16 :goto_11

    :cond_12
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/mediatek/apst/util/entity/contacts/Website;

    invoke-virtual {v9, p1}, Lcom/mediatek/apst/util/entity/contacts/Website;->writeRaw(Ljava/nio/ByteBuffer;)V

    goto/16 :goto_12

    :cond_13
    invoke-virtual {p1, v12}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto/16 :goto_13
.end method

.method public writeRawWithVersion(Ljava/nio/ByteBuffer;I)V
    .locals 13
    .param p1    # Ljava/nio/ByteBuffer;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;,
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    const/4 v12, -0x1

    invoke-super {p0, p1, p2}, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->writeRawWithVersion(Ljava/nio/ByteBuffer;I)V

    iget-boolean v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->starred:Z

    invoke-static {p1, v10}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putBoolean(Ljava/nio/ByteBuffer;Z)V

    iget-boolean v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->sendToVoicemail:Z

    invoke-static {p1, v10}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putBoolean(Ljava/nio/ByteBuffer;Z)V

    iget v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->version:I

    invoke-virtual {p1, v10}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->names:Ljava/util/List;

    if-eqz v10, :cond_2

    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->names:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    invoke-virtual {p1, v10}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->names:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_1

    :goto_1
    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->phones:Ljava/util/List;

    if-eqz v10, :cond_4

    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->phones:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    invoke-virtual {p1, v10}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->phones:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_3

    :goto_3
    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->photos:Ljava/util/List;

    if-eqz v10, :cond_6

    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->photos:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    invoke-virtual {p1, v10}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->photos:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_4
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_5

    :goto_5
    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->emails:Ljava/util/List;

    if-eqz v10, :cond_8

    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->emails:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    invoke-virtual {p1, v10}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->emails:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_6
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_7

    :goto_7
    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->ims:Ljava/util/List;

    if-eqz v10, :cond_a

    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->ims:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    invoke-virtual {p1, v10}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->ims:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_8
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_9

    :goto_9
    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->postals:Ljava/util/List;

    if-eqz v10, :cond_c

    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->postals:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    invoke-virtual {p1, v10}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->postals:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_a
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_b

    :goto_b
    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->organizations:Ljava/util/List;

    if-eqz v10, :cond_e

    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->organizations:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    invoke-virtual {p1, v10}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->organizations:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_c
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_d

    :goto_d
    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->notes:Ljava/util/List;

    if-eqz v10, :cond_10

    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->notes:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    invoke-virtual {p1, v10}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->notes:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_e
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_f

    :goto_f
    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->nicknames:Ljava/util/List;

    if-eqz v10, :cond_12

    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->nicknames:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    invoke-virtual {p1, v10}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->nicknames:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_10
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_11

    :goto_11
    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->websites:Ljava/util/List;

    if-eqz v10, :cond_14

    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->websites:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    invoke-virtual {p1, v10}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    iget-object v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->websites:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_12
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_13

    :goto_13
    iget v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->sourceLocation:I

    invoke-virtual {p1, v10}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    const/4 v10, 0x2

    if-lt p2, v10, :cond_0

    iget v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->simId:I

    invoke-virtual {p1, v10}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    :cond_0
    iget v10, p0, Lcom/mediatek/apst/util/entity/contacts/RawContact;->simIndex:I

    invoke-virtual {p1, v10}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    return-void

    :cond_1
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/apst/util/entity/contacts/StructuredName;

    invoke-virtual {v2, p1}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->writeRaw(Ljava/nio/ByteBuffer;)V

    goto/16 :goto_0

    :cond_2
    invoke-virtual {p1, v12}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto/16 :goto_1

    :cond_3
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/mediatek/apst/util/entity/contacts/Phone;

    invoke-virtual {v6, p1, p2}, Lcom/mediatek/apst/util/entity/contacts/Phone;->writeRawWithVersion(Ljava/nio/ByteBuffer;I)V

    goto/16 :goto_2

    :cond_4
    invoke-virtual {p1, v12}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto/16 :goto_3

    :cond_5
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/mediatek/apst/util/entity/contacts/Photo;

    invoke-virtual {v7, p1}, Lcom/mediatek/apst/util/entity/contacts/Photo;->writeRaw(Ljava/nio/ByteBuffer;)V

    goto/16 :goto_4

    :cond_6
    invoke-virtual {p1, v12}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto/16 :goto_5

    :cond_7
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/apst/util/entity/contacts/Email;

    invoke-virtual {v0, p1}, Lcom/mediatek/apst/util/entity/contacts/Email;->writeRaw(Ljava/nio/ByteBuffer;)V

    goto/16 :goto_6

    :cond_8
    invoke-virtual {p1, v12}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto/16 :goto_7

    :cond_9
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/apst/util/entity/contacts/Im;

    invoke-virtual {v1, p1}, Lcom/mediatek/apst/util/entity/contacts/Im;->writeRaw(Ljava/nio/ByteBuffer;)V

    goto/16 :goto_8

    :cond_a
    invoke-virtual {p1, v12}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto/16 :goto_9

    :cond_b
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;

    invoke-virtual {v8, p1}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->writeRaw(Ljava/nio/ByteBuffer;)V

    goto/16 :goto_a

    :cond_c
    invoke-virtual {p1, v12}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto/16 :goto_b

    :cond_d
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/mediatek/apst/util/entity/contacts/Organization;

    invoke-virtual {v5, p1}, Lcom/mediatek/apst/util/entity/contacts/Organization;->writeRaw(Ljava/nio/ByteBuffer;)V

    goto/16 :goto_c

    :cond_e
    invoke-virtual {p1, v12}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto/16 :goto_d

    :cond_f
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/apst/util/entity/contacts/Note;

    invoke-virtual {v4, p1}, Lcom/mediatek/apst/util/entity/contacts/Note;->writeRaw(Ljava/nio/ByteBuffer;)V

    goto/16 :goto_e

    :cond_10
    invoke-virtual {p1, v12}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto/16 :goto_f

    :cond_11
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/apst/util/entity/contacts/Nickname;

    invoke-virtual {v3, p1}, Lcom/mediatek/apst/util/entity/contacts/Nickname;->writeRaw(Ljava/nio/ByteBuffer;)V

    goto/16 :goto_10

    :cond_12
    invoke-virtual {p1, v12}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto/16 :goto_11

    :cond_13
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/mediatek/apst/util/entity/contacts/Website;

    invoke-virtual {v9, p1}, Lcom/mediatek/apst/util/entity/contacts/Website;->writeRaw(Ljava/nio/ByteBuffer;)V

    goto/16 :goto_12

    :cond_14
    invoke-virtual {p1, v12}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto/16 :goto_13
.end method
