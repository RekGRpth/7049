.class public Lcom/mediatek/apst/util/entity/message/Message;
.super Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;
.source "Message.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field public static final BOX_DRAFT:I = 0x3

.field public static final BOX_FAILED:I = 0x5

.field public static final BOX_INBOX:I = 0x1

.field public static final BOX_NONE:I = -0x1

.field public static final BOX_OUTBOX:I = 0x4

.field public static final BOX_QUEUED:I = 0x6

.field public static final BOX_SENT:I = 0x2

.field public static final SIM1_ID:I = 0x0

.field public static final SIM2_ID:I = 0x1

.field public static final SIM_ID:I = 0x0

.field public static final SIM_NONE:I = -0x1

.field public static final TYPE_MMS:I = 0x2

.field public static final TYPE_SMS:I = 0x1

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private box:I

.field private date:J

.field private date_sent:I

.field private locked:Z

.field private read:Z

.field private simICCId:Ljava/lang/String;

.field private simId:I

.field private simName:Ljava/lang/String;

.field private simNumber:Ljava/lang/String;

.field private subject:Ljava/lang/String;

.field private target:Lcom/mediatek/apst/util/entity/message/TargetAddress;

.field private threadId:J


# direct methods
.method public constructor <init>()V
    .locals 2

    const-wide/16 v0, -0x1

    invoke-direct {p0, v0, v1}, Lcom/mediatek/apst/util/entity/message/Message;-><init>(J)V

    return-void
.end method

.method public constructor <init>(J)V
    .locals 3
    .param p1    # J

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;-><init>(J)V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->threadId:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->box:I

    iput-boolean v2, p0, Lcom/mediatek/apst/util/entity/message/Message;->read:Z

    iput-boolean v2, p0, Lcom/mediatek/apst/util/entity/message/Message;->locked:Z

    iput v2, p0, Lcom/mediatek/apst/util/entity/message/Message;->simId:I

    return-void
.end method

.method public static isOutGoing(I)Z
    .locals 1
    .param p0    # I

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    const/4 v0, 0x6

    if-eq p0, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic clone()Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/mediatek/apst/util/entity/message/Message;->clone()Lcom/mediatek/apst/util/entity/message/Message;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/mediatek/apst/util/entity/message/Message;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-super {p0}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->clone()Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;

    move-result-object v0

    check-cast v0, Lcom/mediatek/apst/util/entity/message/Message;

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/message/Message;->target:Lcom/mediatek/apst/util/entity/message/TargetAddress;

    invoke-virtual {v1}, Lcom/mediatek/apst/util/entity/message/TargetAddress;->clone()Lcom/mediatek/apst/util/entity/message/TargetAddress;

    move-result-object v1

    iput-object v1, v0, Lcom/mediatek/apst/util/entity/message/Message;->target:Lcom/mediatek/apst/util/entity/message/TargetAddress;

    return-object v0
.end method

.method public getBox()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->box:I

    return v0
.end method

.method public getDate()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->date:J

    return-wide v0
.end method

.method public getDate_sent()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->date_sent:I

    return v0
.end method

.method public getSimICCId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->simICCId:Ljava/lang/String;

    return-object v0
.end method

.method public getSimId()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->simId:I

    return v0
.end method

.method public getSimName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->simName:Ljava/lang/String;

    return-object v0
.end method

.method public getSimNumber()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->simNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getSubject()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->subject:Ljava/lang/String;

    return-object v0
.end method

.method public getTarget()Lcom/mediatek/apst/util/entity/message/TargetAddress;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->target:Lcom/mediatek/apst/util/entity/message/TargetAddress;

    return-object v0
.end method

.method public getThreadId()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->threadId:J

    return-wide v0
.end method

.method public isLocked()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->locked:Z

    return v0
.end method

.method public isRead()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->read:Z

    return v0
.end method

.method public readRaw(Ljava/nio/ByteBuffer;)V
    .locals 2
    .param p1    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation

    invoke-super {p0, p1}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->readRaw(Ljava/nio/ByteBuffer;)V

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->threadId:J

    new-instance v0, Lcom/mediatek/apst/util/entity/message/TargetAddress;

    invoke-direct {v0}, Lcom/mediatek/apst/util/entity/message/TargetAddress;-><init>()V

    iput-object v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->target:Lcom/mediatek/apst/util/entity/message/TargetAddress;

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->target:Lcom/mediatek/apst/util/entity/message/TargetAddress;

    invoke-virtual {v0, p1}, Lcom/mediatek/apst/util/entity/message/TargetAddress;->readRaw(Ljava/nio/ByteBuffer;)V

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->date:J

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    iput v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->box:I

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getBoolean(Ljava/nio/ByteBuffer;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->read:Z

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->subject:Ljava/lang/String;

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getBoolean(Ljava/nio/ByteBuffer;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->locked:Z

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    iput v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->simId:I

    return-void
.end method

.method public readRawWithVersion(Ljava/nio/ByteBuffer;I)V
    .locals 2
    .param p1    # Ljava/nio/ByteBuffer;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;,
            Ljava/nio/BufferUnderflowException;
        }
    .end annotation

    invoke-super {p0, p1, p2}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->readRawWithVersion(Ljava/nio/ByteBuffer;I)V

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->threadId:J

    new-instance v0, Lcom/mediatek/apst/util/entity/message/TargetAddress;

    invoke-direct {v0}, Lcom/mediatek/apst/util/entity/message/TargetAddress;-><init>()V

    iput-object v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->target:Lcom/mediatek/apst/util/entity/message/TargetAddress;

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->target:Lcom/mediatek/apst/util/entity/message/TargetAddress;

    invoke-virtual {v0, p1}, Lcom/mediatek/apst/util/entity/message/TargetAddress;->readRaw(Ljava/nio/ByteBuffer;)V

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->date:J

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    iput v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->box:I

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getBoolean(Ljava/nio/ByteBuffer;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->read:Z

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->subject:Ljava/lang/String;

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getBoolean(Ljava/nio/ByteBuffer;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->locked:Z

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    iput v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->simId:I

    const/4 v0, 0x2

    if-lt p2, v0, :cond_0

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->simName:Ljava/lang/String;

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->simNumber:Ljava/lang/String;

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->simICCId:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setBox(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/util/entity/message/Message;->box:I

    return-void
.end method

.method public setDate(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/entity/message/Message;->date:J

    return-void
.end method

.method public setDate_sent(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/util/entity/message/Message;->date_sent:I

    return-void
.end method

.method public setLocked(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/apst/util/entity/message/Message;->locked:Z

    return-void
.end method

.method public setRead(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/apst/util/entity/message/Message;->read:Z

    return-void
.end method

.method public setSimICCId(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/message/Message;->simICCId:Ljava/lang/String;

    return-void
.end method

.method public setSimId(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/util/entity/message/Message;->simId:I

    return-void
.end method

.method public setSimName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/message/Message;->simName:Ljava/lang/String;

    return-void
.end method

.method public setSimNumber(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/message/Message;->simNumber:Ljava/lang/String;

    return-void
.end method

.method public setSubject(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/message/Message;->subject:Ljava/lang/String;

    return-void
.end method

.method public setTarget(Lcom/mediatek/apst/util/entity/message/TargetAddress;)V
    .locals 1
    .param p1    # Lcom/mediatek/apst/util/entity/message/TargetAddress;

    if-nez p1, :cond_0

    new-instance v0, Lcom/mediatek/apst/util/entity/message/TargetAddress;

    invoke-direct {v0}, Lcom/mediatek/apst/util/entity/message/TargetAddress;-><init>()V

    iput-object v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->target:Lcom/mediatek/apst/util/entity/message/TargetAddress;

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/mediatek/apst/util/entity/message/Message;->target:Lcom/mediatek/apst/util/entity/message/TargetAddress;

    goto :goto_0
.end method

.method public setThreadId(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/entity/message/Message;->threadId:J

    return-void
.end method

.method public writeRaw(Ljava/nio/ByteBuffer;)V
    .locals 2
    .param p1    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation

    invoke-super {p0, p1}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->writeRaw(Ljava/nio/ByteBuffer;)V

    iget-wide v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->threadId:J

    invoke-virtual {p1, v0, v1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->target:Lcom/mediatek/apst/util/entity/message/TargetAddress;

    invoke-virtual {v0, p1}, Lcom/mediatek/apst/util/entity/message/TargetAddress;->writeRaw(Ljava/nio/ByteBuffer;)V

    iget-wide v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->date:J

    invoke-virtual {p1, v0, v1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    iget v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->box:I

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    iget-boolean v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->read:Z

    invoke-static {p1, v0}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putBoolean(Ljava/nio/ByteBuffer;Z)V

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->subject:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->locked:Z

    invoke-static {p1, v0}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putBoolean(Ljava/nio/ByteBuffer;Z)V

    iget v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->simId:I

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    return-void
.end method

.method public writeRawWithVersion(Ljava/nio/ByteBuffer;I)V
    .locals 2
    .param p1    # Ljava/nio/ByteBuffer;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;,
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    invoke-super {p0, p1, p2}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->writeRawWithVersion(Ljava/nio/ByteBuffer;I)V

    iget-wide v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->threadId:J

    invoke-virtual {p1, v0, v1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->target:Lcom/mediatek/apst/util/entity/message/TargetAddress;

    invoke-virtual {v0, p1}, Lcom/mediatek/apst/util/entity/message/TargetAddress;->writeRaw(Ljava/nio/ByteBuffer;)V

    iget-wide v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->date:J

    invoke-virtual {p1, v0, v1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    iget v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->box:I

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    iget-boolean v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->read:Z

    invoke-static {p1, v0}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putBoolean(Ljava/nio/ByteBuffer;Z)V

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->subject:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->locked:Z

    invoke-static {p1, v0}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putBoolean(Ljava/nio/ByteBuffer;Z)V

    iget v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->simId:I

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    const/4 v0, 0x2

    if-lt p2, v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->simName:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->simNumber:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/message/Message;->simICCId:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
