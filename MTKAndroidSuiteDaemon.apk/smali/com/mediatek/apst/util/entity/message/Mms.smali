.class public Lcom/mediatek/apst/util/entity/message/Mms;
.super Lcom/mediatek/apst/util/entity/message/Message;
.source "Mms.java"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private contentType:Ljava/lang/String;

.field private d_rpt:Ljava/lang/String;

.field private m_cls:Ljava/lang/String;

.field private m_id:Ljava/lang/String;

.field private m_size:Ljava/lang/String;

.field private m_type:Ljava/lang/String;

.field private parts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/apst/util/entity/message/MmsPart;",
            ">;"
        }
    .end annotation
.end field

.field private seen:Ljava/lang/String;

.field private sub_cs:Ljava/lang/String;

.field private tr_id:Ljava/lang/String;

.field private v:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const-wide/16 v0, -0x1

    invoke-direct {p0, v0, v1}, Lcom/mediatek/apst/util/entity/message/Mms;-><init>(J)V

    return-void
.end method

.method public constructor <init>(J)V
    .locals 1
    .param p1    # J

    invoke-direct {p0, p1, p2}, Lcom/mediatek/apst/util/entity/message/Message;-><init>(J)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/apst/util/entity/message/Mms;->parts:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public bridge synthetic clone()Lcom/mediatek/apst/util/entity/message/Message;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/mediatek/apst/util/entity/message/Mms;->clone()Lcom/mediatek/apst/util/entity/message/Mms;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/mediatek/apst/util/entity/message/Mms;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-super {p0}, Lcom/mediatek/apst/util/entity/message/Message;->clone()Lcom/mediatek/apst/util/entity/message/Message;

    move-result-object v0

    check-cast v0, Lcom/mediatek/apst/util/entity/message/Mms;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v0, Lcom/mediatek/apst/util/entity/message/Mms;->parts:Ljava/util/List;

    iget-object v2, p0, Lcom/mediatek/apst/util/entity/message/Mms;->parts:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    return-object v0

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/apst/util/entity/message/MmsPart;

    iget-object v3, v0, Lcom/mediatek/apst/util/entity/message/Mms;->parts:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getContentType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/message/Mms;->contentType:Ljava/lang/String;

    return-object v0
.end method

.method public getD_rpt()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/message/Mms;->d_rpt:Ljava/lang/String;

    return-object v0
.end method

.method public getM_cls()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/message/Mms;->m_cls:Ljava/lang/String;

    return-object v0
.end method

.method public getM_id()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/message/Mms;->m_id:Ljava/lang/String;

    return-object v0
.end method

.method public getM_size()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/message/Mms;->m_size:Ljava/lang/String;

    return-object v0
.end method

.method public getM_type()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/message/Mms;->m_type:Ljava/lang/String;

    return-object v0
.end method

.method public getParts()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/apst/util/entity/message/MmsPart;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/message/Mms;->parts:Ljava/util/List;

    return-object v0
.end method

.method public getSeen()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/message/Mms;->seen:Ljava/lang/String;

    return-object v0
.end method

.method public getSub_cs()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/message/Mms;->sub_cs:Ljava/lang/String;

    return-object v0
.end method

.method public getTr_id()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/message/Mms;->tr_id:Ljava/lang/String;

    return-object v0
.end method

.method public getV()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/message/Mms;->v:Ljava/lang/String;

    return-object v0
.end method

.method public readAllWithVersion(Ljava/nio/ByteBuffer;I)V
    .locals 4
    .param p1    # Ljava/nio/ByteBuffer;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;,
            Ljava/nio/BufferUnderflowException;
        }
    .end annotation

    invoke-super {p0, p1, p2}, Lcom/mediatek/apst/util/entity/message/Message;->readRawWithVersion(Ljava/nio/ByteBuffer;I)V

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/apst/util/entity/message/Mms;->contentType:Ljava/lang/String;

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/apst/util/entity/message/Mms;->m_id:Ljava/lang/String;

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/apst/util/entity/message/Mms;->sub_cs:Ljava/lang/String;

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/apst/util/entity/message/Mms;->m_cls:Ljava/lang/String;

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/apst/util/entity/message/Mms;->m_type:Ljava/lang/String;

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/apst/util/entity/message/Mms;->v:Ljava/lang/String;

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/apst/util/entity/message/Mms;->m_size:Ljava/lang/String;

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/apst/util/entity/message/Mms;->tr_id:Ljava/lang/String;

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/apst/util/entity/message/Mms;->d_rpt:Ljava/lang/String;

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/apst/util/entity/message/Mms;->seen:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v2

    const/4 v3, -0x1

    if-eq v3, v2, :cond_1

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v3, p0, Lcom/mediatek/apst/util/entity/message/Mms;->parts:Ljava/util/List;

    const/4 v0, 0x0

    :goto_0
    if-lt v0, v2, :cond_0

    :goto_1
    return-void

    :cond_0
    new-instance v1, Lcom/mediatek/apst/util/entity/message/MmsPart;

    invoke-direct {v1}, Lcom/mediatek/apst/util/entity/message/MmsPart;-><init>()V

    invoke-virtual {v1, p1, p2}, Lcom/mediatek/apst/util/entity/message/MmsPart;->readRawWithVersion(Ljava/nio/ByteBuffer;I)V

    iget-object v3, p0, Lcom/mediatek/apst/util/entity/message/Mms;->parts:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/mediatek/apst/util/entity/message/Mms;->parts:Ljava/util/List;

    goto :goto_1
.end method

.method public readRawWithVersion(Ljava/nio/ByteBuffer;I)V
    .locals 1
    .param p1    # Ljava/nio/ByteBuffer;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;,
            Ljava/nio/BufferUnderflowException;
        }
    .end annotation

    invoke-super {p0, p1, p2}, Lcom/mediatek/apst/util/entity/message/Message;->readRawWithVersion(Ljava/nio/ByteBuffer;I)V

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/apst/util/entity/message/Mms;->contentType:Ljava/lang/String;

    return-void
.end method

.method public setContentType(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/message/Mms;->contentType:Ljava/lang/String;

    return-void
.end method

.method public setD_rpt(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/message/Mms;->d_rpt:Ljava/lang/String;

    return-void
.end method

.method public setM_cls(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/message/Mms;->m_cls:Ljava/lang/String;

    return-void
.end method

.method public setM_id(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/message/Mms;->m_id:Ljava/lang/String;

    return-void
.end method

.method public setM_size(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/message/Mms;->m_size:Ljava/lang/String;

    return-void
.end method

.method public setM_type(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/message/Mms;->m_type:Ljava/lang/String;

    return-void
.end method

.method public setParts(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/apst/util/entity/message/MmsPart;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/message/Mms;->parts:Ljava/util/List;

    return-void
.end method

.method public setSeen(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/message/Mms;->seen:Ljava/lang/String;

    return-void
.end method

.method public setSub_cs(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/message/Mms;->sub_cs:Ljava/lang/String;

    return-void
.end method

.method public setTr_id(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/message/Mms;->tr_id:Ljava/lang/String;

    return-void
.end method

.method public setV(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/message/Mms;->v:Ljava/lang/String;

    return-void
.end method

.method public writeAllWithVersion(Ljava/nio/ByteBuffer;I)V
    .locals 3
    .param p1    # Ljava/nio/ByteBuffer;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;,
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    invoke-super {p0, p1, p2}, Lcom/mediatek/apst/util/entity/message/Message;->writeRawWithVersion(Ljava/nio/ByteBuffer;I)V

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/message/Mms;->contentType:Ljava/lang/String;

    invoke-static {p1, v1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/message/Mms;->m_id:Ljava/lang/String;

    invoke-static {p1, v1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/message/Mms;->sub_cs:Ljava/lang/String;

    invoke-static {p1, v1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/message/Mms;->m_cls:Ljava/lang/String;

    invoke-static {p1, v1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/message/Mms;->m_type:Ljava/lang/String;

    invoke-static {p1, v1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/message/Mms;->v:Ljava/lang/String;

    invoke-static {p1, v1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/message/Mms;->m_size:Ljava/lang/String;

    invoke-static {p1, v1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/message/Mms;->tr_id:Ljava/lang/String;

    invoke-static {p1, v1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/message/Mms;->d_rpt:Ljava/lang/String;

    invoke-static {p1, v1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/message/Mms;->seen:Ljava/lang/String;

    invoke-static {p1, v1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/message/Mms;->parts:Ljava/util/List;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/message/Mms;->parts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    iget-object v1, p0, Lcom/mediatek/apst/util/entity/message/Mms;->parts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    return-void

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/apst/util/entity/message/MmsPart;

    invoke-virtual {v0, p1, p2}, Lcom/mediatek/apst/util/entity/message/MmsPart;->writeRawWithVersion(Ljava/nio/ByteBuffer;I)V

    goto :goto_0

    :cond_1
    const/4 v1, -0x1

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto :goto_1
.end method

.method public writeRawWithVersion(Ljava/nio/ByteBuffer;I)V
    .locals 1
    .param p1    # Ljava/nio/ByteBuffer;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;,
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    invoke-super {p0, p1, p2}, Lcom/mediatek/apst/util/entity/message/Message;->writeRawWithVersion(Ljava/nio/ByteBuffer;I)V

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/message/Mms;->contentType:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    return-void
.end method
