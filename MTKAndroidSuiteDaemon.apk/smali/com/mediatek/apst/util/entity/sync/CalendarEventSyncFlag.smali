.class public Lcom/mediatek/apst/util/entity/sync/CalendarEventSyncFlag;
.super Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;
.source "CalendarEventSyncFlag.java"


# static fields
.field private static final serialVersionUID:J = 0x2L


# instance fields
.field private calendarId:J

.field private modifyTime:J

.field private timeFrom:J

.field private timeZone:Ljava/lang/String;

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;-><init>()V

    return-void
.end method


# virtual methods
.method public getCalendarId()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/entity/sync/CalendarEventSyncFlag;->calendarId:J

    return-wide v0
.end method

.method public getModifyTime()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/entity/sync/CalendarEventSyncFlag;->modifyTime:J

    return-wide v0
.end method

.method public getTimeFrom()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/entity/sync/CalendarEventSyncFlag;->timeFrom:J

    return-wide v0
.end method

.method public getTimeZone()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/sync/CalendarEventSyncFlag;->timeZone:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/sync/CalendarEventSyncFlag;->title:Ljava/lang/String;

    return-object v0
.end method

.method public readRawWithVersion(Ljava/nio/ByteBuffer;I)V
    .locals 2
    .param p1    # Ljava/nio/ByteBuffer;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;,
            Ljava/nio/BufferUnderflowException;
        }
    .end annotation

    invoke-super {p0, p1, p2}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->readRawWithVersion(Ljava/nio/ByteBuffer;I)V

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mediatek/apst/util/entity/sync/CalendarEventSyncFlag;->modifyTime:J

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mediatek/apst/util/entity/sync/CalendarEventSyncFlag;->calendarId:J

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/apst/util/entity/sync/CalendarEventSyncFlag;->title:Ljava/lang/String;

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/apst/util/entity/sync/CalendarEventSyncFlag;->timeZone:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mediatek/apst/util/entity/sync/CalendarEventSyncFlag;->timeFrom:J

    return-void
.end method

.method public setCalendarId(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/entity/sync/CalendarEventSyncFlag;->calendarId:J

    return-void
.end method

.method public setModifyTime(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/entity/sync/CalendarEventSyncFlag;->modifyTime:J

    return-void
.end method

.method public setTimeFrom(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/entity/sync/CalendarEventSyncFlag;->timeFrom:J

    return-void
.end method

.method public setTimeZone(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/sync/CalendarEventSyncFlag;->timeZone:Ljava/lang/String;

    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/sync/CalendarEventSyncFlag;->title:Ljava/lang/String;

    return-void
.end method

.method public writeRawWithVersion(Ljava/nio/ByteBuffer;I)V
    .locals 2
    .param p1    # Ljava/nio/ByteBuffer;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;,
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    invoke-super {p0, p1, p2}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->writeRawWithVersion(Ljava/nio/ByteBuffer;I)V

    iget-wide v0, p0, Lcom/mediatek/apst/util/entity/sync/CalendarEventSyncFlag;->modifyTime:J

    invoke-virtual {p1, v0, v1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    iget-wide v0, p0, Lcom/mediatek/apst/util/entity/sync/CalendarEventSyncFlag;->calendarId:J

    invoke-virtual {p1, v0, v1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/sync/CalendarEventSyncFlag;->title:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/sync/CalendarEventSyncFlag;->timeZone:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    iget-wide v0, p0, Lcom/mediatek/apst/util/entity/sync/CalendarEventSyncFlag;->timeFrom:J

    invoke-virtual {p1, v0, v1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    return-void
.end method
