.class Lcom/mediatek/thermalmanager/ThermalEventRepeater$1;
.super Landroid/os/UEventObserver;
.source "ThermalEventRepeater.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/thermalmanager/ThermalEventRepeater;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/thermalmanager/ThermalEventRepeater;


# direct methods
.method constructor <init>(Lcom/mediatek/thermalmanager/ThermalEventRepeater;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/thermalmanager/ThermalEventRepeater$1;->this$0:Lcom/mediatek/thermalmanager/ThermalEventRepeater;

    invoke-direct {p0}, Landroid/os/UEventObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onUEvent(Landroid/os/UEventObserver$UEvent;)V
    .locals 16
    .param p1    # Landroid/os/UEventObserver$UEvent;

    const-string v13, "thermalmanager.ThermalEventRepeater"

    const-string v14, "mThermalEventObserver.onUEvent()\n"

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v13, "SHUTDOWN"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_0

    const-string v13, "1"

    const-string v14, "SHUTDOWN"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    const/4 v11, 0x1

    :goto_0
    const-string v13, "thermalmanager.ThermalEventRepeater"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "mThermalEventObserver.onUEvent() shutdown="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "\n"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v13, 0x1

    if-ne v13, v11, :cond_0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/thermalmanager/ThermalEventRepeater$1;->this$0:Lcom/mediatek/thermalmanager/ThermalEventRepeater;

    invoke-static {v13}, Lcom/mediatek/thermalmanager/ThermalEventRepeater;->access$000(Lcom/mediatek/thermalmanager/ThermalEventRepeater;)Z

    move-result v13

    if-nez v13, :cond_0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/thermalmanager/ThermalEventRepeater$1;->this$0:Lcom/mediatek/thermalmanager/ThermalEventRepeater;

    const/4 v14, 0x1

    invoke-static {v13, v14}, Lcom/mediatek/thermalmanager/ThermalEventRepeater;->access$002(Lcom/mediatek/thermalmanager/ThermalEventRepeater;Z)Z

    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/thermalmanager/ThermalEventRepeater$1;->this$0:Lcom/mediatek/thermalmanager/ThermalEventRepeater;

    invoke-static {v13}, Lcom/mediatek/thermalmanager/ThermalEventRepeater;->access$100(Lcom/mediatek/thermalmanager/ThermalEventRepeater;)Landroid/content/Context;

    move-result-object v13

    const-class v14, Lcom/mediatek/thermalmanager/ShutDownAlertDialogActivity;

    invoke-virtual {v4, v13, v14}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const/high16 v13, 0x10000000

    invoke-virtual {v4, v13}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/thermalmanager/ThermalEventRepeater$1;->this$0:Lcom/mediatek/thermalmanager/ThermalEventRepeater;

    invoke-virtual {v13, v4}, Landroid/content/ContextWrapper;->startActivity(Landroid/content/Intent;)V

    :cond_0
    const-string v13, "SPKRVOL"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_1

    const-string v13, "SPKRVOL"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    const-string v13, "thermalmanager.ThermalEventRepeater"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "mThermalEventObserver.onUEvent() SPKRVOL="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "SPKRVOL"

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "\n"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v13, "thermalmanager.ThermalEventRepeater"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "mThermalEventObserver.onUEvent() spkrVolumeLimit="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "\n"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v13, 0x46

    if-ne v13, v12, :cond_1

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/thermalmanager/ThermalEventRepeater$1;->this$0:Lcom/mediatek/thermalmanager/ThermalEventRepeater;

    invoke-static {v13}, Lcom/mediatek/thermalmanager/ThermalEventRepeater;->access$100(Lcom/mediatek/thermalmanager/ThermalEventRepeater;)Landroid/content/Context;

    move-result-object v13

    const-string v14, "audio"

    invoke-virtual {v13, v14}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    const/4 v13, 0x3

    invoke-virtual {v1, v13}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v6

    const-string v13, "thermalmanager.ThermalEventRepeater"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "mThermalEventObserver.onUEvent() MaxVol="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "\n"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v13, 0x3

    invoke-virtual {v1, v13}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v2

    const-string v13, "thermalmanager.ThermalEventRepeater"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "mThermalEventObserver.onUEvent() curVol="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "\n"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    mul-int/lit8 v13, v6, 0x7

    div-int/lit8 v5, v13, 0xa

    const-string v13, "thermalmanager.ThermalEventRepeater"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "mThermalEventObserver.onUEvent() limitVol="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "\n"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-le v2, v5, :cond_1

    const/4 v13, 0x3

    const/4 v14, 0x0

    invoke-virtual {v1, v13, v5, v14}, Landroid/media/AudioManager;->setStreamVolume(III)V

    const-string v13, "thermalmanager.ThermalEventRepeater"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "mThermalEventObserver.onUEvent() limit to "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "\n"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const-string v13, "BACKLIGHT"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_3

    const/4 v8, 0x0

    const-string v13, "BACKLIGHT"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    const/4 v9, 0x0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/thermalmanager/ThermalEventRepeater$1;->this$0:Lcom/mediatek/thermalmanager/ThermalEventRepeater;

    invoke-virtual {v13}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    const-string v14, "screen_brightness"

    invoke-static {v13, v14}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v9

    const-string v13, "thermalmanager.ThermalEventRepeater"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "mThermalEventObserver.onUEvent() nCurrentBrightness="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "\n"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    packed-switch v7, :pswitch_data_0

    const-string v13, "thermalmanager.ThermalEventRepeater"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "ERROR BACKLIGHT Level:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "\n"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v8, v9

    :goto_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/thermalmanager/ThermalEventRepeater$1;->this$0:Lcom/mediatek/thermalmanager/ThermalEventRepeater;

    invoke-static {v13}, Lcom/mediatek/thermalmanager/ThermalEventRepeater;->access$200(Lcom/mediatek/thermalmanager/ThermalEventRepeater;)I

    move-result v13

    if-eq v13, v8, :cond_3

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/thermalmanager/ThermalEventRepeater$1;->this$0:Lcom/mediatek/thermalmanager/ThermalEventRepeater;

    invoke-virtual {v13}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    const-string v14, "screen_brightness"

    invoke-static {v13, v14, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    :try_start_1
    const-string v13, "power"

    invoke-static {v13}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v13

    invoke-static {v13}, Landroid/os/IPowerManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/IPowerManager;

    move-result-object v10

    if-eqz v10, :cond_2

    const-string v13, "thermalmanager.ThermalEventRepeater"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "mThermalEventObserver.onUEvent() Set Brightness nBrightnessLimit="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "\n"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v10, v8}, Landroid/os/IPowerManager;->setMaxBrightness(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_2
    :goto_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/thermalmanager/ThermalEventRepeater$1;->this$0:Lcom/mediatek/thermalmanager/ThermalEventRepeater;

    invoke-static {v13, v8}, Lcom/mediatek/thermalmanager/ThermalEventRepeater;->access$202(Lcom/mediatek/thermalmanager/ThermalEventRepeater;I)I

    :cond_3
    return-void

    :cond_4
    const/4 v11, 0x0

    goto/16 :goto_0

    :catch_0
    move-exception v3

    const-string v13, "thermalmanager.ThermalEventRepeater"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "ERROR Read Current Brightness Error:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "\n"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v9, 0x0

    goto/16 :goto_1

    :pswitch_0
    const/4 v8, -0x1

    goto :goto_2

    :pswitch_1
    const/16 v8, 0xb2

    goto :goto_2

    :pswitch_2
    const/16 v8, 0x66

    goto :goto_2

    :pswitch_3
    const/16 v8, 0x19

    goto :goto_2

    :catch_1
    move-exception v13

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
