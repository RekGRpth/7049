.class public Lcom/mediatek/thermalmanager/ThermalEventRepeater;
.super Landroid/app/Service;
.source "ThermalEventRepeater.java"


# static fields
.field static final LOG_TAG:Ljava/lang/String; = "thermalmanager.ThermalEventRepeater"


# instance fields
.field private mBrightnessLimit:I

.field private mContext:Landroid/content/Context;

.field private mShutdown:Z

.field private mThermalEventObserver:Landroid/os/UEventObserver;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/thermalmanager/ThermalEventRepeater;->mShutdown:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/thermalmanager/ThermalEventRepeater;->mBrightnessLimit:I

    new-instance v0, Lcom/mediatek/thermalmanager/ThermalEventRepeater$1;

    invoke-direct {v0, p0}, Lcom/mediatek/thermalmanager/ThermalEventRepeater$1;-><init>(Lcom/mediatek/thermalmanager/ThermalEventRepeater;)V

    iput-object v0, p0, Lcom/mediatek/thermalmanager/ThermalEventRepeater;->mThermalEventObserver:Landroid/os/UEventObserver;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/thermalmanager/ThermalEventRepeater;)Z
    .locals 1
    .param p0    # Lcom/mediatek/thermalmanager/ThermalEventRepeater;

    iget-boolean v0, p0, Lcom/mediatek/thermalmanager/ThermalEventRepeater;->mShutdown:Z

    return v0
.end method

.method static synthetic access$002(Lcom/mediatek/thermalmanager/ThermalEventRepeater;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/thermalmanager/ThermalEventRepeater;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/thermalmanager/ThermalEventRepeater;->mShutdown:Z

    return p1
.end method

.method static synthetic access$100(Lcom/mediatek/thermalmanager/ThermalEventRepeater;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/mediatek/thermalmanager/ThermalEventRepeater;

    iget-object v0, p0, Lcom/mediatek/thermalmanager/ThermalEventRepeater;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/thermalmanager/ThermalEventRepeater;)I
    .locals 1
    .param p0    # Lcom/mediatek/thermalmanager/ThermalEventRepeater;

    iget v0, p0, Lcom/mediatek/thermalmanager/ThermalEventRepeater;->mBrightnessLimit:I

    return v0
.end method

.method static synthetic access$202(Lcom/mediatek/thermalmanager/ThermalEventRepeater;I)I
    .locals 0
    .param p0    # Lcom/mediatek/thermalmanager/ThermalEventRepeater;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/thermalmanager/ThermalEventRepeater;->mBrightnessLimit:I

    return p1
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1    # Landroid/content/Intent;

    const-string v0, "thermalmanager.ThermalEventRepeater"

    const-string v1, "onBind() do nothing."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    const-string v0, "thermalmanager.ThermalEventRepeater"

    const-string v1, "onCreate() start observing."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/thermalmanager/ThermalEventRepeater;->mThermalEventObserver:Landroid/os/UEventObserver;

    const-string v1, "SUBSYSTEM=thermal"

    invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/thermalmanager/ThermalEventRepeater;->mContext:Landroid/content/Context;

    return-void
.end method
