.class public Lcom/mediatek/thermalmanager/ThermalSensorActivity;
.super Landroid/app/Activity;
.source "ThermalSensorActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/thermalmanager/ThermalSensorActivity$1;,
        Lcom/mediatek/thermalmanager/ThermalSensorActivity$ThermalSensorsLoader;
    }
.end annotation


# instance fields
.field private fileNameList:[Ljava/lang/String;

.field items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private thermal_sensors_lv:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/thermalmanager/ThermalSensorActivity;)Landroid/widget/ListView;
    .locals 1
    .param p0    # Lcom/mediatek/thermalmanager/ThermalSensorActivity;

    iget-object v0, p0, Lcom/mediatek/thermalmanager/ThermalSensorActivity;->thermal_sensors_lv:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/thermalmanager/ThermalSensorActivity;)[Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/thermalmanager/ThermalSensorActivity;

    iget-object v0, p0, Lcom/mediatek/thermalmanager/ThermalSensorActivity;->fileNameList:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$202(Lcom/mediatek/thermalmanager/ThermalSensorActivity;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/thermalmanager/ThermalSensorActivity;
    .param p1    # [Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/thermalmanager/ThermalSensorActivity;->fileNameList:[Ljava/lang/String;

    return-object p1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030002

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    const v0, 0x7f05000d

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/mediatek/thermalmanager/ThermalSensorActivity;->thermal_sensors_lv:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/mediatek/thermalmanager/ThermalSensorActivity;->thermal_sensors_lv:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v1, Lcom/mediatek/thermalmanager/TzDeviceActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v1, "tz_sysfs_path"

    iget-object v2, p0, Lcom/mediatek/thermalmanager/ThermalSensorActivity;->fileNameList:[Ljava/lang/String;

    aget-object v2, v2, p3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/thermalmanager/ThermalSensorActivity;->items:Ljava/util/List;

    new-instance v0, Lcom/mediatek/thermalmanager/ThermalSensorActivity$ThermalSensorsLoader;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/mediatek/thermalmanager/ThermalSensorActivity$ThermalSensorsLoader;-><init>(Lcom/mediatek/thermalmanager/ThermalSensorActivity;Lcom/mediatek/thermalmanager/ThermalSensorActivity$1;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method
