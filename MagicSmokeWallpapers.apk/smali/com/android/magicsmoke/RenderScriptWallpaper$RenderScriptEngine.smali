.class Lcom/android/magicsmoke/RenderScriptWallpaper$RenderScriptEngine;
.super Landroid/service/wallpaper/WallpaperService$Engine;
.source "RenderScriptWallpaper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/magicsmoke/RenderScriptWallpaper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RenderScriptEngine"
.end annotation


# instance fields
.field private mRenderer:Lcom/android/magicsmoke/RenderScriptScene;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private mRs:Landroid/renderscript/RenderScriptGL;

.field final synthetic this$0:Lcom/android/magicsmoke/RenderScriptWallpaper;


# direct methods
.method private constructor <init>(Lcom/android/magicsmoke/RenderScriptWallpaper;)V
    .locals 0

    iput-object p1, p0, Lcom/android/magicsmoke/RenderScriptWallpaper$RenderScriptEngine;->this$0:Lcom/android/magicsmoke/RenderScriptWallpaper;

    invoke-direct {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;-><init>(Landroid/service/wallpaper/WallpaperService;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/magicsmoke/RenderScriptWallpaper;Lcom/android/magicsmoke/RenderScriptWallpaper$1;)V
    .locals 0
    .param p1    # Lcom/android/magicsmoke/RenderScriptWallpaper;
    .param p2    # Lcom/android/magicsmoke/RenderScriptWallpaper$1;

    invoke-direct {p0, p1}, Lcom/android/magicsmoke/RenderScriptWallpaper$RenderScriptEngine;-><init>(Lcom/android/magicsmoke/RenderScriptWallpaper;)V

    return-void
.end method

.method private destroyRenderer()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/magicsmoke/RenderScriptWallpaper$RenderScriptEngine;->mRenderer:Lcom/android/magicsmoke/RenderScriptScene;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/magicsmoke/RenderScriptWallpaper$RenderScriptEngine;->mRenderer:Lcom/android/magicsmoke/RenderScriptScene;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/magicsmoke/RenderScriptScene;->stop(Z)V

    iput-object v2, p0, Lcom/android/magicsmoke/RenderScriptWallpaper$RenderScriptEngine;->mRenderer:Lcom/android/magicsmoke/RenderScriptScene;

    :cond_0
    iget-object v0, p0, Lcom/android/magicsmoke/RenderScriptWallpaper$RenderScriptEngine;->mRs:Landroid/renderscript/RenderScriptGL;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/magicsmoke/RenderScriptWallpaper$RenderScriptEngine;->mRs:Landroid/renderscript/RenderScriptGL;

    invoke-virtual {v0, v2, v3, v3}, Landroid/renderscript/RenderScriptGL;->setSurface(Landroid/view/SurfaceHolder;II)V

    iget-object v0, p0, Lcom/android/magicsmoke/RenderScriptWallpaper$RenderScriptEngine;->mRs:Landroid/renderscript/RenderScriptGL;

    invoke-virtual {v0}, Landroid/renderscript/RenderScript;->destroy()V

    iput-object v2, p0, Lcom/android/magicsmoke/RenderScriptWallpaper$RenderScriptEngine;->mRs:Landroid/renderscript/RenderScriptGL;

    :cond_1
    return-void
.end method


# virtual methods
.method public onCommand(Ljava/lang/String;IIILandroid/os/Bundle;Z)Landroid/os/Bundle;
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Landroid/os/Bundle;
    .param p6    # Z

    iget-object v0, p0, Lcom/android/magicsmoke/RenderScriptWallpaper$RenderScriptEngine;->mRenderer:Lcom/android/magicsmoke/RenderScriptScene;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/magicsmoke/RenderScriptWallpaper$RenderScriptEngine;->mRenderer:Lcom/android/magicsmoke/RenderScriptScene;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/android/magicsmoke/RenderScriptScene;->onCommand(Ljava/lang/String;IIILandroid/os/Bundle;Z)Landroid/os/Bundle;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/view/SurfaceHolder;)V
    .locals 1
    .param p1    # Landroid/view/SurfaceHolder;

    invoke-super {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;->onCreate(Landroid/view/SurfaceHolder;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/service/wallpaper/WallpaperService$Engine;->setTouchEventsEnabled(Z)V

    invoke-interface {p1}, Landroid/view/SurfaceHolder;->setSizeFromLayout()V

    const/4 v0, 0x4

    invoke-interface {p1, v0}, Landroid/view/SurfaceHolder;->setFormat(I)V

    return-void
.end method

.method public onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/service/wallpaper/WallpaperService$Engine;->onDestroy()V

    invoke-direct {p0}, Lcom/android/magicsmoke/RenderScriptWallpaper$RenderScriptEngine;->destroyRenderer()V

    return-void
.end method

.method public onOffsetsChanged(FFFFII)V
    .locals 7
    .param p1    # F
    .param p2    # F
    .param p3    # F
    .param p4    # F
    .param p5    # I
    .param p6    # I

    iget-object v0, p0, Lcom/android/magicsmoke/RenderScriptWallpaper$RenderScriptEngine;->mRenderer:Lcom/android/magicsmoke/RenderScriptScene;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/android/magicsmoke/RenderScriptScene;->setOffset(FFFFII)V

    return-void
.end method

.method public onSurfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 4
    .param p1    # Landroid/view/SurfaceHolder;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-super {p0, p1, p2, p3, p4}, Landroid/service/wallpaper/WallpaperService$Engine;->onSurfaceChanged(Landroid/view/SurfaceHolder;III)V

    iget-object v0, p0, Lcom/android/magicsmoke/RenderScriptWallpaper$RenderScriptEngine;->mRs:Landroid/renderscript/RenderScriptGL;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/magicsmoke/RenderScriptWallpaper$RenderScriptEngine;->mRs:Landroid/renderscript/RenderScriptGL;

    invoke-virtual {v0, p1, p3, p4}, Landroid/renderscript/RenderScriptGL;->setSurface(Landroid/view/SurfaceHolder;II)V

    :cond_0
    iget-object v0, p0, Lcom/android/magicsmoke/RenderScriptWallpaper$RenderScriptEngine;->mRenderer:Lcom/android/magicsmoke/RenderScriptScene;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/magicsmoke/RenderScriptWallpaper$RenderScriptEngine;->this$0:Lcom/android/magicsmoke/RenderScriptWallpaper;

    invoke-virtual {v0, p3, p4}, Lcom/android/magicsmoke/RenderScriptWallpaper;->createScene(II)Lcom/android/magicsmoke/RenderScriptScene;

    move-result-object v0

    iput-object v0, p0, Lcom/android/magicsmoke/RenderScriptWallpaper$RenderScriptEngine;->mRenderer:Lcom/android/magicsmoke/RenderScriptScene;

    iget-object v0, p0, Lcom/android/magicsmoke/RenderScriptWallpaper$RenderScriptEngine;->mRenderer:Lcom/android/magicsmoke/RenderScriptScene;

    iget-object v1, p0, Lcom/android/magicsmoke/RenderScriptWallpaper$RenderScriptEngine;->mRs:Landroid/renderscript/RenderScriptGL;

    iget-object v2, p0, Lcom/android/magicsmoke/RenderScriptWallpaper$RenderScriptEngine;->this$0:Lcom/android/magicsmoke/RenderScriptWallpaper;

    invoke-virtual {v2}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p0}, Landroid/service/wallpaper/WallpaperService$Engine;->isPreview()Z

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/magicsmoke/RenderScriptScene;->init(Landroid/renderscript/RenderScriptGL;Landroid/content/res/Resources;Z)V

    iget-object v0, p0, Lcom/android/magicsmoke/RenderScriptWallpaper$RenderScriptEngine;->mRenderer:Lcom/android/magicsmoke/RenderScriptScene;

    invoke-virtual {v0}, Lcom/android/magicsmoke/RenderScriptScene;->start()V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/magicsmoke/RenderScriptWallpaper$RenderScriptEngine;->mRenderer:Lcom/android/magicsmoke/RenderScriptScene;

    invoke-virtual {v0, p3, p4}, Lcom/android/magicsmoke/RenderScriptScene;->resize(II)V

    goto :goto_0
.end method

.method public onSurfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 3
    .param p1    # Landroid/view/SurfaceHolder;

    invoke-super {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;->onSurfaceCreated(Landroid/view/SurfaceHolder;)V

    new-instance v0, Landroid/renderscript/RenderScriptGL$SurfaceConfig;

    invoke-direct {v0}, Landroid/renderscript/RenderScriptGL$SurfaceConfig;-><init>()V

    new-instance v1, Landroid/renderscript/RenderScriptGL;

    iget-object v2, p0, Lcom/android/magicsmoke/RenderScriptWallpaper$RenderScriptEngine;->this$0:Lcom/android/magicsmoke/RenderScriptWallpaper;

    invoke-direct {v1, v2, v0}, Landroid/renderscript/RenderScriptGL;-><init>(Landroid/content/Context;Landroid/renderscript/RenderScriptGL$SurfaceConfig;)V

    iput-object v1, p0, Lcom/android/magicsmoke/RenderScriptWallpaper$RenderScriptEngine;->mRs:Landroid/renderscript/RenderScriptGL;

    iget-object v1, p0, Lcom/android/magicsmoke/RenderScriptWallpaper$RenderScriptEngine;->mRs:Landroid/renderscript/RenderScriptGL;

    sget-object v2, Landroid/renderscript/RenderScript$Priority;->LOW:Landroid/renderscript/RenderScript$Priority;

    invoke-virtual {v1, v2}, Landroid/renderscript/RenderScript;->setPriority(Landroid/renderscript/RenderScript$Priority;)V

    return-void
.end method

.method public onSurfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 0
    .param p1    # Landroid/view/SurfaceHolder;

    invoke-super {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;->onSurfaceDestroyed(Landroid/view/SurfaceHolder;)V

    invoke-direct {p0}, Lcom/android/magicsmoke/RenderScriptWallpaper$RenderScriptEngine;->destroyRenderer()V

    return-void
.end method

.method public onVisibilityChanged(Z)V
    .locals 2
    .param p1    # Z

    invoke-super {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;->onVisibilityChanged(Z)V

    iget-object v0, p0, Lcom/android/magicsmoke/RenderScriptWallpaper$RenderScriptEngine;->mRenderer:Lcom/android/magicsmoke/RenderScriptScene;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/magicsmoke/RenderScriptWallpaper$RenderScriptEngine;->mRenderer:Lcom/android/magicsmoke/RenderScriptScene;

    invoke-virtual {v0}, Lcom/android/magicsmoke/RenderScriptScene;->start()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/magicsmoke/RenderScriptWallpaper$RenderScriptEngine;->mRenderer:Lcom/android/magicsmoke/RenderScriptScene;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/magicsmoke/RenderScriptScene;->stop(Z)V

    goto :goto_0
.end method
