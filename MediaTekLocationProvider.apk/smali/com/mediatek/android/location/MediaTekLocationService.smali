.class public Lcom/mediatek/android/location/MediaTekLocationService;
.super Landroid/app/Service;


# static fields
.field private static final d:Z


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;

.field private c:Lcom/android/location/provider/mediatek/MediaTekGeocodeProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, "LocationService"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/mediatek/android/location/MediaTekLocationService;->d:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    const-string v0, "MediaTekLocationService"

    iput-object v0, p0, Lcom/mediatek/android/location/MediaTekLocationService;->a:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 4

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    sget-boolean v1, Lcom/mediatek/android/location/MediaTekLocationService;->d:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/android/location/MediaTekLocationService;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "On Bind"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v1, "com.android.location.service.NetworkLocationProvider"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-boolean v0, Lcom/mediatek/android/location/MediaTekLocationService;->d:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/android/location/MediaTekLocationService;->a:Ljava/lang/String;

    const-string v1, "NetworkLocationProvider is binding"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const-string v0, "GEMINI_SUPPORT"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iget-object v1, p0, Lcom/mediatek/android/location/MediaTekLocationService;->b:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;

    invoke-virtual {v1, v0}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->setGEMINISUPPORT(Z)V

    iget-object v0, p0, Lcom/mediatek/android/location/MediaTekLocationService;->b:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;

    invoke-virtual {v0}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_2
    const-string v1, "com.android.location.service.GeocodeProvider"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-boolean v0, Lcom/mediatek/android/location/MediaTekLocationService;->d:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/mediatek/android/location/MediaTekLocationService;->a:Ljava/lang/String;

    const-string v1, "GeocodeProvider is binding"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-object v0, p0, Lcom/mediatek/android/location/MediaTekLocationService;->c:Lcom/android/location/provider/mediatek/MediaTekGeocodeProvider;

    invoke-virtual {v0}, Lcom/android/location/provider/mediatek/MediaTekGeocodeProvider;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 2

    sget-boolean v0, Lcom/mediatek/android/location/MediaTekLocationService;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/android/location/MediaTekLocationService;->a:Ljava/lang/String;

    const-string v1, "OnCreate() is called"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;

    invoke-direct {v0, p0}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/android/location/MediaTekLocationService;->b:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;

    new-instance v0, Lcom/android/location/provider/mediatek/MediaTekGeocodeProvider;

    invoke-direct {v0, p0}, Lcom/android/location/provider/mediatek/MediaTekGeocodeProvider;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/android/location/MediaTekLocationService;->c:Lcom/android/location/provider/mediatek/MediaTekGeocodeProvider;

    return-void
.end method
