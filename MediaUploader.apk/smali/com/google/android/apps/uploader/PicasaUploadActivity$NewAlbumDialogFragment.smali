.class Lcom/google/android/apps/uploader/PicasaUploadActivity$NewAlbumDialogFragment;
.super Landroid/app/DialogFragment;
.source "PicasaUploadActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/uploader/PicasaUploadActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NewAlbumDialogFragment"
.end annotation


# instance fields
.field private mAccessRadioGroup:Landroid/widget/RadioGroup;

.field private mAlbumTitleEditText:Landroid/widget/EditText;

.field private mCancelButton:Landroid/widget/Button;

.field private mCreateButton:Landroid/widget/Button;

.field final synthetic this$0:Lcom/google/android/apps/uploader/PicasaUploadActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/uploader/PicasaUploadActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity$NewAlbumDialogFragment;->this$0:Lcom/google/android/apps/uploader/PicasaUploadActivity;

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/uploader/PicasaUploadActivity;Lcom/google/android/apps/uploader/PicasaUploadActivity$1;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/uploader/PicasaUploadActivity;
    .param p2    # Lcom/google/android/apps/uploader/PicasaUploadActivity$1;

    invoke-direct {p0, p1}, Lcom/google/android/apps/uploader/PicasaUploadActivity$NewAlbumDialogFragment;-><init>(Lcom/google/android/apps/uploader/PicasaUploadActivity;)V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2
    .param p1    # Landroid/text/Editable;

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity$NewAlbumDialogFragment;->mCreateButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity$NewAlbumDialogFragment;->mAlbumTitleEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity$NewAlbumDialogFragment;->mCreateButton:Landroid/widget/Button;

    if-ne p1, v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity$NewAlbumDialogFragment;->mAlbumTitleEditText:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity$NewAlbumDialogFragment;->mAccessRadioGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v3}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v1

    const v3, 0x7f05000c

    if-ne v1, v3, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity$NewAlbumDialogFragment;->this$0:Lcom/google/android/apps/uploader/PicasaUploadActivity;

    # invokes: Lcom/google/android/apps/uploader/PicasaUploadActivity;->addNewAlbum(Ljava/lang/String;I)V
    invoke-static {v3, v2, v0}, Lcom/google/android/apps/uploader/PicasaUploadActivity;->access$100(Lcom/google/android/apps/uploader/PicasaUploadActivity;Ljava/lang/String;I)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/uploader/PicasaUploadActivity$NewAlbumDialogFragment;->dismiss()V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    const v1, 0x7f060020

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(I)V

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const v1, 0x7f030001

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f05000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity$NewAlbumDialogFragment;->mAlbumTitleEditText:Landroid/widget/EditText;

    const v1, 0x7f05000b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioGroup;

    iput-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity$NewAlbumDialogFragment;->mAccessRadioGroup:Landroid/widget/RadioGroup;

    const v1, 0x7f05000f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity$NewAlbumDialogFragment;->mCreateButton:Landroid/widget/Button;

    const v1, 0x7f05000e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity$NewAlbumDialogFragment;->mCancelButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity$NewAlbumDialogFragment;->mCreateButton:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity$NewAlbumDialogFragment;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity$NewAlbumDialogFragment;->mAlbumTitleEditText:Landroid/widget/EditText;

    invoke-virtual {v1, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity$NewAlbumDialogFragment;->mAlbumTitleEditText:Landroid/widget/EditText;

    new-instance v2, Lcom/google/android/apps/uploader/PicasaUploadActivity$NewAlbumDialogFragment$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/uploader/PicasaUploadActivity$NewAlbumDialogFragment$1;-><init>(Lcom/google/android/apps/uploader/PicasaUploadActivity$NewAlbumDialogFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-object v0
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method
