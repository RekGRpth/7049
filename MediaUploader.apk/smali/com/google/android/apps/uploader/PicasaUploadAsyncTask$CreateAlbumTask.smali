.class Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$CreateAlbumTask;
.super Landroid/os/AsyncTask;
.source "PicasaUploadAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CreateAlbumTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private final mAccess:I

.field private final mAccount:Ljava/lang/String;

.field private mAlbumId:J

.field private final mAlbumTitle:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;


# direct methods
.method constructor <init>(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # I

    iput-object p1, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$CreateAlbumTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$CreateAlbumTask;->mAccount:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$CreateAlbumTask;->mAlbumTitle:Ljava/lang/String;

    iput p4, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$CreateAlbumTask;->mAccess:I

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Landroid/database/Cursor;
    .locals 14
    .param p1    # [Ljava/lang/Void;

    const/4 v13, 0x0

    new-instance v12, Landroid/content/ContentValues;

    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$CreateAlbumTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mApi:Lcom/google/android/picasasync/PicasaApi;
    invoke-static {v1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$1400(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Lcom/google/android/picasasync/PicasaApi;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$CreateAlbumTask;->mAccount:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$CreateAlbumTask;->mAlbumTitle:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$CreateAlbumTask;->mAccess:I

    invoke-virtual {v1, v2, v3, v4, v12}, Lcom/google/android/picasasync/PicasaApi;->createAlbum(Ljava/lang/String;Ljava/lang/String;ILandroid/content/ContentValues;)I

    move-result v10

    if-eqz v10, :cond_0

    move-object v9, v13

    :goto_0
    return-object v9

    :cond_0
    new-instance v8, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;

    invoke-direct {v8}, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;-><init>()V

    const-string v1, "_id"

    invoke-virtual {v12, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$CreateAlbumTask;->mAlbumId:J

    iput-wide v1, v8, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;->id:J

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$CreateAlbumTask;->mAccount:Ljava/lang/String;

    iput-object v1, v8, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;->account:Ljava/lang/String;

    const-string v1, "title"

    invoke-virtual {v12, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v8, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;->title:Ljava/lang/String;

    const-string v1, "num_photos"

    invoke-virtual {v12, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v8, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;->photoCount:I

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$CreateAlbumTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mDbHelper:Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper;
    invoke-static {v1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$700(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    const/4 v9, 0x0

    :try_start_0
    sget-object v1, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v1, v0, v8}, Lcom/android/gallery3d/common/EntrySchema;->insertOrReplace(Landroid/database/sqlite/SQLiteDatabase;Lcom/android/gallery3d/common/Entry;)J

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->ALBUM_TABLE_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$800()Ljava/lang/String;

    move-result-object v1

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->ALBUM_TABLE_PROJECTION:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$900()[Ljava/lang/String;

    move-result-object v2

    const-string v3, "account=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$CreateAlbumTask;->mAccount:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_0

    :catch_0
    move-exception v11

    :try_start_1
    const-string v1, "PicasaUploadAsyncTask"

    const-string v2, "error inserting album info to db"

    invoke-static {v1, v2, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    move-object v9, v13

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$CreateAlbumTask;->doInBackground([Ljava/lang/Void;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/database/Cursor;)V
    .locals 6
    .param p1    # Landroid/database/Cursor;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$CreateAlbumTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mListener:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;
    invoke-static {v0}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$100(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$CreateAlbumTask;->mAccount:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$CreateAlbumTask;->mAlbumTitle:Ljava/lang/String;

    iget-wide v4, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$CreateAlbumTask;->mAlbumId:J

    move-object v2, p1

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;->onAlbumCreated(Ljava/lang/String;Landroid/database/Cursor;Ljava/lang/String;J)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$CreateAlbumTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mListener:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;
    invoke-static {v0}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$100(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;

    move-result-object v0

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$CreateAlbumTask;->mAccount:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;->onError(ILjava/lang/String;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$CreateAlbumTask;->onPostExecute(Landroid/database/Cursor;)V

    return-void
.end method
