.class Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask$1;
.super Ljava/lang/Object;
.source "PicasaUploadAsyncTask.java"

# interfaces
.implements Lcom/google/android/picasasync/PicasaApi$EntryHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;->doInBackground([Ljava/lang/String;)Landroid/database/Cursor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;

.field final synthetic val$account:Ljava/lang/String;

.field final synthetic val$albumList:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask$1;->this$1:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;

    iput-object p2, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask$1;->val$account:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask$1;->val$albumList:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleEntry(Landroid/content/ContentValues;)V
    .locals 4
    .param p1    # Landroid/content/ContentValues;

    const-string v2, "album_type"

    invoke-virtual {p1, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Buzz"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "InstantUpload"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;

    invoke-direct {v0}, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;-><init>()V

    const-string v2, "_id"

    invoke-virtual {p1, v2}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;->id:J

    iget-object v2, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask$1;->val$account:Ljava/lang/String;

    iput-object v2, v0, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;->account:Ljava/lang/String;

    const-string v2, "title"

    invoke-virtual {p1, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;->title:Ljava/lang/String;

    const-string v2, "num_photos"

    invoke-virtual {p1, v2}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v0, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;->photoCount:I

    const-string v2, "Default"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :goto_1
    iput v2, v0, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;->isDefault:I

    iget-object v2, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask$1;->val$albumList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method
