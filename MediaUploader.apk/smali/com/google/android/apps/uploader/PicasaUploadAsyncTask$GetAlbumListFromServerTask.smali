.class Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;
.super Landroid/os/AsyncTask;
.source "PicasaUploadAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetAlbumListFromServerTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private mAccount:Ljava/lang/String;

.field private mErrorCode:I

.field private mLastAlbumId:J

.field final synthetic this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$1;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;
    .param p2    # Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$1;

    invoke-direct {p0, p1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;-><init>(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Landroid/database/Cursor;
    .locals 21
    .param p1    # [Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v9, p1, v2

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;->mAccount:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    # invokes: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->getAccountEtagAsUserEntry(Ljava/lang/String;)Lcom/google/android/picasasync/UserEntry;
    invoke-static {v2, v9}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$1100(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;Ljava/lang/String;)Lcom/google/android/picasasync/UserEntry;

    move-result-object v20

    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mLastAccount:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$1200(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    # invokes: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->refreshAuthToken(Ljava/lang/String;)V
    invoke-static {v2, v9}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$1300(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    :cond_0
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    new-instance v13, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask$1;

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v9, v11}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask$1;-><init>(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;Ljava/lang/String;Ljava/util/ArrayList;)V

    const/16 v18, 0x1

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/google/android/picasasync/UserEntry;->albumsEtag:Ljava/lang/String;

    move-object/from16 v17, v0

    const/4 v14, 0x0

    :goto_0
    const/4 v2, 0x1

    if-gt v14, v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mApi:Lcom/google/android/picasasync/PicasaApi;
    invoke-static {v2}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$1400(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Lcom/google/android/picasasync/PicasaApi;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-virtual {v2, v0, v13}, Lcom/google/android/picasasync/PicasaApi;->getAlbums(Lcom/google/android/picasasync/UserEntry;Lcom/google/android/picasasync/PicasaApi$EntryHandler;)I

    move-result v18

    const/4 v2, 0x2

    move/from16 v0, v18

    if-eq v0, v2, :cond_2

    :cond_1
    packed-switch v18, :pswitch_data_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mDbHelper:Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper;
    invoke-static {v2}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$700(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper;

    move-result-object v2

    move-object/from16 v0, v20

    iget-object v3, v0, Lcom/google/android/picasasync/UserEntry;->albumsEtag:Ljava/lang/String;

    invoke-virtual {v2, v9, v3}, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper;->setEtag(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mDbHelper:Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper;
    invoke-static {v2}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$700(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    const/4 v12, 0x0

    const/4 v2, 0x1

    :try_start_1
    new-array v5, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v9, v5, v2

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->ALBUM_TABLE_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$800()Ljava/lang/String;

    move-result-object v2

    const-string v3, "account=?"

    invoke-virtual {v1, v2, v3, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {v11}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_1
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;

    sget-object v2, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v2, v1, v10}, Lcom/android/gallery3d/common/EntrySchema;->insertOrReplace(Landroid/database/sqlite/SQLiteDatabase;Lcom/android/gallery3d/common/Entry;)J
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v19

    :try_start_2
    const-string v2, "PicasaUploadAsyncTask"

    const-string v3, "error inserting album info to db"

    move-object/from16 v0, v19

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v2, 0x3

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;->mErrorCode:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :goto_2
    return-object v12

    :catch_1
    move-exception v19

    const/4 v2, 0x2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;->mErrorCode:I

    const/4 v12, 0x0

    goto :goto_2

    :cond_2
    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    # invokes: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->refreshAuthToken(Ljava/lang/String;)V
    invoke-static {v2, v9}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$1300(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2

    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_0

    :catch_2
    move-exception v19

    const/4 v2, 0x3

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;->mErrorCode:I

    const/4 v12, 0x0

    goto :goto_2

    :pswitch_0
    const/4 v2, 0x3

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;->mErrorCode:I

    const/4 v12, 0x0

    goto :goto_2

    :pswitch_1
    const/4 v12, 0x0

    goto :goto_2

    :cond_3
    :try_start_4
    # getter for: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->ALBUM_TABLE_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$800()Ljava/lang/String;

    move-result-object v2

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->ALBUM_TABLE_PROJECTION:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$900()[Ljava/lang/String;

    move-result-object v3

    const-string v4, "account=?"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "album_id_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;->mAccount:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mPreferences:Landroid/content/SharedPreferences;
    invoke-static {v2}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$500(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-wide/16 v3, 0x0

    move-object/from16 v0, v16

    invoke-interface {v2, v0, v3, v4}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;->mLastAlbumId:J
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_2

    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;->doInBackground([Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/database/Cursor;)V
    .locals 7
    .param p1    # Landroid/database/Cursor;

    iget-object v6, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    monitor-enter v6

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mGetAlbumListFromServerTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;
    invoke-static {v0}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$1500(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;

    move-result-object v0

    if-eq p0, v0, :cond_0

    monitor-exit v6

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;->mErrorCode:I

    if-nez v0, :cond_2

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mListener:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;
    invoke-static {v0}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$100(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;->mAccount:Ljava/lang/String;

    const/4 v3, 0x0

    iget-wide v4, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;->mLastAlbumId:J

    move-object v2, p1

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;->onAlbumListReady(Ljava/lang/String;Landroid/database/Cursor;ZJ)V

    :cond_1
    :goto_1
    monitor-exit v6

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mListener:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;
    invoke-static {v0}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$100(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;->mErrorCode:I

    iget-object v2, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;->mAccount:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;->onError(ILjava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;->onPostExecute(Landroid/database/Cursor;)V

    return-void
.end method
