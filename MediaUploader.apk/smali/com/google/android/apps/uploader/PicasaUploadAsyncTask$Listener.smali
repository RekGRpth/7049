.class public interface abstract Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;
.super Ljava/lang/Object;
.source "PicasaUploadAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onAccountListReady([Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onAddAccountResult(Z)V
.end method

.method public abstract onAlbumCreated(Ljava/lang/String;Landroid/database/Cursor;Ljava/lang/String;J)V
.end method

.method public abstract onAlbumListReady(Ljava/lang/String;Landroid/database/Cursor;ZJ)V
.end method

.method public abstract onError(ILjava/lang/String;)V
.end method

.method public abstract onUploadRequestsSubmitted(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$UploadRequestsSummary;)V
.end method
