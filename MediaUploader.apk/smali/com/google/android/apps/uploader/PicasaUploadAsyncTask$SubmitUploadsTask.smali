.class Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;
.super Landroid/os/AsyncTask;
.source "PicasaUploadAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SubmitUploadsTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$UploadRequestsSummary;",
        ">;"
    }
.end annotation


# instance fields
.field private final mAccount:Ljava/lang/String;

.field private final mAlbum:Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;

.field private final mCaption:Ljava/lang/String;

.field private final mComponentName:Landroid/content/ComponentName;

.field private final mContentUris:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;


# direct methods
.method constructor <init>(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;Ljava/lang/String;Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;Ljava/lang/String;Ljava/util/List;Landroid/content/ComponentName;)V
    .locals 1
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;
    .param p4    # Ljava/lang/String;
    .param p6    # Landroid/content/ComponentName;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/content/ComponentName;",
            ")V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    invoke-static {p2}, Lcom/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->mAccount:Ljava/lang/String;

    invoke-static {p3}, Lcom/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;

    iput-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->mAlbum:Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;

    iput-object p4, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->mCaption:Ljava/lang/String;

    invoke-static {p5}, Lcom/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->mContentUris:Ljava/util/List;

    iput-object p6, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->mComponentName:Landroid/content/ComponentName;

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$UploadRequestsSummary;
    .locals 22
    .param p1    # [Ljava/lang/Void;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->mContentUris:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v17

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$400(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v19

    move/from16 v0, v17

    new-array v0, v0, [Landroid/content/ContentValues;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->mAccount:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->mAlbum:Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;

    iget-wide v2, v2, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;->id:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->mAlbum:Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;

    iget-object v2, v2, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;->title:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->mCaption:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->mComponentName:Landroid/content/ComponentName;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "album_id_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mPreferences:Landroid/content/SharedPreferences;
    invoke-static {v2}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$500(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->mAlbum:Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;

    iget-wide v3, v3, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;->id:J

    move-object/from16 v0, v16

    invoke-interface {v2, v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "last_account"

    invoke-interface {v2, v3, v9}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    const/4 v15, 0x0

    :goto_0
    move/from16 v0, v17

    if-ge v15, v0, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->mContentUris:Ljava/util/List;

    invoke-interface {v2, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Landroid/net/Uri;

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    # invokes: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->getDisplayName(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$1600(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v20

    if-eqz v20, :cond_0

    const-string v2, "video/"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    add-int/lit8 v7, v7, 0x1

    :cond_0
    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-static {v9, v0, v1, v10, v13}, Lcom/google/android/picasasync/PicasaUploadHelper;->newUploadRequestBuilder(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ComponentName;)Lcom/google/android/picasasync/PicasaUploadHelper$UploadRequestBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Lcom/google/android/picasasync/PicasaUploadHelper$UploadRequestBuilder;->setCaption(Ljava/lang/String;)Lcom/google/android/picasasync/PicasaUploadHelper$UploadRequestBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Lcom/google/android/picasasync/PicasaUploadHelper$UploadRequestBuilder;->setAlbumTitle(Ljava/lang/String;)Lcom/google/android/picasasync/PicasaUploadHelper$UploadRequestBuilder;

    move-result-object v2

    if-nez v14, :cond_1

    const-string v14, ""

    :cond_1
    invoke-virtual {v2, v14}, Lcom/google/android/picasasync/PicasaUploadHelper$UploadRequestBuilder;->setDisplayName(Ljava/lang/String;)Lcom/google/android/picasasync/PicasaUploadHelper$UploadRequestBuilder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/picasasync/PicasaUploadHelper$UploadRequestBuilder;->getRequest()Landroid/content/ContentValues;

    move-result-object v2

    aput-object v2, v18, v15

    add-int/lit8 v15, v15, 0x1

    goto :goto_0

    :cond_2
    sget-object v2, Lcom/google/android/picasasync/PicasaFacade;->uploadsUri:Landroid/net/Uri;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result v8

    move/from16 v0, v17

    if-eq v8, v0, :cond_3

    const-string v2, "PicasaUploadAsyncTask"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "requesting "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " uploads but only "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " inserted"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    new-instance v2, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$UploadRequestsSummary;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->mAccount:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->mAlbum:Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->mContentUris:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->mContentUris:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    sub-int/2addr v6, v7

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$UploadRequestsSummary;-><init>(Ljava/lang/String;Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;Ljava/util/List;III)V

    return-object v2
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->doInBackground([Ljava/lang/Void;)Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$UploadRequestsSummary;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$UploadRequestsSummary;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$UploadRequestsSummary;

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mListener:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;
    invoke-static {v0}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$100(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;->onUploadRequestsSubmitted(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$UploadRequestsSummary;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$UploadRequestsSummary;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->onPostExecute(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$UploadRequestsSummary;)V

    return-void
.end method
