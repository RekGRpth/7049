.class public Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$UploadRequestsSummary;
.super Ljava/lang/Object;
.source "PicasaUploadAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UploadRequestsSummary"
.end annotation


# instance fields
.field public final account:Ljava/lang/String;

.field public final album:Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;

.field public final contentUris:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field public final photoCount:I

.field public final requestCount:I

.field public final videoCount:I


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;Ljava/util/List;III)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;III)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p4, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$UploadRequestsSummary;->photoCount:I

    iput p5, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$UploadRequestsSummary;->videoCount:I

    iput-object p1, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$UploadRequestsSummary;->account:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$UploadRequestsSummary;->album:Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;

    iput-object p3, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$UploadRequestsSummary;->contentUris:Ljava/util/List;

    iput p6, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$UploadRequestsSummary;->requestCount:I

    return-void
.end method
