.class Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;
.super Ljava/lang/Object;
.source "PicasaUploadAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$CreateAlbumTask;,
        Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;,
        Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$UploadRequestsSummary;,
        Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;,
        Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetCachedAlbumListTask;,
        Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAccountListTask;,
        Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;
    }
.end annotation


# static fields
.field private static final ALBUM_TABLE_NAME:Ljava/lang/String;

.field private static final ALBUM_TABLE_PROJECTION:[Ljava/lang/String;

.field private static final PICASA_FEATURES:[Ljava/lang/String;

.field private static final PROJECTION_DISPLAY_NAME:[Ljava/lang/String;


# instance fields
.field private final mApi:Lcom/google/android/picasasync/PicasaApi;

.field private final mContext:Landroid/content/Context;

.field private mCreateAlbumTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$CreateAlbumTask;

.field private final mDbHelper:Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper;

.field private mGetAccountListTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAccountListTask;

.field private mGetAlbumListFromServerTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;

.field private mGetCachedAlbumListTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetCachedAlbumListTask;

.field private mLastAccount:Ljava/lang/String;

.field private mLastAuthToken:Ljava/lang/String;

.field private final mListener:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;

.field private final mPreferences:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "service_lh2"

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->PICASA_FEATURES:[Ljava/lang/String;

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "_display_name"

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->PROJECTION_DISPLAY_NAME:[Ljava/lang/String;

    sget-object v0, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v0}, Lcom/android/gallery3d/common/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->ALBUM_TABLE_NAME:Ljava/lang/String;

    sget-object v0, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v0}, Lcom/android/gallery3d/common/EntrySchema;->getProjection()[Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->ALBUM_TABLE_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;Landroid/content/SharedPreferences;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;
    .param p3    # Landroid/content/SharedPreferences;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mContext:Landroid/content/Context;

    invoke-static {p2}, Lcom/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;

    iput-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mListener:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;

    invoke-static {p3}, Lcom/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mPreferences:Landroid/content/SharedPreferences;

    new-instance v0, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper;

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mDbHelper:Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper;

    new-instance v0, Lcom/google/android/picasasync/PicasaApi;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/picasasync/PicasaApi;-><init>(Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mApi:Lcom/google/android/picasasync/PicasaApi;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;
    .locals 1
    .param p0    # Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mListener:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetCachedAlbumListTask;
    .locals 1
    .param p0    # Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mGetCachedAlbumListTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetCachedAlbumListTask;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;Ljava/lang/String;)Lcom/google/android/picasasync/UserEntry;
    .locals 1
    .param p0    # Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->getAccountEtagAsUserEntry(Ljava/lang/String;)Lcom/google/android/picasasync/UserEntry;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mLastAccount:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/OperationCanceledException;,
            Ljava/io/IOException;,
            Landroid/accounts/AuthenticatorException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->refreshAuthToken(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1400(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Lcom/google/android/picasasync/PicasaApi;
    .locals 1
    .param p0    # Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mApi:Lcom/google/android/picasasync/PicasaApi;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;
    .locals 1
    .param p0    # Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mGetAlbumListFromServerTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;

    return-object v0
.end method

.method static synthetic access$1600(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p0    # Landroid/content/ContentResolver;
    .param p1    # Landroid/net/Uri;

    invoke-static {p0, p1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->getDisplayName(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0    # Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mPreferences:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAccountListTask;
    .locals 1
    .param p0    # Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mGetAccountListTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAccountListTask;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper;
    .locals 1
    .param p0    # Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mDbHelper:Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper;

    return-object v0
.end method

.method static synthetic access$800()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->ALBUM_TABLE_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->ALBUM_TABLE_PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method private getAccountEtagAsUserEntry(Ljava/lang/String;)Lcom/google/android/picasasync/UserEntry;
    .locals 2
    .param p1    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/picasasync/UserEntry;

    invoke-direct {v0}, Lcom/google/android/picasasync/UserEntry;-><init>()V

    iput-object p1, v0, Lcom/google/android/picasasync/UserEntry;->account:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mDbHelper:Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper;->getEtag(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/picasasync/UserEntry;->albumsEtag:Ljava/lang/String;

    return-object v0
.end method

.method private static getDisplayName(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/lang/String;
    .locals 9
    .param p0    # Landroid/content/ContentResolver;
    .param p1    # Landroid/net/Uri;

    const/4 v8, 0x0

    const/4 v6, 0x0

    :try_start_0
    sget-object v2, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->PROJECTION_DISPLAY_NAME:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    invoke-static {v6}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v6}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    :goto_1
    move-object v0, v8

    goto :goto_0

    :catch_0
    move-exception v7

    :try_start_1
    const-string v0, "PicasaUploadAsyncTask"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "no display name for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {v6}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method private refreshAuthToken(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/OperationCanceledException;,
            Ljava/io/IOException;,
            Landroid/accounts/AuthenticatorException;
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mLastAccount:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mLastAuthToken:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, "com.google"

    iget-object v2, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mLastAuthToken:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mLastAccount:Ljava/lang/String;

    new-instance v1, Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mLastAccount:Ljava/lang/String;

    const-string v3, "com.google"

    invoke-direct {v1, v2, v3}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "lh2"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mLastAuthToken:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mApi:Lcom/google/android/picasasync/PicasaApi;

    iget-object v2, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mLastAuthToken:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/picasasync/PicasaApi;->setAuthToken(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public declared-synchronized addAccount(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 8
    .param p1    # Landroid/app/Activity;
    .param p2    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const-string v0, "allowSkip"

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.google"

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->PICASA_FEATURES:[Ljava/lang/String;

    new-instance v6, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$1;

    invoke-direct {v6, p0}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$1;-><init>(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)V

    const/4 v7, 0x0

    move-object v5, p1

    invoke-virtual/range {v0 .. v7}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public addAlbum(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    const/4 v2, 0x1

    invoke-static {p1}, Lcom/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mCreateAlbumTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$CreateAlbumTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mCreateAlbumTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$CreateAlbumTask;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$CreateAlbumTask;->cancel(Z)Z

    :cond_0
    new-instance v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$CreateAlbumTask;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$CreateAlbumTask;-><init>(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;Ljava/lang/String;Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mCreateAlbumTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$CreateAlbumTask;

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mCreateAlbumTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$CreateAlbumTask;

    new-array v2, v2, [Ljava/lang/Void;

    const/4 v3, 0x0

    const/4 v0, 0x0

    check-cast v0, Ljava/lang/Void;

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$CreateAlbumTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public declared-synchronized cancel()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mGetAccountListTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAccountListTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mGetAccountListTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAccountListTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAccountListTask;->cancel(Z)Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mGetCachedAlbumListTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetCachedAlbumListTask;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mGetCachedAlbumListTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetCachedAlbumListTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetCachedAlbumListTask;->cancel(Z)Z

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mGetAlbumListFromServerTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mGetAlbumListFromServerTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;->cancel(Z)Z

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mCreateAlbumTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$CreateAlbumTask;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mCreateAlbumTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$CreateAlbumTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$CreateAlbumTask;->cancel(Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getAccountList()V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mGetAccountListTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAccountListTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mGetAccountListTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAccountListTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAccountListTask;->cancel(Z)Z

    :cond_0
    new-instance v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAccountListTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAccountListTask;-><init>(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$1;)V

    iput-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mGetAccountListTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAccountListTask;

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mGetAccountListTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAccountListTask;

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/Void;

    const/4 v3, 0x0

    const/4 v0, 0x0

    check-cast v0, Ljava/lang/Void;

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAccountListTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getAlbumListFromServer(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mGetAlbumListFromServerTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mGetAlbumListFromServerTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;->cancel(Z)Z

    :cond_0
    new-instance v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;-><init>(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$1;)V

    iput-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mGetAlbumListFromServerTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mGetAlbumListFromServerTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAlbumListFromServerTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getCachedAlbumList(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mGetCachedAlbumListTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetCachedAlbumListTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mGetCachedAlbumListTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetCachedAlbumListTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetCachedAlbumListTask;->cancel(Z)Z

    :cond_0
    new-instance v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetCachedAlbumListTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetCachedAlbumListTask;-><init>(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$1;)V

    iput-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mGetCachedAlbumListTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetCachedAlbumListTask;

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mGetCachedAlbumListTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetCachedAlbumListTask;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetCachedAlbumListTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public submitUploads(Ljava/lang/String;Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;Ljava/lang/String;Ljava/util/List;Landroid/content/ComponentName;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;
    .param p3    # Ljava/lang/String;
    .param p5    # Landroid/content/ComponentName;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/content/ComponentName;",
            ")V"
        }
    .end annotation

    new-instance v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;-><init>(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;Ljava/lang/String;Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;Ljava/lang/String;Ljava/util/List;Landroid/content/ComponentName;)V

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/Void;

    const/4 v3, 0x0

    const/4 v1, 0x0

    check-cast v1, Ljava/lang/Void;

    aput-object v1, v2, v3

    invoke-virtual {v0, v2}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method
