.class Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AccountInfo;
.super Lcom/android/gallery3d/common/Entry;
.source "PicasaUploadDatabaseHelper.java"


# annotations
.annotation runtime Lcom/android/gallery3d/common/Entry$Table;
    value = "accounts"
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AccountInfo"
.end annotation


# static fields
.field public static final SCHEMA:Lcom/android/gallery3d/common/EntrySchema;


# instance fields
.field public account:Ljava/lang/String;
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        indexed = true
        unique = true
        value = "account"
    .end annotation
.end field

.field public etag:Ljava/lang/String;
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "etag"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/android/gallery3d/common/EntrySchema;

    const-class v1, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AccountInfo;

    invoke-direct {v0, v1}, Lcom/android/gallery3d/common/EntrySchema;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AccountInfo;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/gallery3d/common/Entry;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AccountInfo;->account:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AccountInfo;->etag:Ljava/lang/String;

    return-void
.end method
