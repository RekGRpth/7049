.class public Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;
.super Lcom/android/gallery3d/common/Entry;
.source "PicasaUploadDatabaseHelper.java"


# annotations
.annotation runtime Lcom/android/gallery3d/common/Entry$Table;
    value = "albums"
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AlbumInfo"
.end annotation


# static fields
.field public static final SCHEMA:Lcom/android/gallery3d/common/EntrySchema;


# instance fields
.field public account:Ljava/lang/String;
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        indexed = true
        value = "account"
    .end annotation
.end field

.field public isDefault:I
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "is_default"
    .end annotation
.end field

.field public photoCount:I
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "photo_count"
    .end annotation
.end field

.field public title:Ljava/lang/String;
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "title"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/android/gallery3d/common/EntrySchema;

    const-class v1, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;

    invoke-direct {v0, v1}, Lcom/android/gallery3d/common/EntrySchema;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/gallery3d/common/Entry;-><init>()V

    return-void
.end method
