.class Lcom/google/android/apps/uploader/PicasaUploadProgressReceiver$1;
.super Ljava/lang/Thread;
.source "PicasaUploadProgressReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/uploader/PicasaUploadProgressReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/uploader/PicasaUploadProgressReceiver;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$intent:Landroid/content/Intent;

.field final synthetic val$result:Landroid/content/BroadcastReceiver$PendingResult;


# direct methods
.method constructor <init>(Lcom/google/android/apps/uploader/PicasaUploadProgressReceiver;Ljava/lang/String;Landroid/content/Context;Landroid/content/Intent;Landroid/content/BroadcastReceiver$PendingResult;)V
    .locals 0
    .param p2    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/uploader/PicasaUploadProgressReceiver$1;->this$0:Lcom/google/android/apps/uploader/PicasaUploadProgressReceiver;

    iput-object p3, p0, Lcom/google/android/apps/uploader/PicasaUploadProgressReceiver$1;->val$context:Landroid/content/Context;

    iput-object p4, p0, Lcom/google/android/apps/uploader/PicasaUploadProgressReceiver$1;->val$intent:Landroid/content/Intent;

    iput-object p5, p0, Lcom/google/android/apps/uploader/PicasaUploadProgressReceiver$1;->val$result:Landroid/content/BroadcastReceiver$PendingResult;

    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadProgressReceiver$1;->this$0:Lcom/google/android/apps/uploader/PicasaUploadProgressReceiver;

    iget-object v2, p0, Lcom/google/android/apps/uploader/PicasaUploadProgressReceiver$1;->val$context:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/apps/uploader/PicasaUploadProgressReceiver$1;->val$intent:Landroid/content/Intent;

    # invokes: Lcom/google/android/apps/uploader/PicasaUploadProgressReceiver;->updateProgress(Landroid/content/Context;Landroid/content/Intent;)V
    invoke-static {v1, v2, v3}, Lcom/google/android/apps/uploader/PicasaUploadProgressReceiver;->access$000(Lcom/google/android/apps/uploader/PicasaUploadProgressReceiver;Landroid/content/Context;Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadProgressReceiver$1;->val$result:Landroid/content/BroadcastReceiver$PendingResult;

    invoke-virtual {v1}, Landroid/content/BroadcastReceiver$PendingResult;->finish()V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    const-string v1, "PicasaUploadProgressReceiver"

    const-string v2, "updateProgress"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadProgressReceiver$1;->val$result:Landroid/content/BroadcastReceiver$PendingResult;

    invoke-virtual {v1}, Landroid/content/BroadcastReceiver$PendingResult;->finish()V

    goto :goto_0

    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/google/android/apps/uploader/PicasaUploadProgressReceiver$1;->val$result:Landroid/content/BroadcastReceiver$PendingResult;

    invoke-virtual {v2}, Landroid/content/BroadcastReceiver$PendingResult;->finish()V

    throw v1
.end method
