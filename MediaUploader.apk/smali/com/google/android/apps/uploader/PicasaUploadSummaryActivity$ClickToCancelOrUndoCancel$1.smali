.class Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel$1;
.super Ljava/lang/Object;
.source "PicasaUploadSummaryActivity.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel;->createFutureTask(Landroid/content/ContentResolver;Landroid/net/Uri;J)Ljava/util/concurrent/FutureTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel;

.field final synthetic val$resolver:Landroid/content/ContentResolver;

.field final synthetic val$taskId:J

.field final synthetic val$uploadUri:Landroid/net/Uri;


# direct methods
.method constructor <init>(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel;Landroid/content/ContentResolver;Landroid/net/Uri;J)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel$1;->this$1:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel;

    iput-object p2, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel$1;->val$resolver:Landroid/content/ContentResolver;

    iput-object p3, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel$1;->val$uploadUri:Landroid/net/Uri;

    iput-wide p4, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel$1;->val$taskId:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel$1;->call()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public call()Ljava/lang/Void;
    .locals 6

    const/4 v5, 0x0

    const-wide/16 v1, 0x7d0

    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel$1;->val$resolver:Landroid/content/ContentResolver;

    iget-object v2, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel$1;->val$uploadUri:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    const-string v1, "PicasaUploadSummary"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "upload cancelled "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel$1;->val$taskId:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel$1;->this$1:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel;

    iget-object v2, v1, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel;->this$0:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    monitor-enter v2

    :try_start_1
    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel$1;->this$1:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel;

    iget-object v1, v1, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel;->this$0:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mCancelFutureTasks:Landroid/util/LongSparseArray;
    invoke-static {v1}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->access$200(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;)Landroid/util/LongSparseArray;

    move-result-object v1

    iget-wide v3, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel$1;->val$taskId:J

    invoke-virtual {v1, v3, v4}, Landroid/util/LongSparseArray;->remove(J)V

    monitor-exit v2

    :goto_0
    return-object v5

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    :catch_0
    move-exception v0

    :try_start_2
    const-string v1, "PicasaUploadSummary"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "undo cancel "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel$1;->val$taskId:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel$1;->this$1:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel;

    iget-object v2, v1, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel;->this$0:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    monitor-enter v2

    :try_start_3
    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel$1;->this$1:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel;

    iget-object v1, v1, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel;->this$0:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mCancelFutureTasks:Landroid/util/LongSparseArray;
    invoke-static {v1}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->access$200(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;)Landroid/util/LongSparseArray;

    move-result-object v1

    iget-wide v3, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel$1;->val$taskId:J

    invoke-virtual {v1, v3, v4}, Landroid/util/LongSparseArray;->remove(J)V

    monitor-exit v2

    goto :goto_0

    :catchall_1
    move-exception v1

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v1

    :catchall_2
    move-exception v1

    iget-object v2, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel$1;->this$1:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel;

    iget-object v2, v2, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel;->this$0:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    monitor-enter v2

    :try_start_4
    iget-object v3, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel$1;->this$1:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel;

    iget-object v3, v3, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel;->this$0:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mCancelFutureTasks:Landroid/util/LongSparseArray;
    invoke-static {v3}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->access$200(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;)Landroid/util/LongSparseArray;

    move-result-object v3

    iget-wide v4, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel$1;->val$taskId:J

    invoke-virtual {v3, v4, v5}, Landroid/util/LongSparseArray;->remove(J)V

    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    throw v1

    :catchall_3
    move-exception v1

    :try_start_5
    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    throw v1
.end method
