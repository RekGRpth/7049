.class Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel;
.super Ljava/lang/Object;
.source "PicasaUploadSummaryActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ClickToCancelOrUndoCancel"
.end annotation


# instance fields
.field private final mResolver:Landroid/content/ContentResolver;

.field private final mUploadTask:Lcom/google/android/picasasync/UploadTaskEntry;

.field private final mUploadUri:Landroid/net/Uri;

.field final synthetic this$0:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;Landroid/content/ContentResolver;Landroid/net/Uri;Lcom/google/android/picasasync/UploadTaskEntry;)V
    .locals 0
    .param p2    # Landroid/content/ContentResolver;
    .param p3    # Landroid/net/Uri;
    .param p4    # Lcom/google/android/picasasync/UploadTaskEntry;

    iput-object p1, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel;->this$0:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel;->mResolver:Landroid/content/ContentResolver;

    iput-object p3, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel;->mUploadUri:Landroid/net/Uri;

    iput-object p4, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel;->mUploadTask:Lcom/google/android/picasasync/UploadTaskEntry;

    return-void
.end method

.method private createFutureTask(Landroid/content/ContentResolver;Landroid/net/Uri;J)Ljava/util/concurrent/FutureTask;
    .locals 7
    .param p1    # Landroid/content/ContentResolver;
    .param p2    # Landroid/net/Uri;
    .param p3    # J

    new-instance v6, Ljava/util/concurrent/FutureTask;

    new-instance v0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel$1;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel$1;-><init>(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel;Landroid/content/ContentResolver;Landroid/net/Uri;J)V

    invoke-direct {v6, v0}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    return-object v6
.end method

.method private onClickLocked(Landroid/view/View;)V
    .locals 8
    .param p1    # Landroid/view/View;

    iget-object v5, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel;->mUploadTask:Lcom/google/android/picasasync/UploadTaskEntry;

    iget-wide v2, v5, Lcom/google/android/picasasync/UploadTaskEntry;->id:J

    iget-object v5, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel;->this$0:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mCancelFutureTasks:Landroid/util/LongSparseArray;
    invoke-static {v5}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->access$200(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;)Landroid/util/LongSparseArray;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/FutureTask;

    if-nez v0, :cond_0

    const-string v5, "PicasaUploadSummary"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "cancel upload "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel;->mResolver:Landroid/content/ContentResolver;

    iget-object v6, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel;->mUploadUri:Landroid/net/Uri;

    invoke-direct {p0, v5, v6, v2, v3}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel;->createFutureTask(Landroid/content/ContentResolver;Landroid/net/Uri;J)Ljava/util/concurrent/FutureTask;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel;->this$0:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mCancelFutureTasks:Landroid/util/LongSparseArray;
    invoke-static {v5}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->access$200(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;)Landroid/util/LongSparseArray;

    move-result-object v5

    invoke-virtual {v5, v2, v3, v0}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    iget-object v5, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel;->this$0:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mExecutorService:Ljava/util/concurrent/ExecutorService;
    invoke-static {v5}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->access$300(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;)Ljava/util/concurrent/ExecutorService;

    move-result-object v5

    invoke-interface {v5, v0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    const/4 v4, 0x7

    :goto_0
    const v5, 0x7f050015

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    # invokes: Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->getStateMessage(I)I
    invoke-static {v4}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->access$400(I)I

    move-result v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(I)V

    # invokes: Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->getStateColor(I)I
    invoke-static {v4}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->access$500(I)I

    move-result v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setTextColor(I)V

    return-void

    :cond_0
    iget-object v5, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel;->this$0:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mCancelFutureTasks:Landroid/util/LongSparseArray;
    invoke-static {v5}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->access$200(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;)Landroid/util/LongSparseArray;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Landroid/util/LongSparseArray;->remove(J)V

    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Ljava/util/concurrent/FutureTask;->cancel(Z)Z

    iget-object v5, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel;->mUploadTask:Lcom/google/android/picasasync/UploadTaskEntry;

    invoke-virtual {v5}, Lcom/google/android/picasasync/UploadTaskEntry;->getState()I

    move-result v4

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel;->this$0:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel;->onClickLocked(Landroid/view/View;)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
