.class public Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadNotification;
.super Ljava/lang/Object;
.source "PicasaUploadSummaryActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UploadNotification"
.end annotation


# instance fields
.field private mPendingIntent:Landroid/app/PendingIntent;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getPauseMessage(I)I
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    const v0, 0x7f060014

    :goto_0
    return v0

    :pswitch_1
    const v0, 0x7f060013

    goto :goto_0

    :pswitch_2
    const v0, 0x7f060012

    goto :goto_0

    :pswitch_3
    const v0, 0x7f060011

    goto :goto_0

    :pswitch_4
    const v0, 0x7f06000f

    goto :goto_0

    :pswitch_5
    const v0, 0x7f060010

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private updateNotificationSynchronized(Landroid/content/Context;IILjava/lang/String;I)V
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x1

    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    if-nez p3, :cond_0

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->sSummaryActivity:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;
    invoke-static {}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->access$1100()Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v5}, Landroid/app/NotificationManager;->cancel(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadNotification;->mPendingIntent:Landroid/app/PendingIntent;

    if-nez v1, :cond_1

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v2, 0x8000000

    invoke-static {p1, v6, v1, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadNotification;->mPendingIntent:Landroid/app/PendingIntent;

    :cond_1
    new-instance v1, Landroid/app/Notification$Builder;

    invoke-direct {v1, p1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadNotification;->mPendingIntent:Landroid/app/PendingIntent;

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v1

    if-nez p3, :cond_3

    const v2, 0x1080089

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    const v2, 0x7f060009

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    invoke-virtual {v1, v5}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    :cond_2
    :goto_1
    invoke-virtual {v1}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {v0, v5, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0

    :cond_3
    const v2, 0x1080088

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    const v2, 0x7f060008

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    invoke-virtual {v1, v5}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    if-eqz p4, :cond_2

    if-ne p2, v5, :cond_4

    invoke-virtual {v1, p4}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    const/16 v2, 0x64

    invoke-virtual {v1, v2, p5, v6}, Landroid/app/Notification$Builder;->setProgress(IIZ)Landroid/app/Notification$Builder;

    goto :goto_1

    :cond_4
    invoke-static {p2}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadNotification;->getPauseMessage(I)I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    goto :goto_1
.end method


# virtual methods
.method public updateNotification(Landroid/content/Context;IILjava/lang/String;I)V
    .locals 2

    const-class v1, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    monitor-enter v1

    :try_start_0
    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadNotification;->updateNotificationSynchronized(Landroid/content/Context;IILjava/lang/String;I)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
