.class Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadRecordCursorAdapter;
.super Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadTaskCursorAdapter;
.source "PicasaUploadSummaryActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UploadRecordCursorAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;Landroid/content/Context;)V
    .locals 1
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadRecordCursorAdapter;->this$0:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    const v0, 0x7f030003

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadTaskCursorAdapter;-><init>(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;Landroid/content/Context;I)V

    return-void
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 10
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    invoke-static {p3}, Lcom/google/android/picasasync/UploadedEntry;->fromCursor(Landroid/database/Cursor;)Lcom/google/android/picasasync/UploadedEntry;

    move-result-object v9

    iget-object v0, v9, Lcom/google/android/picasasync/UploadedEntry;->contentUri:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, v9, Lcom/google/android/picasasync/UploadedEntry;->account:Ljava/lang/String;

    iget-object v4, v9, Lcom/google/android/picasasync/UploadedEntry;->displayName:Ljava/lang/String;

    iget-object v5, v9, Lcom/google/android/picasasync/UploadedEntry;->albumTitle:Ljava/lang/String;

    iget-wide v6, v9, Lcom/google/android/picasasync/UploadedEntry;->bytesTotal:J

    iget v8, v9, Lcom/google/android/picasasync/UploadedEntry;->state:I

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadRecordCursorAdapter;->bindViewCommon(Landroid/view/View;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JI)V

    const v0, 0x7f050012

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-wide v3, v9, Lcom/google/android/picasasync/UploadedEntry;->uploadedTime:J

    # invokes: Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->getDate(J)Ljava/lang/String;
    invoke-static {v3, v4}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->access$1000(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
