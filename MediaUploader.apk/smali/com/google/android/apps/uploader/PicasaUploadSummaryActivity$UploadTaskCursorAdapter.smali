.class Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadTaskCursorAdapter;
.super Landroid/widget/ResourceCursorAdapter;
.source "PicasaUploadSummaryActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UploadTaskCursorAdapter"
.end annotation


# instance fields
.field private final mResolver:Landroid/content/ContentResolver;

.field final synthetic this$0:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;Landroid/content/Context;)V
    .locals 1
    .param p2    # Landroid/content/Context;

    const v0, 0x7f030005

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadTaskCursorAdapter;-><init>(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;Landroid/content/Context;I)V

    return-void
.end method

.method protected constructor <init>(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;Landroid/content/Context;I)V
    .locals 2
    .param p2    # Landroid/content/Context;
    .param p3    # I

    iput-object p1, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadTaskCursorAdapter;->this$0:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p2, p3, v0, v1}, Landroid/widget/ResourceCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;I)V

    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadTaskCursorAdapter;->mResolver:Landroid/content/ContentResolver;

    return-void
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 12
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    invoke-static {p3}, Lcom/google/android/picasasync/UploadTaskEntry;->fromCursor(Landroid/database/Cursor;)Lcom/google/android/picasasync/UploadTaskEntry;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/picasasync/UploadTaskEntry;->getContentUri()Landroid/net/Uri;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadTaskCursorAdapter;->this$0:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadTaskCursorAdapter;->this$0:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mCancelFutureTasks:Landroid/util/LongSparseArray;
    invoke-static {v0}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->access$200(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;)Landroid/util/LongSparseArray;

    move-result-object v0

    iget-wide v3, v11, Lcom/google/android/picasasync/UploadTaskEntry;->id:J

    invoke-virtual {v0, v3, v4}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/concurrent/FutureTask;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v9, :cond_0

    invoke-virtual {v11}, Lcom/google/android/picasasync/UploadTaskEntry;->getState()I

    move-result v8

    :goto_0
    invoke-virtual {v11}, Lcom/google/android/picasasync/UploadTaskEntry;->getAccount()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v11}, Lcom/google/android/picasasync/UploadTaskEntry;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v11}, Lcom/google/android/picasasync/UploadTaskEntry;->getAlbumTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v11}, Lcom/google/android/picasasync/UploadTaskEntry;->getBytesTotal()J

    move-result-wide v6

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadTaskCursorAdapter;->bindViewCommon(Landroid/view/View;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JI)V

    const v0, 0x7f050016

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ProgressBar;

    const/4 v0, 0x1

    if-ne v8, v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {v10, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    invoke-virtual {v11}, Lcom/google/android/picasasync/UploadTaskEntry;->getPercentageUploaded()I

    move-result v0

    invoke-virtual {v10, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    :goto_1
    new-instance v0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel;

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadTaskCursorAdapter;->this$0:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget-wide v4, v11, Lcom/google/android/picasasync/UploadTaskEntry;->id:J

    invoke-static {v4, v5}, Lcom/google/android/picasasync/PicasaFacade;->getUploadUri(J)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v0, v1, v3, v4, v11}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel;-><init>(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;Landroid/content/ContentResolver;Landroid/net/Uri;Lcom/google/android/picasasync/UploadTaskEntry;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    const/4 v8, 0x7

    goto :goto_0

    :cond_1
    const/16 v0, 0x8

    invoke-virtual {v10, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_1
.end method

.method protected final bindViewCommon(Landroid/view/View;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JI)V
    .locals 12
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/net/Uri;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # J
    .param p8    # I

    const-string v7, "@"

    invoke-virtual {p3, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    if-lez v4, :cond_0

    const/4 v7, 0x0

    invoke-virtual {p3, v7, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p3

    :cond_0
    invoke-static/range {p5 .. p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_2

    move-object/from16 p5, p3

    :goto_0
    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    # invokes: Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->getAlternativeDisplayName(Landroid/net/Uri;)Ljava/lang/String;
    invoke-static {p2}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->access$600(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object p4

    :cond_1
    :try_start_0
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->MEDIA_STORE_IMAGE_URI:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->access$700()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadTaskCursorAdapter;->mResolver:Landroid/content/ContentResolver;

    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    const/4 v10, 0x3

    const/4 v11, 0x0

    invoke-static {v7, v8, v9, v10, v11}, Landroid/provider/MediaStore$Images$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v6

    const v7, 0x7f050010

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    invoke-virtual {v7, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    const v7, 0x7f050011

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    move-object/from16 v0, p4

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->sFormatter:Landroid/text/format/Formatter;
    invoke-static {}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->access$900()Landroid/text/format/Formatter;

    iget-object v7, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadTaskCursorAdapter;->this$0:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    move-wide/from16 v0, p6

    invoke-static {v7, v0, v1}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    const v7, 0x7f050014

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    invoke-virtual {v7, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v7, 0x7f050015

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    # invokes: Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->getStateMessage(I)I
    invoke-static/range {p8 .. p8}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->access$400(I)I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    const v7, 0x7f050015

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    # invokes: Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->getStateColor(I)I
    invoke-static/range {p8 .. p8}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->access$500(I)I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    const v7, 0x7f050013

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    move-object/from16 v0, p5

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_2
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p5

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p5

    goto/16 :goto_0

    :cond_3
    :try_start_1
    # getter for: Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->MEDIA_STORE_VIDEO_URI:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->access$800()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    iget-object v7, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadTaskCursorAdapter;->mResolver:Landroid/content/ContentResolver;

    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    const/4 v10, 0x3

    const/4 v11, 0x0

    invoke-static {v7, v8, v9, v10, v11}, Landroid/provider/MediaStore$Video$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v6

    const v7, 0x7f050010

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    invoke-virtual {v7, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    :catch_0
    move-exception v5

    const-string v7, "PicasaUploadSummary"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "failed to generate thumbnail for "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const v7, 0x7f050010

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_1

    :cond_4
    :try_start_2
    const-string v7, "PicasaUploadSummary"

    const/4 v8, 0x2

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_5

    const-string v7, "PicasaUploadSummary"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "no thumbnail for "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v2}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    const v7, 0x7f050010

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_1
.end method
