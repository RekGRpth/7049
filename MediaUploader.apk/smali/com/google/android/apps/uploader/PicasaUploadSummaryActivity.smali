.class public Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;
.super Landroid/app/Activity;
.source "PicasaUploadSummaryActivity.java"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadNotification;,
        Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MySummaryListFragment;,
        Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTaskListFragment;,
        Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadRecordCursorAdapter;,
        Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadTaskCursorAdapter;,
        Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$ClickToCancelOrUndoCancel;,
        Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/app/Activity;",
        "Landroid/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field private static final MEDIA_STORE_IMAGE_URI:Ljava/lang/String;

.field private static final MEDIA_STORE_VIDEO_URI:Ljava/lang/String;

.field private static final sDateFormat:Ljava/text/DateFormat;

.field private static final sFormatter:Landroid/text/format/Formatter;

.field private static sSummaryActivity:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;


# instance fields
.field private mCancelFutureTasks:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray",
            "<",
            "Ljava/util/concurrent/FutureTask",
            "<",
            "Ljava/lang/Void;",
            ">;>;"
        }
    .end annotation
.end field

.field private mExecutorService:Ljava/util/concurrent/ExecutorService;

.field private mTabPager:Landroid/support/v4/view/ViewPager;

.field private mTabPagerAdapter:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;

.field private mUploadRecordsFragment:Landroid/app/ListFragment;

.field private mUploadRecordsObserver:Landroid/database/ContentObserver;

.field private mUploadTaskFragment:Landroid/app/ListFragment;

.field private mUploadTasksObserver:Landroid/database/ContentObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/text/format/Formatter;

    invoke-direct {v0}, Landroid/text/format/Formatter;-><init>()V

    sput-object v0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->sFormatter:Landroid/text/format/Formatter;

    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->MEDIA_STORE_IMAGE_URI:Ljava/lang/String;

    sget-object v0, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->MEDIA_STORE_VIDEO_URI:Ljava/lang/String;

    invoke-static {}, Ljava/text/DateFormat;->getDateInstance()Ljava/text/DateFormat;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->sDateFormat:Ljava/text/DateFormat;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Landroid/util/LongSparseArray;

    invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mCancelFutureTasks:Landroid/util/LongSparseArray;

    return-void
.end method

.method static synthetic access$1000(J)Ljava/lang/String;
    .locals 1
    .param p0    # J

    invoke-static {p0, p1}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->getDate(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100()Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;
    .locals 1

    sget-object v0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->sSummaryActivity:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;)Landroid/util/LongSparseArray;
    .locals 1
    .param p0    # Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mCancelFutureTasks:Landroid/util/LongSparseArray;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;)Ljava/util/concurrent/ExecutorService;
    .locals 1
    .param p0    # Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method static synthetic access$400(I)I
    .locals 1
    .param p0    # I

    invoke-static {p0}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->getStateMessage(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$500(I)I
    .locals 1
    .param p0    # I

    invoke-static {p0}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->getStateColor(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$600(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p0    # Landroid/net/Uri;

    invoke-static {p0}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->getAlternativeDisplayName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->MEDIA_STORE_IMAGE_URI:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->MEDIA_STORE_VIDEO_URI:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900()Landroid/text/format/Formatter;
    .locals 1

    sget-object v0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->sFormatter:Landroid/text/format/Formatter;

    return-object v0
.end method

.method private static getAlternativeDisplayName(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3
    .param p0    # Landroid/net/Uri;

    invoke-virtual {p0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {v0, v1, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    const-string v1, "/"

    invoke-static {v1, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static getDate(J)Ljava/lang/String;
    .locals 2
    .param p0    # J

    sget-object v0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->sDateFormat:Ljava/text/DateFormat;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getStateColor(I)I
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_1
    const v0, -0x777778

    goto :goto_0

    :pswitch_2
    const/high16 v0, -0x10000

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private static getStateMessage(I)I
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    const v0, 0x7f06001d

    :goto_0
    return v0

    :pswitch_1
    const v0, 0x7f060015

    goto :goto_0

    :pswitch_2
    const v0, 0x7f060016

    goto :goto_0

    :pswitch_3
    const v0, 0x7f060017

    goto :goto_0

    :pswitch_4
    const v0, 0x7f060018

    goto :goto_0

    :pswitch_5
    const v0, 0x7f060019

    goto :goto_0

    :pswitch_6
    const v0, 0x7f06001a

    goto :goto_0

    :pswitch_7
    const v0, 0x7f06001b

    goto :goto_0

    :pswitch_8
    const v0, 0x7f06001c

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_8
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1    # Landroid/os/Bundle;

    const/high16 v9, 0x7f050000

    const/4 v11, 0x0

    const/4 v10, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/16 v7, 0x8

    invoke-virtual {p0, v7}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->requestWindowFeature(I)Z

    new-instance v7, Landroid/support/v4/view/ViewPager;

    invoke-direct {v7, p0}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mTabPager:Landroid/support/v4/view/ViewPager;

    iget-object v7, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mTabPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v7, v9}, Landroid/support/v4/view/ViewPager;->setId(I)V

    iget-object v7, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mTabPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {p0, v7}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->setContentView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v6

    const-string v1, "tag-upload-tasks"

    const-string v0, "tag-upload-records"

    const-string v7, "tag-upload-tasks"

    invoke-virtual {v3, v7}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v7

    check-cast v7, Landroid/app/ListFragment;

    iput-object v7, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadTaskFragment:Landroid/app/ListFragment;

    const-string v7, "tag-upload-records"

    invoke-virtual {v3, v7}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v7

    check-cast v7, Landroid/app/ListFragment;

    iput-object v7, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadRecordsFragment:Landroid/app/ListFragment;

    iget-object v7, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadTaskFragment:Landroid/app/ListFragment;

    if-nez v7, :cond_0

    new-instance v7, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTaskListFragment;

    invoke-direct {v7, p0, v11}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTaskListFragment;-><init>(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$1;)V

    iput-object v7, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadTaskFragment:Landroid/app/ListFragment;

    iget-object v7, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadTaskFragment:Landroid/app/ListFragment;

    new-instance v8, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadTaskCursorAdapter;

    invoke-direct {v8, p0, p0}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadTaskCursorAdapter;-><init>(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;Landroid/content/Context;)V

    invoke-virtual {v7, v8}, Landroid/app/ListFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    iget-object v7, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadTaskFragment:Landroid/app/ListFragment;

    const-string v8, "tag-upload-tasks"

    invoke-virtual {v6, v9, v7, v8}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    :cond_0
    iget-object v7, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadRecordsFragment:Landroid/app/ListFragment;

    if-nez v7, :cond_1

    new-instance v7, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MySummaryListFragment;

    invoke-direct {v7, p0, v11}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MySummaryListFragment;-><init>(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$1;)V

    iput-object v7, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadRecordsFragment:Landroid/app/ListFragment;

    iget-object v7, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadRecordsFragment:Landroid/app/ListFragment;

    new-instance v8, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadRecordCursorAdapter;

    invoke-direct {v8, p0, p0}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadRecordCursorAdapter;-><init>(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;Landroid/content/Context;)V

    invoke-virtual {v7, v8}, Landroid/app/ListFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    iget-object v7, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadRecordsFragment:Landroid/app/ListFragment;

    const-string v8, "tag-upload-records"

    invoke-virtual {v6, v9, v7, v8}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    :cond_1
    iget-object v7, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadTaskFragment:Landroid/app/ListFragment;

    invoke-virtual {v6, v7}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    iget-object v7, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadRecordsFragment:Landroid/app/ListFragment;

    invoke-virtual {v6, v7}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    invoke-virtual {v6}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    invoke-virtual {v3}, Landroid/app/FragmentManager;->executePendingTransactions()Z

    invoke-virtual {p0}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    const/4 v7, 0x2

    invoke-virtual {v2, v7}, Landroid/app/ActionBar;->setNavigationMode(I)V

    const/16 v7, 0xa

    invoke-virtual {v2, v10, v7}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    new-instance v7, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;

    iget-object v8, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mTabPager:Landroid/support/v4/view/ViewPager;

    invoke-direct {v7, p0, v8}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;-><init>(Landroid/app/Activity;Landroid/support/v4/view/ViewPager;)V

    iput-object v7, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mTabPagerAdapter:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;

    iget-object v7, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mTabPagerAdapter:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;

    invoke-virtual {v2}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v8

    const v9, 0x7f06001e

    invoke-virtual {v8, v9}, Landroid/app/ActionBar$Tab;->setText(I)Landroid/app/ActionBar$Tab;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadTaskFragment:Landroid/app/ListFragment;

    invoke-virtual {v7, v8, v9}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;->addTab(Landroid/app/ActionBar$Tab;Landroid/app/Fragment;)V

    iget-object v7, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mTabPagerAdapter:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;

    invoke-virtual {v2}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v8

    const v9, 0x7f06001f

    invoke-virtual {v8, v9}, Landroid/app/ActionBar$Tab;->setText(I)Landroid/app/ActionBar$Tab;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadRecordsFragment:Landroid/app/ListFragment;

    invoke-virtual {v7, v8, v9}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;->addTab(Landroid/app/ActionBar$Tab;Landroid/app/Fragment;)V

    invoke-virtual {p0}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v7

    invoke-virtual {v7, v10, v11, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    invoke-virtual {p0}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {v7, v8, v11, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    new-instance v7, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$1;

    invoke-direct {v7, p0, v4}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$1;-><init>(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;Landroid/os/Handler;)V

    iput-object v7, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadTasksObserver:Landroid/database/ContentObserver;

    new-instance v7, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$2;

    invoke-direct {v7, p0, v4}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$2;-><init>(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;Landroid/os/Handler;)V

    iput-object v7, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadRecordsObserver:Landroid/database/ContentObserver;

    invoke-virtual {p0}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v7, Lcom/google/android/picasasync/PicasaFacade;->uploadsUri:Landroid/net/Uri;

    iget-object v8, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadTasksObserver:Landroid/database/ContentObserver;

    invoke-virtual {v5, v7, v10, v8}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    sget-object v7, Lcom/google/android/picasasync/PicasaFacade;->uploadRecordsUri:Landroid/net/Uri;

    iget-object v8, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadRecordsObserver:Landroid/database/ContentObserver;

    invoke-virtual {v5, v7, v10, v8}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 12
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    const/4 v4, 0x0

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :pswitch_0
    new-instance v0, Landroid/content/CursorLoader;

    sget-object v2, Lcom/google/android/picasasync/PicasaFacade;->uploadsUri:Landroid/net/Uri;

    sget-object v1, Lcom/google/android/picasasync/UploadTaskEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v1}, Lcom/android/gallery3d/common/EntrySchema;->getProjection()[Ljava/lang/String;

    move-result-object v3

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v0

    :pswitch_1
    new-instance v5, Landroid/content/CursorLoader;

    sget-object v7, Lcom/google/android/picasasync/PicasaFacade;->uploadRecordsUri:Landroid/net/Uri;

    sget-object v0, Lcom/google/android/picasasync/UploadedEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v0}, Lcom/android/gallery3d/common/EntrySchema;->getProjection()[Ljava/lang/String;

    move-result-object v8

    const-string v9, "state!=8"

    const-string v11, "uploaded_time DESC"

    move-object v6, p0

    move-object v10, v4

    invoke-direct/range {v5 .. v11}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v5

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    invoke-virtual {p0}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadTasksObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadRecordsObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    return-void
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 2
    .param p2    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    const/4 v1, 0x1

    invoke-virtual {p1}, Landroid/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadTaskFragment:Landroid/app/ListFragment;

    invoke-virtual {v0}, Landroid/app/ListFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/CursorAdapter;

    invoke-virtual {v0, p2}, Landroid/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f060009

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mTabPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    :cond_0
    :goto_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadRecordsFragment:Landroid/app/ListFragment;

    invoke-virtual {v0}, Landroid/app/ListFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/CursorAdapter;

    invoke-virtual {v0, p2}, Landroid/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Landroid/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadTaskFragment:Landroid/app/ListFragment;

    invoke-virtual {v0}, Landroid/app/ListFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/CursorAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    :goto_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadRecordsFragment:Landroid/app/ListFragment;

    invoke-virtual {v0}, Landroid/app/ListFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/CursorAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    const-class v1, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    sput-object v0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->sSummaryActivity:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->finish()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const-class v1, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    monitor-enter v1

    :try_start_0
    sput-object p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->sSummaryActivity:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
