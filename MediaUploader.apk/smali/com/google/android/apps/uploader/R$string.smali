.class public final Lcom/google/android/apps/uploader/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/uploader/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final album_title_hint:I = 0x7f060021

.field public static final cancel:I = 0x7f060025

.field public static final create_album:I = 0x7f060024

.field public static final creating_album:I = 0x7f060026

.field public static final creating_album_done:I = 0x7f060028

.field public static final creating_album_failed:I = 0x7f060027

.field public static final new_album_dialog_title:I = 0x7f060020

.field public static final picasa_title_for_multiple:I = 0x7f060003

.field public static final picasa_title_for_single:I = 0x7f060002

.field public static final picasa_upload:I = 0x7f060000

.field public static final picasa_upload_account:I = 0x7f060005

.field public static final picasa_upload_account_error:I = 0x7f06000c

.field public static final picasa_upload_actionbar_tab_active_uploads:I = 0x7f06001e

.field public static final picasa_upload_actionbar_tab_history:I = 0x7f06001f

.field public static final picasa_upload_add_account:I = 0x7f06000a

.field public static final picasa_upload_album:I = 0x7f060006

.field public static final picasa_upload_caption:I = 0x7f060004

.field public static final picasa_upload_done:I = 0x7f060009

.field public static final picasa_upload_get_album_list_error:I = 0x7f06000d

.field public static final picasa_upload_paused:I = 0x7f060014

.field public static final picasa_upload_paused_auth:I = 0x7f060013

.field public static final picasa_upload_paused_missing_sdcard:I = 0x7f060011

.field public static final picasa_upload_paused_network:I = 0x7f06000f

.field public static final picasa_upload_paused_quota_exceeded:I = 0x7f060012

.field public static final picasa_upload_paused_server_error:I = 0x7f060010

.field public static final picasa_upload_state_cancelled:I = 0x7f060017

.field public static final picasa_upload_state_cancelling:I = 0x7f06001c

.field public static final picasa_upload_state_duplicate:I = 0x7f06001b

.field public static final picasa_upload_state_empty:I = 0x7f06001d

.field public static final picasa_upload_state_failed:I = 0x7f060016

.field public static final picasa_upload_state_invalid_metadata:I = 0x7f06001a

.field public static final picasa_upload_state_queued:I = 0x7f060015

.field public static final picasa_upload_state_quota_exceeded:I = 0x7f060019

.field public static final picasa_upload_state_unauthorized:I = 0x7f060018

.field public static final picasa_upload_toast_no_file_to_upload:I = 0x7f06000e

.field public static final picasa_upload_upload:I = 0x7f060007

.field public static final picasa_upload_upload_start:I = 0x7f06000b

.field public static final picasa_upload_uploading_to:I = 0x7f060008

.field public static final public_album:I = 0x7f060022

.field public static final share_to_picasa:I = 0x7f060001

.field public static final unlisted_album:I = 0x7f060023


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
