.class final Lcom/google/android/picasasync/GDataClient;
.super Ljava/lang/Object;
.source "GDataClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/picasasync/GDataClient$Operation;
    }
.end annotation


# instance fields
.field private mAuthToken:Ljava/lang/String;

.field private mHttpClient:Lorg/apache/http/client/HttpClient;


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "GData/1.0; gzip"

    invoke-static {v0}, Lcom/google/android/picasasync/HttpUtils;->createHttpClient(Ljava/lang/String;)Lorg/apache/http/client/HttpClient;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/picasasync/GDataClient;->mHttpClient:Lorg/apache/http/client/HttpClient;

    return-void
.end method

.method private callMethod(Lorg/apache/http/client/methods/HttpUriRequest;Lcom/google/android/picasasync/GDataClient$Operation;)V
    .locals 17
    .param p1    # Lorg/apache/http/client/methods/HttpUriRequest;
    .param p2    # Lcom/google/android/picasasync/GDataClient$Operation;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v14, "GData-Version"

    const-string v15, "2"

    move-object/from16 v0, p1

    invoke-interface {v0, v14, v15}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    const-string v14, "Accept-Encoding"

    const-string v15, "gzip"

    move-object/from16 v0, p1

    invoke-interface {v0, v14, v15}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/picasasync/GDataClient;->mAuthToken:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_0

    const-string v14, "Authorization"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "GoogleLogin auth="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-interface {v0, v14, v15}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/google/android/picasasync/GDataClient$Operation;->inOutEtag:Ljava/lang/String;

    if-eqz v5, :cond_1

    const-string v14, "If-None-Match"

    move-object/from16 v0, p1

    invoke-interface {v0, v14, v5}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const/4 v8, 0x0

    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v9

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/picasasync/GDataClient;->mHttpClient:Lorg/apache/http/client/HttpClient;

    move-object/from16 v0, p1

    invoke-interface {v14, v0}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v8

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v14

    sub-long/2addr v14, v9

    invoke-static {v14, v15}, Lcom/google/android/picasasync/MetricsUtils;->incrementNetworkOpDuration(J)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-wide/16 v14, 0x1

    invoke-static {v14, v15}, Lcom/google/android/picasasync/MetricsUtils;->incrementNetworkOpCount(J)V

    :goto_0
    const/4 v14, 0x0

    move-object/from16 v0, p2

    iput-object v14, v0, Lcom/google/android/picasasync/GDataClient$Operation;->outBody:Ljava/io/InputStream;

    :try_start_1
    invoke-interface {v8}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v14

    invoke-interface {v14}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v11

    const/4 v12, 0x0

    invoke-interface {v8}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-interface {v4}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v12

    if-eqz v12, :cond_2

    invoke-interface {v4}, Lorg/apache/http/HttpEntity;->getContentEncoding()Lorg/apache/http/Header;

    move-result-object v7

    if-eqz v7, :cond_2

    invoke-interface {v7}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v14

    const-string v15, "gzip"

    invoke-virtual {v14, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_2

    new-instance v13, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v13, v12}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    move-object v12, v13

    :cond_2
    const-string v14, "ETag"

    invoke-interface {v8, v14}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v6

    move-object/from16 v0, p2

    iput v11, v0, Lcom/google/android/picasasync/GDataClient$Operation;->outStatus:I

    if-eqz v6, :cond_4

    invoke-interface {v6}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v14

    :goto_1
    move-object/from16 v0, p2

    iput-object v14, v0, Lcom/google/android/picasasync/GDataClient$Operation;->inOutEtag:Ljava/lang/String;

    move-object/from16 v0, p2

    iput-object v12, v0, Lcom/google/android/picasasync/GDataClient$Operation;->outBody:Ljava/io/InputStream;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object/from16 v0, p2

    iget-object v14, v0, Lcom/google/android/picasasync/GDataClient$Operation;->outBody:Ljava/io/InputStream;

    if-nez v14, :cond_3

    invoke-interface {v8}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-interface {v4}, Lorg/apache/http/HttpEntity;->consumeContent()V

    :cond_3
    return-void

    :catch_0
    move-exception v2

    :try_start_2
    const-string v14, "GDataClient"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Request failed, retry again: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-interface/range {p1 .. p1}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v9

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/picasasync/GDataClient;->mHttpClient:Lorg/apache/http/client/HttpClient;

    move-object/from16 v0, p1

    invoke-interface {v14, v0}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v8

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v14

    sub-long/2addr v14, v9

    invoke-static {v14, v15}, Lcom/google/android/picasasync/MetricsUtils;->incrementNetworkOpDuration(J)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const-wide/16 v14, 0x1

    invoke-static {v14, v15}, Lcom/google/android/picasasync/MetricsUtils;->incrementNetworkOpCount(J)V

    goto/16 :goto_0

    :catch_1
    move-exception v3

    :try_start_4
    const-string v14, "GDataClient"

    const-string v15, "retry failed again"

    invoke-static {v14, v15}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    throw v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catchall_0
    move-exception v14

    const-wide/16 v15, 0x1

    invoke-static/range {v15 .. v16}, Lcom/google/android/picasasync/MetricsUtils;->incrementNetworkOpCount(J)V

    throw v14

    :cond_4
    const/4 v14, 0x0

    goto :goto_1

    :catchall_1
    move-exception v14

    move-object/from16 v0, p2

    iget-object v15, v0, Lcom/google/android/picasasync/GDataClient$Operation;->outBody:Ljava/io/InputStream;

    if-nez v15, :cond_5

    invoke-interface {v8}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v4

    if-eqz v4, :cond_5

    invoke-interface {v4}, Lorg/apache/http/HttpEntity;->consumeContent()V

    :cond_5
    throw v14
.end method

.method private getCompressedEntity([B)Lorg/apache/http/entity/ByteArrayEntity;
    .locals 5
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    array-length v3, p1

    const/16 v4, 0x200

    if-lt v3, v4, :cond_0

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    array-length v3, p1

    div-int/lit8 v3, v3, 0x2

    invoke-direct {v0, v3}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    new-instance v2, Ljava/util/zip/GZIPOutputStream;

    invoke-direct {v2, v0}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {v2, p1}, Ljava/util/zip/GZIPOutputStream;->write([B)V

    invoke-virtual {v2}, Ljava/util/zip/GZIPOutputStream;->close()V

    new-instance v1, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    invoke-direct {v1, v3}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-direct {v1, p1}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    goto :goto_0
.end method

.method private static replaceHttpWithHttps(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;

    const-string v0, "http:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "http:"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    :cond_0
    return-object p0
.end method


# virtual methods
.method public get(Ljava/lang/String;Lcom/google/android/picasasync/GDataClient$Operation;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/picasasync/GDataClient$Operation;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-static {p1}, Lcom/google/android/picasasync/GDataClient;->replaceHttpWithHttps(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0, p2}, Lcom/google/android/picasasync/GDataClient;->callMethod(Lorg/apache/http/client/methods/HttpUriRequest;Lcom/google/android/picasasync/GDataClient$Operation;)V

    return-void
.end method

.method public post(Ljava/lang/String;[BLjava/lang/String;Lcom/google/android/picasasync/GDataClient$Operation;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # [B
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/android/picasasync/GDataClient$Operation;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p2}, Lcom/google/android/picasasync/GDataClient;->getCompressedEntity([B)Lorg/apache/http/entity/ByteArrayEntity;

    move-result-object v0

    invoke-virtual {v0, p3}, Lorg/apache/http/entity/ByteArrayEntity;->setContentType(Ljava/lang/String;)V

    new-instance v1, Lorg/apache/http/client/methods/HttpPost;

    invoke-static {p1}, Lcom/google/android/picasasync/GDataClient;->replaceHttpWithHttps(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    invoke-direct {p0, v1, p4}, Lcom/google/android/picasasync/GDataClient;->callMethod(Lorg/apache/http/client/methods/HttpUriRequest;Lcom/google/android/picasasync/GDataClient$Operation;)V

    return-void
.end method

.method public setAuthToken(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/picasasync/GDataClient;->mAuthToken:Ljava/lang/String;

    return-void
.end method
