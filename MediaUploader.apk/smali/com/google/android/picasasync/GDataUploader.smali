.class Lcom/google/android/picasasync/GDataUploader;
.super Ljava/lang/Object;
.source "GDataUploader.java"

# interfaces
.implements Lcom/google/android/picasasync/Uploader;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/picasasync/GDataUploader$1;,
        Lcom/google/android/picasasync/GDataUploader$GDataResponse;
    }
.end annotation


# static fields
.field private static final RE_RANGE_HEADER:Ljava/util/regex/Pattern;

.field private static sUserAgent:Ljava/lang/String;


# instance fields
.field private mAuthToken:Ljava/lang/String;

.field private mAuthorizer:Lcom/google/android/picasasync/Authorizer;

.field private mContext:Landroid/content/Context;

.field private mHttpClient:Lorg/apache/http/client/HttpClient;

.field private mListener:Lcom/google/android/picasasync/Uploader$UploadProgressListener;

.field private mUploadTask:Lcom/google/android/picasasync/UploadTaskEntry;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "bytes=(\\d+)-(\\d+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/picasasync/GDataUploader;->RE_RANGE_HEADER:Ljava/util/regex/Pattern;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/picasasync/GDataUploader;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/google/android/picasasync/GDataUploader;->getUserAgent(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/picasasync/HttpUtils;->createHttpClient(Ljava/lang/String;)Lorg/apache/http/client/HttpClient;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/picasasync/GDataUploader;->mHttpClient:Lorg/apache/http/client/HttpClient;

    return-void
.end method

.method private executeWithAuthRetry(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    .locals 8
    .param p1    # Lorg/apache/http/client/methods/HttpUriRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/client/ClientProtocolException;,
            Ljava/io/IOException;,
            Lcom/google/android/picasasync/Uploader$UnauthorizedException;
        }
    .end annotation

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-object v5, p0, Lcom/google/android/picasasync/GDataUploader;->mHttpClient:Lorg/apache/http/client/HttpClient;

    invoke-interface {v5, p1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    sub-long/2addr v5, v2

    invoke-static {v5, v6}, Lcom/google/android/picasasync/MetricsUtils;->incrementNetworkOpDuration(J)V

    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v5

    invoke-interface {v5}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v4

    const/16 v5, 0x191

    if-eq v4, v5, :cond_0

    const/16 v5, 0x193

    if-ne v4, v5, :cond_2

    :cond_0
    :try_start_0
    iget-object v5, p0, Lcom/google/android/picasasync/GDataUploader;->mAuthorizer:Lcom/google/android/picasasync/Authorizer;

    iget-object v6, p0, Lcom/google/android/picasasync/GDataUploader;->mUploadTask:Lcom/google/android/picasasync/UploadTaskEntry;

    invoke-virtual {v6}, Lcom/google/android/picasasync/UploadTaskEntry;->getAccount()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/picasasync/GDataUploader;->mAuthToken:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Lcom/google/android/picasasync/Authorizer;->getFreshAuthToken(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/picasasync/GDataUploader;->mAuthToken:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/picasasync/GDataUploader;->mAuthToken:Ljava/lang/String;

    if-nez v5, :cond_1

    new-instance v5, Lcom/google/android/picasasync/Uploader$UnauthorizedException;

    const-string v6, "null auth token"

    invoke-direct {v5, v6}, Lcom/google/android/picasasync/Uploader$UnauthorizedException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_2

    :catch_0
    move-exception v0

    const-string v5, "PicasaUploader"

    const-string v6, "authentication canceled"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v5, Lcom/google/android/picasasync/Uploader$UnauthorizedException;

    invoke-direct {v5, v0}, Lcom/google/android/picasasync/Uploader$UnauthorizedException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    :catch_1
    move-exception v0

    const-string v5, "PicasaUploader"

    const-string v6, "authentication failed"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    throw v0

    :catch_2
    move-exception v0

    const-string v5, "PicasaUploader"

    invoke-static {v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v5, Lcom/google/android/picasasync/Uploader$UnauthorizedException;

    invoke-direct {v5, v0}, Lcom/google/android/picasasync/Uploader$UnauthorizedException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    :cond_1
    const-string v5, "Authorization"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "GoogleLogin auth="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/picasasync/GDataUploader;->mAuthToken:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {p1, v5, v6}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "PicasaUploader"

    const-string v6, "executeWithAuthRetry: attempt #2"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-object v5, p0, Lcom/google/android/picasasync/GDataUploader;->mHttpClient:Lorg/apache/http/client/HttpClient;

    invoke-interface {v5, p1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    sub-long/2addr v5, v2

    invoke-static {v5, v6}, Lcom/google/android/picasasync/MetricsUtils;->incrementNetworkOpDuration(J)V

    :cond_2
    return-object v1
.end method

.method private static getEntity(Lorg/apache/http/HttpResponse;)Lorg/apache/http/HttpEntity;
    .locals 5
    .param p0    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lorg/apache/http/entity/BufferedHttpEntity;

    invoke-interface {p0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/entity/BufferedHttpEntity;-><init>(Lorg/apache/http/HttpEntity;)V

    invoke-virtual {v0}, Lorg/apache/http/entity/BufferedHttpEntity;->getContentLength()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/google/android/picasasync/GDataUploader;->safeConsumeContent(Lorg/apache/http/HttpEntity;)V

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method

.method private static getInitialRequest(Landroid/net/Uri;Ljava/lang/String;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 10
    .param p0    # Landroid/net/Uri;
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    new-instance v7, Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v9}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v6, 0x0

    const-string v9, "\r\n\r\n"

    invoke-virtual {p1, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v9, 0x0

    invoke-virtual {p1, v9, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    :goto_0
    invoke-static {v3}, Lcom/google/android/picasasync/GDataUploader;->parseHeaders(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v7, v5, v8}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_0
    move-object v3, p1

    goto :goto_0

    :cond_1
    if-eqz v6, :cond_2

    new-instance v1, Lorg/apache/http/entity/StringEntity;

    invoke-direct {v1, v6}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;)V

    const/4 v9, 0x0

    check-cast v9, Ljava/lang/String;

    invoke-virtual {v1, v9}, Lorg/apache/http/entity/StringEntity;->setContentType(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    :cond_2
    return-object v7
.end method

.method private static getResumeRequest(Ljava/lang/String;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 3
    .param p0    # Ljava/lang/String;

    new-instance v0, Lorg/apache/http/client/methods/HttpPut;

    invoke-direct {v0, p0}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/lang/String;)V

    const-string v1, "Content-Range"

    const-string v2, "bytes */*"

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpPut;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private static getUploadRequest(Ljava/lang/String;Ljava/lang/String;JIJ[B)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 11
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # J
    .param p4    # I
    .param p5    # J
    .param p7    # [B

    new-instance v6, Lorg/apache/http/client/methods/HttpPut;

    invoke-direct {v6, p0}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/lang/String;)V

    int-to-long v7, p4

    add-long/2addr v7, p2

    const-wide/16 v9, 0x1

    sub-long v4, v7, v9

    const-string v7, "Content-Range"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "bytes "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "-"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-wide/from16 v0, p5

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lorg/apache/http/client/methods/HttpPut;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "Content-Type"

    invoke-virtual {v6, v7, p1}, Lorg/apache/http/client/methods/HttpPut;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Ljava/io/ByteArrayInputStream;

    const/4 v7, 0x0

    move-object/from16 v0, p7

    invoke-direct {v2, v0, v7, p4}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    new-instance v3, Lorg/apache/http/entity/InputStreamEntity;

    int-to-long v7, p4

    invoke-direct {v3, v2, v7, v8}, Lorg/apache/http/entity/InputStreamEntity;-><init>(Ljava/io/InputStream;J)V

    const/4 v7, 0x0

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v3, v7}, Lorg/apache/http/entity/InputStreamEntity;->setContentType(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Lorg/apache/http/client/methods/HttpPut;->setEntity(Lorg/apache/http/HttpEntity;)V

    return-object v6
.end method

.method private static getUserAgent(Landroid/content/Context;)Ljava/lang/String;
    .locals 7
    .param p0    # Landroid/content/Context;

    const/4 v6, 0x1

    const/4 v5, 0x0

    sget-object v2, Lcom/google/android/picasasync/GDataUploader;->sUserAgent:Ljava/lang/String;

    if-nez v2, :cond_0

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    const-string v2, "%s/%s; %s/%s/%s/%s; %s/%s/%s/%d"

    const/16 v3, 0xa

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    aput-object v4, v3, v5

    iget-object v4, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    aput-object v4, v3, v6

    const/4 v4, 0x2

    sget-object v5, Landroid/os/Build;->BRAND:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x3

    sget-object v5, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x4

    sget-object v5, Landroid/os/Build;->MODEL:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x5

    sget-object v5, Landroid/os/Build;->ID:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x6

    sget-object v5, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x7

    sget-object v5, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    aput-object v5, v3, v4

    const/16 v4, 0x8

    sget-object v5, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    aput-object v5, v3, v4

    const/16 v4, 0x9

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/google/android/picasasync/GDataUploader;->sUserAgent:Ljava/lang/String;

    :cond_0
    sget-object v2, Lcom/google/android/picasasync/GDataUploader;->sUserAgent:Ljava/lang/String;

    return-object v2

    :catch_0
    move-exception v0

    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "getPackageInfo failed"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private static isIncompeteStatusCode(I)Z
    .locals 1
    .param p0    # I

    const/16 v0, 0x134

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isSuccessStatusCode(I)Z
    .locals 1
    .param p0    # I

    const/16 v0, 0xc8

    if-eq p0, v0, :cond_0

    const/16 v0, 0xc9

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private newUploadedEntry(Lcom/google/android/picasasync/GDataUploader$GDataResponse;)Lcom/google/android/picasasync/UploadedEntry;
    .locals 10
    .param p1    # Lcom/google/android/picasasync/GDataUploader$GDataResponse;

    new-instance v0, Lcom/google/android/picasasync/UploadedEntry;

    iget-object v1, p0, Lcom/google/android/picasasync/GDataUploader;->mUploadTask:Lcom/google/android/picasasync/UploadTaskEntry;

    iget-wide v2, p1, Lcom/google/android/picasasync/GDataUploader$GDataResponse;->photoId:J

    iget-wide v4, p1, Lcom/google/android/picasasync/GDataUploader$GDataResponse;->photoSize:J

    iget-wide v6, p1, Lcom/google/android/picasasync/GDataUploader$GDataResponse;->timestamp:J

    iget-object v8, p1, Lcom/google/android/picasasync/GDataUploader$GDataResponse;->photoUrl:Ljava/lang/String;

    iget-object v9, p1, Lcom/google/android/picasasync/GDataUploader$GDataResponse;->fingerprint:Lcom/android/gallery3d/common/Fingerprint;

    invoke-virtual {v9}, Lcom/android/gallery3d/common/Fingerprint;->getBytes()[B

    move-result-object v9

    invoke-direct/range {v0 .. v9}, Lcom/google/android/picasasync/UploadedEntry;-><init>(Lcom/google/android/picasasync/UploadTaskEntry;JJJLjava/lang/String;[B)V

    return-object v0
.end method

.method private static parseHeaders(Ljava/lang/String;)Ljava/util/HashMap;
    .locals 9
    .param p0    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const-string v7, "\r\n"

    invoke-virtual {p0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    move-object v0, v5

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v4, v0, v2

    const-string v7, ":"

    invoke-virtual {v4, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    array-length v7, v6

    const/4 v8, 0x2

    if-ne v7, v8, :cond_0

    const/4 v7, 0x0

    aget-object v7, v6, v7

    const/4 v8, 0x1

    aget-object v8, v6, v8

    invoke-virtual {v1, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method private static parseRangeHeaderEndByte(Ljava/lang/String;)J
    .locals 5
    .param p0    # Ljava/lang/String;

    if-eqz p0, :cond_0

    sget-object v1, Lcom/google/android/picasasync/GDataUploader;->RE_RANGE_HEADER:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    :goto_0
    return-wide v1

    :cond_0
    const-wide/16 v1, -0x1

    goto :goto_0
.end method

.method private parseResult(Lorg/apache/http/HttpEntity;)Lcom/google/android/picasasync/GDataUploader$GDataResponse;
    .locals 4
    .param p1    # Lorg/apache/http/HttpEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;,
            Ljava/io/IOException;,
            Lcom/google/android/picasasync/Uploader$UploadException;
        }
    .end annotation

    if-nez p1, :cond_0

    new-instance v2, Lcom/google/android/picasasync/Uploader$UploadException;

    const-string v3, "null HttpEntity in response"

    invoke-direct {v2, v3}, Lcom/google/android/picasasync/Uploader$UploadException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    new-instance v0, Lcom/google/android/picasasync/GDataUploader$GDataResponse;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lcom/google/android/picasasync/GDataUploader$GDataResponse;-><init>(Lcom/google/android/picasasync/GDataUploader$1;)V

    invoke-interface {p1}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v1

    :try_start_0
    sget-object v2, Landroid/util/Xml$Encoding;->UTF_8:Landroid/util/Xml$Encoding;

    invoke-static {v1, v2, v0}, Landroid/util/Xml;->parse(Ljava/io/InputStream;Landroid/util/Xml$Encoding;Lorg/xml/sax/ContentHandler;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    invoke-virtual {v0}, Lcom/google/android/picasasync/GDataUploader$GDataResponse;->validateResult()V

    return-object v0

    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v2
.end method

.method private static readFullyOrToEof(Ljava/io/InputStream;[BII)I
    .locals 4
    .param p0    # Ljava/io/InputStream;
    .param p1    # [B
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p3, :cond_0

    add-int v2, p2, v0

    sub-int v3, p3, v0

    invoke-virtual {p0, p1, v2, v3}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    :cond_0
    return v0

    :cond_1
    add-int/2addr v0, v1

    goto :goto_0
.end method

.method private resetUpload()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/picasasync/GDataUploader;->mUploadTask:Lcom/google/android/picasasync/UploadTaskEntry;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/picasasync/UploadTaskEntry;->setUploadUrl(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/picasasync/GDataUploader;->mUploadTask:Lcom/google/android/picasasync/UploadTaskEntry;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/picasasync/UploadTaskEntry;->setBytesUploaded(J)V

    return-void
.end method

.method private resume(Ljava/io/InputStream;)Lcom/google/android/picasasync/UploadedEntry;
    .locals 11
    .param p1    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/client/ClientProtocolException;,
            Ljava/io/IOException;,
            Lcom/google/android/picasasync/Uploader$PicasaQuotaException;,
            Lorg/xml/sax/SAXException;,
            Lcom/google/android/picasasync/Uploader$UploadException;,
            Lcom/google/android/picasasync/Uploader$LocalIoException;,
            Lcom/google/android/picasasync/Uploader$MediaFileChangedException;,
            Lcom/google/android/picasasync/Uploader$RestartException;,
            Lcom/google/android/picasasync/Uploader$UnauthorizedException;
        }
    .end annotation

    iget-object v8, p0, Lcom/google/android/picasasync/GDataUploader;->mUploadTask:Lcom/google/android/picasasync/UploadTaskEntry;

    invoke-virtual {v8}, Lcom/google/android/picasasync/UploadTaskEntry;->getUploadUrl()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/picasasync/GDataUploader;->getResumeRequest(Ljava/lang/String;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/google/android/picasasync/GDataUploader;->executeWithAuthRetry(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v7

    invoke-interface {v7}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v8

    invoke-interface {v8}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    invoke-static {v7}, Lcom/google/android/picasasync/GDataUploader;->getEntity(Lorg/apache/http/HttpResponse;)Lorg/apache/http/HttpEntity;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v8, "PicasaUploader"

    const-string v9, "  Entity: content length was 0."

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :try_start_0
    invoke-static {v0}, Lcom/google/android/picasasync/GDataUploader;->isIncompeteStatusCode(I)Z

    move-result v8

    if-eqz v8, :cond_2

    const-string v8, "range"

    invoke-interface {v7, v8}, Lorg/apache/http/HttpResponse;->containsHeader(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    const-string v8, "range"

    invoke-interface {v7, v8}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v4

    invoke-interface {v4}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/picasasync/GDataUploader;->parseRangeHeaderEndByte(Ljava/lang/String;)J

    move-result-wide v2

    const-wide/16 v8, 0x0

    cmp-long v8, v2, v8

    if-gez v8, :cond_1

    invoke-direct {p0}, Lcom/google/android/picasasync/GDataUploader;->resetUpload()V

    new-instance v8, Lcom/google/android/picasasync/Uploader$RestartException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "negative range offset: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/google/android/picasasync/Uploader$RestartException;-><init>(Ljava/lang/String;)V

    throw v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v8

    invoke-static {v1}, Lcom/google/android/picasasync/GDataUploader;->safeConsumeContent(Lorg/apache/http/HttpEntity;)V

    throw v8

    :cond_1
    :try_start_1
    invoke-virtual {p1, v2, v3}, Ljava/io/InputStream;->skip(J)J

    const/high16 v8, 0x40000

    invoke-virtual {p1, v8}, Ljava/io/InputStream;->mark(I)V

    iget-object v8, p0, Lcom/google/android/picasasync/GDataUploader;->mUploadTask:Lcom/google/android/picasasync/UploadTaskEntry;

    invoke-virtual {v8, v2, v3}, Lcom/google/android/picasasync/UploadTaskEntry;->setBytesUploaded(J)V

    invoke-direct {p0, p1}, Lcom/google/android/picasasync/GDataUploader;->uploadChunks(Ljava/io/InputStream;)Lcom/google/android/picasasync/UploadedEntry;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v8

    invoke-static {v1}, Lcom/google/android/picasasync/GDataUploader;->safeConsumeContent(Lorg/apache/http/HttpEntity;)V

    :goto_0
    return-object v8

    :cond_2
    :try_start_2
    invoke-static {v0}, Lcom/google/android/picasasync/GDataUploader;->isSuccessStatusCode(I)Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-direct {p0, v1}, Lcom/google/android/picasasync/GDataUploader;->parseResult(Lorg/apache/http/HttpEntity;)Lcom/google/android/picasasync/GDataUploader$GDataResponse;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/google/android/picasasync/GDataUploader;->throwIfQuotaError(Lcom/google/android/picasasync/GDataUploader$GDataResponse;)V

    const-string v8, "PicasaUploader"

    const-string v9, "nothing to resume, upload already completed"

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v8, p0, Lcom/google/android/picasasync/GDataUploader;->mUploadTask:Lcom/google/android/picasasync/UploadTaskEntry;

    iget-object v9, p0, Lcom/google/android/picasasync/GDataUploader;->mUploadTask:Lcom/google/android/picasasync/UploadTaskEntry;

    invoke-virtual {v9}, Lcom/google/android/picasasync/UploadTaskEntry;->getBytesTotal()J

    move-result-wide v9

    invoke-virtual {v8, v9, v10}, Lcom/google/android/picasasync/UploadTaskEntry;->setBytesUploaded(J)V

    invoke-direct {p0, v5}, Lcom/google/android/picasasync/GDataUploader;->newUploadedEntry(Lcom/google/android/picasasync/GDataUploader$GDataResponse;)Lcom/google/android/picasasync/UploadedEntry;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v8

    invoke-static {v1}, Lcom/google/android/picasasync/GDataUploader;->safeConsumeContent(Lorg/apache/http/HttpEntity;)V

    goto :goto_0

    :cond_3
    const/16 v8, 0x191

    if-ne v0, v8, :cond_4

    :try_start_3
    new-instance v8, Lcom/google/android/picasasync/Uploader$UnauthorizedException;

    invoke-interface {v7}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/google/android/picasasync/Uploader$UnauthorizedException;-><init>(Ljava/lang/String;)V

    throw v8

    :cond_4
    invoke-direct {p0}, Lcom/google/android/picasasync/GDataUploader;->resetUpload()V

    new-instance v8, Lcom/google/android/picasasync/Uploader$RestartException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "unexpected resume response: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {v7}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/google/android/picasasync/Uploader$RestartException;-><init>(Ljava/lang/String;)V

    throw v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method

.method private static safeConsumeContent(Lorg/apache/http/HttpEntity;)V
    .locals 1
    .param p0    # Lorg/apache/http/HttpEntity;

    if-eqz p0, :cond_0

    :try_start_0
    invoke-interface {p0}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private start(Ljava/io/InputStream;Landroid/net/Uri;Ljava/lang/String;)Lcom/google/android/picasasync/UploadedEntry;
    .locals 8
    .param p1    # Ljava/io/InputStream;
    .param p2    # Landroid/net/Uri;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/client/ClientProtocolException;,
            Ljava/io/IOException;,
            Lcom/google/android/picasasync/Uploader$PicasaQuotaException;,
            Lorg/xml/sax/SAXException;,
            Lcom/google/android/picasasync/Uploader$UploadException;,
            Lcom/google/android/picasasync/Uploader$MediaFileChangedException;,
            Lcom/google/android/picasasync/Uploader$UnauthorizedException;,
            Lcom/google/android/picasasync/Uploader$RestartException;,
            Lcom/google/android/picasasync/Uploader$LocalIoException;
        }
    .end annotation

    invoke-static {p2, p3}, Lcom/google/android/picasasync/GDataUploader;->getInitialRequest(Landroid/net/Uri;Ljava/lang/String;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/picasasync/GDataUploader;->executeWithAuthRetry(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/picasasync/GDataUploader;->getEntity(Lorg/apache/http/HttpResponse;)Lorg/apache/http/HttpEntity;

    move-result-object v1

    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v5

    invoke-interface {v5}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    :try_start_0
    invoke-static {v0}, Lcom/google/android/picasasync/GDataUploader;->isSuccessStatusCode(I)Z

    move-result v5

    if-eqz v5, :cond_1

    if-eqz v1, :cond_0

    invoke-direct {p0, v1}, Lcom/google/android/picasasync/GDataUploader;->parseResult(Lorg/apache/http/HttpEntity;)Lcom/google/android/picasasync/GDataUploader$GDataResponse;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/google/android/picasasync/GDataUploader;->throwIfQuotaError(Lcom/google/android/picasasync/GDataUploader$GDataResponse;)V

    :cond_0
    const-string v5, "Location"

    invoke-interface {v3, v5}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/picasasync/GDataUploader;->mUploadTask:Lcom/google/android/picasasync/UploadTaskEntry;

    invoke-interface {v4}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/picasasync/UploadTaskEntry;->setUploadUrl(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/picasasync/GDataUploader;->mUploadTask:Lcom/google/android/picasasync/UploadTaskEntry;

    const-wide/16 v6, 0x0

    invoke-virtual {v5, v6, v7}, Lcom/google/android/picasasync/UploadTaskEntry;->setBytesUploaded(J)V

    invoke-direct {p0, p1}, Lcom/google/android/picasasync/GDataUploader;->uploadChunks(Ljava/io/InputStream;)Lcom/google/android/picasasync/UploadedEntry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    invoke-static {v1}, Lcom/google/android/picasasync/GDataUploader;->safeConsumeContent(Lorg/apache/http/HttpEntity;)V

    return-object v5

    :cond_1
    const/16 v5, 0x190

    if-ne v0, v5, :cond_2

    :try_start_1
    new-instance v5, Lcom/google/android/picasasync/Uploader$UploadException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "upload failed (bad payload, file too large) "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/android/picasasync/Uploader$UploadException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v5

    invoke-static {v1}, Lcom/google/android/picasasync/GDataUploader;->safeConsumeContent(Lorg/apache/http/HttpEntity;)V

    throw v5

    :cond_2
    const/16 v5, 0x191

    if-ne v0, v5, :cond_3

    :try_start_2
    new-instance v5, Lcom/google/android/picasasync/Uploader$UnauthorizedException;

    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/android/picasasync/Uploader$UnauthorizedException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_3
    const/16 v5, 0x1f4

    if-lt v0, v5, :cond_4

    const/16 v5, 0x258

    if-ge v0, v5, :cond_4

    new-instance v5, Lcom/google/android/picasasync/Uploader$RestartException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "upload transient error:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/android/picasasync/Uploader$RestartException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_4
    new-instance v5, Lcom/google/android/picasasync/Uploader$UploadException;

    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/android/picasasync/Uploader$UploadException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method private throwIfQuotaError(Lcom/google/android/picasasync/GDataUploader$GDataResponse;)V
    .locals 2
    .param p1    # Lcom/google/android/picasasync/GDataUploader$GDataResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/picasasync/Uploader$PicasaQuotaException;
        }
    .end annotation

    if-eqz p1, :cond_0

    const-string v0, "LimitQuota"

    iget-object v1, p1, Lcom/google/android/picasasync/GDataUploader$GDataResponse;->errorCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/picasasync/Uploader$PicasaQuotaException;

    iget-object v1, p1, Lcom/google/android/picasasync/GDataUploader$GDataResponse;->errorCode:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/picasasync/Uploader$PicasaQuotaException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method private uploadChunks(Ljava/io/InputStream;)Lcom/google/android/picasasync/UploadedEntry;
    .locals 24
    .param p1    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/client/ClientProtocolException;,
            Ljava/io/IOException;,
            Lcom/google/android/picasasync/Uploader$PicasaQuotaException;,
            Lorg/xml/sax/SAXException;,
            Lcom/google/android/picasasync/Uploader$UploadException;,
            Lcom/google/android/picasasync/Uploader$MediaFileChangedException;,
            Lcom/google/android/picasasync/Uploader$RestartException;,
            Lcom/google/android/picasasync/Uploader$LocalIoException;,
            Lcom/google/android/picasasync/Uploader$UnauthorizedException;
        }
    .end annotation

    const/high16 v2, 0x40000

    new-array v9, v2, [B

    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/picasasync/GDataUploader;->mUploadTask:Lcom/google/android/picasasync/UploadTaskEntry;

    invoke-virtual {v2}, Lcom/google/android/picasasync/UploadTaskEntry;->getBytesUploaded()J

    move-result-wide v2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/picasasync/GDataUploader;->mUploadTask:Lcom/google/android/picasasync/UploadTaskEntry;

    invoke-virtual {v7}, Lcom/google/android/picasasync/UploadTaskEntry;->getBytesTotal()J

    move-result-wide v7

    cmp-long v2, v2, v7

    if-gez v2, :cond_d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/picasasync/GDataUploader;->mListener:Lcom/google/android/picasasync/Uploader$UploadProgressListener;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/picasasync/GDataUploader;->mListener:Lcom/google/android/picasasync/Uploader$UploadProgressListener;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/picasasync/GDataUploader;->mUploadTask:Lcom/google/android/picasasync/UploadTaskEntry;

    invoke-interface {v2, v3}, Lcom/google/android/picasasync/Uploader$UploadProgressListener;->onProgress(Lcom/google/android/picasasync/UploadTaskEntry;)V

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/picasasync/GDataUploader;->mUploadTask:Lcom/google/android/picasasync/UploadTaskEntry;

    invoke-virtual {v2}, Lcom/google/android/picasasync/UploadTaskEntry;->isUploading()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x0

    :goto_1
    return-object v2

    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/picasasync/GDataUploader;->mUploadTask:Lcom/google/android/picasasync/UploadTaskEntry;

    invoke-virtual {v2}, Lcom/google/android/picasasync/UploadTaskEntry;->getBytesUploaded()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/picasasync/GDataUploader;->mUploadTask:Lcom/google/android/picasasync/UploadTaskEntry;

    invoke-virtual {v2}, Lcom/google/android/picasasync/UploadTaskEntry;->getBytesTotal()J

    move-result-wide v2

    sub-long/2addr v2, v4

    long-to-int v6, v2

    const/high16 v2, 0x40000

    if-gt v6, v2, :cond_4

    const/16 v17, 0x1

    :goto_2
    if-nez v17, :cond_2

    const/high16 v6, 0x40000

    :cond_2
    const/high16 v2, 0x40000

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/io/InputStream;->mark(I)V

    const/4 v2, 0x0

    :try_start_0
    move-object/from16 v0, p1

    invoke-static {v0, v9, v2, v6}, Lcom/google/android/picasasync/GDataUploader;->readFullyOrToEof(Ljava/io/InputStream;[BII)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v19

    if-nez v17, :cond_3

    move/from16 v0, v19

    if-eq v0, v6, :cond_5

    :cond_3
    const/4 v2, 0x1

    new-array v12, v2, [J

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/picasasync/GDataUploader;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/picasasync/GDataUploader;->mUploadTask:Lcom/google/android/picasasync/UploadTaskEntry;

    invoke-virtual {v3}, Lcom/google/android/picasasync/UploadTaskEntry;->getContentUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v2

    invoke-static {v2, v12}, Lcom/android/gallery3d/common/Fingerprint;->fromInputStream(Ljava/io/InputStream;[J)Lcom/android/gallery3d/common/Fingerprint;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/picasasync/GDataUploader;->mUploadTask:Lcom/google/android/picasasync/UploadTaskEntry;

    invoke-virtual {v2}, Lcom/google/android/picasasync/UploadTaskEntry;->getFingerprint()Lcom/android/gallery3d/common/Fingerprint;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Lcom/android/gallery3d/common/Fingerprint;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/picasasync/GDataUploader;->mUploadTask:Lcom/google/android/picasasync/UploadTaskEntry;

    invoke-virtual {v2}, Lcom/google/android/picasasync/UploadTaskEntry;->getContentUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/picasasync/GDataUploader;->mUploadTask:Lcom/google/android/picasasync/UploadTaskEntry;

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/google/android/picasasync/UploadTaskEntry;->setFingerprint(Lcom/android/gallery3d/common/Fingerprint;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/picasasync/GDataUploader;->mUploadTask:Lcom/google/android/picasasync/UploadTaskEntry;

    invoke-virtual {v2}, Lcom/google/android/picasasync/UploadTaskEntry;->getUploadUrl()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/picasasync/GDataUploader;->mUploadTask:Lcom/google/android/picasasync/UploadTaskEntry;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/picasasync/UploadTaskEntry;->setUploadUrl(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/picasasync/GDataUploader;->mUploadTask:Lcom/google/android/picasasync/UploadTaskEntry;

    const-wide/16 v7, 0x0

    invoke-virtual {v2, v7, v8}, Lcom/google/android/picasasync/UploadTaskEntry;->setBytesUploaded(J)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/picasasync/GDataUploader;->mUploadTask:Lcom/google/android/picasasync/UploadTaskEntry;

    const/4 v3, 0x0

    aget-wide v7, v12, v3

    invoke-virtual {v2, v7, v8}, Lcom/google/android/picasasync/UploadTaskEntry;->setBytesTotal(J)V

    new-instance v2, Lcom/google/android/picasasync/Uploader$MediaFileChangedException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "UPLOAD_SIZE_DATA_MISMATCH: fingerprint changed; uri="

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, ",uploadUrl="

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v23

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/picasasync/Uploader$MediaFileChangedException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_4
    const/16 v17, 0x0

    goto/16 :goto_2

    :catch_0
    move-exception v15

    new-instance v2, Lcom/google/android/picasasync/Uploader$LocalIoException;

    invoke-direct {v2, v15}, Lcom/google/android/picasasync/Uploader$LocalIoException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/picasasync/GDataUploader;->mUploadTask:Lcom/google/android/picasasync/UploadTaskEntry;

    invoke-virtual {v2}, Lcom/google/android/picasasync/UploadTaskEntry;->getUploadUrl()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/picasasync/GDataUploader;->mUploadTask:Lcom/google/android/picasasync/UploadTaskEntry;

    invoke-virtual {v3}, Lcom/google/android/picasasync/UploadTaskEntry;->getMimeType()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/picasasync/GDataUploader;->mUploadTask:Lcom/google/android/picasasync/UploadTaskEntry;

    invoke-virtual {v7}, Lcom/google/android/picasasync/UploadTaskEntry;->getBytesTotal()J

    move-result-wide v7

    invoke-static/range {v2 .. v9}, Lcom/google/android/picasasync/GDataUploader;->getUploadRequest(Ljava/lang/String;Ljava/lang/String;JIJ[B)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/google/android/picasasync/GDataUploader;->executeWithAuthRetry(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v22

    :try_start_1
    invoke-interface/range {v22 .. v22}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v13

    invoke-static {v13}, Lcom/google/android/picasasync/GDataUploader;->isSuccessStatusCode(I)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-static/range {v22 .. v22}, Lcom/google/android/picasasync/GDataUploader;->getEntity(Lorg/apache/http/HttpResponse;)Lorg/apache/http/HttpEntity;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/picasasync/GDataUploader;->parseResult(Lorg/apache/http/HttpEntity;)Lcom/google/android/picasasync/GDataUploader$GDataResponse;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/google/android/picasasync/GDataUploader;->throwIfQuotaError(Lcom/google/android/picasasync/GDataUploader$GDataResponse;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/picasasync/GDataUploader;->mUploadTask:Lcom/google/android/picasasync/UploadTaskEntry;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/picasasync/GDataUploader;->mUploadTask:Lcom/google/android/picasasync/UploadTaskEntry;

    invoke-virtual {v3}, Lcom/google/android/picasasync/UploadTaskEntry;->getBytesTotal()J

    move-result-wide v7

    invoke-virtual {v2, v7, v8}, Lcom/google/android/picasasync/UploadTaskEntry;->setBytesUploaded(J)V

    const-wide/16 v2, 0x1

    invoke-static {v2, v3}, Lcom/google/android/picasasync/MetricsUtils;->incrementNetworkOpCount(J)V

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/google/android/picasasync/GDataUploader;->newUploadedEntry(Lcom/google/android/picasasync/GDataUploader$GDataResponse;)Lcom/google/android/picasasync/UploadedEntry;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    invoke-interface/range {v22 .. v22}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/picasasync/GDataUploader;->safeConsumeContent(Lorg/apache/http/HttpEntity;)V

    goto/16 :goto_1

    :cond_6
    :try_start_2
    invoke-static {v13}, Lcom/google/android/picasasync/GDataUploader;->isIncompeteStatusCode(I)Z

    move-result v2

    if-eqz v2, :cond_a

    const-string v2, "range"

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v18

    if-eqz v18, :cond_7

    invoke-interface/range {v18 .. v18}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/picasasync/GDataUploader;->parseRangeHeaderEndByte(Ljava/lang/String;)J

    move-result-wide v10

    :goto_3
    const-wide/16 v2, 0x0

    cmp-long v2, v10, v2

    if-gez v2, :cond_8

    new-instance v2, Lcom/google/android/picasasync/Uploader$UploadException;

    const-string v3, "malformed or missing range header for subsequent upload"

    invoke-direct {v2, v3}, Lcom/google/android/picasasync/Uploader$UploadException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v2

    invoke-interface/range {v22 .. v22}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/picasasync/GDataUploader;->safeConsumeContent(Lorg/apache/http/HttpEntity;)V

    throw v2

    :cond_7
    const-wide/16 v10, -0x1

    goto :goto_3

    :cond_8
    int-to-long v2, v6

    add-long/2addr v2, v4

    cmp-long v2, v10, v2

    if-gez v2, :cond_9

    :try_start_3
    invoke-virtual/range {p1 .. p1}, Ljava/io/InputStream;->reset()V

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v11}, Ljava/io/InputStream;->skip(J)J

    :cond_9
    move-wide v4, v10

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/picasasync/GDataUploader;->mUploadTask:Lcom/google/android/picasasync/UploadTaskEntry;

    invoke-virtual {v2, v4, v5}, Lcom/google/android/picasasync/UploadTaskEntry;->setBytesUploaded(J)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-interface/range {v22 .. v22}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/picasasync/GDataUploader;->safeConsumeContent(Lorg/apache/http/HttpEntity;)V

    goto/16 :goto_0

    :cond_a
    const/16 v2, 0x190

    if-ne v13, v2, :cond_b

    :try_start_4
    new-instance v2, Lcom/google/android/picasasync/Uploader$UploadException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "upload failed (bad payload, file too large) "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface/range {v22 .. v22}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/picasasync/Uploader$UploadException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_b
    const/16 v2, 0x1f4

    if-lt v13, v2, :cond_c

    const/16 v2, 0x258

    if-ge v13, v2, :cond_c

    new-instance v2, Lcom/google/android/picasasync/Uploader$RestartException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "upload transient error"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface/range {v22 .. v22}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/picasasync/Uploader$RestartException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_c
    new-instance v2, Lcom/google/android/picasasync/Uploader$UploadException;

    invoke-interface/range {v22 .. v22}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/picasasync/Uploader$UploadException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_d
    new-instance v2, Lcom/google/android/picasasync/Uploader$UploadException;

    const-string v3, "upload is done but no server confirmation"

    invoke-direct {v2, v3}, Lcom/google/android/picasasync/Uploader$UploadException;-><init>(Ljava/lang/String;)V

    throw v2
.end method


# virtual methods
.method public close()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/picasasync/GDataUploader;->mHttpClient:Lorg/apache/http/client/HttpClient;

    return-void
.end method

.method public upload(Lcom/google/android/picasasync/UploadTaskEntry;Lcom/google/android/picasasync/Uploader$UploadProgressListener;)Lcom/google/android/picasasync/UploadedEntry;
    .locals 9
    .param p1    # Lcom/google/android/picasasync/UploadTaskEntry;
    .param p2    # Lcom/google/android/picasasync/Uploader$UploadProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/picasasync/Uploader$UploadException;,
            Ljava/io/IOException;,
            Lcom/google/android/picasasync/Uploader$RestartException;,
            Lcom/google/android/picasasync/Uploader$MediaFileChangedException;,
            Lcom/google/android/picasasync/Uploader$UnauthorizedException;,
            Lcom/google/android/picasasync/Uploader$PicasaQuotaException;,
            Lcom/google/android/picasasync/Uploader$LocalIoException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->getBytesTotal()J

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-gtz v5, :cond_0

    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "Zero length file can\'t be uploaded"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_0
    iput-object p1, p0, Lcom/google/android/picasasync/GDataUploader;->mUploadTask:Lcom/google/android/picasasync/UploadTaskEntry;

    iput-object p2, p0, Lcom/google/android/picasasync/GDataUploader;->mListener:Lcom/google/android/picasasync/Uploader$UploadProgressListener;

    new-instance v5, Lcom/google/android/picasasync/Authorizer;

    iget-object v6, p0, Lcom/google/android/picasasync/GDataUploader;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->getAuthTokenType()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/google/android/picasasync/Authorizer;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v5, p0, Lcom/google/android/picasasync/GDataUploader;->mAuthorizer:Lcom/google/android/picasasync/Authorizer;

    :try_start_0
    iget-object v5, p0, Lcom/google/android/picasasync/GDataUploader;->mAuthorizer:Lcom/google/android/picasasync/Authorizer;

    iget-object v6, p0, Lcom/google/android/picasasync/GDataUploader;->mUploadTask:Lcom/google/android/picasasync/UploadTaskEntry;

    invoke-virtual {v6}, Lcom/google/android/picasasync/UploadTaskEntry;->getAccount()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/picasasync/Authorizer;->getAuthToken(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/picasasync/GDataUploader;->mAuthToken:Ljava/lang/String;
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_2

    iget-object v5, p0, Lcom/google/android/picasasync/GDataUploader;->mUploadTask:Lcom/google/android/picasasync/UploadTaskEntry;

    invoke-virtual {v5}, Lcom/google/android/picasasync/UploadTaskEntry;->getRequestTemplate()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/picasasync/GDataUploader;->mAuthToken:Ljava/lang/String;

    if-nez v5, :cond_1

    const-string v5, "Authorization: GoogleLogin auth=%=_auth_token_=%\r\n"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :goto_0
    const/4 v2, 0x0

    :try_start_1
    iget-object v5, p0, Lcom/google/android/picasasync/GDataUploader;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->getContentUri()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v1

    new-instance v3, Ljava/io/BufferedInputStream;

    invoke-direct {v3, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Lorg/xml/sax/SAXException; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v5, p0, Lcom/google/android/picasasync/GDataUploader;->mUploadTask:Lcom/google/android/picasasync/UploadTaskEntry;

    invoke-virtual {v5}, Lcom/google/android/picasasync/UploadTaskEntry;->getUploadUrl()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/android/picasasync/GDataUploader;->mUploadTask:Lcom/google/android/picasasync/UploadTaskEntry;

    invoke-virtual {v5}, Lcom/google/android/picasasync/UploadTaskEntry;->getUrl()Landroid/net/Uri;

    move-result-object v5

    invoke-direct {p0, v3, v5, v4}, Lcom/google/android/picasasync/GDataUploader;->start(Ljava/io/InputStream;Landroid/net/Uri;Ljava/lang/String;)Lcom/google/android/picasasync/UploadedEntry;
    :try_end_2
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_2 .. :try_end_2} :catch_8
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_7
    .catch Lorg/xml/sax/SAXException; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v5

    invoke-static {v3}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    :goto_1
    return-object v5

    :catch_0
    move-exception v0

    const-string v5, "PicasaUploader"

    const-string v6, "authentication canceled"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v5, Lcom/google/android/picasasync/Uploader$UnauthorizedException;

    invoke-direct {v5, v0}, Lcom/google/android/picasasync/Uploader$UnauthorizedException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    :catch_1
    move-exception v0

    const-string v5, "PicasaUploader"

    const-string v6, "authentication failed"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    throw v0

    :catch_2
    move-exception v0

    const-string v5, "PicasaUploader"

    invoke-static {v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v5, Lcom/google/android/picasasync/Uploader$UnauthorizedException;

    invoke-direct {v5, v0}, Lcom/google/android/picasasync/Uploader$UnauthorizedException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    :cond_1
    const-string v5, "%=_auth_token_=%"

    iget-object v6, p0, Lcom/google/android/picasasync/GDataUploader;->mAuthToken:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_2
    :try_start_3
    invoke-direct {p0, v3}, Lcom/google/android/picasasync/GDataUploader;->resume(Ljava/io/InputStream;)Lcom/google/android/picasasync/UploadedEntry;
    :try_end_3
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_3 .. :try_end_3} :catch_8
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_7
    .catch Lorg/xml/sax/SAXException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v5

    invoke-static {v3}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_1

    :catch_3
    move-exception v0

    :goto_2
    :try_start_4
    new-instance v5, Ljava/io/IOException;

    invoke-virtual {v0}, Lorg/apache/http/client/ClientProtocolException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catchall_0
    move-exception v5

    :goto_3
    invoke-static {v2}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v5

    :catch_4
    move-exception v0

    :goto_4
    :try_start_5
    new-instance v5, Lcom/google/android/picasasync/Uploader$LocalIoException;

    invoke-direct {v5, v0}, Lcom/google/android/picasasync/Uploader$LocalIoException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    :catch_5
    move-exception v0

    :goto_5
    const-string v5, "PicasaUploader"

    const-string v6, "error in parsing response"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v5, Lcom/google/android/picasasync/Uploader$UploadException;

    const-string v6, "error in parsing response"

    invoke-direct {v5, v6, v0}, Lcom/google/android/picasasync/Uploader$UploadException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catchall_1
    move-exception v5

    move-object v2, v3

    goto :goto_3

    :catch_6
    move-exception v0

    move-object v2, v3

    goto :goto_5

    :catch_7
    move-exception v0

    move-object v2, v3

    goto :goto_4

    :catch_8
    move-exception v0

    move-object v2, v3

    goto :goto_2
.end method
