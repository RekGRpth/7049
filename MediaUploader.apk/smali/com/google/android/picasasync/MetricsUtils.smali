.class public Lcom/google/android/picasasync/MetricsUtils;
.super Ljava/lang/Object;
.source "MetricsUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/picasasync/MetricsUtils$Metrics;
    }
.end annotation


# static fields
.field private static final LOG_DURATION_LIMIT:J

.field static sFreeMetrics:Lcom/google/android/picasasync/MetricsUtils$Metrics;

.field private static final sMetricsStack:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/picasasync/MetricsUtils$Metrics;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/picasasync/MetricsUtils;->sFreeMetrics:Lcom/google/android/picasasync/MetricsUtils$Metrics;

    const-string v0, "picasasync.metrics.time"

    const-wide/16 v1, 0x64

    invoke-static {v0, v1, v2}, Lcom/google/android/picasasync/SystemProperties;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/picasasync/MetricsUtils;->LOG_DURATION_LIMIT:J

    new-instance v0, Lcom/google/android/picasasync/MetricsUtils$1;

    invoke-direct {v0}, Lcom/google/android/picasasync/MetricsUtils$1;-><init>()V

    sput-object v0, Lcom/google/android/picasasync/MetricsUtils;->sMetricsStack:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static begin(Ljava/lang/String;)I
    .locals 2
    .param p0    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/picasasync/MetricsUtils;->sMetricsStack:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-static {p0}, Lcom/google/android/picasasync/MetricsUtils$Metrics;->obtain(Ljava/lang/String;)Lcom/google/android/picasasync/MetricsUtils$Metrics;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    return v1
.end method

.method public static end(I)V
    .locals 7
    .param p0    # I

    sget-object v2, Lcom/google/android/picasasync/MetricsUtils;->sMetricsStack:Ljava/lang/ThreadLocal;

    invoke-virtual {v2}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-gt p0, v2, :cond_0

    if-gtz p0, :cond_1

    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "size: %s, id: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    :goto_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge p0, v2, :cond_3

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/picasasync/MetricsUtils$Metrics;

    const-string v2, "MetricsUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "WARNING: unclosed metrics: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/picasasync/MetricsUtils$Metrics;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/picasasync/MetricsUtils$Metrics;

    invoke-virtual {v2, v0}, Lcom/google/android/picasasync/MetricsUtils$Metrics;->merge(Lcom/google/android/picasasync/MetricsUtils$Metrics;)V

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/picasasync/MetricsUtils$Metrics;->recycle()V

    goto :goto_0

    :cond_3
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/picasasync/MetricsUtils$Metrics;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/picasasync/MetricsUtils$Metrics;->endTimestamp:J

    sget-wide v2, Lcom/google/android/picasasync/MetricsUtils;->LOG_DURATION_LIMIT:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-ltz v2, :cond_4

    iget-wide v2, v0, Lcom/google/android/picasasync/MetricsUtils$Metrics;->endTimestamp:J

    iget-wide v4, v0, Lcom/google/android/picasasync/MetricsUtils$Metrics;->startTimestamp:J

    sub-long/2addr v2, v4

    sget-wide v4, Lcom/google/android/picasasync/MetricsUtils;->LOG_DURATION_LIMIT:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_4

    const-string v2, "MetricsUtils"

    invoke-virtual {v0}, Lcom/google/android/picasasync/MetricsUtils$Metrics;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/picasasync/MetricsUtils$Metrics;

    invoke-virtual {v2, v0}, Lcom/google/android/picasasync/MetricsUtils$Metrics;->merge(Lcom/google/android/picasasync/MetricsUtils$Metrics;)V

    :cond_5
    invoke-virtual {v0}, Lcom/google/android/picasasync/MetricsUtils$Metrics;->recycle()V

    return-void
.end method

.method public static incrementInBytes(J)V
    .locals 5
    .param p0    # J

    sget-object v3, Lcom/google/android/picasasync/MetricsUtils;->sMetricsStack:Ljava/lang/ThreadLocal;

    invoke-virtual {v3}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    add-int/lit8 v3, v1, -0x1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/picasasync/MetricsUtils$Metrics;

    iget-wide v3, v0, Lcom/google/android/picasasync/MetricsUtils$Metrics;->inBytes:J

    add-long/2addr v3, p0

    iput-wide v3, v0, Lcom/google/android/picasasync/MetricsUtils$Metrics;->inBytes:J

    :cond_0
    return-void
.end method

.method public static incrementNetworkOpCount(J)V
    .locals 5
    .param p0    # J

    sget-object v3, Lcom/google/android/picasasync/MetricsUtils;->sMetricsStack:Ljava/lang/ThreadLocal;

    invoke-virtual {v3}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    add-int/lit8 v3, v1, -0x1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/picasasync/MetricsUtils$Metrics;

    iget v3, v0, Lcom/google/android/picasasync/MetricsUtils$Metrics;->networkOpCount:I

    int-to-long v3, v3

    add-long/2addr v3, p0

    long-to-int v3, v3

    iput v3, v0, Lcom/google/android/picasasync/MetricsUtils$Metrics;->networkOpCount:I

    :cond_0
    return-void
.end method

.method public static incrementNetworkOpDuration(J)V
    .locals 5
    .param p0    # J

    sget-object v3, Lcom/google/android/picasasync/MetricsUtils;->sMetricsStack:Ljava/lang/ThreadLocal;

    invoke-virtual {v3}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    add-int/lit8 v3, v1, -0x1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/picasasync/MetricsUtils$Metrics;

    iget-wide v3, v0, Lcom/google/android/picasasync/MetricsUtils$Metrics;->networkOpDuration:J

    add-long/2addr v3, p0

    iput-wide v3, v0, Lcom/google/android/picasasync/MetricsUtils$Metrics;->networkOpDuration:J

    :cond_0
    return-void
.end method

.method public static incrementOutBytes(J)V
    .locals 5
    .param p0    # J

    sget-object v3, Lcom/google/android/picasasync/MetricsUtils;->sMetricsStack:Ljava/lang/ThreadLocal;

    invoke-virtual {v3}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    add-int/lit8 v3, v1, -0x1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/picasasync/MetricsUtils$Metrics;

    iget-wide v3, v0, Lcom/google/android/picasasync/MetricsUtils$Metrics;->outBytes:J

    add-long/2addr v3, p0

    iput-wide v3, v0, Lcom/google/android/picasasync/MetricsUtils$Metrics;->outBytes:J

    :cond_0
    return-void
.end method

.method public static incrementQueryResultCount(I)V
    .locals 4
    .param p0    # I

    sget-object v3, Lcom/google/android/picasasync/MetricsUtils;->sMetricsStack:Ljava/lang/ThreadLocal;

    invoke-virtual {v3}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    add-int/lit8 v3, v1, -0x1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/picasasync/MetricsUtils$Metrics;

    iget v3, v0, Lcom/google/android/picasasync/MetricsUtils$Metrics;->queryResultCount:I

    add-int/2addr v3, p0

    iput v3, v0, Lcom/google/android/picasasync/MetricsUtils$Metrics;->queryResultCount:I

    :cond_0
    return-void
.end method
