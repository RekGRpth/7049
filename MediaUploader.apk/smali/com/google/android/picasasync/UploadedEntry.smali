.class public Lcom/google/android/picasasync/UploadedEntry;
.super Lcom/android/gallery3d/common/Entry;
.source "UploadedEntry.java"


# annotations
.annotation runtime Lcom/android/gallery3d/common/Entry$Table;
    value = "upload_records"
.end annotation


# static fields
.field public static final SCHEMA:Lcom/android/gallery3d/common/EntrySchema;


# instance fields
.field public final account:Ljava/lang/String;
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "account"
    .end annotation
.end field

.field public final albumId:Ljava/lang/String;
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "album_id"
    .end annotation
.end field

.field public final albumTitle:Ljava/lang/String;
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "album_title"
    .end annotation
.end field

.field public final bytesTotal:J
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "bytes_total"
    .end annotation
.end field

.field public final caption:Ljava/lang/String;
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "caption"
    .end annotation
.end field

.field public final contentUri:Ljava/lang/String;
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "content_uri"
    .end annotation
.end field

.field public final displayName:Ljava/lang/String;
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "display_name"
    .end annotation
.end field

.field public final error:Ljava/lang/String;
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "error"
    .end annotation
.end field

.field public final fingerprint:[B
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "fingerprint"
    .end annotation
.end field

.field final fingerprintHash:I
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        indexed = true
        value = "fingerprint_hash"
    .end annotation
.end field

.field public final idFromServer:J
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "id_from_server"
    .end annotation
.end field

.field public final state:I
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "state"
    .end annotation
.end field

.field public final timestamp:J
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "timestamp"
    .end annotation
.end field

.field public final uploadedTime:J
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "uploaded_time"
    .end annotation
.end field

.field public final url:Ljava/lang/String;
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "url"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/android/gallery3d/common/EntrySchema;

    const-class v1, Lcom/google/android/picasasync/UploadedEntry;

    invoke-direct {v0, v1}, Lcom/android/gallery3d/common/EntrySchema;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/google/android/picasasync/UploadedEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/android/gallery3d/common/Entry;-><init>()V

    iput-object v2, p0, Lcom/google/android/picasasync/UploadedEntry;->error:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/picasasync/UploadedEntry;->albumId:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/picasasync/UploadedEntry;->url:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/picasasync/UploadedEntry;->contentUri:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/picasasync/UploadedEntry;->account:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/picasasync/UploadedEntry;->fingerprint:[B

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/picasasync/UploadedEntry;->timestamp:J

    iput-wide v0, p0, Lcom/google/android/picasasync/UploadedEntry;->bytesTotal:J

    iput-wide v0, p0, Lcom/google/android/picasasync/UploadedEntry;->idFromServer:J

    iput-wide v0, p0, Lcom/google/android/picasasync/UploadedEntry;->uploadedTime:J

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/picasasync/UploadedEntry;->fingerprintHash:I

    iput v0, p0, Lcom/google/android/picasasync/UploadedEntry;->state:I

    iput-object v2, p0, Lcom/google/android/picasasync/UploadedEntry;->displayName:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/picasasync/UploadedEntry;->albumTitle:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/picasasync/UploadedEntry;->caption:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/picasasync/UploadTaskEntry;)V
    .locals 5
    .param p1    # Lcom/google/android/picasasync/UploadTaskEntry;

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/android/gallery3d/common/Entry;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->getAccount()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/picasasync/UploadedEntry;->account:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->getUploadedTime()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/google/android/picasasync/UploadedEntry;->uploadedTime:J

    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/picasasync/UploadedEntry;->contentUri:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->getAlbumId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/picasasync/UploadedEntry;->albumId:Ljava/lang/String;

    const-wide/16 v3, 0x0

    iput-wide v3, p0, Lcom/google/android/picasasync/UploadedEntry;->idFromServer:J

    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->getBytesTotal()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/google/android/picasasync/UploadedEntry;->bytesTotal:J

    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->getUploadedTime()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/google/android/picasasync/UploadedEntry;->timestamp:J

    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->getUrl()Landroid/net/Uri;

    move-result-object v1

    if-nez v1, :cond_0

    move-object v1, v2

    :goto_0
    iput-object v1, p0, Lcom/google/android/picasasync/UploadedEntry;->url:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->getFingerprint()Lcom/android/gallery3d/common/Fingerprint;

    move-result-object v0

    if-nez v0, :cond_1

    iput-object v2, p0, Lcom/google/android/picasasync/UploadedEntry;->fingerprint:[B

    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/picasasync/UploadedEntry;->fingerprintHash:I

    :goto_1
    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->getState()I

    move-result v1

    iput v1, p0, Lcom/google/android/picasasync/UploadedEntry;->state:I

    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->getError()Ljava/lang/Throwable;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/picasasync/UploadedEntry;->getFullErrorMessage(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/picasasync/UploadedEntry;->error:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->getCaption()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/picasasync/UploadedEntry;->caption:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->getAlbumTitle()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/picasasync/UploadedEntry;->albumTitle:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/picasasync/UploadedEntry;->displayName:Ljava/lang/String;

    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->getUrl()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/android/gallery3d/common/Fingerprint;->getBytes()[B

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/picasasync/UploadedEntry;->fingerprint:[B

    invoke-virtual {v0}, Lcom/android/gallery3d/common/Fingerprint;->hashCode()I

    move-result v1

    iput v1, p0, Lcom/google/android/picasasync/UploadedEntry;->fingerprintHash:I

    goto :goto_1
.end method

.method public constructor <init>(Lcom/google/android/picasasync/UploadTaskEntry;JJJLjava/lang/String;[B)V
    .locals 2
    .param p1    # Lcom/google/android/picasasync/UploadTaskEntry;
    .param p2    # J
    .param p4    # J
    .param p6    # J
    .param p8    # Ljava/lang/String;
    .param p9    # [B

    invoke-direct {p0}, Lcom/android/gallery3d/common/Entry;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->getAccount()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/picasasync/UploadedEntry;->account:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->getUploadedTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/picasasync/UploadedEntry;->uploadedTime:J

    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->getContentUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/picasasync/UploadedEntry;->contentUri:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->getAlbumId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/picasasync/UploadedEntry;->albumId:Ljava/lang/String;

    iput-wide p2, p0, Lcom/google/android/picasasync/UploadedEntry;->idFromServer:J

    iput-wide p4, p0, Lcom/google/android/picasasync/UploadedEntry;->bytesTotal:J

    iput-wide p6, p0, Lcom/google/android/picasasync/UploadedEntry;->timestamp:J

    iput-object p8, p0, Lcom/google/android/picasasync/UploadedEntry;->url:Ljava/lang/String;

    iput-object p9, p0, Lcom/google/android/picasasync/UploadedEntry;->fingerprint:[B

    new-instance v0, Lcom/android/gallery3d/common/Fingerprint;

    invoke-direct {v0, p9}, Lcom/android/gallery3d/common/Fingerprint;-><init>([B)V

    invoke-virtual {v0}, Lcom/android/gallery3d/common/Fingerprint;->hashCode()I

    move-result v0

    iput v0, p0, Lcom/google/android/picasasync/UploadedEntry;->fingerprintHash:I

    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/picasasync/UploadedEntry;->state:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/picasasync/UploadedEntry;->error:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->getCaption()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/picasasync/UploadedEntry;->caption:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->getAlbumTitle()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/picasasync/UploadedEntry;->albumTitle:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/picasasync/UploadedEntry;->displayName:Ljava/lang/String;

    return-void
.end method

.method public static fromCursor(Landroid/database/Cursor;)Lcom/google/android/picasasync/UploadedEntry;
    .locals 2
    .param p0    # Landroid/database/Cursor;

    sget-object v0, Lcom/google/android/picasasync/UploadedEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    new-instance v1, Lcom/google/android/picasasync/UploadedEntry;

    invoke-direct {v1}, Lcom/google/android/picasasync/UploadedEntry;-><init>()V

    invoke-virtual {v0, p0, v1}, Lcom/android/gallery3d/common/EntrySchema;->cursorToObject(Landroid/database/Cursor;Lcom/android/gallery3d/common/Entry;)Lcom/android/gallery3d/common/Entry;

    move-result-object v0

    check-cast v0, Lcom/google/android/picasasync/UploadedEntry;

    return-object v0
.end method

.method private static getFullErrorMessage(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 8
    .param p0    # Ljava/lang/Throwable;

    if-nez p0, :cond_0

    const/4 v6, 0x0

    :goto_0
    return-object v6

    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    :goto_1
    if-eqz p0, :cond_1

    move-object v1, p0

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p0

    goto :goto_1

    :cond_1
    invoke-virtual {v1}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v0

    array-length v4, v0

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v4, :cond_2

    aget-object v2, v0, v3

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_2
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 4

    sget-object v0, Lcom/google/android/picasasync/UploadedEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "id_from_server"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "content_uri"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "bytes_total"

    aput-object v3, v1, v2

    invoke-virtual {v0, p0, v1}, Lcom/android/gallery3d/common/EntrySchema;->toDebugString(Lcom/android/gallery3d/common/Entry;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
