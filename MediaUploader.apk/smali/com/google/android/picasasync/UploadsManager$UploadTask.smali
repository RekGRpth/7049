.class Lcom/google/android/picasasync/UploadsManager$UploadTask;
.super Lcom/google/android/picasasync/SyncTask;
.source "UploadsManager.java"

# interfaces
.implements Lcom/google/android/picasasync/Uploader$UploadProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/picasasync/UploadsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UploadTask"
.end annotation


# instance fields
.field protected mCurrentTask:Lcom/google/android/picasasync/UploadTaskEntry;

.field protected volatile mRunning:Z

.field protected mSyncContext:Lcom/google/android/picasasync/PicasaSyncHelper$SyncContext;

.field private final mTypePrefix:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/picasasync/UploadsManager;


# direct methods
.method protected constructor <init>(Lcom/google/android/picasasync/UploadsManager;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    invoke-direct {p0, p2}, Lcom/google/android/picasasync/SyncTask;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->mRunning:Z

    iput-object p3, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->mTypePrefix:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public cancelSync()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->mRunning:Z

    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/google/android/picasasync/UploadsManager$UploadTask;->stopCurrentTask(I)V

    iget-object v0, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->mSyncContext:Lcom/google/android/picasasync/PicasaSyncHelper$SyncContext;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->mSyncContext:Lcom/google/android/picasasync/PicasaSyncHelper$SyncContext;

    invoke-virtual {v0}, Lcom/google/android/picasasync/PicasaSyncHelper$SyncContext;->stopSync()V

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public cancelTask(J)V
    .locals 4
    .param p1    # J

    iget-object v1, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->mCurrentTask:Lcom/google/android/picasasync/UploadTaskEntry;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->mCurrentTask:Lcom/google/android/picasasync/UploadTaskEntry;

    iget-wide v2, v0, Lcom/google/android/picasasync/UploadTaskEntry;->id:J

    cmp-long v0, p1, v2

    if-nez v0, :cond_0

    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/google/android/picasasync/UploadsManager$UploadTask;->stopCurrentTask(I)V

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected getNextUpload()Lcom/google/android/picasasync/UploadTaskEntry;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    iget-object v1, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->mTypePrefix:Ljava/lang/String;

    # invokes: Lcom/google/android/picasasync/UploadsManager;->getNextManualUploadFromDb(Ljava/lang/String;)Lcom/google/android/picasasync/UploadTaskEntry;
    invoke-static {v0, v1}, Lcom/google/android/picasasync/UploadsManager;->access$1300(Lcom/google/android/picasasync/UploadsManager;Ljava/lang/String;)Lcom/google/android/picasasync/UploadTaskEntry;

    move-result-object v0

    return-object v0
.end method

.method public isBackgroundSync()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isSyncOnBattery()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isSyncOnExternalStorageOnly()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isSyncOnRoaming()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isSyncOnWifiOnly()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isUploadedBefore(Lcom/google/android/picasasync/UploadTaskEntry;)Z
    .locals 14
    .param p1    # Lcom/google/android/picasasync/UploadTaskEntry;

    const/4 v13, 0x1

    const/4 v12, 0x0

    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->getFingerprint()Lcom/android/gallery3d/common/Fingerprint;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/gallery3d/common/Fingerprint;->hashCode()I

    move-result v10

    iget-object v1, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    # getter for: Lcom/google/android/picasasync/UploadsManager;->mPicasaDbHelper:Lcom/google/android/picasasync/PicasaDatabaseHelper;
    invoke-static {v1}, Lcom/google/android/picasasync/UploadsManager;->access$1700(Lcom/google/android/picasasync/UploadsManager;)Lcom/google/android/picasasync/PicasaDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/picasasync/PicasaDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_0
    iget-object v1, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    # getter for: Lcom/google/android/picasasync/UploadsManager;->mPicasaDbHelper:Lcom/google/android/picasasync/PicasaDatabaseHelper;
    invoke-static {v1}, Lcom/google/android/picasasync/UploadsManager;->access$1700(Lcom/google/android/picasasync/UploadsManager;)Lcom/google/android/picasasync/PicasaDatabaseHelper;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->getAccount()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->getAlbumId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/picasasync/PicasaDatabaseHelper;->getAlbumEntry(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/picasasync/AlbumEntry;

    move-result-object v8

    if-eqz v8, :cond_0

    const-string v1, "Buzz"

    iget-object v2, v8, Lcom/google/android/picasasync/AlbumEntry;->albumType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    move v1, v12

    :goto_0
    return v1

    :cond_1
    :try_start_1
    # getter for: Lcom/google/android/picasasync/UploadsManager;->PHOTO_TABLE_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/picasasync/UploadsManager;->access$1800()Ljava/lang/String;

    move-result-object v1

    # getter for: Lcom/google/android/picasasync/UploadsManager;->PROJECTION_FINGERPRINT:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/picasasync/UploadsManager;->access$1900()[Ljava/lang/String;

    move-result-object v2

    const-string v3, "fingerprint_hash=? AND album_id=?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-wide v6, v8, Lcom/google/android/picasasync/AlbumEntry;->id:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v9

    if-nez v9, :cond_2

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    move v1, v12

    goto :goto_0

    :cond_2
    :try_start_2
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v11

    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->getFingerprint()Lcom/android/gallery3d/common/Fingerprint;

    move-result-object v1

    invoke-virtual {v1, v11}, Lcom/android/gallery3d/common/Fingerprint;->equals([B)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v1

    if-eqz v1, :cond_2

    :try_start_3
    invoke-interface {v9}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    move v1, v13

    goto :goto_0

    :cond_3
    :try_start_4
    invoke-interface {v9}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    move v1, v12

    goto :goto_0

    :catchall_0
    move-exception v1

    :try_start_5
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catchall_1
    move-exception v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1
.end method

.method public onProgress(Lcom/google/android/picasasync/UploadTaskEntry;)V
    .locals 4
    .param p1    # Lcom/google/android/picasasync/UploadTaskEntry;

    iget-object v1, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->mRunning:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    # invokes: Lcom/google/android/picasasync/UploadsManager;->updateTaskStateAndProgressInDb(Lcom/google/android/picasasync/UploadTaskEntry;)V
    invoke-static {v0, p1}, Lcom/google/android/picasasync/UploadsManager;->access$1000(Lcom/google/android/picasasync/UploadsManager;Lcom/google/android/picasasync/UploadTaskEntry;)V

    iget-object v0, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    const/4 v2, 0x0

    # invokes: Lcom/google/android/picasasync/UploadsManager;->notifyManualUploadDbChanges(Z)V
    invoke-static {v0, v2}, Lcom/google/android/picasasync/UploadsManager;->access$1400(Lcom/google/android/picasasync/UploadsManager;Z)V

    iget-object v0, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    const/4 v2, 0x0

    const/4 v3, 0x1

    # invokes: Lcom/google/android/picasasync/UploadsManager;->sendManualUploadReport(Lcom/google/android/picasasync/UploadTaskEntry;Lcom/google/android/picasasync/UploadedEntry;I)V
    invoke-static {v0, p1, v2, v3}, Lcom/google/android/picasasync/UploadsManager;->access$1500(Lcom/google/android/picasasync/UploadsManager;Lcom/google/android/picasasync/UploadTaskEntry;Lcom/google/android/picasasync/UploadedEntry;I)V

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected onQuotaReached(Lcom/google/android/picasasync/UploadTaskEntry;)V
    .locals 3
    .param p1    # Lcom/google/android/picasasync/UploadTaskEntry;

    iget-object v0, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/picasasync/UploadsManager;->notifyManualUploadDbChanges(Z)V
    invoke-static {v0, v1}, Lcom/google/android/picasasync/UploadsManager;->access$1400(Lcom/google/android/picasasync/UploadsManager;Z)V

    iget-object v0, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    const/4 v1, 0x0

    const/16 v2, 0x9

    # invokes: Lcom/google/android/picasasync/UploadsManager;->sendManualUploadReport(Lcom/google/android/picasasync/UploadTaskEntry;Lcom/google/android/picasasync/UploadedEntry;I)V
    invoke-static {v0, p1, v1, v2}, Lcom/google/android/picasasync/UploadsManager;->access$1500(Lcom/google/android/picasasync/UploadsManager;Lcom/google/android/picasasync/UploadTaskEntry;Lcom/google/android/picasasync/UploadedEntry;I)V

    return-void
.end method

.method public onRejected(I)V
    .locals 3
    .param p1    # I

    const-string v0, "PicasaUploader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "manual upload rejected! "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected onStalled(Lcom/google/android/picasasync/UploadTaskEntry;Z)V
    .locals 3
    .param p1    # Lcom/google/android/picasasync/UploadTaskEntry;
    .param p2    # Z

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/picasasync/UploadsManager;->notifyManualUploadDbChanges(Z)V
    invoke-static {v0, v1}, Lcom/google/android/picasasync/UploadsManager;->access$1400(Lcom/google/android/picasasync/UploadsManager;Z)V

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    # getter for: Lcom/google/android/picasasync/UploadsManager;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/picasasync/UploadsManager;->access$600(Lcom/google/android/picasasync/UploadsManager;)Landroid/content/Context;

    move-result-object v0

    # invokes: Lcom/google/android/picasasync/UploadsManager;->hasNetworkConnectivity(Landroid/content/Context;)Z
    invoke-static {v0}, Lcom/google/android/picasasync/UploadsManager;->access$1600(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    const/16 v1, 0xf

    # invokes: Lcom/google/android/picasasync/UploadsManager;->sendManualUploadReport(Lcom/google/android/picasasync/UploadTaskEntry;Lcom/google/android/picasasync/UploadedEntry;I)V
    invoke-static {v0, p1, v2, v1}, Lcom/google/android/picasasync/UploadsManager;->access$1500(Lcom/google/android/picasasync/UploadsManager;Lcom/google/android/picasasync/UploadTaskEntry;Lcom/google/android/picasasync/UploadedEntry;I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    const/16 v1, 0xe

    # invokes: Lcom/google/android/picasasync/UploadsManager;->sendManualUploadReport(Lcom/google/android/picasasync/UploadTaskEntry;Lcom/google/android/picasasync/UploadedEntry;I)V
    invoke-static {v0, p1, v2, v1}, Lcom/google/android/picasasync/UploadsManager;->access$1500(Lcom/google/android/picasasync/UploadsManager;Lcom/google/android/picasasync/UploadTaskEntry;Lcom/google/android/picasasync/UploadedEntry;I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    const/16 v1, 0xd

    # invokes: Lcom/google/android/picasasync/UploadsManager;->sendManualUploadReport(Lcom/google/android/picasasync/UploadTaskEntry;Lcom/google/android/picasasync/UploadedEntry;I)V
    invoke-static {v0, p1, v2, v1}, Lcom/google/android/picasasync/UploadsManager;->access$1500(Lcom/google/android/picasasync/UploadsManager;Lcom/google/android/picasasync/UploadTaskEntry;Lcom/google/android/picasasync/UploadedEntry;I)V

    goto :goto_0
.end method

.method protected onTaskDone(Lcom/google/android/picasasync/UploadTaskEntry;Lcom/google/android/picasasync/UploadedEntry;)V
    .locals 3
    .param p1    # Lcom/google/android/picasasync/UploadTaskEntry;
    .param p2    # Lcom/google/android/picasasync/UploadedEntry;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    if-eqz p2, :cond_0

    move v0, v1

    :goto_0
    # invokes: Lcom/google/android/picasasync/UploadsManager;->notifyManualUploadDbChanges(Z)V
    invoke-static {v2, v0}, Lcom/google/android/picasasync/UploadsManager;->access$1400(Lcom/google/android/picasasync/UploadsManager;Z)V

    iget-object v0, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    # invokes: Lcom/google/android/picasasync/UploadsManager;->sendManualUploadReport(Lcom/google/android/picasasync/UploadTaskEntry;Lcom/google/android/picasasync/UploadedEntry;I)V
    invoke-static {v0, p1, p2, v1}, Lcom/google/android/picasasync/UploadsManager;->access$1500(Lcom/google/android/picasasync/UploadsManager;Lcom/google/android/picasasync/UploadTaskEntry;Lcom/google/android/picasasync/UploadedEntry;I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onUnauthorized(Lcom/google/android/picasasync/UploadTaskEntry;)V
    .locals 3
    .param p1    # Lcom/google/android/picasasync/UploadTaskEntry;

    iget-object v0, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/picasasync/UploadsManager;->notifyManualUploadDbChanges(Z)V
    invoke-static {v0, v1}, Lcom/google/android/picasasync/UploadsManager;->access$1400(Lcom/google/android/picasasync/UploadsManager;Z)V

    iget-object v0, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    const/4 v1, 0x0

    const/16 v2, 0xa

    # invokes: Lcom/google/android/picasasync/UploadsManager;->sendManualUploadReport(Lcom/google/android/picasasync/UploadTaskEntry;Lcom/google/android/picasasync/UploadedEntry;I)V
    invoke-static {v0, p1, v1, v2}, Lcom/google/android/picasasync/UploadsManager;->access$1500(Lcom/google/android/picasasync/UploadsManager;Lcom/google/android/picasasync/UploadTaskEntry;Lcom/google/android/picasasync/UploadedEntry;I)V

    return-void
.end method

.method public final performSync(Landroid/content/SyncResult;)V
    .locals 5
    .param p1    # Landroid/content/SyncResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    monitor-enter v2

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->mRunning:Z

    if-nez v1, :cond_0

    monitor-exit v2

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    # getter for: Lcom/google/android/picasasync/UploadsManager;->mSyncHelper:Lcom/google/android/picasasync/PicasaSyncHelper;
    invoke-static {v1}, Lcom/google/android/picasasync/UploadsManager;->access$400(Lcom/google/android/picasasync/UploadsManager;)Lcom/google/android/picasasync/PicasaSyncHelper;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v1, p1, v3}, Lcom/google/android/picasasync/PicasaSyncHelper;->createSyncContext(Landroid/content/SyncResult;Ljava/lang/Thread;)Lcom/google/android/picasasync/PicasaSyncHelper$SyncContext;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->mSyncContext:Lcom/google/android/picasasync/PicasaSyncHelper$SyncContext;

    iget-object v1, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->mSyncContext:Lcom/google/android/picasasync/PicasaSyncHelper$SyncContext;

    iget-object v3, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->syncAccount:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/google/android/picasasync/PicasaSyncHelper$SyncContext;->setAccount(Ljava/lang/String;)Z

    iget-object v1, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    # invokes: Lcom/google/android/picasasync/UploadsManager;->setCurrentUploadTask(Lcom/google/android/picasasync/UploadsManager$UploadTask;)V
    invoke-static {v1, p0}, Lcom/google/android/picasasync/UploadsManager;->access$500(Lcom/google/android/picasasync/UploadsManager;Lcom/google/android/picasasync/UploadsManager$UploadTask;)V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/picasasync/MetricsUtils;->begin(Ljava/lang/String;)I

    move-result v0

    :try_start_1
    invoke-virtual {p0, p1}, Lcom/google/android/picasasync/UploadsManager$UploadTask;->performSyncInternal(Landroid/content/SyncResult;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    iget-object v1, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    # invokes: Lcom/google/android/picasasync/UploadsManager;->setCurrentUploadTask(Lcom/google/android/picasasync/UploadsManager$UploadTask;)V
    invoke-static {v1, v4}, Lcom/google/android/picasasync/UploadsManager;->access$500(Lcom/google/android/picasasync/UploadsManager;Lcom/google/android/picasasync/UploadsManager$UploadTask;)V

    iput-object v4, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->mSyncContext:Lcom/google/android/picasasync/PicasaSyncHelper$SyncContext;

    invoke-static {v0}, Lcom/google/android/picasasync/MetricsUtils;->end(I)V

    goto :goto_0

    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :catchall_1
    move-exception v1

    iget-object v2, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    # invokes: Lcom/google/android/picasasync/UploadsManager;->setCurrentUploadTask(Lcom/google/android/picasasync/UploadsManager$UploadTask;)V
    invoke-static {v2, v4}, Lcom/google/android/picasasync/UploadsManager;->access$500(Lcom/google/android/picasasync/UploadsManager;Lcom/google/android/picasasync/UploadsManager$UploadTask;)V

    iput-object v4, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->mSyncContext:Lcom/google/android/picasasync/PicasaSyncHelper$SyncContext;

    invoke-static {v0}, Lcom/google/android/picasasync/MetricsUtils;->end(I)V

    throw v1
.end method

.method protected performSyncInternal(Landroid/content/SyncResult;)V
    .locals 12
    .param p1    # Landroid/content/SyncResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v4, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    :goto_0
    iget-boolean v7, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->mRunning:Z

    if-eqz v7, :cond_0

    invoke-virtual {p0}, Lcom/google/android/picasasync/UploadsManager$UploadTask;->getNextUpload()Lcom/google/android/picasasync/UploadTaskEntry;

    move-result-object v6

    if-nez v6, :cond_1

    :cond_0
    :goto_1
    return-void

    :cond_1
    invoke-virtual {v6}, Lcom/google/android/picasasync/UploadTaskEntry;->getAccount()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->syncAccount:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    const-string v7, "PicasaUploader"

    const-string v8, "*** change account from %s to %s"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->syncAccount:Ljava/lang/String;

    invoke-static {v11}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-virtual {v6}, Lcom/google/android/picasasync/UploadTaskEntry;->getAccount()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_2
    invoke-virtual {v6}, Lcom/google/android/picasasync/UploadTaskEntry;->isStartedYet()Z

    move-result v7

    if-nez v7, :cond_3

    invoke-virtual {v6}, Lcom/google/android/picasasync/UploadTaskEntry;->setUploadedTime()V

    :try_start_0
    iget-object v7, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    # getter for: Lcom/google/android/picasasync/UploadsManager;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/google/android/picasasync/UploadsManager;->access$600(Lcom/google/android/picasasync/UploadsManager;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v7, v6}, Lcom/google/android/picasasync/PicasaUploadHelper;->fillRequest(Landroid/content/Context;Lcom/google/android/picasasync/UploadTaskEntry;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    iget-object v7, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->syncAccount:Ljava/lang/String;

    invoke-virtual {v6}, Lcom/google/android/picasasync/UploadTaskEntry;->getAlbumId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, p1, v7, v8}, Lcom/google/android/picasasync/UploadsManager$UploadTask;->syncAlbum(Landroid/content/SyncResult;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_4

    const-string v7, "PicasaUploader"

    const-string v8, "sync has been interrupted"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    iget-object v8, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    monitor-enter v8

    const/4 v7, 0x0

    :try_start_1
    iput-object v7, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->mCurrentTask:Lcom/google/android/picasasync/UploadTaskEntry;

    monitor-exit v8

    goto :goto_0

    :catchall_0
    move-exception v7

    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v7

    :catch_0
    move-exception v5

    const-string v7, "PicasaUploader"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "failed to process the request: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/16 v7, 0xb

    invoke-virtual {v6, v7, v5}, Lcom/google/android/picasasync/UploadTaskEntry;->setState(ILjava/lang/Throwable;)V

    iget-wide v7, v4, Landroid/content/SyncStats;->numSkippedEntries:J

    const-wide/16 v9, 0x1

    add-long/2addr v7, v9

    iput-wide v7, v4, Landroid/content/SyncStats;->numSkippedEntries:J

    iget-object v7, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    iget-wide v8, v6, Lcom/google/android/picasasync/UploadTaskEntry;->id:J

    # invokes: Lcom/google/android/picasasync/UploadsManager;->removeTaskFromDb(J)Z
    invoke-static {v7, v8, v9}, Lcom/google/android/picasasync/UploadsManager;->access$700(Lcom/google/android/picasasync/UploadsManager;J)Z

    iget-object v7, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    new-instance v8, Lcom/google/android/picasasync/UploadedEntry;

    invoke-direct {v8, v6}, Lcom/google/android/picasasync/UploadedEntry;-><init>(Lcom/google/android/picasasync/UploadTaskEntry;)V

    # invokes: Lcom/google/android/picasasync/UploadsManager;->recordResult(Lcom/google/android/picasasync/UploadedEntry;)Lcom/google/android/picasasync/UploadedEntry;
    invoke-static {v7, v8}, Lcom/google/android/picasasync/UploadsManager;->access$800(Lcom/google/android/picasasync/UploadsManager;Lcom/google/android/picasasync/UploadedEntry;)Lcom/google/android/picasasync/UploadedEntry;

    move-result-object v3

    invoke-virtual {p0, v6, v3}, Lcom/google/android/picasasync/UploadsManager$UploadTask;->onTaskDone(Lcom/google/android/picasasync/UploadTaskEntry;Lcom/google/android/picasasync/UploadedEntry;)V

    goto/16 :goto_0

    :cond_4
    const/4 v3, 0x0

    invoke-virtual {p0, v6}, Lcom/google/android/picasasync/UploadsManager$UploadTask;->isUploadedBefore(Lcom/google/android/picasasync/UploadTaskEntry;)Z

    move-result v7

    if-eqz v7, :cond_7

    const-string v7, "PicasaUploader"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "duplicate upload: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v7, 0xc

    invoke-virtual {v6, v7}, Lcom/google/android/picasasync/UploadTaskEntry;->setState(I)V

    :cond_5
    :goto_3
    iget-object v7, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    iget-wide v8, v6, Lcom/google/android/picasasync/UploadTaskEntry;->id:J

    # invokes: Lcom/google/android/picasasync/UploadsManager;->removeTaskFromDb(J)Z
    invoke-static {v7, v8, v9}, Lcom/google/android/picasasync/UploadsManager;->access$700(Lcom/google/android/picasasync/UploadsManager;J)Z

    if-nez v3, :cond_6

    new-instance v3, Lcom/google/android/picasasync/UploadedEntry;

    invoke-direct {v3, v6}, Lcom/google/android/picasasync/UploadedEntry;-><init>(Lcom/google/android/picasasync/UploadTaskEntry;)V

    :cond_6
    iget-object v7, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    # invokes: Lcom/google/android/picasasync/UploadsManager;->recordResult(Lcom/google/android/picasasync/UploadedEntry;)Lcom/google/android/picasasync/UploadedEntry;
    invoke-static {v7, v3}, Lcom/google/android/picasasync/UploadsManager;->access$800(Lcom/google/android/picasasync/UploadsManager;Lcom/google/android/picasasync/UploadedEntry;)Lcom/google/android/picasasync/UploadedEntry;

    invoke-virtual {p0, v6, v3}, Lcom/google/android/picasasync/UploadsManager$UploadTask;->onTaskDone(Lcom/google/android/picasasync/UploadTaskEntry;Lcom/google/android/picasasync/UploadedEntry;)V

    goto/16 :goto_2

    :cond_7
    iget-object v8, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    monitor-enter v8

    :try_start_2
    iget-boolean v7, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->mRunning:Z

    if-nez v7, :cond_8

    monitor-exit v8

    goto/16 :goto_1

    :catchall_1
    move-exception v7

    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v7

    :cond_8
    :try_start_3
    iput-object v6, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->mCurrentTask:Lcom/google/android/picasasync/UploadTaskEntry;

    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    const-string v7, "PicasaUploader"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "upload "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-wide v1, v4, Landroid/content/SyncStats;->numIoExceptions:J

    iget-object v7, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    # invokes: Lcom/google/android/picasasync/UploadsManager;->doUpload(Lcom/google/android/picasasync/UploadTaskEntry;Lcom/google/android/picasasync/Uploader$UploadProgressListener;Landroid/content/SyncResult;)Lcom/google/android/picasasync/UploadedEntry;
    invoke-static {v7, v6, p0, p1}, Lcom/google/android/picasasync/UploadsManager;->access$900(Lcom/google/android/picasasync/UploadsManager;Lcom/google/android/picasasync/UploadTaskEntry;Lcom/google/android/picasasync/Uploader$UploadProgressListener;Landroid/content/SyncResult;)Lcom/google/android/picasasync/UploadedEntry;

    move-result-object v3

    if-nez v3, :cond_a

    invoke-virtual {v6}, Lcom/google/android/picasasync/UploadTaskEntry;->getState()I

    move-result v7

    packed-switch v7, :pswitch_data_0

    :pswitch_0
    invoke-virtual {v6}, Lcom/google/android/picasasync/UploadTaskEntry;->getState()I

    move-result v7

    const/4 v8, 0x5

    if-eq v7, v8, :cond_5

    const-string v7, "PicasaUploader"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "wrong state after upload: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v7, 0x5

    new-instance v8, Ljava/lang/RuntimeException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "wrong state after upload: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v6}, Lcom/google/android/picasasync/UploadTaskEntry;->getState()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v7, v8}, Lcom/google/android/picasasync/UploadTaskEntry;->setState(ILjava/lang/Throwable;)V

    goto/16 :goto_3

    :pswitch_1
    const/4 v7, 0x3

    invoke-virtual {v6, v7}, Lcom/google/android/picasasync/UploadTaskEntry;->setState(I)V

    iget-object v7, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    # invokes: Lcom/google/android/picasasync/UploadsManager;->updateTaskStateAndProgressInDb(Lcom/google/android/picasasync/UploadTaskEntry;)V
    invoke-static {v7, v6}, Lcom/google/android/picasasync/UploadsManager;->access$1000(Lcom/google/android/picasasync/UploadsManager;Lcom/google/android/picasasync/UploadTaskEntry;)V

    iget-wide v7, v4, Landroid/content/SyncStats;->numIoExceptions:J

    cmp-long v7, v7, v1

    if-lez v7, :cond_9

    const/4 v0, 0x1

    :goto_4
    invoke-virtual {p0, v6, v0}, Lcom/google/android/picasasync/UploadsManager$UploadTask;->onStalled(Lcom/google/android/picasasync/UploadTaskEntry;Z)V

    goto/16 :goto_1

    :cond_9
    const/4 v0, 0x0

    goto :goto_4

    :pswitch_2
    iget-object v7, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    # getter for: Lcom/google/android/picasasync/UploadsManager;->mProblematicAccounts:Ljava/util/HashSet;
    invoke-static {v7}, Lcom/google/android/picasasync/UploadsManager;->access$1100(Lcom/google/android/picasasync/UploadsManager;)Ljava/util/HashSet;

    move-result-object v7

    invoke-virtual {v6}, Lcom/google/android/picasasync/UploadTaskEntry;->getAccount()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, v6}, Lcom/google/android/picasasync/UploadsManager$UploadTask;->onUnauthorized(Lcom/google/android/picasasync/UploadTaskEntry;)V

    goto/16 :goto_1

    :pswitch_3
    iget-object v7, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    # getter for: Lcom/google/android/picasasync/UploadsManager;->mProblematicAccounts:Ljava/util/HashSet;
    invoke-static {v7}, Lcom/google/android/picasasync/UploadsManager;->access$1100(Lcom/google/android/picasasync/UploadsManager;)Ljava/util/HashSet;

    move-result-object v7

    invoke-virtual {v6}, Lcom/google/android/picasasync/UploadTaskEntry;->getAccount()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, v6}, Lcom/google/android/picasasync/UploadsManager$UploadTask;->onQuotaReached(Lcom/google/android/picasasync/UploadTaskEntry;)V

    goto/16 :goto_1

    :pswitch_4
    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Lcom/google/android/picasasync/UploadTaskEntry;->setState(I)V

    goto/16 :goto_3

    :cond_a
    const/4 v7, 0x4

    invoke-virtual {v6, v7}, Lcom/google/android/picasasync/UploadTaskEntry;->setState(I)V

    iget-object v7, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v8, v7, Landroid/content/SyncStats;->numEntries:J

    const-wide/16 v10, 0x1

    add-long/2addr v8, v10

    iput-wide v8, v7, Landroid/content/SyncStats;->numEntries:J

    iget-object v7, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v8, v7, Landroid/content/SyncStats;->numInserts:J

    const-wide/16 v10, 0x1

    add-long/2addr v8, v10

    iput-wide v8, v7, Landroid/content/SyncStats;->numInserts:J

    iget-object v7, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    # invokes: Lcom/google/android/picasasync/UploadsManager;->writeToPhotoTable(Lcom/google/android/picasasync/UploadTaskEntry;Lcom/google/android/picasasync/UploadedEntry;Landroid/content/SyncResult;)Z
    invoke-static {v7, v6, v3, p1}, Lcom/google/android/picasasync/UploadsManager;->access$1200(Lcom/google/android/picasasync/UploadsManager;Lcom/google/android/picasasync/UploadTaskEntry;Lcom/google/android/picasasync/UploadedEntry;Landroid/content/SyncResult;)Z

    move-result v7

    if-nez v7, :cond_5

    const-string v7, "PicasaUploader"

    const-string v8, "sync album now: %s, %s"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-virtual {v6}, Lcom/google/android/picasasync/UploadTaskEntry;->getAlbumId()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-virtual {v6}, Lcom/google/android/picasasync/UploadTaskEntry;->getAccount()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v7, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->syncAccount:Ljava/lang/String;

    invoke-virtual {v6}, Lcom/google/android/picasasync/UploadTaskEntry;->getAlbumId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, p1, v7, v8}, Lcom/google/android/picasasync/UploadsManager$UploadTask;->syncAlbum(Landroid/content/SyncResult;Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected stopCurrentTask(I)V
    .locals 4
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->mCurrentTask:Lcom/google/android/picasasync/UploadTaskEntry;

    const-string v1, "PicasaUploader"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "stopCurrentTask: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    monitor-enter v0

    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/picasasync/UploadTaskEntry;->isCancellable()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0, p1}, Lcom/google/android/picasasync/UploadTaskEntry;->setState(I)V

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    :cond_1
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected syncAlbum(Landroid/content/SyncResult;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 15
    .param p1    # Landroid/content/SyncResult;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p2

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ","

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, p3

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->mSyncContext:Lcom/google/android/picasasync/PicasaSyncHelper$SyncContext;

    iget-object v10, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    # getter for: Lcom/google/android/picasasync/UploadsManager;->mSyncHelper:Lcom/google/android/picasasync/PicasaSyncHelper;
    invoke-static {v10}, Lcom/google/android/picasasync/UploadsManager;->access$400(Lcom/google/android/picasasync/UploadsManager;)Lcom/google/android/picasasync/PicasaSyncHelper;

    move-result-object v8

    iget-object v11, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    monitor-enter v11

    :try_start_0
    iget-object v10, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    # getter for: Lcom/google/android/picasasync/UploadsManager;->mSyncedAccountAlbumPairs:Ljava/util/HashSet;
    invoke-static {v10}, Lcom/google/android/picasasync/UploadsManager;->access$2000(Lcom/google/android/picasasync/UploadsManager;)Ljava/util/HashSet;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    const/4 v7, 0x1

    monitor-exit v11

    :goto_0
    return v7

    :cond_0
    iget-boolean v10, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->mRunning:Z

    if-nez v10, :cond_1

    const/4 v7, 0x0

    monitor-exit v11

    goto :goto_0

    :catchall_0
    move-exception v10

    monitor-exit v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v10

    :cond_1
    :try_start_1
    monitor-exit v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-string v10, "PicasaUploader"

    const-string v11, "sync album for dedup: %s/%s"

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-static/range {p2 .. p2}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x1

    invoke-static/range {p3 .. p3}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v10, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    # getter for: Lcom/google/android/picasasync/UploadsManager;->mPicasaDbHelper:Lcom/google/android/picasasync/PicasaDatabaseHelper;
    invoke-static {v10}, Lcom/google/android/picasasync/UploadsManager;->access$1700(Lcom/google/android/picasasync/UploadsManager;)Lcom/google/android/picasasync/PicasaDatabaseHelper;

    move-result-object v10

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v10, v0, v1}, Lcom/google/android/picasasync/PicasaDatabaseHelper;->getAlbumEntry(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/picasasync/AlbumEntry;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v10, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v3, v10, Landroid/content/SyncStats;->numAuthExceptions:J

    if-nez v2, :cond_2

    const-string v10, "PicasaUploader"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "sync albumlist to get ID for "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static/range {p3 .. p3}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p2

    invoke-virtual {v8, v0}, Lcom/google/android/picasasync/PicasaSyncHelper;->findUser(Ljava/lang/String;)Lcom/google/android/picasasync/UserEntry;

    move-result-object v9

    if-eqz v9, :cond_4

    invoke-virtual {v8, v6, v9}, Lcom/google/android/picasasync/PicasaSyncHelper;->syncAlbumsForUser(Lcom/google/android/picasasync/PicasaSyncHelper$SyncContext;Lcom/google/android/picasasync/UserEntry;)V

    iget-object v10, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    # getter for: Lcom/google/android/picasasync/UploadsManager;->mPicasaDbHelper:Lcom/google/android/picasasync/PicasaDatabaseHelper;
    invoke-static {v10}, Lcom/google/android/picasasync/UploadsManager;->access$1700(Lcom/google/android/picasasync/UploadsManager;)Lcom/google/android/picasasync/PicasaDatabaseHelper;

    move-result-object v10

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v10, v0, v1}, Lcom/google/android/picasasync/PicasaDatabaseHelper;->getAlbumEntry(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/picasasync/AlbumEntry;

    move-result-object v2

    :cond_2
    if-eqz v2, :cond_6

    const-string v10, "Buzz"

    iget-object v11, v2, Lcom/google/android/picasasync/AlbumEntry;->albumType:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_5

    invoke-virtual {v8, v6, v2}, Lcom/google/android/picasasync/PicasaSyncHelper;->syncPhotosForAlbum(Lcom/google/android/picasasync/PicasaSyncHelper$SyncContext;Lcom/google/android/picasasync/AlbumEntry;)V

    :goto_1
    iget-object v11, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    monitor-enter v11

    :try_start_2
    iget-boolean v7, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->mRunning:Z

    if-eqz v7, :cond_3

    if-eqz v2, :cond_3

    iget-object v10, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    # getter for: Lcom/google/android/picasasync/UploadsManager;->mSyncedAccountAlbumPairs:Ljava/util/HashSet;
    invoke-static {v10}, Lcom/google/android/picasasync/UploadsManager;->access$2000(Lcom/google/android/picasasync/UploadsManager;)Ljava/util/HashSet;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_3
    monitor-exit v11

    goto/16 :goto_0

    :catchall_1
    move-exception v10

    monitor-exit v11
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v10

    :cond_4
    const-string v10, "PicasaUploader"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "no userEntry for "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static/range {p2 .. p2}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/google/android/picasasync/UploadsManager$UploadTask;->cancelSync()V

    iget-object v10, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    # getter for: Lcom/google/android/picasasync/UploadsManager;->mProblematicAccounts:Ljava/util/HashSet;
    invoke-static {v10}, Lcom/google/android/picasasync/UploadsManager;->access$1100(Lcom/google/android/picasasync/UploadsManager;)Ljava/util/HashSet;

    move-result-object v10

    move-object/from16 v0, p2

    invoke-virtual {v10, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const/4 v7, 0x0

    goto/16 :goto_0

    :cond_5
    const-string v10, "PicasaUploader"

    const-string v11, "post album; don\'t sync"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_6
    move-object/from16 v0, p1

    iget-object v10, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v10, v10, Landroid/content/SyncStats;->numAuthExceptions:J

    cmp-long v10, v3, v10

    if-gez v10, :cond_7

    const-string v10, "PicasaUploader"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "need authorization for picasa access: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static/range {p2 .. p2}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/google/android/picasasync/UploadsManager$UploadTask;->cancelSync()V

    iget-object v10, p0, Lcom/google/android/picasasync/UploadsManager$UploadTask;->this$0:Lcom/google/android/picasasync/UploadsManager;

    # getter for: Lcom/google/android/picasasync/UploadsManager;->mProblematicAccounts:Ljava/util/HashSet;
    invoke-static {v10}, Lcom/google/android/picasasync/UploadsManager;->access$1100(Lcom/google/android/picasasync/UploadsManager;)Ljava/util/HashSet;

    move-result-object v10

    move-object/from16 v0, p2

    invoke-virtual {v10, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const/4 v7, 0x0

    goto/16 :goto_0

    :cond_7
    const-string v10, "PicasaUploader"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "album doesn\'t exist: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static/range {p3 .. p3}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/google/android/picasasync/UploadsManager$UploadTask;->cancelSync()V

    const/4 v7, 0x0

    goto/16 :goto_0
.end method
