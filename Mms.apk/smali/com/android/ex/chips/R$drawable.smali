.class public final Lcom/android/ex/chips/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/ex/chips/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final add_contact_nor:I = 0x7f020000

.field public static final add_contact_nor_exist:I = 0x7f020001

.field public static final add_contact_pressed:I = 0x7f020002

.field public static final add_contact_pressed_exist:I = 0x7f020003

.field public static final add_contact_sel:I = 0x7f020004

.field public static final add_contact_sel_exist:I = 0x7f020005

.field public static final add_contact_selector:I = 0x7f020006

.field public static final add_contact_selector_exist:I = 0x7f020007

.field public static final alert_wappush_si_expired:I = 0x7f020008

.field public static final attachment_editor_bg:I = 0x7f020009

.field public static final attachment_selector:I = 0x7f02000a

.field public static final bg_separator:I = 0x7f02000b

.field public static final bg_separator_inset:I = 0x7f02000c

.field public static final btn_cling_normal:I = 0x7f02000d

.field public static final btn_cling_pressed:I = 0x7f02000e

.field public static final btn_zoom_down_disabled:I = 0x7f02000f

.field public static final btn_zoom_down_disabled_focused:I = 0x7f020010

.field public static final btn_zoom_down_normal:I = 0x7f020011

.field public static final btn_zoom_down_pressed:I = 0x7f020012

.field public static final btn_zoom_down_selected:I = 0x7f020013

.field public static final btn_zoom_up_disabled:I = 0x7f020014

.field public static final btn_zoom_up_disabled_focused:I = 0x7f020015

.field public static final btn_zoom_up_normal:I = 0x7f020016

.field public static final btn_zoom_up_pressed:I = 0x7f020017

.field public static final btn_zoom_up_selected:I = 0x7f020018

.field public static final cab_background_bottom_holo_dark:I = 0x7f020019

.field public static final cab_background_top_holo_dark:I = 0x7f02001a

.field public static final chat_history_selector:I = 0x7f02001b

.field public static final chip_background:I = 0x7f02001c

.field public static final chip_background_invalid:I = 0x7f02001d

.field public static final chip_background_selected:I = 0x7f02001e

.field public static final chip_checkmark:I = 0x7f02001f

.field public static final chip_delete:I = 0x7f020020

.field public static final class_zero_background:I = 0x7f0200d2

.field public static final cling_button_bg:I = 0x7f020021

.field public static final contact_button_selector:I = 0x7f020022

.field public static final conversation_item_background_read:I = 0x7f020023

.field public static final conversation_item_background_unread:I = 0x7f020024

.field public static final divider_horizontal:I = 0x7f020025

.field public static final edit_text_holo_light:I = 0x7f020026

.field public static final emo_im_angel:I = 0x7f020027

.field public static final emo_im_cool:I = 0x7f020028

.field public static final emo_im_crying:I = 0x7f020029

.field public static final emo_im_embarrassed:I = 0x7f02002a

.field public static final emo_im_foot_in_mouth:I = 0x7f02002b

.field public static final emo_im_happy:I = 0x7f02002c

.field public static final emo_im_heart:I = 0x7f02002d

.field public static final emo_im_kissing:I = 0x7f02002e

.field public static final emo_im_laughing:I = 0x7f02002f

.field public static final emo_im_lips_are_sealed:I = 0x7f020030

.field public static final emo_im_mad:I = 0x7f020031

.field public static final emo_im_money_mouth:I = 0x7f020032

.field public static final emo_im_pokerface:I = 0x7f020033

.field public static final emo_im_sad:I = 0x7f020034

.field public static final emo_im_smirk:I = 0x7f020035

.field public static final emo_im_surprised:I = 0x7f020036

.field public static final emo_im_tongue_sticking_out:I = 0x7f020037

.field public static final emo_im_undecided:I = 0x7f020038

.field public static final emo_im_winking:I = 0x7f020039

.field public static final emo_im_wtf:I = 0x7f02003a

.field public static final emo_im_yelling:I = 0x7f02003b

.field public static final fasttrack_badge_middle:I = 0x7f02003c

.field public static final fasttrack_badge_middle_normal:I = 0x7f02003d

.field public static final fasttrack_badge_middle_pressed:I = 0x7f02003e

.field public static final file_attachment_background:I = 0x7f02003f

.field public static final file_attachment_divider:I = 0x7f020040

.field public static final foldermodeview_background_highlight:I = 0x7f020041

.field public static final gradient_bg_mms_widget_holo:I = 0x7f020042

.field public static final gradient_bg_widget_holo:I = 0x7f020043

.field public static final hairline:I = 0x7f020044

.field public static final hairline_left:I = 0x7f020045

.field public static final hairline_right:I = 0x7f020046

.field public static final header_bg_mms_widget_holo:I = 0x7f020047

.field public static final ic_attach_audio_holo_light:I = 0x7f020048

.field public static final ic_attach_capture_audio_holo_light:I = 0x7f020049

.field public static final ic_attach_capture_picture_holo_light:I = 0x7f02004a

.field public static final ic_attach_capture_video_holo_light:I = 0x7f02004b

.field public static final ic_attach_picture_holo_light:I = 0x7f02004c

.field public static final ic_attach_slideshow_holo_light:I = 0x7f02004d

.field public static final ic_attach_video_holo_light:I = 0x7f02004e

.field public static final ic_attachment_universal_small:I = 0x7f02004f

.field public static final ic_cab_done_holo_dark:I = 0x7f020050

.field public static final ic_calendar_attach_menu:I = 0x7f020051

.field public static final ic_cellbroadcast:I = 0x7f020052

.field public static final ic_close_btn:I = 0x7f020053

.field public static final ic_contact_picture:I = 0x7f020054

.field public static final ic_delete_file_attachment:I = 0x7f020055

.field public static final ic_dialog_attach:I = 0x7f020056

.field public static final ic_dialog_info:I = 0x7f020057

.field public static final ic_dialog_info_holo_light:I = 0x7f020058

.field public static final ic_dialog_menu_generic:I = 0x7f020059

.field public static final ic_gallery_video_overlay:I = 0x7f02005a

.field public static final ic_launcher_contacts:I = 0x7f02005b

.field public static final ic_launcher_phone:I = 0x7f02005c

.field public static final ic_launcher_record_audio:I = 0x7f02005d

.field public static final ic_launcher_smsmms:I = 0x7f02005e

.field public static final ic_left_arrow:I = 0x7f02005f

.field public static final ic_list_alert_sms_failed:I = 0x7f020060

.field public static final ic_lock_message_sms:I = 0x7f020061

.field public static final ic_maps_back:I = 0x7f020062

.field public static final ic_maps_next:I = 0x7f020063

.field public static final ic_menu_add_slide:I = 0x7f020064

.field public static final ic_menu_add_sound:I = 0x7f020065

.field public static final ic_menu_attachment:I = 0x7f020066

.field public static final ic_menu_call:I = 0x7f020067

.field public static final ic_menu_clear_select:I = 0x7f020068

.field public static final ic_menu_contact:I = 0x7f020069

.field public static final ic_menu_delete_played:I = 0x7f02006a

.field public static final ic_menu_duration:I = 0x7f02006b

.field public static final ic_menu_edit:I = 0x7f02006c

.field public static final ic_menu_emoticons:I = 0x7f02006d

.field public static final ic_menu_move_down:I = 0x7f02006e

.field public static final ic_menu_move_up:I = 0x7f02006f

.field public static final ic_menu_movie:I = 0x7f020070

.field public static final ic_menu_msg_compose_holo_dark:I = 0x7f020071

.field public static final ic_menu_omacp:I = 0x7f020072

.field public static final ic_menu_picture:I = 0x7f020073

.field public static final ic_menu_quick_text:I = 0x7f020074

.field public static final ic_menu_remove_picture:I = 0x7f020075

.field public static final ic_menu_remove_sound:I = 0x7f020076

.field public static final ic_menu_remove_text:I = 0x7f020077

.field public static final ic_menu_remove_video:I = 0x7f020078

.field public static final ic_menu_save:I = 0x7f020079

.field public static final ic_menu_search_holo_dark:I = 0x7f02007a

.field public static final ic_menu_select_all:I = 0x7f02007b

.field public static final ic_menu_sim_capacity:I = 0x7f02007c

.field public static final ic_menu_sim_sms:I = 0x7f02007d

.field public static final ic_menu_text_vcard:I = 0x7f02007e

.field public static final ic_menu_trash_holo_dark:I = 0x7f02007f

.field public static final ic_missing_thumbnail_picture:I = 0x7f020080

.field public static final ic_missing_thumbnail_video:I = 0x7f020081

.field public static final ic_mms:I = 0x7f020082

.field public static final ic_mms_drm_protected:I = 0x7f020083

.field public static final ic_mms_duration:I = 0x7f020084

.field public static final ic_mms_layout:I = 0x7f020085

.field public static final ic_mms_music:I = 0x7f020086

.field public static final ic_mms_text_bottom:I = 0x7f020087

.field public static final ic_mms_text_top:I = 0x7f020088

.field public static final ic_multi_save_thumb_audio:I = 0x7f020089

.field public static final ic_multi_save_thumb_image:I = 0x7f02008a

.field public static final ic_multi_save_thumb_video:I = 0x7f02008b

.field public static final ic_right_arrow:I = 0x7f02008c

.field public static final ic_select_siminfo:I = 0x7f02008d

.field public static final ic_send_disabled_holo_light:I = 0x7f02008e

.field public static final ic_send_holo_light:I = 0x7f02008f

.field public static final ic_sim_cardone:I = 0x7f020090

.field public static final ic_sim_cardtwo:I = 0x7f020091

.field public static final ic_sms:I = 0x7f020092

.field public static final ic_sms_mms_delivered:I = 0x7f020093

.field public static final ic_sms_mms_details:I = 0x7f020094

.field public static final ic_sms_mms_not_delivered:I = 0x7f020095

.field public static final ic_sms_mms_pending:I = 0x7f020096

.field public static final ic_unread_label:I = 0x7f020097

.field public static final ic_vcalendar_attach:I = 0x7f020098

.field public static final ic_vcalendar_attach_menu:I = 0x7f020099

.field public static final ic_vcard_attach:I = 0x7f02009a

.field public static final ic_vcard_attach_menu:I = 0x7f02009b

.field public static final ic_video_call:I = 0x7f02009c

.field public static final ic_wappush:I = 0x7f02009d

.field public static final incoming:I = 0x7f02009e

.field public static final light_blue_background:I = 0x7f0200d7

.field public static final list_activated_holo:I = 0x7f02009f

.field public static final list_div_top_btm_mms_widget_holo:I = 0x7f0200a0

.field public static final list_focused_holo:I = 0x7f0200a1

.field public static final list_item_font_primary:I = 0x7f0200a2

.field public static final list_item_font_secondary:I = 0x7f0200a3

.field public static final list_pressed_holo:I = 0x7f0200a4

.field public static final list_pressed_holo_light:I = 0x7f0200a5

.field public static final list_read_holo:I = 0x7f0200a6

.field public static final list_selected_holo:I = 0x7f0200a7

.field public static final list_selected_holo_light:I = 0x7f0200a8

.field public static final list_selector_background_selected:I = 0x7f0200a9

.field public static final list_unread_holo:I = 0x7f0200aa

.field public static final listitem_background:I = 0x7f0200ab

.field public static final listitem_background_lightblue:I = 0x7f0200ac

.field public static final longpress_composer:I = 0x7f0200ad

.field public static final longpress_conversationlist:I = 0x7f0200ae

.field public static final mms_play_btn:I = 0x7f0200af

.field public static final movie:I = 0x7f0200b0

.field public static final msg_bubble_left:I = 0x7f0200b1

.field public static final msg_bubble_right:I = 0x7f0200b2

.field public static final notify_panel_notification_icon_bg:I = 0x7f0200b3

.field public static final notify_panel_notification_icon_bg_tile:I = 0x7f0200b4

.field public static final outgoing:I = 0x7f0200b5

.field public static final search_acitivity_item_background:I = 0x7f0200b6

.field public static final send_button_selector:I = 0x7f0200b7

.field public static final sim1_unchecked:I = 0x7f0200b8

.field public static final sim2_checked:I = 0x7f0200b9

.field public static final sim2_unchecked:I = 0x7f0200ba

.field public static final sim_sms_multidel_button_selector:I = 0x7f0200bb

.field public static final stat_notify_mms:I = 0x7f0200bc

.field public static final stat_notify_sms:I = 0x7f0200bd

.field public static final stat_notify_sms_failed:I = 0x7f0200be

.field public static final stat_notify_wappush:I = 0x7f0200bf

.field public static final stat_sys_no_sim:I = 0x7f0200c0

.field public static final tab_manage_sim2:I = 0x7f0200c1

.field public static final text_color:I = 0x7f0200d9

.field public static final text_color_black:I = 0x7f0200d4

.field public static final text_color_blue:I = 0x7f0200d6

.field public static final text_color_offwhite:I = 0x7f0200d5

.field public static final text_color_red:I = 0x7f0200d3

.field public static final textfield_activated_holo_light:I = 0x7f0200c2

.field public static final textfield_default_holo_light:I = 0x7f0200c3

.field public static final textfield_disabled_focused_holo_light:I = 0x7f0200c4

.field public static final textfield_disabled_holo_light:I = 0x7f0200c5

.field public static final textfield_focused_holo_light:I = 0x7f0200c6

.field public static final textfield_im_user_pressed_pad:I = 0x7f0200c7

.field public static final textfield_im_user_selected_pad:I = 0x7f0200c8

.field public static final textfield_multiline_activated_holo_light:I = 0x7f0200c9

.field public static final textfield_multiline_default_holo_light:I = 0x7f0200ca

.field public static final textfield_multiline_disabled_focused_holo_light:I = 0x7f0200cb

.field public static final textfield_multiline_disabled_holo_light:I = 0x7f0200cc

.field public static final textfield_multiline_focused_holo_light:I = 0x7f0200cd

.field public static final white_background:I = 0x7f0200d8

.field public static final widget_conversation_read_selector:I = 0x7f0200ce

.field public static final widget_conversation_unread_selector:I = 0x7f0200cf

.field public static final zoomin_selector:I = 0x7f0200d0

.field public static final zoomout_selector:I = 0x7f0200d1


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
