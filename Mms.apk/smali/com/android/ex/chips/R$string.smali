.class public final Lcom/android/ex/chips/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/ex/chips/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final Cancel:I = 0x7f0a0007

.field public static final Insert_sdcard:I = 0x7f0a0001

.field public static final OK:I = 0x7f0a0006

.field public static final add:I = 0x7f0a011b

.field public static final add_attachment:I = 0x7f0a0160

.field public static final add_attachment_activity:I = 0x7f0a0057

.field public static final add_contact_dialog_existing:I = 0x7f0a0086

.field public static final add_contact_dialog_message:I = 0x7f0a0084

.field public static final add_contact_dialog_new:I = 0x7f0a0085

.field public static final add_contacts:I = 0x7f0a0064

.field public static final add_music:I = 0x7f0a015a

.field public static final add_picture:I = 0x7f0a0158

.field public static final add_quick_text_successful:I = 0x7f0a0121

.field public static final add_slide:I = 0x7f0a0153

.field public static final add_slide_hint:I = 0x7f0a0154

.field public static final add_subject:I = 0x7f0a0161

.field public static final add_video:I = 0x7f0a015c

.field public static final adding_attachments:I = 0x7f0a0185

.field public static final adding_attachments_title:I = 0x7f0a0184

.field public static final adding_recipients:I = 0x7f0a0246

.field public static final all_threads:I = 0x7f0a0168

.field public static final allmessage:I = 0x7f0a0104

.field public static final already_have_quick_text:I = 0x7f0a0122

.field public static final anonymous_recipient:I = 0x7f0a0033

.field public static final app_label:I = 0x7f0a012e

.field public static final ask_for_automatically_resize:I = 0x7f0a0042

.field public static final attach_image:I = 0x7f0a01e8

.field public static final attach_record_sound:I = 0x7f0a01ed

.field public static final attach_record_video:I = 0x7f0a01eb

.field public static final attach_ringtone:I = 0x7f0a0053

.field public static final attach_slideshow:I = 0x7f0a01ee

.field public static final attach_sound:I = 0x7f0a01ec

.field public static final attach_take_photo:I = 0x7f0a01e9

.field public static final attach_vcalendar:I = 0x7f0a00b3

.field public static final attach_vcard:I = 0x7f0a00b5

.field public static final attach_video:I = 0x7f0a01ea

.field public static final attachment_audio:I = 0x7f0a0127

.field public static final attachment_picture:I = 0x7f0a012a

.field public static final attachment_slideshow:I = 0x7f0a0128

.field public static final attachment_video:I = 0x7f0a0129

.field public static final bcc_label:I = 0x7f0a01c9

.field public static final broadcast_from_to:I = 0x7f0a0032

.field public static final building_slideshow_title:I = 0x7f0a023a

.field public static final by_card:I = 0x7f0a00fa

.field public static final call_assistant:I = 0x7f0a00d3

.field public static final call_callback:I = 0x7f0a00c8

.field public static final call_car:I = 0x7f0a00c9

.field public static final call_company_main:I = 0x7f0a00ca

.field public static final call_custom:I = 0x7f0a00c0

.field public static final call_fax_home:I = 0x7f0a00c5

.field public static final call_fax_work:I = 0x7f0a00c4

.field public static final call_home:I = 0x7f0a00c1

.field public static final call_isdn:I = 0x7f0a00cb

.field public static final call_main:I = 0x7f0a00cc

.field public static final call_mms:I = 0x7f0a00d4

.field public static final call_mobile:I = 0x7f0a00c2

.field public static final call_other:I = 0x7f0a00c7

.field public static final call_other_fax:I = 0x7f0a00cd

.field public static final call_pager:I = 0x7f0a00c6

.field public static final call_radio:I = 0x7f0a00ce

.field public static final call_telex:I = 0x7f0a00cf

.field public static final call_tty_tdd:I = 0x7f0a00d0

.field public static final call_video_call:I = 0x7f0a00ff

.field public static final call_work:I = 0x7f0a00c3

.field public static final call_work_mobile:I = 0x7f0a00d1

.field public static final call_work_pager:I = 0x7f0a00d2

.field public static final cannot_add_picture_and_video:I = 0x7f0a017f

.field public static final cannot_add_recipient:I = 0x7f0a0045

.field public static final cannot_add_slide_anymore:I = 0x7f0a017e

.field public static final cannot_forward_drm_obj:I = 0x7f0a017b

.field public static final cannot_get_details:I = 0x7f0a01c1

.field public static final cannot_load_message:I = 0x7f0a0043

.field public static final cannot_play_audio:I = 0x7f0a0186

.field public static final cannot_save_message:I = 0x7f0a0180

.field public static final cannot_send_message:I = 0x7f0a0179

.field public static final cannot_send_message_reason:I = 0x7f0a017a

.field public static final cannot_send_message_reason_no_content:I = 0x7f0a005e

.field public static final cb_default_channel_name:I = 0x7f0a0062

.field public static final cb_message:I = 0x7f0a0061

.field public static final cb_read_message:I = 0x7f0a005f

.field public static final cb_read_message2:I = 0x7f0a0060

.field public static final cdma_not_support:I = 0x7f0a0103

.field public static final cell_broadcast:I = 0x7f0a0123

.field public static final cell_broadcast_settings:I = 0x7f0a0125

.field public static final cell_broadcast_title:I = 0x7f0a0124

.field public static final change_duration_activity:I = 0x7f0a0218

.field public static final changeview:I = 0x7f0a00ed

.field public static final chat:I = 0x7f0a00e7

.field public static final chat_aim:I = 0x7f0a00df

.field public static final chat_gtalk:I = 0x7f0a00e4

.field public static final chat_icq:I = 0x7f0a00e5

.field public static final chat_jabber:I = 0x7f0a00e6

.field public static final chat_msn:I = 0x7f0a00e0

.field public static final chat_qq:I = 0x7f0a00e3

.field public static final chat_skype:I = 0x7f0a00e2

.field public static final chat_yahoo:I = 0x7f0a00e1

.field public static final class_0_message_activity:I = 0x7f0a021c

.field public static final compose_title:I = 0x7f0a0040

.field public static final compressing:I = 0x7f0a016b

.field public static final confirm:I = 0x7f0a01ff

.field public static final confirm_clear_search_text:I = 0x7f0a0224

.field public static final confirm_clear_search_title:I = 0x7f0a0223

.field public static final confirm_delete_SIM_message:I = 0x7f0a01be

.field public static final confirm_delete_all_SIM_messages:I = 0x7f0a01bd

.field public static final confirm_delete_all_conversations:I = 0x7f0a01ba

.field public static final confirm_delete_all_messages:I = 0x7f0a0049

.field public static final confirm_delete_allmessage:I = 0x7f0a0106

.field public static final confirm_delete_conversation:I = 0x7f0a01b9

.field public static final confirm_delete_locked_message:I = 0x7f0a01bc

.field public static final confirm_delete_message:I = 0x7f0a01bb

.field public static final confirm_delete_selected_messages:I = 0x7f0a0072

.field public static final confirm_delete_selected_theads:I = 0x7f0a0073

.field public static final confirm_dialog_locked_title:I = 0x7f0a01b8

.field public static final confirm_dialog_title:I = 0x7f0a01b7

.field public static final confirm_download_message:I = 0x7f0a00f7

.field public static final confirm_rate_limit:I = 0x7f0a0207

.field public static final confirm_restricted_audio:I = 0x7f0a0018

.field public static final confirm_restricted_image:I = 0x7f0a0019

.field public static final confirm_restricted_video:I = 0x7f0a001a

.field public static final contact_address:I = 0x7f0a007d

.field public static final contact_email:I = 0x7f0a007b

.field public static final contact_name:I = 0x7f0a0079

.field public static final contact_organization:I = 0x7f0a007c

.field public static final contact_tel:I = 0x7f0a007a

.field public static final converting_to_picture_message:I = 0x7f0a017c

.field public static final converting_to_text_message:I = 0x7f0a017d

.field public static final copy_email:I = 0x7f0a024b

.field public static final copy_message_text:I = 0x7f0a014d

.field public static final copy_number:I = 0x7f0a024c

.field public static final copy_to_sdcard:I = 0x7f0a020d

.field public static final copy_to_sdcard_fail:I = 0x7f0a020f

.field public static final copy_to_sdcard_success:I = 0x7f0a020e

.field public static final create_new_message:I = 0x7f0a002e

.field public static final cu_subject:I = 0x7f0a0013

.field public static final data_connected_confirm_message:I = 0x7f0a006a

.field public static final data_connected_confirm_title:I = 0x7f0a0069

.field public static final delete:I = 0x7f0a01c0

.field public static final delete_all_confirm:I = 0x7f0a00f3

.field public static final delete_btn_str:I = 0x7f0a0108

.field public static final delete_message:I = 0x7f0a0140

.field public static final delete_successful:I = 0x7f0a011f

.field public static final delete_thread:I = 0x7f0a0146

.field public static final delete_unlocked:I = 0x7f0a01bf

.field public static final delete_unsuccessful:I = 0x7f0a0120

.field public static final deleting:I = 0x7f0a0087

.field public static final delivered_label:I = 0x7f0a0244

.field public static final delivery_header_title:I = 0x7f0a01de

.field public static final delivery_report_activity:I = 0x7f0a0216

.field public static final delivery_toast_body:I = 0x7f0a01f1

.field public static final disable_notifications_dialog_message:I = 0x7f0a0096

.field public static final discard:I = 0x7f0a0162

.field public static final discard_message:I = 0x7f0a0174

.field public static final discard_message_reason:I = 0x7f0a0175

.field public static final discard_slideshow:I = 0x7f0a0155

.field public static final dl_expired_notification:I = 0x7f0a0056

.field public static final dl_failure_notification:I = 0x7f0a0205

.field public static final done:I = 0x7f0a014f

.field public static final download:I = 0x7f0a0148

.field public static final download_failed_due_to_full_memory:I = 0x7f0a003b

.field public static final download_later:I = 0x7f0a020b

.field public static final downloading:I = 0x7f0a0149

.field public static final draft_separator:I = 0x7f0a0247

.field public static final draftbox:I = 0x7f0a00ea

.field public static final drm_protected_text:I = 0x7f0a014b

.field public static final duration_not_a_number:I = 0x7f0a0190

.field public static final duration_sec:I = 0x7f0a018a

.field public static final duration_selector_title:I = 0x7f0a018b

.field public static final duration_zero:I = 0x7f0a0191

.field public static final edit:I = 0x7f0a019a

.field public static final edit_slide_activity:I = 0x7f0a021a

.field public static final edit_slideshow_activity:I = 0x7f0a0219

.field public static final edit_text_activity:I = 0x7f0a0059

.field public static final email:I = 0x7f0a00da

.field public static final email_custom:I = 0x7f0a00d9

.field public static final email_home:I = 0x7f0a00d5

.field public static final email_mobile:I = 0x7f0a00d6

.field public static final email_other:I = 0x7f0a00d8

.field public static final email_work:I = 0x7f0a00d7

.field public static final empty_vcard:I = 0x7f0a00b9

.field public static final enumeration_comma:I = 0x7f0a012c

.field public static final error_add_attachment:I = 0x7f0a0101

.field public static final error_code_label:I = 0x7f0a01d4

.field public static final error_state:I = 0x7f0a022e

.field public static final error_state_text:I = 0x7f0a022f

.field public static final error_unsupported_scheme:I = 0x7f0a0028

.field public static final exceed_message_size_limitation:I = 0x7f0a016e

.field public static final exceed_subject_length_limitation:I = 0x7f0a0095

.field public static final expire_on:I = 0x7f0a0141

.field public static final export_disk_problem:I = 0x7f0a00ab

.field public static final export_message_empty:I = 0x7f0a00a8

.field public static final export_message_fail:I = 0x7f0a00a5

.field public static final export_message_ongoing:I = 0x7f0a00a0

.field public static final export_message_success:I = 0x7f0a00a2

.field public static final failed_to_add_media:I = 0x7f0a0170

.field public static final failed_to_parse_vcard:I = 0x7f0a00b8

.field public static final failed_to_resize_image:I = 0x7f0a0171

.field public static final fdn_check_failure:I = 0x7f0a0202

.field public static final fdn_enabled:I = 0x7f0a005d

.field public static final file_attachment_import_vcard:I = 0x7f0a00b4

.field public static final file_attachment_vcalendar_name:I = 0x7f0a00b1

.field public static final file_attachment_vcard_name:I = 0x7f0a00b2

.field public static final finish:I = 0x7f0a0118

.field public static final folder_download:I = 0x7f0a00f6

.field public static final folder_recieve_date:I = 0x7f0a00f5

.field public static final folder_recipient:I = 0x7f0a00f4

.field public static final forward_from:I = 0x7f0a00ae

.field public static final forward_prefix:I = 0x7f0a0173

.field public static final from_label:I = 0x7f0a01c7

.field public static final gemini_3g_indic:I = 0x7f0a0009

.field public static final get_icc_sms_capacity_failed:I = 0x7f0a0092

.field public static final has_draft:I = 0x7f0a013a

.field public static final has_invalid_recipient:I = 0x7f0a0176

.field public static final hidden_sender_address:I = 0x7f0a0195

.field public static final icc_sms_total:I = 0x7f0a0091

.field public static final icc_sms_used:I = 0x7f0a0090

.field public static final image_resolution_too_large:I = 0x7f0a001d

.field public static final image_too_large:I = 0x7f0a0041

.field public static final import_message_fail:I = 0x7f0a00a4

.field public static final import_message_list:I = 0x7f0a00a6

.field public static final import_message_list_empty:I = 0x7f0a00a7

.field public static final import_message_ongoing:I = 0x7f0a00a1

.field public static final import_message_success:I = 0x7f0a00a3

.field public static final inbox:I = 0x7f0a00e8

.field public static final inline_subject:I = 0x7f0a014a

.field public static final insufficient_drm_rights:I = 0x7f0a014c

.field public static final invalid_contact_message:I = 0x7f0a00f9

.field public static final invalid_destination:I = 0x7f0a0177

.field public static final invalid_recipient_message:I = 0x7f0a0178

.field public static final kilobyte:I = 0x7f0a0142

.field public static final layout_bottom:I = 0x7f0a018e

.field public static final layout_selector_title:I = 0x7f0a018c

.field public static final layout_top:I = 0x7f0a018d

.field public static final loading_conversations:I = 0x7f0a012d

.field public static final locked_message_cannot_be_deleted:I = 0x7f0a0044

.field public static final long_press_composer_content:I = 0x7f0a0110

.field public static final long_press_conversationlist_content:I = 0x7f0a010f

.field public static final long_press_conversationlist_title:I = 0x7f0a010e

.field public static final map_custom:I = 0x7f0a00de

.field public static final map_home:I = 0x7f0a00db

.field public static final map_other:I = 0x7f0a00dd

.field public static final map_work:I = 0x7f0a00dc

.field public static final mark_as_read_btn_str:I = 0x7f0a0107

.field public static final max_recipients_message:I = 0x7f0a007f

.field public static final max_recipients_title:I = 0x7f0a007e

.field public static final me:I = 0x7f0a0065

.field public static final menu_add_address_to_contacts:I = 0x7f0a0134

.field public static final menu_add_to_bookmark:I = 0x7f0a0026

.field public static final menu_add_to_contacts:I = 0x7f0a0194

.field public static final menu_call:I = 0x7f0a0135

.field public static final menu_call_back:I = 0x7f0a0130

.field public static final menu_cell_broadcasts:I = 0x7f0a023d

.field public static final menu_compose_new:I = 0x7f0a0132

.field public static final menu_debug_dump:I = 0x7f0a023c

.field public static final menu_delete:I = 0x7f0a0137

.field public static final menu_delete_all:I = 0x7f0a0136

.field public static final menu_delete_messages:I = 0x7f0a01d6

.field public static final menu_edit:I = 0x7f0a01d5

.field public static final menu_forward:I = 0x7f0a0147

.field public static final menu_goto_browser:I = 0x7f0a0025

.field public static final menu_insert_quick_text:I = 0x7f0a0119

.field public static final menu_insert_smiley:I = 0x7f0a0213

.field public static final menu_insert_text_vcard:I = 0x7f0a0078

.field public static final menu_item_send_by:I = 0x7f0a0068

.field public static final menu_item_sim1:I = 0x7f0a006b

.field public static final menu_item_sim2:I = 0x7f0a006c

.field public static final menu_lock:I = 0x7f0a01d7

.field public static final menu_omacp:I = 0x7f0a0030

.field public static final menu_preferences:I = 0x7f0a0133

.field public static final menu_reply:I = 0x7f0a003d

.field public static final menu_retry_sending:I = 0x7f0a004b

.field public static final menu_retry_sending_all:I = 0x7f0a004c

.field public static final menu_search:I = 0x7f0a023b

.field public static final menu_send_email:I = 0x7f0a0131

.field public static final menu_send_sms:I = 0x7f0a002f

.field public static final menu_show_icc_sms_capacity:I = 0x7f0a008e

.field public static final menu_sim_sms:I = 0x7f0a003e

.field public static final menu_undelivered_messages:I = 0x7f0a0031

.field public static final menu_unlock:I = 0x7f0a01d8

.field public static final menu_view:I = 0x7f0a0138

.field public static final menu_view_contact:I = 0x7f0a0193

.field public static final message_class_label:I = 0x7f0a01d3

.field public static final message_count_format:I = 0x7f0a0248

.field public static final message_count_notification:I = 0x7f0a0249

.field public static final message_details_title:I = 0x7f0a01c2

.field public static final message_download_failed_title:I = 0x7f0a0208

.field public static final message_failed_body:I = 0x7f0a020a

.field public static final message_font_size_dialog_cancel:I = 0x7f0a0117

.field public static final message_font_size_dialog_title:I = 0x7f0a0116

.field public static final message_open_email_fail:I = 0x7f0a0063

.field public static final message_options:I = 0x7f0a014e

.field public static final message_queued:I = 0x7f0a0201

.field public static final message_saved_as_draft:I = 0x7f0a0181

.field public static final message_send_failed_title:I = 0x7f0a0209

.field public static final message_send_read_report:I = 0x7f0a0200

.field public static final message_size_label:I = 0x7f0a01ce

.field public static final message_too_big_for_video:I = 0x7f0a016f

.field public static final message_type_label:I = 0x7f0a01c3

.field public static final messagelist_sender_self:I = 0x7f0a013c

.field public static final mms:I = 0x7f0a0167

.field public static final mms_priority_label:I = 0x7f0a004a

.field public static final mms_too_big_to_download:I = 0x7f0a003c

.field public static final modify_successful:I = 0x7f0a011d

.field public static final modify_unsuccessful:I = 0x7f0a011e

.field public static final more_recipients:I = 0x7f0a0000

.field public static final more_string:I = 0x7f0a024a

.field public static final move_down:I = 0x7f0a0151

.field public static final move_up:I = 0x7f0a0150

.field public static final multimedia_message:I = 0x7f0a01c5

.field public static final multimedia_notification:I = 0x7f0a01c6

.field public static final multiple_recipients:I = 0x7f0a0046

.field public static final name_colon:I = 0x7f0a003f

.field public static final new_message:I = 0x7f0a012f

.field public static final nickname:I = 0x7f0a00bf

.field public static final no:I = 0x7f0a0197

.field public static final no_application_response:I = 0x7f0a0102

.field public static final no_conversations:I = 0x7f0a0231

.field public static final no_item_selected:I = 0x7f0a0111

.field public static final no_messages:I = 0x7f0a00fe

.field public static final no_sd_card:I = 0x7f0a009f

.field public static final no_sim_1:I = 0x7f0a006d

.field public static final no_sim_2:I = 0x7f0a006e

.field public static final no_subject:I = 0x7f0a0203

.field public static final no_subject_view:I = 0x7f0a013b

.field public static final note:I = 0x7f0a00bc

.field public static final notification_failed_multiple:I = 0x7f0a01f4

.field public static final notification_failed_multiple_title:I = 0x7f0a01f5

.field public static final notification_multiple:I = 0x7f0a01f2

.field public static final notification_multiple_cb:I = 0x7f0a0054

.field public static final notification_multiple_cb_title:I = 0x7f0a0055

.field public static final notification_multiple_title:I = 0x7f0a01f3

.field public static final notification_separator:I = 0x7f0a012b

.field public static final open:I = 0x7f0a00f2

.field public static final open_keyboard_to_compose_message:I = 0x7f0a016a

.field public static final organization:I = 0x7f0a00bd

.field public static final outbox:I = 0x7f0a00e9

.field public static final page:I = 0x7f0a00fb

.field public static final parse_vcard:I = 0x7f0a00ba

.field public static final pick_numbers_activity:I = 0x7f0a005a

.field public static final pick_too_many_recipients:I = 0x7f0a0245

.field public static final play:I = 0x7f0a0199

.field public static final play_as_slideshow:I = 0x7f0a00f8

.field public static final please_wait:I = 0x7f0a00b7

.field public static final prefDefault_vibrateWhen:I = 0x7f0a0237

.field public static final prefDefault_vibrate_false:I = 0x7f0a0239

.field public static final prefDefault_vibrate_true:I = 0x7f0a0238

.field public static final prefDialogTitle_vibrateWhen:I = 0x7f0a0243

.field public static final pref_key_mms_priority:I = 0x7f0a0235

.field public static final pref_messages_to_save:I = 0x7f0a01b2

.field public static final pref_mms_clear_search_history_summary:I = 0x7f0a0226

.field public static final pref_mms_clear_search_history_title:I = 0x7f0a0225

.field public static final pref_mms_settings_title:I = 0x7f0a019f

.field public static final pref_notification_settings_title:I = 0x7f0a019e

.field public static final pref_sms_settings_title:I = 0x7f0a01a0

.field public static final pref_sms_storage_title:I = 0x7f0a01a1

.field public static final pref_summary_auto_delete:I = 0x7f0a01a6

.field public static final pref_summary_delete_limit:I = 0x7f0a01a7

.field public static final pref_summary_export_msg:I = 0x7f0a009d

.field public static final pref_summary_import_msg:I = 0x7f0a009b

.field public static final pref_summary_manage_sim_messages:I = 0x7f0a01a2

.field public static final pref_summary_message_font_size:I = 0x7f0a0115

.field public static final pref_summary_mms_auto_reply_read_reports:I = 0x7f0a0094

.field public static final pref_summary_mms_auto_retrieval:I = 0x7f0a01b4

.field public static final pref_summary_mms_creation_mode:I = 0x7f0a0015

.field public static final pref_summary_mms_delivery_reports:I = 0x7f0a01a3

.field public static final pref_summary_mms_enable_to_send_delivery_reports:I = 0x7f0a0003

.field public static final pref_summary_mms_read_reports:I = 0x7f0a01a4

.field public static final pref_summary_mms_retrieval_during_roaming:I = 0x7f0a01b6

.field public static final pref_summary_mms_size_limit:I = 0x7f0a0017

.field public static final pref_summary_notification_enabled:I = 0x7f0a01b0

.field public static final pref_summary_notification_vibrateWhen:I = 0x7f0a0242

.field public static final pref_summary_sms_delivery_reports:I = 0x7f0a01a5

.field public static final pref_summary_wappush_enable:I = 0x7f0a002b

.field public static final pref_summary_wappush_sl_autoloading:I = 0x7f0a002d

.field public static final pref_title_auto_delete:I = 0x7f0a01ac

.field public static final pref_title_export_msg:I = 0x7f0a009e

.field public static final pref_title_font_size_setting:I = 0x7f0a0113

.field public static final pref_title_import_msg:I = 0x7f0a009c

.field public static final pref_title_io_settings:I = 0x7f0a009a

.field public static final pref_title_long_press_operation_guide:I = 0x7f0a010c

.field public static final pref_title_manage_sim_messages:I = 0x7f0a01a8

.field public static final pref_title_message_font_size:I = 0x7f0a0114

.field public static final pref_title_mms_auto_reply_read_reports:I = 0x7f0a0093

.field public static final pref_title_mms_auto_retrieval:I = 0x7f0a01b3

.field public static final pref_title_mms_creation_mode:I = 0x7f0a0014

.field public static final pref_title_mms_delete:I = 0x7f0a01ae

.field public static final pref_title_mms_delivery_reports:I = 0x7f0a01a9

.field public static final pref_title_mms_enable_to_send_delivery_reports:I = 0x7f0a0002

.field public static final pref_title_mms_read_reports:I = 0x7f0a01aa

.field public static final pref_title_mms_retrieval_during_roaming:I = 0x7f0a01b5

.field public static final pref_title_mms_size_limit:I = 0x7f0a0016

.field public static final pref_title_notification_enabled:I = 0x7f0a01af

.field public static final pref_title_notification_ringtone:I = 0x7f0a01b1

.field public static final pref_title_notification_vibrateWhen:I = 0x7f0a0241

.field public static final pref_title_sms_delete:I = 0x7f0a01ad

.field public static final pref_title_sms_delivery_reports:I = 0x7f0a01ab

.field public static final pref_title_storage_status:I = 0x7f0a0088

.field public static final pref_title_wappush_enable:I = 0x7f0a002a

.field public static final pref_title_wappush_settings:I = 0x7f0a0029

.field public static final pref_title_wappush_sl_autoloading:I = 0x7f0a002c

.field public static final pref_title_watch_animation:I = 0x7f0a010d

.field public static final preferences_title:I = 0x7f0a019c

.field public static final preview:I = 0x7f0a0187

.field public static final preview_slideshow:I = 0x7f0a0188

.field public static final priority_high:I = 0x7f0a01d0

.field public static final priority_label:I = 0x7f0a01cf

.field public static final priority_low:I = 0x7f0a01d2

.field public static final priority_normal:I = 0x7f0a01d1

.field public static final quick_text_editor:I = 0x7f0a011c

.field public static final rate_limit_surpassed:I = 0x7f0a0206

.field public static final received_header:I = 0x7f0a010b

.field public static final received_label:I = 0x7f0a01cb

.field public static final received_on:I = 0x7f0a0037

.field public static final recipient_label:I = 0x7f0a01e6

.field public static final refreshing:I = 0x7f0a0139

.field public static final remove:I = 0x7f0a0165

.field public static final remove_music:I = 0x7f0a015b

.field public static final remove_picture:I = 0x7f0a0159

.field public static final remove_slide:I = 0x7f0a0152

.field public static final remove_text:I = 0x7f0a0157

.field public static final remove_video:I = 0x7f0a015d

.field public static final replace:I = 0x7f0a0164

.field public static final replace_image:I = 0x7f0a0189

.field public static final reply_send:I = 0x7f0a0109

.field public static final resize:I = 0x7f0a0047

.field public static final resize_image_error_information:I = 0x7f0a0172

.field public static final restore_default:I = 0x7f0a019d

.field public static final restricted_forward_message:I = 0x7f0a0075

.field public static final restricted_forward_title:I = 0x7f0a0074

.field public static final retrying_dialog_body:I = 0x7f0a004d

.field public static final save:I = 0x7f0a0227

.field public static final save_message_to_sim:I = 0x7f0a0035

.field public static final save_message_to_sim_successful:I = 0x7f0a0038

.field public static final save_message_to_sim_unsuccessful:I = 0x7f0a0039

.field public static final save_ringtone:I = 0x7f0a0210

.field public static final saved_label:I = 0x7f0a01cc

.field public static final saved_ringtone:I = 0x7f0a0211

.field public static final saved_ringtone_fail:I = 0x7f0a0212

.field public static final search:I = 0x7f0a021f

.field public static final search_empty:I = 0x7f0a0221

.field public static final search_hint:I = 0x7f0a021e

.field public static final search_history:I = 0x7f0a0222

.field public static final search_label:I = 0x7f0a021d

.field public static final search_setting_description:I = 0x7f0a0220

.field public static final secs:I = 0x7f0a0192

.field public static final select_all:I = 0x7f0a0071

.field public static final select_audio:I = 0x7f0a020c

.field public static final select_bottom_text:I = 0x7f0a01ef

.field public static final select_contact_method_activity:I = 0x7f0a005c

.field public static final select_conversations:I = 0x7f0a0230

.field public static final select_different_media:I = 0x7f0a016d

.field public static final select_different_media_type:I = 0x7f0a001c

.field public static final select_layout_activity:I = 0x7f0a0058

.field public static final select_link_title:I = 0x7f0a0214

.field public static final select_message:I = 0x7f0a0105

.field public static final select_quick_text:I = 0x7f0a011a

.field public static final select_text:I = 0x7f0a00ac

.field public static final select_top_text:I = 0x7f0a01f0

.field public static final send:I = 0x7f0a0166

.field public static final send_by_sim1:I = 0x7f0a0067

.field public static final send_by_sim2:I = 0x7f0a0066

.field public static final send_using_mms_activity:I = 0x7f0a005b

.field public static final sending_message:I = 0x7f0a022d

.field public static final sent_label:I = 0x7f0a01ca

.field public static final sent_on:I = 0x7f0a0036

.field public static final sentbox:I = 0x7f0a00eb

.field public static final service_center_label:I = 0x7f0a0012

.field public static final service_message_not_found:I = 0x7f0a023f

.field public static final service_network_problem:I = 0x7f0a0240

.field public static final service_not_activated:I = 0x7f0a023e

.field public static final set:I = 0x7f0a0198

.field public static final set_service_center_OK:I = 0x7f0a0004

.field public static final set_service_center_fail:I = 0x7f0a0005

.field public static final show_icc_sms_capacity_title:I = 0x7f0a008f

.field public static final sim1_full_title:I = 0x7f0a006f

.field public static final sim2_full_title:I = 0x7f0a0070

.field public static final sim_copy_to_phone_memory:I = 0x7f0a01d9

.field public static final sim_delete:I = 0x7f0a01da

.field public static final sim_empty:I = 0x7f0a01dd

.field public static final sim_full_body:I = 0x7f0a01f7

.field public static final sim_full_title:I = 0x7f0a01f6

.field public static final sim_manage_messages_title:I = 0x7f0a01db

.field public static final sim_selected_dialog_title:I = 0x7f0a0082

.field public static final sim_view:I = 0x7f0a01dc

.field public static final simbox:I = 0x7f0a00ec

.field public static final slide_number:I = 0x7f0a0215

.field public static final slide_show_part:I = 0x7f0a0156

.field public static final slideshow_activity:I = 0x7f0a021b

.field public static final slideshow_details:I = 0x7f0a00f1

.field public static final sms_forward_setting:I = 0x7f0a00af

.field public static final sms_forward_setting_summary:I = 0x7f0a00b0

.field public static final sms_full_body:I = 0x7f0a01f9

.field public static final sms_full_title:I = 0x7f0a01f8

.field public static final sms_input_mode_dialog_title:I = 0x7f0a0098

.field public static final sms_input_mode_summary:I = 0x7f0a0099

.field public static final sms_input_mode_title:I = 0x7f0a0097

.field public static final sms_rejected_body:I = 0x7f0a01fb

.field public static final sms_rejected_title:I = 0x7f0a01fa

.field public static final sms_save_location:I = 0x7f0a0008

.field public static final sms_service_center:I = 0x7f0a000a

.field public static final sms_validity_period:I = 0x7f0a000b

.field public static final sms_validity_period_12hours:I = 0x7f0a000f

.field public static final sms_validity_period_1day:I = 0x7f0a0010

.field public static final sms_validity_period_1hour:I = 0x7f0a000d

.field public static final sms_validity_period_6hours:I = 0x7f0a000e

.field public static final sms_validity_period_max:I = 0x7f0a0011

.field public static final sms_validity_period_nosetting:I = 0x7f0a000c

.field public static final space_not_enough:I = 0x7f0a00fc

.field public static final space_not_enough_for_audio:I = 0x7f0a00fd

.field public static final status_deferred:I = 0x7f0a004f

.field public static final status_expired:I = 0x7f0a004e

.field public static final status_failed:I = 0x7f0a01e3

.field public static final status_indeterminate:I = 0x7f0a0051

.field public static final status_label:I = 0x7f0a01e7

.field public static final status_none:I = 0x7f0a01df

.field public static final status_pending:I = 0x7f0a01e0

.field public static final status_read:I = 0x7f0a01e1

.field public static final status_received:I = 0x7f0a01e2

.field public static final status_rejected:I = 0x7f0a01e5

.field public static final status_unreachable:I = 0x7f0a0052

.field public static final status_unread:I = 0x7f0a01e4

.field public static final status_unrecognized:I = 0x7f0a0050

.field public static final storage_dialog_attachments:I = 0x7f0a008b

.field public static final storage_dialog_available_space:I = 0x7f0a008d

.field public static final storage_dialog_database:I = 0x7f0a008c

.field public static final storage_dialog_mms:I = 0x7f0a0089

.field public static final storage_dialog_mms_size:I = 0x7f0a0100

.field public static final storage_dialog_sms:I = 0x7f0a008a

.field public static final storage_limits_activity:I = 0x7f0a0217

.field public static final storage_limits_message:I = 0x7f0a0229

.field public static final storage_limits_setting:I = 0x7f0a022a

.field public static final storage_limits_setting_dismiss:I = 0x7f0a022b

.field public static final storage_limits_title:I = 0x7f0a0228

.field public static final strFail:I = 0x7f0a0077

.field public static final strOk:I = 0x7f0a0076

.field public static final string_via:I = 0x7f0a0236

.field public static final subject_hint:I = 0x7f0a015f

.field public static final subject_label:I = 0x7f0a01cd

.field public static final suggested:I = 0x7f0a0083

.field public static final sync_mms_to_db:I = 0x7f0a0112

.field public static final test_number:I = 0x7f0a0233

.field public static final test_operator:I = 0x7f0a0232

.field public static final test_status:I = 0x7f0a0234

.field public static final text_message:I = 0x7f0a01c4

.field public static final to_address_label:I = 0x7f0a01c8

.field public static final to_hint:I = 0x7f0a015e

.field public static final to_label:I = 0x7f0a0048

.field public static final too_many_attachments:I = 0x7f0a0183

.field public static final too_many_recipients:I = 0x7f0a0182

.field public static final too_many_unsent_mms:I = 0x7f0a022c

.field public static final transmission_transiently_failed:I = 0x7f0a003a

.field public static final try_to_send:I = 0x7f0a019b

.field public static final type_audio:I = 0x7f0a01fc

.field public static final type_common_file:I = 0x7f0a00b6

.field public static final type_picture:I = 0x7f0a01fd

.field public static final type_to_compose_text_enter_to_send:I = 0x7f0a0169

.field public static final type_to_compose_text_or_leave_blank:I = 0x7f0a018f

.field public static final type_video:I = 0x7f0a01fe

.field public static final undelivered_msg_dialog_body:I = 0x7f0a0144

.field public static final undelivered_msg_dialog_title:I = 0x7f0a0143

.field public static final undelivered_sms_dialog_body:I = 0x7f0a0145

.field public static final unknown_sender:I = 0x7f0a0204

.field public static final unselect_all:I = 0x7f0a00ad

.field public static final unsupport_media_type:I = 0x7f0a001b

.field public static final unsupported_media_format:I = 0x7f0a016c

.field public static final vcard_no_details:I = 0x7f0a00bb

.field public static final via_test:I = 0x7f0a010a

.field public static final via_without_time_for_recieve:I = 0x7f0a0081

.field public static final via_without_time_for_send:I = 0x7f0a0080

.field public static final view:I = 0x7f0a0163

.field public static final view_delivery_report:I = 0x7f0a013f

.field public static final view_message_details:I = 0x7f0a013e

.field public static final view_more_conversations:I = 0x7f0a0126

.field public static final view_picture:I = 0x7f0a0034

.field public static final view_slideshow:I = 0x7f0a013d

.field public static final viewer_title_cb:I = 0x7f0a00f0

.field public static final viewer_title_sms:I = 0x7f0a00ee

.field public static final viewer_title_wappush:I = 0x7f0a00ef

.field public static final website:I = 0x7f0a00be

.field public static final whether_export_item:I = 0x7f0a00a9

.field public static final whether_import_item:I = 0x7f0a00aa

.field public static final wp_msg_created_label:I = 0x7f0a001f

.field public static final wp_msg_created_unknown:I = 0x7f0a0020

.field public static final wp_msg_expiration_label:I = 0x7f0a0027

.field public static final wp_msg_priority_high:I = 0x7f0a0024

.field public static final wp_msg_priority_label:I = 0x7f0a0021

.field public static final wp_msg_priority_low:I = 0x7f0a0022

.field public static final wp_msg_priority_medium:I = 0x7f0a0023

.field public static final wp_msg_type:I = 0x7f0a001e

.field public static final yes:I = 0x7f0a0196


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
