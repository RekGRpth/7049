.class Lcom/android/ex/chips/RecipientChip;
.super Landroid/text/style/ImageSpan;
.source "RecipientChip.java"


# instance fields
.field private final mContactId:J

.field private final mDataId:J

.field private final mDisplay:Ljava/lang/CharSequence;

.field private mEntry:Lcom/android/ex/chips/RecipientEntry;

.field private mOriginalText:Ljava/lang/CharSequence;

.field private mSelected:Z

.field private final mValue:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;Lcom/android/ex/chips/RecipientEntry;I)V
    .locals 2
    .param p1    # Landroid/graphics/drawable/Drawable;
    .param p2    # Lcom/android/ex/chips/RecipientEntry;
    .param p3    # I

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;I)V

    iput-boolean v0, p0, Lcom/android/ex/chips/RecipientChip;->mSelected:Z

    invoke-virtual {p2}, Lcom/android/ex/chips/RecipientEntry;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/ex/chips/RecipientChip;->mDisplay:Ljava/lang/CharSequence;

    invoke-virtual {p2}, Lcom/android/ex/chips/RecipientEntry;->getDestination()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/ex/chips/RecipientChip;->mValue:Ljava/lang/CharSequence;

    invoke-virtual {p2}, Lcom/android/ex/chips/RecipientEntry;->getContactId()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/ex/chips/RecipientChip;->mContactId:J

    invoke-virtual {p2}, Lcom/android/ex/chips/RecipientEntry;->getDataId()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/ex/chips/RecipientChip;->mDataId:J

    iput-object p2, p0, Lcom/android/ex/chips/RecipientChip;->mEntry:Lcom/android/ex/chips/RecipientEntry;

    return-void
.end method


# virtual methods
.method public getContactId()J
    .locals 2

    iget-wide v0, p0, Lcom/android/ex/chips/RecipientChip;->mContactId:J

    return-wide v0
.end method

.method public getDataId()J
    .locals 2

    iget-wide v0, p0, Lcom/android/ex/chips/RecipientChip;->mDataId:J

    return-wide v0
.end method

.method public getDisplay()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/android/ex/chips/RecipientChip;->mDisplay:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getEntry()Lcom/android/ex/chips/RecipientEntry;
    .locals 1

    iget-object v0, p0, Lcom/android/ex/chips/RecipientChip;->mEntry:Lcom/android/ex/chips/RecipientEntry;

    return-object v0
.end method

.method public getOriginalText()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/android/ex/chips/RecipientChip;->mOriginalText:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/ex/chips/RecipientChip;->mOriginalText:Ljava/lang/CharSequence;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/ex/chips/RecipientChip;->mEntry:Lcom/android/ex/chips/RecipientEntry;

    invoke-virtual {v0}, Lcom/android/ex/chips/RecipientEntry;->getDestination()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getValue()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/android/ex/chips/RecipientChip;->mValue:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public isSelected()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/ex/chips/RecipientChip;->mSelected:Z

    return v0
.end method

.method public setOriginalText(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    :cond_0
    iput-object p1, p0, Lcom/android/ex/chips/RecipientChip;->mOriginalText:Ljava/lang/CharSequence;

    return-void
.end method

.method public setSelected(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/ex/chips/RecipientChip;->mSelected:Z

    return-void
.end method
