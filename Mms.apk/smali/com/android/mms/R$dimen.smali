.class public final Lcom/android/mms/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final avatar_width_height:I = 0x7f090003

.field public static final chip_height:I = 0x7f09000e

.field public static final chip_padding:I = 0x7f09000d

.field public static final chip_text_size:I = 0x7f09000f

.field public static final input_text_height:I = 0x7f090004

.field public static final input_text_height_adjusted:I = 0x7f090005

.field public static final line_spacing_extra:I = 0x7f090010

.field public static final longpress_conversationlist_margin:I = 0x7f090006

.field public static final longpress_conversationlist_margin_bottom:I = 0x7f090007

.field public static final message_item_avatar_on_right_text_indent:I = 0x7f090000

.field public static final message_item_text_padding_left_right:I = 0x7f090001

.field public static final message_item_text_padding_top:I = 0x7f090002

.field public static final mms_inline_attachment_size:I = 0x7f090008

.field public static final widget_margin_bottom:I = 0x7f09000c

.field public static final widget_margin_left:I = 0x7f09000a

.field public static final widget_margin_right:I = 0x7f09000b

.field public static final widget_margin_top:I = 0x7f090009


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
