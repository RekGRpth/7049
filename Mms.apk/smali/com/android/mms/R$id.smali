.class public final Lcom/android/mms/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final LinearLayout01:I = 0x7f0e00db

.field public static final LinearLayout02:I = 0x7f0e00dd

.field public static final account_spinner:I = 0x7f0e0069

.field public static final account_spinner_list:I = 0x7f0e006c

.field public static final action_cell_broadcasts:I = 0x7f0e00fc

.field public static final action_compose_new:I = 0x7f0e00f6

.field public static final action_debug_dump:I = 0x7f0e00fd

.field public static final action_delete_all:I = 0x7f0e00f9

.field public static final action_omacp:I = 0x7f0e00fb

.field public static final action_settings:I = 0x7f0e00fa

.field public static final action_siminfo:I = 0x7f0e00f8

.field public static final album_name:I = 0x7f0e000b

.field public static final all_apps_cling_title:I = 0x7f0e007e

.field public static final artist_name:I = 0x7f0e000c

.field public static final attach_Name:I = 0x7f0e0075

.field public static final attachment:I = 0x7f0e0045

.field public static final attachment_editor:I = 0x7f0e002a

.field public static final attachment_editor_scroll_view:I = 0x7f0e0029

.field public static final attachment_icon:I = 0x7f0e00d6

.field public static final attachment_name:I = 0x7f0e00d5

.field public static final audio:I = 0x7f0e0055

.field public static final audio_attachment_view:I = 0x7f0e0008

.field public static final audio_attachment_view_stub:I = 0x7f0e002e

.field public static final audio_error_msg:I = 0x7f0e000d

.field public static final audio_icon:I = 0x7f0e008c

.field public static final audio_image:I = 0x7f0e0009

.field public static final audio_name:I = 0x7f0e000a

.field public static final avatar:I = 0x7f0e0016

.field public static final body:I = 0x7f0e0022

.field public static final body_scroll:I = 0x7f0e0020

.field public static final body_text_view:I = 0x7f0e00ba

.field public static final bottom_panel:I = 0x7f0e0030

.field public static final bottom_text:I = 0x7f0e0091

.field public static final boxname:I = 0x7f0e006a

.field public static final btn_download_msg:I = 0x7f0e0085

.field public static final btn_no:I = 0x7f0e003e

.field public static final btn_yes:I = 0x7f0e003d

.field public static final buttonPanel:I = 0x7f0e003c

.field public static final button_and_counter:I = 0x7f0e00ac

.field public static final button_with_counter:I = 0x7f0e0033

.field public static final buttons_linear:I = 0x7f0e00a6

.field public static final by_card:I = 0x7f0e0072

.field public static final cancel:I = 0x7f0e003a

.field public static final cancel_button:I = 0x7f0e00ad

.field public static final cancel_select:I = 0x7f0e00f5

.field public static final cbmsg_list_item_recv:I = 0x7f0e0013

.field public static final changed_linear_layout:I = 0x7f0e0024

.field public static final checkbox:I = 0x7f0e00b0

.field public static final close_button:I = 0x7f0e0099

.field public static final contact_img:I = 0x7f0e009c

.field public static final content_linear:I = 0x7f0e009b

.field public static final content_scroll_view:I = 0x7f0e009a

.field public static final content_selector:I = 0x7f0e0007

.field public static final controler:I = 0x7f0e0058

.field public static final conversation_list:I = 0x7f0e00ec

.field public static final custom_title_root:I = 0x7f0e003f

.field public static final date:I = 0x7f0e0043

.field public static final date_view:I = 0x7f0e001a

.field public static final delete:I = 0x7f0e003b

.field public static final delete_btn:I = 0x7f0e00a8

.field public static final delete_locked:I = 0x7f0e004a

.field public static final delete_panel:I = 0x7f0e0037

.field public static final delivered_indicator:I = 0x7f0e001c

.field public static final delivery_date:I = 0x7f0e0051

.field public static final detail_content_linear:I = 0x7f0e009d

.field public static final details_indicator:I = 0x7f0e001d

.field public static final dismillall:I = 0x7f0e0080

.field public static final divider_1:I = 0x7f0e0039

.field public static final divider_2:I = 0x7f0e00c2

.field public static final done:I = 0x7f0e0062

.field public static final done_button:I = 0x7f0e005f

.field public static final drm_audio_lock:I = 0x7f0e0057

.field public static final drm_imagevideo_lock:I = 0x7f0e0056

.field public static final drm_lock:I = 0x7f0e000e

.field public static final duration_text:I = 0x7f0e00d7

.field public static final edit_slideshow_button:I = 0x7f0e00ce

.field public static final embedded_reply_text_editor:I = 0x7f0e00aa

.field public static final embedded_text_editor:I = 0x7f0e0031

.field public static final empty:I = 0x7f0e0048

.field public static final empty_message:I = 0x7f0e00c1

.field public static final error:I = 0x7f0e0044

.field public static final expiration_indicator:I = 0x7f0e00f4

.field public static final file_attachment_button_remove:I = 0x7f0e0068

.field public static final file_attachment_divider:I = 0x7f0e0067

.field public static final file_attachment_name_info:I = 0x7f0e0065

.field public static final file_attachment_size_info:I = 0x7f0e0066

.field public static final file_attachment_thumbnail:I = 0x7f0e0064

.field public static final file_attachment_view:I = 0x7f0e0063

.field public static final file_attachment_view_stub:I = 0x7f0e002b

.field public static final folderview_cancel_select:I = 0x7f0e00ff

.field public static final folderview_delete:I = 0x7f0e0100

.field public static final folderview_select_all:I = 0x7f0e00fe

.field public static final from:I = 0x7f0e0042

.field public static final has_file_attachment:I = 0x7f0e0074

.field public static final history:I = 0x7f0e0028

.field public static final icon:I = 0x7f0e004d

.field public static final icon_id:I = 0x7f0e006e

.field public static final image:I = 0x7f0e0054

.field public static final image_attachment_view:I = 0x7f0e0079

.field public static final image_attachment_view_stub:I = 0x7f0e002c

.field public static final image_content:I = 0x7f0e007a

.field public static final image_preview:I = 0x7f0e00d2

.field public static final image_view:I = 0x7f0e0088

.field public static final itemDivider:I = 0x7f0e0052

.field public static final item_list:I = 0x7f0e00af

.field public static final label:I = 0x7f0e0060

.field public static final label_downloading:I = 0x7f0e0086

.field public static final left_arrow:I = 0x7f0e0094

.field public static final left_text:I = 0x7f0e008e

.field public static final line:I = 0x7f0e00bf

.field public static final loading_text:I = 0x7f0e00f1

.field public static final locked_indicator:I = 0x7f0e001b

.field public static final mark_as_read_btn:I = 0x7f0e00a7

.field public static final media_size_info:I = 0x7f0e000f

.field public static final message:I = 0x7f0e0049

.field public static final message_block:I = 0x7f0e0017

.field public static final message_count:I = 0x7f0e006b

.field public static final message_delete:I = 0x7f0e0104

.field public static final message_forward:I = 0x7f0e0103

.field public static final messages:I = 0x7f0e00c0

.field public static final mms_download_controls:I = 0x7f0e0084

.field public static final mms_downloading_view_stub:I = 0x7f0e0019

.field public static final mms_file_attachment_view_stub:I = 0x7f0e0082

.field public static final mms_layout_view_parent:I = 0x7f0e0014

.field public static final mms_layout_view_stub:I = 0x7f0e001f

.field public static final mms_view:I = 0x7f0e0087

.field public static final msg_content:I = 0x7f0e009e

.field public static final msg_counter:I = 0x7f0e0095

.field public static final msg_date:I = 0x7f0e0071

.field public static final msg_dlg_image_view:I = 0x7f0e00a4

.field public static final msg_dlg_mms_view:I = 0x7f0e00a3

.field public static final msg_dlg_play_slideshow_button:I = 0x7f0e00a5

.field public static final msg_list_item_recv:I = 0x7f0e0081

.field public static final msg_list_item_send:I = 0x7f0e0083

.field public static final msg_number_bar_linear:I = 0x7f0e0093

.field public static final msg_number_bar_relative:I = 0x7f0e0092

.field public static final msg_recipent:I = 0x7f0e0070

.field public static final msg_recv_timer:I = 0x7f0e009f

.field public static final msg_subject:I = 0x7f0e0073

.field public static final msg_text:I = 0x7f0e0076

.field public static final name:I = 0x7f0e00b2

.field public static final new_quick_text:I = 0x7f0e00de

.field public static final next_slide_button:I = 0x7f0e005a

.field public static final number:I = 0x7f0e006f

.field public static final number_picker:I = 0x7f0e00b4

.field public static final page_index:I = 0x7f0e008b

.field public static final play_audio_button:I = 0x7f0e0010

.field public static final play_slideshow_button:I = 0x7f0e0089

.field public static final playing_audio:I = 0x7f0e00b5

.field public static final pre_slide_button:I = 0x7f0e0059

.field public static final presence:I = 0x7f0e0041

.field public static final preview_button:I = 0x7f0e005b

.field public static final quickText_add_button:I = 0x7f0e00df

.field public static final quick_text:I = 0x7f0e00b6

.field public static final quick_text_list:I = 0x7f0e00dc

.field public static final recepient_bar_relative:I = 0x7f0e0097

.field public static final recepient_name:I = 0x7f0e0098

.field public static final recipient:I = 0x7f0e004f

.field public static final recipients_editor:I = 0x7f0e00b7

.field public static final recipients_editor_stub:I = 0x7f0e0026

.field public static final recipients_picker:I = 0x7f0e00b8

.field public static final recipients_subject_linear:I = 0x7f0e0025

.field public static final remove_audio_button:I = 0x7f0e0012

.field public static final remove_image_button:I = 0x7f0e007c

.field public static final remove_slide_button:I = 0x7f0e005d

.field public static final remove_slideshow_button:I = 0x7f0e00d0

.field public static final remove_video_button:I = 0x7f0e00e6

.field public static final replace_audio_button:I = 0x7f0e0011

.field public static final replace_image_button:I = 0x7f0e005c

.field public static final replace_video_button:I = 0x7f0e00e5

.field public static final reply_linear:I = 0x7f0e00a9

.field public static final reply_send_button:I = 0x7f0e00ab

.field public static final right_arrow:I = 0x7f0e0096

.field public static final right_text:I = 0x7f0e0090

.field public static final save:I = 0x7f0e0101

.field public static final search:I = 0x7f0e00f7

.field public static final select_all:I = 0x7f0e0038

.field public static final select_check_box:I = 0x7f0e0015

.field public static final select_items:I = 0x7f0e00ae

.field public static final selected_conv_count:I = 0x7f0e0047

.field public static final selectedid:I = 0x7f0e00be

.field public static final send_button:I = 0x7f0e0032

.field public static final send_button_mms:I = 0x7f0e0035

.field public static final send_button_sms:I = 0x7f0e0036

.field public static final send_slideshow_button:I = 0x7f0e00cf

.field public static final shownext:I = 0x7f0e007f

.field public static final sim3g:I = 0x7f0e0003

.field public static final simCardPicid:I = 0x7f0e00bc

.field public static final simCardTexid:I = 0x7f0e00bd

.field public static final simIcon:I = 0x7f0e0001

.field public static final simName:I = 0x7f0e0005

.field public static final simNumber:I = 0x7f0e0006

.field public static final simNumberShort:I = 0x7f0e0004

.field public static final simStatus:I = 0x7f0e0002

.field public static final sim_icon:I = 0x7f0e00c4

.field public static final sim_info_layout:I = 0x7f0e0000

.field public static final sim_info_linear:I = 0x7f0e00a0

.field public static final sim_layout:I = 0x7f0e00c3

.field public static final sim_name:I = 0x7f0e00a2

.field public static final sim_number:I = 0x7f0e00c7

.field public static final sim_number_short:I = 0x7f0e00c5

.field public static final sim_status:I = 0x7f0e001e

.field public static final sim_suggested:I = 0x7f0e00c8

.field public static final sim_via_text:I = 0x7f0e00a1

.field public static final siminfolist:I = 0x7f0e00c9

.field public static final size:I = 0x7f0e00b3

.field public static final slide_editor_view:I = 0x7f0e0053

.field public static final slide_item_list:I = 0x7f0e008a

.field public static final slide_number_text:I = 0x7f0e00d3

.field public static final slide_view:I = 0x7f0e00ca

.field public static final slidegroup:I = 0x7f0e0102

.field public static final slideshow_attachment_view:I = 0x7f0e00cb

.field public static final slideshow_attachment_view_portrait:I = 0x7f0e00d1

.field public static final slideshow_attachment_view_stub:I = 0x7f0e002f

.field public static final slideshow_image:I = 0x7f0e00cc

.field public static final slideshow_text:I = 0x7f0e00cd

.field public static final smiley_icon:I = 0x7f0e00d8

.field public static final smiley_name:I = 0x7f0e00d9

.field public static final smiley_text:I = 0x7f0e00da

.field public static final sms_viewer_title:I = 0x7f0e0077

.field public static final spinner_line_2:I = 0x7f0e006d

.field public static final status:I = 0x7f0e0050

.field public static final status_bar_latest_event_content:I = 0x7f0e00e0

.field public static final status_bar_latest_event_content_large_icon:I = 0x7f0e00e1

.field public static final subject:I = 0x7f0e0027

.field public static final subtitle:I = 0x7f0e00bb

.field public static final text:I = 0x7f0e0061

.field public static final text1:I = 0x7f0e0078

.field public static final text_counter:I = 0x7f0e0034

.field public static final text_layout:I = 0x7f0e00c6

.field public static final text_message:I = 0x7f0e005e

.field public static final text_preview:I = 0x7f0e00d4

.field public static final text_view:I = 0x7f0e0018

.field public static final thumbnail:I = 0x7f0e00b1

.field public static final title:I = 0x7f0e0046

.field public static final titleDivider:I = 0x7f0e004e

.field public static final title_template:I = 0x7f0e004c

.field public static final title_text_view:I = 0x7f0e00b9

.field public static final topPanel:I = 0x7f0e004b

.field public static final top_text:I = 0x7f0e008d

.field public static final unread_conv_count:I = 0x7f0e0040

.field public static final video:I = 0x7f0e008f

.field public static final video_attachment_view:I = 0x7f0e00e2

.field public static final video_attachment_view_stub:I = 0x7f0e002d

.field public static final video_thumbnail:I = 0x7f0e00e3

.field public static final viewFlipper:I = 0x7f0e007d

.field public static final view_image_button:I = 0x7f0e007b

.field public static final view_parent:I = 0x7f0e0021

.field public static final view_video_button:I = 0x7f0e00e4

.field public static final widget_compose:I = 0x7f0e00eb

.field public static final widget_conversation:I = 0x7f0e00ed

.field public static final widget_header:I = 0x7f0e00e7

.field public static final widget_icon:I = 0x7f0e00e8

.field public static final widget_label:I = 0x7f0e00e9

.field public static final widget_loading:I = 0x7f0e00f0

.field public static final widget_read_background:I = 0x7f0e00ef

.field public static final widget_unread_background:I = 0x7f0e00ee

.field public static final widget_unread_count:I = 0x7f0e00ea

.field public static final wpms_layout_view_parent:I = 0x7f0e00f3

.field public static final wpms_list_item:I = 0x7f0e00f2

.field public static final wpms_timestamp:I = 0x7f0e0023


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
