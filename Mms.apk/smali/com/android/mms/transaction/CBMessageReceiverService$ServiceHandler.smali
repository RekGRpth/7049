.class final Lcom/android/mms/transaction/CBMessageReceiverService$ServiceHandler;
.super Landroid/os/Handler;
.source "CBMessageReceiverService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/transaction/CBMessageReceiverService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ServiceHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/transaction/CBMessageReceiverService;


# direct methods
.method public constructor <init>(Lcom/android/mms/transaction/CBMessageReceiverService;Landroid/os/Looper;)V
    .locals 0
    .param p2    # Landroid/os/Looper;

    iput-object p1, p0, Lcom/android/mms/transaction/CBMessageReceiverService$ServiceHandler;->this$0:Lcom/android/mms/transaction/CBMessageReceiverService;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1    # Landroid/os/Message;

    iget v2, p1, Landroid/os/Message;->arg1:I

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/content/Intent;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v3, "android.provider.Telephony.CB_SMS_RECEIVED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "android.provider.Telephony.SMS_CB_RECEIVED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "android.provider.Telephony.SMS_EMERGENCY_CB_RECEIVED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    iget-object v3, p0, Lcom/android/mms/transaction/CBMessageReceiverService$ServiceHandler;->this$0:Lcom/android/mms/transaction/CBMessageReceiverService;

    invoke-static {v3, v1}, Lcom/android/mms/transaction/CBMessageReceiverService;->access$000(Lcom/android/mms/transaction/CBMessageReceiverService;Landroid/content/Intent;)V

    :cond_1
    iget-object v3, p0, Lcom/android/mms/transaction/CBMessageReceiverService$ServiceHandler;->this$0:Lcom/android/mms/transaction/CBMessageReceiverService;

    invoke-static {v3, v2}, Lcom/android/mms/transaction/CBMessageReceiver;->finishStartingService(Landroid/app/Service;I)V

    return-void
.end method
