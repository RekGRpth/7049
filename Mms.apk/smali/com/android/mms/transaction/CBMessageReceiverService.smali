.class public Lcom/android/mms/transaction/CBMessageReceiverService;
.super Landroid/app/Service;
.source "CBMessageReceiverService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mms/transaction/CBMessageReceiverService$ServiceHandler;
    }
.end annotation


# static fields
.field public static final CLASS_ZERO_BODY_KEY:Ljava/lang/String; = "CLASS_ZERO_BODY"

.field private static final DEFAULT_SIM_ID:I = 0x1

.field private static final MESSAGE_URI:Landroid/net/Uri;

.field private static final REPLACE_COLUMN_ID:I = 0x0

.field private static final TAG:Ljava/lang/String; = "CBMessageReceiverService"


# instance fields
.field private mResultCode:I

.field private mServiceHandler:Lcom/android/mms/transaction/CBMessageReceiverService$ServiceHandler;

.field private mServiceLooper:Landroid/os/Looper;

.field public mToastHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Landroid/provider/Telephony$CbSms;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/android/mms/transaction/CBMessageReceiverService;->MESSAGE_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Lcom/android/mms/transaction/CBMessageReceiverService$1;

    invoke-direct {v0, p0}, Lcom/android/mms/transaction/CBMessageReceiverService$1;-><init>(Lcom/android/mms/transaction/CBMessageReceiverService;)V

    iput-object v0, p0, Lcom/android/mms/transaction/CBMessageReceiverService;->mToastHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/android/mms/transaction/CBMessageReceiverService;Landroid/content/Intent;)V
    .locals 0
    .param p0    # Lcom/android/mms/transaction/CBMessageReceiverService;
    .param p1    # Landroid/content/Intent;

    invoke-direct {p0, p1}, Lcom/android/mms/transaction/CBMessageReceiverService;->handleCBMessageReceived(Landroid/content/Intent;)V

    return-void
.end method

.method private getCBContentValue(Landroid/telephony/CbSmsMessage;Ljava/lang/String;)Landroid/content/ContentValues;
    .locals 6
    .param p1    # Landroid/telephony/CbSmsMessage;
    .param p2    # Ljava/lang/String;

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    invoke-virtual {p1}, Landroid/telephony/CbSmsMessage;->getMessageSimId()I

    move-result v1

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v1}, Landroid/provider/Telephony$SIMInfo;->getSIMInfoBySlot(Landroid/content/Context;I)Landroid/provider/Telephony$SIMInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v3, "sim_id"

    iget-wide v4, v0, Landroid/provider/Telephony$SIMInfo;->mSimId:J

    long-to-int v4, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :goto_0
    const-string v3, "date"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v3, "channel_id"

    invoke-virtual {p1}, Landroid/telephony/CbSmsMessage;->getMessageID()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "read"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "body"

    invoke-virtual {v2, v3, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2

    :cond_0
    const-string v3, "Mms/Txn"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getCBContentValue:SIMInfo is null for slot "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private handleCBMessageReceived(Landroid/content/Intent;)V
    .locals 10
    .param p1    # Landroid/content/Intent;

    const/4 v9, 0x0

    invoke-static {p1}, Landroid/provider/Telephony$CbSms$Intents;->getMessagesFromIntent(Landroid/content/Intent;)[Landroid/telephony/CbSmsMessage;

    move-result-object v2

    if-nez v2, :cond_1

    const-string v6, "Mms/Txn"

    const-string v7, "Intents.getMessagesFromIntent return null !!"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p0, v2}, Lcom/android/mms/transaction/CBMessageReceiverService;->insertMessage(Landroid/content/Context;[Landroid/telephony/CbSmsMessage;)Landroid/net/Uri;

    move-result-object v0

    const-string v6, "Mms:transaction"

    const/4 v7, 0x2

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_2

    aget-object v5, v2, v9

    const-string v6, "CBMessageReceiverService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "handleSmsReceived messageUri: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", body: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5}, Landroid/telephony/CbSmsMessage;->getMessageBody()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    if-eqz v0, :cond_0

    aget-object v1, v2, v9

    invoke-virtual {v1}, Landroid/telephony/CbSmsMessage;->getMessageSimId()I

    move-result v4

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v4}, Landroid/provider/Telephony$SIMInfo;->getSIMInfoBySlot(Landroid/content/Context;I)Landroid/provider/Telephony$SIMInfo;

    move-result-object v3

    if-eqz v3, :cond_3

    const/4 v6, 0x1

    iget-wide v7, v3, Landroid/provider/Telephony$SIMInfo;->mSimId:J

    long-to-int v7, v7

    invoke-static {p0, v6, v7}, Lcom/android/mms/transaction/CBMessagingNotification;->updateNewMessageIndicatorGemini(Landroid/content/Context;ZI)V

    goto :goto_0

    :cond_3
    const-string v6, "Mms/Txn"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "handleCBMessageReceived:SIMInfo is null for slot "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private insertMessage(Landroid/content/Context;[Landroid/telephony/CbSmsMessage;)Landroid/net/Uri;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # [Landroid/telephony/CbSmsMessage;

    invoke-direct {p0, p1, p2}, Lcom/android/mms/transaction/CBMessageReceiverService;->storeCBMessage(Landroid/content/Context;[Landroid/telephony/CbSmsMessage;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private storeCBMessage(Landroid/content/Context;[Landroid/telephony/CbSmsMessage;)Landroid/net/Uri;
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # [Landroid/telephony/CbSmsMessage;

    const/4 v6, 0x0

    aget-object v4, p2, v6

    array-length v2, p2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v6, 0x1

    if-ne v2, v6, :cond_1

    invoke-virtual {v4}, Landroid/telephony/CbSmsMessage;->getMessageBody()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v4, v6}, Lcom/android/mms/transaction/CBMessageReceiverService;->getCBContentValue(Landroid/telephony/CbSmsMessage;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v5

    const-string v6, "Mms/Txn"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "CB message body: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v6, Lcom/android/mms/transaction/CBMessageReceiverService;->MESSAGE_URI:Landroid/net/Uri;

    invoke-virtual {v3, v6, v5}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v6

    return-object v6

    :cond_1
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v4, p2, v1

    invoke-virtual {v4}, Landroid/telephony/CbSmsMessage;->getMessageBody()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "CBMessageReceiverService"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mms/transaction/CBMessageReceiverService;->mServiceLooper:Landroid/os/Looper;

    iget-object v1, p0, Lcom/android/mms/transaction/CBMessageReceiverService;->mServiceLooper:Landroid/os/Looper;

    if-eqz v1, :cond_0

    new-instance v1, Lcom/android/mms/transaction/CBMessageReceiverService$ServiceHandler;

    iget-object v2, p0, Lcom/android/mms/transaction/CBMessageReceiverService;->mServiceLooper:Landroid/os/Looper;

    invoke-direct {v1, p0, v2}, Lcom/android/mms/transaction/CBMessageReceiverService$ServiceHandler;-><init>(Lcom/android/mms/transaction/CBMessageReceiverService;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/android/mms/transaction/CBMessageReceiverService;->mServiceHandler:Lcom/android/mms/transaction/CBMessageReceiverService$ServiceHandler;

    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/android/mms/transaction/CBMessageReceiverService;->mServiceLooper:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    const-string v2, "result"

    invoke-virtual {p1, v2, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    :cond_0
    iput v1, p0, Lcom/android/mms/transaction/CBMessageReceiverService;->mResultCode:I

    iget-object v1, p0, Lcom/android/mms/transaction/CBMessageReceiverService;->mServiceHandler:Lcom/android/mms/transaction/CBMessageReceiverService$ServiceHandler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput p3, v0, Landroid/os/Message;->arg1:I

    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v1, p0, Lcom/android/mms/transaction/CBMessageReceiverService;->mServiceHandler:Lcom/android/mms/transaction/CBMessageReceiverService$ServiceHandler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const/4 v1, 0x2

    return v1
.end method
