.class final Lcom/android/mms/transaction/WapPushMessagingNotification$1;
.super Ljava/lang/Object;
.source "WapPushMessagingNotification.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/transaction/WapPushMessagingNotification;->nonBlockingUpdateNewMessageIndicator(Landroid/content/Context;J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$newMsgThreadId:J


# direct methods
.method constructor <init>(Landroid/content/Context;J)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/transaction/WapPushMessagingNotification$1;->val$context:Landroid/content/Context;

    iput-wide p2, p0, Lcom/android/mms/transaction/WapPushMessagingNotification$1;->val$newMsgThreadId:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/android/mms/transaction/WapPushMessagingNotification$1;->val$context:Landroid/content/Context;

    iget-wide v1, p0, Lcom/android/mms/transaction/WapPushMessagingNotification$1;->val$newMsgThreadId:J

    invoke-static {v0, v1, v2}, Lcom/android/mms/transaction/WapPushMessagingNotification;->blockingUpdateNewMessageIndicator(Landroid/content/Context;J)V

    return-void
.end method
