.class final Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;
.super Ljava/lang/Object;
.source "WapPushMessagingNotification.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/transaction/WapPushMessagingNotification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "WapPushNotificationInfo"
.end annotation


# instance fields
.field public final mClickIntent:Landroid/content/Intent;

.field public final mMessage:Ljava/lang/String;

.field public final mSender:Lcom/android/mms/data/Contact;

.field public final mThreadId:J

.field public final mTicker:Ljava/lang/CharSequence;

.field public final mTimeMillis:J

.field public final mTitle:Ljava/lang/String;

.field public mUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/CharSequence;JLjava/lang/String;Lcom/android/mms/data/Contact;JLandroid/net/Uri;)V
    .locals 0
    .param p1    # Landroid/content/Intent;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/CharSequence;
    .param p4    # J
    .param p6    # Ljava/lang/String;
    .param p7    # Lcom/android/mms/data/Contact;
    .param p8    # J
    .param p10    # Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;->mClickIntent:Landroid/content/Intent;

    iput-object p2, p0, Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;->mMessage:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;->mTicker:Ljava/lang/CharSequence;

    iput-wide p4, p0, Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;->mTimeMillis:J

    iput-object p6, p0, Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;->mTitle:Ljava/lang/String;

    iput-object p7, p0, Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;->mSender:Lcom/android/mms/data/Contact;

    iput-wide p8, p0, Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;->mThreadId:J

    iput-object p10, p0, Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;->mUri:Landroid/net/Uri;

    return-void
.end method


# virtual methods
.method public formatBigMessage(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 5
    .param p1    # Landroid/content/Context;

    iget-object v2, p0, Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;->mMessage:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;->mMessage:Ljava/lang/String;

    const-string v3, "\\n\\s+"

    const-string v4, "\n"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    iget-object v2, p0, Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;->mMessage:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_0

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    :cond_0
    iget-object v2, p0, Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;->mMessage:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_1
    return-object v1

    :cond_2
    const-string v0, ""

    goto :goto_0
.end method

.method public formatInboxMessage(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 11
    .param p1    # Landroid/content/Context;

    const/4 v10, 0x0

    new-instance v1, Landroid/text/style/TextAppearanceSpan;

    const v7, 0x7f0c0006

    invoke-direct {v1, p1, v7}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    new-instance v2, Landroid/text/style/TextAppearanceSpan;

    const v7, 0x7f0c0008

    invoke-direct {v2, p1, v7}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iget-object v7, p0, Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;->mMessage:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    iget-object v7, p0, Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;->mMessage:Ljava/lang/String;

    const-string v8, "\\n\\s+"

    const-string v9, "\n"

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-instance v5, Landroid/text/SpannableStringBuilder;

    invoke-direct {v5}, Landroid/text/SpannableStringBuilder;-><init>()V

    iget-object v7, p0, Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;->mSender:Lcom/android/mms/data/Contact;

    invoke-virtual {v7}, Lcom/android/mms/data/Contact;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    invoke-virtual {v5, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v5, v1, v10, v7, v10}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_0
    const v7, 0x7f0a012b

    invoke-virtual {p1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_2

    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    if-lez v7, :cond_1

    invoke-virtual {v5, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_1
    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v6

    invoke-virtual {v5, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v7, v6

    invoke-virtual {v5, v2, v6, v7, v10}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_2
    return-object v5

    :cond_3
    const-string v0, ""

    goto :goto_0
.end method

.method public getTime()J
    .locals 2

    iget-wide v0, p0, Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;->mTimeMillis:J

    return-wide v0
.end method
