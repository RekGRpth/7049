.class public Lcom/android/mms/transaction/WapPushMessagingNotification;
.super Ljava/lang/Object;
.source "WapPushMessagingNotification.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfoComparator;,
        Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;
    }
.end annotation


# static fields
.field private static final COLUMN_DATE:I = 0x1

.field private static final COLUMN_THREAD_ID:I = 0x0

.field private static final COLUMN_WAPPUSH_ADDRESS:I = 0x5

.field private static final COLUMN_WAPPUSH_ID:I = 0x2

.field private static final COLUMN_WAPPUSH_TEXT:I = 0x4

.field private static final COLUMN_WAPPUSH_URL:I = 0x3

.field private static final INFO_COMPARATOR:Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfoComparator;

.field private static final IN_CONVERSATION_NOTIFICATION_VOLUME:F = 0.25f

.field private static final MAX_MESSAGES_TO_SHOW:I = 0x8

.field private static final NEW_INCOMING_SM_CONSTRAINT:Ljava/lang/String; = "(seen = 0)"

.field private static final NOTIFICATION_ID:I = 0x7f

.field public static final SL_AUTOLAUNCH_NOTIFICATION_ID:I = 0x15c9

.field private static final TAG:Ljava/lang/String; = "Mms/WapPush"

.field public static final THREAD_ALL:J = -0x1L

.field public static final THREAD_NONE:J = -0x2L

.field private static final URL_MESSAGES:Landroid/net/Uri;

.field private static final WAPPUSH_STATUS_PROJECTION:[Ljava/lang/String;

.field private static final WAPPUSH_THREAD_ID_PROJECTION:[Ljava/lang/String;

.field private static sCurrentlyDisplayedThreadId:J

.field private static final sCurrentlyDisplayedThreadLock:Ljava/lang/Object;

.field private static sNotificationSet:Ljava/util/SortedSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/SortedSet",
            "<",
            "Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    sget-object v0, Landroid/provider/Telephony$WapPush;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/android/mms/transaction/WapPushMessagingNotification;->URL_MESSAGES:Landroid/net/Uri;

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "thread_id"

    aput-object v1, v0, v3

    const-string v1, "date"

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "url"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "text"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "address"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/mms/transaction/WapPushMessagingNotification;->WAPPUSH_STATUS_PROJECTION:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "thread_id"

    aput-object v1, v0, v3

    sput-object v0, Lcom/android/mms/transaction/WapPushMessagingNotification;->WAPPUSH_THREAD_ID_PROJECTION:[Ljava/lang/String;

    new-instance v0, Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfoComparator;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfoComparator;-><init>(Lcom/android/mms/transaction/WapPushMessagingNotification$1;)V

    sput-object v0, Lcom/android/mms/transaction/WapPushMessagingNotification;->INFO_COMPARATOR:Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfoComparator;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/mms/transaction/WapPushMessagingNotification;->sCurrentlyDisplayedThreadLock:Ljava/lang/Object;

    new-instance v0, Ljava/util/TreeSet;

    sget-object v1, Lcom/android/mms/transaction/WapPushMessagingNotification;->INFO_COMPARATOR:Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfoComparator;

    invoke-direct {v0, v1}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    sput-object v0, Lcom/android/mms/transaction/WapPushMessagingNotification;->sNotificationSet:Ljava/util/SortedSet;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static final addWapPushNotificationInfos(Landroid/content/Context;Ljava/util/Set;)V
    .locals 20
    .param p0    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/android/mms/transaction/WapPushMessagingNotification;->URL_MESSAGES:Landroid/net/Uri;

    sget-object v4, Lcom/android/mms/transaction/WapPushMessagingNotification;->WAPPUSH_STATUS_PROJECTION:[Ljava/lang/String;

    const-string v5, "(seen = 0)"

    const/4 v6, 0x0

    const-string v7, "date desc"

    move-object/from16 v1, p0

    invoke-static/range {v1 .. v7}, Lcom/google/android/mms/util/SqliteWrapper;->query(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    if-nez v12, :cond_0

    :goto_0
    return-void

    :cond_0
    :goto_1
    :try_start_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x2

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    const/4 v1, 0x3

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    const/4 v1, 0x4

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    const/4 v1, 0x5

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v1, 0x0

    invoke-static {v4, v1}, Lcom/android/mms/data/Contact;->get(Ljava/lang/String;Z)Lcom/android/mms/data/Contact;

    move-result-object v10

    const/4 v1, 0x0

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    const/4 v1, 0x1

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    const-string v5, ""

    if-eqz v16, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    :cond_1
    if-eqz v17, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    :cond_2
    sget-object v1, Lcom/android/mms/transaction/WapPushMessagingNotification;->URL_MESSAGES:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-static {v14, v15}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v11

    move-object/from16 v3, p0

    invoke-static/range {v3 .. v11}, Lcom/android/mms/transaction/WapPushMessagingNotification;->getNewMessageNotificationInfo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JJLcom/android/mms/data/Contact;Landroid/net/Uri;)Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;

    move-result-object v13

    if-eqz v16, :cond_3

    const-string v1, ""

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    iget-object v1, v13, Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;->mClickIntent:Landroid/content/Intent;

    const-string v3, "URL"

    move-object/from16 v0, v17

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_4
    sget-object v1, Lcom/android/mms/transaction/WapPushMessagingNotification;->sNotificationSet:Ljava/util/SortedSet;

    invoke-interface {v1, v13}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x0

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_1

    :catchall_0
    move-exception v1

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    throw v1

    :cond_5
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0
.end method

.method public static blockingUpdateNewMessageIndicator(Landroid/content/Context;J)V
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # J

    sget-object v3, Lcom/android/mms/transaction/WapPushMessagingNotification;->sCurrentlyDisplayedThreadLock:Ljava/lang/Object;

    monitor-enter v3

    const-wide/16 v4, 0x0

    cmp-long v2, p1, v4

    if-lez v2, :cond_1

    :try_start_0
    sget-wide v4, Lcom/android/mms/transaction/WapPushMessagingNotification;->sCurrentlyDisplayedThreadId:J

    cmp-long v2, p1, v4

    if-nez v2, :cond_1

    invoke-static {p0}, Lcom/android/mms/transaction/WapPushMessagingNotification;->playInConversationNotificationSound(Landroid/content/Context;)V

    monitor-exit v3

    :cond_0
    :goto_0
    return-void

    :cond_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-object v2, Lcom/android/mms/transaction/WapPushMessagingNotification;->sNotificationSet:Ljava/util/SortedSet;

    invoke-interface {v2}, Ljava/util/SortedSet;->clear()V

    new-instance v1, Ljava/util/HashSet;

    const/4 v2, 0x4

    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(I)V

    const/4 v0, 0x0

    invoke-static {p0, v1}, Lcom/android/mms/transaction/WapPushMessagingNotification;->addWapPushNotificationInfos(Landroid/content/Context;Ljava/util/Set;)V

    const/16 v2, 0x7f

    invoke-static {p0, v2}, Lcom/android/mms/transaction/WapPushMessagingNotification;->cancelNotification(Landroid/content/Context;I)V

    sget-object v2, Lcom/android/mms/transaction/WapPushMessagingNotification;->sNotificationSet:Ljava/util/SortedSet;

    invoke-interface {v2}, Ljava/util/SortedSet;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "Mms/WapPush"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "blockingUpdateNewMessageIndicator: count="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", newMsgThreadId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v2, -0x2

    cmp-long v2, p1, v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :goto_1
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v3

    invoke-static {p0, v2, v3}, Lcom/android/mms/transaction/WapPushMessagingNotification;->updateNotification(Landroid/content/Context;ZI)V

    goto :goto_0

    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method protected static buildTickerMessage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 9
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const/4 v8, 0x1

    const/16 v7, 0xd

    const/16 v6, 0xa

    const/16 v5, 0x20

    invoke-static {p1, v8}, Lcom/android/mms/data/Contact;->get(Ljava/lang/String;Z)Lcom/android/mms/data/Contact;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/mms/data/Contact;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    if-nez v1, :cond_2

    const-string v4, ""

    :goto_0
    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v4, 0x3a

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {p2, v6, v5}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v7, v5}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_0
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {p3, v6, v5}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v7, v5}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    new-instance v3, Landroid/text/SpannableString;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    new-instance v4, Landroid/text/style/StyleSpan;

    invoke-direct {v4, v8}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/4 v5, 0x0

    const/16 v6, 0x21

    invoke-virtual {v3, v4, v5, v2, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    return-object v3

    :cond_2
    invoke-virtual {v1, v6, v5}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v7, v5}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public static cancelNotification(Landroid/content/Context;I)V
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # I

    const-string v1, "notification"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-virtual {v0, p1}, Landroid/app/NotificationManager;->cancel(I)V

    return-void
.end method

.method private static formatSenders(Landroid/content/Context;Ljava/util/ArrayList;)Ljava/lang/CharSequence;
    .locals 7
    .param p0    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;",
            ">;)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    const/4 v6, 0x0

    new-instance v2, Landroid/text/style/TextAppearanceSpan;

    const v5, 0x7f0c0006

    invoke-direct {v2, p0, v5}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    const v5, 0x7f0a012c

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Landroid/text/SpannableStringBuilder;

    invoke-direct {v4}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    if-lez v0, :cond_0

    invoke-virtual {v4, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_0
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;

    iget-object v5, v5, Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;->mSender:Lcom/android/mms/data/Contact;

    invoke-virtual {v5}, Lcom/android/mms/data/Contact;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v5

    invoke-virtual {v4, v2, v6, v5, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    return-object v4
.end method

.method private static getNewMessageNotificationInfo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JJLcom/android/mms/data/Contact;Landroid/net/Uri;)Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;
    .locals 14
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # J
    .param p5    # J
    .param p7    # Lcom/android/mms/data/Contact;
    .param p8    # Landroid/net/Uri;

    move-wide/from16 v0, p3

    invoke-static {p0, v0, v1}, Lcom/android/mms/ui/WPMessageActivity;->createIntent(Landroid/content/Context;J)Landroid/content/Intent;

    move-result-object v3

    const/high16 v2, 0x34000000

    invoke-virtual {v3, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-static {p0, p1, v2, v4}, Lcom/android/mms/transaction/WapPushMessagingNotification;->buildTickerMessage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v13

    const/4 v2, 0x0

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    invoke-virtual {v13, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    const/4 v2, 0x0

    move-object/from16 v0, p2

    invoke-static {p0, p1, v2, v0}, Lcom/android/mms/transaction/WapPushMessagingNotification;->buildTickerMessage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v5

    new-instance v2, Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;

    move-object/from16 v4, p2

    move-wide/from16 v6, p5

    move-object/from16 v9, p7

    move-wide/from16 v10, p3

    move-object/from16 v12, p8

    invoke-direct/range {v2 .. v12}, Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;-><init>(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/CharSequence;JLjava/lang/String;Lcom/android/mms/data/Contact;JLandroid/net/Uri;)V

    return-object v2
.end method

.method public static getWapPushThreadId(Landroid/content/Context;Landroid/net/Uri;)J
    .locals 10
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/net/Uri;

    const-wide/16 v8, -0x2

    const/4 v4, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v3, Lcom/android/mms/transaction/WapPushMessagingNotification;->WAPPUSH_THREAD_ID_PROJECTION:[Ljava/lang/String;

    move-object v0, p0

    move-object v2, p1

    move-object v5, v4

    move-object v6, v4

    invoke-static/range {v0 .. v6}, Lcom/google/android/mms/util/SqliteWrapper;->query(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    if-nez v7, :cond_0

    move-wide v0, v8

    :goto_0
    return-wide v0

    :cond_0
    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "thread_id"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    move-wide v0, v8

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static nonBlockingUpdateNewMessageIndicator(Landroid/content/Context;J)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # J

    const-string v0, "Mms/WapPush"

    const-string v1, "nonBlockingUpdateNewMessageIndicator"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/android/mms/transaction/WapPushMessagingNotification$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/mms/transaction/WapPushMessagingNotification$1;-><init>(Landroid/content/Context;J)V

    const-string v2, "WapPushMessagingNotification.nonBlockingUpdateNewMessageIndicator"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public static notifySlAutoLanuchMessage(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 10
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v6, 0x1

    const-string v7, "Mms/WapPush"

    const-string v8, "notifySlAutoLanuchMessage"

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v5

    new-instance v2, Landroid/app/Notification;

    invoke-direct {v2}, Landroid/app/Notification;-><init>()V

    const-string v7, "pref_key_enable_notifications"

    invoke-interface {v5, v7, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    const-string v7, "Mms/WapPush"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "notifySlAutoLanuchMessage, enabled = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v0, :cond_0

    const/4 v6, 0x0

    :goto_0
    return v6

    :cond_0
    const-string v7, "pref_key_ringtone"

    invoke-interface {v5, v7, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    :goto_1
    invoke-static {p0, v2, v3}, Lcom/android/mms/transaction/MessagingNotification;->processNotificationSound(Landroid/content/Context;Landroid/app/Notification;Landroid/net/Uri;)V

    iput-object p1, v2, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    iget v7, v2, Landroid/app/Notification;->flags:I

    or-int/lit8 v7, v7, 0x1

    iput v7, v2, Landroid/app/Notification;->flags:I

    const v7, -0xff0100

    iput v7, v2, Landroid/app/Notification;->ledARGB:I

    const/16 v7, 0x1f4

    iput v7, v2, Landroid/app/Notification;->ledOnMS:I

    const/16 v7, 0x7d0

    iput v7, v2, Landroid/app/Notification;->ledOffMS:I

    const-string v7, "notification"

    invoke-virtual {p0, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    const/16 v7, 0x15c9

    invoke-virtual {v1, v7, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0

    :cond_1
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    goto :goto_1
.end method

.method private static playInConversationNotificationSound(Landroid/content/Context;)V
    .locals 8
    .param p0    # Landroid/content/Context;

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v7

    const-string v1, "pref_key_ringtone"

    const/4 v3, 0x0

    invoke-interface {v7, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    new-instance v0, Lcom/android/mms/transaction/NotificationPlayer;

    const-string v1, "Mms/WapPush"

    invoke-direct {v0, v1}, Lcom/android/mms/transaction/NotificationPlayer;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x5

    const/high16 v5, 0x3e800000

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/android/mms/transaction/NotificationPlayer;->play(Landroid/content/Context;Landroid/net/Uri;ZIF)V

    goto :goto_0
.end method

.method public static setCurrentlyDisplayedThreadId(J)V
    .locals 2
    .param p0    # J

    sget-object v1, Lcom/android/mms/transaction/WapPushMessagingNotification;->sCurrentlyDisplayedThreadLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sput-wide p0, Lcom/android/mms/transaction/WapPushMessagingNotification;->sCurrentlyDisplayedThreadId:J

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static updateNotification(Landroid/content/Context;ZI)V
    .locals 42
    .param p0    # Landroid/content/Context;
    .param p1    # Z
    .param p2    # I

    invoke-static/range {p0 .. p0}, Lcom/android/mms/ui/MessagingPreferenceActivity;->getNotificationEnabled(Landroid/content/Context;)Z

    move-result v36

    if-nez v36, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v36, Lcom/android/mms/transaction/WapPushMessagingNotification;->sNotificationSet:Ljava/util/SortedSet;

    invoke-interface/range {v36 .. v36}, Ljava/util/SortedSet;->size()I

    move-result v19

    if-nez v19, :cond_2

    const-string v36, "Mms/WapPush"

    const-string v37, "updateNotification.messageCount is 0."

    invoke-static/range {v36 .. v37}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    sget-object v36, Lcom/android/mms/transaction/WapPushMessagingNotification;->sNotificationSet:Ljava/util/SortedSet;

    invoke-interface/range {v36 .. v36}, Ljava/util/SortedSet;->first()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;

    new-instance v36, Landroid/app/Notification$Builder;

    move-object/from16 v0, v36

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v21

    iget-wide v0, v0, Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;->mTimeMillis:J

    move-wide/from16 v37, v0

    invoke-virtual/range {v36 .. v38}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v23

    if-eqz p1, :cond_3

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;->mTicker:Ljava/lang/CharSequence;

    move-object/from16 v36, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    :cond_3
    invoke-static/range {p0 .. p0}, Landroid/app/TaskStackBuilder;->create(Landroid/content/Context;)Landroid/app/TaskStackBuilder;

    move-result-object v32

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v29

    const/16 v33, 0x0

    const/4 v5, 0x0

    const/16 v36, 0x1

    move/from16 v0, p2

    move/from16 v1, v36

    if-le v0, v1, :cond_6

    new-instance v17, Landroid/content/Intent;

    const-string v36, "android.intent.action.MAIN"

    move-object/from16 v0, v17

    move-object/from16 v1, v36

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v36, 0x34000000

    move-object/from16 v0, v17

    move/from16 v1, v36

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v36, "vnd.android-dir/mms-sms"

    move-object/from16 v0, v17

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    move-object/from16 v0, v32

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Landroid/app/TaskStackBuilder;

    const v36, 0x7f0a0249

    const/16 v37, 0x1

    move/from16 v0, v37

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v37, v0

    const/16 v38, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v39

    aput-object v39, v37, v38

    move-object/from16 v0, p0

    move/from16 v1, v36

    move-object/from16 v2, v37

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v33

    :goto_1
    const v36, 0x7f0200bf

    move-object/from16 v0, v23

    move/from16 v1, v36

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    const-string v36, "notification"

    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Landroid/app/NotificationManager;

    move-object/from16 v0, v23

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v36

    const/16 v37, 0x0

    const/high16 v38, 0x8000000

    move-object/from16 v0, v32

    move/from16 v1, v37

    move/from16 v2, v38

    invoke-virtual {v0, v1, v2}, Landroid/app/TaskStackBuilder;->getPendingIntent(II)Landroid/app/PendingIntent;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v36

    const-string v37, "android.message"

    invoke-virtual/range {v36 .. v37}, Landroid/app/Notification$Builder;->addKind(Ljava/lang/String;)Landroid/app/Notification$Builder;

    move-result-object v36

    const/16 v37, 0x0

    invoke-virtual/range {v36 .. v37}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;

    const/4 v9, 0x0

    if-eqz p1, :cond_5

    invoke-static/range {p0 .. p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v31

    const-string v36, "audio"

    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/media/AudioManager;

    const/16 v36, 0x1

    move/from16 v0, v36

    invoke-virtual {v4, v0}, Landroid/media/AudioManager;->getVibrateSetting(I)I

    move-result v36

    const/16 v37, 0x1

    move/from16 v0, v36

    move/from16 v1, v37

    if-ne v0, v1, :cond_4

    or-int/lit8 v9, v9, 0x2

    :cond_4
    const-string v36, "pref_key_ringtone"

    const/16 v37, 0x0

    move-object/from16 v0, v31

    move-object/from16 v1, v36

    move-object/from16 v2, v37

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v36

    if-eqz v36, :cond_b

    const/16 v36, 0x0

    :goto_2
    move-object/from16 v0, v23

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setSound(Landroid/net/Uri;)Landroid/app/Notification$Builder;

    :cond_5
    or-int/lit8 v9, v9, 0x4

    move-object/from16 v0, v23

    invoke-virtual {v0, v9}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    const/16 v36, 0x1

    move/from16 v0, v19

    move/from16 v1, v36

    if-ne v0, v1, :cond_c

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;->formatBigMessage(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v36

    move-object/from16 v0, v23

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    new-instance v36, Landroid/app/Notification$BigTextStyle;

    move-object/from16 v0, v36

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Landroid/app/Notification$BigTextStyle;-><init>(Landroid/app/Notification$Builder;)V

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;->formatBigMessage(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Landroid/app/Notification$BigTextStyle;->build()Landroid/app/Notification;

    move-result-object v25

    :goto_3
    const/16 v36, 0x7f

    move-object/from16 v0, v22

    move/from16 v1, v36

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    if-eqz p1, :cond_0

    new-instance v7, Landroid/content/Intent;

    const-string v36, "android.intent.action.MAIN"

    move-object/from16 v0, v36

    invoke-direct {v7, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v36, "android.intent.category.LAUNCHER"

    move-object/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const-string v36, "com.android.mms"

    const-string v37, "com.android.mms.ui.BootActivity"

    move-object/from16 v0, v36

    move-object/from16 v1, v37

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/16 v36, 0x0

    const/high16 v37, 0x8000000

    move-object/from16 v0, p0

    move/from16 v1, v36

    move/from16 v2, v37

    invoke-static {v0, v1, v7, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v28

    new-instance v36, Landroid/app/NotificationPlus$Builder;

    move-object/from16 v0, v36

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/app/NotificationPlus$Builder;-><init>(Landroid/content/Context;)V

    const v37, 0x7f0a012f

    move-object/from16 v0, p0

    move/from16 v1, v37

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Landroid/app/NotificationPlus$Builder;->setTitle(Ljava/lang/String;)Landroid/app/NotificationPlus$Builder;

    move-result-object v36

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v37

    const v38, 0x7f0b0005

    const/16 v39, 0x1

    move/from16 v0, v39

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v39, v0

    const/16 v40, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v41

    aput-object v41, v39, v40

    move-object/from16 v0, v37

    move/from16 v1, v38

    move/from16 v2, v19

    move-object/from16 v3, v39

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Landroid/app/NotificationPlus$Builder;->setMessage(Ljava/lang/String;)Landroid/app/NotificationPlus$Builder;

    move-result-object v36

    const v37, 0x7f0a0163

    move-object/from16 v0, p0

    move/from16 v1, v37

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v37

    move-object/from16 v0, v36

    move-object/from16 v1, v37

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationPlus$Builder;->setPositiveButton(Ljava/lang/String;Landroid/app/PendingIntent;)Landroid/app/NotificationPlus$Builder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Landroid/app/NotificationPlus$Builder;->create()Landroid/app/NotificationPlus;

    move-result-object v24

    const/16 v36, 0x1

    move/from16 v0, v36

    move-object/from16 v1, v24

    invoke-static {v0, v1}, Landroid/app/NotificationManagerPlus;->notify(ILandroid/app/NotificationPlus;)V

    goto/16 :goto_0

    :cond_6
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;->mTitle:Ljava/lang/String;

    move-object/from16 v33, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;->mSender:Lcom/android/mms/data/Contact;

    move-object/from16 v36, v0

    const/16 v37, 0x0

    move-object/from16 v0, v36

    move-object/from16 v1, p0

    move-object/from16 v2, v37

    invoke-virtual {v0, v1, v2}, Lcom/android/mms/data/Contact;->getAvatar(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    check-cast v8, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v8, :cond_8

    invoke-virtual {v8}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v5

    if-eqz v5, :cond_8

    const v36, 0x1050006

    move-object/from16 v0, v29

    move/from16 v1, v36

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    const v36, 0x1050005

    move-object/from16 v0, v29

    move/from16 v1, v36

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v36

    move/from16 v0, v36

    if-ge v0, v11, :cond_7

    const/16 v36, 0x1

    move/from16 v0, v36

    invoke-static {v5, v12, v11, v0}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v5

    :cond_7
    if-eqz v5, :cond_8

    move-object/from16 v0, v23

    invoke-virtual {v0, v5}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    :cond_8
    const-class v36, Lcom/android/mms/ui/WPMessageActivity;

    move-object/from16 v0, v32

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Landroid/app/TaskStackBuilder;->addParentStack(Ljava/lang/Class;)Landroid/app/TaskStackBuilder;

    invoke-static {}, Lcom/android/mms/MmsConfig;->getMmsDirMode()Z

    move-result v36

    if-eqz v36, :cond_a

    new-instance v7, Landroid/content/Intent;

    const-string v36, "android.intent.action.MAIN"

    move-object/from16 v0, v36

    invoke-direct {v7, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v36, 0x24000000

    move/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v36, "floderview_key"

    const/16 v37, 0x0

    move-object/from16 v0, v36

    move/from16 v1, v37

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v36, "com.android.mms"

    const-string v37, "com.android.mms.ui.FolderViewList"

    move-object/from16 v0, v36

    move-object/from16 v1, v37

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/16 v36, 0x1

    move/from16 v0, v19

    move/from16 v1, v36

    if-ne v0, v1, :cond_9

    const-string v36, "msg_type"

    const/16 v37, 0x3

    move-object/from16 v0, v36

    move/from16 v1, v37

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/16 v36, 0x0

    move/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;->mUri:Landroid/net/Uri;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const-string v36, "com.android.mms"

    const-string v37, "com.android.mms.ui.FolderModeSmsViewer"

    move-object/from16 v0, v36

    move-object/from16 v1, v37

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_9
    move-object/from16 v0, v32

    invoke-virtual {v0, v7}, Landroid/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Landroid/app/TaskStackBuilder;

    goto/16 :goto_1

    :cond_a
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;->mClickIntent:Landroid/content/Intent;

    move-object/from16 v36, v0

    move-object/from16 v0, v32

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Landroid/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Landroid/app/TaskStackBuilder;

    goto/16 :goto_1

    :cond_b
    invoke-static/range {v30 .. v30}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v36

    goto/16 :goto_2

    :cond_c
    const/16 v36, 0x1

    move/from16 v0, p2

    move/from16 v1, v36

    if-ne v0, v1, :cond_10

    new-instance v6, Landroid/text/SpannableStringBuilder;

    invoke-direct {v6}, Landroid/text/SpannableStringBuilder;-><init>()V

    sget-object v36, Lcom/android/mms/transaction/WapPushMessagingNotification;->sNotificationSet:Ljava/util/SortedSet;

    sget-object v37, Lcom/android/mms/transaction/WapPushMessagingNotification;->sNotificationSet:Ljava/util/SortedSet;

    invoke-interface/range {v37 .. v37}, Ljava/util/SortedSet;->size()I

    move-result v37

    move/from16 v0, v37

    new-array v0, v0, [Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;

    move-object/from16 v37, v0

    invoke-interface/range {v36 .. v37}, Ljava/util/SortedSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v15

    check-cast v15, [Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;

    array-length v0, v15

    move/from16 v16, v0

    add-int/lit8 v10, v16, -0x1

    :goto_4
    if-ltz v10, :cond_e

    aget-object v14, v15, v10

    move-object/from16 v0, p0

    invoke-virtual {v14, v0}, Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;->formatBigMessage(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v36

    move-object/from16 v0, v36

    invoke-virtual {v6, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    if-eqz v10, :cond_d

    const/16 v36, 0xa

    move/from16 v0, v36

    invoke-virtual {v6, v0}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    :cond_d
    add-int/lit8 v10, v10, -0x1

    goto :goto_4

    :cond_e
    const v36, 0x7f0a0249

    const/16 v37, 0x1

    move/from16 v0, v37

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v37, v0

    const/16 v38, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v39

    aput-object v39, v37, v38

    move-object/from16 v0, p0

    move/from16 v1, v36

    move-object/from16 v2, v37

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, v23

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    new-instance v36, Landroid/app/Notification$BigTextStyle;

    move-object/from16 v0, v36

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Landroid/app/Notification$BigTextStyle;-><init>(Landroid/app/Notification$Builder;)V

    move-object/from16 v0, v36

    invoke-virtual {v0, v6}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v37

    if-nez v5, :cond_f

    const/16 v36, 0x0

    :goto_5
    move-object/from16 v0, v37

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Landroid/app/Notification$BigTextStyle;->setSummaryText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Landroid/app/Notification$BigTextStyle;->build()Landroid/app/Notification;

    move-result-object v25

    goto/16 :goto_3

    :cond_f
    const-string v36, " "

    goto :goto_5

    :cond_10
    new-instance v35, Ljava/util/HashSet;

    sget-object v36, Lcom/android/mms/transaction/WapPushMessagingNotification;->sNotificationSet:Ljava/util/SortedSet;

    invoke-interface/range {v36 .. v36}, Ljava/util/SortedSet;->size()I

    move-result v36

    invoke-direct/range {v35 .. v36}, Ljava/util/HashSet;-><init>(I)V

    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    sget-object v36, Lcom/android/mms/transaction/WapPushMessagingNotification;->sNotificationSet:Ljava/util/SortedSet;

    invoke-interface/range {v36 .. v36}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v27

    :cond_11
    :goto_6
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->hasNext()Z

    move-result v36

    if-eqz v36, :cond_12

    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;

    move-object/from16 v0, v26

    iget-wide v0, v0, Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;->mThreadId:J

    move-wide/from16 v36, v0

    invoke-static/range {v36 .. v37}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v36

    if-nez v36, :cond_11

    move-object/from16 v0, v26

    iget-wide v0, v0, Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;->mThreadId:J

    move-wide/from16 v36, v0

    invoke-static/range {v36 .. v37}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, v20

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    :cond_12
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Lcom/android/mms/transaction/WapPushMessagingNotification;->formatSenders(Landroid/content/Context;Ljava/util/ArrayList;)Ljava/lang/CharSequence;

    move-result-object v36

    move-object/from16 v0, v23

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    new-instance v13, Landroid/app/Notification$InboxStyle;

    move-object/from16 v0, v23

    invoke-direct {v13, v0}, Landroid/app/Notification$InboxStyle;-><init>(Landroid/app/Notification$Builder;)V

    const-string v36, " "

    move-object/from16 v0, v36

    invoke-virtual {v13, v0}, Landroid/app/Notification$InboxStyle;->setSummaryText(Ljava/lang/CharSequence;)Landroid/app/Notification$InboxStyle;

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v34

    const/16 v36, 0x8

    move/from16 v0, v36

    move/from16 v1, v34

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v18

    const/4 v10, 0x0

    :goto_7
    move/from16 v0, v18

    if-ge v10, v0, :cond_13

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;

    move-object/from16 v0, p0

    invoke-virtual {v14, v0}, Lcom/android/mms/transaction/WapPushMessagingNotification$WapPushNotificationInfo;->formatInboxMessage(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v36

    move-object/from16 v0, v36

    invoke-virtual {v13, v0}, Landroid/app/Notification$InboxStyle;->addLine(Ljava/lang/CharSequence;)Landroid/app/Notification$InboxStyle;

    add-int/lit8 v10, v10, 0x1

    goto :goto_7

    :cond_13
    invoke-virtual {v13}, Landroid/app/Notification$InboxStyle;->build()Landroid/app/Notification;

    move-result-object v25

    goto/16 :goto_3
.end method
