.class public interface abstract Lcom/android/mms/ui/AdvancedCheckBoxPreference$GetSimInfo;
.super Ljava/lang/Object;
.source "AdvancedCheckBoxPreference.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/ui/AdvancedCheckBoxPreference;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "GetSimInfo"
.end annotation


# virtual methods
.method public abstract getNumberFormat(I)I
.end method

.method public abstract getSimColor(I)I
.end method

.method public abstract getSimName(I)Ljava/lang/CharSequence;
.end method

.method public abstract getSimNumber(I)Ljava/lang/CharSequence;
.end method

.method public abstract getSimStatus(I)I
.end method

.method public abstract is3G(I)Z
.end method
