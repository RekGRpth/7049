.class Lcom/android/mms/ui/CBMessageListActivity$3;
.super Landroid/os/Handler;
.source "CBMessageListActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/ui/CBMessageListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/CBMessageListActivity;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/CBMessageListActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/CBMessageListActivity$3;->this$0:Lcom/android/mms/ui/CBMessageListActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1    # Landroid/os/Message;

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity$3;->this$0:Lcom/android/mms/ui/CBMessageListActivity;

    invoke-static {v0}, Lcom/android/mms/ui/CBMessageListActivity;->access$200(Lcom/android/mms/ui/CBMessageListActivity;)Lcom/android/mms/ui/CBMessageListAdapter;

    move-result-object v0

    iget-boolean v0, v0, Lcom/android/mms/ui/CBMessageListAdapter;->mIsDeleteMode:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity$3;->this$0:Lcom/android/mms/ui/CBMessageListActivity;

    invoke-static {v0}, Lcom/android/mms/ui/CBMessageListActivity;->access$200(Lcom/android/mms/ui/CBMessageListActivity;)Lcom/android/mms/ui/CBMessageListAdapter;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/android/mms/ui/CBMessageListAdapter;->changeSelectedState(J)V

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity$3;->this$0:Lcom/android/mms/ui/CBMessageListActivity;

    invoke-static {v0}, Lcom/android/mms/ui/CBMessageListActivity;->access$200(Lcom/android/mms/ui/CBMessageListActivity;)Lcom/android/mms/ui/CBMessageListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/mms/ui/CBMessageListAdapter;->getSelectedNumber()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity$3;->this$0:Lcom/android/mms/ui/CBMessageListActivity;

    invoke-static {v0}, Lcom/android/mms/ui/CBMessageListActivity;->access$300(Lcom/android/mms/ui/CBMessageListActivity;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity$3;->this$0:Lcom/android/mms/ui/CBMessageListActivity;

    invoke-static {v0}, Lcom/android/mms/ui/CBMessageListActivity;->access$200(Lcom/android/mms/ui/CBMessageListActivity;)Lcom/android/mms/ui/CBMessageListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/mms/ui/CBMessageListAdapter;->getSelectedNumber()I

    move-result v0

    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListActivity$3;->this$0:Lcom/android/mms/ui/CBMessageListActivity;

    invoke-static {v1}, Lcom/android/mms/ui/CBMessageListActivity;->access$200(Lcom/android/mms/ui/CBMessageListActivity;)Lcom/android/mms/ui/CBMessageListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CursorAdapter;->getCount()I

    move-result v1

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity$3;->this$0:Lcom/android/mms/ui/CBMessageListActivity;

    invoke-static {v0, v4}, Lcom/android/mms/ui/CBMessageListActivity;->access$402(Lcom/android/mms/ui/CBMessageListActivity;Z)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity$3;->this$0:Lcom/android/mms/ui/CBMessageListActivity;

    invoke-static {v0}, Lcom/android/mms/ui/CBMessageListActivity;->access$300(Lcom/android/mms/ui/CBMessageListActivity;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    :cond_2
    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity$3;->this$0:Lcom/android/mms/ui/CBMessageListActivity;

    invoke-static {v0, v3}, Lcom/android/mms/ui/CBMessageListActivity;->access$402(Lcom/android/mms/ui/CBMessageListActivity;Z)Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
    .end packed-switch
.end method
