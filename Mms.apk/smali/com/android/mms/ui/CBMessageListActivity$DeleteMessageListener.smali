.class public Lcom/android/mms/ui/CBMessageListActivity$DeleteMessageListener;
.super Ljava/lang/Object;
.source "CBMessageListActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/ui/CBMessageListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DeleteMessageListener"
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mDeleteLockedMessages:Z

.field private final mDeleteUri:Landroid/net/Uri;

.field private final mHandler:Landroid/content/AsyncQueryHandler;

.field private final mMessageId:J


# direct methods
.method public constructor <init>(JLandroid/content/AsyncQueryHandler;Landroid/content/Context;)V
    .locals 1
    .param p1    # J
    .param p3    # Landroid/content/AsyncQueryHandler;
    .param p4    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/android/mms/ui/CBMessageListActivity$DeleteMessageListener;->mMessageId:J

    iput-object p3, p0, Lcom/android/mms/ui/CBMessageListActivity$DeleteMessageListener;->mHandler:Landroid/content/AsyncQueryHandler;

    iput-object p4, p0, Lcom/android/mms/ui/CBMessageListActivity$DeleteMessageListener;->mContext:Landroid/content/Context;

    sget-object v0, Lcom/android/mms/ui/CBMessageListActivity;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity$DeleteMessageListener;->mDeleteUri:Landroid/net/Uri;

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity$DeleteMessageListener;->mHandler:Landroid/content/AsyncQueryHandler;

    const/16 v1, 0x76d

    iget-object v3, p0, Lcom/android/mms/ui/CBMessageListActivity$DeleteMessageListener;->mDeleteUri:Landroid/net/Uri;

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/AsyncQueryHandler;->startDelete(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method public setDeleteLockedMessage(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/mms/ui/CBMessageListActivity$DeleteMessageListener;->mDeleteLockedMessages:Z

    return-void
.end method
