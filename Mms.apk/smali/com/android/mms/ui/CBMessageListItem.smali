.class public Lcom/android/mms/ui/CBMessageListItem;
.super Landroid/widget/LinearLayout;
.source "CBMessageListItem.java"


# static fields
.field private static final DEFAULT_ICON_INDENT:I = 0x5

.field public static final EXTRA_URLS:Ljava/lang/String; = "com.android.mms.ExtraUrls"

.field private static final STYLE_BOLD:Landroid/text/style/StyleSpan;

.field private static final TAG:Ljava/lang/String; = "CBMessageListItem"

.field public static final UPDATE_CHANNEL:I = 0xf

.field private static sDefaultContactImage:Landroid/graphics/drawable/Drawable;


# instance fields
.field private mAvatar:Lcom/android/mms/ui/QuickContactDivot;

.field private mBodyTextView:Landroid/widget/TextView;

.field mColorSpan:Landroid/text/style/ForegroundColorSpan;

.field private mDateView:Landroid/widget/TextView;

.field private mHandler:Landroid/os/Handler;

.field private mIsLastItemInList:Z

.field private mItemContainer:Landroid/view/View;

.field public mMessageBlock:Landroid/view/View;

.field private mMessageItem:Lcom/android/mms/ui/CBMessageItem;

.field private mPaint:Landroid/graphics/Paint;

.field private mPath:Landroid/graphics/Path;

.field private mSelectedBox:Landroid/widget/CheckBox;

.field private mSimStatus:Landroid/widget/TextView;

.field private mSpan:Landroid/text/style/LineHeightSpan;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/text/style/StyleSpan;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/style/StyleSpan;-><init>(I)V

    sput-object v0, Lcom/android/mms/ui/CBMessageListItem;->STYLE_BOLD:Landroid/text/style/StyleSpan;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/android/mms/ui/CBMessageListItem;->mPath:Landroid/graphics/Path;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/mms/ui/CBMessageListItem;->mPaint:Landroid/graphics/Paint;

    new-instance v0, Lcom/android/mms/ui/CBMessageListItem$2;

    invoke-direct {v0, p0}, Lcom/android/mms/ui/CBMessageListItem$2;-><init>(Lcom/android/mms/ui/CBMessageListItem;)V

    iput-object v0, p0, Lcom/android/mms/ui/CBMessageListItem;->mSpan:Landroid/text/style/LineHeightSpan;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mms/ui/CBMessageListItem;->mColorSpan:Landroid/text/style/ForegroundColorSpan;

    sget-object v0, Lcom/android/mms/ui/CBMessageListItem;->sDefaultContactImage:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020054

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lcom/android/mms/ui/CBMessageListItem;->sDefaultContactImage:Landroid/graphics/drawable/Drawable;

    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/android/mms/ui/CBMessageListItem;->mPath:Landroid/graphics/Path;

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/android/mms/ui/CBMessageListItem;->mPaint:Landroid/graphics/Paint;

    new-instance v1, Lcom/android/mms/ui/CBMessageListItem$2;

    invoke-direct {v1, p0}, Lcom/android/mms/ui/CBMessageListItem$2;-><init>(Lcom/android/mms/ui/CBMessageListItem;)V

    iput-object v1, p0, Lcom/android/mms/ui/CBMessageListItem;->mSpan:Landroid/text/style/LineHeightSpan;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/mms/ui/CBMessageListItem;->mColorSpan:Landroid/text/style/ForegroundColorSpan;

    iget-object v1, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070003

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v1, v0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    iput-object v1, p0, Lcom/android/mms/ui/CBMessageListItem;->mColorSpan:Landroid/text/style/ForegroundColorSpan;

    sget-object v1, Lcom/android/mms/ui/CBMessageListItem;->sDefaultContactImage:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020054

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/android/mms/ui/CBMessageListItem;->sDefaultContactImage:Landroid/graphics/drawable/Drawable;

    :cond_0
    return-void
.end method

.method private bindCommonMessage(Lcom/android/mms/ui/CBMessageItem;)V
    .locals 7
    .param p1    # Lcom/android/mms/ui/CBMessageItem;

    const/4 v6, 0x0

    iget-object v3, p0, Lcom/android/mms/ui/CBMessageListItem;->mBodyTextView:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/HideReturnsTransformationMethod;->getInstance()Landroid/text/method/HideReturnsTransformationMethod;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    invoke-virtual {p1}, Lcom/android/mms/ui/CBMessageItem;->getCachedFormattedMessage()Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/android/mms/ui/CBMessageItem;->getSubject()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/android/mms/ui/CBMessageItem;->getDate()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {p0, v3, v4, v5}, Lcom/android/mms/ui/CBMessageListItem;->formatMessage(Ljava/lang/String;Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/android/mms/ui/CBMessageItem;->setCachedFormattedMessage(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v3, p0, Lcom/android/mms/ui/CBMessageListItem;->mBodyTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Lcom/android/mms/ui/CBMessageItem;->getDate()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, p1, v3}, Lcom/android/mms/ui/CBMessageListItem;->formatTimestamp(Lcom/android/mms/ui/CBMessageItem;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    iget-object v3, p0, Lcom/android/mms/ui/CBMessageListItem;->mDateView:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, p1}, Lcom/android/mms/ui/CBMessageListItem;->formatSimStatus(Lcom/android/mms/ui/CBMessageItem;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/android/mms/ui/CBMessageListItem;->mSimStatus:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/mms/ui/CBMessageListItem;->mSimStatus:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v3, p0, Lcom/android/mms/ui/CBMessageListItem;->mAvatar:Lcom/android/mms/ui/QuickContactDivot;

    sget-object v4, Lcom/android/mms/ui/CBMessageListItem;->sDefaultContactImage:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v3, p0, Lcom/android/mms/ui/CBMessageListItem;->mDateView:Landroid/widget/TextView;

    const/4 v4, 0x5

    invoke-virtual {v3, v4, v6, v6, v6}, Landroid/widget/TextView;->setPadding(IIII)V

    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    return-void

    :cond_1
    iget-object v3, p0, Lcom/android/mms/ui/CBMessageListItem;->mSimStatus:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private formatMessage(Ljava/lang/String;Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/CharSequence;
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/util/regex/Pattern;

    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {}, Lcom/android/mms/util/SmileyParser;->getInstance()Lcom/android/mms/util/SmileyParser;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/android/mms/util/SmileyParser;->addSmileySpans(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_0
    if-eqz p3, :cond_1

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    :goto_0
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_1

    new-instance v3, Landroid/text/style/StyleSpan;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->start()I

    move-result v4

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->end()I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private formatSimStatus(Lcom/android/mms/ui/CBMessageItem;)Ljava/lang/CharSequence;
    .locals 6
    .param p1    # Lcom/android/mms/ui/CBMessageItem;

    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    iget-object v3, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    iget v4, p1, Lcom/android/mms/ui/CBMessageItem;->mSimId:I

    invoke-static {v3, v4}, Lcom/android/mms/ui/MessageUtils;->getSimInfo(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-lez v3, :cond_0

    const-string v3, " "

    invoke-virtual {v0, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iget-object v3, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    const v4, 0x7f0a0081

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    const-string v3, " "

    invoke-virtual {v0, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const-string v3, " "

    invoke-virtual {v0, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_0
    iget-object v3, p0, Lcom/android/mms/ui/CBMessageListItem;->mColorSpan:Landroid/text/style/ForegroundColorSpan;

    const/4 v4, 0x0

    const/16 v5, 0x21

    invoke-virtual {v0, v3, v4, v2, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    return-object v0
.end method

.method private formatTimestamp(Lcom/android/mms/ui/CBMessageItem;Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 5
    .param p1    # Lcom/android/mms/ui/CBMessageItem;
    .param p2    # Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string p2, " "

    :cond_0
    invoke-virtual {v0, p2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListItem;->mSpan:Landroid/text/style/LineHeightSpan;

    const/4 v2, 0x1

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListItem;->mColorSpan:Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    const/16 v3, 0x21

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    return-object v0
.end method


# virtual methods
.method public bind(Lcom/android/mms/ui/CBMessageItem;ZZ)V
    .locals 3
    .param p1    # Lcom/android/mms/ui/CBMessageItem;
    .param p2    # Z
    .param p3    # Z

    const/4 v2, 0x0

    iput-object p1, p0, Lcom/android/mms/ui/CBMessageListItem;->mMessageItem:Lcom/android/mms/ui/CBMessageItem;

    iput-boolean p2, p0, Lcom/android/mms/ui/CBMessageListItem;->mIsLastItemInList:Z

    invoke-virtual {p0, v2}, Lcom/android/mms/ui/CBMessageListItem;->setSelectedBackGroud(Z)V

    if-eqz p3, :cond_1

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListItem;->mSelectedBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1}, Lcom/android/mms/ui/CBMessageItem;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/mms/ui/CBMessageListItem;->setSelectedBackGroud(Z)V

    :cond_0
    :goto_0
    invoke-virtual {p0, v2}, Landroid/view/View;->setLongClickable(Z)V

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListItem;->mItemContainer:Landroid/view/View;

    new-instance v1, Lcom/android/mms/ui/CBMessageListItem$1;

    invoke-direct {v1, p0}, Lcom/android/mms/ui/CBMessageListItem$1;-><init>(Lcom/android/mms/ui/CBMessageListItem;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0, p1}, Lcom/android/mms/ui/CBMessageListItem;->bindCommonMessage(Lcom/android/mms/ui/CBMessageItem;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListItem;->mSelectedBox:Landroid/widget/CheckBox;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 11
    .param p1    # Landroid/graphics/Canvas;

    const/high16 v10, 0x3f800000

    iget-object v7, p0, Lcom/android/mms/ui/CBMessageListItem;->mMessageBlock:Landroid/view/View;

    const/4 v5, 0x0

    iget-object v8, p0, Lcom/android/mms/ui/CBMessageListItem;->mSelectedBox:Landroid/widget/CheckBox;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/android/mms/ui/CBMessageListItem;->mSelectedBox:Landroid/widget/CheckBox;

    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    move-result v8

    if-nez v8, :cond_0

    iget-object v8, p0, Lcom/android/mms/ui/CBMessageListItem;->mSelectedBox:Landroid/widget/CheckBox;

    invoke-virtual {v8}, Landroid/view/View;->getWidth()I

    move-result v5

    :cond_0
    if-eqz v7, :cond_5

    invoke-virtual {v7}, Landroid/view/View;->getX()F

    move-result v8

    int-to-float v9, v5

    add-float v1, v8, v9

    invoke-virtual {v7}, Landroid/view/View;->getY()F

    move-result v6

    invoke-virtual {v7}, Landroid/view/View;->getX()F

    move-result v8

    invoke-virtual {v7}, Landroid/view/View;->getWidth()I

    move-result v9

    int-to-float v9, v9

    add-float/2addr v8, v9

    int-to-float v9, v5

    add-float v4, v8, v9

    invoke-virtual {v7}, Landroid/view/View;->getY()F

    move-result v8

    invoke-virtual {v7}, Landroid/view/View;->getHeight()I

    move-result v9

    int-to-float v9, v9

    add-float v0, v8, v9

    iget-object v3, p0, Lcom/android/mms/ui/CBMessageListItem;->mPath:Landroid/graphics/Path;

    invoke-virtual {v3}, Landroid/graphics/Path;->reset()V

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    invoke-virtual {v3}, Landroid/graphics/Path;->reset()V

    sub-float/2addr v4, v10

    iget-boolean v8, p0, Lcom/android/mms/ui/CBMessageListItem;->mIsLastItemInList:Z

    if-eqz v8, :cond_1

    sub-float/2addr v0, v10

    :cond_1
    iget-object v8, p0, Lcom/android/mms/ui/CBMessageListItem;->mAvatar:Lcom/android/mms/ui/QuickContactDivot;

    invoke-virtual {v8}, Lcom/android/mms/ui/QuickContactDivot;->getPosition()I

    move-result v8

    const/4 v9, 0x4

    if-ne v8, v9, :cond_4

    iget-object v8, p0, Lcom/android/mms/ui/CBMessageListItem;->mAvatar:Lcom/android/mms/ui/QuickContactDivot;

    invoke-virtual {v8}, Lcom/android/mms/ui/QuickContactDivot;->getCloseOffset()F

    move-result v8

    add-float/2addr v8, v6

    invoke-virtual {v3, v1, v8}, Landroid/graphics/Path;->moveTo(FF)V

    invoke-virtual {v3, v1, v6}, Landroid/graphics/Path;->lineTo(FF)V

    if-lez v5, :cond_2

    iget-object v8, p0, Lcom/android/mms/ui/CBMessageListItem;->mAvatar:Lcom/android/mms/ui/QuickContactDivot;

    invoke-virtual {v8}, Landroid/view/View;->getWidth()I

    move-result v8

    int-to-float v8, v8

    sub-float v8, v1, v8

    int-to-float v9, v5

    sub-float/2addr v8, v9

    invoke-virtual {v3, v8, v6}, Landroid/graphics/Path;->lineTo(FF)V

    :cond_2
    invoke-virtual {v3, v4, v6}, Landroid/graphics/Path;->lineTo(FF)V

    invoke-virtual {v3, v4, v0}, Landroid/graphics/Path;->lineTo(FF)V

    invoke-virtual {v3, v1, v0}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v8, p0, Lcom/android/mms/ui/CBMessageListItem;->mAvatar:Lcom/android/mms/ui/QuickContactDivot;

    invoke-virtual {v8}, Lcom/android/mms/ui/QuickContactDivot;->getFarOffset()F

    move-result v8

    add-float/2addr v8, v6

    invoke-virtual {v3, v1, v8}, Landroid/graphics/Path;->lineTo(FF)V

    :cond_3
    :goto_0
    iget-object v2, p0, Lcom/android/mms/ui/CBMessageListItem;->mPaint:Landroid/graphics/Paint;

    const v8, -0x333334

    invoke-virtual {v2, v8}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {v2, v10}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    sget-object v8, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v8}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-virtual {p1, v3, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    :goto_1
    return-void

    :cond_4
    iget-object v8, p0, Lcom/android/mms/ui/CBMessageListItem;->mAvatar:Lcom/android/mms/ui/QuickContactDivot;

    invoke-virtual {v8}, Lcom/android/mms/ui/QuickContactDivot;->getPosition()I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_3

    iget-object v8, p0, Lcom/android/mms/ui/CBMessageListItem;->mAvatar:Lcom/android/mms/ui/QuickContactDivot;

    invoke-virtual {v8}, Lcom/android/mms/ui/QuickContactDivot;->getCloseOffset()F

    move-result v8

    add-float/2addr v8, v6

    invoke-virtual {v3, v4, v8}, Landroid/graphics/Path;->moveTo(FF)V

    invoke-virtual {v3, v4, v6}, Landroid/graphics/Path;->lineTo(FF)V

    int-to-float v8, v5

    sub-float v8, v1, v8

    invoke-virtual {v3, v8, v6}, Landroid/graphics/Path;->lineTo(FF)V

    int-to-float v8, v5

    sub-float v8, v1, v8

    invoke-virtual {v3, v8, v0}, Landroid/graphics/Path;->lineTo(FF)V

    invoke-virtual {v3, v4, v0}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v8, p0, Lcom/android/mms/ui/CBMessageListItem;->mAvatar:Lcom/android/mms/ui/QuickContactDivot;

    invoke-virtual {v8}, Lcom/android/mms/ui/QuickContactDivot;->getFarOffset()F

    move-result v8

    add-float/2addr v8, v6

    invoke-virtual {v3, v4, v8}, Landroid/graphics/Path;->lineTo(FF)V

    goto :goto_0

    :cond_5
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    goto :goto_1
.end method

.method public getMessageItem()Lcom/android/mms/ui/CBMessageItem;
    .locals 1

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListItem;->mMessageItem:Lcom/android/mms/ui/CBMessageItem;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 2

    const-string v0, "MMSLog"

    const-string v1, "CBMessageListItem.onFinishInflate()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/view/View;->onFinishInflate()V

    const v0, 0x7f0e0018

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/mms/ui/CBMessageListItem;->mBodyTextView:Landroid/widget/TextView;

    const v0, 0x7f0e001a

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/mms/ui/CBMessageListItem;->mDateView:Landroid/widget/TextView;

    const v0, 0x7f0e0014

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/CBMessageListItem;->mItemContainer:Landroid/view/View;

    const v0, 0x7f0e0017

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/CBMessageListItem;->mMessageBlock:Landroid/view/View;

    const v0, 0x7f0e0016

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/mms/ui/QuickContactDivot;

    iput-object v0, p0, Lcom/android/mms/ui/CBMessageListItem;->mAvatar:Lcom/android/mms/ui/QuickContactDivot;

    const v0, 0x7f0e001e

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/mms/ui/CBMessageListItem;->mSimStatus:Landroid/widget/TextView;

    const v0, 0x7f0e0015

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/android/mms/ui/CBMessageListItem;->mSelectedBox:Landroid/widget/CheckBox;

    return-void
.end method

.method public onMessageListItemClick()V
    .locals 3

    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListItem;->mSelectedBox:Landroid/widget/CheckBox;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListItem;->mSelectedBox:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListItem;->mSelectedBox:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/android/mms/ui/CBMessageListItem;->setSelectedBackGroud(Z)V

    :goto_0
    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListItem;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListItem;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x5

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListItem;->mMessageItem:Lcom/android/mms/ui/CBMessageItem;

    invoke-virtual {v1}, Lcom/android/mms/ui/CBMessageItem;->getMessageId()J

    move-result-wide v1

    long-to-int v1, v1

    iput v1, v0, Landroid/os/Message;->arg1:I

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :cond_0
    return-void

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/android/mms/ui/CBMessageListItem;->setSelectedBackGroud(Z)V

    goto :goto_0
.end method

.method public setMsgListItemHandler(Landroid/os/Handler;)V
    .locals 0
    .param p1    # Landroid/os/Handler;

    iput-object p1, p0, Lcom/android/mms/ui/CBMessageListItem;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method public setSelectedBackGroud(Z)V
    .locals 4
    .param p1    # Z

    const v3, 0x7f0200ab

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListItem;->mSelectedBox:Landroid/widget/CheckBox;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListItem;->mSelectedBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListItem;->mMessageBlock:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListItem;->mDateView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const v0, 0x7f0200a8

    invoke-virtual {p0, v0}, Landroid/view/View;->setBackgroundResource(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListItem;->mSelectedBox:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListItem;->mSelectedBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListItem;->mMessageBlock:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListItem;->mDateView:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public unbind()V
    .locals 0

    return-void
.end method
