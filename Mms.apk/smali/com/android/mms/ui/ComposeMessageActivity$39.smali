.class Lcom/android/mms/ui/ComposeMessageActivity$39;
.super Ljava/lang/Object;
.source "ComposeMessageActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/ComposeMessageActivity;->handleSendIntent()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/ComposeMessageActivity;

.field final synthetic val$mimeType:Ljava/lang/String;

.field final synthetic val$numberToImport:I

.field final synthetic val$uris:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/ComposeMessageActivity;ILjava/util/ArrayList;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/ComposeMessageActivity$39;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    iput p2, p0, Lcom/android/mms/ui/ComposeMessageActivity$39;->val$numberToImport:I

    iput-object p3, p0, Lcom/android/mms/ui/ComposeMessageActivity$39;->val$uris:Ljava/util/ArrayList;

    iput-object p4, p0, Lcom/android/mms/ui/ComposeMessageActivity$39;->val$mimeType:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x1

    iget-object v4, p0, Lcom/android/mms/ui/ComposeMessageActivity$39;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v4, v7}, Lcom/android/mms/ui/ComposeMessageActivity;->access$8102(Lcom/android/mms/ui/ComposeMessageActivity;I)I

    iget-object v4, p0, Lcom/android/mms/ui/ComposeMessageActivity$39;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v4, v6}, Lcom/android/mms/ui/ComposeMessageActivity;->access$7202(Lcom/android/mms/ui/ComposeMessageActivity;Z)Z

    iget-object v4, p0, Lcom/android/mms/ui/ComposeMessageActivity$39;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v4, v6}, Lcom/android/mms/ui/ComposeMessageActivity;->access$7702(Lcom/android/mms/ui/ComposeMessageActivity;Z)Z

    const/4 v0, 0x0

    :goto_0
    iget v4, p0, Lcom/android/mms/ui/ComposeMessageActivity$39;->val$numberToImport:I

    if-ge v0, v4, :cond_1

    iget-object v4, p0, Lcom/android/mms/ui/ComposeMessageActivity$39;->val$uris:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Parcelable;

    move-object v4, v3

    check-cast v4, Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v4, "file"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/mms/ui/ComposeMessageActivity$39;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    iget-object v5, p0, Lcom/android/mms/ui/ComposeMessageActivity$39;->val$mimeType:Ljava/lang/String;

    check-cast v3, Landroid/net/Uri;

    invoke-static {v4, v5, v3, v6}, Lcom/android/mms/ui/ComposeMessageActivity;->access$8200(Lcom/android/mms/ui/ComposeMessageActivity;Ljava/lang/String;Landroid/net/Uri;Z)V

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v4, p0, Lcom/android/mms/ui/ComposeMessageActivity$39;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    iget-object v5, p0, Lcom/android/mms/ui/ComposeMessageActivity$39;->val$mimeType:Ljava/lang/String;

    check-cast v3, Landroid/net/Uri;

    invoke-static {v4, v5, v3, v6}, Lcom/android/mms/ui/ComposeMessageActivity;->access$8000(Lcom/android/mms/ui/ComposeMessageActivity;Ljava/lang/String;Landroid/net/Uri;Z)V

    goto :goto_1

    :cond_1
    iget-object v4, p0, Lcom/android/mms/ui/ComposeMessageActivity$39;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v4, v7}, Lcom/android/mms/ui/ComposeMessageActivity;->access$8102(Lcom/android/mms/ui/ComposeMessageActivity;I)I

    iget-object v4, p0, Lcom/android/mms/ui/ComposeMessageActivity$39;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v4}, Lcom/android/mms/ui/ComposeMessageActivity;->access$100(Lcom/android/mms/ui/ComposeMessageActivity;)Lcom/android/mms/data/WorkingMessage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/mms/data/WorkingMessage;->getSlideshow()Lcom/android/mms/model/SlideshowModel;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/android/mms/model/SlideshowModel;->size()I

    move-result v4

    if-lez v4, :cond_2

    iget-object v4, p0, Lcom/android/mms/ui/ComposeMessageActivity$39;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v4}, Lcom/android/mms/ui/ComposeMessageActivity;->access$100(Lcom/android/mms/ui/ComposeMessageActivity;)Lcom/android/mms/data/WorkingMessage;

    move-result-object v4

    invoke-virtual {v4, v7}, Lcom/android/mms/data/WorkingMessage;->saveAsMms(Z)Landroid/net/Uri;

    :cond_2
    return-void
.end method
