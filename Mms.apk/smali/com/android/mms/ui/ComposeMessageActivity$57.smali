.class Lcom/android/mms/ui/ComposeMessageActivity$57;
.super Ljava/lang/Object;
.source "ComposeMessageActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/ComposeMessageActivity;->getContactSIM(Ljava/lang/String;)I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/ComposeMessageActivity;

.field final synthetic val$dbQueryLock:Ljava/lang/Object;

.field final synthetic val$mContextTemp:Landroid/content/Context;

.field final synthetic val$num:Ljava/lang/String;

.field final synthetic val$simID:Lcom/android/mms/ui/ComposeMessageActivity$1Int;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/ComposeMessageActivity;Ljava/lang/String;Landroid/content/Context;Ljava/lang/Object;Lcom/android/mms/ui/ComposeMessageActivity$1Int;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/ComposeMessageActivity$57;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    iput-object p2, p0, Lcom/android/mms/ui/ComposeMessageActivity$57;->val$num:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/mms/ui/ComposeMessageActivity$57;->val$mContextTemp:Landroid/content/Context;

    iput-object p4, p0, Lcom/android/mms/ui/ComposeMessageActivity$57;->val$dbQueryLock:Ljava/lang/Object;

    iput-object p5, p0, Lcom/android/mms/ui/ComposeMessageActivity$57;->val$simID:Lcom/android/mms/ui/ComposeMessageActivity$1Int;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    const/4 v5, 0x1

    const/4 v10, 0x0

    const/4 v9, -0x1

    iget-object v8, p0, Lcom/android/mms/ui/ComposeMessageActivity$57;->val$num:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$57;->val$mContextTemp:Landroid/content/Context;

    invoke-static {v8, v0}, Lcom/android/mms/ui/MessageUtils;->formatNumber(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$57;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "sim_id"

    aput-object v3, v2, v10

    const-string v3, "mimetype=\'vnd.android.cursor.item/phone_v2\' AND (data1=? OR data1=?) AND (sim_id!= -1)"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    aput-object v8, v4, v10

    aput-object v7, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v9

    :goto_0
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    iget-object v1, p0, Lcom/android/mms/ui/ComposeMessageActivity$57;->val$dbQueryLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$57;->val$simID:Lcom/android/mms/ui/ComposeMessageActivity$1Int;

    invoke-virtual {v0, v9}, Lcom/android/mms/ui/ComposeMessageActivity$1Int;->set(I)V

    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$57;->val$dbQueryLock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1

    return-void

    :cond_1
    const/4 v9, -0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
