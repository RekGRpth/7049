.class Lcom/android/mms/ui/ComposeMessageActivity$58$1;
.super Ljava/lang/Object;
.source "ComposeMessageActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/ComposeMessageActivity$58;->onClick(Landroid/content/DialogInterface;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/mms/ui/ComposeMessageActivity$58;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/ComposeMessageActivity$58;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/ComposeMessageActivity$58$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$58;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    iget-object v3, p0, Lcom/android/mms/ui/ComposeMessageActivity$58$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$58;

    iget-object v3, v3, Lcom/android/mms/ui/ComposeMessageActivity$58;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v3}, Lcom/android/mms/ui/ComposeMessageActivity;->access$2300(Lcom/android/mms/ui/ComposeMessageActivity;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/mms/ui/ComposeMessageActivity$58$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$58;

    iget-object v3, v3, Lcom/android/mms/ui/ComposeMessageActivity$58;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v3}, Lcom/android/mms/ui/ComposeMessageActivity;->access$2400(Lcom/android/mms/ui/ComposeMessageActivity;)Lcom/android/mms/ui/RecipientsEditor;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/mms/ui/RecipientsEditor;->getNumbers()Ljava/util/List;

    move-result-object v2

    :goto_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v3, p0, Lcom/android/mms/ui/ComposeMessageActivity$58$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$58;

    iget-object v3, v3, Lcom/android/mms/ui/ComposeMessageActivity$58;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v3}, Lcom/android/mms/ui/ComposeMessageActivity;->access$11400(Lcom/android/mms/ui/ComposeMessageActivity;)I

    move-result v3

    iget-object v4, p0, Lcom/android/mms/ui/ComposeMessageActivity$58$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$58;

    iget v4, v4, Lcom/android/mms/ui/ComposeMessageActivity$58;->val$mmsLimitCount:I

    mul-int/lit8 v4, v4, 0x2

    if-le v3, v4, :cond_2

    const/4 v0, 0x0

    :goto_1
    iget-object v3, p0, Lcom/android/mms/ui/ComposeMessageActivity$58$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$58;

    iget v3, v3, Lcom/android/mms/ui/ComposeMessageActivity$58;->val$mmsLimitCount:I

    if-ge v0, v3, :cond_1

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/android/mms/ui/ComposeMessageActivity$58$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$58;

    iget-object v3, v3, Lcom/android/mms/ui/ComposeMessageActivity$58;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v3}, Lcom/android/mms/ui/ComposeMessageActivity;->access$3500(Lcom/android/mms/ui/ComposeMessageActivity;)Lcom/android/mms/data/ContactList;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/mms/data/ContactList;->getNumbers()[Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/android/mms/ui/ComposeMessageActivity$58$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$58;

    iget-object v3, v3, Lcom/android/mms/ui/ComposeMessageActivity$58;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v3}, Lcom/android/mms/ui/ComposeMessageActivity;->access$100(Lcom/android/mms/ui/ComposeMessageActivity;)Lcom/android/mms/data/WorkingMessage;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/android/mms/data/WorkingMessage;->setWorkingRecipients(Ljava/util/List;)V

    :goto_2
    iget-object v3, p0, Lcom/android/mms/ui/ComposeMessageActivity$58$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$58;

    iget-object v3, v3, Lcom/android/mms/ui/ComposeMessageActivity$58;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v3}, Lcom/android/mms/ui/ComposeMessageActivity;->access$11500(Lcom/android/mms/ui/ComposeMessageActivity;)V

    return-void

    :cond_2
    iget-object v3, p0, Lcom/android/mms/ui/ComposeMessageActivity$58$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$58;

    iget-object v3, v3, Lcom/android/mms/ui/ComposeMessageActivity$58;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v3}, Lcom/android/mms/ui/ComposeMessageActivity;->access$11400(Lcom/android/mms/ui/ComposeMessageActivity;)I

    move-result v3

    add-int/lit8 v0, v3, -0x1

    :goto_3
    iget-object v3, p0, Lcom/android/mms/ui/ComposeMessageActivity$58$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$58;

    iget v3, v3, Lcom/android/mms/ui/ComposeMessageActivity$58;->val$mmsLimitCount:I

    if-lt v0, v3, :cond_3

    invoke-interface {v2, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    add-int/lit8 v0, v0, -0x1

    goto :goto_3

    :cond_3
    iget-object v3, p0, Lcom/android/mms/ui/ComposeMessageActivity$58$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$58;

    iget-object v3, v3, Lcom/android/mms/ui/ComposeMessageActivity$58;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v3}, Lcom/android/mms/ui/ComposeMessageActivity;->access$100(Lcom/android/mms/ui/ComposeMessageActivity;)Lcom/android/mms/data/WorkingMessage;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/android/mms/data/WorkingMessage;->setWorkingRecipients(Ljava/util/List;)V

    goto :goto_2
.end method
