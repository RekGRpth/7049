.class Lcom/android/mms/ui/ComposeMessageActivity$SoloAlertDialog;
.super Landroid/app/AlertDialog;
.source "ComposeMessageActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/ui/ComposeMessageActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SoloAlertDialog"
.end annotation


# instance fields
.field private mAlertDialog:Landroid/app/AlertDialog;

.field final synthetic this$0:Lcom/android/mms/ui/ComposeMessageActivity;


# direct methods
.method private constructor <init>(Lcom/android/mms/ui/ComposeMessageActivity;Landroid/content/Context;)V
    .locals 0
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/android/mms/ui/ComposeMessageActivity$SoloAlertDialog;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-direct {p0, p2}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/mms/ui/ComposeMessageActivity;Landroid/content/Context;Lcom/android/mms/ui/ComposeMessageActivity$1;)V
    .locals 0
    .param p1    # Lcom/android/mms/ui/ComposeMessageActivity;
    .param p2    # Landroid/content/Context;
    .param p3    # Lcom/android/mms/ui/ComposeMessageActivity$1;

    invoke-direct {p0, p1, p2}, Lcom/android/mms/ui/ComposeMessageActivity$SoloAlertDialog;-><init>(Lcom/android/mms/ui/ComposeMessageActivity;Landroid/content/Context;)V

    return-void
.end method

.method private needShow()Z
    .locals 1

    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$SoloAlertDialog;->mAlertDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$SoloAlertDialog;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public show(Z)V
    .locals 5
    .param p1    # Z

    invoke-direct {p0}, Lcom/android/mms/ui/ComposeMessageActivity$SoloAlertDialog;->needShow()Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f020056

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f0a0160

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/android/mms/ui/ComposeMessageActivity$SoloAlertDialog;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v2}, Lcom/android/mms/ui/ComposeMessageActivity;->access$100(Lcom/android/mms/ui/ComposeMessageActivity;)Lcom/android/mms/data/WorkingMessage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/mms/data/WorkingMessage;->hasSlideshow()Z

    move-result v2

    if-eqz v2, :cond_2

    or-int/lit8 v1, v1, 0x8

    :goto_1
    iget-object v2, p0, Lcom/android/mms/ui/ComposeMessageActivity$SoloAlertDialog;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v2}, Lcom/android/mms/ui/MessageUtils;->isVCalendarAvailable(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    or-int/lit8 v1, v1, 0x10

    :cond_1
    iget-object v2, p0, Lcom/android/mms/ui/ComposeMessageActivity$SoloAlertDialog;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    new-instance v3, Lcom/android/mms/ui/AttachmentTypeSelectorAdapter;

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4, v1}, Lcom/android/mms/ui/AttachmentTypeSelectorAdapter;-><init>(Landroid/content/Context;I)V

    invoke-static {v2, v3}, Lcom/android/mms/ui/ComposeMessageActivity;->access$12002(Lcom/android/mms/ui/ComposeMessageActivity;Lcom/android/mms/ui/AttachmentTypeSelectorAdapter;)Lcom/android/mms/ui/AttachmentTypeSelectorAdapter;

    iget-object v2, p0, Lcom/android/mms/ui/ComposeMessageActivity$SoloAlertDialog;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v2}, Lcom/android/mms/ui/ComposeMessageActivity;->access$12000(Lcom/android/mms/ui/ComposeMessageActivity;)Lcom/android/mms/ui/AttachmentTypeSelectorAdapter;

    move-result-object v2

    new-instance v3, Lcom/android/mms/ui/ComposeMessageActivity$SoloAlertDialog$1;

    invoke-direct {v3, p0, p1}, Lcom/android/mms/ui/ComposeMessageActivity$SoloAlertDialog$1;-><init>(Lcom/android/mms/ui/ComposeMessageActivity$SoloAlertDialog;Z)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/android/mms/ui/ComposeMessageActivity$SoloAlertDialog;->mAlertDialog:Landroid/app/AlertDialog;

    goto :goto_0

    :cond_2
    or-int/lit8 v1, v1, 0x4

    goto :goto_1
.end method
