.class Lcom/android/mms/ui/ConversationList$DeleteThreadListener$1;
.super Ljava/lang/Object;
.source "ConversationList.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/ConversationList$DeleteThreadListener;->onClick(Landroid/content/DialogInterface;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/ConversationList$DeleteThreadListener;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/ConversationList$DeleteThreadListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/ConversationList$DeleteThreadListener$1;->this$0:Lcom/android/mms/ui/ConversationList$DeleteThreadListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private showProgressDialog()V
    .locals 2

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList$DeleteThreadListener$1;->this$0:Lcom/android/mms/ui/ConversationList$DeleteThreadListener;

    invoke-static {v0}, Lcom/android/mms/ui/ConversationList$DeleteThreadListener;->access$1100(Lcom/android/mms/ui/ConversationList$DeleteThreadListener;)Landroid/content/AsyncQueryHandler;

    move-result-object v0

    instance-of v0, v0, Lcom/android/mms/ui/ConversationList$BaseProgressQueryHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList$DeleteThreadListener$1;->this$0:Lcom/android/mms/ui/ConversationList$DeleteThreadListener;

    invoke-static {v0}, Lcom/android/mms/ui/ConversationList$DeleteThreadListener;->access$1100(Lcom/android/mms/ui/ConversationList$DeleteThreadListener;)Landroid/content/AsyncQueryHandler;

    move-result-object v0

    check-cast v0, Lcom/android/mms/ui/ConversationList$BaseProgressQueryHandler;

    iget-object v1, p0, Lcom/android/mms/ui/ConversationList$DeleteThreadListener$1;->this$0:Lcom/android/mms/ui/ConversationList$DeleteThreadListener;

    invoke-static {v1}, Lcom/android/mms/ui/ConversationList$DeleteThreadListener;->access$1600(Lcom/android/mms/ui/ConversationList$DeleteThreadListener;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/android/mms/ui/DeleteProgressDialogUtil;->getProgressDialog(Landroid/content/Context;)Lcom/android/mms/ui/NewProgressDialog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/mms/ui/ConversationList$BaseProgressQueryHandler;->setProgressDialog(Lcom/android/mms/ui/NewProgressDialog;)V

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList$DeleteThreadListener$1;->this$0:Lcom/android/mms/ui/ConversationList$DeleteThreadListener;

    invoke-static {v0}, Lcom/android/mms/ui/ConversationList$DeleteThreadListener;->access$1100(Lcom/android/mms/ui/ConversationList$DeleteThreadListener;)Landroid/content/AsyncQueryHandler;

    move-result-object v0

    check-cast v0, Lcom/android/mms/ui/ConversationList$BaseProgressQueryHandler;

    invoke-virtual {v0}, Lcom/android/mms/ui/ConversationList$BaseProgressQueryHandler;->showProgressDialog()V

    :cond_0
    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    const/4 v8, 0x0

    invoke-direct {p0}, Lcom/android/mms/ui/ConversationList$DeleteThreadListener$1;->showProgressDialog()V

    const/16 v1, 0x709

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList$DeleteThreadListener$1;->this$0:Lcom/android/mms/ui/ConversationList$DeleteThreadListener;

    invoke-static {v0}, Lcom/android/mms/ui/ConversationList$DeleteThreadListener;->access$1000(Lcom/android/mms/ui/ConversationList$DeleteThreadListener;)Ljava/util/Collection;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList$DeleteThreadListener$1;->this$0:Lcom/android/mms/ui/ConversationList$DeleteThreadListener;

    invoke-static {v0}, Lcom/android/mms/ui/ConversationList$DeleteThreadListener;->access$1100(Lcom/android/mms/ui/ConversationList$DeleteThreadListener;)Landroid/content/AsyncQueryHandler;

    move-result-object v0

    iget-object v2, p0, Lcom/android/mms/ui/ConversationList$DeleteThreadListener$1;->this$0:Lcom/android/mms/ui/ConversationList$DeleteThreadListener;

    invoke-static {v2}, Lcom/android/mms/ui/ConversationList$DeleteThreadListener;->access$1200(Lcom/android/mms/ui/ConversationList$DeleteThreadListener;)Z

    move-result v2

    iget-object v5, p0, Lcom/android/mms/ui/ConversationList$DeleteThreadListener$1;->this$0:Lcom/android/mms/ui/ConversationList$DeleteThreadListener;

    invoke-static {v5}, Lcom/android/mms/ui/ConversationList$DeleteThreadListener;->access$1300(Lcom/android/mms/ui/ConversationList$DeleteThreadListener;)I

    move-result v5

    iget-object v6, p0, Lcom/android/mms/ui/ConversationList$DeleteThreadListener$1;->this$0:Lcom/android/mms/ui/ConversationList$DeleteThreadListener;

    invoke-static {v6}, Lcom/android/mms/ui/ConversationList$DeleteThreadListener;->access$1400(Lcom/android/mms/ui/ConversationList$DeleteThreadListener;)I

    move-result v6

    invoke-static {v0, v1, v2, v5, v6}, Lcom/android/mms/data/Conversation;->startDeleteAll(Landroid/content/AsyncQueryHandler;IZII)V

    invoke-static {}, Lcom/android/mms/util/DraftCache;->getInstance()Lcom/android/mms/util/DraftCache;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/mms/util/DraftCache;->refresh()V

    :goto_0
    return-void

    :cond_0
    invoke-static {v8}, Lcom/android/mms/ui/ConversationList;->access$1502(I)I

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList$DeleteThreadListener$1;->this$0:Lcom/android/mms/ui/ConversationList$DeleteThreadListener;

    invoke-static {v0}, Lcom/android/mms/ui/ConversationList$DeleteThreadListener;->access$1000(Lcom/android/mms/ui/ConversationList$DeleteThreadListener;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {}, Lcom/android/mms/ui/ConversationList;->access$1508()I

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList$DeleteThreadListener$1;->this$0:Lcom/android/mms/ui/ConversationList$DeleteThreadListener;

    invoke-static {v0}, Lcom/android/mms/ui/ConversationList$DeleteThreadListener;->access$1100(Lcom/android/mms/ui/ConversationList$DeleteThreadListener;)Landroid/content/AsyncQueryHandler;

    move-result-object v0

    iget-object v2, p0, Lcom/android/mms/ui/ConversationList$DeleteThreadListener$1;->this$0:Lcom/android/mms/ui/ConversationList$DeleteThreadListener;

    invoke-static {v2}, Lcom/android/mms/ui/ConversationList$DeleteThreadListener;->access$1200(Lcom/android/mms/ui/ConversationList$DeleteThreadListener;)Z

    move-result v2

    iget-object v5, p0, Lcom/android/mms/ui/ConversationList$DeleteThreadListener$1;->this$0:Lcom/android/mms/ui/ConversationList$DeleteThreadListener;

    invoke-static {v5}, Lcom/android/mms/ui/ConversationList$DeleteThreadListener;->access$1300(Lcom/android/mms/ui/ConversationList$DeleteThreadListener;)I

    move-result v5

    iget-object v6, p0, Lcom/android/mms/ui/ConversationList$DeleteThreadListener$1;->this$0:Lcom/android/mms/ui/ConversationList$DeleteThreadListener;

    invoke-static {v6}, Lcom/android/mms/ui/ConversationList$DeleteThreadListener;->access$1400(Lcom/android/mms/ui/ConversationList$DeleteThreadListener;)I

    move-result v6

    invoke-static/range {v0 .. v6}, Lcom/android/mms/data/Conversation;->startDelete(Landroid/content/AsyncQueryHandler;IZJII)V

    invoke-static {}, Lcom/android/mms/util/DraftCache;->getInstance()Lcom/android/mms/util/DraftCache;

    move-result-object v0

    invoke-virtual {v0, v3, v4, v8}, Lcom/android/mms/util/DraftCache;->setDraftState(JZ)V

    goto :goto_1

    :cond_1
    const-string v0, "ConversationList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mDeleteCounter = "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/android/mms/ui/ConversationList;->access$1500()I

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
