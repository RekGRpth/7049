.class Lcom/android/mms/ui/FolderModeSmsViewer$16;
.super Ljava/lang/Object;
.source "FolderModeSmsViewer.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/FolderModeSmsViewer;->showSimSelectedDialog(Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

.field final synthetic val$it:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/FolderModeSmsViewer;Landroid/content/Intent;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/FolderModeSmsViewer$16;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    iput-object p2, p0, Lcom/android/mms/ui/FolderModeSmsViewer$16;->val$it:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/4 v5, -0x1

    iget-object v2, p0, Lcom/android/mms/ui/FolderModeSmsViewer$16;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    iget-object v1, p0, Lcom/android/mms/ui/FolderModeSmsViewer$16;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-static {v1}, Lcom/android/mms/ui/FolderModeSmsViewer;->access$2500(Lcom/android/mms/ui/FolderModeSmsViewer;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/provider/Telephony$SIMInfo;

    iget-wide v3, v1, Landroid/provider/Telephony$SIMInfo;->mSimId:J

    long-to-int v1, v3

    invoke-static {v2, v1}, Lcom/android/mms/ui/FolderModeSmsViewer;->access$2402(Lcom/android/mms/ui/FolderModeSmsViewer;I)I

    iget-object v1, p0, Lcom/android/mms/ui/FolderModeSmsViewer$16;->val$it:Landroid/content/Intent;

    const-string v2, "Select_type"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/mms/ui/FolderModeSmsViewer$16;->val$it:Landroid/content/Intent;

    const-string v2, "Select_type"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/mms/ui/FolderModeSmsViewer$16;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-static {v1}, Lcom/android/mms/ui/FolderModeSmsViewer;->access$1900(Lcom/android/mms/ui/FolderModeSmsViewer;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x68

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mms/ui/FolderModeSmsViewer$16;->val$it:Landroid/content/Intent;

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v1, p0, Lcom/android/mms/ui/FolderModeSmsViewer$16;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-static {v1}, Lcom/android/mms/ui/FolderModeSmsViewer;->access$1900(Lcom/android/mms/ui/FolderModeSmsViewer;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
