.class Lcom/android/mms/ui/FolderModeSmsViewer$2;
.super Ljava/lang/Object;
.source "FolderModeSmsViewer.java"

# interfaces
.implements Landroid/view/View$OnCreateContextMenuListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/ui/FolderModeSmsViewer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/FolderModeSmsViewer;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/FolderModeSmsViewer;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/FolderModeSmsViewer$2;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 4
    .param p1    # Landroid/view/ContextMenu;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ContextMenu$ContextMenuInfo;

    const/4 v3, 0x0

    const v1, 0x7f0a014e

    invoke-interface {p1, v1}, Landroid/view/ContextMenu;->setHeaderTitle(I)Landroid/view/ContextMenu;

    new-instance v0, Lcom/android/mms/ui/FolderModeSmsViewer$MsgListMenuClickListener;

    iget-object v1, p0, Lcom/android/mms/ui/FolderModeSmsViewer$2;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/android/mms/ui/FolderModeSmsViewer$MsgListMenuClickListener;-><init>(Lcom/android/mms/ui/FolderModeSmsViewer;Lcom/android/mms/ui/FolderModeSmsViewer$1;)V

    iget-object v1, p0, Lcom/android/mms/ui/FolderModeSmsViewer$2;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-static {v1, p1, v0}, Lcom/android/mms/ui/FolderModeSmsViewer;->access$100(Lcom/android/mms/ui/FolderModeSmsViewer;Landroid/view/ContextMenu;Lcom/android/mms/ui/FolderModeSmsViewer$MsgListMenuClickListener;)V

    iget-object v1, p0, Lcom/android/mms/ui/FolderModeSmsViewer$2;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-static {v1}, Lcom/android/mms/ui/FolderModeSmsViewer;->access$200(Lcom/android/mms/ui/FolderModeSmsViewer;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    const/16 v1, 0x18

    const v2, 0x7f0a014d

    invoke-interface {p1, v3, v1, v3, v2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    const/16 v1, 0x1f

    const v2, 0x7f0a0035

    invoke-interface {p1, v3, v1, v3, v2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    :cond_0
    const/16 v1, 0x22

    const v2, 0x7f0a00ac

    invoke-interface {p1, v3, v1, v3, v2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    const/16 v1, 0x11

    const v2, 0x7f0a013e

    invoke-interface {p1, v3, v1, v3, v2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    const/16 v1, 0x12

    const v2, 0x7f0a0140

    invoke-interface {p1, v3, v1, v3, v2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    return-void
.end method
