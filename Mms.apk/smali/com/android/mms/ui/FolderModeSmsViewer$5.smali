.class Lcom/android/mms/ui/FolderModeSmsViewer$5;
.super Ljava/lang/Object;
.source "FolderModeSmsViewer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/FolderModeSmsViewer;->resendMsg()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

.field final synthetic val$cr:Landroid/content/ContentResolver;

.field final synthetic val$ct:Landroid/content/Context;

.field final synthetic val$mUri:Landroid/net/Uri;

.field final synthetic val$sender:Lcom/android/mms/transaction/MessageSender;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/FolderModeSmsViewer;Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Lcom/android/mms/transaction/MessageSender;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/FolderModeSmsViewer$5;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    iput-object p2, p0, Lcom/android/mms/ui/FolderModeSmsViewer$5;->val$ct:Landroid/content/Context;

    iput-object p3, p0, Lcom/android/mms/ui/FolderModeSmsViewer$5;->val$cr:Landroid/content/ContentResolver;

    iput-object p4, p0, Lcom/android/mms/ui/FolderModeSmsViewer$5;->val$mUri:Landroid/net/Uri;

    iput-object p5, p0, Lcom/android/mms/ui/FolderModeSmsViewer$5;->val$sender:Lcom/android/mms/transaction/MessageSender;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    :try_start_0
    iget-object v0, p0, Lcom/android/mms/ui/FolderModeSmsViewer$5;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-static {v0}, Lcom/android/mms/ui/FolderModeSmsViewer;->access$1400(Lcom/android/mms/ui/FolderModeSmsViewer;)I

    move-result v0

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v0, "status"

    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v0, p0, Lcom/android/mms/ui/FolderModeSmsViewer$5;->val$ct:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/mms/ui/FolderModeSmsViewer$5;->val$cr:Landroid/content/ContentResolver;

    iget-object v2, p0, Lcom/android/mms/ui/FolderModeSmsViewer$5;->val$mUri:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Landroid/database/sqlite/SqliteWrapper;->update(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/mms/ui/FolderModeSmsViewer$5;->val$sender:Lcom/android/mms/transaction/MessageSender;

    const-wide/16 v1, -0x1

    invoke-interface {v0, v1, v2}, Lcom/android/mms/transaction/MessageSender;->sendMessage(J)Z
    :try_end_0
    .catch Lcom/google/android/mms/MmsException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-static {}, Lcom/android/mms/util/Recycler;->getSmsRecycler()Lcom/android/mms/util/Recycler$SmsRecycler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mms/ui/FolderModeSmsViewer$5;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/mms/ui/FolderModeSmsViewer$5;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-static {v2}, Lcom/android/mms/ui/FolderModeSmsViewer;->access$1500(Lcom/android/mms/ui/FolderModeSmsViewer;)I

    move-result v2

    int-to-long v4, v2

    invoke-virtual {v0, v1, v4, v5}, Lcom/android/mms/util/Recycler;->deleteOldMessagesByThreadId(Landroid/content/Context;J)V

    return-void

    :catch_0
    move-exception v6

    const-string v0, "Mms/FolderModeSmsViewer"

    const-string v1, "Can\'t resend mms."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
