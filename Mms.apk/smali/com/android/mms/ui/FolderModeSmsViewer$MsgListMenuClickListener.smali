.class final Lcom/android/mms/ui/FolderModeSmsViewer$MsgListMenuClickListener;
.super Ljava/lang/Object;
.source "FolderModeSmsViewer.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/ui/FolderModeSmsViewer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "MsgListMenuClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/FolderModeSmsViewer;


# direct methods
.method private constructor <init>(Lcom/android/mms/ui/FolderModeSmsViewer;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/FolderModeSmsViewer$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/mms/ui/FolderModeSmsViewer;Lcom/android/mms/ui/FolderModeSmsViewer$1;)V
    .locals 0
    .param p1    # Lcom/android/mms/ui/FolderModeSmsViewer;
    .param p2    # Lcom/android/mms/ui/FolderModeSmsViewer$1;

    invoke-direct {p0, p1}, Lcom/android/mms/ui/FolderModeSmsViewer$MsgListMenuClickListener;-><init>(Lcom/android/mms/ui/FolderModeSmsViewer;)V

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 14
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v9

    sparse-switch v9, :sswitch_data_0

    const/4 v9, 0x0

    :goto_0
    return v9

    :sswitch_0
    iget-object v9, p0, Lcom/android/mms/ui/FolderModeSmsViewer$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    iget-object v10, p0, Lcom/android/mms/ui/FolderModeSmsViewer$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-static {v10}, Lcom/android/mms/ui/FolderModeSmsViewer;->access$300(Lcom/android/mms/ui/FolderModeSmsViewer;)J

    move-result-wide v10

    const/4 v12, 0x1

    invoke-static {v9, v10, v11, v12}, Lcom/android/mms/ui/FolderModeSmsViewer;->access$400(Lcom/android/mms/ui/FolderModeSmsViewer;JZ)V

    const/4 v9, 0x1

    goto :goto_0

    :sswitch_1
    iget-object v9, p0, Lcom/android/mms/ui/FolderModeSmsViewer$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    iget-object v10, p0, Lcom/android/mms/ui/FolderModeSmsViewer$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-static {v10}, Lcom/android/mms/ui/FolderModeSmsViewer;->access$300(Lcom/android/mms/ui/FolderModeSmsViewer;)J

    move-result-wide v10

    const/4 v12, 0x0

    invoke-static {v9, v10, v11, v12}, Lcom/android/mms/ui/FolderModeSmsViewer;->access$400(Lcom/android/mms/ui/FolderModeSmsViewer;JZ)V

    const/4 v9, 0x1

    goto :goto_0

    :sswitch_2
    iget-object v9, p0, Lcom/android/mms/ui/FolderModeSmsViewer$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    new-instance v10, Lcom/android/mms/ui/FolderModeSmsViewer$SaveMsgThread;

    iget-object v11, p0, Lcom/android/mms/ui/FolderModeSmsViewer$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    iget-object v12, p0, Lcom/android/mms/ui/FolderModeSmsViewer$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-static {v12}, Lcom/android/mms/ui/FolderModeSmsViewer;->access$300(Lcom/android/mms/ui/FolderModeSmsViewer;)J

    move-result-wide v12

    invoke-direct {v10, v11, v12, v13}, Lcom/android/mms/ui/FolderModeSmsViewer$SaveMsgThread;-><init>(Lcom/android/mms/ui/FolderModeSmsViewer;J)V

    invoke-static {v9, v10}, Lcom/android/mms/ui/FolderModeSmsViewer;->access$502(Lcom/android/mms/ui/FolderModeSmsViewer;Ljava/lang/Thread;)Ljava/lang/Thread;

    iget-object v9, p0, Lcom/android/mms/ui/FolderModeSmsViewer$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-static {v9}, Lcom/android/mms/ui/FolderModeSmsViewer;->access$500(Lcom/android/mms/ui/FolderModeSmsViewer;)Ljava/lang/Thread;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Thread;->start()V

    const/4 v9, 0x1

    goto :goto_0

    :sswitch_3
    const/4 v6, -0x1

    iget-object v9, p0, Lcom/android/mms/ui/FolderModeSmsViewer$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-virtual {v9}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v9

    iget-object v10, p0, Lcom/android/mms/ui/FolderModeSmsViewer$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-static {v10}, Lcom/android/mms/ui/FolderModeSmsViewer;->access$600(Lcom/android/mms/ui/FolderModeSmsViewer;)I

    move-result v10

    int-to-long v10, v10

    invoke-static {v9, v10, v11}, Landroid/provider/Telephony$SIMInfo;->getSlotById(Landroid/content/Context;J)I

    move-result v7

    const-string v9, "Mms/FolderModeSmsViewer"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "slot is:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ",simId:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/android/mms/ui/FolderModeSmsViewer$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-static {v11}, Lcom/android/mms/ui/FolderModeSmsViewer;->access$600(Lcom/android/mms/ui/FolderModeSmsViewer;)I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-ltz v7, :cond_0

    iget-object v9, p0, Lcom/android/mms/ui/FolderModeSmsViewer$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-static {v9}, Lcom/android/mms/ui/FolderModeSmsViewer;->access$600(Lcom/android/mms/ui/FolderModeSmsViewer;)I

    move-result v6

    :cond_0
    iget-object v9, p0, Lcom/android/mms/ui/FolderModeSmsViewer$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-virtual {v9}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v9

    iget-object v10, p0, Lcom/android/mms/ui/FolderModeSmsViewer$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-static {v10}, Lcom/android/mms/ui/FolderModeSmsViewer;->access$700(Lcom/android/mms/ui/FolderModeSmsViewer;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10, v6}, Lcom/android/mms/ui/MessageUtils;->replyMessage(Landroid/content/Context;Ljava/lang/String;I)V

    const/4 v9, 0x1

    goto/16 :goto_0

    :sswitch_4
    iget-object v9, p0, Lcom/android/mms/ui/FolderModeSmsViewer$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-static {v9}, Lcom/android/mms/ui/FolderModeSmsViewer;->access$800(Lcom/android/mms/ui/FolderModeSmsViewer;)V

    const/4 v9, 0x1

    goto/16 :goto_0

    :sswitch_5
    iget-object v9, p0, Lcom/android/mms/ui/FolderModeSmsViewer$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-static {v9}, Lcom/android/mms/ui/FolderModeSmsViewer;->access$900(Lcom/android/mms/ui/FolderModeSmsViewer;)V

    const/4 v9, 0x1

    goto/16 :goto_0

    :sswitch_6
    iget-object v9, p0, Lcom/android/mms/ui/FolderModeSmsViewer$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    iget-object v10, p0, Lcom/android/mms/ui/FolderModeSmsViewer$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-static {v10}, Lcom/android/mms/ui/FolderModeSmsViewer;->access$1000(Lcom/android/mms/ui/FolderModeSmsViewer;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/android/mms/ui/FolderModeSmsViewer;->access$1100(Lcom/android/mms/ui/FolderModeSmsViewer;Ljava/lang/String;)V

    const/4 v9, 0x1

    goto/16 :goto_0

    :sswitch_7
    iget-object v9, p0, Lcom/android/mms/ui/FolderModeSmsViewer$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-static {v9}, Lcom/android/mms/ui/FolderModeSmsViewer;->access$1200(Lcom/android/mms/ui/FolderModeSmsViewer;)Ljava/lang/String;

    move-result-object v5

    new-instance v9, Landroid/app/AlertDialog$Builder;

    iget-object v10, p0, Lcom/android/mms/ui/FolderModeSmsViewer$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-direct {v9, v10}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v10, 0x7f0a01c2

    invoke-virtual {v9, v10}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v9

    invoke-virtual {v9, v5}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v9

    const v10, 0x104000a

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v9

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v9

    invoke-virtual {v9}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    const/4 v9, 0x1

    goto/16 :goto_0

    :sswitch_8
    iget-object v9, p0, Lcom/android/mms/ui/FolderModeSmsViewer$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-static {v9}, Lcom/android/mms/ui/FolderModeSmsViewer;->access$1300(Lcom/android/mms/ui/FolderModeSmsViewer;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_2

    iget-object v10, p0, Lcom/android/mms/ui/FolderModeSmsViewer$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    const/4 v11, 0x0

    iget-object v9, p0, Lcom/android/mms/ui/FolderModeSmsViewer$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-static {v9}, Lcom/android/mms/ui/FolderModeSmsViewer;->access$1300(Lcom/android/mms/ui/FolderModeSmsViewer;)Ljava/util/ArrayList;

    move-result-object v9

    const/4 v12, 0x0

    invoke-virtual {v9, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-static {v10, v11, v9}, Landroid/provider/Browser;->saveBookmark(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_1
    const/4 v9, 0x1

    goto/16 :goto_0

    :cond_2
    iget-object v9, p0, Lcom/android/mms/ui/FolderModeSmsViewer$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-static {v9}, Lcom/android/mms/ui/FolderModeSmsViewer;->access$1300(Lcom/android/mms/ui/FolderModeSmsViewer;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    const/4 v10, 0x1

    if-le v9, v10, :cond_1

    iget-object v9, p0, Lcom/android/mms/ui/FolderModeSmsViewer$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-static {v9}, Lcom/android/mms/ui/FolderModeSmsViewer;->access$1300(Lcom/android/mms/ui/FolderModeSmsViewer;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    new-array v4, v9, [Ljava/lang/CharSequence;

    const/4 v3, 0x0

    :goto_2
    iget-object v9, p0, Lcom/android/mms/ui/FolderModeSmsViewer$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-static {v9}, Lcom/android/mms/ui/FolderModeSmsViewer;->access$1300(Lcom/android/mms/ui/FolderModeSmsViewer;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v3, v9, :cond_3

    iget-object v9, p0, Lcom/android/mms/ui/FolderModeSmsViewer$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-static {v9}, Lcom/android/mms/ui/FolderModeSmsViewer;->access$1300(Lcom/android/mms/ui/FolderModeSmsViewer;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/CharSequence;

    aput-object v9, v4, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_3
    new-instance v9, Landroid/app/AlertDialog$Builder;

    iget-object v10, p0, Lcom/android/mms/ui/FolderModeSmsViewer$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-direct {v9, v10}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v10, 0x7f0a0026

    invoke-virtual {v9, v10}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v9

    const v10, 0x7f020059

    invoke-virtual {v9, v10}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v9

    new-instance v10, Lcom/android/mms/ui/FolderModeSmsViewer$MsgListMenuClickListener$1;

    invoke-direct {v10, p0}, Lcom/android/mms/ui/FolderModeSmsViewer$MsgListMenuClickListener$1;-><init>(Lcom/android/mms/ui/FolderModeSmsViewer$MsgListMenuClickListener;)V

    invoke-virtual {v9, v4, v10}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v9

    invoke-virtual {v9}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_1

    :sswitch_9
    new-instance v9, Landroid/app/AlertDialog$Builder;

    iget-object v10, p0, Lcom/android/mms/ui/FolderModeSmsViewer$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-direct {v9, v10}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v10, 0x7f0a0196

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v9, 0x7f040002

    const/4 v10, 0x0

    invoke-virtual {v2, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    const v9, 0x7f0e0007

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iget-object v9, p0, Lcom/android/mms/ui/FolderModeSmsViewer$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-static {v9}, Lcom/android/mms/ui/FolderModeSmsViewer;->access$1000(Lcom/android/mms/ui/FolderModeSmsViewer;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v8}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v9

    invoke-virtual {v9}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    const/4 v9, 0x1

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_3
        0x3 -> :sswitch_4
        0x11 -> :sswitch_7
        0x12 -> :sswitch_5
        0x18 -> :sswitch_6
        0x1c -> :sswitch_0
        0x1d -> :sswitch_1
        0x1f -> :sswitch_2
        0x22 -> :sswitch_9
        0x23 -> :sswitch_8
    .end sparse-switch
.end method
