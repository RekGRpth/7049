.class Lcom/android/mms/ui/FolderViewList$5;
.super Ljava/lang/Object;
.source "FolderViewList.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/FolderViewList;->confirmDeleteMessageDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/FolderViewList;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/FolderViewList;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/FolderViewList$5;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    iget-object v1, p0, Lcom/android/mms/ui/FolderViewList$5;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v1}, Lcom/android/mms/ui/FolderViewList;->access$1000(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;

    move-result-object v1

    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$5;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v2}, Lcom/android/mms/ui/DeleteProgressDialogUtil;->getProgressDialog(Landroid/content/Context;)Lcom/android/mms/ui/NewProgressDialog;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/mms/ui/FolderViewList$BaseProgressQueryHandler;->setProgressDialog(Lcom/android/mms/ui/NewProgressDialog;)V

    iget-object v1, p0, Lcom/android/mms/ui/FolderViewList$5;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v1}, Lcom/android/mms/ui/FolderViewList;->access$1000(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/mms/ui/FolderViewList$BaseProgressQueryHandler;->showProgressDialog()V

    const/4 v0, 0x0

    sget v1, Lcom/android/mms/ui/FolderViewList;->mgViewID:I

    if-nez v1, :cond_1

    const-string v1, "content://mms-sms/folder_delete/"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-wide/16 v2, 0x1

    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/mms/ui/FolderViewList$5;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v1}, Lcom/android/mms/ui/FolderViewList;->access$1000(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;

    move-result-object v1

    const/16 v2, 0x3e9

    iget-object v3, p0, Lcom/android/mms/ui/FolderViewList$5;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v3}, Lcom/android/mms/ui/FolderViewList;->access$1100(Lcom/android/mms/ui/FolderViewList;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v0, v3}, Lcom/android/mms/data/FolderView;->startDeleteBoxMessage(Landroid/content/AsyncQueryHandler;ILandroid/net/Uri;Ljava/lang/String;)V

    return-void

    :cond_1
    sget v1, Lcom/android/mms/ui/FolderViewList;->mgViewID:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    const-string v1, "content://mms-sms/folder_delete/"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-wide/16 v2, 0x4

    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    :cond_2
    sget v1, Lcom/android/mms/ui/FolderViewList;->mgViewID:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    const-string v1, "content://mms-sms/folder_delete/"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-wide/16 v2, 0x3

    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    :cond_3
    sget v1, Lcom/android/mms/ui/FolderViewList;->mgViewID:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    const-string v1, "content://mms-sms/folder_delete/"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-wide/16 v2, 0x2

    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method
