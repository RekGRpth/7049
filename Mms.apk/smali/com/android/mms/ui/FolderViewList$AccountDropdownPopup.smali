.class Lcom/android/mms/ui/FolderViewList$AccountDropdownPopup;
.super Landroid/widget/ListPopupWindow;
.source "FolderViewList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/ui/FolderViewList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AccountDropdownPopup"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/FolderViewList;


# direct methods
.method public constructor <init>(Lcom/android/mms/ui/FolderViewList;Landroid/content/Context;)V
    .locals 1
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/android/mms/ui/FolderViewList$AccountDropdownPopup;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-direct {p0, p2}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/android/mms/ui/FolderViewList;->access$400(Lcom/android/mms/ui/FolderViewList;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/widget/ListPopupWindow;->setAnchorView(Landroid/view/View;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/widget/ListPopupWindow;->setModal(Z)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/ListPopupWindow;->setPromptPosition(I)V

    new-instance v0, Lcom/android/mms/ui/FolderViewList$AccountDropdownPopup$1;

    invoke-direct {v0, p0, p1}, Lcom/android/mms/ui/FolderViewList$AccountDropdownPopup$1;-><init>(Lcom/android/mms/ui/FolderViewList$AccountDropdownPopup;Lcom/android/mms/ui/FolderViewList;)V

    invoke-virtual {p0, v0}, Landroid/widget/ListPopupWindow;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method


# virtual methods
.method public show()V
    .locals 2

    const/16 v0, 0xc8

    invoke-virtual {p0, v0}, Landroid/widget/ListPopupWindow;->setWidth(I)V

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Landroid/widget/ListPopupWindow;->setInputMethodMode(I)V

    invoke-super {p0}, Landroid/widget/ListPopupWindow;->show()V

    invoke-virtual {p0}, Landroid/widget/ListPopupWindow;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setChoiceMode(I)V

    return-void
.end method
