.class final Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;
.super Lcom/android/mms/ui/FolderViewList$BaseProgressQueryHandler;
.source "FolderViewList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/ui/FolderViewList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ThreadListQueryHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/FolderViewList;


# direct methods
.method public constructor <init>(Lcom/android/mms/ui/FolderViewList;Landroid/content/ContentResolver;)V
    .locals 0
    .param p2    # Landroid/content/ContentResolver;

    iput-object p1, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-direct {p0, p2}, Lcom/android/mms/ui/FolderViewList$BaseProgressQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    return-void
.end method


# virtual methods
.method protected onDeleteComplete(ILjava/lang/Object;I)V
    .locals 5
    .param p1    # I
    .param p2    # Ljava/lang/Object;
    .param p3    # I

    const-wide/16 v3, -0x2

    const/4 v2, 0x0

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    invoke-static {}, Lcom/android/mms/ui/FolderViewList;->access$1900()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    invoke-static {}, Lcom/android/mms/ui/FolderViewList;->access$1910()I

    const-string v0, "this"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "igonre a onDeleteComplete,mDeleteCounter:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/android/mms/ui/FolderViewList;->access$1900()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    invoke-static {v2}, Lcom/android/mms/ui/FolderViewList;->access$1902(I)I

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v0, v3, v4, v2}, Lcom/android/mms/transaction/MessagingNotification;->nonBlockingUpdateNewMessageIndicator(Landroid/content/Context;JZ)V

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v0, v3, v4}, Lcom/android/mms/transaction/WapPushMessagingNotification;->nonBlockingUpdateNewMessageIndicator(Landroid/content/Context;J)V

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v0}, Lcom/android/mms/transaction/CBMessagingNotification;->updateAllNotifications(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v0}, Lcom/android/mms/ui/FolderViewList;->access$1500(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/mms/ui/FolderViewListAdapter;->clearbackupstate()V

    invoke-virtual {p0}, Lcom/android/mms/ui/FolderViewList$BaseProgressQueryHandler;->progress()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/mms/ui/FolderViewList$BaseProgressQueryHandler;->dismissProgressDialog()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_0
    .end packed-switch
.end method

.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 6
    .param p1    # I
    .param p2    # Ljava/lang/Object;
    .param p3    # Landroid/database/Cursor;

    const/4 v5, 0x0

    if-eqz p3, :cond_0

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    const-string v2, "FolderViewList"

    const-string v3, "cursor == null||count==0."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v2}, Lcom/android/mms/ui/FolderViewList;->access$1400(Lcom/android/mms/ui/FolderViewList;)Landroid/widget/TextView;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    if-eqz p3, :cond_1

    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v2}, Lcom/android/mms/ui/FolderViewList;->access$1500(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewListAdapter;

    move-result-object v2

    invoke-virtual {v2, p3}, Landroid/widget/CursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    :cond_1
    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-virtual {v2, v5}, Landroid/app/Activity;->setProgressBarIndeterminateVisibility(Z)V

    :goto_0
    return-void

    :cond_2
    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v2}, Lcom/android/mms/ui/FolderViewList;->access$1600(Lcom/android/mms/ui/FolderViewList;)Landroid/widget/SearchView;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v2}, Lcom/android/mms/ui/FolderViewList;->access$1600(Lcom/android/mms/ui/FolderViewList;)Landroid/widget/SearchView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_3

    const-string v2, "FolderViewList"

    const-string v3, "onQueryComplete mSearchView != null"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v2}, Lcom/android/mms/ui/FolderViewList;->access$1600(Lcom/android/mms/ui/FolderViewList;)Landroid/widget/SearchView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/SearchView;->getSuggestionsAdapter()Landroid/widget/CursorAdapter;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    :cond_3
    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v2}, Lcom/android/mms/ui/FolderViewList;->access$1400(Lcom/android/mms/ui/FolderViewList;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    sparse-switch p1, :sswitch_data_0

    const-string v2, "FolderViewList"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onQueryComplete called with unknown token "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    :goto_1
    :sswitch_0
    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v2}, Lcom/android/mms/ui/FolderViewList;->access$1800(Lcom/android/mms/ui/FolderViewList;)Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-virtual {v2}, Landroid/app/Activity;->invalidateOptionsMenu()V

    const-string v2, "FolderViewList"

    const-string v3, "onQueryComplete invalidateOptionsMenu"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-virtual {v2, v5}, Landroid/app/Activity;->setProgressBarIndeterminateVisibility(Z)V

    goto :goto_0

    :sswitch_1
    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v2}, Lcom/android/mms/ui/FolderViewList;->access$1400(Lcom/android/mms/ui/FolderViewList;)Landroid/widget/TextView;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v2, "FolderViewList"

    const-string v3, "onQueryComplete DRAFTFOLDER_LIST_QUERY_TOKEN"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v2}, Lcom/android/mms/ui/FolderViewList;->access$1500(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewListAdapter;

    move-result-object v2

    invoke-virtual {v2, p3}, Landroid/widget/CursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v2}, Lcom/android/mms/ui/FolderViewList;->access$1700(Lcom/android/mms/ui/FolderViewList;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v2, v5}, Lcom/android/mms/ui/FolderViewList;->access$1702(Lcom/android/mms/ui/FolderViewList;Z)Z

    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-virtual {v2}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v5}, Lcom/android/mms/data/Conversation;->markAllConversationsAsSeen(Landroid/content/Context;I)V

    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v2}, Lcom/android/mms/ui/FolderViewList;->access$1300(Lcom/android/mms/ui/FolderViewList;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v3}, Lcom/android/mms/ui/FolderViewList;->access$1200(Lcom/android/mms/ui/FolderViewList;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    :sswitch_2
    const/4 v0, 0x0

    :cond_6
    :goto_2
    invoke-interface {p3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_7

    const/4 v2, 0x5

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-nez v2, :cond_6

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_7
    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v2}, Lcom/android/mms/ui/FolderViewList;->access$1400(Lcom/android/mms/ui/FolderViewList;)Landroid/widget/TextView;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v2, "FolderViewList"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onQueryComplete INBOXFOLDER_LIST_QUERY_TOKEN count "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v2}, Lcom/android/mms/ui/FolderViewList;->access$1500(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewListAdapter;

    move-result-object v2

    invoke-virtual {v2, p3}, Landroid/widget/CursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    goto/16 :goto_1

    :sswitch_3
    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v2}, Lcom/android/mms/ui/FolderViewList;->access$1400(Lcom/android/mms/ui/FolderViewList;)Landroid/widget/TextView;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v2, "FolderViewList"

    const-string v3, "onQueryComplete OUTBOXFOLDER_LIST_QUERY_TOKEN"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v2}, Lcom/android/mms/ui/FolderViewList;->access$1500(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewListAdapter;

    move-result-object v2

    invoke-virtual {v2, p3}, Landroid/widget/CursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    goto/16 :goto_1

    :sswitch_4
    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v2}, Lcom/android/mms/ui/FolderViewList;->access$1400(Lcom/android/mms/ui/FolderViewList;)Landroid/widget/TextView;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v2, "FolderViewList"

    const-string v3, "onQueryComplete SENTFOLDER_LIST_QUERY_TOKEN"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v2}, Lcom/android/mms/ui/FolderViewList;->access$1500(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewListAdapter;

    move-result-object v2

    invoke-virtual {v2, p3}, Landroid/widget/CursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x3ea -> :sswitch_0
        0x3f1 -> :sswitch_1
        0x457 -> :sswitch_2
        0x461 -> :sswitch_3
        0x46b -> :sswitch_4
    .end sparse-switch
.end method
