.class Lcom/android/mms/ui/ImportSmsActivity$8;
.super Ljava/lang/Thread;
.source "ImportSmsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/ImportSmsActivity;->importMessages(Ljava/lang/String;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/ImportSmsActivity;

.field final synthetic val$key:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/ImportSmsActivity;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/ImportSmsActivity$8;->this$0:Lcom/android/mms/ui/ImportSmsActivity;

    iput-object p2, p0, Lcom/android/mms/ui/ImportSmsActivity$8;->val$key:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 19

    const/4 v11, 0x0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mms/ui/ImportSmsActivity$8;->this$0:Lcom/android/mms/ui/ImportSmsActivity;

    invoke-virtual {v2}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "storage"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Landroid/os/storage/StorageManager;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/storage/StorageManager;->getDefaultPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "//message//"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/ImportSmsActivity$8;->val$key:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->openDatabase(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "sms"

    invoke-static {}, Lcom/android/mms/ui/ImportSmsActivity;->access$400()[Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v8, "date ASC"

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    if-nez v11, :cond_2

    const-string v2, "MMS/ImportSmsActivity"

    const-string v3, "importDict sms cursor is null "

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mms/ui/ImportSmsActivity$8;->this$0:Lcom/android/mms/ui/ImportSmsActivity;

    iget-object v2, v2, Lcom/android/mms/ui/ImportSmsActivity;->mMainHandler:Landroid/os/Handler;

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v11, :cond_0

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mms/ui/ImportSmsActivity$8;->this$0:Lcom/android/mms/ui/ImportSmsActivity;

    invoke-static {v2}, Lcom/android/mms/ui/ImportSmsActivity;->access$500(Lcom/android/mms/ui/ImportSmsActivity;)Landroid/app/ProgressDialog;

    move-result-object v2

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mms/ui/ImportSmsActivity$8;->this$0:Lcom/android/mms/ui/ImportSmsActivity;

    invoke-static {v2}, Lcom/android/mms/ui/ImportSmsActivity;->access$500(Lcom/android/mms/ui/ImportSmsActivity;)Landroid/app/ProgressDialog;

    move-result-object v2

    :goto_0
    invoke-virtual {v2}, Landroid/app/Dialog;->dismiss()V

    :cond_1
    return-void

    :cond_2
    :try_start_1
    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v10

    const-string v2, "MMS/ImportSmsActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "importDict sms count = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-lez v10, :cond_c

    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    const/4 v15, 0x0

    const/4 v13, 0x1

    :cond_3
    :goto_1
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_8

    if-eqz v13, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mms/ui/ImportSmsActivity$8;->this$0:Lcom/android/mms/ui/ImportSmsActivity;

    invoke-static {v2}, Lcom/android/mms/ui/ImportSmsActivity;->access$500(Lcom/android/mms/ui/ImportSmsActivity;)Landroid/app/ProgressDialog;

    move-result-object v2

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mms/ui/ImportSmsActivity$8;->this$0:Lcom/android/mms/ui/ImportSmsActivity;

    invoke-static {v2}, Lcom/android/mms/ui/ImportSmsActivity;->access$500(Lcom/android/mms/ui/ImportSmsActivity;)Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Dialog;->isShowing()Z

    move-result v2

    if-nez v2, :cond_4

    const/4 v13, 0x0

    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mms/ui/ImportSmsActivity$8;->this$0:Lcom/android/mms/ui/ImportSmsActivity;

    invoke-static {v2, v11}, Lcom/android/mms/ui/ImportSmsActivity;->access$600(Lcom/android/mms/ui/ImportSmsActivity;Landroid/database/Cursor;)Landroid/content/ContentValues;

    move-result-object v18

    sget-object v2, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v9

    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    invoke-virtual {v9}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v14, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v15, v15, 0x1

    rem-int/lit8 v2, v15, 0x14

    if-nez v2, :cond_3

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-lez v2, :cond_3

    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mms/ui/ImportSmsActivity$8;->this$0:Lcom/android/mms/ui/ImportSmsActivity;

    invoke-virtual {v2}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "sms"

    invoke-virtual {v2, v3, v14}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    const-string v2, "MMS/ImportSmsActivity"

    const-string v3, "apply end"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    invoke-virtual {v14}, Ljava/util/ArrayList;->clear()V
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :goto_2
    const/4 v15, 0x0

    goto :goto_1

    :catch_0
    move-exception v12

    :try_start_4
    const-string v2, "MMS/ImportSmsActivity"

    const-string v3, "%s: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v12}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v12}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    invoke-virtual {v14}, Ljava/util/ArrayList;->clear()V
    :try_end_5
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    :catch_1
    move-exception v12

    :try_start_6
    const-string v2, "MMS/ImportSmsActivity"

    const-string v3, "can\'t open the database file"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mms/ui/ImportSmsActivity$8;->this$0:Lcom/android/mms/ui/ImportSmsActivity;

    iget-object v2, v2, Lcom/android/mms/ui/ImportSmsActivity;->mMainHandler:Landroid/os/Handler;

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    if-eqz v11, :cond_5

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mms/ui/ImportSmsActivity$8;->this$0:Lcom/android/mms/ui/ImportSmsActivity;

    invoke-static {v2}, Lcom/android/mms/ui/ImportSmsActivity;->access$500(Lcom/android/mms/ui/ImportSmsActivity;)Landroid/app/ProgressDialog;

    move-result-object v2

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mms/ui/ImportSmsActivity$8;->this$0:Lcom/android/mms/ui/ImportSmsActivity;

    invoke-static {v2}, Lcom/android/mms/ui/ImportSmsActivity;->access$500(Lcom/android/mms/ui/ImportSmsActivity;)Landroid/app/ProgressDialog;

    move-result-object v2

    goto/16 :goto_0

    :catch_2
    move-exception v12

    :try_start_7
    const-string v2, "MMS/ImportSmsActivity"

    const-string v3, "%s: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v12}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v12}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    invoke-virtual {v14}, Ljava/util/ArrayList;->clear()V
    :try_end_8
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v2

    if-eqz v11, :cond_6

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/ImportSmsActivity$8;->this$0:Lcom/android/mms/ui/ImportSmsActivity;

    invoke-static {v3}, Lcom/android/mms/ui/ImportSmsActivity;->access$500(Lcom/android/mms/ui/ImportSmsActivity;)Landroid/app/ProgressDialog;

    move-result-object v3

    if-eqz v3, :cond_7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/ImportSmsActivity$8;->this$0:Lcom/android/mms/ui/ImportSmsActivity;

    invoke-static {v3}, Lcom/android/mms/ui/ImportSmsActivity;->access$500(Lcom/android/mms/ui/ImportSmsActivity;)Landroid/app/ProgressDialog;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Dialog;->dismiss()V

    :cond_7
    throw v2

    :catchall_1
    move-exception v2

    :try_start_9
    invoke-virtual {v14}, Ljava/util/ArrayList;->clear()V

    const/4 v15, 0x0

    throw v2

    :cond_8
    if-lez v15, :cond_9

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I
    :try_end_9
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move-result v2

    if-lez v2, :cond_9

    :try_start_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mms/ui/ImportSmsActivity$8;->this$0:Lcom/android/mms/ui/ImportSmsActivity;

    invoke-virtual {v2}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "sms"

    invoke-virtual {v2, v3, v14}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_a} :catch_3
    .catch Landroid/content/OperationApplicationException; {:try_start_a .. :try_end_a} :catch_4
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    :try_start_b
    invoke-virtual {v14}, Ljava/util/ArrayList;->clear()V

    :goto_3
    const/4 v15, 0x0

    :cond_9
    const-string v2, "MMS/ImportSmsActivity"

    const-string v3, "import message success"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mms/ui/ImportSmsActivity$8;->this$0:Lcom/android/mms/ui/ImportSmsActivity;

    invoke-static {v2}, Lcom/android/mms/ui/ImportSmsActivity;->access$500(Lcom/android/mms/ui/ImportSmsActivity;)Landroid/app/ProgressDialog;

    move-result-object v2

    if-eqz v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mms/ui/ImportSmsActivity$8;->this$0:Lcom/android/mms/ui/ImportSmsActivity;

    invoke-static {v2}, Lcom/android/mms/ui/ImportSmsActivity;->access$500(Lcom/android/mms/ui/ImportSmsActivity;)Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Dialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mms/ui/ImportSmsActivity$8;->this$0:Lcom/android/mms/ui/ImportSmsActivity;

    iget-object v2, v2, Lcom/android/mms/ui/ImportSmsActivity;->mMainHandler:Landroid/os/Handler;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_a
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteClosable;->close()V
    :try_end_b
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    if-eqz v11, :cond_b

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mms/ui/ImportSmsActivity$8;->this$0:Lcom/android/mms/ui/ImportSmsActivity;

    invoke-static {v2}, Lcom/android/mms/ui/ImportSmsActivity;->access$500(Lcom/android/mms/ui/ImportSmsActivity;)Landroid/app/ProgressDialog;

    move-result-object v2

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mms/ui/ImportSmsActivity$8;->this$0:Lcom/android/mms/ui/ImportSmsActivity;

    invoke-static {v2}, Lcom/android/mms/ui/ImportSmsActivity;->access$500(Lcom/android/mms/ui/ImportSmsActivity;)Landroid/app/ProgressDialog;

    move-result-object v2

    goto/16 :goto_0

    :catch_3
    move-exception v12

    :try_start_c
    const-string v2, "MMS/ImportSmsActivity"

    const-string v3, "%s: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v12}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v12}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    :try_start_d
    invoke-virtual {v14}, Ljava/util/ArrayList;->clear()V
    :try_end_d
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_d .. :try_end_d} :catch_1
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    goto :goto_3

    :catch_4
    move-exception v12

    :try_start_e
    const-string v2, "MMS/ImportSmsActivity"

    const-string v3, "%s: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v12}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v12}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    :try_start_f
    invoke-virtual {v14}, Ljava/util/ArrayList;->clear()V

    goto/16 :goto_3

    :catchall_2
    move-exception v2

    invoke-virtual {v14}, Ljava/util/ArrayList;->clear()V

    const/4 v15, 0x0

    throw v2

    :cond_c
    const-string v2, "MMS/ImportSmsActivity"

    const-string v3, "importDict sms is empty"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_f
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_f .. :try_end_f} :catch_1
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    if-eqz v11, :cond_d

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mms/ui/ImportSmsActivity$8;->this$0:Lcom/android/mms/ui/ImportSmsActivity;

    invoke-static {v2}, Lcom/android/mms/ui/ImportSmsActivity;->access$500(Lcom/android/mms/ui/ImportSmsActivity;)Landroid/app/ProgressDialog;

    move-result-object v2

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mms/ui/ImportSmsActivity$8;->this$0:Lcom/android/mms/ui/ImportSmsActivity;

    invoke-static {v2}, Lcom/android/mms/ui/ImportSmsActivity;->access$500(Lcom/android/mms/ui/ImportSmsActivity;)Landroid/app/ProgressDialog;

    move-result-object v2

    goto/16 :goto_0
.end method
