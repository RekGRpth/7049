.class public Lcom/android/mms/ui/LongPressGuideLineActivity;
.super Landroid/app/Activity;
.source "LongPressGuideLineActivity.java"


# static fields
.field protected static GUIDE_LINE_DISMISS:Ljava/lang/String;


# instance fields
.field private CURRENT_FILPPER:Ljava/lang/String;

.field private mCurrentFlipper:I

.field private mFlipper:Landroid/widget/ViewFlipper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "com.android.mms_guide_line_dismiss"

    sput-object v0, Lcom/android/mms/ui/LongPressGuideLineActivity;->GUIDE_LINE_DISMISS:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/mms/ui/LongPressGuideLineActivity;->mCurrentFlipper:I

    const-string v0, "current_flipper"

    iput-object v0, p0, Lcom/android/mms/ui/LongPressGuideLineActivity;->CURRENT_FILPPER:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public dismissAll(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public onBackPressed()V
    .locals 0

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f040021

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    const v0, 0x7f0e007d

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewFlipper;

    iput-object v0, p0, Lcom/android/mms/ui/LongPressGuideLineActivity;->mFlipper:Landroid/widget/ViewFlipper;

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/android/mms/ui/LongPressGuideLineActivity;->CURRENT_FILPPER:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/android/mms/ui/LongPressGuideLineActivity;->mCurrentFlipper:I

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/android/mms/ui/LongPressGuideLineActivity;->mCurrentFlipper:I

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/mms/ui/LongPressGuideLineActivity;->mFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v1}, Landroid/widget/ViewAnimator;->showNext()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/mms/ui/LongPressGuideLineActivity;->CURRENT_FILPPER:Ljava/lang/String;

    iget v1, p0, Lcom/android/mms/ui/LongPressGuideLineActivity;->mCurrentFlipper:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method public showNext(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/android/mms/ui/LongPressGuideLineActivity;->mFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v0}, Landroid/widget/ViewAnimator;->showNext()V

    iget v0, p0, Lcom/android/mms/ui/LongPressGuideLineActivity;->mCurrentFlipper:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/mms/ui/LongPressGuideLineActivity;->mCurrentFlipper:I

    return-void
.end method
