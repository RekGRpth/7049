.class Lcom/android/mms/ui/ManageSimMessages$QueryHandler$1;
.super Ljava/lang/Object;
.source "ManageSimMessages.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/ManageSimMessages$QueryHandler;->onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/mms/ui/ManageSimMessages$QueryHandler;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/ManageSimMessages$QueryHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/ManageSimMessages$QueryHandler$1;->this$1:Lcom/android/mms/ui/ManageSimMessages$QueryHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 10
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    const/4 v9, 0x1

    const/4 v8, 0x0

    if-eqz p2, :cond_2

    move-object v2, p2

    check-cast v2, Lcom/android/mms/ui/MessageListItem;

    iget-object v5, v2, Lcom/android/mms/ui/MessageListItem;->mSelectedBox:Landroid/widget/CheckBox;

    if-eqz v5, :cond_4

    iget-object v5, v2, Lcom/android/mms/ui/MessageListItem;->mSelectedBox:Landroid/widget/CheckBox;

    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v5

    if-nez v5, :cond_4

    iget-object v5, v2, Lcom/android/mms/ui/MessageListItem;->mSelectedBox:Landroid/widget/CheckBox;

    invoke-virtual {v5}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v2, v9}, Lcom/android/mms/ui/MessageListItem;->setSelectedBackGroud(Z)V

    :goto_0
    iget-object v5, p0, Lcom/android/mms/ui/ManageSimMessages$QueryHandler$1;->this$1:Lcom/android/mms/ui/ManageSimMessages$QueryHandler;

    iget-object v5, v5, Lcom/android/mms/ui/ManageSimMessages$QueryHandler;->this$0:Lcom/android/mms/ui/ManageSimMessages;

    invoke-static {v5}, Lcom/android/mms/ui/ManageSimMessages;->access$400(Lcom/android/mms/ui/ManageSimMessages;)Lcom/android/mms/ui/MessageListAdapter;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    const-string v5, "index_on_icc"

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v5, "Mms/Txn"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "simMsg msgIndex = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, ";"

    invoke-virtual {v3, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x0

    :goto_1
    array-length v5, v1

    if-ge v4, v5, :cond_1

    iget-object v5, p0, Lcom/android/mms/ui/ManageSimMessages$QueryHandler$1;->this$1:Lcom/android/mms/ui/ManageSimMessages$QueryHandler;

    iget-object v5, v5, Lcom/android/mms/ui/ManageSimMessages$QueryHandler;->this$0:Lcom/android/mms/ui/ManageSimMessages;

    invoke-static {v5}, Lcom/android/mms/ui/ManageSimMessages;->access$400(Lcom/android/mms/ui/ManageSimMessages;)Lcom/android/mms/ui/MessageListAdapter;

    move-result-object v5

    aget-object v6, v1, v4

    invoke-virtual {v5, v6}, Lcom/android/mms/ui/MessageListAdapter;->changeSelectedState(Ljava/lang/String;)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_0
    invoke-virtual {v2, v8}, Lcom/android/mms/ui/MessageListItem;->setSelectedBackGroud(Z)V

    goto :goto_0

    :cond_1
    iget-object v5, p0, Lcom/android/mms/ui/ManageSimMessages$QueryHandler$1;->this$1:Lcom/android/mms/ui/ManageSimMessages$QueryHandler;

    iget-object v5, v5, Lcom/android/mms/ui/ManageSimMessages$QueryHandler;->this$0:Lcom/android/mms/ui/ManageSimMessages;

    invoke-static {v5}, Lcom/android/mms/ui/ManageSimMessages;->access$400(Lcom/android/mms/ui/ManageSimMessages;)Lcom/android/mms/ui/MessageListAdapter;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/mms/ui/MessageListAdapter;->getSelectedNumber()I

    move-result v5

    if-lez v5, :cond_3

    iget-object v5, p0, Lcom/android/mms/ui/ManageSimMessages$QueryHandler$1;->this$1:Lcom/android/mms/ui/ManageSimMessages$QueryHandler;

    iget-object v5, v5, Lcom/android/mms/ui/ManageSimMessages$QueryHandler;->this$0:Lcom/android/mms/ui/ManageSimMessages;

    invoke-static {v5}, Lcom/android/mms/ui/ManageSimMessages;->access$800(Lcom/android/mms/ui/ManageSimMessages;)Landroid/widget/ImageButton;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/view/View;->setEnabled(Z)V

    :cond_2
    :goto_2
    return-void

    :cond_3
    iget-object v5, p0, Lcom/android/mms/ui/ManageSimMessages$QueryHandler$1;->this$1:Lcom/android/mms/ui/ManageSimMessages$QueryHandler;

    iget-object v5, v5, Lcom/android/mms/ui/ManageSimMessages$QueryHandler;->this$0:Lcom/android/mms/ui/ManageSimMessages;

    invoke-static {v5}, Lcom/android/mms/ui/ManageSimMessages;->access$800(Lcom/android/mms/ui/ManageSimMessages;)Landroid/widget/ImageButton;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_2

    :cond_4
    invoke-virtual {v2}, Lcom/android/mms/ui/MessageListItem;->onMessageListItemClick()V

    goto :goto_2
.end method
