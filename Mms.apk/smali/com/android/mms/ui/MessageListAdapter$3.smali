.class Lcom/android/mms/ui/MessageListAdapter$3;
.super Ljava/lang/Object;
.source "MessageListAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/MessageListAdapter;->getCachedMessageItem(Ljava/lang/String;JLandroid/database/Cursor;)Lcom/android/mms/ui/MessageItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/MessageListAdapter;

.field final synthetic val$boxId:I

.field final synthetic val$charset:I

.field final synthetic val$deliveryReport:Ljava/lang/String;

.field final synthetic val$errorType:I

.field final synthetic val$highlight:Ljava/util/regex/Pattern;

.field final synthetic val$key:J

.field final synthetic val$locked:I

.field final synthetic val$mMsgId:J

.field final synthetic val$messageType:I

.field final synthetic val$mmsType:Ljava/lang/String;

.field final synthetic val$object:Ljava/lang/Object;

.field final synthetic val$r:Ljava/lang/Runnable;

.field final synthetic val$readReport:Ljava/lang/String;

.field final synthetic val$serviceCenter:Ljava/lang/String;

.field final synthetic val$simId:I

.field final synthetic val$subject:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/MessageListAdapter;JIIIIIIJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/regex/Pattern;Ljava/lang/Runnable;Ljava/lang/Object;)V
    .locals 1

    iput-object p1, p0, Lcom/android/mms/ui/MessageListAdapter$3;->this$0:Lcom/android/mms/ui/MessageListAdapter;

    iput-wide p2, p0, Lcom/android/mms/ui/MessageListAdapter$3;->val$key:J

    iput p4, p0, Lcom/android/mms/ui/MessageListAdapter$3;->val$boxId:I

    iput p5, p0, Lcom/android/mms/ui/MessageListAdapter$3;->val$messageType:I

    iput p6, p0, Lcom/android/mms/ui/MessageListAdapter$3;->val$simId:I

    iput p7, p0, Lcom/android/mms/ui/MessageListAdapter$3;->val$errorType:I

    iput p8, p0, Lcom/android/mms/ui/MessageListAdapter$3;->val$locked:I

    iput p9, p0, Lcom/android/mms/ui/MessageListAdapter$3;->val$charset:I

    iput-wide p10, p0, Lcom/android/mms/ui/MessageListAdapter$3;->val$mMsgId:J

    iput-object p12, p0, Lcom/android/mms/ui/MessageListAdapter$3;->val$mmsType:Ljava/lang/String;

    iput-object p13, p0, Lcom/android/mms/ui/MessageListAdapter$3;->val$subject:Ljava/lang/String;

    iput-object p14, p0, Lcom/android/mms/ui/MessageListAdapter$3;->val$serviceCenter:Ljava/lang/String;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/android/mms/ui/MessageListAdapter$3;->val$deliveryReport:Ljava/lang/String;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/android/mms/ui/MessageListAdapter$3;->val$readReport:Ljava/lang/String;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/android/mms/ui/MessageListAdapter$3;->val$highlight:Ljava/util/regex/Pattern;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/android/mms/ui/MessageListAdapter$3;->val$r:Ljava/lang/Runnable;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/android/mms/ui/MessageListAdapter$3;->val$object:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 18

    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mms/ui/MessageListAdapter$3;->this$0:Lcom/android/mms/ui/MessageListAdapter;

    invoke-static {v2}, Lcom/android/mms/ui/MessageListAdapter;->access$100(Lcom/android/mms/ui/MessageListAdapter;)Lcom/android/mms/ui/MessageListAdapter$MessageItemCache;

    move-result-object v2

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/android/mms/ui/MessageListAdapter$3;->val$key:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/mms/ui/MessageItem;

    if-nez v1, :cond_0

    new-instance v1, Lcom/android/mms/ui/MessageItem;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mms/ui/MessageListAdapter$3;->this$0:Lcom/android/mms/ui/MessageListAdapter;

    invoke-static {v2}, Lcom/android/mms/ui/MessageListAdapter;->access$200(Lcom/android/mms/ui/MessageListAdapter;)Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/mms/ui/MessageListAdapter$3;->val$boxId:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/mms/ui/MessageListAdapter$3;->val$messageType:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/mms/ui/MessageListAdapter$3;->val$simId:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/mms/ui/MessageListAdapter$3;->val$errorType:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/mms/ui/MessageListAdapter$3;->val$locked:I

    move-object/from16 v0, p0

    iget v8, v0, Lcom/android/mms/ui/MessageListAdapter$3;->val$charset:I

    move-object/from16 v0, p0

    iget-wide v9, v0, Lcom/android/mms/ui/MessageListAdapter$3;->val$mMsgId:J

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/mms/ui/MessageListAdapter$3;->val$mmsType:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/mms/ui/MessageListAdapter$3;->val$subject:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/mms/ui/MessageListAdapter$3;->val$serviceCenter:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mms/ui/MessageListAdapter$3;->val$deliveryReport:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/mms/ui/MessageListAdapter$3;->val$readReport:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mms/ui/MessageListAdapter$3;->val$highlight:Ljava/util/regex/Pattern;

    move-object/from16 v16, v0

    invoke-direct/range {v1 .. v16}, Lcom/android/mms/ui/MessageItem;-><init>(Landroid/content/Context;IIIIIIJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/regex/Pattern;)V

    const-string v2, "Mms/MessageItemCache"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getCachedMessageItem(): put new MessageItem into cache, messageId = -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, v1, Lcom/android/mms/ui/MessageItem;->mMsgId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mms/ui/MessageListAdapter$3;->this$0:Lcom/android/mms/ui/MessageListAdapter;

    invoke-static {v2}, Lcom/android/mms/ui/MessageListAdapter;->access$100(Lcom/android/mms/ui/MessageListAdapter;)Lcom/android/mms/ui/MessageListAdapter$MessageItemCache;

    move-result-object v2

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/android/mms/ui/MessageListAdapter$3;->val$key:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mms/ui/MessageListAdapter$3;->this$0:Lcom/android/mms/ui/MessageListAdapter;

    invoke-static {v2}, Lcom/android/mms/ui/MessageListAdapter;->access$000(Lcom/android/mms/ui/MessageListAdapter;)Landroid/os/Handler;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/MessageListAdapter$3;->val$r:Ljava/lang/Runnable;

    const-wide/16 v4, 0xc8

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/MessageListAdapter$3;->val$object:Ljava/lang/Object;

    monitor-enter v3
    :try_end_0
    .catch Lcom/google/android/mms/MmsException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mms/ui/MessageListAdapter$3;->val$object:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v3

    :goto_0
    return-void

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v2
    :try_end_2
    .catch Lcom/google/android/mms/MmsException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v17

    const-string v2, "MessageListAdapter"

    const-string v3, "getCachedMessageItem: "

    move-object/from16 v0, v17

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/MessageListAdapter$3;->val$object:Ljava/lang/Object;

    monitor-enter v3

    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mms/ui/MessageListAdapter$3;->val$object:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v3

    goto :goto_0

    :catchall_1
    move-exception v2

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v2
.end method
