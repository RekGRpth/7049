.class Lcom/android/mms/ui/MmsPlayerActivity$2;
.super Ljava/lang/Object;
.source "MmsPlayerActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/MmsPlayerActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/MmsPlayerActivity;

.field final synthetic val$sender:Lcom/android/mms/transaction/MessageSender;

.field final synthetic val$threadId:J


# direct methods
.method constructor <init>(Lcom/android/mms/ui/MmsPlayerActivity;Lcom/android/mms/transaction/MessageSender;J)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/MmsPlayerActivity$2;->this$0:Lcom/android/mms/ui/MmsPlayerActivity;

    iput-object p2, p0, Lcom/android/mms/ui/MmsPlayerActivity$2;->val$sender:Lcom/android/mms/transaction/MessageSender;

    iput-wide p3, p0, Lcom/android/mms/ui/MmsPlayerActivity$2;->val$threadId:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    :try_start_0
    iget-object v1, p0, Lcom/android/mms/ui/MmsPlayerActivity$2;->val$sender:Lcom/android/mms/transaction/MessageSender;

    iget-wide v2, p0, Lcom/android/mms/ui/MmsPlayerActivity$2;->val$threadId:J

    invoke-interface {v1, v2, v3}, Lcom/android/mms/transaction/MessageSender;->sendMessage(J)Z
    :try_end_0
    .catch Lcom/google/android/mms/MmsException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-static {}, Lcom/android/mms/util/Recycler;->getMmsRecycler()Lcom/android/mms/util/Recycler$MmsRecycler;

    move-result-object v1

    iget-object v2, p0, Lcom/android/mms/ui/MmsPlayerActivity$2;->this$0:Lcom/android/mms/ui/MmsPlayerActivity;

    invoke-virtual {v2}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-wide v3, p0, Lcom/android/mms/ui/MmsPlayerActivity$2;->val$threadId:J

    invoke-virtual {v1, v2, v3, v4}, Lcom/android/mms/util/Recycler;->deleteOldMessagesByThreadId(Landroid/content/Context;J)V

    return-void

    :catch_0
    move-exception v0

    const-string v1, "MmsPlayerActivity"

    const-string v2, "Can\'t resend mms."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
