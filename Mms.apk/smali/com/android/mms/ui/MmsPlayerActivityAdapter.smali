.class public Lcom/android/mms/ui/MmsPlayerActivityAdapter;
.super Landroid/widget/BaseAdapter;
.source "MmsPlayerActivityAdapter.java"


# static fields
.field private static final LOCAL_LOGV:Z = false

.field private static final TAG:Ljava/lang/String; = "Mms/MmsPlayerAdapter"


# instance fields
.field private mAllCount:I

.field private mContext:Landroid/content/Context;

.field private mDisplayMetrics:Landroid/util/DisplayMetrics;

.field private final mFactory:Landroid/view/LayoutInflater;

.field private mListItem:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/mms/ui/MmsPlayerActivityItemData;",
            ">;"
        }
    .end annotation
.end field

.field private mListItemViewCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private parser:Lcom/android/mms/util/SmileyParser;

.field private textSize:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/mms/ui/MmsPlayerActivityItemData;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->textSize:F

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->mListItemViewCache:Ljava/util/HashMap;

    invoke-static {}, Lcom/android/mms/util/SmileyParser;->getInstance()Lcom/android/mms/util/SmileyParser;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->parser:Lcom/android/mms/util/SmileyParser;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->mFactory:Landroid/view/LayoutInflater;

    iput-object p2, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->mListItem:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->mListItem:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->mAllCount:I

    iput-object p1, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    return-void
.end method


# virtual methods
.method public clearAllCache()V
    .locals 5

    iget-object v4, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->mListItemViewCache:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    if-lez v4, :cond_1

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->mListItemViewCache:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    iget-object v4, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->mListItemViewCache:Ljava/util/HashMap;

    invoke-virtual {v4, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    const v4, 0x7f0e0054

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/mediatek/banyan/widget/MTKImageView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/mediatek/banyan/widget/MTKImageView;->setImageURI(Landroid/net/Uri;)V

    goto :goto_0

    :cond_0
    iget-object v4, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->mListItemViewCache:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    :cond_1
    return-void
.end method

.method public getCount()I
    .locals 1

    iget v0, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->mAllCount:I

    return v0
.end method

.method public getItem(I)Lcom/android/mms/ui/MmsPlayerActivityItemData;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->mListItem:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mms/ui/MmsPlayerActivityItemData;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->getItem(I)Lcom/android/mms/ui/MmsPlayerActivityItemData;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getListItem()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/mms/ui/MmsPlayerActivityItemData;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->mListItem:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 26
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const-string v21, "Mms/MmsPlayerAdapter"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "getView, for position "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->mListItemViewCache:Ljava/util/HashMap;

    move-object/from16 v21, v0

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/mms/ui/MmsPlayerActivityItem;

    if-eqz v8, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->mListItem:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/android/mms/ui/MmsPlayerActivityItemData;

    invoke-virtual/range {v21 .. v21}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getText()Ljava/lang/String;

    move-result-object v19

    if-eqz v19, :cond_0

    invoke-virtual {v8}, Lcom/android/mms/ui/MmsPlayerActivityItem;->getCurrentTextView()Landroid/widget/TextView;

    move-result-object v16

    const-string v22, "Mms/MmsPlayerAdapter"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "getView(): text view is null? "

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    if-nez v16, :cond_1

    const/16 v21, 0x1

    :goto_0
    move-object/from16 v0, v23

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->textSize:F

    move/from16 v21, v0

    const/16 v22, 0x0

    cmpl-float v21, v21, v22

    if-eqz v21, :cond_0

    const-string v21, "Mms/MmsPlayerAdapter"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "getView(): before set text size, textSize = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v16 .. v16}, Landroid/widget/TextView;->getTextSize()F

    move-result v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->textSize:F

    move/from16 v21, v0

    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    const-string v21, "Mms/MmsPlayerAdapter"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "getView(): after set text size, textSize = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v16 .. v16}, Landroid/widget/TextView;->getTextSize()F

    move-result v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v21, "Mms/MmsPlayerAdapter"

    const-string v22, "getView(): from cache."

    invoke-static/range {v21 .. v22}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object v9, v8

    :goto_1
    return-object v9

    :cond_1
    const/16 v21, 0x0

    goto :goto_0

    :cond_2
    const-string v21, "Mms/MmsPlayerAdapter"

    const-string v22, "getView(): create new one."

    invoke-static/range {v21 .. v22}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->mFactory:Landroid/view/LayoutInflater;

    move-object/from16 v21, v0

    const v22, 0x7f040028

    const/16 v23, 0x0

    invoke-virtual/range {v21 .. v23}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/android/mms/ui/MmsPlayerActivityItem;

    const v21, 0x7f0e008b

    move/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    const v21, 0x7f0e0054

    move/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Lcom/mediatek/banyan/widget/MTKImageView;

    const v21, 0x7f0e008f

    move/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/ImageView;

    const v21, 0x7f0e0055

    move/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    const v21, 0x7f0e000a

    move/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    const v21, 0x7f0e008c

    move/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/ImageView;

    const v21, 0x7f0e008d

    move/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->mListItem:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/mms/ui/MmsPlayerActivityItemData;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f0a00fb

    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    add-int/lit8 v25, p1, 0x1

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    aput-object v25, v23, v24

    invoke-virtual/range {v21 .. v23}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v15, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v7}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getImageUri()Landroid/net/Uri;

    move-result-object v5

    if-eqz v5, :cond_7

    const-string v21, "Mms/MmsPlayerAdapter"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "set image: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v21, 0x0

    const/16 v22, 0x1

    const/16 v23, 0x0

    const/16 v24, 0x0

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-virtual {v14, v0, v1, v2, v3}, Landroid/view/View;->setPadding(IIII)V

    invoke-virtual {v14, v5}, Lcom/mediatek/banyan/widget/MTKImageView;->setImageURI(Landroid/net/Uri;)V

    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v14, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_2
    invoke-virtual {v7}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getVideoThumbnail()Landroid/graphics/Bitmap;

    move-result-object v18

    if-eqz v18, :cond_8

    invoke-virtual/range {v17 .. v18}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    const/16 v21, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_3
    invoke-virtual {v7}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getAudioName()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_9

    const-string v21, "Mms/MmsPlayerAdapter"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "show audio name:"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v13, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/high16 v21, 0x41900000

    move/from16 v0, v21

    invoke-virtual {v13, v0}, Landroid/widget/TextView;->setTextSize(F)V

    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v12, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v11, v0}, Landroid/view/View;->setVisibility(I)V

    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v13, v0}, Landroid/view/View;->setVisibility(I)V

    :goto_4
    invoke-virtual {v7}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getText()Ljava/lang/String;

    move-result-object v19

    if-nez v5, :cond_3

    if-eqz v18, :cond_4

    :cond_3
    if-eqz v19, :cond_4

    invoke-virtual {v7}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getImageOrVideoLeft()I

    move-result v21

    invoke-virtual {v7}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getTextLeft()I

    move-result v22

    sub-int v21, v21, v22

    invoke-static/range {v21 .. v21}, Ljava/lang/Math;->abs(I)I

    move-result v10

    invoke-virtual {v7}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getImageOrVideoTop()I

    move-result v21

    invoke-virtual {v7}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getTextTop()I

    move-result v22

    sub-int v21, v21, v22

    invoke-static/range {v21 .. v21}, Ljava/lang/Math;->abs(I)I

    move-result v20

    move/from16 v0, v20

    if-le v10, v0, :cond_d

    invoke-virtual {v7}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getImageOrVideoLeft()I

    move-result v21

    invoke-virtual {v7}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getTextLeft()I

    move-result v22

    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_b

    const v21, 0x7f0e008e

    move/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/TextView;

    invoke-virtual {v7}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getTextWidth()I

    move-result v21

    if-lez v21, :cond_a

    invoke-virtual {v7}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getTextWidth()I

    move-result v21

    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setWidth(I)V

    :cond_4
    :goto_5
    if-eqz v19, :cond_e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->parser:Lcom/android/mms/util/SmileyParser;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/android/mms/util/SmileyParser;->addSmileySpans(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v21

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->textSize:F

    move/from16 v21, v0

    const/16 v22, 0x0

    cmpl-float v21, v21, v22

    if-eqz v21, :cond_5

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->textSize:F

    move/from16 v21, v0

    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    :cond_5
    const/16 v21, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-static {}, Lcom/android/mms/MmsConfig;->getDialogForUrlEnabled()Z

    move-result v21

    if-eqz v21, :cond_6

    const/16 v21, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnableShowUrlDialog(Z)V

    :cond_6
    move-object/from16 v0, v16

    invoke-virtual {v8, v0}, Lcom/android/mms/ui/MmsPlayerActivityItem;->setCurrentTextView(Landroid/widget/TextView;)V

    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->mListItemViewCache:Ljava/util/HashMap;

    move-object/from16 v21, v0

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v9, v8

    goto/16 :goto_1

    :cond_7
    const/16 v21, 0x8

    move/from16 v0, v21

    invoke-virtual {v14, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2

    :cond_8
    const/16 v21, 0x8

    move-object/from16 v0, v17

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_3

    :cond_9
    const/16 v21, 0x8

    move/from16 v0, v21

    invoke-virtual {v12, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    const/16 v21, 0x8

    move/from16 v0, v21

    invoke-virtual {v13, v0}, Landroid/view/View;->setVisibility(I)V

    const/16 v21, 0x8

    move/from16 v0, v21

    invoke-virtual {v11, v0}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_4

    :cond_a
    invoke-virtual {v7}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getImageOrVideoLeft()I

    move-result v21

    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setWidth(I)V

    goto/16 :goto_5

    :cond_b
    const v21, 0x7f0e0090

    move/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/TextView;

    invoke-virtual {v7}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getTextWidth()I

    move-result v21

    if-lez v21, :cond_c

    invoke-virtual {v7}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getTextWidth()I

    move-result v21

    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setWidth(I)V

    goto/16 :goto_5

    :cond_c
    invoke-virtual {v7}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getTextLeft()I

    move-result v21

    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setWidth(I)V

    goto/16 :goto_5

    :cond_d
    invoke-virtual {v7}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getTextTop()I

    move-result v21

    invoke-virtual {v7}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getImageOrVideoTop()I

    move-result v22

    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_4

    const v21, 0x7f0e0091

    move/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/TextView;

    goto/16 :goto_5

    :cond_e
    const/16 v21, 0x8

    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_6
.end method

.method public onMovedToScrapHeap(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    return-void
.end method

.method public setTextSize(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->textSize:F

    return-void
.end method
