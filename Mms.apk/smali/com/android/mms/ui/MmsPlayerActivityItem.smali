.class public Lcom/android/mms/ui/MmsPlayerActivityItem;
.super Landroid/widget/RelativeLayout;
.source "MmsPlayerActivityItem.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "Mms/MmsPlayerActivityItem"


# instance fields
.field private mAudio:Landroid/view/View;

.field private mAudioName:Landroid/widget/TextView;

.field private mBottomText:Landroid/widget/TextView;

.field private mImage:Lcom/mediatek/banyan/widget/MTKImageView;

.field private mLeftText:Landroid/widget/TextView;

.field private mRightText:Landroid/widget/TextView;

.field private mText:Landroid/widget/TextView;

.field private mTopText:Landroid/widget/TextView;

.field private mVideo:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public getCurrentTextView()Landroid/widget/TextView;
    .locals 2

    iget-object v0, p0, Lcom/android/mms/ui/MmsPlayerActivityItem;->mText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/MmsPlayerActivityItem;->mText:Landroid/widget/TextView;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "Mms/MmsPlayerActivityItem"

    const-string v1, "textView is null!"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 2

    const/16 v1, 0x8

    invoke-super {p0}, Landroid/view/View;->onFinishInflate()V

    const v0, 0x7f0e0054

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/mediatek/banyan/widget/MTKImageView;

    iput-object v0, p0, Lcom/android/mms/ui/MmsPlayerActivityItem;->mImage:Lcom/mediatek/banyan/widget/MTKImageView;

    const v0, 0x7f0e008f

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/mms/ui/MmsPlayerActivityItem;->mVideo:Landroid/widget/ImageView;

    const v0, 0x7f0e0055

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/MmsPlayerActivityItem;->mAudio:Landroid/view/View;

    const v0, 0x7f0e000a

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/mms/ui/MmsPlayerActivityItem;->mAudioName:Landroid/widget/TextView;

    const v0, 0x7f0e0091

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/mms/ui/MmsPlayerActivityItem;->mBottomText:Landroid/widget/TextView;

    const v0, 0x7f0e008d

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/mms/ui/MmsPlayerActivityItem;->mTopText:Landroid/widget/TextView;

    const v0, 0x7f0e008e

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/mms/ui/MmsPlayerActivityItem;->mLeftText:Landroid/widget/TextView;

    const v0, 0x7f0e0090

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/mms/ui/MmsPlayerActivityItem;->mRightText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/mms/ui/MmsPlayerActivityItem;->mImage:Lcom/mediatek/banyan/widget/MTKImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/mms/ui/MmsPlayerActivityItem;->mVideo:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/mms/ui/MmsPlayerActivityItem;->mAudio:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/mms/ui/MmsPlayerActivityItem;->mAudioName:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public setCurrentTextView(Landroid/widget/TextView;)V
    .locals 0
    .param p1    # Landroid/widget/TextView;

    iput-object p1, p0, Lcom/android/mms/ui/MmsPlayerActivityItem;->mText:Landroid/widget/TextView;

    return-void
.end method

.method public setTextSize(F)V
    .locals 1
    .param p1    # F

    iget-object v0, p0, Lcom/android/mms/ui/MmsPlayerActivityItem;->mText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/MmsPlayerActivityItem;->mText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    :cond_0
    return-void
.end method
