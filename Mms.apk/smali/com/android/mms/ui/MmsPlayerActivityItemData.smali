.class public Lcom/android/mms/ui/MmsPlayerActivityItemData;
.super Ljava/lang/Object;
.source "MmsPlayerActivityItemData.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "Mms/MmsPlayerActivityItemData"

.field private static sThumbDefaultImage:Landroid/graphics/Bitmap;

.field private static sThumbDefaultVideo:Landroid/graphics/Bitmap;


# instance fields
.field private mAudioName:Ljava/lang/String;

.field private mImageOrVideoHeight:I

.field private mImageOrVideoLeft:I

.field private mImageOrVideoTop:I

.field private mImageOrVideoWidth:I

.field private mImageUri:Landroid/net/Uri;

.field private mText:Ljava/lang/String;

.field private mTextHeight:I

.field private mTextLeft:I

.field private mTextTop:I

.field private mTextWidth:I

.field private mVideoThumbnail:Landroid/graphics/Bitmap;

.field private mVideoUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;IIIIIIII)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/net/Uri;
    .param p3    # Landroid/net/Uri;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # I
    .param p7    # I
    .param p8    # I
    .param p9    # I
    .param p10    # I
    .param p11    # I
    .param p12    # I
    .param p13    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v2, 0x0

    iput v2, p0, Lcom/android/mms/ui/MmsPlayerActivityItemData;->mImageOrVideoLeft:I

    const/4 v2, 0x0

    iput v2, p0, Lcom/android/mms/ui/MmsPlayerActivityItemData;->mImageOrVideoTop:I

    const/4 v2, 0x0

    iput v2, p0, Lcom/android/mms/ui/MmsPlayerActivityItemData;->mImageOrVideoWidth:I

    const/4 v2, 0x0

    iput v2, p0, Lcom/android/mms/ui/MmsPlayerActivityItemData;->mImageOrVideoHeight:I

    const/4 v2, 0x0

    iput v2, p0, Lcom/android/mms/ui/MmsPlayerActivityItemData;->mTextLeft:I

    const/4 v2, 0x0

    iput v2, p0, Lcom/android/mms/ui/MmsPlayerActivityItemData;->mTextTop:I

    const/4 v2, 0x0

    iput v2, p0, Lcom/android/mms/ui/MmsPlayerActivityItemData;->mTextWidth:I

    const/4 v2, 0x0

    iput v2, p0, Lcom/android/mms/ui/MmsPlayerActivityItemData;->mTextHeight:I

    iput-object p2, p0, Lcom/android/mms/ui/MmsPlayerActivityItemData;->mImageUri:Landroid/net/Uri;

    iput-object p3, p0, Lcom/android/mms/ui/MmsPlayerActivityItemData;->mVideoUri:Landroid/net/Uri;

    iput-object p4, p0, Lcom/android/mms/ui/MmsPlayerActivityItemData;->mAudioName:Ljava/lang/String;

    iput-object p5, p0, Lcom/android/mms/ui/MmsPlayerActivityItemData;->mText:Ljava/lang/String;

    iput p6, p0, Lcom/android/mms/ui/MmsPlayerActivityItemData;->mImageOrVideoLeft:I

    iput p7, p0, Lcom/android/mms/ui/MmsPlayerActivityItemData;->mImageOrVideoTop:I

    iput p8, p0, Lcom/android/mms/ui/MmsPlayerActivityItemData;->mImageOrVideoWidth:I

    iput p9, p0, Lcom/android/mms/ui/MmsPlayerActivityItemData;->mImageOrVideoHeight:I

    iput p10, p0, Lcom/android/mms/ui/MmsPlayerActivityItemData;->mTextLeft:I

    move/from16 v0, p11

    iput v0, p0, Lcom/android/mms/ui/MmsPlayerActivityItemData;->mTextTop:I

    move/from16 v0, p12

    iput v0, p0, Lcom/android/mms/ui/MmsPlayerActivityItemData;->mTextWidth:I

    move/from16 v0, p13

    iput v0, p0, Lcom/android/mms/ui/MmsPlayerActivityItemData;->mTextHeight:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v1, v2, Landroid/util/DisplayMetrics;->density:F

    iget-object v2, p0, Lcom/android/mms/ui/MmsPlayerActivityItemData;->mVideoUri:Landroid/net/Uri;

    invoke-direct {p0, v1}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getDesiredThumbnailWidth(F)I

    move-result v3

    invoke-direct {p0, v1}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getDesiredThumbnailHeight(F)I

    move-result v4

    invoke-direct {p0, v2, p1, v3, v4}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getThumbnailFromVideoUri(Landroid/net/Uri;Landroid/content/Context;II)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/android/mms/ui/MmsPlayerActivityItemData;->mVideoThumbnail:Landroid/graphics/Bitmap;

    return-void
.end method

.method private getDesiredThumbnailHeight(F)I
    .locals 1
    .param p1    # F

    const/high16 v0, 0x42c80000

    mul-float/2addr v0, p1

    float-to-int v0, v0

    return v0
.end method

.method private getDesiredThumbnailWidth(F)I
    .locals 1
    .param p1    # F

    const/high16 v0, 0x42c80000

    mul-float/2addr v0, p1

    float-to-int v0, v0

    return v0
.end method

.method private getThumbnailFromImageUri(Landroid/net/Uri;Landroid/content/Context;II)Landroid/graphics/Bitmap;
    .locals 7
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/Context;
    .param p3    # I
    .param p4    # I

    const/4 v4, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-object v4

    :cond_1
    const/4 v2, 0x0

    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-virtual {v5, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v2

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v2, v5, v6}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    if-eqz v2, :cond_2

    :try_start_1
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_2

    :cond_2
    :goto_1
    if-nez v3, :cond_5

    sget-object v5, Lcom/android/mms/ui/MmsPlayerActivityItemData;->sThumbDefaultImage:Landroid/graphics/Bitmap;

    if-nez v5, :cond_3

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f02008a

    invoke-static {v5, v6}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    sput-object v5, Lcom/android/mms/ui/MmsPlayerActivityItemData;->sThumbDefaultImage:Landroid/graphics/Bitmap;

    :cond_3
    sget-object v4, Lcom/android/mms/ui/MmsPlayerActivityItemData;->sThumbDefaultImage:Landroid/graphics/Bitmap;

    goto :goto_0

    :catchall_0
    move-exception v5

    if-eqz v2, :cond_4

    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    :cond_4
    throw v5
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_0
    move-exception v0

    const-string v5, "Mms/MmsPlayerActivityItemData"

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :catch_1
    move-exception v0

    const-string v5, "Mms/MmsPlayerActivityItemData"

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :catch_2
    move-exception v1

    invoke-static {}, Lcom/android/mms/ui/MessageUtils;->writeHprofDataToFile()V

    throw v1

    :cond_5
    move-object v4, v3

    if-eq v4, v3, :cond_0

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0
.end method

.method private getThumbnailFromVideoUri(Landroid/net/Uri;Landroid/content/Context;II)Landroid/graphics/Bitmap;
    .locals 5
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/Context;
    .param p3    # I
    .param p4    # I

    if-nez p1, :cond_1

    const/4 v2, 0x0

    :cond_0
    :goto_0
    return-object v2

    :cond_1
    new-instance v1, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v1}, Landroid/media/MediaMetadataRetriever;-><init>()V

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {v1, p2, p1}, Landroid/media/MediaMetadataRetriever;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    const-wide/16 v3, -0x1

    invoke-virtual {v1, v3, v4}, Landroid/media/MediaMetadataRetriever;->getFrameAtTime(J)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :try_start_1
    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    if-nez v0, :cond_3

    sget-object v3, Lcom/android/mms/ui/MmsPlayerActivityItemData;->sThumbDefaultVideo:Landroid/graphics/Bitmap;

    if-nez v3, :cond_2

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02008b

    invoke-static {v3, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    sput-object v3, Lcom/android/mms/ui/MmsPlayerActivityItemData;->sThumbDefaultVideo:Landroid/graphics/Bitmap;

    :cond_2
    sget-object v2, Lcom/android/mms/ui/MmsPlayerActivityItemData;->sThumbDefaultVideo:Landroid/graphics/Bitmap;

    goto :goto_0

    :catchall_0
    move-exception v3

    :try_start_2
    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V

    throw v3
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_0
    move-exception v3

    goto :goto_1

    :cond_3
    const/4 v3, 0x1

    invoke-static {v0, p3, p4, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    if-eq v2, v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    :catch_1
    move-exception v3

    goto :goto_1
.end method


# virtual methods
.method public getAudioName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/mms/ui/MmsPlayerActivityItemData;->mAudioName:Ljava/lang/String;

    return-object v0
.end method

.method public getImageOrVideoHeight()I
    .locals 1

    iget v0, p0, Lcom/android/mms/ui/MmsPlayerActivityItemData;->mImageOrVideoHeight:I

    return v0
.end method

.method public getImageOrVideoLeft()I
    .locals 1

    iget v0, p0, Lcom/android/mms/ui/MmsPlayerActivityItemData;->mImageOrVideoLeft:I

    return v0
.end method

.method public getImageOrVideoTop()I
    .locals 1

    iget v0, p0, Lcom/android/mms/ui/MmsPlayerActivityItemData;->mImageOrVideoTop:I

    return v0
.end method

.method public getImageOrVideoWidth()I
    .locals 1

    iget v0, p0, Lcom/android/mms/ui/MmsPlayerActivityItemData;->mImageOrVideoWidth:I

    return v0
.end method

.method public getImageUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/android/mms/ui/MmsPlayerActivityItemData;->mImageUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/mms/ui/MmsPlayerActivityItemData;->mText:Ljava/lang/String;

    return-object v0
.end method

.method public getTextHeight()I
    .locals 1

    iget v0, p0, Lcom/android/mms/ui/MmsPlayerActivityItemData;->mTextHeight:I

    return v0
.end method

.method public getTextLeft()I
    .locals 1

    iget v0, p0, Lcom/android/mms/ui/MmsPlayerActivityItemData;->mTextLeft:I

    return v0
.end method

.method public getTextTop()I
    .locals 1

    iget v0, p0, Lcom/android/mms/ui/MmsPlayerActivityItemData;->mTextTop:I

    return v0
.end method

.method public getTextWidth()I
    .locals 1

    iget v0, p0, Lcom/android/mms/ui/MmsPlayerActivityItemData;->mTextWidth:I

    return v0
.end method

.method public getVideoThumbnail()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/android/mms/ui/MmsPlayerActivityItemData;->mVideoThumbnail:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getVideoUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/android/mms/ui/MmsPlayerActivityItemData;->mVideoUri:Landroid/net/Uri;

    return-object v0
.end method
