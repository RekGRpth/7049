.class Lcom/android/mms/ui/MultiDeleteActivity$5;
.super Landroid/os/Handler;
.source "MultiDeleteActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/ui/MultiDeleteActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/MultiDeleteActivity;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/MultiDeleteActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/MultiDeleteActivity$5;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1    # Landroid/os/Message;

    const/4 v4, 0x1

    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    const-string v1, "Mms/MultiDeleteActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown message: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :pswitch_0
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/mms/ui/MultiDeleteActivity$5;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v1, v1, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    iget v2, p1, Landroid/os/Message;->arg1:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Lcom/android/mms/ui/MessageListAdapter;->changeSelectedState(J)V

    iget-object v1, p0, Lcom/android/mms/ui/MultiDeleteActivity$5;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v1, v1, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v1}, Lcom/android/mms/ui/MessageListAdapter;->getSelectedNumber()I

    move-result v1

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/android/mms/ui/MultiDeleteActivity$5;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v1, v1, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v1}, Lcom/android/mms/ui/MessageListAdapter;->getSelectedNumber()I

    move-result v1

    iget-object v2, p0, Lcom/android/mms/ui/MultiDeleteActivity$5;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v2, v2, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v2}, Landroid/widget/CursorAdapter;->getCount()I

    move-result v2

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/mms/ui/MultiDeleteActivity$5;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v1, v4}, Lcom/android/mms/ui/MultiDeleteActivity;->access$802(Lcom/android/mms/ui/MultiDeleteActivity;Z)Z

    iget-object v1, p0, Lcom/android/mms/ui/MultiDeleteActivity$5;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-virtual {v1}, Landroid/app/Activity;->invalidateOptionsMenu()V

    iget-object v1, p0, Lcom/android/mms/ui/MultiDeleteActivity$5;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v1}, Lcom/android/mms/ui/MultiDeleteActivity;->access$900(Lcom/android/mms/ui/MultiDeleteActivity;)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_1
    iget-object v1, p0, Lcom/android/mms/ui/MultiDeleteActivity$5;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/mms/ui/MultiDeleteActivity;->access$802(Lcom/android/mms/ui/MultiDeleteActivity;Z)Z

    iget-object v1, p0, Lcom/android/mms/ui/MultiDeleteActivity$5;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-virtual {v1}, Landroid/app/Activity;->invalidateOptionsMenu()V

    iget-object v1, p0, Lcom/android/mms/ui/MultiDeleteActivity$5;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v1}, Lcom/android/mms/ui/MultiDeleteActivity;->access$900(Lcom/android/mms/ui/MultiDeleteActivity;)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
    .end packed-switch
.end method
