.class Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener$2;
.super Ljava/lang/Object;
.source "MultiDeleteActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;->onClick(Landroid/content/DialogInterface;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener$2;->this$1:Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 17

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener$2;->this$1:Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;

    iget-object v1, v1, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v1, v1, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v1}, Lcom/android/mms/ui/MessageListAdapter;->getItemList()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v15

    const/4 v4, 0x0

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener$2;->this$1:Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;

    iget-object v1, v1, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v1, v1, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v1}, Lcom/android/mms/ui/MessageListAdapter;->getSelectedNumber()I

    move-result v1

    new-array v6, v1, [Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener$2;->this$1:Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;

    iget-object v1, v1, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v1, v1, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v1}, Lcom/android/mms/ui/MessageListAdapter;->getSelectedNumber()I

    move-result v1

    new-array v12, v1, [Ljava/lang/String;

    const/4 v14, 0x0

    const/16 v16, 0x0

    :cond_0
    :goto_0
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/util/Map$Entry;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener$2;->this$1:Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;

    invoke-static {v1}, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;->access$300(Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;)Z

    move-result v1

    if-nez v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener$2;->this$1:Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;

    iget-object v1, v1, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v1, v13}, Lcom/android/mms/ui/MultiDeleteActivity;->access$600(Lcom/android/mms/ui/MultiDeleteActivity;Ljava/util/Map$Entry;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_1
    invoke-interface {v13}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v13}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const-wide/16 v7, 0x0

    cmp-long v1, v1, v7

    if-lez v1, :cond_2

    const-string v1, "Mms/MultiDeleteActivity"

    const-string v2, "sms"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v13}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v14

    const-string v1, "Mms/MultiDeleteActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "argsSms[i]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v6, v14

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v4, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    :cond_2
    const-string v1, "Mms/MultiDeleteActivity"

    const-string v2, "mms"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v13}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    neg-long v1, v1

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v12, v16

    const-string v1, "Mms/MultiDeleteActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "argsMms[j]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v12, v16

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v10, Landroid/provider/Telephony$Mms;->CONTENT_URI:Landroid/net/Uri;

    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_0

    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener$2;->this$1:Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;

    iget-object v1, v1, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v1}, Lcom/android/mms/ui/MultiDeleteActivity;->access$200(Lcom/android/mms/ui/MultiDeleteActivity;)Lcom/android/mms/ui/MultiDeleteActivity$BackgroundQueryHandler;

    move-result-object v3

    if-eqz v4, :cond_7

    const/4 v1, 0x1

    move v2, v1

    :goto_1
    if-eqz v10, :cond_8

    const/4 v1, 0x1

    :goto_2
    add-int/2addr v1, v2

    invoke-virtual {v3, v1}, Lcom/android/mms/ui/ConversationList$BaseProgressQueryHandler;->setMax(I)V

    if-eqz v4, :cond_4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener$2;->this$1:Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;

    iget-object v1, v1, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v1}, Lcom/android/mms/ui/MultiDeleteActivity;->access$708(Lcom/android/mms/ui/MultiDeleteActivity;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener$2;->this$1:Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;

    iget-object v1, v1, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v1}, Lcom/android/mms/ui/MultiDeleteActivity;->access$200(Lcom/android/mms/ui/MultiDeleteActivity;)Lcom/android/mms/ui/MultiDeleteActivity$BackgroundQueryHandler;

    move-result-object v1

    const/16 v2, 0x25e4

    const/4 v3, 0x0

    const-string v5, "ForMultiDelete"

    invoke-virtual/range {v1 .. v6}, Landroid/content/AsyncQueryHandler;->startDelete(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    :cond_4
    if-eqz v10, :cond_5

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener$2;->this$1:Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;

    iget-object v1, v1, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v1}, Lcom/android/mms/ui/MultiDeleteActivity;->access$708(Lcom/android/mms/ui/MultiDeleteActivity;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener$2;->this$1:Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;

    iget-object v1, v1, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v1}, Lcom/android/mms/ui/MultiDeleteActivity;->access$200(Lcom/android/mms/ui/MultiDeleteActivity;)Lcom/android/mms/ui/MultiDeleteActivity$BackgroundQueryHandler;

    move-result-object v7

    const/16 v8, 0x25e4

    const/4 v9, 0x0

    const-string v11, "ForMultiDelete"

    invoke-virtual/range {v7 .. v12}, Landroid/content/AsyncQueryHandler;->startDelete(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    :cond_5
    if-nez v4, :cond_6

    if-nez v10, :cond_6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener$2;->this$1:Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;

    iget-object v1, v1, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v1}, Lcom/android/mms/ui/MultiDeleteActivity;->access$200(Lcom/android/mms/ui/MultiDeleteActivity;)Lcom/android/mms/ui/MultiDeleteActivity$BackgroundQueryHandler;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/mms/ui/ConversationList$BaseProgressQueryHandler;->dismissProgressDialog()V

    :cond_6
    return-void

    :cond_7
    const/4 v1, 0x0

    move v2, v1

    goto :goto_1

    :cond_8
    const/4 v1, 0x0

    goto :goto_2
.end method
