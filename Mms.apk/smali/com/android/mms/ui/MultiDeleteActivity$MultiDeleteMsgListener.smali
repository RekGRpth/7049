.class Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;
.super Ljava/lang/Object;
.source "MultiDeleteActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/ui/MultiDeleteActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MultiDeleteMsgListener"
.end annotation


# instance fields
.field private mDeleteLockedMessages:Z

.field private mDeleteThread:Z

.field private mHasLockedMsg:Z

.field private maxMmsId:I

.field private maxSmsId:I

.field final synthetic this$0:Lcom/android/mms/ui/MultiDeleteActivity;


# direct methods
.method public constructor <init>(Lcom/android/mms/ui/MultiDeleteActivity;)V
    .locals 1

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;->mDeleteLockedMessages:Z

    iput-boolean v0, p0, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;->mDeleteThread:Z

    iput-boolean v0, p0, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;->mHasLockedMsg:Z

    return-void
.end method

.method static synthetic access$300(Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;)Z
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;

    iget-boolean v0, p0, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;->mDeleteLockedMessages:Z

    return v0
.end method

.method static synthetic access$400(Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;)I
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;

    iget v0, p0, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;->maxMmsId:I

    return v0
.end method

.method static synthetic access$500(Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;)I
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;

    iget v0, p0, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;->maxSmsId:I

    return v0
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v0}, Lcom/android/mms/ui/MultiDeleteActivity;->access$200(Lcom/android/mms/ui/MultiDeleteActivity;)Lcom/android/mms/ui/MultiDeleteActivity$BackgroundQueryHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v1}, Lcom/android/mms/ui/DeleteProgressDialogUtil;->getProgressDialog(Landroid/content/Context;)Lcom/android/mms/ui/NewProgressDialog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/mms/ui/ConversationList$BaseProgressQueryHandler;->setProgressDialog(Lcom/android/mms/ui/NewProgressDialog;)V

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v0}, Lcom/android/mms/ui/MultiDeleteActivity;->access$200(Lcom/android/mms/ui/MultiDeleteActivity;)Lcom/android/mms/ui/MultiDeleteActivity$BackgroundQueryHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/mms/ui/ConversationList$BaseProgressQueryHandler;->showProgressDialog()V

    iget-boolean v0, p0, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;->mDeleteThread:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;->mHasLockedMsg:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;->mDeleteLockedMessages:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;->mHasLockedMsg:Z

    if-eqz v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener$1;

    invoke-direct {v1, p0}, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener$1;-><init>(Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :goto_0
    return-void

    :cond_1
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener$2;

    invoke-direct {v1, p0}, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener$2;-><init>(Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public setDeleteLockedMessage(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;->mDeleteLockedMessages:Z

    return-void
.end method

.method public setDeleteThread(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;->mDeleteThread:Z

    return-void
.end method

.method public setHasLockedMsg(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;->mHasLockedMsg:Z

    return-void
.end method

.method public setMaxMsgId(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    iput p1, p0, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;->maxMmsId:I

    iput p2, p0, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;->maxSmsId:I

    return-void
.end method
