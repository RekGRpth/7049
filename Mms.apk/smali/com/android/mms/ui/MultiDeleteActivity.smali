.class public Lcom/android/mms/ui/MultiDeleteActivity;
.super Landroid/app/ListActivity;
.source "MultiDeleteActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mms/ui/MultiDeleteActivity$BackgroundQueryHandler;,
        Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;
    }
.end annotation


# static fields
.field private static final DELETE_MESSAGE_TOKEN:I = 0x25e4

.field private static final FOR_MULTIDELETE:Ljava/lang/String; = "ForMultiDelete"

.field private static final MESSAGE_LIST_QUERY_TOKEN:I = 0x2537

.field public static final TAG:Ljava/lang/String; = "Mms/MultiDeleteActivity"


# instance fields
.field private mActionBarText:Landroid/widget/TextView;

.field private mBackgroundQueryHandler:Lcom/android/mms/ui/MultiDeleteActivity$BackgroundQueryHandler;

.field private mCancelSelect:Landroid/view/MenuItem;

.field private mConversation:Lcom/android/mms/data/Conversation;

.field private final mDataSetChangedListener:Lcom/android/mms/ui/MessageListAdapter$OnDataSetChangedListener;

.field private mDelete:Landroid/view/MenuItem;

.field private mDeleteRunningCount:I

.field private mIsSelectedAll:Z

.field private final mMessageListItemHandler:Landroid/os/Handler;

.field public mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

.field private mMsgListView:Landroid/widget/ListView;

.field private mPossiblePendingNotification:Z

.field private mSelectAll:Landroid/view/MenuItem;

.field private mThreadCountManager:Lcom/android/mms/util/ThreadCountManager;

.field private threadId:J


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    invoke-static {}, Lcom/android/mms/util/ThreadCountManager;->getInstance()Lcom/android/mms/util/ThreadCountManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mThreadCountManager:Lcom/android/mms/util/ThreadCountManager;

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mDeleteRunningCount:I

    new-instance v0, Lcom/android/mms/ui/MultiDeleteActivity$5;

    invoke-direct {v0, p0}, Lcom/android/mms/ui/MultiDeleteActivity$5;-><init>(Lcom/android/mms/ui/MultiDeleteActivity;)V

    iput-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMessageListItemHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/mms/ui/MultiDeleteActivity$6;

    invoke-direct {v0, p0}, Lcom/android/mms/ui/MultiDeleteActivity$6;-><init>(Lcom/android/mms/ui/MultiDeleteActivity;)V

    iput-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mDataSetChangedListener:Lcom/android/mms/ui/MessageListAdapter$OnDataSetChangedListener;

    return-void
.end method

.method static synthetic access$000(Lcom/android/mms/ui/MultiDeleteActivity;)J
    .locals 2
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    iget-wide v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->threadId:J

    return-wide v0
.end method

.method static synthetic access$100(Lcom/android/mms/ui/MultiDeleteActivity;)Lcom/android/mms/data/Conversation;
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mConversation:Lcom/android/mms/data/Conversation;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/mms/ui/MultiDeleteActivity;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-direct {p0}, Lcom/android/mms/ui/MultiDeleteActivity;->startMsgListQuery()V

    return-void
.end method

.method static synthetic access$102(Lcom/android/mms/ui/MultiDeleteActivity;Lcom/android/mms/data/Conversation;)Lcom/android/mms/data/Conversation;
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;
    .param p1    # Lcom/android/mms/data/Conversation;

    iput-object p1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mConversation:Lcom/android/mms/data/Conversation;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/android/mms/ui/MultiDeleteActivity;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-direct {p0}, Lcom/android/mms/ui/MultiDeleteActivity;->updateSendFailedNotification()V

    return-void
.end method

.method static synthetic access$1200(Lcom/android/mms/ui/MultiDeleteActivity;)Lcom/android/mms/util/ThreadCountManager;
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mThreadCountManager:Lcom/android/mms/util/ThreadCountManager;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/android/mms/ui/MultiDeleteActivity;Z)Z
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mPossiblePendingNotification:Z

    return p1
.end method

.method static synthetic access$200(Lcom/android/mms/ui/MultiDeleteActivity;)Lcom/android/mms/ui/MultiDeleteActivity$BackgroundQueryHandler;
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mBackgroundQueryHandler:Lcom/android/mms/ui/MultiDeleteActivity$BackgroundQueryHandler;

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/mms/ui/MultiDeleteActivity;Ljava/util/Map$Entry;)Z
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;
    .param p1    # Ljava/util/Map$Entry;

    invoke-direct {p0, p1}, Lcom/android/mms/ui/MultiDeleteActivity;->isMsgLocked(Ljava/util/Map$Entry;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/android/mms/ui/MultiDeleteActivity;)I
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    iget v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mDeleteRunningCount:I

    return v0
.end method

.method static synthetic access$702(Lcom/android/mms/ui/MultiDeleteActivity;I)I
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;
    .param p1    # I

    iput p1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mDeleteRunningCount:I

    return p1
.end method

.method static synthetic access$708(Lcom/android/mms/ui/MultiDeleteActivity;)I
    .locals 2
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    iget v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mDeleteRunningCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mDeleteRunningCount:I

    return v0
.end method

.method static synthetic access$710(Lcom/android/mms/ui/MultiDeleteActivity;)I
    .locals 2
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    iget v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mDeleteRunningCount:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mDeleteRunningCount:I

    return v0
.end method

.method static synthetic access$802(Lcom/android/mms/ui/MultiDeleteActivity;Z)Z
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mIsSelectedAll:Z

    return p1
.end method

.method static synthetic access$900(Lcom/android/mms/ui/MultiDeleteActivity;)Landroid/view/MenuItem;
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mDelete:Landroid/view/MenuItem;

    return-object v0
.end method

.method private checkPendingNotification()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mPossiblePendingNotification:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->hasWindowFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mConversation:Lcom/android/mms/data/Conversation;

    invoke-virtual {v0}, Lcom/android/mms/data/Conversation;->markAsRead()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mPossiblePendingNotification:Z

    :cond_0
    return-void
.end method

.method private confirmMultiDeleteMsgDialog(Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;ZZLjava/lang/Long;Landroid/content/Context;)V
    .locals 14
    .param p1    # Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;
    .param p2    # Z
    .param p3    # Z
    .param p4    # Ljava/lang/Long;
    .param p5    # Landroid/content/Context;

    const v1, 0x7f040010

    const/4 v2, 0x0

    move-object/from16 v0, p5

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    const v1, 0x7f0e0049

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    if-nez p3, :cond_4

    const v1, 0x7f0a0072

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v12, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    const v1, 0x7f0e004a

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/CheckBox;

    if-nez p2, :cond_6

    const/16 v1, 0x8

    invoke-virtual {v8, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    const/4 v10, 0x0

    const/4 v13, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {p5 .. p5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "max(_id)"

    aput-object v5, v3, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    if-eqz v10, :cond_1

    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    const-string v1, "Mms/MultiDeleteActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "confirmMultiDeleteMsgDialog max SMS id = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    const/4 v10, 0x0

    :cond_1
    invoke-virtual/range {p5 .. p5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/Telephony$Mms;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "max(_id)"

    aput-object v5, v3, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    if-eqz v10, :cond_3

    :try_start_1
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    const-string v1, "Mms/MultiDeleteActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "confirmMultiDeleteMsgDialog max MMS id = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_2
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    const/4 v10, 0x0

    :cond_3
    invoke-virtual {p1, v11, v13}, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;->setMaxMsgId(II)V

    new-instance v7, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p5

    invoke-direct {v7, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0a01b7

    invoke-virtual {v7, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x1010355

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a01c0

    invoke-virtual {v1, v2, p1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a0197

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v9}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void

    :cond_4
    move/from16 v0, p3

    invoke-virtual {p1, v0}, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;->setDeleteThread(Z)V

    invoke-virtual/range {p1 .. p2}, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;->setHasLockedMsg(Z)V

    if-nez p4, :cond_5

    const v1, 0x7f0a01ba

    invoke-virtual {v12, v1}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    :cond_5
    invoke-virtual/range {p5 .. p5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0004

    const/4 v3, 0x1

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v12, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_6
    invoke-virtual {v8}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    invoke-virtual {p1, v1}, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;->setDeleteLockedMessage(Z)V

    new-instance v1, Lcom/android/mms/ui/MultiDeleteActivity$3;

    invoke-direct {v1, p0, p1, v8}, Lcom/android/mms/ui/MultiDeleteActivity$3;-><init>(Lcom/android/mms/ui/MultiDeleteActivity;Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;Landroid/widget/CheckBox;)V

    invoke-virtual {v8, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1

    :catchall_0
    move-exception v1

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    const/4 v10, 0x0

    throw v1

    :catchall_1
    move-exception v1

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    const/4 v10, 0x0

    throw v1
.end method

.method private getSelectedCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v0}, Lcom/android/mms/ui/MessageListAdapter;->getSelectedNumber()I

    move-result v0

    return v0
.end method

.method private initActivityState(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const/4 v4, 0x1

    if-eqz p1, :cond_0

    const-string v2, "is_all_selected"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    const/4 v3, 0x0

    invoke-virtual {v2, v4, v3}, Lcom/android/mms/ui/MessageListAdapter;->setItemsValue(Z[J)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v2, "select_list"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v2, v4, v1}, Lcom/android/mms/ui/MessageListAdapter;->setItemsValue(Z[J)V

    goto :goto_0
.end method

.method private initMessageList()V
    .locals 8

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "highlight"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_1

    move-object v5, v2

    :goto_1
    new-instance v0, Lcom/android/mms/ui/MessageListAdapter;

    iget-object v3, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListView:Landroid/widget/ListView;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/mms/ui/MessageListAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Landroid/widget/ListView;ZLjava/util/regex/Pattern;)V

    iput-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    iput-boolean v4, v0, Lcom/android/mms/ui/MessageListAdapter;->mIsDeleteMode:Z

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    iget-object v1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMessageListItemHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/android/mms/ui/MessageListAdapter;->setMsgListItemHandler(Landroid/os/Handler;)V

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    iget-object v1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mDataSetChangedListener:Lcom/android/mms/ui/MessageListAdapter$OnDataSetChangedListener;

    invoke-virtual {v0, v1}, Lcom/android/mms/ui/MessageListAdapter;->setOnDataSetChangedListener(Lcom/android/mms/ui/MessageListAdapter$OnDataSetChangedListener;)V

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListView:Landroid/widget/ListView;

    invoke-virtual {v0, v7}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListView:Landroid/widget/ListView;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\\b"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v6}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v5

    goto :goto_1
.end method

.method private isMsgLocked(Ljava/util/Map$Entry;)Z
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            ">;)Z"
        }
    .end annotation

    const/4 v8, 0x0

    const/4 v4, 0x0

    if-nez p1, :cond_0

    move v3, v4

    :goto_0
    return v3

    :cond_0
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const/4 v0, 0x0

    const-wide/16 v5, 0x0

    cmp-long v3, v1, v5

    if-gez v3, :cond_1

    iget-object v3, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    const-string v5, "mms"

    neg-long v6, v1

    invoke-virtual {v3, v5, v6, v7, v8}, Lcom/android/mms/ui/MessageListAdapter;->getCachedMessageItem(Ljava/lang/String;JLandroid/database/Cursor;)Lcom/android/mms/ui/MessageItem;

    move-result-object v0

    :goto_1
    if-nez v0, :cond_2

    move v3, v4

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    const-string v5, "sms"

    invoke-virtual {v3, v5, v1, v2, v8}, Lcom/android/mms/ui/MessageListAdapter;->getCachedMessageItem(Ljava/lang/String;JLandroid/database/Cursor;)Lcom/android/mms/ui/MessageItem;

    move-result-object v0

    goto :goto_1

    :cond_2
    iget-boolean v3, v0, Lcom/android/mms/ui/MessageItem;->mLocked:Z

    goto :goto_0
.end method

.method private markCheckedState(Z)V
    .locals 5
    .param p1    # Z

    iget-object v3, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    const/4 v4, 0x0

    invoke-virtual {v3, p1, v4}, Lcom/android/mms/ui/MessageListAdapter;->setItemsValue(Z[J)V

    iget-object v3, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListView:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v2, 0x0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v3, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListView:Landroid/widget/ListView;

    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/mms/ui/MessageListItem;

    invoke-virtual {v2, p1}, Lcom/android/mms/ui/MessageListItem;->setSelectedBackGroud(Z)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private selectedMsgHasLocked()Z
    .locals 11

    const/4 v10, 0x0

    const/4 v3, 0x0

    iget-object v6, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    if-nez v6, :cond_0

    const/4 v6, 0x0

    :goto_0
    return v6

    :cond_0
    iget-object v6, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v6}, Lcom/android/mms/ui/MessageListAdapter;->getItemList()Ljava/util/Map;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const/4 v2, 0x0

    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-gez v6, :cond_3

    iget-object v6, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    const-string v7, "mms"

    neg-long v8, v4

    invoke-virtual {v6, v7, v8, v9, v10}, Lcom/android/mms/ui/MessageListAdapter;->getCachedMessageItem(Ljava/lang/String;JLandroid/database/Cursor;)Lcom/android/mms/ui/MessageItem;

    move-result-object v2

    :goto_1
    if-eqz v2, :cond_1

    iget-boolean v6, v2, Lcom/android/mms/ui/MessageItem;->mLocked:Z

    if-eqz v6, :cond_1

    const/4 v3, 0x1

    :cond_2
    move v6, v3

    goto :goto_0

    :cond_3
    iget-object v6, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    const-string v7, "sms"

    invoke-virtual {v6, v7, v4, v5, v10}, Lcom/android/mms/ui/MessageListAdapter;->getCachedMessageItem(Ljava/lang/String;JLandroid/database/Cursor;)Lcom/android/mms/ui/MessageItem;

    move-result-object v2

    goto :goto_1
.end method

.method private setUpActionBar()V
    .locals 6

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f04002b

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    const/16 v3, 0x10

    const/16 v4, 0x1a

    invoke-virtual {v0, v3, v4}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    const v3, 0x7f0e00ad

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    new-instance v3, Lcom/android/mms/ui/MultiDeleteActivity$1;

    invoke-direct {v3, p0}, Lcom/android/mms/ui/MultiDeleteActivity$1;-><init>(Lcom/android/mms/ui/MultiDeleteActivity;)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v3, 0x7f0e00ae

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mActionBarText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    return-void
.end method

.method private startMsgListQuery()V
    .locals 5

    iget-object v1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mBackgroundQueryHandler:Lcom/android/mms/ui/MultiDeleteActivity$BackgroundQueryHandler;

    const/16 v2, 0x2537

    invoke-virtual {v1, v2}, Landroid/content/AsyncQueryHandler;->cancelOperation(I)V

    :try_start_0
    iget-object v1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mBackgroundQueryHandler:Lcom/android/mms/ui/MultiDeleteActivity$BackgroundQueryHandler;

    new-instance v2, Lcom/android/mms/ui/MultiDeleteActivity$2;

    invoke-direct {v2, p0}, Lcom/android/mms/ui/MultiDeleteActivity$2;-><init>(Lcom/android/mms/ui/MultiDeleteActivity;)V

    const-wide/16 v3, 0x32

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {p0, v0}, Landroid/database/sqlite/SqliteWrapper;->checkSQLiteException(Landroid/content/Context;Landroid/database/sqlite/SQLiteException;)V

    goto :goto_0
.end method

.method private updateSendFailedNotification()V
    .locals 5

    iget-object v2, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mConversation:Lcom/android/mms/data/Conversation;

    invoke-virtual {v2}, Lcom/android/mms/data/Conversation;->getThreadId()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/android/mms/ui/MultiDeleteActivity$4;

    invoke-direct {v3, p0, v0, v1}, Lcom/android/mms/ui/MultiDeleteActivity$4;-><init>(Lcom/android/mms/ui/MultiDeleteActivity;J)V

    const-string v4, "updateSendFailedNotification"

    invoke-direct {v2, v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1    # Landroid/content/res/Configuration;

    const-string v0, "Mms/MultiDeleteActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onConfigurationChanged "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const-wide/16 v3, 0x0

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f04002c

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    invoke-virtual {p0, v2}, Landroid/app/Activity;->setProgressBarVisibility(Z)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "thread_id"

    invoke-virtual {v0, v1, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->threadId:J

    iget-wide v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->threadId:J

    cmp-long v0, v0, v3

    if-nez v0, :cond_0

    const-string v0, "TAG"

    const-string v1, "threadId can\'t be zero"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_0
    iget-wide v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->threadId:J

    invoke-static {p0, v0, v1, v2}, Lcom/android/mms/data/Conversation;->get(Landroid/content/Context;JZ)Lcom/android/mms/data/Conversation;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mConversation:Lcom/android/mms/data/Conversation;

    invoke-virtual {p0}, Landroid/app/ListActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListView:Landroid/widget/ListView;

    invoke-direct {p0}, Lcom/android/mms/ui/MultiDeleteActivity;->setUpActionBar()V

    invoke-direct {p0}, Lcom/android/mms/ui/MultiDeleteActivity;->initMessageList()V

    invoke-direct {p0, p1}, Lcom/android/mms/ui/MultiDeleteActivity;->initActivityState(Landroid/os/Bundle;)V

    new-instance v0, Lcom/android/mms/ui/MultiDeleteActivity$BackgroundQueryHandler;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/mms/ui/MultiDeleteActivity$BackgroundQueryHandler;-><init>(Lcom/android/mms/ui/MultiDeleteActivity;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mBackgroundQueryHandler:Lcom/android/mms/ui/MultiDeleteActivity$BackgroundQueryHandler;

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    invoke-virtual {p0}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f0d0000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v0, 0x7f0e0038

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mSelectAll:Landroid/view/MenuItem;

    const v0, 0x7f0e00f5

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mCancelSelect:Landroid/view/MenuItem;

    const v0, 0x7f0e003b

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mDelete:Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mDelete:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    const/4 v0, 0x1

    return v0
.end method

.method protected onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 0
    .param p1    # Landroid/widget/ListView;
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J

    if-eqz p2, :cond_0

    check-cast p2, Lcom/android/mms/ui/MessageListItem;

    invoke-virtual {p2}, Lcom/android/mms/ui/MessageListItem;->onMessageListItemClick()V

    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 12
    .param p1    # Landroid/view/MenuItem;

    const/4 v8, 0x0

    const/4 v3, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :cond_0
    :goto_0
    return v3

    :sswitch_0
    iget-boolean v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mIsSelectedAll:Z

    if-nez v0, :cond_1

    iput-boolean v3, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mIsSelectedAll:Z

    iget-boolean v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mIsSelectedAll:Z

    invoke-direct {p0, v0}, Lcom/android/mms/ui/MultiDeleteActivity;->markCheckedState(Z)V

    invoke-virtual {p0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    :cond_1
    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mDelete:Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v0}, Lcom/android/mms/ui/MessageListAdapter;->getSelectedNumber()I

    move-result v0

    if-lez v0, :cond_2

    iput-boolean v8, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mIsSelectedAll:Z

    iget-boolean v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mIsSelectedAll:Z

    invoke-direct {p0, v0}, Lcom/android/mms/ui/MultiDeleteActivity;->markCheckedState(Z)V

    invoke-virtual {p0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    :cond_2
    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mDelete:Landroid/view/MenuItem;

    invoke-interface {v0, v8}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v0}, Lcom/android/mms/ui/MessageListAdapter;->getSelectedNumber()I

    move-result v11

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v0}, Landroid/widget/CursorAdapter;->getCount()I

    move-result v0

    if-lt v11, v0, :cond_3

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mConversation:Lcom/android/mms/data/Conversation;

    invoke-virtual {v0}, Lcom/android/mms/data/Conversation;->getThreadId()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    new-instance v1, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;

    invoke-direct {v1, p0}, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;-><init>(Lcom/android/mms/ui/MultiDeleteActivity;)V

    invoke-direct {p0}, Lcom/android/mms/ui/MultiDeleteActivity;->selectedMsgHasLocked()Z

    move-result v2

    move-object v0, p0

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/mms/ui/MultiDeleteActivity;->confirmMultiDeleteMsgDialog(Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;ZZLjava/lang/Long;Landroid/content/Context;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v0}, Lcom/android/mms/ui/MessageListAdapter;->getSelectedNumber()I

    move-result v0

    if-lez v0, :cond_0

    new-instance v1, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;

    invoke-direct {v1, p0}, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;-><init>(Lcom/android/mms/ui/MultiDeleteActivity;)V

    invoke-direct {p0}, Lcom/android/mms/ui/MultiDeleteActivity;->selectedMsgHasLocked()Z

    move-result v7

    const/4 v9, 0x0

    move-object v5, p0

    move-object v6, v1

    move-object v10, p0

    invoke-direct/range {v5 .. v10}, Lcom/android/mms/ui/MultiDeleteActivity;->confirmMultiDeleteMsgDialog(Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;ZZLjava/lang/Long;Landroid/content/Context;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x7f0e0038 -> :sswitch_0
        0x7f0e003b -> :sswitch_2
        0x7f0e00f5 -> :sswitch_1
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 8
    .param p1    # Landroid/view/Menu;

    const/4 v7, 0x1

    invoke-direct {p0}, Lcom/android/mms/ui/MultiDeleteActivity;->getSelectedCount()I

    move-result v0

    iget-object v1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mActionBarText:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0001

    new-array v4, v7, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return v7
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v4, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v4}, Lcom/android/mms/ui/MessageListAdapter;->getSelectedNumber()I

    move-result v4

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v5}, Landroid/widget/CursorAdapter;->getCount()I

    move-result v5

    if-ne v4, v5, :cond_1

    const-string v4, "is_all_selected"

    const/4 v5, 0x1

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v4, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v4}, Lcom/android/mms/ui/MessageListAdapter;->getSelectedNumber()I

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v4}, Lcom/android/mms/ui/MessageListAdapter;->getSelectedNumber()I

    move-result v4

    new-array v0, v4, [J

    iget-object v4, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v4}, Lcom/android/mms/ui/MessageListAdapter;->getItemList()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    const/4 v2, 0x0

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    aput-wide v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    const-string v4, "select_list"

    invoke-virtual {p1, v4, v0}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    goto :goto_0
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mConversation:Lcom/android/mms/data/Conversation;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/mms/data/Conversation;->blockMarkAsRead(Z)V

    invoke-direct {p0}, Lcom/android/mms/ui/MultiDeleteActivity;->startMsgListQuery()V

    return-void
.end method

.method public onUserInteraction()V
    .locals 0

    invoke-direct {p0}, Lcom/android/mms/ui/MultiDeleteActivity;->checkPendingNotification()V

    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0
    .param p1    # Z

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/android/mms/ui/MultiDeleteActivity;->checkPendingNotification()V

    :cond_0
    return-void
.end method
