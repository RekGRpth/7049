.class Lcom/android/mms/ui/MultiSaveActivity$1;
.super Ljava/lang/Object;
.source "MultiSaveActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/MultiSaveActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/MultiSaveActivity;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/MultiSaveActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/MultiSaveActivity$1;->this$0:Lcom/android/mms/ui/MultiSaveActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    if-eqz p2, :cond_0

    check-cast p2, Lcom/android/mms/ui/MultiSaveListItem;

    invoke-virtual {p2}, Lcom/android/mms/ui/MultiSaveListItem;->clickListItem()V

    iget-object v0, p0, Lcom/android/mms/ui/MultiSaveActivity$1;->this$0:Lcom/android/mms/ui/MultiSaveActivity;

    invoke-static {v0}, Lcom/android/mms/ui/MultiSaveActivity;->access$000(Lcom/android/mms/ui/MultiSaveActivity;)Lcom/android/mms/ui/MultiSaveListAdapter;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/android/mms/ui/MultiSaveListAdapter;->changeSelectedState(I)V

    iget-object v0, p0, Lcom/android/mms/ui/MultiSaveActivity$1;->this$0:Lcom/android/mms/ui/MultiSaveActivity;

    invoke-static {v0}, Lcom/android/mms/ui/MultiSaveActivity;->access$100(Lcom/android/mms/ui/MultiSaveActivity;)V

    :cond_0
    return-void
.end method
