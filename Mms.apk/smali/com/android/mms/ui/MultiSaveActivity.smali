.class public Lcom/android/mms/ui/MultiSaveActivity;
.super Landroid/app/Activity;
.source "MultiSaveActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "Mms/MultiSaveActivity"


# instance fields
.field private mActionBarText:Landroid/widget/TextView;

.field private mContentResolver:Landroid/content/ContentResolver;

.field private mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

.field private mMultiSaveList:Landroid/widget/ListView;

.field private needQuit:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/mms/ui/MultiSaveActivity;->needQuit:Z

    return-void
.end method

.method static synthetic access$000(Lcom/android/mms/ui/MultiSaveActivity;)Lcom/android/mms/ui/MultiSaveListAdapter;
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiSaveActivity;

    iget-object v0, p0, Lcom/android/mms/ui/MultiSaveActivity;->mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/mms/ui/MultiSaveActivity;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiSaveActivity;

    invoke-direct {p0}, Lcom/android/mms/ui/MultiSaveActivity;->updateActionBarText()V

    return-void
.end method

.method private copyMedia()Z
    .locals 8

    const/4 v4, 0x1

    iget-object v7, p0, Lcom/android/mms/ui/MultiSaveActivity;->mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

    invoke-virtual {v7}, Lcom/android/mms/ui/MultiSaveListAdapter;->getItemList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v5, :cond_3

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/mms/ui/MultiSaveListItemData;

    invoke-virtual {v7}, Lcom/android/mms/ui/MultiSaveListItemData;->isSelected()Z

    move-result v7

    if-nez v7, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/mms/ui/MultiSaveListItemData;

    invoke-virtual {v7}, Lcom/android/mms/ui/MultiSaveListItemData;->getPduPart()Lcom/google/android/mms/pdu/PduPart;

    move-result-object v3

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/mms/ui/MultiSaveListItemData;

    invoke-virtual {v7}, Lcom/android/mms/ui/MultiSaveListItemData;->getName()Ljava/lang/String;

    move-result-object v0

    new-instance v6, Ljava/lang/String;

    invoke-virtual {v3}, Lcom/google/android/mms/pdu/PduPart;->getContentType()[B

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/String;-><init>([B)V

    invoke-static {v6}, Lcom/google/android/mms/ContentType;->isImageType(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    invoke-static {v6}, Lcom/google/android/mms/ContentType;->isVideoType(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    invoke-static {v6}, Lcom/google/android/mms/ContentType;->isAudioType(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    const-string v7, "application/ogg"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    invoke-static {v3}, Lcom/android/mms/model/FileAttachmentModel;->isSupportedFile(Lcom/google/android/mms/pdu/PduPart;)Z

    move-result v7

    if-eqz v7, :cond_0

    :cond_2
    invoke-direct {p0, v3, v0}, Lcom/android/mms/ui/MultiSaveActivity;->copyPart(Lcom/google/android/mms/pdu/PduPart;Ljava/lang/String;)Z

    move-result v7

    and-int/2addr v4, v7

    goto :goto_1

    :cond_3
    return v4
.end method

.method private copyPart(Lcom/google/android/mms/pdu/PduPart;Ljava/lang/String;)Z
    .locals 17
    .param p1    # Lcom/google/android/mms/pdu/PduPart;
    .param p2    # Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/mms/pdu/PduPart;->getDataUri()Landroid/net/Uri;

    move-result-object v13

    const-string v14, "Mms/MultiSaveActivity"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "copyPart, copy part into sdcard uri "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v8, 0x0

    const/4 v6, 0x0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mms/ui/MultiSaveActivity;->mContentResolver:Landroid/content/ContentResolver;

    invoke-virtual {v14, v13}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v8

    instance-of v14, v8, Ljava/io/FileInputStream;

    if-eqz v14, :cond_7

    move-object v0, v8

    check-cast v0, Ljava/io/FileInputStream;

    move-object v5, v0

    const-string v2, ""

    invoke-virtual/range {p0 .. p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v14

    const-string v15, "storage"

    invoke-virtual {v14, v15}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/os/storage/StorageManager;

    invoke-static {}, Landroid/os/storage/StorageManager;->getDefaultPath()Ljava/lang/String;

    move-result-object v10

    if-nez v10, :cond_2

    const-string v14, "Mms/MultiSaveActivity"

    const-string v15, "default path is null"

    invoke-static {v14, v15}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_b
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v14, 0x0

    if-eqz v8, :cond_0

    :try_start_1
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6

    :cond_0
    if-eqz v6, :cond_1

    :try_start_2
    #Replaced unresolvable odex instruction with a throw
    throw v6
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_7

    :cond_1
    :goto_0
    return v14

    :cond_2
    :try_start_3
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "/"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    sget-object v15, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "/"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v14, "Mms/MultiSaveActivity"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "copyPart,  file full path is "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p2

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/android/mms/ui/MultiSaveActivity;->getUniqueDestination(Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v14

    if-nez v14, :cond_4

    invoke-virtual {v9}, Ljava/io/File;->mkdirs()Z

    move-result v14

    if-nez v14, :cond_4

    const-string v14, "Mms/MultiSaveActivity"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "[MMS] copyPart: mkdirs for "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v9}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " failed!"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_b
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const/4 v14, 0x0

    if-eqz v8, :cond_3

    :try_start_4
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_8

    :cond_3
    if-eqz v6, :cond_1

    :try_start_5
    #Replaced unresolvable odex instruction with a throw
    throw v6
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v3

    const-string v14, "Mms/MultiSaveActivity"

    const-string v15, "IOException caught while closing stream"

    invoke-static {v14, v15, v3}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_1
    const/4 v14, 0x0

    goto/16 :goto_0

    :cond_4
    :try_start_6
    new-instance v7, Ljava/io/FileOutputStream;

    invoke-direct {v7, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_b
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    const/16 v14, 0x1f40

    :try_start_7
    new-array v1, v14, [B

    const/4 v11, 0x0

    :goto_2
    invoke-virtual {v5, v1}, Ljava/io/InputStream;->read([B)I

    move-result v11

    const/4 v14, -0x1

    if-eq v11, v14, :cond_6

    const/4 v14, 0x0

    invoke-virtual {v7, v1, v14, v11}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_2

    :catch_1
    move-exception v3

    move-object v6, v7

    :goto_3
    :try_start_8
    const-string v14, "Mms/MultiSaveActivity"

    const-string v15, "IOException caught while opening or reading stream"

    invoke-static {v14, v15, v3}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    const/4 v14, 0x0

    if-eqz v8, :cond_5

    :try_start_9
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5

    :cond_5
    if-eqz v6, :cond_1

    :try_start_a
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_2

    goto/16 :goto_0

    :catch_2
    move-exception v3

    const-string v14, "Mms/MultiSaveActivity"

    const-string v15, "IOException caught while closing stream"

    invoke-static {v14, v15, v3}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :cond_6
    :try_start_b
    new-instance v14, Landroid/content/Intent;

    const-string v15, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v16

    invoke-direct/range {v14 .. v16}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    move-object v6, v7

    :cond_7
    if-eqz v8, :cond_8

    :try_start_c
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_9

    :cond_8
    if-eqz v6, :cond_9

    :try_start_d
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_a

    :cond_9
    const/4 v14, 0x1

    goto/16 :goto_0

    :catchall_0
    move-exception v14

    :goto_4
    if-eqz v8, :cond_a

    :try_start_e
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_3

    :cond_a
    if-eqz v6, :cond_b

    :try_start_f
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_4

    :cond_b
    throw v14

    :catch_3
    move-exception v3

    const-string v14, "Mms/MultiSaveActivity"

    const-string v15, "IOException caught while closing stream"

    invoke-static {v14, v15, v3}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_5
    const/4 v14, 0x0

    goto/16 :goto_0

    :catch_4
    move-exception v3

    const-string v14, "Mms/MultiSaveActivity"

    const-string v15, "IOException caught while closing stream"

    invoke-static {v14, v15, v3}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :catch_5
    move-exception v3

    const-string v14, "Mms/MultiSaveActivity"

    const-string v15, "IOException caught while closing stream"

    invoke-static {v14, v15, v3}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_5

    :catch_6
    move-exception v3

    const-string v14, "Mms/MultiSaveActivity"

    const-string v15, "IOException caught while closing stream"

    invoke-static {v14, v15, v3}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_5

    :catch_7
    move-exception v3

    const-string v14, "Mms/MultiSaveActivity"

    const-string v15, "IOException caught while closing stream"

    invoke-static {v14, v15, v3}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_1

    :catch_8
    move-exception v3

    const-string v14, "Mms/MultiSaveActivity"

    const-string v15, "IOException caught while closing stream"

    invoke-static {v14, v15, v3}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_5

    :catch_9
    move-exception v3

    const-string v14, "Mms/MultiSaveActivity"

    const-string v15, "IOException caught while closing stream"

    invoke-static {v14, v15, v3}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_5

    :catch_a
    move-exception v3

    const-string v14, "Mms/MultiSaveActivity"

    const-string v15, "IOException caught while closing stream"

    invoke-static {v14, v15, v3}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_1

    :catchall_1
    move-exception v14

    move-object v6, v7

    goto :goto_4

    :catch_b
    move-exception v3

    goto/16 :goto_3
.end method

.method private getUniqueDestination(Ljava/lang/String;)Ljava/io/File;
    .locals 7
    .param p1    # Ljava/lang/String;

    const-string v5, "."

    invoke-virtual {p1, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v5, v4, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    const/4 v5, 0x0

    invoke-virtual {p1, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x2

    :goto_0
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_0

    new-instance v2, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    return-object v2
.end method

.method private initActivityState(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const/4 v5, 0x0

    const/4 v4, 0x1

    if-eqz p1, :cond_2

    const-string v2, "is_all_selected"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/android/mms/ui/MultiSaveActivity;->mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

    invoke-virtual {v2, v4, v5}, Lcom/android/mms/ui/MultiSaveListAdapter;->setItemsValue(Z[I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v2, "select_list"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/android/mms/ui/MultiSaveActivity;->mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

    invoke-virtual {v2, v4, v1}, Lcom/android/mms/ui/MultiSaveListAdapter;->setItemsValue(Z[I)V

    goto :goto_0

    :cond_2
    const-string v2, "Mms/MultiSaveActivity"

    const-string v3, "initActivityState, fresh start select all"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/mms/ui/MultiSaveActivity;->mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

    invoke-virtual {v2, v4, v5}, Lcom/android/mms/ui/MultiSaveListAdapter;->setItemsValue(Z[I)V

    invoke-direct {p0, v4}, Lcom/android/mms/ui/MultiSaveActivity;->markCheckedState(Z)V

    goto :goto_0
.end method

.method private initListAdapter(J)V
    .locals 10
    .param p1    # J

    invoke-static {p0, p1, p2}, Lcom/android/mms/ui/ComposeMessageActivity;->getPduBody(Landroid/content/Context;J)Lcom/google/android/mms/pdu/PduBody;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v8, "Mms/MultiSaveActivity"

    const-string v9, "initListAdapter, oops, getPduBody returns null"

    invoke-static {v8, v9}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v1}, Lcom/google/android/mms/pdu/PduBody;->getPartsNum()I

    move-result v6

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v6}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v6, :cond_5

    invoke-virtual {v1, v3}, Lcom/google/android/mms/pdu/PduBody;->getPart(I)Lcom/google/android/mms/pdu/PduPart;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/mms/pdu/PduPart;->getFilename()[B

    move-result-object v2

    const/4 v4, 0x0

    if-nez v2, :cond_1

    invoke-virtual {v5}, Lcom/google/android/mms/pdu/PduPart;->getContentLocation()[B

    move-result-object v2

    :cond_1
    if-eqz v2, :cond_2

    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v2}, Ljava/lang/String;-><init>([B)V

    :cond_2
    new-instance v8, Ljava/lang/String;

    invoke-virtual {v5}, Lcom/google/android/mms/pdu/PduPart;->getContentType()[B

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/String;-><init>([B)V

    invoke-static {v8, v4}, Lcom/android/mms/ui/MessageUtils;->getContentType(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/google/android/mms/pdu/PduPart;->setContentType([B)V

    invoke-static {v7}, Lcom/google/android/mms/ContentType;->isImageType(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_3

    invoke-static {v7}, Lcom/google/android/mms/ContentType;->isVideoType(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_3

    const-string v8, "application/ogg"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_3

    invoke-static {v7}, Lcom/google/android/mms/ContentType;->isAudioType(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_3

    invoke-static {v5}, Lcom/android/mms/model/FileAttachmentModel;->isSupportedFile(Lcom/google/android/mms/pdu/PduPart;)Z

    move-result v8

    if-eqz v8, :cond_4

    :cond_3
    new-instance v8, Lcom/android/mms/ui/MultiSaveListItemData;

    invoke-direct {v8, p0, v5, p1, p2}, Lcom/android/mms/ui/MultiSaveListItemData;-><init>(Landroid/content/Context;Lcom/google/android/mms/pdu/PduPart;J)V

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_5
    invoke-virtual {v0}, Ljava/util/ArrayList;->trimToSize()V

    new-instance v8, Lcom/android/mms/ui/MultiSaveListAdapter;

    invoke-direct {v8, p0, v0}, Lcom/android/mms/ui/MultiSaveListAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    iput-object v8, p0, Lcom/android/mms/ui/MultiSaveActivity;->mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

    iget-object v8, p0, Lcom/android/mms/ui/MultiSaveActivity;->mMultiSaveList:Landroid/widget/ListView;

    iget-object v9, p0, Lcom/android/mms/ui/MultiSaveActivity;->mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

    invoke-virtual {v8, v9}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0
.end method

.method private markCheckedState(Z)V
    .locals 6
    .param p1    # Z

    iget-object v3, p0, Lcom/android/mms/ui/MultiSaveActivity;->mMultiSaveList:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const-string v3, "Mms/MultiSaveActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "markCheckState count is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", state is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v3, p0, Lcom/android/mms/ui/MultiSaveActivity;->mMultiSaveList:Landroid/widget/ListView;

    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/mms/ui/MultiSaveListItem;

    invoke-virtual {v2, p1}, Lcom/android/mms/ui/MultiSaveListItem;->selectItem(Z)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private setUpActionBar()V
    .locals 6

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f04002b

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    const/16 v3, 0x10

    const/16 v4, 0x1a

    invoke-virtual {v0, v3, v4}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    const v3, 0x7f0e00ad

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    new-instance v3, Lcom/android/mms/ui/MultiSaveActivity$2;

    invoke-direct {v3, p0}, Lcom/android/mms/ui/MultiSaveActivity$2;-><init>(Lcom/android/mms/ui/MultiSaveActivity;)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v3, 0x7f0e00ae

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/android/mms/ui/MultiSaveActivity;->mActionBarText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    return-void
.end method

.method private updateActionBarText()V
    .locals 7

    iget-object v0, p0, Lcom/android/mms/ui/MultiSaveActivity;->mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/MultiSaveActivity;->mActionBarText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/MultiSaveActivity;->mActionBarText:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0001

    iget-object v3, p0, Lcom/android/mms/ui/MultiSaveActivity;->mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

    invoke-virtual {v3}, Lcom/android/mms/ui/MultiSaveListAdapter;->getSelectedNumber()I

    move-result v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/android/mms/ui/MultiSaveActivity;->mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

    invoke-virtual {v6}, Lcom/android/mms/ui/MultiSaveListAdapter;->getSelectedNumber()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v3, 0x7f0a0227

    invoke-virtual {p0, v3}, Landroid/app/Activity;->setTitle(I)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iput-object v3, p0, Lcom/android/mms/ui/MultiSaveActivity;->mContentResolver:Landroid/content/ContentResolver;

    const v3, 0x7f04002d

    invoke-virtual {p0, v3}, Landroid/app/Activity;->setContentView(I)V

    const v3, 0x7f0e00af

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ListView;

    iput-object v3, p0, Lcom/android/mms/ui/MultiSaveActivity;->mMultiSaveList:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/android/mms/ui/MultiSaveActivity;->mMultiSaveList:Landroid/widget/ListView;

    new-instance v4, Lcom/android/mms/ui/MultiSaveActivity$1;

    invoke-direct {v4, p0}, Lcom/android/mms/ui/MultiSaveActivity$1;-><init>(Lcom/android/mms/ui/MultiSaveActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-wide/16 v1, -0x1

    if-eqz v0, :cond_0

    const-string v3, "msgid"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "msgid"

    const-wide/16 v4, -0x1

    invoke-virtual {v0, v3, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    :cond_0
    invoke-direct {p0}, Lcom/android/mms/ui/MultiSaveActivity;->setUpActionBar()V

    invoke-direct {p0, v1, v2}, Lcom/android/mms/ui/MultiSaveActivity;->initListAdapter(J)V

    invoke-direct {p0, p1}, Lcom/android/mms/ui/MultiSaveActivity;->initActivityState(Landroid/os/Bundle;)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    invoke-virtual {p0}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0d0005

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 6
    .param p1    # Landroid/view/MenuItem;

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    return v3

    :sswitch_0
    iget-object v2, p0, Lcom/android/mms/ui/MultiSaveActivity;->mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

    if-eqz v2, :cond_0

    invoke-direct {p0, v3}, Lcom/android/mms/ui/MultiSaveActivity;->markCheckedState(Z)V

    iget-object v2, p0, Lcom/android/mms/ui/MultiSaveActivity;->mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

    invoke-virtual {v2, v3, v5}, Lcom/android/mms/ui/MultiSaveListAdapter;->setItemsValue(Z[I)V

    invoke-direct {p0}, Lcom/android/mms/ui/MultiSaveActivity;->updateActionBarText()V

    goto :goto_0

    :sswitch_1
    iget-object v2, p0, Lcom/android/mms/ui/MultiSaveActivity;->mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

    if-eqz v2, :cond_0

    invoke-direct {p0, v4}, Lcom/android/mms/ui/MultiSaveActivity;->markCheckedState(Z)V

    iget-object v2, p0, Lcom/android/mms/ui/MultiSaveActivity;->mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

    invoke-virtual {v2, v4, v5}, Lcom/android/mms/ui/MultiSaveListAdapter;->setItemsValue(Z[I)V

    invoke-direct {p0}, Lcom/android/mms/ui/MultiSaveActivity;->updateActionBarText()V

    goto :goto_0

    :sswitch_2
    iget-object v2, p0, Lcom/android/mms/ui/MultiSaveActivity;->mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

    invoke-virtual {v2}, Lcom/android/mms/ui/MultiSaveListAdapter;->getSelectedNumber()I

    move-result v2

    if-lez v2, :cond_0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/android/mms/ui/MultiSaveActivity;->copyMedia()Z

    move-result v1

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v2, "multi_save_result"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/4 v2, -0x1

    invoke-virtual {p0, v2, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x7f0e0038 -> :sswitch_0
        0x7f0e00f5 -> :sswitch_1
        0x7f0e0101 -> :sswitch_2
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1    # Landroid/view/Menu;

    invoke-direct {p0}, Lcom/android/mms/ui/MultiSaveActivity;->updateActionBarText()V

    const/4 v0, 0x1

    return v0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const-string v3, "Mms/MultiSaveActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onSaveInstanceState, with bundle "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v3, p0, Lcom/android/mms/ui/MultiSaveActivity;->mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/mms/ui/MultiSaveActivity;->mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

    invoke-virtual {v3}, Lcom/android/mms/ui/MultiSaveListAdapter;->isAllSelected()Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "is_all_selected"

    const/4 v4, 0x1

    invoke-virtual {p1, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/android/mms/ui/MultiSaveActivity;->mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

    invoke-virtual {v3}, Lcom/android/mms/ui/MultiSaveListAdapter;->getSelectedNumber()I

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/mms/ui/MultiSaveActivity;->mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

    invoke-virtual {v3}, Lcom/android/mms/ui/MultiSaveListAdapter;->getSelectedNumber()I

    move-result v3

    new-array v0, v3, [I

    iget-object v3, p0, Lcom/android/mms/ui/MultiSaveActivity;->mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

    invoke-virtual {v3}, Lcom/android/mms/ui/MultiSaveListAdapter;->getItemList()Ljava/util/ArrayList;

    move-result-object v2

    const/4 v1, 0x0

    :goto_1
    array-length v3, v0

    if-ge v1, v3, :cond_3

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/mms/ui/MultiSaveListItemData;

    invoke-virtual {v3}, Lcom/android/mms/ui/MultiSaveListItemData;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_2

    aput v1, v0, v1

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    const-string v3, "select_list"

    invoke-virtual {p1, v3, v0}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    goto :goto_0
.end method
