.class public Lcom/android/mms/ui/MultiSaveListItem;
.super Landroid/widget/RelativeLayout;
.source "MultiSaveListItem.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "Mms/MultiSaveListItem"


# instance fields
.field private mCheckbox:Landroid/widget/CheckBox;

.field private mNameView:Landroid/widget/TextView;

.field private mSizeView:Landroid/widget/TextView;

.field private mThumbnailView:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private markItem()V
    .locals 2

    iget-object v0, p0, Lcom/android/mms/ui/MultiSaveListItem;->mCheckbox:Landroid/widget/CheckBox;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    const v0, 0x7f0200a8

    invoke-virtual {p0, v0}, Landroid/view/View;->setBackgroundResource(I)V

    return-void
.end method

.method private unMarkItem()V
    .locals 2

    iget-object v0, p0, Lcom/android/mms/ui/MultiSaveListItem;->mCheckbox:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    const v0, 0x7f0200ab

    invoke-virtual {p0, v0}, Landroid/view/View;->setBackgroundResource(I)V

    return-void
.end method


# virtual methods
.method public clickListItem()V
    .locals 1

    iget-object v0, p0, Lcom/android/mms/ui/MultiSaveListItem;->mCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/mms/ui/MultiSaveListItem;->unMarkItem()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/android/mms/ui/MultiSaveListItem;->markItem()V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/view/View;->onFinishInflate()V

    const v0, 0x7f0e00b2

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/mms/ui/MultiSaveListItem;->mNameView:Landroid/widget/TextView;

    const v0, 0x7f0e00b3

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/mms/ui/MultiSaveListItem;->mSizeView:Landroid/widget/TextView;

    const v0, 0x7f0e00b1

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/mms/ui/MultiSaveListItem;->mThumbnailView:Landroid/widget/ImageView;

    const v0, 0x7f0e00b0

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/android/mms/ui/MultiSaveListItem;->mCheckbox:Landroid/widget/CheckBox;

    return-void
.end method

.method public selectItem(Z)V
    .locals 0
    .param p1    # Z

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/android/mms/ui/MultiSaveListItem;->markItem()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/android/mms/ui/MultiSaveListItem;->unMarkItem()V

    goto :goto_0
.end method
