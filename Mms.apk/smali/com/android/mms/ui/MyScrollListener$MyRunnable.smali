.class Lcom/android/mms/ui/MyScrollListener$MyRunnable;
.super Ljava/lang/Object;
.source "MyScrollListener.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/ui/MyScrollListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyRunnable"
.end annotation


# instance fields
.field private bindDefaultViewTimes:I

.field private bindViewTimes:I

.field private count:I

.field mListAdapter:Lcom/android/mms/ui/MessageCursorAdapter;

.field private mNeedRun:Z

.field final synthetic this$0:Lcom/android/mms/ui/MyScrollListener;


# direct methods
.method public constructor <init>(Lcom/android/mms/ui/MyScrollListener;Z)V
    .locals 1
    .param p2    # Z

    iput-object p1, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->this$0:Lcom/android/mms/ui/MyScrollListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->mNeedRun:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->mListAdapter:Lcom/android/mms/ui/MessageCursorAdapter;

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->count:I

    const/4 v0, 0x3

    iput v0, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->bindViewTimes:I

    iget v0, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->bindViewTimes:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->bindDefaultViewTimes:I

    iput-boolean p2, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->mNeedRun:Z

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    :goto_0
    iget-object v2, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->this$0:Lcom/android/mms/ui/MyScrollListener;

    invoke-static {v2}, Lcom/android/mms/ui/MyScrollListener;->access$000(Lcom/android/mms/ui/MyScrollListener;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    iget-boolean v2, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->mNeedRun:Z

    if-eqz v2, :cond_3

    invoke-static {}, Lcom/android/mms/ui/MyScrollListener;->access$100()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "OnScrollListener.run(): count="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->count:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v2, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->count:I

    iget v3, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->bindViewTimes:I

    rem-int/2addr v2, v3

    iget v3, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->bindDefaultViewTimes:I

    if-eq v2, v3, :cond_1

    iget-object v2, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->mListAdapter:Lcom/android/mms/ui/MessageCursorAdapter;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/android/mms/ui/MessageCursorAdapter;->setIsScrolling(Z)V

    :goto_2
    iget v2, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->count:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->count:I

    iget-object v2, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->this$0:Lcom/android/mms/ui/MyScrollListener;

    invoke-static {v2}, Lcom/android/mms/ui/MyScrollListener;->access$000(Lcom/android/mms/ui/MyScrollListener;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->mListAdapter:Lcom/android/mms/ui/MessageCursorAdapter;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/android/mms/ui/MessageCursorAdapter;->setIsScrolling(Z)V

    goto :goto_2

    :cond_2
    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->this$0:Lcom/android/mms/ui/MyScrollListener;

    invoke-static {v2}, Lcom/android/mms/ui/MyScrollListener;->access$200(Lcom/android/mms/ui/MyScrollListener;)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_3
    :try_start_1
    monitor-exit v1

    goto :goto_1

    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {}, Lcom/android/mms/ui/MyScrollListener;->access$100()Ljava/lang/String;

    move-result-object v2

    const-string v3, "wait has been interrupted."

    invoke-static {v2, v3, v0}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    :cond_3
    iget-object v2, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->this$0:Lcom/android/mms/ui/MyScrollListener;

    invoke-static {v2}, Lcom/android/mms/ui/MyScrollListener;->access$000(Lcom/android/mms/ui/MyScrollListener;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Lcom/android/mms/ui/MyScrollListener;->access$100()Ljava/lang/String;

    move-result-object v2

    const-string v3, "OnScrollListener.run(): listener is wait."

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    monitor-enter p0

    :try_start_3
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :goto_4
    :try_start_4
    monitor-exit p0

    goto :goto_0

    :catchall_1
    move-exception v2

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v2

    :catch_1
    move-exception v0

    :try_start_5
    invoke-static {}, Lcom/android/mms/ui/MyScrollListener;->access$100()Ljava/lang/String;

    move-result-object v2

    const-string v3, "wait has been interrupted."

    invoke-static {v2, v3, v0}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_4
.end method

.method public setConversationListAdapter(Lcom/android/mms/ui/MessageCursorAdapter;)V
    .locals 0
    .param p1    # Lcom/android/mms/ui/MessageCursorAdapter;

    iput-object p1, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->mListAdapter:Lcom/android/mms/ui/MessageCursorAdapter;

    return-void
.end method

.method public setNeedRun(Z)V
    .locals 1
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->mNeedRun:Z

    iget-boolean v0, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->mNeedRun:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->count:I

    :cond_0
    return-void
.end method
