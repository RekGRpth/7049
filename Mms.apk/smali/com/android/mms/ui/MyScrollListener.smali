.class public Lcom/android/mms/ui/MyScrollListener;
.super Ljava/lang/Object;
.source "MyScrollListener.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mms/ui/MyScrollListener$MyRunnable;
    }
.end annotation


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private HANDLE_FLING_THREAD_WAIT_TIME:I

.field private mMinCursorCount:I

.field private mNeedDestroy:Z

.field private mThreadName:Ljava/lang/String;

.field private myThread:Ljava/lang/Thread;

.field private runnable:Lcom/android/mms/ui/MyScrollListener$MyRunnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "Mms/ScrollListener"

    sput-object v0, Lcom/android/mms/ui/MyScrollListener;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 2
    .param p1    # I
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0xc8

    iput v0, p0, Lcom/android/mms/ui/MyScrollListener;->HANDLE_FLING_THREAD_WAIT_TIME:I

    const-string v0, "ConversationList_Scroll_Tread"

    iput-object v0, p0, Lcom/android/mms/ui/MyScrollListener;->mThreadName:Ljava/lang/String;

    const/16 v0, 0x64

    iput v0, p0, Lcom/android/mms/ui/MyScrollListener;->mMinCursorCount:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mms/ui/MyScrollListener;->myThread:Ljava/lang/Thread;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/mms/ui/MyScrollListener;->mNeedDestroy:Z

    new-instance v0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lcom/android/mms/ui/MyScrollListener$MyRunnable;-><init>(Lcom/android/mms/ui/MyScrollListener;Z)V

    iput-object v0, p0, Lcom/android/mms/ui/MyScrollListener;->runnable:Lcom/android/mms/ui/MyScrollListener$MyRunnable;

    iput p1, p0, Lcom/android/mms/ui/MyScrollListener;->mMinCursorCount:I

    iput-object p2, p0, Lcom/android/mms/ui/MyScrollListener;->mThreadName:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/android/mms/ui/MyScrollListener;)Z
    .locals 1
    .param p0    # Lcom/android/mms/ui/MyScrollListener;

    iget-boolean v0, p0, Lcom/android/mms/ui/MyScrollListener;->mNeedDestroy:Z

    return v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/mms/ui/MyScrollListener;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/mms/ui/MyScrollListener;)I
    .locals 1
    .param p0    # Lcom/android/mms/ui/MyScrollListener;

    iget v0, p0, Lcom/android/mms/ui/MyScrollListener;->HANDLE_FLING_THREAD_WAIT_TIME:I

    return v0
.end method


# virtual methods
.method public destroyThread()V
    .locals 3

    iget-object v1, p0, Lcom/android/mms/ui/MyScrollListener;->runnable:Lcom/android/mms/ui/MyScrollListener$MyRunnable;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/mms/ui/MyScrollListener;->TAG:Ljava/lang/String;

    const-string v2, "destroy thread."

    invoke-static {v0, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/mms/ui/MyScrollListener;->mNeedDestroy:Z

    iget-object v0, p0, Lcom/android/mms/ui/MyScrollListener;->runnable:Lcom/android/mms/ui/MyScrollListener$MyRunnable;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->setNeedRun(Z)V

    iget-object v0, p0, Lcom/android/mms/ui/MyScrollListener;->runnable:Lcom/android/mms/ui/MyScrollListener$MyRunnable;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 0
    .param p1    # Landroid/widget/AbsListView;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 5
    .param p1    # Landroid/widget/AbsListView;
    .param p2    # I

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Lcom/android/mms/ui/MessageCursorAdapter;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/CursorAdapter;->getCount()I

    move-result v1

    iget v2, p0, Lcom/android/mms/ui/MyScrollListener;->mMinCursorCount:I

    if-lt v1, v2, :cond_0

    if-ne p2, v3, :cond_1

    sget-object v1, Lcom/android/mms/ui/MyScrollListener;->TAG:Ljava/lang/String;

    const-string v2, "OnScrollListener.onScrollStateChanged(): on touch state."

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0, v4}, Lcom/android/mms/ui/MessageCursorAdapter;->setIsScrolling(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x2

    if-ne p2, v1, :cond_2

    sget-object v1, Lcom/android/mms/ui/MyScrollListener;->TAG:Ljava/lang/String;

    const-string v2, "OnScrollListener.onScrollStateChanged(): scrolling..."

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0, v3}, Lcom/android/mms/ui/MessageCursorAdapter;->setIsScrolling(Z)V

    iget-object v2, p0, Lcom/android/mms/ui/MyScrollListener;->runnable:Lcom/android/mms/ui/MyScrollListener$MyRunnable;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/android/mms/ui/MyScrollListener;->runnable:Lcom/android/mms/ui/MyScrollListener$MyRunnable;

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    iget-object v1, p0, Lcom/android/mms/ui/MyScrollListener;->runnable:Lcom/android/mms/ui/MyScrollListener$MyRunnable;

    invoke-virtual {v1, v0}, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->setConversationListAdapter(Lcom/android/mms/ui/MessageCursorAdapter;)V

    iget-object v1, p0, Lcom/android/mms/ui/MyScrollListener;->runnable:Lcom/android/mms/ui/MyScrollListener$MyRunnable;

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->setNeedRun(Z)V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lcom/android/mms/ui/MyScrollListener;->myThread:Ljava/lang/Thread;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/Thread;

    iget-object v2, p0, Lcom/android/mms/ui/MyScrollListener;->runnable:Lcom/android/mms/ui/MyScrollListener$MyRunnable;

    iget-object v3, p0, Lcom/android/mms/ui/MyScrollListener;->mThreadName:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/mms/ui/MyScrollListener;->myThread:Ljava/lang/Thread;

    iget-object v1, p0, Lcom/android/mms/ui/MyScrollListener;->myThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    goto :goto_0

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    :cond_2
    if-nez p2, :cond_0

    sget-object v1, Lcom/android/mms/ui/MyScrollListener;->TAG:Ljava/lang/String;

    const-string v2, "OnScrollListener.onScrollStateChanged(): stop scrolling!"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/mms/ui/MyScrollListener;->myThread:Ljava/lang/Thread;

    if-eqz v1, :cond_3

    iget-object v2, p0, Lcom/android/mms/ui/MyScrollListener;->runnable:Lcom/android/mms/ui/MyScrollListener$MyRunnable;

    monitor-enter v2

    :try_start_2
    iget-object v1, p0, Lcom/android/mms/ui/MyScrollListener;->runnable:Lcom/android/mms/ui/MyScrollListener$MyRunnable;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->setNeedRun(Z)V

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_3
    invoke-virtual {v0, v4}, Lcom/android/mms/ui/MessageCursorAdapter;->setIsScrolling(Z)V

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    goto :goto_0

    :catchall_1
    move-exception v1

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v1
.end method
