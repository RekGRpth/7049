.class public Lcom/android/mms/ui/QuickContactDivot;
.super Landroid/widget/QuickContactBadge;
.source "QuickContactDivot.java"

# interfaces
.implements Lcom/android/mms/ui/Divot;


# instance fields
.field private mDensity:F

.field private mDrawable:Landroid/graphics/drawable/Drawable;

.field private mDrawableIntrinsicHeight:I

.field private mDrawableIntrinsicWidth:I

.field private mPosition:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/QuickContactBadge;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/mms/ui/QuickContactDivot;->initialize(Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/QuickContactBadge;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0, p2}, Lcom/android/mms/ui/QuickContactDivot;->initialize(Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/QuickContactBadge;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0, p2}, Lcom/android/mms/ui/QuickContactDivot;->initialize(Landroid/util/AttributeSet;)V

    return-void
.end method

.method private computeBounds(Landroid/graphics/Canvas;)V
    .locals 13
    .param p1    # Landroid/graphics/Canvas;

    const/4 v3, 0x0

    const/4 v6, 0x0

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v5

    div-int/lit8 v4, v5, 0x2

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/mms/ui/QuickContactDivot;->getCloseOffset()F

    move-result v7

    float-to-int v1, v7

    iget v7, p0, Lcom/android/mms/ui/QuickContactDivot;->mPosition:I

    sparse-switch v7, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    iget-object v7, p0, Lcom/android/mms/ui/QuickContactDivot;->mDrawable:Landroid/graphics/drawable/Drawable;

    iget v8, p0, Lcom/android/mms/ui/QuickContactDivot;->mDrawableIntrinsicWidth:I

    sub-int v8, v5, v8

    add-int/lit8 v9, v1, 0x0

    add-int/lit8 v10, v1, 0x0

    iget v11, p0, Lcom/android/mms/ui/QuickContactDivot;->mDrawableIntrinsicHeight:I

    add-int/2addr v10, v11

    invoke-virtual {v7, v8, v9, v5, v10}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto :goto_0

    :sswitch_1
    iget-object v7, p0, Lcom/android/mms/ui/QuickContactDivot;->mDrawable:Landroid/graphics/drawable/Drawable;

    const/4 v8, 0x0

    add-int/lit8 v9, v1, 0x0

    iget v10, p0, Lcom/android/mms/ui/QuickContactDivot;->mDrawableIntrinsicWidth:I

    add-int/lit8 v10, v10, 0x0

    add-int/lit8 v11, v1, 0x0

    iget v12, p0, Lcom/android/mms/ui/QuickContactDivot;->mDrawableIntrinsicHeight:I

    add-int/2addr v11, v12

    invoke-virtual {v7, v8, v9, v10, v11}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto :goto_0

    :sswitch_2
    iget v7, p0, Lcom/android/mms/ui/QuickContactDivot;->mDrawableIntrinsicWidth:I

    div-int/lit8 v2, v7, 0x2

    iget-object v7, p0, Lcom/android/mms/ui/QuickContactDivot;->mDrawable:Landroid/graphics/drawable/Drawable;

    sub-int v8, v4, v2

    iget v9, p0, Lcom/android/mms/ui/QuickContactDivot;->mDrawableIntrinsicHeight:I

    sub-int v9, v0, v9

    add-int v10, v4, v2

    invoke-virtual {v7, v8, v9, v10, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x4 -> :sswitch_0
        0xb -> :sswitch_2
    .end sparse-switch
.end method

.method private initialize(Landroid/util/AttributeSet;)V
    .locals 5
    .param p1    # Landroid/util/AttributeSet;

    if-eqz p1, :cond_0

    const/4 v1, 0x0

    const-string v2, "position"

    sget-object v3, Lcom/android/mms/ui/QuickContactDivot;->sPositionChoices:[Ljava/lang/String;

    const/4 v4, -0x1

    invoke-interface {p1, v1, v2, v3, v4}, Landroid/util/AttributeSet;->getAttributeListValue(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/android/mms/ui/QuickContactDivot;->mPosition:I

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    iput v1, p0, Lcom/android/mms/ui/QuickContactDivot;->mDensity:F

    invoke-direct {p0}, Lcom/android/mms/ui/QuickContactDivot;->setDrawable()V

    return-void
.end method

.method private setDrawable()V
    .locals 2

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/android/mms/ui/QuickContactDivot;->mPosition:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    iget-object v1, p0, Lcom/android/mms/ui/QuickContactDivot;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iput v1, p0, Lcom/android/mms/ui/QuickContactDivot;->mDrawableIntrinsicWidth:I

    iget-object v1, p0, Lcom/android/mms/ui/QuickContactDivot;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    iput v1, p0, Lcom/android/mms/ui/QuickContactDivot;->mDrawableIntrinsicHeight:I

    return-void

    :pswitch_0
    const v1, 0x7f0200b2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mms/ui/QuickContactDivot;->mDrawable:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :pswitch_1
    const v1, 0x7f0200b1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mms/ui/QuickContactDivot;->mDrawable:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public asImageView()Landroid/widget/ImageView;
    .locals 0

    return-object p0
.end method

.method public assignContactFromEmail(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Landroid/widget/QuickContactBadge;->assignContactFromEmail(Ljava/lang/String;Z)V

    return-void
.end method

.method public getCloseOffset()F
    .locals 2

    const/high16 v0, 0x41400000

    iget v1, p0, Lcom/android/mms/ui/QuickContactDivot;->mDensity:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getFarOffset()F
    .locals 2

    invoke-virtual {p0}, Lcom/android/mms/ui/QuickContactDivot;->getCloseOffset()F

    move-result v0

    iget v1, p0, Lcom/android/mms/ui/QuickContactDivot;->mDrawableIntrinsicHeight:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    return v0
.end method

.method public getPosition()I
    .locals 1

    iget v0, p0, Lcom/android/mms/ui/QuickContactDivot;->mPosition:I

    return v0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1    # Landroid/graphics/Canvas;

    invoke-super {p0, p1}, Landroid/widget/QuickContactBadge;->onDraw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    invoke-direct {p0, p1}, Lcom/android/mms/ui/QuickContactDivot;->computeBounds(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/android/mms/ui/QuickContactDivot;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method

.method public setPosition(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/mms/ui/QuickContactDivot;->mPosition:I

    invoke-direct {p0}, Lcom/android/mms/ui/QuickContactDivot;->setDrawable()V

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method
