.class public interface abstract Lcom/android/mms/ui/ScaleDetector$OnScaleListener;
.super Ljava/lang/Object;
.source "ScaleDetector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/ui/ScaleDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnScaleListener"
.end annotation


# virtual methods
.method public abstract onScale(Lcom/android/mms/ui/ScaleDetector;)Z
.end method

.method public abstract onScaleEnd(Lcom/android/mms/ui/ScaleDetector;)V
.end method

.method public abstract onScaleStart(Lcom/android/mms/ui/ScaleDetector;)Z
.end method
