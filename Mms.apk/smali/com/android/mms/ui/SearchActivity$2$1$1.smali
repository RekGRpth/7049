.class Lcom/android/mms/ui/SearchActivity$2$1$1;
.super Ljava/lang/Object;
.source "SearchActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/SearchActivity$2$1;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/android/mms/ui/SearchActivity$2$1;

.field final synthetic val$msgBox:I

.field final synthetic val$msgType:I

.field final synthetic val$rowid:J

.field final synthetic val$threadId:J

.field final synthetic val$threadType:I


# direct methods
.method constructor <init>(Lcom/android/mms/ui/SearchActivity$2$1;IIIJJ)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/SearchActivity$2$1$1;->this$2:Lcom/android/mms/ui/SearchActivity$2$1;

    iput p2, p0, Lcom/android/mms/ui/SearchActivity$2$1$1;->val$threadType:I

    iput p3, p0, Lcom/android/mms/ui/SearchActivity$2$1$1;->val$msgType:I

    iput p4, p0, Lcom/android/mms/ui/SearchActivity$2$1$1;->val$msgBox:I

    iput-wide p5, p0, Lcom/android/mms/ui/SearchActivity$2$1$1;->val$rowid:J

    iput-wide p7, p0, Lcom/android/mms/ui/SearchActivity$2$1$1;->val$threadId:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12
    .param p1    # Landroid/view/View;

    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x1

    const/4 v5, 0x0

    invoke-static {}, Lcom/android/mms/MmsConfig;->getMmsDirMode()Z

    move-result v4

    const-string v6, "Mms/SearchActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onClickIntent1 dirMode ="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v4, :cond_2

    iget v6, p0, Lcom/android/mms/ui/SearchActivity$2$1$1;->val$threadType:I

    if-ne v6, v9, :cond_1

    const-string v6, "Mms/WapPush"

    const-string v7, "SearchActivity: onClickIntent WPMessageActivity."

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v5, Landroid/content/Intent;

    iget-object v6, p0, Lcom/android/mms/ui/SearchActivity$2$1$1;->this$2:Lcom/android/mms/ui/SearchActivity$2$1;

    iget-object v6, v6, Lcom/android/mms/ui/SearchActivity$2$1;->this$1:Lcom/android/mms/ui/SearchActivity$2;

    iget-object v6, v6, Lcom/android/mms/ui/SearchActivity$2;->this$0:Lcom/android/mms/ui/SearchActivity;

    const-class v7, Lcom/android/mms/ui/WPMessageActivity;

    invoke-direct {v5, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    :cond_0
    :goto_0
    const-string v6, "thread_id"

    iget-wide v7, p0, Lcom/android/mms/ui/SearchActivity$2$1$1;->val$threadId:J

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v6, "highlight"

    iget-object v7, p0, Lcom/android/mms/ui/SearchActivity$2$1$1;->this$2:Lcom/android/mms/ui/SearchActivity$2$1;

    iget-object v7, v7, Lcom/android/mms/ui/SearchActivity$2$1;->this$1:Lcom/android/mms/ui/SearchActivity$2;

    iget-object v7, v7, Lcom/android/mms/ui/SearchActivity$2;->this$0:Lcom/android/mms/ui/SearchActivity;

    invoke-static {v7}, Lcom/android/mms/ui/SearchActivity;->access$300(Lcom/android/mms/ui/SearchActivity;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v6, "select_id"

    iget-wide v7, p0, Lcom/android/mms/ui/SearchActivity$2$1$1;->val$rowid:J

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    iget-object v6, p0, Lcom/android/mms/ui/SearchActivity$2$1$1;->this$2:Lcom/android/mms/ui/SearchActivity$2$1;

    iget-object v6, v6, Lcom/android/mms/ui/SearchActivity$2$1;->this$1:Lcom/android/mms/ui/SearchActivity$2;

    iget-object v6, v6, Lcom/android/mms/ui/SearchActivity$2;->this$0:Lcom/android/mms/ui/SearchActivity;

    invoke-virtual {v6, v5}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void

    :cond_1
    const-string v6, "Mms/WapPush"

    const-string v7, "SearchActivity: onClickIntent ComposeMessageActivity."

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v5, Landroid/content/Intent;

    iget-object v6, p0, Lcom/android/mms/ui/SearchActivity$2$1$1;->this$2:Lcom/android/mms/ui/SearchActivity$2$1;

    iget-object v6, v6, Lcom/android/mms/ui/SearchActivity$2$1;->this$1:Lcom/android/mms/ui/SearchActivity$2;

    iget-object v6, v6, Lcom/android/mms/ui/SearchActivity$2;->this$0:Lcom/android/mms/ui/SearchActivity;

    const-class v7, Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-direct {v5, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_0

    :cond_2
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    iget v6, p0, Lcom/android/mms/ui/SearchActivity$2$1$1;->val$msgType:I

    if-ne v6, v9, :cond_4

    iget v6, p0, Lcom/android/mms/ui/SearchActivity$2$1$1;->val$msgBox:I

    if-ne v6, v10, :cond_3

    new-instance v5, Landroid/content/Intent;

    iget-object v6, p0, Lcom/android/mms/ui/SearchActivity$2$1$1;->this$2:Lcom/android/mms/ui/SearchActivity$2$1;

    iget-object v6, v6, Lcom/android/mms/ui/SearchActivity$2$1;->this$1:Lcom/android/mms/ui/SearchActivity$2;

    iget-object v6, v6, Lcom/android/mms/ui/SearchActivity$2;->this$0:Lcom/android/mms/ui/SearchActivity;

    const-class v7, Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-direct {v5, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_0

    :cond_3
    const-string v6, "content://sms/"

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v6, p0, Lcom/android/mms/ui/SearchActivity$2$1$1;->this$2:Lcom/android/mms/ui/SearchActivity$2$1;

    iget-object v6, v6, Lcom/android/mms/ui/SearchActivity$2$1;->this$1:Lcom/android/mms/ui/SearchActivity$2;

    iget-object v6, v6, Lcom/android/mms/ui/SearchActivity$2;->this$0:Lcom/android/mms/ui/SearchActivity;

    const-class v7, Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    iget-wide v6, p0, Lcom/android/mms/ui/SearchActivity$2$1$1;->val$rowid:J

    invoke-static {v2, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const-string v6, "msg_type"

    invoke-virtual {v5, v6, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    :cond_4
    iget v6, p0, Lcom/android/mms/ui/SearchActivity$2$1$1;->val$msgType:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_6

    iget v6, p0, Lcom/android/mms/ui/SearchActivity$2$1$1;->val$msgBox:I

    if-ne v6, v10, :cond_5

    new-instance v5, Landroid/content/Intent;

    iget-object v6, p0, Lcom/android/mms/ui/SearchActivity$2$1$1;->this$2:Lcom/android/mms/ui/SearchActivity$2$1;

    iget-object v6, v6, Lcom/android/mms/ui/SearchActivity$2$1;->this$1:Lcom/android/mms/ui/SearchActivity$2;

    iget-object v6, v6, Lcom/android/mms/ui/SearchActivity$2;->this$0:Lcom/android/mms/ui/SearchActivity;

    const-class v7, Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-direct {v5, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto/16 :goto_0

    :cond_5
    const-string v6, "content://mms/"

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v6, p0, Lcom/android/mms/ui/SearchActivity$2$1$1;->this$2:Lcom/android/mms/ui/SearchActivity$2$1;

    iget-object v6, v6, Lcom/android/mms/ui/SearchActivity$2$1;->this$1:Lcom/android/mms/ui/SearchActivity$2;

    iget-object v6, v6, Lcom/android/mms/ui/SearchActivity$2;->this$0:Lcom/android/mms/ui/SearchActivity;

    const-class v7, Lcom/android/mms/ui/MmsPlayerActivity;

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    iget-wide v6, p0, Lcom/android/mms/ui/SearchActivity$2$1$1;->val$rowid:J

    invoke-static {v1, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const-string v6, "dirmode"

    invoke-virtual {v5, v6, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto/16 :goto_0

    :cond_6
    iget v6, p0, Lcom/android/mms/ui/SearchActivity$2$1$1;->val$msgType:I

    if-ne v6, v10, :cond_7

    const-string v6, "content://wappush/"

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v6, p0, Lcom/android/mms/ui/SearchActivity$2$1$1;->this$2:Lcom/android/mms/ui/SearchActivity$2$1;

    iget-object v6, v6, Lcom/android/mms/ui/SearchActivity$2$1;->this$1:Lcom/android/mms/ui/SearchActivity$2;

    iget-object v6, v6, Lcom/android/mms/ui/SearchActivity$2;->this$0:Lcom/android/mms/ui/SearchActivity;

    const-class v7, Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    iget-wide v6, p0, Lcom/android/mms/ui/SearchActivity$2$1$1;->val$rowid:J

    invoke-static {v3, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const-string v6, "msg_type"

    invoke-virtual {v5, v6, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto/16 :goto_0

    :cond_7
    iget v6, p0, Lcom/android/mms/ui/SearchActivity$2$1$1;->val$msgType:I

    if-ne v6, v11, :cond_0

    const-string v6, "content://cb/messages/"

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v6, p0, Lcom/android/mms/ui/SearchActivity$2$1$1;->this$2:Lcom/android/mms/ui/SearchActivity$2$1;

    iget-object v6, v6, Lcom/android/mms/ui/SearchActivity$2$1;->this$1:Lcom/android/mms/ui/SearchActivity$2;

    iget-object v6, v6, Lcom/android/mms/ui/SearchActivity$2;->this$0:Lcom/android/mms/ui/SearchActivity;

    const-class v7, Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    iget-wide v6, p0, Lcom/android/mms/ui/SearchActivity$2$1$1;->val$rowid:J

    invoke-static {v0, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const-string v6, "msg_type"

    invoke-virtual {v5, v6, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto/16 :goto_0
.end method
