.class Lcom/android/mms/ui/SelectCardPreferenceActivity$PositiveButtonListener;
.super Ljava/lang/Object;
.source "SelectCardPreferenceActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/ui/SelectCardPreferenceActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PositiveButtonListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/SelectCardPreferenceActivity;


# direct methods
.method private constructor <init>(Lcom/android/mms/ui/SelectCardPreferenceActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity$PositiveButtonListener;->this$0:Lcom/android/mms/ui/SelectCardPreferenceActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/mms/ui/SelectCardPreferenceActivity;Lcom/android/mms/ui/SelectCardPreferenceActivity$1;)V
    .locals 0
    .param p1    # Lcom/android/mms/ui/SelectCardPreferenceActivity;
    .param p2    # Lcom/android/mms/ui/SelectCardPreferenceActivity$1;

    invoke-direct {p0, p1}, Lcom/android/mms/ui/SelectCardPreferenceActivity$PositiveButtonListener;-><init>(Lcom/android/mms/ui/SelectCardPreferenceActivity;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v1, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity$PositiveButtonListener;->this$0:Lcom/android/mms/ui/SelectCardPreferenceActivity;

    invoke-static {}, Lcom/mediatek/telephony/TelephonyManagerEx;->getDefault()Lcom/mediatek/telephony/TelephonyManagerEx;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/mms/ui/SelectCardPreferenceActivity;->access$402(Lcom/android/mms/ui/SelectCardPreferenceActivity;Lcom/mediatek/telephony/TelephonyManagerEx;)Lcom/mediatek/telephony/TelephonyManagerEx;

    iget-object v1, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity$PositiveButtonListener;->this$0:Lcom/android/mms/ui/SelectCardPreferenceActivity;

    invoke-static {v1}, Lcom/android/mms/ui/SelectCardPreferenceActivity;->access$500(Lcom/android/mms/ui/SelectCardPreferenceActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Mms/SelectCardPreferenceActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setScNumber is: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "Mms/SelectCardPreferenceActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mCurrentSim is: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity$PositiveButtonListener;->this$0:Lcom/android/mms/ui/SelectCardPreferenceActivity;

    invoke-static {v3}, Lcom/android/mms/ui/SelectCardPreferenceActivity;->access$600(Lcom/android/mms/ui/SelectCardPreferenceActivity;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/android/mms/ui/SelectCardPreferenceActivity$PositiveButtonListener$1;

    invoke-direct {v2, p0}, Lcom/android/mms/ui/SelectCardPreferenceActivity$PositiveButtonListener$1;-><init>(Lcom/android/mms/ui/SelectCardPreferenceActivity$PositiveButtonListener;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    return-void
.end method
