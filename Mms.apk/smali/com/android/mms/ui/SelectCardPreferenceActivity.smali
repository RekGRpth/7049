.class public Lcom/android/mms/ui/SelectCardPreferenceActivity;
.super Landroid/preference/PreferenceActivity;
.source "SelectCardPreferenceActivity.java"

# interfaces
.implements Lcom/android/mms/ui/AdvancedEditorPreference$GetSimInfo;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mms/ui/SelectCardPreferenceActivity$NegativeButtonListener;,
        Lcom/android/mms/ui/SelectCardPreferenceActivity$PositiveButtonListener;
    }
.end annotation


# static fields
.field private static final MAX_EDITABLE_LENGTH:I = 0x14

.field public static final SUB_TITLE_NAME:Ljava/lang/String; = "sub_title_name"

.field private static final TAG:Ljava/lang/String; = "Mms/SelectCardPreferenceActivity"


# instance fields
.field private mCurrentSim:I

.field mIntentPreference:Ljava/lang/String;

.field private mListSimInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/provider/Telephony$SIMInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mNumberText:Landroid/widget/EditText;

.field private mNumberTextDialog:Landroid/app/AlertDialog$Builder;

.field private mSaveLocDialog:Landroid/app/AlertDialog;

.field private mSim1:Lcom/android/mms/ui/AdvancedEditorPreference;

.field private mSim1Number:Ljava/lang/String;

.field private mSim2:Lcom/android/mms/ui/AdvancedEditorPreference;

.field private mSim2Number:Ljava/lang/String;

.field private mSim3:Lcom/android/mms/ui/AdvancedEditorPreference;

.field private mSim3Number:Ljava/lang/String;

.field private mSim4:Lcom/android/mms/ui/AdvancedEditorPreference;

.field private mSim4Number:Ljava/lang/String;

.field private mSimCount:I

.field private mSpref:Landroid/content/SharedPreferences;

.field private mTelephonyManager:Lcom/mediatek/telephony/TelephonyManagerEx;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mCurrentSim:I

    return-void
.end method

.method static synthetic access$000(Lcom/android/mms/ui/SelectCardPreferenceActivity;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0    # Lcom/android/mms/ui/SelectCardPreferenceActivity;

    iget-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSpref:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/mms/ui/SelectCardPreferenceActivity;)Landroid/app/AlertDialog;
    .locals 1
    .param p0    # Lcom/android/mms/ui/SelectCardPreferenceActivity;

    iget-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSaveLocDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$102(Lcom/android/mms/ui/SelectCardPreferenceActivity;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0    # Lcom/android/mms/ui/SelectCardPreferenceActivity;
    .param p1    # Landroid/app/AlertDialog;

    iput-object p1, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSaveLocDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$400(Lcom/android/mms/ui/SelectCardPreferenceActivity;)Lcom/mediatek/telephony/TelephonyManagerEx;
    .locals 1
    .param p0    # Lcom/android/mms/ui/SelectCardPreferenceActivity;

    iget-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mTelephonyManager:Lcom/mediatek/telephony/TelephonyManagerEx;

    return-object v0
.end method

.method static synthetic access$402(Lcom/android/mms/ui/SelectCardPreferenceActivity;Lcom/mediatek/telephony/TelephonyManagerEx;)Lcom/mediatek/telephony/TelephonyManagerEx;
    .locals 0
    .param p0    # Lcom/android/mms/ui/SelectCardPreferenceActivity;
    .param p1    # Lcom/mediatek/telephony/TelephonyManagerEx;

    iput-object p1, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mTelephonyManager:Lcom/mediatek/telephony/TelephonyManagerEx;

    return-object p1
.end method

.method static synthetic access$500(Lcom/android/mms/ui/SelectCardPreferenceActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0    # Lcom/android/mms/ui/SelectCardPreferenceActivity;

    iget-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mNumberText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/mms/ui/SelectCardPreferenceActivity;)I
    .locals 1
    .param p0    # Lcom/android/mms/ui/SelectCardPreferenceActivity;

    iget v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mCurrentSim:I

    return v0
.end method

.method private changeMultiCardKeyToSimRelated(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    const-string v0, "pref_key_sim1"

    invoke-virtual {p0, v0}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/mms/ui/AdvancedEditorPreference;

    iput-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim1:Lcom/android/mms/ui/AdvancedEditorPreference;

    const-string v0, "pref_key_sim2"

    invoke-virtual {p0, v0}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/mms/ui/AdvancedEditorPreference;

    iput-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim2:Lcom/android/mms/ui/AdvancedEditorPreference;

    iget-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mListSimInfo:Ljava/util/List;

    if-eqz v0, :cond_8

    iget v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSimCount:I

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mListSimInfo:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/provider/Telephony$SIMInfo;

    iget v0, v0, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim1:Lcom/android/mms/ui/AdvancedEditorPreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim1:Lcom/android/mms/ui/AdvancedEditorPreference;

    invoke-virtual {v0, p0, v1, p1}, Lcom/android/mms/ui/AdvancedEditorPreference;->init(Landroid/content/Context;ILjava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim2:Lcom/android/mms/ui/AdvancedEditorPreference;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim2:Lcom/android/mms/ui/AdvancedEditorPreference;

    invoke-virtual {v0, p0, v2, p1}, Lcom/android/mms/ui/AdvancedEditorPreference;->init(Landroid/content/Context;ILjava/lang/String;)V

    :cond_1
    :goto_0
    const-string v0, "pref_key_sim3"

    invoke-virtual {p0, v0}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/mms/ui/AdvancedEditorPreference;

    iput-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim3:Lcom/android/mms/ui/AdvancedEditorPreference;

    iget-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim3:Lcom/android/mms/ui/AdvancedEditorPreference;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim3:Lcom/android/mms/ui/AdvancedEditorPreference;

    invoke-virtual {v0, p0, v3, p1}, Lcom/android/mms/ui/AdvancedEditorPreference;->init(Landroid/content/Context;ILjava/lang/String;)V

    :cond_2
    const-string v0, "pref_key_sim4"

    invoke-virtual {p0, v0}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/mms/ui/AdvancedEditorPreference;

    iput-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim4:Lcom/android/mms/ui/AdvancedEditorPreference;

    iget-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim4:Lcom/android/mms/ui/AdvancedEditorPreference;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim4:Lcom/android/mms/ui/AdvancedEditorPreference;

    invoke-virtual {v0, p0, v4, p1}, Lcom/android/mms/ui/AdvancedEditorPreference;->init(Landroid/content/Context;ILjava/lang/String;)V

    :cond_3
    iget v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSimCount:I

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim1:Lcom/android/mms/ui/AdvancedEditorPreference;

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim1:Lcom/android/mms/ui/AdvancedEditorPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    :cond_4
    iget-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim2:Lcom/android/mms/ui/AdvancedEditorPreference;

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim2:Lcom/android/mms/ui/AdvancedEditorPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    :cond_5
    iget-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim3:Lcom/android/mms/ui/AdvancedEditorPreference;

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim3:Lcom/android/mms/ui/AdvancedEditorPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    :cond_6
    iget-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim4:Lcom/android/mms/ui/AdvancedEditorPreference;

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim4:Lcom/android/mms/ui/AdvancedEditorPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    :cond_7
    :goto_1
    return-void

    :cond_8
    iget-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim1:Lcom/android/mms/ui/AdvancedEditorPreference;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim1:Lcom/android/mms/ui/AdvancedEditorPreference;

    invoke-virtual {v0, p0, v2, p1}, Lcom/android/mms/ui/AdvancedEditorPreference;->init(Landroid/content/Context;ILjava/lang/String;)V

    :cond_9
    iget-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim2:Lcom/android/mms/ui/AdvancedEditorPreference;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim2:Lcom/android/mms/ui/AdvancedEditorPreference;

    invoke-virtual {v0, p0, v1, p1}, Lcom/android/mms/ui/AdvancedEditorPreference;->init(Landroid/content/Context;ILjava/lang/String;)V

    goto :goto_0

    :cond_a
    iget v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSimCount:I

    if-ne v0, v2, :cond_d

    iget-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim2:Lcom/android/mms/ui/AdvancedEditorPreference;

    if-eqz v0, :cond_b

    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim2:Lcom/android/mms/ui/AdvancedEditorPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    :cond_b
    iget-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim3:Lcom/android/mms/ui/AdvancedEditorPreference;

    if-eqz v0, :cond_c

    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim3:Lcom/android/mms/ui/AdvancedEditorPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    :cond_c
    iget-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim4:Lcom/android/mms/ui/AdvancedEditorPreference;

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim4:Lcom/android/mms/ui/AdvancedEditorPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_1

    :cond_d
    iget v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSimCount:I

    if-ne v0, v3, :cond_f

    iget-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim3:Lcom/android/mms/ui/AdvancedEditorPreference;

    if-eqz v0, :cond_e

    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim3:Lcom/android/mms/ui/AdvancedEditorPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    :cond_e
    iget-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim4:Lcom/android/mms/ui/AdvancedEditorPreference;

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim4:Lcom/android/mms/ui/AdvancedEditorPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_1

    :cond_f
    iget v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSimCount:I

    if-ne v0, v4, :cond_7

    iget-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim4:Lcom/android/mms/ui/AdvancedEditorPreference;

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim4:Lcom/android/mms/ui/AdvancedEditorPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_1
.end method

.method private getSelectedPosition(Ljava/lang/String;[Ljava/lang/String;)I
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/String;

    iget-object v2, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSpref:Landroid/content/SharedPreferences;

    const-string v3, "Phone"

    invoke-interface {v2, p1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Mms/SelectCardPreferenceActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getSelectedPosition found the res = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    array-length v2, p2

    if-ge v0, v2, :cond_1

    aget-object v2, p2, v0

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "Mms/SelectCardPreferenceActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getSelectedPosition found the position = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const-string v2, "Mms/SelectCardPreferenceActivity"

    const-string v3, "getSelectedPosition not found the position"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_1
.end method

.method private getServiceCenter(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    invoke-static {}, Lcom/mediatek/telephony/TelephonyManagerEx;->getDefault()Lcom/mediatek/telephony/TelephonyManagerEx;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mTelephonyManager:Lcom/mediatek/telephony/TelephonyManagerEx;

    iget-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mTelephonyManager:Lcom/mediatek/telephony/TelephonyManagerEx;

    invoke-virtual {v0, p1}, Lcom/mediatek/telephony/TelephonyManagerEx;->getScAddress(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private setSaveLocation(JLcom/android/mms/ui/AdvancedEditorPreference;Landroid/content/Context;)V
    .locals 9
    .param p1    # J
    .param p3    # Lcom/android/mms/ui/AdvancedEditorPreference;
    .param p4    # Landroid/content/Context;

    const-string v0, "Mms/SelectCardPreferenceActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "currentSlot is: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f080000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f06000c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f06000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v7

    :goto_0
    if-eqz v3, :cond_0

    if-nez v7, :cond_2

    :cond_0
    const-string v0, "Mms/SelectCardPreferenceActivity"

    const-string v1, "setSaveLocation is null"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return-void

    :cond_1
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f06000e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f06000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "pref_key_sms_save_location"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v3}, Lcom/android/mms/ui/SelectCardPreferenceActivity;->getSelectedPosition(Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0a0008

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a0007

    new-instance v4, Lcom/android/mms/ui/SelectCardPreferenceActivity$NegativeButtonListener;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lcom/android/mms/ui/SelectCardPreferenceActivity$NegativeButtonListener;-><init>(Lcom/android/mms/ui/SelectCardPreferenceActivity;Lcom/android/mms/ui/SelectCardPreferenceActivity$1;)V

    invoke-virtual {v0, v1, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    new-instance v0, Lcom/android/mms/ui/SelectCardPreferenceActivity$2;

    move-object v1, p0

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/android/mms/ui/SelectCardPreferenceActivity$2;-><init>(Lcom/android/mms/ui/SelectCardPreferenceActivity;Ljava/lang/String;[Ljava/lang/String;Lcom/android/mms/ui/AdvancedEditorPreference;Landroid/content/Context;)V

    invoke-virtual {v8, v7, v6, v0}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSaveLocDialog:Landroid/app/AlertDialog;

    goto :goto_1
.end method

.method private setServiceCenter(Ljava/lang/String;I)Z
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-static {}, Lcom/mediatek/telephony/TelephonyManagerEx;->getDefault()Lcom/mediatek/telephony/TelephonyManagerEx;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mTelephonyManager:Lcom/mediatek/telephony/TelephonyManagerEx;

    const-string v0, "Mms/SelectCardPreferenceActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setScAddress is: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mTelephonyManager:Lcom/mediatek/telephony/TelephonyManagerEx;

    invoke-virtual {v0, p1, p2}, Lcom/mediatek/telephony/TelephonyManagerEx;->setScAddress(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method private showToast(I)V
    .locals 4
    .param p1    # I

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method private startCellBroadcast(I)V
    .locals 4
    .param p1    # I

    iget-object v1, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mListSimInfo:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mListSimInfo:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le p1, v1, :cond_1

    :cond_0
    const-string v1, "Mms/SelectCardPreferenceActivity"

    const-string v2, "startCellBroadcast mListSimInfo is null "

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "Mms/SelectCardPreferenceActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "currentSlot is: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "Mms/SelectCardPreferenceActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "currentSlot name is: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, p1}, Lcom/android/mms/ui/SelectCardPreferenceActivity;->getSimName(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "com.android.phone"

    const-string v2, "com.mediatek.settings.CellBroadcastActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "simId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "sub_title_name"

    invoke-static {p0, p1}, Landroid/provider/Telephony$SIMInfo;->getSIMInfoBySlot(Landroid/content/Context;I)Landroid/provider/Telephony$SIMInfo;

    move-result-object v2

    iget-object v2, v2, Landroid/provider/Telephony$SIMInfo;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private tostScFail()V
    .locals 2

    const v0, 0x7f0a0005

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    return-void
.end method

.method private tostScOK()V
    .locals 2

    const v0, 0x7f0a0004

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    return-void
.end method


# virtual methods
.method public getNumberFormat(I)I
    .locals 3
    .param p1    # I

    iget-object v2, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mListSimInfo:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/provider/Telephony$SIMInfo;

    iget v2, v1, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    if-ne v2, p1, :cond_0

    iget v2, v1, Landroid/provider/Telephony$SIMInfo;->mDispalyNumberFormat:I

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getSimColor(I)I
    .locals 3
    .param p1    # I

    iget-object v2, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mListSimInfo:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/provider/Telephony$SIMInfo;

    iget v2, v1, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    if-ne v2, p1, :cond_0

    iget v2, v1, Landroid/provider/Telephony$SIMInfo;->mSimBackgroundRes:I

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public bridge synthetic getSimName(I)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/android/mms/ui/SelectCardPreferenceActivity;->getSimName(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSimName(I)Ljava/lang/String;
    .locals 5
    .param p1    # I

    const-string v2, "Mms/SelectCardPreferenceActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getSimName"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mListSimInfo:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/provider/Telephony$SIMInfo;

    iget v2, v1, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    if-ne v2, p1, :cond_0

    iget-object v2, v1, Landroid/provider/Telephony$SIMInfo;->mDisplayName:Ljava/lang/String;

    :goto_0
    return-object v2

    :cond_1
    const-string v2, ""

    goto :goto_0
.end method

.method public bridge synthetic getSimNumber(I)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/android/mms/ui/SelectCardPreferenceActivity;->getSimNumber(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSimNumber(I)Ljava/lang/String;
    .locals 3
    .param p1    # I

    iget-object v2, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mListSimInfo:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/provider/Telephony$SIMInfo;

    iget v2, v1, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    if-ne v2, p1, :cond_0

    iget-object v2, v1, Landroid/provider/Telephony$SIMInfo;->mNumber:Ljava/lang/String;

    :goto_0
    return-object v2

    :cond_1
    const-string v2, ""

    goto :goto_0
.end method

.method public getSimStatus(I)I
    .locals 2
    .param p1    # I

    const/4 v0, -0x1

    invoke-static {}, Lcom/mediatek/telephony/TelephonyManagerEx;->getDefault()Lcom/mediatek/telephony/TelephonyManagerEx;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mTelephonyManager:Lcom/mediatek/telephony/TelephonyManagerEx;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mTelephonyManager:Lcom/mediatek/telephony/TelephonyManagerEx;

    invoke-virtual {v0, p1}, Lcom/mediatek/telephony/TelephonyManagerEx;->getSimIndicatorStateGemini(I)I

    move-result v0

    :cond_0
    return v0
.end method

.method public is3G(I)Z
    .locals 1
    .param p1    # I

    invoke-static {}, Lcom/mediatek/telephony/TelephonyManagerEx;->getDefault()Lcom/mediatek/telephony/TelephonyManagerEx;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mTelephonyManager:Lcom/mediatek/telephony/TelephonyManagerEx;

    invoke-static {}, Lcom/android/mms/ui/MessageUtils;->get3GCapabilitySIM()I

    move-result v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isUSimType(I)Z
    .locals 10
    .param p1    # I

    const/4 v7, 0x2

    const/4 v9, 0x1

    const/4 v3, 0x0

    const-string v4, "phone"

    invoke-static {v4}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v4}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v4, "Mms/SelectCardPreferenceActivity"

    const-string v5, "[isUIMType]: iTel = null"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v3

    :cond_0
    :try_start_0
    invoke-interface {v1, p1}, Lcom/android/internal/telephony/ITelephony;->getIccCardTypeGemini(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, p1}, Lcom/android/internal/telephony/ITelephony;->getIccCardTypeGemini(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "UIM"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v3

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v4, "Mms/SelectCardPreferenceActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[isUSIMType]: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%s: %s"

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v3

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v4, "Mms/SelectCardPreferenceActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[isUSIMType]: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%s: %s"

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v3

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1    # Landroid/content/res/Configuration;

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    iput-object v2, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSpref:Landroid/content/SharedPreferences;

    const v2, 0x7f050002

    invoke-virtual {p0, v2}, Landroid/preference/PreferenceActivity;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "preference"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mIntentPreference:Ljava/lang/String;

    const-string v2, "preferenceTitle"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 12
    .param p1    # Landroid/preference/PreferenceScreen;
    .param p2    # Landroid/preference/Preference;

    const/4 v1, 0x0

    iget-object v9, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim1:Lcom/android/mms/ui/AdvancedEditorPreference;

    if-ne p2, v9, :cond_3

    iget-object v9, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mListSimInfo:Ljava/util/List;

    if-eqz v9, :cond_2

    iget-object v9, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mListSimInfo:Ljava/util/List;

    const/4 v10, 0x0

    invoke-interface {v9, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/provider/Telephony$SIMInfo;

    iget v9, v9, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    if-nez v9, :cond_2

    const/4 v1, 0x0

    :cond_0
    :goto_0
    iget-object v9, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mIntentPreference:Ljava/lang/String;

    const-string v10, "pref_key_manage_sim_messages"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    const v9, 0x7f0a01a8

    invoke-virtual {p0, v9}, Landroid/app/Activity;->setTitle(I)V

    invoke-virtual {p0, v1}, Lcom/android/mms/ui/SelectCardPreferenceActivity;->startManageSimMessages(I)V

    :cond_1
    :goto_1
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v9

    return v9

    :cond_2
    const/4 v1, 0x1

    goto :goto_0

    :cond_3
    iget-object v9, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim2:Lcom/android/mms/ui/AdvancedEditorPreference;

    if-ne p2, v9, :cond_5

    iget-object v9, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mListSimInfo:Ljava/util/List;

    if-eqz v9, :cond_4

    iget-object v9, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mListSimInfo:Ljava/util/List;

    const/4 v10, 0x0

    invoke-interface {v9, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/provider/Telephony$SIMInfo;

    iget v9, v9, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    const/4 v10, 0x1

    if-ne v9, v10, :cond_4

    const/4 v1, 0x0

    goto :goto_0

    :cond_4
    const/4 v1, 0x1

    goto :goto_0

    :cond_5
    iget-object v9, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim3:Lcom/android/mms/ui/AdvancedEditorPreference;

    if-ne p2, v9, :cond_6

    const/4 v1, 0x2

    goto :goto_0

    :cond_6
    iget-object v9, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim4:Lcom/android/mms/ui/AdvancedEditorPreference;

    if-ne p2, v9, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    :cond_7
    iget-object v9, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mIntentPreference:Ljava/lang/String;

    const-string v10, "pref_key_cell_broadcast"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    invoke-direct {p0, v1}, Lcom/android/mms/ui/SelectCardPreferenceActivity;->startCellBroadcast(I)V

    goto :goto_1

    :cond_8
    iget-object v9, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mIntentPreference:Ljava/lang/String;

    const-string v10, "pref_key_sms_service_center"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_9

    iget-object v9, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mListSimInfo:Ljava/util/List;

    invoke-interface {v9, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/provider/Telephony$SIMInfo;

    iget v9, v9, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    iput v9, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mCurrentSim:I

    iget v9, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mCurrentSim:I

    invoke-virtual {p0, v9}, Lcom/android/mms/ui/SelectCardPreferenceActivity;->setServiceCenter(I)V

    goto :goto_1

    :cond_9
    iget-object v9, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mIntentPreference:Ljava/lang/String;

    const-string v10, "pref_key_sms_save_location"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_a

    int-to-long v10, v1

    move-object v9, p2

    check-cast v9, Lcom/android/mms/ui/AdvancedEditorPreference;

    invoke-direct {p0, v10, v11, v9, p0}, Lcom/android/mms/ui/SelectCardPreferenceActivity;->setSaveLocation(JLcom/android/mms/ui/AdvancedEditorPreference;Landroid/content/Context;)V

    goto :goto_1

    :cond_a
    iget-object v9, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mIntentPreference:Ljava/lang/String;

    const-string v10, "pref_key_sms_validity_period"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    move v6, v1

    const/4 v9, 0x6

    new-array v5, v9, [I

    fill-array-data v5, :array_0

    const/4 v9, 0x6

    new-array v4, v9, [Ljava/lang/CharSequence;

    const/4 v9, 0x0

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0a000c

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v10

    aput-object v10, v4, v9

    const/4 v9, 0x1

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0a000d

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v10

    aput-object v10, v4, v9

    const/4 v9, 0x2

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0a000e

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v10

    aput-object v10, v4, v9

    const/4 v9, 0x3

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0a000f

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v10

    aput-object v10, v4, v9

    const/4 v9, 0x4

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0a0010

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v10

    aput-object v10, v4, v9

    const/4 v9, 0x5

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0a0011

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v10

    aput-object v10, v4, v9

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    int-to-long v10, v6

    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "_"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "pref_key_sms_validity_period"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSpref:Landroid/content/SharedPreferences;

    const/4 v10, -0x1

    invoke-interface {v9, v8, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v7

    const/4 v2, 0x0

    const-string v9, "Mms/SelectCardPreferenceActivity"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "validity found the res = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x0

    :goto_2
    array-length v9, v5

    if-ge v3, v9, :cond_b

    aget v9, v5, v3

    if-ne v7, v9, :cond_c

    const-string v9, "Mms/SelectCardPreferenceActivity"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "validity found the position = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v3

    :cond_b
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0a000b

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v9

    invoke-virtual {v0, v9}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    new-instance v9, Lcom/android/mms/ui/SelectCardPreferenceActivity$1;

    invoke-direct {v9, p0, v8, v5}, Lcom/android/mms/ui/SelectCardPreferenceActivity$1;-><init>(Lcom/android/mms/ui/SelectCardPreferenceActivity;Ljava/lang/String;[I)V

    invoke-virtual {v0, v4, v2, v9}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    goto/16 :goto_1

    :cond_c
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :array_0
    .array-data 4
        -0x1
        0xb
        0x47
        0x8f
        0xa7
        0xff
    .end array-data
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    invoke-static {p0}, Landroid/provider/Telephony$SIMInfo;->getInsertedSIMList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mListSimInfo:Ljava/util/List;

    iget-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mListSimInfo:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSimCount:I

    iget-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mIntentPreference:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/mms/ui/SelectCardPreferenceActivity;->changeMultiCardKeyToSimRelated(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim1:Lcom/android/mms/ui/AdvancedEditorPreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim1:Lcom/android/mms/ui/AdvancedEditorPreference;

    invoke-virtual {v0, p0}, Lcom/android/mms/ui/AdvancedEditorPreference;->setNotifyChange(Landroid/content/Context;)V

    :cond_0
    iget-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim2:Lcom/android/mms/ui/AdvancedEditorPreference;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mSim2:Lcom/android/mms/ui/AdvancedEditorPreference;

    invoke-virtual {v0, p0}, Lcom/android/mms/ui/AdvancedEditorPreference;->setNotifyChange(Landroid/content/Context;)V

    :cond_1
    return-void
.end method

.method public setServiceCenter(I)V
    .locals 7
    .param p1    # I

    const/4 v6, 0x0

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mNumberTextDialog:Landroid/app/AlertDialog$Builder;

    new-instance v1, Landroid/widget/EditText;

    iget-object v2, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mNumberTextDialog:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mNumberText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mNumberText:Landroid/widget/EditText;

    const v2, 0x7f0a0169

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setHint(I)V

    iget-object v1, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mNumberText:Landroid/widget/EditText;

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/text/InputFilter;

    const/4 v3, 0x0

    new-instance v4, Landroid/text/InputFilter$LengthFilter;

    const/16 v5, 0x14

    invoke-direct {v4, v5}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setFilters([Landroid/text/InputFilter;)V

    iget-object v1, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mNumberText:Landroid/widget/EditText;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setInputType(I)V

    iget-object v1, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mNumberText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/TextView;->computeScroll()V

    const-string v1, "Mms/SelectCardPreferenceActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "currentSlot is: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1}, Lcom/android/mms/ui/SelectCardPreferenceActivity;->getServiceCenter(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Mms/SelectCardPreferenceActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getScNumber is: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mNumberText:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mNumberTextDialog:Landroid/app/AlertDialog$Builder;

    const v2, 0x7f020058

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a000a

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mNumberText:Landroid/widget/EditText;

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a0006

    new-instance v3, Lcom/android/mms/ui/SelectCardPreferenceActivity$PositiveButtonListener;

    invoke-direct {v3, p0, v6}, Lcom/android/mms/ui/SelectCardPreferenceActivity$PositiveButtonListener;-><init>(Lcom/android/mms/ui/SelectCardPreferenceActivity;Lcom/android/mms/ui/SelectCardPreferenceActivity$1;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a0007

    new-instance v3, Lcom/android/mms/ui/SelectCardPreferenceActivity$NegativeButtonListener;

    invoke-direct {v3, p0, v6}, Lcom/android/mms/ui/SelectCardPreferenceActivity$NegativeButtonListener;-><init>(Lcom/android/mms/ui/SelectCardPreferenceActivity;Lcom/android/mms/ui/SelectCardPreferenceActivity$1;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method public startManageSimMessages(I)V
    .locals 5
    .param p1    # I

    iget-object v1, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mListSimInfo:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mListSimInfo:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le p1, v1, :cond_1

    :cond_0
    const-string v1, "Mms/SelectCardPreferenceActivity"

    const-string v2, "startManageSimMessages mListSimInfo is null "

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "Mms/SelectCardPreferenceActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "currentSlot is: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "Mms/SelectCardPreferenceActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "currentSlot name is: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, p0, Lcom/android/mms/ui/SelectCardPreferenceActivity;->mListSimInfo:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/provider/Telephony$SIMInfo;

    iget-object v1, v1, Landroid/provider/Telephony$SIMInfo;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-class v1, Lcom/android/mms/ui/ManageSimMessages;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v1, "SlotId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
