.class final Lcom/android/mms/ui/SlideshowEditActivity$SlideListMenuClickListener;
.super Ljava/lang/Object;
.source "SlideshowEditActivity.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/ui/SlideshowEditActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SlideListMenuClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/SlideshowEditActivity;


# direct methods
.method private constructor <init>(Lcom/android/mms/ui/SlideshowEditActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/SlideshowEditActivity$SlideListMenuClickListener;->this$0:Lcom/android/mms/ui/SlideshowEditActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/mms/ui/SlideshowEditActivity;Lcom/android/mms/ui/SlideshowEditActivity$1;)V
    .locals 0
    .param p1    # Lcom/android/mms/ui/SlideshowEditActivity;
    .param p2    # Lcom/android/mms/ui/SlideshowEditActivity$1;

    invoke-direct {p0, p1}, Lcom/android/mms/ui/SlideshowEditActivity$SlideListMenuClickListener;-><init>(Lcom/android/mms/ui/SlideshowEditActivity;)V

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1    # Landroid/view/MenuItem;

    iget-object v1, p0, Lcom/android/mms/ui/SlideshowEditActivity$SlideListMenuClickListener;->this$0:Lcom/android/mms/ui/SlideshowEditActivity;

    invoke-static {v1}, Lcom/android/mms/ui/SlideshowEditActivity;->access$300(Lcom/android/mms/ui/SlideshowEditActivity;)I

    move-result v0

    iget-object v1, p0, Lcom/android/mms/ui/SlideshowEditActivity$SlideListMenuClickListener;->this$0:Lcom/android/mms/ui/SlideshowEditActivity;

    const/4 v2, -0x1

    invoke-static {v1, v2}, Lcom/android/mms/ui/SlideshowEditActivity;->access$302(Lcom/android/mms/ui/SlideshowEditActivity;I)I

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    :pswitch_0
    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/android/mms/ui/SlideshowEditActivity$SlideListMenuClickListener;->this$0:Lcom/android/mms/ui/SlideshowEditActivity;

    invoke-static {v1}, Lcom/android/mms/ui/SlideshowEditActivity;->access$700(Lcom/android/mms/ui/SlideshowEditActivity;)Lcom/android/mms/model/SlideshowModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/mms/model/SlideshowModel;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/mms/ui/SlideshowEditActivity$SlideListMenuClickListener;->this$0:Lcom/android/mms/ui/SlideshowEditActivity;

    invoke-static {v1}, Lcom/android/mms/ui/SlideshowEditActivity;->access$800(Lcom/android/mms/ui/SlideshowEditActivity;)Lcom/android/mms/ui/SlideshowEditor;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/mms/ui/SlideshowEditor;->moveSlideUp(I)V

    iget-object v1, p0, Lcom/android/mms/ui/SlideshowEditActivity$SlideListMenuClickListener;->this$0:Lcom/android/mms/ui/SlideshowEditActivity;

    invoke-static {v1}, Lcom/android/mms/ui/SlideshowEditActivity;->access$600(Lcom/android/mms/ui/SlideshowEditActivity;)Lcom/android/mms/ui/SlideshowEditActivity$SlideListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    iget-object v1, p0, Lcom/android/mms/ui/SlideshowEditActivity$SlideListMenuClickListener;->this$0:Lcom/android/mms/ui/SlideshowEditActivity;

    invoke-static {v1}, Lcom/android/mms/ui/SlideshowEditActivity;->access$500(Lcom/android/mms/ui/SlideshowEditActivity;)Landroid/widget/ListView;

    move-result-object v1

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setSelection(I)V

    goto :goto_0

    :pswitch_1
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/android/mms/ui/SlideshowEditActivity$SlideListMenuClickListener;->this$0:Lcom/android/mms/ui/SlideshowEditActivity;

    invoke-static {v1}, Lcom/android/mms/ui/SlideshowEditActivity;->access$700(Lcom/android/mms/ui/SlideshowEditActivity;)Lcom/android/mms/model/SlideshowModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/mms/model/SlideshowModel;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/mms/ui/SlideshowEditActivity$SlideListMenuClickListener;->this$0:Lcom/android/mms/ui/SlideshowEditActivity;

    invoke-static {v1}, Lcom/android/mms/ui/SlideshowEditActivity;->access$800(Lcom/android/mms/ui/SlideshowEditActivity;)Lcom/android/mms/ui/SlideshowEditor;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/mms/ui/SlideshowEditor;->moveSlideDown(I)V

    iget-object v1, p0, Lcom/android/mms/ui/SlideshowEditActivity$SlideListMenuClickListener;->this$0:Lcom/android/mms/ui/SlideshowEditActivity;

    invoke-static {v1}, Lcom/android/mms/ui/SlideshowEditActivity;->access$600(Lcom/android/mms/ui/SlideshowEditActivity;)Lcom/android/mms/ui/SlideshowEditActivity$SlideListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    iget-object v1, p0, Lcom/android/mms/ui/SlideshowEditActivity$SlideListMenuClickListener;->this$0:Lcom/android/mms/ui/SlideshowEditActivity;

    invoke-static {v1}, Lcom/android/mms/ui/SlideshowEditActivity;->access$500(Lcom/android/mms/ui/SlideshowEditActivity;)Landroid/widget/ListView;

    move-result-object v1

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setSelection(I)V

    goto :goto_0

    :pswitch_2
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/android/mms/ui/SlideshowEditActivity$SlideListMenuClickListener;->this$0:Lcom/android/mms/ui/SlideshowEditActivity;

    invoke-static {v1}, Lcom/android/mms/ui/SlideshowEditActivity;->access$700(Lcom/android/mms/ui/SlideshowEditActivity;)Lcom/android/mms/model/SlideshowModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/mms/model/SlideshowModel;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/mms/ui/SlideshowEditActivity$SlideListMenuClickListener;->this$0:Lcom/android/mms/ui/SlideshowEditActivity;

    invoke-static {v1}, Lcom/android/mms/ui/SlideshowEditActivity;->access$800(Lcom/android/mms/ui/SlideshowEditActivity;)Lcom/android/mms/ui/SlideshowEditor;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/mms/ui/SlideshowEditor;->removeSlide(I)V

    iget-object v1, p0, Lcom/android/mms/ui/SlideshowEditActivity$SlideListMenuClickListener;->this$0:Lcom/android/mms/ui/SlideshowEditActivity;

    invoke-static {v1}, Lcom/android/mms/ui/SlideshowEditActivity;->access$600(Lcom/android/mms/ui/SlideshowEditActivity;)Lcom/android/mms/ui/SlideshowEditActivity$SlideListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/android/mms/ui/SlideshowEditActivity$SlideListMenuClickListener;->this$0:Lcom/android/mms/ui/SlideshowEditActivity;

    invoke-static {v1}, Lcom/android/mms/ui/SlideshowEditActivity;->access$700(Lcom/android/mms/ui/SlideshowEditActivity;)Lcom/android/mms/model/SlideshowModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/mms/model/SlideshowModel;->size()I

    move-result v1

    if-eq v0, v1, :cond_1

    add-int/lit8 v0, v0, 0x1

    :cond_1
    iget-object v1, p0, Lcom/android/mms/ui/SlideshowEditActivity$SlideListMenuClickListener;->this$0:Lcom/android/mms/ui/SlideshowEditActivity;

    invoke-static {v1}, Lcom/android/mms/ui/SlideshowEditActivity;->access$800(Lcom/android/mms/ui/SlideshowEditActivity;)Lcom/android/mms/ui/SlideshowEditor;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/mms/ui/SlideshowEditor;->addNewSlide(I)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/mms/ui/SlideshowEditActivity$SlideListMenuClickListener;->this$0:Lcom/android/mms/ui/SlideshowEditActivity;

    invoke-static {v1}, Lcom/android/mms/ui/SlideshowEditActivity;->access$600(Lcom/android/mms/ui/SlideshowEditActivity;)Lcom/android/mms/ui/SlideshowEditActivity$SlideListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    iget-object v1, p0, Lcom/android/mms/ui/SlideshowEditActivity$SlideListMenuClickListener;->this$0:Lcom/android/mms/ui/SlideshowEditActivity;

    invoke-static {v1}, Lcom/android/mms/ui/SlideshowEditActivity;->access$500(Lcom/android/mms/ui/SlideshowEditActivity;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    iget-object v1, p0, Lcom/android/mms/ui/SlideshowEditActivity$SlideListMenuClickListener;->this$0:Lcom/android/mms/ui/SlideshowEditActivity;

    invoke-static {v1}, Lcom/android/mms/ui/SlideshowEditActivity;->access$500(Lcom/android/mms/ui/SlideshowEditActivity;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setSelection(I)V

    goto/16 :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/mms/ui/SlideshowEditActivity$SlideListMenuClickListener;->this$0:Lcom/android/mms/ui/SlideshowEditActivity;

    const v2, 0x7f0a017e

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :pswitch_4
    iget-object v1, p0, Lcom/android/mms/ui/SlideshowEditActivity$SlideListMenuClickListener;->this$0:Lcom/android/mms/ui/SlideshowEditActivity;

    invoke-static {v1}, Lcom/android/mms/ui/SlideshowEditActivity;->access$800(Lcom/android/mms/ui/SlideshowEditActivity;)Lcom/android/mms/ui/SlideshowEditor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/mms/ui/SlideshowEditor;->removeAllSlides()V

    iget-object v1, p0, Lcom/android/mms/ui/SlideshowEditActivity$SlideListMenuClickListener;->this$0:Lcom/android/mms/ui/SlideshowEditActivity;

    invoke-static {v1}, Lcom/android/mms/ui/SlideshowEditActivity;->access$600(Lcom/android/mms/ui/SlideshowEditActivity;)Lcom/android/mms/ui/SlideshowEditActivity$SlideListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    iget-object v1, p0, Lcom/android/mms/ui/SlideshowEditActivity$SlideListMenuClickListener;->this$0:Lcom/android/mms/ui/SlideshowEditActivity;

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
