.class Lcom/android/mms/ui/SmsTemplateEditActivity$UpdateButtonListener;
.super Ljava/lang/Object;
.source "SmsTemplateEditActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/ui/SmsTemplateEditActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UpdateButtonListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/SmsTemplateEditActivity;


# direct methods
.method private constructor <init>(Lcom/android/mms/ui/SmsTemplateEditActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/SmsTemplateEditActivity$UpdateButtonListener;->this$0:Lcom/android/mms/ui/SmsTemplateEditActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/mms/ui/SmsTemplateEditActivity;Lcom/android/mms/ui/SmsTemplateEditActivity$1;)V
    .locals 0
    .param p1    # Lcom/android/mms/ui/SmsTemplateEditActivity;
    .param p2    # Lcom/android/mms/ui/SmsTemplateEditActivity$1;

    invoke-direct {p0, p1}, Lcom/android/mms/ui/SmsTemplateEditActivity$UpdateButtonListener;-><init>(Lcom/android/mms/ui/SmsTemplateEditActivity;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v2, p0, Lcom/android/mms/ui/SmsTemplateEditActivity$UpdateButtonListener;->this$0:Lcom/android/mms/ui/SmsTemplateEditActivity;

    invoke-static {v2}, Lcom/android/mms/ui/SmsTemplateEditActivity;->access$1000(Lcom/android/mms/ui/SmsTemplateEditActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/mms/ui/SmsTemplateEditActivity$UpdateButtonListener;->this$0:Lcom/android/mms/ui/SmsTemplateEditActivity;

    iget-object v3, p0, Lcom/android/mms/ui/SmsTemplateEditActivity$UpdateButtonListener;->this$0:Lcom/android/mms/ui/SmsTemplateEditActivity;

    invoke-static {v3}, Lcom/android/mms/ui/SmsTemplateEditActivity;->access$100(Lcom/android/mms/ui/SmsTemplateEditActivity;)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/android/mms/ui/SmsTemplateEditActivity;->access$1100(Lcom/android/mms/ui/SmsTemplateEditActivity;Ljava/lang/Integer;Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    if-lez v0, :cond_0

    iget-object v2, p0, Lcom/android/mms/ui/SmsTemplateEditActivity$UpdateButtonListener;->this$0:Lcom/android/mms/ui/SmsTemplateEditActivity;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/android/mms/ui/SmsTemplateEditActivity;->access$102(Lcom/android/mms/ui/SmsTemplateEditActivity;Ljava/lang/Integer;)Ljava/lang/Integer;

    iget-object v2, p0, Lcom/android/mms/ui/SmsTemplateEditActivity$UpdateButtonListener;->this$0:Lcom/android/mms/ui/SmsTemplateEditActivity;

    invoke-static {v2}, Lcom/android/mms/ui/SmsTemplateEditActivity;->access$1200(Lcom/android/mms/ui/SmsTemplateEditActivity;)V

    iget-object v2, p0, Lcom/android/mms/ui/SmsTemplateEditActivity$UpdateButtonListener;->this$0:Lcom/android/mms/ui/SmsTemplateEditActivity;

    const v3, 0x7f0a011d

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/mms/ui/SmsTemplateEditActivity;->access$1300(Lcom/android/mms/ui/SmsTemplateEditActivity;Ljava/lang/Integer;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/android/mms/ui/SmsTemplateEditActivity;->access$1400()I

    move-result v2

    if-ne v0, v2, :cond_1

    iget-object v2, p0, Lcom/android/mms/ui/SmsTemplateEditActivity$UpdateButtonListener;->this$0:Lcom/android/mms/ui/SmsTemplateEditActivity;

    const v3, 0x7f0a0122

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/mms/ui/SmsTemplateEditActivity;->access$1300(Lcom/android/mms/ui/SmsTemplateEditActivity;Ljava/lang/Integer;)V

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/android/mms/ui/SmsTemplateEditActivity;->access$1500()I

    move-result v2

    if-ne v0, v2, :cond_2

    iget-object v2, p0, Lcom/android/mms/ui/SmsTemplateEditActivity$UpdateButtonListener;->this$0:Lcom/android/mms/ui/SmsTemplateEditActivity;

    const v3, 0x7f0a0180

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/mms/ui/SmsTemplateEditActivity;->access$1300(Lcom/android/mms/ui/SmsTemplateEditActivity;Ljava/lang/Integer;)V

    iget-object v2, p0, Lcom/android/mms/ui/SmsTemplateEditActivity$UpdateButtonListener;->this$0:Lcom/android/mms/ui/SmsTemplateEditActivity;

    iget-object v3, p0, Lcom/android/mms/ui/SmsTemplateEditActivity$UpdateButtonListener;->this$0:Lcom/android/mms/ui/SmsTemplateEditActivity;

    invoke-static {v3}, Lcom/android/mms/ui/SmsTemplateEditActivity;->access$300(Lcom/android/mms/ui/SmsTemplateEditActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/mms/ui/SmsTemplateEditActivity;->access$900(Lcom/android/mms/ui/SmsTemplateEditActivity;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/android/mms/ui/SmsTemplateEditActivity$UpdateButtonListener;->this$0:Lcom/android/mms/ui/SmsTemplateEditActivity;

    const v3, 0x7f0a011e

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/mms/ui/SmsTemplateEditActivity;->access$1300(Lcom/android/mms/ui/SmsTemplateEditActivity;Ljava/lang/Integer;)V

    goto :goto_0
.end method
