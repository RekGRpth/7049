.class public Lcom/android/mms/ui/SmsTemplateEditActivity;
.super Landroid/app/Activity;
.source "SmsTemplateEditActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mms/ui/SmsTemplateEditActivity$CancelButtonListener;,
        Lcom/android/mms/ui/SmsTemplateEditActivity$DeleteButtonListener;,
        Lcom/android/mms/ui/SmsTemplateEditActivity$UpdateButtonListener;,
        Lcom/android/mms/ui/SmsTemplateEditActivity$EditButtonListener;
    }
.end annotation


# static fields
.field private static QUICK_TEXT_HAS_ALREADY:I

.field private static QUICK_TEXT_NULL:I

.field private static TAG:Ljava/lang/String;

.field private static TEXT:Ljava/lang/String;


# instance fields
.field private final MAX_EDITABLE_LENGTH:I

.field private adapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private addButton:Landroid/widget/Button;

.field private allQuickTextIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private allQuickTexts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private cursor:Landroid/database/Cursor;

.field private mListView:Landroid/widget/ListView;

.field private mMaxQuickTextId:Ljava/lang/Integer;

.field private mNewQuickText:Landroid/widget/EditText;

.field private mOldQuickText:Landroid/widget/EditText;

.field private mQuickText:Ljava/lang/String;

.field private mQuickTextId:Ljava/lang/Integer;

.field private mQuicktextAlertDialog:Landroid/app/AlertDialog;

.field private mToast:Landroid/widget/Toast;

.field private textItem:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "MMS/SmsTempalteEditor"

    sput-object v0, Lcom/android/mms/ui/SmsTemplateEditActivity;->TAG:Ljava/lang/String;

    const-string v0, "text"

    sput-object v0, Lcom/android/mms/ui/SmsTemplateEditActivity;->TEXT:Ljava/lang/String;

    const/4 v0, -0x1

    sput v0, Lcom/android/mms/ui/SmsTemplateEditActivity;->QUICK_TEXT_HAS_ALREADY:I

    const/4 v0, -0x2

    sput v0, Lcom/android/mms/ui/SmsTemplateEditActivity;->QUICK_TEXT_NULL:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->allQuickTextIds:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->allQuickTexts:Ljava/util/List;

    const/16 v0, 0x80

    iput v0, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->MAX_EDITABLE_LENGTH:I

    return-void
.end method

.method static synthetic access$000(Lcom/android/mms/ui/SmsTemplateEditActivity;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/SmsTemplateEditActivity;

    invoke-direct {p0}, Lcom/android/mms/ui/SmsTemplateEditActivity;->addQuickText()V

    return-void
.end method

.method static synthetic access$100(Lcom/android/mms/ui/SmsTemplateEditActivity;)Ljava/lang/Integer;
    .locals 1
    .param p0    # Lcom/android/mms/ui/SmsTemplateEditActivity;

    iget-object v0, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->mQuickTextId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/mms/ui/SmsTemplateEditActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0    # Lcom/android/mms/ui/SmsTemplateEditActivity;

    iget-object v0, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->mOldQuickText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$102(Lcom/android/mms/ui/SmsTemplateEditActivity;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0
    .param p0    # Lcom/android/mms/ui/SmsTemplateEditActivity;
    .param p1    # Ljava/lang/Integer;

    iput-object p1, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->mQuickTextId:Ljava/lang/Integer;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/android/mms/ui/SmsTemplateEditActivity;Ljava/lang/Integer;Ljava/lang/String;)I
    .locals 1
    .param p0    # Lcom/android/mms/ui/SmsTemplateEditActivity;
    .param p1    # Ljava/lang/Integer;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/android/mms/ui/SmsTemplateEditActivity;->updateST(Ljava/lang/Integer;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/android/mms/ui/SmsTemplateEditActivity;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/SmsTemplateEditActivity;

    invoke-direct {p0}, Lcom/android/mms/ui/SmsTemplateEditActivity;->updateAllQuicktexts()V

    return-void
.end method

.method static synthetic access$1300(Lcom/android/mms/ui/SmsTemplateEditActivity;Ljava/lang/Integer;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/SmsTemplateEditActivity;
    .param p1    # Ljava/lang/Integer;

    invoke-direct {p0, p1}, Lcom/android/mms/ui/SmsTemplateEditActivity;->makeAToast(Ljava/lang/Integer;)V

    return-void
.end method

.method static synthetic access$1400()I
    .locals 1

    sget v0, Lcom/android/mms/ui/SmsTemplateEditActivity;->QUICK_TEXT_HAS_ALREADY:I

    return v0
.end method

.method static synthetic access$1500()I
    .locals 1

    sget v0, Lcom/android/mms/ui/SmsTemplateEditActivity;->QUICK_TEXT_NULL:I

    return v0
.end method

.method static synthetic access$1600(Lcom/android/mms/ui/SmsTemplateEditActivity;Ljava/lang/Integer;)I
    .locals 1
    .param p0    # Lcom/android/mms/ui/SmsTemplateEditActivity;
    .param p1    # Ljava/lang/Integer;

    invoke-direct {p0, p1}, Lcom/android/mms/ui/SmsTemplateEditActivity;->delST(Ljava/lang/Integer;)I

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/android/mms/ui/SmsTemplateEditActivity;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/android/mms/ui/SmsTemplateEditActivity;

    iget-object v0, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->allQuickTextIds:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/mms/ui/SmsTemplateEditActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/mms/ui/SmsTemplateEditActivity;

    iget-object v0, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->mQuickText:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$302(Lcom/android/mms/ui/SmsTemplateEditActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/android/mms/ui/SmsTemplateEditActivity;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->mQuickText:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$400(Lcom/android/mms/ui/SmsTemplateEditActivity;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/android/mms/ui/SmsTemplateEditActivity;

    iget-object v0, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->allQuickTexts:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/mms/ui/SmsTemplateEditActivity;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/SmsTemplateEditActivity;

    invoke-direct {p0}, Lcom/android/mms/ui/SmsTemplateEditActivity;->showEditDialog()V

    return-void
.end method

.method static synthetic access$900(Lcom/android/mms/ui/SmsTemplateEditActivity;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/SmsTemplateEditActivity;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/android/mms/ui/SmsTemplateEditActivity;->showEditDialog(Ljava/lang/String;)V

    return-void
.end method

.method private addQuickText()V
    .locals 3

    iget-object v1, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->mNewQuickText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f0a0180

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/mms/ui/SmsTemplateEditActivity;->makeAToast(Ljava/lang/Integer;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0, v0}, Lcom/android/mms/ui/SmsTemplateEditActivity;->addST(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->mNewQuickText:Landroid/widget/EditText;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/android/mms/ui/SmsTemplateEditActivity;->updateAllQuicktexts()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f0a0121

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : \n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/mms/ui/SmsTemplateEditActivity;->makeAToast(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const v1, 0x7f0a0122

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/mms/ui/SmsTemplateEditActivity;->makeAToast(Ljava/lang/Integer;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->mToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private addST(Ljava/lang/String;)Z
    .locals 3
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/android/mms/ui/SmsTemplateEditActivity;->hasQuicktext(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "_id"

    iget-object v2, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->mMaxQuickTextId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v1, Lcom/android/mms/ui/SmsTemplateEditActivity;->TEXT:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/Telephony$MmsSms;->CONTENT_URI_QUICKTEXT:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    const/4 v1, 0x1

    goto :goto_0
.end method

.method private delST(Ljava/lang/Integer;)I
    .locals 4
    .param p1    # Ljava/lang/Integer;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/Telephony$MmsSms;->CONTENT_URI_QUICKTEXT:Landroid/net/Uri;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private getSTs()Landroid/database/Cursor;
    .locals 7

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/Telephony$MmsSms;->CONTENT_URI_QUICKTEXT:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-virtual {p0, v6}, Landroid/app/Activity;->startManagingCursor(Landroid/database/Cursor;)V

    return-object v6
.end method

.method private hasQuicktext(Ljava/lang/String;)Z
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v2, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->allQuickTexts:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private makeAToast(Ljava/lang/Integer;)V
    .locals 2
    .param p1    # Ljava/lang/Integer;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void
.end method

.method private makeAToast(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void
.end method

.method private showEditDialog()V
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x1010355

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a011c

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->mQuickText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a019a

    new-instance v2, Lcom/android/mms/ui/SmsTemplateEditActivity$EditButtonListener;

    invoke-direct {v2, p0, v3}, Lcom/android/mms/ui/SmsTemplateEditActivity$EditButtonListener;-><init>(Lcom/android/mms/ui/SmsTemplateEditActivity;Lcom/android/mms/ui/SmsTemplateEditActivity$1;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a01c0

    new-instance v2, Lcom/android/mms/ui/SmsTemplateEditActivity$DeleteButtonListener;

    invoke-direct {v2, p0, v3}, Lcom/android/mms/ui/SmsTemplateEditActivity$DeleteButtonListener;-><init>(Lcom/android/mms/ui/SmsTemplateEditActivity;Lcom/android/mms/ui/SmsTemplateEditActivity$1;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    new-instance v2, Lcom/android/mms/ui/SmsTemplateEditActivity$CancelButtonListener;

    invoke-direct {v2, p0, v3}, Lcom/android/mms/ui/SmsTemplateEditActivity$CancelButtonListener;-><init>(Lcom/android/mms/ui/SmsTemplateEditActivity;Lcom/android/mms/ui/SmsTemplateEditActivity$1;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->mQuicktextAlertDialog:Landroid/app/AlertDialog;

    return-void
.end method

.method private showEditDialog(Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->mOldQuickText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->mOldQuickText:Landroid/widget/EditText;

    const v2, 0x7f0a0169

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setHint(I)V

    iget-object v1, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->mOldQuickText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/TextView;->computeScroll()V

    iget-object v1, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->mOldQuickText:Landroid/widget/EditText;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->mOldQuickText:Landroid/widget/EditText;

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/text/InputFilter;

    const/4 v3, 0x0

    new-instance v4, Landroid/text/InputFilter$LengthFilter;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setFilters([Landroid/text/InputFilter;)V

    iget-object v1, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->mOldQuickText:Landroid/widget/EditText;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMaxLines(I)V

    const v1, 0x1010355

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a011c

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->mOldQuickText:Landroid/widget/EditText;

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x104000a

    new-instance v3, Lcom/android/mms/ui/SmsTemplateEditActivity$UpdateButtonListener;

    invoke-direct {v3, p0, v6}, Lcom/android/mms/ui/SmsTemplateEditActivity$UpdateButtonListener;-><init>(Lcom/android/mms/ui/SmsTemplateEditActivity;Lcom/android/mms/ui/SmsTemplateEditActivity$1;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x1040000

    new-instance v3, Lcom/android/mms/ui/SmsTemplateEditActivity$CancelButtonListener;

    invoke-direct {v3, p0, v6}, Lcom/android/mms/ui/SmsTemplateEditActivity$CancelButtonListener;-><init>(Lcom/android/mms/ui/SmsTemplateEditActivity;Lcom/android/mms/ui/SmsTemplateEditActivity$1;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->mQuicktextAlertDialog:Landroid/app/AlertDialog;

    return-void
.end method

.method private updateAllQuicktexts()V
    .locals 4

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->mMaxQuickTextId:Ljava/lang/Integer;

    invoke-direct {p0}, Lcom/android/mms/ui/SmsTemplateEditActivity;->getSTs()Landroid/database/Cursor;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->cursor:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->allQuickTextIds:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    iget-object v1, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->allQuickTexts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    iget-object v1, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->cursor:Landroid/database/Cursor;

    if-eqz v1, :cond_1

    :goto_0
    iget-object v1, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->cursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->cursor:Landroid/database/Cursor;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iget-object v1, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->allQuickTextIds:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->allQuickTexts:Ljava/util/List;

    iget-object v2, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->cursor:Landroid/database/Cursor;

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->mMaxQuickTextId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-lt v1, v0, :cond_0

    iget-object v1, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->mMaxQuickTextId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->mMaxQuickTextId:Ljava/lang/Integer;

    goto :goto_0

    :cond_1
    new-instance v1, Landroid/widget/ArrayAdapter;

    const v2, 0x7f040031

    iget-object v3, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->allQuickTexts:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v1, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->adapter:Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->mListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->adapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method private updateST(Ljava/lang/Integer;Ljava/lang/String;)I
    .locals 5
    .param p1    # Ljava/lang/Integer;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget v1, Lcom/android/mms/ui/SmsTemplateEditActivity;->QUICK_TEXT_NULL:I

    :goto_0
    return v1

    :cond_0
    invoke-direct {p0, p2}, Lcom/android/mms/ui/SmsTemplateEditActivity;->hasQuicktext(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget v1, Lcom/android/mms/ui/SmsTemplateEditActivity;->QUICK_TEXT_HAS_ALREADY:I

    goto :goto_0

    :cond_1
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v1, Lcom/android/mms/ui/SmsTemplateEditActivity;->TEXT:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/Telephony$MmsSms;->CONTENT_URI_QUICKTEXT:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    goto :goto_0
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1    # Landroid/content/res/Configuration;

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const v2, 0x7f0e00de

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f040042

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    const v0, 0x7f0a0180

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->mToast:Landroid/widget/Toast;

    const v0, 0x7f0e00df

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->addButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->addButton:Landroid/widget/Button;

    const/high16 v1, 0x41500000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v0, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->addButton:Landroid/widget/Button;

    new-instance v1, Lcom/android/mms/ui/SmsTemplateEditActivity$1;

    invoke-direct {v1, p0}, Lcom/android/mms/ui/SmsTemplateEditActivity$1;-><init>(Lcom/android/mms/ui/SmsTemplateEditActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0e00dc

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->mListView:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->mListView:Landroid/widget/ListView;

    new-instance v1, Lcom/android/mms/ui/SmsTemplateEditActivity$2;

    invoke-direct {v1, p0}, Lcom/android/mms/ui/SmsTemplateEditActivity$2;-><init>(Lcom/android/mms/ui/SmsTemplateEditActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->mNewQuickText:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->mNewQuickText:Landroid/widget/EditText;

    const v1, 0x7f0a0169

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setHint(I)V

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/mms/ui/SmsTemplateEditActivity;->textItem:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/android/mms/ui/SmsTemplateEditActivity;->updateAllQuicktexts()V

    return-void
.end method
