.class Lcom/android/mms/ui/WPMessageActivity$11;
.super Ljava/lang/Object;
.source "WPMessageActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/WPMessageActivity;->onUpdate(Lcom/android/mms/data/Contact;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/WPMessageActivity;

.field final synthetic val$updated:Lcom/android/mms/data/Contact;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/WPMessageActivity;Lcom/android/mms/data/Contact;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/WPMessageActivity$11;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    iput-object p2, p0, Lcom/android/mms/ui/WPMessageActivity$11;->val$updated:Lcom/android/mms/data/Contact;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v1, p0, Lcom/android/mms/ui/WPMessageActivity$11;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-static {v1}, Lcom/android/mms/ui/WPMessageActivity;->access$1100(Lcom/android/mms/ui/WPMessageActivity;)Lcom/android/mms/data/ContactList;

    move-result-object v0

    const-string v1, "Mms:app"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[CMA] onUpdate contact updated: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/mms/ui/WPMessageActivity$11;->val$updated:Lcom/android/mms/data/Contact;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/mms/ui/WPMessageActivity;->access$1700(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[CMA] onUpdate recipients: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/mms/ui/WPMessageActivity;->access$1700(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/android/mms/ui/WPMessageActivity$11;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-static {v1, v0}, Lcom/android/mms/ui/WPMessageActivity;->access$1200(Lcom/android/mms/ui/WPMessageActivity;Lcom/android/mms/data/ContactList;)V

    iget-object v1, p0, Lcom/android/mms/ui/WPMessageActivity$11;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-virtual {v1}, Landroid/app/Activity;->invalidateOptionsMenu()V

    iget-object v1, p0, Lcom/android/mms/ui/WPMessageActivity$11;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    iget-object v1, v1, Lcom/android/mms/ui/WPMessageActivity;->mMsgListAdapter:Lcom/android/mms/ui/WPMessageListAdapter;

    invoke-virtual {v1}, Lcom/android/mms/ui/WPMessageListAdapter;->notifyDataSetChanged()V

    return-void
.end method
