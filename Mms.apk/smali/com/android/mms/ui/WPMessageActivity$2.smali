.class Lcom/android/mms/ui/WPMessageActivity$2;
.super Ljava/lang/Object;
.source "WPMessageActivity.java"

# interfaces
.implements Landroid/view/View$OnCreateContextMenuListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/ui/WPMessageActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/WPMessageActivity;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/WPMessageActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/WPMessageActivity$2;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 10
    .param p1    # Landroid/view/ContextMenu;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ContextMenu$ContextMenuInfo;

    const/4 v9, 0x0

    iget-object v6, p0, Lcom/android/mms/ui/WPMessageActivity$2;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-static {v6}, Lcom/android/mms/ui/WPMessageActivity;->access$100(Lcom/android/mms/ui/WPMessageActivity;)Z

    move-result v6

    if-nez v6, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v6, p0, Lcom/android/mms/ui/WPMessageActivity$2;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    iget-object v6, v6, Lcom/android/mms/ui/WPMessageActivity;->mMsgListAdapter:Lcom/android/mms/ui/WPMessageListAdapter;

    invoke-virtual {v6}, Landroid/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    const/4 v6, 0x6

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    const-string v6, "Mms/WapPush"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onCreateContextMenu:type:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", msgid:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/android/mms/ui/WPMessageActivity$2;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    iget-object v6, v6, Lcom/android/mms/ui/WPMessageActivity;->mMsgListAdapter:Lcom/android/mms/ui/WPMessageListAdapter;

    invoke-virtual {v6, v5, v2, v3, v0}, Lcom/android/mms/ui/WPMessageListAdapter;->getCachedMessageItem(IJLandroid/database/Cursor;)Lcom/android/mms/ui/WPMessageItem;

    move-result-object v4

    if-nez v4, :cond_1

    const-string v6, "Mms/WapPush"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "WPMessageActivity: Cannot load message item for type = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", msgId = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const v6, 0x7f0a014e

    invoke-interface {p1, v6}, Landroid/view/ContextMenu;->setHeaderTitle(I)Landroid/view/ContextMenu;

    new-instance v1, Lcom/android/mms/ui/WPMessageActivity$MsgListMenuClickListener;

    iget-object v6, p0, Lcom/android/mms/ui/WPMessageActivity$2;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-direct {v1, v6, v4}, Lcom/android/mms/ui/WPMessageActivity$MsgListMenuClickListener;-><init>(Lcom/android/mms/ui/WPMessageActivity;Lcom/android/mms/ui/WPMessageItem;)V

    iget-boolean v6, v4, Lcom/android/mms/ui/WPMessageItem;->mLocked:Z

    if-eqz v6, :cond_3

    const/16 v6, 0x1d

    const v7, 0x7f0a01d8

    invoke-interface {p1, v9, v6, v9, v7}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v6

    invoke-interface {v6, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    :goto_1
    iget-object v6, p0, Lcom/android/mms/ui/WPMessageActivity$2;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-static {v6, p1, v1, v4}, Lcom/android/mms/ui/WPMessageActivity;->access$200(Lcom/android/mms/ui/WPMessageActivity;Landroid/view/ContextMenu;Lcom/android/mms/ui/WPMessageActivity$MsgListMenuClickListener;Lcom/android/mms/ui/WPMessageItem;)V

    iget-object v6, v4, Lcom/android/mms/ui/WPMessageItem;->mURL:Ljava/lang/String;

    if-eqz v6, :cond_2

    const/16 v6, 0x1f

    const v7, 0x7f0a0025

    invoke-interface {p1, v9, v6, v9, v7}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v6

    invoke-interface {v6, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    const/16 v6, 0x1e

    const v7, 0x7f0a0026

    invoke-interface {p1, v9, v6, v9, v7}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v6

    invoke-interface {v6, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    :cond_2
    const/16 v6, 0x15

    const v7, 0x7f0a0147

    invoke-interface {p1, v9, v6, v9, v7}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v6

    invoke-interface {v6, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    const/16 v6, 0x18

    const v7, 0x7f0a014d

    invoke-interface {p1, v9, v6, v9, v7}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v6

    invoke-interface {v6, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    const/16 v6, 0x11

    const v7, 0x7f0a013e

    invoke-interface {p1, v9, v6, v9, v7}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v6

    invoke-interface {v6, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    const/16 v6, 0x12

    const v7, 0x7f0a0140

    invoke-interface {p1, v9, v6, v9, v7}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v6

    invoke-interface {v6, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto/16 :goto_0

    :cond_3
    const/16 v6, 0x1c

    const v7, 0x7f0a01d7

    invoke-interface {p1, v9, v6, v9, v7}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v6

    invoke-interface {v6, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_1
.end method
