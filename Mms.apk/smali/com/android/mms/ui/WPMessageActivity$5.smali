.class Lcom/android/mms/ui/WPMessageActivity$5;
.super Ljava/lang/Object;
.source "WPMessageActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/WPMessageActivity;->onAddContactButtonClickInt(Ljava/lang/String;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/WPMessageActivity;

.field final synthetic val$number:Ljava/lang/String;

.field final synthetic val$startActivityForResult:Z


# direct methods
.method constructor <init>(Lcom/android/mms/ui/WPMessageActivity;Ljava/lang/String;Z)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/WPMessageActivity$5;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    iput-object p2, p0, Lcom/android/mms/ui/WPMessageActivity$5;->val$number:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/android/mms/ui/WPMessageActivity$5;->val$startActivityForResult:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.INSERT"

    sget-object v2, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v1, "phone"

    iget-object v2, p0, Lcom/android/mms/ui/WPMessageActivity$5;->val$number:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-boolean v1, p0, Lcom/android/mms/ui/WPMessageActivity$5;->val$startActivityForResult:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/mms/ui/WPMessageActivity$5;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    const/16 v2, 0x12

    invoke-virtual {v1, v0, v2}, Lcom/android/mms/ui/WPMessageActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/mms/ui/WPMessageActivity$5;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
