.class final Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;
.super Lcom/android/mms/ui/ConversationList$BaseProgressQueryHandler;
.source "WPMessageActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/ui/WPMessageActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "BackgroundQueryHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/WPMessageActivity;


# direct methods
.method public constructor <init>(Lcom/android/mms/ui/WPMessageActivity;Landroid/content/ContentResolver;)V
    .locals 0
    .param p2    # Landroid/content/ContentResolver;

    iput-object p1, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-direct {p0, p2}, Lcom/android/mms/ui/ConversationList$BaseProgressQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    return-void
.end method


# virtual methods
.method protected onDeleteComplete(ILjava/lang/Object;I)V
    .locals 3
    .param p1    # I
    .param p2    # Ljava/lang/Object;
    .param p3    # I

    sparse-switch p1, :sswitch_data_0

    :cond_0
    :goto_0
    const/16 v0, 0x709

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-static {v0}, Lcom/android/mms/data/Contact;->init(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-static {v0}, Lcom/android/mms/data/Conversation;->init(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_1
    return-void

    :sswitch_0
    iget-object v0, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-static {v0}, Lcom/android/mms/ui/WPMessageActivity;->access$1600(Lcom/android/mms/ui/WPMessageActivity;)Lcom/android/mms/data/Conversation;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/mms/data/Conversation;->setMessageCount(I)V

    :sswitch_1
    iget-object v0, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    const-wide/16 v1, -0x2

    invoke-static {v0, v1, v2}, Lcom/android/mms/transaction/WapPushMessagingNotification;->nonBlockingUpdateNewMessageIndicator(Landroid/content/Context;J)V

    invoke-virtual {p0}, Lcom/android/mms/ui/ConversationList$BaseProgressQueryHandler;->progress()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/mms/ui/ConversationList$BaseProgressQueryHandler;->dismissProgressDialog()V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x709 -> :sswitch_0
        0x25e4 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 12
    .param p1    # I
    .param p2    # Ljava/lang/Object;
    .param p3    # Landroid/database/Cursor;

    sparse-switch p1, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iget-object v8, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-static {v8}, Lcom/android/mms/ui/WPMessageActivity;->access$1600(Lcom/android/mms/ui/WPMessageActivity;)Lcom/android/mms/data/Conversation;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/mms/data/Conversation;->getThreadId()J

    move-result-wide v8

    cmp-long v8, v6, v8

    if-eqz v8, :cond_0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onQueryComplete: msg history query result is for threadId "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", but mConversation has threadId "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-static {v9}, Lcom/android/mms/ui/WPMessageActivity;->access$1600(Lcom/android/mms/ui/WPMessageActivity;)Lcom/android/mms/data/Conversation;

    move-result-object v9

    invoke-virtual {v9}, Lcom/android/mms/data/Conversation;->getThreadId()J

    move-result-wide v9

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " starting a new query"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/android/mms/ui/WPMessageActivity;->access$1700(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-static {v8}, Lcom/android/mms/ui/WPMessageActivity;->access$1500(Lcom/android/mms/ui/WPMessageActivity;)V

    goto :goto_0

    :cond_0
    const/4 v2, -0x1

    iget-object v8, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-virtual {v8}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    const-string v9, "select_id"

    const-wide/16 v10, -0x1

    invoke-virtual {v8, v9, v10, v11}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    const-wide/16 v8, -0x1

    cmp-long v8, v3, v8

    if-eqz v8, :cond_2

    const/4 v8, -0x1

    invoke-interface {p3, v8}, Landroid/database/Cursor;->moveToPosition(I)Z

    const-wide/16 v0, 0x0

    :cond_1
    invoke-interface {p3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v8, 0x0

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    cmp-long v8, v0, v3

    if-nez v8, :cond_1

    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    :cond_2
    iget-object v8, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    iget-object v8, v8, Lcom/android/mms/ui/WPMessageActivity;->mMsgListAdapter:Lcom/android/mms/ui/WPMessageListAdapter;

    invoke-virtual {v8, p3}, Landroid/widget/CursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    const/4 v8, -0x1

    if-eq v2, v8, :cond_3

    iget-object v8, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-static {v8}, Lcom/android/mms/ui/WPMessageActivity;->access$1800(Lcom/android/mms/ui/WPMessageActivity;)Lcom/android/mms/ui/WPMessageListView;

    move-result-object v8

    invoke-virtual {v8, v2}, Landroid/widget/ListView;->setSelection(I)V

    :cond_3
    iget-object v8, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-static {v8}, Lcom/android/mms/ui/WPMessageActivity;->access$1600(Lcom/android/mms/ui/WPMessageActivity;)Lcom/android/mms/data/Conversation;

    move-result-object v8

    iget-object v9, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    iget-object v9, v9, Lcom/android/mms/ui/WPMessageActivity;->mMsgListAdapter:Lcom/android/mms/ui/WPMessageListAdapter;

    invoke-virtual {v9}, Landroid/widget/CursorAdapter;->getCount()I

    move-result v9

    invoke-virtual {v8, v9}, Lcom/android/mms/data/Conversation;->setMessageCount(I)V

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v8

    if-nez v8, :cond_4

    sget-boolean v8, Lcom/android/mms/ui/WPMessageActivity;->mDestroy:Z

    if-nez v8, :cond_4

    iget-object v8, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-static {v8}, Lcom/android/mms/ui/WPMessageActivity;->access$1600(Lcom/android/mms/ui/WPMessageActivity;)Lcom/android/mms/data/Conversation;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/mms/data/Conversation;->clearThreadId()V

    iget-object v8, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-static {v8}, Lcom/android/mms/ui/WPMessageActivity;->access$1600(Lcom/android/mms/ui/WPMessageActivity;)Lcom/android/mms/data/Conversation;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/mms/data/Conversation;->ensureThreadId()J

    iget-object v8, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-static {v8}, Lcom/android/mms/ui/WPMessageActivity;->access$1300(Lcom/android/mms/ui/WPMessageActivity;)V

    :cond_4
    iget-object v8, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-static {v8}, Lcom/android/mms/ui/WPMessageActivity;->access$1600(Lcom/android/mms/ui/WPMessageActivity;)Lcom/android/mms/data/Conversation;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/android/mms/data/Conversation;->blockMarkAsRead(Z)V

    iget-object v8, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-virtual {v8}, Landroid/app/Activity;->invalidateOptionsMenu()V

    goto/16 :goto_0

    :sswitch_1
    move-object v5, p2

    check-cast v5, Ljava/util/ArrayList;

    new-instance v9, Lcom/android/mms/ui/ConversationList$DeleteThreadListener;

    iget-object v8, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-static {v8}, Lcom/android/mms/ui/WPMessageActivity;->access$000(Lcom/android/mms/ui/WPMessageActivity;)Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;

    move-result-object v8

    iget-object v10, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-direct {v9, v5, v8, v10}, Lcom/android/mms/ui/ConversationList$DeleteThreadListener;-><init>(Ljava/util/Collection;Landroid/content/AsyncQueryHandler;Landroid/content/Context;)V

    if-eqz p3, :cond_5

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v8

    if-lez v8, :cond_5

    const/4 v8, 0x1

    :goto_1
    iget-object v10, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-static {v9, v5, v8, v10}, Lcom/android/mms/ui/ConversationList;->confirmDeleteThreadDialog(Lcom/android/mms/ui/ConversationList$DeleteThreadListener;Ljava/util/Collection;ZLandroid/content/Context;)V

    goto/16 :goto_0

    :cond_5
    const/4 v8, 0x0

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x70a -> :sswitch_1
        0x2537 -> :sswitch_0
    .end sparse-switch
.end method
