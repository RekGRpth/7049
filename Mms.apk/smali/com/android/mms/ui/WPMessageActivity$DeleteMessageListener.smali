.class Lcom/android/mms/ui/WPMessageActivity$DeleteMessageListener;
.super Ljava/lang/Object;
.source "WPMessageActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/ui/WPMessageActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DeleteMessageListener"
.end annotation


# instance fields
.field private final mDeleteLocked:Z

.field private final mDeleteUri:Landroid/net/Uri;

.field final synthetic this$0:Lcom/android/mms/ui/WPMessageActivity;


# direct methods
.method public constructor <init>(Lcom/android/mms/ui/WPMessageActivity;JIZ)V
    .locals 1
    .param p2    # J
    .param p4    # I
    .param p5    # Z

    iput-object p1, p0, Lcom/android/mms/ui/WPMessageActivity$DeleteMessageListener;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Landroid/provider/Telephony$WapPush;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, p2, p3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/WPMessageActivity$DeleteMessageListener;->mDeleteUri:Landroid/net/Uri;

    iput-boolean p5, p0, Lcom/android/mms/ui/WPMessageActivity$DeleteMessageListener;->mDeleteLocked:Z

    return-void
.end method

.method public constructor <init>(Lcom/android/mms/ui/WPMessageActivity;Landroid/net/Uri;Z)V
    .locals 0
    .param p2    # Landroid/net/Uri;
    .param p3    # Z

    iput-object p1, p0, Lcom/android/mms/ui/WPMessageActivity$DeleteMessageListener;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/android/mms/ui/WPMessageActivity$DeleteMessageListener;->mDeleteUri:Landroid/net/Uri;

    iput-boolean p3, p0, Lcom/android/mms/ui/WPMessageActivity$DeleteMessageListener;->mDeleteLocked:Z

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageActivity$DeleteMessageListener;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-static {v0}, Lcom/android/mms/ui/WPMessageActivity;->access$000(Lcom/android/mms/ui/WPMessageActivity;)Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;

    move-result-object v0

    const/16 v1, 0x25e4

    iget-object v3, p0, Lcom/android/mms/ui/WPMessageActivity$DeleteMessageListener;->mDeleteUri:Landroid/net/Uri;

    iget-boolean v4, p0, Lcom/android/mms/ui/WPMessageActivity$DeleteMessageListener;->mDeleteLocked:Z

    if-eqz v4, :cond_0

    move-object v4, v2

    :goto_0
    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/AsyncQueryHandler;->startDelete(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    return-void

    :cond_0
    const-string v4, "locked=0"

    goto :goto_0
.end method
