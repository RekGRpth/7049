.class final Lcom/android/mms/ui/WPMessageActivity$MsgListMenuClickListener;
.super Ljava/lang/Object;
.source "WPMessageActivity.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/ui/WPMessageActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "MsgListMenuClickListener"
.end annotation


# instance fields
.field private msgItem:Lcom/android/mms/ui/WPMessageItem;

.field final synthetic this$0:Lcom/android/mms/ui/WPMessageActivity;


# direct methods
.method public constructor <init>(Lcom/android/mms/ui/WPMessageActivity;Lcom/android/mms/ui/WPMessageItem;)V
    .locals 0
    .param p2    # Lcom/android/mms/ui/WPMessageItem;

    iput-object p1, p0, Lcom/android/mms/ui/WPMessageActivity$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/android/mms/ui/WPMessageActivity$MsgListMenuClickListener;->msgItem:Lcom/android/mms/ui/WPMessageItem;

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 14
    .param p1    # Landroid/view/MenuItem;

    iget-object v1, p0, Lcom/android/mms/ui/WPMessageActivity$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-static {v1}, Lcom/android/mms/ui/WPMessageActivity;->access$100(Lcom/android/mms/ui/WPMessageActivity;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/android/mms/ui/WPMessageActivity$MsgListMenuClickListener;->msgItem:Lcom/android/mms/ui/WPMessageItem;

    if-nez v1, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    const/4 v1, 0x0

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/android/mms/ui/WPMessageActivity$MsgListMenuClickListener;->msgItem:Lcom/android/mms/ui/WPMessageItem;

    iget-object v1, v1, Lcom/android/mms/ui/WPMessageItem;->mBody:Ljava/lang/String;

    const-string v2, "\\r\\n"

    const-string v5, "\n"

    invoke-virtual {v1, v2, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iget-object v1, p0, Lcom/android/mms/ui/WPMessageActivity$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-static {v1, v7}, Lcom/android/mms/ui/WPMessageActivity;->access$300(Lcom/android/mms/ui/WPMessageActivity;Ljava/lang/String;)V

    const/4 v1, 0x1

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/android/mms/ui/WPMessageActivity$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    iget-object v2, p0, Lcom/android/mms/ui/WPMessageActivity$MsgListMenuClickListener;->msgItem:Lcom/android/mms/ui/WPMessageItem;

    invoke-static {v1, v2}, Lcom/android/mms/ui/WPMessageActivity;->access$400(Lcom/android/mms/ui/WPMessageActivity;Lcom/android/mms/ui/WPMessageItem;)V

    const/4 v1, 0x1

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/android/mms/ui/WPMessageActivity$MsgListMenuClickListener;->msgItem:Lcom/android/mms/ui/WPMessageItem;

    iget-object v1, v1, Lcom/android/mms/ui/WPMessageItem;->mURL:Ljava/lang/String;

    if-nez v1, :cond_2

    const-string v1, "Mms/WapPush"

    const-string v2, "WPMessageActivity: MENU_GOTO_BROWSER msgItem.mURL is null."

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/mms/ui/WPMessageActivity$MsgListMenuClickListener;->msgItem:Lcom/android/mms/ui/WPMessageItem;

    iget-object v1, v1, Lcom/android/mms/ui/WPMessageItem;->mURL:Ljava/lang/String;

    invoke-static {v1}, Lcom/android/mms/ui/MessageUtils;->checkAndModifyUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v13

    new-instance v10, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v10, v1, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v1, "com.android.browser.application_id"

    iget-object v2, p0, Lcom/android/mms/ui/WPMessageActivity$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-virtual {v2}, Landroid/content/ContextWrapper;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x80000

    invoke-virtual {v10, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    :try_start_0
    iget-object v1, p0, Lcom/android/mms/ui/WPMessageActivity$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-virtual {v1, v10}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    :catch_0
    move-exception v8

    iget-object v1, p0, Lcom/android/mms/ui/WPMessageActivity$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    const v2, 0x7f0a0028

    const/4 v5, 0x1

    invoke-static {v1, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    const-string v1, "Mms/WapPush"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Scheme "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v13}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "is not supported!"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :pswitch_4
    iget-object v1, p0, Lcom/android/mms/ui/WPMessageActivity$MsgListMenuClickListener;->msgItem:Lcom/android/mms/ui/WPMessageItem;

    iget-object v1, v1, Lcom/android/mms/ui/WPMessageItem;->mURL:Ljava/lang/String;

    if-nez v1, :cond_3

    const-string v1, "Mms/WapPush"

    const-string v2, "WPMessageActivity: MENU_GOTO_BROWSER msgItem.mURL is null."

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x1

    goto/16 :goto_0

    :cond_3
    new-instance v9, Landroid/content/Intent;

    invoke-direct {v9}, Landroid/content/Intent;-><init>()V

    const-string v1, "android.intent.action.INSERT"

    invoke-virtual {v9, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "vnd.android.cursor.dir/bookmark"

    invoke-virtual {v9, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "url"

    iget-object v2, p0, Lcom/android/mms/ui/WPMessageActivity$MsgListMenuClickListener;->msgItem:Lcom/android/mms/ui/WPMessageItem;

    iget-object v2, v2, Lcom/android/mms/ui/WPMessageItem;->mURL:Ljava/lang/String;

    invoke-static {v2}, Lcom/android/mms/ui/MessageUtils;->checkAndModifyUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/mms/ui/WPMessageActivity$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-virtual {v1, v9}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    const/4 v1, 0x1

    goto/16 :goto_0

    :pswitch_5
    iget-object v1, p0, Lcom/android/mms/ui/WPMessageActivity$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    iget-object v2, p0, Lcom/android/mms/ui/WPMessageActivity$MsgListMenuClickListener;->msgItem:Lcom/android/mms/ui/WPMessageItem;

    invoke-static {v1, v2}, Lcom/android/mms/ui/WPMessageActivity;->access$500(Landroid/content/Context;Lcom/android/mms/ui/WPMessageItem;)Ljava/lang/String;

    move-result-object v11

    iget-object v1, p0, Lcom/android/mms/ui/WPMessageActivity$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/android/mms/ui/WPMessageActivity$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-direct {v2, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v5, 0x7f0a01c2

    invoke-virtual {v2, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v11}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/mms/ui/WPMessageActivity;->access$602(Lcom/android/mms/ui/WPMessageActivity;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    const/4 v1, 0x1

    goto/16 :goto_0

    :pswitch_6
    new-instance v0, Lcom/android/mms/ui/WPMessageActivity$DeleteMessageListener;

    iget-object v1, p0, Lcom/android/mms/ui/WPMessageActivity$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    iget-object v2, p0, Lcom/android/mms/ui/WPMessageActivity$MsgListMenuClickListener;->msgItem:Lcom/android/mms/ui/WPMessageItem;

    iget-wide v2, v2, Lcom/android/mms/ui/WPMessageItem;->mMsgId:J

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/android/mms/ui/WPMessageActivity$MsgListMenuClickListener;->msgItem:Lcom/android/mms/ui/WPMessageItem;

    iget-boolean v5, v5, Lcom/android/mms/ui/WPMessageItem;->mLocked:Z

    invoke-direct/range {v0 .. v5}, Lcom/android/mms/ui/WPMessageActivity$DeleteMessageListener;-><init>(Lcom/android/mms/ui/WPMessageActivity;JIZ)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "_id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/mms/ui/WPMessageActivity$MsgListMenuClickListener;->msgItem:Lcom/android/mms/ui/WPMessageItem;

    iget-wide v5, v2, Lcom/android/mms/ui/WPMessageItem;->mMsgId:J

    invoke-virtual {v1, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v1, 0x1

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "thread_id"

    aput-object v2, v3, v1

    const-string v1, "Mms/WapPush"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "WPMessageActivity: where:"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/mms/ui/WPMessageActivity$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/Telephony$WapPush;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/mms/ui/WPMessageActivity$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    const/4 v2, 0x0

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/mms/ui/WPMessageActivity;->access$702(Lcom/android/mms/ui/WPMessageActivity;Ljava/lang/Long;)Ljava/lang/Long;

    :cond_4
    iget-object v1, p0, Lcom/android/mms/ui/WPMessageActivity$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    iget-object v2, p0, Lcom/android/mms/ui/WPMessageActivity$MsgListMenuClickListener;->msgItem:Lcom/android/mms/ui/WPMessageItem;

    iget-boolean v2, v2, Lcom/android/mms/ui/WPMessageItem;->mLocked:Z

    invoke-static {v1, v0, v2}, Lcom/android/mms/ui/WPMessageActivity;->access$800(Lcom/android/mms/ui/WPMessageActivity;Landroid/content/DialogInterface$OnClickListener;Z)V

    const/4 v1, 0x1

    goto/16 :goto_0

    :pswitch_7
    iget-object v1, p0, Lcom/android/mms/ui/WPMessageActivity$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    iget-object v2, p0, Lcom/android/mms/ui/WPMessageActivity$MsgListMenuClickListener;->msgItem:Lcom/android/mms/ui/WPMessageItem;

    const/4 v5, 0x1

    invoke-static {v1, v2, v5}, Lcom/android/mms/ui/WPMessageActivity;->access$900(Lcom/android/mms/ui/WPMessageActivity;Lcom/android/mms/ui/WPMessageItem;Z)V

    const/4 v1, 0x1

    goto/16 :goto_0

    :pswitch_8
    iget-object v1, p0, Lcom/android/mms/ui/WPMessageActivity$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    iget-object v2, p0, Lcom/android/mms/ui/WPMessageActivity$MsgListMenuClickListener;->msgItem:Lcom/android/mms/ui/WPMessageItem;

    const/4 v5, 0x0

    invoke-static {v1, v2, v5}, Lcom/android/mms/ui/WPMessageActivity;->access$900(Lcom/android/mms/ui/WPMessageActivity;Lcom/android/mms/ui/WPMessageItem;Z)V

    const/4 v1, 0x1

    goto/16 :goto_0

    :pswitch_9
    iget-object v1, p0, Lcom/android/mms/ui/WPMessageActivity$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-interface {p1}, Landroid/view/MenuItem;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/mms/ui/WPMessageActivity;->access$1002(Lcom/android/mms/ui/WPMessageActivity;Landroid/content/Intent;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/mms/ui/WPMessageActivity$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    iget-object v2, p0, Lcom/android/mms/ui/WPMessageActivity$MsgListMenuClickListener;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-static {v2}, Lcom/android/mms/ui/WPMessageActivity;->access$1000(Lcom/android/mms/ui/WPMessageActivity;)Landroid/content/Intent;

    move-result-object v2

    const/16 v5, 0x12

    invoke-virtual {v1, v2, v5}, Lcom/android/mms/ui/WPMessageActivity;->startActivityForResult(Landroid/content/Intent;I)V

    const/4 v1, 0x1

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x11
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_7
        :pswitch_8
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method
