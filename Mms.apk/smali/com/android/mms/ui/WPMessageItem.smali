.class public Lcom/android/mms/ui/WPMessageItem;
.super Ljava/lang/Object;
.source "WPMessageItem.java"


# static fields
.field private static WP_TAG:Ljava/lang/String;


# instance fields
.field isExpired:I

.field mAction:I

.field mAddress:Ljava/lang/String;

.field mBody:Ljava/lang/String;

.field mContact:Ljava/lang/String;

.field final mContext:Landroid/content/Context;

.field mCreate:J

.field mDate:J

.field mExpiration:Ljava/lang/String;

.field mExpirationLong:J

.field mHighlight:Ljava/util/regex/Pattern;

.field mLocked:Z

.field final mMsgId:J

.field mSimId:I

.field mText:Ljava/lang/String;

.field mTimestamp:Ljava/lang/String;

.field final mType:I

.field mURL:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "Mms/WapPush"

    sput-object v0, Lcom/android/mms/ui/WPMessageItem;->WP_TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;Ljava/util/regex/Pattern;)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # Landroid/database/Cursor;
    .param p4    # Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;
    .param p5    # Ljava/util/regex/Pattern;

    const-wide/16 v7, 0x3e8

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/mms/ui/WPMessageItem;->mContext:Landroid/content/Context;

    iget v3, p4, Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;->mColumnMsgId:I

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/android/mms/ui/WPMessageItem;->mMsgId:J

    iput-object p5, p0, Lcom/android/mms/ui/WPMessageItem;->mHighlight:Ljava/util/regex/Pattern;

    iput p2, p0, Lcom/android/mms/ui/WPMessageItem;->mType:I

    iget v3, p4, Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;->mColumnWpmsDate:I

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/android/mms/ui/WPMessageItem;->mDate:J

    const v3, 0x7f0a0037

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v1, [Ljava/lang/Object;

    iget-wide v5, p0, Lcom/android/mms/ui/WPMessageItem;->mDate:J

    invoke-static {p1, v5, v6}, Lcom/android/mms/ui/MessageUtils;->formatTimeStampString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/mms/ui/WPMessageItem;->mTimestamp:Ljava/lang/String;

    iget v3, p4, Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;->mColumnWpmsAddr:I

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/mms/ui/WPMessageItem;->mAddress:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/mms/ui/WPMessageItem;->mAddress:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/android/mms/ui/WPMessageItem;->mAddress:Ljava/lang/String;

    invoke-static {v3, v2}, Lcom/android/mms/data/Contact;->get(Ljava/lang/String;Z)Lcom/android/mms/data/Contact;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/mms/data/Contact;->getName()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/mms/ui/WPMessageItem;->mContact:Ljava/lang/String;

    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    iget v3, p4, Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;->mColumnWpmsText:I

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/mms/ui/WPMessageItem;->mText:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/mms/ui/WPMessageItem;->mText:Ljava/lang/String;

    if-eqz v3, :cond_0

    const-string v3, ""

    iget-object v4, p0, Lcom/android/mms/ui/WPMessageItem;->mText:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/android/mms/ui/WPMessageItem;->mText:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget v3, p4, Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;->mColumnWpmsURL:I

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/mms/ui/WPMessageItem;->mURL:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/mms/ui/WPMessageItem;->mURL:Ljava/lang/String;

    if-eqz v3, :cond_1

    const-string v3, ""

    iget-object v4, p0, Lcom/android/mms/ui/WPMessageItem;->mURL:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/android/mms/ui/WPMessageItem;->mURL:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/mms/ui/WPMessageItem;->mBody:Ljava/lang/String;

    iget v3, p4, Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;->mColumnWpmsCreate:I

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    mul-long/2addr v3, v7

    iput-wide v3, p0, Lcom/android/mms/ui/WPMessageItem;->mCreate:J

    iget v3, p4, Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;->mColumnWpmsExpiration:I

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    mul-long/2addr v3, v7

    iput-wide v3, p0, Lcom/android/mms/ui/WPMessageItem;->mExpirationLong:J

    const-wide/16 v3, 0x0

    iget-wide v5, p0, Lcom/android/mms/ui/WPMessageItem;->mExpirationLong:J

    cmp-long v3, v3, v5

    if-eqz v3, :cond_2

    const v3, 0x7f0a0027

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v1, [Ljava/lang/Object;

    iget-wide v5, p0, Lcom/android/mms/ui/WPMessageItem;->mExpirationLong:J

    invoke-static {p1, v5, v6}, Lcom/android/mms/ui/MessageUtils;->formatTimeStampString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/mms/ui/WPMessageItem;->mExpiration:Ljava/lang/String;

    :cond_2
    iget v3, p4, Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;->mColumnWpmsError:I

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, p0, Lcom/android/mms/ui/WPMessageItem;->isExpired:I

    iget v3, p4, Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;->mColumnWpmsAction:I

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, p0, Lcom/android/mms/ui/WPMessageItem;->mAction:I

    iget v3, p4, Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;->mColumnWpmsSimId:I

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, p0, Lcom/android/mms/ui/WPMessageItem;->mSimId:I

    iget v3, p4, Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;->mColumnWpmsLocked:I

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-eqz v3, :cond_4

    :goto_1
    iput-boolean v1, p0, Lcom/android/mms/ui/WPMessageItem;->mLocked:Z

    return-void

    :cond_3
    const-string v3, ""

    iput-object v3, p0, Lcom/android/mms/ui/WPMessageItem;->mContact:Ljava/lang/String;

    goto/16 :goto_0

    :cond_4
    move v1, v2

    goto :goto_1
.end method


# virtual methods
.method public getSimId()I
    .locals 1

    iget v0, p0, Lcom/android/mms/ui/WPMessageItem;->mSimId:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/mms/ui/WPMessageItem;->mType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " sim: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/mms/ui/WPMessageItem;->mSimId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " text: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mms/ui/WPMessageItem;->mText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " url: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mms/ui/WPMessageItem;->mURL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " time: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mms/ui/WPMessageItem;->mTimestamp:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " address: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mms/ui/WPMessageItem;->mAddress:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " contact: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mms/ui/WPMessageItem;->mContact:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " create: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/mms/ui/WPMessageItem;->mCreate:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " expiration: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mms/ui/WPMessageItem;->mExpiration:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " action: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/mms/ui/WPMessageItem;->mAction:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
