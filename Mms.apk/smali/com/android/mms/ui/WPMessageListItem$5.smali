.class Lcom/android/mms/ui/WPMessageListItem$5;
.super Ljava/lang/Object;
.source "WPMessageListItem.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/WPMessageListItem;->onMessageListItemClick()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/WPMessageListItem;

.field final synthetic val$spansFinal:[Landroid/text/style/URLSpan;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/WPMessageListItem;[Landroid/text/style/URLSpan;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/WPMessageListItem$5;->this$0:Lcom/android/mms/ui/WPMessageListItem;

    iput-object p2, p0, Lcom/android/mms/ui/WPMessageListItem$5;->val$spansFinal:[Landroid/text/style/URLSpan;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    if-ltz p2, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageListItem$5;->val$spansFinal:[Landroid/text/style/URLSpan;

    aget-object v0, v0, p2

    iget-object v1, p0, Lcom/android/mms/ui/WPMessageListItem$5;->this$0:Lcom/android/mms/ui/WPMessageListItem;

    invoke-static {v1}, Lcom/android/mms/ui/WPMessageListItem;->access$300(Lcom/android/mms/ui/WPMessageListItem;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/style/URLSpan;->onClick(Landroid/view/View;)V

    :cond_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method
