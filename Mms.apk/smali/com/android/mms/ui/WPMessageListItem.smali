.class public Lcom/android/mms/ui/WPMessageListItem;
.super Landroid/widget/LinearLayout;
.source "WPMessageListItem.java"


# static fields
.field private static final DEFAULT_ICON_INDENT:I = 0x5

.field public static final EXTRA_URLS:Ljava/lang/String; = "com.android.mms.ExtraUrls"

.field private static final STYLE_BOLD:Landroid/text/style/StyleSpan;

.field private static final WP_TAG:Ljava/lang/String; = "Mms/WapPush"

.field private static sDefaultContactImage:Landroid/graphics/drawable/Drawable;


# instance fields
.field private mAvatar:Lcom/android/mms/ui/QuickContactDivot;

.field private mBodyTextView:Landroid/widget/TextView;

.field mColorSpan:Landroid/text/style/ForegroundColorSpan;

.field private mDefaultCountryIso:Ljava/lang/String;

.field private mExpirationIndicator:Landroid/widget/ImageView;

.field private mHandler:Landroid/os/Handler;

.field private mIsLastItemInList:Z

.field private mItemContainer:Landroid/view/View;

.field private mLinkSpan:Landroid/text/style/ClickableSpan;

.field private mLockedIndicator:Landroid/widget/ImageView;

.field public mMessageBlock:Landroid/view/View;

.field private mMessageItem:Lcom/android/mms/ui/WPMessageItem;

.field private mPaint:Landroid/graphics/Paint;

.field private mPath:Landroid/graphics/Path;

.field private mSimStatus:Landroid/widget/TextView;

.field private mSpan:Landroid/text/style/LineHeightSpan;

.field private mTimestamp:Landroid/widget/TextView;

.field private statusIconIndent:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/text/style/StyleSpan;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/style/StyleSpan;-><init>(I)V

    sput-object v0, Lcom/android/mms/ui/WPMessageListItem;->STYLE_BOLD:Landroid/text/style/StyleSpan;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mPath:Landroid/graphics/Path;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mPaint:Landroid/graphics/Paint;

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/mms/ui/WPMessageListItem;->statusIconIndent:I

    new-instance v0, Lcom/android/mms/ui/WPMessageListItem$2;

    invoke-direct {v0, p0}, Lcom/android/mms/ui/WPMessageListItem$2;-><init>(Lcom/android/mms/ui/WPMessageListItem;)V

    iput-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mSpan:Landroid/text/style/LineHeightSpan;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mColorSpan:Landroid/text/style/ForegroundColorSpan;

    new-instance v0, Lcom/android/mms/ui/WPMessageListItem$3;

    invoke-direct {v0, p0}, Lcom/android/mms/ui/WPMessageListItem$3;-><init>(Lcom/android/mms/ui/WPMessageListItem;)V

    iput-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mLinkSpan:Landroid/text/style/ClickableSpan;

    invoke-static {}, Lcom/android/mms/MmsApp;->getApplication()Lcom/android/mms/MmsApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/mms/MmsApp;->getCurrentCountryIso()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mDefaultCountryIso:Ljava/lang/String;

    sget-object v0, Lcom/android/mms/ui/WPMessageListItem;->sDefaultContactImage:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020054

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lcom/android/mms/ui/WPMessageListItem;->sDefaultContactImage:Landroid/graphics/drawable/Drawable;

    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/android/mms/ui/WPMessageListItem;->mPath:Landroid/graphics/Path;

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/android/mms/ui/WPMessageListItem;->mPaint:Landroid/graphics/Paint;

    const/4 v1, 0x0

    iput v1, p0, Lcom/android/mms/ui/WPMessageListItem;->statusIconIndent:I

    new-instance v1, Lcom/android/mms/ui/WPMessageListItem$2;

    invoke-direct {v1, p0}, Lcom/android/mms/ui/WPMessageListItem$2;-><init>(Lcom/android/mms/ui/WPMessageListItem;)V

    iput-object v1, p0, Lcom/android/mms/ui/WPMessageListItem;->mSpan:Landroid/text/style/LineHeightSpan;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/mms/ui/WPMessageListItem;->mColorSpan:Landroid/text/style/ForegroundColorSpan;

    new-instance v1, Lcom/android/mms/ui/WPMessageListItem$3;

    invoke-direct {v1, p0}, Lcom/android/mms/ui/WPMessageListItem$3;-><init>(Lcom/android/mms/ui/WPMessageListItem;)V

    iput-object v1, p0, Lcom/android/mms/ui/WPMessageListItem;->mLinkSpan:Landroid/text/style/ClickableSpan;

    iget-object v1, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070003

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v1, v0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    iput-object v1, p0, Lcom/android/mms/ui/WPMessageListItem;->mColorSpan:Landroid/text/style/ForegroundColorSpan;

    invoke-static {}, Lcom/android/mms/MmsApp;->getApplication()Lcom/android/mms/MmsApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/mms/MmsApp;->getCurrentCountryIso()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mms/ui/WPMessageListItem;->mDefaultCountryIso:Ljava/lang/String;

    sget-object v1, Lcom/android/mms/ui/WPMessageListItem;->sDefaultContactImage:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020054

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/android/mms/ui/WPMessageListItem;->sDefaultContactImage:Landroid/graphics/drawable/Drawable;

    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/android/mms/ui/WPMessageListItem;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/android/mms/ui/WPMessageListItem;

    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/mms/ui/WPMessageListItem;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/android/mms/ui/WPMessageListItem;

    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/mms/ui/WPMessageListItem;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/mms/ui/WPMessageListItem;

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mDefaultCountryIso:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/mms/ui/WPMessageListItem;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/android/mms/ui/WPMessageListItem;

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mBodyTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method private bindCommonMessage(Lcom/android/mms/ui/WPMessageItem;)V
    .locals 13
    .param p1    # Lcom/android/mms/ui/WPMessageItem;

    const/4 v12, 0x0

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mBodyTextView:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/HideReturnsTransformationMethod;->getInstance()Landroid/text/method/HideReturnsTransformationMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    iget-object v8, p1, Lcom/android/mms/ui/WPMessageItem;->mAddress:Ljava/lang/String;

    iget-object v0, p1, Lcom/android/mms/ui/WPMessageItem;->mAddress:Ljava/lang/String;

    invoke-direct {p0, v0, v12}, Lcom/android/mms/ui/WPMessageListItem;->updateAvatarView(Ljava/lang/String;Z)V

    iget-object v2, p1, Lcom/android/mms/ui/WPMessageItem;->mContact:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/mms/ui/WPMessageItem;->mText:Ljava/lang/String;

    iget-object v4, p1, Lcom/android/mms/ui/WPMessageItem;->mURL:Ljava/lang/String;

    iget-object v5, p1, Lcom/android/mms/ui/WPMessageItem;->mTimestamp:Ljava/lang/String;

    iget-object v6, p1, Lcom/android/mms/ui/WPMessageItem;->mExpiration:Ljava/lang/String;

    iget-object v7, p1, Lcom/android/mms/ui/WPMessageItem;->mHighlight:Ljava/util/regex/Pattern;

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/android/mms/ui/WPMessageListItem;->formatMessage(Lcom/android/mms/ui/WPMessageItem;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/CharSequence;

    move-result-object v9

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mBodyTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p1, Lcom/android/mms/ui/WPMessageItem;->mTimestamp:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/android/mms/ui/WPMessageListItem;->formatTimestamp(Lcom/android/mms/ui/WPMessageItem;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v11

    invoke-direct {p0, p1}, Lcom/android/mms/ui/WPMessageListItem;->formatSimStatus(Lcom/android/mms/ui/WPMessageItem;)Ljava/lang/CharSequence;

    move-result-object v10

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mTimestamp:Landroid/widget/TextView;

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mSimStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, p1}, Lcom/android/mms/ui/WPMessageListItem;->drawRightStatusIndicator(Lcom/android/mms/ui/WPMessageItem;)V

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mTimestamp:Landroid/widget/TextView;

    iget v1, p0, Lcom/android/mms/ui/WPMessageListItem;->statusIconIndent:I

    invoke-virtual {v0, v1, v12, v12, v12}, Landroid/widget/TextView;->setPadding(IIII)V

    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    return-void
.end method

.method private drawRightStatusIndicator(Lcom/android/mms/ui/WPMessageItem;)V
    .locals 4
    .param p1    # Lcom/android/mms/ui/WPMessageItem;

    const/16 v3, 0x8

    const/4 v2, 0x0

    const/4 v0, 0x5

    iput v0, p0, Lcom/android/mms/ui/WPMessageListItem;->statusIconIndent:I

    iget-boolean v0, p1, Lcom/android/mms/ui/WPMessageItem;->mLocked:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mLockedIndicator:Landroid/widget/ImageView;

    const v1, 0x7f020061

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mLockedIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget v0, p0, Lcom/android/mms/ui/WPMessageListItem;->statusIconIndent:I

    iget-object v1, p0, Lcom/android/mms/ui/WPMessageListItem;->mLockedIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/mms/ui/WPMessageListItem;->statusIconIndent:I

    :goto_0
    const/4 v0, 0x1

    iget v1, p1, Lcom/android/mms/ui/WPMessageItem;->isExpired:I

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mExpirationIndicator:Landroid/widget/ImageView;

    const v1, 0x7f020008

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mExpirationIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget v0, p0, Lcom/android/mms/ui/WPMessageListItem;->statusIconIndent:I

    iget-object v1, p0, Lcom/android/mms/ui/WPMessageListItem;->mExpirationIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/mms/ui/WPMessageListItem;->statusIconIndent:I

    :goto_1
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mLockedIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mExpirationIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method private formatMessage(Lcom/android/mms/ui/WPMessageItem;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/CharSequence;
    .locals 8
    .param p1    # Lcom/android/mms/ui/WPMessageItem;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/util/regex/Pattern;

    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {}, Lcom/android/mms/util/SmileyParser;->getInstance()Lcom/android/mms/util/SmileyParser;

    move-result-object v2

    invoke-virtual {v2, p3}, Lcom/android/mms/util/SmileyParser;->addSmileySpans(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const-string v4, "\n"

    invoke-virtual {v0, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_0
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    invoke-virtual {v0, p4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iget-object v4, p0, Lcom/android/mms/ui/WPMessageListItem;->mLinkSpan:Landroid/text/style/ClickableSpan;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v5

    const/16 v6, 0x21

    invoke-virtual {v0, v4, v3, v5, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_1
    if-eqz p7, :cond_2

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p7, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    :goto_0
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v4

    if-eqz v4, :cond_2

    new-instance v4, Landroid/text/style/StyleSpan;

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->start()I

    move-result v5

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->end()I

    move-result v6

    const/4 v7, 0x0

    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method private formatSimStatus(Lcom/android/mms/ui/WPMessageItem;)Ljava/lang/CharSequence;
    .locals 6
    .param p1    # Lcom/android/mms/ui/WPMessageItem;

    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    iget-object v3, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/mms/ui/WPMessageListItem;->mMessageItem:Lcom/android/mms/ui/WPMessageItem;

    iget v4, v4, Lcom/android/mms/ui/WPMessageItem;->mSimId:I

    invoke-static {v3, v4}, Lcom/android/mms/ui/MessageUtils;->getSimInfo(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-lez v3, :cond_0

    const-string v3, " "

    invoke-virtual {v0, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iget-object v3, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    const v4, 0x7f0a0081

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    const-string v3, " "

    invoke-virtual {v0, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_0
    iget-object v3, p0, Lcom/android/mms/ui/WPMessageListItem;->mColorSpan:Landroid/text/style/ForegroundColorSpan;

    const/4 v4, 0x0

    const/16 v5, 0x21

    invoke-virtual {v0, v3, v4, v2, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    return-object v0
.end method

.method private formatTimestamp(Lcom/android/mms/ui/WPMessageItem;Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 5
    .param p1    # Lcom/android/mms/ui/WPMessageItem;
    .param p2    # Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string p2, " "

    :cond_0
    invoke-virtual {v0, p2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iget-object v1, p0, Lcom/android/mms/ui/WPMessageListItem;->mSpan:Landroid/text/style/LineHeightSpan;

    const/4 v2, 0x1

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    iget-object v1, p0, Lcom/android/mms/ui/WPMessageListItem;->mColorSpan:Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    const/16 v3, 0x21

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    return-object v0
.end method

.method private updateAvatarView(Ljava/lang/String;Z)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    const/4 v3, 0x0

    if-nez p2, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    :cond_0
    if-eqz p2, :cond_1

    invoke-static {v3}, Lcom/android/mms/data/Contact;->getMe(Z)Lcom/android/mms/data/Contact;

    move-result-object v1

    :goto_0
    iget-object v2, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    sget-object v3, Lcom/android/mms/ui/WPMessageListItem;->sDefaultContactImage:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2, v3}, Lcom/android/mms/data/Contact;->getAvatar(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz p2, :cond_2

    iget-object v2, p0, Lcom/android/mms/ui/WPMessageListItem;->mAvatar:Lcom/android/mms/ui/QuickContactDivot;

    sget-object v3, Landroid/provider/ContactsContract$Profile;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/widget/QuickContactBadge;->assignContactUri(Landroid/net/Uri;)V

    :goto_1
    iget-object v2, p0, Lcom/android/mms/ui/WPMessageListItem;->mAvatar:Lcom/android/mms/ui/QuickContactDivot;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void

    :cond_1
    invoke-static {p1, v3}, Lcom/android/mms/data/Contact;->get(Ljava/lang/String;Z)Lcom/android/mms/data/Contact;

    move-result-object v1

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Lcom/android/mms/data/Contact;->existsInDatabase()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/mms/ui/WPMessageListItem;->mAvatar:Lcom/android/mms/ui/QuickContactDivot;

    invoke-virtual {v1}, Lcom/android/mms/data/Contact;->getUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/QuickContactBadge;->assignContactUri(Landroid/net/Uri;)V

    goto :goto_1

    :cond_3
    iget-object v2, p0, Lcom/android/mms/ui/WPMessageListItem;->mAvatar:Lcom/android/mms/ui/QuickContactDivot;

    invoke-virtual {v1}, Lcom/android/mms/data/Contact;->getNumber()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/widget/QuickContactBadge;->assignContactFromPhone(Ljava/lang/String;Z)V

    goto :goto_1

    :cond_4
    sget-object v0, Lcom/android/mms/ui/WPMessageListItem;->sDefaultContactImage:Landroid/graphics/drawable/Drawable;

    goto :goto_1
.end method


# virtual methods
.method public bind(Lcom/android/mms/ui/WPMessageItem;Z)V
    .locals 2
    .param p1    # Lcom/android/mms/ui/WPMessageItem;
    .param p2    # Z

    iput-object p1, p0, Lcom/android/mms/ui/WPMessageListItem;->mMessageItem:Lcom/android/mms/ui/WPMessageItem;

    iput-boolean p2, p0, Lcom/android/mms/ui/WPMessageListItem;->mIsLastItemInList:Z

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setLongClickable(Z)V

    invoke-direct {p0, p1}, Lcom/android/mms/ui/WPMessageListItem;->bindCommonMessage(Lcom/android/mms/ui/WPMessageItem;)V

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mItemContainer:Landroid/view/View;

    new-instance v1, Lcom/android/mms/ui/WPMessageListItem$1;

    invoke-direct {v1, p0}, Lcom/android/mms/ui/WPMessageListItem$1;-><init>(Lcom/android/mms/ui/WPMessageListItem;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1    # Landroid/graphics/Canvas;

    const/high16 v9, 0x3f800000

    iget-object v6, p0, Lcom/android/mms/ui/WPMessageListItem;->mMessageBlock:Landroid/view/View;

    if-eqz v6, :cond_3

    invoke-virtual {v6}, Landroid/view/View;->getX()F

    move-result v1

    invoke-virtual {v6}, Landroid/view/View;->getY()F

    move-result v5

    invoke-virtual {v6}, Landroid/view/View;->getX()F

    move-result v7

    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    move-result v8

    int-to-float v8, v8

    add-float v4, v7, v8

    invoke-virtual {v6}, Landroid/view/View;->getY()F

    move-result v7

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v8

    int-to-float v8, v8

    add-float v0, v7, v8

    iget-object v3, p0, Lcom/android/mms/ui/WPMessageListItem;->mPath:Landroid/graphics/Path;

    invoke-virtual {v3}, Landroid/graphics/Path;->reset()V

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    invoke-virtual {v3}, Landroid/graphics/Path;->reset()V

    sub-float/2addr v4, v9

    iget-boolean v7, p0, Lcom/android/mms/ui/WPMessageListItem;->mIsLastItemInList:Z

    if-eqz v7, :cond_0

    sub-float/2addr v0, v9

    :cond_0
    iget-object v7, p0, Lcom/android/mms/ui/WPMessageListItem;->mAvatar:Lcom/android/mms/ui/QuickContactDivot;

    invoke-virtual {v7}, Lcom/android/mms/ui/QuickContactDivot;->getPosition()I

    move-result v7

    const/4 v8, 0x4

    if-ne v7, v8, :cond_2

    iget-object v7, p0, Lcom/android/mms/ui/WPMessageListItem;->mAvatar:Lcom/android/mms/ui/QuickContactDivot;

    invoke-virtual {v7}, Lcom/android/mms/ui/QuickContactDivot;->getCloseOffset()F

    move-result v7

    add-float/2addr v7, v5

    invoke-virtual {v3, v1, v7}, Landroid/graphics/Path;->moveTo(FF)V

    invoke-virtual {v3, v1, v5}, Landroid/graphics/Path;->lineTo(FF)V

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    invoke-virtual {v3, v4, v0}, Landroid/graphics/Path;->lineTo(FF)V

    invoke-virtual {v3, v1, v0}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v7, p0, Lcom/android/mms/ui/WPMessageListItem;->mAvatar:Lcom/android/mms/ui/QuickContactDivot;

    invoke-virtual {v7}, Lcom/android/mms/ui/QuickContactDivot;->getFarOffset()F

    move-result v7

    add-float/2addr v7, v5

    invoke-virtual {v3, v1, v7}, Landroid/graphics/Path;->lineTo(FF)V

    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/android/mms/ui/WPMessageListItem;->mPaint:Landroid/graphics/Paint;

    const v7, -0x333334

    invoke-virtual {v2, v7}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {v2, v9}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    sget-object v7, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v7}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-virtual {p1, v3, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    :goto_1
    return-void

    :cond_2
    iget-object v7, p0, Lcom/android/mms/ui/WPMessageListItem;->mAvatar:Lcom/android/mms/ui/QuickContactDivot;

    invoke-virtual {v7}, Lcom/android/mms/ui/QuickContactDivot;->getPosition()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_1

    iget-object v7, p0, Lcom/android/mms/ui/WPMessageListItem;->mAvatar:Lcom/android/mms/ui/QuickContactDivot;

    invoke-virtual {v7}, Lcom/android/mms/ui/QuickContactDivot;->getCloseOffset()F

    move-result v7

    add-float/2addr v7, v5

    invoke-virtual {v3, v4, v7}, Landroid/graphics/Path;->moveTo(FF)V

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    invoke-virtual {v3, v1, v5}, Landroid/graphics/Path;->lineTo(FF)V

    invoke-virtual {v3, v1, v0}, Landroid/graphics/Path;->lineTo(FF)V

    invoke-virtual {v3, v4, v0}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v7, p0, Lcom/android/mms/ui/WPMessageListItem;->mAvatar:Lcom/android/mms/ui/QuickContactDivot;

    invoke-virtual {v7}, Lcom/android/mms/ui/QuickContactDivot;->getFarOffset()F

    move-result v7

    add-float/2addr v7, v5

    invoke-virtual {v3, v4, v7}, Landroid/graphics/Path;->lineTo(FF)V

    goto :goto_0

    :cond_3
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    goto :goto_1
.end method

.method public getItemContainer()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mItemContainer:Landroid/view/View;

    return-object v0
.end method

.method public getMessageItem()Lcom/android/mms/ui/WPMessageItem;
    .locals 1

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mMessageItem:Lcom/android/mms/ui/WPMessageItem;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/view/View;->onFinishInflate()V

    const v0, 0x7f0e00f3

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mItemContainer:Landroid/view/View;

    const v0, 0x7f0e0017

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mMessageBlock:Landroid/view/View;

    const v0, 0x7f0e0016

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/mms/ui/QuickContactDivot;

    iput-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mAvatar:Lcom/android/mms/ui/QuickContactDivot;

    const v0, 0x7f0e0018

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mBodyTextView:Landroid/widget/TextView;

    const v0, 0x7f0e001a

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mTimestamp:Landroid/widget/TextView;

    const v0, 0x7f0e001b

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mLockedIndicator:Landroid/widget/ImageView;

    const v0, 0x7f0e00f4

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mExpirationIndicator:Landroid/widget/ImageView;

    const v0, 0x7f0e001e

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mSimStatus:Landroid/widget/TextView;

    return-void
.end method

.method public onMessageListItemClick()V
    .locals 18

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mms/ui/WPMessageListItem;->mBodyTextView:Landroid/widget/TextView;

    invoke-virtual {v14}, Landroid/widget/TextView;->getUrls()[Landroid/text/style/URLSpan;

    move-result-object v12

    array-length v14, v12

    const/4 v15, 0x1

    if-lt v14, v15, :cond_4

    const/4 v5, 0x0

    move-object v2, v12

    array-length v10, v2

    const/4 v7, 0x0

    :goto_0
    if-ge v7, v10, :cond_1

    aget-object v8, v2, v7

    invoke-virtual {v8}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v14

    const-string v15, "tel:"

    invoke-virtual {v14, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_0

    add-int/lit8 v5, v5, 0x1

    :cond_0
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_1
    move-object v11, v12

    array-length v14, v11

    add-int/2addr v14, v5

    new-array v12, v14, [Landroid/text/style/URLSpan;

    const/4 v6, 0x0

    const/4 v9, 0x0

    move-object v2, v11

    array-length v10, v2

    const/4 v7, 0x0

    :goto_1
    if-ge v7, v10, :cond_3

    aget-object v8, v2, v7

    aget-object v14, v11, v9

    aput-object v14, v12, v6

    invoke-virtual {v8}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v14

    const-string v15, "tel:"

    invoke-virtual {v14, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_2

    add-int/lit8 v6, v6, 0x1

    new-instance v14, Landroid/text/style/URLSpan;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "smsto:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v8}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v16

    const-string v17, "tel:"

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15}, Landroid/text/style/URLSpan;-><init>(Ljava/lang/String;)V

    aput-object v14, v12, v6

    :cond_2
    add-int/lit8 v6, v6, 0x1

    add-int/lit8 v9, v9, 0x1

    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_3
    move-object v13, v12

    new-instance v1, Lcom/android/mms/ui/WPMessageListItem$4;

    move-object/from16 v0, p0

    iget-object v14, v0, Landroid/view/View;->mContext:Landroid/content/Context;

    const v15, 0x1090011

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v14, v15, v13}, Lcom/android/mms/ui/WPMessageListItem$4;-><init>(Lcom/android/mms/ui/WPMessageListItem;Landroid/content/Context;I[Landroid/text/style/URLSpan;)V

    new-instance v3, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    iget-object v14, v0, Landroid/view/View;->mContext:Landroid/content/Context;

    invoke-direct {v3, v14}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    new-instance v4, Lcom/android/mms/ui/WPMessageListItem$5;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v13}, Lcom/android/mms/ui/WPMessageListItem$5;-><init>(Lcom/android/mms/ui/WPMessageListItem;[Landroid/text/style/URLSpan;)V

    const v14, 0x7f0a0214

    invoke-virtual {v3, v14}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const/4 v14, 0x1

    invoke-virtual {v3, v14}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v3, v1, v4}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/high16 v14, 0x1040000

    new-instance v15, Lcom/android/mms/ui/WPMessageListItem$6;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/android/mms/ui/WPMessageListItem$6;-><init>(Lcom/android/mms/ui/WPMessageListItem;)V

    invoke-virtual {v3, v14, v15}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    :cond_4
    return-void
.end method

.method public setMsgListItemHandler(Landroid/os/Handler;)V
    .locals 0
    .param p1    # Landroid/os/Handler;

    iput-object p1, p0, Lcom/android/mms/ui/WPMessageListItem;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method public setTextSize(F)V
    .locals 1
    .param p1    # F

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mBodyTextView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mBodyTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    :cond_0
    return-void
.end method

.method public unbind()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mMessageItem:Lcom/android/mms/ui/WPMessageItem;

    return-void
.end method
