.class public Lcom/android/mms/util/ImageCacheService;
.super Ljava/lang/Object;
.source "ImageCacheService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mms/util/ImageCacheService$ImageData;
    }
.end annotation


# static fields
.field public static final IMAGE_CACHE_FILE:Ljava/lang/String; = "imgcache"

.field private static final IMAGE_CACHE_MAX_BYTES:I = 0x1400000

.field private static final IMAGE_CACHE_MAX_ENTRIES:I = 0x1f4

.field private static final IMAGE_CACHE_VERSION:I = 0x3

.field private static final INITIALCRC:J = -0x1L

.field private static final POLY64REV:J = -0x6a536cd653b4364bL

.field private static final TAG:Ljava/lang/String; = "ImageCacheService"

.field private static sCrcTable:[J


# instance fields
.field private mCache:Lcom/android/mms/util/BlobCache;

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/16 v8, 0x100

    new-array v6, v8, [J

    sput-object v6, Lcom/android/mms/util/ImageCacheService;->sCrcTable:[J

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v8, :cond_2

    int-to-long v2, v0

    const/4 v1, 0x0

    :goto_1
    const/16 v6, 0x8

    if-ge v1, v6, :cond_1

    long-to-int v6, v2

    and-int/lit8 v6, v6, 0x1

    if-eqz v6, :cond_0

    const-wide v4, -0x6a536cd653b4364bL

    :goto_2
    const/4 v6, 0x1

    shr-long v6, v2, v6

    xor-long v2, v6, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    const-wide/16 v4, 0x0

    goto :goto_2

    :cond_1
    sget-object v6, Lcom/android/mms/util/ImageCacheService;->sCrcTable:[J

    aput-wide v2, v6, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "imgcache"

    const/16 v1, 0x1f4

    const/high16 v2, 0x1400000

    const/4 v3, 0x3

    invoke-static {p1, v0, v1, v2, v3}, Lcom/android/mms/util/CacheManager;->getCache(Landroid/content/Context;Ljava/lang/String;III)Lcom/android/mms/util/BlobCache;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/util/ImageCacheService;->mCache:Lcom/android/mms/util/BlobCache;

    iput-object p1, p0, Lcom/android/mms/util/ImageCacheService;->mContext:Landroid/content/Context;

    return-void
.end method

.method public static final crc64Long(Ljava/lang/String;)J
    .locals 2
    .param p0    # Ljava/lang/String;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_1
    invoke-static {p0}, Lcom/android/mms/util/ImageCacheService;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/android/mms/util/ImageCacheService;->crc64Long([B)J

    move-result-wide v0

    goto :goto_0
.end method

.method public static final crc64Long([B)J
    .locals 8
    .param p0    # [B

    const-wide/16 v0, -0x1

    const/4 v2, 0x0

    array-length v3, p0

    :goto_0
    if-ge v2, v3, :cond_0

    sget-object v4, Lcom/android/mms/util/ImageCacheService;->sCrcTable:[J

    long-to-int v5, v0

    aget-byte v6, p0, v2

    xor-int/2addr v5, v6

    and-int/lit16 v5, v5, 0xff

    aget-wide v4, v4, v5

    const/16 v6, 0x8

    shr-long v6, v0, v6

    xor-long v0, v4, v6

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-wide v0
.end method

.method public static getBytes(Ljava/lang/String;)[B
    .locals 8
    .param p0    # Ljava/lang/String;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v7

    mul-int/lit8 v7, v7, 0x2

    new-array v6, v7, [B

    const/4 v4, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v3, v0

    const/4 v2, 0x0

    move v5, v4

    :goto_0
    if-ge v2, v3, :cond_0

    aget-char v1, v0, v2

    add-int/lit8 v4, v5, 0x1

    and-int/lit16 v7, v1, 0xff

    int-to-byte v7, v7

    aput-byte v7, v6, v5

    add-int/lit8 v5, v4, 0x1

    shr-int/lit8 v7, v1, 0x8

    int-to-byte v7, v7

    aput-byte v7, v6, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v6
.end method

.method private static isSameKey([B[B)Z
    .locals 5
    .param p0    # [B
    .param p1    # [B

    const/4 v2, 0x0

    array-length v1, p0

    array-length v3, p1

    if-ge v3, v1, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_2

    aget-byte v3, p0, v0

    aget-byte v4, p1, v0

    if-ne v3, v4, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private static makeKey(Ljava/lang/String;I)[B
    .locals 2
    .param p0    # Ljava/lang/String;
    .param p1    # I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/mms/util/ImageCacheService;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public clear()V
    .locals 1

    iget-object v0, p0, Lcom/android/mms/util/ImageCacheService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/mms/util/CacheManager;->clear(Landroid/content/Context;)V

    return-void
.end method

.method public getImageData(Ljava/lang/String;I)Lcom/android/mms/util/ImageCacheService$ImageData;
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v5, 0x0

    invoke-static {p1, p2}, Lcom/android/mms/util/ImageCacheService;->makeKey(Ljava/lang/String;I)[B

    move-result-object v2

    invoke-static {v2}, Lcom/android/mms/util/ImageCacheService;->crc64Long([B)J

    move-result-wide v0

    const/4 v4, 0x0

    :try_start_0
    iget-object v7, p0, Lcom/android/mms/util/ImageCacheService;->mCache:Lcom/android/mms/util/BlobCache;

    monitor-enter v7
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v6, p0, Lcom/android/mms/util/ImageCacheService;->mCache:Lcom/android/mms/util/BlobCache;

    invoke-virtual {v6, v0, v1}, Lcom/android/mms/util/BlobCache;->lookup(J)[B

    move-result-object v4

    monitor-exit v7

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return-object v5

    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v6

    :catch_0
    move-exception v6

    goto :goto_0

    :cond_1
    invoke-static {v2, v4}, Lcom/android/mms/util/ImageCacheService;->isSameKey([B[B)Z

    move-result v6

    if-eqz v6, :cond_0

    array-length v3, v2

    new-instance v6, Lcom/android/mms/util/ImageCacheService$ImageData;

    invoke-direct {v6, v4, v3}, Lcom/android/mms/util/ImageCacheService$ImageData;-><init>([BI)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v5, v6

    goto :goto_0
.end method

.method public putImageData(Ljava/lang/String;I[B)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # [B

    invoke-static {p1, p2}, Lcom/android/mms/util/ImageCacheService;->makeKey(Ljava/lang/String;I)[B

    move-result-object v3

    invoke-static {v3}, Lcom/android/mms/util/ImageCacheService;->crc64Long([B)J

    move-result-wide v1

    array-length v4, v3

    array-length v5, p3

    add-int/2addr v4, v5

    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p3}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    iget-object v5, p0, Lcom/android/mms/util/ImageCacheService;->mCache:Lcom/android/mms/util/BlobCache;

    monitor-enter v5

    :try_start_0
    iget-object v4, p0, Lcom/android/mms/util/ImageCacheService;->mCache:Lcom/android/mms/util/BlobCache;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v6

    invoke-virtual {v4, v1, v2, v6}, Lcom/android/mms/util/BlobCache;->insert(J[B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    :try_start_1
    monitor-exit v5

    return-void

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    :catch_0
    move-exception v4

    goto :goto_0
.end method
