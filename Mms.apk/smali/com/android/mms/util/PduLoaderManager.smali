.class public Lcom/android/mms/util/PduLoaderManager;
.super Lcom/android/mms/util/BackgroundLoaderManager;
.source "PduLoaderManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mms/util/PduLoaderManager$PduLoaded;,
        Lcom/android/mms/util/PduLoaderManager$PduTask;
    }
.end annotation


# static fields
.field private static final DEBUG_DISABLE_CACHE:Z = false

.field private static final DEBUG_DISABLE_PDUS:Z = false

.field private static final DEBUG_LONG_WAIT:Z = false

.field private static final TAG:Ljava/lang/String; = "Mms:PduLoaderManager"

.field private static mPduCache:Lcom/google/android/mms/util/PduCache;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mPduPersister:Lcom/google/android/mms/pdu/PduPersister;

.field private final mSlideshowCache:Lcom/android/mms/util/SimpleCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/mms/util/SimpleCache",
            "<",
            "Landroid/net/Uri;",
            "Lcom/android/mms/model/SlideshowModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/android/mms/util/BackgroundLoaderManager;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/android/mms/util/SimpleCache;

    const/16 v1, 0x8

    const/16 v2, 0x10

    const/high16 v3, 0x3f400000

    const/4 v4, 0x1

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/mms/util/SimpleCache;-><init>(IIFZ)V

    iput-object v0, p0, Lcom/android/mms/util/PduLoaderManager;->mSlideshowCache:Lcom/android/mms/util/SimpleCache;

    invoke-static {}, Lcom/google/android/mms/util/PduCache;->getInstance()Lcom/google/android/mms/util/PduCache;

    move-result-object v0

    sput-object v0, Lcom/android/mms/util/PduLoaderManager;->mPduCache:Lcom/google/android/mms/util/PduCache;

    invoke-static {p1}, Lcom/google/android/mms/pdu/PduPersister;->getPduPersister(Landroid/content/Context;)Lcom/google/android/mms/pdu/PduPersister;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/util/PduLoaderManager;->mPduPersister:Lcom/google/android/mms/pdu/PduPersister;

    iput-object p1, p0, Lcom/android/mms/util/PduLoaderManager;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$000(Lcom/android/mms/util/PduLoaderManager;)Lcom/google/android/mms/pdu/PduPersister;
    .locals 1
    .param p0    # Lcom/android/mms/util/PduLoaderManager;

    iget-object v0, p0, Lcom/android/mms/util/PduLoaderManager;->mPduPersister:Lcom/google/android/mms/pdu/PduPersister;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/mms/util/PduLoaderManager;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/android/mms/util/PduLoaderManager;

    iget-object v0, p0, Lcom/android/mms/util/PduLoaderManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/mms/util/PduLoaderManager;)Lcom/android/mms/util/SimpleCache;
    .locals 1
    .param p0    # Lcom/android/mms/util/PduLoaderManager;

    iget-object v0, p0, Lcom/android/mms/util/PduLoaderManager;->mSlideshowCache:Lcom/android/mms/util/SimpleCache;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic addCallback(Landroid/net/Uri;Lcom/android/mms/util/ItemLoadedCallback;)Z
    .locals 1
    .param p1    # Landroid/net/Uri;
    .param p2    # Lcom/android/mms/util/ItemLoadedCallback;

    invoke-super {p0, p1, p2}, Lcom/android/mms/util/BackgroundLoaderManager;->addCallback(Landroid/net/Uri;Lcom/android/mms/util/ItemLoadedCallback;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic cancelCallback(Lcom/android/mms/util/ItemLoadedCallback;)V
    .locals 0
    .param p1    # Lcom/android/mms/util/ItemLoadedCallback;

    invoke-super {p0, p1}, Lcom/android/mms/util/BackgroundLoaderManager;->cancelCallback(Lcom/android/mms/util/ItemLoadedCallback;)V

    return-void
.end method

.method public clear()V
    .locals 2

    invoke-super {p0}, Lcom/android/mms/util/BackgroundLoaderManager;->clear()V

    sget-object v1, Lcom/android/mms/util/PduLoaderManager;->mPduCache:Lcom/google/android/mms/util/PduCache;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/mms/util/PduLoaderManager;->mPduCache:Lcom/google/android/mms/util/PduCache;

    invoke-virtual {v0}, Lcom/google/android/mms/util/PduCache;->purgeAll()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/android/mms/util/PduLoaderManager;->mSlideshowCache:Lcom/android/mms/util/SimpleCache;

    invoke-virtual {v0}, Lcom/android/mms/util/SimpleCache;->clear()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getPdu(Landroid/net/Uri;ZLcom/android/mms/util/ItemLoadedCallback;)Lcom/android/mms/util/ItemLoadedFuture;
    .locals 12
    .param p1    # Landroid/net/Uri;
    .param p2    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Z",
            "Lcom/android/mms/util/ItemLoadedCallback",
            "<",
            "Lcom/android/mms/util/PduLoaderManager$PduLoaded;",
            ">;)",
            "Lcom/android/mms/util/ItemLoadedFuture;"
        }
    .end annotation

    if-nez p1, :cond_0

    new-instance v10, Ljava/lang/NullPointerException;

    invoke-direct {v10}, Ljava/lang/NullPointerException;-><init>()V

    throw v10

    :cond_0
    const/4 v1, 0x0

    sget-object v11, Lcom/android/mms/util/PduLoaderManager;->mPduCache:Lcom/google/android/mms/util/PduCache;

    monitor-enter v11

    :try_start_0
    sget-object v10, Lcom/android/mms/util/PduLoaderManager;->mPduCache:Lcom/google/android/mms/util/PduCache;

    invoke-virtual {v10, p1}, Lcom/google/android/mms/util/PduCache;->isUpdating(Landroid/net/Uri;)Z

    move-result v10

    if-nez v10, :cond_1

    sget-object v10, Lcom/android/mms/util/PduLoaderManager;->mPduCache:Lcom/google/android/mms/util/PduCache;

    invoke-virtual {v10, p1}, Lcom/google/android/mms/util/AbstractCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    move-object v0, v10

    check-cast v0, Lcom/google/android/mms/util/PduCacheEntry;

    move-object v1, v0

    :cond_1
    monitor-exit v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p2, :cond_5

    iget-object v10, p0, Lcom/android/mms/util/PduLoaderManager;->mSlideshowCache:Lcom/android/mms/util/SimpleCache;

    invoke-virtual {v10, p1}, Lcom/android/mms/util/SimpleCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/mms/model/SlideshowModel;

    move-object v6, v10

    :goto_0
    if-eqz p2, :cond_2

    if-eqz v6, :cond_6

    :cond_2
    const/4 v7, 0x1

    :goto_1
    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/google/android/mms/util/PduCacheEntry;->getPdu()Lcom/google/android/mms/pdu/GenericPdu;

    move-result-object v10

    if-eqz v10, :cond_7

    const/4 v4, 0x1

    :goto_2
    iget-object v10, p0, Lcom/android/mms/util/BackgroundLoaderManager;->mPendingTaskUris:Ljava/util/Set;

    invoke-interface {v10, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v4, :cond_3

    if-nez v7, :cond_8

    :cond_3
    if-nez v9, :cond_8

    const/4 v3, 0x1

    :goto_3
    if-eqz p3, :cond_9

    const/4 v2, 0x1

    :goto_4
    if-eqz v4, :cond_a

    if-eqz v7, :cond_a

    if-eqz v2, :cond_4

    new-instance v5, Lcom/android/mms/util/PduLoaderManager$PduLoaded;

    invoke-virtual {v1}, Lcom/google/android/mms/util/PduCacheEntry;->getPdu()Lcom/google/android/mms/pdu/GenericPdu;

    move-result-object v10

    invoke-direct {v5, v10, v6}, Lcom/android/mms/util/PduLoaderManager$PduLoaded;-><init>(Lcom/google/android/mms/pdu/GenericPdu;Lcom/android/mms/model/SlideshowModel;)V

    const/4 v10, 0x0

    invoke-interface {p3, v5, v10}, Lcom/android/mms/util/ItemLoadedCallback;->onItemLoaded(Ljava/lang/Object;Ljava/lang/Throwable;)V

    :cond_4
    new-instance v10, Lcom/android/mms/util/NullItemLoadedFuture;

    invoke-direct {v10}, Lcom/android/mms/util/NullItemLoadedFuture;-><init>()V

    :goto_5
    return-object v10

    :catchall_0
    move-exception v10

    :try_start_1
    monitor-exit v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v10

    :cond_5
    const/4 v6, 0x0

    goto :goto_0

    :cond_6
    const/4 v7, 0x0

    goto :goto_1

    :cond_7
    const/4 v4, 0x0

    goto :goto_2

    :cond_8
    const/4 v3, 0x0

    goto :goto_3

    :cond_9
    const/4 v2, 0x0

    goto :goto_4

    :cond_a
    if-eqz v2, :cond_b

    invoke-virtual {p0, p1, p3}, Lcom/android/mms/util/PduLoaderManager;->addCallback(Landroid/net/Uri;Lcom/android/mms/util/ItemLoadedCallback;)Z

    :cond_b
    if-eqz v3, :cond_c

    iget-object v10, p0, Lcom/android/mms/util/BackgroundLoaderManager;->mPendingTaskUris:Ljava/util/Set;

    invoke-interface {v10, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v8, Lcom/android/mms/util/PduLoaderManager$PduTask;

    invoke-direct {v8, p0, p1, p2}, Lcom/android/mms/util/PduLoaderManager$PduTask;-><init>(Lcom/android/mms/util/PduLoaderManager;Landroid/net/Uri;Z)V

    iget-object v10, p0, Lcom/android/mms/util/BackgroundLoaderManager;->mExecutor:Ljava/util/concurrent/Executor;

    invoke-interface {v10, v8}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    :cond_c
    new-instance v10, Lcom/android/mms/util/PduLoaderManager$1;

    invoke-direct {v10, p0, p3}, Lcom/android/mms/util/PduLoaderManager$1;-><init>(Lcom/android/mms/util/PduLoaderManager;Lcom/android/mms/util/ItemLoadedCallback;)V

    goto :goto_5
.end method

.method public getTag()Ljava/lang/String;
    .locals 1

    const-string v0, "Mms:PduLoaderManager"

    return-object v0
.end method

.method public bridge synthetic onLowMemory()V
    .locals 0

    invoke-super {p0}, Lcom/android/mms/util/BackgroundLoaderManager;->onLowMemory()V

    return-void
.end method

.method public removePdu(Landroid/net/Uri;)V
    .locals 3
    .param p1    # Landroid/net/Uri;

    const-string v0, "Mms:PduLoaderManager"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Mms:PduLoaderManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "removePdu: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    sget-object v1, Lcom/android/mms/util/PduLoaderManager;->mPduCache:Lcom/google/android/mms/util/PduCache;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/mms/util/PduLoaderManager;->mPduCache:Lcom/google/android/mms/util/PduCache;

    invoke-virtual {v0, p1}, Lcom/google/android/mms/util/PduCache;->purge(Landroid/net/Uri;)Lcom/google/android/mms/util/PduCacheEntry;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/android/mms/util/PduLoaderManager;->mSlideshowCache:Lcom/android/mms/util/SimpleCache;

    invoke-virtual {v0, p1}, Lcom/android/mms/util/SimpleCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
