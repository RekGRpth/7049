.class public Lcom/android/mms/util/ThreadCountManager;
.super Ljava/lang/Object;
.source "ThreadCountManager.java"


# static fields
.field public static final OP_FLAG_DECREASE:I = 0x1

.field public static final OP_FLAG_INCREASE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "ThreadCountManager"

.field private static final THREAD_MAX_SIZE:I = 0x64

.field private static sInstance:Lcom/android/mms/util/ThreadCountManager;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private get(Ljava/lang/Long;Landroid/content/Context;)I
    .locals 8
    .param p1    # Ljava/lang/Long;
    .param p2    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "thread_id="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v2, v5, [Ljava/lang/String;

    const-string v5, "_id"

    aput-object v5, v2, v0

    const/4 v1, 0x0

    const/4 v5, 0x0

    :try_start_0
    invoke-static {v3, v2, v4, v5}, Landroid/provider/Telephony$Sms;->query(Landroid/content/ContentResolver;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-nez v1, :cond_1

    if-eqz v1, :cond_0

    const-string v5, "ThreadCountManager"

    const-string v6, "close cursor"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    :goto_0
    return v0

    :cond_1
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    const-string v5, "ThreadCountManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "sms count is :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_0

    const-string v5, "ThreadCountManager"

    const-string v6, "close cursor"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v5

    if-eqz v1, :cond_2

    const-string v6, "ThreadCountManager"

    const-string v7, "close cursor"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v5
.end method

.method public static declared-synchronized getInstance()Lcom/android/mms/util/ThreadCountManager;
    .locals 2

    const-class v1, Lcom/android/mms/util/ThreadCountManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/mms/util/ThreadCountManager;->sInstance:Lcom/android/mms/util/ThreadCountManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/mms/util/ThreadCountManager;

    invoke-direct {v0}, Lcom/android/mms/util/ThreadCountManager;-><init>()V

    sput-object v0, Lcom/android/mms/util/ThreadCountManager;->sInstance:Lcom/android/mms/util/ThreadCountManager;

    :cond_0
    sget-object v0, Lcom/android/mms/util/ThreadCountManager;->sInstance:Lcom/android/mms/util/ThreadCountManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public isFull(Ljava/lang/Long;Landroid/content/Context;I)V
    .locals 6
    .param p1    # Ljava/lang/Long;
    .param p2    # Landroid/content/Context;
    .param p3    # I

    const/16 v5, 0x64

    const/4 v4, 0x1

    :try_start_0
    const-string v2, "phone"

    invoke-static {v2}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-interface {v1}, Lcom/android/internal/telephony/ITelephony;->isTestIccCard()Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "gsm.gcf.testmode"

    const-string v3, "0"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    :cond_0
    const-string v2, "ThreadCountManager"

    const-string v3, "Now using test icc card..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p3, :cond_2

    invoke-direct {p0, p1, p2}, Lcom/android/mms/util/ThreadCountManager;->get(Ljava/lang/Long;Landroid/content/Context;)I

    move-result v2

    if-lt v2, v5, :cond_1

    const-string v2, "ThreadCountManager"

    const-string v3, "Storage is full. send notification..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/telephony/SmsManager;->setSmsMemoryStatus(Z)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    if-ne p3, v4, :cond_1

    invoke-direct {p0, p1, p2}, Lcom/android/mms/util/ThreadCountManager;->get(Ljava/lang/Long;Landroid/content/Context;)I

    move-result v2

    if-ge v2, v5, :cond_1

    const-string v2, "ThreadCountManager"

    const-string v3, "Storage is available. send notification..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/telephony/SmsManager;->setSmsMemoryStatus(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "ThreadCountManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    :try_start_1
    const-string v2, "ThreadCountManager"

    const-string v3, "Telephony service is not available!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
