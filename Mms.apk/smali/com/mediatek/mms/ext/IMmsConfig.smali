.class public interface abstract Lcom/mediatek/mms/ext/IMmsConfig;
.super Ljava/lang/Object;
.source "IMmsConfig.java"


# virtual methods
.method public abstract getCapturePictureIntent()Landroid/content/Intent;
.end method

.method public abstract getHttpSocketTimeout()I
.end method

.method public abstract getMaxTextLimit()I
.end method

.method public abstract getMmsRecipientLimit()I
.end method

.method public abstract getSmsToMmsTextThreshold()I
.end method

.method public abstract getUAProf(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract isEnableAdjustFontSize()Z
.end method

.method public abstract isEnableDialogForUrl()Z
.end method

.method public abstract isEnableFolderMode()Z
.end method

.method public abstract isEnableForwardWithSender()Z
.end method

.method public abstract isEnableMultiSmsSaveLocation()Z
.end method

.method public abstract isEnableReportAllowed()Z
.end method

.method public abstract isEnableSIMLongSmsConcatenate()Z
.end method

.method public abstract isEnableSIMSmsForSetting()Z
.end method

.method public abstract isEnableSmsEncodingType()Z
.end method

.method public abstract isEnableSmsValidityPeriod()Z
.end method

.method public abstract isEnableStorageFullToast()Z
.end method

.method public abstract isEnableStorageStatusDisp()Z
.end method

.method public abstract isShowUrlDialog()Z
.end method

.method public abstract setHttpSocketTimeout(I)V
.end method

.method public abstract setMaxTextLimit(I)V
.end method

.method public abstract setMmsRecipientLimit(I)V
.end method

.method public abstract setSmsToMmsTextThreshold(I)V
.end method
