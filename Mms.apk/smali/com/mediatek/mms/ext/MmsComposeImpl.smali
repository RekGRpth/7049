.class public Lcom/mediatek/mms/ext/MmsComposeImpl;
.super Landroid/content/ContextWrapper;
.source "MmsComposeImpl.java"

# interfaces
.implements Lcom/mediatek/mms/ext/IMmsCompose;


# static fields
.field private static final TAG:Ljava/lang/String; = "Mms/MmsComposeImpl"


# instance fields
.field private mHost:Lcom/mediatek/mms/ext/IMmsComposeHost;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/mms/ext/MmsComposeImpl;->mHost:Lcom/mediatek/mms/ext/IMmsComposeHost;

    return-void
.end method


# virtual methods
.method public addContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;Ljava/lang/CharSequence;)V
    .locals 0
    .param p1    # Landroid/view/ContextMenu;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ContextMenu$ContextMenuInfo;
    .param p4    # Ljava/lang/CharSequence;

    return-void
.end method

.method public configSubjectEditor(Landroid/widget/EditText;)V
    .locals 0
    .param p1    # Landroid/widget/EditText;

    return-void
.end method

.method public getCapturePicMode()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected getHost()Lcom/mediatek/mms/ext/IMmsComposeHost;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/mms/ext/MmsComposeImpl;->mHost:Lcom/mediatek/mms/ext/IMmsComposeHost;

    return-object v0
.end method

.method public init(Lcom/mediatek/mms/ext/IMmsComposeHost;)V
    .locals 0
    .param p1    # Lcom/mediatek/mms/ext/IMmsComposeHost;

    iput-object p1, p0, Lcom/mediatek/mms/ext/MmsComposeImpl;->mHost:Lcom/mediatek/mms/ext/IMmsComposeHost;

    return-void
.end method

.method public isAddMmsUrlToBookMark()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isAppendSender()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
