.class public Lcom/mediatek/mms/ext/MmsConversationImpl;
.super Landroid/content/ContextWrapper;
.source "MmsConversationImpl.java"

# interfaces
.implements Lcom/mediatek/mms/ext/IMmsConversation;


# static fields
.field private static final TAG:Ljava/lang/String; = "Mms/MmsConversationImpl"


# instance fields
.field private mHost:Lcom/mediatek/mms/ext/IMmsConversationHost;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/mms/ext/MmsConversationImpl;->mHost:Lcom/mediatek/mms/ext/IMmsConversationHost;

    return-void
.end method


# virtual methods
.method public addOptionMenu(Landroid/view/Menu;I)V
    .locals 0
    .param p1    # Landroid/view/Menu;
    .param p2    # I

    return-void
.end method

.method protected getHost()Lcom/mediatek/mms/ext/IMmsConversationHost;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/mms/ext/MmsConversationImpl;->mHost:Lcom/mediatek/mms/ext/IMmsConversationHost;

    return-object v0
.end method

.method public init(Lcom/mediatek/mms/ext/IMmsConversationHost;)V
    .locals 0
    .param p1    # Lcom/mediatek/mms/ext/IMmsConversationHost;

    iput-object p1, p0, Lcom/mediatek/mms/ext/MmsConversationImpl;->mHost:Lcom/mediatek/mms/ext/IMmsConversationHost;

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    const/4 v0, 0x0

    return v0
.end method
