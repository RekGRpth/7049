.class public Lcom/mediatek/mms/ext/MmsPlayerImpl;
.super Landroid/content/ContextWrapper;
.source "MmsPlayerImpl.java"

# interfaces
.implements Lcom/mediatek/mms/ext/IMmsPlayer;


# static fields
.field private static final TAG:Ljava/lang/String; = "Mms/MmsPlayerImpl"


# instance fields
.field private mHost:Lcom/mediatek/mms/ext/IMmsPlayerHost;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/mms/ext/MmsPlayerImpl;->mHost:Lcom/mediatek/mms/ext/IMmsPlayerHost;

    return-void
.end method


# virtual methods
.method public addContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;Ljava/lang/CharSequence;)V
    .locals 0
    .param p1    # Landroid/view/ContextMenu;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ContextMenu$ContextMenuInfo;
    .param p4    # Ljava/lang/CharSequence;

    return-void
.end method

.method protected getHost()Lcom/mediatek/mms/ext/IMmsPlayerHost;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/mms/ext/MmsPlayerImpl;->mHost:Lcom/mediatek/mms/ext/IMmsPlayerHost;

    return-object v0
.end method

.method public init(Lcom/mediatek/mms/ext/IMmsPlayerHost;)V
    .locals 0
    .param p1    # Lcom/mediatek/mms/ext/IMmsPlayerHost;

    iput-object p1, p0, Lcom/mediatek/mms/ext/MmsPlayerImpl;->mHost:Lcom/mediatek/mms/ext/IMmsPlayerHost;

    return-void
.end method
