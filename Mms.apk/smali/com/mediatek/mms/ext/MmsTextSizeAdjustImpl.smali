.class public Lcom/mediatek/mms/ext/MmsTextSizeAdjustImpl;
.super Landroid/content/ContextWrapper;
.source "MmsTextSizeAdjustImpl.java"

# interfaces
.implements Lcom/mediatek/mms/ext/IMmsTextSizeAdjust;


# instance fields
.field private mHost:Lcom/mediatek/mms/ext/IMmsTextSizeAdjustHost;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/mms/ext/MmsTextSizeAdjustImpl;->mHost:Lcom/mediatek/mms/ext/IMmsTextSizeAdjustHost;

    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x0

    return v0
.end method

.method public getHost()Lcom/mediatek/mms/ext/IMmsTextSizeAdjustHost;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/mms/ext/MmsTextSizeAdjustImpl;->mHost:Lcom/mediatek/mms/ext/IMmsTextSizeAdjustHost;

    return-object v0
.end method

.method public init(Lcom/mediatek/mms/ext/IMmsTextSizeAdjustHost;Landroid/app/Activity;)V
    .locals 0
    .param p1    # Lcom/mediatek/mms/ext/IMmsTextSizeAdjustHost;
    .param p2    # Landroid/app/Activity;

    iput-object p1, p0, Lcom/mediatek/mms/ext/MmsTextSizeAdjustImpl;->mHost:Lcom/mediatek/mms/ext/IMmsTextSizeAdjustHost;

    return-void
.end method

.method public refresh()V
    .locals 0

    return-void
.end method
