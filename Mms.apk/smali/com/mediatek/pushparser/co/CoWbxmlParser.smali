.class public Lcom/mediatek/pushparser/co/CoWbxmlParser;
.super Lcom/mediatek/pushparser/Parser;
.source "CoWbxmlParser.java"


# static fields
.field public static final ATTR_START_TABLE:[Ljava/lang/String;

.field public static final ATTR_VALUE_TABLE:[Ljava/lang/String;

.field private static TAG:Ljava/lang/String;

.field public static final TAG_TABLE:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    const-string v0, "PUSH"

    sput-object v0, Lcom/mediatek/pushparser/co/CoWbxmlParser;->TAG:Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "co"

    aput-object v1, v0, v2

    const-string v1, "invalidate-object"

    aput-object v1, v0, v3

    const-string v1, "invalidate-service"

    aput-object v1, v0, v4

    sput-object v0, Lcom/mediatek/pushparser/co/CoWbxmlParser;->TAG_TABLE:[Ljava/lang/String;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "uri"

    aput-object v1, v0, v2

    const-string v1, "uri=http://"

    aput-object v1, v0, v3

    const-string v1, "uri=http://www."

    aput-object v1, v0, v4

    const-string v1, "uri=https://"

    aput-object v1, v0, v5

    const-string v1, "uri=https://www."

    aput-object v1, v0, v6

    sput-object v0, Lcom/mediatek/pushparser/co/CoWbxmlParser;->ATTR_START_TABLE:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, ".com/"

    aput-object v1, v0, v2

    const-string v1, ".edu/"

    aput-object v1, v0, v3

    const-string v1, ".net/"

    aput-object v1, v0, v4

    const-string v1, ".org/"

    aput-object v1, v0, v5

    sput-object v0, Lcom/mediatek/pushparser/co/CoWbxmlParser;->ATTR_VALUE_TABLE:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/pushparser/Parser;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public parse(Ljava/io/InputStream;)Lcom/mediatek/pushparser/ParsedMessage;
    .locals 10
    .param p1    # Ljava/io/InputStream;

    const/4 v0, 0x0

    :try_start_0
    new-instance v5, Lorg/kxml2/wap/WbxmlParser;

    invoke-direct {v5}, Lorg/kxml2/wap/WbxmlParser;-><init>()V

    const/4 v7, 0x0

    sget-object v8, Lcom/mediatek/pushparser/co/CoWbxmlParser;->TAG_TABLE:[Ljava/lang/String;

    invoke-virtual {v5, v7, v8}, Lorg/kxml2/wap/WbxmlParser;->setTagTable(I[Ljava/lang/String;)V

    const/4 v7, 0x0

    sget-object v8, Lcom/mediatek/pushparser/co/CoWbxmlParser;->ATTR_START_TABLE:[Ljava/lang/String;

    invoke-virtual {v5, v7, v8}, Lorg/kxml2/wap/WbxmlParser;->setAttrStartTable(I[Ljava/lang/String;)V

    const/4 v7, 0x0

    sget-object v8, Lcom/mediatek/pushparser/co/CoWbxmlParser;->ATTR_VALUE_TABLE:[Ljava/lang/String;

    invoke-virtual {v5, v7, v8}, Lorg/kxml2/wap/WbxmlParser;->setAttrValueTable(I[Ljava/lang/String;)V

    const/4 v7, 0x0

    invoke-virtual {v5, p1, v7}, Lorg/kxml2/wap/WbxmlParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-virtual {v5}, Lorg/kxml2/wap/WbxmlParser;->getEventType()I

    move-result v3

    move-object v1, v0

    :goto_0
    const/4 v7, 0x1

    if-eq v3, v7, :cond_3

    const/4 v4, 0x0

    const/4 v6, 0x0

    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    move-object v0, v1

    :cond_1
    :goto_2
    invoke-virtual {v5}, Lorg/kxml2/wap/WbxmlParser;->next()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    move-object v1, v0

    goto :goto_0

    :pswitch_1
    move-object v0, v1

    goto :goto_2

    :pswitch_2
    :try_start_1
    invoke-virtual {v5}, Lorg/kxml2/wap/WbxmlParser;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5}, Lorg/kxml2/wap/WbxmlParser;->getNamespace()Ljava/lang/String;

    move-result-object v6

    const-string v7, "co"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    new-instance v0, Lcom/mediatek/pushparser/co/CoMessage;

    sget-object v7, Lcom/mediatek/pushparser/co/CoMessage;->TYPE:Ljava/lang/String;

    invoke-direct {v0, v7}, Lcom/mediatek/pushparser/co/CoMessage;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :try_start_2
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, v0, Lcom/mediatek/pushparser/co/CoMessage;->objects:Ljava/util/ArrayList;

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, v0, Lcom/mediatek/pushparser/co/CoMessage;->services:Ljava/util/ArrayList;

    :goto_3
    const-string v7, "invalidate-object"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    if-eqz v0, :cond_1

    iget-object v7, v0, Lcom/mediatek/pushparser/co/CoMessage;->objects:Ljava/util/ArrayList;

    const-string v8, "uri"

    invoke-virtual {v5, v6, v8}, Lorg/kxml2/wap/WbxmlParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    :catch_0
    move-exception v2

    :goto_4
    sget-object v7, Lcom/mediatek/pushparser/co/CoWbxmlParser;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Parser Error:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_5
    return-object v0

    :cond_2
    :try_start_3
    const-string v7, "invalidate-service"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    if-eqz v0, :cond_1

    iget-object v7, v0, Lcom/mediatek/pushparser/co/CoMessage;->services:Ljava/util/ArrayList;

    const-string v8, "uri"

    invoke-virtual {v5, v6, v8}, Lorg/kxml2/wap/WbxmlParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_2

    :pswitch_3
    :try_start_4
    invoke-virtual {v5}, Lorg/kxml2/wap/WbxmlParser;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v7, "co"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    move-result v7

    if-eqz v7, :cond_0

    goto/16 :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_5

    :catch_1
    move-exception v2

    move-object v0, v1

    goto :goto_4

    :cond_4
    move-object v0, v1

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
