.class public Lcom/mediatek/pushparser/si/SiTextParser;
.super Lcom/mediatek/pushparser/Parser;
.source "SiTextParser.java"


# static fields
.field public static final INDICATION:Ljava/lang/String; = "indication"

.field public static final INFO:Ljava/lang/String; = "info"

.field public static final SI:Ljava/lang/String; = "si"

.field private static TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "PUSH"

    sput-object v0, Lcom/mediatek/pushparser/si/SiTextParser;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/pushparser/Parser;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public parse(Ljava/io/InputStream;)Lcom/mediatek/pushparser/ParsedMessage;
    .locals 12
    .param p1    # Ljava/io/InputStream;

    const/4 v8, 0x0

    const/4 v10, 0x1

    const/4 v5, 0x0

    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v4

    const/4 v9, 0x0

    :try_start_0
    invoke-interface {v4, p1, v9}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v2

    move-object v6, v5

    :goto_0
    if-eq v2, v10, :cond_8

    const/4 v3, 0x0

    const/4 v7, 0x0

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    move-object v5, v6

    :goto_2
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->next()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    move-object v6, v5

    goto :goto_0

    :pswitch_1
    move-object v5, v6

    goto :goto_2

    :pswitch_2
    :try_start_1
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v9, "si"

    invoke-virtual {v9, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    new-instance v5, Lcom/mediatek/pushparser/si/SiMessage;

    sget-object v9, Lcom/mediatek/pushparser/si/SiMessage;->TYPE:Ljava/lang/String;

    invoke-direct {v5, v9}, Lcom/mediatek/pushparser/si/SiMessage;-><init>(Ljava/lang/String;)V

    goto :goto_2

    :cond_1
    const-string v9, "indication"

    invoke-virtual {v9, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getNamespace()Ljava/lang/String;

    move-result-object v7

    if-eqz v6, :cond_0

    const-string v9, "si-id"

    invoke-interface {v4, v7, v9}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v6, Lcom/mediatek/pushparser/si/SiMessage;->siid:Ljava/lang/String;

    const-string v9, "href"

    invoke-interface {v4, v7, v9}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v6, Lcom/mediatek/pushparser/si/SiMessage;->url:Ljava/lang/String;

    const-string v9, "created"

    invoke-interface {v4, v7, v9}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/mediatek/pushparser/si/SiDateDecoderUtil;->XmlDateDecoder(Ljava/lang/String;)I

    move-result v9

    iput v9, v6, Lcom/mediatek/pushparser/si/SiMessage;->create:I

    const-string v9, "si-expires"

    invoke-interface {v4, v7, v9}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/mediatek/pushparser/si/SiDateDecoderUtil;->XmlDateDecoder(Ljava/lang/String;)I

    move-result v9

    iput v9, v6, Lcom/mediatek/pushparser/si/SiMessage;->expiration:I

    const-string v9, "action"

    invoke-interface {v4, v7, v9}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v9

    iput-object v9, v6, Lcom/mediatek/pushparser/si/SiMessage;->text:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    :cond_2
    const/4 v9, 0x2

    iput v9, v6, Lcom/mediatek/pushparser/si/SiMessage;->action:I

    const-string v9, "signal-none"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    const/4 v9, 0x0

    iput v9, v6, Lcom/mediatek/pushparser/si/SiMessage;->action:I

    :cond_3
    :goto_3
    move-object v5, v6

    goto :goto_2

    :cond_4
    const-string v9, "signal-low"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    const/4 v9, 0x1

    iput v9, v6, Lcom/mediatek/pushparser/si/SiMessage;->action:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    :catch_0
    move-exception v1

    move-object v5, v6

    :goto_4
    sget-object v9, Lcom/mediatek/pushparser/si/SiTextParser;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Parser Error:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v6, v8

    :goto_5
    return-object v6

    :cond_5
    :try_start_2
    const-string v9, "signal-medium"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    const/4 v9, 0x2

    iput v9, v6, Lcom/mediatek/pushparser/si/SiMessage;->action:I

    goto :goto_3

    :cond_6
    const-string v9, "signal-high"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    const/4 v9, 0x3

    iput v9, v6, Lcom/mediatek/pushparser/si/SiMessage;->action:I

    goto :goto_3

    :cond_7
    const-string v9, "delete"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    const/4 v9, 0x4

    iput v9, v6, Lcom/mediatek/pushparser/si/SiMessage;->action:I

    goto :goto_3

    :pswitch_3
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v9, "indication"

    invoke-virtual {v9, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result v9

    if-eqz v9, :cond_0

    goto/16 :goto_1

    :cond_8
    move-object v5, v6

    goto :goto_5

    :catch_1
    move-exception v1

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
