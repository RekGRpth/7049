.class public Lcom/mediatek/wappush/SiManager;
.super Lcom/mediatek/wappush/WapPushManager;
.source "SiManager.java"


# static fields
.field public static final TAG:Ljava/lang/String; = "Mms/WapPush"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/mediatek/wappush/WapPushManager;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public handleIncoming(Lcom/mediatek/pushparser/ParsedMessage;)V
    .locals 29
    .param p1    # Lcom/mediatek/pushparser/ParsedMessage;

    if-nez p1, :cond_1

    const-string v3, "Mms/WapPush"

    const-string v7, "SiManager handleIncoming: null message"

    invoke-static {v3, v7}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object/from16 v18, p1

    check-cast v18, Lcom/mediatek/pushparser/si/SiMessage;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/wappush/WapPushManager;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v12, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v25

    const-wide/16 v27, 0x3e8

    div-long v25, v25, v27

    move-wide/from16 v0, v25

    long-to-int v11, v0

    move-object/from16 v0, v18

    iget v3, v0, Lcom/mediatek/pushparser/si/SiMessage;->create:I

    if-nez v3, :cond_2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v25

    const-wide/16 v27, 0x3e8

    div-long v25, v25, v27

    move-wide/from16 v0, v25

    long-to-int v3, v0

    move-object/from16 v0, v18

    iput v3, v0, Lcom/mediatek/pushparser/si/SiMessage;->create:I

    :cond_2
    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/mediatek/pushparser/si/SiMessage;->siid:Ljava/lang/String;

    if-nez v3, :cond_3

    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/mediatek/pushparser/si/SiMessage;->url:Ljava/lang/String;

    move-object/from16 v0, v18

    iput-object v3, v0, Lcom/mediatek/pushparser/si/SiMessage;->siid:Ljava/lang/String;

    :cond_3
    move-object/from16 v0, v18

    iget v3, v0, Lcom/mediatek/pushparser/si/SiMessage;->expiration:I

    if-lez v3, :cond_4

    move-object/from16 v0, v18

    iget v3, v0, Lcom/mediatek/pushparser/si/SiMessage;->expiration:I

    if-ge v3, v11, :cond_4

    const-string v3, "Mms/WapPush"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "SiManager:Expired Message! "

    move-object/from16 v0, v25

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/mediatek/pushparser/si/SiMessage;->url:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    move-object/from16 v0, v18

    iget v3, v0, Lcom/mediatek/pushparser/si/SiMessage;->action:I

    if-nez v3, :cond_5

    const-string v3, "Mms/WapPush"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "SiManager:Discard None Message! "

    move-object/from16 v0, v25

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/mediatek/pushparser/si/SiMessage;->url:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_5
    const/4 v3, 0x6

    new-array v4, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v7, "_id"

    aput-object v7, v4, v3

    const/4 v3, 0x1

    const-string v7, "siid"

    aput-object v7, v4, v3

    const/4 v3, 0x2

    const-string v7, "url"

    aput-object v7, v4, v3

    const/4 v3, 0x3

    const-string v7, "created"

    aput-object v7, v4, v3

    const/4 v3, 0x4

    const-string v7, "address"

    aput-object v7, v4, v3

    const/4 v3, 0x5

    const-string v7, "text"

    aput-object v7, v4, v3

    const-string v5, "siid=?"

    const/4 v3, 0x1

    new-array v6, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    move-object/from16 v0, v18

    iget-object v7, v0, Lcom/mediatek/pushparser/si/SiMessage;->siid:Ljava/lang/String;

    aput-object v7, v6, v3

    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/mediatek/pushparser/si/SiMessage;->siid:Ljava/lang/String;

    if-eqz v3, :cond_6

    sget-object v3, Landroid/provider/Telephony$WapPush;->CONTENT_URI_SI:Landroid/net/Uri;

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    :cond_6
    const/4 v15, 0x0

    const-wide/16 v16, 0x0

    if-eqz v12, :cond_b

    :try_start_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_a

    :cond_7
    const/4 v3, 0x0

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v13

    const/4 v3, 0x1

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    const/4 v3, 0x3

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    const/4 v3, 0x4

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v3, 0x5

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/mediatek/pushparser/si/SiMessage;->siid:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    move-object/from16 v0, v18

    iget v3, v0, Lcom/mediatek/pushparser/si/SiMessage;->create:I

    if-lez v3, :cond_8

    move-object/from16 v0, v18

    iget v3, v0, Lcom/mediatek/pushparser/si/SiMessage;->create:I

    if-ge v3, v10, :cond_8

    const-string v3, "Mms/WapPush"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "SiManager:Out of order Message! "

    move-object/from16 v0, v25

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/mediatek/pushparser/si/SiMessage;->url:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    :cond_8
    :try_start_1
    move-object/from16 v0, v18

    iget v3, v0, Lcom/mediatek/pushparser/si/SiMessage;->create:I

    if-lt v3, v10, :cond_9

    const/4 v15, 0x1

    move-wide/from16 v16, v13

    :cond_9
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    if-nez v3, :cond_7

    :cond_a
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_b
    move-object/from16 v0, v18

    iget v3, v0, Lcom/mediatek/pushparser/si/SiMessage;->action:I

    const/4 v7, 0x4

    if-ne v3, v7, :cond_c

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/wappush/WapPushManager;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v7, Landroid/provider/Telephony$WapPush;->CONTENT_URI:Landroid/net/Uri;

    move-wide/from16 v0, v16

    invoke-static {v7, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v7

    const/16 v25, 0x0

    const/16 v26, 0x0

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-virtual {v3, v7, v0, v1}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/wappush/WapPushManager;->mContext:Landroid/content/Context;

    const-wide/16 v25, -0x2

    move-wide/from16 v0, v25

    invoke-static {v3, v0, v1}, Lcom/android/mms/transaction/WapPushMessagingNotification;->blockingUpdateNewMessageIndicator(Landroid/content/Context;J)V

    const-string v3, "Mms/WapPush"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "SiManager:Discard delete Message! "

    move-object/from16 v0, v25

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/mediatek/pushparser/si/SiMessage;->url:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :catchall_0
    move-exception v3

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    throw v3

    :cond_c
    new-instance v24, Landroid/content/ContentValues;

    invoke-direct/range {v24 .. v24}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "address"

    invoke-virtual/range {v18 .. v18}, Lcom/mediatek/pushparser/ParsedMessage;->getSenderAddr()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "service_center"

    invoke-virtual/range {v18 .. v18}, Lcom/mediatek/pushparser/ParsedMessage;->getServiceCenterAddr()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "sim_id"

    invoke-virtual/range {v18 .. v18}, Lcom/mediatek/pushparser/ParsedMessage;->getSimId()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "url"

    move-object/from16 v0, v18

    iget-object v7, v0, Lcom/mediatek/pushparser/si/SiMessage;->url:Ljava/lang/String;

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "siid"

    move-object/from16 v0, v18

    iget-object v7, v0, Lcom/mediatek/pushparser/si/SiMessage;->siid:Ljava/lang/String;

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "action"

    move-object/from16 v0, v18

    iget v7, v0, Lcom/mediatek/pushparser/si/SiMessage;->action:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "created"

    move-object/from16 v0, v18

    iget v7, v0, Lcom/mediatek/pushparser/si/SiMessage;->create:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "expiration"

    move-object/from16 v0, v18

    iget v7, v0, Lcom/mediatek/pushparser/si/SiMessage;->expiration:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "text"

    move-object/from16 v0, v18

    iget-object v7, v0, Lcom/mediatek/pushparser/si/SiMessage;->text:Ljava/lang/String;

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v23, 0x0

    if-nez v15, :cond_d

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/wappush/WapPushManager;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v7, Landroid/provider/Telephony$WapPush;->CONTENT_URI_SI:Landroid/net/Uri;

    move-object/from16 v0, v24

    invoke-virtual {v3, v7, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v23

    :goto_1
    const-string v3, "Mms/WapPush"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "uri:"

    move-object/from16 v0, v25

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {v23 .. v23}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v23, :cond_0

    const-string v3, "Mms/WapPush"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "SiManager:Store msg! "

    move-object/from16 v0, v25

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/mediatek/pushparser/si/SiMessage;->url:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/wappush/WapPushManager;->mContext:Landroid/content/Context;

    move-object/from16 v0, v23

    invoke-static {v3, v0}, Lcom/android/mms/transaction/WapPushMessagingNotification;->getWapPushThreadId(Landroid/content/Context;Landroid/net/Uri;)J

    move-result-wide v21

    const-string v3, "Mms/WapPush"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "insert new Si message, threaId:"

    move-object/from16 v0, v25

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-wide/from16 v0, v21

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/wappush/WapPushManager;->mContext:Landroid/content/Context;

    move-wide/from16 v0, v21

    invoke-static {v3, v0, v1}, Lcom/android/mms/transaction/WapPushMessagingNotification;->blockingUpdateNewMessageIndicator(Landroid/content/Context;J)V

    goto/16 :goto_0

    :cond_d
    const-string v3, "read"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "seen"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/wappush/WapPushManager;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v7, Landroid/provider/Telephony$WapPush;->CONTENT_URI_SI:Landroid/net/Uri;

    move-object/from16 v0, v24

    invoke-virtual {v3, v7, v0, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v9

    sget-object v3, Landroid/provider/Telephony$WapPush;->CONTENT_URI:Landroid/net/Uri;

    move-wide/from16 v0, v16

    invoke-static {v3, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v23

    const-string v3, "Mms/WapPush"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "update a si message.count:"

    move-object/from16 v0, v25

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method
