.class Lcom/mediatek/mobilelog/AndroidLogFilterList$1;
.super Ljava/lang/Object;
.source "AndroidLogFilterList.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/mobilelog/AndroidLogFilterList;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/mobilelog/AndroidLogFilterList;


# direct methods
.method constructor <init>(Lcom/mediatek/mobilelog/AndroidLogFilterList;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/mobilelog/AndroidLogFilterList$1;->this$0:Lcom/mediatek/mobilelog/AndroidLogFilterList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v2, p0, Lcom/mediatek/mobilelog/AndroidLogFilterList$1;->this$0:Lcom/mediatek/mobilelog/AndroidLogFilterList;

    const-class v3, Lcom/mediatek/mobilelog/FilterEdit;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "Module Name"

    const-string v3, "Module Name"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "Module Level"

    const-string v3, "verbose"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/mediatek/mobilelog/AndroidLogFilterList$1;->this$0:Lcom/mediatek/mobilelog/AndroidLogFilterList;

    invoke-virtual {v2, v1}, Lcom/mediatek/mobilelog/AndroidLogFilterList;->startActivity(Landroid/content/Intent;)V

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method
