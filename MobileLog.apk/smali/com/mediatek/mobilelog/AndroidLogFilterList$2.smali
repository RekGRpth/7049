.class Lcom/mediatek/mobilelog/AndroidLogFilterList$2;
.super Ljava/lang/Object;
.source "AndroidLogFilterList.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/mobilelog/AndroidLogFilterList;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/mobilelog/AndroidLogFilterList;


# direct methods
.method constructor <init>(Lcom/mediatek/mobilelog/AndroidLogFilterList;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/mobilelog/AndroidLogFilterList$2;->this$0:Lcom/mediatek/mobilelog/AndroidLogFilterList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v3, p0, Lcom/mediatek/mobilelog/AndroidLogFilterList$2;->this$0:Lcom/mediatek/mobilelog/AndroidLogFilterList;

    const-class v4, Lcom/mediatek/mobilelog/FilterEdit;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v3, p0, Lcom/mediatek/mobilelog/AndroidLogFilterList$2;->this$0:Lcom/mediatek/mobilelog/AndroidLogFilterList;

    iget-object v3, v3, Lcom/mediatek/mobilelog/AndroidLogFilterList;->filterListView:Landroid/widget/ListView;

    invoke-virtual {v3, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/HashMap;

    const-string v4, "Module Name"

    const-string v3, "ItemTitle"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "Module Level"

    const-string v3, "ItemText"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    iget-object v3, p0, Lcom/mediatek/mobilelog/AndroidLogFilterList$2;->this$0:Lcom/mediatek/mobilelog/AndroidLogFilterList;

    invoke-virtual {v3, v1}, Lcom/mediatek/mobilelog/AndroidLogFilterList;->startActivity(Landroid/content/Intent;)V

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method
