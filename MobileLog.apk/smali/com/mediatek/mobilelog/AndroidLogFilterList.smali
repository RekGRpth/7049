.class public Lcom/mediatek/mobilelog/AndroidLogFilterList;
.super Landroid/app/Activity;
.source "AndroidLogFilterList.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MobileLog"

.field private static final intentExtraKey:Ljava/lang/String; = "AddOrEdit"

.field private static final intentExtraValueAdd:Ljava/lang/String; = "Add"

.field private static final intentExtraValueEdit:Ljava/lang/String; = "Edit"


# instance fields
.field filterListView:Landroid/widget/ListView;

.field private mLogSettings:Lcom/mediatek/mobilelog/LogSettings;

.field private mLogSettingsPath:Ljava/lang/String;

.field newFilterView:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const-string v0, "/data/data/com.mediatek.mobilelog/files"

    iput-object v0, p0, Lcom/mediatek/mobilelog/AndroidLogFilterList;->mLogSettingsPath:Ljava/lang/String;

    return-void
.end method

.method private levelIsValid(Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    const-string v1, "verbose"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v1, "debug"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "info"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "warn"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "error"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "assert"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateUi()V
    .locals 20

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    new-instance v16, Ljava/util/HashMap;

    invoke-direct/range {v16 .. v16}, Ljava/util/HashMap;-><init>()V

    const-string v2, "ItemTitle"

    const-string v5, "Add New Filter"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "ItemText"

    const-string v5, "Click to add"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v18, 0x0

    const-string v2, "xlog.tag.(\\S+)"

    invoke-static {v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v17

    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/mobilelog/AndroidLogFilterList;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;

    invoke-virtual {v2}, Lcom/mediatek/mobilelog/LogSettings;->loadConfig()Ljava/util/Properties;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/util/Properties;->propertyNames()Ljava/util/Enumeration;

    move-result-object v15

    const-string v2, "MobileLog"

    const-string v5, "show prop"

    invoke-static {v2, v5}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    invoke-interface {v15}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v15}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    const-string v2, "MobileLog"

    invoke-static {v2, v12}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v13

    invoke-virtual {v13}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v14, Ljava/util/HashMap;

    invoke-direct {v14}, Ljava/util/HashMap;-><init>()V

    const-string v2, "ItemTitle"

    const/4 v5, 0x1

    invoke-virtual {v13, v5}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v14, v2, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "ItemText"

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v14, v2, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v11, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v10

    const-string v2, "MobileLog"

    invoke-virtual {v10}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    new-instance v1, Landroid/widget/SimpleAdapter;

    const v4, 0x7f030002

    const/4 v2, 0x2

    new-array v5, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v6, "ItemTitle"

    aput-object v6, v5, v2

    const/4 v2, 0x1

    const-string v6, "ItemText"

    aput-object v6, v5, v2

    const/4 v2, 0x2

    new-array v6, v2, [I

    fill-array-data v6, :array_0

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v6}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    new-instance v4, Landroid/widget/SimpleAdapter;

    const v7, 0x7f030002

    const/4 v2, 0x2

    new-array v8, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v5, "ItemTitle"

    aput-object v5, v8, v2

    const/4 v2, 0x1

    const-string v5, "ItemText"

    aput-object v5, v8, v2

    const/4 v2, 0x2

    new-array v9, v2, [I

    fill-array-data v9, :array_1

    move-object/from16 v5, p0

    move-object v6, v11

    invoke-direct/range {v4 .. v9}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/mobilelog/AndroidLogFilterList;->newFilterView:Landroid/widget/ListView;

    invoke-virtual {v2, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/mobilelog/AndroidLogFilterList;->filterListView:Landroid/widget/ListView;

    invoke-virtual {v2, v4}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void

    :array_0
    .array-data 4
        0x7f08000b
        0x7f08000c
    .end array-data

    :array_1
    .array-data 4
        0x7f08000b
        0x7f08000c
    .end array-data
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030003

    invoke-virtual {p0, v0}, Lcom/mediatek/mobilelog/AndroidLogFilterList;->setContentView(I)V

    new-instance v0, Lcom/mediatek/mobilelog/LogSettings;

    iget-object v1, p0, Lcom/mediatek/mobilelog/AndroidLogFilterList;->mLogSettingsPath:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/mediatek/mobilelog/LogSettings;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/mediatek/mobilelog/AndroidLogFilterList;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;

    const v0, 0x7f08000e

    invoke-virtual {p0, v0}, Lcom/mediatek/mobilelog/AndroidLogFilterList;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/mediatek/mobilelog/AndroidLogFilterList;->newFilterView:Landroid/widget/ListView;

    const v0, 0x7f080010

    invoke-virtual {p0, v0}, Lcom/mediatek/mobilelog/AndroidLogFilterList;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/mediatek/mobilelog/AndroidLogFilterList;->filterListView:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/mediatek/mobilelog/AndroidLogFilterList;->newFilterView:Landroid/widget/ListView;

    new-instance v1, Lcom/mediatek/mobilelog/AndroidLogFilterList$1;

    invoke-direct {v1, p0}, Lcom/mediatek/mobilelog/AndroidLogFilterList$1;-><init>(Lcom/mediatek/mobilelog/AndroidLogFilterList;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/mediatek/mobilelog/AndroidLogFilterList;->filterListView:Landroid/widget/ListView;

    new-instance v1, Lcom/mediatek/mobilelog/AndroidLogFilterList$2;

    invoke-direct {v1, p0}, Lcom/mediatek/mobilelog/AndroidLogFilterList$2;-><init>(Lcom/mediatek/mobilelog/AndroidLogFilterList;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method protected onPause()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    const-string v0, "MobileLog"

    const-string v1, "AndroidLogFilterList onPause"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const-string v0, "MobileLog"

    const-string v1, "AndroidLogFilterList onResume"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/mobilelog/AndroidLogFilterList;->updateUi()V

    return-void
.end method

.method protected onStop()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    const-string v0, "MobileLog"

    const-string v1, "AndroidLogFilterList: onStop"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
