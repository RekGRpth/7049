.class public Lcom/mediatek/mobilelog/DialogActivity;
.super Landroid/app/Activity;
.source "DialogActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MobileLog"


# instance fields
.field dialog:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Lcom/mediatek/mobilelog/DialogActivity;->requestWindowFeature(I)Z

    const/high16 v3, 0x7f030000

    invoke-virtual {p0, v3}, Lcom/mediatek/mobilelog/DialogActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/mediatek/mobilelog/DialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "DialogTitle"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/mediatek/mobilelog/DialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "DialogMessage"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const v3, 0x1040013

    new-instance v4, Lcom/mediatek/mobilelog/DialogActivity$1;

    invoke-direct {v4, p0}, Lcom/mediatek/mobilelog/DialogActivity$1;-><init>(Lcom/mediatek/mobilelog/DialogActivity;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/mobilelog/DialogActivity;->dialog:Landroid/app/AlertDialog;

    iget-object v3, p0, Lcom/mediatek/mobilelog/DialogActivity;->dialog:Landroid/app/AlertDialog;

    new-instance v4, Lcom/mediatek/mobilelog/DialogActivity$2;

    invoke-direct {v4, p0}, Lcom/mediatek/mobilelog/DialogActivity$2;-><init>(Lcom/mediatek/mobilelog/DialogActivity;)V

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    iget-object v3, p0, Lcom/mediatek/mobilelog/DialogActivity;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    const-string v0, "MobileLog"

    const-string v1, "UI activity on destroy!"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/mobilelog/DialogActivity;->dialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/mobilelog/DialogActivity;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->cancel()V

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method
