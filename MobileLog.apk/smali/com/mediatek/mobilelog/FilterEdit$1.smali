.class Lcom/mediatek/mobilelog/FilterEdit$1;
.super Ljava/lang/Object;
.source "FilterEdit.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/mobilelog/FilterEdit;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/mobilelog/FilterEdit;


# direct methods
.method constructor <init>(Lcom/mediatek/mobilelog/FilterEdit;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/mobilelog/FilterEdit$1;->this$0:Lcom/mediatek/mobilelog/FilterEdit;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 13
    .param p1    # Landroid/view/View;

    monitor-enter p0

    const/4 v7, 0x0

    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const-string v10, "/system/bin/xlog"

    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v10, "MobileLog"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "set "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/mediatek/mobilelog/FilterEdit$1;->this$0:Lcom/mediatek/mobilelog/FilterEdit;

    # getter for: Lcom/mediatek/mobilelog/FilterEdit;->mModuleName:Ljava/lang/String;
    invoke-static {v12}, Lcom/mediatek/mobilelog/FilterEdit;->access$000(Lcom/mediatek/mobilelog/FilterEdit;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/mediatek/mobilelog/FilterEdit$1;->this$0:Lcom/mediatek/mobilelog/FilterEdit;

    # getter for: Lcom/mediatek/mobilelog/FilterEdit;->mModuleLevel:Ljava/lang/String;
    invoke-static {v12}, Lcom/mediatek/mobilelog/FilterEdit;->access$100(Lcom/mediatek/mobilelog/FilterEdit;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v10, p0, Lcom/mediatek/mobilelog/FilterEdit$1;->this$0:Lcom/mediatek/mobilelog/FilterEdit;

    iget-object v11, p0, Lcom/mediatek/mobilelog/FilterEdit$1;->this$0:Lcom/mediatek/mobilelog/FilterEdit;

    # getter for: Lcom/mediatek/mobilelog/FilterEdit;->mTextbox:Landroid/widget/EditText;
    invoke-static {v11}, Lcom/mediatek/mobilelog/FilterEdit;->access$200(Lcom/mediatek/mobilelog/FilterEdit;)Landroid/widget/EditText;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v11

    # setter for: Lcom/mediatek/mobilelog/FilterEdit;->mModuleName:Ljava/lang/String;
    invoke-static {v10, v11}, Lcom/mediatek/mobilelog/FilterEdit;->access$002(Lcom/mediatek/mobilelog/FilterEdit;Ljava/lang/String;)Ljava/lang/String;

    iget-object v10, p0, Lcom/mediatek/mobilelog/FilterEdit$1;->this$0:Lcom/mediatek/mobilelog/FilterEdit;

    # getter for: Lcom/mediatek/mobilelog/FilterEdit;->mModuleName:Ljava/lang/String;
    invoke-static {v10}, Lcom/mediatek/mobilelog/FilterEdit;->access$000(Lcom/mediatek/mobilelog/FilterEdit;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "All"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    const-string v10, "MobileLog"

    const-string v11, "setAll"

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v10, "all"

    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v10, p0, Lcom/mediatek/mobilelog/FilterEdit$1;->this$0:Lcom/mediatek/mobilelog/FilterEdit;

    # getter for: Lcom/mediatek/mobilelog/FilterEdit;->mModuleLevel:Ljava/lang/String;
    invoke-static {v10}, Lcom/mediatek/mobilelog/FilterEdit;->access$100(Lcom/mediatek/mobilelog/FilterEdit;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v11

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v10

    new-array v10, v10, [Ljava/lang/String;

    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [Ljava/lang/String;

    invoke-virtual {v11, v10}, Ljava/lang/Runtime;->exec([Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Process;->waitFor()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    :try_start_2
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    iget-object v10, p0, Lcom/mediatek/mobilelog/FilterEdit$1;->this$0:Lcom/mediatek/mobilelog/FilterEdit;

    const-class v11, Lcom/mediatek/mobilelog/AndroidLogFilterList;

    invoke-virtual {v11}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5, v10, v11}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v10, p0, Lcom/mediatek/mobilelog/FilterEdit$1;->this$0:Lcom/mediatek/mobilelog/FilterEdit;

    invoke-virtual {v10, v5}, Lcom/mediatek/mobilelog/FilterEdit;->startActivity(Landroid/content/Intent;)V

    iget-object v10, p0, Lcom/mediatek/mobilelog/FilterEdit$1;->this$0:Lcom/mediatek/mobilelog/FilterEdit;

    invoke-virtual {v10}, Lcom/mediatek/mobilelog/FilterEdit;->finish()V

    monitor-exit p0

    return-void

    :catch_0
    move-exception v2

    const-string v10, "MobileLog"

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catchall_0
    move-exception v10

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v10

    :cond_0
    :try_start_3
    iget-object v10, p0, Lcom/mediatek/mobilelog/FilterEdit$1;->this$0:Lcom/mediatek/mobilelog/FilterEdit;

    # getter for: Lcom/mediatek/mobilelog/FilterEdit;->mModuleName:Ljava/lang/String;
    invoke-static {v10}, Lcom/mediatek/mobilelog/FilterEdit;->access$000(Lcom/mediatek/mobilelog/FilterEdit;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v10, p0, Lcom/mediatek/mobilelog/FilterEdit$1;->this$0:Lcom/mediatek/mobilelog/FilterEdit;

    # getter for: Lcom/mediatek/mobilelog/FilterEdit;->mModuleLevel:Ljava/lang/String;
    invoke-static {v10}, Lcom/mediatek/mobilelog/FilterEdit;->access$100(Lcom/mediatek/mobilelog/FilterEdit;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v11

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v10

    new-array v10, v10, [Ljava/lang/String;

    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [Ljava/lang/String;

    invoke-virtual {v11, v10}, Ljava/lang/Runtime;->exec([Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v9

    new-instance v4, Ljava/io/InputStreamReader;

    invoke-direct {v4, v9}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    new-instance v0, Ljava/io/BufferedReader;

    invoke-direct {v0, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    const/4 v6, 0x0

    :goto_1
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    const-string v10, "MobileLog"

    invoke-static {v10, v6}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    :catch_1
    move-exception v2

    :try_start_5
    const-string v10, "MobileLog"

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    :cond_1
    :try_start_6
    invoke-virtual {v7}, Ljava/lang/Process;->waitFor()I

    move-result v3

    iget-object v10, p0, Lcom/mediatek/mobilelog/FilterEdit$1;->this$0:Lcom/mediatek/mobilelog/FilterEdit;

    # getter for: Lcom/mediatek/mobilelog/FilterEdit;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;
    invoke-static {v10}, Lcom/mediatek/mobilelog/FilterEdit;->access$300(Lcom/mediatek/mobilelog/FilterEdit;)Lcom/mediatek/mobilelog/LogSettings;

    move-result-object v10

    invoke-virtual {v10}, Lcom/mediatek/mobilelog/LogSettings;->loadConfig()Ljava/util/Properties;

    move-result-object v8

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "xlog.tag."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/mediatek/mobilelog/FilterEdit$1;->this$0:Lcom/mediatek/mobilelog/FilterEdit;

    # getter for: Lcom/mediatek/mobilelog/FilterEdit;->mModuleName:Ljava/lang/String;
    invoke-static {v11}, Lcom/mediatek/mobilelog/FilterEdit;->access$000(Lcom/mediatek/mobilelog/FilterEdit;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    iget-object v11, p0, Lcom/mediatek/mobilelog/FilterEdit$1;->this$0:Lcom/mediatek/mobilelog/FilterEdit;

    # getter for: Lcom/mediatek/mobilelog/FilterEdit;->mModuleLevel:Ljava/lang/String;
    invoke-static {v11}, Lcom/mediatek/mobilelog/FilterEdit;->access$100(Lcom/mediatek/mobilelog/FilterEdit;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v10, v11}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v10, p0, Lcom/mediatek/mobilelog/FilterEdit$1;->this$0:Lcom/mediatek/mobilelog/FilterEdit;

    # getter for: Lcom/mediatek/mobilelog/FilterEdit;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;
    invoke-static {v10}, Lcom/mediatek/mobilelog/FilterEdit;->access$300(Lcom/mediatek/mobilelog/FilterEdit;)Lcom/mediatek/mobilelog/LogSettings;

    move-result-object v10

    invoke-virtual {v10, v8}, Lcom/mediatek/mobilelog/LogSettings;->saveConfig(Ljava/util/Properties;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0
.end method
