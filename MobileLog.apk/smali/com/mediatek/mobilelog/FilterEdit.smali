.class public Lcom/mediatek/mobilelog/FilterEdit;
.super Landroid/app/Activity;
.source "FilterEdit.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MobileLog"


# instance fields
.field private mBtnSetListener:Landroid/view/View$OnClickListener;

.field private mButtonSet:Landroid/widget/Button;

.field private mLogSettings:Lcom/mediatek/mobilelog/LogSettings;

.field private mLogSettingsPath:Ljava/lang/String;

.field private mModuleLevel:Ljava/lang/String;

.field private mModuleName:Ljava/lang/String;

.field private mRadioGroup:Landroid/widget/RadioGroup;

.field private mRadioGroupListener:Landroid/widget/RadioGroup$OnCheckedChangeListener;

.field private mTextbox:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const-string v0, "verbose"

    iput-object v0, p0, Lcom/mediatek/mobilelog/FilterEdit;->mModuleLevel:Ljava/lang/String;

    const-string v0, "/data/data/com.mediatek.mobilelog/files"

    iput-object v0, p0, Lcom/mediatek/mobilelog/FilterEdit;->mLogSettingsPath:Ljava/lang/String;

    new-instance v0, Lcom/mediatek/mobilelog/FilterEdit$1;

    invoke-direct {v0, p0}, Lcom/mediatek/mobilelog/FilterEdit$1;-><init>(Lcom/mediatek/mobilelog/FilterEdit;)V

    iput-object v0, p0, Lcom/mediatek/mobilelog/FilterEdit;->mBtnSetListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/mediatek/mobilelog/FilterEdit$2;

    invoke-direct {v0, p0}, Lcom/mediatek/mobilelog/FilterEdit$2;-><init>(Lcom/mediatek/mobilelog/FilterEdit;)V

    iput-object v0, p0, Lcom/mediatek/mobilelog/FilterEdit;->mRadioGroupListener:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/mobilelog/FilterEdit;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/FilterEdit;

    iget-object v0, p0, Lcom/mediatek/mobilelog/FilterEdit;->mModuleName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$002(Lcom/mediatek/mobilelog/FilterEdit;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/mobilelog/FilterEdit;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/mobilelog/FilterEdit;->mModuleName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100(Lcom/mediatek/mobilelog/FilterEdit;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/FilterEdit;

    iget-object v0, p0, Lcom/mediatek/mobilelog/FilterEdit;->mModuleLevel:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/mediatek/mobilelog/FilterEdit;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/mobilelog/FilterEdit;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/mobilelog/FilterEdit;->mModuleLevel:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$200(Lcom/mediatek/mobilelog/FilterEdit;)Landroid/widget/EditText;
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/FilterEdit;

    iget-object v0, p0, Lcom/mediatek/mobilelog/FilterEdit;->mTextbox:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/mobilelog/FilterEdit;)Lcom/mediatek/mobilelog/LogSettings;
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/FilterEdit;

    iget-object v0, p0, Lcom/mediatek/mobilelog/FilterEdit;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;

    return-object v0
.end method

.method private getRadioId(Ljava/lang/String;)I
    .locals 2
    .param p1    # Ljava/lang/String;

    const v0, 0x7f080004

    const-string v1, "verbose"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v1, "debug"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const v0, 0x7f080005

    goto :goto_0

    :cond_2
    const-string v1, "info"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const v0, 0x7f080006

    goto :goto_0

    :cond_3
    const-string v1, "warn"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const v0, 0x7f080007

    goto :goto_0

    :cond_4
    const-string v1, "error"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const v0, 0x7f080008

    goto :goto_0
.end method

.method private updateUi()V
    .locals 0

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v2, 0x7f030001

    invoke-virtual {p0, v2}, Lcom/mediatek/mobilelog/FilterEdit;->setContentView(I)V

    new-instance v2, Lcom/mediatek/mobilelog/LogSettings;

    iget-object v3, p0, Lcom/mediatek/mobilelog/FilterEdit;->mLogSettingsPath:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/mediatek/mobilelog/LogSettings;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/mediatek/mobilelog/FilterEdit;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;

    const-string v2, "MobileLog"

    const-string v3, "filter edit create"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p0}, Lcom/mediatek/mobilelog/FilterEdit;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "Module Name"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v2, "Module Level"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v2, "Module Name"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/mobilelog/FilterEdit;->mModuleName:Ljava/lang/String;

    const-string v2, "Module Level"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/mobilelog/FilterEdit;->mModuleLevel:Ljava/lang/String;

    const-string v2, "MobileLog"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "recv: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/mobilelog/FilterEdit;->mModuleName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/mobilelog/FilterEdit;->mModuleLevel:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const v2, 0x7f080009

    invoke-virtual {p0, v2}, Lcom/mediatek/mobilelog/FilterEdit;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/mediatek/mobilelog/FilterEdit;->mButtonSet:Landroid/widget/Button;

    const v2, 0x7f080001

    invoke-virtual {p0, v2}, Lcom/mediatek/mobilelog/FilterEdit;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/mediatek/mobilelog/FilterEdit;->mTextbox:Landroid/widget/EditText;

    const v2, 0x7f080003

    invoke-virtual {p0, v2}, Lcom/mediatek/mobilelog/FilterEdit;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioGroup;

    iput-object v2, p0, Lcom/mediatek/mobilelog/FilterEdit;->mRadioGroup:Landroid/widget/RadioGroup;

    iget-object v2, p0, Lcom/mediatek/mobilelog/FilterEdit;->mButtonSet:Landroid/widget/Button;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/mobilelog/FilterEdit;->mTextbox:Landroid/widget/EditText;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/mobilelog/FilterEdit;->mRadioGroup:Landroid/widget/RadioGroup;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/mobilelog/FilterEdit;->mButtonSet:Landroid/widget/Button;

    iget-object v3, p0, Lcom/mediatek/mobilelog/FilterEdit;->mBtnSetListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/mediatek/mobilelog/FilterEdit;->mTextbox:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/mediatek/mobilelog/FilterEdit;->mModuleName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/mediatek/mobilelog/FilterEdit;->mRadioGroup:Landroid/widget/RadioGroup;

    iget-object v3, p0, Lcom/mediatek/mobilelog/FilterEdit;->mRadioGroupListener:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    invoke-virtual {v2, v3}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    iget-object v2, p0, Lcom/mediatek/mobilelog/FilterEdit;->mRadioGroup:Landroid/widget/RadioGroup;

    iget-object v3, p0, Lcom/mediatek/mobilelog/FilterEdit;->mModuleLevel:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/mediatek/mobilelog/FilterEdit;->getRadioId(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    const-string v0, "MobileLog"

    const-string v1, "FilterEdit onPause"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    invoke-direct {p0}, Lcom/mediatek/mobilelog/FilterEdit;->updateUi()V

    const-string v0, "MobileLog"

    const-string v1, "FilterEdit onResume"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected onStop()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    const-string v0, "MobileLog"

    const-string v1, "FilterEdit onStop"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
