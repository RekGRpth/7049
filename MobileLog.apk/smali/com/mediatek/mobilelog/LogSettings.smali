.class public Lcom/mediatek/mobilelog/LogSettings;
.super Ljava/lang/Object;
.source "LogSettings.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MobileLogJ/LogSet"


# instance fields
.field private boot_option:Ljava/lang/String;

.field private log_path:Ljava/lang/String;

.field private log_path_type:Ljava/lang/String;

.field private log_size:Ljava/lang/String;

.field private mConfigPath:Ljava/lang/String;

.field private mFile:Ljava/io/File;

.field private mFileName:Ljava/lang/String;

.field private service_status:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "MobileLogSettings.xml"

    iput-object v0, p0, Lcom/mediatek/mobilelog/LogSettings;->mFileName:Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/mobilelog/LogSettings;->mConfigPath:Ljava/lang/String;

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/mediatek/mobilelog/LogSettings;->mConfigPath:Ljava/lang/String;

    iget-object v2, p0, Lcom/mediatek/mobilelog/LogSettings;->mFileName:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/mediatek/mobilelog/LogSettings;->mFile:Ljava/io/File;

    invoke-virtual {p0}, Lcom/mediatek/mobilelog/LogSettings;->setDefaultValue()V

    iget-object v0, p0, Lcom/mediatek/mobilelog/LogSettings;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/mobilelog/LogSettings;->createConfig()V

    :cond_0
    return-void
.end method


# virtual methods
.method public createConfig()V
    .locals 5

    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/mediatek/mobilelog/LogSettings;->mConfigPath:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "MobileLogJ/LogSet"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mkdirs error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/mobilelog/LogSettings;->mConfigPath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mediatek/mobilelog/LogSettings;->mFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    invoke-virtual {p0}, Lcom/mediatek/mobilelog/LogSettings;->initConfig()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    :try_start_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/mediatek/mobilelog/LogSettings;->mConfigPath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/mobilelog/LogSettings;->mFileName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/mediatek/mobilelog/MobileLogJNI;->chmod_file(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    const-string v2, "MobileLogJ/LogSet"

    const-string v3, "jni done,configure file permission has changed"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :catch_0
    move-exception v1

    const-string v2, "MobileLogJ/LogSet"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "LogSettings "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v2, "MobileLogJ/LogSet"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "chmod_file "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public getBootOption()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/mobilelog/LogSettings;->boot_option:Ljava/lang/String;

    return-object v0
.end method

.method public getLogPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/mobilelog/LogSettings;->log_path:Ljava/lang/String;

    return-object v0
.end method

.method public getLogPathType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/mobilelog/LogSettings;->log_path_type:Ljava/lang/String;

    return-object v0
.end method

.method public getRightPath(Landroid/content/Context;)Ljava/lang/String;
    .locals 5
    .param p1    # Landroid/content/Context;

    const-string v2, "/mnt/sdcard"

    invoke-virtual {p0}, Lcom/mediatek/mobilelog/LogSettings;->loadConfig()Ljava/util/Properties;

    move-result-object v1

    const-string v3, "storage"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/storage/StorageManager;

    const-string v3, "log_path"

    const-string v4, "/mnt/sdcard"

    invoke-virtual {v1, v3, v4}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "persist.radio.log2sd.path"

    invoke-static {v3, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "/data"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "/data"

    :goto_0
    return-object v3

    :cond_0
    const-string v3, "/mnt/sdcard"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Landroid/os/storage/StorageManager;->getInternalStoragePath()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    move-object v3, v2

    goto :goto_0

    :cond_1
    const-string v3, "MobileLogJ/LogSet"

    const-string v4, "getInternalStoragePath() returns null !!!"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "/storage/sdcard0"

    goto :goto_0

    :cond_2
    const-string v3, "/mnt/sdcard2"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v0}, Landroid/os/storage/StorageManager;->getExternalStoragePath()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    move-object v3, v2

    goto :goto_0

    :cond_3
    const-string v3, "MobileLogJ/LogSet"

    const-string v4, "getExteranlStoragePath() returns null !!!"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "/storage/sdcard1"

    goto :goto_0

    :cond_4
    const-string v3, "MobileLogJ/LogSet"

    const-string v4, "log path invalid"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "/storage/sdcard0"

    goto :goto_0
.end method

.method public initConfig()V
    .locals 8

    new-instance v0, Ljava/io/File;

    const-string v5, "/system/etc/mtklog-config.prop"

    invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/util/Properties;

    invoke-direct {v4}, Ljava/util/Properties;-><init>()V

    const/4 v5, 0x1

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v6

    if-ne v5, v6, :cond_1

    new-instance v2, Ljava/util/Properties;

    invoke-direct {v2}, Ljava/util/Properties;-><init>()V

    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    const-string v5, "/system/etc/mtklog-config.prop"

    invoke-direct {v1, v5}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    const-string v5, "com.mediatek.log.mobile.enabled"

    const-string v6, "true"

    invoke-virtual {v2, v5, v6}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/mediatek/mobilelog/LogSettings;->boot_option:Ljava/lang/String;

    const-string v5, "com.mediatek.log.mobile.maxsize"

    const-string v6, "300"

    invoke-virtual {v2, v5, v6}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/mediatek/mobilelog/LogSettings;->log_size:Ljava/lang/String;

    iget-object v5, p0, Lcom/mediatek/mobilelog/LogSettings;->log_size:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "300"

    iput-object v5, p0, Lcom/mediatek/mobilelog/LogSettings;->log_size:Ljava/lang/String;

    :cond_0
    const-string v5, "persist.radio.log2sd.path"

    const-string v6, "/mnt/sdcard"

    invoke-virtual {v2, v5, v6}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/mediatek/mobilelog/LogSettings;->log_path:Ljava/lang/String;

    const/4 v5, -0x1

    iget-object v6, p0, Lcom/mediatek/mobilelog/LogSettings;->log_path:Ljava/lang/String;

    const-string v7, "/data"

    invoke-virtual {v6, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    if-eq v5, v6, :cond_2

    const-string v5, "phone"

    iput-object v5, p0, Lcom/mediatek/mobilelog/LogSettings;->log_path_type:Ljava/lang/String;

    :goto_0
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    const-string v5, "service_boot_option"

    iget-object v6, p0, Lcom/mediatek/mobilelog/LogSettings;->boot_option:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "service_status"

    iget-object v6, p0, Lcom/mediatek/mobilelog/LogSettings;->service_status:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "log_size"

    iget-object v6, p0, Lcom/mediatek/mobilelog/LogSettings;->log_size:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "log_path"

    iget-object v6, p0, Lcom/mediatek/mobilelog/LogSettings;->log_path:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "log_path_type"

    iget-object v6, p0, Lcom/mediatek/mobilelog/LogSettings;->log_path_type:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "AndroidLog"

    const-string v6, "true"

    invoke-virtual {v4, v5, v6}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "KernelLog"

    const-string v6, "true"

    invoke-virtual {v4, v5, v6}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "BTLog"

    const-string v6, "true"

    invoke-virtual {v4, v5, v6}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0, v4}, Lcom/mediatek/mobilelog/LogSettings;->saveConfig(Ljava/util/Properties;)V

    return-void

    :cond_2
    :try_start_1
    const-string v5, "sdcard"

    iput-object v5, p0, Lcom/mediatek/mobilelog/LogSettings;->log_path_type:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v3

    const-string v5, "MobileLogJ/LogSet"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "read customize config file error!"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public loadConfig()Ljava/util/Properties;
    .locals 6

    new-instance v2, Ljava/util/Properties;

    invoke-direct {v2}, Ljava/util/Properties;-><init>()V

    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    iget-object v3, p0, Lcom/mediatek/mobilelog/LogSettings;->mFile:Ljava/io/File;

    invoke-direct {v1, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v2, v1}, Ljava/util/Properties;->loadFromXML(Ljava/io/InputStream;)V

    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    const-string v3, "MobileLogJ/LogSet"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "loadConfig "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/mediatek/mobilelog/LogSettings;->recreateConfig()V

    goto :goto_0
.end method

.method public recreateConfig()V
    .locals 2

    const-string v0, "MobileLogJ/LogSet"

    const-string v1, "current configure file may be destroyed or deleted, recreate it"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/mobilelog/LogSettings;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/mobilelog/LogSettings;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    :cond_0
    invoke-virtual {p0}, Lcom/mediatek/mobilelog/LogSettings;->createConfig()V

    return-void
.end method

.method public saveConfig(Ljava/util/Properties;)V
    .locals 5
    .param p1    # Ljava/util/Properties;

    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    iget-object v2, p0, Lcom/mediatek/mobilelog/LogSettings;->mFile:Ljava/io/File;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    const-string v2, ""

    invoke-virtual {p1, v1, v2}, Ljava/util/Properties;->storeToXML(Ljava/io/OutputStream;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "MobileLogJ/LogSet"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "saveConfig "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setDefaultValue()V
    .locals 2

    const-string v0, "ro.build.type"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "user"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "MobileLogJ/LogSet"

    const-string v1, "user build, will not boot mobilelog"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "false"

    iput-object v0, p0, Lcom/mediatek/mobilelog/LogSettings;->boot_option:Ljava/lang/String;

    :goto_0
    const-string v0, "/mnt/sdcard"

    iput-object v0, p0, Lcom/mediatek/mobilelog/LogSettings;->log_path:Ljava/lang/String;

    const-string v0, "300"

    iput-object v0, p0, Lcom/mediatek/mobilelog/LogSettings;->log_size:Ljava/lang/String;

    const-string v0, "sdcard"

    iput-object v0, p0, Lcom/mediatek/mobilelog/LogSettings;->log_path_type:Ljava/lang/String;

    const-string v0, "service_stopped"

    iput-object v0, p0, Lcom/mediatek/mobilelog/LogSettings;->service_status:Ljava/lang/String;

    return-void

    :cond_0
    const-string v0, "true"

    iput-object v0, p0, Lcom/mediatek/mobilelog/LogSettings;->boot_option:Ljava/lang/String;

    goto :goto_0
.end method
