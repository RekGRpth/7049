.class public Lcom/mediatek/mobilelog/MlConnection;
.super Ljava/lang/Object;
.source "MlConnection.java"


# static fields
.field public static final IMONKEY_HOST:Ljava/lang/String; = "127.0.0.1"

.field private static final MSG_ALIVE:I = 0x4

.field private static final MSG_CONN_FAIL:I = 0x2

.field private static final MSG_DIE:I = 0x3

.field private static final MSG_SD_ALMOST_FULL:I = 0x8

.field private static final TAG:Ljava/lang/String; = "MobileLogJ/Ml"


# instance fields
.field private final BUFFER_SIZE:I

.field address:Landroid/net/LocalSocketAddress;

.field mHandler:Landroid/os/Handler;

.field private mIMonkeyPort:I

.field private mInputStream:Ljava/io/InputStream;

.field private mMsg:Landroid/os/Message;

.field private mOutputStream:Ljava/io/OutputStream;

.field private mShouldStop:Z

.field private mlistenThread:Ljava/lang/Thread;

.field socket:Landroid/net/LocalSocket;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/os/Handler;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/os/Handler;

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/mediatek/mobilelog/MlConnection;->mlistenThread:Ljava/lang/Thread;

    iput-object v0, p0, Lcom/mediatek/mobilelog/MlConnection;->mHandler:Landroid/os/Handler;

    const/16 v0, 0x162d

    iput v0, p0, Lcom/mediatek/mobilelog/MlConnection;->mIMonkeyPort:I

    const/16 v0, 0x20

    iput v0, p0, Lcom/mediatek/mobilelog/MlConnection;->BUFFER_SIZE:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/mobilelog/MlConnection;->mShouldStop:Z

    iput-object p2, p0, Lcom/mediatek/mobilelog/MlConnection;->mHandler:Landroid/os/Handler;

    new-instance v0, Landroid/net/LocalSocket;

    invoke-direct {v0}, Landroid/net/LocalSocket;-><init>()V

    iput-object v0, p0, Lcom/mediatek/mobilelog/MlConnection;->socket:Landroid/net/LocalSocket;

    new-instance v0, Landroid/net/LocalSocketAddress;

    invoke-direct {v0, p1}, Landroid/net/LocalSocketAddress;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/mediatek/mobilelog/MlConnection;->address:Landroid/net/LocalSocketAddress;

    return-void
.end method


# virtual methods
.method public connect()Z
    .locals 3

    :try_start_0
    iget-object v1, p0, Lcom/mediatek/mobilelog/MlConnection;->socket:Landroid/net/LocalSocket;

    iget-object v2, p0, Lcom/mediatek/mobilelog/MlConnection;->address:Landroid/net/LocalSocketAddress;

    invoke-virtual {v1, v2}, Landroid/net/LocalSocket;->connect(Landroid/net/LocalSocketAddress;)V

    iget-object v1, p0, Lcom/mediatek/mobilelog/MlConnection;->socket:Landroid/net/LocalSocket;

    invoke-virtual {v1}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/mobilelog/MlConnection;->mOutputStream:Ljava/io/OutputStream;

    iget-object v1, p0, Lcom/mediatek/mobilelog/MlConnection;->socket:Landroid/net/LocalSocket;

    invoke-virtual {v1}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/mobilelog/MlConnection;->mInputStream:Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v1, Lcom/mediatek/mobilelog/MlConnection$1;

    invoke-direct {v1, p0}, Lcom/mediatek/mobilelog/MlConnection$1;-><init>(Lcom/mediatek/mobilelog/MlConnection;)V

    iput-object v1, p0, Lcom/mediatek/mobilelog/MlConnection;->mlistenThread:Ljava/lang/Thread;

    iget-object v1, p0, Lcom/mediatek/mobilelog/MlConnection;->mlistenThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    const-string v1, "MobileLogJ/Ml"

    const-string v2, "Start a new thread to listen from deamon,done"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const-string v1, "MobileLogJ/Ml"

    const-string v2, "Communications error"

    invoke-static {v1, v2, v0}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public listen()V
    .locals 7

    const/16 v3, 0x20

    const/4 v6, 0x3

    new-array v0, v3, [B

    :goto_0
    iget-boolean v3, p0, Lcom/mediatek/mobilelog/MlConnection;->mShouldStop:Z

    if-nez v3, :cond_0

    :try_start_0
    iget-object v3, p0, Lcom/mediatek/mobilelog/MlConnection;->mInputStream:Ljava/io/InputStream;

    const/4 v4, 0x0

    const/16 v5, 0x20

    invoke-virtual {v3, v0, v4, v5}, Ljava/io/InputStream;->read([BII)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-gez v1, :cond_2

    :cond_0
    :goto_1
    iget-boolean v3, p0, Lcom/mediatek/mobilelog/MlConnection;->mShouldStop:Z

    if-nez v3, :cond_1

    const-string v3, "MobileLogJ/Ml"

    const-string v4, "listen break"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/mobilelog/MlConnection;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/mobilelog/MlConnection;->mMsg:Landroid/os/Message;

    iget-object v3, p0, Lcom/mediatek/mobilelog/MlConnection;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/mediatek/mobilelog/MlConnection;->mMsg:Landroid/os/Message;

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    return-void

    :cond_2
    const/4 v3, 0x0

    :try_start_1
    aget-byte v3, v0, v3

    sparse-switch v3, :sswitch_data_0

    iget-object v3, p0, Lcom/mediatek/mobilelog/MlConnection;->mHandler:Landroid/os/Handler;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/mobilelog/MlConnection;->mMsg:Landroid/os/Message;

    const-string v3, "MobileLogJ/Ml"

    const-string v4, "dont know the cmd from deamon,think it as alive"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    iget-object v3, p0, Lcom/mediatek/mobilelog/MlConnection;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/mediatek/mobilelog/MlConnection;->mMsg:Landroid/os/Message;

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    const-string v3, "MobileLogJ/Ml"

    const-string v4, "read failed"

    invoke-static {v3, v4, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :sswitch_0
    :try_start_2
    iget-object v3, p0, Lcom/mediatek/mobilelog/MlConnection;->mHandler:Landroid/os/Handler;

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/mobilelog/MlConnection;->mMsg:Landroid/os/Message;

    const-string v3, "MobileLogJ/Ml"

    const-string v4, "recv DIE from deamon"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :sswitch_1
    iget-object v3, p0, Lcom/mediatek/mobilelog/MlConnection;->mHandler:Landroid/os/Handler;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/mobilelog/MlConnection;->mMsg:Landroid/os/Message;

    const-string v3, "MobileLogJ/Ml"

    const-string v4, "recv ALIVE from deamon"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :sswitch_2
    iget-object v3, p0, Lcom/mediatek/mobilelog/MlConnection;->mHandler:Landroid/os/Handler;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/mobilelog/MlConnection;->mMsg:Landroid/os/Message;

    const-string v3, "MobileLogJ/Ml"

    const-string v4, "from deamon:sd card is almost full"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    :sswitch_data_0
    .sparse-switch
        0x61 -> :sswitch_1
        0x64 -> :sswitch_0
        0x73 -> :sswitch_2
    .end sparse-switch
.end method

.method public sendCmd(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    const-string v2, "MobileLogJ/Ml"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "send cmd: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/mediatek/mobilelog/MlConnection;->mOutputStream:Ljava/io/OutputStream;

    if-nez v2, :cond_0

    const-string v2, "MobileLogJ/Ml"

    const-string v3, "outputstream is null, can\'t send cmd"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/mediatek/mobilelog/MlConnection;->stop()V

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v2, p0, Lcom/mediatek/mobilelog/MlConnection;->mOutputStream:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/OutputStream;->write([B)V

    iget-object v2, p0, Lcom/mediatek/mobilelog/MlConnection;->mOutputStream:Ljava/io/OutputStream;

    invoke-virtual {v2}, Ljava/io/OutputStream;->flush()V

    const-string v2, "MobileLogJ/Ml"

    const-string v3, "command send success"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_2
    const-string v2, "MobileLogJ/Ml"

    const-string v3, "IOException in do_command"

    invoke-static {v2, v3, v1}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/mediatek/mobilelog/MlConnection;->mOutputStream:Ljava/io/OutputStream;

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2
.end method

.method public stop()V
    .locals 5

    const/4 v4, 0x0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/mediatek/mobilelog/MlConnection;->mShouldStop:Z

    const-string v1, "MobileLogJ/Ml"

    const-string v2, "thread stop"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/mobilelog/MlConnection;->socket:Landroid/net/LocalSocket;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/mediatek/mobilelog/MlConnection;->socket:Landroid/net/LocalSocket;

    invoke-virtual {v1}, Landroid/net/LocalSocket;->shutdownInput()V

    iget-object v1, p0, Lcom/mediatek/mobilelog/MlConnection;->socket:Landroid/net/LocalSocket;

    invoke-virtual {v1}, Landroid/net/LocalSocket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iput-object v4, p0, Lcom/mediatek/mobilelog/MlConnection;->mlistenThread:Ljava/lang/Thread;

    iput-object v4, p0, Lcom/mediatek/mobilelog/MlConnection;->socket:Landroid/net/LocalSocket;

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "MobileLogJ/Ml"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "socket close: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
