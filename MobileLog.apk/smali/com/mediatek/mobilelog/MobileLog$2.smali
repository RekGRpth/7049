.class Lcom/mediatek/mobilelog/MobileLog$2;
.super Landroid/content/BroadcastReceiver;
.source "MobileLog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/mobilelog/MobileLog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/mobilelog/MobileLog;


# direct methods
.method constructor <init>(Lcom/mediatek/mobilelog/MobileLog;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/mobilelog/MobileLog$2;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const-string v1, "MobileLogJ/Activity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BroadcastReceiver onReceive: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.mediatek.mobilelog.APLogger_START"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLog$2;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    const/4 v2, 0x1

    # invokes: Lcom/mediatek/mobilelog/MobileLog;->updateUI(Z)V
    invoke-static {v1, v2}, Lcom/mediatek/mobilelog/MobileLog;->access$000(Lcom/mediatek/mobilelog/MobileLog;Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "com.mediatek.mobilelog.APLogger_STOP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLog$2;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    const/4 v2, 0x0

    # invokes: Lcom/mediatek/mobilelog/MobileLog;->updateUI(Z)V
    invoke-static {v1, v2}, Lcom/mediatek/mobilelog/MobileLog;->access$000(Lcom/mediatek/mobilelog/MobileLog;Z)V

    goto :goto_0
.end method
