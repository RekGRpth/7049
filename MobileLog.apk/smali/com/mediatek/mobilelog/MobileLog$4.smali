.class Lcom/mediatek/mobilelog/MobileLog$4;
.super Ljava/lang/Object;
.source "MobileLog.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/mobilelog/MobileLog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/mobilelog/MobileLog;


# direct methods
.method constructor <init>(Lcom/mediatek/mobilelog/MobileLog;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/mobilelog/MobileLog$4;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3
    .param p1    # Landroid/widget/CompoundButton;
    .param p2    # Z

    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLog$4;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    # getter for: Lcom/mediatek/mobilelog/MobileLog;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;
    invoke-static {v1}, Lcom/mediatek/mobilelog/MobileLog;->access$100(Lcom/mediatek/mobilelog/MobileLog;)Lcom/mediatek/mobilelog/LogSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/mobilelog/LogSettings;->loadConfig()Ljava/util/Properties;

    move-result-object v0

    const/4 v1, 0x1

    if-ne p2, v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLog$4;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    # getter for: Lcom/mediatek/mobilelog/MobileLog;->mAndroidLog:Ljava/lang/String;
    invoke-static {v1}, Lcom/mediatek/mobilelog/MobileLog;->access$400(Lcom/mediatek/mobilelog/MobileLog;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/mobilelog/MobileLog$4;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    # getter for: Lcom/mediatek/mobilelog/MobileLog;->mLogChecked:Ljava/lang/String;
    invoke-static {v2}, Lcom/mediatek/mobilelog/MobileLog;->access$500(Lcom/mediatek/mobilelog/MobileLog;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLog$4;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    # getter for: Lcom/mediatek/mobilelog/MobileLog;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;
    invoke-static {v1}, Lcom/mediatek/mobilelog/MobileLog;->access$100(Lcom/mediatek/mobilelog/MobileLog;)Lcom/mediatek/mobilelog/LogSettings;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/mediatek/mobilelog/LogSettings;->saveConfig(Ljava/util/Properties;)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLog$4;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    # getter for: Lcom/mediatek/mobilelog/MobileLog;->mAndroidLog:Ljava/lang/String;
    invoke-static {v1}, Lcom/mediatek/mobilelog/MobileLog;->access$400(Lcom/mediatek/mobilelog/MobileLog;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/mobilelog/MobileLog$4;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    # getter for: Lcom/mediatek/mobilelog/MobileLog;->mLogUnChecked:Ljava/lang/String;
    invoke-static {v2}, Lcom/mediatek/mobilelog/MobileLog;->access$600(Lcom/mediatek/mobilelog/MobileLog;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
