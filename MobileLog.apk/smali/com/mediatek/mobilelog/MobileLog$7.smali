.class Lcom/mediatek/mobilelog/MobileLog$7;
.super Ljava/lang/Object;
.source "MobileLog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/mobilelog/MobileLog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/mobilelog/MobileLog;


# direct methods
.method constructor <init>(Lcom/mediatek/mobilelog/MobileLog;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/mobilelog/MobileLog$7;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11
    .param p1    # Landroid/view/View;

    const v10, 0x7f070010

    const v7, 0x7f07000f

    const/4 v9, 0x1

    const/4 v8, 0x0

    iget-object v4, p0, Lcom/mediatek/mobilelog/MobileLog$7;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    # getter for: Lcom/mediatek/mobilelog/MobileLog;->mCheckBoxAndroidLog:Landroid/widget/CheckBox;
    invoke-static {v4}, Lcom/mediatek/mobilelog/MobileLog;->access$900(Lcom/mediatek/mobilelog/MobileLog;)Landroid/widget/CheckBox;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/mediatek/mobilelog/MobileLog$7;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    # getter for: Lcom/mediatek/mobilelog/MobileLog;->mCheckBoxKernelLog:Landroid/widget/CheckBox;
    invoke-static {v4}, Lcom/mediatek/mobilelog/MobileLog;->access$1000(Lcom/mediatek/mobilelog/MobileLog;)Landroid/widget/CheckBox;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/mediatek/mobilelog/MobileLog$7;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    # getter for: Lcom/mediatek/mobilelog/MobileLog;->mCheckBoxBTLog:Landroid/widget/CheckBox;
    invoke-static {v4}, Lcom/mediatek/mobilelog/MobileLog;->access$1100(Lcom/mediatek/mobilelog/MobileLog;)Landroid/widget/CheckBox;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/mediatek/mobilelog/MobileLog$7;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    const-string v5, "You should select at least one type of Log!"

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :cond_0
    const/4 v2, 0x0

    :try_start_0
    iget-object v4, p0, Lcom/mediatek/mobilelog/MobileLog$7;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    # getter for: Lcom/mediatek/mobilelog/MobileLog;->mTextbox:Landroid/widget/EditText;
    invoke-static {v4}, Lcom/mediatek/mobilelog/MobileLog;->access$1200(Lcom/mediatek/mobilelog/MobileLog;)Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    if-gez v2, :cond_1

    new-instance v4, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLog$7;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    invoke-direct {v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v5, 0x7f070010

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f07000f

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const-string v5, "Ok"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v4, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLog$7;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    invoke-direct {v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v10}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const-string v5, "Ok"

    invoke-virtual {v4, v5, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    :cond_1
    if-nez v2, :cond_2

    :try_start_1
    const-string v4, "300"

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v2

    :cond_2
    iget-object v4, p0, Lcom/mediatek/mobilelog/MobileLog$7;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    # getter for: Lcom/mediatek/mobilelog/MobileLog;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;
    invoke-static {v4}, Lcom/mediatek/mobilelog/MobileLog;->access$100(Lcom/mediatek/mobilelog/MobileLog;)Lcom/mediatek/mobilelog/LogSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mediatek/mobilelog/LogSettings;->loadConfig()Ljava/util/Properties;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/mobilelog/MobileLog$7;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    const-string v5, "log_path_type"

    invoke-virtual {v3, v5}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    # setter for: Lcom/mediatek/mobilelog/MobileLog;->mLogPathType:Ljava/lang/String;
    invoke-static {v4, v5}, Lcom/mediatek/mobilelog/MobileLog;->access$1302(Lcom/mediatek/mobilelog/MobileLog;Ljava/lang/String;)Ljava/lang/String;

    iget-object v4, p0, Lcom/mediatek/mobilelog/MobileLog$7;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    # getter for: Lcom/mediatek/mobilelog/MobileLog;->mLogPathType:Ljava/lang/String;
    invoke-static {v4}, Lcom/mediatek/mobilelog/MobileLog;->access$1300(Lcom/mediatek/mobilelog/MobileLog;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/mediatek/mobilelog/MobileLog$7;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    # getter for: Lcom/mediatek/mobilelog/MobileLog;->mLogPath:Ljava/lang/String;
    invoke-static {v4}, Lcom/mediatek/mobilelog/MobileLog;->access$1400(Lcom/mediatek/mobilelog/MobileLog;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_4

    :cond_3
    iget-object v4, p0, Lcom/mediatek/mobilelog/MobileLog$7;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLog$7;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    # getter for: Lcom/mediatek/mobilelog/MobileLog;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;
    invoke-static {v5}, Lcom/mediatek/mobilelog/MobileLog;->access$100(Lcom/mediatek/mobilelog/MobileLog;)Lcom/mediatek/mobilelog/LogSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mediatek/mobilelog/LogSettings;->getLogPathType()Ljava/lang/String;

    move-result-object v5

    # setter for: Lcom/mediatek/mobilelog/MobileLog;->mLogPathType:Ljava/lang/String;
    invoke-static {v4, v5}, Lcom/mediatek/mobilelog/MobileLog;->access$1302(Lcom/mediatek/mobilelog/MobileLog;Ljava/lang/String;)Ljava/lang/String;

    iget-object v4, p0, Lcom/mediatek/mobilelog/MobileLog$7;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLog$7;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    # getter for: Lcom/mediatek/mobilelog/MobileLog;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;
    invoke-static {v5}, Lcom/mediatek/mobilelog/MobileLog;->access$100(Lcom/mediatek/mobilelog/MobileLog;)Lcom/mediatek/mobilelog/LogSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mediatek/mobilelog/LogSettings;->getLogPath()Ljava/lang/String;

    move-result-object v5

    # setter for: Lcom/mediatek/mobilelog/MobileLog;->mLogPath:Ljava/lang/String;
    invoke-static {v4, v5}, Lcom/mediatek/mobilelog/MobileLog;->access$1402(Lcom/mediatek/mobilelog/MobileLog;Ljava/lang/String;)Ljava/lang/String;

    iget-object v4, p0, Lcom/mediatek/mobilelog/MobileLog$7;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    # getter for: Lcom/mediatek/mobilelog/MobileLog;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;
    invoke-static {v4}, Lcom/mediatek/mobilelog/MobileLog;->access$100(Lcom/mediatek/mobilelog/MobileLog;)Lcom/mediatek/mobilelog/LogSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mediatek/mobilelog/LogSettings;->recreateConfig()V

    iget-object v4, p0, Lcom/mediatek/mobilelog/MobileLog$7;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    # getter for: Lcom/mediatek/mobilelog/MobileLog;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;
    invoke-static {v4}, Lcom/mediatek/mobilelog/MobileLog;->access$100(Lcom/mediatek/mobilelog/MobileLog;)Lcom/mediatek/mobilelog/LogSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mediatek/mobilelog/LogSettings;->loadConfig()Ljava/util/Properties;

    move-result-object v3

    :cond_4
    iget-object v4, p0, Lcom/mediatek/mobilelog/MobileLog$7;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLog$7;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    # getter for: Lcom/mediatek/mobilelog/MobileLog;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;
    invoke-static {v5}, Lcom/mediatek/mobilelog/MobileLog;->access$100(Lcom/mediatek/mobilelog/MobileLog;)Lcom/mediatek/mobilelog/LogSettings;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLog$7;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    invoke-virtual {v6}, Lcom/mediatek/mobilelog/MobileLog;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/mediatek/mobilelog/LogSettings;->getRightPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    # setter for: Lcom/mediatek/mobilelog/MobileLog;->mLogPath:Ljava/lang/String;
    invoke-static {v4, v5}, Lcom/mediatek/mobilelog/MobileLog;->access$1402(Lcom/mediatek/mobilelog/MobileLog;Ljava/lang/String;)Ljava/lang/String;

    iget-object v4, p0, Lcom/mediatek/mobilelog/MobileLog$7;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    # getter for: Lcom/mediatek/mobilelog/MobileLog;->mLogPathType:Ljava/lang/String;
    invoke-static {v4}, Lcom/mediatek/mobilelog/MobileLog;->access$1300(Lcom/mediatek/mobilelog/MobileLog;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLog$7;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    # getter for: Lcom/mediatek/mobilelog/MobileLog;->mLogPathTypeSdCard:Ljava/lang/String;
    invoke-static {v5}, Lcom/mediatek/mobilelog/MobileLog;->access$1500(Lcom/mediatek/mobilelog/MobileLog;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    const-string v4, "mounted"

    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLog$7;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    # getter for: Lcom/mediatek/mobilelog/MobileLog;->exStorageManager:Landroid/os/storage/StorageManager;
    invoke-static {v5}, Lcom/mediatek/mobilelog/MobileLog;->access$1600(Lcom/mediatek/mobilelog/MobileLog;)Landroid/os/storage/StorageManager;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLog$7;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    # getter for: Lcom/mediatek/mobilelog/MobileLog;->mLogPath:Ljava/lang/String;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLog;->access$1400(Lcom/mediatek/mobilelog/MobileLog;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    new-instance v4, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLog$7;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    invoke-direct {v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v5, "ERROR"

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "No SD Card:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLog$7;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    # getter for: Lcom/mediatek/mobilelog/MobileLog;->mLogPath:Ljava/lang/String;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLog;->access$1400(Lcom/mediatek/mobilelog/MobileLog;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const-string v5, "Ok"

    invoke-virtual {v4, v5, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_0

    :cond_5
    int-to-long v4, v2

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLog$7;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    # invokes: Lcom/mediatek/mobilelog/MobileLog;->getAvailableExternalMemorySize()J
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLog;->access$1700(Lcom/mediatek/mobilelog/MobileLog;)J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-ltz v4, :cond_6

    new-instance v4, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLog$7;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    invoke-direct {v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v5, "ERROR"

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "No Enough Space on SD card:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLog$7;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    # getter for: Lcom/mediatek/mobilelog/MobileLog;->mLogPath:Ljava/lang/String;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLog;->access$1400(Lcom/mediatek/mobilelog/MobileLog;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const-string v5, "Ok"

    invoke-virtual {v4, v5, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_0

    :cond_6
    if-nez v2, :cond_7

    iget-object v4, p0, Lcom/mediatek/mobilelog/MobileLog$7;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    # invokes: Lcom/mediatek/mobilelog/MobileLog;->getAvailableExternalMemorySize()J
    invoke-static {v4}, Lcom/mediatek/mobilelog/MobileLog;->access$1700(Lcom/mediatek/mobilelog/MobileLog;)J

    move-result-wide v4

    long-to-int v4, v4

    add-int/lit8 v2, v4, -0x1

    :cond_7
    :goto_1
    iget-object v4, p0, Lcom/mediatek/mobilelog/MobileLog$7;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    # getter for: Lcom/mediatek/mobilelog/MobileLog;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v4}, Lcom/mediatek/mobilelog/MobileLog;->access$1900(Lcom/mediatek/mobilelog/MobileLog;)Landroid/widget/CheckBox;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/widget/CheckBox;->setChecked(Z)V

    const-string v4, "service_boot_option"

    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLog$7;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    # getter for: Lcom/mediatek/mobilelog/MobileLog;->mServiceOnBootEnabled:Ljava/lang/String;
    invoke-static {v5}, Lcom/mediatek/mobilelog/MobileLog;->access$200(Lcom/mediatek/mobilelog/MobileLog;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v4, "service_status"

    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLog$7;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    # getter for: Lcom/mediatek/mobilelog/MobileLog;->mServiceRunning:Ljava/lang/String;
    invoke-static {v5}, Lcom/mediatek/mobilelog/MobileLog;->access$2000(Lcom/mediatek/mobilelog/MobileLog;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v4, "log_size"

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v4, p0, Lcom/mediatek/mobilelog/MobileLog$7;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    # getter for: Lcom/mediatek/mobilelog/MobileLog;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;
    invoke-static {v4}, Lcom/mediatek/mobilelog/MobileLog;->access$100(Lcom/mediatek/mobilelog/MobileLog;)Lcom/mediatek/mobilelog/LogSettings;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/mediatek/mobilelog/LogSettings;->saveConfig(Ljava/util/Properties;)V

    new-instance v1, Landroid/content/Intent;

    const-string v4, "com.mediatek.mobilelog.MobileLogService"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "com.mediatek.mobilelog.LOGPATH"

    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLog$7;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    # getter for: Lcom/mediatek/mobilelog/MobileLog;->mLogPath:Ljava/lang/String;
    invoke-static {v5}, Lcom/mediatek/mobilelog/MobileLog;->access$1400(Lcom/mediatek/mobilelog/MobileLog;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "com.mediatek.mobilelog.LOGPATHTYPE"

    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLog$7;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    # getter for: Lcom/mediatek/mobilelog/MobileLog;->mLogPathType:Ljava/lang/String;
    invoke-static {v5}, Lcom/mediatek/mobilelog/MobileLog;->access$1300(Lcom/mediatek/mobilelog/MobileLog;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "com.mediatek.Service.type"

    const-string v5, "com.mediatek.Service.normal"

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "com.mediatek.mobilelog.LOGSIZE"

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v4, p0, Lcom/mediatek/mobilelog/MobileLog$7;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    invoke-virtual {v4}, Lcom/mediatek/mobilelog/MobileLog;->getBaseContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    iget-object v4, p0, Lcom/mediatek/mobilelog/MobileLog$7;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    # invokes: Lcom/mediatek/mobilelog/MobileLog;->updateUI(Z)V
    invoke-static {v4, v9}, Lcom/mediatek/mobilelog/MobileLog;->access$000(Lcom/mediatek/mobilelog/MobileLog;Z)V

    goto/16 :goto_0

    :cond_8
    int-to-long v4, v2

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLog$7;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    # invokes: Lcom/mediatek/mobilelog/MobileLog;->getAvailableInternalMemorySize()J
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLog;->access$1800(Lcom/mediatek/mobilelog/MobileLog;)J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-ltz v4, :cond_9

    new-instance v4, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLog$7;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    invoke-direct {v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v5, "ERROR"

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const-string v5, "No Enough Space on Phone"

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const-string v5, "Ok"

    invoke-virtual {v4, v5, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_0

    :cond_9
    if-nez v2, :cond_7

    iget-object v4, p0, Lcom/mediatek/mobilelog/MobileLog$7;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    # invokes: Lcom/mediatek/mobilelog/MobileLog;->getAvailableInternalMemorySize()J
    invoke-static {v4}, Lcom/mediatek/mobilelog/MobileLog;->access$1800(Lcom/mediatek/mobilelog/MobileLog;)J

    move-result-wide v4

    long-to-int v4, v4

    add-int/lit8 v2, v4, -0x1

    goto/16 :goto_1
.end method
