.class Lcom/mediatek/mobilelog/MobileLog$8;
.super Ljava/lang/Object;
.source "MobileLog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/mobilelog/MobileLog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/mobilelog/MobileLog;


# direct methods
.method constructor <init>(Lcom/mediatek/mobilelog/MobileLog;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/mobilelog/MobileLog$8;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/mediatek/mobilelog/MobileLog$8;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    # invokes: Lcom/mediatek/mobilelog/MobileLog;->monkey_running()Z
    invoke-static {v2}, Lcom/mediatek/mobilelog/MobileLog;->access$2100(Lcom/mediatek/mobilelog/MobileLog;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/mobilelog/MobileLog$8;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    # getter for: Lcom/mediatek/mobilelog/MobileLog;->mButtonStop:Landroid/widget/Button;
    invoke-static {v2}, Lcom/mediatek/mobilelog/MobileLog;->access$2200(Lcom/mediatek/mobilelog/MobileLog;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setEnabled(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/mediatek/mobilelog/MobileLog$8;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    # getter for: Lcom/mediatek/mobilelog/MobileLog;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;
    invoke-static {v2}, Lcom/mediatek/mobilelog/MobileLog;->access$100(Lcom/mediatek/mobilelog/MobileLog;)Lcom/mediatek/mobilelog/LogSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/mobilelog/LogSettings;->loadConfig()Ljava/util/Properties;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/mobilelog/MobileLog$8;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    # getter for: Lcom/mediatek/mobilelog/MobileLog;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/mediatek/mobilelog/MobileLog;->access$1900(Lcom/mediatek/mobilelog/MobileLog;)Landroid/widget/CheckBox;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    const-string v2, "service_boot_option"

    iget-object v3, p0, Lcom/mediatek/mobilelog/MobileLog$8;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    # getter for: Lcom/mediatek/mobilelog/MobileLog;->mServiceOnBootDisabled:Ljava/lang/String;
    invoke-static {v3}, Lcom/mediatek/mobilelog/MobileLog;->access$300(Lcom/mediatek/mobilelog/MobileLog;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lcom/mediatek/mobilelog/MobileLog$8;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    # getter for: Lcom/mediatek/mobilelog/MobileLog;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;
    invoke-static {v2}, Lcom/mediatek/mobilelog/MobileLog;->access$100(Lcom/mediatek/mobilelog/MobileLog;)Lcom/mediatek/mobilelog/LogSettings;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/mediatek/mobilelog/LogSettings;->saveConfig(Ljava/util/Properties;)V

    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.mediatek.mobilelog.MobileLogService"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/mobilelog/MobileLog$8;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    invoke-virtual {v2}, Lcom/mediatek/mobilelog/MobileLog;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    iget-object v2, p0, Lcom/mediatek/mobilelog/MobileLog$8;->this$0:Lcom/mediatek/mobilelog/MobileLog;

    # invokes: Lcom/mediatek/mobilelog/MobileLog;->updateUI(Z)V
    invoke-static {v2, v4}, Lcom/mediatek/mobilelog/MobileLog;->access$000(Lcom/mediatek/mobilelog/MobileLog;Z)V

    goto :goto_0
.end method
