.class public Lcom/mediatek/mobilelog/MobileLog;
.super Landroid/app/Activity;
.source "MobileLog.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MobileLogJ/Activity"


# instance fields
.field private exStorageManager:Landroid/os/storage/StorageManager;

.field private isRunning:Z

.field private mAndroidLog:Ljava/lang/String;

.field private mBTLog:Ljava/lang/String;

.field private mBindListener:Landroid/view/View$OnClickListener;

.field private mBootOption:Ljava/lang/String;

.field private mBtnShowSpaceListener:Landroid/view/View$OnClickListener;

.field private mButtonClearLog:Landroid/widget/Button;

.field private mButtonShowSpace:Landroid/widget/Button;

.field private mButtonStart:Landroid/widget/Button;

.field private mButtonStop:Landroid/widget/Button;

.field private mCheckBox:Landroid/widget/CheckBox;

.field private mCheckBoxAndroidListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private mCheckBoxAndroidLog:Landroid/widget/CheckBox;

.field private mCheckBoxBTListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private mCheckBoxBTLog:Landroid/widget/CheckBox;

.field private mCheckBoxKernelListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private mCheckBoxKernelLog:Landroid/widget/CheckBox;

.field private mCheckBoxListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private mClearLogs:Landroid/view/View$OnClickListener;

.field private mIntentReceiverService:Landroid/content/BroadcastReceiver;

.field private mKernelLog:Ljava/lang/String;

.field private mLogChecked:Ljava/lang/String;

.field private mLogPath:Ljava/lang/String;

.field private mLogPathType:Ljava/lang/String;

.field private mLogPathTypePhone:Ljava/lang/String;

.field private mLogPathTypeSdCard:Ljava/lang/String;

.field private mLogSettings:Lcom/mediatek/mobilelog/LogSettings;

.field private mLogUnChecked:Ljava/lang/String;

.field private mServiceOnBootDisabled:Ljava/lang/String;

.field private mServiceOnBootEnabled:Ljava/lang/String;

.field private mServiceReceiver:Landroid/content/BroadcastReceiver;

.field private mServiceRunning:Ljava/lang/String;

.field private mServiceStopped:Ljava/lang/String;

.field private mTextbox:Landroid/widget/EditText;

.field private mUnbindListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/mobilelog/MobileLog;->isRunning:Z

    const-string v0, "service_running"

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mServiceRunning:Ljava/lang/String;

    const-string v0, "service_stopped"

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mServiceStopped:Ljava/lang/String;

    const-string v0, "true"

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mServiceOnBootEnabled:Ljava/lang/String;

    const-string v0, "false"

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mServiceOnBootDisabled:Ljava/lang/String;

    const-string v0, "/mnt/sdcard"

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mLogPath:Ljava/lang/String;

    const-string v0, "phone"

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mLogPathTypePhone:Ljava/lang/String;

    const-string v0, "sdcard"

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mLogPathTypeSdCard:Ljava/lang/String;

    const-string v0, "AndroidLog"

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mAndroidLog:Ljava/lang/String;

    const-string v0, "KernelLog"

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mKernelLog:Ljava/lang/String;

    const-string v0, "BTLog"

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mBTLog:Ljava/lang/String;

    const-string v0, "true"

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mLogChecked:Ljava/lang/String;

    const-string v0, "false"

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mLogUnChecked:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->exStorageManager:Landroid/os/storage/StorageManager;

    new-instance v0, Lcom/mediatek/mobilelog/MobileLog$1;

    invoke-direct {v0, p0}, Lcom/mediatek/mobilelog/MobileLog$1;-><init>(Lcom/mediatek/mobilelog/MobileLog;)V

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mIntentReceiverService:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/mediatek/mobilelog/MobileLog$2;

    invoke-direct {v0, p0}, Lcom/mediatek/mobilelog/MobileLog$2;-><init>(Lcom/mediatek/mobilelog/MobileLog;)V

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mServiceReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/mediatek/mobilelog/MobileLog$3;

    invoke-direct {v0, p0}, Lcom/mediatek/mobilelog/MobileLog$3;-><init>(Lcom/mediatek/mobilelog/MobileLog;)V

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mCheckBoxListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    new-instance v0, Lcom/mediatek/mobilelog/MobileLog$4;

    invoke-direct {v0, p0}, Lcom/mediatek/mobilelog/MobileLog$4;-><init>(Lcom/mediatek/mobilelog/MobileLog;)V

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mCheckBoxAndroidListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    new-instance v0, Lcom/mediatek/mobilelog/MobileLog$5;

    invoke-direct {v0, p0}, Lcom/mediatek/mobilelog/MobileLog$5;-><init>(Lcom/mediatek/mobilelog/MobileLog;)V

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mCheckBoxKernelListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    new-instance v0, Lcom/mediatek/mobilelog/MobileLog$6;

    invoke-direct {v0, p0}, Lcom/mediatek/mobilelog/MobileLog$6;-><init>(Lcom/mediatek/mobilelog/MobileLog;)V

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mCheckBoxBTListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    new-instance v0, Lcom/mediatek/mobilelog/MobileLog$7;

    invoke-direct {v0, p0}, Lcom/mediatek/mobilelog/MobileLog$7;-><init>(Lcom/mediatek/mobilelog/MobileLog;)V

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mBindListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/mediatek/mobilelog/MobileLog$8;

    invoke-direct {v0, p0}, Lcom/mediatek/mobilelog/MobileLog$8;-><init>(Lcom/mediatek/mobilelog/MobileLog;)V

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mUnbindListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/mediatek/mobilelog/MobileLog$9;

    invoke-direct {v0, p0}, Lcom/mediatek/mobilelog/MobileLog$9;-><init>(Lcom/mediatek/mobilelog/MobileLog;)V

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mBtnShowSpaceListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/mediatek/mobilelog/MobileLog$10;

    invoke-direct {v0, p0}, Lcom/mediatek/mobilelog/MobileLog$10;-><init>(Lcom/mediatek/mobilelog/MobileLog;)V

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mClearLogs:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/mobilelog/MobileLog;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/mobilelog/MobileLog;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/mobilelog/MobileLog;->updateUI(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/mobilelog/MobileLog;)Lcom/mediatek/mobilelog/LogSettings;
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/MobileLog;

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/mediatek/mobilelog/MobileLog;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/MobileLog;

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mCheckBoxKernelLog:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/mediatek/mobilelog/MobileLog;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/MobileLog;

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mCheckBoxBTLog:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/mediatek/mobilelog/MobileLog;)Landroid/widget/EditText;
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/MobileLog;

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mTextbox:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/mediatek/mobilelog/MobileLog;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/MobileLog;

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mLogPathType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/mediatek/mobilelog/MobileLog;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/mobilelog/MobileLog;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/mobilelog/MobileLog;->mLogPathType:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/mediatek/mobilelog/MobileLog;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/MobileLog;

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mLogPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/mediatek/mobilelog/MobileLog;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/mobilelog/MobileLog;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/mobilelog/MobileLog;->mLogPath:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/mediatek/mobilelog/MobileLog;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/MobileLog;

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mLogPathTypeSdCard:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/mediatek/mobilelog/MobileLog;)Landroid/os/storage/StorageManager;
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/MobileLog;

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->exStorageManager:Landroid/os/storage/StorageManager;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/mediatek/mobilelog/MobileLog;)J
    .locals 2
    .param p0    # Lcom/mediatek/mobilelog/MobileLog;

    invoke-direct {p0}, Lcom/mediatek/mobilelog/MobileLog;->getAvailableExternalMemorySize()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$1800(Lcom/mediatek/mobilelog/MobileLog;)J
    .locals 2
    .param p0    # Lcom/mediatek/mobilelog/MobileLog;

    invoke-direct {p0}, Lcom/mediatek/mobilelog/MobileLog;->getAvailableInternalMemorySize()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$1900(Lcom/mediatek/mobilelog/MobileLog;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/MobileLog;

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/mobilelog/MobileLog;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/MobileLog;

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mServiceOnBootEnabled:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/mediatek/mobilelog/MobileLog;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/MobileLog;

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mServiceRunning:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/mediatek/mobilelog/MobileLog;)Z
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/MobileLog;

    invoke-direct {p0}, Lcom/mediatek/mobilelog/MobileLog;->monkey_running()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2200(Lcom/mediatek/mobilelog/MobileLog;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/MobileLog;

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mButtonStop:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/mediatek/mobilelog/MobileLog;)V
    .locals 0
    .param p0    # Lcom/mediatek/mobilelog/MobileLog;

    invoke-direct {p0}, Lcom/mediatek/mobilelog/MobileLog;->clearOldLog()V

    return-void
.end method

.method static synthetic access$2400(Lcom/mediatek/mobilelog/MobileLog;Ljava/io/File;)Z
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/MobileLog;
    .param p1    # Ljava/io/File;

    invoke-direct {p0, p1}, Lcom/mediatek/mobilelog/MobileLog;->clearLogs(Ljava/io/File;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/mediatek/mobilelog/MobileLog;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/MobileLog;

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mServiceOnBootDisabled:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/mobilelog/MobileLog;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/MobileLog;

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mAndroidLog:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mediatek/mobilelog/MobileLog;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/MobileLog;

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mLogChecked:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/mediatek/mobilelog/MobileLog;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/MobileLog;

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mLogUnChecked:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/mediatek/mobilelog/MobileLog;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/MobileLog;

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mKernelLog:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/mediatek/mobilelog/MobileLog;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/MobileLog;

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mBTLog:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900(Lcom/mediatek/mobilelog/MobileLog;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/MobileLog;

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mCheckBoxAndroidLog:Landroid/widget/CheckBox;

    return-object v0
.end method

.method private clearLogs(Ljava/io/File;)Z
    .locals 8
    .param p1    # Ljava/io/File;

    const/4 v5, 0x0

    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "MobileLogJ/Activity"

    const-string v7, "the file you delete is not exist!!!"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v5

    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    if-nez v2, :cond_1

    const-string v6, "MobileLogJ/Activity"

    const-string v7, "listFiles() returns null"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    move-object v0, v2

    array-length v4, v0

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v4, :cond_4

    aget-object v1, v0, v3

    const-string v5, "MobileLogJ/Activity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Clear file: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_2
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-direct {p0, v1}, Lcom/mediatek/mobilelog/MobileLog;->clearLogs(Ljava/io/File;)Z

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    goto :goto_2

    :cond_4
    const/4 v5, 0x1

    goto :goto_0
.end method

.method private clearOldLog()V
    .locals 4

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f070028

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f070029

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const-string v2, "OK"

    new-instance v3, Lcom/mediatek/mobilelog/MobileLog$12;

    invoke-direct {v3, p0}, Lcom/mediatek/mobilelog/MobileLog$12;-><init>(Lcom/mediatek/mobilelog/MobileLog;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const-string v2, "Cancel"

    new-instance v3, Lcom/mediatek/mobilelog/MobileLog$11;

    invoke-direct {v3, p0}, Lcom/mediatek/mobilelog/MobileLog$11;-><init>(Lcom/mediatek/mobilelog/MobileLog;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method private getAvailableExternalMemorySize()J
    .locals 9

    const-string v5, "mounted"

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLog;->exStorageManager:Landroid/os/storage/StorageManager;

    iget-object v7, p0, Lcom/mediatek/mobilelog/MobileLog;->mLogPath:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    new-instance v4, Landroid/os/StatFs;

    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLog;->mLogPath:Ljava/lang/String;

    invoke-direct {v4, v5}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Landroid/os/StatFs;->getBlockSize()I

    move-result v5

    int-to-long v2, v5

    invoke-virtual {v4}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v5

    int-to-long v0, v5

    mul-long v5, v0, v2

    const-wide/32 v7, 0x100000

    div-long/2addr v5, v7

    :goto_0
    return-wide v5

    :cond_0
    const-wide/16 v5, 0x0

    goto :goto_0
.end method

.method private getAvailableInternalMemorySize()J
    .locals 10

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v4

    new-instance v5, Landroid/os/StatFs;

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Landroid/os/StatFs;->getBlockSize()I

    move-result v6

    int-to-long v2, v6

    invoke-virtual {v5}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v6

    int-to-long v0, v6

    mul-long v6, v0, v2

    const-wide/32 v8, 0x100000

    div-long/2addr v6, v8

    return-wide v6
.end method

.method private monkey_running()Z
    .locals 3

    const-string v1, "ro.monkey"

    const-string v2, "false"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "MobileLogJ/Activity"

    const-string v2, "monkey is running !!!"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private updateUI(Z)V
    .locals 4
    .param p1    # Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/mediatek/mobilelog/MobileLog;->mButtonStart:Landroid/widget/Button;

    if-nez p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v3, p0, Lcom/mediatek/mobilelog/MobileLog;->mButtonClearLog:Landroid/widget/Button;

    if-nez p1, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v3, p0, Lcom/mediatek/mobilelog/MobileLog;->mTextbox:Landroid/widget/EditText;

    if-nez p1, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-object v3, p0, Lcom/mediatek/mobilelog/MobileLog;->mCheckBox:Landroid/widget/CheckBox;

    if-nez p1, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setEnabled(Z)V

    iget-object v3, p0, Lcom/mediatek/mobilelog/MobileLog;->mCheckBoxAndroidLog:Landroid/widget/CheckBox;

    if-nez p1, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setEnabled(Z)V

    iget-object v3, p0, Lcom/mediatek/mobilelog/MobileLog;->mCheckBoxKernelLog:Landroid/widget/CheckBox;

    if-nez p1, :cond_5

    move v0, v1

    :goto_5
    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mCheckBoxBTLog:Landroid/widget/CheckBox;

    if-nez p1, :cond_6

    :goto_6
    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mButtonStop:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_4

    :cond_5
    move v0, v2

    goto :goto_5

    :cond_6
    move v1, v2

    goto :goto_6
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const v3, 0x7f080011

    const-string v1, "persist.radio.mobilelog.enable"

    const-string v2, "1"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const-string v1, "MobileLogJ/Activity"

    const-string v2, "MobileLog Version: 1.4.110418"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const v1, 0x7f030004

    invoke-virtual {p0, v1}, Lcom/mediatek/mobilelog/MobileLog;->setContentView(I)V

    new-instance v1, Lcom/mediatek/mobilelog/LogSettings;

    const-string v2, "/data/data/com.mediatek.mobilelog/files"

    invoke-direct {v1, v2}, Lcom/mediatek/mobilelog/LogSettings;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/mediatek/mobilelog/MobileLog;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;

    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLog;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;

    invoke-virtual {v1}, Lcom/mediatek/mobilelog/LogSettings;->loadConfig()Ljava/util/Properties;

    move-result-object v0

    const v1, 0x7f080015

    invoke-virtual {p0, v1}, Lcom/mediatek/mobilelog/MobileLog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/mediatek/mobilelog/MobileLog;->mCheckBox:Landroid/widget/CheckBox;

    const v1, 0x7f080019

    invoke-virtual {p0, v1}, Lcom/mediatek/mobilelog/MobileLog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/mobilelog/MobileLog;->mButtonStart:Landroid/widget/Button;

    const v1, 0x7f08001a

    invoke-virtual {p0, v1}, Lcom/mediatek/mobilelog/MobileLog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/mobilelog/MobileLog;->mButtonStop:Landroid/widget/Button;

    const v1, 0x7f080014

    invoke-virtual {p0, v1}, Lcom/mediatek/mobilelog/MobileLog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/mobilelog/MobileLog;->mButtonShowSpace:Landroid/widget/Button;

    const v1, 0x7f080013

    invoke-virtual {p0, v1}, Lcom/mediatek/mobilelog/MobileLog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/mediatek/mobilelog/MobileLog;->mTextbox:Landroid/widget/EditText;

    const v1, 0x7f08001b

    invoke-virtual {p0, v1}, Lcom/mediatek/mobilelog/MobileLog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/mobilelog/MobileLog;->mButtonClearLog:Landroid/widget/Button;

    invoke-virtual {p0, v3}, Lcom/mediatek/mobilelog/MobileLog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v3}, Lcom/mediatek/mobilelog/MobileLog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLog;->mCheckBox:Landroid/widget/CheckBox;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLog;->mButtonStart:Landroid/widget/Button;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLog;->mButtonStop:Landroid/widget/Button;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLog;->mButtonShowSpace:Landroid/widget/Button;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLog;->mButtonClearLog:Landroid/widget/Button;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLog;->mCheckBox:Landroid/widget/CheckBox;

    iget-object v2, p0, Lcom/mediatek/mobilelog/MobileLog;->mCheckBoxListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLog;->mButtonStart:Landroid/widget/Button;

    iget-object v2, p0, Lcom/mediatek/mobilelog/MobileLog;->mBindListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLog;->mButtonStop:Landroid/widget/Button;

    iget-object v2, p0, Lcom/mediatek/mobilelog/MobileLog;->mUnbindListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLog;->mButtonShowSpace:Landroid/widget/Button;

    iget-object v2, p0, Lcom/mediatek/mobilelog/MobileLog;->mBtnShowSpaceListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLog;->mButtonClearLog:Landroid/widget/Button;

    iget-object v2, p0, Lcom/mediatek/mobilelog/MobileLog;->mClearLogs:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    const v1, 0x7f080016

    invoke-virtual {p0, v1}, Lcom/mediatek/mobilelog/MobileLog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/mediatek/mobilelog/MobileLog;->mCheckBoxAndroidLog:Landroid/widget/CheckBox;

    const v1, 0x7f080017

    invoke-virtual {p0, v1}, Lcom/mediatek/mobilelog/MobileLog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/mediatek/mobilelog/MobileLog;->mCheckBoxKernelLog:Landroid/widget/CheckBox;

    const v1, 0x7f080018

    invoke-virtual {p0, v1}, Lcom/mediatek/mobilelog/MobileLog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/mediatek/mobilelog/MobileLog;->mCheckBoxBTLog:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLog;->mCheckBoxAndroidLog:Landroid/widget/CheckBox;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLog;->mCheckBoxKernelLog:Landroid/widget/CheckBox;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLog;->mCheckBoxBTLog:Landroid/widget/CheckBox;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLog;->mCheckBoxAndroidLog:Landroid/widget/CheckBox;

    iget-object v2, p0, Lcom/mediatek/mobilelog/MobileLog;->mCheckBoxAndroidListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLog;->mCheckBoxKernelLog:Landroid/widget/CheckBox;

    iget-object v2, p0, Lcom/mediatek/mobilelog/MobileLog;->mCheckBoxKernelListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLog;->mCheckBoxBTLog:Landroid/widget/CheckBox;

    iget-object v2, p0, Lcom/mediatek/mobilelog/MobileLog;->mCheckBoxBTListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    :cond_2
    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLog;->exStorageManager:Landroid/os/storage/StorageManager;

    if-nez v1, :cond_3

    const-string v1, "storage"

    invoke-virtual {p0, v1}, Lcom/mediatek/mobilelog/MobileLog;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/storage/StorageManager;

    iput-object v1, p0, Lcom/mediatek/mobilelog/MobileLog;->exStorageManager:Landroid/os/storage/StorageManager;

    :cond_3
    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLog;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;

    invoke-virtual {p0}, Lcom/mediatek/mobilelog/MobileLog;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/mediatek/mobilelog/LogSettings;->getRightPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/mobilelog/MobileLog;->mLogPath:Ljava/lang/String;

    const-string v1, "MobileLogJ/Activity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mLogPath first get: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/mobilelog/MobileLog;->mLogPath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected onPause()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mServiceReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/mediatek/mobilelog/MobileLog;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLog;->mIntentReceiverService:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/mediatek/mobilelog/MobileLog;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const-string v0, "MobileLogJ/Activity"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected onResume()V
    .locals 4

    const-string v2, "MobileLogJ/Activity"

    const-string v3, "onResume"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "com.mediatek.mobilelog.SDCARD_DAMAGE"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "com.mediatek.mobilelog.LOW_STORAGE"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "com.mediatek.mobilelog.SDCARD_ALMOST_FULL"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/mobilelog/MobileLog;->mIntentReceiverService:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v1}, Lcom/mediatek/mobilelog/MobileLog;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "com.mediatek.mobilelog.APLogger_START"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "com.mediatek.mobilelog.APLogger_STOP"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/mobilelog/MobileLog;->mServiceReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v0}, Lcom/mediatek/mobilelog/MobileLog;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    invoke-virtual {p0}, Lcom/mediatek/mobilelog/MobileLog;->updateUi()V

    return-void
.end method

.method protected onStop()V
    .locals 2

    const-string v0, "MobileLogJ/Activity"

    const-string v1, "MobileLog activity onStop"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method

.method public updateUi()V
    .locals 10

    const/4 v9, 0x1

    const/4 v8, 0x0

    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLog;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;

    invoke-virtual {v5}, Lcom/mediatek/mobilelog/LogSettings;->loadConfig()Ljava/util/Properties;

    move-result-object v4

    const-string v5, "service_boot_option"

    invoke-virtual {v4, v5}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/mediatek/mobilelog/MobileLog;->mBootOption:Ljava/lang/String;

    const-string v5, "log_path"

    invoke-virtual {v4, v5}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/mediatek/mobilelog/MobileLog;->mLogPath:Ljava/lang/String;

    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLog;->mBootOption:Ljava/lang/String;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLog;->mLogPath:Ljava/lang/String;

    if-nez v5, :cond_1

    :cond_0
    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLog;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;

    invoke-virtual {v5}, Lcom/mediatek/mobilelog/LogSettings;->getBootOption()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/mediatek/mobilelog/MobileLog;->mBootOption:Ljava/lang/String;

    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLog;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;

    invoke-virtual {v5}, Lcom/mediatek/mobilelog/LogSettings;->getLogPath()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/mediatek/mobilelog/MobileLog;->mLogPath:Ljava/lang/String;

    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLog;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;

    invoke-virtual {v5}, Lcom/mediatek/mobilelog/LogSettings;->recreateConfig()V

    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLog;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;

    invoke-virtual {v5}, Lcom/mediatek/mobilelog/LogSettings;->loadConfig()Ljava/util/Properties;

    move-result-object v4

    :cond_1
    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLog;->mTextbox:Landroid/widget/EditText;

    const-string v6, "log_size"

    const-string v7, "300"

    invoke-virtual {v4, v6, v7}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLog;->mAndroidLog:Ljava/lang/String;

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLog;->mLogChecked:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLog;->mKernelLog:Ljava/lang/String;

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLog;->mLogChecked:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLog;->mBTLog:Ljava/lang/String;

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLog;->mLogChecked:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v5, "true"

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLog;->mServiceOnBootEnabled:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLog;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v5, v9}, Landroid/widget/CheckBox;->setChecked(Z)V

    :goto_0
    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLog;->mLogChecked:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLog;->mCheckBoxAndroidLog:Landroid/widget/CheckBox;

    invoke-virtual {v5, v9}, Landroid/widget/CheckBox;->setChecked(Z)V

    :goto_1
    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLog;->mLogChecked:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLog;->mCheckBoxKernelLog:Landroid/widget/CheckBox;

    invoke-virtual {v5, v9}, Landroid/widget/CheckBox;->setChecked(Z)V

    :goto_2
    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLog;->mLogChecked:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLog;->mCheckBoxBTLog:Landroid/widget/CheckBox;

    invoke-virtual {v5, v9}, Landroid/widget/CheckBox;->setChecked(Z)V

    :goto_3
    const-string v5, "debug.MB.running"

    const-string v6, "0"

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "1"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    invoke-direct {p0, v3}, Lcom/mediatek/mobilelog/MobileLog;->updateUI(Z)V

    invoke-direct {p0}, Lcom/mediatek/mobilelog/MobileLog;->monkey_running()Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLog;->mButtonStop:Landroid/widget/Button;

    invoke-virtual {v5, v8}, Landroid/widget/Button;->setEnabled(Z)V

    :cond_2
    return-void

    :cond_3
    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLog;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v5, v8}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    :cond_4
    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLog;->mCheckBoxAndroidLog:Landroid/widget/CheckBox;

    invoke-virtual {v5, v8}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_1

    :cond_5
    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLog;->mCheckBoxKernelLog:Landroid/widget/CheckBox;

    invoke-virtual {v5, v8}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_2

    :cond_6
    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLog;->mCheckBoxBTLog:Landroid/widget/CheckBox;

    invoke-virtual {v5, v8}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_3
.end method
