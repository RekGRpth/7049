.class public Lcom/mediatek/mobilelog/MobileLogBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MobileLogBroadcastReceiver.java"


# static fields
.field static final BOOT_ACTION:Ljava/lang/String; = "android.intent.action.BOOT_COMPLETED"

.field static final EXTRA_COMMON_UI:Ljava/lang/String; = "com.meidatek.syslogger.action"

.field static final IPO_BOOT_ACTION:Ljava/lang/String; = "android.intent.action.ACTION_BOOT_IPO"

.field private static final TAG:Ljava/lang/String; = "MobileLogJ/Broadcast"

.field public static start_flag:Z


# instance fields
.field private mLogPath:Ljava/lang/String;

.field private mLogPathTypePhone:Ljava/lang/String;

.field private mLogPathTypeSdCard:Ljava/lang/String;

.field private mLogPathTypeTag:Ljava/lang/String;

.field mLogSettings:Lcom/mediatek/mobilelog/LogSettings;

.field private mServiceBootOptionTag:Ljava/lang/String;

.field private mServiceOnBootDisabled:Ljava/lang/String;

.field private mServiceOnBootEnabled:Ljava/lang/String;

.field private mServiceRunning:Ljava/lang/String;

.field private mServiceStatusTag:Ljava/lang/String;

.field private mServiceStopped:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/mediatek/mobilelog/MobileLogBroadcastReceiver;->start_flag:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    const-string v0, "service_boot_option"

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLogBroadcastReceiver;->mServiceBootOptionTag:Ljava/lang/String;

    const-string v0, "true"

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLogBroadcastReceiver;->mServiceOnBootEnabled:Ljava/lang/String;

    const-string v0, "false"

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLogBroadcastReceiver;->mServiceOnBootDisabled:Ljava/lang/String;

    const-string v0, "service_status"

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLogBroadcastReceiver;->mServiceStatusTag:Ljava/lang/String;

    const-string v0, "service_running"

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLogBroadcastReceiver;->mServiceRunning:Ljava/lang/String;

    const-string v0, "service_stopped"

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLogBroadcastReceiver;->mServiceStopped:Ljava/lang/String;

    const-string v0, "log_path_type"

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLogBroadcastReceiver;->mLogPathTypeTag:Ljava/lang/String;

    const-string v0, "phone"

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLogBroadcastReceiver;->mLogPathTypePhone:Ljava/lang/String;

    const-string v0, "sdcard"

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLogBroadcastReceiver;->mLogPathTypeSdCard:Ljava/lang/String;

    const-string v0, "/mnt/sdcard"

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLogBroadcastReceiver;->mLogPath:Ljava/lang/String;

    new-instance v0, Lcom/mediatek/mobilelog/LogSettings;

    const-string v1, "/data/data/com.mediatek.mobilelog/files"

    invoke-direct {v0, v1}, Lcom/mediatek/mobilelog/LogSettings;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLogBroadcastReceiver;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 11
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const-string v8, "MobileLogJ/Broadcast"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "static onReceive "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    const-string v9, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    const-string v9, "android.intent.action.ACTION_BOOT_IPO"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    const-string v9, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_a

    :cond_0
    const-string v8, "MobileLogJ/Broadcast"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "recv intent "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v8, p0, Lcom/mediatek/mobilelog/MobileLogBroadcastReceiver;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;

    invoke-virtual {v8}, Lcom/mediatek/mobilelog/LogSettings;->loadConfig()Ljava/util/Properties;

    move-result-object v5

    iget-object v8, p0, Lcom/mediatek/mobilelog/MobileLogBroadcastReceiver;->mServiceBootOptionTag:Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    iget-object v8, p0, Lcom/mediatek/mobilelog/MobileLogBroadcastReceiver;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;

    invoke-virtual {v8}, Lcom/mediatek/mobilelog/LogSettings;->getBootOption()Ljava/lang/String;

    move-result-object v1

    iget-object v8, p0, Lcom/mediatek/mobilelog/MobileLogBroadcastReceiver;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;

    invoke-virtual {v8}, Lcom/mediatek/mobilelog/LogSettings;->recreateConfig()V

    :cond_1
    iget-object v8, p0, Lcom/mediatek/mobilelog/MobileLogBroadcastReceiver;->mServiceOnBootEnabled:Ljava/lang/String;

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    new-instance v4, Landroid/content/Intent;

    const-string v8, "com.mediatek.mobilelog.MobileLogService"

    invoke-direct {v4, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    const-string v9, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    const-string v8, "com.mediatek.mobilelog.DEVICEBOOTUP"

    const-string v9, "true"

    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v8, "com.mediatek.Service.type"

    const-string v9, "com.mediatek.Service.BOOT"

    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v8, 0x1

    sput-boolean v8, Lcom/mediatek/mobilelog/MobileLogBroadcastReceiver;->start_flag:Z

    :goto_0
    invoke-virtual {p1, v4}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_2
    :goto_1
    return-void

    :cond_3
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    const-string v9, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    iget-object v8, p0, Lcom/mediatek/mobilelog/MobileLogBroadcastReceiver;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;

    invoke-virtual {v8, p1}, Lcom/mediatek/mobilelog/LogSettings;->getRightPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/mediatek/mobilelog/MobileLogBroadcastReceiver;->mLogPath:Ljava/lang/String;

    const-string v8, "storage_volume"

    invoke-virtual {p2, v8}, Landroid/content/Intent;->getExtra(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/os/storage/StorageVolume;

    if-nez v6, :cond_4

    const-string v8, "MobileLogJ/Broadcast"

    const-string v9, "StorageVolume is null!"

    invoke-static {v8, v9}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_4
    invoke-virtual {v6}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_6

    const-string v8, "MobileLogJ/Broadcast"

    const-string v9, "Can not get sdcard path!"

    invoke-static {v8, v9}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    sget-boolean v8, Lcom/mediatek/mobilelog/MobileLogBroadcastReceiver;->start_flag:Z

    if-eqz v8, :cond_7

    const/4 v8, 0x0

    sput-boolean v8, Lcom/mediatek/mobilelog/MobileLogBroadcastReceiver;->start_flag:Z

    goto :goto_1

    :cond_6
    iget-object v8, p0, Lcom/mediatek/mobilelog/MobileLogBroadcastReceiver;->mLogPath:Ljava/lang/String;

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_5

    const-string v8, "MobileLogJ/Broadcast"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Ignore intent: The mounted sdcard is "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", and current log path is "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/mediatek/mobilelog/MobileLogBroadcastReceiver;->mLogPath:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_7
    const-string v8, "com.mediatek.mobilelog.DEVICEBOOTUP"

    const-string v9, "true"

    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v8, "com.mediatek.Service.type"

    const-string v9, "com.mediatek.Service.sdmounted"

    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    :cond_8
    const-string v8, "com.mediatek.mobilelog.DEVICEBOOTUP"

    const-string v9, "false"

    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v8, "com.mediatek.Service.type"

    const-string v9, "com.mediatek.Service.IPO"

    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_0

    :cond_9
    iget-object v8, p0, Lcom/mediatek/mobilelog/MobileLogBroadcastReceiver;->mServiceStatusTag:Ljava/lang/String;

    iget-object v9, p0, Lcom/mediatek/mobilelog/MobileLogBroadcastReceiver;->mServiceStopped:Ljava/lang/String;

    invoke-virtual {v5, v8, v9}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/mediatek/mobilelog/MobileLogBroadcastReceiver;->mServiceRunning:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/mediatek/mobilelog/MobileLogBroadcastReceiver;->mServiceStatusTag:Ljava/lang/String;

    iget-object v9, p0, Lcom/mediatek/mobilelog/MobileLogBroadcastReceiver;->mServiceStopped:Ljava/lang/String;

    invoke-virtual {v5, v8, v9}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v8, p0, Lcom/mediatek/mobilelog/MobileLogBroadcastReceiver;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;

    invoke-virtual {v8, v5}, Lcom/mediatek/mobilelog/LogSettings;->saveConfig(Ljava/util/Properties;)V

    goto/16 :goto_1

    :cond_a
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    const-string v9, "com.mediatek.syslogger.action"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const-string v8, "MobileLogJ/Broadcast"

    const-string v9, "Start from Common UI"

    invoke-static {v8, v9}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v8, "From"

    invoke-virtual {p2, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v8, "To"

    invoke-virtual {p2, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "Command"

    invoke-virtual {p2, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v3, :cond_2

    if-eqz v7, :cond_2

    if-eqz v2, :cond_2

    const-string v8, "CommonUI"

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const-string v8, "MobileLog"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const-string v8, "Start"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    new-instance v4, Landroid/content/Intent;

    const-string v8, "com.mediatek.mobilelog.MobileLogService"

    invoke-direct {v4, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v8, "com.mediatek.mobilelog.DEVICEBOOTUP"

    const-string v9, "false"

    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v8, "com.mediatek.Service.type"

    const-string v9, "com.mediatek.Service.commonUI"

    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p1, v4}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_1
.end method
