.class public Lcom/mediatek/mobilelog/MobileLogJNI;
.super Ljava/lang/Object;
.source "MobileLogJNI.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MobileLog_JNI"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, "MobileLog_JNI"

    const-string v1, "Load the library: mobilelog_jni"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "mobilelog_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static native aee_warning(Ljava/lang/String;)I
.end method

.method public static native chmod_file(Ljava/lang/String;)I
.end method
