.class Lcom/mediatek/mobilelog/MobileLogService$1;
.super Landroid/content/BroadcastReceiver;
.source "MobileLogService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/mobilelog/MobileLogService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/mobilelog/MobileLogService;


# direct methods
.method constructor <init>(Lcom/mediatek/mobilelog/MobileLogService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/mobilelog/MobileLogService$1;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v3, "MobileLogJ/Service"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "receive intent: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/mobilelog/MobileLogService$1;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    iget-object v4, p0, Lcom/mediatek/mobilelog/MobileLogService$1;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;
    invoke-static {v4}, Lcom/mediatek/mobilelog/MobileLogService;->access$100(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/LogSettings;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLogService$1;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    invoke-virtual {v5}, Lcom/mediatek/mobilelog/MobileLogService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/mediatek/mobilelog/LogSettings;->getRightPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    # setter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogPath:Ljava/lang/String;
    invoke-static {v3, v4}, Lcom/mediatek/mobilelog/MobileLogService;->access$002(Lcom/mediatek/mobilelog/MobileLogService;Ljava/lang/String;)Ljava/lang/String;

    const-string v3, "MobileLogJ/Service"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "when sdcard mounted, we get log path: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLogService$1;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogPath:Ljava/lang/String;
    invoke-static {v5}, Lcom/mediatek/mobilelog/MobileLogService;->access$000(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "storage_volume"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getExtra(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/storage/StorageVolume;

    if-nez v2, :cond_1

    const-string v3, "MobileLogJ/Service"

    const-string v4, "StorageVolume is null!"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v2}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_7

    const-string v3, "MobileLogJ/Service"

    const-string v4, "Can not get sdcard path!"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const-string v3, "android.intent.action.MEDIA_BAD_REMOVAL"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "android.intent.action.MEDIA_REMOVED"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_3
    const-string v3, "MobileLogJ/Service"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "runtime receive "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/mobilelog/MobileLogService$1;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # setter for: Lcom/mediatek/mobilelog/MobileLogService;->sdMounted:Z
    invoke-static {v3, v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$202(Lcom/mediatek/mobilelog/MobileLogService;Z)Z

    iget-object v3, p0, Lcom/mediatek/mobilelog/MobileLogService$1;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->curWriteToPhone:Z
    invoke-static {v3}, Lcom/mediatek/mobilelog/MobileLogService;->access$300(Lcom/mediatek/mobilelog/MobileLogService;)Z

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/mediatek/mobilelog/MobileLogService$1;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->isIpoShutdown:Ljava/lang/Boolean;
    invoke-static {v3}, Lcom/mediatek/mobilelog/MobileLogService;->access$400(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/mediatek/mobilelog/MobileLogService$1;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/mediatek/mobilelog/MobileLogService;->access$500(Lcom/mediatek/mobilelog/MobileLogService;)Landroid/os/Handler;

    move-result-object v3

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/mediatek/mobilelog/MobileLogService$1;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/mediatek/mobilelog/MobileLogService;->access$500(Lcom/mediatek/mobilelog/MobileLogService;)Landroid/os/Handler;

    move-result-object v3

    const/4 v4, 0x5

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_1
    const-string v3, "android.intent.action.MEDIA_BAD_REMOVAL"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/mediatek/mobilelog/MobileLogService$1;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # invokes: Lcom/mediatek/mobilelog/MobileLogService;->triggerAEEWarning(I)V
    invoke-static {v3, v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$600(Lcom/mediatek/mobilelog/MobileLogService;I)V

    :cond_4
    const-string v3, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "MobileLogJ/Service"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "runtime receive "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    const-string v3, "mounted"

    iget-object v4, p0, Lcom/mediatek/mobilelog/MobileLogService$1;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->exStorageManager:Landroid/os/storage/StorageManager;
    invoke-static {v4}, Lcom/mediatek/mobilelog/MobileLogService;->access$700(Lcom/mediatek/mobilelog/MobileLogService;)Landroid/os/storage/StorageManager;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLogService$1;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogPath:Ljava/lang/String;
    invoke-static {v5}, Lcom/mediatek/mobilelog/MobileLogService;->access$000(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v3, "MobileLogJ/Service"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SDCARD:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLogService$1;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogPath:Ljava/lang/String;
    invoke-static {v5}, Lcom/mediatek/mobilelog/MobileLogService;->access$000(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " has ready!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/mobilelog/MobileLogService$1;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # setter for: Lcom/mediatek/mobilelog/MobileLogService;->sdMounted:Z
    invoke-static {v3, v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$202(Lcom/mediatek/mobilelog/MobileLogService;Z)Z

    iget-object v3, p0, Lcom/mediatek/mobilelog/MobileLogService$1;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/mediatek/mobilelog/MobileLogService;->access$500(Lcom/mediatek/mobilelog/MobileLogService;)Landroid/os/Handler;

    move-result-object v3

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/mediatek/mobilelog/MobileLogService$1;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/mediatek/mobilelog/MobileLogService;->access$500(Lcom/mediatek/mobilelog/MobileLogService;)Landroid/os/Handler;

    move-result-object v3

    const/16 v4, 0xd

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V

    :cond_6
    iget-object v3, p0, Lcom/mediatek/mobilelog/MobileLogService$1;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->curWriteToPhone:Z
    invoke-static {v3}, Lcom/mediatek/mobilelog/MobileLogService;->access$300(Lcom/mediatek/mobilelog/MobileLogService;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/mobilelog/MobileLogService$1;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->isIpoShutdown:Ljava/lang/Boolean;
    invoke-static {v3}, Lcom/mediatek/mobilelog/MobileLogService;->access$400(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_a

    iget-object v3, p0, Lcom/mediatek/mobilelog/MobileLogService$1;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/mediatek/mobilelog/MobileLogService;->access$500(Lcom/mediatek/mobilelog/MobileLogService;)Landroid/os/Handler;

    move-result-object v3

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/mediatek/mobilelog/MobileLogService$1;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->shouldcpy:Ljava/lang/Boolean;
    invoke-static {v3}, Lcom/mediatek/mobilelog/MobileLogService;->access$800(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-ne v7, v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/mobilelog/MobileLogService$1;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/mediatek/mobilelog/MobileLogService;->access$500(Lcom/mediatek/mobilelog/MobileLogService;)Landroid/os/Handler;

    move-result-object v3

    const/4 v4, 0x7

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v3, p0, Lcom/mediatek/mobilelog/MobileLogService$1;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    # setter for: Lcom/mediatek/mobilelog/MobileLogService;->shouldcpy:Ljava/lang/Boolean;
    invoke-static {v3, v4}, Lcom/mediatek/mobilelog/MobileLogService;->access$802(Lcom/mediatek/mobilelog/MobileLogService;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    goto/16 :goto_0

    :cond_7
    iget-object v3, p0, Lcom/mediatek/mobilelog/MobileLogService$1;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogPath:Ljava/lang/String;
    invoke-static {v3}, Lcom/mediatek/mobilelog/MobileLogService;->access$000(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "MobileLogJ/Service"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Ignore intent: The intent is sent by "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", but current path is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLogService$1;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogPath:Ljava/lang/String;
    invoke-static {v5}, Lcom/mediatek/mobilelog/MobileLogService;->access$000(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_8
    const-string v3, "MobileLogJ/Service"

    const-string v4, "handler == null"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_9
    const-string v3, "MobileLogJ/Service"

    const-string v4, "handler == null"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_a
    iget-object v3, p0, Lcom/mediatek/mobilelog/MobileLogService$1;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->shouldcpy:Ljava/lang/Boolean;
    invoke-static {v3}, Lcom/mediatek/mobilelog/MobileLogService;->access$800(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-ne v7, v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/mobilelog/MobileLogService$1;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/mediatek/mobilelog/MobileLogService;->access$500(Lcom/mediatek/mobilelog/MobileLogService;)Landroid/os/Handler;

    move-result-object v3

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/mediatek/mobilelog/MobileLogService$1;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/mediatek/mobilelog/MobileLogService;->access$500(Lcom/mediatek/mobilelog/MobileLogService;)Landroid/os/Handler;

    move-result-object v3

    const/16 v4, 0x9

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v3, p0, Lcom/mediatek/mobilelog/MobileLogService$1;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    # setter for: Lcom/mediatek/mobilelog/MobileLogService;->shouldcpy:Ljava/lang/Boolean;
    invoke-static {v3, v4}, Lcom/mediatek/mobilelog/MobileLogService;->access$802(Lcom/mediatek/mobilelog/MobileLogService;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    goto/16 :goto_0

    :cond_b
    const-string v3, "MobileLogJ/Service"

    const-string v4, "handler == null"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
