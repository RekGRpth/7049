.class Lcom/mediatek/mobilelog/MobileLogService$2;
.super Landroid/content/BroadcastReceiver;
.source "MobileLogService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/mobilelog/MobileLogService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/mobilelog/MobileLogService;


# direct methods
.method constructor <init>(Lcom/mediatek/mobilelog/MobileLogService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/mobilelog/MobileLogService$2;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v2, "MobileLogJ/Service"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "runtime receive intent: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/mobilelog/MobileLogService$2;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;
    invoke-static {v2}, Lcom/mediatek/mobilelog/MobileLogService;->access$100(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/LogSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/mobilelog/LogSettings;->loadConfig()Ljava/util/Properties;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/mobilelog/MobileLogService$2;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    const-string v3, "log_path_type"

    invoke-virtual {v1, v3}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    # setter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogPathType:Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/mediatek/mobilelog/MobileLogService;->access$902(Lcom/mediatek/mobilelog/MobileLogService;Ljava/lang/String;)Ljava/lang/String;

    iget-object v2, p0, Lcom/mediatek/mobilelog/MobileLogService$2;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogPathType:Ljava/lang/String;
    invoke-static {v2}, Lcom/mediatek/mobilelog/MobileLogService;->access$900(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    const-string v2, "MobileLogJ/Service"

    const-string v3, "cant read mLogPathType from config file !!!"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/mobilelog/MobileLogService$2;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    iget-object v3, p0, Lcom/mediatek/mobilelog/MobileLogService$2;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;
    invoke-static {v3}, Lcom/mediatek/mobilelog/MobileLogService;->access$100(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/LogSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/mobilelog/LogSettings;->getLogPathType()Ljava/lang/String;

    move-result-object v3

    # setter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogPathType:Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/mediatek/mobilelog/MobileLogService;->access$902(Lcom/mediatek/mobilelog/MobileLogService;Ljava/lang/String;)Ljava/lang/String;

    iget-object v2, p0, Lcom/mediatek/mobilelog/MobileLogService$2;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;
    invoke-static {v2}, Lcom/mediatek/mobilelog/MobileLogService;->access$100(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/LogSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/mobilelog/LogSettings;->recreateConfig()V

    :cond_0
    iget-object v2, p0, Lcom/mediatek/mobilelog/MobileLogService$2;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogPathType:Ljava/lang/String;
    invoke-static {v2}, Lcom/mediatek/mobilelog/MobileLogService;->access$900(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/mobilelog/MobileLogService$2;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogPathTypeSdCard:Ljava/lang/String;
    invoke-static {v3}, Lcom/mediatek/mobilelog/MobileLogService;->access$1000(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/mobilelog/MobileLogService$2;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    const/4 v3, 0x0

    # setter for: Lcom/mediatek/mobilelog/MobileLogService;->curWriteToPhone:Z
    invoke-static {v2, v3}, Lcom/mediatek/mobilelog/MobileLogService;->access$302(Lcom/mediatek/mobilelog/MobileLogService;Z)Z

    :goto_0
    const-string v2, "android.intent.action.DEVICE_STORAGE_LOW"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "MobileLogJ/Service"

    const-string v3, "Low Storage!"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/mobilelog/MobileLogService$2;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    const v3, 0x1080074

    const/4 v4, 0x3

    # invokes: Lcom/mediatek/mobilelog/MobileLogService;->showNotification(II)V
    invoke-static {v2, v3, v4}, Lcom/mediatek/mobilelog/MobileLogService;->access$1100(Lcom/mediatek/mobilelog/MobileLogService;II)V

    iget-object v2, p0, Lcom/mediatek/mobilelog/MobileLogService$2;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->curWriteToPhone:Z
    invoke-static {v2}, Lcom/mediatek/mobilelog/MobileLogService;->access$300(Lcom/mediatek/mobilelog/MobileLogService;)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "MobileLogJ/Service"

    const-string v3, "Currnet write to phone,will stop mobilelog!"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/mobilelog/MobileLogService$2;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/mediatek/mobilelog/MobileLogService;->access$500(Lcom/mediatek/mobilelog/MobileLogService;)Landroid/os/Handler;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/mediatek/mobilelog/MobileLogService$2;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/mediatek/mobilelog/MobileLogService;->access$500(Lcom/mediatek/mobilelog/MobileLogService;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-object v2, p0, Lcom/mediatek/mobilelog/MobileLogService$2;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    const/4 v3, 0x1

    # setter for: Lcom/mediatek/mobilelog/MobileLogService;->curWriteToPhone:Z
    invoke-static {v2, v3}, Lcom/mediatek/mobilelog/MobileLogService;->access$302(Lcom/mediatek/mobilelog/MobileLogService;Z)Z

    goto :goto_0

    :cond_3
    const-string v2, "MobileLogJ/Service"

    const-string v3, "handler == null"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_4
    const-string v2, "MobileLogJ/Service"

    const-string v3, "Currnet write to sdcard! IGNORE"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
