.class Lcom/mediatek/mobilelog/MobileLogService$3;
.super Landroid/content/BroadcastReceiver;
.source "MobileLogService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/mobilelog/MobileLogService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/mobilelog/MobileLogService;


# direct methods
.method constructor <init>(Lcom/mediatek/mobilelog/MobileLogService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/mobilelog/MobileLogService$3;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const-string v6, "MobileLogJ/Service"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mIntentReceiverCommonUI receive "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v6, "com.mediatek.syslogger.action"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const-string v6, "From"

    invoke-virtual {p2, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v6, "To"

    invoke-virtual {p2, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "Command"

    invoke-virtual {p2, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v2, :cond_0

    if-eqz v5, :cond_0

    if-eqz v1, :cond_0

    const-string v6, "CommonUI"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const-string v6, "MobileLog"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const-string v6, "Stop"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    new-instance v3, Landroid/content/Intent;

    const-string v6, "com.mediatek.syslogger.action"

    invoke-direct {v3, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v6, "From"

    const-string v7, "MobileLog"

    invoke-virtual {v3, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v6, "To"

    const-string v7, "CommonUI"

    invoke-virtual {v3, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v6, "Command"

    const-string v7, "Stop"

    invoke-virtual {v3, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v6, "Return"

    const-string v7, "Normal"

    invoke-virtual {v3, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$3;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    invoke-virtual {v6, v3}, Lcom/mediatek/mobilelog/MobileLogService;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$3;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$500(Lcom/mediatek/mobilelog/MobileLogService;)Landroid/os/Handler;

    move-result-object v6

    const/4 v7, 0x5

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$3;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$100(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/LogSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mediatek/mobilelog/LogSettings;->loadConfig()Ljava/util/Properties;

    move-result-object v4

    const-string v6, "service_boot_option"

    iget-object v7, p0, Lcom/mediatek/mobilelog/MobileLogService$3;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mServiceOnBootDisabled:Ljava/lang/String;
    invoke-static {v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$1200(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v6, "service_status"

    iget-object v7, p0, Lcom/mediatek/mobilelog/MobileLogService$3;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mServiceStopped:Ljava/lang/String;
    invoke-static {v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$1300(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$3;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$100(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/LogSettings;

    move-result-object v6

    invoke-virtual {v6, v4}, Lcom/mediatek/mobilelog/LogSettings;->saveConfig(Ljava/util/Properties;)V

    :cond_0
    return-void
.end method
