.class Lcom/mediatek/mobilelog/MobileLogService$5;
.super Landroid/os/Handler;
.source "MobileLogService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/mobilelog/MobileLogService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/mobilelog/MobileLogService;


# direct methods
.method constructor <init>(Lcom/mediatek/mobilelog/MobileLogService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 11
    .param p1    # Landroid/os/Message;

    sget-object v6, Lcom/mediatek/mobilelog/MobileLogService;->sSelf:Lcom/mediatek/mobilelog/MobileLogService;

    if-nez v6, :cond_1

    const-string v6, "MobileLogJ/Service"

    const-string v7, "Unhandle message %d\n"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget v10, p1, Landroid/os/Message;->what:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v6, p1, Landroid/os/Message;->what:I

    packed-switch v6, :pswitch_data_0

    :pswitch_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto :goto_0

    :pswitch_1
    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$100(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/LogSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mediatek/mobilelog/LogSettings;->loadConfig()Ljava/util/Properties;

    move-result-object v4

    const/16 v5, 0x8

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mMlConnection:Lcom/mediatek/mobilelog/MlConnection;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$1400(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/MlConnection;

    move-result-object v6

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    new-instance v7, Lcom/mediatek/mobilelog/MlConnection;

    const-string v8, "mobilelogd"

    iget-object v9, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mHandler:Landroid/os/Handler;
    invoke-static {v9}, Lcom/mediatek/mobilelog/MobileLogService;->access$500(Lcom/mediatek/mobilelog/MobileLogService;)Landroid/os/Handler;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lcom/mediatek/mobilelog/MlConnection;-><init>(Ljava/lang/String;Landroid/os/Handler;)V

    # setter for: Lcom/mediatek/mobilelog/MobileLogService;->mMlConnection:Lcom/mediatek/mobilelog/MlConnection;
    invoke-static {v6, v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$1402(Lcom/mediatek/mobilelog/MobileLogService;Lcom/mediatek/mobilelog/MlConnection;)Lcom/mediatek/mobilelog/MlConnection;

    :goto_1
    if-eqz v5, :cond_2

    :try_start_0
    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mMlConnection:Lcom/mediatek/mobilelog/MlConnection;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$1400(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/MlConnection;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mediatek/mobilelog/MlConnection;->connect()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    if-eqz v6, :cond_5

    :cond_2
    :goto_2
    if-nez v5, :cond_6

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    const/4 v7, 0x0

    # setter for: Lcom/mediatek/mobilelog/MobileLogService;->mMlConnection:Lcom/mediatek/mobilelog/MlConnection;
    invoke-static {v6, v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$1402(Lcom/mediatek/mobilelog/MobileLogService;Lcom/mediatek/mobilelog/MlConnection;)Lcom/mediatek/mobilelog/MlConnection;

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    const-string v7, "service_boot_option"

    invoke-virtual {v4, v7}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    # setter for: Lcom/mediatek/mobilelog/MobileLogService;->mBootOption:Ljava/lang/String;
    invoke-static {v6, v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$1502(Lcom/mediatek/mobilelog/MobileLogService;Ljava/lang/String;)Ljava/lang/String;

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mBootOption:Ljava/lang/String;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$1500(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_3

    const-string v6, "MobileLogJ/Service"

    const-string v7, "cant read mBootOption from config file !!!"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    iget-object v7, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;
    invoke-static {v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$100(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/LogSettings;

    move-result-object v7

    invoke-virtual {v7}, Lcom/mediatek/mobilelog/LogSettings;->getBootOption()Ljava/lang/String;

    move-result-object v7

    # setter for: Lcom/mediatek/mobilelog/MobileLogService;->mBootOption:Ljava/lang/String;
    invoke-static {v6, v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$1502(Lcom/mediatek/mobilelog/MobileLogService;Ljava/lang/String;)Ljava/lang/String;

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$100(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/LogSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mediatek/mobilelog/LogSettings;->recreateConfig()V

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$100(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/LogSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mediatek/mobilelog/LogSettings;->loadConfig()Ljava/util/Properties;

    move-result-object v4

    :cond_3
    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mBootOption:Ljava/lang/String;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$1500(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mServiceOnBootEnabled:Ljava/lang/String;
    invoke-static {v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$1600(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    const-string v6, "service_boot_option"

    iget-object v7, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mServiceOnBootDisabled:Ljava/lang/String;
    invoke-static {v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$1200(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v6, "service_status"

    iget-object v7, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mServiceStopped:Ljava/lang/String;
    invoke-static {v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$1300(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$100(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/LogSettings;

    move-result-object v6

    invoke-virtual {v6, v4}, Lcom/mediatek/mobilelog/LogSettings;->saveConfig(Ljava/util/Properties;)V

    :cond_4
    const-string v6, "MobileLogJ/Service"

    const-string v7, "Unable connect to mobilelogD, exit"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->itCommonUI:Landroid/content/Intent;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$1700(Lcom/mediatek/mobilelog/MobileLogService;)Landroid/content/Intent;

    move-result-object v6

    const-string v7, "Return"

    const-string v8, "DaemonUnable"

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    iget-object v7, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->itCommonUI:Landroid/content/Intent;
    invoke-static {v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$1700(Lcom/mediatek/mobilelog/MobileLogService;)Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/mediatek/mobilelog/MobileLogService;->sendBroadcast(Landroid/content/Intent;)V

    const-string v6, "MobileLogJ/Service"

    const-string v7, "send intent to commonUI: Connect to Daemon error!"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    const/4 v7, 0x3

    # invokes: Lcom/mediatek/mobilelog/MobileLogService;->triggerAEEWarning(I)V
    invoke-static {v6, v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$600(Lcom/mediatek/mobilelog/MobileLogService;I)V

    goto/16 :goto_0

    :cond_5
    :try_start_1
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    const-wide/16 v6, 0x3e8

    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V

    const-string v6, "MobileLogJ/Service"

    const-string v7, "Connection retry"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    add-int/lit8 v5, v5, -0x1

    goto/16 :goto_1

    :catch_0
    move-exception v2

    const-string v6, "MobileLogJ/Service"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "thread "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_6
    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    invoke-virtual {v6}, Lcom/mediatek/mobilelog/MobileLogService;->readConfig()Z

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    const-string v7, "log_path_type"

    invoke-virtual {v4, v7}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    # setter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogPathType:Ljava/lang/String;
    invoke-static {v6, v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$902(Lcom/mediatek/mobilelog/MobileLogService;Ljava/lang/String;)Ljava/lang/String;

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogPathType:Ljava/lang/String;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$900(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_7

    const-string v6, "MobileLogJ/Service"

    const-string v7, "cant read mLogPathType from config file !!!"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    iget-object v7, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;
    invoke-static {v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$100(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/LogSettings;

    move-result-object v7

    invoke-virtual {v7}, Lcom/mediatek/mobilelog/LogSettings;->getLogPathType()Ljava/lang/String;

    move-result-object v7

    # setter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogPathType:Ljava/lang/String;
    invoke-static {v6, v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$902(Lcom/mediatek/mobilelog/MobileLogService;Ljava/lang/String;)Ljava/lang/String;

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$100(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/LogSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mediatek/mobilelog/LogSettings;->recreateConfig()V

    :cond_7
    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogPathType:Ljava/lang/String;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$900(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogPathTypeSdCard:Ljava/lang/String;
    invoke-static {v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$1000(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_f

    const-string v6, "mounted"

    iget-object v7, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->exStorageManager:Landroid/os/storage/StorageManager;
    invoke-static {v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$700(Lcom/mediatek/mobilelog/MobileLogService;)Landroid/os/storage/StorageManager;

    move-result-object v7

    iget-object v8, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogPath:Ljava/lang/String;
    invoke-static {v8}, Lcom/mediatek/mobilelog/MobileLogService;->access$000(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    const/4 v7, 0x1

    # setter for: Lcom/mediatek/mobilelog/MobileLogService;->sdMounted:Z
    invoke-static {v6, v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$202(Lcom/mediatek/mobilelog/MobileLogService;Z)Z

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mServiceStartType:Ljava/lang/String;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$1800(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "com.mediatek.Service.commonUI"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_8

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mServiceStartType:Ljava/lang/String;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$1800(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "com.mediatek.Service.normal"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_8

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mServiceStartType:Ljava/lang/String;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$1800(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "com.mediatek.Service.sdmounted"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    :cond_8
    const/16 v6, 0xc

    invoke-virtual {p0, v6}, Lcom/mediatek/mobilelog/MobileLogService$5;->sendEmptyMessage(I)Z

    :cond_9
    :goto_3
    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # invokes: Lcom/mediatek/mobilelog/MobileLogService;->checkMobileLogStatus()V
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$1900(Lcom/mediatek/mobilelog/MobileLogService;)V

    goto/16 :goto_0

    :cond_a
    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mServiceStartType:Ljava/lang/String;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$1800(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "com.mediatek.Service.BOOT"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    const-string v6, "MobileLogJ/Service"

    const-string v7, "normal boot,but send MSG_SD"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v6, 0x7

    invoke-virtual {p0, v6}, Lcom/mediatek/mobilelog/MobileLogService$5;->sendEmptyMessage(I)Z

    goto :goto_3

    :cond_b
    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mServiceStartType:Ljava/lang/String;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$1800(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "com.mediatek.Service.IPO"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    const/16 v6, 0x9

    invoke-virtual {p0, v6}, Lcom/mediatek/mobilelog/MobileLogService$5;->sendEmptyMessage(I)Z

    goto :goto_3

    :cond_c
    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mServiceStartType:Ljava/lang/String;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$1800(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "com.mediatek.Service.commonUI"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    const-string v6, "MobileLogJ/Service"

    const-string v7, "Start from CommonUI:No sdcard!"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->itCommonUI:Landroid/content/Intent;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$1700(Lcom/mediatek/mobilelog/MobileLogService;)Landroid/content/Intent;

    move-result-object v6

    const-string v7, "Return"

    const-string v8, "NoSDCard"

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    iget-object v7, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->itCommonUI:Landroid/content/Intent;
    invoke-static {v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$1700(Lcom/mediatek/mobilelog/MobileLogService;)Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/mediatek/mobilelog/MobileLogService;->sendBroadcast(Landroid/content/Intent;)V

    const/4 v6, 0x5

    invoke-virtual {p0, v6}, Lcom/mediatek/mobilelog/MobileLogService$5;->sendEmptyMessage(I)Z

    goto :goto_3

    :cond_d
    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mServiceStartType:Ljava/lang/String;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$1800(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "com.mediatek.Service.BOOT"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_e

    const-string v6, "MobileLogJ/Service"

    const-string v7, "Start from BOOT_COMPLETE: start timeout count!"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    # setter for: Lcom/mediatek/mobilelog/MobileLogService;->shouldcpy:Ljava/lang/Boolean;
    invoke-static {v6, v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$802(Lcom/mediatek/mobilelog/MobileLogService;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    const/16 v6, 0xd

    invoke-virtual {p0, v6}, Lcom/mediatek/mobilelog/MobileLogService$5;->obtainMessage(I)Landroid/os/Message;

    move-result-object v6

    const-wide/32 v7, 0x1d4c0

    invoke-virtual {p0, v6, v7, v8}, Lcom/mediatek/mobilelog/MobileLogService$5;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_3

    :cond_e
    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mServiceStartType:Ljava/lang/String;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$1800(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "com.mediatek.Service.IPO"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    const-string v6, "MobileLogJ/Service"

    const-string v7, "Start from IPO: start timeout count!"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    # setter for: Lcom/mediatek/mobilelog/MobileLogService;->shouldcpy:Ljava/lang/Boolean;
    invoke-static {v6, v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$802(Lcom/mediatek/mobilelog/MobileLogService;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    const/16 v6, 0xd

    invoke-virtual {p0, v6}, Lcom/mediatek/mobilelog/MobileLogService$5;->obtainMessage(I)Landroid/os/Message;

    move-result-object v6

    const-wide/32 v7, 0x1d4c0

    invoke-virtual {p0, v6, v7, v8}, Lcom/mediatek/mobilelog/MobileLogService$5;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_3

    :cond_f
    const/16 v6, 0xc

    invoke-virtual {p0, v6}, Lcom/mediatek/mobilelog/MobileLogService$5;->sendEmptyMessage(I)Z

    goto/16 :goto_3

    :pswitch_2
    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$100(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/LogSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mediatek/mobilelog/LogSettings;->loadConfig()Ljava/util/Properties;

    move-result-object v4

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    const-string v7, "log_path_type"

    invoke-virtual {v4, v7}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    # setter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogPathType:Ljava/lang/String;
    invoke-static {v6, v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$902(Lcom/mediatek/mobilelog/MobileLogService;Ljava/lang/String;)Ljava/lang/String;

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogPathType:Ljava/lang/String;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$900(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_10

    const-string v6, "MobileLogJ/Service"

    const-string v7, "cant read mLogPathType from config file !!!"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    iget-object v7, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;
    invoke-static {v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$100(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/LogSettings;

    move-result-object v7

    invoke-virtual {v7}, Lcom/mediatek/mobilelog/LogSettings;->getLogPathType()Ljava/lang/String;

    move-result-object v7

    # setter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogPathType:Ljava/lang/String;
    invoke-static {v6, v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$902(Lcom/mediatek/mobilelog/MobileLogService;Ljava/lang/String;)Ljava/lang/String;

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$100(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/LogSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mediatek/mobilelog/LogSettings;->recreateConfig()V

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$100(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/LogSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mediatek/mobilelog/LogSettings;->loadConfig()Ljava/util/Properties;

    move-result-object v4

    :cond_10
    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogPathType:Ljava/lang/String;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$900(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogPathTypePhone:Ljava/lang/String;
    invoke-static {v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$2000(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_11

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # invokes: Lcom/mediatek/mobilelog/MobileLogService;->getAvailableInternalMemorySize()J
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$2100(Lcom/mediatek/mobilelog/MobileLogService;)J

    move-result-wide v0

    :goto_4
    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$100(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/LogSettings;

    move-result-object v6

    invoke-virtual {v6, v4}, Lcom/mediatek/mobilelog/LogSettings;->saveConfig(Ljava/util/Properties;)V

    const-string v6, "MobileLogJ/Service"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "LogSize is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogSize:I
    invoke-static {v8}, Lcom/mediatek/mobilelog/MobileLogService;->access$2300(Lcom/mediatek/mobilelog/MobileLogService;)I

    move-result v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "MB and Current available Memory is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "MB!"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogSize:I
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$2300(Lcom/mediatek/mobilelog/MobileLogService;)I

    move-result v6

    add-int/lit8 v6, v6, 0xa

    int-to-long v6, v6

    cmp-long v6, v6, v0

    if-ltz v6, :cond_12

    const-string v6, "MobileLogJ/Service"

    const-string v7, "SDcard is full!"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v6, "MobileLogJ/Service"

    const-string v7, "Send intent to CommonUI:SDcard full!"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->itCommonUI:Landroid/content/Intent;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$1700(Lcom/mediatek/mobilelog/MobileLogService;)Landroid/content/Intent;

    move-result-object v6

    const-string v7, "Return"

    const-string v8, "SDCardFull"

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    iget-object v7, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->itCommonUI:Landroid/content/Intent;
    invoke-static {v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$1700(Lcom/mediatek/mobilelog/MobileLogService;)Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/mediatek/mobilelog/MobileLogService;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    const v7, 0x1080074

    const/4 v8, 0x4

    # invokes: Lcom/mediatek/mobilelog/MobileLogService;->showNotification(II)V
    invoke-static {v6, v7, v8}, Lcom/mediatek/mobilelog/MobileLogService;->access$1100(Lcom/mediatek/mobilelog/MobileLogService;II)V

    const/4 v6, 0x5

    invoke-virtual {p0, v6}, Lcom/mediatek/mobilelog/MobileLogService$5;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :cond_11
    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # invokes: Lcom/mediatek/mobilelog/MobileLogService;->getAvailableExternalMemorySize()J
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$2200(Lcom/mediatek/mobilelog/MobileLogService;)J

    move-result-wide v0

    goto/16 :goto_4

    :cond_12
    const-string v6, "MobileLogJ/Service"

    const-string v7, "MobileLog Start!"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mMlConnection:Lcom/mediatek/mobilelog/MlConnection;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$1400(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/MlConnection;

    move-result-object v6

    const-string v7, "start"

    invoke-virtual {v6, v7}, Lcom/mediatek/mobilelog/MlConnection;->sendCmd(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/mediatek/mobilelog/MobileLogService;->updateServiceStatus(Z)V

    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Lcom/mediatek/mobilelog/MobileLogService$5;->removeMessages(I)V

    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Lcom/mediatek/mobilelog/MobileLogService$5;->obtainMessage(I)Landroid/os/Message;

    move-result-object v6

    const-wide/16 v7, 0x1388

    invoke-virtual {p0, v6, v7, v8}, Lcom/mediatek/mobilelog/MobileLogService$5;->sendMessageDelayed(Landroid/os/Message;J)Z

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    const/4 v7, 0x1

    # setter for: Lcom/mediatek/mobilelog/MobileLogService;->mNeedCheck:Z
    invoke-static {v6, v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$2402(Lcom/mediatek/mobilelog/MobileLogService;Z)Z

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$100(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/LogSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mediatek/mobilelog/LogSettings;->loadConfig()Ljava/util/Properties;

    move-result-object v4

    const-string v6, "service_boot_option"

    iget-object v7, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mServiceOnBootEnabled:Ljava/lang/String;
    invoke-static {v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$1600(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$100(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/LogSettings;

    move-result-object v6

    invoke-virtual {v6, v4}, Lcom/mediatek/mobilelog/LogSettings;->saveConfig(Ljava/util/Properties;)V

    goto/16 :goto_0

    :pswitch_3
    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->sdMounted:Z
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$200(Lcom/mediatek/mobilelog/MobileLogService;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "MobileLogJ/Service"

    const-string v7, "There is no sd!"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->itCommonUI:Landroid/content/Intent;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$1700(Lcom/mediatek/mobilelog/MobileLogService;)Landroid/content/Intent;

    move-result-object v6

    const-string v7, "Return"

    const-string v8, "NoSDCard"

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    iget-object v7, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->itCommonUI:Landroid/content/Intent;
    invoke-static {v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$1700(Lcom/mediatek/mobilelog/MobileLogService;)Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/mediatek/mobilelog/MobileLogService;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    const v7, 0x1080075

    const/4 v8, 0x2

    # invokes: Lcom/mediatek/mobilelog/MobileLogService;->showNotification(II)V
    invoke-static {v6, v7, v8}, Lcom/mediatek/mobilelog/MobileLogService;->access$1100(Lcom/mediatek/mobilelog/MobileLogService;II)V

    const/4 v6, 0x5

    invoke-virtual {p0, v6}, Lcom/mediatek/mobilelog/MobileLogService$5;->sendEmptyMessage(I)Z

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    const/4 v7, 0x2

    # invokes: Lcom/mediatek/mobilelog/MobileLogService;->triggerAEEWarning(I)V
    invoke-static {v6, v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$600(Lcom/mediatek/mobilelog/MobileLogService;I)V

    goto/16 :goto_0

    :pswitch_4
    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mMlConnection:Lcom/mediatek/mobilelog/MlConnection;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$1400(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/MlConnection;

    move-result-object v6

    const-string v7, "check"

    invoke-virtual {v6, v7}, Lcom/mediatek/mobilelog/MlConnection;->sendCmd(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mNeedCheck:Z
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$2400(Lcom/mediatek/mobilelog/MobileLogService;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Lcom/mediatek/mobilelog/MobileLogService$5;->obtainMessage(I)Landroid/os/Message;

    move-result-object v6

    const-wide/16 v7, 0x1388

    invoke-virtual {p0, v6, v7, v8}, Lcom/mediatek/mobilelog/MobileLogService$5;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    :pswitch_5
    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mMlConnection:Lcom/mediatek/mobilelog/MlConnection;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$1400(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/MlConnection;

    move-result-object v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mMlConnection:Lcom/mediatek/mobilelog/MlConnection;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$1400(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/MlConnection;

    move-result-object v6

    const-string v7, "die"

    invoke-virtual {v6, v7}, Lcom/mediatek/mobilelog/MlConnection;->sendCmd(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mMlConnection:Lcom/mediatek/mobilelog/MlConnection;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$1400(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/MlConnection;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mediatek/mobilelog/MlConnection;->stop()V

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    const/4 v7, 0x0

    # setter for: Lcom/mediatek/mobilelog/MobileLogService;->mMlConnection:Lcom/mediatek/mobilelog/MlConnection;
    invoke-static {v6, v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$1402(Lcom/mediatek/mobilelog/MobileLogService;Lcom/mediatek/mobilelog/MlConnection;)Lcom/mediatek/mobilelog/MlConnection;

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/mediatek/mobilelog/MobileLogService;->updateServiceStatus(Z)V

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    const/4 v7, 0x0

    # setter for: Lcom/mediatek/mobilelog/MobileLogService;->mNeedCheck:Z
    invoke-static {v6, v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$2402(Lcom/mediatek/mobilelog/MobileLogService;Z)Z

    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Lcom/mediatek/mobilelog/MobileLogService$5;->removeMessages(I)V

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$100(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/LogSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mediatek/mobilelog/LogSettings;->loadConfig()Ljava/util/Properties;

    move-result-object v4

    const-string v6, "service_status"

    iget-object v7, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mServiceStopped:Ljava/lang/String;
    invoke-static {v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$1300(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$100(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/LogSettings;

    move-result-object v6

    invoke-virtual {v6, v4}, Lcom/mediatek/mobilelog/LogSettings;->saveConfig(Ljava/util/Properties;)V

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # invokes: Lcom/mediatek/mobilelog/MobileLogService;->getAvailableExternalMemorySize()J
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$2200(Lcom/mediatek/mobilelog/MobileLogService;)J

    move-result-wide v6

    const-wide/16 v8, 0x2

    cmp-long v6, v6, v8

    if-gtz v6, :cond_13

    new-instance v3, Landroid/content/Intent;

    const-string v6, "com.mediatek.mobilelog.SDCARD_ALMOST_FULL"

    invoke-direct {v3, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v6, "msg"

    const-string v7, "SD Card is almost full, Mobilelog service is stopping!!"

    invoke-virtual {v3, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    invoke-virtual {v6, v3}, Lcom/mediatek/mobilelog/MobileLogService;->sendBroadcast(Landroid/content/Intent;)V

    const-string v6, "MobileLogJ/Service"

    const-string v7, "sd card is almost full, MobileLog service stop"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    invoke-virtual {v6}, Lcom/mediatek/mobilelog/MobileLogService;->stopSelf()V

    goto/16 :goto_0

    :cond_13
    new-instance v3, Landroid/content/Intent;

    const-string v6, "com.mediatek.mobilelog.SDCARD_DAMAGE"

    invoke-direct {v3, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v6, "msg"

    const-string v7, "SDCARD has damaged, MobileLog service stop"

    invoke-virtual {v3, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    invoke-virtual {v6, v3}, Lcom/mediatek/mobilelog/MobileLogService;->sendBroadcast(Landroid/content/Intent;)V

    const-string v6, "MobileLogJ/Service"

    const-string v7, "sdcard damaged,send intent"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :pswitch_6
    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/mediatek/mobilelog/MobileLogService;->updateServiceStatus(Z)V

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    const/4 v7, 0x0

    # setter for: Lcom/mediatek/mobilelog/MobileLogService;->mNeedCheck:Z
    invoke-static {v6, v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$2402(Lcom/mediatek/mobilelog/MobileLogService;Z)Z

    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Lcom/mediatek/mobilelog/MobileLogService$5;->removeMessages(I)V

    goto/16 :goto_0

    :pswitch_7
    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/mediatek/mobilelog/MobileLogService;->updateServiceStatus(Z)V

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    const/4 v7, 0x1

    # setter for: Lcom/mediatek/mobilelog/MobileLogService;->mNeedCheck:Z
    invoke-static {v6, v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$2402(Lcom/mediatek/mobilelog/MobileLogService;Z)Z

    goto/16 :goto_0

    :pswitch_8
    const-string v6, "MobileLogJ/Service"

    const-string v7, "stop!"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    const/4 v7, 0x0

    # setter for: Lcom/mediatek/mobilelog/MobileLogService;->mNeedCheck:Z
    invoke-static {v6, v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$2402(Lcom/mediatek/mobilelog/MobileLogService;Z)Z

    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Lcom/mediatek/mobilelog/MobileLogService$5;->removeMessages(I)V

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$100(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/LogSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mediatek/mobilelog/LogSettings;->loadConfig()Ljava/util/Properties;

    move-result-object v4

    const-string v6, "service_status"

    iget-object v7, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mServiceStopped:Ljava/lang/String;
    invoke-static {v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$1300(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$100(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/LogSettings;

    move-result-object v6

    invoke-virtual {v6, v4}, Lcom/mediatek/mobilelog/LogSettings;->saveConfig(Ljava/util/Properties;)V

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    invoke-virtual {v6}, Lcom/mediatek/mobilelog/MobileLogService;->stopSelf()V

    goto/16 :goto_0

    :pswitch_9
    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$100(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/LogSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mediatek/mobilelog/LogSettings;->loadConfig()Ljava/util/Properties;

    move-result-object v4

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    const/4 v7, 0x0

    # setter for: Lcom/mediatek/mobilelog/MobileLogService;->mNeedCheck:Z
    invoke-static {v6, v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$2402(Lcom/mediatek/mobilelog/MobileLogService;Z)Z

    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Lcom/mediatek/mobilelog/MobileLogService$5;->removeMessages(I)V

    new-instance v3, Landroid/content/Intent;

    const-string v6, "com.mediatek.mobilelog.LOW_STORAGE"

    invoke-direct {v3, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v6, "msg"

    const-string v7, "Low Storage, MobileLog service stop"

    invoke-virtual {v3, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    invoke-virtual {v6, v3}, Lcom/mediatek/mobilelog/MobileLogService;->sendBroadcast(Landroid/content/Intent;)V

    const-string v6, "MobileLogJ/Service"

    const-string v7, "lwo storage,send intent"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v6, "service_status"

    iget-object v7, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mServiceStopped:Ljava/lang/String;
    invoke-static {v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$1300(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v6, "service_boot_option"

    iget-object v7, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mServiceOnBootDisabled:Ljava/lang/String;
    invoke-static {v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$1200(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$100(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/LogSettings;

    move-result-object v6

    invoke-virtual {v6, v4}, Lcom/mediatek/mobilelog/LogSettings;->saveConfig(Ljava/util/Properties;)V

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    const/4 v7, 0x1

    # invokes: Lcom/mediatek/mobilelog/MobileLogService;->triggerAEEWarning(I)V
    invoke-static {v6, v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$600(Lcom/mediatek/mobilelog/MobileLogService;I)V

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    invoke-virtual {v6}, Lcom/mediatek/mobilelog/MobileLogService;->stopSelf()V

    :pswitch_a
    const-string v6, "MobileLogJ/Service"

    const-string v7, "MOUNTED!"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mMlConnection:Lcom/mediatek/mobilelog/MlConnection;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$1400(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/MlConnection;

    move-result-object v6

    if-nez v6, :cond_14

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    new-instance v7, Lcom/mediatek/mobilelog/MlConnection;

    const-string v8, "mobilelogd"

    iget-object v9, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mHandler:Landroid/os/Handler;
    invoke-static {v9}, Lcom/mediatek/mobilelog/MobileLogService;->access$500(Lcom/mediatek/mobilelog/MobileLogService;)Landroid/os/Handler;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lcom/mediatek/mobilelog/MlConnection;-><init>(Ljava/lang/String;Landroid/os/Handler;)V

    # setter for: Lcom/mediatek/mobilelog/MobileLogService;->mMlConnection:Lcom/mediatek/mobilelog/MlConnection;
    invoke-static {v6, v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$1402(Lcom/mediatek/mobilelog/MobileLogService;Lcom/mediatek/mobilelog/MlConnection;)Lcom/mediatek/mobilelog/MlConnection;

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mMlConnection:Lcom/mediatek/mobilelog/MlConnection;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$1400(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/MlConnection;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mediatek/mobilelog/MlConnection;->connect()Z

    :cond_14
    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$100(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/LogSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mediatek/mobilelog/LogSettings;->loadConfig()Ljava/util/Properties;

    move-result-object v4

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    const-string v7, "service_boot_option"

    invoke-virtual {v4, v7}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    # setter for: Lcom/mediatek/mobilelog/MobileLogService;->mBootOption:Ljava/lang/String;
    invoke-static {v6, v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$1502(Lcom/mediatek/mobilelog/MobileLogService;Ljava/lang/String;)Ljava/lang/String;

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    const-string v7, "log_path_type"

    invoke-virtual {v4, v7}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    # setter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogPathType:Ljava/lang/String;
    invoke-static {v6, v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$902(Lcom/mediatek/mobilelog/MobileLogService;Ljava/lang/String;)Ljava/lang/String;

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mBootOption:Ljava/lang/String;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$1500(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_15

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogPathType:Ljava/lang/String;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$900(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_16

    :cond_15
    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    iget-object v7, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;
    invoke-static {v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$100(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/LogSettings;

    move-result-object v7

    invoke-virtual {v7}, Lcom/mediatek/mobilelog/LogSettings;->getBootOption()Ljava/lang/String;

    move-result-object v7

    # setter for: Lcom/mediatek/mobilelog/MobileLogService;->mBootOption:Ljava/lang/String;
    invoke-static {v6, v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$1502(Lcom/mediatek/mobilelog/MobileLogService;Ljava/lang/String;)Ljava/lang/String;

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    iget-object v7, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;
    invoke-static {v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$100(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/LogSettings;

    move-result-object v7

    invoke-virtual {v7}, Lcom/mediatek/mobilelog/LogSettings;->getLogPathType()Ljava/lang/String;

    move-result-object v7

    # setter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogPathType:Ljava/lang/String;
    invoke-static {v6, v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$902(Lcom/mediatek/mobilelog/MobileLogService;Ljava/lang/String;)Ljava/lang/String;

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$100(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/LogSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mediatek/mobilelog/LogSettings;->recreateConfig()V

    :cond_16
    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mBootOption:Ljava/lang/String;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$1500(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mServiceOnBootEnabled:Ljava/lang/String;
    invoke-static {v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$1600(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogPathType:Ljava/lang/String;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$900(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogPathTypeSdCard:Ljava/lang/String;
    invoke-static {v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$1000(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # invokes: Lcom/mediatek/mobilelog/MobileLogService;->getAvailableExternalMemorySize()J
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$2200(Lcom/mediatek/mobilelog/MobileLogService;)J

    move-result-wide v0

    const-string v6, "MobileLogJ/Service"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "LogSize is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogSize:I
    invoke-static {v8}, Lcom/mediatek/mobilelog/MobileLogService;->access$2300(Lcom/mediatek/mobilelog/MobileLogService;)I

    move-result v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "MB and Current available externalMemory is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "MB!"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mLogSize:I
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$2300(Lcom/mediatek/mobilelog/MobileLogService;)I

    move-result v6

    add-int/lit8 v6, v6, 0xa

    int-to-long v6, v6

    cmp-long v6, v6, v0

    if-ltz v6, :cond_17

    const-string v6, "MobileLogJ/Service"

    const-string v7, "Send intent to CommonUI:SDcard full!"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->itCommonUI:Landroid/content/Intent;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$1700(Lcom/mediatek/mobilelog/MobileLogService;)Landroid/content/Intent;

    move-result-object v6

    const-string v7, "Return"

    const-string v8, "SDCardFull"

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    iget-object v7, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->itCommonUI:Landroid/content/Intent;
    invoke-static {v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$1700(Lcom/mediatek/mobilelog/MobileLogService;)Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/mediatek/mobilelog/MobileLogService;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    const v7, 0x1080074

    const/4 v8, 0x4

    # invokes: Lcom/mediatek/mobilelog/MobileLogService;->showNotification(II)V
    invoke-static {v6, v7, v8}, Lcom/mediatek/mobilelog/MobileLogService;->access$1100(Lcom/mediatek/mobilelog/MobileLogService;II)V

    const/4 v6, 0x5

    invoke-virtual {p0, v6}, Lcom/mediatek/mobilelog/MobileLogService$5;->sendEmptyMessage(I)Z

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    const/4 v7, 0x4

    # invokes: Lcom/mediatek/mobilelog/MobileLogService;->triggerAEEWarning(I)V
    invoke-static {v6, v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$600(Lcom/mediatek/mobilelog/MobileLogService;I)V

    goto/16 :goto_0

    :cond_17
    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mMlConnection:Lcom/mediatek/mobilelog/MlConnection;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$1400(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/MlConnection;

    move-result-object v6

    const-string v7, "copy"

    invoke-virtual {v6, v7}, Lcom/mediatek/mobilelog/MlConnection;->sendCmd(Ljava/lang/String;)V

    const-string v6, "MobileLogJ/Service"

    const-string v7, "send copy cmd"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Lcom/mediatek/mobilelog/MobileLogService$5;->removeMessages(I)V

    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Lcom/mediatek/mobilelog/MobileLogService$5;->obtainMessage(I)Landroid/os/Message;

    move-result-object v6

    const-wide/16 v7, 0x1388

    invoke-virtual {p0, v6, v7, v8}, Lcom/mediatek/mobilelog/MobileLogService$5;->sendMessageDelayed(Landroid/os/Message;J)Z

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    const/4 v7, 0x1

    # setter for: Lcom/mediatek/mobilelog/MobileLogService;->mNeedCheck:Z
    invoke-static {v6, v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$2402(Lcom/mediatek/mobilelog/MobileLogService;Z)Z

    goto/16 :goto_0

    :pswitch_b
    const-string v6, "MobileLogJ/Service"

    const-string v7, "send IPO_BOOT cmd"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mMlConnection:Lcom/mediatek/mobilelog/MlConnection;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$1400(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/MlConnection;

    move-result-object v6

    if-nez v6, :cond_18

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    new-instance v7, Lcom/mediatek/mobilelog/MlConnection;

    const-string v8, "mobilelogd"

    iget-object v9, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mHandler:Landroid/os/Handler;
    invoke-static {v9}, Lcom/mediatek/mobilelog/MobileLogService;->access$500(Lcom/mediatek/mobilelog/MobileLogService;)Landroid/os/Handler;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lcom/mediatek/mobilelog/MlConnection;-><init>(Ljava/lang/String;Landroid/os/Handler;)V

    # setter for: Lcom/mediatek/mobilelog/MobileLogService;->mMlConnection:Lcom/mediatek/mobilelog/MlConnection;
    invoke-static {v6, v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$1402(Lcom/mediatek/mobilelog/MobileLogService;Lcom/mediatek/mobilelog/MlConnection;)Lcom/mediatek/mobilelog/MlConnection;

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mMlConnection:Lcom/mediatek/mobilelog/MlConnection;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$1400(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/MlConnection;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mediatek/mobilelog/MlConnection;->connect()Z

    :cond_18
    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mMlConnection:Lcom/mediatek/mobilelog/MlConnection;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$1400(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/MlConnection;

    move-result-object v6

    const-string v7, "IPO_BOOT"

    invoke-virtual {v6, v7}, Lcom/mediatek/mobilelog/MlConnection;->sendCmd(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    # setter for: Lcom/mediatek/mobilelog/MobileLogService;->isIpoShutdown:Ljava/lang/Boolean;
    invoke-static {v6, v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$402(Lcom/mediatek/mobilelog/MobileLogService;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Lcom/mediatek/mobilelog/MobileLogService$5;->removeMessages(I)V

    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Lcom/mediatek/mobilelog/MobileLogService$5;->obtainMessage(I)Landroid/os/Message;

    move-result-object v6

    const-wide/16 v7, 0x1388

    invoke-virtual {p0, v6, v7, v8}, Lcom/mediatek/mobilelog/MobileLogService$5;->sendMessageDelayed(Landroid/os/Message;J)Z

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    const/4 v7, 0x1

    # setter for: Lcom/mediatek/mobilelog/MobileLogService;->mNeedCheck:Z
    invoke-static {v6, v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$2402(Lcom/mediatek/mobilelog/MobileLogService;Z)Z

    goto/16 :goto_0

    :pswitch_c
    const-string v6, "MobileLogJ/Service"

    const-string v7, "send IPO_SHUTDOWN cmd"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mMlConnection:Lcom/mediatek/mobilelog/MlConnection;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$1400(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/MlConnection;

    move-result-object v6

    if-nez v6, :cond_19

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    new-instance v7, Lcom/mediatek/mobilelog/MlConnection;

    const-string v8, "mobilelogd"

    iget-object v9, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mHandler:Landroid/os/Handler;
    invoke-static {v9}, Lcom/mediatek/mobilelog/MobileLogService;->access$500(Lcom/mediatek/mobilelog/MobileLogService;)Landroid/os/Handler;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lcom/mediatek/mobilelog/MlConnection;-><init>(Ljava/lang/String;Landroid/os/Handler;)V

    # setter for: Lcom/mediatek/mobilelog/MobileLogService;->mMlConnection:Lcom/mediatek/mobilelog/MlConnection;
    invoke-static {v6, v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$1402(Lcom/mediatek/mobilelog/MobileLogService;Lcom/mediatek/mobilelog/MlConnection;)Lcom/mediatek/mobilelog/MlConnection;

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mMlConnection:Lcom/mediatek/mobilelog/MlConnection;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$1400(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/MlConnection;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mediatek/mobilelog/MlConnection;->connect()Z

    :cond_19
    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    # getter for: Lcom/mediatek/mobilelog/MobileLogService;->mMlConnection:Lcom/mediatek/mobilelog/MlConnection;
    invoke-static {v6}, Lcom/mediatek/mobilelog/MobileLogService;->access$1400(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/MlConnection;

    move-result-object v6

    const-string v7, "IPO_SHUTDOWN"

    invoke-virtual {v6, v7}, Lcom/mediatek/mobilelog/MlConnection;->sendCmd(Ljava/lang/String;)V

    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Lcom/mediatek/mobilelog/MobileLogService$5;->removeMessages(I)V

    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Lcom/mediatek/mobilelog/MobileLogService$5;->obtainMessage(I)Landroid/os/Message;

    move-result-object v6

    const-wide/16 v7, 0x1388

    invoke-virtual {p0, v6, v7, v8}, Lcom/mediatek/mobilelog/MobileLogService$5;->sendMessageDelayed(Landroid/os/Message;J)Z

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService$5;->this$0:Lcom/mediatek/mobilelog/MobileLogService;

    const/4 v7, 0x1

    # setter for: Lcom/mediatek/mobilelog/MobileLogService;->mNeedCheck:Z
    invoke-static {v6, v7}, Lcom/mediatek/mobilelog/MobileLogService;->access$2402(Lcom/mediatek/mobilelog/MobileLogService;Z)Z

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_4
        :pswitch_6
        :pswitch_5
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_b
        :pswitch_c
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
