.class public Lcom/mediatek/mobilelog/MobileLogService;
.super Landroid/app/Service;
.source "MobileLogService.java"


# static fields
.field public static final EXTRA_COMMON_UI:Ljava/lang/String; = "com.meidatek.syslogger.action"

.field public static final EXTRA_Device_Bootup:Ljava/lang/String; = "com.mediatek.mobilelog.DEVICEBOOTUP"

.field public static final EXTRA_FILESIZE:Ljava/lang/String; = "com.mediatek.mobilelog.LOGSIZE"

.field public static final EXTRA_LOGPATH:Ljava/lang/String; = "com.mediatek.mobilelog.LOGPATH"

.field public static final EXTRA_LOGPATHTYPE:Ljava/lang/String; = "com.mediatek.mobilelog.LOGPATHTYPE"

.field public static final LOW_STORAGE:Ljava/lang/String; = "com.mediatek.mobilelog.LOW_STORAGE"

.field public static final MAX_LOG_SIZE:I = 0x400

.field private static final MSG_ALIVE:I = 0x4

.field private static final MSG_CHECK:I = 0x1

.field private static final MSG_CONN_FAIL:I = 0x2

.field private static final MSG_DIE:I = 0x3

.field private static final MSG_INIT:I = 0x0

.field private static final MSG_IPO_BOOT:I = 0x9

.field private static final MSG_IPO_SHUTDOWN:I = 0xa

.field private static final MSG_LOW:I = 0x6

.field private static final MSG_SD:I = 0x7

.field private static final MSG_SD_TIMEOUT:I = 0xd

.field private static final MSG_START:I = 0xc

.field private static final MSG_STOP:I = 0x5

.field public static final SDCARD_ALMOST_FULL:Ljava/lang/String; = "com.mediatek.mobilelog.SDCARD_ALMOST_FULL"

.field public static final SDCARD_DAMAGE:Ljava/lang/String; = "com.mediatek.mobilelog.SDCARD_DAMAGE"

.field private static final SD_TIMEOUT:I = 0x1d4c0

.field static final SERVICE_START_ACTION:Ljava/lang/String; = "com.mediatek.mobilelog.MobileLogService"

.field private static final TAG:Ljava/lang/String; = "MobileLogJ/Service"

.field protected static sSelf:Lcom/mediatek/mobilelog/MobileLogService;


# instance fields
.field private curWriteToPhone:Z

.field private exStorageManager:Landroid/os/storage/StorageManager;

.field private exceptiontype:I

.field private isBootup:Ljava/lang/String;

.field private isIpoBootUp:Ljava/lang/Boolean;

.field private isIpoShutdown:Ljava/lang/Boolean;

.field private itCommonUI:Landroid/content/Intent;

.field private mBootOption:Ljava/lang/String;

.field private final mHandler:Landroid/os/Handler;

.field private mIntentFilterCommonUI:Landroid/content/IntentFilter;

.field private mIntentFilterIPO:Landroid/content/IntentFilter;

.field private mIntentFilterSDCard:Landroid/content/IntentFilter;

.field private mIntentFilterStorage:Landroid/content/IntentFilter;

.field private mIntentReceiverCommonUI:Landroid/content/BroadcastReceiver;

.field private mIntentReceiverIPO:Landroid/content/BroadcastReceiver;

.field private mIntentReceiverSDCard:Landroid/content/BroadcastReceiver;

.field private mIntentReceiverStorage:Landroid/content/BroadcastReceiver;

.field private mLogPath:Ljava/lang/String;

.field private mLogPathType:Ljava/lang/String;

.field private mLogPathTypePhone:Ljava/lang/String;

.field private mLogPathTypeSdCard:Ljava/lang/String;

.field private mLogSettings:Lcom/mediatek/mobilelog/LogSettings;

.field private mLogSize:I

.field private mMlConnection:Lcom/mediatek/mobilelog/MlConnection;

.field private mNM:Landroid/app/NotificationManager;

.field private mNeedCheck:Z

.field private mServiceOnBootDisabled:Ljava/lang/String;

.field private mServiceOnBootEnabled:Ljava/lang/String;

.field private mServiceRunning:Ljava/lang/String;

.field private mServiceStartType:Ljava/lang/String;

.field private mServiceStopped:Ljava/lang/String;

.field private mStatusOn:Z

.field private sdMounted:Z

.field private shouldcpy:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    iput-boolean v2, p0, Lcom/mediatek/mobilelog/MobileLogService;->mStatusOn:Z

    iput-object v3, p0, Lcom/mediatek/mobilelog/MobileLogService;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;

    const-string v0, "true"

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mServiceOnBootEnabled:Ljava/lang/String;

    const-string v0, "false"

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mServiceOnBootDisabled:Ljava/lang/String;

    const-string v0, "service_running"

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mServiceRunning:Ljava/lang/String;

    const-string v0, "service_stopped"

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mServiceStopped:Ljava/lang/String;

    const-string v0, "phone"

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mLogPathTypePhone:Ljava/lang/String;

    const-string v0, "sdcard"

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mLogPathTypeSdCard:Ljava/lang/String;

    const-string v0, "com.mediatek.Service.BOOT"

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mServiceStartType:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->shouldcpy:Ljava/lang/Boolean;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->isIpoShutdown:Ljava/lang/Boolean;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->isIpoBootUp:Ljava/lang/Boolean;

    const-string v0, "false"

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->isBootup:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/mediatek/mobilelog/MobileLogService;->curWriteToPhone:Z

    iput-object v3, p0, Lcom/mediatek/mobilelog/MobileLogService;->mMlConnection:Lcom/mediatek/mobilelog/MlConnection;

    iput-boolean v2, p0, Lcom/mediatek/mobilelog/MobileLogService;->mNeedCheck:Z

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.mediatek.syslogger.action"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->itCommonUI:Landroid/content/Intent;

    iput-boolean v2, p0, Lcom/mediatek/mobilelog/MobileLogService;->sdMounted:Z

    iput-object v3, p0, Lcom/mediatek/mobilelog/MobileLogService;->exStorageManager:Landroid/os/storage/StorageManager;

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->exceptiontype:I

    new-instance v0, Lcom/mediatek/mobilelog/MobileLogService$1;

    invoke-direct {v0, p0}, Lcom/mediatek/mobilelog/MobileLogService$1;-><init>(Lcom/mediatek/mobilelog/MobileLogService;)V

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mIntentReceiverSDCard:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/mediatek/mobilelog/MobileLogService$2;

    invoke-direct {v0, p0}, Lcom/mediatek/mobilelog/MobileLogService$2;-><init>(Lcom/mediatek/mobilelog/MobileLogService;)V

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mIntentReceiverStorage:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/mediatek/mobilelog/MobileLogService$3;

    invoke-direct {v0, p0}, Lcom/mediatek/mobilelog/MobileLogService$3;-><init>(Lcom/mediatek/mobilelog/MobileLogService;)V

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mIntentReceiverCommonUI:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/mediatek/mobilelog/MobileLogService$4;

    invoke-direct {v0, p0}, Lcom/mediatek/mobilelog/MobileLogService$4;-><init>(Lcom/mediatek/mobilelog/MobileLogService;)V

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mIntentReceiverIPO:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/mediatek/mobilelog/MobileLogService$5;

    invoke-direct {v0, p0}, Lcom/mediatek/mobilelog/MobileLogService$5;-><init>(Lcom/mediatek/mobilelog/MobileLogService;)V

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/MobileLogService;

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mLogPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$002(Lcom/mediatek/mobilelog/MobileLogService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/mobilelog/MobileLogService;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/mobilelog/MobileLogService;->mLogPath:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/LogSettings;
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/MobileLogService;

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/MobileLogService;

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mLogPathTypeSdCard:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/mediatek/mobilelog/MobileLogService;II)V
    .locals 0
    .param p0    # Lcom/mediatek/mobilelog/MobileLogService;
    .param p1    # I
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/mediatek/mobilelog/MobileLogService;->showNotification(II)V

    return-void
.end method

.method static synthetic access$1200(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/MobileLogService;

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mServiceOnBootDisabled:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/MobileLogService;

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mServiceStopped:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/mediatek/mobilelog/MobileLogService;)Lcom/mediatek/mobilelog/MlConnection;
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/MobileLogService;

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mMlConnection:Lcom/mediatek/mobilelog/MlConnection;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/mediatek/mobilelog/MobileLogService;Lcom/mediatek/mobilelog/MlConnection;)Lcom/mediatek/mobilelog/MlConnection;
    .locals 0
    .param p0    # Lcom/mediatek/mobilelog/MobileLogService;
    .param p1    # Lcom/mediatek/mobilelog/MlConnection;

    iput-object p1, p0, Lcom/mediatek/mobilelog/MobileLogService;->mMlConnection:Lcom/mediatek/mobilelog/MlConnection;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/MobileLogService;

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mBootOption:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/mediatek/mobilelog/MobileLogService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/mobilelog/MobileLogService;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/mobilelog/MobileLogService;->mBootOption:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/MobileLogService;

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mServiceOnBootEnabled:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/mediatek/mobilelog/MobileLogService;)Landroid/content/Intent;
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/MobileLogService;

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->itCommonUI:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/MobileLogService;

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mServiceStartType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/mediatek/mobilelog/MobileLogService;)V
    .locals 0
    .param p0    # Lcom/mediatek/mobilelog/MobileLogService;

    invoke-direct {p0}, Lcom/mediatek/mobilelog/MobileLogService;->checkMobileLogStatus()V

    return-void
.end method

.method static synthetic access$200(Lcom/mediatek/mobilelog/MobileLogService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/MobileLogService;

    iget-boolean v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->sdMounted:Z

    return v0
.end method

.method static synthetic access$2000(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/MobileLogService;

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mLogPathTypePhone:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$202(Lcom/mediatek/mobilelog/MobileLogService;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/mobilelog/MobileLogService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/mobilelog/MobileLogService;->sdMounted:Z

    return p1
.end method

.method static synthetic access$2100(Lcom/mediatek/mobilelog/MobileLogService;)J
    .locals 2
    .param p0    # Lcom/mediatek/mobilelog/MobileLogService;

    invoke-direct {p0}, Lcom/mediatek/mobilelog/MobileLogService;->getAvailableInternalMemorySize()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$2200(Lcom/mediatek/mobilelog/MobileLogService;)J
    .locals 2
    .param p0    # Lcom/mediatek/mobilelog/MobileLogService;

    invoke-direct {p0}, Lcom/mediatek/mobilelog/MobileLogService;->getAvailableExternalMemorySize()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$2300(Lcom/mediatek/mobilelog/MobileLogService;)I
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/MobileLogService;

    iget v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mLogSize:I

    return v0
.end method

.method static synthetic access$2400(Lcom/mediatek/mobilelog/MobileLogService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/MobileLogService;

    iget-boolean v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mNeedCheck:Z

    return v0
.end method

.method static synthetic access$2402(Lcom/mediatek/mobilelog/MobileLogService;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/mobilelog/MobileLogService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/mobilelog/MobileLogService;->mNeedCheck:Z

    return p1
.end method

.method static synthetic access$2500(Lcom/mediatek/mobilelog/MobileLogService;)I
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/MobileLogService;

    iget v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->exceptiontype:I

    return v0
.end method

.method static synthetic access$300(Lcom/mediatek/mobilelog/MobileLogService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/MobileLogService;

    iget-boolean v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->curWriteToPhone:Z

    return v0
.end method

.method static synthetic access$302(Lcom/mediatek/mobilelog/MobileLogService;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/mobilelog/MobileLogService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/mobilelog/MobileLogService;->curWriteToPhone:Z

    return p1
.end method

.method static synthetic access$400(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/Boolean;
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/MobileLogService;

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->isIpoShutdown:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic access$402(Lcom/mediatek/mobilelog/MobileLogService;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .param p0    # Lcom/mediatek/mobilelog/MobileLogService;
    .param p1    # Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/mediatek/mobilelog/MobileLogService;->isIpoShutdown:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic access$500(Lcom/mediatek/mobilelog/MobileLogService;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/MobileLogService;

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$600(Lcom/mediatek/mobilelog/MobileLogService;I)V
    .locals 0
    .param p0    # Lcom/mediatek/mobilelog/MobileLogService;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/mobilelog/MobileLogService;->triggerAEEWarning(I)V

    return-void
.end method

.method static synthetic access$700(Lcom/mediatek/mobilelog/MobileLogService;)Landroid/os/storage/StorageManager;
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/MobileLogService;

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->exStorageManager:Landroid/os/storage/StorageManager;

    return-object v0
.end method

.method static synthetic access$800(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/Boolean;
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/MobileLogService;

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->shouldcpy:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic access$802(Lcom/mediatek/mobilelog/MobileLogService;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .param p0    # Lcom/mediatek/mobilelog/MobileLogService;
    .param p1    # Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/mediatek/mobilelog/MobileLogService;->shouldcpy:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic access$900(Lcom/mediatek/mobilelog/MobileLogService;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/mobilelog/MobileLogService;

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mLogPathType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$902(Lcom/mediatek/mobilelog/MobileLogService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/mobilelog/MobileLogService;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/mobilelog/MobileLogService;->mLogPathType:Ljava/lang/String;

    return-object p1
.end method

.method private checkMobileLogStatus()V
    .locals 5

    const-string v1, "MobileLogJ/Service"

    const-string v2, "check status!"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "MobileLogJ/Service"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "nand size is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0}, Lcom/mediatek/mobilelog/MobileLogService;->getAvailableInternalMemorySize()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/mobilelog/MobileLogService;->getAvailableInternalMemorySize()J

    move-result-wide v1

    const-wide/16 v3, 0x2

    cmp-long v1, v1, v3

    if-gtz v1, :cond_0

    const-string v1, "MobileLogJ/Service"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Current available size in nand is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0}, Lcom/mediatek/mobilelog/MobileLogService;->getAvailableInternalMemorySize()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mediatek/mobilelog/DialogActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "DialogTitle"

    const-string v2, "Warning"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "DialogMessage"

    const-string v2, "MobileLog is running under Low Storage, may loose some boot logs"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/mediatek/mobilelog/MobileLogService;->startActivity(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method private getAvailableExternalMemorySize()J
    .locals 9

    const-string v5, "mounted"

    iget-object v6, p0, Lcom/mediatek/mobilelog/MobileLogService;->exStorageManager:Landroid/os/storage/StorageManager;

    iget-object v7, p0, Lcom/mediatek/mobilelog/MobileLogService;->mLogPath:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    new-instance v4, Landroid/os/StatFs;

    iget-object v5, p0, Lcom/mediatek/mobilelog/MobileLogService;->mLogPath:Ljava/lang/String;

    invoke-direct {v4, v5}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Landroid/os/StatFs;->getBlockSize()I

    move-result v5

    int-to-long v2, v5

    invoke-virtual {v4}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v5

    int-to-long v0, v5

    mul-long v5, v0, v2

    const-wide/32 v7, 0x100000

    div-long/2addr v5, v7

    :goto_0
    return-wide v5

    :cond_0
    const-wide/16 v5, 0x0

    goto :goto_0
.end method

.method private getAvailableInternalMemorySize()J
    .locals 10

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v4

    new-instance v5, Landroid/os/StatFs;

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Landroid/os/StatFs;->getBlockSize()I

    move-result v6

    int-to-long v2, v6

    invoke-virtual {v5}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v6

    int-to-long v0, v6

    mul-long v6, v0, v2

    const-wide/32 v8, 0x100000

    div-long/2addr v6, v8

    return-wide v6
.end method

.method private showNotification(II)V
    .locals 12
    .param p1    # I
    .param p2    # I

    const v11, 0x7f070016

    const v10, 0x7f070015

    const v9, 0x7f070014

    const v8, 0x7f070013

    const/4 v7, 0x0

    invoke-virtual {p0, v8}, Lcom/mediatek/mobilelog/MobileLogService;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    packed-switch p2, :pswitch_data_0

    :goto_0
    new-instance v2, Landroid/app/Notification;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v2, p1, v3, v4, v5}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    const/4 v1, 0x0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    new-instance v4, Landroid/content/ComponentName;

    const-string v5, "com.mediatek.engineermode"

    const-string v6, "com.mediatek.engineermode.syslogger.SysLogger"

    invoke-direct {v4, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/mediatek/mobilelog/MobileLogService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {v4, v0, v7}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {p0, v7, v0, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    :goto_1
    invoke-virtual {p0, v8}, Lcom/mediatek/mobilelog/MobileLogService;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v2, p0, v4, v3, v1}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    packed-switch p2, :pswitch_data_1

    :goto_2
    return-void

    :pswitch_0
    invoke-virtual {p0, v9}, Lcom/mediatek/mobilelog/MobileLogService;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0, v10}, Lcom/mediatek/mobilelog/MobileLogService;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0, v11}, Lcom/mediatek/mobilelog/MobileLogService;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    goto :goto_0

    :pswitch_3
    const v4, 0x7f070017

    invoke-virtual {p0, v4}, Lcom/mediatek/mobilelog/MobileLogService;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    goto :goto_0

    :pswitch_4
    const v4, 0x7f070018

    invoke-virtual {p0, v4}, Lcom/mediatek/mobilelog/MobileLogService;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    goto :goto_0

    :cond_0
    new-instance v4, Landroid/content/Intent;

    const-class v5, Lcom/mediatek/mobilelog/MobileLog;

    invoke-direct {v4, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {p0, v7, v4, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    goto :goto_1

    :pswitch_5
    invoke-virtual {p0, v9, v2}, Lcom/mediatek/mobilelog/MobileLogService;->startForeground(ILandroid/app/Notification;)V

    goto :goto_2

    :pswitch_6
    invoke-virtual {p0, v10, v2}, Lcom/mediatek/mobilelog/MobileLogService;->startForeground(ILandroid/app/Notification;)V

    goto :goto_2

    :pswitch_7
    invoke-virtual {p0, v11, v2}, Lcom/mediatek/mobilelog/MobileLogService;->startForeground(ILandroid/app/Notification;)V

    goto :goto_2

    :pswitch_8
    const v4, 0x7f070017

    invoke-virtual {p0, v4, v2}, Lcom/mediatek/mobilelog/MobileLogService;->startForeground(ILandroid/app/Notification;)V

    goto :goto_2

    :pswitch_9
    const v4, 0x7f070018

    invoke-virtual {p0, v4, v2}, Lcom/mediatek/mobilelog/MobileLogService;->startForeground(ILandroid/app/Notification;)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method private showStorageFull(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "SDCardFull"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "msg"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/mediatek/mobilelog/MobileLogService;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method private triggerAEEWarning(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/mediatek/mobilelog/MobileLogService;->exceptiontype:I

    const/4 v0, 0x0

    new-instance v0, Lcom/mediatek/mobilelog/MobileLogService$6;

    invoke-direct {v0, p0}, Lcom/mediatek/mobilelog/MobileLogService$6;-><init>(Lcom/mediatek/mobilelog/MobileLogService;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    const-string v0, "MobileLogJ/Service"

    const-string v1, "MobileLogService onCreate"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    sput-object p0, Lcom/mediatek/mobilelog/MobileLogService;->sSelf:Lcom/mediatek/mobilelog/MobileLogService;

    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/mediatek/mobilelog/MobileLogService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mNM:Landroid/app/NotificationManager;

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->exStorageManager:Landroid/os/storage/StorageManager;

    if-nez v0, :cond_0

    const-string v0, "storage"

    invoke-virtual {p0, v0}, Lcom/mediatek/mobilelog/MobileLogService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/storage/StorageManager;

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->exStorageManager:Landroid/os/storage/StorageManager;

    :cond_0
    new-instance v0, Lcom/mediatek/mobilelog/LogSettings;

    const-string v1, "/data/data/com.mediatek.mobilelog/files"

    invoke-direct {v0, v1}, Lcom/mediatek/mobilelog/LogSettings;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->itCommonUI:Landroid/content/Intent;

    const-string v1, "From"

    const-string v2, "MobileLog"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->itCommonUI:Landroid/content/Intent;

    const-string v1, "To"

    const-string v2, "CommonUI"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->itCommonUI:Landroid/content/Intent;

    const-string v1, "Command"

    const-string v2, "Start"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mIntentFilterSDCard:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mIntentFilterSDCard:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_BAD_REMOVAL"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mIntentFilterSDCard:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mIntentFilterSDCard:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mIntentFilterSDCard:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mIntentFilterSDCard:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mIntentFilterSDCard:Landroid/content/IntentFilter;

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mIntentReceiverSDCard:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLogService;->mIntentFilterSDCard:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/mediatek/mobilelog/MobileLogService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mIntentFilterStorage:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mIntentFilterStorage:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.DEVICE_STORAGE_LOW"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mIntentFilterStorage:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.DEVICE_STORAGE_OK"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mIntentReceiverStorage:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLogService;->mIntentFilterStorage:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/mediatek/mobilelog/MobileLogService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mIntentFilterCommonUI:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mIntentFilterCommonUI:Landroid/content/IntentFilter;

    const-string v1, "com.mediatek.syslogger.action"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mIntentReceiverCommonUI:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLogService;->mIntentFilterCommonUI:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/mediatek/mobilelog/MobileLogService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mIntentFilterIPO:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mIntentFilterIPO:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.ACTION_SHUTDOWN_IPO"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mIntentReceiverIPO:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLogService;->mIntentFilterIPO:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/mediatek/mobilelog/MobileLogService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public onDestroy()V
    .locals 3

    const-string v1, "MobileLogJ/Service"

    const-string v2, "MobileLogService onDestroy"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    sput-object v1, Lcom/mediatek/mobilelog/MobileLogService;->sSelf:Lcom/mediatek/mobilelog/MobileLogService;

    const-string v1, "ro.monkey"

    const-string v2, "false"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "MobileLogJ/Service"

    const-string v2, "Monkey is running, ignore service stop"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLogService;->mIntentReceiverSDCard:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/mediatek/mobilelog/MobileLogService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLogService;->mIntentReceiverStorage:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/mediatek/mobilelog/MobileLogService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLogService;->mIntentReceiverCommonUI:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/mediatek/mobilelog/MobileLogService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLogService;->mIntentReceiverIPO:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/mediatek/mobilelog/MobileLogService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLogService;->mMlConnection:Lcom/mediatek/mobilelog/MlConnection;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLogService;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLogService;->mMlConnection:Lcom/mediatek/mobilelog/MlConnection;

    const-string v2, "stop"

    invoke-virtual {v1, v2}, Lcom/mediatek/mobilelog/MlConnection;->sendCmd(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLogService;->mMlConnection:Lcom/mediatek/mobilelog/MlConnection;

    invoke-virtual {v1}, Lcom/mediatek/mobilelog/MlConnection;->stop()V

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/mediatek/mobilelog/MobileLogService;->updateServiceStatus(Z)V

    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLogService;->mNM:Landroid/app/NotificationManager;

    const v2, 0x7f070014

    invoke-virtual {v1, v2}, Landroid/app/NotificationManager;->cancel(I)V

    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLogService;->mNM:Landroid/app/NotificationManager;

    invoke-virtual {v1}, Landroid/app/NotificationManager;->cancelAll()V

    goto :goto_0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    const/4 v3, 0x0

    if-nez p1, :cond_1

    const-string v0, "MobileLogJ/Service"

    const-string v1, "onStartCommand intent == null!"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "false"

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->isBootup:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->isIpoShutdown:Ljava/lang/Boolean;

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const/4 v0, 0x1

    return v0

    :cond_1
    const-string v0, "MobileLogJ/Service"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStartCommand "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " flags "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "com.mediatek.mobilelog.DEVICEBOOTUP"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->isBootup:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "com.mediatek.Service.type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->mServiceStartType:Ljava/lang/String;

    const-string v0, "MobileLogJ/Service"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Start type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/mobilelog/MobileLogService;->mServiceStartType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->isBootup:Ljava/lang/String;

    if-nez v0, :cond_2

    const-string v0, "false"

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->isBootup:Ljava/lang/String;

    :cond_2
    iget-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->isBootup:Ljava/lang/String;

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/mobilelog/MobileLogService;->isIpoShutdown:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public readConfig()Z
    .locals 4

    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLogService;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;

    invoke-virtual {v1}, Lcom/mediatek/mobilelog/LogSettings;->loadConfig()Ljava/util/Properties;

    move-result-object v0

    const-string v1, "log_size"

    const-string v2, "300"

    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/mediatek/mobilelog/MobileLogService;->mLogSize:I

    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLogService;->exStorageManager:Landroid/os/storage/StorageManager;

    if-nez v1, :cond_0

    const-string v1, "storage"

    invoke-virtual {p0, v1}, Lcom/mediatek/mobilelog/MobileLogService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/storage/StorageManager;

    iput-object v1, p0, Lcom/mediatek/mobilelog/MobileLogService;->exStorageManager:Landroid/os/storage/StorageManager;

    :cond_0
    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLogService;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;

    invoke-virtual {p0}, Lcom/mediatek/mobilelog/MobileLogService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/mediatek/mobilelog/LogSettings;->getRightPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/mobilelog/MobileLogService;->mLogPath:Ljava/lang/String;

    const-string v1, "MobileLogJ/Service"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mLogPath first get: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/mobilelog/MobileLogService;->mLogPath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x1

    return v1
.end method

.method public updateServiceStatus(Z)V
    .locals 5
    .param p1    # Z

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLogService;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;

    invoke-virtual {v1}, Lcom/mediatek/mobilelog/LogSettings;->loadConfig()Ljava/util/Properties;

    move-result-object v0

    if-eqz p1, :cond_2

    iget-boolean v1, p0, Lcom/mediatek/mobilelog/MobileLogService;->mStatusOn:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-boolean v4, p0, Lcom/mediatek/mobilelog/MobileLogService;->mStatusOn:Z

    const-string v1, "service_status"

    iget-object v2, p0, Lcom/mediatek/mobilelog/MobileLogService;->mServiceRunning:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const v1, 0x1080074

    invoke-direct {p0, v1, v3}, Lcom/mediatek/mobilelog/MobileLogService;->showNotification(II)V

    :goto_1
    iget-object v1, p0, Lcom/mediatek/mobilelog/MobileLogService;->mLogSettings:Lcom/mediatek/mobilelog/LogSettings;

    invoke-virtual {v1, v0}, Lcom/mediatek/mobilelog/LogSettings;->saveConfig(Ljava/util/Properties;)V

    const-string v1, "MobileLogJ/Service"

    const-string v2, "leave updateServiceStatus"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    iget-boolean v1, p0, Lcom/mediatek/mobilelog/MobileLogService;->mStatusOn:Z

    if-eqz v1, :cond_0

    iput-boolean v3, p0, Lcom/mediatek/mobilelog/MobileLogService;->mStatusOn:Z

    const-string v1, "service_status"

    iget-object v2, p0, Lcom/mediatek/mobilelog/MobileLogService;->mServiceStopped:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const v1, 0x1080075

    invoke-direct {p0, v1, v4}, Lcom/mediatek/mobilelog/MobileLogService;->showNotification(II)V

    goto :goto_1
.end method
