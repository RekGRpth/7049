.class public final Lcom/mediatek/mobilelog/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/mobilelog/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final activity_name_filter_list:I = 0x7f070002

.field public static final aee_clean_data:I = 0x7f07002e

.field public static final aee_clear_redscreen:I = 0x7f07002d

.field public static final aee_service_connected:I = 0x7f070037

.field public static final aee_service_disconnected:I = 0x7f070038

.field public static final aee_service_label:I = 0x7f070034

.field public static final aee_service_started:I = 0x7f070035

.field public static final aee_service_stopped:I = 0x7f070036

.field public static final android_filter_setting:I = 0x7f070019

.field public static final app_debugutils:I = 0x7f07002c

.field public static final app_name:I = 0x7f070001

.field public static final btn_clearlogs:I = 0x7f070027

.field public static final check_box_android:I = 0x7f07000b

.field public static final check_box_bt:I = 0x7f07000d

.field public static final check_box_kernel:I = 0x7f07000c

.field public static final check_box_option:I = 0x7f07000a

.field public static final clear_dlg_content:I = 0x7f07002b

.field public static final clear_dlg_title:I = 0x7f07002a

.field public static final dialog_error:I = 0x7f070010

.field public static final dialog_title_list_preference_aeemode:I = 0x7f070033

.field public static final error_invalid_file_path:I = 0x7f07000e

.field public static final error_invalid_log_size:I = 0x7f07000f

.field public static final filter_edit:I = 0x7f07001d

.field public static final filter_list:I = 0x7f07001c

.field public static final hello:I = 0x7f070000

.field public static final level_debug:I = 0x7f070022

.field public static final level_error:I = 0x7f070025

.field public static final level_info:I = 0x7f070023

.field public static final level_verbose:I = 0x7f070021

.field public static final level_warn:I = 0x7f070024

.field public static final log_path_default:I = 0x7f070004

.field public static final log_path_hint:I = 0x7f070003

.field public static final log_size_default:I = 0x7f070008

.field public static final log_size_hint:I = 0x7f070007

.field public static final message_deletelog:I = 0x7f070029

.field public static final mobilelog_service_label:I = 0x7f070013

.field public static final mobilelog_service_lownand:I = 0x7f070017

.field public static final mobilelog_service_nosd:I = 0x7f070016

.field public static final mobilelog_service_sdfull:I = 0x7f070018

.field public static final mobilelog_service_start:I = 0x7f070014

.field public static final mobilelog_service_stopped:I = 0x7f070015

.field public static final module_level_hint:I = 0x7f070020

.field public static final module_name_default:I = 0x7f07001f

.field public static final module_name_hint:I = 0x7f07001e

.field public static final new_filter:I = 0x7f07001b

.field public static final phone_path:I = 0x7f070005

.field public static final preference_aeemode:I = 0x7f070030

.field public static final preference_cleandata:I = 0x7f07003e

.field public static final preference_cleardal:I = 0x7f070039

.field public static final preferences_aee_settings:I = 0x7f07002f

.field public static final preferences_device_maintain:I = 0x7f07003d

.field public static final sd_path:I = 0x7f070006

.field public static final set_filter:I = 0x7f070026

.field public static final show_available_space:I = 0x7f070009

.field public static final start:I = 0x7f070011

.field public static final stop:I = 0x7f070012

.field public static final summary_preference_aeemode:I = 0x7f070032

.field public static final summary_preference_cleandata:I = 0x7f07003f

.field public static final summary_preference_cleardal:I = 0x7f07003a

.field public static final title_confirm:I = 0x7f070028

.field public static final title_preference_aeemode:I = 0x7f070031

.field public static final title_preference_cleandata:I = 0x7f070040

.field public static final title_preference_cleardal:I = 0x7f07003b

.field public static final title_preferences_device_maintain:I = 0x7f07003c

.field public static final version:I = 0x7f07001a


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
