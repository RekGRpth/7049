.class public Lcom/mediatek/mobilelog/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# static fields
.field public static final ACTION_ACTIVITY_START:Ljava/lang/String; = "com.mediatek.mobilelog.APLogger_START"

.field public static final ACTION_ACTIVITY_STOP:Ljava/lang/String; = "com.mediatek.mobilelog.APLogger_STOP"

.field public static final ACTION_BOOT_COMPLETE:Ljava/lang/String; = "android.intent.action.BOOT_COMPLETED"

.field public static final ACTION_EM_SYSTEM_LOGGER:Ljava/lang/String; = "com.mediatek.syslogger.action"

.field public static final ACTION_IPO_BOOT:Ljava/lang/String; = "android.intent.action.ACTION_BOOT_IPO"

.field public static final ACTION_IPO_SHUTDOWN:Ljava/lang/String; = "android.intent.action.ACTION_SHUTDOWN_IPO"

.field public static final ACTION_SERVICE_BOOT:Ljava/lang/String; = "com.mediatek.Service.BOOT"

.field public static final ACTION_SERVICE_COMMONUI:Ljava/lang/String; = "com.mediatek.Service.commonUI"

.field public static final ACTION_SERVICE_IPO:Ljava/lang/String; = "com.mediatek.Service.IPO"

.field public static final ACTION_SERVICE_NORMAL:Ljava/lang/String; = "com.mediatek.Service.normal"

.field public static final ACTION_SERVICE_SDMOUNTED:Ljava/lang/String; = "com.mediatek.Service.sdmounted"

.field public static final ACTION_SERVICE_TYPE:Ljava/lang/String; = "com.mediatek.Service.type"

.field public static final ACTION_START_SERVICE:Ljava/lang/String; = "com.mediatek.mobilelog.MobileLogService"

.field public static final BUILD_ENG:Ljava/lang/String; = "eng"

.field public static final BUILD_USER:Ljava/lang/String; = "user"

.field public static final CFG_ANDROID_LOG:Ljava/lang/String; = "AndroidLog"

.field public static final CFG_BOOT_OPTION:Ljava/lang/String; = "service_boot_option"

.field public static final CFG_BT_LOG:Ljava/lang/String; = "BTLog"

.field public static final CFG_KERNEL_LOG:Ljava/lang/String; = "KernelLog"

.field public static final CFG_LOG_PATH:Ljava/lang/String; = "log_path"

.field public static final CFG_LOG_PATH_TYPE:Ljava/lang/String; = "log_path_type"

.field public static final CFG_LOG_SIZE:Ljava/lang/String; = "log_size"

.field public static final CFG_SERVICE_STATUS:Ljava/lang/String; = "service_status"

.field public static final COMMAND_START:Ljava/lang/String; = "Start"

.field public static final COMMAND_STOP:Ljava/lang/String; = "Stop"

.field public static final COMMAND_UNKNOWN:Ljava/lang/String; = "Unknown"

.field public static final CUSTOMIZE_CONFIG_FILE:Ljava/lang/String; = "/system/etc/mtklog-config.prop"

.field public static final CUSTOMIZE_ITEM_BOOT:Ljava/lang/String; = "com.mediatek.log.mobile.enabled"

.field public static final CUSTOMIZE_ITEM_LOG_PATH:Ljava/lang/String; = "persist.radio.log2sd.path"

.field public static final CUSTOMIZE_ITEM_LOG_SIZE:Ljava/lang/String; = "com.mediatek.log.mobile.maxsize"

.field public static final DEFAULT_LOG_SIZE:Ljava/lang/String; = "300"

.field public static final EXTRA_FLAG_COMMON_UI_COMMAND:Ljava/lang/String; = "Command"

.field public static final EXTRA_FLAG_COMMON_UI_FROM:Ljava/lang/String; = "From"

.field public static final EXTRA_FLAG_COMMON_UI_RETURN:Ljava/lang/String; = "Return"

.field public static final EXTRA_FLAG_COMMON_UI_TO:Ljava/lang/String; = "To"

.field public static final FROM_ACTIVITY:I = 0x2

.field public static final FROM_COMMONUI:I = 0x3

.field public static final FROM_MDLOGGER:I = 0x1

.field public static final LOCAL_CFG_PATH:Ljava/lang/String; = "/data/data/com.mediatek.mobilelog/files"

.field public static final LOG_PATH_PHONE:Ljava/lang/String; = "/data"

.field public static final LOG_PATH_SDCARD:Ljava/lang/String; = "/mnt/sdcard"

.field public static final LOG_PATH_SDCARD2:Ljava/lang/String; = "/mnt/sdcard2"

.field public static final LOG_PATH_SUFFIX:Ljava/lang/String; = "/mtklog/mobilelog/"

.field public static final LOW_NAND_WATERLEVEL:I = 0x2

.field public static final NM_STATUS_LOWNAND:I = 0x3

.field public static final NM_STATUS_NOSD:I = 0x2

.field public static final NM_STATUS_SDFULL:I = 0x4

.field public static final NM_STATUS_START:I = 0x0

.field public static final NM_STATUS_STOP:I = 0x1

.field public static final PROP_BUILD_TYPE:Ljava/lang/String; = "ro.build.type"

.field public static final PROP_MONKEY:Ljava/lang/String; = "ro.monkey"

.field public static final PROP_RUNNING:Ljava/lang/String; = "debug.MB.running"

.field public static final PROP_SDCARD:Ljava/lang/String; = "persist.radio.log2sd.path"

.field public static final REAL_PATH_SDCARD:Ljava/lang/String; = "/storage/sdcard0"

.field public static final REAL_PATH_SDCARD2:Ljava/lang/String; = "/storage/sdcard1"

.field public static final RESERVE_SIZE:I = 0xa

.field public static final RETURN_CONFIG:Ljava/lang/String; = "ConfigFile"

.field public static final RETURN_DAEMON:Ljava/lang/String; = "DaemonUnable"

.field public static final RETURN_NANDFULL:Ljava/lang/String; = "NandFull"

.field public static final RETURN_NORMAL:Ljava/lang/String; = "Normal"

.field public static final RETURN_NOSD:Ljava/lang/String; = "NoSDCard"

.field public static final RETURN_SDFULL:Ljava/lang/String; = "SDCardFull"

.field public static final RETURN_UNKNOWN:Ljava/lang/String; = "Unknown"

.field public static final SOURCE_APLOGGER:Ljava/lang/String; = "MobileLog"

.field public static final SOURCE_COMMONUI:Ljava/lang/String; = "CommonUI"

.field public static final SOURCE_UNKNOWN:Ljava/lang/String; = "Unknown"

.field public static final WARN_BAD_REMOVAL:I = 0x0

.field public static final WARN_LOW_NAND:I = 0x1

.field public static final WARN_MESSAGE:[Ljava/lang/String;

.field public static final WARN_NO_DAEMON:I = 0x3

.field public static final WARN_NO_SIZE:I = 0x4

.field public static final WARN_SD_TIMEOUT:I = 0x2


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "MobileLog: bad removal"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "MobileLog: low storage"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "MobileLog: SD mount timeout"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "MobileLog: Cannot connect to Daemon"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "MobileLog: No enough size"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mediatek/mobilelog/Utils;->WARN_MESSAGE:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
