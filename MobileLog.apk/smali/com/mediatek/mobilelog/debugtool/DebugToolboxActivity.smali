.class public Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;
.super Landroid/preference/PreferenceActivity;
.source "DebugToolboxActivity.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "Mobilelog/Debugutils"

.field private static final sAEE_BUILD_INFO:Ljava/lang/String; = "ro.aee.build.info"

.field private static final sAEE_MODE:Ljava/lang/String; = "persist.mtk.aee.mode"

.field private static final sAEE_MODE_STRING:[Ljava/lang/String;

.field private static final sRO_BUILD_TYPE:Ljava/lang/String; = "ro.build.type"


# instance fields
.field cleanData:Landroid/preference/PreferenceScreen;

.field clearDAL:Landroid/preference/PreferenceScreen;

.field private mBoundService:Lcom/mediatek/mobilelog/debugtool/AEEControlService;

.field private mConnection:Landroid/content/ServiceConnection;

.field private mIsBound:Z

.field pref_aeemode:Landroid/preference/ListPreference;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "MediatekEngineer"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "MediatekUser"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "CustomerEngineer"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "CustomerUser"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->sAEE_MODE_STRING:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    new-instance v0, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity$1;

    invoke-direct {v0, p0}, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity$1;-><init>(Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;)V

    iput-object v0, p0, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->mConnection:Landroid/content/ServiceConnection;

    return-void
.end method

.method static synthetic access$002(Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;Lcom/mediatek/mobilelog/debugtool/AEEControlService;)Lcom/mediatek/mobilelog/debugtool/AEEControlService;
    .locals 0
    .param p0    # Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;
    .param p1    # Lcom/mediatek/mobilelog/debugtool/AEEControlService;

    iput-object p1, p0, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->mBoundService:Lcom/mediatek/mobilelog/debugtool/AEEControlService;

    return-object p1
.end method

.method private static currentAEEMode()Ljava/lang/String;
    .locals 10

    const/4 v1, 0x4

    const/4 v6, 0x1

    const-string v7, "persist.mtk.aee.mode"

    invoke-static {v7}, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-lt v3, v6, :cond_0

    if-gt v3, v1, :cond_0

    sget-object v7, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->sAEE_MODE_STRING:[Ljava/lang/String;

    aget-object v2, v7, v3

    :goto_0
    return-object v2

    :cond_0
    const-string v7, "ro.aee.build.info"

    invoke-static {v7}, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v7, "ro.build.type"

    invoke-static {v7}, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v7, "Mobilelog/Debugutils"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[ro.build.type]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "[ro.aee.build.info]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v7, "mtk"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    const/4 v1, 0x2

    :cond_1
    const-string v7, "eng"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    :goto_1
    sget-object v7, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->sAEE_MODE_STRING:[Ljava/lang/String;

    sub-int v8, v1, v6

    aget-object v2, v7, v8

    goto :goto_0

    :cond_2
    const/4 v6, 0x0

    goto :goto_1
.end method

.method private static getProperty(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0    # Ljava/lang/String;

    invoke-static {p0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method doBindService()V
    .locals 3

    const/4 v2, 0x1

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mediatek/mobilelog/debugtool/AEEControlService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0, v1, v2}, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    iput-boolean v2, p0, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->mIsBound:Z

    return-void
.end method

.method doUnbindService()V
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->mIsBound:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->unbindService(Landroid/content/ServiceConnection;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->mIsBound:Z

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;

    const v7, 0x7f070030

    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    const/high16 v5, 0x7f040000

    invoke-virtual {p0, v5}, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    invoke-virtual {p0, v7}, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    check-cast v5, Landroid/preference/ListPreference;

    iput-object v5, p0, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->pref_aeemode:Landroid/preference/ListPreference;

    const-string v5, "ro.aee.build.info"

    invoke-static {v5}, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v5, "mtk"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    iget-object v5, p0, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->pref_aeemode:Landroid/preference/ListPreference;

    const v6, 0x7f050002

    invoke-virtual {v5, v6}, Landroid/preference/ListPreference;->setEntries(I)V

    iget-object v5, p0, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->pref_aeemode:Landroid/preference/ListPreference;

    const v6, 0x7f050003

    invoke-virtual {v5, v6}, Landroid/preference/ListPreference;->setEntryValues(I)V

    :cond_1
    invoke-virtual {p0}, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    invoke-virtual {v5}, Landroid/preference/PreferenceScreen;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-static {}, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->currentAEEMode()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-virtual {p0, v7}, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->apply()V

    iget-object v5, p0, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->pref_aeemode:Landroid/preference/ListPreference;

    invoke-virtual {v5, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    const v6, 0x7f070039

    invoke-virtual {p0, v6}, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    check-cast v5, Landroid/preference/PreferenceScreen;

    iput-object v5, p0, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->clearDAL:Landroid/preference/PreferenceScreen;

    iget-object v5, p0, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->clearDAL:Landroid/preference/PreferenceScreen;

    invoke-virtual {v5, p0}, Landroid/preference/PreferenceScreen;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    invoke-virtual {p0}, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    const v6, 0x7f07003e

    invoke-virtual {p0, v6}, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    check-cast v5, Landroid/preference/PreferenceScreen;

    iput-object v5, p0, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->cleanData:Landroid/preference/PreferenceScreen;

    iget-object v5, p0, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->cleanData:Landroid/preference/PreferenceScreen;

    invoke-virtual {v5, p0}, Landroid/preference/PreferenceScreen;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    const-string v5, "ro.build.type"

    invoke-static {v5}, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    const-string v5, "eng"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    :cond_2
    iget-object v5, p0, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->cleanData:Landroid/preference/PreferenceScreen;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/preference/PreferenceScreen;->setEnabled(Z)V

    :cond_3
    invoke-virtual {p0}, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    const v6, 0x7f07003d

    invoke-virtual {p0, v6}, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/PreferenceCategory;

    invoke-virtual {p0}, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    invoke-virtual {p0}, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->doBindService()V

    return-void
.end method

.method protected onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    invoke-virtual {p0}, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->doUnbindService()V

    return-void
.end method

.method protected onPause()V
    .locals 1

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onPause()V

    invoke-virtual {p0}, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    return-void
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 3
    .param p1    # Landroid/preference/Preference;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->clearDAL:Landroid/preference/PreferenceScreen;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :try_start_0
    iget-object v2, p0, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->mBoundService:Lcom/mediatek/mobilelog/debugtool/AEEControlService;

    invoke-virtual {v2}, Lcom/mediatek/mobilelog/debugtool/AEEControlService;->clearDAL()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const-string v1, "Mobilelog/Debugutils"

    const-string v2, "AEE Service is not started"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->cleanData:Landroid/preference/PreferenceScreen;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :try_start_1
    iget-object v2, p0, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->mBoundService:Lcom/mediatek/mobilelog/debugtool/AEEControlService;

    invoke-virtual {v2}, Lcom/mediatek/mobilelog/debugtool/AEEControlService;->cleanData()V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v1, "Mobilelog/Debugutils"

    const-string v2, "AEE Service is not started"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    invoke-virtual {p0}, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 6
    .param p1    # Landroid/content/SharedPreferences;
    .param p2    # Ljava/lang/String;

    const v3, 0x7f070030

    invoke-virtual {p0, v3}, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x0

    invoke-interface {p1, p2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    const-string v3, "Mobilelog/Debugutils"

    const-string v4, "AEE working mode is set to NULL."

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->mBoundService:Lcom/mediatek/mobilelog/debugtool/AEEControlService;

    invoke-virtual {v3, v2}, Lcom/mediatek/mobilelog/debugtool/AEEControlService;->changeAEEMode(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    invoke-static {}, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->currentAEEMode()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3, p2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    const-string v3, "Mobilelog/Debugutils"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error occured in changing aee mode ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "-->"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v3, p0, Lcom/mediatek/mobilelog/debugtool/DebugToolboxActivity;->pref_aeemode:Landroid/preference/ListPreference;

    invoke-virtual {v3, v0}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v3, "Mobilelog/Debugutils"

    const-string v4, "AEE Service is not started"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
