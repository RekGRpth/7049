.class public Lcom/mediatek/mdlogger/MDLoggerJNI;
.super Ljava/lang/Object;
.source "MDLoggerJNI.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ModemLog_JNI"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, "ModemLog_JNI"

    const-string v1, "Load the library: mdlogger_ctrl_jni"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "mdlogger_ctrl_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static native disableMemoryDump()Z
.end method

.method public static native enableMemoryDump()Z
.end method

.method public static native getAutoStartMDLoggingMode()I
.end method

.method public static native getCurrentMDLoggingMode()I
.end method

.method public static native isMDLoggingPaused()Z
.end method

.method public static native isMemoryDumpDone()Z
.end method

.method public static native pauseMDLogging()I
.end method

.method public static native resetModem()V
.end method

.method public static native resumeMDLogging()I
.end method

.method public static native setAutoStartMDLoggingMode(I)I
.end method

.method public static native startMDLogging(I)I
.end method

.method public static native stopMDLogging()I
.end method

.method public static native stopWaitingSD()Z
.end method

.method public static native testPollingMode()Z
.end method
