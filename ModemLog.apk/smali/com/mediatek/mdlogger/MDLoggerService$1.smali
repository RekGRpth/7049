.class Lcom/mediatek/mdlogger/MDLoggerService$1;
.super Landroid/os/Handler;
.source "MDLoggerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/mdlogger/MDLoggerService;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/mdlogger/MDLoggerService;


# direct methods
.method constructor <init>(Lcom/mediatek/mdlogger/MDLoggerService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/mdlogger/MDLoggerService$1;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1    # Landroid/os/Message;

    const/4 v4, 0x1

    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x3e8

    if-ne v0, v2, :cond_1

    iget v1, p1, Landroid/os/Message;->arg1:I

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const-string v2, "ModemLog_Service"

    const-string v3, "Check SD return: SD card is full, show SD card full dialog, inform activity pause"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService$1;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z
    invoke-static {v2}, Lcom/mediatek/mdlogger/MDLoggerService;->access$000(Lcom/mediatek/mdlogger/MDLoggerService;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService$1;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # invokes: Lcom/mediatek/mdlogger/MDLoggerService;->informActivityPause()V
    invoke-static {v2}, Lcom/mediatek/mdlogger/MDLoggerService;->access$100(Lcom/mediatek/mdlogger/MDLoggerService;)V

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService$1;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # invokes: Lcom/mediatek/mdlogger/MDLoggerService;->showSDCardFull()V
    invoke-static {v2}, Lcom/mediatek/mdlogger/MDLoggerService;->access$200(Lcom/mediatek/mdlogger/MDLoggerService;)V

    goto :goto_0

    :pswitch_1
    const-string v2, "ModemLog_Service"

    const-string v3, "Check SD card return: SD card is not writable, show toast, inform activity pause"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService$1;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    const-string v3, "SD card is not writable"

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService$1;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # invokes: Lcom/mediatek/mdlogger/MDLoggerService;->informActivityPause()V
    invoke-static {v2}, Lcom/mediatek/mdlogger/MDLoggerService;->access$100(Lcom/mediatek/mdlogger/MDLoggerService;)V

    goto :goto_0

    :pswitch_2
    const-string v2, "ModemLog_Service"

    const-string v3, "Check SD card return: SD card is exist, inform activity pause"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService$1;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # invokes: Lcom/mediatek/mdlogger/MDLoggerService;->informActivityPause()V
    invoke-static {v2}, Lcom/mediatek/mdlogger/MDLoggerService;->access$100(Lcom/mediatek/mdlogger/MDLoggerService;)V

    goto :goto_0

    :cond_1
    const/16 v2, 0x3e9

    if-ne v0, v2, :cond_2

    const-string v2, "ModemLog_Service"

    const-string v3, "Show Memory dump dialog when start service"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService$1;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # setter for: Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z
    invoke-static {v2, v4}, Lcom/mediatek/mdlogger/MDLoggerService;->access$002(Lcom/mediatek/mdlogger/MDLoggerService;Z)Z

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService$1;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # invokes: Lcom/mediatek/mdlogger/MDLoggerService;->informActivityStopPolling()V
    invoke-static {v2}, Lcom/mediatek/mdlogger/MDLoggerService;->access$300(Lcom/mediatek/mdlogger/MDLoggerService;)V

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService$1;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    const-string v3, "/data/mdl/bootupLog"

    # invokes: Lcom/mediatek/mdlogger/MDLoggerService;->showMemoryDumpDone(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/mediatek/mdlogger/MDLoggerService;->access$400(Lcom/mediatek/mdlogger/MDLoggerService;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v2, "ModemLog_Service"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ModemLog Service encounter undefined message ID: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
