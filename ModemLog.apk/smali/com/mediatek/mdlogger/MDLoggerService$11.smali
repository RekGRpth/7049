.class Lcom/mediatek/mdlogger/MDLoggerService$11;
.super Ljava/lang/Object;
.source "MDLoggerService.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/mdlogger/MDLoggerService;->showMemoryDumpDone(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/mdlogger/MDLoggerService;


# direct methods
.method constructor <init>(Lcom/mediatek/mdlogger/MDLoggerService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/mdlogger/MDLoggerService$11;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const-string v0, "ModemLog_Service"

    const-string v1, "Click OK in memory dump done dialog"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/mediatek/mdlogger/MDLoggerService$11$1;

    invoke-direct {v0, p0}, Lcom/mediatek/mdlogger/MDLoggerService$11$1;-><init>(Lcom/mediatek/mdlogger/MDLoggerService$11;)V

    invoke-virtual {v0}, Lcom/mediatek/mdlogger/MDLoggerService$11$1;->start()V

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService$11;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mAassertRingtone:Landroid/media/Ringtone;
    invoke-static {v0}, Lcom/mediatek/mdlogger/MDLoggerService;->access$2800(Lcom/mediatek/mdlogger/MDLoggerService;)Landroid/media/Ringtone;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService$11;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mAassertRingtone:Landroid/media/Ringtone;
    invoke-static {v0}, Lcom/mediatek/mdlogger/MDLoggerService;->access$2800(Lcom/mediatek/mdlogger/MDLoggerService;)Landroid/media/Ringtone;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/Ringtone;->stop()V

    :cond_0
    return-void
.end method
