.class Lcom/mediatek/mdlogger/MDLoggerService$14;
.super Ljava/lang/Object;
.source "MDLoggerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/mdlogger/MDLoggerService;->runMonitorSDCardStatus()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/mdlogger/MDLoggerService;


# direct methods
.method constructor <init>(Lcom/mediatek/mdlogger/MDLoggerService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/mdlogger/MDLoggerService$14;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    const/16 v10, 0x3c

    const/4 v9, 0x3

    const/4 v8, 0x1

    const/4 v7, 0x2

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    add-int/lit8 v2, v3, 0x1

    const/16 v5, 0x3d

    if-ge v3, v5, :cond_0

    const-wide/16 v5, 0x7d0

    :try_start_0
    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    const-string v5, "ModemLog_Service"

    const-string v6, "run Monitor SDCard Status"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/mediatek/mdlogger/MDLoggerService$14;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I
    invoke-static {v5}, Lcom/mediatek/mdlogger/MDLoggerService;->access$500(Lcom/mediatek/mdlogger/MDLoggerService;)I

    move-result v5

    if-ne v5, v7, :cond_5

    iget-object v5, p0, Lcom/mediatek/mdlogger/MDLoggerService$14;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # invokes: Lcom/mediatek/mdlogger/MDLoggerService;->checkSDStatus()I
    invoke-static {v5}, Lcom/mediatek/mdlogger/MDLoggerService;->access$2600(Lcom/mediatek/mdlogger/MDLoggerService;)I

    move-result v4

    iget-object v5, p0, Lcom/mediatek/mdlogger/MDLoggerService$14;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/mediatek/mdlogger/MDLoggerService;->access$2900(Lcom/mediatek/mdlogger/MDLoggerService;)Landroid/os/Handler;

    move-result-object v5

    const/16 v6, 0x3e8

    invoke-virtual {v5, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    if-ne v8, v4, :cond_1

    iput v8, v1, Landroid/os/Message;->arg1:I

    iget-object v5, p0, Lcom/mediatek/mdlogger/MDLoggerService$14;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/mediatek/mdlogger/MDLoggerService;->access$2900(Lcom/mediatek/mdlogger/MDLoggerService;)Landroid/os/Handler;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    :goto_2
    const-string v5, "ModemLog_Service"

    const-string v6, "Exit Check sdcard thread"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    :cond_1
    if-ne v9, v4, :cond_2

    iput v9, v1, Landroid/os/Message;->arg1:I

    if-lt v2, v10, :cond_4

    iget-object v5, p0, Lcom/mediatek/mdlogger/MDLoggerService$14;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/mediatek/mdlogger/MDLoggerService;->access$2900(Lcom/mediatek/mdlogger/MDLoggerService;)Landroid/os/Handler;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_2

    :cond_2
    if-ne v7, v4, :cond_3

    iput v7, v1, Landroid/os/Message;->arg1:I

    if-lt v2, v10, :cond_4

    iget-object v5, p0, Lcom/mediatek/mdlogger/MDLoggerService$14;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/mediatek/mdlogger/MDLoggerService;->access$2900(Lcom/mediatek/mdlogger/MDLoggerService;)Landroid/os/Handler;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_2

    :cond_3
    if-eqz v4, :cond_0

    :cond_4
    move v3, v2

    goto :goto_0

    :cond_5
    move v3, v2

    goto :goto_0
.end method
