.class Lcom/mediatek/mdlogger/MDLoggerService$15;
.super Ljava/lang/Object;
.source "MDLoggerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/mdlogger/MDLoggerService;->runMonitoringLogSizeThread()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/mdlogger/MDLoggerService;


# direct methods
.method constructor <init>(Lcom/mediatek/mdlogger/MDLoggerService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/mdlogger/MDLoggerService$15;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    const/4 v5, 0x1

    const/4 v6, 0x0

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService$15;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    # setter for: Lcom/mediatek/mdlogger/MDLoggerService;->mMonitorThreadStop:Ljava/lang/Boolean;
    invoke-static {v2, v3}, Lcom/mediatek/mdlogger/MDLoggerService;->access$1402(Lcom/mediatek/mdlogger/MDLoggerService;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService$15;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    # setter for: Lcom/mediatek/mdlogger/MDLoggerService;->mMonitorThreadRunning:Ljava/lang/Boolean;
    invoke-static {v2, v3}, Lcom/mediatek/mdlogger/MDLoggerService;->access$3002(Lcom/mediatek/mdlogger/MDLoggerService;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    const-string v2, "ModemLog_Service"

    const-string v3, "Start Check log thread"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0x3e8

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService$15;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mSharedPrefLogQueryJNI:Landroid/content/SharedPreferences;
    invoke-static {v2}, Lcom/mediatek/mdlogger/MDLoggerService;->access$3100(Lcom/mediatek/mdlogger/MDLoggerService;)Landroid/content/SharedPreferences;

    move-result-object v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService$15;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    iget-object v3, p0, Lcom/mediatek/mdlogger/MDLoggerService$15;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    const-string v4, "PREF_LOG_QUERY_TABLE"

    invoke-virtual {v3, v4, v6}, Lcom/mediatek/mdlogger/MDLoggerService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    # setter for: Lcom/mediatek/mdlogger/MDLoggerService;->mSharedPrefLogQueryJNI:Landroid/content/SharedPreferences;
    invoke-static {v2, v3}, Lcom/mediatek/mdlogger/MDLoggerService;->access$3102(Lcom/mediatek/mdlogger/MDLoggerService;Landroid/content/SharedPreferences;)Landroid/content/SharedPreferences;

    :cond_0
    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService$15;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mSharedPrefLogQueryJNI:Landroid/content/SharedPreferences;
    invoke-static {v2}, Lcom/mediatek/mdlogger/MDLoggerService;->access$3100(Lcom/mediatek/mdlogger/MDLoggerService;)Landroid/content/SharedPreferences;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService$15;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mSharedPrefLogQueryJNI:Landroid/content/SharedPreferences;
    invoke-static {v2}, Lcom/mediatek/mdlogger/MDLoggerService;->access$3100(Lcom/mediatek/mdlogger/MDLoggerService;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "PREF_LOG_SIZE_CUST_INIT"

    invoke-interface {v2, v3, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService$15;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # invokes: Lcom/mediatek/mdlogger/MDLoggerService;->initLogCustmizeSize()I
    invoke-static {v2}, Lcom/mediatek/mdlogger/MDLoggerService;->access$3200(Lcom/mediatek/mdlogger/MDLoggerService;)I

    move-result v1

    :cond_1
    :goto_0
    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService$15;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    # setter for: Lcom/mediatek/mdlogger/MDLoggerService;->mMonitorThreadStop:Ljava/lang/Boolean;
    invoke-static {v2, v3}, Lcom/mediatek/mdlogger/MDLoggerService;->access$1402(Lcom/mediatek/mdlogger/MDLoggerService;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    const-string v2, "ModemLog_Service"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Full sd card mode "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService$15;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mMonitorThreadStop:Ljava/lang/Boolean;
    invoke-static {v2}, Lcom/mediatek/mdlogger/MDLoggerService;->access$1400(Lcom/mediatek/mdlogger/MDLoggerService;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService$15;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # invokes: Lcom/mediatek/mdlogger/MDLoggerService;->getSdSwapPathOfService()V
    invoke-static {v2}, Lcom/mediatek/mdlogger/MDLoggerService;->access$1800(Lcom/mediatek/mdlogger/MDLoggerService;)V

    const-string v2, "ModemLog_Service"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Check Log Limited Size:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "M. sd path:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerService$15;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mSdPathFromPropert:Ljava/lang/String;
    invoke-static {v4}, Lcom/mediatek/mdlogger/MDLoggerService;->access$1700(Lcom/mediatek/mdlogger/MDLoggerService;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mediatek/mdlogger/MDLoggerClearLog;->getInstance()Lcom/mediatek/mdlogger/MDLoggerClearLog;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/mdlogger/MDLoggerService$15;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mSdPathFromPropert:Ljava/lang/String;
    invoke-static {v3}, Lcom/mediatek/mdlogger/MDLoggerService;->access$1700(Lcom/mediatek/mdlogger/MDLoggerService;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lcom/mediatek/mdlogger/MDLoggerClearLog;->checkAndClearLog(ILjava/lang/String;)V

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->sWaitNextCheck:Ljava/lang/Object;
    invoke-static {}, Lcom/mediatek/mdlogger/MDLoggerService;->access$1600()Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    :try_start_0
    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->sWaitNextCheck:Ljava/lang/Object;
    invoke-static {}, Lcom/mediatek/mdlogger/MDLoggerService;->access$1600()Ljava/lang/Object;

    move-result-object v2

    const-wide/16 v4, 0x7530

    invoke-virtual {v2, v4, v5}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_2
    :try_start_1
    monitor-exit v3

    goto :goto_1

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    :cond_3
    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService$15;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mSharedPrefLogQueryJNI:Landroid/content/SharedPreferences;
    invoke-static {v2}, Lcom/mediatek/mdlogger/MDLoggerService;->access$3100(Lcom/mediatek/mdlogger/MDLoggerService;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "PREF_LOG_SIZE_CUST_SIZE"

    const/16 v4, 0x3e8

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    goto/16 :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    :cond_4
    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService$15;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    # setter for: Lcom/mediatek/mdlogger/MDLoggerService;->mMonitorThreadRunning:Ljava/lang/Boolean;
    invoke-static {v2, v3}, Lcom/mediatek/mdlogger/MDLoggerService;->access$3002(Lcom/mediatek/mdlogger/MDLoggerService;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    const-string v2, "ModemLog_Service"

    const-string v3, "Exit Check log thread"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
