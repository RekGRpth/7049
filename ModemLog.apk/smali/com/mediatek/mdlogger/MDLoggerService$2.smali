.class Lcom/mediatek/mdlogger/MDLoggerService$2;
.super Landroid/content/BroadcastReceiver;
.source "MDLoggerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/mdlogger/MDLoggerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/mdlogger/MDLoggerService;


# direct methods
.method constructor <init>(Lcom/mediatek/mdlogger/MDLoggerService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/mdlogger/MDLoggerService$2;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v6, 0x2

    const/4 v5, 0x1

    const-string v2, "ModemLog_Service"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Receive the mdlogger broadcast: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v2, "com.mediatek.mdlogger.LOGFILE_NOTEXIST"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "ModemLog_Service"

    const-string v3, "Stop logging: log file doesn\'t exist!"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService$2;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z
    invoke-static {v2}, Lcom/mediatek/mdlogger/MDLoggerService;->access$000(Lcom/mediatek/mdlogger/MDLoggerService;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService$2;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I
    invoke-static {v2}, Lcom/mediatek/mdlogger/MDLoggerService;->access$500(Lcom/mediatek/mdlogger/MDLoggerService;)I

    move-result v2

    if-eq v2, v6, :cond_2

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService$2;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # setter for: Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I
    invoke-static {v2, v6}, Lcom/mediatek/mdlogger/MDLoggerService;->access$502(Lcom/mediatek/mdlogger/MDLoggerService;I)I

    :cond_2
    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService$2;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z
    invoke-static {v2}, Lcom/mediatek/mdlogger/MDLoggerService;->access$000(Lcom/mediatek/mdlogger/MDLoggerService;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService$2;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    const-string v3, "Log file is lost"

    invoke-static {v2, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    :cond_3
    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService$2;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # invokes: Lcom/mediatek/mdlogger/MDLoggerService;->informActivityPause()V
    invoke-static {v2}, Lcom/mediatek/mdlogger/MDLoggerService;->access$100(Lcom/mediatek/mdlogger/MDLoggerService;)V

    goto :goto_0

    :cond_4
    const-string v2, "com.mediatek.mdlogger.MEMORYDUMP_START"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "ModemLog_Service"

    const-string v3, "Start to memory dump"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService$2;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    const/4 v3, 0x3

    # setter for: Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I
    invoke-static {v2, v3}, Lcom/mediatek/mdlogger/MDLoggerService;->access$502(Lcom/mediatek/mdlogger/MDLoggerService;I)I

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService$2;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # invokes: Lcom/mediatek/mdlogger/MDLoggerService;->setPrefereceLogQuryForJNI()V
    invoke-static {v2}, Lcom/mediatek/mdlogger/MDLoggerService;->access$600(Lcom/mediatek/mdlogger/MDLoggerService;)V

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService$2;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # invokes: Lcom/mediatek/mdlogger/MDLoggerService;->informActivityStartPolling()V
    invoke-static {v2}, Lcom/mediatek/mdlogger/MDLoggerService;->access$700(Lcom/mediatek/mdlogger/MDLoggerService;)V

    goto :goto_0

    :cond_5
    const-string v2, "com.mediatek.mdlogger.MEMORYDUMP_DONE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v2, "ModemLog_Service"

    const-string v3, "Finish to memory dump"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "LogPath"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService$2;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # setter for: Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z
    invoke-static {v2, v5}, Lcom/mediatek/mdlogger/MDLoggerService;->access$002(Lcom/mediatek/mdlogger/MDLoggerService;Z)Z

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService$2;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # invokes: Lcom/mediatek/mdlogger/MDLoggerService;->informActivityStopPolling()V
    invoke-static {v2}, Lcom/mediatek/mdlogger/MDLoggerService;->access$300(Lcom/mediatek/mdlogger/MDLoggerService;)V

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService$2;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # invokes: Lcom/mediatek/mdlogger/MDLoggerService;->showMemoryDumpDone(Ljava/lang/String;)V
    invoke-static {v2, v1}, Lcom/mediatek/mdlogger/MDLoggerService;->access$400(Lcom/mediatek/mdlogger/MDLoggerService;Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    const-string v2, "com.mediatek.mdlogger.FAIL_SENDFILTER"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    const-string v2, "ModemLog_Service"

    const-string v3, "Stop logging: fail to send catcher_filter.bin!"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService$2;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # invokes: Lcom/mediatek/mdlogger/MDLoggerService;->informActivityPause()V
    invoke-static {v2}, Lcom/mediatek/mdlogger/MDLoggerService;->access$100(Lcom/mediatek/mdlogger/MDLoggerService;)V

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService$2;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    iget-object v3, p0, Lcom/mediatek/mdlogger/MDLoggerService$2;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    const v4, 0x7f06001d

    invoke-virtual {v3, v4}, Lcom/mediatek/mdlogger/MDLoggerService;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerService$2;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    const v5, 0x7f060026

    invoke-virtual {v4, v5}, Lcom/mediatek/mdlogger/MDLoggerService;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    # invokes: Lcom/mediatek/mdlogger/MDLoggerService;->showAlertDialog(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v2, v3, v4}, Lcom/mediatek/mdlogger/MDLoggerService;->access$800(Lcom/mediatek/mdlogger/MDLoggerService;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    const-string v2, "com.mediatek.mdlogger.FAIL_WRITEFILE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    const-string v2, "ModemLog_Service"

    const-string v3, "Stop logging: failed to write file!"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService$2;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    const-string v3, "Failed to write log file"

    invoke-static {v2, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService$2;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # invokes: Lcom/mediatek/mdlogger/MDLoggerService;->informActivityPause()V
    invoke-static {v2}, Lcom/mediatek/mdlogger/MDLoggerService;->access$100(Lcom/mediatek/mdlogger/MDLoggerService;)V

    goto/16 :goto_0

    :cond_8
    const-string v2, "com.mediatek.mdlogger.SDCARD_NOTEXIST"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    const-string v2, "ModemLog_Service"

    const-string v3, "Stop logging: No SD card!"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService$2;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # invokes: Lcom/mediatek/mdlogger/MDLoggerService;->informActivityPauseUI()V
    invoke-static {v2}, Lcom/mediatek/mdlogger/MDLoggerService;->access$900(Lcom/mediatek/mdlogger/MDLoggerService;)V

    goto/16 :goto_0

    :cond_9
    const-string v2, "com.mediatek.mdlogger.SDCARD_FULL"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "ModemLog_Service"

    const-string v3, "Stop logging: SD card is full!"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService$2;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # invokes: Lcom/mediatek/mdlogger/MDLoggerService;->showSDCardFull()V
    invoke-static {v2}, Lcom/mediatek/mdlogger/MDLoggerService;->access$200(Lcom/mediatek/mdlogger/MDLoggerService;)V

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService$2;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # invokes: Lcom/mediatek/mdlogger/MDLoggerService;->informActivityPause()V
    invoke-static {v2}, Lcom/mediatek/mdlogger/MDLoggerService;->access$100(Lcom/mediatek/mdlogger/MDLoggerService;)V

    goto/16 :goto_0
.end method
