.class Lcom/mediatek/mdlogger/MDLoggerService$5;
.super Landroid/content/BroadcastReceiver;
.source "MDLoggerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/mdlogger/MDLoggerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/mdlogger/MDLoggerService;


# direct methods
.method constructor <init>(Lcom/mediatek/mdlogger/MDLoggerService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/mdlogger/MDLoggerService$5;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v4, 0x0

    const/4 v3, 0x1

    const-string v0, "ModemLog_Service"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Receive ModemLog activity broadcast: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.mediatek.mdlogger.MDLoggerService"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService$5;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    const-string v1, "RunStatus"

    invoke-virtual {p2, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    # setter for: Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z
    invoke-static {v0, v1}, Lcom/mediatek/mdlogger/MDLoggerService;->access$002(Lcom/mediatek/mdlogger/MDLoggerService;Z)Z

    const-string v0, "ModemLog_Service"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Receive notify from activity, current mode is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService$5;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I
    invoke-static {v2}, Lcom/mediatek/mdlogger/MDLoggerService;->access$500(Lcom/mediatek/mdlogger/MDLoggerService;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", and pause is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService$5;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z
    invoke-static {v2}, Lcom/mediatek/mdlogger/MDLoggerService;->access$000(Lcom/mediatek/mdlogger/MDLoggerService;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService$5;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z
    invoke-static {v0}, Lcom/mediatek/mdlogger/MDLoggerService;->access$000(Lcom/mediatek/mdlogger/MDLoggerService;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {v4}, Lcom/mediatek/mdlogger/MDLoggerJNI;->setAutoStartMDLoggingMode(I)I

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService$5;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    const/4 v1, 0x2

    # invokes: Lcom/mediatek/mdlogger/MDLoggerService;->updateNotificationBar(I)V
    invoke-static {v0, v1}, Lcom/mediatek/mdlogger/MDLoggerService;->access$1300(Lcom/mediatek/mdlogger/MDLoggerService;I)V

    :goto_0
    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService$5;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z
    invoke-static {v0}, Lcom/mediatek/mdlogger/MDLoggerService;->access$000(Lcom/mediatek/mdlogger/MDLoggerService;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService$5;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I
    invoke-static {v0}, Lcom/mediatek/mdlogger/MDLoggerService;->access$500(Lcom/mediatek/mdlogger/MDLoggerService;)I

    move-result v0

    if-eq v0, v3, :cond_2

    const-string v0, "ModemLog_Service"

    const-string v1, "Thread notify mMonitorThreadStop = false"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService$5;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    # setter for: Lcom/mediatek/mdlogger/MDLoggerService;->mMonitorThreadStop:Ljava/lang/Boolean;
    invoke-static {v0, v1}, Lcom/mediatek/mdlogger/MDLoggerService;->access$1402(Lcom/mediatek/mdlogger/MDLoggerService;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService$5;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # invokes: Lcom/mediatek/mdlogger/MDLoggerService;->runMonitoringLogSizeThread()V
    invoke-static {v0}, Lcom/mediatek/mdlogger/MDLoggerService;->access$1500(Lcom/mediatek/mdlogger/MDLoggerService;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService$5;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I
    invoke-static {v0}, Lcom/mediatek/mdlogger/MDLoggerService;->access$500(Lcom/mediatek/mdlogger/MDLoggerService;)I

    move-result v0

    invoke-static {v0}, Lcom/mediatek/mdlogger/MDLoggerJNI;->setAutoStartMDLoggingMode(I)I

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService$5;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # invokes: Lcom/mediatek/mdlogger/MDLoggerService;->updateNotificationBar(I)V
    invoke-static {v0, v3}, Lcom/mediatek/mdlogger/MDLoggerService;->access$1300(Lcom/mediatek/mdlogger/MDLoggerService;I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService$5;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z
    invoke-static {v0}, Lcom/mediatek/mdlogger/MDLoggerService;->access$000(Lcom/mediatek/mdlogger/MDLoggerService;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService$5;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I
    invoke-static {v0}, Lcom/mediatek/mdlogger/MDLoggerService;->access$500(Lcom/mediatek/mdlogger/MDLoggerService;)I

    move-result v0

    if-eq v0, v3, :cond_0

    const-string v0, "ModemLog_Service"

    const-string v1, "Thread notify mMonitorThreadStop = true"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService$5;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    # setter for: Lcom/mediatek/mdlogger/MDLoggerService;->mMonitorThreadStop:Ljava/lang/Boolean;
    invoke-static {v0, v1}, Lcom/mediatek/mdlogger/MDLoggerService;->access$1402(Lcom/mediatek/mdlogger/MDLoggerService;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->sWaitNextCheck:Ljava/lang/Object;
    invoke-static {}, Lcom/mediatek/mdlogger/MDLoggerService;->access$1600()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->sWaitNextCheck:Ljava/lang/Object;
    invoke-static {}, Lcom/mediatek/mdlogger/MDLoggerService;->access$1600()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    const-string v0, "ModemLog_Service"

    const-string v2, "notify stop check log size"

    invoke-static {v0, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
