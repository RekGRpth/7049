.class Lcom/mediatek/mdlogger/MDLoggerService$6;
.super Landroid/content/BroadcastReceiver;
.source "MDLoggerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/mdlogger/MDLoggerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/mdlogger/MDLoggerService;


# direct methods
.method constructor <init>(Lcom/mediatek/mdlogger/MDLoggerService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/mdlogger/MDLoggerService$6;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v9, 0x1

    const/4 v8, 0x2

    const-string v4, "ModemLog_Service"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Receive SD card status change broadcast: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "storage_volume"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getExtra(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/storage/StorageVolume;

    if-nez v3, :cond_1

    const-string v4, "ModemLog_Service"

    const-string v5, "StorageVolume is null!"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v3}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerService$6;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mReceivedIPOShutdown:Z
    invoke-static {v4}, Lcom/mediatek/mdlogger/MDLoggerService;->access$1200(Lcom/mediatek/mdlogger/MDLoggerService;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerService$6;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # invokes: Lcom/mediatek/mdlogger/MDLoggerService;->informActivityPause()V
    invoke-static {v4}, Lcom/mediatek/mdlogger/MDLoggerService;->access$100(Lcom/mediatek/mdlogger/MDLoggerService;)V

    const-string v4, "ModemLog_Service"

    const-string v5, "IPO shut off"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerService$6;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    invoke-virtual {v4}, Lcom/mediatek/mdlogger/MDLoggerService;->stopSelf()V

    :cond_2
    const-string v4, "ModemLog_Service"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "The SD card path from StorageVolume: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v1, :cond_3

    const-string v4, "ModemLog_Service"

    const-string v5, "Can not get sdcard path from StorageVolume!"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v4, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, "android.intent.action.MEDIA_SHARED"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    :cond_4
    const-string v4, "ModemLog_Service"

    const-string v5, "SD card change to be unavailable"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "ModemLog_Service"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "The sdPathFromPropert path is: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/mdlogger/MDLoggerService$6;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mSdPathFromPropert:Ljava/lang/String;
    invoke-static {v6}, Lcom/mediatek/mdlogger/MDLoggerService;->access$1700(Lcom/mediatek/mdlogger/MDLoggerService;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerService$6;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # invokes: Lcom/mediatek/mdlogger/MDLoggerService;->getSdSwapPathOfService()V
    invoke-static {v4}, Lcom/mediatek/mdlogger/MDLoggerService;->access$1800(Lcom/mediatek/mdlogger/MDLoggerService;)V

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerService$6;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mSdPathFromPropert:Ljava/lang/String;
    invoke-static {v4}, Lcom/mediatek/mdlogger/MDLoggerService;->access$1700(Lcom/mediatek/mdlogger/MDLoggerService;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerService$6;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # invokes: Lcom/mediatek/mdlogger/MDLoggerService;->initLogSavePathForSdSwap()V
    invoke-static {v4}, Lcom/mediatek/mdlogger/MDLoggerService;->access$1900(Lcom/mediatek/mdlogger/MDLoggerService;)V

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerService$6;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mSdPathFromPropert:Ljava/lang/String;
    invoke-static {v4}, Lcom/mediatek/mdlogger/MDLoggerService;->access$1700(Lcom/mediatek/mdlogger/MDLoggerService;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    const-string v4, "ModemLog_Service"

    const-string v5, "mSdPathFromPropert isn\'t same as StorageVolume!"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_5
    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerService$6;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I
    invoke-static {v4}, Lcom/mediatek/mdlogger/MDLoggerService;->access$500(Lcom/mediatek/mdlogger/MDLoggerService;)I

    move-result v4

    if-ne v4, v8, :cond_8

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerService$6;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z
    invoke-static {v4}, Lcom/mediatek/mdlogger/MDLoggerService;->access$000(Lcom/mediatek/mdlogger/MDLoggerService;)Z

    move-result v4

    if-nez v4, :cond_8

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerService$6;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    const-string v5, "KEY_LOG_STATE"

    const-string v6, "MODE_SD"

    # invokes: Lcom/mediatek/mdlogger/MDLoggerService;->setPreferenceLogMode(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v4, v5, v6}, Lcom/mediatek/mdlogger/MDLoggerService;->access$2000(Lcom/mediatek/mdlogger/MDLoggerService;Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    :goto_1
    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerService$6;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mSdcardErased:Z
    invoke-static {v4}, Lcom/mediatek/mdlogger/MDLoggerService;->access$2100(Lcom/mediatek/mdlogger/MDLoggerService;)Z

    move-result v4

    if-nez v4, :cond_7

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerService$6;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    const/4 v5, 0x3

    # invokes: Lcom/mediatek/mdlogger/MDLoggerService;->updateNotificationBar(I)V
    invoke-static {v4, v5}, Lcom/mediatek/mdlogger/MDLoggerService;->access$1300(Lcom/mediatek/mdlogger/MDLoggerService;I)V

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerService$6;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # invokes: Lcom/mediatek/mdlogger/MDLoggerService;->informActivityPause()V
    invoke-static {v4}, Lcom/mediatek/mdlogger/MDLoggerService;->access$100(Lcom/mediatek/mdlogger/MDLoggerService;)V

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerService$6;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    iget-object v5, p0, Lcom/mediatek/mdlogger/MDLoggerService$6;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mSdPathFromPropert:Ljava/lang/String;
    invoke-static {v5}, Lcom/mediatek/mdlogger/MDLoggerService;->access$1700(Lcom/mediatek/mdlogger/MDLoggerService;)Ljava/lang/String;

    move-result-object v5

    # setter for: Lcom/mediatek/mdlogger/MDLoggerService;->mSdOlderPath:Ljava/lang/String;
    invoke-static {v4, v5}, Lcom/mediatek/mdlogger/MDLoggerService;->access$2202(Lcom/mediatek/mdlogger/MDLoggerService;Ljava/lang/String;)Ljava/lang/String;

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerService$6;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mReceivedIPOShutdown:Z
    invoke-static {v4}, Lcom/mediatek/mdlogger/MDLoggerService;->access$1200(Lcom/mediatek/mdlogger/MDLoggerService;)Z

    move-result v4

    if-eqz v4, :cond_7

    const-string v4, "ModemLog_Service"

    const-string v5, "IPO shut off"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerService$6;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    invoke-virtual {v4}, Lcom/mediatek/mdlogger/MDLoggerService;->stopSelf()V

    :cond_7
    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerService$6;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # setter for: Lcom/mediatek/mdlogger/MDLoggerService;->mSdcardErased:Z
    invoke-static {v4, v9}, Lcom/mediatek/mdlogger/MDLoggerService;->access$2102(Lcom/mediatek/mdlogger/MDLoggerService;Z)Z

    goto/16 :goto_0

    :cond_8
    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerService$6;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mSdcardErased:Z
    invoke-static {v4}, Lcom/mediatek/mdlogger/MDLoggerService;->access$2100(Lcom/mediatek/mdlogger/MDLoggerService;)Z

    move-result v4

    if-nez v4, :cond_6

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerService$6;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    const-string v5, "KEY_LOG_STATE"

    const-string v6, ""

    # invokes: Lcom/mediatek/mdlogger/MDLoggerService;->setPreferenceLogMode(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v4, v5, v6}, Lcom/mediatek/mdlogger/MDLoggerService;->access$2000(Lcom/mediatek/mdlogger/MDLoggerService;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_9
    const-string v4, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "ModemLog_Service"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SD card change to be remounted bPaused = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/mdlogger/MDLoggerService$6;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z
    invoke-static {v6}, Lcom/mediatek/mdlogger/MDLoggerService;->access$000(Lcom/mediatek/mdlogger/MDLoggerService;)Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerService$6;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z
    invoke-static {v4}, Lcom/mediatek/mdlogger/MDLoggerService;->access$000(Lcom/mediatek/mdlogger/MDLoggerService;)Z

    move-result v4

    if-eqz v4, :cond_e

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerService$6;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mServiceOn:Z
    invoke-static {v4}, Lcom/mediatek/mdlogger/MDLoggerService;->access$2300(Lcom/mediatek/mdlogger/MDLoggerService;)Z

    move-result v4

    if-eqz v4, :cond_e

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerService$6;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mSdcardErased:Z
    invoke-static {v4}, Lcom/mediatek/mdlogger/MDLoggerService;->access$2100(Lcom/mediatek/mdlogger/MDLoggerService;)Z

    move-result v4

    if-eqz v4, :cond_e

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerService$6;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    iget-object v5, p0, Lcom/mediatek/mdlogger/MDLoggerService$6;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mSharedPrefLogState:Landroid/content/SharedPreferences;
    invoke-static {v5}, Lcom/mediatek/mdlogger/MDLoggerService;->access$2500(Lcom/mediatek/mdlogger/MDLoggerService;)Landroid/content/SharedPreferences;

    move-result-object v5

    const-string v6, "KEY_LOG_STATE"

    const/4 v7, 0x0

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    # setter for: Lcom/mediatek/mdlogger/MDLoggerService;->mResumeLogState:Ljava/lang/String;
    invoke-static {v4, v5}, Lcom/mediatek/mdlogger/MDLoggerService;->access$2402(Lcom/mediatek/mdlogger/MDLoggerService;Ljava/lang/String;)Ljava/lang/String;

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerService$6;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mResumeLogState:Ljava/lang/String;
    invoke-static {v4}, Lcom/mediatek/mdlogger/MDLoggerService;->access$2400(Lcom/mediatek/mdlogger/MDLoggerService;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    const-string v4, "ModemLog_Service"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "The mSdOlderPath path is: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/mdlogger/MDLoggerService$6;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mSdOlderPath:Ljava/lang/String;
    invoke-static {v6}, Lcom/mediatek/mdlogger/MDLoggerService;->access$2200(Lcom/mediatek/mdlogger/MDLoggerService;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerService$6;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mSdOlderPath:Ljava/lang/String;
    invoke-static {v4}, Lcom/mediatek/mdlogger/MDLoggerService;->access$2200(Lcom/mediatek/mdlogger/MDLoggerService;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_a

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerService$6;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # invokes: Lcom/mediatek/mdlogger/MDLoggerService;->initLogSavePathForSdSwap()V
    invoke-static {v4}, Lcom/mediatek/mdlogger/MDLoggerService;->access$1900(Lcom/mediatek/mdlogger/MDLoggerService;)V

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerService$6;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mSdPathFromPropert:Ljava/lang/String;
    invoke-static {v4}, Lcom/mediatek/mdlogger/MDLoggerService;->access$1700(Lcom/mediatek/mdlogger/MDLoggerService;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_a

    const-string v4, "ModemLog_Service"

    const-string v5, "mSdPathFromPropert isn\'t same as StorageVolume!"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_a
    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerService$6;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mResumeLogState:Ljava/lang/String;
    invoke-static {v4}, Lcom/mediatek/mdlogger/MDLoggerService;->access$2400(Lcom/mediatek/mdlogger/MDLoggerService;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "MODE_SD"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_b

    const-string v4, "ModemLog_Service"

    const-string v5, "Preference is not SD card mode. Don\'t need to resume Modomlog."

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_b
    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerService$6;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # invokes: Lcom/mediatek/mdlogger/MDLoggerService;->checkSDStatus()I
    invoke-static {v4}, Lcom/mediatek/mdlogger/MDLoggerService;->access$2600(Lcom/mediatek/mdlogger/MDLoggerService;)I

    move-result v2

    if-ne v2, v9, :cond_c

    const-string v4, "ModemLog_Service"

    const-string v5, "Check SD card: full"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_c
    if-ne v2, v8, :cond_d

    const-string v4, "ModemLog_Service"

    const-string v5, "Check SD card: not exist"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_d
    if-nez v2, :cond_e

    const-string v4, "ModemLog_Service"

    const-string v5, "Check SD card: normal"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerService$6;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I
    invoke-static {v4}, Lcom/mediatek/mdlogger/MDLoggerService;->access$500(Lcom/mediatek/mdlogger/MDLoggerService;)I

    move-result v4

    if-ne v4, v8, :cond_f

    const-string v4, "ModemLog_Service"

    const-string v5, "SD card remount and Resume MDLogger"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerService$6;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # invokes: Lcom/mediatek/mdlogger/MDLoggerService;->informActivityResume()V
    invoke-static {v4}, Lcom/mediatek/mdlogger/MDLoggerService;->access$1100(Lcom/mediatek/mdlogger/MDLoggerService;)V

    :cond_e
    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerService$6;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    const/4 v5, 0x0

    # setter for: Lcom/mediatek/mdlogger/MDLoggerService;->mSdcardErased:Z
    invoke-static {v4, v5}, Lcom/mediatek/mdlogger/MDLoggerService;->access$2102(Lcom/mediatek/mdlogger/MDLoggerService;Z)Z

    goto/16 :goto_0

    :cond_f
    const-string v4, "ModemLog_Service"

    const-string v5, "SD card remounted but currunt is not SD card mode and don\'t resume MDLogger"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
