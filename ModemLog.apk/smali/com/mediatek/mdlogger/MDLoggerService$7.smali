.class Lcom/mediatek/mdlogger/MDLoggerService$7;
.super Landroid/content/BroadcastReceiver;
.source "MDLoggerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/mdlogger/MDLoggerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/mdlogger/MDLoggerService;


# direct methods
.method constructor <init>(Lcom/mediatek/mdlogger/MDLoggerService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/mdlogger/MDLoggerService$7;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v9, 0x2

    const/4 v8, 0x0

    const-string v5, "ModemLog_Service"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Receive Common UI broadcast: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/mediatek/mdlogger/MDLoggerService$7;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I
    invoke-static {v5}, Lcom/mediatek/mdlogger/MDLoggerService;->access$500(Lcom/mediatek/mdlogger/MDLoggerService;)I

    move-result v5

    if-ne v5, v9, :cond_3

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v5, "com.mediatek.syslogger.action"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v5, "From"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v5, "To"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "Command"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v2, :cond_0

    if-eqz v4, :cond_0

    if-nez v1, :cond_2

    :cond_0
    const-string v5, "ModemLog_Service"

    const-string v6, "Null string from Common UI!"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v5, "CommonUI"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v5, "ModemLog"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v5, "Stop"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v5, "ModemLog_Service"

    const-string v6, "Stop logging from Common UI"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/mediatek/mdlogger/MDLoggerService$7;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    const/4 v6, 0x1

    # setter for: Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z
    invoke-static {v5, v6}, Lcom/mediatek/mdlogger/MDLoggerService;->access$002(Lcom/mediatek/mdlogger/MDLoggerService;Z)Z

    invoke-static {}, Lcom/mediatek/mdlogger/MDLoggerJNI;->pauseMDLogging()I

    invoke-static {v8}, Lcom/mediatek/mdlogger/MDLoggerJNI;->setAutoStartMDLoggingMode(I)I

    iget-object v5, p0, Lcom/mediatek/mdlogger/MDLoggerService$7;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # setter for: Lcom/mediatek/mdlogger/MDLoggerService;->mAutoStartMode:I
    invoke-static {v5, v8}, Lcom/mediatek/mdlogger/MDLoggerService;->access$2702(Lcom/mediatek/mdlogger/MDLoggerService;I)I

    iget-object v5, p0, Lcom/mediatek/mdlogger/MDLoggerService$7;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # invokes: Lcom/mediatek/mdlogger/MDLoggerService;->updateNotificationBar(I)V
    invoke-static {v5, v9}, Lcom/mediatek/mdlogger/MDLoggerService;->access$1300(Lcom/mediatek/mdlogger/MDLoggerService;I)V

    new-instance v3, Landroid/content/Intent;

    const-string v5, "com.mediatek.syslogger.action"

    invoke-direct {v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v5, "From"

    const-string v6, "ModemLog"

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v5, "To"

    const-string v6, "CommonUI"

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v5, "Command"

    const-string v6, "Stop"

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v5, "Return"

    const-string v6, "Normal"

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v5, p0, Lcom/mediatek/mdlogger/MDLoggerService$7;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    # invokes: Lcom/mediatek/mdlogger/MDLoggerService;->setPrefereceLogQuryForJNI()V
    invoke-static {v5}, Lcom/mediatek/mdlogger/MDLoggerService;->access$600(Lcom/mediatek/mdlogger/MDLoggerService;)V

    iget-object v5, p0, Lcom/mediatek/mdlogger/MDLoggerService$7;->this$0:Lcom/mediatek/mdlogger/MDLoggerService;

    invoke-virtual {v5, v3}, Lcom/mediatek/mdlogger/MDLoggerService;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    :cond_3
    const-string v5, "ModemLog_Service"

    const-string v6, "Common UI: It is not SD mode!"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
