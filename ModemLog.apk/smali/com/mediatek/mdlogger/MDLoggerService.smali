.class public Lcom/mediatek/mdlogger/MDLoggerService;
.super Landroid/app/Service;
.source "MDLoggerService.java"


# static fields
.field private static final DELAYED_TIME:I = 0x3e8

.field private static final MSGID_SHOW_ALERT_MEMORY_DUMP:I = 0x3e9

.field private static final MSGID_TICK_EVENT:I = 0x3e8

.field private static final TAG:Ljava/lang/String; = "ModemLog_Service"

.field private static sWaitNextCheck:Ljava/lang/Object;


# instance fields
.field private mADBShellFilter:Landroid/content/IntentFilter;

.field private mADBShellReceiver:Landroid/content/BroadcastReceiver;

.field private mAassertRingtone:Landroid/media/Ringtone;

.field private mActivityFilter:Landroid/content/IntentFilter;

.field private mActivityReceiver:Landroid/content/BroadcastReceiver;

.field private mAlertRingUri:Landroid/net/Uri;

.field private mAutoStartMode:I

.field private mCommonUIFilter:Landroid/content/IntentFilter;

.field private mCommonUIReceiver:Landroid/content/BroadcastReceiver;

.field private mCurrentMode:I

.field private mHandler:Landroid/os/Handler;

.field private mMDLoggerFilter:Landroid/content/IntentFilter;

.field private mMDLoggerReceiver:Landroid/content/BroadcastReceiver;

.field private volatile mMonitorThreadRunning:Ljava/lang/Boolean;

.field private volatile mMonitorThreadStop:Ljava/lang/Boolean;

.field private mNotificationBar:Landroid/app/NotificationManager;

.field private mPaused:Z

.field private mReceivedIPOShutdown:Z

.field private mRegisterBroadcast:Z

.field private mRegisterOtherBroadcast:Z

.field private mResumeLogState:Ljava/lang/String;

.field private mSDStatusFilter:Landroid/content/IntentFilter;

.field private mSDStatusReceiver:Landroid/content/BroadcastReceiver;

.field private mSdOlderPath:Ljava/lang/String;

.field private mSdPathFromPropert:Ljava/lang/String;

.field private mSdcardErased:Z

.field private mServiceOn:Z

.field private mSharedPrefLogQueryJNI:Landroid/content/SharedPreferences;

.field private mSharedPrefLogState:Landroid/content/SharedPreferences;

.field private mShutdownActionFilter:Landroid/content/IntentFilter;

.field private mShutdownReceiver:Landroid/content/BroadcastReceiver;

.field private mStorageManagerHandle:Landroid/os/storage/StorageManager;

.field private mThread:Ljava/lang/Thread;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/mediatek/mdlogger/MDLoggerService;->sWaitNextCheck:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    iput-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSDStatusFilter:Landroid/content/IntentFilter;

    iput-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mActivityFilter:Landroid/content/IntentFilter;

    iput-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mShutdownActionFilter:Landroid/content/IntentFilter;

    iput-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mMDLoggerFilter:Landroid/content/IntentFilter;

    iput-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mADBShellFilter:Landroid/content/IntentFilter;

    iput-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCommonUIFilter:Landroid/content/IntentFilter;

    iput-boolean v2, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mReceivedIPOShutdown:Z

    iput-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mHandler:Landroid/os/Handler;

    iput v2, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mAutoStartMode:I

    iput-boolean v2, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mServiceOn:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z

    iput-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSharedPrefLogState:Landroid/content/SharedPreferences;

    iput-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSharedPrefLogQueryJNI:Landroid/content/SharedPreferences;

    iput-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mResumeLogState:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSdcardErased:Z

    iput-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mAassertRingtone:Landroid/media/Ringtone;

    iput-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mAlertRingUri:Landroid/net/Uri;

    iput-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mStorageManagerHandle:Landroid/os/storage/StorageManager;

    iput-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSdPathFromPropert:Ljava/lang/String;

    iput-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSdOlderPath:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mRegisterBroadcast:Z

    iput-boolean v2, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mRegisterOtherBroadcast:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mMonitorThreadRunning:Ljava/lang/Boolean;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mMonitorThreadStop:Ljava/lang/Boolean;

    iput-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mThread:Ljava/lang/Thread;

    new-instance v0, Lcom/mediatek/mdlogger/MDLoggerService$2;

    invoke-direct {v0, p0}, Lcom/mediatek/mdlogger/MDLoggerService$2;-><init>(Lcom/mediatek/mdlogger/MDLoggerService;)V

    iput-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mMDLoggerReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/mediatek/mdlogger/MDLoggerService$3;

    invoke-direct {v0, p0}, Lcom/mediatek/mdlogger/MDLoggerService$3;-><init>(Lcom/mediatek/mdlogger/MDLoggerService;)V

    iput-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mADBShellReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/mediatek/mdlogger/MDLoggerService$4;

    invoke-direct {v0, p0}, Lcom/mediatek/mdlogger/MDLoggerService$4;-><init>(Lcom/mediatek/mdlogger/MDLoggerService;)V

    iput-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mShutdownReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/mediatek/mdlogger/MDLoggerService$5;

    invoke-direct {v0, p0}, Lcom/mediatek/mdlogger/MDLoggerService$5;-><init>(Lcom/mediatek/mdlogger/MDLoggerService;)V

    iput-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mActivityReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/mediatek/mdlogger/MDLoggerService$6;

    invoke-direct {v0, p0}, Lcom/mediatek/mdlogger/MDLoggerService$6;-><init>(Lcom/mediatek/mdlogger/MDLoggerService;)V

    iput-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSDStatusReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/mediatek/mdlogger/MDLoggerService$7;

    invoke-direct {v0, p0}, Lcom/mediatek/mdlogger/MDLoggerService$7;-><init>(Lcom/mediatek/mdlogger/MDLoggerService;)V

    iput-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCommonUIReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/mdlogger/MDLoggerService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerService;

    iget-boolean v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z

    return v0
.end method

.method static synthetic access$002(Lcom/mediatek/mdlogger/MDLoggerService;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z

    return p1
.end method

.method static synthetic access$100(Lcom/mediatek/mdlogger/MDLoggerService;)V
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerService;

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->informActivityPause()V

    return-void
.end method

.method static synthetic access$1000(Lcom/mediatek/mdlogger/MDLoggerService;)V
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerService;

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->informActivityStop()V

    return-void
.end method

.method static synthetic access$1100(Lcom/mediatek/mdlogger/MDLoggerService;)V
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerService;

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->informActivityResume()V

    return-void
.end method

.method static synthetic access$1200(Lcom/mediatek/mdlogger/MDLoggerService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerService;

    iget-boolean v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mReceivedIPOShutdown:Z

    return v0
.end method

.method static synthetic access$1202(Lcom/mediatek/mdlogger/MDLoggerService;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mReceivedIPOShutdown:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/mediatek/mdlogger/MDLoggerService;I)V
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerService;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/mdlogger/MDLoggerService;->updateNotificationBar(I)V

    return-void
.end method

.method static synthetic access$1400(Lcom/mediatek/mdlogger/MDLoggerService;)Ljava/lang/Boolean;
    .locals 1
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerService;

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mMonitorThreadStop:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/mediatek/mdlogger/MDLoggerService;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerService;
    .param p1    # Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mMonitorThreadStop:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/mediatek/mdlogger/MDLoggerService;)V
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerService;

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->runMonitoringLogSizeThread()V

    return-void
.end method

.method static synthetic access$1600()Ljava/lang/Object;
    .locals 1

    sget-object v0, Lcom/mediatek/mdlogger/MDLoggerService;->sWaitNextCheck:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/mediatek/mdlogger/MDLoggerService;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerService;

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSdPathFromPropert:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/mediatek/mdlogger/MDLoggerService;)V
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerService;

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->getSdSwapPathOfService()V

    return-void
.end method

.method static synthetic access$1900(Lcom/mediatek/mdlogger/MDLoggerService;)V
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerService;

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->initLogSavePathForSdSwap()V

    return-void
.end method

.method static synthetic access$200(Lcom/mediatek/mdlogger/MDLoggerService;)V
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerService;

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->showSDCardFull()V

    return-void
.end method

.method static synthetic access$2000(Lcom/mediatek/mdlogger/MDLoggerService;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerService;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/mediatek/mdlogger/MDLoggerService;->setPreferenceLogMode(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2100(Lcom/mediatek/mdlogger/MDLoggerService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerService;

    iget-boolean v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSdcardErased:Z

    return v0
.end method

.method static synthetic access$2102(Lcom/mediatek/mdlogger/MDLoggerService;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSdcardErased:Z

    return p1
.end method

.method static synthetic access$2200(Lcom/mediatek/mdlogger/MDLoggerService;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerService;

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSdOlderPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2202(Lcom/mediatek/mdlogger/MDLoggerService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerService;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSdOlderPath:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2300(Lcom/mediatek/mdlogger/MDLoggerService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerService;

    iget-boolean v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mServiceOn:Z

    return v0
.end method

.method static synthetic access$2400(Lcom/mediatek/mdlogger/MDLoggerService;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerService;

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mResumeLogState:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2402(Lcom/mediatek/mdlogger/MDLoggerService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerService;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mResumeLogState:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2500(Lcom/mediatek/mdlogger/MDLoggerService;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerService;

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSharedPrefLogState:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/mediatek/mdlogger/MDLoggerService;)I
    .locals 1
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerService;

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->checkSDStatus()I

    move-result v0

    return v0
.end method

.method static synthetic access$2702(Lcom/mediatek/mdlogger/MDLoggerService;I)I
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerService;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mAutoStartMode:I

    return p1
.end method

.method static synthetic access$2800(Lcom/mediatek/mdlogger/MDLoggerService;)Landroid/media/Ringtone;
    .locals 1
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerService;

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mAassertRingtone:Landroid/media/Ringtone;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/mediatek/mdlogger/MDLoggerService;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerService;

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/mdlogger/MDLoggerService;)V
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerService;

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->informActivityStopPolling()V

    return-void
.end method

.method static synthetic access$3002(Lcom/mediatek/mdlogger/MDLoggerService;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerService;
    .param p1    # Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mMonitorThreadRunning:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic access$3100(Lcom/mediatek/mdlogger/MDLoggerService;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerService;

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSharedPrefLogQueryJNI:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$3102(Lcom/mediatek/mdlogger/MDLoggerService;Landroid/content/SharedPreferences;)Landroid/content/SharedPreferences;
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerService;
    .param p1    # Landroid/content/SharedPreferences;

    iput-object p1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSharedPrefLogQueryJNI:Landroid/content/SharedPreferences;

    return-object p1
.end method

.method static synthetic access$3200(Lcom/mediatek/mdlogger/MDLoggerService;)I
    .locals 1
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerService;

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->initLogCustmizeSize()I

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/mediatek/mdlogger/MDLoggerService;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerService;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/mdlogger/MDLoggerService;->showMemoryDumpDone(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500(Lcom/mediatek/mdlogger/MDLoggerService;)I
    .locals 1
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerService;

    iget v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I

    return v0
.end method

.method static synthetic access$502(Lcom/mediatek/mdlogger/MDLoggerService;I)I
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerService;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I

    return p1
.end method

.method static synthetic access$600(Lcom/mediatek/mdlogger/MDLoggerService;)V
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerService;

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->setPrefereceLogQuryForJNI()V

    return-void
.end method

.method static synthetic access$700(Lcom/mediatek/mdlogger/MDLoggerService;)V
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerService;

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->informActivityStartPolling()V

    return-void
.end method

.method static synthetic access$800(Lcom/mediatek/mdlogger/MDLoggerService;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerService;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/mediatek/mdlogger/MDLoggerService;->showAlertDialog(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$900(Lcom/mediatek/mdlogger/MDLoggerService;)V
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerService;

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->informActivityPauseUI()V

    return-void
.end method

.method private checkSDStatus()I
    .locals 12

    const/4 v8, 0x3

    const/4 v9, 0x0

    const/4 v7, 0x2

    iget-object v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSdPathFromPropert:Ljava/lang/String;

    if-nez v6, :cond_0

    const-string v6, "ModemLog_Service"

    const-string v8, "mSdPathFromPropert is null"

    invoke-static {v6, v8}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v6, v7

    :goto_0
    return v6

    :cond_0
    iget-object v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mStorageManagerHandle:Landroid/os/storage/StorageManager;

    if-nez v6, :cond_1

    const-string v6, "storage"

    invoke-virtual {p0, v6}, Lcom/mediatek/mdlogger/MDLoggerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/os/storage/StorageManager;

    iput-object v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mStorageManagerHandle:Landroid/os/storage/StorageManager;

    :cond_1
    const-string v6, "ModemLog_Service"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "The SD card path sdPathFromPropert: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSdPathFromPropert:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v6, v10}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v6, "mounted"

    iget-object v10, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mStorageManagerHandle:Landroid/os/storage/StorageManager;

    iget-object v11, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSdPathFromPropert:Ljava/lang/String;

    invoke-virtual {v10, v11}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSdPathFromPropert:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    const-string v10, "sdcard"

    invoke-virtual {v6, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_6

    :cond_2
    new-instance v4, Ljava/io/File;

    iget-object v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSdPathFromPropert:Ljava/lang/String;

    invoke-direct {v4, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSdPathFromPropert:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    const-string v10, "sdcard"

    invoke-virtual {v6, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_3

    const-string v6, "ModemLog_Service"

    const-string v8, "The SD Card doesn\'t exist"

    invoke-static {v6, v8}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v6, v7

    goto :goto_0

    :cond_3
    invoke-virtual {v4}, Ljava/io/File;->canWrite()Z

    move-result v6

    if-nez v6, :cond_4

    const-string v6, "ModemLog_Service"

    const-string v7, "The SD Card is not writtable"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v6, v8

    goto :goto_0

    :cond_4
    new-instance v5, Landroid/os/StatFs;

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v0

    invoke-virtual {v5}, Landroid/os/StatFs;->getBlockSize()I

    move-result v6

    div-int/lit16 v3, v6, 0x400

    mul-int v6, v0, v3

    int-to-long v1, v6

    const-wide/16 v6, 0x283c

    cmp-long v6, v1, v6

    if-gez v6, :cond_5

    const-string v6, "ModemLog_Service"

    const-string v7, "The SD Card has not enough space"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v6, "ModemLog_Service"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Available blocks is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " and block size is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", and total available "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "K"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v6, 0x1

    goto/16 :goto_0

    :cond_5
    move v6, v9

    goto/16 :goto_0

    :cond_6
    const-string v6, "mounted"

    iget-object v10, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mStorageManagerHandle:Landroid/os/storage/StorageManager;

    iget-object v11, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSdPathFromPropert:Ljava/lang/String;

    invoke-virtual {v10, v11}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_9

    new-instance v4, Ljava/io/File;

    iget-object v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSdPathFromPropert:Ljava/lang/String;

    invoke-direct {v4, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v6, "ModemLog_Service"

    const-string v10, "getVolumeState is false"

    invoke-static {v6, v10}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_7

    const-string v6, "ModemLog_Service"

    const-string v8, "The SD Card doesn\'t exist"

    invoke-static {v6, v8}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v6, v7

    goto/16 :goto_0

    :cond_7
    invoke-virtual {v4}, Ljava/io/File;->canWrite()Z

    move-result v6

    if-nez v6, :cond_8

    const-string v6, "ModemLog_Service"

    const-string v7, "The SD Card is not writtable"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v6, v8

    goto/16 :goto_0

    :cond_8
    move v6, v9

    goto/16 :goto_0

    :cond_9
    const-string v6, "ModemLog_Service"

    const-string v8, "The SD Card is not available"

    invoke-static {v6, v8}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v6, v7

    goto/16 :goto_0
.end method

.method private getCustSizeFromProperty()I
    .locals 9

    const/16 v0, 0x3e8

    new-instance v5, Ljava/util/Properties;

    invoke-direct {v5}, Ljava/util/Properties;-><init>()V

    const/4 v2, 0x0

    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    const-string v6, "/system/etc/mtklog-config.prop"

    invoke-direct {v3, v6}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-virtual {v5, v3}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    const-string v6, "com.mediatek.log.modem.maxsize"

    invoke-virtual {v5, v6}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    const/16 v0, 0x3e8

    :try_start_3
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_3

    move-result v0

    :goto_0
    const/16 v6, 0x258

    if-ge v0, v6, :cond_0

    const/16 v0, 0x258

    const-string v6, "ModemLog_Service"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Min size is 600M, but custmize value is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "M "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v6, "ModemLog_Service"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Custmize value is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "M."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v3

    :goto_1
    return v0

    :catch_0
    move-exception v1

    const-string v6, "ModemLog_Service"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "File Not Found Exception "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :catch_1
    move-exception v1

    const-string v6, "ModemLog_Service"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "IOException load\ufffd\ufffd "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v3

    goto :goto_1

    :catch_2
    move-exception v1

    const-string v6, "ModemLog_Service"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "IOException close "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v3

    goto :goto_1

    :catch_3
    move-exception v1

    const/16 v0, 0x3e8

    goto/16 :goto_0
.end method

.method private getSdSwapPathOfService()V
    .locals 3

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mStorageManagerHandle:Landroid/os/storage/StorageManager;

    if-nez v0, :cond_0

    const-string v0, "storage"

    invoke-virtual {p0, v0}, Lcom/mediatek/mdlogger/MDLoggerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/storage/StorageManager;

    iput-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mStorageManagerHandle:Landroid/os/storage/StorageManager;

    :cond_0
    const-string v0, "debug.mdl.sdswap.path"

    const-string v1, "/mnt/sdcard"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSdPathFromPropert:Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSdPathFromPropert:Ljava/lang/String;

    if-eqz v0, :cond_1

    const-string v0, "ModemLog_Service"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SDcard path from debug.mdl.sdswap.path is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSdPathFromPropert:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void
.end method

.method private informActivityPause()V
    .locals 4

    const/4 v3, 0x2

    const-string v0, "ModemLog_Service"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Pause: current mode is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", and pause is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I

    if-ne v0, v3, :cond_0

    iget-boolean v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->setPrefereceLogQuryForJNI()V

    new-instance v0, Lcom/mediatek/mdlogger/MDLoggerService$9;

    invoke-direct {v0, p0}, Lcom/mediatek/mdlogger/MDLoggerService$9;-><init>(Lcom/mediatek/mdlogger/MDLoggerService;)V

    invoke-virtual {v0}, Lcom/mediatek/mdlogger/MDLoggerService$9;->start()V

    invoke-direct {p0, v3}, Lcom/mediatek/mdlogger/MDLoggerService;->updateNotificationBar(I)V

    :cond_0
    return-void
.end method

.method private informActivityPauseUI()V
    .locals 4

    const/4 v3, 0x2

    const-string v0, "ModemLog_Service"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Pause UI: current mode is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", and pause is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I

    if-ne v0, v3, :cond_0

    iget-boolean v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z

    if-nez v0, :cond_0

    const-string v0, "ModemLog_Service"

    const-string v1, "Inform Activity to pause"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z

    new-instance v0, Lcom/mediatek/mdlogger/MDLoggerService$8;

    invoke-direct {v0, p0}, Lcom/mediatek/mdlogger/MDLoggerService$8;-><init>(Lcom/mediatek/mdlogger/MDLoggerService;)V

    invoke-virtual {v0}, Lcom/mediatek/mdlogger/MDLoggerService$8;->start()V

    invoke-direct {p0, v3}, Lcom/mediatek/mdlogger/MDLoggerService;->updateNotificationBar(I)V

    :cond_0
    return-void
.end method

.method private informActivityResume()V
    .locals 3

    const-string v0, "ModemLog_Service"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Resume: current mode is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", and pause is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->initLogSavePathForSdSwap()V

    iget v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->setPrefereceLogQuryForJNI()V

    new-instance v0, Lcom/mediatek/mdlogger/MDLoggerService$10;

    invoke-direct {v0, p0}, Lcom/mediatek/mdlogger/MDLoggerService$10;-><init>(Lcom/mediatek/mdlogger/MDLoggerService;)V

    invoke-virtual {v0}, Lcom/mediatek/mdlogger/MDLoggerService$10;->start()V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/mediatek/mdlogger/MDLoggerService;->updateNotificationBar(I)V

    :cond_0
    return-void
.end method

.method private informActivityStartPolling()V
    .locals 3

    const-string v1, "ModemLog_Service"

    const-string v2, "Inform Activity that start polling"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.mediatek.mdlogger.MDLoggerTest_STARTPOLLING"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/mediatek/mdlogger/MDLoggerService;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method private informActivityStop()V
    .locals 4

    const-string v1, "ModemLog_Service"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Stop: current mode is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", and pause is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.mediatek.mdlogger.MDLoggerTest_STOP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "ModemLog_Service"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Inform Activity to stop: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mediatek/mdlogger/MDLoggerJNI;->stopMDLogging()I

    const-string v1, "ModemLog_Service"

    const-string v2, "Stop service by self"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v0}, Lcom/mediatek/mdlogger/MDLoggerService;->sendBroadcast(Landroid/content/Intent;)V

    const-string v1, "ModemLog_Service"

    const-string v2, "Inform MDLogger to stop"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->stopSelf()V

    :cond_1
    return-void
.end method

.method private informActivityStopPolling()V
    .locals 3

    const-string v1, "ModemLog_Service"

    const-string v2, "Inform Activity that stop polling"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.mediatek.mdlogger.MDLoggerTest_STOPPOLLING"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/mediatek/mdlogger/MDLoggerService;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method private initLogCustmizeSize()I
    .locals 4

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->getCustSizeFromProperty()I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSharedPrefLogQueryJNI:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "PREF_LOG_SIZE_CUST_INIT"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v2, "PREF_LOG_SIZE_CUST_SIZE"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "ModemLog_Service"

    const-string v3, "Set init customize size failed."

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return v1
.end method

.method private initLogSavePathForSdSwap()V
    .locals 5

    const-string v2, "debug.log2sd.defaultpath"

    const-string v3, "/mnt/sdcard"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "ModemLog_Service"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SDcard path from debug.log2sd.defaultpath is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mStorageManagerHandle:Landroid/os/storage/StorageManager;

    if-nez v2, :cond_0

    const-string v2, "storage"

    invoke-virtual {p0, v2}, Lcom/mediatek/mdlogger/MDLoggerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/storage/StorageManager;

    iput-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mStorageManagerHandle:Landroid/os/storage/StorageManager;

    :cond_0
    const/4 v1, 0x0

    const-string v2, "/mnt/sdcard2"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mStorageManagerHandle:Landroid/os/storage/StorageManager;

    invoke-virtual {v2}, Landroid/os/storage/StorageManager;->getExternalStoragePath()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ModemLog_Service"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "get sd path from extternal path:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    if-eqz v1, :cond_2

    iput-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSdPathFromPropert:Ljava/lang/String;

    :cond_2
    return-void

    :cond_3
    const-string v2, "/mnt/sdcard"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mStorageManagerHandle:Landroid/os/storage/StorageManager;

    invoke-virtual {v2}, Landroid/os/storage/StorageManager;->getInternalStoragePath()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ModemLog_Service"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "get sd path from internal path:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v1, :cond_1

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mStorageManagerHandle:Landroid/os/storage/StorageManager;

    invoke-virtual {v2}, Landroid/os/storage/StorageManager;->getExternalStoragePath()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ModemLog_Service"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "get sd path is null from internal path: may be not emmc"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private registerADBShellListener()V
    .locals 2

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mADBShellFilter:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mADBShellFilter:Landroid/content/IntentFilter;

    const-string v1, "com.mediatek.mdlogger.STOP"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mADBShellFilter:Landroid/content/IntentFilter;

    const-string v1, "com.mediatek.mdlogger.PAUSE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mADBShellFilter:Landroid/content/IntentFilter;

    const-string v1, "com.mediatek.mdlogger.RESUME"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mADBShellFilter:Landroid/content/IntentFilter;

    const-string v1, "com.mediatek.mdlogger.ENABLE_AUTOSTART"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mADBShellFilter:Landroid/content/IntentFilter;

    const-string v1, "com.mediatek.mdlogger.DISABLE_AUTOSTART"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mADBShellFilter:Landroid/content/IntentFilter;

    const-string v1, "com.mediatek.mdlogger.ENABLE_MEMORYDUMP"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mADBShellFilter:Landroid/content/IntentFilter;

    const-string v1, "com.mediatek.mdlogger.DISABLE_MEMORYDUMP"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mADBShellFilter:Landroid/content/IntentFilter;

    const-string v1, "com.mediatek.mdlogger.FORCE_MODEM_ASSERT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mADBShellReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mADBShellFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/mediatek/mdlogger/MDLoggerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const-string v0, "ModemLog_Service"

    const-string v1, "Register ADB Shell listener"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private registerActivityListener()V
    .locals 2

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mActivityFilter:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mActivityFilter:Landroid/content/IntentFilter;

    const-string v1, "com.mediatek.mdlogger.MDLoggerService"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mActivityReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mActivityFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/mediatek/mdlogger/MDLoggerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const-string v0, "ModemLog_Service"

    const-string v1, "Register Activity listener"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private registerCommonUIListener()V
    .locals 2

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCommonUIFilter:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCommonUIFilter:Landroid/content/IntentFilter;

    const-string v1, "com.mediatek.syslogger.action"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCommonUIReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCommonUIFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/mediatek/mdlogger/MDLoggerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const-string v0, "ModemLog_Service"

    const-string v1, "Register Common UI listener"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private registerMDLoggerListener()V
    .locals 2

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mMDLoggerFilter:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mMDLoggerFilter:Landroid/content/IntentFilter;

    const-string v1, "com.mediatek.mdlogger.LOGFILE_NOTEXIST"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mMDLoggerFilter:Landroid/content/IntentFilter;

    const-string v1, "com.mediatek.mdlogger.FAIL_SENDFILTER"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mMDLoggerFilter:Landroid/content/IntentFilter;

    const-string v1, "com.mediatek.mdlogger.MEMORYDUMP_START"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mMDLoggerFilter:Landroid/content/IntentFilter;

    const-string v1, "com.mediatek.mdlogger.MEMORYDUMP_DONE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mMDLoggerFilter:Landroid/content/IntentFilter;

    const-string v1, "com.mediatek.mdlogger.FAIL_WRITEFILE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mMDLoggerFilter:Landroid/content/IntentFilter;

    const-string v1, "com.mediatek.mdlogger.SDCARD_NOTEXIST"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mMDLoggerFilter:Landroid/content/IntentFilter;

    const-string v1, "com.mediatek.mdlogger.SDCARD_FULL"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mMDLoggerReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mMDLoggerFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/mediatek/mdlogger/MDLoggerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const-string v0, "ModemLog_Service"

    const-string v1, "Register MDLogger listener"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private registerSDStatusListener()V
    .locals 2

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSDStatusFilter:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSDStatusFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSDStatusFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_SHARED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSDStatusFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSDStatusFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSDStatusFilter:Landroid/content/IntentFilter;

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSDStatusReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSDStatusFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/mediatek/mdlogger/MDLoggerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mRegisterBroadcast:Z

    const-string v0, "ModemLog_Service"

    const-string v1, "Register SD Status listener"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private registerShutdownListener()V
    .locals 2

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mShutdownActionFilter:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mShutdownActionFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.ACTION_SHUTDOWN_IPO"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mShutdownReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mShutdownActionFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/mediatek/mdlogger/MDLoggerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const-string v0, "ModemLog_Service"

    const-string v1, "Register IPO Shutdown listener"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private runMonitorSDCardStatus()V
    .locals 2

    const-string v0, "ModemLog_Service"

    const-string v1, "run Monitor SDCard Status"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/mediatek/mdlogger/MDLoggerService$14;

    invoke-direct {v1, p0}, Lcom/mediatek/mdlogger/MDLoggerService$14;-><init>(Lcom/mediatek/mdlogger/MDLoggerService;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private runMonitoringLogSizeThread()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mMonitorThreadRunning:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mMonitorThreadStop:Ljava/lang/Boolean;

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/mediatek/mdlogger/MDLoggerService$15;

    invoke-direct {v1, p0}, Lcom/mediatek/mdlogger/MDLoggerService$15;-><init>(Lcom/mediatek/mdlogger/MDLoggerService;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mThread:Ljava/lang/Thread;

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method private setPrefereceLogQuryForJNI()V
    .locals 4

    const/4 v3, -0x1

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSharedPrefLogQueryJNI:Landroid/content/SharedPreferences;

    if-nez v1, :cond_0

    const-string v1, "PREF_LOG_QUERY_TABLE"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/mediatek/mdlogger/MDLoggerService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSharedPrefLogQueryJNI:Landroid/content/SharedPreferences;

    :cond_0
    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSharedPrefLogQueryJNI:Landroid/content/SharedPreferences;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSharedPrefLogQueryJNI:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "PREF_CURRENT_MODE"

    iget v2, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v1, "PREF_LOG_PAUSED"

    iget-boolean v2, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    iget v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mAutoStartMode:I

    if-eq v1, v3, :cond_1

    const-string v1, "PREF_AUTO_START"

    iget v2, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mAutoStartMode:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    :cond_1
    iput v3, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mAutoStartMode:I

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "ModemLog_Service"

    const-string v2, "Set sharedPrefLogQueryJNI failed. "

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    :goto_0
    return-void

    :cond_3
    const-string v1, "ModemLog_Service"

    const-string v2, "sharedPrefLogQueryJNI is null "

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setPreferenceLogMode(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSharedPrefLogState:Landroid/content/SharedPreferences;

    if-nez v1, :cond_0

    const-string v1, "PREF_LOG_STATE"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/mediatek/mdlogger/MDLoggerService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSharedPrefLogState:Landroid/content/SharedPreferences;

    :cond_0
    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSharedPrefLogState:Landroid/content/SharedPreferences;

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSharedPrefLogState:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "ModemLog_Service"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Set sharedPrefLogState is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v1, "ModemLog_Service"

    const-string v2, "Set sharedPrefLogState MODE_SD failed."

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private showAlertDialog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const-string v2, "ModemLog_Service"

    const-string v3, "Show alert dialog"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const v2, 0x1040013

    new-instance v3, Lcom/mediatek/mdlogger/MDLoggerService$13;

    invoke-direct {v3, p0}, Lcom/mediatek/mdlogger/MDLoggerService$13;-><init>(Lcom/mediatek/mdlogger/MDLoggerService;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x7d3

    invoke-virtual {v2, v3}, Landroid/view/Window;->setType(I)V

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method private showMemoryDumpDone(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    iget-object v3, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mAlertRingUri:Landroid/net/Uri;

    if-nez v3, :cond_0

    const/4 v3, 0x4

    invoke-static {v3}, Landroid/media/RingtoneManager;->getDefaultUri(I)Landroid/net/Uri;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mAlertRingUri:Landroid/net/Uri;

    :cond_0
    iget-object v3, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mAlertRingUri:Landroid/net/Uri;

    if-eqz v3, :cond_2

    const-string v3, "ModemLog_Service"

    const-string v4, "Play the ringtone"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mAassertRingtone:Landroid/media/Ringtone;

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mAlertRingUri:Landroid/net/Uri;

    invoke-static {p0, v3}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mAassertRingtone:Landroid/media/Ringtone;

    :cond_1
    iget-object v3, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mAassertRingtone:Landroid/media/Ringtone;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mAassertRingtone:Landroid/media/Ringtone;

    invoke-virtual {v3}, Landroid/media/Ringtone;->play()V

    :cond_2
    const-string v3, "ModemLog_Service"

    const-string v4, "Show memory dump dialog"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const v4, 0x7f060018

    invoke-virtual {p0, v4}, Lcom/mediatek/mdlogger/MDLoggerService;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f06001d

    invoke-virtual {p0, v3}, Lcom/mediatek/mdlogger/MDLoggerService;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const v3, 0x1040013

    new-instance v4, Lcom/mediatek/mdlogger/MDLoggerService$11;

    invoke-direct {v4, p0}, Lcom/mediatek/mdlogger/MDLoggerService$11;-><init>(Lcom/mediatek/mdlogger/MDLoggerService;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    const/16 v4, 0x7d3

    invoke-virtual {v3, v4}, Landroid/view/Window;->setType(I)V

    new-instance v3, Lcom/mediatek/mdlogger/MDLoggerService$12;

    invoke-direct {v3, p0}, Lcom/mediatek/mdlogger/MDLoggerService$12;-><init>(Lcom/mediatek/mdlogger/MDLoggerService;)V

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method private showSDCardFull()V
    .locals 4

    const-string v2, "ModemLog_Service"

    const-string v3, "Show SD card full dialog"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const v2, 0x7f06001d

    invoke-virtual {p0, v2}, Lcom/mediatek/mdlogger/MDLoggerService;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f06001c

    invoke-virtual {p0, v3}, Lcom/mediatek/mdlogger/MDLoggerService;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lcom/mediatek/mdlogger/MDLoggerService;->showAlertDialog(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x2

    invoke-static {v2}, Landroid/media/RingtoneManager;->getDefaultUri(I)Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v2, "ModemLog_Service"

    const-string v3, "Play the ringtone"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0, v0}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/media/Ringtone;->play()V

    :cond_0
    return-void
.end method

.method private updateNotificationBar(I)V
    .locals 14
    .param p1    # I

    const/4 v13, 0x2

    const v12, 0x7f060014

    const v11, 0x7f060015

    const v10, 0x7f060013

    const/4 v9, 0x0

    const v6, 0x7f060016

    invoke-virtual {p0, v6}, Lcom/mediatek/mdlogger/MDLoggerService;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    const/4 v2, 0x0

    const/4 v6, 0x1

    if-ne p1, v6, :cond_2

    const v2, 0x108000c

    invoke-virtual {p0, v10}, Lcom/mediatek/mdlogger/MDLoggerService;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    :cond_0
    :goto_0
    const-string v6, "ro.build.type"

    const-string v7, "eng"

    invoke-static {v6, v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v6, "eng"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    if-ne p1, v13, :cond_4

    const-string v6, "ModemLog_Service"

    const-string v7, "Update notification bar: stop by build type user."

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mNotificationBar:Landroid/app/NotificationManager;

    invoke-virtual {v6, v10}, Landroid/app/NotificationManager;->cancel(I)V

    iget-object v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mNotificationBar:Landroid/app/NotificationManager;

    invoke-virtual {v6, v11}, Landroid/app/NotificationManager;->cancel(I)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    if-ne p1, v13, :cond_3

    const v2, 0x108000b

    invoke-virtual {p0, v12}, Lcom/mediatek/mdlogger/MDLoggerService;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    goto :goto_0

    :cond_3
    const/4 v6, 0x3

    if-ne p1, v6, :cond_0

    const v2, 0x108000a

    invoke-virtual {p0, v11}, Lcom/mediatek/mdlogger/MDLoggerService;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    goto :goto_0

    :cond_4
    new-instance v4, Landroid/app/Notification;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-direct {v4, v2, v5, v6, v7}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    iget v6, v4, Landroid/app/Notification;->flags:I

    or-int/lit8 v6, v6, 0x20

    iput v6, v4, Landroid/app/Notification;->flags:I

    const/4 v1, 0x0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    new-instance v6, Landroid/content/ComponentName;

    const-string v7, "com.mediatek.engineermode"

    const-string v8, "com.mediatek.engineermode.syslogger.SysLogger"

    invoke-direct {v6, v7, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const-string v6, "WHICH"

    const-string v7, "Modem"

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    invoke-virtual {v6, v0, v9}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v6

    if-eqz v6, :cond_5

    invoke-static {p0, v9, v0, v9}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    :goto_2
    const v6, 0x7f060016

    invoke-virtual {p0, v6}, Lcom/mediatek/mdlogger/MDLoggerService;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v4, p0, v6, v5, v1}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    const/4 v6, 0x1

    if-ne p1, v6, :cond_6

    const-string v6, "ModemLog_Service"

    const-string v7, "Update notification bar: start"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mNotificationBar:Landroid/app/NotificationManager;

    invoke-virtual {v6, v12}, Landroid/app/NotificationManager;->cancel(I)V

    iget-object v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mNotificationBar:Landroid/app/NotificationManager;

    invoke-virtual {v6, v11}, Landroid/app/NotificationManager;->cancel(I)V

    iget-object v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mNotificationBar:Landroid/app/NotificationManager;

    invoke-virtual {v6, v10, v4}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_1

    :cond_5
    new-instance v6, Landroid/content/Intent;

    const-class v7, Lcom/mediatek/mdlogger/MDLoggerTest;

    invoke-direct {v6, p0, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {p0, v9, v6, v9}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    const-string v6, "ModemLog_Service"

    const-string v7, "Can\'t resolve activity SysLogger!"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_6
    if-ne p1, v13, :cond_7

    iget-object v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mNotificationBar:Landroid/app/NotificationManager;

    invoke-virtual {v6, v10}, Landroid/app/NotificationManager;->cancel(I)V

    iget-object v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mNotificationBar:Landroid/app/NotificationManager;

    invoke-virtual {v6, v11}, Landroid/app/NotificationManager;->cancel(I)V

    const-string v6, "ModemLog_Service"

    const-string v7, "Update notification bar: stop"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mNotificationBar:Landroid/app/NotificationManager;

    invoke-virtual {v6, v12, v4}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto/16 :goto_1

    :cond_7
    const/4 v6, 0x3

    if-ne p1, v6, :cond_1

    const-string v6, "ModemLog_Service"

    const-string v7, "Update notification bar: no sd card"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mNotificationBar:Landroid/app/NotificationManager;

    invoke-virtual {v6, v10}, Landroid/app/NotificationManager;->cancel(I)V

    iget-object v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mNotificationBar:Landroid/app/NotificationManager;

    invoke-virtual {v6, v12}, Landroid/app/NotificationManager;->cancel(I)V

    iget-object v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mNotificationBar:Landroid/app/NotificationManager;

    invoke-virtual {v6, v11, v4}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto/16 :goto_1
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    const/4 v3, 0x0

    const-string v0, "ModemLog_Service"

    const-string v1, "MDLoggerService onCreate"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/mediatek/mdlogger/MDLoggerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mNotificationBar:Landroid/app/NotificationManager;

    const-string v0, "PREF_LOG_STATE"

    invoke-virtual {p0, v0, v3}, Lcom/mediatek/mdlogger/MDLoggerService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSharedPrefLogState:Landroid/content/SharedPreferences;

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSharedPrefLogState:Landroid/content/SharedPreferences;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSharedPrefLogState:Landroid/content/SharedPreferences;

    const-string v1, "KEY_LOG_STATE"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mResumeLogState:Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mResumeLogState:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, "ModemLog_Service"

    const-string v1, "Re-Init sharedPrefLogState when MDLogger service is started."

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "KEY_LOG_STATE"

    const-string v1, ""

    invoke-direct {p0, v0, v1}, Lcom/mediatek/mdlogger/MDLoggerService;->setPreferenceLogMode(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    iput-boolean v3, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mRegisterBroadcast:Z

    iput-boolean v3, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mRegisterOtherBroadcast:Z

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->getSdSwapPathOfService()V

    new-instance v0, Lcom/mediatek/mdlogger/MDLoggerService$1;

    invoke-direct {v0, p0}, Lcom/mediatek/mdlogger/MDLoggerService$1;-><init>(Lcom/mediatek/mdlogger/MDLoggerService;)V

    iput-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mHandler:Landroid/os/Handler;

    return-void

    :cond_1
    const-string v0, "ModemLog_Service"

    const-string v1, "Log state sharedPrefLogState is null"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 4

    const/4 v3, 0x0

    const-string v1, "ro.monkey"

    const-string v2, "false"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "ModemLog_Service"

    const-string v2, "monkey is running, IGNORE this stop"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const-string v1, "ModemLog_Service"

    const-string v2, "MDLoggerService onDestroy"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mRegisterOtherBroadcast:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mShutdownReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/mediatek/mdlogger/MDLoggerService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mActivityReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/mediatek/mdlogger/MDLoggerService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mMDLoggerReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/mediatek/mdlogger/MDLoggerService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mADBShellReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/mediatek/mdlogger/MDLoggerService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const-string v1, "ModemLog_Service"

    const-string v2, "unregister common ui receiver"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCommonUIReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/mediatek/mdlogger/MDLoggerService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const-string v1, "ModemLog_Service"

    const-string v2, "unregister shutdown receiver"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "ModemLog_Service"

    const-string v2, "unregister activity receiver"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "ModemLog_Service"

    const-string v2, "unregister mdlogger receiver"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "ModemLog_Service"

    const-string v2, "unregister adb receiver"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x3e8

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    iget-boolean v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mRegisterBroadcast:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mSDStatusReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/mediatek/mdlogger/MDLoggerService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const-string v1, "ModemLog_Service"

    const-string v2, "unregister sd status receiver"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mNotificationBar:Landroid/app/NotificationManager;

    invoke-virtual {v1}, Landroid/app/NotificationManager;->cancelAll()V

    iput-boolean v3, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mServiceOn:Z

    iput-boolean v3, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mRegisterOtherBroadcast:Z

    goto :goto_0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 10
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    if-nez p1, :cond_8

    const-string v6, "ModemLog_Service"

    const-string v7, "START_STICKY work intent is null."

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mediatek/mdlogger/MDLoggerJNI;->getCurrentMDLoggingMode()I

    move-result v6

    iput v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I

    iget v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I

    if-nez v6, :cond_0

    const-string v6, "ModemLog_Service"

    const-string v7, "Intent is null. but service is idle"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v6, 0x2

    :goto_0
    return v6

    :cond_0
    invoke-static {}, Lcom/mediatek/mdlogger/MDLoggerJNI;->isMDLoggingPaused()Z

    move-result v6

    iput-boolean v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z

    iget-boolean v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mServiceOn:Z

    if-nez v6, :cond_6

    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mRegisterOtherBroadcast:Z

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->registerShutdownListener()V

    iget v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_1

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->registerSDStatusListener()V

    :cond_1
    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->registerActivityListener()V

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->registerMDLoggerListener()V

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->registerADBShellListener()V

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->registerCommonUIListener()V

    iget v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I

    const/4 v7, 0x3

    if-ne v6, v7, :cond_2

    invoke-static {}, Lcom/mediatek/mdlogger/MDLoggerJNI;->isMemoryDumpDone()Z

    move-result v6

    if-eqz v6, :cond_5

    const-string v6, "ModemLog_Service"

    const-string v7, "Memory dump is done, send the message to show memory dump dialog"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mHandler:Landroid/os/Handler;

    const/16 v7, 0x3e9

    const-wide/16 v8, 0x3e8

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_2
    :goto_1
    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->runMonitorSDCardStatus()V

    :goto_2
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mServiceOn:Z

    const/4 v4, 0x1

    iget v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_4

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->checkSDStatus()I

    move-result v5

    if-eqz v5, :cond_3

    const-string v6, "ModemLog_Service"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Check SD card state is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x0

    :cond_3
    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->runMonitoringLogSizeThread()V

    :cond_4
    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->setPrefereceLogQuryForJNI()V

    iget-object v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mNotificationBar:Landroid/app/NotificationManager;

    invoke-virtual {v6}, Landroid/app/NotificationManager;->cancelAll()V

    iget-boolean v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z

    if-nez v6, :cond_7

    if-eqz v4, :cond_7

    const/4 v6, 0x1

    invoke-direct {p0, v6}, Lcom/mediatek/mdlogger/MDLoggerService;->updateNotificationBar(I)V

    :goto_3
    const-string v6, "ModemLog_Service"

    const-string v7, "Service onStartCommand done by null intent."

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v6, 0x1

    goto/16 :goto_0

    :cond_5
    const-string v6, "ModemLog_Service"

    const-string v7, "Memory dump is going to be done, send the delay message to show memory dump dialog"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mHandler:Landroid/os/Handler;

    const/16 v7, 0x3e9

    const-wide/16 v8, 0xbb8

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1

    :cond_6
    const-string v6, "ModemLog_Service"

    const-string v7, "Service is already running"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_7
    const/4 v6, 0x2

    invoke-direct {p0, v6}, Lcom/mediatek/mdlogger/MDLoggerService;->updateNotificationBar(I)V

    goto :goto_3

    :cond_8
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    const-string v7, "com.mediatek.mdlogger.MDLoggerService"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_a

    const-string v6, "ModemLog_Service"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "intent error: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mediatek/mdlogger/MDLoggerJNI;->getCurrentMDLoggingMode()I

    move-result v6

    iput v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I

    iget v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I

    if-nez v6, :cond_9

    const-string v6, "ModemLog_Service"

    const-string v7, "Intent is null. but service is idle"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v6, 0x2

    goto/16 :goto_0

    :cond_9
    iget-object v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mNotificationBar:Landroid/app/NotificationManager;

    invoke-virtual {v6}, Landroid/app/NotificationManager;->cancelAll()V

    const/4 v6, 0x2

    invoke-direct {p0, v6}, Lcom/mediatek/mdlogger/MDLoggerService;->updateNotificationBar(I)V

    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mServiceOn:Z

    const/4 v6, 0x1

    goto/16 :goto_0

    :cond_a
    const-string v6, "ModemLog_Service"

    const-string v7, "MDLoggerService onStartCommand"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v6, "ModemLog_Service"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Receive intent: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " with flags "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x1

    const-string v6, "ActivityStartService"

    const/4 v7, 0x1

    invoke-virtual {p1, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const/4 v6, 0x2

    if-ne v1, v6, :cond_f

    const-string v6, "ModemLog_Service"

    const-string v7, "Start service from activity"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v6, "LoggingMode"

    const/4 v7, 0x2

    invoke-virtual {p1, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    iput v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I

    const-string v6, "RunStatus"

    const/4 v7, 0x0

    invoke-virtual {p1, v6, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    iput-boolean v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z

    :cond_b
    :goto_4
    const-string v7, "ModemLog_Service"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Current Mode is "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v8, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", and Running status is "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-boolean v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z

    if-nez v6, :cond_1f

    const/4 v6, 0x1

    :goto_5
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v6}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mServiceOn:Z

    if-nez v6, :cond_21

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->registerShutdownListener()V

    iget v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_c

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->registerSDStatusListener()V

    :cond_c
    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->registerCommonUIListener()V

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->registerActivityListener()V

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->registerMDLoggerListener()V

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->registerADBShellListener()V

    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mRegisterOtherBroadcast:Z

    iget v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I

    const/4 v7, 0x3

    if-ne v6, v7, :cond_d

    invoke-static {}, Lcom/mediatek/mdlogger/MDLoggerJNI;->isMemoryDumpDone()Z

    move-result v6

    if-eqz v6, :cond_20

    const-string v6, "ModemLog_Service"

    const-string v7, "Memory dump is done, send the message to show memory dump dialog"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mHandler:Landroid/os/Handler;

    const/16 v7, 0x3e9

    const-wide/16 v8, 0x3e8

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_d
    :goto_6
    const-string v6, "ModemLog_Service"

    const-string v7, "Monitor sd card by start service"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->runMonitorSDCardStatus()V

    :goto_7
    iget v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I

    if-eqz v6, :cond_e

    iget v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I

    const/4 v7, 0x1

    if-eq v6, v7, :cond_e

    const-string v6, "ModemLog_Service"

    const-string v7, "On start command Thread mMonitorThreadStop = false"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    iput-object v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mMonitorThreadStop:Ljava/lang/Boolean;

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->runMonitoringLogSizeThread()V

    :cond_e
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mServiceOn:Z

    iget-object v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mNotificationBar:Landroid/app/NotificationManager;

    invoke-virtual {v6}, Landroid/app/NotificationManager;->cancelAll()V

    iget-boolean v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z

    if-nez v6, :cond_22

    if-eqz v4, :cond_22

    const/4 v6, 0x1

    invoke-direct {p0, v6}, Lcom/mediatek/mdlogger/MDLoggerService;->updateNotificationBar(I)V

    :goto_8
    const-string v6, "ModemLog_Service"

    const-string v7, "Service onStartCommand done"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v6, 0x1

    goto/16 :goto_0

    :cond_f
    const/4 v6, 0x5

    if-ne v1, v6, :cond_11

    invoke-static {}, Lcom/mediatek/mdlogger/MDLoggerJNI;->getCurrentMDLoggingMode()I

    move-result v6

    iput v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I

    invoke-static {}, Lcom/mediatek/mdlogger/MDLoggerJNI;->isMDLoggingPaused()Z

    move-result v6

    iput-boolean v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z

    invoke-static {}, Lcom/mediatek/mdlogger/MDLoggerJNI;->getAutoStartMDLoggingMode()I

    move-result v6

    iput v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mAutoStartMode:I

    iget-boolean v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z

    if-nez v6, :cond_10

    iget v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I

    const/4 v7, 0x1

    if-eq v6, v7, :cond_10

    const-string v6, "ModemLog_Service"

    const-string v7, "On start command Thread mMonitorThreadStop = false"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    iput-object v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mMonitorThreadStop:Ljava/lang/Boolean;

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->runMonitoringLogSizeThread()V

    :cond_10
    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->setPrefereceLogQuryForJNI()V

    const-string v6, "ModemLog_Service"

    const-string v7, "Start service to init prefrence"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v6

    invoke-static {v6}, Landroid/os/Process;->killProcess(I)V

    const/4 v6, 0x1

    goto/16 :goto_0

    :cond_11
    const/4 v6, 0x1

    if-ne v1, v6, :cond_13

    const-string v6, "ModemLog_Service"

    const-string v7, "Start service from mdlogger"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mediatek/mdlogger/MDLoggerJNI;->getCurrentMDLoggingMode()I

    move-result v6

    iput v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I

    iget-boolean v3, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z

    invoke-static {}, Lcom/mediatek/mdlogger/MDLoggerJNI;->isMDLoggingPaused()Z

    move-result v6

    iput-boolean v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z

    iget v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_12

    const-string v6, "KEY_LOG_STATE"

    const-string v7, "MODE_SD"

    invoke-direct {p0, v6, v7}, Lcom/mediatek/mdlogger/MDLoggerService;->setPreferenceLogMode(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z

    if-nez v6, :cond_12

    const-string v6, "ModemLog_Service"

    const-string v7, "On start command Thread mMonitorThreadStop = false"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    iput-object v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mMonitorThreadStop:Ljava/lang/Boolean;

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->runMonitoringLogSizeThread()V

    :cond_12
    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->setPrefereceLogQuryForJNI()V

    iget-boolean v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mServiceOn:Z

    if-eqz v6, :cond_b

    iget-boolean v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z

    if-ne v3, v6, :cond_b

    const-string v6, "ModemLog_Service"

    const-string v7, "Start service bServiceOn is true"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v6, 0x1

    goto/16 :goto_0

    :cond_13
    const/4 v6, 0x3

    if-ne v1, v6, :cond_1a

    const-string v6, "ModemLog_Service"

    const-string v7, "Start service from common ui"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Landroid/content/Intent;

    const-string v6, "com.mediatek.syslogger.action"

    invoke-direct {v2, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v6, "From"

    const-string v7, "ModemLog"

    invoke-virtual {v2, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v6, "To"

    const-string v7, "CommonUI"

    invoke-virtual {v2, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v6, "Command"

    const-string v7, "Start"

    invoke-virtual {v2, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-boolean v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z

    if-nez v6, :cond_14

    const-string v6, "ModemLog_Service"

    const-string v7, "MDLogger is already running"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v6, "Return"

    const-string v7, "AlreadyRun"

    invoke-virtual {v2, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_14
    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->checkSDStatus()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_16

    const-string v6, "ModemLog_Service"

    const-string v7, "Check SD card: full"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v6, "Return"

    const-string v7, "SDCardFull"

    invoke-virtual {v2, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v4, 0x0

    :cond_15
    :goto_9
    const-wide/16 v6, 0x7d0

    :try_start_0
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_a
    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->setPrefereceLogQuryForJNI()V

    invoke-virtual {p0, v2}, Lcom/mediatek/mdlogger/MDLoggerService;->sendBroadcast(Landroid/content/Intent;)V

    const-string v6, "ModemLog_Service"

    const-string v7, "CommonUI: return the info"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    :cond_16
    const/4 v6, 0x2

    if-ne v5, v6, :cond_17

    const-string v6, "ModemLog_Service"

    const-string v7, "Check SD card: not exist"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v6, "Return"

    const-string v7, "NoSDCard"

    invoke-virtual {v2, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v4, 0x0

    goto :goto_9

    :cond_17
    if-nez v5, :cond_15

    const-string v6, "ModemLog_Service"

    const-string v7, "Check SD card: normal"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mServiceOn:Z

    if-nez v6, :cond_18

    const/4 v6, 0x2

    invoke-static {v6}, Lcom/mediatek/mdlogger/MDLoggerJNI;->startMDLogging(I)I

    const/4 v6, 0x2

    invoke-static {v6}, Lcom/mediatek/mdlogger/MDLoggerJNI;->setAutoStartMDLoggingMode(I)I

    const/4 v6, 0x2

    iput v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I

    const/4 v6, 0x2

    iput v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mAutoStartMode:I

    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z

    const-string v6, "ModemLog_Service"

    const-string v7, "CommonUI: Start MDLogger"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_b
    const-string v6, "Return"

    const-string v7, "Normal"

    invoke-virtual {v2, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_9

    :cond_18
    iget v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_19

    invoke-static {}, Lcom/mediatek/mdlogger/MDLoggerJNI;->resumeMDLogging()I

    const/4 v6, 0x2

    invoke-static {v6}, Lcom/mediatek/mdlogger/MDLoggerJNI;->setAutoStartMDLoggingMode(I)I

    const/4 v6, 0x2

    iput v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mAutoStartMode:I

    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z

    const-string v6, "ModemLog_Service"

    const-string v7, "CommonUI: Resume MDLogger"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_b

    :cond_19
    const/4 v4, 0x0

    goto :goto_b

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_a

    :cond_1a
    const/4 v6, 0x4

    if-ne v1, v6, :cond_b

    const-string v6, "ModemLog_Service"

    const-string v7, "Start service from Auto Test"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mediatek/mdlogger/MDLoggerJNI;->getCurrentMDLoggingMode()I

    move-result v6

    iput v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I

    iget v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_1e

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->checkSDStatus()I

    move-result v5

    if-eqz v5, :cond_1b

    const-string v6, "ModemLog_Service"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Check SD card state is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x0

    :goto_c
    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerService;->setPrefereceLogQuryForJNI()V

    goto/16 :goto_4

    :cond_1b
    iget-boolean v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mServiceOn:Z

    if-nez v6, :cond_1c

    const/4 v6, 0x2

    invoke-static {v6}, Lcom/mediatek/mdlogger/MDLoggerJNI;->startMDLogging(I)I

    const/4 v6, 0x2

    invoke-static {v6}, Lcom/mediatek/mdlogger/MDLoggerJNI;->setAutoStartMDLoggingMode(I)I

    const/4 v6, 0x2

    iput v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I

    const/4 v6, 0x2

    iput v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mAutoStartMode:I

    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z

    const-string v6, "ModemLog_Service"

    const-string v7, "Auto Test: Start MDLogger"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_d
    invoke-static {}, Lcom/mediatek/mdlogger/MDLoggerJNI;->isMDLoggingPaused()Z

    move-result v6

    iput-boolean v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z

    goto :goto_c

    :cond_1c
    iget v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mCurrentMode:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_1d

    invoke-static {}, Lcom/mediatek/mdlogger/MDLoggerJNI;->resumeMDLogging()I

    const/4 v6, 0x2

    invoke-static {v6}, Lcom/mediatek/mdlogger/MDLoggerJNI;->setAutoStartMDLoggingMode(I)I

    const/4 v6, 0x2

    iput v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mAutoStartMode:I

    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mPaused:Z

    const-string v6, "ModemLog_Service"

    const-string v7, "Auto Test: Resume MDLogger"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_d

    :cond_1d
    const/4 v4, 0x0

    goto :goto_d

    :cond_1e
    const/4 v4, 0x0

    goto :goto_c

    :cond_1f
    const/4 v6, 0x0

    goto/16 :goto_5

    :cond_20
    const-string v6, "ModemLog_Service"

    const-string v7, "Memory dump is going to be done, send the delay message to show memory dump dialog"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/mdlogger/MDLoggerService;->mHandler:Landroid/os/Handler;

    const/16 v7, 0x3e9

    const-wide/16 v8, 0xbb8

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_6

    :cond_21
    const-string v6, "ModemLog_Service"

    const-string v7, "Service is already running"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_7

    :cond_22
    const/4 v6, 0x2

    invoke-direct {p0, v6}, Lcom/mediatek/mdlogger/MDLoggerService;->updateNotificationBar(I)V

    goto/16 :goto_8
.end method
