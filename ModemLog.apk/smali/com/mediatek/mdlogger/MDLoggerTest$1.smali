.class Lcom/mediatek/mdlogger/MDLoggerTest$1;
.super Landroid/os/Handler;
.source "MDLoggerTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/mdlogger/MDLoggerTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/mdlogger/MDLoggerTest;


# direct methods
.method constructor <init>(Lcom/mediatek/mdlogger/MDLoggerTest;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/mdlogger/MDLoggerTest$1;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1    # Landroid/os/Message;

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest$1;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerTest;->mWaitingStopDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/mediatek/mdlogger/MDLoggerTest;->access$000(Lcom/mediatek/mdlogger/MDLoggerTest;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest$1;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerTest;->mWaitingStopDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/mediatek/mdlogger/MDLoggerTest;->access$000(Lcom/mediatek/mdlogger/MDLoggerTest;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest$1;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    const/4 v2, 0x0

    # setter for: Lcom/mediatek/mdlogger/MDLoggerTest;->mWaitingStopDialog:Landroid/app/ProgressDialog;
    invoke-static {v1, v2}, Lcom/mediatek/mdlogger/MDLoggerTest;->access$002(Lcom/mediatek/mdlogger/MDLoggerTest;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest$1;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    const-string v2, "SD card avaliable space is not enough for recycle size."

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3ea
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
