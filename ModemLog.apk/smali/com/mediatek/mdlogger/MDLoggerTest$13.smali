.class Lcom/mediatek/mdlogger/MDLoggerTest$13;
.super Ljava/lang/Object;
.source "MDLoggerTest.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/mdlogger/MDLoggerTest;->clearOldLog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/mdlogger/MDLoggerTest;


# direct methods
.method constructor <init>(Lcom/mediatek/mdlogger/MDLoggerTest;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/mdlogger/MDLoggerTest$13;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 8
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/mediatek/mdlogger/MDLoggerTest$13;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerTest;->mSdPathFromPropert:Ljava/lang/String;
    invoke-static {v3}, Lcom/mediatek/mdlogger/MDLoggerTest;->access$2700(Lcom/mediatek/mdlogger/MDLoggerTest;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mtklog/mdlog/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "ModemLog_Activity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Clear log path:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest$13;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    iget-object v3, p0, Lcom/mediatek/mdlogger/MDLoggerTest$13;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerTest$13;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    const v5, 0x7f060021

    invoke-virtual {v4, v5}, Lcom/mediatek/mdlogger/MDLoggerTest;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/mdlogger/MDLoggerTest$13;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    const v6, 0x7f060022

    invoke-virtual {v5, v6}, Lcom/mediatek/mdlogger/MDLoggerTest;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-static {v3, v4, v5, v6, v7}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)Landroid/app/ProgressDialog;

    move-result-object v3

    # setter for: Lcom/mediatek/mdlogger/MDLoggerTest;->mWaitingStopDialog:Landroid/app/ProgressDialog;
    invoke-static {v2, v3}, Lcom/mediatek/mdlogger/MDLoggerTest;->access$002(Lcom/mediatek/mdlogger/MDLoggerTest;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    new-instance v2, Lcom/mediatek/mdlogger/MDLoggerTest$13$1;

    invoke-direct {v2, p0, v1}, Lcom/mediatek/mdlogger/MDLoggerTest$13$1;-><init>(Lcom/mediatek/mdlogger/MDLoggerTest$13;Ljava/io/File;)V

    invoke-virtual {v2}, Lcom/mediatek/mdlogger/MDLoggerTest$13$1;->start()V

    :cond_0
    return-void
.end method
