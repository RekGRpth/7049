.class Lcom/mediatek/mdlogger/MDLoggerTest$2;
.super Ljava/lang/Object;
.source "MDLoggerTest.java"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/mdlogger/MDLoggerTest;->mapViewListeners()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/mdlogger/MDLoggerTest;


# direct methods
.method constructor <init>(Lcom/mediatek/mdlogger/MDLoggerTest;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/mdlogger/MDLoggerTest$2;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 2
    .param p1    # Landroid/widget/RadioGroup;
    .param p2    # I

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest$2;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerTest;->mRadBtnUSBMode:Landroid/widget/RadioButton;
    invoke-static {v0}, Lcom/mediatek/mdlogger/MDLoggerTest;->access$100(Lcom/mediatek/mdlogger/MDLoggerTest;)Landroid/widget/RadioButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/RadioButton;->getId()I

    move-result v0

    if-ne p2, v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest$2;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    const/4 v1, 0x1

    # setter for: Lcom/mediatek/mdlogger/MDLoggerTest;->mSelectedMode:I
    invoke-static {v0, v1}, Lcom/mediatek/mdlogger/MDLoggerTest;->access$202(Lcom/mediatek/mdlogger/MDLoggerTest;I)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest$2;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerTest;->mRadBtnSDMode:Landroid/widget/RadioButton;
    invoke-static {v0}, Lcom/mediatek/mdlogger/MDLoggerTest;->access$300(Lcom/mediatek/mdlogger/MDLoggerTest;)Landroid/widget/RadioButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/RadioButton;->getId()I

    move-result v0

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest$2;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    const/4 v1, 0x2

    # setter for: Lcom/mediatek/mdlogger/MDLoggerTest;->mSelectedMode:I
    invoke-static {v0, v1}, Lcom/mediatek/mdlogger/MDLoggerTest;->access$202(Lcom/mediatek/mdlogger/MDLoggerTest;I)I

    goto :goto_0
.end method
