.class Lcom/mediatek/mdlogger/MDLoggerTest$3;
.super Ljava/lang/Object;
.source "MDLoggerTest.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/mdlogger/MDLoggerTest;->mapViewListeners()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/mdlogger/MDLoggerTest;


# direct methods
.method constructor <init>(Lcom/mediatek/mdlogger/MDLoggerTest;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/mdlogger/MDLoggerTest$3;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1    # Landroid/view/View;

    const/4 v8, 0x0

    const v7, 0x7f06001d

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerTest$3;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerTest;->mUIInit:Z
    invoke-static {v4}, Lcom/mediatek/mdlogger/MDLoggerTest;->access$400(Lcom/mediatek/mdlogger/MDLoggerTest;)Z

    move-result v4

    if-nez v4, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerTest$3;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerTest;->mLimitlogSize:I
    invoke-static {v4}, Lcom/mediatek/mdlogger/MDLoggerTest;->access$500(Lcom/mediatek/mdlogger/MDLoggerTest;)I

    move-result v1

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerTest$3;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerTest;->mLogSizeLimitEdit:Landroid/widget/EditText;
    invoke-static {v4}, Lcom/mediatek/mdlogger/MDLoggerTest;->access$600(Lcom/mediatek/mdlogger/MDLoggerTest;)Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/mediatek/mdlogger/MDLoggerTest$3;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    invoke-virtual {v5}, Lcom/mediatek/mdlogger/MDLoggerTest;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f06002b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " (600~"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7fffffff

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :try_start_0
    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerTest$3;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    # setter for: Lcom/mediatek/mdlogger/MDLoggerTest;->mLimitlogSize:I
    invoke-static {v4, v5}, Lcom/mediatek/mdlogger/MDLoggerTest;->access$502(Lcom/mediatek/mdlogger/MDLoggerTest;I)I

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerTest$3;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerTest;->mLimitlogSize:I
    invoke-static {v4}, Lcom/mediatek/mdlogger/MDLoggerTest;->access$500(Lcom/mediatek/mdlogger/MDLoggerTest;)I

    move-result v4

    const/16 v5, 0x258

    if-ge v4, v5, :cond_1

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerTest$3;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    # setter for: Lcom/mediatek/mdlogger/MDLoggerTest;->mLimitlogSize:I
    invoke-static {v4, v1}, Lcom/mediatek/mdlogger/MDLoggerTest;->access$502(Lcom/mediatek/mdlogger/MDLoggerTest;I)I

    new-instance v4, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/mediatek/mdlogger/MDLoggerTest$3;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    invoke-direct {v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v5, 0x7f06001d

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const-string v5, "Ok"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v4, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/mediatek/mdlogger/MDLoggerTest$3;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    invoke-direct {v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const-string v5, "Ok"

    invoke-virtual {v4, v5, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerTest$3;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    # setter for: Lcom/mediatek/mdlogger/MDLoggerTest;->mLimitlogSize:I
    invoke-static {v4, v1}, Lcom/mediatek/mdlogger/MDLoggerTest;->access$502(Lcom/mediatek/mdlogger/MDLoggerTest;I)I

    goto/16 :goto_0

    :cond_1
    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerTest$3;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerTest;->mBtnStart:Landroid/widget/Button;
    invoke-static {v4}, Lcom/mediatek/mdlogger/MDLoggerTest;->access$700(Lcom/mediatek/mdlogger/MDLoggerTest;)Landroid/widget/Button;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerTest$3;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    iget-object v5, p0, Lcom/mediatek/mdlogger/MDLoggerTest$3;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerTest;->mSelectedMode:I
    invoke-static {v5}, Lcom/mediatek/mdlogger/MDLoggerTest;->access$200(Lcom/mediatek/mdlogger/MDLoggerTest;)I

    move-result v5

    # setter for: Lcom/mediatek/mdlogger/MDLoggerTest;->mAutoStartModeTemp:I
    invoke-static {v4, v5}, Lcom/mediatek/mdlogger/MDLoggerTest;->access$802(Lcom/mediatek/mdlogger/MDLoggerTest;I)I

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerTest$3;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    # invokes: Lcom/mediatek/mdlogger/MDLoggerTest;->initLogSavePathForSdSwap()V
    invoke-static {v4}, Lcom/mediatek/mdlogger/MDLoggerTest;->access$900(Lcom/mediatek/mdlogger/MDLoggerTest;)V

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerTest$3;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerTest;->mStarted:Z
    invoke-static {v4}, Lcom/mediatek/mdlogger/MDLoggerTest;->access$1000(Lcom/mediatek/mdlogger/MDLoggerTest;)Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerTest$3;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    # invokes: Lcom/mediatek/mdlogger/MDLoggerTest;->startLogging()V
    invoke-static {v4}, Lcom/mediatek/mdlogger/MDLoggerTest;->access$1100(Lcom/mediatek/mdlogger/MDLoggerTest;)V

    goto/16 :goto_0

    :cond_2
    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerTest$3;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    # invokes: Lcom/mediatek/mdlogger/MDLoggerTest;->resumeLogging()V
    invoke-static {v4}, Lcom/mediatek/mdlogger/MDLoggerTest;->access$1200(Lcom/mediatek/mdlogger/MDLoggerTest;)V

    goto/16 :goto_0
.end method
