.class Lcom/mediatek/mdlogger/MDLoggerTest$4;
.super Ljava/lang/Object;
.source "MDLoggerTest.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/mdlogger/MDLoggerTest;->mapViewListeners()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/mdlogger/MDLoggerTest;


# direct methods
.method constructor <init>(Lcom/mediatek/mdlogger/MDLoggerTest;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/mdlogger/MDLoggerTest$4;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest$4;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerTest;->mUIInit:Z
    invoke-static {v0}, Lcom/mediatek/mdlogger/MDLoggerTest;->access$400(Lcom/mediatek/mdlogger/MDLoggerTest;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest$4;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    # setter for: Lcom/mediatek/mdlogger/MDLoggerTest;->mAutoStartModeTemp:I
    invoke-static {v0, v1}, Lcom/mediatek/mdlogger/MDLoggerTest;->access$802(Lcom/mediatek/mdlogger/MDLoggerTest;I)I

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest$4;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerTest;->mBtnStop:Landroid/widget/Button;
    invoke-static {v0}, Lcom/mediatek/mdlogger/MDLoggerTest;->access$1300(Lcom/mediatek/mdlogger/MDLoggerTest;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest$4;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    const/4 v1, 0x1

    # invokes: Lcom/mediatek/mdlogger/MDLoggerTest;->pauseLogging(Z)V
    invoke-static {v0, v1}, Lcom/mediatek/mdlogger/MDLoggerTest;->access$1400(Lcom/mediatek/mdlogger/MDLoggerTest;Z)V

    goto :goto_0
.end method
