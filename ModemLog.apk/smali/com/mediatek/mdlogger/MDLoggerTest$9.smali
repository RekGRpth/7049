.class Lcom/mediatek/mdlogger/MDLoggerTest$9;
.super Landroid/content/BroadcastReceiver;
.source "MDLoggerTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/mdlogger/MDLoggerTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/mdlogger/MDLoggerTest;


# direct methods
.method constructor <init>(Lcom/mediatek/mdlogger/MDLoggerTest;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/mdlogger/MDLoggerTest$9;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x0

    const-string v1, "ModemLog_Activity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Receive ModemLog Service broadcast: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.mediatek.mdlogger.MDLoggerTest_PAUSE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "com.mediatek.mdlogger.MDLoggerTest_RESUME"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest$9;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    # invokes: Lcom/mediatek/mdlogger/MDLoggerTest;->updateLoggingStatus()V
    invoke-static {v1}, Lcom/mediatek/mdlogger/MDLoggerTest;->access$1900(Lcom/mediatek/mdlogger/MDLoggerTest;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v1, "com.mediatek.mdlogger.MDLoggerTest_STOP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "ModemLog_Activity"

    const-string v2, "Stop logging from service"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest$9;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    # invokes: Lcom/mediatek/mdlogger/MDLoggerTest;->stopLogging()V
    invoke-static {v1}, Lcom/mediatek/mdlogger/MDLoggerTest;->access$2000(Lcom/mediatek/mdlogger/MDLoggerTest;)V

    goto :goto_0

    :cond_3
    const-string v1, "com.mediatek.mdlogger.MDLoggerTest_STARTPOLLING"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "ModemLog_Activity"

    const-string v2, "Memory dump is started"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest$9;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    # setter for: Lcom/mediatek/mdlogger/MDLoggerTest;->mPolling:Z
    invoke-static {v1, v5}, Lcom/mediatek/mdlogger/MDLoggerTest;->access$2102(Lcom/mediatek/mdlogger/MDLoggerTest;Z)Z

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest$9;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    # setter for: Lcom/mediatek/mdlogger/MDLoggerTest;->mStarted:Z
    invoke-static {v1, v5}, Lcom/mediatek/mdlogger/MDLoggerTest;->access$1002(Lcom/mediatek/mdlogger/MDLoggerTest;Z)Z

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest$9;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    # setter for: Lcom/mediatek/mdlogger/MDLoggerTest;->mPaused:Z
    invoke-static {v1, v4}, Lcom/mediatek/mdlogger/MDLoggerTest;->access$2202(Lcom/mediatek/mdlogger/MDLoggerTest;Z)Z

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest$9;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    # setter for: Lcom/mediatek/mdlogger/MDLoggerTest;->mCurrentMode:I
    invoke-static {v1, v6}, Lcom/mediatek/mdlogger/MDLoggerTest;->access$2302(Lcom/mediatek/mdlogger/MDLoggerTest;I)I

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest$9;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerTest;->mBtnStart:Landroid/widget/Button;
    invoke-static {v1}, Lcom/mediatek/mdlogger/MDLoggerTest;->access$700(Lcom/mediatek/mdlogger/MDLoggerTest;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest$9;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerTest;->mBtnStop:Landroid/widget/Button;
    invoke-static {v1}, Lcom/mediatek/mdlogger/MDLoggerTest;->access$1300(Lcom/mediatek/mdlogger/MDLoggerTest;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest$9;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerTest;->mBtnForceAssert:Landroid/widget/Button;
    invoke-static {v1}, Lcom/mediatek/mdlogger/MDLoggerTest;->access$2400(Lcom/mediatek/mdlogger/MDLoggerTest;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest$9;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerTest;->mBtnClearLog:Landroid/widget/Button;
    invoke-static {v1}, Lcom/mediatek/mdlogger/MDLoggerTest;->access$2500(Lcom/mediatek/mdlogger/MDLoggerTest;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest$9;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest$9;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    # getter for: Lcom/mediatek/mdlogger/MDLoggerTest;->mPaused:Z
    invoke-static {v2}, Lcom/mediatek/mdlogger/MDLoggerTest;->access$2200(Lcom/mediatek/mdlogger/MDLoggerTest;)Z

    move-result v2

    # invokes: Lcom/mediatek/mdlogger/MDLoggerTest;->setLoggingModeText(IZ)V
    invoke-static {v1, v6, v2}, Lcom/mediatek/mdlogger/MDLoggerTest;->access$2600(Lcom/mediatek/mdlogger/MDLoggerTest;IZ)V

    goto :goto_0

    :cond_4
    const-string v1, "com.mediatek.mdlogger.MDLoggerTest_STOPPOLLING"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "ModemLog_Activity"

    const-string v2, "Memory dump is done, finish activity"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest$9;->this$0:Lcom/mediatek/mdlogger/MDLoggerTest;

    invoke-virtual {v1}, Lcom/mediatek/mdlogger/MDLoggerTest;->finish()V

    goto :goto_0
.end method
