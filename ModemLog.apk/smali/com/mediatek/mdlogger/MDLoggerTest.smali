.class public Lcom/mediatek/mdlogger/MDLoggerTest;
.super Landroid/app/Activity;
.source "MDLoggerTest.java"


# static fields
.field private static final MSGID_NOTIFY_SD_RECYCLE:I = 0x3eb

.field private static final MSGID_STOP_TIMEOUT:I = 0x3ea

.field private static final TAG:Ljava/lang/String; = "ModemLog_Activity"


# instance fields
.field private mAutoStartMode:I

.field private mAutoStartModeTemp:I

.field private mBtnClearLog:Landroid/widget/Button;

.field private mBtnForceAssert:Landroid/widget/Button;

.field private mBtnStart:Landroid/widget/Button;

.field private mBtnStop:Landroid/widget/Button;

.field private mCheckAutoStart:Z

.field private mChkAutoStart:Landroid/widget/CheckBox;

.field private mChkEnableMemoryDump:Landroid/widget/CheckBox;

.field private mCurrentMode:I

.field private mInfoToShow:Landroid/widget/TextView;

.field private mLimitlogSize:I

.field private mLogSizeLimitEdit:Landroid/widget/EditText;

.field private mMessageHandler:Landroid/os/Handler;

.field private mNotifySize:J

.field private mPaused:Z

.field private mPolling:Z

.field private mPositionMode:I

.field private mRadBtnSDMode:Landroid/widget/RadioButton;

.field private mRadBtnUSBMode:Landroid/widget/RadioButton;

.field private mRadGrpMode:Landroid/widget/RadioGroup;

.field private mSdPathFromPropert:Ljava/lang/String;

.field private mSelectedMode:I

.field private mServiceFilter:Landroid/content/IntentFilter;

.field private mServiceReceiver:Landroid/content/BroadcastReceiver;

.field private mSharedPrefLogQueryJNI:Landroid/content/SharedPreferences;

.field private mStarted:Z

.field private mStorageManagerHandle:Landroid/os/storage/StorageManager;

.field private mTitleLogSize:Landroid/widget/TextView;

.field private mUIInit:Z

.field private mWaitingSD:Z

.field private mWaitingStopDialog:Landroid/app/ProgressDialog;


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mWaitingStopDialog:Landroid/app/ProgressDialog;

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mAutoStartModeTemp:I

    iput v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mCurrentMode:I

    iput v3, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mAutoStartMode:I

    iput v3, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mSelectedMode:I

    iput-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mSharedPrefLogQueryJNI:Landroid/content/SharedPreferences;

    iput-boolean v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mStarted:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mPaused:Z

    iput-boolean v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mCheckAutoStart:Z

    iput-boolean v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mUIInit:Z

    iput-boolean v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mPolling:Z

    iput-boolean v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mWaitingSD:Z

    iput-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mStorageManagerHandle:Landroid/os/storage/StorageManager;

    iput-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mSdPathFromPropert:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mNotifySize:J

    iput v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mLimitlogSize:I

    new-instance v0, Lcom/mediatek/mdlogger/MDLoggerTest$1;

    invoke-direct {v0, p0}, Lcom/mediatek/mdlogger/MDLoggerTest$1;-><init>(Lcom/mediatek/mdlogger/MDLoggerTest;)V

    iput-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mMessageHandler:Landroid/os/Handler;

    new-instance v0, Lcom/mediatek/mdlogger/MDLoggerTest$9;

    invoke-direct {v0, p0}, Lcom/mediatek/mdlogger/MDLoggerTest$9;-><init>(Lcom/mediatek/mdlogger/MDLoggerTest;)V

    iput-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mServiceReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/mdlogger/MDLoggerTest;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerTest;

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mWaitingStopDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$002(Lcom/mediatek/mdlogger/MDLoggerTest;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerTest;
    .param p1    # Landroid/app/ProgressDialog;

    iput-object p1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mWaitingStopDialog:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic access$100(Lcom/mediatek/mdlogger/MDLoggerTest;)Landroid/widget/RadioButton;
    .locals 1
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerTest;

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mRadBtnUSBMode:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/mediatek/mdlogger/MDLoggerTest;)Z
    .locals 1
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerTest;

    iget-boolean v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mStarted:Z

    return v0
.end method

.method static synthetic access$1002(Lcom/mediatek/mdlogger/MDLoggerTest;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerTest;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mStarted:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/mediatek/mdlogger/MDLoggerTest;)V
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerTest;

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->startLogging()V

    return-void
.end method

.method static synthetic access$1200(Lcom/mediatek/mdlogger/MDLoggerTest;)V
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerTest;

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->resumeLogging()V

    return-void
.end method

.method static synthetic access$1300(Lcom/mediatek/mdlogger/MDLoggerTest;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerTest;

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mBtnStop:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/mediatek/mdlogger/MDLoggerTest;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerTest;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/mdlogger/MDLoggerTest;->pauseLogging(Z)V

    return-void
.end method

.method static synthetic access$1500(Lcom/mediatek/mdlogger/MDLoggerTest;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerTest;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/mdlogger/MDLoggerTest;->checkAutoStart(Z)V

    return-void
.end method

.method static synthetic access$1600(Lcom/mediatek/mdlogger/MDLoggerTest;)V
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerTest;

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->forceModemAssert()V

    return-void
.end method

.method static synthetic access$1700(Lcom/mediatek/mdlogger/MDLoggerTest;)V
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerTest;

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->clearOldLog()V

    return-void
.end method

.method static synthetic access$1800(Lcom/mediatek/mdlogger/MDLoggerTest;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerTest;

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mMessageHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/mediatek/mdlogger/MDLoggerTest;)V
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerTest;

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->updateLoggingStatus()V

    return-void
.end method

.method static synthetic access$200(Lcom/mediatek/mdlogger/MDLoggerTest;)I
    .locals 1
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerTest;

    iget v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mSelectedMode:I

    return v0
.end method

.method static synthetic access$2000(Lcom/mediatek/mdlogger/MDLoggerTest;)V
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerTest;

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->stopLogging()V

    return-void
.end method

.method static synthetic access$202(Lcom/mediatek/mdlogger/MDLoggerTest;I)I
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerTest;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mSelectedMode:I

    return p1
.end method

.method static synthetic access$2102(Lcom/mediatek/mdlogger/MDLoggerTest;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerTest;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mPolling:Z

    return p1
.end method

.method static synthetic access$2200(Lcom/mediatek/mdlogger/MDLoggerTest;)Z
    .locals 1
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerTest;

    iget-boolean v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mPaused:Z

    return v0
.end method

.method static synthetic access$2202(Lcom/mediatek/mdlogger/MDLoggerTest;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerTest;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mPaused:Z

    return p1
.end method

.method static synthetic access$2302(Lcom/mediatek/mdlogger/MDLoggerTest;I)I
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerTest;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mCurrentMode:I

    return p1
.end method

.method static synthetic access$2400(Lcom/mediatek/mdlogger/MDLoggerTest;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerTest;

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mBtnForceAssert:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/mediatek/mdlogger/MDLoggerTest;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerTest;

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mBtnClearLog:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/mediatek/mdlogger/MDLoggerTest;IZ)V
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerTest;
    .param p1    # I
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/mediatek/mdlogger/MDLoggerTest;->setLoggingModeText(IZ)V

    return-void
.end method

.method static synthetic access$2700(Lcom/mediatek/mdlogger/MDLoggerTest;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerTest;

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mSdPathFromPropert:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/mediatek/mdlogger/MDLoggerTest;Ljava/io/File;)Z
    .locals 1
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerTest;
    .param p1    # Ljava/io/File;

    invoke-direct {p0, p1}, Lcom/mediatek/mdlogger/MDLoggerTest;->clearLogs(Ljava/io/File;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2900(Lcom/mediatek/mdlogger/MDLoggerTest;)V
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerTest;

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->sendToastNotifySpaceForCycle()V

    return-void
.end method

.method static synthetic access$300(Lcom/mediatek/mdlogger/MDLoggerTest;)Landroid/widget/RadioButton;
    .locals 1
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerTest;

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mRadBtnSDMode:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/mdlogger/MDLoggerTest;)Z
    .locals 1
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerTest;

    iget-boolean v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mUIInit:Z

    return v0
.end method

.method static synthetic access$500(Lcom/mediatek/mdlogger/MDLoggerTest;)I
    .locals 1
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerTest;

    iget v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mLimitlogSize:I

    return v0
.end method

.method static synthetic access$502(Lcom/mediatek/mdlogger/MDLoggerTest;I)I
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerTest;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mLimitlogSize:I

    return p1
.end method

.method static synthetic access$600(Lcom/mediatek/mdlogger/MDLoggerTest;)Landroid/widget/EditText;
    .locals 1
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerTest;

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mLogSizeLimitEdit:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$700(Lcom/mediatek/mdlogger/MDLoggerTest;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerTest;

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mBtnStart:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$802(Lcom/mediatek/mdlogger/MDLoggerTest;I)I
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerTest;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mAutoStartModeTemp:I

    return p1
.end method

.method static synthetic access$900(Lcom/mediatek/mdlogger/MDLoggerTest;)V
    .locals 0
    .param p0    # Lcom/mediatek/mdlogger/MDLoggerTest;

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->initLogSavePathForSdSwap()V

    return-void
.end method

.method private calculateFolderSize(Ljava/io/File;)J
    .locals 9
    .param p1    # Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v5

    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    if-eqz v2, :cond_0

    move-object v0, v2

    array-length v4, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v1, v0, v3

    invoke-direct {p0, v1}, Lcom/mediatek/mdlogger/MDLoggerTest;->getFileSize(Ljava/io/File;)J

    move-result-wide v7

    add-long/2addr v5, v7

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    return-wide v5
.end method

.method private checkAutoStart(Z)V
    .locals 3
    .param p1    # Z

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    const-string v0, "ModemLog_Activity"

    const-string v1, "Check auto start"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mSelectedMode:I

    invoke-static {v0}, Lcom/mediatek/mdlogger/MDLoggerJNI;->setAutoStartMDLoggingMode(I)I

    iget v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mSelectedMode:I

    iput v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mAutoStartModeTemp:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mCheckAutoStart:Z

    :goto_0
    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->setPrefereceLogQuryForJNI()V

    return-void

    :cond_0
    const-string v0, "ModemLog_Activity"

    const-string v1, "Uncheck auto start"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v2}, Lcom/mediatek/mdlogger/MDLoggerJNI;->setAutoStartMDLoggingMode(I)I

    iput v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mAutoStartModeTemp:I

    iput-boolean v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mCheckAutoStart:Z

    goto :goto_0
.end method

.method private checkSDcardStorage()Z
    .locals 11

    const v10, 0x7f060012

    const/4 v6, 0x0

    const-string v7, "ModemLog_Activity"

    const-string v8, "Check the sd card storage and status"

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v7, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mSdPathFromPropert:Ljava/lang/String;

    if-nez v7, :cond_0

    const-string v7, "ModemLog_Activity"

    const-string v8, "mSdPathFromPropert is null"

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v6

    :cond_0
    const-string v7, "mounted"

    iget-object v8, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mStorageManagerHandle:Landroid/os/storage/StorageManager;

    iget-object v9, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mSdPathFromPropert:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    iget-object v7, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mSdPathFromPropert:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    const-string v8, "sdcard"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_5

    :cond_1
    new-instance v5, Ljava/io/File;

    iget-object v7, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mSdPathFromPropert:Ljava/lang/String;

    invoke-direct {v5, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mSdPathFromPropert:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    const-string v8, "sdcard"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {v5}, Ljava/io/File;->canWrite()Z

    move-result v7

    if-nez v7, :cond_3

    :cond_2
    const-string v7, "ModemLog_Activity"

    const-string v8, "SD Card is not exist or not writable"

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0, v10, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_3
    new-instance v4, Landroid/os/StatFs;

    invoke-virtual {v5}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v7}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v0

    invoke-virtual {v4}, Landroid/os/StatFs;->getBlockSize()I

    move-result v7

    div-int/lit16 v3, v7, 0x400

    mul-int v7, v0, v3

    int-to-long v1, v7

    const-string v7, "ModemLog_Activity"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Path: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mSdPathFromPropert:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ". Available blocks is "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " and block size is "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", and total available "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "K"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v7, 0x283c

    cmp-long v7, v1, v7

    if-gez v7, :cond_4

    const v7, 0x7f060011

    invoke-static {p0, v7, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :cond_4
    const-string v6, "ModemLog_Activity"

    const-string v7, "checkSDcardStorage: the available size is bigger than 10300"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v1, v2}, Lcom/mediatek/mdlogger/MDLoggerTest;->makeTostNoifyUser(J)V

    const/4 v6, 0x1

    goto/16 :goto_0

    :cond_5
    const-string v7, "ModemLog_Activity"

    const-string v8, "Can\'t find SD card"

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0, v10, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0
.end method

.method private clearLogs(Ljava/io/File;)Z
    .locals 8
    .param p1    # Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-nez v5, :cond_0

    const/4 v5, 0x0

    :goto_0
    return v5

    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    move-object v0, v2

    array-length v4, v0

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v4, :cond_3

    aget-object v1, v0, v3

    const-string v5, "ModemLog_Activity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Clear file: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "catcher_filter.bin"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_1
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-direct {p0, v1}, Lcom/mediatek/mdlogger/MDLoggerTest;->clearLogs(Ljava/io/File;)Z

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    goto :goto_2

    :cond_3
    const/4 v5, 0x1

    goto :goto_0
.end method

.method private clearOldLog()V
    .locals 4

    const-string v1, "ModemLog_Activity"

    const-string v2, "Clear old logs in SD card"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f06001f

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f060020

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const-string v2, "OK"

    new-instance v3, Lcom/mediatek/mdlogger/MDLoggerTest$13;

    invoke-direct {v3, p0}, Lcom/mediatek/mdlogger/MDLoggerTest$13;-><init>(Lcom/mediatek/mdlogger/MDLoggerTest;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const-string v2, "Cancel"

    new-instance v3, Lcom/mediatek/mdlogger/MDLoggerTest$12;

    invoke-direct {v3, p0}, Lcom/mediatek/mdlogger/MDLoggerTest$12;-><init>(Lcom/mediatek/mdlogger/MDLoggerTest;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method private findViews()V
    .locals 1

    const/high16 v0, 0x7f070000

    invoke-virtual {p0, v0}, Lcom/mediatek/mdlogger/MDLoggerTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mTitleLogSize:Landroid/widget/TextView;

    const v0, 0x7f070001

    invoke-virtual {p0, v0}, Lcom/mediatek/mdlogger/MDLoggerTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mLogSizeLimitEdit:Landroid/widget/EditText;

    const v0, 0x7f070004

    invoke-virtual {p0, v0}, Lcom/mediatek/mdlogger/MDLoggerTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mRadGrpMode:Landroid/widget/RadioGroup;

    const v0, 0x7f070006

    invoke-virtual {p0, v0}, Lcom/mediatek/mdlogger/MDLoggerTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mRadBtnUSBMode:Landroid/widget/RadioButton;

    const v0, 0x7f070005

    invoke-virtual {p0, v0}, Lcom/mediatek/mdlogger/MDLoggerTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mRadBtnSDMode:Landroid/widget/RadioButton;

    const v0, 0x7f07000a

    invoke-virtual {p0, v0}, Lcom/mediatek/mdlogger/MDLoggerTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mBtnStart:Landroid/widget/Button;

    const v0, 0x7f07000b

    invoke-virtual {p0, v0}, Lcom/mediatek/mdlogger/MDLoggerTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mBtnStop:Landroid/widget/Button;

    const v0, 0x7f070007

    invoke-virtual {p0, v0}, Lcom/mediatek/mdlogger/MDLoggerTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mChkAutoStart:Landroid/widget/CheckBox;

    const v0, 0x7f070009

    invoke-virtual {p0, v0}, Lcom/mediatek/mdlogger/MDLoggerTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mInfoToShow:Landroid/widget/TextView;

    const v0, 0x7f07000f

    invoke-virtual {p0, v0}, Lcom/mediatek/mdlogger/MDLoggerTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mBtnForceAssert:Landroid/widget/Button;

    const v0, 0x7f07000d

    invoke-virtual {p0, v0}, Lcom/mediatek/mdlogger/MDLoggerTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mBtnClearLog:Landroid/widget/Button;

    const v0, 0x7f070008

    invoke-virtual {p0, v0}, Lcom/mediatek/mdlogger/MDLoggerTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mChkEnableMemoryDump:Landroid/widget/CheckBox;

    return-void
.end method

.method private forceModemAssert()V
    .locals 4

    const-string v1, "ModemLog_Activity"

    const-string v2, "Show force modem assert dialog"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f06001f

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f060024

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const-string v2, "OK"

    new-instance v3, Lcom/mediatek/mdlogger/MDLoggerTest$11;

    invoke-direct {v3, p0}, Lcom/mediatek/mdlogger/MDLoggerTest$11;-><init>(Lcom/mediatek/mdlogger/MDLoggerTest;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const-string v2, "Cancel"

    new-instance v3, Lcom/mediatek/mdlogger/MDLoggerTest$10;

    invoke-direct {v3, p0}, Lcom/mediatek/mdlogger/MDLoggerTest$10;-><init>(Lcom/mediatek/mdlogger/MDLoggerTest;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method private getCustSizeFromProperty()I
    .locals 9

    const/16 v0, 0x3e8

    new-instance v5, Ljava/util/Properties;

    invoke-direct {v5}, Ljava/util/Properties;-><init>()V

    const/4 v2, 0x0

    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    const-string v6, "/system/etc/mtklog-config.prop"

    invoke-direct {v3, v6}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-virtual {v5, v3}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    const-string v6, "com.mediatek.log.modem.maxsize"

    invoke-virtual {v5, v6}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    const/16 v0, 0x3e8

    :try_start_3
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_3

    move-result v0

    :goto_0
    const-string v6, "ModemLog_Activity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Custmize value is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "M."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v6, 0x258

    if-ge v0, v6, :cond_0

    const/16 v0, 0x258

    const-string v6, "ModemLog_Activity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Min size is 600M, but custmize value is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "M "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move-object v2, v3

    :goto_1
    return v0

    :catch_0
    move-exception v1

    const-string v6, "ModemLog_Activity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "File Not Found Exception "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :catch_1
    move-exception v1

    const-string v6, "ModemLog_Activity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "IOException load\ufffd\ufffd "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v3

    goto :goto_1

    :catch_2
    move-exception v1

    const-string v6, "ModemLog_Activity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "IOException close "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v3

    goto :goto_1

    :catch_3
    move-exception v1

    const/16 v0, 0x3e8

    goto/16 :goto_0
.end method

.method private getFileSize(Ljava/io/File;)J
    .locals 12
    .param p1    # Ljava/io/File;

    const/4 v11, 0x0

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v7

    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v9

    if-eqz v9, :cond_2

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_2

    invoke-virtual {v4, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    if-eqz v2, :cond_1

    move-object v0, v2

    array-length v6, v0

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v6, :cond_1

    aget-object v1, v0, v5

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v9

    add-long/2addr v7, v9

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {v4, v11}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    return-wide v7
.end method

.method private getMDLoggerInfo()V
    .locals 5

    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    const-string v2, "ModemLog_Activity"

    const-string v3, "<<<<Get initial MDLogger information>>>>"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mSharedPrefLogQueryJNI:Landroid/content/SharedPreferences;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mSharedPrefLogQueryJNI:Landroid/content/SharedPreferences;

    const-string v3, "PREF_CURRENT_MODE"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mCurrentMode:I

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mSharedPrefLogQueryJNI:Landroid/content/SharedPreferences;

    const-string v3, "PREF_AUTO_START"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mAutoStartMode:I

    :goto_0
    iget v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mCurrentMode:I

    if-eq v2, v4, :cond_0

    iget v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mCurrentMode:I

    if-ne v2, v0, :cond_4

    :cond_0
    iput-boolean v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mStarted:Z

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mSharedPrefLogQueryJNI:Landroid/content/SharedPreferences;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mSharedPrefLogQueryJNI:Landroid/content/SharedPreferences;

    const-string v3, "PREF_LOG_PAUSED"

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mPaused:Z

    :cond_1
    :goto_1
    iget v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mAutoStartMode:I

    if-eq v2, v4, :cond_2

    iget v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mAutoStartMode:I

    if-ne v2, v0, :cond_6

    :cond_2
    iput-boolean v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mCheckAutoStart:Z

    :goto_2
    const-string v2, "ModemLog_Activity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "---->Current Mode is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mCurrentMode:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "ModemLog_Activity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "---->Running status is: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mPaused:Z

    if-nez v4, :cond_7

    :goto_3
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "ModemLog_Activity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "---->Auto start mode is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mAutoStartMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_3
    const-string v2, "ModemLog_Activity"

    const-string v3, "Log state sharedPrefLogState is null"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "PREF_LOG_QUERY_TABLE"

    invoke-virtual {p0, v2, v1}, Lcom/mediatek/mdlogger/MDLoggerTest;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mSharedPrefLogQueryJNI:Landroid/content/SharedPreferences;

    goto/16 :goto_0

    :cond_4
    iget v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mCurrentMode:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_5

    iput-boolean v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mPolling:Z

    iput-boolean v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mStarted:Z

    iput-boolean v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mPaused:Z

    goto :goto_1

    :cond_5
    iput-boolean v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mStarted:Z

    iput-boolean v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mPaused:Z

    goto :goto_1

    :cond_6
    iput-boolean v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mCheckAutoStart:Z

    goto :goto_2

    :cond_7
    move v0, v1

    goto :goto_3
.end method

.method private getSdSwapPathOfService()V
    .locals 3

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mStorageManagerHandle:Landroid/os/storage/StorageManager;

    if-nez v0, :cond_0

    const-string v0, "storage"

    invoke-virtual {p0, v0}, Lcom/mediatek/mdlogger/MDLoggerTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/storage/StorageManager;

    iput-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mStorageManagerHandle:Landroid/os/storage/StorageManager;

    :cond_0
    const-string v0, "debug.mdl.sdswap.path"

    const-string v1, "/mnt/sdcard"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mSdPathFromPropert:Ljava/lang/String;

    const-string v0, "ModemLog_Activity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SDcard path from debug.mdl.sdswap.path is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mSdPathFromPropert:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private informService()V
    .locals 4

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.mediatek.mdlogger.MDLoggerService"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "RunStatus"

    iget-boolean v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mPaused:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/mediatek/mdlogger/MDLoggerTest;->sendBroadcast(Landroid/content/Intent;)V

    const-string v1, "ModemLog_Activity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Inform Service: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private initLogCustmizeSize()I
    .locals 4

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->getCustSizeFromProperty()I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mSharedPrefLogQueryJNI:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "PREF_LOG_SIZE_CUST_INIT"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v2, "PREF_LOG_SIZE_CUST_SIZE"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "ModemLog_Activity"

    const-string v3, "Set init customize size failed."

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return v1
.end method

.method private initLogSavePathForSdSwap()V
    .locals 4

    const-string v1, "debug.log2sd.defaultpath"

    const-string v2, "/mnt/sdcard"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mSdPathFromPropert:Ljava/lang/String;

    const-string v1, "ModemLog_Activity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SDcard path from debug.log2sd.defaultpath is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mSdPathFromPropert:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mStorageManagerHandle:Landroid/os/storage/StorageManager;

    if-nez v1, :cond_0

    const-string v1, "storage"

    invoke-virtual {p0, v1}, Lcom/mediatek/mdlogger/MDLoggerTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/storage/StorageManager;

    iput-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mStorageManagerHandle:Landroid/os/storage/StorageManager;

    :cond_0
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mSdPathFromPropert:Ljava/lang/String;

    const-string v2, "/mnt/sdcard2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mStorageManagerHandle:Landroid/os/storage/StorageManager;

    invoke-virtual {v1}, Landroid/os/storage/StorageManager;->getExternalStoragePath()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ModemLog_Activity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "get sd path from extternal path:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    if-eqz v0, :cond_2

    iput-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mSdPathFromPropert:Ljava/lang/String;

    :cond_2
    return-void

    :cond_3
    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mSdPathFromPropert:Ljava/lang/String;

    const-string v2, "/mnt/sdcard"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mStorageManagerHandle:Landroid/os/storage/StorageManager;

    invoke-virtual {v1}, Landroid/os/storage/StorageManager;->getInternalStoragePath()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ModemLog_Activity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "get sd path from internal path:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mStorageManagerHandle:Landroid/os/storage/StorageManager;

    invoke-virtual {v1}, Landroid/os/storage/StorageManager;->getExternalStoragePath()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ModemLog_Activity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "get sd path is null from internal path: may be not emmc"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private initUI()V
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    const-string v3, "ModemLog_Activity"

    const-string v4, "Init UI"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v3, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mStarted:Z

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mPaused:Z

    if-eqz v3, :cond_6

    :cond_0
    iget-object v3, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mBtnStart:Landroid/widget/Button;

    invoke-virtual {v3, v2}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v3, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mBtnStop:Landroid/widget/Button;

    invoke-virtual {v3, v1}, Landroid/widget/Button;->setEnabled(Z)V

    :goto_0
    iget-boolean v3, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mPolling:Z

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mBtnStart:Landroid/widget/Button;

    invoke-virtual {v3, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v3, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mBtnStop:Landroid/widget/Button;

    invoke-virtual {v3, v1}, Landroid/widget/Button;->setEnabled(Z)V

    :cond_1
    iget-boolean v3, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mWaitingSD:Z

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mBtnStart:Landroid/widget/Button;

    invoke-virtual {v3, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v3, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mBtnStop:Landroid/widget/Button;

    invoke-virtual {v3, v2}, Landroid/widget/Button;->setEnabled(Z)V

    :cond_2
    iget v3, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mCurrentMode:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mBtnStart:Landroid/widget/Button;

    invoke-virtual {v3, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v3, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mBtnStop:Landroid/widget/Button;

    invoke-virtual {v3, v1}, Landroid/widget/Button;->setEnabled(Z)V

    :cond_3
    iget-boolean v3, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mStarted:Z

    if-eqz v3, :cond_4

    iget v3, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mCurrentMode:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_7

    iget-object v3, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mRadBtnUSBMode:Landroid/widget/RadioButton;

    invoke-virtual {v3, v1}, Landroid/widget/RadioButton;->setEnabled(Z)V

    iget-object v3, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mRadBtnSDMode:Landroid/widget/RadioButton;

    invoke-virtual {v3, v2}, Landroid/widget/RadioButton;->setEnabled(Z)V

    iget-object v3, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mRadGrpMode:Landroid/widget/RadioGroup;

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mRadBtnSDMode:Landroid/widget/RadioButton;

    invoke-virtual {v4}, Landroid/widget/RadioButton;->getId()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/RadioGroup;->check(I)V

    :cond_4
    :goto_1
    iget-object v3, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mChkAutoStart:Landroid/widget/CheckBox;

    iget-boolean v4, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mCheckAutoStart:Z

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v3, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mChkAutoStart:Landroid/widget/CheckBox;

    iget-boolean v4, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mPaused:Z

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setEnabled(Z)V

    iget-object v3, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mBtnClearLog:Landroid/widget/Button;

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mBtnStart:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->isEnabled()Z

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v3, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mChkEnableMemoryDump:Landroid/widget/CheckBox;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setVisibility(I)V

    iget-object v3, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mBtnForceAssert:Landroid/widget/Button;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v3, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mBtnForceAssert:Landroid/widget/Button;

    iget-boolean v4, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mPaused:Z

    if-nez v4, :cond_5

    move v1, v2

    :cond_5
    invoke-virtual {v3, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mLogSizeLimitEdit:Landroid/widget/EditText;

    iget-boolean v3, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mPaused:Z

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mLogSizeLimitEdit:Landroid/widget/EditText;

    iget v3, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mLimitlogSize:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f060028

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mTitleLogSize:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iput-boolean v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mUIInit:Z

    return-void

    :cond_6
    iget-object v3, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mBtnStart:Landroid/widget/Button;

    invoke-virtual {v3, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v3, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mBtnStop:Landroid/widget/Button;

    invoke-virtual {v3, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto/16 :goto_0

    :cond_7
    iget v3, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mCurrentMode:I

    if-ne v3, v2, :cond_8

    iget-object v3, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mRadBtnUSBMode:Landroid/widget/RadioButton;

    invoke-virtual {v3, v2}, Landroid/widget/RadioButton;->setEnabled(Z)V

    iget-object v3, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mRadBtnSDMode:Landroid/widget/RadioButton;

    invoke-virtual {v3, v1}, Landroid/widget/RadioButton;->setEnabled(Z)V

    iget-object v3, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mRadGrpMode:Landroid/widget/RadioGroup;

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mRadBtnUSBMode:Landroid/widget/RadioButton;

    invoke-virtual {v4}, Landroid/widget/RadioButton;->getId()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_1

    :cond_8
    iget v3, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mCurrentMode:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_4

    iget-object v3, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mRadBtnUSBMode:Landroid/widget/RadioButton;

    invoke-virtual {v3, v2}, Landroid/widget/RadioButton;->setEnabled(Z)V

    iget-object v3, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mRadBtnSDMode:Landroid/widget/RadioButton;

    invoke-virtual {v3, v2}, Landroid/widget/RadioButton;->setEnabled(Z)V

    iget-object v3, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mRadGrpMode:Landroid/widget/RadioGroup;

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mRadBtnSDMode:Landroid/widget/RadioButton;

    invoke-virtual {v4}, Landroid/widget/RadioButton;->getId()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/RadioGroup;->check(I)V

    goto/16 :goto_1
.end method

.method private makeTostNoifyUser(J)V
    .locals 4
    .param p1    # J

    const/4 v1, 0x0

    iget v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mPositionMode:I

    if-nez v2, :cond_1

    const/16 v1, 0x258

    :goto_0
    mul-int/lit16 v2, v1, 0x400

    int-to-long v2, v2

    cmp-long v2, p1, v2

    if-ltz v2, :cond_2

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mPositionMode:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    const/16 v1, 0x3e8

    goto :goto_0

    :cond_2
    mul-int/lit16 v2, v1, 0x400

    int-to-long v2, v2

    sub-long/2addr v2, p1

    iput-wide v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mNotifySize:J

    new-instance v0, Ljava/lang/Thread;

    new-instance v2, Lcom/mediatek/mdlogger/MDLoggerTest$14;

    invoke-direct {v2, p0}, Lcom/mediatek/mdlogger/MDLoggerTest$14;-><init>(Lcom/mediatek/mdlogger/MDLoggerTest;)V

    invoke-direct {v0, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_1
.end method

.method private mapViewListeners()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mRadGrpMode:Landroid/widget/RadioGroup;

    new-instance v1, Lcom/mediatek/mdlogger/MDLoggerTest$2;

    invoke-direct {v1, p0}, Lcom/mediatek/mdlogger/MDLoggerTest$2;-><init>(Lcom/mediatek/mdlogger/MDLoggerTest;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mBtnStart:Landroid/widget/Button;

    new-instance v1, Lcom/mediatek/mdlogger/MDLoggerTest$3;

    invoke-direct {v1, p0}, Lcom/mediatek/mdlogger/MDLoggerTest$3;-><init>(Lcom/mediatek/mdlogger/MDLoggerTest;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mBtnStop:Landroid/widget/Button;

    new-instance v1, Lcom/mediatek/mdlogger/MDLoggerTest$4;

    invoke-direct {v1, p0}, Lcom/mediatek/mdlogger/MDLoggerTest$4;-><init>(Lcom/mediatek/mdlogger/MDLoggerTest;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mChkAutoStart:Landroid/widget/CheckBox;

    new-instance v1, Lcom/mediatek/mdlogger/MDLoggerTest$5;

    invoke-direct {v1, p0}, Lcom/mediatek/mdlogger/MDLoggerTest$5;-><init>(Lcom/mediatek/mdlogger/MDLoggerTest;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mBtnForceAssert:Landroid/widget/Button;

    new-instance v1, Lcom/mediatek/mdlogger/MDLoggerTest$6;

    invoke-direct {v1, p0}, Lcom/mediatek/mdlogger/MDLoggerTest$6;-><init>(Lcom/mediatek/mdlogger/MDLoggerTest;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mBtnClearLog:Landroid/widget/Button;

    new-instance v1, Lcom/mediatek/mdlogger/MDLoggerTest$7;

    invoke-direct {v1, p0}, Lcom/mediatek/mdlogger/MDLoggerTest$7;-><init>(Lcom/mediatek/mdlogger/MDLoggerTest;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private pauseLogging(Z)V
    .locals 3
    .param p1    # Z

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mStarted:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mPaused:Z

    if-nez v0, :cond_2

    const-string v0, "ModemLog_Activity"

    const-string v1, "Pause logging from activity"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "ModemLog_Activity"

    const-string v1, "Inform MDLogger to pause logging"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mediatek/mdlogger/MDLoggerJNI;->pauseMDLogging()I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mPaused:Z

    iput-boolean v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mCheckAutoStart:Z

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->setPrefereceLogQuryForJNI()V

    const-string v0, "ModemLog_Activity"

    const-string v1, "Inform ModemLog Service to pause logging"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->informService()V

    invoke-virtual {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->updateUI()V

    if-eqz p1, :cond_1

    const-string v0, "ModemLog_Activity"

    const-string v1, "Show waiting dialog to pause"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mWaitingStopDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mWaitingStopDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    :cond_0
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mWaitingStopDialog:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mWaitingStopDialog:Landroid/app/ProgressDialog;

    const v1, 0x7f06000f

    invoke-virtual {p0, v1}, Lcom/mediatek/mdlogger/MDLoggerTest;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mWaitingStopDialog:Landroid/app/ProgressDialog;

    const v1, 0x7f060010

    invoke-virtual {p0, v1}, Lcom/mediatek/mdlogger/MDLoggerTest;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mWaitingStopDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mWaitingStopDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mWaitingStopDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    new-instance v0, Lcom/mediatek/mdlogger/MDLoggerTest$8;

    invoke-direct {v0, p0}, Lcom/mediatek/mdlogger/MDLoggerTest$8;-><init>(Lcom/mediatek/mdlogger/MDLoggerTest;)V

    invoke-virtual {v0}, Lcom/mediatek/mdlogger/MDLoggerTest$8;->start()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-boolean v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mWaitingSD:Z

    if-eqz v0, :cond_1

    const-string v0, "ModemLog_Activity"

    const-string v1, "Stop waiting SD card ready from activity"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mediatek/mdlogger/MDLoggerJNI;->stopWaitingSD()Z

    iput-boolean v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mWaitingSD:Z

    iput v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mCurrentMode:I

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->setPrefereceLogQuryForJNI()V

    invoke-virtual {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->updateUI()V

    goto :goto_0
.end method

.method private registerServiceListener()V
    .locals 2

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mServiceFilter:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mServiceFilter:Landroid/content/IntentFilter;

    const-string v1, "com.mediatek.mdlogger.MDLoggerTest_PAUSE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mServiceFilter:Landroid/content/IntentFilter;

    const-string v1, "com.mediatek.mdlogger.MDLoggerTest_RESUME"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mServiceFilter:Landroid/content/IntentFilter;

    const-string v1, "com.mediatek.mdlogger.MDLoggerTest_STOP"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mServiceFilter:Landroid/content/IntentFilter;

    const-string v1, "com.mediatek.mdlogger.MDLoggerTest_STARTPOLLING"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mServiceFilter:Landroid/content/IntentFilter;

    const-string v1, "com.mediatek.mdlogger.MDLoggerTest_STOPPOLLING"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mServiceReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mServiceFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/mediatek/mdlogger/MDLoggerTest;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const-string v0, "ModemLog_Activity"

    const-string v1, "Register ModemLog Service listener"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private resumeLogging()V
    .locals 3

    const/4 v2, 0x1

    iget-boolean v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mPaused:Z

    if-eqz v0, :cond_0

    const-string v0, "ModemLog_Activity"

    const-string v1, "Resume logging from activity"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mCurrentMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->checkSDcardStorage()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "ModemLog_Activity"

    const-string v1, "Failed to resume logging in SD Mode, SD card is full"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mBtnStart:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-boolean v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mCheckAutoStart:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mPaused:Z

    const-string v0, "ModemLog_Activity"

    const-string v1, "Inform ModemLog Service to resume logging"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->setPrefereceLogQuryForJNI()V

    const-string v0, "ModemLog_Activity"

    const-string v1, "Inform MDLogger to resume logging"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mediatek/mdlogger/MDLoggerJNI;->resumeMDLogging()I

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->startService()V

    invoke-virtual {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->updateUI()V

    goto :goto_0
.end method

.method private sendToastNotifySpaceForCycle()V
    .locals 6

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mSdPathFromPropert:Ljava/lang/String;

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mSdPathFromPropert:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/mtklog/mdlog/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v3}, Lcom/mediatek/mdlogger/MDLoggerTest;->calculateFolderSize(Ljava/io/File;)J

    move-result-wide v0

    const/4 v3, 0x0

    iget-wide v4, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mNotifySize:J

    cmp-long v4, v0, v4

    if-gez v4, :cond_0

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mMessageHandler:Landroid/os/Handler;

    const/16 v5, 0x3eb

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method private setLoggingModeText(IZ)V
    .locals 5
    .param p1    # I
    .param p2    # Z

    const v4, 0x7f06000c

    const/4 v1, 0x0

    packed-switch p1, :pswitch_data_0

    invoke-virtual {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060008

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/high16 v0, -0x10000

    :goto_0
    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mInfoToShow:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mInfoToShow:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060009

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/high16 v0, -0x10000

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f06000b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    if-eqz p2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const v0, -0x8100

    goto :goto_0

    :cond_0
    const v0, -0xff0100

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f06000a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    if-eqz p2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const v0, -0x8100

    goto :goto_0

    :cond_1
    const v0, -0xff0100

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f06001a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v0, -0xff0100

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private setPrefereceLogQuryForJNI()V
    .locals 4

    const/4 v3, -0x1

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mSharedPrefLogQueryJNI:Landroid/content/SharedPreferences;

    if-nez v1, :cond_0

    const-string v1, "PREF_LOG_QUERY_TABLE"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/mediatek/mdlogger/MDLoggerTest;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mSharedPrefLogQueryJNI:Landroid/content/SharedPreferences;

    :cond_0
    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mSharedPrefLogQueryJNI:Landroid/content/SharedPreferences;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mSharedPrefLogQueryJNI:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "PREF_CURRENT_MODE"

    iget v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mCurrentMode:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v1, "PREF_LOG_PAUSED"

    iget-boolean v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mPaused:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const-string v1, "PREF_LOG_SIZE_CUST_SIZE"

    iget v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mLimitlogSize:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    iget v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mAutoStartModeTemp:I

    if-eq v1, v3, :cond_1

    const-string v1, "PREF_AUTO_START"

    iget v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mAutoStartModeTemp:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    iput v3, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mAutoStartModeTemp:I

    :cond_1
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "ModemLog_Activity"

    const-string v2, "Set sharedPrefLogQueryJNI is "

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_2
    const-string v1, "ModemLog_Activity"

    const-string v2, "Set sharedPrefLogQueryJNI failed."

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    const-string v1, "ModemLog_Activity"

    const-string v2, "sharedPrefLogQueryJNI is null "

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private startLogging()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    const-string v0, "ModemLog_Activity"

    const-string v1, "Sart logging from activity"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mSelectedMode:I

    if-ne v0, v4, :cond_0

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->checkSDcardStorage()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ModemLog_Activity"

    const-string v1, "Failed to start logging in SD Mode, SD card is full"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mBtnStart:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    :goto_0
    return-void

    :cond_0
    iput-boolean v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mStarted:Z

    iget v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mSelectedMode:I

    iput v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mCurrentMode:I

    iput-boolean v3, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mPaused:Z

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->setPrefereceLogQuryForJNI()V

    iget v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mCurrentMode:I

    if-ne v0, v4, :cond_2

    const-string v0, "ModemLog_Activity"

    const-string v1, "Start logging in SD mode"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mRadBtnUSBMode:Landroid/widget/RadioButton;

    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mRadBtnSDMode:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setEnabled(Z)V

    :cond_1
    :goto_1
    iput-boolean v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mCheckAutoStart:Z

    invoke-virtual {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->updateUI()V

    const-string v0, "ModemLog_Activity"

    const-string v1, "Inform MDLogger to start logging"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mCurrentMode:I

    invoke-static {v0}, Lcom/mediatek/mdlogger/MDLoggerJNI;->startMDLogging(I)I

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->startService()V

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mCurrentMode:I

    if-ne v0, v2, :cond_1

    const-string v0, "ModemLog_Activity"

    const-string v1, "Start logging in USB mode"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mRadBtnUSBMode:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mRadBtnSDMode:Landroid/widget/RadioButton;

    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setEnabled(Z)V

    goto :goto_1
.end method

.method private startService()V
    .locals 4

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.mediatek.mdlogger.MDLoggerService"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "ActivityStartService"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "LoggingMode"

    iget v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mCurrentMode:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "RunStatus"

    iget-boolean v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mPaused:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    const-string v1, "ModemLog_Activity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Start ModemLog service: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private stopLogging()V
    .locals 2

    iget v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mCurrentMode:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mCurrentMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    :cond_0
    const-string v0, "ModemLog_Activity"

    const-string v1, "Stop logging"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->finish()V

    :cond_1
    return-void
.end method

.method private updateLoggingStatus()V
    .locals 5

    const/4 v1, 0x0

    const/4 v0, 0x1

    const-string v2, "ModemLog_Activity"

    const-string v3, "Update logging status due to service operation"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mCurrentMode:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    iget v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mCurrentMode:I

    if-ne v2, v0, :cond_1

    :cond_0
    iget-boolean v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mStarted:Z

    if-nez v2, :cond_3

    :cond_1
    const-string v2, "ModemLog_Activity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Update Logging Status: Wrong, current mdoe is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mCurrentMode:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "and bStarted is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mStarted:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " and running status is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mPaused:Z

    if-nez v4, :cond_2

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    iget-boolean v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mPaused:Z

    if-eqz v2, :cond_4

    const-string v2, "ModemLog_Activity"

    const-string v3, "Resume logging from service"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mCheckAutoStart:Z

    iput-boolean v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mPaused:Z

    :goto_2
    invoke-virtual {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->updateUI()V

    goto :goto_1

    :cond_4
    const-string v1, "ModemLog_Activity"

    const-string v2, "Pause logging from service"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mPaused:Z

    goto :goto_2
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/4 v3, 0x0

    const-string v0, "ModemLog_Activity"

    const-string v1, "ModemLog activity onCreate"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030001

    invoke-virtual {p0, v0}, Lcom/mediatek/mdlogger/MDLoggerTest;->setContentView(I)V

    const-string v0, "PREF_LOG_QUERY_TABLE"

    invoke-virtual {p0, v0, v3}, Lcom/mediatek/mdlogger/MDLoggerTest;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mSharedPrefLogQueryJNI:Landroid/content/SharedPreferences;

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mSharedPrefLogQueryJNI:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    const-string v0, "ModemLog_Activity"

    const-string v1, "sharedPrefLogQueryJNI is null "

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->findViews()V

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->mapViewListeners()V

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->getSdSwapPathOfService()V

    const-string v0, "ModemLog_Activity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SD Card from property: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mSdPathFromPropert:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mSharedPrefLogQueryJNI:Landroid/content/SharedPreferences;

    const-string v1, "PREF_LOG_SIZE_CUST_INIT"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->initLogCustmizeSize()I

    move-result v0

    iput v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mLimitlogSize:I

    const-string v0, "ModemLog_Activity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Init Customization Size done:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mLimitlogSize:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "M"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mSharedPrefLogQueryJNI:Landroid/content/SharedPreferences;

    const-string v1, "PREF_LOG_SIZE_CUST_SIZE"

    const/16 v2, 0x3e8

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mLimitlogSize:I

    const-string v0, "ModemLog_Activity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Limit log size is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mLimitlogSize:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "M."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->getMDLoggerInfo()V

    iget v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mCurrentMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->checkSDcardStorage()Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mPaused:Z

    if-nez v0, :cond_2

    const-string v0, "ModemLog_Activity"

    const-string v1, "Running in SD mode, but SD card is not available, so stop logging from activity"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v3}, Lcom/mediatek/mdlogger/MDLoggerTest;->pauseLogging(Z)V

    :cond_2
    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->initUI()V

    iget v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mCurrentMode:I

    iget-boolean v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mPaused:Z

    invoke-direct {p0, v0, v1}, Lcom/mediatek/mdlogger/MDLoggerTest;->setLoggingModeText(IZ)V

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->registerServiceListener()V

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    const-string v0, "ModemLog_Activity"

    const-string v1, "ModemLog activity onDestroy"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mServiceReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/mediatek/mdlogger/MDLoggerTest;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mWaitingStopDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mWaitingStopDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mWaitingStopDialog:Landroid/app/ProgressDialog;

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method protected onResume()V
    .locals 2

    const-string v0, "ModemLog_Activity"

    const-string v1, "ModemLog activity onResume"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    invoke-direct {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->getMDLoggerInfo()V

    invoke-virtual {p0}, Lcom/mediatek/mdlogger/MDLoggerTest;->updateUI()V

    return-void
.end method

.method public updateUI()V
    .locals 5

    const/4 v2, 0x1

    const/4 v3, 0x0

    const-string v1, "ModemLog_Activity"

    const-string v4, "Update UI"

    invoke-static {v1, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mBtnStart:Landroid/widget/Button;

    iget-boolean v4, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mPaused:Z

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v4, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mBtnStop:Landroid/widget/Button;

    iget-boolean v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mPaused:Z

    if-nez v1, :cond_1

    move v1, v2

    :goto_0
    invoke-virtual {v4, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mBtnClearLog:Landroid/widget/Button;

    iget-boolean v4, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mPaused:Z

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mChkAutoStart:Landroid/widget/CheckBox;

    iget-boolean v4, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mPaused:Z

    invoke-virtual {v1, v4}, Landroid/widget/CheckBox;->setEnabled(Z)V

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mChkAutoStart:Landroid/widget/CheckBox;

    iget-boolean v4, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mCheckAutoStart:Z

    invoke-virtual {v1, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mCurrentMode:I

    iget-boolean v4, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mPaused:Z

    invoke-direct {p0, v1, v4}, Lcom/mediatek/mdlogger/MDLoggerTest;->setLoggingModeText(IZ)V

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mBtnForceAssert:Landroid/widget/Button;

    iget-boolean v4, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mPaused:Z

    if-nez v4, :cond_2

    :goto_1
    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mLogSizeLimitEdit:Landroid/widget/EditText;

    iget-boolean v2, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mPaused:Z

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setEnabled(Z)V

    const-string v1, "ro.monkey"

    const-string v2, "false"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "ModemLog_Activity"

    const-string v2, "monkey is running"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mBtnStart:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mBtnStop:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mBtnClearLog:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mChkAutoStart:Landroid/widget/CheckBox;

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setEnabled(Z)V

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mBtnForceAssert:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v1, p0, Lcom/mediatek/mdlogger/MDLoggerTest;->mLogSizeLimitEdit:Landroid/widget/EditText;

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setEnabled(Z)V

    :cond_0
    return-void

    :cond_1
    move v1, v3

    goto :goto_0

    :cond_2
    move v2, v3

    goto :goto_1
.end method
