.class public final Lcom/mediatek/mdlogger/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/mdlogger/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final ChooseModeMessage:I = 0x7f060007

.field public static final ChooseModeTitle:I = 0x7f060006

.field public static final SDCloseToFull:I = 0x7f060011

.field public static final SDNotAvailable:I = 0x7f060012

.field public static final StopDialogContent:I = 0x7f060010

.field public static final StopDialogTitle:I = 0x7f06000f

.field public static final app_name:I = 0x7f060002

.field public static final btn_clearlogs:I = 0x7f06001e

.field public static final btn_force_assert:I = 0x7f060019

.field public static final btn_start:I = 0x7f060000

.field public static final btn_stop:I = 0x7f060001

.field public static final btn_test_polling_mode:I = 0x7f06000d

.field public static final chk_autostart:I = 0x7f060005

.field public static final clear_dlg_content:I = 0x7f060022

.field public static final clear_dlg_title:I = 0x7f060021

.field public static final dlg_tip_force_assert:I = 0x7f060024

.field public static final dlg_title_force_assert:I = 0x7f060025

.field public static final invalid_log_size:I = 0x7f06002b

.field public static final log_default_size:I = 0x7f06002a

.field public static final mdlog_enable_memorydump:I = 0x7f060023

.field public static final mdlog_service_label:I = 0x7f060016

.field public static final mdlog_service_nosd:I = 0x7f060015

.field public static final mdlog_service_start:I = 0x7f060013

.field public static final mdlog_service_stopped:I = 0x7f060014

.field public static final memorydump_done:I = 0x7f060018

.field public static final message_deletelog:I = 0x7f060020

.field public static final message_nofilter:I = 0x7f060026

.field public static final message_nosdcard:I = 0x7f060027

.field public static final mode_SD:I = 0x7f060004

.field public static final mode_USB:I = 0x7f060003

.field public static final sdcard_full:I = 0x7f06001c

.field public static final set_log_size_summary:I = 0x7f060029

.field public static final set_max_log_size:I = 0x7f060028

.field public static final title_confirm:I = 0x7f06001f

.field public static final tst_sd_unavailable:I = 0x7f06000e

.field public static final txt_current_mode_IDLE:I = 0x7f060009

.field public static final txt_current_mode_Polling:I = 0x7f06001a

.field public static final txt_current_mode_SD:I = 0x7f06000a

.field public static final txt_current_mode_UNKNOWN:I = 0x7f060008

.field public static final txt_current_mode_USB:I = 0x7f06000b

.field public static final txt_current_mode_WaitingSD:I = 0x7f06001b

.field public static final txt_paused:I = 0x7f06000c

.field public static final version:I = 0x7f060017

.field public static final warning:I = 0x7f06001d


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
