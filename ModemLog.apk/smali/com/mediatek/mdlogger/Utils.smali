.class public Lcom/mediatek/mdlogger/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# static fields
.field public static final ACTION_ACTIVITY_PAUSE_LOGGING:Ljava/lang/String; = "com.mediatek.mdlogger.MDLoggerTest_PAUSE"

.field public static final ACTION_ACTIVITY_RESUME_LOGGING:Ljava/lang/String; = "com.mediatek.mdlogger.MDLoggerTest_RESUME"

.field public static final ACTION_ACTIVITY_START_POLLING:Ljava/lang/String; = "com.mediatek.mdlogger.MDLoggerTest_STARTPOLLING"

.field public static final ACTION_ACTIVITY_STOP_LOGGING:Ljava/lang/String; = "com.mediatek.mdlogger.MDLoggerTest_STOP"

.field public static final ACTION_ACTIVITY_STOP_POLLING:Ljava/lang/String; = "com.mediatek.mdlogger.MDLoggerTest_STOPPOLLING"

.field public static final ACTION_ADB_AUTO_TEST_START_MDLOGGER:Ljava/lang/String; = "com.mediatek.mdlogger.AUTO_TEST_START_MDLOGGER"

.field public static final ACTION_ADB_DISABLE_AUTOSTART:Ljava/lang/String; = "com.mediatek.mdlogger.DISABLE_AUTOSTART"

.field public static final ACTION_ADB_DISABLE_MEMORYDUMP:Ljava/lang/String; = "com.mediatek.mdlogger.DISABLE_MEMORYDUMP"

.field public static final ACTION_ADB_ENABLE_AUTOSTART:Ljava/lang/String; = "com.mediatek.mdlogger.ENABLE_AUTOSTART"

.field public static final ACTION_ADB_ENABLE_MEMORYDUMP:Ljava/lang/String; = "com.mediatek.mdlogger.ENABLE_MEMORYDUMP"

.field public static final ACTION_ADB_FORCE_MODEM_ASSERT:Ljava/lang/String; = "com.mediatek.mdlogger.FORCE_MODEM_ASSERT"

.field public static final ACTION_ADB_PAUSE_LOGGING:Ljava/lang/String; = "com.mediatek.mdlogger.PAUSE"

.field public static final ACTION_ADB_RESUME_LOGGING:Ljava/lang/String; = "com.mediatek.mdlogger.RESUME"

.field public static final ACTION_ADB_STOP_LOGGING:Ljava/lang/String; = "com.mediatek.mdlogger.STOP"

.field public static final ACTION_EM_SYSTEM_LOGGER:Ljava/lang/String; = "com.mediatek.syslogger.action"

.field public static final ACTION_IPO_BOOT_UP:Ljava/lang/String; = "android.intent.action.ACTION_BOOT_IPO"

.field public static final ACTION_IPO_SHUTDOWN:Ljava/lang/String; = "android.intent.action.ACTION_SHUTDOWN_IPO"

.field public static final ACTION_MDLOGGER_AUTO_START:Ljava/lang/String; = "com.mediatek.mdlogger.AUTOSTART_COMPLETE"

.field public static final ACTION_MDLOGGER_FAIL_SEND_FILTER:Ljava/lang/String; = "com.mediatek.mdlogger.FAIL_SENDFILTER"

.field public static final ACTION_MDLOGGER_FAIL_WRITE_FILE:Ljava/lang/String; = "com.mediatek.mdlogger.FAIL_WRITEFILE"

.field public static final ACTION_MDLOGGER_FINISH_MEMORY_DUMP:Ljava/lang/String; = "com.mediatek.mdlogger.MEMORYDUMP_DONE"

.field public static final ACTION_MDLOGGER_FULL_SDCARD:Ljava/lang/String; = "com.mediatek.mdlogger.SDCARD_FULL"

.field public static final ACTION_MDLOGGER_NO_LOGGING_FILE:Ljava/lang/String; = "com.mediatek.mdlogger.LOGFILE_NOTEXIST"

.field public static final ACTION_MDLOGGER_NO_SDCARD:Ljava/lang/String; = "com.mediatek.mdlogger.SDCARD_NOTEXIST"

.field public static final ACTION_MDLOGGER_START_MEMORY_DUMP:Ljava/lang/String; = "com.mediatek.mdlogger.MEMORYDUMP_START"

.field public static final ACTION_NORMAL_BOOT_UP:Ljava/lang/String; = "android.intent.action.BOOT_COMPLETED"

.field public static final ACTION_START_SERVICE:Ljava/lang/String; = "com.mediatek.mdlogger.MDLoggerService"

.field public static final BUILD_TYPE:Ljava/lang/String; = "ro.build.type"

.field public static final COMMAND_START:Ljava/lang/String; = "Start"

.field public static final COMMAND_STOP:Ljava/lang/String; = "Stop"

.field public static final COMMAND_UNKNOWN:Ljava/lang/String; = "Unknown"

.field public static final ENGINEER_MODE_PACKAGE:Ljava/lang/String; = "com.mediatek.engineermode"

.field public static final ENG_BUILD:Ljava/lang/String; = "eng"

.field public static final EXTRA_FLAG_COMMON_UI_COMMAND:Ljava/lang/String; = "Command"

.field public static final EXTRA_FLAG_COMMON_UI_FROM:Ljava/lang/String; = "From"

.field public static final EXTRA_FLAG_COMMON_UI_RETURN:Ljava/lang/String; = "Return"

.field public static final EXTRA_FLAG_COMMON_UI_TO:Ljava/lang/String; = "To"

.field public static final EXTRA_FLAG_DIALOG_MESSAGE:Ljava/lang/String; = "DialogMessage"

.field public static final EXTRA_FLAG_DIALOG_TITLE:Ljava/lang/String; = "DialogTitle"

.field public static final EXTRA_FLAG_FROM_SERVICE:Ljava/lang/String; = "FromService"

.field public static final EXTRA_FLAG_LOGGING_MODE:Ljava/lang/String; = "LoggingMode"

.field public static final EXTRA_FLAG_LOG_FOLDER_PATH:Ljava/lang/String; = "LogPath"

.field public static final EXTRA_FLAG_RUNNING_STATUS:Ljava/lang/String; = "RunStatus"

.field public static final EXTRA_FLAG_START_SERVICE_SOURCE:Ljava/lang/String; = "ActivityStartService"

.field public static final EXTRA_FLAG_SYSLOGGER_WHICH:Ljava/lang/String; = "WHICH"

.field public static final EXT_SDCARD_PATH:Ljava/lang/String; = "/mnt/sdcard2"

.field public static final FILTER_FILE:Ljava/lang/String; = "catcher_filter.bin"

.field public static final FROM_ACTIVITY:I = 0x2

.field public static final FROM_AUTOTEST:I = 0x4

.field public static final FROM_COMMONUI:I = 0x3

.field public static final FROM_MDLOGGER:I = 0x1

.field public static final FROM_UPATE_PREF:I = 0x5

.field public static final LOG_PATH:Ljava/lang/String; = "/mnt/sdcard/mtklog/mdlog/"

.field public static final LOG_SIZE_DEFAULT_MODE:I = 0x1

.field public static final LOG_SIZE_MODE0:I = 0x0

.field public static final LOG_SIZE_MODE0_SIZE:I = 0x258

.field public static final LOG_SIZE_MODE1:I = 0x1

.field public static final LOG_SIZE_MODE1_SIZE:I = 0x3e8

.field public static final LOG_SIZE_MODE_SIZE_STOP_UNTIL_FULL:I = 0x0

.field public static final LOG_SIZE_MODE_STOP_UNTIL_FULL:I = 0x2

.field public static final LOG_TO_SD_PROPERTY:Ljava/lang/String; = "debug.log2sd.defaultpath"

.field public static final LOG_TREE_FILE:Ljava/lang/String; = "file_tree.txt"

.field public static final MAX_SIZE_KEY_PROP:Ljava/lang/String; = "com.mediatek.log.modem.maxsize"

.field public static final MDLOGGER_LOG_SDWAP_PATH:Ljava/lang/String; = "debug.mdl.sdswap.path"

.field public static final MDL_FALSE:I = 0x0

.field public static final MDL_TRUE:I = 0x1

.field public static final MODE_IDLE:I = 0x0

.field public static final MODE_POLLING:I = 0x3

.field public static final MODE_SD:I = 0x2

.field public static final MODE_UNKNOWN:I = -0x1

.field public static final MODE_USB:I = 0x1

.field public static final MODE_WAITSD:I = 0x4

.field public static final MTKLOG_PATH:Ljava/lang/String; = "/mtklog/mdlog/"

.field public static final NM_STATUS_NOSD:I = 0x3

.field public static final NM_STATUS_START:I = 0x1

.field public static final NM_STATUS_STOP:I = 0x2

.field public static final PREF_LOG_AUTO_START:Ljava/lang/String; = "PREF_AUTO_START"

.field public static final PREF_LOG_CURRENT_MODE:Ljava/lang/String; = "PREF_CURRENT_MODE"

.field public static final PREF_LOG_CUSTMIZE_INIT:Ljava/lang/String; = "PREF_LOG_SIZE_CUST_INIT"

.field public static final PREF_LOG_CUSTMIZE_SIZE:Ljava/lang/String; = "PREF_LOG_SIZE_CUST_SIZE"

.field public static final PREF_LOG_NAME:Ljava/lang/String; = "PREF_LOG_STATE"

.field public static final PREF_LOG_PAUSED:Ljava/lang/String; = "PREF_LOG_PAUSED"

.field public static final PREF_LOG_QUERY_INSTEAD_JNI:Ljava/lang/String; = "PREF_LOG_QUERY_TABLE"

.field public static final PREF_LOG_SIZE_MODE:Ljava/lang/String; = "PREF_LOG_SIZE_MODE"

.field public static final PREF_LOG_STATE_KEY:Ljava/lang/String; = "KEY_LOG_STATE"

.field public static final PREF_LOG_STATE_SD_MODE:Ljava/lang/String; = "MODE_SD"

.field public static final PROP_MONKEY:Ljava/lang/String; = "ro.monkey"

.field public static final RESERVED_SIZE:J = 0x283cL

.field public static final RETURN_ALREADY_RUN:Ljava/lang/String; = "AlreadyRun"

.field public static final RETURN_NORMAL:Ljava/lang/String; = "Normal"

.field public static final RETURN_NOSD:Ljava/lang/String; = "NoSDCard"

.field public static final RETURN_SDFULL:Ljava/lang/String; = "SDCardFull"

.field public static final RETURN_UNKNOWN:Ljava/lang/String; = "Unknown"

.field public static final SDCARD_PATH:Ljava/lang/String; = "/mnt/sdcard"

.field public static final SD_FULL:I = 0x1

.field public static final SD_NORMAL:I = 0x0

.field public static final SD_NOT_EXIST:I = 0x2

.field public static final SD_NOT_WRITABLE:I = 0x3

.field public static final SOURCE_COMMONUI:Ljava/lang/String; = "CommonUI"

.field public static final SOURCE_MDLOGGER:Ljava/lang/String; = "ModemLog"

.field public static final SOURCE_UNKNOWN:Ljava/lang/String; = "Unknown"

.field public static final SYSLOGGER_CLASS:Ljava/lang/String; = "com.mediatek.engineermode.syslogger.SysLogger"

.field public static final TAB_MODEMLOG:Ljava/lang/String; = "Modem"

.field public static final TAG_ASTL1:Ljava/lang/String; = "MDLog_ASTL1"

.field public static final TAG_DAK:Ljava/lang/String; = "MDLog_DAK"

.field public static final TAG_DMDSPMLT:Ljava/lang/String; = "MDLog_DMDSPMLT"

.field public static final TAG_L1:Ljava/lang/String; = "MDLog_L1"

.field public static final TAG_MD2GMLT:Ljava/lang/String; = "MDLog_MD2GMLT"

.field public static final TAG_PS:Ljava/lang/String; = "MDLog_PS"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
