.class public Lcom/android/bluetooth/pbap/BluetoothVCardEntryHandler;
.super Ljava/lang/Object;
.source "BluetoothVCardEntryHandler.java"

# interfaces
.implements Lcom/android/bluetooth/pbap/BluetoothVCardComposer$OneEntryHandler;


# static fields
.field private static final TAG:Ljava/lang/String; = "BluetoothVCardEntryHandler"


# instance fields
.field mVcard:Ljava/lang/String;

.field mWriter:Lcom/android/bluetooth/pbap/BluetoothPbapWriter;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothVCardEntryHandler;->mWriter:Lcom/android/bluetooth/pbap/BluetoothPbapWriter;

    iput-object p1, p0, Lcom/android/bluetooth/pbap/BluetoothVCardEntryHandler;->mVcard:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothVCardEntryHandler;->mWriter:Lcom/android/bluetooth/pbap/BluetoothPbapWriter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothVCardEntryHandler;->mWriter:Lcom/android/bluetooth/pbap/BluetoothPbapWriter;

    invoke-virtual {v0}, Lcom/android/bluetooth/pbap/BluetoothPbapWriter;->getPath()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onEntryCreated(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothVCardEntryHandler;->mWriter:Lcom/android/bluetooth/pbap/BluetoothPbapWriter;

    invoke-virtual {v0, p1}, Lcom/android/bluetooth/pbap/BluetoothPbapWriter;->write(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public onInit(Landroid/content/Context;)Z
    .locals 3
    .param p1    # Landroid/content/Context;

    const/4 v2, 0x0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothVCardEntryHandler;->mWriter:Lcom/android/bluetooth/pbap/BluetoothPbapWriter;

    if-nez v1, :cond_0

    new-instance v1, Lcom/android/bluetooth/pbap/BluetoothPbapWriter;

    invoke-direct {v1}, Lcom/android/bluetooth/pbap/BluetoothPbapWriter;-><init>()V

    iput-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothVCardEntryHandler;->mWriter:Lcom/android/bluetooth/pbap/BluetoothPbapWriter;

    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothVCardEntryHandler;->mWriter:Lcom/android/bluetooth/pbap/BluetoothPbapWriter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothVCardEntryHandler;->mWriter:Lcom/android/bluetooth/pbap/BluetoothPbapWriter;

    invoke-virtual {v1, p1}, Lcom/android/bluetooth/pbap/BluetoothPbapWriter;->init(Landroid/content/Context;)Z

    move-result v1

    if-eq v1, v0, :cond_1

    iput-object v2, p0, Lcom/android/bluetooth/pbap/BluetoothVCardEntryHandler;->mWriter:Lcom/android/bluetooth/pbap/BluetoothPbapWriter;

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothVCardEntryHandler;->mWriter:Lcom/android/bluetooth/pbap/BluetoothPbapWriter;

    if-eqz v1, :cond_2

    :goto_1
    return v0

    :cond_1
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothVCardEntryHandler;->mVcard:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothVCardEntryHandler;->mVcard:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/android/bluetooth/pbap/BluetoothVCardEntryHandler;->onEntryCreated(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothVCardEntryHandler;->mWriter:Lcom/android/bluetooth/pbap/BluetoothPbapWriter;

    invoke-virtual {v1}, Lcom/android/bluetooth/pbap/BluetoothPbapWriter;->terminate()V

    iput-object v2, p0, Lcom/android/bluetooth/pbap/BluetoothVCardEntryHandler;->mWriter:Lcom/android/bluetooth/pbap/BluetoothPbapWriter;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public onTerminate()V
    .locals 1

    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothVCardEntryHandler;->mWriter:Lcom/android/bluetooth/pbap/BluetoothPbapWriter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothVCardEntryHandler;->mWriter:Lcom/android/bluetooth/pbap/BluetoothPbapWriter;

    invoke-virtual {v0}, Lcom/android/bluetooth/pbap/BluetoothPbapWriter;->terminate()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothVCardEntryHandler;->mWriter:Lcom/android/bluetooth/pbap/BluetoothPbapWriter;

    :cond_0
    return-void
.end method
