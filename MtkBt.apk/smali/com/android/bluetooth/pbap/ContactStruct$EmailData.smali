.class public Lcom/android/bluetooth/pbap/ContactStruct$EmailData;
.super Ljava/lang/Object;
.source "ContactStruct.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/pbap/ContactStruct;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "EmailData"
.end annotation


# instance fields
.field public final data:Ljava/lang/String;

.field public isPrimary:Z

.field public final label:Ljava/lang/String;

.field public final type:I


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/android/bluetooth/pbap/ContactStruct$EmailData;->type:I

    iput-object p2, p0, Lcom/android/bluetooth/pbap/ContactStruct$EmailData;->data:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/bluetooth/pbap/ContactStruct$EmailData;->label:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/android/bluetooth/pbap/ContactStruct$EmailData;->isPrimary:Z

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x0

    instance-of v2, p1, Lcom/android/bluetooth/pbap/ContactStruct$EmailData;

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    check-cast v0, Lcom/android/bluetooth/pbap/ContactStruct$EmailData;

    iget v2, p0, Lcom/android/bluetooth/pbap/ContactStruct$EmailData;->type:I

    iget v3, v0, Lcom/android/bluetooth/pbap/ContactStruct$EmailData;->type:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/android/bluetooth/pbap/ContactStruct$EmailData;->data:Ljava/lang/String;

    iget-object v3, v0, Lcom/android/bluetooth/pbap/ContactStruct$EmailData;->data:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/bluetooth/pbap/ContactStruct$EmailData;->label:Ljava/lang/String;

    iget-object v3, v0, Lcom/android/bluetooth/pbap/ContactStruct$EmailData;->label:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/android/bluetooth/pbap/ContactStruct$EmailData;->isPrimary:Z

    iget-boolean v3, v0, Lcom/android/bluetooth/pbap/ContactStruct$EmailData;->isPrimary:Z

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const-string v0, "type: %d, data: %s, label: %s, isPrimary: %s"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lcom/android/bluetooth/pbap/ContactStruct$EmailData;->type:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/android/bluetooth/pbap/ContactStruct$EmailData;->data:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/android/bluetooth/pbap/ContactStruct$EmailData;->label:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-boolean v3, p0, Lcom/android/bluetooth/pbap/ContactStruct$EmailData;->isPrimary:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
