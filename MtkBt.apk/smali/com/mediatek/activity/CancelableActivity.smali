.class public abstract Lcom/mediatek/activity/CancelableActivity;
.super Landroid/app/Activity;
.source "CancelableActivity.java"


# static fields
.field public static final ACTION_CANCEL_ACTIVITY:Ljava/lang/String; = "com.mediatek.activity.CancelableActivity.action.CANCEL_ACTIVITY"

.field public static final EXTRA_CANCEL_ID:Ljava/lang/String; = "com.mediatek.activity.CancelableActivity.extra.ID"

.field public static final NULL_CANCEL_ID:I = -0x9a9c3


# instance fields
.field private cancelReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lcom/mediatek/activity/CancelableActivity$1;

    invoke-direct {v0, p0}, Lcom/mediatek/activity/CancelableActivity$1;-><init>(Lcom/mediatek/activity/CancelableActivity;)V

    iput-object v0, p0, Lcom/mediatek/activity/CancelableActivity;->cancelReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method public static sendCancelActivityIntent(Landroid/content/Context;I)V
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # I

    const-string v1, "CancelableActivity.sendCancelActivityIntent()"

    invoke-static {v1}, Lcom/mediatek/bluetooth/util/BtLog;->d(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.mediatek.activity.CancelableActivity.action.CANCEL_ACTIVITY"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.mediatek.activity.CancelableActivity.extra.ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method protected abstract onActivityCancel(I)V
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const-string v0, "CancelableActivity.onCreate() - registering BroadReceiver..."

    invoke-static {v0}, Lcom/mediatek/bluetooth/util/BtLog;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/activity/CancelableActivity;->cancelReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.mediatek.activity.CancelableActivity.action.CANCEL_ACTIVITY"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    iget-object v0, p0, Lcom/mediatek/activity/CancelableActivity;->cancelReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method
