.class public final Lcom/mediatek/bluetooth/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/bluetooth/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final app_description:I = 0x7f060148

.field public static final app_label:I = 0x7f060147

.field public static final app_name:I = 0x7f060022

.field public static final auth_cancel:I = 0x7f060048

.field public static final auth_ok:I = 0x7f060047

.field public static final auth_passcode:I = 0x7f060046

.field public static final bluetooth_device_context_transfer_files:I = 0x7f06009b

.field public static final bluetooth_dun_authorize_allow:I = 0x7f06004f

.field public static final bluetooth_dun_authorize_decline:I = 0x7f060050

.field public static final bluetooth_dun_authorize_message:I = 0x7f06004e

.field public static final bluetooth_dun_authorize_title:I = 0x7f06004d

.field public static final bluetooth_dun_notification_connect_request_message:I = 0x7f060053

.field public static final bluetooth_dun_notification_connect_request_ticker:I = 0x7f060051

.field public static final bluetooth_dun_notification_connect_request_title:I = 0x7f060052

.field public static final bluetooth_ftp:I = 0x7f06008f

.field public static final bluetooth_ftp_both_off_sd_unmounted:I = 0x7f060062

.field public static final bluetooth_ftp_cancel:I = 0x7f060097

.field public static final bluetooth_ftp_client:I = 0x7f060091

.field public static final bluetooth_ftp_client_activated_notify_message:I = 0x7f06006a

.field public static final bluetooth_ftp_client_activated_notify_ticker:I = 0x7f060068

.field public static final bluetooth_ftp_client_activated_notify_title:I = 0x7f060069

.field public static final bluetooth_ftp_client_browse_failed:I = 0x7f060072

.field public static final bluetooth_ftp_client_cancelling:I = 0x7f060067

.field public static final bluetooth_ftp_client_confirm_delete_message:I = 0x7f060078

.field public static final bluetooth_ftp_client_confirm_delete_title:I = 0x7f060077

.field public static final bluetooth_ftp_client_confirm_exit_message:I = 0x7f06007e

.field public static final bluetooth_ftp_client_confirm_exit_title:I = 0x7f06007d

.field public static final bluetooth_ftp_client_connecting:I = 0x7f060064

.field public static final bluetooth_ftp_client_connection_failed:I = 0x7f06006b

.field public static final bluetooth_ftp_client_current_is_root:I = 0x7f06007f

.field public static final bluetooth_ftp_client_delete_failed_message:I = 0x7f06007a

.field public static final bluetooth_ftp_client_delete_failed_title:I = 0x7f060079

.field public static final bluetooth_ftp_client_disconnected:I = 0x7f06006c

.field public static final bluetooth_ftp_client_empty_folder:I = 0x7f060082

.field public static final bluetooth_ftp_client_getting:I = 0x7f06006f

.field public static final bluetooth_ftp_client_menu_create_folder:I = 0x7f060084

.field public static final bluetooth_ftp_client_menu_exit:I = 0x7f060088

.field public static final bluetooth_ftp_client_menu_get_marked:I = 0x7f06008a

.field public static final bluetooth_ftp_client_menu_goto_root:I = 0x7f060086

.field public static final bluetooth_ftp_client_menu_mark_all:I = 0x7f06008b

.field public static final bluetooth_ftp_client_menu_mark_several:I = 0x7f060087

.field public static final bluetooth_ftp_client_menu_refresh:I = 0x7f060085

.field public static final bluetooth_ftp_client_menu_send_files:I = 0x7f060083

.field public static final bluetooth_ftp_client_menu_send_marked:I = 0x7f060089

.field public static final bluetooth_ftp_client_menu_unmark_all:I = 0x7f06008c

.field public static final bluetooth_ftp_client_new_folder_failed_message:I = 0x7f06007c

.field public static final bluetooth_ftp_client_new_folder_failed_title:I = 0x7f06007b

.field public static final bluetooth_ftp_client_new_folder_message:I = 0x7f06008e

.field public static final bluetooth_ftp_client_new_folder_title:I = 0x7f06008d

.field public static final bluetooth_ftp_client_no_file_to_mark:I = 0x7f060080

.field public static final bluetooth_ftp_client_no_marked_file:I = 0x7f060081

.field public static final bluetooth_ftp_client_off_sd_unmounted:I = 0x7f060061

.field public static final bluetooth_ftp_client_parent_folder:I = 0x7f060076

.field public static final bluetooth_ftp_client_processing:I = 0x7f060065

.field public static final bluetooth_ftp_client_refreshing:I = 0x7f060066

.field public static final bluetooth_ftp_client_root_folder:I = 0x7f060075

.field public static final bluetooth_ftp_client_sd_not_ready:I = 0x7f060063

.field public static final bluetooth_ftp_client_sending:I = 0x7f06006e

.field public static final bluetooth_ftp_client_set_path_failed_message:I = 0x7f060074

.field public static final bluetooth_ftp_client_set_path_failed_title:I = 0x7f060073

.field public static final bluetooth_ftp_client_transfer_finished:I = 0x7f060070

.field public static final bluetooth_ftp_client_transfer_unfinished:I = 0x7f060071

.field public static final bluetooth_ftp_client_transferring:I = 0x7f06006d

.field public static final bluetooth_ftp_date:I = 0x7f060098

.field public static final bluetooth_ftp_local_browser:I = 0x7f060092

.field public static final bluetooth_ftp_no:I = 0x7f060095

.field public static final bluetooth_ftp_ok:I = 0x7f060096

.field public static final bluetooth_ftp_profile_summary_connected:I = 0x7f06009c

.field public static final bluetooth_ftp_profile_summary_not_connected:I = 0x7f06009d

.field public static final bluetooth_ftp_server:I = 0x7f060090

.field public static final bluetooth_ftp_server_authorize_allow:I = 0x7f060059

.field public static final bluetooth_ftp_server_authorize_decline:I = 0x7f06005a

.field public static final bluetooth_ftp_server_authorize_message:I = 0x7f060058

.field public static final bluetooth_ftp_server_authorize_notify_message:I = 0x7f060056

.field public static final bluetooth_ftp_server_authorize_notify_ticker:I = 0x7f060054

.field public static final bluetooth_ftp_server_authorize_notify_title:I = 0x7f060055

.field public static final bluetooth_ftp_server_authorize_title:I = 0x7f060057

.field public static final bluetooth_ftp_server_connected_notify_message:I = 0x7f06005d

.field public static final bluetooth_ftp_server_connected_notify_ticker:I = 0x7f06005b

.field public static final bluetooth_ftp_server_connected_notify_title:I = 0x7f06005c

.field public static final bluetooth_ftp_server_disconnect_message:I = 0x7f06005f

.field public static final bluetooth_ftp_server_disconnect_title:I = 0x7f06005e

.field public static final bluetooth_ftp_server_enable_summary:I = 0x7f0600a0

.field public static final bluetooth_ftp_server_enable_title:I = 0x7f06009f

.field public static final bluetooth_ftp_server_off_sd_unmounted:I = 0x7f060060

.field public static final bluetooth_ftp_server_permission_summary_fullctrl:I = 0x7f0600a3

.field public static final bluetooth_ftp_server_permission_summary_readonly:I = 0x7f0600a2

.field public static final bluetooth_ftp_server_permission_title:I = 0x7f0600a1

.field public static final bluetooth_ftp_server_sd_dialog_msg:I = 0x7f0600a5

.field public static final bluetooth_ftp_server_sd_dialog_ok:I = 0x7f0600a6

.field public static final bluetooth_ftp_server_sd_dialog_title:I = 0x7f0600a4

.field public static final bluetooth_ftp_server_settings:I = 0x7f06009e

.field public static final bluetooth_ftp_service_busy:I = 0x7f06009a

.field public static final bluetooth_ftp_several_marker:I = 0x7f060093

.field public static final bluetooth_ftp_size:I = 0x7f060099

.field public static final bluetooth_ftp_yes:I = 0x7f060094

.field public static final bluetooth_hid_addnewdevice:I = 0x7f0600ab

.field public static final bluetooth_hid_auth_confirm:I = 0x7f0600c5

.field public static final bluetooth_hid_auth_confirm_title:I = 0x7f0600b1

.field public static final bluetooth_hid_bt_fail:I = 0x7f0600ae

.field public static final bluetooth_hid_bt_ok:I = 0x7f0600ad

.field public static final bluetooth_hid_btnoton:I = 0x7f0600cb

.field public static final bluetooth_hid_connect:I = 0x7f0600a8

.field public static final bluetooth_hid_connect_fail:I = 0x7f0600be

.field public static final bluetooth_hid_connect_ok:I = 0x7f0600bd

.field public static final bluetooth_hid_connect_request_notify_message:I = 0x7f0600bc

.field public static final bluetooth_hid_connect_request_notify_ticker:I = 0x7f0600b9

.field public static final bluetooth_hid_connect_request_notify_title:I = 0x7f0600ba

.field public static final bluetooth_hid_connected_notify_message:I = 0x7f0600bb

.field public static final bluetooth_hid_connected_notify_ticker:I = 0x7f0600b7

.field public static final bluetooth_hid_connected_notify_title:I = 0x7f0600b8

.field public static final bluetooth_hid_disconnect:I = 0x7f0600a9

.field public static final bluetooth_hid_disconnect_confirm:I = 0x7f0600c3

.field public static final bluetooth_hid_disconnect_confirm_title:I = 0x7f0600af

.field public static final bluetooth_hid_disconnect_fail:I = 0x7f0600c0

.field public static final bluetooth_hid_disconnect_ok:I = 0x7f0600bf

.field public static final bluetooth_hid_getreport:I = 0x7f0600c8

.field public static final bluetooth_hid_lable:I = 0x7f0600a7

.field public static final bluetooth_hid_no:I = 0x7f0600b6

.field public static final bluetooth_hid_paireddevices:I = 0x7f0600ac

.field public static final bluetooth_hid_sendreport:I = 0x7f0600c9

.field public static final bluetooth_hid_setreport:I = 0x7f0600ca

.field public static final bluetooth_hid_spinner:I = 0x7f0600c7

.field public static final bluetooth_hid_summary_connected:I = 0x7f0600b2

.field public static final bluetooth_hid_summary_connecting:I = 0x7f0600b3

.field public static final bluetooth_hid_summary_disconnecting:I = 0x7f0600c6

.field public static final bluetooth_hid_summary_not_connected:I = 0x7f0600b4

.field public static final bluetooth_hid_unplug:I = 0x7f0600aa

.field public static final bluetooth_hid_unplug_confirm:I = 0x7f0600c4

.field public static final bluetooth_hid_unplug_confirm_title:I = 0x7f0600b0

.field public static final bluetooth_hid_unplug_fail:I = 0x7f0600c2

.field public static final bluetooth_hid_unplug_ok:I = 0x7f0600c1

.field public static final bluetooth_hid_yes:I = 0x7f0600b5

.field public static final bluetooth_map_server_OK:I = 0x7f0600e0

.field public static final bluetooth_map_server_account_index_title:I = 0x7f0600de

.field public static final bluetooth_map_server_authorize_confirm_allow:I = 0x7f0600d1

.field public static final bluetooth_map_server_authorize_confirm_reject:I = 0x7f0600d2

.field public static final bluetooth_map_server_authorize_message:I = 0x7f0600d0

.field public static final bluetooth_map_server_authorize_request_message:I = 0x7f0600ce

.field public static final bluetooth_map_server_authorize_request_ticker:I = 0x7f0600cc

.field public static final bluetooth_map_server_authorize_request_title:I = 0x7f0600cd

.field public static final bluetooth_map_server_authorize_title:I = 0x7f0600cf

.field public static final bluetooth_map_server_cancel:I = 0x7f0600e1

.field public static final bluetooth_map_server_connect_notify_message:I = 0x7f0600d5

.field public static final bluetooth_map_server_connect_notify_ticker:I = 0x7f0600d3

.field public static final bluetooth_map_server_connect_notify_title:I = 0x7f0600d4

.field public static final bluetooth_map_server_disconnect_message:I = 0x7f0600d7

.field public static final bluetooth_map_server_disconnect_title:I = 0x7f0600d6

.field public static final bluetooth_map_server_enable_summary:I = 0x7f0600dc

.field public static final bluetooth_map_server_enable_title:I = 0x7f0600db

.field public static final bluetooth_map_server_no:I = 0x7f0600d9

.field public static final bluetooth_map_server_no_account:I = 0x7f0600df

.field public static final bluetooth_map_server_settings:I = 0x7f0600da

.field public static final bluetooth_map_server_sim_index_title:I = 0x7f0600dd

.field public static final bluetooth_map_server_yes:I = 0x7f0600d8

.field public static final bluetooth_pan_GN_authorize_confirm:I = 0x7f0600ff

.field public static final bluetooth_pan_GN_authorize_confirm_title:I = 0x7f0600fd

.field public static final bluetooth_pan_GN_authorize_notify_message:I = 0x7f060109

.field public static final bluetooth_pan_GN_authorize_notify_ticker:I = 0x7f060107

.field public static final bluetooth_pan_GN_authorize_notify_title:I = 0x7f060108

.field public static final bluetooth_pan_GN_connect_fail:I = 0x7f060114

.field public static final bluetooth_pan_GN_connect_ok:I = 0x7f060113

.field public static final bluetooth_pan_GN_connected_confirm:I = 0x7f060103

.field public static final bluetooth_pan_GN_connected_confirm_title:I = 0x7f060101

.field public static final bluetooth_pan_GN_connected_notify_message:I = 0x7f06010f

.field public static final bluetooth_pan_GN_connected_notify_ticker:I = 0x7f06010d

.field public static final bluetooth_pan_GN_connected_notify_title:I = 0x7f06010e

.field public static final bluetooth_pan_GN_disconnect_fail:I = 0x7f060119

.field public static final bluetooth_pan_GN_disconnect_ind:I = 0x7f06011c

.field public static final bluetooth_pan_GN_disconnect_ok:I = 0x7f060118

.field public static final bluetooth_pan_NAP_authorize_confirm:I = 0x7f060100

.field public static final bluetooth_pan_NAP_authorize_confirm_title:I = 0x7f0600fe

.field public static final bluetooth_pan_NAP_authorize_notify_message:I = 0x7f06010c

.field public static final bluetooth_pan_NAP_authorize_notify_ticker:I = 0x7f06010a

.field public static final bluetooth_pan_NAP_authorize_notify_title:I = 0x7f06010b

.field public static final bluetooth_pan_NAP_connect_fail:I = 0x7f060116

.field public static final bluetooth_pan_NAP_connect_ok:I = 0x7f060115

.field public static final bluetooth_pan_NAP_connected_confirm:I = 0x7f060104

.field public static final bluetooth_pan_NAP_connected_confirm_title:I = 0x7f060102

.field public static final bluetooth_pan_NAP_connected_notify_message:I = 0x7f060112

.field public static final bluetooth_pan_NAP_connected_notify_ticker:I = 0x7f060110

.field public static final bluetooth_pan_NAP_connected_notify_title:I = 0x7f060111

.field public static final bluetooth_pan_NAP_disconnect_fail:I = 0x7f06011b

.field public static final bluetooth_pan_NAP_disconnect_ind:I = 0x7f06011d

.field public static final bluetooth_pan_NAP_disconnect_ok:I = 0x7f06011a

.field public static final bluetooth_pan_NAP_unavailable:I = 0x7f060117

.field public static final bluetooth_pan_gn_enable_summary:I = 0x7f060122

.field public static final bluetooth_pan_gn_enable_title:I = 0x7f060121

.field public static final bluetooth_pan_nap_enable_summary:I = 0x7f060120

.field public static final bluetooth_pan_nap_enable_title:I = 0x7f06011f

.field public static final bluetooth_pan_no:I = 0x7f060106

.field public static final bluetooth_pan_server_settings:I = 0x7f06011e

.field public static final bluetooth_pan_yes:I = 0x7f060105

.field public static final bluetooth_pbap_server:I = 0x7f060123

.field public static final bluetooth_pbap_server_auth_cancel:I = 0x7f06012a

.field public static final bluetooth_pbap_server_auth_chall_message:I = 0x7f06012f

.field public static final bluetooth_pbap_server_auth_chall_notify_ticker:I = 0x7f060125

.field public static final bluetooth_pbap_server_auth_chall_notify_title:I = 0x7f06012e

.field public static final bluetooth_pbap_server_auth_ok:I = 0x7f060129

.field public static final bluetooth_pbap_server_auth_passcode:I = 0x7f060128

.field public static final bluetooth_pbap_server_authorize_allow:I = 0x7f06012b

.field public static final bluetooth_pbap_server_authorize_alwaysallowed:I = 0x7f060131

.field public static final bluetooth_pbap_server_authorize_decline:I = 0x7f060130

.field public static final bluetooth_pbap_server_authorize_message:I = 0x7f06012d

.field public static final bluetooth_pbap_server_authorize_notify_message:I = 0x7f060126

.field public static final bluetooth_pbap_server_authorize_notify_ticker:I = 0x7f060124

.field public static final bluetooth_pbap_server_authorize_notify_title:I = 0x7f06012c

.field public static final bluetooth_pbap_server_authorize_title:I = 0x7f060127

.field public static final bluetooth_printing:I = 0x7f060034

.field public static final bluetooth_simap_server_enable_summary:I = 0x7f060143

.field public static final bluetooth_simap_server_enable_title:I = 0x7f060142

.field public static final bluetooth_simap_server_settings:I = 0x7f060141

.field public static final bluetooth_simap_server_sim_index_summary_sim1:I = 0x7f060145

.field public static final bluetooth_simap_server_sim_index_summary_sim2:I = 0x7f060146

.field public static final bluetooth_simap_server_sim_index_title:I = 0x7f060144

.field public static final bt_base_activity_busy_message:I = 0x7f060149

.field public static final bt_base_activity_service_disconnected:I = 0x7f06014a

.field public static final bt_base_profile_feature_disabled:I = 0x7f06014b

.field public static final bt_bip_app_name:I = 0x7f060000

.field public static final bt_bip_auth_cancel:I = 0x7f06001f

.field public static final bt_bip_auth_ok:I = 0x7f06001e

.field public static final bt_bip_auth_password:I = 0x7f06001c

.field public static final bt_bip_auth_userid:I = 0x7f06001d

.field public static final bt_bip_cancel_pending_job:I = 0x7f06000d

.field public static final bt_bip_pending_job:I = 0x7f06000e

.field public static final bt_bip_receiving:I = 0x7f060016

.field public static final bt_bip_receiving_fail:I = 0x7f060018

.field public static final bt_bip_receiving_successful:I = 0x7f060017

.field public static final bt_bip_toast_connect_fail:I = 0x7f060019

.field public static final bt_bip_toast_sending_object:I = 0x7f06001b

.field public static final bt_bip_toast_storage_unmounted:I = 0x7f06001a

.field public static final bt_bip_transmitting:I = 0x7f06000a

.field public static final bt_bip_transmitting_dialog_cancel:I = 0x7f060006

.field public static final bt_bip_transmitting_dialog_hide:I = 0x7f060005

.field public static final bt_bip_transmitting_dialog_no:I = 0x7f060009

.field public static final bt_bip_transmitting_dialog_ok:I = 0x7f060007

.field public static final bt_bip_transmitting_dialog_yes:I = 0x7f060008

.field public static final bt_bip_transmitting_fail:I = 0x7f06000c

.field public static final bt_bip_transmitting_successful:I = 0x7f06000b

.field public static final bt_bipi_push_confirmation:I = 0x7f060001

.field public static final bt_bipi_push_confirmation_cancel:I = 0x7f060004

.field public static final bt_bipi_push_confirmation_ok:I = 0x7f060003

.field public static final bt_bipi_unsupported_format:I = 0x7f060002

.field public static final bt_bipr_accept_always:I = 0x7f060013

.field public static final bt_bipr_accept_content:I = 0x7f060012

.field public static final bt_bipr_accept_no:I = 0x7f060015

.field public static final bt_bipr_accept_title:I = 0x7f060011

.field public static final bt_bipr_accept_yes:I = 0x7f060014

.field public static final bt_bipr_incoming_request:I = 0x7f06000f

.field public static final bt_bipr_incoming_request_description:I = 0x7f060010

.field public static final bt_bpp_copies_cancel:I = 0x7f06004b

.field public static final bt_bpp_copies_set:I = 0x7f06004a

.field public static final bt_bpp_copies_title:I = 0x7f060049

.field public static final bt_bpp_reentry_error:I = 0x7f06004c

.field public static final bt_opp_cancel_task_message:I = 0x7f0600eb

.field public static final bt_opp_cancel_task_no:I = 0x7f0600ed

.field public static final bt_opp_cancel_task_title:I = 0x7f0600ea

.field public static final bt_opp_cancel_task_yes:I = 0x7f0600ec

.field public static final bt_opp_canceling_task_message:I = 0x7f0600ee

.field public static final bt_opp_notification_in_coming_message:I = 0x7f0600e5

.field public static final bt_opp_notification_in_coming_ticker:I = 0x7f0600e3

.field public static final bt_opp_notification_in_coming_title:I = 0x7f0600e4

.field public static final bt_opp_notification_in_ongoing_message:I = 0x7f0600e7

.field public static final bt_opp_notification_in_ongoing_ticker:I = 0x7f0600e6

.field public static final bt_opp_notification_out_ongoing_message:I = 0x7f0600e9

.field public static final bt_opp_notification_out_ongoing_ticker:I = 0x7f0600e8

.field public static final bt_opp_push_file_name:I = 0x7f0600e2

.field public static final bt_oppc_push_action_label:I = 0x7f0600f3

.field public static final bt_oppc_push_toast_sending_file:I = 0x7f0600f4

.field public static final bt_oppc_push_toast_sending_files:I = 0x7f0600f5

.field public static final bt_oppc_push_toast_unsupported_uri:I = 0x7f0600f6

.field public static final bt_opps_confirm_push_message:I = 0x7f0600f0

.field public static final bt_opps_confirm_push_no:I = 0x7f0600f2

.field public static final bt_opps_confirm_push_title:I = 0x7f0600ef

.field public static final bt_opps_confirm_push_yes:I = 0x7f0600f1

.field public static final bt_opps_push_toast_invalid_request:I = 0x7f0600f8

.field public static final bt_opps_toast_storage_unavailable:I = 0x7f0600f7

.field public static final bt_print_entry_label:I = 0x7f060024

.field public static final bt_share_mgmt_notification_message:I = 0x7f060157

.field public static final bt_share_mgmt_notification_ticker:I = 0x7f060155

.field public static final bt_share_mgmt_notification_title:I = 0x7f060156

.field public static final bt_share_mgmt_tab_dialog_message_recfail:I = 0x7f060152

.field public static final bt_share_mgmt_tab_dialog_message_resend:I = 0x7f060153

.field public static final bt_share_mgmt_tab_dialog_message_sent:I = 0x7f060154

.field public static final bt_share_mgmt_tab_dialog_no:I = 0x7f060151

.field public static final bt_share_mgmt_tab_dialog_title:I = 0x7f06014f

.field public static final bt_share_mgmt_tab_dialog_yes:I = 0x7f060150

.field public static final bt_share_mgmt_tab_in_title:I = 0x7f06014c

.field public static final bt_share_mgmt_tab_menu_clear:I = 0x7f06014e

.field public static final bt_share_mgmt_tab_out_title:I = 0x7f06014d

.field public static final bt_share_openfile_confirm:I = 0x7f06015c

.field public static final bt_share_openfile_message_invalid_param:I = 0x7f060159

.field public static final bt_share_openfile_message_resource_not_found:I = 0x7f06015b

.field public static final bt_share_openfile_message_unsupported_type:I = 0x7f06015a

.field public static final bt_share_openfile_title:I = 0x7f060158

.field public static final dialog_bt_not_ready_confirm:I = 0x7f060026

.field public static final dialog_printing_cancel:I = 0x7f060038

.field public static final dialog_printing_hide:I = 0x7f060037

.field public static final dialog_printing_ok:I = 0x7f060039

.field public static final dialog_title_bpp_not_ready:I = 0x7f060025

.field public static final get_default_value:I = 0x7f06002d

.field public static final number_of_copies:I = 0x7f06002f

.field public static final orientation:I = 0x7f060032

.field public static final pages_per_sheet:I = 0x7f060031

.field public static final paper_size:I = 0x7f06002e

.field public static final permdesc_bluetoothWhitelist:I = 0x7f0600fa

.field public static final permdesc_handoverStatus:I = 0x7f0600fc

.field public static final permission_desp_bluetoothPrint:I = 0x7f060021

.field public static final permission_label_bluetoothPrint:I = 0x7f060020

.field public static final permlab_bluetoothWhitelist:I = 0x7f0600f9

.field public static final permlab_handoverStatus:I = 0x7f0600fb

.field public static final print:I = 0x7f06002c

.field public static final printer_settings:I = 0x7f060023

.field public static final printing_fail:I = 0x7f060036

.field public static final printing_successful:I = 0x7f060035

.field public static final quality:I = 0x7f060033

.field public static final reason_attention_required:I = 0x7f06003a

.field public static final reason_door_open:I = 0x7f06003d

.field public static final reason_marker_failure:I = 0x7f060044

.field public static final reason_marker_supply_empty:I = 0x7f060043

.field public static final reason_marker_supply_low:I = 0x7f060042

.field public static final reason_media_empty:I = 0x7f06003f

.field public static final reason_media_jam:I = 0x7f06003b

.field public static final reason_media_low:I = 0x7f06003e

.field public static final reason_nondefine:I = 0x7f060045

.field public static final reason_output_area_almost_full:I = 0x7f060040

.field public static final reason_output_area_full:I = 0x7f060041

.field public static final reason_paused:I = 0x7f06003c

.field public static final sides:I = 0x7f060030

.field public static final simap_acceptance_dialog_title:I = 0x7f060133

.field public static final simap_acceptance_timeout_message:I = 0x7f060134

.field public static final simap_connected_notify_message:I = 0x7f06013c

.field public static final simap_connected_notify_ticker:I = 0x7f06013a

.field public static final simap_connected_notify_title:I = 0x7f06013b

.field public static final simap_defaultname:I = 0x7f060136

.field public static final simap_disconnect_message:I = 0x7f06013e

.field public static final simap_disconnect_no:I = 0x7f060140

.field public static final simap_disconnect_title:I = 0x7f06013d

.field public static final simap_disconnect_yes:I = 0x7f06013f

.field public static final simap_disconnected_message:I = 0x7f060135

.field public static final simap_incoming_conn_confirm_cancel:I = 0x7f060138

.field public static final simap_incoming_conn_confirm_ok:I = 0x7f060139

.field public static final simap_remote_request:I = 0x7f060132

.field public static final simap_request_notif_message:I = 0x7f060137

.field public static final toast_bpp_enable_fail:I = 0x7f060027

.field public static final toast_connect_fail:I = 0x7f060028

.field public static final toast_get_default_value:I = 0x7f06002b

.field public static final toast_get_printer_attr:I = 0x7f06002a

.field public static final toast_get_printer_attr_fail:I = 0x7f060029


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
