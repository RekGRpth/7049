.class public Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;
.super Ljava/lang/Object;
.source "BTAvrcpBrowseAlbum.java"


# static fields
.field public static final ALBUM_TYPE:B = 0x2t

.field public static final ALL_TYPE:B = 0xet

.field public static final ARTIST_TYPE:B = 0x3t

.field public static final EMPTY_TYPE:B = 0xft

.field public static final GENRES_TYPE:B = 0x4t

.field public static final MIX_TYPE:B = 0x0t

.field public static final PLAYLIST_TYPE:B = 0x5t

.field public static final TAG:Ljava/lang/String; = "AVRCP_ALBUM"

.field public static final TITLE_TYPE:B = 0x1t

.field public static final YEAR_TYPE:B = 0x6t


# instance fields
.field private mContext:Landroid/content/Context;

.field private mId:J

.field private mIdList:[J

.field private mIdSubList:[J

.field private mNameList:[Ljava/lang/String;

.field private mPathArray:[Ljava/lang/String;

.field private mSelectId:J

.field private mSubAlbumLis:[Ljava/lang/String;

.field private mSubArtistLis:[Ljava/lang/String;

.field private mSubPathArray:[Ljava/lang/String;

.field private mSubTitleLis:[Ljava/lang/String;

.field private mtype:B


# direct methods
.method constructor <init>(JLandroid/content/Context;B)V
    .locals 8
    .param p1    # J
    .param p3    # Landroid/content/Context;
    .param p4    # B

    const-wide/16 v6, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mId:J

    iput-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mIdList:[J

    iput-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mNameList:[Ljava/lang/String;

    iput-wide v6, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSelectId:J

    iput-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mIdSubList:[J

    iput-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSubTitleLis:[Ljava/lang/String;

    iput-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSubArtistLis:[Ljava/lang/String;

    iput-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSubAlbumLis:[Ljava/lang/String;

    iput-byte v5, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mtype:B

    iput-wide p1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mId:J

    iput-object p3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->resetSubFolder()V

    iput-byte p4, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mtype:B

    new-array v0, v5, [Ljava/lang/String;

    iput-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mPathArray:[Ljava/lang/String;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSubPathArray:[Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mPathArray:[Ljava/lang/String;

    const-string v1, "root"

    aput-object v1, v0, v4

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSubPathArray:[Ljava/lang/String;

    const-string v1, "root"

    aput-object v1, v0, v4

    sparse-switch p4, :sswitch_data_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mPathArray:[Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSubPathArray:[Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    :goto_0
    iget-wide v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mId:J

    cmp-long v0, v6, v0

    if-nez v0, :cond_0

    const-string v0, "AVRCP_ALBUM"

    const-string v1, "[BT][AVRCP] Should not use 0 as Id "

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void

    :sswitch_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mPathArray:[Ljava/lang/String;

    const-string v1, "Artist"

    aput-object v1, v0, v3

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSubPathArray:[Ljava/lang/String;

    const-string v1, "Artist"

    aput-object v1, v0, v3

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mPathArray:[Ljava/lang/String;

    const-string v1, "Album"

    aput-object v1, v0, v3

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSubPathArray:[Ljava/lang/String;

    const-string v1, "Album"

    aput-object v1, v0, v3

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mPathArray:[Ljava/lang/String;

    const-string v1, "Empty"

    aput-object v1, v0, v3

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSubPathArray:[Ljava/lang/String;

    const-string v1, "Empty"

    aput-object v1, v0, v3

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mPathArray:[Ljava/lang/String;

    const-string v1, "All"

    aput-object v1, v0, v3

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSubPathArray:[Ljava/lang/String;

    const-string v1, "All"

    aput-object v1, v0, v3

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_3
        0x2 -> :sswitch_1
        0x3 -> :sswitch_0
        0xe -> :sswitch_3
        0xf -> :sswitch_2
    .end sparse-switch
.end method

.method private destroySubFolder()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mIdList:[J

    return-void
.end method

.method public static query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/net/Uri;
    .param p2    # [Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-static/range {v0 .. v6}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;I)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public static query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;I)Landroid/database/Cursor;
    .locals 8
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/net/Uri;
    .param p2    # [Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # I

    const/4 v7, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v1, v7

    :goto_0
    return-object v1

    :cond_0
    if-lez p6, :cond_1

    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "limit"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p1

    :cond_1
    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception v6

    move-object v1, v7

    goto :goto_0
.end method

.method private updateSongs(Landroid/content/Context;)[J
    .locals 10
    .param p1    # Landroid/content/Context;

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v9, 0x1

    iget-byte v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mtype:B

    sparse-switch v0, :sswitch_data_0

    new-array v2, v5, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v1

    const-string v0, "album"

    aput-object v0, v2, v4

    sget-object v1, Landroid/provider/MediaStore$Audio$Albums;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    :goto_0
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    :goto_1
    return-object v3

    :sswitch_0
    new-array v0, v1, [J

    iput-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mIdList:[J

    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mNameList:[Ljava/lang/String;

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mIdList:[J

    goto :goto_1

    :sswitch_1
    new-array v2, v5, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v1

    const-string v0, "artist"

    aput-object v0, v2, v4

    sget-object v1, Landroid/provider/MediaStore$Audio$Artists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    goto :goto_0

    :sswitch_2
    new-array v2, v5, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v1

    const-string v0, "artist"

    aput-object v0, v2, v4

    sget-object v1, Landroid/provider/MediaStore$Audio$Artists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    goto :goto_0

    :cond_2
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v8

    if-lez v8, :cond_3

    new-array v0, v8, [J

    iput-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mIdList:[J

    new-array v0, v8, [Ljava/lang/String;

    iput-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mNameList:[Ljava/lang/String;

    const/4 v7, 0x0

    :goto_2
    if-ge v7, v8, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mIdList:[J

    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    aput-wide v3, v0, v7

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mNameList:[Ljava/lang/String;

    const/4 v1, 0x1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v7

    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    :cond_3
    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mIdList:[J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1

    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_2
        0xe -> :sswitch_1
        0xf -> :sswitch_0
    .end sparse-switch
.end method

.method private updateSubSongs(Landroid/content/Context;J)[J
    .locals 15
    .param p1    # Landroid/content/Context;
    .param p2    # J

    const/4 v12, 0x1

    const/4 v2, 0x4

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v4, v2

    const/4 v2, 0x1

    const-string v3, "title"

    aput-object v3, v4, v2

    const/4 v2, 0x2

    const-string v3, "artist"

    aput-object v3, v4, v2

    const/4 v2, 0x3

    const-string v3, "album"

    aput-object v3, v4, v2

    iget-byte v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mtype:B

    sparse-switch v2, :sswitch_data_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "album_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, p2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "is_music"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=1"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    :goto_0
    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    const-string v7, "track"

    move-object/from16 v2, p1

    invoke-static/range {v2 .. v7}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    if-eqz v8, :cond_0

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    const/4 v2, 0x0

    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_1
    :goto_1
    return-object v2

    :sswitch_0
    const/4 v2, 0x0

    new-array v2, v2, [J

    iput-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mIdSubList:[J

    iget-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mIdSubList:[J

    goto :goto_1

    :sswitch_1
    const-string v5, "is_music=1"

    goto :goto_0

    :sswitch_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "artist_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, p2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "is_music"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=1"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    :cond_2
    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v11

    if-lez v11, :cond_4

    new-array v2, v11, [J

    iput-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mIdSubList:[J

    new-array v2, v11, [Ljava/lang/String;

    iput-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSubTitleLis:[Ljava/lang/String;

    new-array v2, v11, [Ljava/lang/String;

    iput-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSubArtistLis:[Ljava/lang/String;

    new-array v2, v11, [Ljava/lang/String;

    iput-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSubAlbumLis:[Ljava/lang/String;

    const/4 v10, 0x0

    :goto_2
    if-ge v10, v11, :cond_4

    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    iget-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mIdSubList:[J

    const/4 v3, 0x0

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    aput-wide v6, v2, v10

    iget-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSubTitleLis:[Ljava/lang/String;

    const/4 v3, 0x1

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v10

    iget-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSubArtistLis:[Ljava/lang/String;

    const/4 v3, 0x2

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v10

    iget-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSubAlbumLis:[Ljava/lang/String;

    const/4 v3, 0x3

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v10

    const/4 v2, 0x1

    if-ne v12, v2, :cond_3

    move-object v9, v8

    const-string v2, "AVRCP_ALBUM"

    const-string v3, "[AVRCP][UTIL] id:%d title:%s artist:%s album:%s"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const/4 v13, 0x0

    invoke-interface {v9, v13}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v13

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v6, v7

    const/4 v7, 0x1

    const/4 v13, 0x1

    invoke-interface {v9, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v6, v7

    const/4 v7, 0x2

    const/4 v13, 0x2

    invoke-interface {v9, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v6, v7

    const/4 v7, 0x3

    const/4 v13, 0x3

    invoke-interface {v9, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v6, v7

    invoke-static {v3, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    :cond_4
    iget-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mIdSubList:[J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    :catchall_0
    move-exception v2

    if-eqz v8, :cond_5

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v2

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_2
        0xe -> :sswitch_1
        0xf -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public buildSubFolder()V
    .locals 3

    iget-byte v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mtype:B

    const/16 v1, 0xe

    if-ne v0, v1, :cond_0

    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSelectId:J

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mContext:Landroid/content/Context;

    const-wide/16 v1, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->updateSubSongs(Landroid/content/Context;J)[J

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mIdList:[J

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->updateSongs(Landroid/content/Context;)[J

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mIdList:[J

    goto :goto_0
.end method

.method public getAttributeByIndex(II)Ljava/lang/String;
    .locals 4
    .param p1    # I
    .param p2    # I

    const-wide/16 v0, 0x0

    iget-wide v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSelectId:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mIdList:[J

    array-length v0, v0

    if-ge p1, v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mNameList:[Ljava/lang/String;

    aget-object v0, v0, p1

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mIdSubList:[J

    array-length v0, v0

    if-ge p1, v0, :cond_1

    packed-switch p2, :pswitch_data_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSubTitleLis:[Ljava/lang/String;

    aget-object v0, v0, p1

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSubArtistLis:[Ljava/lang/String;

    aget-object v0, v0, p1

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSubAlbumLis:[Ljava/lang/String;

    aget-object v0, v0, p1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getCategoryName()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mPathArray:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mPathArray:[Ljava/lang/String;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "<unknow>"

    goto :goto_0
.end method

.method public getCurPathDepth()I
    .locals 5

    const/4 v0, 0x1

    iget-byte v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mtype:B

    const/16 v2, 0xe

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-wide/16 v1, 0x0

    iget-wide v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSelectId:J

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0
.end method

.method public getCurPathItems()I
    .locals 5

    const/4 v0, 0x0

    const-wide/16 v1, 0x0

    iget-wide v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSelectId:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mIdList:[J

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mIdList:[J

    array-length v0, v0

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mIdSubList:[J

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mIdSubList:[J

    array-length v0, v0

    goto :goto_0
.end method

.method public getCurPaths()[Ljava/lang/String;
    .locals 5

    const-wide/16 v0, 0x0

    iget-wide v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSelectId:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mPathArray:[Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSubPathArray:[Ljava/lang/String;

    const/4 v1, 0x2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSelectId:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSubPathArray:[Ljava/lang/String;

    goto :goto_0
.end method

.method public getCurrentList()[J
    .locals 5

    const-wide/16 v0, 0x0

    iget-wide v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSelectId:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mIdList:[J

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSubPathArray:[Ljava/lang/String;

    const/4 v1, 0x2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSelectId:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mIdSubList:[J

    goto :goto_0
.end method

.method public getFolderType()B
    .locals 1

    iget-byte v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mtype:B

    return v0
.end method

.method public getItemAttribute(BJSI)Ljava/lang/String;
    .locals 6
    .param p1    # B
    .param p2    # J
    .param p4    # S
    .param p5    # I

    const/4 v0, 0x0

    const/4 v2, 0x1

    if-lt p5, v2, :cond_3

    const/4 v2, 0x3

    if-gt p5, v2, :cond_3

    const-wide/16 v2, 0x0

    iget-wide v4, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSelectId:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mIdSubList:[J

    array-length v2, v2

    if-ge v0, v2, :cond_3

    iget-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mIdSubList:[J

    aget-wide v2, v2, v0

    cmp-long v2, v2, p2

    if-nez v2, :cond_0

    move v1, v0

    packed-switch p5, :pswitch_data_0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :pswitch_0
    iget-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSubTitleLis:[Ljava/lang/String;

    aget-object v2, v2, v1

    :goto_1
    return-object v2

    :pswitch_1
    iget-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSubArtistLis:[Ljava/lang/String;

    aget-object v2, v2, v1

    goto :goto_1

    :pswitch_2
    iget-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSubAlbumLis:[Ljava/lang/String;

    aget-object v2, v2, v1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_2
    iget-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mIdList:[J

    array-length v2, v2

    if-ge v0, v2, :cond_3

    iget-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mIdList:[J

    aget-wide v2, v2, v0

    cmp-long v2, v2, p2

    if-nez v2, :cond_2

    move v1, v0

    iget-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mNameList:[Ljava/lang/String;

    aget-object v2, v2, v1

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getNameByIndex(I)Ljava/lang/String;
    .locals 4
    .param p1    # I

    const-wide/16 v0, 0x0

    iget-wide v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSelectId:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mIdList:[J

    array-length v0, v0

    if-ge p1, v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mNameList:[Ljava/lang/String;

    aget-object v0, v0, p1

    :goto_0
    return-object v0

    :cond_0
    iget-byte v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mtype:B

    sparse-switch v0, :sswitch_data_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mIdSubList:[J

    array-length v0, v0

    if-ge p1, v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSubAlbumLis:[Ljava/lang/String;

    aget-object v0, v0, p1

    goto :goto_0

    :sswitch_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mIdSubList:[J

    array-length v0, v0

    if-ge p1, v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSubTitleLis:[Ljava/lang/String;

    aget-object v0, v0, p1

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mIdSubList:[J

    array-length v0, v0

    if-ge p1, v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSubArtistLis:[Ljava/lang/String;

    aget-object v0, v0, p1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_1
        0xe -> :sswitch_0
    .end sparse-switch
.end method

.method public getPresentId()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mId:J

    return-wide v0
.end method

.method public getType()B
    .locals 5

    const/4 v0, 0x3

    iget-byte v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mtype:B

    const/16 v2, 0xe

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-wide/16 v1, 0x0

    iget-wide v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSelectId:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0
.end method

.method goDown(J)Z
    .locals 6
    .param p1    # J

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    iget-wide v4, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSelectId:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    const-string v2, "AVRCP_ALBUM"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[BT][AVRCP] goDown reject because has mSelectId:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSelectId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mIdList:[J

    array-length v2, v2

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mIdList:[J

    aget-wide v2, v2, v0

    cmp-long v2, v2, p1

    if-nez v2, :cond_1

    iput-wide p1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSelectId:J

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mContext:Landroid/content/Context;

    invoke-direct {p0, v1, p1, p2}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->updateSubSongs(Landroid/content/Context;J)[J

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const-string v2, "AVRCP_ALBUM"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[BT][AVRCP] goDown fail because not found id:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method goUp()Z
    .locals 4

    const-wide/16 v0, 0x0

    iget-wide v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSelectId:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->resetSubFolder()V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isCategoryRoot()Z
    .locals 5

    const/4 v0, 0x1

    iget-byte v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mtype:B

    const/16 v2, 0xe

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-wide/16 v1, 0x0

    iget-wide v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSelectId:J

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public resetSubFolder()V
    .locals 3

    const/4 v2, 0x0

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSelectId:J

    iput-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mIdSubList:[J

    iput-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSubTitleLis:[Ljava/lang/String;

    iput-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSubArtistLis:[Ljava/lang/String;

    iput-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSubAlbumLis:[Ljava/lang/String;

    iget-byte v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mtype:B

    const/16 v1, 0xe

    if-ne v0, v1, :cond_0

    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->mSelectId:J

    :cond_0
    return-void
.end method
