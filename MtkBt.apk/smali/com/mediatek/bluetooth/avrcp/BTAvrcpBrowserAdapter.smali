.class public Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;
.super Ljava/lang/Thread;
.source "BTAvrcpBrowserAdapter.java"


# static fields
.field private static final ACTION_CHANGE_PATH_DOWN:I = 0x12

.field private static final ACTION_CHANGE_PATH_UP:I = 0x11

.field private static final ACTION_GET_ATTR:I = 0x13

.field private static final ACTION_GET_FILELIST:I = 0x15

.field private static final ACTION_NONE:I = 0x10

.field private static final ACTION_SEARCH:I = 0x14

.field private static final ALBUM_FOLDER_ID:J = 0x102L

.field private static final ARTIST_FOLDER_ID:J = 0x101L

.field private static final DELAY_TIME:I = 0x0

.field private static final FAIL:B = 0x1t

.field private static final NO_EXIST_FOLDER:J = 0xf0fL

.field private static final OK:B = 0x0t

.field private static final ROOT_FOLDER_ID:J = 0x100L

.field public static final STATUS_FAIL:B = 0x1t

.field public static final STATUS_OK:B = 0x4t

.field public static final TAG:Ljava/lang/String; = "BWS_AVRCP"

.field private static final TEST_FOLDER_ID:J = 0x103L

.field private static final UNAWARE_UIDCOUNTER:S = 0x0s

.field private static final UTF8_CHARSET:S = 0x6as


# instance fields
.field private isSearch:Z

.field mAlbumCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

.field mAllCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

.field mArtistCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

.field private mAvrcpServ:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

.field mCategory:[Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

.field mCurCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

.field private mCurFakeFolderID:J

.field private mCurFolderID:J

.field private mCurPathItemNum:I

.field private mCurPaths:[Ljava/lang/String;

.field private mDebug:Z

.field private mDepth:B

.field mEmptyCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

.field private mGetFileAttr:[I

.field private mGetFileAttrCount:I

.field private mHandler:Landroid/os/Handler;

.field private mInsideFakeFolder:Z

.field private mNowAlbum:Ljava/lang/String;

.field private mNowArtist:Ljava/lang/String;

.field private mNowId:J

.field private mNowTtitle:Ljava/lang/String;

.field private mParentID:J

.field private mSearchAlbum:Ljava/lang/String;

.field private mSearchArtist:Ljava/lang/String;

.field private mSearchId:J

.field private mSearchResultList:[J

.field private mSearchText:Ljava/lang/String;

.field private mSearchTtitle:Ljava/lang/String;

.field private volatile mServiceLooper:Landroid/os/Looper;

.field private mUidCounter:S

.field private searchAdapter:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserSearch;


# direct methods
.method constructor <init>(Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;)V
    .locals 9
    .param p1    # Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    const-wide/16 v0, 0x100

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput-boolean v7, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mInsideFakeFolder:Z

    iput-wide v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mParentID:J

    iput-wide v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurFakeFolderID:J

    iput-wide v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurFolderID:J

    iput-short v6, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mUidCounter:S

    iput v8, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurPathItemNum:I

    iput-byte v7, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mDepth:B

    iput-boolean v6, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->isSearch:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mNowId:J

    iput-object v5, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mNowTtitle:Ljava/lang/String;

    iput-object v5, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mNowAlbum:Ljava/lang/String;

    iput-object v5, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mNowArtist:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mSearchId:J

    iput-object v5, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mSearchTtitle:Ljava/lang/String;

    iput-object v5, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mSearchAlbum:Ljava/lang/String;

    iput-object v5, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mSearchArtist:Ljava/lang/String;

    iput-object v5, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    iput v6, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mGetFileAttrCount:I

    iput-object v5, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mGetFileAttr:[I

    iput-object v5, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mServiceLooper:Landroid/os/Looper;

    iput-boolean v6, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mDebug:Z

    iput-object p1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mAvrcpServ:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    iput-boolean v6, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->isSearch:Z

    new-instance v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserSearch;

    invoke-direct {v0, p1}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserSearch;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->searchAdapter:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserSearch;

    const-string v0, "BTAvrcpBrowserAdapterThread"

    invoke-virtual {p0, v0}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    new-instance v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    const-wide/16 v1, 0x1001

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mAvrcpServ:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    invoke-direct {v0, v1, v2, v3, v8}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;-><init>(JLandroid/content/Context;B)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mAlbumCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    new-instance v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    const-wide/16 v1, 0x1002

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mAvrcpServ:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    const/4 v4, 0x3

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;-><init>(JLandroid/content/Context;B)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mArtistCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    new-instance v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    const-wide/16 v1, 0x1003

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mAvrcpServ:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    const/16 v4, 0xf

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;-><init>(JLandroid/content/Context;B)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mEmptyCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    new-instance v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    const-wide/16 v1, 0x1004

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mAvrcpServ:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    const/16 v4, 0xe

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;-><init>(JLandroid/content/Context;B)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mAllCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->checkPTSMode()Z

    move-result v0

    if-ne v7, v0, :cond_0

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    iput-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCategory:[Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCategory:[Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mAllCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    aput-object v1, v0, v6

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCategory:[Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mArtistCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    aput-object v1, v0, v7

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCategory:[Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mAlbumCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    aput-object v1, v0, v8

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCategory:[Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mEmptyCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    aput-object v2, v0, v1

    :goto_0
    iput-object v5, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    invoke-static {}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->isSupportBrowse()Z

    move-result v0

    if-ne v7, v0, :cond_1

    invoke-virtual {p0}, Ljava/lang/Thread;->start()V

    const-string v0, "BWS_AVRCP"

    const-string v1, "[BT][AVRCP] AvrcpBrowse start looper"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return-void

    :cond_0
    new-array v0, v8, [Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    iput-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCategory:[Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCategory:[Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mArtistCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    aput-object v1, v0, v6

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCategory:[Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mAlbumCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    aput-object v1, v0, v7

    goto :goto_0

    :cond_1
    const-string v0, "BWS_AVRCP"

    const-string v1, "[BT][AVRCP] No AvrcpBrowse debug looper"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private checkPTSMode()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method private static getCursorById(Landroid/content/Context;J)Landroid/database/Cursor;
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # J

    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "title"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "artist"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string v1, "album"

    aput-object v1, v2, v0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "is_music"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const-string v5, "track"

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    return-object v6
.end method

.method private handleChangePath(SBJ)V
    .locals 8
    .param p1    # S
    .param p2    # B
    .param p3    # J

    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v0, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    const-string v3, "BWS_AVRCP"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[BT][AVRCP] handleChangePath uid_counter:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " dir:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " uid:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p2, :cond_4

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    invoke-virtual {v3}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->isCategoryRoot()Z

    move-result v3

    if-ne v6, v3, :cond_1

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCategory:[Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    array-length v3, v3

    iput v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurPathItemNum:I

    const-string v3, "BWS_AVRCP"

    const-string v4, "[BT][AVRCP][BWS] handleChangePath goUp to catagory"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    :cond_0
    :goto_0
    const-string v3, "BWS_AVRCP"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[BT][AVRCP] changePath final bRet:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-ne v6, v0, :cond_b

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mAvrcpServ:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    iget v4, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurPathItemNum:I

    invoke-virtual {v3, v7, v2, v4}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->changePathRspNative(BBI)Z

    :goto_1
    return-void

    :cond_1
    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    invoke-virtual {v3}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->goUp()Z

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    invoke-virtual {v3}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->getCurPathItems()I

    move-result v3

    iput v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurPathItemNum:I

    :goto_2
    const-string v3, "BWS_AVRCP"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[BT][AVRCP][BWS] handleChangePath goUp ok num-of-items:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurPathItemNum:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    iput v7, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurPathItemNum:I

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    const/4 v2, 0x7

    goto :goto_0

    :cond_4
    if-ne p2, v6, :cond_a

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    invoke-virtual {v3, p3, p4}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->goDown(J)Z

    move-result v3

    if-ne v6, v3, :cond_5

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    invoke-virtual {v3}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->getCurPathItems()I

    move-result v3

    iput v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurPathItemNum:I

    const-string v3, "BWS_AVRCP"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[BT][AVRCP][BWS] handleChangePath goDown ok:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " num-of-items:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurPathItemNum:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_5
    const/16 v2, 0x8

    const-string v3, "BWS_AVRCP"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[BT][AVRCP][BWS] handleChangePath mCurCategory.goDown fail uid"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_6
    const/4 v1, 0x0

    :goto_3
    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCategory:[Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    array-length v3, v3

    if-ge v1, v3, :cond_7

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCategory:[Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->getPresentId()J

    move-result-wide v3

    cmp-long v3, p3, v3

    if-nez v3, :cond_9

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCategory:[Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    aget-object v3, v3, v1

    iput-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    invoke-virtual {v3}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->resetSubFolder()V

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    invoke-virtual {v3}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->buildSubFolder()V

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    invoke-virtual {v3}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->getCurPathItems()I

    move-result v3

    iput v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurPathItemNum:I

    :goto_4
    const/4 v0, 0x1

    const-string v3, "BWS_AVRCP"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[BT][AVRCP][BWS] handleChangePath down to category ok:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " num:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurPathItemNum:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    if-eq v6, v0, :cond_0

    const-string v3, "BWS_AVRCP"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[BT][AVRCP][BWS] handleChangePath down to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " fail! cannot found "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    const/16 v2, 0x9

    goto/16 :goto_0

    :cond_8
    iput v7, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurPathItemNum:I

    goto :goto_4

    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_a
    const/4 v0, 0x0

    const/16 v2, 0x9

    goto/16 :goto_0

    :cond_b
    packed-switch v2, :pswitch_data_0

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mAvrcpServ:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    const/16 v4, 0x9

    iget v5, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurPathItemNum:I

    invoke-virtual {v3, v4, v2, v5}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->changePathRspNative(BBI)Z

    goto/16 :goto_1

    :pswitch_0
    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mAvrcpServ:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    iget v4, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurPathItemNum:I

    invoke-virtual {v3, v2, v2, v4}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->changePathRspNative(BBI)Z

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private handleGetFileList(III[I)V
    .locals 29
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # [I

    const/16 v27, 0x0

    const/16 v24, 0x0

    const/16 v26, 0x0

    const-string v2, "BWS_AVRCP"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[BT][AVRCP][BWS] handleGetFileList start:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " end:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mAvrcpServ:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    invoke-virtual {v2}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->getFileSystemItemStartRspNative()Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    if-eqz v2, :cond_b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    invoke-virtual {v2}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->getCurrentList()[J

    move-result-object v25

    if-eqz v25, :cond_0

    move-object/from16 v0, v25

    array-length v0, v0

    move/from16 v27, v0

    :cond_0
    if-eqz v25, :cond_1

    move/from16 v0, p1

    move/from16 v1, v27

    if-lt v0, v1, :cond_2

    :cond_1
    const-string v2, "BWS_AVRCP"

    const-string v3, "[BT][AVRCP] handleGetFileList  out-of-bound start:%d list.length:%d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v6

    const/4 v6, 0x1

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mAvrcpServ:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    const/4 v3, 0x1

    const/16 v4, 0xb

    move-object/from16 v0, p0

    iget-short v6, v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mUidCounter:S

    invoke-virtual {v2, v3, v4, v6}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->getFileSystemItemEndRspNative(BBS)Z

    :goto_0
    return-void

    :cond_2
    move-object/from16 v0, v25

    array-length v2, v0

    if-nez v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mAvrcpServ:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    const/4 v3, 0x1

    const/16 v4, 0xb

    move-object/from16 v0, p0

    iget-short v6, v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mUidCounter:S

    invoke-virtual {v2, v3, v4, v6}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->getFileSystemItemEndRspNative(BBS)Z

    goto :goto_0

    :cond_3
    const-string v2, "BWS_AVRCP"

    const-string v3, "[BT][AVRCP] handleGetFileList start:%d end:%d list.length:%d"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v6

    const/4 v6, 0x1

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v6

    const/4 v6, 0x2

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    move/from16 v0, p2

    move/from16 v1, v27

    if-lt v0, v1, :cond_4

    add-int/lit8 p2, v27, -0x1

    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    invoke-virtual {v2}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->getType()B

    move-result v2

    packed-switch v2, :pswitch_data_0

    const-string v2, "BWS_AVRCP"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[BT][AVRCP]  mCurCategory.getType Wrong "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    invoke-virtual {v4}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->getType()B

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mAvrcpServ:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    const/4 v3, 0x1

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget-short v6, v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mUidCounter:S

    invoke-virtual {v2, v3, v4, v6}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->getFileSystemItemEndRspNative(BBS)Z

    goto :goto_0

    :pswitch_0
    sub-int v2, p2, p1

    add-int/lit8 v28, v2, 0x1

    move/from16 v24, p1

    :goto_1
    move/from16 v0, v24

    move/from16 v1, p2

    if-gt v0, v1, :cond_6

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    move/from16 v0, v24

    invoke-virtual {v2, v0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->getNameByIndex(I)Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_5

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v2

    int-to-short v11, v2

    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mAvrcpServ:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    const/4 v3, 0x0

    sub-int v4, v24, p1

    int-to-byte v4, v4

    move/from16 v0, v28

    int-to-byte v5, v0

    aget-wide v6, v25, v24

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    invoke-virtual {v10}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->getFolderType()B

    move-result v8

    const/4 v9, 0x0

    const/16 v10, 0x6a

    invoke-virtual/range {v2 .. v12}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->getFileSystemItemFolderRspNative(BBBJBBSSLjava/lang/String;)Z

    add-int/lit8 v24, v24, 0x1

    goto :goto_1

    :cond_5
    const/4 v11, 0x0

    goto :goto_2

    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mAvrcpServ:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-short v6, v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mUidCounter:S

    invoke-virtual {v2, v3, v4, v6}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->getFileSystemItemEndRspNative(BBS)Z

    goto/16 :goto_0

    :pswitch_1
    sub-int v2, p2, p1

    add-int/lit8 v28, v2, 0x1

    move/from16 v24, p1

    :goto_3
    move/from16 v0, v24

    move/from16 v1, p2

    if-gt v0, v1, :cond_a

    const/4 v5, 0x0

    const/16 v26, 0x0

    :goto_4
    move/from16 v0, v26

    move/from16 v1, p3

    if-ge v0, v1, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    aget v3, p4, v26

    move/from16 v0, v24

    invoke-virtual {v2, v0, v3}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->getAttributeByIndex(II)Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_7

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v2

    int-to-short v8, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mAvrcpServ:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    const/4 v3, 0x0

    sub-int v4, v24, p1

    int-to-byte v4, v4

    aget v6, p4, v26

    const/16 v7, 0x6a

    invoke-virtual/range {v2 .. v9}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->getFileSystemItemFileAttrRspNative(BBBISSLjava/lang/String;)Z

    add-int/lit8 v2, v5, 0x1

    int-to-byte v5, v2

    :cond_7
    add-int/lit8 v26, v26, 0x1

    goto :goto_4

    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    move/from16 v0, v24

    invoke-virtual {v2, v0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->getNameByIndex(I)Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_9

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v2

    int-to-short v11, v2

    :goto_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mAvrcpServ:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    const/4 v14, 0x0

    sub-int v2, v24, p1

    int-to-byte v15, v2

    move/from16 v0, v28

    int-to-byte v0, v0

    move/from16 v16, v0

    aget-wide v17, v25, v24

    const/16 v19, 0x0

    const/16 v20, 0x6a

    move/from16 v21, v11

    move-object/from16 v22, v12

    invoke-virtual/range {v13 .. v22}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->getFileSystemItemFileRspNative(BBBJBSSLjava/lang/String;)Z

    add-int/lit8 v24, v24, 0x1

    goto :goto_3

    :cond_9
    const/4 v11, 0x0

    goto :goto_5

    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mAvrcpServ:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-short v6, v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mUidCounter:S

    invoke-virtual {v2, v3, v4, v6}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->getFileSystemItemEndRspNative(BBS)Z

    goto/16 :goto_0

    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCategory:[Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    array-length v2, v2

    move/from16 v0, p1

    if-le v0, v2, :cond_c

    const-string v2, "BWS_AVRCP"

    const-string v3, "[BT][AVRCP] handleGetFileList out-of-bound start:%d mCategory.length:%d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v6

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCategory:[Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    array-length v7, v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mAvrcpServ:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    const/4 v3, 0x1

    const/16 v4, 0xb

    move-object/from16 v0, p0

    iget-short v6, v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mUidCounter:S

    invoke-virtual {v2, v3, v4, v6}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->getFileSystemItemEndRspNative(BBS)Z

    goto/16 :goto_0

    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCategory:[Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    array-length v2, v2

    move/from16 v0, p2

    if-lt v0, v2, :cond_d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCategory:[Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    array-length v2, v2

    add-int/lit8 p2, v2, -0x1

    :cond_d
    sub-int v2, p2, p1

    add-int/lit8 v28, v2, 0x1

    move/from16 v24, p1

    :goto_6
    move/from16 v0, v24

    move/from16 v1, p2

    if-gt v0, v1, :cond_f

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCategory:[Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    array-length v2, v2

    move/from16 v0, v24

    if-lt v0, v2, :cond_e

    const-string v2, "BWS_AVRCP"

    const-string v3, "[BT][AVRCP][ERR] Out-of-Array mCategory.length:%d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v6

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCategory:[Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    array-length v7, v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mAvrcpServ:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    const/4 v3, 0x1

    const/16 v4, 0xb

    move-object/from16 v0, p0

    iget-short v6, v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mUidCounter:S

    invoke-virtual {v2, v3, v4, v6}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->getFileSystemItemEndRspNative(BBS)Z

    goto/16 :goto_0

    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCategory:[Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    aget-object v2, v2, v24

    invoke-virtual {v2}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->getCategoryName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v2

    int-to-short v11, v2

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mAvrcpServ:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    const/4 v14, 0x0

    sub-int v2, v24, p1

    int-to-byte v15, v2

    move/from16 v0, v28

    int-to-byte v0, v0

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCategory:[Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    aget-object v2, v2, v24

    invoke-virtual {v2}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->getPresentId()J

    move-result-wide v17

    const/16 v19, 0x2

    const/16 v20, 0x0

    const/16 v21, 0x6a

    move/from16 v22, v11

    move-object/from16 v23, v12

    invoke-virtual/range {v13 .. v23}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->getFileSystemItemFolderRspNative(BBBJBBSSLjava/lang/String;)Z

    add-int/lit8 v24, v24, 0x1

    goto :goto_6

    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mAvrcpServ:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-short v6, v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mUidCounter:S

    invoke-virtual {v2, v3, v4, v6}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->getFileSystemItemEndRspNative(BBS)Z

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private handleSearch(Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v0, 0x3

    const/4 v1, 0x4

    const-string v2, "BWS_AVRCP"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[BT][AVRCP] handleSearch "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x1

    iget-boolean v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->isSearch:Z

    if-eq v2, v3, :cond_0

    const-string v2, "BWS_AVRCP"

    const-string v3, "[BT][AVRCP] isSearch false. No return result "

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "BWS_AVRCP"

    const-string v3, "[BT][AVRCP][WRN] handleSearch request empty string !"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mAvrcpServ:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    invoke-virtual {v2, v5, v1, v5, v0}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->searchRspNative(BBSI)Z

    iput-boolean v5, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->isSearch:Z

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->searchAdapter:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserSearch;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->searchAdapter:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserSearch;

    invoke-virtual {v2, p1}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserSearch;->search(Ljava/lang/String;)I

    move-result v0

    :cond_2
    iput-boolean v5, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->isSearch:Z

    iget-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mAvrcpServ:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    invoke-virtual {v2, v5, v1, v5, v0}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->searchRspNative(BBSI)Z

    goto :goto_0
.end method

.method private static query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/net/Uri;
    .param p2    # [Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-static/range {v0 .. v6}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;I)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private static query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;I)Landroid/database/Cursor;
    .locals 8
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/net/Uri;
    .param p2    # [Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # I

    const/4 v7, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v1, v7

    :goto_0
    return-object v1

    :cond_0
    if-lez p6, :cond_1

    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "limit"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p1

    :cond_1
    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception v6

    move-object v1, v7

    goto :goto_0
.end method

.method private sendMyselfMsg(III)Z
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # I

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mHandler:Landroid/os/Handler;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput p1, v0, Landroid/os/Message;->what:I

    iput p2, v0, Landroid/os/Message;->arg1:I

    iput p3, v0, Landroid/os/Message;->arg2:I

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    move-result v1

    goto :goto_0
.end method

.method private sendMyselfMsg(ILjava/lang/String;)Z
    .locals 4
    .param p1    # I
    .param p2    # Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput p1, v0, Landroid/os/Message;->what:I

    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private updateNowSongId(J)Z
    .locals 6
    .param p1    # J

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mAvrcpServ:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    invoke-static {v3, p1, p2}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->getCursorById(Landroid/content/Context;J)Landroid/database/Cursor;

    move-result-object v0

    const-string v3, "BWS_AVRCP"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[BT][AVRCP][BWS] updateNowSongId "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v0, :cond_1

    const-string v2, "BWS_AVRCP"

    const-string v3, "[BT][AVRCP] updateNowSongId got null"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return v1

    :cond_1
    if-eqz v0, :cond_4

    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_3

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mNowTtitle:Ljava/lang/String;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mNowArtist:Ljava/lang/String;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mNowAlbum:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mNowTtitle:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mNowArtist:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mNowAlbum:Ljava/lang/String;

    iput-wide p1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mNowId:J

    const-string v1, "BWS_AVRCP"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[BT][AVRCP] updateNowSongId \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mNowTtitle:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\' \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mNowArtist:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\' \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mNowAlbum:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    const/4 v0, 0x0

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    :try_start_1
    const-string v2, "BWS_AVRCP"

    const-string v3, "[BT][AVRCP] query and get empty result !"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_4
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v1

    if-eqz v0, :cond_5

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    const/4 v0, 0x0

    :cond_5
    throw v1
.end method

.method private updateSearchSongId(J)Z
    .locals 6
    .param p1    # J

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mAvrcpServ:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    invoke-static {v3, p1, p2}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->getCursorById(Landroid/content/Context;J)Landroid/database/Cursor;

    move-result-object v0

    const-string v3, "BWS_AVRCP"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[BT][AVRCP][BWS] updateSearchSongId "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v0, :cond_1

    const-string v2, "BWS_AVRCP"

    const-string v3, "[BT][AVRCP] updateSearchSongId got null"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return v1

    :cond_1
    if-eqz v0, :cond_4

    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_3

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mSearchTtitle:Ljava/lang/String;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mSearchArtist:Ljava/lang/String;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mSearchAlbum:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mSearchTtitle:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mSearchArtist:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mSearchAlbum:Ljava/lang/String;

    iput-wide p1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mSearchId:J

    const-string v1, "BWS_AVRCP"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[BT][AVRCP] updateSearchSongId \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mSearchTtitle:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\' \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mSearchArtist:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\' \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mSearchAlbum:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    const/4 v0, 0x0

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    :try_start_1
    const-string v2, "BWS_AVRCP"

    const-string v3, "[BT][AVRCP] query and get empty result !"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_4
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v1

    if-eqz v0, :cond_5

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    const/4 v0, 0x0

    :cond_5
    throw v1
.end method


# virtual methods
.method public changePath(SBJ)Z
    .locals 5
    .param p1    # S
    .param p2    # B
    .param p3    # J

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    const-string v2, "BWS_AVRCP"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[BT][AVRCP] changePath fail to  uid_counter:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return v1

    :cond_1
    if-nez p1, :cond_2

    const-wide/16 v3, 0x0

    cmp-long v3, p3, v3

    if-nez v3, :cond_2

    if-eqz p2, :cond_2

    const-string v2, "BWS_AVRCP"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[BT][AVRCP] changePath fail  uid_counter:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " dir:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " uid:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    if-ne p2, v2, :cond_3

    const/16 v1, 0x12

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v3}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->sendMyselfMsg(ILjava/lang/String;)Z

    move v1, v2

    goto :goto_0

    :cond_3
    if-nez p2, :cond_0

    const/16 v1, 0x11

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v3}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->sendMyselfMsg(ILjava/lang/String;)Z

    move v1, v2

    goto :goto_0
.end method

.method public checkSongIdExisted(J)Z
    .locals 4
    .param p1    # J

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mAvrcpServ:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    invoke-static {v2, p1, p2}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->getCursorById(Landroid/content/Context;J)Landroid/database/Cursor;

    move-result-object v1

    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-nez v2, :cond_2

    const/4 v0, 0x1

    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    const/4 v1, 0x0

    :cond_1
    return v0

    :cond_2
    if-nez v1, :cond_3

    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public deinit()V
    .locals 3

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mServiceLooper:Landroid/os/Looper;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mServiceLooper:Landroid/os/Looper;

    invoke-virtual {v1}, Landroid/os/Looper;->quit()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mServiceLooper:Landroid/os/Looper;

    :cond_0
    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_1

    const-string v1, "BWS_AVRCP"

    const-string v2, "[BT][AVRCP] mHandler is existed. call join"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Ljava/lang/Thread;->interrupt()V

    const-wide/16 v1, 0x64

    :try_start_0
    invoke-virtual {p0, v1, v2}, Ljava/lang/Thread;->join(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "BWS_AVRCP"

    const-string v2, "[BT][AVRCP] join fail"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getCurPathDepth()B
    .locals 3

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-byte v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mDepth:B

    :goto_0
    const-string v0, "BWS_AVRCP"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[BT][AVRCP] getCurPathDepth mDepth:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-byte v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mDepth:B

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-byte v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mDepth:B

    return v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    invoke-virtual {v0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->getCurPathDepth()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    int-to-byte v0, v0

    iput-byte v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mDepth:B

    goto :goto_0
.end method

.method public getCurPathItems()I
    .locals 3

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCategory:[Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    array-length v0, v0

    iput v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurPathItemNum:I

    :goto_0
    const-string v0, "BWS_AVRCP"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[BT][AVRCP] getCurPathItems mCurPathItemNum:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurPathItemNum:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurPathItemNum:I

    return v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    invoke-virtual {v0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->getCurPathItems()I

    move-result v0

    iput v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurPathItemNum:I

    goto :goto_0
.end method

.method public getCurPaths()[Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    invoke-virtual {v0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->getCurPaths()[Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurPaths:[Ljava/lang/String;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurPaths:[Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurPaths:[Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "root"

    aput-object v2, v0, v1

    :cond_1
    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurPaths:[Ljava/lang/String;

    goto :goto_0
.end method

.method public getFileSystemitemsList(IIB[I)Z
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # B
    .param p4    # [I

    iput p3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mGetFileAttrCount:I

    iput-object p4, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mGetFileAttr:[I

    const/16 v0, 0x15

    invoke-direct {p0, v0, p1, p2}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->sendMyselfMsg(III)Z

    const/4 v0, 0x1

    return v0
.end method

.method getItemAttribute(BJSI)Ljava/lang/String;
    .locals 7
    .param p1    # B
    .param p2    # J
    .param p4    # S
    .param p5    # I

    const/4 v0, 0x0

    const/4 v6, 0x0

    if-eqz p4, :cond_1

    const-string v1, "BWS_AVRCP"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[BT][AVRCP] getItemAttribute wrong uid_counter"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    packed-switch p1, :pswitch_data_0

    iget-wide v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mNowId:J

    cmp-long v0, p2, v0

    if-eqz v0, :cond_2

    invoke-direct {p0, p2, p3}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->updateNowSongId(J)Z

    :cond_2
    iget-wide v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mNowId:J

    cmp-long v0, p2, v0

    if-nez v0, :cond_5

    packed-switch p5, :pswitch_data_1

    :goto_1
    move-object v0, v6

    goto :goto_0

    :pswitch_0
    const-string v0, "Player"

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    move v1, p1

    move-wide v2, p2

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->getItemAttribute(BJSI)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    iget-wide v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mSearchId:J

    cmp-long v0, p2, v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    invoke-direct {p0, p2, p3}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->updateSearchSongId(J)Z

    move-result v1

    if-eq v0, v1, :cond_3

    :cond_3
    iget-wide v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mSearchId:J

    cmp-long v0, p2, v0

    if-nez v0, :cond_4

    packed-switch p5, :pswitch_data_2

    :goto_2
    const-string v0, "BWS_AVRCP"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[BT][AVRCP] getItemAttribute ret:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :pswitch_3
    iget-object v6, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mSearchTtitle:Ljava/lang/String;

    goto :goto_2

    :pswitch_4
    iget-object v6, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mSearchArtist:Ljava/lang/String;

    goto :goto_2

    :pswitch_5
    iget-object v6, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mSearchAlbum:Ljava/lang/String;

    goto :goto_2

    :cond_4
    const-string v0, "BWS_AVRCP"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[BT][AVRCP] fail to updateSearchSongId ! id:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :pswitch_6
    iget-object v6, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mNowTtitle:Ljava/lang/String;

    goto :goto_1

    :pswitch_7
    iget-object v6, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mNowArtist:Ljava/lang/String;

    goto :goto_1

    :pswitch_8
    iget-object v6, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mNowAlbum:Ljava/lang/String;

    goto :goto_1

    :cond_5
    const-string v0, "BWS_AVRCP"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[BT][AVRCP] fail to updateNowSongId ! id:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method getSearchedItemAttribute(II)Ljava/lang/String;
    .locals 1
    .param p1    # I
    .param p2    # I

    packed-switch p2, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->searchAdapter:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserSearch;

    invoke-virtual {v0, p1}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserSearch;->getSearchedTitleString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->searchAdapter:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserSearch;

    invoke-virtual {v0, p1}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserSearch;->getSearchedArtistString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->searchAdapter:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserSearch;

    invoke-virtual {v0, p1}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserSearch;->getSearchedAlbumString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getSearchedList()[J
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->searchAdapter:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserSearch;

    invoke-virtual {v0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserSearch;->getSearchedList()[J

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mSearchResultList:[J

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mSearchResultList:[J

    return-object v0
.end method

.method public getUidCounter()S
    .locals 1

    iget-short v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mUidCounter:S

    return v0
.end method

.method public isItemExist(BJS)Z
    .locals 1
    .param p1    # B
    .param p2    # J
    .param p4    # S

    if-eqz p4, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isSearch()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->isSearch:Z

    return v0
.end method

.method public onConnect()V
    .locals 2

    const-string v0, "BWS_AVRCP"

    const-string v1, "[BT][AVRCP] onConnect"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    invoke-virtual {v0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;->resetSubFolder()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mCurCategory:Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowseAlbum;

    :cond_0
    const/4 v0, 0x1

    sget-boolean v1, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->bSupportBrowse:Z

    if-eq v0, v1, :cond_1

    const-string v0, "BWS_AVRCP"

    const-string v1, "[BT][AVRCP] No Support Avrcp Browse feature"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->isSearch:Z

    return-void
.end method

.method public onDisconnect()V
    .locals 2

    const-string v0, "BWS_AVRCP"

    const-string v1, "[BT][AVRCP] onDisconnect"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onSelect()V
    .locals 2

    const-string v0, "BWS_AVRCP"

    const-string v1, "[BT][AVRCP] OnSelect "

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onUnselect()V
    .locals 2

    const-string v0, "BWS_AVRCP"

    const-string v1, "[BT][AVRCP] OnUnselect "

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public passToHandleMessage(Landroid/os/Message;)V
    .locals 9
    .param p1    # Landroid/os/Message;

    const/4 v8, 0x0

    const/4 v7, 0x1

    const-string v3, "[BT][AVRCP] AVRCPBrowser Receive a msg.what:%d msg.arg1:%d msg.arg2:%d "

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    iget v5, p1, Landroid/os/Message;->what:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    iget v5, p1, Landroid/os/Message;->arg1:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    const/4 v5, 0x2

    iget v6, p1, Landroid/os/Message;->arg2:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "BWS_AVRCP"

    const-string v4, "[BT][AVRCP] AVRCPBWS passToHandleMessage what:%d"

    new-array v5, v7, [Ljava/lang/Object;

    iget v6, p1, Landroid/os/Message;->what:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-boolean v3, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->bDebugMsg:Z

    if-ne v7, v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mAvrcpServ:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    invoke-static {v3, v0, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    :cond_0
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    :pswitch_0
    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mAvrcpServ:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    const-string v4, "[BT][AVRCP][BWS]no Handle msg !"

    invoke-static {v3, v4, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    :goto_0
    :pswitch_1
    return-void

    :pswitch_2
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const-string v3, "BWS_AVRCP"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[BT][AVRCP] compose up id:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v8, v8, v1, v2}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->handleChangePath(SBJ)V

    goto :goto_0

    :pswitch_3
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const-string v3, "BWS_AVRCP"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[BT][AVRCP] compose down id:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v8, v7, v1, v2}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->handleChangePath(SBJ)V

    goto :goto_0

    :pswitch_4
    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mSearchText:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->handleSearch(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_5
    iget v3, p1, Landroid/os/Message;->arg1:I

    iget v4, p1, Landroid/os/Message;->arg2:I

    iget v5, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mGetFileAttrCount:I

    iget-object v6, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mGetFileAttr:[I

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->handleGetFileList(III[I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x10
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public registerNotification(BI)Z
    .locals 1
    .param p1    # B
    .param p2    # I

    const/4 v0, 0x0

    return v0
.end method

.method public run()V
    .locals 2

    const-string v0, "BWS_AVRCP"

    const-string v1, "[BT][AVRCP] browse run"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/os/Looper;->prepare()V

    const-string v0, "BWS_AVRCP"

    const-string v1, "[BT][AVRCP] browse run prepare "

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mServiceLooper:Landroid/os/Looper;

    new-instance v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter$1;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter$1;-><init>(Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mHandler:Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->loop()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method public search(Ljava/lang/String;)Z
    .locals 5
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x1

    const/4 v1, 0x0

    const-string v3, "BWS_AVRCP"

    const-string v4, "[BT][AVRCP] search"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_0

    const-string v3, ""

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-boolean v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->isSearch:Z

    if-eqz v3, :cond_2

    const-string v2, "BWS_AVRCP"

    const-string v3, "[BT][AVRCP] search is in progress"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    sget-boolean v3, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->bSupportBrowse:Z

    if-ne v2, v3, :cond_5

    invoke-virtual {p0}, Ljava/lang/Thread;->isAlive()Z

    move-result v3

    if-eqz v3, :cond_3

    :goto_1
    invoke-virtual {p0}, Ljava/lang/Thread;->isAlive()Z

    move-result v3

    if-eqz v3, :cond_4

    iput-boolean v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->isSearch:Z

    iput-object p1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->mSearchText:Ljava/lang/String;

    const/16 v3, 0x14

    invoke-direct {p0, v3, v1, v1}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->sendMyselfMsg(III)Z

    move v1, v2

    goto :goto_0

    :cond_3
    invoke-static {}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->yield()V

    const-wide/16 v3, 0x2710

    :try_start_0
    invoke-static {v3, v4}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpBrowserAdapter;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    const-string v3, "BWS_AVRCP"

    const-string v4, "[BT][AVRCP] delay search 10000"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v3, "BWS_AVRCP"

    const-string v4, "[BT][AVRCP] sleep fail"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_4
    const-string v2, "BWS_AVRCP"

    const-string v3, "[BT][AVRCP] Thread not start yets!"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_5
    const-string v2, "BWS_AVRCP"

    const-string v3, "[BT][AVRCP] search fail because bSupportBrowse is false"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
