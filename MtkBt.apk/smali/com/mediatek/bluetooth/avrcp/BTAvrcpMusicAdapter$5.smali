.class Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter$5;
.super Ljava/lang/Object;
.source "BTAvrcpMusicAdapter.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;


# direct methods
.method constructor <init>(Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter$5;->this$0:Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 8
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Landroid/os/IBinder;

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    const-string v2, "MMI_AVRCP"

    const-string v3, "[BT][AVRCP][b] onServiceConnected className:%s"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter$5;->this$0:Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;

    invoke-static {p2}, Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic$Stub;->asInterface(Landroid/os/IBinder;)Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->access$202(Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;)Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;

    iget-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter$5;->this$0:Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;

    invoke-static {v2}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->access$300(Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;)Z

    move-result v2

    if-ne v2, v7, :cond_0

    iget-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter$5;->this$0:Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;

    invoke-static {v2}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->access$400(Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;)Landroid/content/Context;

    move-result-object v2

    const-string v3, "[BT][AVRCP] MusicService onConnected"

    invoke-static {v2, v3, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter$5;->this$0:Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;

    invoke-static {v2}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->access$200(Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;)Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string v2, "MMI_AVRCP"

    const-string v3, "[BT][AVRCP][b] registercallback"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter$5;->this$0:Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;

    invoke-static {v2}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->access$200(Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;)Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter$5;->this$0:Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;

    invoke-static {v3}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->access$500(Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;)Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter$AvrcpMusicAdapterStub;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;->registerCallback(Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusicCallback;)V

    const-string v2, "MMI_AVRCP"

    const-string v3, "[BT][AVRCP] mMusicService.getAudioId:%d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter$5;->this$0:Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;

    invoke-static {v6}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->access$200(Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;)Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;

    move-result-object v6

    invoke-interface {v6}, Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;->getAudioId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    const-string v2, "MMI_AVRCP"

    const-string v3, "[BT][AVRCP] mMusicService.getAudioId:null"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 6
    .param p1    # Landroid/content/ComponentName;

    const/4 v5, 0x1

    const/4 v4, 0x0

    const-string v0, "MMI_AVRCP"

    const-string v1, "[BT][AVRCP][b] onServiceDisconnected className:%s"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter$5;->this$0:Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;

    invoke-static {v0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->access$200(Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;)Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "MMI_AVRCP"

    const-string v1, "[BT][AVRCP][b] unregistercallback "

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter$5;->this$0:Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;

    invoke-static {v0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->access$200(Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;)Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter$5;->this$0:Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;

    invoke-static {v1}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->access$500(Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;)Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter$AvrcpMusicAdapterStub;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;->unregisterCallback(Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusicCallback;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter$5;->this$0:Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->access$202(Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;)Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter$5;->this$0:Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;

    invoke-static {v0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->access$300(Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;)Z

    move-result v0

    if-ne v0, v5, :cond_1

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter$5;->this$0:Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;

    invoke-static {v0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->access$400(Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "[BT][AVRCP] MusicService onDisconnected !"

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_1
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
