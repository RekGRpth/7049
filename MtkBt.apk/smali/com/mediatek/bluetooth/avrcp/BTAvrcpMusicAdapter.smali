.class public Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;
.super Ljava/lang/Thread;
.source "BTAvrcpMusicAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter$AvrcpMusicAdapterStub;
    }
.end annotation


# static fields
.field public static final AVRCP_PLAY_PAUSE:I = 0x2

.field public static final AVRCP_PLAY_PLAYING:I = 0x1

.field public static final AVRCP_PLAY_STOP:I = 0x0

.field public static final DEFAULT_METADATA_STRING:Ljava/lang/String; = " "

.field public static final LAST:I = 0x3

.field public static final META_CHANGED:Ljava/lang/String; = "com.android.music.metachanged"

.field public static final NEXT:I = 0x2

.field public static final NOW:I = 0x1

.field public static final PLAYBACK_COMPLETE:Ljava/lang/String; = "com.android.music.playbackcomplete"

.field public static final PLAYSTATE_CHANGED:Ljava/lang/String; = "com.android.music.playstatechanged"

.field private static final PLAY_UPDATE_PERIOD:I = 0x3e8

.field public static final QUEUE_CHANGED:Ljava/lang/String; = "com.android.music.queuechanged"

.field public static final QUIT_PLAYBACK:Ljava/lang/String; = "com.android.music.quitplayback"

.field private static final STATUS_FWD_SEEK:I = 0x3

.field private static final STATUS_PAUSED:I = 0x2

.field private static final STATUS_PLAYING:I = 0x1

.field private static final STATUS_REV_SEEK:I = 0x4

.field private static final STATUS_STOPPED:I = 0x0

.field public static final TAG:Ljava/lang/String; = "MMI_AVRCP"

.field private static mExtraAttribute:Z

.field private static mMusicAlbum:Ljava/lang/String;

.field private static mMusicArtist:Ljava/lang/String;

.field private static mMusicId:J

.field private static mMusicPlaying:Z

.field private static mMusicTrack:Ljava/lang/String;

.field private static mPlayStatus:I

.field private static mPreviousFFPlayStatus:I

.field private static mPreviousPlayStatus:I

.field private static mStartReceiver:Z

.field private static mbPlayServiceInterface:Z


# instance fields
.field private final ACTION_KEY:I

.field private final ACTION_KEY_INFO:I

.field private final ACTION_REG_NOTIFY:I

.field private final ACTION_SET_SETTING:I

.field private avrcpSrv:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

.field private mAdapterCallback:Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter$AvrcpMusicAdapterStub;

.field private mAddToNowList:[J

.field private mAttrs:[B

.field private mAudioMax:I

.field private mAudioMgr:Landroid/media/AudioManager;

.field private mCapabilities:[B

.field private mConnected:Z

.field private mConnection:Landroid/content/ServiceConnection;

.field private mContext:Landroid/content/Context;

.field public mCurEqualSetting:B

.field public mCurRepeatSetting:B

.field public mCurScanSetting:B

.field public mCurShuffleSetting:B

.field private mCurValue:[B

.field private mDebug:Z

.field private mHandler:Landroid/os/Handler;

.field private mInitCapability:Z

.field private mMusicService:Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;

.field private mNotifySongId:J

.field private mPendingRegBit:Ljava/util/BitSet;

.field private mPlayConnection:Landroid/content/ServiceConnection;

.field private mPlayService:Lcom/android/music/IMediaPlaybackService;

.field private mPlayerStatus:B

.field private mRegBit:Ljava/util/BitSet;

.field private volatile mServiceLooper:Landroid/os/Looper;

.field private mStatusListener:Landroid/content/BroadcastReceiver;

.field private mSystemListener:Lcom/mediatek/bluetooth/avrcp/BTAvrcpSystemListener;

.field private mUpdateSending:Z

.field private mValueNum:[B

.field private mValuesEqualizer:[B

.field private mValuesRepeat:[B

.field private mValuesScan:[B

.field private mValuesShuffle:[B

.field private mVolume:B

.field private mbPlayStartBind:Z

.field private mbStartBind:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    sput-boolean v2, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mStartReceiver:Z

    sput-object v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicArtist:Ljava/lang/String;

    sput-object v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicAlbum:Ljava/lang/String;

    sput-object v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicTrack:Ljava/lang/String;

    const-wide/16 v0, -0x1

    sput-wide v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicId:J

    sput-boolean v2, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicPlaying:Z

    sput-boolean v3, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mExtraAttribute:Z

    sput-boolean v3, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mbPlayServiceInterface:Z

    sput v2, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayStatus:I

    sput v2, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPreviousPlayStatus:I

    sput v3, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPreviousFFPlayStatus:I

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    const/16 v5, 0x12

    const/4 v4, 0x3

    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->avrcpSrv:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    iput-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mAudioMgr:Landroid/media/AudioManager;

    const/16 v0, 0x64

    iput v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mAudioMax:I

    const/4 v0, 0x2

    iput-byte v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayerStatus:B

    iput-byte v5, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mVolume:B

    iput-byte v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mCurEqualSetting:B

    iput-byte v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mCurRepeatSetting:B

    iput-byte v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mCurShuffleSetting:B

    iput-byte v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mCurScanSetting:B

    const/16 v0, 0x11

    iput v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->ACTION_KEY:I

    iput v5, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->ACTION_SET_SETTING:I

    const/16 v0, 0x21

    iput v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->ACTION_KEY_INFO:I

    const/16 v0, 0x22

    iput v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->ACTION_REG_NOTIFY:I

    iput-boolean v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mbStartBind:Z

    iput-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mServiceLooper:Landroid/os/Looper;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mNotifySongId:J

    iput-boolean v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mbPlayStartBind:Z

    iput-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayService:Lcom/android/music/IMediaPlaybackService;

    iput-boolean v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mConnected:Z

    iput-boolean v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mUpdateSending:Z

    iput-boolean v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mInitCapability:Z

    new-instance v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter$2;

    invoke-direct {v0, p0, p0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter$2;-><init>(Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mSystemListener:Lcom/mediatek/bluetooth/avrcp/BTAvrcpSystemListener;

    iput-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicService:Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;

    iput-boolean v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mDebug:Z

    new-instance v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter$3;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter$3;-><init>(Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mStatusListener:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter$4;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter$4;-><init>(Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayConnection:Landroid/content/ServiceConnection;

    new-instance v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter$5;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter$5;-><init>(Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mConnection:Landroid/content/ServiceConnection;

    new-instance v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter$AvrcpMusicAdapterStub;

    invoke-direct {v0, p0, p0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter$AvrcpMusicAdapterStub;-><init>(Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mAdapterCallback:Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter$AvrcpMusicAdapterStub;

    iput-object p1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->avrcpSrv:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mAudioMgr:Landroid/media/AudioManager;

    const-string v0, "BTAvrcpMusicAdapterThread "

    invoke-virtual {p0, v0}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mAudioMgr:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mAudioMgr:Landroid/media/AudioManager;

    invoke-virtual {v0, v4}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v0

    iput v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mAudioMax:I

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mAudioMgr:Landroid/media/AudioManager;

    invoke-virtual {v0, v4}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->convertToAbosoluteVolume(I)B

    move-result v0

    iput-byte v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mVolume:B

    :goto_0
    const/16 v0, 0x3f

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->convertToMgrVolume(B)I

    const/16 v0, 0x7f

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->convertToMgrVolume(B)I

    iget v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mAudioMax:I

    div-int/lit8 v0, v0, 0x2

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->convertToAbosoluteVolume(I)B

    iget v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mAudioMax:I

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->convertToAbosoluteVolume(I)B

    const-string v0, "MMI_AVRCP"

    const-string v1, "[BT][AVRCP] BTAvrcpMusicAdapter construct"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->checkCapability()V

    sput-boolean v2, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mStartReceiver:Z

    return-void

    :cond_0
    iput-byte v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mVolume:B

    iput v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mAudioMax:I

    goto :goto_0
.end method

.method static synthetic access$000()I
    .locals 1

    sget v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPreviousPlayStatus:I

    return v0
.end method

.method static synthetic access$002(I)I
    .locals 0
    .param p0    # I

    sput p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPreviousPlayStatus:I

    return p0
.end method

.method static synthetic access$102(Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;Lcom/android/music/IMediaPlaybackService;)Lcom/android/music/IMediaPlaybackService;
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;
    .param p1    # Lcom/android/music/IMediaPlaybackService;

    iput-object p1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayService:Lcom/android/music/IMediaPlaybackService;

    return-object p1
.end method

.method static synthetic access$200(Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;)Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicService:Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;

    return-object v0
.end method

.method static synthetic access$202(Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;)Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;
    .param p1    # Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;

    iput-object p1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicService:Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;

    return-object p1
.end method

.method static synthetic access$300(Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;)Z
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;

    iget-boolean v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mDebug:Z

    return v0
.end method

.method static synthetic access$400(Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;)Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter$AvrcpMusicAdapterStub;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mAdapterCallback:Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter$AvrcpMusicAdapterStub;

    return-object v0
.end method

.method static synthetic access$600(Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;)Ljava/util/BitSet;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mRegBit:Ljava/util/BitSet;

    return-object v0
.end method

.method static synthetic access$700(Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;)Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->avrcpSrv:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    return-object v0
.end method

.method private checkCapability()V
    .locals 11

    const/16 v10, 0xe

    const/4 v9, 0x3

    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/16 v2, 0xa

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->getSupportVersion()B

    move-result v2

    iget-boolean v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mInitCapability:Z

    if-ne v6, v3, :cond_0

    const-string v3, "MMI_AVRCP"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[BT][AVRCP] version:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/Byte;->toString(B)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const-string v3, "MMI_AVRCP"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[BT][AVRCP] init capability version:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/Byte;->toString(B)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v6, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mInitCapability:Z

    new-array v3, v7, [B

    iput-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mAttrs:[B

    new-array v3, v7, [B

    iput-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mValueNum:[B

    const/4 v3, 0x4

    new-array v3, v3, [B

    iput-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mCurValue:[B

    const/4 v0, 0x0

    if-ne v2, v10, :cond_2

    const/4 v3, 0x5

    new-array v3, v3, [B

    iput-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mCapabilities:[B

    :goto_1
    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mCapabilities:[B

    int-to-byte v1, v6

    aput-byte v6, v3, v0

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mCapabilities:[B

    add-int/lit8 v4, v1, 0x1

    int-to-byte v0, v4

    aput-byte v7, v3, v1

    if-ne v2, v10, :cond_1

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mCapabilities:[B

    add-int/lit8 v4, v0, 0x1

    int-to-byte v1, v4

    const/16 v4, 0x9

    aput-byte v4, v3, v0

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mCapabilities:[B

    add-int/lit8 v4, v1, 0x1

    int-to-byte v0, v4

    const/16 v4, 0xa

    aput-byte v4, v3, v1

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mCapabilities:[B

    add-int/lit8 v4, v0, 0x1

    int-to-byte v1, v4

    const/16 v4, 0xb

    aput-byte v4, v3, v0

    move v0, v1

    :cond_1
    const/4 v0, 0x0

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mAttrs:[B

    int-to-byte v1, v6

    aput-byte v7, v3, v0

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mAttrs:[B

    add-int/lit8 v4, v1, 0x1

    int-to-byte v0, v4

    aput-byte v9, v3, v1

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mValueNum:[B

    int-to-byte v1, v6

    aput-byte v7, v3, v0

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mValueNum:[B

    add-int/lit8 v4, v1, 0x1

    int-to-byte v0, v4

    aput-byte v7, v3, v1

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mCurValue:[B

    aput-byte v6, v3, v8

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mCurValue:[B

    aput-byte v6, v3, v6

    new-array v3, v9, [B

    iput-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mValuesRepeat:[B

    new-array v3, v7, [B

    iput-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mValuesShuffle:[B

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mValuesRepeat:[B

    aput-byte v6, v3, v8

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mValuesRepeat:[B

    aput-byte v7, v3, v6

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mValuesRepeat:[B

    aput-byte v9, v3, v7

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mValuesShuffle:[B

    aput-byte v6, v3, v8

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mValuesShuffle:[B

    aput-byte v7, v3, v6

    new-instance v3, Ljava/util/BitSet;

    const/16 v4, 0x10

    invoke-direct {v3, v4}, Ljava/util/BitSet;-><init>(I)V

    iput-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mRegBit:Ljava/util/BitSet;

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mRegBit:Ljava/util/BitSet;

    invoke-virtual {v3}, Ljava/util/BitSet;->clear()V

    new-instance v3, Ljava/util/BitSet;

    const/16 v4, 0x10

    invoke-direct {v3, v4}, Ljava/util/BitSet;-><init>(I)V

    iput-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPendingRegBit:Ljava/util/BitSet;

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPendingRegBit:Ljava/util/BitSet;

    invoke-virtual {v3}, Ljava/util/BitSet;->clear()V

    sget-boolean v3, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->bSupportMusicUI:Z

    if-ne v6, v3, :cond_3

    invoke-virtual {p0}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    :cond_2
    new-array v3, v7, [B

    iput-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mCapabilities:[B

    goto/16 :goto_1

    :cond_3
    const-string v3, "MMI_AVRCP"

    const-string v4, "[BT][AVRCP] No AvrcpMusic debug looper"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private checkPlayStatusChange()V
    .locals 5

    const/4 v4, 0x1

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->getPlayerstatus()B

    move-result v1

    sget v2, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPreviousPlayStatus:I

    if-eq v2, v1, :cond_1

    sput v1, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPreviousPlayStatus:I

    const/4 v0, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->passNotifyMsg(II)Z

    move-result v2

    if-eq v4, v2, :cond_0

    const-string v2, "MMI_AVRCP"

    const-string v3, "[BT][AVRCP] onReceive EVENT_TRACK_CHANGED fail"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x1

    sget v2, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayStatus:I

    invoke-virtual {p0, v0, v2}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->passNotifyMsg(II)Z

    move-result v2

    if-eq v4, v2, :cond_1

    const-string v2, "MMI_AVRCP"

    const-string v3, "[BT][AVRCP] onReceive EVENT_PLAYBACK_STATUS_CHANGED fail"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void
.end method

.method private convertToAbosoluteVolume(I)B
    .locals 6
    .param p1    # I

    const/4 v0, 0x0

    int-to-float v1, p1

    iget v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mAudioMax:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    const/high16 v2, 0x42fe0000

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-byte v0, v1

    const-string v1, "MMI_AVRCP"

    const-string v2, "[BT][AVRCP] Adapter convertToAbosoluteVolume Mgr(%d) to abs(%d) MaxMgr(%d)"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget v5, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mAudioMax:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method private convertToMgrVolume(B)I
    .locals 6
    .param p1    # B

    const/4 v0, 0x0

    int-to-float v1, p1

    const/high16 v2, 0x42fe0000

    div-float/2addr v1, v2

    iget v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mAudioMax:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-int v0, v1

    const-string v1, "MMI_AVRCP"

    const-string v2, "[BT][AVRCP] Adapter convertToMgrVolume absolute(%d) to Mgr(%d) MaxMgr(%d)"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget v5, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mAudioMax:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method private handleKeyMessage(Landroid/os/Message;)V
    .locals 14
    .param p1    # Landroid/os/Message;

    const/4 v13, 0x2

    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    iget v5, p1, Landroid/os/Message;->what:I

    sparse-switch v5, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    const-string v5, "[BT][AVRCP] Receive a Avrcpkey:%d "

    new-array v6, v10, [Ljava/lang/Object;

    iget v7, p1, Landroid/os/Message;->arg1:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "MMI_AVRCP"

    const-string v6, "[BT][AVRCP] ACTION_KEY msg.what:%d arg1:%d arg2:%d"

    new-array v7, v11, [Ljava/lang/Object;

    iget v8, p1, Landroid/os/Message;->what:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    iget v8, p1, Landroid/os/Message;->arg1:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v10

    iget v8, p1, Landroid/os/Message;->arg2:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v13

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mContext:Landroid/content/Context;

    invoke-static {v5, v4, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->handleKeyMessageKeyEvent(Landroid/os/Message;)V

    goto :goto_0

    :sswitch_1
    const-string v5, "MMI_AVRCP"

    const-string v6, "[BT][AVRCP] KEY_INFO msg.what:%d arg1:%d arg2:%d"

    new-array v7, v11, [Ljava/lang/Object;

    iget v8, p1, Landroid/os/Message;->what:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    iget v8, p1, Landroid/os/Message;->arg1:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v10

    iget v8, p1, Landroid/os/Message;->arg2:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v13

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v5, p1, Landroid/os/Message;->arg1:I

    packed-switch v5, :pswitch_data_0

    const-string v5, "KeyCode:%d"

    new-array v6, v10, [Ljava/lang/Object;

    iget v7, p1, Landroid/os/Message;->arg1:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :pswitch_0
    const-string v4, "POWER Key"

    goto :goto_0

    :pswitch_1
    const-string v4, "VOLUME UP"

    goto/16 :goto_0

    :pswitch_2
    const-string v4, "VOLUME DOWN"

    goto/16 :goto_0

    :pswitch_3
    const-string v4, "MUTE"

    goto/16 :goto_0

    :pswitch_4
    const-string v4, "PLAY"

    goto/16 :goto_0

    :pswitch_5
    const-string v4, "STOP"

    goto/16 :goto_0

    :pswitch_6
    const-string v4, "PAUSE"

    goto/16 :goto_0

    :pswitch_7
    const-string v4, "RECORD"

    goto/16 :goto_0

    :pswitch_8
    const-string v4, "REWIND"

    iget v5, p1, Landroid/os/Message;->arg2:I

    if-nez v5, :cond_1

    sget v5, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayStatus:I

    if-ne v5, v12, :cond_0

    const-string v5, "MMI_AVRCP"

    const-string v6, "[AVRCP] back to playing status from rev_seek"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget v5, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPreviousFFPlayStatus:I

    sput v5, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayStatus:I

    const-string v5, "MMI_AVRCP"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[BT][AVRCP] update-info back mPlayStatus:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget v7, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayStatus:I

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_1
    invoke-direct {p0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->checkPlayStatusChange()V

    goto/16 :goto_0

    :cond_1
    sget v5, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayStatus:I

    if-eq v5, v12, :cond_2

    sget v5, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayStatus:I

    if-ne v5, v11, :cond_3

    :cond_2
    sput v10, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPreviousFFPlayStatus:I

    :goto_2
    sput v12, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayStatus:I

    goto :goto_1

    :cond_3
    sget v5, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayStatus:I

    sput v5, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPreviousFFPlayStatus:I

    goto :goto_2

    :pswitch_9
    const-string v4, "FAST FORWARD"

    iget v5, p1, Landroid/os/Message;->arg2:I

    if-nez v5, :cond_5

    sget v5, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayStatus:I

    if-ne v5, v11, :cond_4

    const-string v5, "MMI_AVRCP"

    const-string v6, "[AVRCP] back to playing status from fwd_seek"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget v5, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPreviousFFPlayStatus:I

    sput v5, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayStatus:I

    const-string v5, "MMI_AVRCP"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[BT][AVRCP] update-info back mPlayStatus:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget v7, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayStatus:I

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    :goto_3
    invoke-direct {p0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->checkPlayStatusChange()V

    goto/16 :goto_0

    :cond_5
    sget v5, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayStatus:I

    if-eq v5, v12, :cond_6

    sget v5, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayStatus:I

    if-ne v5, v11, :cond_7

    :cond_6
    sput v10, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPreviousFFPlayStatus:I

    :goto_4
    sput v11, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayStatus:I

    goto :goto_3

    :cond_7
    sget v5, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayStatus:I

    sput v5, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPreviousFFPlayStatus:I

    goto :goto_4

    :pswitch_a
    const-string v4, "EJECT"

    goto/16 :goto_0

    :pswitch_b
    const-string v4, "FORWARD"

    goto/16 :goto_0

    :pswitch_c
    const-string v4, "BACKWARD"

    goto/16 :goto_0

    :sswitch_2
    const-string v5, "MMI_AVRCP"

    const-string v6, "[BT][AVRCP] ACTION_REG_NOTIFY for notifyChange msg.what:%d arg1:%d arg2:%d cardinality:%d"

    new-array v7, v12, [Ljava/lang/Object;

    iget v8, p1, Landroid/os/Message;->what:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    iget v8, p1, Landroid/os/Message;->arg1:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v10

    iget v8, p1, Landroid/os/Message;->arg2:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v13

    iget-object v8, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mRegBit:Ljava/util/BitSet;

    invoke-virtual {v8}, Ljava/util/BitSet;->cardinality()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v11

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mRegBit:Ljava/util/BitSet;

    monitor-enter v6

    :try_start_0
    iget v5, p1, Landroid/os/Message;->arg1:I

    sparse-switch v5, :sswitch_data_1

    :cond_8
    :goto_5
    monitor-exit v6

    goto/16 :goto_0

    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    :sswitch_3
    const/4 v1, 0x1

    :try_start_1
    iget-object v5, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mRegBit:Ljava/util/BitSet;

    invoke-virtual {v5, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v5

    if-ne v10, v5, :cond_8

    iget-object v5, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->avrcpSrv:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    const/4 v7, 0x0

    const/4 v8, 0x0

    iget v9, p1, Landroid/os/Message;->arg1:I

    int-to-byte v9, v9

    invoke-virtual {v5, v7, v8, v9}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->notificationPlayStatusChangedNative(BBB)Z

    iget-object v5, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mRegBit:Ljava/util/BitSet;

    invoke-virtual {v5, v1}, Ljava/util/BitSet;->clear(I)V

    goto :goto_5

    :sswitch_4
    const/4 v1, 0x2

    iget-object v5, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mRegBit:Ljava/util/BitSet;

    invoke-virtual {v5, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v5

    if-ne v10, v5, :cond_8

    sget-wide v7, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicId:J

    iput-wide v7, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mNotifySongId:J

    const-string v5, "MMI_AVRCP"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[BT][AVRCP] songid:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mNotifySongId:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->avrcpSrv:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    const/4 v7, 0x0

    const/4 v8, 0x0

    iget-wide v9, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mNotifySongId:J

    invoke-virtual {v5, v7, v8, v9, v10}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->notificationTrackChangedNative(BBJ)Z

    iget-object v5, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mRegBit:Ljava/util/BitSet;

    invoke-virtual {v5, v1}, Ljava/util/BitSet;->clear(I)V

    goto :goto_5

    :sswitch_5
    const/16 v1, 0x9

    iget-object v5, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mRegBit:Ljava/util/BitSet;

    invoke-virtual {v5, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v5

    if-ne v10, v5, :cond_8

    iget-object v5, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->avrcpSrv:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v5, v7, v8}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->notificationNowPlayingChangedNative(BB)Z

    iget-object v5, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mRegBit:Ljava/util/BitSet;

    invoke-virtual {v5, v1}, Ljava/util/BitSet;->clear(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_5

    :sswitch_data_0
    .sparse-switch
        0x11 -> :sswitch_0
        0x21 -> :sswitch_1
        0x22 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x40
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch

    :sswitch_data_1
    .sparse-switch
        0x1 -> :sswitch_3
        0x2 -> :sswitch_4
        0x9 -> :sswitch_5
    .end sparse-switch
.end method

.method private handleKeyMessageKeyEvent(Landroid/os/Message;)V
    .locals 8
    .param p1    # Landroid/os/Message;

    const/4 v7, 0x0

    const/4 v6, 0x1

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicService:Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    iget v1, p1, Landroid/os/Message;->arg1:I

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    const-string v1, "MMI_AVRCP"

    const-string v2, "[BT][AVRCP] Unhandle AvrcpKey:%d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p1, Landroid/os/Message;->what:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "MMI_AVRCP"

    const-string v2, "[BT][AVRCP] AVRCP fail to passToHandleMessage what:%d"

    new-array v3, v6, [Ljava/lang/Object;

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_1
    :try_start_1
    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicService:Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;

    invoke-interface {v1}, Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;->getPlayStatus()B

    move-result v1

    if-eq v6, v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicService:Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;

    invoke-interface {v1}, Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;->play()V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicService:Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;

    invoke-interface {v1}, Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;->getPlayStatus()B

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicService:Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;

    invoke-interface {v1}, Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;->stop()V

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicService:Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;

    invoke-interface {v1}, Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;->getPlayStatus()B

    move-result v1

    if-ne v6, v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicService:Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;

    invoke-interface {v1}, Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;->pause()V

    goto :goto_0

    :pswitch_4
    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicService:Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;

    invoke-interface {v1}, Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;->next()V

    goto :goto_0

    :pswitch_5
    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicService:Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;

    invoke-interface {v1}, Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;->prev()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x44
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private handleSettingMessage(Landroid/os/Message;)V
    .locals 0
    .param p1    # Landroid/os/Message;

    return-void
.end method

.method public static hasStartReceiver()Z
    .locals 1

    sget-boolean v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mStartReceiver:Z

    return v0
.end method

.method private sendHandlerMessageDelayed(IIIJ)Z
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # J

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mContext:Landroid/content/Context;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mHandler:Landroid/os/Handler;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput p1, v0, Landroid/os/Message;->what:I

    iput p2, v0, Landroid/os/Message;->arg1:I

    iput p3, v0, Landroid/os/Message;->arg2:I

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v0, p4, p5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v3, "MMI_AVRCP"

    const-string v4, "[BT][AVRCP] sendMessageDelayed fail ! "

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move v2, v1

    :goto_0
    return v2

    :cond_1
    move v2, v1

    goto :goto_0
.end method

.method public static updateMusicTrackInfo(Landroid/content/Intent;)V
    .locals 10
    .param p0    # Landroid/content/Intent;

    const/4 v4, 0x1

    const/4 v3, 0x0

    if-eqz p0, :cond_2

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v5, "com.android.music.metachanged"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "artist"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicArtist:Ljava/lang/String;

    const-string v2, "album"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicAlbum:Ljava/lang/String;

    const-string v2, "track"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicTrack:Ljava/lang/String;

    const-string v2, "id"

    const-wide/16 v5, -0x1

    invoke-virtual {p0, v2, v5, v6}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v5

    sput-wide v5, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicId:J

    const-string v2, "MMI_AVRCP"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[BT][AVRCP] update-info id:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%d"

    new-array v7, v4, [Ljava/lang/Object;

    sget-wide v8, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicId:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v7, v3

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v5, "com.android.music.playstatechanged"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "playing"

    invoke-virtual {p0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v2, "playstate"

    invoke-virtual {p0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_1
    move v2, v4

    :goto_0
    sput-boolean v2, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicPlaying:Z

    const-string v2, "MMI_AVRCP"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[BT][AVRCP] update-info playing:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    invoke-static {v6}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    invoke-static {v6}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    sget-object v2, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicArtist:Ljava/lang/String;

    if-eqz v2, :cond_4

    const-string v2, "MMI_AVRCP"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[BT][AVRCP] track-info artist:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicArtist:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " isPlaying:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%b"

    new-array v4, v4, [Ljava/lang/Object;

    sget-boolean v7, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicPlaying:Z

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v4, v3

    invoke-static {v6, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " id:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-wide v4, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return-void

    :cond_3
    move v2, v3

    goto :goto_0

    :cond_4
    const-string v2, "MMI_AVRCP"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[BT][AVRCP] track-info isPlaying:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%b"

    new-array v4, v4, [Ljava/lang/Object;

    sget-boolean v7, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicPlaying:Z

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v4, v3

    invoke-static {v6, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " id:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-wide v4, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private static updateNewPlayStatus(I)V
    .locals 6
    .param p0    # I

    const/4 v5, 0x4

    const/4 v4, 0x3

    sget v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayStatus:I

    const-string v1, "MMI_AVRCP"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[BT][AVRCP] updateNewPlayStatuso status:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayStatus:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " newStatus:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    if-eq v4, p0, :cond_0

    if-eq v5, p0, :cond_0

    const/4 v1, 0x1

    if-ne v1, p0, :cond_3

    :cond_0
    const/4 p0, 0x1

    sget v1, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayStatus:I

    if-eq v4, v1, :cond_1

    if-eq v5, p0, :cond_1

    sput p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayStatus:I

    :cond_1
    :goto_0
    sget v1, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayStatus:I

    if-eq v0, v1, :cond_2

    const-string v1, "MMI_AVRCP"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[BT][AVRCP] update-info new mPlayStatus:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayStatus:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void

    :cond_3
    sput p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayStatus:I

    goto :goto_0
.end method


# virtual methods
.method public abortContinueInd()V
    .locals 2

    const-string v0, "MMI_AVRCP"

    const-string v1, "Receive an abort indication !"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public addToNowPlaying(J)Z
    .locals 10
    .param p1    # J

    const/4 v3, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->checkAndBindPlayService()V

    iget-object v4, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayService:Lcom/android/music/IMediaPlaybackService;

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    :try_start_0
    new-array v0, v4, [J

    const/4 v4, 0x0

    aput-wide p1, v0, v4

    iget-object v4, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayService:Lcom/android/music/IMediaPlaybackService;

    const/4 v5, 0x3

    invoke-interface {v4, v0, v5}, Lcom/android/music/IMediaPlaybackService;->enqueue([JI)V

    const-string v4, "MMI_AVRCP"

    const-string v5, "[BT][AVRCP] enqueu %d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const/4 v8, 0x0

    aget-wide v8, v0, v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v3, 0x1

    :cond_0
    :goto_0
    return v3

    :catch_0
    move-exception v4

    goto :goto_0
.end method

.method public checkAndBindMusicService()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicService:Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;

    if-nez v0, :cond_0

    :cond_0
    return-void
.end method

.method public checkAndBindPlayService()V
    .locals 2

    const/4 v0, 0x1

    sget-boolean v1, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mbPlayServiceInterface:Z

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayService:Lcom/android/music/IMediaPlaybackService;

    if-nez v0, :cond_0

    :try_start_0
    invoke-virtual {p0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->startToBindPlayService()V

    const-wide/16 v0, 0x7d0

    invoke-static {v0, v1}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "MMI_AVRCP"

    const-string v1, "[BT][AVRCP] ignore the mMusic playService"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public deinit()V
    .locals 5

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mConnection:Landroid/content/ServiceConnection;

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mbStartBind:Z

    if-ne v1, v2, :cond_1

    const-string v1, "MMI_AVRCP"

    const-string v2, "[BT][AVRCP] Adapter deinit"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicService:Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;

    if-eqz v1, :cond_0

    const-string v1, "MMI_AVRCP"

    const-string v2, "[BT][AVRCP][TT] unregistercallback "

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicService:Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;

    iget-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mAdapterCallback:Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter$AvrcpMusicAdapterStub;

    invoke-interface {v1, v2}, Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;->unregisterCallback(Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusicCallback;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :cond_0
    :goto_0
    iput-object v4, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicService:Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->stopToBind()V

    :cond_1
    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayService:Lcom/android/music/IMediaPlaybackService;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->avrcpSrv:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    iget-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2}, Landroid/content/ContextWrapper;->unbindService(Landroid/content/ServiceConnection;)V

    iput-object v4, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayService:Lcom/android/music/IMediaPlaybackService;

    :cond_2
    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mServiceLooper:Landroid/os/Looper;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mServiceLooper:Landroid/os/Looper;

    invoke-virtual {v1}, Landroid/os/Looper;->quit()V

    iput-object v4, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mServiceLooper:Landroid/os/Looper;

    :cond_3
    invoke-virtual {p0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->stopReceiver()V

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_4

    const-string v1, "MMI_AVRCP"

    const-string v2, "[BT][AVRCP] BTAvrcpMusicAdapter mHandler join 2"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Ljava/lang/Thread;->interrupt()V

    const-wide/16 v1, 0x64

    :try_start_1
    invoke-virtual {p0, v1, v2}, Ljava/lang/Thread;->join(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :cond_4
    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v1, "MMI_AVRCP"

    const-string v2, "[BT][AVRCP] join fail"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public getAbsoluteVolume()B
    .locals 1

    iget-byte v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mVolume:B

    return v0
.end method

.method public getCurPlayerAppValue(B)B
    .locals 10
    .param p1    # B

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->checkAndBindMusicService()V

    packed-switch p1, :pswitch_data_0

    const-string v3, "MMI_AVRCP"

    const-string v4, "[BT][AVRCP] attr_id is not find attr_id:%d"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v2

    :pswitch_0
    :try_start_0
    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicService:Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;

    invoke-interface {v3}, Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;->getRepeatMode()I

    move-result v1

    const-string v3, "MMI_AVRCP"

    const-string v4, "[BT][AVRCP] getRepeatMode ret %d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    int-to-byte v2, v1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v3, "MMI_AVRCP"

    const-string v4, "[BT][AVRCP] Exception ! Fail to getRepeatMode %d %s"

    new-array v5, v9, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :pswitch_1
    :try_start_1
    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicService:Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;

    invoke-interface {v3}, Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;->getShuffleMode()I

    move-result v1

    const-string v3, "MMI_AVRCP"

    const-string v4, "[BT][AVRCP] getShuffleMode ret %d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    int-to-byte v2, v1

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v3, "MMI_AVRCP"

    const-string v4, "[BT][AVRCP] Exception ! Fail to getShuffleMode %d %s"

    new-array v5, v9, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getElementAttribute(JI)Ljava/lang/String;
    .locals 12
    .param p1    # J
    .param p3    # I

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x0

    const-wide/16 v1, 0x0

    const-string v8, "MMI_AVRCP"

    const-string v9, "[BT][AVRCP] AVRCP getElementAttribute %b"

    new-array v10, v6, [Ljava/lang/Object;

    iget-object v11, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayService:Lcom/android/music/IMediaPlaybackService;

    if-eqz v11, :cond_0

    :goto_0
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v10, v7

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v8, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch p3, :pswitch_data_0

    move-object v5, v4

    :goto_1
    return-object v5

    :cond_0
    move v6, v7

    goto :goto_0

    :pswitch_0
    :try_start_0
    sget-object v4, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicTrack:Ljava/lang/String;

    if-nez v4, :cond_1

    const-string v4, ""

    :cond_1
    :goto_2
    :pswitch_1
    if-nez v4, :cond_2

    const-string v4, ""

    :cond_2
    :goto_3
    move-object v5, v4

    goto :goto_1

    :pswitch_2
    sget-object v4, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicArtist:Ljava/lang/String;

    if-nez v4, :cond_1

    const-string v4, ""

    goto :goto_2

    :pswitch_3
    sget-object v4, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicAlbum:Ljava/lang/String;

    if-nez v4, :cond_1

    const-string v4, ""
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    const-string v4, ""

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public getMaxElementAttribute()B
    .locals 2

    const/4 v0, 0x1

    sget-boolean v1, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mExtraAttribute:Z

    if-ne v0, v1, :cond_0

    const/4 v0, 0x7

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public getNowPlaying()[J
    .locals 5

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->checkAndBindPlayService()V

    iget-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayService:Lcom/android/music/IMediaPlaybackService;

    if-nez v2, :cond_0

    const-string v2, "MMI_AVRCP"

    const-string v3, "[AVRCP] no mPlayService for getNowPlaying"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v2}, Lcom/android/music/IMediaPlaybackService;->getQueue()[J

    move-result-object v1

    const-string v2, "MMI_AVRCP"

    const-string v3, "[AVRCP] getQueue from mPlayService"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v1, :cond_1

    const-string v2, "MMI_AVRCP"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[AVRCP] getQueue from mPlayService length:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v4, v1

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    move-object v2, v1

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    goto :goto_1
.end method

.method public getPlayerAppAttrText(B)Ljava/lang/String;
    .locals 5
    .param p1    # B

    packed-switch p1, :pswitch_data_0

    const-string v0, "MMI_AVRCP"

    const-string v1, "[BT][AVRCP] getPlayerAppAttrText unknow id:%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "Equalizer Setting"

    goto :goto_0

    :pswitch_1
    const-string v0, "RepeatMode Setting"

    goto :goto_0

    :pswitch_2
    const-string v0, "Shuffle Setting"

    goto :goto_0

    :pswitch_3
    const-string v0, "Scan Setting"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getPlayerAppValueText(BB)Ljava/lang/String;
    .locals 1
    .param p1    # B
    .param p2    # B

    packed-switch p1, :pswitch_data_0

    :goto_0
    const/4 v0, 0x0

    :goto_1
    return-object v0

    :pswitch_0
    packed-switch p2, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    const-string v0, "Equal Off"

    goto :goto_1

    :pswitch_2
    const-string v0, "Equal On"

    goto :goto_1

    :pswitch_3
    packed-switch p2, :pswitch_data_2

    :pswitch_4
    packed-switch p2, :pswitch_data_3

    :pswitch_5
    packed-switch p2, :pswitch_data_4

    goto :goto_0

    :pswitch_6
    const-string v0, "Equal Off"

    goto :goto_1

    :pswitch_7
    const-string v0, "Repeat Off"

    goto :goto_1

    :pswitch_8
    const-string v0, "Repeat Single"

    goto :goto_1

    :pswitch_9
    const-string v0, "Repeat All"

    goto :goto_1

    :pswitch_a
    const-string v0, "Shuffle Off"

    goto :goto_1

    :pswitch_b
    const-string v0, "Shuffle All"

    goto :goto_1

    :pswitch_c
    const-string v0, "Equal On"

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_a
        :pswitch_b
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_6
        :pswitch_c
    .end packed-switch
.end method

.method public getPlayerstatus()B
    .locals 8

    const/4 v3, -0x1

    const/4 v0, 0x0

    const-wide/16 v1, 0x0

    sget-boolean v4, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicPlaying:Z

    if-eqz v4, :cond_0

    const/4 v3, 0x1

    :goto_0
    iput-byte v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayerStatus:B

    return v3

    :cond_0
    const-wide/16 v4, -0x1

    sget-wide v6, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicId:J

    cmp-long v4, v4, v6

    if-eqz v4, :cond_1

    const/4 v3, 0x2

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public getPlayerstatusSongLength()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->checkAndBindPlayService()V

    iget-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayService:Lcom/android/music/IMediaPlaybackService;

    if-eqz v2, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v2}, Lcom/android/music/IMediaPlaybackService;->duration()J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    long-to-int v0, v2

    :goto_0
    move v2, v0

    :goto_1
    return v2

    :catch_0
    move-exception v1

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public getPlayerstatusSongPos()I
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->checkAndBindPlayService()V

    iget-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayService:Lcom/android/music/IMediaPlaybackService;

    if-eqz v2, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v2}, Lcom/android/music/IMediaPlaybackService;->position()J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    long-to-int v1, v2

    :goto_0
    move v2, v1

    :goto_1
    return v2

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public getSupportVersion()B
    .locals 2

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->checkAndBindPlayService()V

    const/4 v0, 0x1

    sget-boolean v1, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mbPlayServiceInterface:Z

    if-ne v0, v1, :cond_0

    const/16 v0, 0xe

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0xd

    goto :goto_0
.end method

.method public informBatteryStatus(B)V
    .locals 5
    .param p1    # B

    const-string v0, "MMI_AVRCP"

    const-string v1, "[BT][AVRCP] informBatteryStatus status:%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public informDisplayCharset(B[S)Z
    .locals 7
    .param p1    # B
    .param p2    # [S

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x0

    :goto_0
    array-length v3, p2

    if-ge v0, v3, :cond_1

    if-ge v0, p1, :cond_1

    const-string v3, "MMI_AVRCP"

    const-string v4, "[BT][AVRCP] charset i:%d value:%d"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v6

    aput-object v6, v5, v2

    aget-short v6, p2, v0

    invoke-static {v6}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    aget-short v3, p2, v0

    const/16 v4, 0x6a

    if-ne v3, v4, :cond_0

    :goto_1
    return v1

    :cond_0
    add-int/lit8 v3, v0, 0x1

    int-to-byte v0, v3

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public init()V
    .locals 0

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->startReceiver()V

    return-void
.end method

.method public listPlayerAppAttribute()[B
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mAttrs:[B

    return-object v0
.end method

.method public listPlayerAppValue(B)[B
    .locals 5
    .param p1    # B

    packed-switch p1, :pswitch_data_0

    const-string v0, "MMI_AVRCP"

    const-string v1, "[BT][AVRCP] listPlayerAppValue attr_id:%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mValuesEqualizer:[B

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mValuesRepeat:[B

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mValuesShuffle:[B

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mValuesScan:[B

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public notificationBatteryStatusChanged(BBB)V
    .locals 1
    .param p1    # B
    .param p2    # B
    .param p3    # B

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->avrcpSrv:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->avrcpSrv:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    invoke-virtual {v0, p1, p2, p3}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->notificationBatteryStatusChanged(BBB)V

    :cond_0
    return-void
.end method

.method public notificationSystemStatusChanged(BBB)V
    .locals 1
    .param p1    # B
    .param p2    # B
    .param p3    # B

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->avrcpSrv:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->avrcpSrv:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    invoke-virtual {v0, p1, p2, p3}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->notificationSystemStatusChanged(BBB)V

    :cond_0
    return-void
.end method

.method public notificationVolumeChanged(BBB)V
    .locals 1
    .param p1    # B
    .param p2    # B
    .param p3    # B

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->avrcpSrv:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->avrcpSrv:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    invoke-virtual {v0, p1, p2, p3}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->notificationVolumeChanged(BBB)V

    :cond_0
    return-void
.end method

.method public onConnect()V
    .locals 3

    const-string v0, "MMI_AVRCP"

    const-string v1, "[BT][AVRCP] Adapter onConnect"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->startToBind()V

    const-string v0, "MMI_AVRCP"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[BT][AVRCP] mbPlayServiceInterface is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mbPlayServiceInterface:Z

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mAudioMgr:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->checkCapability()V

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mRegBit:Ljava/util/BitSet;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mRegBit:Ljava/util/BitSet;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mRegBit:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPendingRegBit:Ljava/util/BitSet;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPendingRegBit:Ljava/util/BitSet;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPendingRegBit:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mConnected:Z

    return-void

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public onDisconnect()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mConnection:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mbStartBind:Z

    if-ne v0, v1, :cond_1

    const-string v0, "MMI_AVRCP"

    const-string v1, "[BT][AVRCP] Adapter onDisconnect"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicService:Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;

    if-eqz v0, :cond_0

    const-string v0, "MMI_AVRCP"

    const-string v1, "[BT][AVRCP][TT] unregistercallback "

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicService:Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mAdapterCallback:Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter$AvrcpMusicAdapterStub;

    invoke-interface {v0, v1}, Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;->unregisterCallback(Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusicCallback;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicService:Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->stopToBind()V

    :cond_1
    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mAudioMgr:Landroid/media/AudioManager;

    if-eqz v0, :cond_2

    :cond_2
    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mRegBit:Ljava/util/BitSet;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mRegBit:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPendingRegBit:Ljava/util/BitSet;

    monitor-enter v1

    :try_start_2
    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPendingRegBit:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    iput-boolean v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mConnected:Z

    iput-boolean v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mUpdateSending:Z

    return-void

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public passNotifyMsg(II)Z
    .locals 2
    .param p1    # I
    .param p2    # I

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    const/16 v1, 0x22

    iput v1, v0, Landroid/os/Message;->what:I

    iput p1, v0, Landroid/os/Message;->arg1:I

    iput p2, v0, Landroid/os/Message;->arg2:I

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public passThroughKeyInd(IB)V
    .locals 8
    .param p1    # I
    .param p2    # B

    const/4 v7, 0x1

    const/4 v0, 0x0

    const-string v3, "[BT][AVRCP] Receive a Avrcpkey:%d (APKey:%d)"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "MMI_AVRCP"

    invoke-static {v3, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mContext:Landroid/content/Context;

    if-eqz v3, :cond_0

    sget-boolean v3, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->bSupportMusicUI:Z

    if-ne v7, v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mHandler:Landroid/os/Handler;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    const/16 v3, 0x21

    iput v3, v1, Landroid/os/Message;->what:I

    iput p1, v1, Landroid/os/Message;->arg1:I

    iput p2, v1, Landroid/os/Message;->arg2:I

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    return-void
.end method

.method public passToHandleMessage(Landroid/os/Message;)V
    .locals 1
    .param p1    # Landroid/os/Message;

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->handleKeyMessage(Landroid/os/Message;)V

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->handleSettingMessage(Landroid/os/Message;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x12
        :pswitch_0
    .end packed-switch
.end method

.method public playItems(J)Z
    .locals 10
    .param p1    # J

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const-wide/16 v5, 0x0

    cmp-long v5, p1, v5

    if-nez v5, :cond_0

    const-string v5, "MMI_AVRCP"

    const-string v6, "[BT][AVRCP] Wrong id 0"

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v4

    :cond_0
    invoke-virtual {p0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->checkAndBindPlayService()V

    iget-object v4, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayService:Lcom/android/music/IMediaPlaybackService;

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    :try_start_0
    new-array v0, v4, [J

    const/4 v4, 0x0

    aput-wide p1, v0, v4

    iget-object v4, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayService:Lcom/android/music/IMediaPlaybackService;

    const/4 v5, 0x1

    invoke-interface {v4, v0, v5}, Lcom/android/music/IMediaPlaybackService;->enqueue([JI)V

    const-string v4, "MMI_AVRCP"

    const-string v5, "[BT][AVRCP] enqueu %d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const/4 v8, 0x0

    aget-wide v8, v0, v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v3, 0x1

    :cond_1
    :goto_1
    move v4, v3

    goto :goto_0

    :catch_0
    move-exception v4

    goto :goto_1
.end method

.method public playerAppCapabilities()[B
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mCapabilities:[B

    return-object v0
.end method

.method public registerNotification(BI)Z
    .locals 10
    .param p1    # B
    .param p2    # I

    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    if-ne v8, v0, :cond_1

    iget-object v5, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mRegBit:Ljava/util/BitSet;

    monitor-enter v5

    :try_start_0
    iget-object v4, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mRegBit:Ljava/util/BitSet;

    invoke-virtual {v4, p1}, Ljava/util/BitSet;->set(I)V

    iget-object v4, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mRegBit:Ljava/util/BitSet;

    invoke-virtual {v4, p1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    const-string v4, "MMI_AVRCP"

    const-string v6, "[BT][AVRCP] mRegBit set %d Reg:%b cardinality:%d"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {p1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x2

    iget-object v9, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mRegBit:Ljava/util/BitSet;

    invoke-virtual {v9}, Ljava/util/BitSet;->cardinality()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    return v0

    :pswitch_1
    const-string v4, "MMI_AVRCP"

    const-string v5, "[BT][AVRCP] MusicAdapter blocks support register event:%d"

    new-array v6, v8, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0

    :pswitch_2
    sparse-switch p1, :sswitch_data_0

    :goto_1
    const-string v4, "MMI_AVRCP"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[BT][AVRCP] registerNotification "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :sswitch_0
    invoke-virtual {p0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->getPlayerstatus()B

    move-result v3

    iget-object v4, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->avrcpSrv:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    invoke-virtual {v4, v9, v8, v3}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->notificationPlayStatusChangedNative(BBB)Z

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_1
    invoke-virtual {p0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->getPlayerstatus()B

    move-result v3

    packed-switch v3, :pswitch_data_1

    const-wide/16 v1, -0x1

    :goto_2
    iget-object v4, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->avrcpSrv:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    invoke-virtual {v4, v9, v8, v1, v2}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->notificationTrackChangedNative(BBJ)Z

    const/4 v0, 0x1

    goto :goto_1

    :pswitch_3
    sget-wide v1, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicId:J

    goto :goto_2

    :sswitch_2
    iget-object v4, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->avrcpSrv:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    invoke-virtual {v4, v9, v8}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->notificationNowPlayingChangedNative(BB)Z

    const/4 v0, 0x1

    goto :goto_1

    :pswitch_4
    iget-object v4, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mSystemListener:Lcom/mediatek/bluetooth/avrcp/BTAvrcpSystemListener;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mSystemListener:Lcom/mediatek/bluetooth/avrcp/BTAvrcpSystemListener;

    invoke-virtual {v4, p1, p2}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpSystemListener;->regNotificationEvent(BI)Z

    move-result v0

    goto/16 :goto_0

    :pswitch_5
    const-string v4, "MMI_AVRCP"

    const-string v5, "[BT][AVRCP] MusicAdapter blocks support register event:%d"

    new-array v6, v8, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto/16 :goto_0

    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_4
        :pswitch_4
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x9 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public run()V
    .locals 1

    invoke-static {}, Landroid/os/Looper;->prepare()V

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mServiceLooper:Landroid/os/Looper;

    new-instance v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter$1;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter$1;-><init>(Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mHandler:Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->loop()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method public sendAvrcpKeyEvent(IB)V
    .locals 8
    .param p1    # I
    .param p2    # B

    const/4 v7, 0x1

    const/4 v0, 0x0

    const-string v3, "[BT][AVRCP] Receive a Avrcpkey:%d (APKey:%d)"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "MMI_AVRCP"

    invoke-static {v3, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mContext:Landroid/content/Context;

    if-eqz v3, :cond_0

    if-ne p2, v7, :cond_0

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mHandler:Landroid/os/Handler;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    const/16 v3, 0x11

    iput v3, v1, Landroid/os/Message;->what:I

    iput p1, v1, Landroid/os/Message;->arg1:I

    iput p2, v1, Landroid/os/Message;->arg2:I

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    return-void
.end method

.method public setAbsoluteVolume(B)Z
    .locals 10
    .param p1    # B

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v7, 0x3

    iget-object v5, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mAudioMgr:Landroid/media/AudioManager;

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->convertToMgrVolume(B)I

    move-result v0

    iget-object v5, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mAudioMgr:Landroid/media/AudioManager;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mAudioMgr:Landroid/media/AudioManager;

    invoke-virtual {v5, v7}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v2

    iget-object v5, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mAudioMgr:Landroid/media/AudioManager;

    const/4 v6, 0x4

    invoke-virtual {v5, v7, v0, v6}, Landroid/media/AudioManager;->setStreamVolume(III)V

    iget-object v5, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mAudioMgr:Landroid/media/AudioManager;

    invoke-virtual {v5, v7}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v1

    const-string v5, "MMI_AVRCP"

    const-string v6, "[BT][AVRCP] Adapter before:%d to-set:%d after:%d"

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v4

    const/4 v8, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-ne v1, v0, :cond_0

    iput-byte p1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mVolume:B

    move v3, v4

    goto :goto_0
.end method

.method public setPlayerAppValue(BB)Z
    .locals 11
    .param p1    # B
    .param p2    # B

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->checkAndBindMusicService()V

    if-ne p1, v10, :cond_1

    :try_start_0
    iget-object v4, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicService:Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;

    invoke-interface {v4}, Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;->getRepeatMode()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    move-result v2

    :goto_0
    if-ne p2, v2, :cond_0

    const-string v4, "MMI_AVRCP"

    const-string v5, "[BT][AVRCP] Already in repeat mode"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return v3

    :cond_0
    packed-switch p2, :pswitch_data_0

    :cond_1
    :goto_2
    const/4 v4, 0x3

    if-ne p1, v4, :cond_3

    :try_start_1
    iget-object v4, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicService:Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;

    invoke-interface {v4}, Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;->getShuffleMode()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    move-result v2

    :goto_3
    if-ne p2, v2, :cond_2

    const-string v4, "MMI_AVRCP"

    const-string v5, "Already in shutffle mode"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :pswitch_0
    :try_start_2
    iget-object v4, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicService:Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;

    invoke-interface {v4, p2}, Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;->setRepeatMode(I)Z

    move-result v1

    if-ne v1, v3, :cond_1

    const-string v4, "MMI_AVRCP"

    const-string v5, "[BT][AVRCP] setRepeatMode ret %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    const-string v4, "MMI_AVRCP"

    const-string v5, "[BT][AVRCP] Exception ! Fail to setRepeatMode %d %s"

    new-array v6, v10, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_2
    packed-switch p2, :pswitch_data_1

    :cond_3
    :goto_4
    if-nez v1, :cond_4

    const-string v4, "MMI_AVRCP"

    const-string v5, "[BT][AVRCP] fail to set attr_id:%d to value:%d"

    new-array v6, v10, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {p2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    move v3, v1

    goto :goto_1

    :pswitch_1
    :try_start_3
    iget-object v4, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mMusicService:Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;

    invoke-interface {v4, p2}, Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;->setShuffleMode(I)Z

    move-result v1

    if-ne v1, v3, :cond_3

    const-string v4, "MMI_AVRCP"

    const-string v5, "[BT][AVRCP] setShuffleMode ret %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_4

    :catch_1
    move-exception v0

    const-string v4, "MMI_AVRCP"

    const-string v5, "[BT][AVRCP] Exception ! Fail to setShuffleMode %d %s"

    new-array v6, v10, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "MMI_AVRCP"

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    :catch_2
    move-exception v4

    goto/16 :goto_3

    :catch_3
    move-exception v4

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public startReceiver()V
    .locals 5

    const/4 v4, 0x1

    const-string v1, "MMI_AVRCP"

    const-string v2, "[BT][AVRCP][b] startReceiver"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-boolean v1, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mStartReceiver:Z

    if-ne v4, v1, :cond_0

    const-string v1, "MMI_AVRCP"

    const-string v2, "[BT][AVRCP][b] startReceiver ignore"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const-string v1, "MMI_AVRCP"

    const-string v2, "[BT][AVRCP][b] startReceiver music intent"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.android.music.playstatechanged"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.android.music.metachanged"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.android.music.quitplayback"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.android.music.queuechanged"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->avrcpSrv:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    iget-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mStatusListener:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3, v0}, Landroid/content/IntentFilter;-><init>(Landroid/content/IntentFilter;)V

    invoke-virtual {v1, v2, v3}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    sput-boolean v4, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mStartReceiver:Z

    goto :goto_0
.end method

.method public startToBind()V
    .locals 0

    return-void
.end method

.method public startToBindPlayService()V
    .locals 8

    const/4 v7, 0x0

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayService:Lcom/android/music/IMediaPlaybackService;

    if-nez v2, :cond_0

    sget-boolean v2, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mbPlayServiceInterface:Z

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sput-boolean v7, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mbPlayServiceInterface:Z

    :try_start_0
    iget-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->avrcpSrv:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.android.music.MediaPlaybackService"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/content/ContextWrapper;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    iget-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->avrcpSrv:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.android.music.MediaPlaybackService"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayConnection:Landroid/content/ServiceConnection;

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContextWrapper;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    const-string v2, "MMI_AVRCP"

    const-string v3, "[BT][AVRCP][b] startPlaybackService bBindRet:%b"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sput-boolean v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mbPlayServiceInterface:Z

    if-nez v0, :cond_2

    const-string v2, "MMI_AVRCP"

    const-string v3, "[BT][AVRCP] mMusicService does not have play interface "

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mbPlayStartBind:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    const-string v2, "MMI_AVRCP"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[BT][AVRCP] mbPlayServiceInterface is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-boolean v4, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mbPlayServiceInterface:Z

    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_0
    move-exception v1

    sput-boolean v7, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mbPlayServiceInterface:Z

    goto :goto_1
.end method

.method public stopReceiver()V
    .locals 2

    sget-boolean v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mStartReceiver:Z

    if-nez v0, :cond_0

    const-string v0, "MMI_AVRCP"

    const-string v1, "[BT][AVRCP][b] stopReceiver ignore"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const-string v0, "MMI_AVRCP"

    const-string v1, "[BT][AVRCP] startReceiver stop "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->avrcpSrv:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mStatusListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    sput-boolean v0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mStartReceiver:Z

    goto :goto_0
.end method

.method public stopToBind()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "MMI_AVRCP"

    const-string v1, "[BT][AVRCP][b] stopToBind"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->avrcpSrv:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/ContextWrapper;->unbindService(Landroid/content/ServiceConnection;)V

    iput-boolean v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mbStartBind:Z

    const-string v0, "MMI_AVRCP"

    const-string v1, "[BT][AVRCP][b] PlayService stopToBind"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->avrcpSrv:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mPlayConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/ContextWrapper;->unbindService(Landroid/content/ServiceConnection;)V

    iput-boolean v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->mbPlayStartBind:Z

    return-void
.end method
