.class public Lcom/mediatek/bluetooth/avrcp/BTAvrcpSystemListener;
.super Ljava/lang/Object;
.source "BTAvrcpSystemListener.java"


# static fields
.field public static final TAG:Ljava/lang/String; = "MIS_AVRCP"


# instance fields
.field private iBatteryStatus:I

.field private iSystemStatus:I

.field private isRegBattery:Z

.field private isRegSystem:Z

.field private mAdapter:Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;


# direct methods
.method public constructor <init>(Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;)V
    .locals 1
    .param p1    # Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpSystemListener;->mAdapter:Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;

    iput-boolean v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpSystemListener;->isRegBattery:Z

    iput-boolean v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpSystemListener;->isRegSystem:Z

    iput v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpSystemListener;->iBatteryStatus:I

    iput v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpSystemListener;->iSystemStatus:I

    invoke-virtual {p0, v0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpSystemListener;->onBatteryStatusChange(I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpSystemListener;->onSystemStatusChange(I)V

    return-void
.end method


# virtual methods
.method public declared-synchronized onBatteryStatusChange(I)V
    .locals 4
    .param p1    # I

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpSystemListener;->mAdapter:Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpSystemListener;->isRegBattery:Z

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpSystemListener;->isRegBattery:Z

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpSystemListener;->mAdapter:Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;

    const/4 v1, 0x0

    const/4 v2, 0x0

    int-to-byte v3, p1

    invoke-virtual {v0, v1, v2, v3}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->notificationBatteryStatusChanged(BBB)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const-string v0, "AVRCP"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[BT][AVRCP] onReceive :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public declared-synchronized onSystemStatusChange(I)V
    .locals 4
    .param p1    # I

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpSystemListener;->mAdapter:Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpSystemListener;->isRegSystem:Z

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpSystemListener;->isRegSystem:Z

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpSystemListener;->mAdapter:Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;

    const/4 v1, 0x0

    const/4 v2, 0x0

    int-to-byte v3, p1

    invoke-virtual {v0, v1, v2, v3}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->notificationSystemStatusChanged(BBB)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onVolumeStatusChange(I)V
    .locals 4
    .param p1    # I

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpSystemListener;->mAdapter:Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpSystemListener;->isRegSystem:Z

    if-ne v1, v2, :cond_0

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpSystemListener;->isRegSystem:Z

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpSystemListener;->mAdapter:Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->notificationVolumeChanged(BBB)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public regNotificationEvent(BI)Z
    .locals 3
    .param p1    # B
    .param p2    # I

    const/4 v0, 0x0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpSystemListener;->mAdapter:Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v2, 0x6

    if-ne v2, p1, :cond_2

    iget-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpSystemListener;->mAdapter:Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;

    invoke-virtual {v2, v0, v1, v1}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->notificationBatteryStatusChanged(BBB)V

    move v0, v1

    goto :goto_0

    :cond_2
    const/4 v2, 0x7

    if-ne v2, p1, :cond_3

    iget-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpSystemListener;->mAdapter:Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;

    invoke-virtual {v2, v0, v1, v1}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->notificationSystemStatusChanged(BBB)V

    move v0, v1

    goto :goto_0

    :cond_3
    const/16 v2, 0xd

    if-ne v2, p1, :cond_0

    iget-object v2, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpSystemListener;->mAdapter:Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;

    invoke-virtual {v2, v0, v1, v1}, Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;->notificationVolumeChanged(BBB)V

    move v0, v1

    goto :goto_0
.end method

.method public declared-synchronized startListener(Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;)V
    .locals 1
    .param p1    # Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpSystemListener;->mAdapter:Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized stopListener()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BTAvrcpSystemListener;->mAdapter:Lcom/mediatek/bluetooth/avrcp/BTAvrcpMusicAdapter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
