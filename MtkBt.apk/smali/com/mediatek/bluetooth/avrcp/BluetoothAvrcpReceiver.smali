.class public Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpReceiver;
.super Landroid/content/BroadcastReceiver;
.source "BluetoothAvrcpReceiver.java"


# static fields
.field public static final TAG:Ljava/lang/String; = "AVRCP"

.field public static mAvrcpServer:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;


# instance fields
.field private mNativeData:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpReceiver;->mAvrcpServer:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public destroyMyself(Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;)V
    .locals 2
    .param p1    # Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    sget-object v0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpReceiver;->mAvrcpServer:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    if-ne p1, v0, :cond_0

    const-string v0, "AVRCP"

    const-string v1, "destroyMyself"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public initalConnect(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    sget-object v0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpReceiver;->mAvrcpServer:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    if-eqz v0, :cond_0

    const-string v0, "AVRCP"

    const-string v1, "AVRCP initConnect connectReqNative used!"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpReceiver;->mAvrcpServer:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    invoke-virtual {v0, p1}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->connectReqNative(Ljava/lang/String;)Z

    :goto_0
    return-void

    :cond_0
    const-string v0, "AVRCP"

    const-string v1, "AVRCP initConnect fail !!! no mAvrcpServer"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v9, 0x3

    const/4 v8, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v5, "[BT][AVRCP] onReceive "

    invoke-virtual {v5, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "AVRCP"

    invoke-static {v5, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v3, 0x0

    const-string v5, "android.bluetooth.adapter.extra.STATE"

    const/high16 v6, -0x80000000

    invoke-virtual {p2, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    const/16 v5, 0xc

    if-eq v5, v3, :cond_0

    const/16 v5, 0xa

    if-ne v5, v3, :cond_1

    :cond_0
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v2, p2}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    const-class v5, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    invoke-virtual {v2, p1, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v5, "action"

    invoke-virtual {v2, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p1, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_1
    const-string v5, "android.provider.Telephony.SECRET_CODE"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {p2}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v1

    const-string v5, "AVRCP"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[BT][AVRCP] Get the securty code ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v1, :cond_2

    const-string v5, "AVRCP"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[BT][AVRCP] Get the securty code data: ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const-string v4, "AVRCP PTS enable mode (Source:Telephone)"

    if-eqz v1, :cond_7

    const-string v5, "2872710"

    invoke-virtual {v1, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_7

    const-string v5, "00:00:00:00:00:00"

    invoke-virtual {p0, v5}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpReceiver;->initalConnect(Ljava/lang/String;)V

    const-string v4, "AVRCP PTS connect mode (Source:Telephone)"

    :goto_0
    invoke-static {p1, v4, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    :cond_3
    const-string v5, "android.mediatek.bluetooth.avrcp.pts"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    const-string v5, "AVRCP"

    const-string v6, "Get the avrcp.pts code"

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    sput v9, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->mPTSDebugMode:I

    const-string v4, "AVRCP PTS enable mode (Source:pts action)"

    invoke-static {p1, v4, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    :cond_4
    const-string v5, "android.mediatek.bluetooth.avrcp.connect"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    const-string v5, "AVRCP"

    const-string v6, "Get the avrcp.connect code"

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "AVRCP PTS connect (Source: action)"

    invoke-static {p1, v4, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    const-string v5, "00:00:00:00:00:00"

    invoke-virtual {p0, v5}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpReceiver;->initalConnect(Ljava/lang/String;)V

    :cond_5
    const-string v5, "android.mediatek.bluetooth.avrcp.disconnect"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    const-string v5, "AVRCP"

    const-string v6, "Get the avrcp.disconnect code"

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v5, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpReceiver;->mAvrcpServer:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    if-eqz v5, :cond_6

    const-string v4, "AVRCP PTS disconnect (Source: action)"

    invoke-static {p1, v4, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    sget-object v5, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpReceiver;->mAvrcpServer:Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    invoke-virtual {v5}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->disconnectNative()Z

    :cond_6
    return-void

    :cond_7
    sput v9, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->mPTSDebugMode:I

    goto :goto_0
.end method
