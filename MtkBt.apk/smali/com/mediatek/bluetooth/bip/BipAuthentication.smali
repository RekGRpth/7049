.class public Lcom/mediatek/bluetooth/bip/BipAuthentication;
.super Lcom/android/internal/app/AlertActivity;
.source "BipAuthentication.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# static fields
.field public static final EXTRA_FROM:Ljava/lang/String; = "com.mediatek.bluetooth.bipauthentication.extra.FROM"

.field public static final EXTRA_NEED_USERID:Ljava/lang/String; = "com.mediatek.bluetooth.bipauthentication.extra.NEED_USERID"

.field private static final TAG:Ljava/lang/String; = "BipAuthentication"

.field private static mFrom:Ljava/lang/String;

.field private static mNeedUserId:Z

.field private static mPara:Lcom/android/internal/app/AlertController$AlertParams;

.field private static mPasswordEdit:Landroid/widget/EditText;

.field private static mUserIdEdit:Landroid/widget/EditText;

.field private static mUserIdText:Landroid/widget/TextView;

.field private static mView:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/mediatek/bluetooth/bip/BipAuthentication;->mView:Landroid/view/View;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/internal/app/AlertActivity;-><init>()V

    return-void
.end method

.method private createView()Landroid/view/View;
    .locals 4

    const/16 v3, 0x8

    const-string v0, "BipAuthentication"

    const-string v1, "createView"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030003

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    sput-object v0, Lcom/mediatek/bluetooth/bip/BipAuthentication;->mView:Landroid/view/View;

    sget-object v0, Lcom/mediatek/bluetooth/bip/BipAuthentication;->mView:Landroid/view/View;

    const v1, 0x7f080004

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    sput-object v0, Lcom/mediatek/bluetooth/bip/BipAuthentication;->mPasswordEdit:Landroid/widget/EditText;

    sget-object v0, Lcom/mediatek/bluetooth/bip/BipAuthentication;->mView:Landroid/view/View;

    const v1, 0x7f080005

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sput-object v0, Lcom/mediatek/bluetooth/bip/BipAuthentication;->mUserIdText:Landroid/widget/TextView;

    sget-object v0, Lcom/mediatek/bluetooth/bip/BipAuthentication;->mView:Landroid/view/View;

    const v1, 0x7f080006

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    sput-object v0, Lcom/mediatek/bluetooth/bip/BipAuthentication;->mUserIdEdit:Landroid/widget/EditText;

    sget-boolean v0, Lcom/mediatek/bluetooth/bip/BipAuthentication;->mNeedUserId:Z

    if-nez v0, :cond_1

    sget-object v0, Lcom/mediatek/bluetooth/bip/BipAuthentication;->mUserIdText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/mediatek/bluetooth/bip/BipAuthentication;->mUserIdEdit:Landroid/widget/EditText;

    if-nez v0, :cond_2

    :cond_0
    const-string v0, "BipAuthentication"

    const-string v1, "UserId text is null or UserId edit is null"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    sget-object v0, Lcom/mediatek/bluetooth/bip/BipAuthentication;->mView:Landroid/view/View;

    return-object v0

    :cond_2
    sget-object v0, Lcom/mediatek/bluetooth/bip/BipAuthentication;->mUserIdText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    sget-object v0, Lcom/mediatek/bluetooth/bip/BipAuthentication;->mUserIdEdit:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private setUpDialog()V
    .locals 2

    const-string v0, "BipAuthentication"

    const-string v1, "setUpDialog"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/internal/app/AlertActivity;->mAlertParams:Lcom/android/internal/app/AlertController$AlertParams;

    sput-object v0, Lcom/mediatek/bluetooth/bip/BipAuthentication;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    sget-object v0, Lcom/mediatek/bluetooth/bip/BipAuthentication;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    const v1, 0x108009b

    iput v1, v0, Lcom/android/internal/app/AlertController$AlertParams;->mIconId:I

    sget-object v0, Lcom/mediatek/bluetooth/bip/BipAuthentication;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    const v1, 0x7f060022

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/internal/app/AlertController$AlertParams;->mTitle:Ljava/lang/CharSequence;

    sget-object v0, Lcom/mediatek/bluetooth/bip/BipAuthentication;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    const v1, 0x7f06001e

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/internal/app/AlertController$AlertParams;->mPositiveButtonText:Ljava/lang/CharSequence;

    sget-object v0, Lcom/mediatek/bluetooth/bip/BipAuthentication;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    iput-object p0, v0, Lcom/android/internal/app/AlertController$AlertParams;->mPositiveButtonListener:Landroid/content/DialogInterface$OnClickListener;

    sget-object v0, Lcom/mediatek/bluetooth/bip/BipAuthentication;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    const v1, 0x7f06001f

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/internal/app/AlertController$AlertParams;->mNegativeButtonText:Ljava/lang/CharSequence;

    sget-object v0, Lcom/mediatek/bluetooth/bip/BipAuthentication;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    iput-object p0, v0, Lcom/android/internal/app/AlertController$AlertParams;->mNegativeButtonListener:Landroid/content/DialogInterface$OnClickListener;

    sget-object v0, Lcom/mediatek/bluetooth/bip/BipAuthentication;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/bip/BipAuthentication;->createView()Landroid/view/View;

    move-result-object v1

    iput-object v1, v0, Lcom/android/internal/app/AlertController$AlertParams;->mView:Landroid/view/View;

    invoke-virtual {p0}, Lcom/android/internal/app/AlertActivity;->setupAlert()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const-string v1, "BipAuthentication"

    const-string v2, "onClick"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, -0x1

    if-ne p2, v1, :cond_3

    const-string v1, "BipAuthentication"

    const-string v2, "positive button"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mediatek/bluetooth/bip/BipService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v1, Lcom/mediatek/bluetooth/bip/BipAuthentication;->mFrom:Ljava/lang/String;

    const-string v2, "BIPI"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "action"

    const-string v2, "com.mediatek.bluetooth.bppmanager.action.BIPI_AUTH_INFO"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :goto_0
    sget-boolean v1, Lcom/mediatek/bluetooth/bip/BipAuthentication;->mNeedUserId:Z

    if-eqz v1, :cond_2

    const-string v1, "com.mediatek.bluetooth.bipiservice.extra.EXTRA_AUTH_USERID"

    sget-object v2, Lcom/mediatek/bluetooth/bip/BipAuthentication;->mUserIdEdit:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :goto_1
    const-string v1, "com.mediatek.bluetooth.bipiservice.extra.EXTRA_AUTH_PASSWD"

    sget-object v2, Lcom/mediatek/bluetooth/bip/BipAuthentication;->mPasswordEdit:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_0
    :goto_2
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void

    :cond_1
    const-string v1, "action"

    const-string v2, "com.mediatek.bluetooth.bppmanager.action.BIPR_AUTH_INFO"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    :cond_2
    const-string v1, "com.mediatek.bluetooth.bipiservice.extra.EXTRA_AUTH_USERID"

    const-string v2, "UserId"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    :cond_3
    const/4 v1, -0x2

    if-ne p2, v1, :cond_0

    const-string v1, "BipAuthentication"

    const-string v2, "negative button"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mediatek/bluetooth/bip/BipService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v1, Lcom/mediatek/bluetooth/bip/BipAuthentication;->mFrom:Ljava/lang/String;

    const-string v2, "BIPI"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "action"

    const-string v2, "com.mediatek.bluetooth.bipiservice.action.BIPI_CANCEL"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :goto_3
    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_2

    :cond_4
    const-string v1, "action"

    const-string v2, "com.mediatek.bluetooth.bipiservice.action.BIPR_CANCEL"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_3
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const-string v2, "BipAuthentication"

    const-string v3, "OnCreate"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Lcom/android/internal/app/AlertActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v2, "com.mediatek.bluetooth.bipauthentication.extra.FROM"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/mediatek/bluetooth/bip/BipAuthentication;->mFrom:Ljava/lang/String;

    const-string v2, "com.mediatek.bluetooth.bipauthentication.extra.NEED_USERID"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    sput-boolean v2, Lcom/mediatek/bluetooth/bip/BipAuthentication;->mNeedUserId:Z

    invoke-direct {p0}, Lcom/mediatek/bluetooth/bip/BipAuthentication;->setUpDialog()V

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    const-string v0, "BipAuthentication"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method protected onStart()V
    .locals 2

    const-string v0, "BipAuthentication"

    const-string v1, "onStart"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    return-void
.end method

.method protected onStop()V
    .locals 2

    const-string v0, "BipAuthentication"

    const-string v1, "onStop()"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method
