.class public Lcom/mediatek/bluetooth/bip/BipImage;
.super Ljava/lang/Object;
.source "BipImage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/bluetooth/bip/BipImage$CreateThumbnailThread;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "BipImage"

.field private static final thumFileName:Ljava/lang/String; = "thumbnail.jpg"

.field private static final thumFilePath:Ljava/lang/String; = "/data/@btmtk/profile/"


# instance fields
.field public AcceptableFileSize:I

.field public DirName:Ljava/lang/String;

.field public Encoding:Ljava/lang/String;

.field public FileName:Ljava/lang/String;

.field public Height:I

.field public Height2:I

.field public ObjectSize:I

.field public Size:I

.field public ThumbnailFullPath:Ljava/lang/String;

.field public Transform:Lcom/mediatek/bluetooth/bip/Transformation;

.field public Version:Ljava/lang/String;

.field public Width:I

.field public Width2:I

.field private mCreateThumbnailThread:Lcom/mediatek/bluetooth/bip/BipImage$CreateThumbnailThread;


# direct methods
.method public constructor <init>(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 8
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Landroid/content/Context;

    const/16 v7, 0x2f

    const/4 v3, 0x0

    const/4 v6, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v3, p0, Lcom/mediatek/bluetooth/bip/BipImage;->DirName:Ljava/lang/String;

    iput-object v3, p0, Lcom/mediatek/bluetooth/bip/BipImage;->FileName:Ljava/lang/String;

    iput-object v3, p0, Lcom/mediatek/bluetooth/bip/BipImage;->ThumbnailFullPath:Ljava/lang/String;

    iput v6, p0, Lcom/mediatek/bluetooth/bip/BipImage;->ObjectSize:I

    iput v6, p0, Lcom/mediatek/bluetooth/bip/BipImage;->AcceptableFileSize:I

    iput-object v3, p0, Lcom/mediatek/bluetooth/bip/BipImage;->Version:Ljava/lang/String;

    iput-object v3, p0, Lcom/mediatek/bluetooth/bip/BipImage;->Encoding:Ljava/lang/String;

    iput v6, p0, Lcom/mediatek/bluetooth/bip/BipImage;->Width:I

    iput v6, p0, Lcom/mediatek/bluetooth/bip/BipImage;->Height:I

    iput v6, p0, Lcom/mediatek/bluetooth/bip/BipImage;->Width2:I

    iput v6, p0, Lcom/mediatek/bluetooth/bip/BipImage;->Height2:I

    iput v6, p0, Lcom/mediatek/bluetooth/bip/BipImage;->Size:I

    iput-object v3, p0, Lcom/mediatek/bluetooth/bip/BipImage;->mCreateThumbnailThread:Lcom/mediatek/bluetooth/bip/BipImage$CreateThumbnailThread;

    const-string v3, "1.0"

    iput-object v3, p0, Lcom/mediatek/bluetooth/bip/BipImage;->Version:Ljava/lang/String;

    const-string v3, "BipImage"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Image Descriptor Version: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BipImage;->Version:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    const/4 v3, 0x2

    new-array v1, v3, [I

    invoke-virtual {p5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "BipImage"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "File Path: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2, v7}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {p2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/bluetooth/bip/BipImage;->FileName:Ljava/lang/String;

    const-string v3, "BipImage"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "File Name: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BipImage;->FileName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2, v7}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    invoke-virtual {p2, v6, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/bluetooth/bip/BipImage;->DirName:Ljava/lang/String;

    const-string v3, "BipImage"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Dir Name: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BipImage;->DirName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/mediatek/bluetooth/bip/BipImage;->ObjectSize:I

    iget v3, p0, Lcom/mediatek/bluetooth/bip/BipImage;->ObjectSize:I

    iput v3, p0, Lcom/mediatek/bluetooth/bip/BipImage;->Size:I

    const-string v3, "BipImage"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "File Size: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/bluetooth/bip/BipImage;->ObjectSize:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "BipImage"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "File Mime: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p4}, Lcom/mediatek/bluetooth/bip/BipImage;->getImageEncoding(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/bluetooth/bip/BipImage;->Encoding:Ljava/lang/String;

    const-string v3, "BipImage"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Encoding: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BipImage;->Encoding:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1, v0}, Lcom/mediatek/bluetooth/bip/BipImage;->getImageDimension(Landroid/net/Uri;Landroid/content/ContentResolver;)[I

    move-result-object v1

    aget v3, v1, v6

    iput v3, p0, Lcom/mediatek/bluetooth/bip/BipImage;->Width:I

    const/4 v3, 0x1

    aget v3, v1, v3

    iput v3, p0, Lcom/mediatek/bluetooth/bip/BipImage;->Height:I

    const-string v3, "BipImage"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Width: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/bluetooth/bip/BipImage;->Width:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "BipImage"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Height: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/bluetooth/bip/BipImage;->Height:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "/data/@btmtk/profile/thumbnail.jpg"

    iput-object v3, p0, Lcom/mediatek/bluetooth/bip/BipImage;->ThumbnailFullPath:Ljava/lang/String;

    const-string v3, "BipImage"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ThumbnailFullPath: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BipImage;->ThumbnailFullPath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Lcom/mediatek/bluetooth/bip/BipImage$CreateThumbnailThread;

    invoke-direct {v3, p0, p1, p5, v0}, Lcom/mediatek/bluetooth/bip/BipImage$CreateThumbnailThread;-><init>(Lcom/mediatek/bluetooth/bip/BipImage;Landroid/net/Uri;Landroid/content/Context;Landroid/content/ContentResolver;)V

    iput-object v3, p0, Lcom/mediatek/bluetooth/bip/BipImage;->mCreateThumbnailThread:Lcom/mediatek/bluetooth/bip/BipImage$CreateThumbnailThread;

    iget-object v3, p0, Lcom/mediatek/bluetooth/bip/BipImage;->mCreateThumbnailThread:Lcom/mediatek/bluetooth/bip/BipImage$CreateThumbnailThread;

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # I
    .param p8    # I

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/mediatek/bluetooth/bip/BipImage;->DirName:Ljava/lang/String;

    iput-object v0, p0, Lcom/mediatek/bluetooth/bip/BipImage;->FileName:Ljava/lang/String;

    iput-object v0, p0, Lcom/mediatek/bluetooth/bip/BipImage;->ThumbnailFullPath:Ljava/lang/String;

    iput v1, p0, Lcom/mediatek/bluetooth/bip/BipImage;->ObjectSize:I

    iput v1, p0, Lcom/mediatek/bluetooth/bip/BipImage;->AcceptableFileSize:I

    iput-object v0, p0, Lcom/mediatek/bluetooth/bip/BipImage;->Version:Ljava/lang/String;

    iput-object v0, p0, Lcom/mediatek/bluetooth/bip/BipImage;->Encoding:Ljava/lang/String;

    iput v1, p0, Lcom/mediatek/bluetooth/bip/BipImage;->Width:I

    iput v1, p0, Lcom/mediatek/bluetooth/bip/BipImage;->Height:I

    iput v1, p0, Lcom/mediatek/bluetooth/bip/BipImage;->Width2:I

    iput v1, p0, Lcom/mediatek/bluetooth/bip/BipImage;->Height2:I

    iput v1, p0, Lcom/mediatek/bluetooth/bip/BipImage;->Size:I

    iput-object v0, p0, Lcom/mediatek/bluetooth/bip/BipImage;->mCreateThumbnailThread:Lcom/mediatek/bluetooth/bip/BipImage$CreateThumbnailThread;

    iput-object p1, p0, Lcom/mediatek/bluetooth/bip/BipImage;->DirName:Ljava/lang/String;

    iput-object p2, p0, Lcom/mediatek/bluetooth/bip/BipImage;->FileName:Ljava/lang/String;

    iput-object p3, p0, Lcom/mediatek/bluetooth/bip/BipImage;->ThumbnailFullPath:Ljava/lang/String;

    invoke-static {p4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/mediatek/bluetooth/bip/BipImage;->ObjectSize:I

    iput-object p5, p0, Lcom/mediatek/bluetooth/bip/BipImage;->Version:Ljava/lang/String;

    iput-object p6, p0, Lcom/mediatek/bluetooth/bip/BipImage;->Encoding:Ljava/lang/String;

    iput p7, p0, Lcom/mediatek/bluetooth/bip/BipImage;->Height:I

    iput p8, p0, Lcom/mediatek/bluetooth/bip/BipImage;->Width:I

    iput v1, p0, Lcom/mediatek/bluetooth/bip/BipImage;->Height2:I

    iput v1, p0, Lcom/mediatek/bluetooth/bip/BipImage;->Width2:I

    invoke-static {p4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/mediatek/bluetooth/bip/BipImage;->Size:I

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/bluetooth/bip/BipImage;Landroid/net/Uri;Landroid/content/Context;Landroid/content/ContentResolver;)Z
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/bip/BipImage;
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/content/ContentResolver;

    invoke-direct {p0, p1, p2, p3}, Lcom/mediatek/bluetooth/bip/BipImage;->createImageThumbnail(Landroid/net/Uri;Landroid/content/Context;Landroid/content/ContentResolver;)Z

    move-result v0

    return v0
.end method

.method private createImageThumbnail(Landroid/net/Uri;Landroid/content/Context;Landroid/content/ContentResolver;)Z
    .locals 35
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/content/ContentResolver;

    const-string v29, "BipImage"

    const-string v30, "Thumbnail creating......"

    invoke-static/range {v29 .. v30}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v18, Ljava/lang/Long;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v29

    const/16 v30, 0x3

    invoke-interface/range {v29 .. v30}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Ljava/lang/String;

    move-object/from16 v0, v18

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Ljava/lang/Long;-><init>(Ljava/lang/String;)V

    const-string v29, "BipImage"

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "getSeg ID: "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Long;->longValue()J

    move-result-wide v29

    const/16 v31, 0x3

    const/16 v32, 0x0

    move-object/from16 v0, p3

    move-wide/from16 v1, v29

    move/from16 v3, v31

    move-object/from16 v4, v32

    invoke-static {v0, v1, v2, v3, v4}, Landroid/provider/MediaStore$Images$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v5

    const-string v29, "content://media/external/images/thumbnails"

    invoke-static/range {v29 .. v29}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v29

    invoke-static/range {v29 .. v29}, Landroid/media/MiniThumbFile;->instance(Landroid/net/Uri;)Landroid/media/MiniThumbFile;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/media/MiniThumbFile;->deactivate()V

    if-eqz v5, :cond_2

    const-string v29, "BipImage"

    const-string v30, "getThumbnail success"

    invoke-static/range {v29 .. v30}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    const-string v29, "BipImage"

    const-string v30, "resize thumbnail......"

    invoke-static/range {v29 .. v30}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v28

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v17

    div-int/lit8 v15, v28, 0x2

    div-int/lit8 v16, v17, 0x2

    mul-int/lit8 v29, v28, 0x78

    move/from16 v0, v29

    div-int/lit16 v9, v0, 0xa0

    const/16 v29, 0x0

    div-int/lit8 v30, v9, 0x2

    sub-int v30, v16, v30

    sub-int v31, v17, v9

    invoke-static/range {v30 .. v31}, Ljava/lang/Math;->min(II)I

    move-result v30

    invoke-static/range {v29 .. v30}, Ljava/lang/Math;->max(II)I

    move-result v12

    const/4 v11, 0x0

    move/from16 v10, v28

    const/high16 v29, 0x43200000

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v30, v0

    div-float v24, v29, v30

    const/16 v29, 0xa0

    const/16 v30, 0x78

    sget-object v31, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static/range {v29 .. v31}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v14

    new-instance v7, Landroid/graphics/Canvas;

    invoke-direct {v7, v14}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v20, Landroid/graphics/Paint;

    invoke-direct/range {v20 .. v20}, Landroid/graphics/Paint;-><init>()V

    const/16 v29, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    const/16 v29, 0x0

    move/from16 v0, v29

    invoke-virtual {v7, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    new-instance v29, Landroid/graphics/Rect;

    add-int v30, v11, v10

    add-int v31, v12, v9

    move-object/from16 v0, v29

    move/from16 v1, v30

    move/from16 v2, v31

    invoke-direct {v0, v11, v12, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v30, Landroid/graphics/Rect;

    const/16 v31, 0x0

    const/16 v32, 0x0

    const/16 v33, 0xa0

    const/16 v34, 0x78

    invoke-direct/range {v30 .. v34}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    move-object/from16 v2, v20

    invoke-virtual {v7, v5, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->recycle()V

    const-string v29, "BipImage"

    const-string v30, "compress and save thumbnail......"

    invoke-static/range {v29 .. v30}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v6, Ljava/io/ByteArrayOutputStream;

    const/16 v29, 0x4b00

    move/from16 v0, v29

    invoke-direct {v6, v0}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    :try_start_0
    sget-object v29, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v30, 0x50

    move-object/from16 v0, v29

    move/from16 v1, v30

    invoke-virtual {v14, v0, v1, v6}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    new-instance v26, Ljava/io/File;

    const-string v29, "/data/@btmtk/profile/"

    move-object/from16 v0, v26

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v26 .. v26}, Ljava/io/File;->exists()Z

    move-result v29

    if-nez v29, :cond_0

    invoke-virtual/range {v26 .. v26}, Ljava/io/File;->mkdirs()Z

    :cond_0
    new-instance v27, Ljava/io/File;

    const-string v29, "/data/@btmtk/profile/"

    const-string v30, "thumbnail.jpg"

    move-object/from16 v0, v27

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {v27 .. v27}, Ljava/io/File;->exists()Z

    move-result v29

    if-eqz v29, :cond_1

    invoke-virtual/range {v27 .. v27}, Ljava/io/File;->delete()Z

    :cond_1
    invoke-virtual/range {v27 .. v27}, Ljava/io/File;->createNewFile()Z

    new-instance v19, Ljava/io/FileOutputStream;

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/io/ByteArrayOutputStream;->writeTo(Ljava/io/OutputStream;)V

    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->close()V

    invoke-virtual/range {v19 .. v19}, Ljava/io/FileOutputStream;->close()V

    invoke-virtual {v14}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "chmod 604 "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/bip/BipImage;->ThumbnailFullPath:Ljava/lang/String;

    move-object/from16 v30, v0

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const-string v29, "BipImage"

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "cmd="

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v8}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v21

    const-string v8, "chmod 777 /data/@btmtk/profile/"

    const-string v29, "BipImage"

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "cmd="

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, v23

    invoke-virtual {v0, v8}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    const/16 v29, 0x1

    :goto_1
    return v29

    :cond_2
    const-string v29, "BipImage"

    const-string v30, "getThumbnail fail"

    invoke-static/range {v29 .. v30}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v29, 0x7f020021

    move-object/from16 v0, v22

    move/from16 v1, v29

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    goto/16 :goto_0

    :catch_0
    move-exception v13

    :try_start_2
    const-string v29, "BipImage"

    const-string v30, "chmod fail!"

    invoke-static/range {v29 .. v30}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v13}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    const/16 v29, 0x0

    goto :goto_1

    :catch_1
    move-exception v13

    const-string v29, "BipImage"

    const-string v30, "save thumbnail fail"

    invoke-static/range {v29 .. v30}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v29, 0x0

    goto :goto_1
.end method

.method private getImageDimension(Landroid/net/Uri;Landroid/content/ContentResolver;)[I
    .locals 11
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/ContentResolver;

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v0, 0x0

    const/4 v6, 0x2

    new-array v1, v6, [I

    :try_start_0
    const-string v6, "r"

    invoke-virtual {p2, p1, v6}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v3

    new-instance v5, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v5}, Landroid/graphics/BitmapFactory$Options;-><init>()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v6, 0x1

    :try_start_1
    iput-boolean v6, v5, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v6, v7, v5}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    const/4 v6, 0x0

    iget v7, v5, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    aput v7, v1, v6

    const/4 v6, 0x1

    iget v7, v5, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    aput v7, v1, v6
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v3, :cond_0

    :try_start_2
    invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    move-object v4, v5

    :cond_1
    :goto_0
    const-string v6, "BipImage"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Dimension[0]: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget v8, v1, v9

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v6, "BipImage"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Dimension[1]: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget v8, v1, v10

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return-object v1

    :cond_2
    :try_start_3
    const-string v6, "BipImage"

    const-string v7, "input != null"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v3, :cond_3

    :try_start_4
    invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_3

    :cond_3
    :goto_2
    move-object v4, v5

    goto :goto_1

    :catch_0
    move-exception v6

    move-object v4, v5

    goto :goto_0

    :catch_1
    move-exception v2

    :goto_3
    const/4 v6, 0x0

    const/4 v7, 0x0

    :try_start_5
    aput v7, v1, v6

    const/4 v6, 0x1

    const/4 v7, 0x0

    aput v7, v1, v6
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-eqz v3, :cond_1

    :try_start_6
    invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_0

    :catch_2
    move-exception v6

    goto :goto_0

    :catchall_0
    move-exception v6

    :goto_4
    if-eqz v3, :cond_4

    :try_start_7
    invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_4

    :cond_4
    :goto_5
    throw v6

    :catch_3
    move-exception v6

    goto :goto_2

    :catch_4
    move-exception v7

    goto :goto_5

    :catchall_1
    move-exception v6

    move-object v4, v5

    goto :goto_4

    :catch_5
    move-exception v2

    move-object v4, v5

    goto :goto_3
.end method

.method private getImageEncoding(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/lang/String;

    const-string v1, "image/jpeg"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "JPEG"

    :goto_0
    const-string v1, "BipImage"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "encoding: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-object v0

    :cond_0
    const-string v1, "image/png"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v0, "PNG"

    goto :goto_0

    :cond_1
    const-string v1, "image/gif"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v0, "GIF"

    goto :goto_0

    :cond_2
    const-string v1, "image/bmp"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "image/x-ms-bmp"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    const-string v0, "BMP"

    goto :goto_0

    :cond_4
    const-string v1, "image/vnd.wap.wbmp"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v0, "WBMP"

    goto :goto_0

    :cond_5
    const-string v1, "BipImage"

    const-string v2, "Unsupport format"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "unknow"

    goto :goto_0
.end method
