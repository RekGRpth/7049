.class Lcom/mediatek/bluetooth/bip/BipService$5;
.super Landroid/os/Handler;
.source "BipService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/bluetooth/bip/BipService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/bluetooth/bip/BipService;


# direct methods
.method constructor <init>(Lcom/mediatek/bluetooth/bip/BipService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 17
    .param p1    # Landroid/os/Message;

    const-string v1, "BipService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Handler(): got msg="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mediatek/bluetooth/bip/BipService;->access$1100()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "BipService"

    const-string v2, "Service does not start"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object/from16 v0, p1

    iget v1, v0, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    const-string v1, "BipService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BIPI_ENABLE_RESULT: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p1

    iget v1, v0, Landroid/os/Message;->arg1:I

    if-eqz v1, :cond_2

    const-string v1, "BipService"

    const-string v2, "enable error, disable bipi"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0x8

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$202(I)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$600(Lcom/mediatek/bluetooth/bip/BipService;)Lcom/mediatek/bluetooth/bip/BluetoothBipServer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->bipiDisable()Z

    goto :goto_0

    :cond_2
    const/4 v1, 0x2

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$202(I)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v2}, Lcom/mediatek/bluetooth/bip/BipService;->access$1200(Lcom/mediatek/bluetooth/bip/BipService;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/bluetooth/bip/BipService;->access$1300(Lcom/mediatek/bluetooth/bip/BipService;Landroid/os/PowerManager$WakeLock;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$1400(Lcom/mediatek/bluetooth/bip/BipService;)V

    goto :goto_0

    :pswitch_2
    const-string v1, "BipService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BIPI_DISABLE_RESULT: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p1

    iget v1, v0, Landroid/os/Message;->arg1:I

    if-eqz v1, :cond_3

    const-string v1, "BipService"

    const-string v2, "disable error"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    const/16 v1, 0x9

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$202(I)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v2}, Lcom/mediatek/bluetooth/bip/BipService;->access$1200(Lcom/mediatek/bluetooth/bip/BipService;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/bluetooth/bip/BipService;->access$1500(Lcom/mediatek/bluetooth/bip/BipService;Landroid/os/PowerManager$WakeLock;)V

    goto/16 :goto_0

    :pswitch_3
    const-string v1, "BipService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MSG_ON_BIPI_OBEX_AUTHREQ:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg2:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v12, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$1000(Lcom/mediatek/bluetooth/bip/BipService;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/mediatek/bluetooth/bip/BipAuthentication;

    invoke-direct {v12, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x50000000

    invoke-virtual {v12, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, "com.mediatek.bluetooth.bipauthentication.extra.FROM"

    const-string v2, "BIPI"

    invoke-virtual {v12, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v1, 0x1

    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->arg2:I

    if-ne v1, v2, :cond_4

    const-string v1, "com.mediatek.bluetooth.bipauthentication.extra.NEED_USERID"

    const/4 v2, 0x1

    invoke-virtual {v12, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-virtual {v1, v12}, Landroid/content/ContextWrapper;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_4
    const-string v1, "com.mediatek.bluetooth.bipauthentication.extra.NEED_USERID"

    const/4 v2, 0x0

    invoke-virtual {v12, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_1

    :pswitch_4
    const-string v1, "BipService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BIPI_CONNECT_CNF: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p1

    iget v1, v0, Landroid/os/Message;->arg1:I

    if-eqz v1, :cond_0

    const-string v1, "BipService"

    const-string v2, "connect error, disable bipi"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0x8

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$202(I)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$600(Lcom/mediatek/bluetooth/bip/BipService;)Lcom/mediatek/bluetooth/bip/BluetoothBipServer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->bipiDisable()Z

    goto/16 :goto_0

    :pswitch_5
    const-string v1, "BipService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BIPI_GET_CAPABILITYT_CNF: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p1

    iget v1, v0, Landroid/os/Message;->arg1:I

    if-eqz v1, :cond_5

    const-string v1, "BipService"

    const-string v2, "get capability error, disconnect bipi"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$1000(Lcom/mediatek/bluetooth/bip/BipService;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f060019

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    const-wide/16 v2, 0x0

    const/16 v4, 0xd

    invoke-static {v1, v2, v3, v4}, Lcom/mediatek/bluetooth/bip/BipService;->access$1600(Lcom/mediatek/bluetooth/bip/BipService;JI)V

    const/4 v1, 0x6

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$202(I)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$600(Lcom/mediatek/bluetooth/bip/BipService;)Lcom/mediatek/bluetooth/bip/BluetoothBipServer;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v2}, Lcom/mediatek/bluetooth/bip/BipService;->access$300(Lcom/mediatek/bluetooth/bip/BipService;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->bipiDisconnect(Ljava/lang/String;)Z

    goto/16 :goto_0

    :cond_5
    const-string v1, "BipService"

    const-string v2, "get capability success"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p1

    iget-object v15, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v15, Lcom/mediatek/bluetooth/bip/Capability;

    const-string v1, "BipService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Version: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v15, Lcom/mediatek/bluetooth/bip/Capability;->PreferFormat:Lcom/mediatek/bluetooth/bip/ImageDescriptor;

    iget-object v3, v3, Lcom/mediatek/bluetooth/bip/ImageDescriptor;->Version:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\tEncoding: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v15, Lcom/mediatek/bluetooth/bip/Capability;->PreferFormat:Lcom/mediatek/bluetooth/bip/ImageDescriptor;

    iget-object v3, v3, Lcom/mediatek/bluetooth/bip/ImageDescriptor;->Encoding:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\tWidth: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v15, Lcom/mediatek/bluetooth/bip/Capability;->PreferFormat:Lcom/mediatek/bluetooth/bip/ImageDescriptor;

    iget v3, v3, Lcom/mediatek/bluetooth/bip/ImageDescriptor;->Width:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\tHeight: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v15, Lcom/mediatek/bluetooth/bip/Capability;->PreferFormat:Lcom/mediatek/bluetooth/bip/ImageDescriptor;

    iget v3, v3, Lcom/mediatek/bluetooth/bip/ImageDescriptor;->Height:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\tWidth2: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v15, Lcom/mediatek/bluetooth/bip/Capability;->PreferFormat:Lcom/mediatek/bluetooth/bip/ImageDescriptor;

    iget v3, v3, Lcom/mediatek/bluetooth/bip/ImageDescriptor;->Width2:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\tHeight2: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v15, Lcom/mediatek/bluetooth/bip/Capability;->PreferFormat:Lcom/mediatek/bluetooth/bip/ImageDescriptor;

    iget v3, v3, Lcom/mediatek/bluetooth/bip/ImageDescriptor;->Height2:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\tSize: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v15, Lcom/mediatek/bluetooth/bip/Capability;->PreferFormat:Lcom/mediatek/bluetooth/bip/ImageDescriptor;

    iget v3, v3, Lcom/mediatek/bluetooth/bip/ImageDescriptor;->Size:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\tTransform: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v15, Lcom/mediatek/bluetooth/bip/Capability;->PreferFormat:Lcom/mediatek/bluetooth/bip/ImageDescriptor;

    iget v3, v3, Lcom/mediatek/bluetooth/bip/ImageDescriptor;->Transform:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\tNumFormats: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v15, Lcom/mediatek/bluetooth/bip/Capability;->NumImageFormats:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v11, 0x0

    :goto_2
    iget v1, v15, Lcom/mediatek/bluetooth/bip/Capability;->NumImageFormats:I

    if-ge v11, v1, :cond_6

    const-string v1, "BipService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SupEncoding: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v15, Lcom/mediatek/bluetooth/bip/Capability;->ImageFormats:[Lcom/mediatek/bluetooth/bip/ImageFormat;

    aget-object v3, v3, v11

    iget-object v3, v3, Lcom/mediatek/bluetooth/bip/ImageFormat;->Encoding:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\tSupWidth: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v15, Lcom/mediatek/bluetooth/bip/Capability;->ImageFormats:[Lcom/mediatek/bluetooth/bip/ImageFormat;

    aget-object v3, v3, v11

    iget v3, v3, Lcom/mediatek/bluetooth/bip/ImageFormat;->Width:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\tSupHeight: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v15, Lcom/mediatek/bluetooth/bip/Capability;->ImageFormats:[Lcom/mediatek/bluetooth/bip/ImageFormat;

    aget-object v3, v3, v11

    iget v3, v3, Lcom/mediatek/bluetooth/bip/ImageFormat;->Height:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\tSupWidth2: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v15, Lcom/mediatek/bluetooth/bip/Capability;->ImageFormats:[Lcom/mediatek/bluetooth/bip/ImageFormat;

    aget-object v3, v3, v11

    iget v3, v3, Lcom/mediatek/bluetooth/bip/ImageFormat;->Width2:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\tSupHeight2: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v15, Lcom/mediatek/bluetooth/bip/Capability;->ImageFormats:[Lcom/mediatek/bluetooth/bip/ImageFormat;

    aget-object v3, v3, v11

    iget v3, v3, Lcom/mediatek/bluetooth/bip/ImageFormat;->Height2:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\tSupSize: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v15, Lcom/mediatek/bluetooth/bip/Capability;->ImageFormats:[Lcom/mediatek/bluetooth/bip/ImageFormat;

    aget-object v3, v3, v11

    iget v3, v3, Lcom/mediatek/bluetooth/bip/ImageFormat;->Size:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    move-object/from16 v16, v0

    new-instance v1, Lcom/mediatek/bluetooth/bip/BipImage;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v2}, Lcom/mediatek/bluetooth/bip/BipService;->access$1800(Lcom/mediatek/bluetooth/bip/BipService;)Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v3}, Lcom/mediatek/bluetooth/bip/BipService;->access$1900(Lcom/mediatek/bluetooth/bip/BipService;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v4}, Lcom/mediatek/bluetooth/bip/BipService;->access$2000(Lcom/mediatek/bluetooth/bip/BipService;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v5}, Lcom/mediatek/bluetooth/bip/BipService;->access$2100(Lcom/mediatek/bluetooth/bip/BipService;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v6}, Lcom/mediatek/bluetooth/bip/BipService;->access$1000(Lcom/mediatek/bluetooth/bip/BipService;)Landroid/content/Context;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, Lcom/mediatek/bluetooth/bip/BipImage;-><init>(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    move-object/from16 v0, v16

    invoke-static {v0, v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$1702(Lcom/mediatek/bluetooth/bip/BipService;Lcom/mediatek/bluetooth/bip/BipImage;)Lcom/mediatek/bluetooth/bip/BipImage;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v2}, Lcom/mediatek/bluetooth/bip/BipService;->access$1700(Lcom/mediatek/bluetooth/bip/BipService;)Lcom/mediatek/bluetooth/bip/BipImage;

    move-result-object v2

    invoke-static {v1, v15, v2}, Lcom/mediatek/bluetooth/bip/BipService;->access$2200(Lcom/mediatek/bluetooth/bip/BipService;Lcom/mediatek/bluetooth/bip/Capability;Lcom/mediatek/bluetooth/bip/BipImage;)Z

    move-result v1

    if-eqz v1, :cond_7

    const/4 v1, 0x4

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$202(I)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v2}, Lcom/mediatek/bluetooth/bip/BipService;->access$1000(Lcom/mediatek/bluetooth/bip/BipService;)Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x3

    invoke-static {v1, v2, v3, v4}, Lcom/mediatek/bluetooth/bip/BipService;->access$2300(Lcom/mediatek/bluetooth/bip/BipService;Landroid/content/Context;ZI)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$600(Lcom/mediatek/bluetooth/bip/BipService;)Lcom/mediatek/bluetooth/bip/BluetoothBipServer;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v2}, Lcom/mediatek/bluetooth/bip/BipService;->access$300(Lcom/mediatek/bluetooth/bip/BipService;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v3}, Lcom/mediatek/bluetooth/bip/BipService;->access$1700(Lcom/mediatek/bluetooth/bip/BipService;)Lcom/mediatek/bluetooth/bip/BipImage;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->bipiPushImage(Ljava/lang/String;Lcom/mediatek/bluetooth/bip/BipImage;)Z

    goto/16 :goto_0

    :cond_7
    const-string v1, "BipService"

    const-string v2, "Responder dosen\'t support this format"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v12, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$1000(Lcom/mediatek/bluetooth/bip/BipService;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/mediatek/bluetooth/bip/BipInitPushConfirmation;

    invoke-direct {v12, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x50000000

    invoke-virtual {v12, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-virtual {v1, v12}, Landroid/content/ContextWrapper;->startActivity(Landroid/content/Intent;)V

    const/16 v1, 0x8

    const/4 v2, -0x1

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x4e20

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    :pswitch_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$1000(Lcom/mediatek/bluetooth/bip/BipService;)Landroid/content/Context;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    const v3, 0x7f06001b

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v6}, Lcom/mediatek/bluetooth/bip/BipService;->access$2400(Lcom/mediatek/bluetooth/bip/BipService;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    const-string v1, "BipService"

    const-string v2, "BIPI_IMAGE_PUSH_START"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :pswitch_7
    const-string v1, "BipService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BIPI_PROGRESS_IND: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "BipService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "progress: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg2:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mediatek/bluetooth/bip/BipService;->access$200()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->arg2:I

    int-to-long v2, v2

    const/16 v4, 0xb

    invoke-static {v1, v2, v3, v4}, Lcom/mediatek/bluetooth/bip/BipService;->access$1600(Lcom/mediatek/bluetooth/bip/BipService;JI)V

    goto/16 :goto_0

    :pswitch_8
    const-string v1, "BipService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BIPI_PUSH_CNF: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p1

    iget v1, v0, Landroid/os/Message;->arg1:I

    if-eqz v1, :cond_9

    const-string v1, "BipService"

    const-string v2, "push error, disconnect bipi"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mediatek/bluetooth/bip/BipService;->access$200()I

    move-result v1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_8

    new-instance v8, Landroid/content/Intent;

    const-string v1, "com.mediatek.bluetooth.bipinitpushconfirmation.action.TIMEOUT"

    invoke-direct {v8, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-virtual {v1, v8}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    :cond_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    const-wide/16 v2, 0x0

    const/16 v4, 0xd

    invoke-static {v1, v2, v3, v4}, Lcom/mediatek/bluetooth/bip/BipService;->access$1600(Lcom/mediatek/bluetooth/bip/BipService;JI)V

    :goto_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$600(Lcom/mediatek/bluetooth/bip/BipService;)Lcom/mediatek/bluetooth/bip/BluetoothBipServer;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v2}, Lcom/mediatek/bluetooth/bip/BipService;->access$300(Lcom/mediatek/bluetooth/bip/BipService;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->bipiDisconnect(Ljava/lang/String;)Z

    const/4 v1, 0x6

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$202(I)I

    goto/16 :goto_0

    :cond_9
    const-string v1, "BipService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "image handler: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg2:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v2}, Lcom/mediatek/bluetooth/bip/BipService;->access$1700(Lcom/mediatek/bluetooth/bip/BipService;)Lcom/mediatek/bluetooth/bip/BipImage;

    move-result-object v2

    iget v2, v2, Lcom/mediatek/bluetooth/bip/BipImage;->Size:I

    int-to-long v2, v2

    const/16 v4, 0xc

    invoke-static {v1, v2, v3, v4}, Lcom/mediatek/bluetooth/bip/BipService;->access$1600(Lcom/mediatek/bluetooth/bip/BipService;JI)V

    goto :goto_3

    :pswitch_9
    const-string v1, "BipService"

    const-string v2, "BIPI_THUMBNAIL_REQ"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "BipService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "image handler: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg2:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$600(Lcom/mediatek/bluetooth/bip/BipService;)Lcom/mediatek/bluetooth/bip/BluetoothBipServer;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v2}, Lcom/mediatek/bluetooth/bip/BipService;->access$300(Lcom/mediatek/bluetooth/bip/BipService;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v3}, Lcom/mediatek/bluetooth/bip/BipService;->access$1700(Lcom/mediatek/bluetooth/bip/BipService;)Lcom/mediatek/bluetooth/bip/BipImage;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->bipiPushThumbnail(Ljava/lang/String;Lcom/mediatek/bluetooth/bip/BipImage;)Z

    const/4 v1, 0x5

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$202(I)I

    goto/16 :goto_0

    :pswitch_a
    const-string v1, "BipService"

    const-string v2, "BIPI_THUMBNAIL_PUSH_START"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :pswitch_b
    const-string v1, "BipService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BIPI_THUMBNAIL_PUSH_CNF: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p1

    iget v1, v0, Landroid/os/Message;->arg1:I

    if-eqz v1, :cond_a

    const-string v1, "BipService"

    const-string v2, "push thumbnail error, disconnect bipi"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    const-wide/16 v2, 0x0

    const/16 v4, 0xd

    invoke-static {v1, v2, v3, v4}, Lcom/mediatek/bluetooth/bip/BipService;->access$1600(Lcom/mediatek/bluetooth/bip/BipService;JI)V

    :goto_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$600(Lcom/mediatek/bluetooth/bip/BipService;)Lcom/mediatek/bluetooth/bip/BluetoothBipServer;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v2}, Lcom/mediatek/bluetooth/bip/BipService;->access$300(Lcom/mediatek/bluetooth/bip/BipService;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->bipiDisconnect(Ljava/lang/String;)Z

    const/4 v1, 0x6

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$202(I)I

    goto/16 :goto_0

    :cond_a
    const-string v1, "BipService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "image handler: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg2:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v2}, Lcom/mediatek/bluetooth/bip/BipService;->access$1700(Lcom/mediatek/bluetooth/bip/BipService;)Lcom/mediatek/bluetooth/bip/BipImage;

    move-result-object v2

    iget v2, v2, Lcom/mediatek/bluetooth/bip/BipImage;->Size:I

    int-to-long v2, v2

    const/16 v4, 0xc

    invoke-static {v1, v2, v3, v4}, Lcom/mediatek/bluetooth/bip/BipService;->access$1600(Lcom/mediatek/bluetooth/bip/BipService;JI)V

    goto :goto_4

    :pswitch_c
    const-string v1, "BipService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BIPI_DISCONNECT_RESULT: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p1

    iget v1, v0, Landroid/os/Message;->arg1:I

    if-eqz v1, :cond_b

    const-string v1, "BipService"

    const-string v2, "disconnect error, disable bipi"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_b
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v2}, Lcom/mediatek/bluetooth/bip/BipService;->access$1000(Lcom/mediatek/bluetooth/bip/BipService;)Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x7

    invoke-static {v1, v2, v3, v4}, Lcom/mediatek/bluetooth/bip/BipService;->access$2300(Lcom/mediatek/bluetooth/bip/BipService;Landroid/content/Context;ZI)V

    const-wide/16 v1, 0x294

    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$1400(Lcom/mediatek/bluetooth/bip/BipService;)V

    goto/16 :goto_0

    :catch_0
    move-exception v10

    invoke-virtual {v10}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_5

    :pswitch_d
    const-string v1, "BipService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BIPI_CANCEL_RESULT: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p1

    iget v1, v0, Landroid/os/Message;->arg1:I

    if-eqz v1, :cond_0

    const-string v1, "BipService"

    const-string v2, "cancel error, disconnecting..."

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :pswitch_e
    const-string v1, "BipService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BIPR_ENABLE_RESULT: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p1

    iget v1, v0, Landroid/os/Message;->arg1:I

    if-eqz v1, :cond_c

    const-string v1, "BipService"

    const-string v2, "enable error, disable bipr"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$600(Lcom/mediatek/bluetooth/bip/BipService;)Lcom/mediatek/bluetooth/bip/BluetoothBipServer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->biprDisable()Z

    const/16 v1, 0x9

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$702(I)I

    goto/16 :goto_0

    :cond_c
    const/4 v1, 0x2

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$702(I)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v2}, Lcom/mediatek/bluetooth/bip/BipService;->access$1000(Lcom/mediatek/bluetooth/bip/BipService;)Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x2

    invoke-static {v1, v2, v3, v4}, Lcom/mediatek/bluetooth/bip/BipService;->access$2300(Lcom/mediatek/bluetooth/bip/BipService;Landroid/content/Context;ZI)V

    goto/16 :goto_0

    :pswitch_f
    const-string v1, "BipService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BIPR_DISABLE_RESULT: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p1

    iget v1, v0, Landroid/os/Message;->arg1:I

    if-eqz v1, :cond_d

    const-string v1, "BipService"

    const-string v2, "disable error"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_d
    const/16 v1, 0xa

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$702(I)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    new-instance v2, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v3}, Lcom/mediatek/bluetooth/bip/BipService;->access$1000(Lcom/mediatek/bluetooth/bip/BipService;)Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/mediatek/bluetooth/bip/BipService;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Landroid/content/ContextWrapper;->stopService(Landroid/content/Intent;)Z

    goto/16 :goto_0

    :pswitch_10
    const-string v1, "BipService"

    const-string v2, "BIPR_AUTH_REQ"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p1

    iget-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, [Ljava/lang/String;

    move-object v7, v1

    check-cast v7, [Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    const/4 v2, 0x0

    aget-object v2, v7, v2

    invoke-static {v1, v2}, Lcom/mediatek/bluetooth/bip/BipService;->access$2502(Lcom/mediatek/bluetooth/bip/BipService;Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    const/4 v2, 0x1

    aget-object v2, v7, v2

    invoke-static {v1, v2}, Lcom/mediatek/bluetooth/bip/BipService;->access$802(Lcom/mediatek/bluetooth/bip/BipService;Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$2500(Lcom/mediatek/bluetooth/bip/BipService;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_e

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    const-string v2, "UNKNOWN DEVICE"

    invoke-static {v1, v2}, Lcom/mediatek/bluetooth/bip/BipService;->access$2502(Lcom/mediatek/bluetooth/bip/BipService;Ljava/lang/String;)Ljava/lang/String;

    :cond_e
    const/4 v1, 0x3

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$702(I)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$600(Lcom/mediatek/bluetooth/bip/BipService;)Lcom/mediatek/bluetooth/bip/BluetoothBipServer;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->biprAuthorizeRsp(I)Z

    goto/16 :goto_0

    :pswitch_11
    const-string v1, "BipService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MSG_ON_BIPI_OBEX_AUTHREQ:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg2:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v12, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$1000(Lcom/mediatek/bluetooth/bip/BipService;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/mediatek/bluetooth/bip/BipAuthentication;

    invoke-direct {v12, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x50000000

    invoke-virtual {v12, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, "com.mediatek.bluetooth.bipauthentication.extra.FROM"

    const-string v2, "BIPR"

    invoke-virtual {v12, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v1, 0x1

    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->arg2:I

    if-ne v1, v2, :cond_f

    const-string v1, "com.mediatek.bluetooth.bipauthentication.extra.NEED_USERID"

    const/4 v2, 0x1

    invoke-virtual {v12, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :goto_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-virtual {v1, v12}, Landroid/content/ContextWrapper;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_f
    const-string v1, "com.mediatek.bluetooth.bipauthentication.extra.NEED_USERID"

    const/4 v2, 0x0

    invoke-virtual {v12, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_6

    :pswitch_12
    const-string v1, "BipService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BIPR_CONNECT_CNF: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p1

    iget v1, v0, Landroid/os/Message;->arg1:I

    if-eqz v1, :cond_0

    const-string v1, "BipService"

    const-string v2, "connect error, disable bipi"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$600(Lcom/mediatek/bluetooth/bip/BipService;)Lcom/mediatek/bluetooth/bip/BluetoothBipServer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->biprDisable()Z

    const/16 v1, 0x9

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$702(I)I

    goto/16 :goto_0

    :pswitch_13
    const/16 v1, 0x21

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    invoke-static {}, Lcom/mediatek/bluetooth/bip/BipService;->access$2600()I

    move-result v1

    const v2, 0x26259fe

    if-ge v1, v2, :cond_10

    invoke-static {}, Lcom/mediatek/bluetooth/bip/BipService;->access$2608()I

    :goto_7
    const-string v1, "BipService"

    const-string v2, "MSG_ON_BIPR_GET_CAPABILITY"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v9, Lcom/mediatek/bluetooth/bip/Capability;

    const/4 v1, 0x4

    invoke-direct {v9, v1}, Lcom/mediatek/bluetooth/bip/Capability;-><init>(I)V

    iget-object v1, v9, Lcom/mediatek/bluetooth/bip/Capability;->PreferFormat:Lcom/mediatek/bluetooth/bip/ImageDescriptor;

    const-string v2, "1.0"

    iput-object v2, v1, Lcom/mediatek/bluetooth/bip/ImageDescriptor;->Version:Ljava/lang/String;

    iget-object v1, v9, Lcom/mediatek/bluetooth/bip/Capability;->PreferFormat:Lcom/mediatek/bluetooth/bip/ImageDescriptor;

    const-string v2, "JPEG"

    iput-object v2, v1, Lcom/mediatek/bluetooth/bip/ImageDescriptor;->Encoding:Ljava/lang/String;

    iget-object v1, v9, Lcom/mediatek/bluetooth/bip/Capability;->PreferFormat:Lcom/mediatek/bluetooth/bip/ImageDescriptor;

    const/4 v2, 0x1

    iput v2, v1, Lcom/mediatek/bluetooth/bip/ImageDescriptor;->Width:I

    iget-object v1, v9, Lcom/mediatek/bluetooth/bip/Capability;->PreferFormat:Lcom/mediatek/bluetooth/bip/ImageDescriptor;

    const/4 v2, 0x1

    iput v2, v1, Lcom/mediatek/bluetooth/bip/ImageDescriptor;->Height:I

    iget-object v1, v9, Lcom/mediatek/bluetooth/bip/Capability;->PreferFormat:Lcom/mediatek/bluetooth/bip/ImageDescriptor;

    const v2, 0xffff

    iput v2, v1, Lcom/mediatek/bluetooth/bip/ImageDescriptor;->Width2:I

    iget-object v1, v9, Lcom/mediatek/bluetooth/bip/Capability;->PreferFormat:Lcom/mediatek/bluetooth/bip/ImageDescriptor;

    const v2, 0xffff

    iput v2, v1, Lcom/mediatek/bluetooth/bip/ImageDescriptor;->Height2:I

    iget-object v1, v9, Lcom/mediatek/bluetooth/bip/Capability;->PreferFormat:Lcom/mediatek/bluetooth/bip/ImageDescriptor;

    const v2, 0x1312d00

    iput v2, v1, Lcom/mediatek/bluetooth/bip/ImageDescriptor;->Size:I

    iget-object v1, v9, Lcom/mediatek/bluetooth/bip/Capability;->PreferFormat:Lcom/mediatek/bluetooth/bip/ImageDescriptor;

    const/4 v2, 0x0

    iput v2, v1, Lcom/mediatek/bluetooth/bip/ImageDescriptor;->Transform:I

    iget-object v1, v9, Lcom/mediatek/bluetooth/bip/Capability;->ImageFormats:[Lcom/mediatek/bluetooth/bip/ImageFormat;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    const-string v2, "JPEG"

    iput-object v2, v1, Lcom/mediatek/bluetooth/bip/ImageFormat;->Encoding:Ljava/lang/String;

    iget-object v1, v9, Lcom/mediatek/bluetooth/bip/Capability;->ImageFormats:[Lcom/mediatek/bluetooth/bip/ImageFormat;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    const/4 v2, 0x1

    iput v2, v1, Lcom/mediatek/bluetooth/bip/ImageFormat;->Width:I

    iget-object v1, v9, Lcom/mediatek/bluetooth/bip/Capability;->ImageFormats:[Lcom/mediatek/bluetooth/bip/ImageFormat;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    const/4 v2, 0x1

    iput v2, v1, Lcom/mediatek/bluetooth/bip/ImageFormat;->Height:I

    iget-object v1, v9, Lcom/mediatek/bluetooth/bip/Capability;->ImageFormats:[Lcom/mediatek/bluetooth/bip/ImageFormat;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    const v2, 0xffff

    iput v2, v1, Lcom/mediatek/bluetooth/bip/ImageFormat;->Width2:I

    iget-object v1, v9, Lcom/mediatek/bluetooth/bip/Capability;->ImageFormats:[Lcom/mediatek/bluetooth/bip/ImageFormat;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    const v2, 0xffff

    iput v2, v1, Lcom/mediatek/bluetooth/bip/ImageFormat;->Height2:I

    iget-object v1, v9, Lcom/mediatek/bluetooth/bip/Capability;->ImageFormats:[Lcom/mediatek/bluetooth/bip/ImageFormat;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    const v2, 0x1312d00

    iput v2, v1, Lcom/mediatek/bluetooth/bip/ImageFormat;->Size:I

    iget-object v1, v9, Lcom/mediatek/bluetooth/bip/Capability;->ImageFormats:[Lcom/mediatek/bluetooth/bip/ImageFormat;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    const-string v2, "PNG"

    iput-object v2, v1, Lcom/mediatek/bluetooth/bip/ImageFormat;->Encoding:Ljava/lang/String;

    iget-object v1, v9, Lcom/mediatek/bluetooth/bip/Capability;->ImageFormats:[Lcom/mediatek/bluetooth/bip/ImageFormat;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    const/4 v2, 0x1

    iput v2, v1, Lcom/mediatek/bluetooth/bip/ImageFormat;->Width:I

    iget-object v1, v9, Lcom/mediatek/bluetooth/bip/Capability;->ImageFormats:[Lcom/mediatek/bluetooth/bip/ImageFormat;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    const/4 v2, 0x1

    iput v2, v1, Lcom/mediatek/bluetooth/bip/ImageFormat;->Height:I

    iget-object v1, v9, Lcom/mediatek/bluetooth/bip/Capability;->ImageFormats:[Lcom/mediatek/bluetooth/bip/ImageFormat;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    const v2, 0xffff

    iput v2, v1, Lcom/mediatek/bluetooth/bip/ImageFormat;->Width2:I

    iget-object v1, v9, Lcom/mediatek/bluetooth/bip/Capability;->ImageFormats:[Lcom/mediatek/bluetooth/bip/ImageFormat;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    const v2, 0xffff

    iput v2, v1, Lcom/mediatek/bluetooth/bip/ImageFormat;->Height2:I

    iget-object v1, v9, Lcom/mediatek/bluetooth/bip/Capability;->ImageFormats:[Lcom/mediatek/bluetooth/bip/ImageFormat;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    const v2, 0x1312d00

    iput v2, v1, Lcom/mediatek/bluetooth/bip/ImageFormat;->Size:I

    iget-object v1, v9, Lcom/mediatek/bluetooth/bip/Capability;->ImageFormats:[Lcom/mediatek/bluetooth/bip/ImageFormat;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    const-string v2, "BMP"

    iput-object v2, v1, Lcom/mediatek/bluetooth/bip/ImageFormat;->Encoding:Ljava/lang/String;

    iget-object v1, v9, Lcom/mediatek/bluetooth/bip/Capability;->ImageFormats:[Lcom/mediatek/bluetooth/bip/ImageFormat;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    const/4 v2, 0x1

    iput v2, v1, Lcom/mediatek/bluetooth/bip/ImageFormat;->Width:I

    iget-object v1, v9, Lcom/mediatek/bluetooth/bip/Capability;->ImageFormats:[Lcom/mediatek/bluetooth/bip/ImageFormat;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    const/4 v2, 0x1

    iput v2, v1, Lcom/mediatek/bluetooth/bip/ImageFormat;->Height:I

    iget-object v1, v9, Lcom/mediatek/bluetooth/bip/Capability;->ImageFormats:[Lcom/mediatek/bluetooth/bip/ImageFormat;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    const/16 v2, 0x7d0

    iput v2, v1, Lcom/mediatek/bluetooth/bip/ImageFormat;->Width2:I

    iget-object v1, v9, Lcom/mediatek/bluetooth/bip/Capability;->ImageFormats:[Lcom/mediatek/bluetooth/bip/ImageFormat;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    const/16 v2, 0x7d0

    iput v2, v1, Lcom/mediatek/bluetooth/bip/ImageFormat;->Height2:I

    iget-object v1, v9, Lcom/mediatek/bluetooth/bip/Capability;->ImageFormats:[Lcom/mediatek/bluetooth/bip/ImageFormat;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    const v2, 0x1312d00

    iput v2, v1, Lcom/mediatek/bluetooth/bip/ImageFormat;->Size:I

    iget-object v1, v9, Lcom/mediatek/bluetooth/bip/Capability;->ImageFormats:[Lcom/mediatek/bluetooth/bip/ImageFormat;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    const-string v2, "GIF"

    iput-object v2, v1, Lcom/mediatek/bluetooth/bip/ImageFormat;->Encoding:Ljava/lang/String;

    iget-object v1, v9, Lcom/mediatek/bluetooth/bip/Capability;->ImageFormats:[Lcom/mediatek/bluetooth/bip/ImageFormat;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    const/4 v2, 0x1

    iput v2, v1, Lcom/mediatek/bluetooth/bip/ImageFormat;->Width:I

    iget-object v1, v9, Lcom/mediatek/bluetooth/bip/Capability;->ImageFormats:[Lcom/mediatek/bluetooth/bip/ImageFormat;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    const/4 v2, 0x1

    iput v2, v1, Lcom/mediatek/bluetooth/bip/ImageFormat;->Height:I

    iget-object v1, v9, Lcom/mediatek/bluetooth/bip/Capability;->ImageFormats:[Lcom/mediatek/bluetooth/bip/ImageFormat;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    const/16 v2, 0x7d0

    iput v2, v1, Lcom/mediatek/bluetooth/bip/ImageFormat;->Width2:I

    iget-object v1, v9, Lcom/mediatek/bluetooth/bip/Capability;->ImageFormats:[Lcom/mediatek/bluetooth/bip/ImageFormat;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    const/16 v2, 0x7d0

    iput v2, v1, Lcom/mediatek/bluetooth/bip/ImageFormat;->Height2:I

    iget-object v1, v9, Lcom/mediatek/bluetooth/bip/Capability;->ImageFormats:[Lcom/mediatek/bluetooth/bip/ImageFormat;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    const v2, 0x1312d00

    iput v2, v1, Lcom/mediatek/bluetooth/bip/ImageFormat;->Size:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$600(Lcom/mediatek/bluetooth/bip/BipService;)Lcom/mediatek/bluetooth/bip/BluetoothBipServer;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v9}, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->biprGetCapabilityRsp(ILcom/mediatek/bluetooth/bip/Capability;)Z

    goto/16 :goto_0

    :cond_10
    const v1, 0x2160ec1

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$2602(I)I

    goto/16 :goto_7

    :pswitch_14
    const-string v1, "BipService"

    const-string v2, "MSG_ON_BIPR_CAPABILITY_RES"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p1

    iget v1, v0, Landroid/os/Message;->arg1:I

    if-eqz v1, :cond_0

    const-string v1, "BipService"

    const-string v2, "issue capability response error"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :pswitch_15
    const-string v1, "BipService"

    const-string v2, "MSG_ON_BIPR_ACCESS_REQ"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    move-object/from16 v0, p1

    iget-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-static {v2, v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$2702(Lcom/mediatek/bluetooth/bip/BipService;Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->arg2:I

    invoke-static {v1, v2}, Lcom/mediatek/bluetooth/bip/BipService;->access$2802(Lcom/mediatek/bluetooth/bip/BipService;I)I

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mounted"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_11

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$1000(Lcom/mediatek/bluetooth/bip/BipService;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f06001a

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    const/4 v1, 0x7

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$702(I)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$600(Lcom/mediatek/bluetooth/bip/BipService;)Lcom/mediatek/bluetooth/bip/BluetoothBipServer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->biprDisconnect()Z

    goto/16 :goto_0

    :cond_11
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$1000(Lcom/mediatek/bluetooth/bip/BipService;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/mediatek/bluetooth/util/SystemUtils;->getReceivedFilePath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v14

    new-instance v13, Ljava/io/File;

    invoke-direct {v13, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_12

    invoke-virtual {v13}, Ljava/io/File;->mkdirs()Z

    :cond_12
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1, v14}, Lcom/mediatek/bluetooth/bip/BipService;->access$2902(Lcom/mediatek/bluetooth/bip/BipService;Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    invoke-static {v1, v2}, Lcom/mediatek/bluetooth/bip/BipService;->access$3002(Lcom/mediatek/bluetooth/bip/BipService;Landroid/content/ContentValues;)Landroid/content/ContentValues;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$3000(Lcom/mediatek/bluetooth/bip/BipService;)Landroid/content/ContentValues;

    move-result-object v1

    const-string v2, "type"

    const/16 v3, 0x1f

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$3000(Lcom/mediatek/bluetooth/bip/BipService;)Landroid/content/ContentValues;

    move-result-object v1

    const-string v2, "state"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$3000(Lcom/mediatek/bluetooth/bip/BipService;)Landroid/content/ContentValues;

    move-result-object v1

    const-string v2, "result"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$3000(Lcom/mediatek/bluetooth/bip/BipService;)Landroid/content/ContentValues;

    move-result-object v1

    const-string v2, "name"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v3}, Lcom/mediatek/bluetooth/bip/BipService;->access$2700(Lcom/mediatek/bluetooth/bip/BipService;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$3000(Lcom/mediatek/bluetooth/bip/BipService;)Landroid/content/ContentValues;

    move-result-object v1

    const-string v2, "uri"

    const-string v3, "xxx"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$3000(Lcom/mediatek/bluetooth/bip/BipService;)Landroid/content/ContentValues;

    move-result-object v1

    const-string v2, "data"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v4}, Lcom/mediatek/bluetooth/bip/BipService;->access$2900(Lcom/mediatek/bluetooth/bip/BipService;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v4}, Lcom/mediatek/bluetooth/bip/BipService;->access$2700(Lcom/mediatek/bluetooth/bip/BipService;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$3000(Lcom/mediatek/bluetooth/bip/BipService;)Landroid/content/ContentValues;

    move-result-object v1

    const-string v2, "mime"

    const-string v3, "image/*"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$3000(Lcom/mediatek/bluetooth/bip/BipService;)Landroid/content/ContentValues;

    move-result-object v1

    const-string v2, "peer_name"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v3}, Lcom/mediatek/bluetooth/bip/BipService;->access$2500(Lcom/mediatek/bluetooth/bip/BipService;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$3000(Lcom/mediatek/bluetooth/bip/BipService;)Landroid/content/ContentValues;

    move-result-object v1

    const-string v2, "peer_addr"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v3}, Lcom/mediatek/bluetooth/bip/BipService;->access$800(Lcom/mediatek/bluetooth/bip/BipService;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$3000(Lcom/mediatek/bluetooth/bip/BipService;)Landroid/content/ContentValues;

    move-result-object v1

    const-string v2, "total"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v3}, Lcom/mediatek/bluetooth/bip/BipService;->access$2800(Lcom/mediatek/bluetooth/bip/BipService;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$3000(Lcom/mediatek/bluetooth/bip/BipService;)Landroid/content/ContentValues;

    move-result-object v1

    const-string v2, "done"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v2}, Lcom/mediatek/bluetooth/bip/BipService;->access$100(Lcom/mediatek/bluetooth/bip/BipService;)Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/mediatek/bluetooth/share/BluetoothShareTask$BluetoothShareTaskMetaData;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v4}, Lcom/mediatek/bluetooth/bip/BipService;->access$3000(Lcom/mediatek/bluetooth/bip/BipService;)Landroid/content/ContentValues;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/bluetooth/bip/BipService;->access$3102(Lcom/mediatek/bluetooth/bip/BipService;Landroid/net/Uri;)Landroid/net/Uri;

    invoke-static {}, Lcom/mediatek/bluetooth/bip/BipService;->access$3200()Z

    move-result v1

    if-eqz v1, :cond_13

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$3300(Lcom/mediatek/bluetooth/bip/BipService;)V

    goto/16 :goto_0

    :cond_13
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->arg2:I

    div-int/lit16 v2, v2, 0x400

    int-to-long v2, v2

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4}, Lcom/mediatek/bluetooth/bip/BipService;->access$1600(Lcom/mediatek/bluetooth/bip/BipService;JI)V

    const/16 v1, 0x1f

    const/4 v2, -0x1

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x4e20

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    :pswitch_16
    const-string v1, "BipService"

    const-string v2, "MSG_ON_BIPR_IMAGE_RECEIVE_START"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$3400(Lcom/mediatek/bluetooth/bip/BipService;)Landroid/media/MediaScannerConnection;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/MediaScannerConnection;->connect()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v2}, Lcom/mediatek/bluetooth/bip/BipService;->access$3500(Lcom/mediatek/bluetooth/bip/BipService;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/bluetooth/bip/BipService;->access$1300(Lcom/mediatek/bluetooth/bip/BipService;Landroid/os/PowerManager$WakeLock;)V

    goto/16 :goto_0

    :pswitch_17
    const-string v1, "BipService"

    const-string v2, "BIPR_PROGRESS_IND"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "BipService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "object length: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "BipService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "progress: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg2:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->arg1:I

    invoke-static {v1, v2}, Lcom/mediatek/bluetooth/bip/BipService;->access$2802(Lcom/mediatek/bluetooth/bip/BipService;I)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->arg2:I

    invoke-static {v1, v2}, Lcom/mediatek/bluetooth/bip/BipService;->access$3602(Lcom/mediatek/bluetooth/bip/BipService;I)I

    invoke-static {}, Lcom/mediatek/bluetooth/bip/BipService;->access$700()I

    move-result v1

    const/4 v2, 0x5

    if-ne v1, v2, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->arg2:I

    int-to-long v2, v2

    const/16 v4, 0x15

    invoke-static {v1, v2, v3, v4}, Lcom/mediatek/bluetooth/bip/BipService;->access$1600(Lcom/mediatek/bluetooth/bip/BipService;JI)V

    goto/16 :goto_0

    :pswitch_18
    const-string v1, "BipService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BIPR_RECEIVE_CNF: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$500(Lcom/mediatek/bluetooth/bip/BipService;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x1f

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v2}, Lcom/mediatek/bluetooth/bip/BipService;->access$3500(Lcom/mediatek/bluetooth/bip/BipService;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/bluetooth/bip/BipService;->access$1500(Lcom/mediatek/bluetooth/bip/BipService;Landroid/os/PowerManager$WakeLock;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$500(Lcom/mediatek/bluetooth/bip/BipService;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x23

    const-wide/16 v3, 0x4e20

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    move-object/from16 v0, p1

    iget v1, v0, Landroid/os/Message;->arg1:I

    if-eqz v1, :cond_16

    invoke-static {}, Lcom/mediatek/bluetooth/bip/BipService;->access$700()I

    move-result v1

    const/4 v2, 0x5

    if-eq v1, v2, :cond_14

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$900(Lcom/mediatek/bluetooth/bip/BipService;)Landroid/app/NotificationManager;

    move-result-object v1

    const v2, 0x26259ff

    invoke-virtual {v1, v2}, Landroid/app/NotificationManager;->cancel(I)V

    new-instance v8, Landroid/content/Intent;

    const-string v1, "com.mediatek.bluetooth.biprpushconfirmation.action.CANCEL_BY_PEER"

    invoke-direct {v8, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-virtual {v1, v8}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    :cond_14
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    const-wide/16 v2, 0x0

    const/16 v4, 0x17

    invoke-static {v1, v2, v3, v4}, Lcom/mediatek/bluetooth/bip/BipService;->access$1600(Lcom/mediatek/bluetooth/bip/BipService;JI)V

    const-string v1, "BipService"

    const-string v2, "receive error, disconnect bipr"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x7

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$702(I)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$600(Lcom/mediatek/bluetooth/bip/BipService;)Lcom/mediatek/bluetooth/bip/BluetoothBipServer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->biprDisconnect()Z

    :goto_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$2500(Lcom/mediatek/bluetooth/bip/BipService;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "PTS"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_15

    const/16 v1, 0x21

    const-wide/16 v2, 0x1388

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_15
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$3400(Lcom/mediatek/bluetooth/bip/BipService;)Landroid/media/MediaScannerConnection;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/MediaScannerConnection;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$3400(Lcom/mediatek/bluetooth/bip/BipService;)Landroid/media/MediaScannerConnection;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v3}, Lcom/mediatek/bluetooth/bip/BipService;->access$1000(Lcom/mediatek/bluetooth/bip/BipService;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/mediatek/bluetooth/util/SystemUtils;->getReceivedFilePath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v3}, Lcom/mediatek/bluetooth/bip/BipService;->access$2700(Lcom/mediatek/bluetooth/bip/BipService;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/media/MediaScannerConnection;->scanFile(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_16
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$3600(Lcom/mediatek/bluetooth/bip/BipService;)I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v2}, Lcom/mediatek/bluetooth/bip/BipService;->access$2800(Lcom/mediatek/bluetooth/bip/BipService;)I

    move-result v2

    if-eq v1, v2, :cond_17

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$3600(Lcom/mediatek/bluetooth/bip/BipService;)I

    move-result v1

    if-nez v1, :cond_18

    :cond_17
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v2}, Lcom/mediatek/bluetooth/bip/BipService;->access$2800(Lcom/mediatek/bluetooth/bip/BipService;)I

    move-result v2

    int-to-long v2, v2

    const/16 v4, 0x16

    invoke-static {v1, v2, v3, v4}, Lcom/mediatek/bluetooth/bip/BipService;->access$1600(Lcom/mediatek/bluetooth/bip/BipService;JI)V

    goto/16 :goto_8

    :cond_18
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    const-wide/16 v2, 0x0

    const/16 v4, 0x17

    invoke-static {v1, v2, v3, v4}, Lcom/mediatek/bluetooth/bip/BipService;->access$1600(Lcom/mediatek/bluetooth/bip/BipService;JI)V

    goto/16 :goto_8

    :pswitch_19
    const/16 v1, 0x21

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    const/4 v1, 0x6

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$702(I)I

    const-string v1, "BipService"

    const-string v2, "MSG_ON_BIPR_THUMBNAIL_RECEIVE_START"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :pswitch_1a
    const-string v1, "BipService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BIPR_THUMBNAIL_RECEIVE_CNF: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p1

    iget v1, v0, Landroid/os/Message;->arg1:I

    if-eqz v1, :cond_19

    const-string v1, "BipService"

    const-string v2, "receive error, disconnect bipr"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_19
    const/4 v1, 0x7

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$702(I)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$600(Lcom/mediatek/bluetooth/bip/BipService;)Lcom/mediatek/bluetooth/bip/BluetoothBipServer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->biprDisconnect()Z

    goto/16 :goto_0

    :pswitch_1b
    const-string v1, "BipService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BIPR_DISCONNECT_RESULT: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p1

    iget v1, v0, Landroid/os/Message;->arg1:I

    if-eqz v1, :cond_1a

    const-string v1, "BipService"

    const-string v2, "disconnect error, disable bipr"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1a
    invoke-static {}, Lcom/mediatek/bluetooth/bip/BipService;->access$2600()I

    move-result v1

    const v2, 0x26259fe

    if-ge v1, v2, :cond_1b

    invoke-static {}, Lcom/mediatek/bluetooth/bip/BipService;->access$2608()I

    :goto_9
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v2}, Lcom/mediatek/bluetooth/bip/BipService;->access$1000(Lcom/mediatek/bluetooth/bip/BipService;)Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x1

    const/16 v4, 0x8

    invoke-static {v1, v2, v3, v4}, Lcom/mediatek/bluetooth/bip/BipService;->access$2300(Lcom/mediatek/bluetooth/bip/BipService;Landroid/content/Context;ZI)V

    const/16 v1, 0x8

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$702(I)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v2}, Lcom/mediatek/bluetooth/bip/BipService;->access$3500(Lcom/mediatek/bluetooth/bip/BipService;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/bluetooth/bip/BipService;->access$1500(Lcom/mediatek/bluetooth/bip/BipService;Landroid/os/PowerManager$WakeLock;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/bip/BipService$5;->this$0:Lcom/mediatek/bluetooth/bip/BipService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$500(Lcom/mediatek/bluetooth/bip/BipService;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x23

    const-wide/16 v3, 0x4e20

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    :cond_1b
    const v1, 0x2160ec1

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$2602(I)I

    goto :goto_9

    :pswitch_1c
    const-string v1, "BipService"

    const-string v2, "MSG_ON_BIPR_ALWAYS_ACCEPT"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    invoke-static {v1}, Lcom/mediatek/bluetooth/bip/BipService;->access$3202(Z)Z

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
    .end packed-switch
.end method
