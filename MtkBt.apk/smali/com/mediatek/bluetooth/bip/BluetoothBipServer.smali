.class public Lcom/mediatek/bluetooth/bip/BluetoothBipServer;
.super Ljava/lang/Object;
.source "BluetoothBipServer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/bluetooth/bip/BluetoothBipServer$BipListener;
    }
.end annotation


# static fields
.field private static final BIP_INITIATOR_CANCEL_FAIL:I = 0x14

.field private static final BIP_INITIATOR_CANCEL_SUCCESS:I = 0x13

.field private static final BIP_INITIATOR_CONNECT_FAIL:I = 0x6

.field private static final BIP_INITIATOR_CONNECT_SUCCESS:I = 0x5

.field private static final BIP_INITIATOR_DISABLE_FAIL:I = 0x3

.field private static final BIP_INITIATOR_DISABLE_SUCCESS:I = 0x2

.field private static final BIP_INITIATOR_DISCONNECT_FAIL:I = 0x12

.field private static final BIP_INITIATOR_DISCONNECT_SUCCESS:I = 0x11

.field private static final BIP_INITIATOR_ENABLE_FAIL:I = 0x1

.field private static final BIP_INITIATOR_ENABLE_SUCCESS:I = 0x0

.field private static final BIP_INITIATOR_GET_CAPABILITY_FAIL:I = 0x8

.field private static final BIP_INITIATOR_GET_CAPABILITY_SUCCESS:I = 0x7

.field private static final BIP_INITIATOR_IMAGE_PUSH_FAIL:I = 0xc

.field private static final BIP_INITIATOR_IMAGE_PUSH_START:I = 0x9

.field private static final BIP_INITIATOR_IMAGE_PUSH_SUCCESS:I = 0xb

.field private static final BIP_INITIATOR_OBEX_AUTHREQ:I = 0x4

.field private static final BIP_INITIATOR_PROGRESS:I = 0xa

.field private static final BIP_INITIATOR_THUMBNAIL_PUSH_FAIL:I = 0x10

.field private static final BIP_INITIATOR_THUMBNAIL_PUSH_START:I = 0xe

.field private static final BIP_INITIATOR_THUMBNAIL_PUSH_SUCCESS:I = 0xf

.field private static final BIP_INITIATOR_THUMBNAIL_REQ:I = 0xd

.field private static final BIP_RESPONDER_ACCESS_REQ:I = 0x1d

.field private static final BIP_RESPONDER_AUTH_REQ:I = 0x19

.field private static final BIP_RESPONDER_CAPABILITY_RES_SUCCESS:I = 0x1f

.field private static final BIP_RESPONDER_CONNECT_FAIL:I = 0x1c

.field private static final BIP_RESPONDER_CONNECT_SUCCESS:I = 0x1b

.field private static final BIP_RESPONDER_DISABLE_FAIL:I = 0x18

.field private static final BIP_RESPONDER_DISABLE_SUCCESS:I = 0x17

.field private static final BIP_RESPONDER_DISCONNECT_FAIL:I = 0x28

.field private static final BIP_RESPONDER_DISCONNECT_SUCCESS:I = 0x27

.field private static final BIP_RESPONDER_ENABLE_FAIL:I = 0x16

.field private static final BIP_RESPONDER_ENABLE_SUCCESS:I = 0x15

.field private static final BIP_RESPONDER_GET_CAPABILITY_REQ:I = 0x1e

.field private static final BIP_RESPONDER_IMAGE_RECEIVE_FAIL:I = 0x23

.field private static final BIP_RESPONDER_IMAGE_RECEIVE_START:I = 0x20

.field private static final BIP_RESPONDER_IMAGE_RECEIVE_SUCCESS:I = 0x22

.field private static final BIP_RESPONDER_OBEX_AUTHREQ:I = 0x1a

.field private static final BIP_RESPONDER_PROGRESS:I = 0x21

.field private static final BIP_RESPONDER_THUMBNAIL_RECEIVE_FAIL:I = 0x26

.field private static final BIP_RESPONDER_THUMBNAIL_RECEIVE_START:I = 0x24

.field private static final BIP_RESPONDER_THUMBNAIL_RECEIVE_SUCCESS:I = 0x25

.field private static final TAG:Ljava/lang/String; = "BluetoothBipServer"

.field private static mCurReceiveLength:I

.field private static mCurSentLength:I

.field private static mPreReceiveLength:I

.field private static mPreSentLength:I

.field private static mRObjLength:I

.field private static mSObjLength:I


# instance fields
.field private mCallback:Landroid/os/Handler;

.field private mListener:Lcom/mediatek/bluetooth/bip/BluetoothBipServer$BipListener;

.field private mNativeData:I

.field private mPath:Ljava/lang/String;

.field private mReceivedFileName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mPreReceiveLength:I

    sput v0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCurReceiveLength:I

    sput v0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mRObjLength:I

    sput v0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mPreSentLength:I

    sput v0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCurSentLength:I

    sput v0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mSObjLength:I

    const-string v0, "extbip_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->classInitNative()V

    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;)V
    .locals 2
    .param p1    # Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mNativeData:I

    iput-object v1, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mPath:Ljava/lang/String;

    iput-object v1, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mReceivedFileName:Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/bluetooth/bip/BluetoothBipServer;)Z
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/bip/BluetoothBipServer;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->startListenNative()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/mediatek/bluetooth/bip/BluetoothBipServer;)Z
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/bip/BluetoothBipServer;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->stopListenNative()Z

    move-result v0

    return v0
.end method

.method private native bipAuthRspNative(Lcom/mediatek/bluetooth/bip/AuthInfo;Z)Z
.end method

.method private native bipiDisableNative()Z
.end method

.method private native bipiDisconnectNative(Ljava/lang/String;)Z
.end method

.method private native bipiEnableNative()Z
.end method

.method private native bipiGetCapabilityReqNative(Ljava/lang/String;)Z
.end method

.method private native bipiPushImageNative(Ljava/lang/String;Lcom/mediatek/bluetooth/bip/BipImage;)Z
.end method

.method private native bipiPushThumbnailNative(Ljava/lang/String;Lcom/mediatek/bluetooth/bip/BipImage;)Z
.end method

.method private native biprAccessRspNative(IILjava/lang/String;)Z
.end method

.method private native biprAuthorizeRspNative(I)Z
.end method

.method private native biprDisableNative()Z
.end method

.method private native biprDisconnectNative()Z
.end method

.method private native biprEnableNative(Ljava/lang/String;)Z
.end method

.method private native biprGetCapabilityRspNative(ILcom/mediatek/bluetooth/bip/Capability;)Z
.end method

.method private native biprObjRename(Ljava/lang/String;)V
.end method

.method private static native classInitNative()V
.end method

.method private native cleanupDataNative()V
.end method

.method private native disableServiceNative()V
.end method

.method private native enableServiceNative()Z
.end method

.method private native initializeDataNative()V
.end method

.method private onCallback(III[Ljava/lang/String;)V
    .locals 10
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # [Ljava/lang/String;

    packed-switch p1, :pswitch_data_0

    const-string v5, "BluetoothBipServer"

    const-string v6, "UN-KNOWN EVENT"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const-string v5, "BluetoothBipServer"

    const-string v6, "BIP_INITIATOR_ENABLE_SUCCESS"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v1, v7}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    :pswitch_1
    const-string v5, "BluetoothBipServer"

    const-string v6, "BIP_INITIATOR_ENABLE_FAIL"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, -0x1

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v1, v7}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    :pswitch_2
    const-string v5, "BluetoothBipServer"

    const-string v6, "BIP_INITIATOR_DISABLE_SUCCESS"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    const/4 v6, 0x2

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v1, v7}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    :pswitch_3
    const-string v5, "BluetoothBipServer"

    const-string v6, "BIP_INITIATOR_DISABLE_FAIL"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, -0x1

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    const/4 v6, 0x2

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v1, v7}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    :pswitch_4
    const-string v5, "BluetoothBipServer"

    const-string v6, "BIP_INITIATOR_OBEX_AUTHREQ"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v5, 0x0

    aget-object v5, p4, v5

    const-string v6, "1"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v5, "BluetoothBipServer"

    const-string v6, "UserID required"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    const/4 v6, 0x3

    const/4 v7, 0x0

    const/4 v8, 0x0

    aget-object v8, p4, v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v5, v6, v7, v8}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    :pswitch_5
    const-string v5, "BluetoothBipServer"

    const-string v6, "BIP_INITIATOR_CONNECT_SUCCESS"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    const/4 v6, 0x4

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v1, v7}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_6
    const-string v5, "BluetoothBipServer"

    const-string v6, "BIP_INITIATOR_CONNECT_FAIL"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, -0x1

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    const/4 v6, 0x4

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v1, v7}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_7
    const-string v5, "BluetoothBipServer"

    const-string v6, "BIP_INITIATOR_GET_CAPABILITY_SUCCESS"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/mediatek/bluetooth/bip/Capability;

    const/16 v5, 0x8

    aget-object v5, p4, v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-direct {v0, v5}, Lcom/mediatek/bluetooth/bip/Capability;-><init>(I)V

    iget-object v5, v0, Lcom/mediatek/bluetooth/bip/Capability;->PreferFormat:Lcom/mediatek/bluetooth/bip/ImageDescriptor;

    const/4 v6, 0x0

    aget-object v6, p4, v6

    iput-object v6, v5, Lcom/mediatek/bluetooth/bip/ImageDescriptor;->Version:Ljava/lang/String;

    iget-object v5, v0, Lcom/mediatek/bluetooth/bip/Capability;->PreferFormat:Lcom/mediatek/bluetooth/bip/ImageDescriptor;

    const/4 v6, 0x1

    aget-object v6, p4, v6

    iput-object v6, v5, Lcom/mediatek/bluetooth/bip/ImageDescriptor;->Encoding:Ljava/lang/String;

    iget-object v5, v0, Lcom/mediatek/bluetooth/bip/Capability;->PreferFormat:Lcom/mediatek/bluetooth/bip/ImageDescriptor;

    const/4 v6, 0x2

    aget-object v6, p4, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    iput v6, v5, Lcom/mediatek/bluetooth/bip/ImageDescriptor;->Width:I

    iget-object v5, v0, Lcom/mediatek/bluetooth/bip/Capability;->PreferFormat:Lcom/mediatek/bluetooth/bip/ImageDescriptor;

    const/4 v6, 0x3

    aget-object v6, p4, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    iput v6, v5, Lcom/mediatek/bluetooth/bip/ImageDescriptor;->Height:I

    iget-object v5, v0, Lcom/mediatek/bluetooth/bip/Capability;->PreferFormat:Lcom/mediatek/bluetooth/bip/ImageDescriptor;

    const/4 v6, 0x4

    aget-object v6, p4, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    iput v6, v5, Lcom/mediatek/bluetooth/bip/ImageDescriptor;->Width2:I

    iget-object v5, v0, Lcom/mediatek/bluetooth/bip/Capability;->PreferFormat:Lcom/mediatek/bluetooth/bip/ImageDescriptor;

    const/4 v6, 0x5

    aget-object v6, p4, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    iput v6, v5, Lcom/mediatek/bluetooth/bip/ImageDescriptor;->Height2:I

    iget-object v5, v0, Lcom/mediatek/bluetooth/bip/Capability;->PreferFormat:Lcom/mediatek/bluetooth/bip/ImageDescriptor;

    const/4 v6, 0x6

    aget-object v6, p4, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    iput v6, v5, Lcom/mediatek/bluetooth/bip/ImageDescriptor;->Size:I

    iget-object v5, v0, Lcom/mediatek/bluetooth/bip/Capability;->PreferFormat:Lcom/mediatek/bluetooth/bip/ImageDescriptor;

    const/4 v6, 0x7

    aget-object v6, p4, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    iput v6, v5, Lcom/mediatek/bluetooth/bip/ImageDescriptor;->Transform:I

    const/16 v5, 0x8

    aget-object v5, p4, v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    iput v5, v0, Lcom/mediatek/bluetooth/bip/Capability;->NumImageFormats:I

    const/4 v3, 0x0

    :goto_1
    iget v5, v0, Lcom/mediatek/bluetooth/bip/Capability;->NumImageFormats:I

    if-ge v3, v5, :cond_2

    iget-object v5, v0, Lcom/mediatek/bluetooth/bip/Capability;->ImageFormats:[Lcom/mediatek/bluetooth/bip/ImageFormat;

    aget-object v5, v5, v3

    mul-int/lit8 v6, v3, 0x6

    add-int/lit8 v6, v6, 0x9

    aget-object v6, p4, v6

    iput-object v6, v5, Lcom/mediatek/bluetooth/bip/ImageFormat;->Encoding:Ljava/lang/String;

    iget-object v5, v0, Lcom/mediatek/bluetooth/bip/Capability;->ImageFormats:[Lcom/mediatek/bluetooth/bip/ImageFormat;

    aget-object v5, v5, v3

    mul-int/lit8 v6, v3, 0x6

    add-int/lit8 v6, v6, 0x9

    add-int/lit8 v6, v6, 0x1

    aget-object v6, p4, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    iput v6, v5, Lcom/mediatek/bluetooth/bip/ImageFormat;->Width:I

    iget-object v5, v0, Lcom/mediatek/bluetooth/bip/Capability;->ImageFormats:[Lcom/mediatek/bluetooth/bip/ImageFormat;

    aget-object v5, v5, v3

    mul-int/lit8 v6, v3, 0x6

    add-int/lit8 v6, v6, 0x9

    add-int/lit8 v6, v6, 0x2

    aget-object v6, p4, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    iput v6, v5, Lcom/mediatek/bluetooth/bip/ImageFormat;->Height:I

    iget-object v5, v0, Lcom/mediatek/bluetooth/bip/Capability;->ImageFormats:[Lcom/mediatek/bluetooth/bip/ImageFormat;

    aget-object v5, v5, v3

    mul-int/lit8 v6, v3, 0x6

    add-int/lit8 v6, v6, 0x9

    add-int/lit8 v6, v6, 0x3

    aget-object v6, p4, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    iput v6, v5, Lcom/mediatek/bluetooth/bip/ImageFormat;->Width2:I

    iget-object v5, v0, Lcom/mediatek/bluetooth/bip/Capability;->ImageFormats:[Lcom/mediatek/bluetooth/bip/ImageFormat;

    aget-object v5, v5, v3

    mul-int/lit8 v6, v3, 0x6

    add-int/lit8 v6, v6, 0x9

    add-int/lit8 v6, v6, 0x4

    aget-object v6, p4, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    iput v6, v5, Lcom/mediatek/bluetooth/bip/ImageFormat;->Height2:I

    iget-object v5, v0, Lcom/mediatek/bluetooth/bip/Capability;->ImageFormats:[Lcom/mediatek/bluetooth/bip/ImageFormat;

    aget-object v5, v5, v3

    mul-int/lit8 v6, v3, 0x6

    add-int/lit8 v6, v6, 0x9

    add-int/lit8 v6, v6, 0x5

    aget-object v6, p4, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    iput v6, v5, Lcom/mediatek/bluetooth/bip/ImageFormat;->Size:I

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    const/4 v6, 0x5

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v1, v7, v0}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_8
    const-string v5, "BluetoothBipServer"

    const-string v6, "BIP_INITIATOR_GET_CAPABILITY_FAIL"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, -0x1

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    const/4 v6, 0x5

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v1, v7}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_9
    const-string v5, "BluetoothBipServer"

    const-string v6, "BIP_INITIATOR_IMAGE_PUSH_START"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    const/4 v6, 0x6

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v7, v8}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_a
    const-string v5, "BluetoothBipServer"

    const-string v6, "BIP_INITIATOR_PROGRESS"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "BluetoothBipServer"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Sending Length: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x0

    aget-object v7, p4, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v5, 0x0

    aget-object v5, p4, v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    sput v5, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCurSentLength:I

    sget v5, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mSObjLength:I

    sget v6, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCurSentLength:I

    sub-int/2addr v5, v6

    sget v6, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mSObjLength:I

    div-int/lit8 v6, v6, 0x64

    if-gt v5, v6, :cond_3

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    const/4 v6, 0x7

    const/4 v7, 0x0

    sget v8, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCurSentLength:I

    invoke-virtual {v5, v6, v7, v8}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :cond_3
    sget v5, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCurSentLength:I

    sget v6, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mPreSentLength:I

    sub-int/2addr v5, v6

    sget v6, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mSObjLength:I

    div-int/lit8 v6, v6, 0x28

    if-le v5, v6, :cond_0

    sget v5, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCurSentLength:I

    sput v5, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mPreSentLength:I

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    const/4 v6, 0x7

    const/4 v7, 0x0

    sget v8, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCurSentLength:I

    invoke-virtual {v5, v6, v7, v8}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_b
    const-string v5, "BluetoothBipServer"

    const-string v6, "BIP_INITIATOR_IMAGE_PUSH_SUCCESS"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "BluetoothBipServer"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Image Handle: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x0

    aget-object v7, p4, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    const/4 v2, -0x1

    const/4 v5, 0x0

    aget-object v5, p4, v5

    const-string v6, "[0-9]"

    invoke-virtual {v5, v6}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    const/4 v5, 0x0

    aget-object v5, p4, v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    :cond_4
    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    const/16 v6, 0x8

    invoke-virtual {v5, v6, v1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_c
    const-string v5, "BluetoothBipServer"

    const-string v6, "BIP_INITIATOR_IMAGE_PUSH_FAIL"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, -0x1

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    const/16 v6, 0x8

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v1, v7}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_d
    const-string v5, "BluetoothBipServer"

    const-string v6, "BIP_INITIATOR_THUMBNAIL_REQ"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "BluetoothBipServer"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Image Handle: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x0

    aget-object v7, p4, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    const/16 v6, 0x9

    const/4 v7, 0x0

    const/4 v8, 0x0

    aget-object v8, p4, v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v5, v6, v7, v8}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_e
    const-string v5, "BluetoothBipServer"

    const-string v6, "BIP_INITIATOR_THUMBNAIL_PUSH_START"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    const/16 v6, 0xa

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v7, v8}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_f
    const-string v5, "BluetoothBipServer"

    const-string v6, "BIP_INITIATOR_THUMBNAIL_PUSH_SUCCESS"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    const/16 v6, 0xb

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v1, v7}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_10
    const-string v5, "BluetoothBipServer"

    const-string v6, "BIP_INITIATOR_THUMBNAIL_PUSH_FAIL"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, -0x1

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    const/16 v6, 0xb

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v1, v7}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_11
    const-string v5, "BluetoothBipServer"

    const-string v6, "BIP_INITIATOR_DISCONNECT_SUCCESS"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    const/16 v6, 0xc

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v1, v7}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_12
    const-string v5, "BluetoothBipServer"

    const-string v6, "BIP_INITIATOR_DISCONNECT_FAIL"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, -0x1

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    const/16 v6, 0xc

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v1, v7}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_13
    const-string v5, "BluetoothBipServer"

    const-string v6, "BIP_INITIATOR_CANCEL_SUCCESS"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    const/16 v6, 0xd

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v1, v7}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_14
    const-string v5, "BluetoothBipServer"

    const-string v6, "BIP_INITIATOR_CANCEL_FAIL"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, -0x1

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    const/16 v6, 0xd

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v1, v7}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_15
    const-string v5, "BluetoothBipServer"

    const-string v6, "BIP_RESPONDER_ENABLE_SUCCESS"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    const/16 v6, 0x15

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v1, v7}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_16
    const-string v5, "BluetoothBipServer"

    const-string v6, "BIP_RESPONDER_ENABLE_FAIL"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, -0x1

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    const/16 v6, 0x15

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v1, v7}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_17
    const-string v5, "BluetoothBipServer"

    const-string v6, "BIP_RESPONDER_DISABLE_SUCCESS"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    const/16 v6, 0x16

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v1, v7}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_18
    const-string v5, "BluetoothBipServer"

    const-string v6, "BIP_RESPONDER_DISABLE_FAIL"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, -0x1

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    const/16 v6, 0x16

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v1, v7}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_19
    const-string v5, "BluetoothBipServer"

    const-string v6, "BIP_RESPONDER_AUTH_REQ"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "BluetoothBipServer"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "RemoteDevName: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x0

    aget-object v7, p4, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "BluetoothBipServer"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "RemoteBtAddr: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x1

    aget-object v7, p4, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    const/16 v6, 0x17

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v7, v8, p4}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_1a
    const-string v5, "BluetoothBipServer"

    const-string v6, "BIP_RESPONDER_OBEX_AUTHREQ"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v5, 0x0

    aget-object v5, p4, v5

    const-string v6, "1"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    const-string v5, "BluetoothBipServer"

    const-string v6, "UserID required"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    const/16 v6, 0x18

    const/4 v7, 0x0

    const/4 v8, 0x0

    aget-object v8, p4, v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v5, v6, v7, v8}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_1b
    const-string v5, "BluetoothBipServer"

    const-string v6, "BIP_RESPONDER_CONNECT_SUCCESS"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    const/16 v6, 0x19

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v1, v7}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_1c
    const-string v5, "BluetoothBipServer"

    const-string v6, "BIP_RESPONDER_CONNECT_FAIL"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, -0x1

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    const/16 v6, 0x19

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v1, v7}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_1d
    const-string v5, "BluetoothBipServer"

    const-string v6, "BIP_RESPONDER_GET_CAPABILITY_REQ"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    const/16 v6, 0x1a

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v7, v8}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_1e
    const-string v5, "BluetoothBipServer"

    const-string v6, "BIP_RESPONDER_CAPABILITY_RES_SUCCESS"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    const/16 v6, 0x1b

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v1, v7}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_1f
    const/4 v5, 0x0

    aget-object v5, p4, v5

    iput-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mReceivedFileName:Ljava/lang/String;

    const-string v5, "BluetoothBipServer"

    const-string v6, "BIP_RESPONDER_ACCESS_REQ"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "BluetoothBipServer"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "fileName: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x0

    aget-object v7, p4, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "BluetoothBipServer"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "fileSize: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x1

    aget-object v7, p4, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v5, 0x0

    sput v5, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mPreReceiveLength:I

    const/4 v5, 0x0

    sput v5, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCurReceiveLength:I

    const/4 v5, 0x1

    aget-object v5, p4, v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    sput v5, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mRObjLength:I

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    const/16 v6, 0x1c

    const/4 v7, 0x0

    sget v8, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mRObjLength:I

    const/4 v9, 0x0

    aget-object v9, p4, v9

    invoke-virtual {v5, v6, v7, v8, v9}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_20
    const-string v5, "BluetoothBipServer"

    const-string v6, "BIP_RESPONDER_IMAGE_RECEIVE_START"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    const/16 v6, 0x1d

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v7, v8}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_21
    const-string v5, "BluetoothBipServer"

    const-string v6, "BIP_RESPONDER_PROGRESS"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "BluetoothBipServer"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Object Length: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x1

    aget-object v7, p4, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v5, 0x1

    aget-object v5, p4, v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    sget v5, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mRObjLength:I

    if-nez v5, :cond_6

    if-eqz v4, :cond_6

    sput v4, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mRObjLength:I

    :cond_6
    const-string v5, "BluetoothBipServer"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Receiving Length: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x0

    aget-object v7, p4, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v5, 0x0

    aget-object v5, p4, v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    sput v5, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCurReceiveLength:I

    sget v5, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mRObjLength:I

    sget v6, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCurReceiveLength:I

    sub-int/2addr v5, v6

    sget v6, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mRObjLength:I

    div-int/lit8 v6, v6, 0x64

    if-gt v5, v6, :cond_7

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    const/16 v6, 0x1e

    sget v7, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mRObjLength:I

    sget v8, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCurReceiveLength:I

    invoke-virtual {v5, v6, v7, v8}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :cond_7
    sget v5, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCurReceiveLength:I

    sget v6, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mPreReceiveLength:I

    sub-int/2addr v5, v6

    sget v6, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mRObjLength:I

    div-int/lit8 v6, v6, 0x28

    if-le v5, v6, :cond_0

    sget v5, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCurReceiveLength:I

    sput v5, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mPreReceiveLength:I

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    const/16 v6, 0x1e

    sget v7, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mRObjLength:I

    sget v8, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCurReceiveLength:I

    invoke-virtual {v5, v6, v7, v8}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_22
    const-string v5, "BluetoothBipServer"

    const-string v6, "BIP_RESPONDER_IMAGE_RECEIVE_SUCCESS"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    const/16 v6, 0x1f

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v1, v7}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_23
    const-string v5, "BluetoothBipServer"

    const-string v6, "BIP_RESPONDER_IMAGE_RECEIVE_FAIL"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, -0x1

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    const/16 v6, 0x1f

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v1, v7}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_24
    const-string v5, "BluetoothBipServer"

    const-string v6, "BIP_RESPONDER_THUMBNAIL_RECEIVE_START"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    const/16 v6, 0x20

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v7, v8}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_25
    const-string v5, "BluetoothBipServer"

    const-string v6, "BIP_RESPONDER_THUMBNAIL_RECEIVE_SUCCESS"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    const/16 v6, 0x21

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v1, v7}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_26
    const-string v5, "BluetoothBipServer"

    const-string v6, "BIP_RESPONDER_THUMBNAIL_RECEIVE_FAIL"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, -0x1

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    const/16 v6, 0x21

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v1, v7}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_27
    const-string v5, "BluetoothBipServer"

    const-string v6, "BIP_RESPONDER_DISCONNECT_SUCCESS"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    const/16 v6, 0x22

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v1, v7}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_28
    const-string v5, "BluetoothBipServer"

    const-string v6, "BIP_RESPONDER_DISCONNECT_FAIL"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, -0x1

    iget-object v5, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCallback:Landroid/os/Handler;

    const/16 v6, 0x22

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v1, v7}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1f
        :pswitch_1d
        :pswitch_1e
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
    .end packed-switch
.end method

.method private native startListenNative()Z
.end method

.method private native stopListenNative()Z
.end method


# virtual methods
.method public bipAuthRsp(Lcom/mediatek/bluetooth/bip/AuthInfo;Z)Z
    .locals 2
    .param p1    # Lcom/mediatek/bluetooth/bip/AuthInfo;
    .param p2    # Z

    const-string v0, "BluetoothBipServer"

    const-string v1, "+bipAuthRspNative"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1, p2}, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->bipAuthRspNative(Lcom/mediatek/bluetooth/bip/AuthInfo;Z)Z

    const-string v0, "BluetoothBipServer"

    const-string v1, "-bipAuthRspNative"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    return v0
.end method

.method public bipiDisable()Z
    .locals 2

    const-string v0, "BluetoothBipServer"

    const-string v1, "+bipiDisable"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->bipiDisableNative()Z

    const-string v0, "BluetoothBipServer"

    const-string v1, "-bipiDisable"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    return v0
.end method

.method public bipiDisconnect(Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v0, "BluetoothBipServer"

    const-string v1, "+bipiDisconnect"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_0

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->bipiDisconnectNative(Ljava/lang/String;)Z

    :goto_0
    const-string v0, "BluetoothBipServer"

    const-string v1, "-bipiDisconnect"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    return v0

    :cond_0
    const-string v0, "BluetoothBipServer"

    const-string v1, "-Empty btaddr"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public bipiEnable()Z
    .locals 2

    const-string v0, "BluetoothBipServer"

    const-string v1, "+bipiEnable"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->bipiEnableNative()Z

    const-string v0, "BluetoothBipServer"

    const-string v1, "-bipiEnable"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    return v0
.end method

.method public bipiGetCapabilityReq(Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v0, "BluetoothBipServer"

    const-string v1, "+bipiGetCapabilityReq"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->bipiGetCapabilityReqNative(Ljava/lang/String;)Z

    const-string v0, "BluetoothBipServer"

    const-string v1, "-bipiGetCapabilityReq"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    return v0
.end method

.method public bipiPushImage(Ljava/lang/String;Lcom/mediatek/bluetooth/bip/BipImage;)Z
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/mediatek/bluetooth/bip/BipImage;

    const/4 v2, 0x0

    const-string v0, "BluetoothBipServer"

    const-string v1, "+bipiPushImage"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p2, Lcom/mediatek/bluetooth/bip/BipImage;->ObjectSize:I

    sput v0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mSObjLength:I

    sput v2, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mPreSentLength:I

    sput v2, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mCurSentLength:I

    invoke-direct {p0, p1, p2}, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->bipiPushImageNative(Ljava/lang/String;Lcom/mediatek/bluetooth/bip/BipImage;)Z

    const-string v0, "BluetoothBipServer"

    const-string v1, "-bipiPushImage"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    return v0
.end method

.method public bipiPushThumbnail(Ljava/lang/String;Lcom/mediatek/bluetooth/bip/BipImage;)Z
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/mediatek/bluetooth/bip/BipImage;

    const-string v0, "BluetoothBipServer"

    const-string v1, "+bipiPushThumbnail"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1, p2}, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->bipiPushThumbnailNative(Ljava/lang/String;Lcom/mediatek/bluetooth/bip/BipImage;)Z

    const-string v0, "BluetoothBipServer"

    const-string v1, "-bipiPushThumbnail"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    return v0
.end method

.method public biprAccessRsp(IILjava/lang/String;)Z
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # Ljava/lang/String;

    const-string v2, "BluetoothBipServer"

    const-string v3, "+biprAccessRsp"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mPath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mReceivedFileName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "BluetoothBipServer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "tempPath"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v1}, Lcom/mediatek/bluetooth/util/SystemUtils;->createNewFileForSaving(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mReceivedFileName:Ljava/lang/String;

    iget-object v2, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mReceivedFileName:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->biprObjRename(Ljava/lang/String;)V

    :cond_0
    const-string v2, "BluetoothBipServer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mReceivedFileName:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mReceivedFileName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1, p2, p3}, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->biprAccessRspNative(IILjava/lang/String;)Z

    const-string v2, "BluetoothBipServer"

    const-string v3, "-biprAccessRsp"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x1

    return v2
.end method

.method public biprAuthorizeRsp(I)Z
    .locals 2
    .param p1    # I

    const-string v0, "BluetoothBipServer"

    const-string v1, "+biprAuthorizeRsp"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->biprAuthorizeRspNative(I)Z

    const-string v0, "BluetoothBipServer"

    const-string v1, "-biprAuthorizeRsp"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    return v0
.end method

.method public biprDisable()Z
    .locals 2

    const-string v0, "BluetoothBipServer"

    const-string v1, "+biprDisable"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->biprDisableNative()Z

    const-string v0, "BluetoothBipServer"

    const-string v1, "-biprDisable"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    return v0
.end method

.method public biprDisconnect()Z
    .locals 2

    const-string v0, "BluetoothBipServer"

    const-string v1, "+biprDisconnect"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->biprDisconnectNative()Z

    const-string v0, "BluetoothBipServer"

    const-string v1, "-biprDisconnect"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    return v0
.end method

.method public biprEnable(Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v0, "BluetoothBipServer"

    const-string v1, "+biprEnable"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->biprEnableNative(Ljava/lang/String;)Z

    const-string v0, "BluetoothBipServer"

    const-string v1, "-biprEnable"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    return v0
.end method

.method public biprGetCapabilityRsp(ILcom/mediatek/bluetooth/bip/Capability;)Z
    .locals 2
    .param p1    # I
    .param p2    # Lcom/mediatek/bluetooth/bip/Capability;

    const-string v0, "BluetoothBipServer"

    const-string v1, "+biprGetCapabilityRsp"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1, p2}, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->biprGetCapabilityRspNative(ILcom/mediatek/bluetooth/bip/Capability;)Z

    const-string v0, "BluetoothBipServer"

    const-string v1, "-biprGetCapabilityRsp"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    return v0
.end method

.method public disable()V
    .locals 3

    const-string v1, "BluetoothBipServer"

    const-string v2, "+disable"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v1, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mNativeData:I

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mListener:Lcom/mediatek/bluetooth/bip/BluetoothBipServer$BipListener;

    invoke-virtual {v1}, Lcom/mediatek/bluetooth/bip/BluetoothBipServer$BipListener;->shutdown()V

    iget-object v1, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mListener:Lcom/mediatek/bluetooth/bip/BluetoothBipServer$BipListener;

    invoke-virtual {v1}, Ljava/lang/Thread;->join()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mListener:Lcom/mediatek/bluetooth/bip/BluetoothBipServer$BipListener;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-direct {p0}, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->disableServiceNative()V

    invoke-direct {p0}, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->cleanupDataNative()V

    :goto_1
    const-string v1, "BluetoothBipServer"

    const-string v2, "-disable"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :catch_0
    move-exception v0

    const-string v1, "BluetoothBipServer"

    const-string v2, "BipServer mListener close error"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    const-string v1, "BluetoothBipServer"

    const-string v2, "mNativeData has been cleaned"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public enable()Z
    .locals 2

    const-string v0, "BluetoothBipServer"

    const-string v1, "+enable"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mNativeData:I

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->initializeDataNative()V

    invoke-direct {p0}, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->enableServiceNative()Z

    new-instance v0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer$BipListener;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/bip/BluetoothBipServer$BipListener;-><init>(Lcom/mediatek/bluetooth/bip/BluetoothBipServer;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mListener:Lcom/mediatek/bluetooth/bip/BluetoothBipServer$BipListener;

    iget-object v0, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mListener:Lcom/mediatek/bluetooth/bip/BluetoothBipServer$BipListener;

    invoke-virtual {v0}, Lcom/mediatek/bluetooth/bip/BluetoothBipServer$BipListener;->startup()V

    :goto_0
    const-string v0, "BluetoothBipServer"

    const-string v1, "-enable"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    return v0

    :cond_0
    const-string v0, "BluetoothBipServer"

    const-string v1, "mNativeData has been initialized"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setRecvPath(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/bluetooth/bip/BluetoothBipServer;->mPath:Ljava/lang/String;

    return-void
.end method
