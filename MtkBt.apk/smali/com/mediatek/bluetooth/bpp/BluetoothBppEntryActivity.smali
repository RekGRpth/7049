.class public Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;
.super Landroid/app/Activity;
.source "BluetoothBppEntryActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity$FileOperThread;
    }
.end annotation


# static fields
.field private static final ACTION_PRINT:Ljava/lang/String; = "mediatek.intent.action.PRINT"

.field private static final BLUETOOTH_DEVICE_REQUEST:I = 0x1

.field private static final FILTER_TYPE_PRINTER:I = 0x3

.field private static final TAG:Ljava/lang/String; = "BluetoothBppEntryActivity"

.field private static mAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private static mContext:Landroid/content/Context;

.field private static mEntryError:[Ljava/lang/String;

.field private static mFilePath:Ljava/lang/String;

.field private static mFileSize:Ljava/lang/String;

.field private static mFileUri:Landroid/net/Uri;

.field private static mMimeType:Ljava/lang/String;

.field private static mReentry:Z

.field private static result:I


# instance fields
.field private mBack:Z

.field private mFileOperThread:Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity$FileOperThread;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mReentry:Z

    const/4 v0, -0x1

    sput v0, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->result:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mBack:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mFileOperThread:Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity$FileOperThread;

    return-void
.end method

.method static synthetic access$002(I)I
    .locals 0
    .param p0    # I

    sput p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->result:I

    return p0
.end method

.method static synthetic access$100(Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;)I
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->getFileInfo()I

    move-result v0

    return v0
.end method

.method private getFileInfo()I
    .locals 27

    sget-object v2, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mMimeType:Ljava/lang/String;

    const-string v3, "text/x-vcard"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "BluetoothBppEntryActivity"

    const-string v3, "v-card"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    sget-object v2, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mFileUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v18

    if-nez v18, :cond_0

    const-string v2, "BluetoothBppEntryActivity"

    const-string v3, "input stream is null"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const-string v17, "/data/@btmtk/profile/"

    const-string v16, "Contact.vcf"

    new-instance v10, Ljava/io/File;

    move-object/from16 v0, v17

    invoke-direct {v10, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v10}, Ljava/io/File;->mkdirs()Z

    :cond_1
    new-instance v11, Ljava/io/File;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-direct {v11, v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v11}, Ljava/io/File;->delete()Z

    :cond_2
    invoke-virtual {v11}, Ljava/io/File;->createNewFile()Z

    new-instance v20, Ljava/io/FileOutputStream;

    move-object/from16 v0, v20

    invoke-direct {v0, v11}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    const/16 v26, 0x0

    const/16 v2, 0x400

    new-array v8, v2, [B

    :goto_1
    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Ljava/io/InputStream;->read([B)I

    move-result v23

    const/4 v2, -0x1

    move/from16 v0, v23

    if-eq v0, v2, :cond_3

    const/4 v2, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v23

    invoke-virtual {v0, v8, v2, v1}, Ljava/io/FileOutputStream;->write([BII)V

    add-int v26, v26, v23

    goto :goto_1

    :cond_3
    invoke-virtual/range {v18 .. v18}, Ljava/io/InputStream;->close()V

    invoke-virtual/range {v20 .. v20}, Ljava/io/FileOutputStream;->close()V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mFilePath:Ljava/lang/String;

    const-string v2, "text/x-vcard:3.0"

    sput-object v2, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mMimeType:Ljava/lang/String;

    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mFileSize:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "chmod 604 "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mFilePath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v2, "BluetoothBppEntryActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cmd="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v9}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v22

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "chmod 777 "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v2, "BluetoothBppEntryActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cmd="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, v24

    invoke-virtual {v0, v9}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :try_start_2
    const-string v2, "BluetoothBppEntryActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "File Path: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v5, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mFilePath:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "BluetoothBppEntryActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "File Size: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v5, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mFileSize:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "BluetoothBppEntryActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "File Mime: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v5, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mMimeType:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    goto/16 :goto_0

    :catch_0
    move-exception v13

    const-string v2, "BluetoothBppEntryActivity"

    const-string v3, "chmod fail!"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v13}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    const/4 v2, 0x1

    goto/16 :goto_0

    :catch_1
    move-exception v14

    const-string v2, "BluetoothBppEntryActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "create v-card file fail: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v14}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x1

    goto/16 :goto_0

    :cond_4
    const-string v2, "BluetoothBppEntryActivity"

    const-string v3, "image"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "content"

    sget-object v3, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mFileUri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    const-string v2, "media"

    sget-object v3, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mFileUri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x3

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_data"

    aput-object v3, v4, v2

    const/4 v2, 0x1

    const-string v3, "mime_type"

    aput-object v3, v4, v2

    const/4 v2, 0x2

    const-string v3, "_size"

    aput-object v3, v4, v2

    sget-object v3, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mFileUri:Landroid/net/Uri;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, Landroid/app/Activity;->managedQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    if-nez v12, :cond_5

    const-string v2, "BluetoothBppEntryActivity"

    const-string v3, "cursor is null"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x2

    goto/16 :goto_0

    :cond_5
    const-string v2, "_data"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v21

    const-string v2, "mime_type"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v19

    const-string v2, "_size"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v25

    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move/from16 v0, v21

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mFilePath:Ljava/lang/String;

    move/from16 v0, v19

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mMimeType:Ljava/lang/String;

    :cond_6
    :goto_2
    new-instance v15, Ljava/io/File;

    sget-object v2, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mFilePath:Ljava/lang/String;

    invoke-direct {v15, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mFileSize:Ljava/lang/String;

    const-string v2, "BluetoothBppEntryActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "File Path: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v5, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mFilePath:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "BluetoothBppEntryActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "File Size: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v5, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mFileSize:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "BluetoothBppEntryActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "File Mime: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v5, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mMimeType:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v2, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mMimeType:Ljava/lang/String;

    const-string v3, "image/jpeg"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    sget-object v2, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mMimeType:Ljava/lang/String;

    const-string v3, "image/gif"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    sget-object v2, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mMimeType:Ljava/lang/String;

    const-string v3, "image/png"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    const/4 v2, 0x2

    goto/16 :goto_0

    :cond_7
    const-string v2, "file"

    sget-object v3, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mFileUri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    sget-object v2, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mFileUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mFilePath:Ljava/lang/String;

    sget-object v2, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mFilePath:Ljava/lang/String;

    sget-object v3, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mFilePath:Ljava/lang/String;

    const/16 v5, 0x2f

    const/4 v6, 0x2

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->indexOf(II)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mFilePath:Ljava/lang/String;

    goto/16 :goto_2

    :cond_8
    const-string v2, "BluetoothBppEntryActivity"

    const-string v3, "unhandled URI"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_9
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method private sendFileInfo()Z
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.mediatek.bluetooth.bppReceiver.action.PASS_OBJECT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.mediatek.bluetooth.bppmanager.extra.FILE_PATH"

    sget-object v2, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mFilePath:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.mediatek.bluetooth.bppmanager.extra.MIME_TYPE"

    sget-object v2, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mMimeType:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.mediatek.bluetooth.bppmanager.extra.FILE_SIZE"

    sget-object v2, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mFileSize:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    const/4 v1, 0x1

    return v1
.end method

.method private startDevicePicker()V
    .locals 3

    const-string v1, "BluetoothBppEntryActivity"

    const-string v2, "Start Device Picker!"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.bluetooth.devicepicker.action.LAUNCH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v1, 0x800000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, "android.bluetooth.devicepicker.extra.NEED_AUTH"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "android.bluetooth.devicepicker.extra.FILTER_TYPE"

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "android.bluetooth.devicepicker.extra.LAUNCH_PACKAGE"

    const-string v2, "com.mediatek.bluetooth"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.bluetooth.devicepicker.extra.DEVICE_PICKER_LAUNCH_CLASS"

    const-class v2, Lcom/mediatek/bluetooth/bpp/BluetoothBppReceiver;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    const/4 v1, 0x0

    sput-boolean v1, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mReentry:Z

    const/4 v1, -0x1

    if-ne v1, p2, :cond_1

    invoke-direct {p0}, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->sendFileInfo()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->startDevicePicker()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, ""

    if-eqz p3, :cond_2

    :cond_2
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f060022

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f060025

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity$2;

    invoke-direct {v2, p0}, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity$2;-><init>(Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f060026

    new-instance v3, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity$1;

    invoke-direct {v3, p0}, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity$1;-><init>(Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 2

    const-string v0, "BluetoothBppEntryActivity"

    const-string v1, "onBackPressed......"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mBack:Z

    if-nez v0, :cond_0

    const-string v0, "BluetoothBppEntryActivity"

    const-string v1, "mBack is false"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    :goto_0
    return-void

    :cond_0
    const-string v0, "BluetoothBppEntryActivity"

    const-string v1, "mBack is true"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;

    const/4 v8, 0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const-string v5, "BluetoothBppEntryActivity"

    const-string v6, "onCreate......"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v5, 0x0

    sput-boolean v5, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mReentry:Z

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    sput-object v5, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mContext:Landroid/content/Context;

    sput-object v4, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mMimeType:Ljava/lang/String;

    const-string v5, "android.intent.extra.STREAM"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Landroid/net/Uri;

    sput-object v5, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mFileUri:Landroid/net/Uri;

    sget-object v5, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f050005

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mEntryError:[Ljava/lang/String;

    sget-object v5, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mFileUri:Landroid/net/Uri;

    if-eqz v5, :cond_0

    if-eqz v4, :cond_0

    const-string v5, "BluetoothBppEntryActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Get ACTION_SEND intent: Uri = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mFileUri:Landroid/net/Uri;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "; mimetype = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v5, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity$FileOperThread;

    invoke-direct {v5, p0}, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity$FileOperThread;-><init>(Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;)V

    iput-object v5, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mFileOperThread:Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity$FileOperThread;

    iget-object v5, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mFileOperThread:Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity$FileOperThread;

    invoke-virtual {v5}, Ljava/lang/Thread;->start()V

    :try_start_0
    iget-object v5, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mFileOperThread:Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity$FileOperThread;

    invoke-virtual {v5}, Ljava/lang/Thread;->join()V

    const/4 v5, 0x0

    iput-object v5, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mFileOperThread:Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity$FileOperThread;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const-string v5, "BluetoothBppEntryActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "result = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget v7, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->result:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    sget v5, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->result:I

    if-eqz v5, :cond_0

    sget-object v5, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mEntryError:[Ljava/lang/String;

    sget v6, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->result:I

    aget-object v5, v5, v6

    invoke-static {p0, v5, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_1
    return-void

    :catch_0
    move-exception v1

    const-string v5, "BluetoothBppEntryActivity"

    const-string v6, "mFileOperThread close error."

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v5

    sput-object v5, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    sget-object v5, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-nez v5, :cond_1

    const-string v5, "BluetoothBppEntryActivity"

    const-string v6, "bluetooth service is not started! "

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_1

    :cond_1
    sget-object v5, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v5}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v5

    if-eqz v5, :cond_3

    const-string v5, "BluetoothBppEntryActivity"

    const-string v6, "bluetooth service is available"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->sendFileInfo()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-direct {p0}, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->startDevicePicker()V

    :cond_2
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_1

    :cond_3
    const-string v5, "BluetoothBppEntryActivity"

    const-string v6, "bluetooth service is not available! "

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "BluetoothBppEntryActivity"

    const-string v6, "turning on bluetooth......"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Landroid/content/Intent;

    const-string v5, "android.bluetooth.adapter.action.REQUEST_ENABLE"

    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2, v8}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 2

    const-string v0, "BluetoothBppEntryActivity"

    const-string v1, "onDestroy......"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onPause()V
    .locals 3

    const/4 v2, 0x1

    const-string v0, "BluetoothBppEntryActivity"

    const-string v1, "onPause......"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mBack:Z

    sput-boolean v2, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mReentry:Z

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 2

    const-string v0, "BluetoothBppEntryActivity"

    const-string v1, "onResume......"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    sget-boolean v0, Lcom/mediatek/bluetooth/bpp/BluetoothBppEntryActivity;->mReentry:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 2

    const-string v0, "BluetoothBppEntryActivity"

    const-string v1, "onStart......"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    return-void
.end method

.method public onStop()V
    .locals 2

    const-string v0, "BluetoothBppEntryActivity"

    const-string v1, "onStop......"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method
