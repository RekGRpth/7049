.class Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$2;
.super Landroid/content/BroadcastReceiver;
.source "BluetoothBppManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;


# direct methods
.method constructor <init>(Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$2;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v8, -0x1

    const-string v4, "BluetoothBppManager"

    const-string v5, "onReceive"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v1, 0x0

    const-string v4, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    new-instance v1, Ljava/io/File;

    invoke-static {}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$200()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/mediatek/bluetooth/util/SystemUtils;->getReceivedFilePath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$300()I

    move-result v4

    if-ne v4, v8, :cond_1

    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.mediatek.bluetooth.bppprinting.action.ATTR_UPDATE"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, " com.mediatek.bluetooth.bppprintjobsettings.extra.exception"

    invoke-virtual {v2, v4, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$2;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-virtual {v4, v2}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    :goto_0
    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$2;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-static {v4}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$100(Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;)Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->bppDisconnect()V

    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$2;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$502(Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;Z)Z

    :cond_0
    return-void

    :cond_1
    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$2;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-static {v4}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$400(Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;)Landroid/os/Handler;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$2;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-static {v5}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$400(Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;)Landroid/os/Handler;

    move-result-object v5

    const/16 v6, 0x8

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v8, v7}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
