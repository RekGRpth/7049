.class Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;
.super Landroid/os/Handler;
.source "BluetoothBppManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;


# direct methods
.method constructor <init>(Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 12
    .param p1    # Landroid/os/Message;

    const/4 v11, 0x2

    const/4 v10, 0x0

    const/16 v7, 0x64

    const v9, 0x7f060045

    const/4 v8, 0x1

    const-string v4, "BluetoothBppManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Handler(): got msg="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p1, Landroid/os/Message;->what:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget v4, p1, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const-string v4, "BluetoothBppManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "BPP_ENABLE_RESULT: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget v4, p1, Landroid/os/Message;->arg1:I

    if-eqz v4, :cond_1

    const-string v4, "BluetoothBppManager"

    const-string v5, "enable error, stop bpp server"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$200()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f060027

    invoke-static {v4, v5, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-static {v4}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$100(Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;)Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->bppDisable()V

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-static {}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$200()Landroid/content/Context;

    move-result-object v5

    invoke-static {v4, v5, v8}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$600(Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;Landroid/content/Context;I)V

    goto :goto_0

    :pswitch_1
    const-string v4, "BluetoothBppManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "BPP_DISABLE_RESULT: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget v4, p1, Landroid/os/Message;->arg1:I

    if-eqz v4, :cond_2

    const-string v4, "BluetoothBppManager"

    const-string v5, "disable error"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-static {v4}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$100(Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;)Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->disableService()V

    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-static {}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$200()Landroid/content/Context;

    move-result-object v5

    invoke-static {v4, v5, v11}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$600(Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;Landroid/content/Context;I)V

    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    new-instance v5, Landroid/content/Intent;

    invoke-static {}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$200()Landroid/content/Context;

    move-result-object v6

    const-class v7, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-direct {v5, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v4, v5}, Landroid/content/ContextWrapper;->stopService(Landroid/content/Intent;)Z

    goto/16 :goto_0

    :pswitch_2
    const-string v4, "BluetoothBppManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CONNECT_CNF: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget v4, p1, Landroid/os/Message;->arg1:I

    if-eqz v4, :cond_3

    const-string v4, "BluetoothBppManager"

    const-string v5, "Connect fail, stop bpp server"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$200()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f060028

    invoke-static {v4, v5, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-static {v4}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$100(Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;)Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->bppDisable()V

    goto/16 :goto_0

    :cond_3
    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-static {v4, v8}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$702(Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;Z)Z

    goto/16 :goto_0

    :pswitch_3
    const-string v4, "BluetoothBppManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "GET_PRINTER_ATTR_CNF: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, -0x1

    invoke-static {v4}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$302(I)I

    iget v4, p1, Landroid/os/Message;->arg1:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_4

    const-string v4, "BluetoothBppManager"

    const-string v5, "get attributes error"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$200()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f060029

    invoke-static {v4, v5, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-static {v4}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$700(Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;)Z

    move-result v4

    if-ne v4, v8, :cond_0

    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-static {v4}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$100(Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;)Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->bppDisconnect()V

    goto/16 :goto_0

    :cond_4
    iget v4, p1, Landroid/os/Message;->arg1:I

    if-ne v4, v8, :cond_5

    invoke-static {}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$800()Ljava/lang/String;

    move-result-object v4

    const-string v5, "image/jpeg"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    invoke-static {}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$800()Ljava/lang/String;

    move-result-object v4

    const-string v5, "text/x-vcard:3.0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    const-string v4, "BluetoothBppManager"

    const-string v5, "special printer: HP Photosmart D7200 series "

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$200()Landroid/content/Context;

    move-result-object v4

    invoke-static {}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$900()[Ljava/lang/String;

    move-result-object v5

    aget-object v5, v5, v11

    invoke-static {v4, v5, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-static {v4}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$700(Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;)Z

    move-result v4

    if-ne v4, v8, :cond_0

    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-static {v4}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$100(Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;)Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->bppDisconnect()V

    goto/16 :goto_0

    :cond_5
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Lcom/mediatek/bluetooth/bpp/PrinterAttr;

    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$200()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;

    invoke-direct {v0, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v4, 0x10000000

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v4, " com.mediatek.bluetooth.bppprintjobsettings.extra.papersize"

    iget-object v5, v2, Lcom/mediatek/bluetooth/bpp/PrinterAttr;->MediaSize:[Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, " com.mediatek.bluetooth.bppprintjobsettings.extra.copies"

    iget v5, v2, Lcom/mediatek/bluetooth/bpp/PrinterAttr;->MaxCopies:I

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v4, " com.mediatek.bluetooth.bppprintjobsettings.extra.sides"

    iget-object v5, v2, Lcom/mediatek/bluetooth/bpp/PrinterAttr;->Sides:[Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, " com.mediatek.bluetooth.bppprintjobsettings.extra.pagespersheet"

    iget-object v5, v2, Lcom/mediatek/bluetooth/bpp/PrinterAttr;->MaxNumberup:[Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, " com.mediatek.bluetooth.bppprintjobsettings.extra.orientation"

    iget-object v5, v2, Lcom/mediatek/bluetooth/bpp/PrinterAttr;->Orientations:[Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, " com.mediatek.bluetooth.bppprintjobsettings.extra.quality"

    iget-object v5, v2, Lcom/mediatek/bluetooth/bpp/PrinterAttr;->Qualities:[Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, " com.mediatek.bluetooth.bppprintjobsettings.extra.filename"

    invoke-static {}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$1000()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-virtual {v4, v0}, Landroid/content/ContextWrapper;->startActivity(Landroid/content/Intent;)V

    new-instance v1, Landroid/content/Intent;

    const-string v4, "com.mediatek.bluetooth.bppprinting.action.ATTR_UPDATE"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-virtual {v4, v1}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    :pswitch_4
    const-string v4, "BluetoothBppManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "PRINT_DOC_CNF: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-static {v4, v10}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$1102(Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;Z)Z

    iget v4, p1, Landroid/os/Message;->arg1:I

    if-eqz v4, :cond_6

    const-string v4, "BluetoothBppManager"

    const-string v5, "print doc error"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "BluetoothBppManager"

    const-string v5, "print doc error - SEND broadcast to DIALOG"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-string v4, "com.mediatek.bluetooth.bppprinting.action.PRINTING_UPDATE"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "com.mediatek.bluetooth.bppprinting.extra.DIALOG_TYPE"

    const/4 v5, 0x4

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v4, "com.mediatek.bluetooth.bppprinting.extra.PERCENTAGE"

    invoke-virtual {v0, v4, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v4, "com.mediatek.bluetooth.bppprinting.extra.FILE_NAME"

    invoke-static {}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$1000()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "com.mediatek.bluetooth.bppprinting.extra.REASON"

    iget-object v5, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-virtual {v5, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-virtual {v4, v0}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    iget-object v5, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-virtual {v5, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x3

    invoke-static {v4, v10, v7, v5, v6}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$1200(Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;IILjava/lang/String;I)V

    :goto_1
    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-static {v4}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$100(Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;)Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->bppDisconnect()V

    goto/16 :goto_0

    :cond_6
    const-string v4, "BluetoothBppManager"

    const-string v5, "print doc success"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "BluetoothBppManager"

    const-string v5, "print doc success - SEND broadcast to DIALOG"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-string v4, "com.mediatek.bluetooth.bppprinting.action.PRINTING_UPDATE"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "com.mediatek.bluetooth.bppprinting.extra.DIALOG_TYPE"

    const/4 v5, 0x3

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v4, "com.mediatek.bluetooth.bppprinting.extra.PERCENTAGE"

    invoke-virtual {v0, v4, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v4, "com.mediatek.bluetooth.bppprinting.extra.FILE_NAME"

    invoke-static {}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$1000()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "com.mediatek.bluetooth.bppprinting.extra.REASON"

    iget-object v5, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-virtual {v5, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-virtual {v4, v0}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    iget-object v5, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-virtual {v5, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v7, v7, v5, v11}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$1200(Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;IILjava/lang/String;I)V

    goto :goto_1

    :pswitch_5
    const-string v4, "BluetoothBppManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CANCEL_CNF:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget v4, p1, Landroid/os/Message;->arg1:I

    if-eqz v4, :cond_7

    const-string v4, "BluetoothBppManager"

    const-string v5, "cancel error"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_7
    const-string v4, "BluetoothBppManager"

    const-string v5, "cancel success"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :pswitch_6
    const-string v4, "BluetoothBppManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "DISCONNECT_CNF:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget v4, p1, Landroid/os/Message;->arg1:I

    if-eqz v4, :cond_8

    const-string v4, "BluetoothBppManager"

    const-string v5, "disconnect error, stop bpp server"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-static {v4, v10}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$702(Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;Z)Z

    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-static {v4}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$100(Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;)Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->bppDisable()V

    goto/16 :goto_0

    :cond_8
    const-string v4, "BluetoothBppManager"

    const-string v5, "disconnect success, stop bpp server"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :pswitch_7
    const-string v4, "BluetoothBppManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "sentdatalength: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\ttotaldatalength:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-static {v4, v8}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$1102(Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;Z)Z

    iget v4, p1, Landroid/os/Message;->arg1:I

    mul-int/lit8 v4, v4, 0x64

    iget v5, p1, Landroid/os/Message;->arg2:I

    div-int/2addr v4, v5

    invoke-static {v4}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$1302(I)I

    invoke-static {}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$1300()I

    move-result v4

    if-ne v4, v7, :cond_9

    const/16 v4, 0x63

    invoke-static {v4}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$1302(I)I

    :cond_9
    const-string v4, "BluetoothBppManager"

    const-string v5, "print progress ind - SEND broadcast to DIALOG"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-string v4, "com.mediatek.bluetooth.bppprinting.action.PRINTING_UPDATE"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "com.mediatek.bluetooth.bppprinting.extra.FILE_NAME"

    invoke-static {}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$1000()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "com.mediatek.bluetooth.bppprinting.extra.PERCENTAGE"

    invoke-static {}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$1300()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v4, "com.mediatek.bluetooth.bppprinting.extra.REASON"

    iget-object v5, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-virtual {v5, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-virtual {v4, v0}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    invoke-static {}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$1300()I

    move-result v4

    const/16 v5, 0x63

    if-ne v4, v5, :cond_a

    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-static {v4}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$500(Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    const/16 v5, 0x63

    iget-object v6, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-virtual {v6, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v7, v6, v8}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$1200(Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;IILjava/lang/String;I)V

    goto/16 :goto_0

    :cond_a
    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-static {v4}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$500(Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    iget v5, p1, Landroid/os/Message;->arg1:I

    iget v6, p1, Landroid/os/Message;->arg2:I

    iget-object v7, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-virtual {v7, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v5, v6, v7, v8}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$1200(Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;IILjava/lang/String;I)V

    goto/16 :goto_0

    :pswitch_8
    const-string v4, "BluetoothBppManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "jobstatus: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "BluetoothBppManager"

    const-string v5, "job status indpr ind - SEND broadcast to DIALOG"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-string v4, "com.mediatek.bluetooth.bppprinting.action.PRINTING_UPDATE"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "com.mediatek.bluetooth.bppprinting.extra.PERCENTAGE"

    invoke-static {}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$1300()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v4, "com.mediatek.bluetooth.bppprinting.extra.FILE_NAME"

    invoke-static {}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$1000()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v3, 0x0

    iget v4, p1, Landroid/os/Message;->arg1:I

    packed-switch v4, :pswitch_data_1

    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-virtual {v4, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_3
    const-string v4, "com.mediatek.bluetooth.bppprinting.extra.REASON"

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-virtual {v4, v0}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-static {v4}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$500(Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-static {}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$1300()I

    move-result v5

    invoke-static {v4, v5, v7, v3, v8}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$1200(Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;IILjava/lang/String;I)V

    goto/16 :goto_0

    :pswitch_9
    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    const v5, 0x7f06003a

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    :pswitch_a
    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    const v5, 0x7f06003b

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    :pswitch_b
    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    const v5, 0x7f06003c

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    :pswitch_c
    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    const v5, 0x7f06003d

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    :pswitch_d
    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    const v5, 0x7f06003e

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    :pswitch_e
    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    const v5, 0x7f06003f

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    :pswitch_f
    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    const v5, 0x7f060040

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    :pswitch_10
    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    const v5, 0x7f060041

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    :pswitch_11
    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    const v5, 0x7f060042

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    :pswitch_12
    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    const v5, 0x7f060043

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    :pswitch_13
    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    const v5, 0x7f060044

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_3

    :pswitch_14
    const-string v4, "BluetoothBppManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "MSG_ON_BPP_AUTH_IND:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;->access$200()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/mediatek/bluetooth/bpp/BluetoothBppAuthenticating;

    invoke-direct {v0, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v4, 0x50000000

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager$3;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-virtual {v4, v0}, Landroid/content/ContextWrapper;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_7
        :pswitch_8
        :pswitch_14
        :pswitch_4
        :pswitch_6
        :pswitch_6
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
    .end packed-switch
.end method
