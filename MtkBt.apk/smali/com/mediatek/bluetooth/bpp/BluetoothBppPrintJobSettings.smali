.class public Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;
.super Landroid/preference/PreferenceActivity;
.source "BluetoothBppPrintJobSettings.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/mediatek/bluetooth/bpp/CopiesPickerDialog$OnCopiesSetListener;


# static fields
.field public static final ACTION_ATTR_UPDATE:Ljava/lang/String; = "com.mediatek.bluetooth.bppprinting.action.ATTR_UPDATE"

.field private static final DIALOG_COPIESPICKER:I = 0x0

.field public static final EXTRA_EXCEPTION:Ljava/lang/String; = " com.mediatek.bluetooth.bppprintjobsettings.extra.exception"

.field public static final EXTRA_FILE_NAME:Ljava/lang/String; = " com.mediatek.bluetooth.bppprintjobsettings.extra.filename"

.field public static final EXTRA_NUMBER_COPIES:Ljava/lang/String; = " com.mediatek.bluetooth.bppprintjobsettings.extra.copies"

.field public static final EXTRA_ORIENTATION_SETTING:Ljava/lang/String; = " com.mediatek.bluetooth.bppprintjobsettings.extra.orientation"

.field public static final EXTRA_PAPER_SIZE:Ljava/lang/String; = " com.mediatek.bluetooth.bppprintjobsettings.extra.papersize"

.field public static final EXTRA_QUALITY_SETTING:Ljava/lang/String; = " com.mediatek.bluetooth.bppprintjobsettings.extra.quality"

.field public static final EXTRA_SHEET_SETTING:Ljava/lang/String; = " com.mediatek.bluetooth.bppprintjobsettings.extra.pagespersheet"

.field public static final EXTRA_SIDES_SETTING:Ljava/lang/String; = " com.mediatek.bluetooth.bppprintjobsettings.extra.sides"

.field private static final KEY_NUMBER_COPIES:Ljava/lang/String; = "number_of_copies_setting"

.field private static final KEY_ORIENTATION_SETTING:Ljava/lang/String; = "orientation_setting"

.field private static final KEY_PAPER_SIZE:Ljava/lang/String; = "paper_size_setting"

.field private static final KEY_QUALITY_SETTING:Ljava/lang/String; = "quality_setting"

.field private static final KEY_SHEET_SETTING:Ljava/lang/String; = "pages_per_sheet_setting"

.field private static final KEY_SIDES_SETTING:Ljava/lang/String; = "sides_setting"

.field private static final REQUEST_PRINT_PROCESSING_RESULT:I = 0x7bd

.field private static final TAG:Ljava/lang/String; = "BluetoothBppPrintJobSettings"


# instance fields
.field private mCopies:I

.field private mFileName:Ljava/lang/String;

.field private mGetDefaultValue:Landroid/widget/Button;

.field private mMaxCopies:I

.field private mNumberOfCopies:Landroid/preference/Preference;

.field private mOrientation:Landroid/preference/ListPreference;

.field private mPagesPerSheet:Landroid/preference/ListPreference;

.field private mPaperSize:Landroid/preference/ListPreference;

.field private mPrint:Landroid/widget/Button;

.field private mQualitySetting:Landroid/preference/ListPreference;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mSidesSetting:Landroid/preference/ListPreference;

.field private retrunFromPrintingDialog:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->retrunFromPrintingDialog:Z

    const/4 v0, 0x1

    iput v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mCopies:I

    new-instance v0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings$1;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings$1;-><init>(Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->updateAttr()V

    return-void
.end method

.method private updateAttr()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mPaperSize:Landroid/preference/ListPreference;

    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->setValueIndex(I)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mPaperSize:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mPaperSize:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mSidesSetting:Landroid/preference/ListPreference;

    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->setValueIndex(I)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mSidesSetting:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mSidesSetting:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mPagesPerSheet:Landroid/preference/ListPreference;

    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->setValueIndex(I)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mPagesPerSheet:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mPagesPerSheet:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mOrientation:Landroid/preference/ListPreference;

    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->setValueIndex(I)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mOrientation:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mOrientation:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mQualitySetting:Landroid/preference/ListPreference;

    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->setValueIndex(I)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mQualitySetting:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mQualitySetting:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    const/4 v0, 0x1

    iput v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mCopies:I

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mNumberOfCopies:Landroid/preference/Preference;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    invoke-super {p0, p1, p2, p3}, Landroid/preference/PreferenceActivity;->onActivityResult(IILandroid/content/Intent;)V

    const-string v0, "BluetoothBppPrintJobSettings"

    const-string v1, "onActivityResult !"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0x7bd

    if-ne p1, v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->retrunFromPrintingDialog:Z

    const/4 v0, -0x1

    if-ne p2, v0, :cond_2

    const-string v0, "BluetoothBppPrintJobSettings"

    const-string v1, "Result_OK"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x2

    if-ne p2, v0, :cond_3

    const-string v0, "BluetoothBppPrintJobSettings"

    const-string v1, "Result_HIDE"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    const/4 v0, 0x3

    if-ne p2, v0, :cond_0

    const-string v0, "BluetoothBppPrintJobSettings"

    const-string v1, "Result_CANCEL"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    const/4 v3, 0x1

    iget-object v1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mGetDefaultValue:Landroid/widget/Button;

    if-ne p1, v1, :cond_1

    const-string v1, "BluetoothBppPrintJobSettings"

    const-string v2, "Start Bpp Manager to change printer !"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "action"

    const-string v2, "com.mediatek.bluetooth.bppmanager.action.GET_DEFAULT_VALUE"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mPrint:Landroid/widget/Button;

    if-ne p1, v1, :cond_0

    const-string v1, "BluetoothBppPrintJobSettings"

    const-string v2, "Start Bpp Manager to print !"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "action"

    const-string v2, "com.mediatek.bluetooth.bppmanager.action.PRINT"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, " com.mediatek.bluetooth.bppprintjobsettings.extra.papersize"

    iget-object v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mPaperSize:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, " com.mediatek.bluetooth.bppprintjobsettings.extra.copies"

    iget v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mCopies:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, " com.mediatek.bluetooth.bppprintjobsettings.extra.sides"

    iget-object v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mSidesSetting:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, " com.mediatek.bluetooth.bppprintjobsettings.extra.pagespersheet"

    iget-object v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mPagesPerSheet:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, " com.mediatek.bluetooth.bppprintjobsettings.extra.orientation"

    iget-object v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mOrientation:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, " com.mediatek.bluetooth.bppprintjobsettings.extra.quality"

    iget-object v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mQualitySetting:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const/high16 v1, 0x40000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, "action"

    const-string v2, "com.mediatek.bluetooth.bppprinting.action.PRINTING_UPDATE"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.mediatek.bluetooth.bppprinting.extra.PERCENTAGE"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "com.mediatek.bluetooth.bppprinting.extra.DIALOG_TYPE"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "com.mediatek.bluetooth.bppprinting.extra.FILE_NAME"

    iget-object v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mFileName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.mediatek.bluetooth.bppprinting.extra.REASON"

    const v2, 0x7f060045

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iput-boolean v3, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->retrunFromPrintingDialog:Z

    const/16 v1, 0x7bd

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0
.end method

.method public onCopiesSet(I)V
    .locals 2
    .param p1    # I

    iput p1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mCopies:I

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mNumberOfCopies:Landroid/preference/Preference;

    iget v1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mCopies:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;

    const/4 v8, 0x1

    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    const-string v6, "BluetoothBppPrintJobSettings"

    const-string v7, "onCreate......"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const v6, 0x7f03000b

    invoke-virtual {p0, v6}, Landroid/app/Activity;->setContentView(I)V

    const v6, 0x7f040003

    invoke-virtual {p0, v6}, Landroid/preference/PreferenceActivity;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v6, " com.mediatek.bluetooth.bppprintjobsettings.extra.papersize"

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const-string v6, " com.mediatek.bluetooth.bppprintjobsettings.extra.sides"

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    const-string v6, " com.mediatek.bluetooth.bppprintjobsettings.extra.pagespersheet"

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const-string v6, " com.mediatek.bluetooth.bppprintjobsettings.extra.orientation"

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const-string v6, " com.mediatek.bluetooth.bppprintjobsettings.extra.quality"

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    const-string v6, " com.mediatek.bluetooth.bppprintjobsettings.extra.copies"

    invoke-virtual {v0, v6, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    iput v6, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mMaxCopies:I

    const-string v6, " com.mediatek.bluetooth.bppprintjobsettings.extra.filename"

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mFileName:Ljava/lang/String;

    const-string v6, "paper_size_setting"

    invoke-virtual {p0, v6}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v6

    check-cast v6, Landroid/preference/ListPreference;

    iput-object v6, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mPaperSize:Landroid/preference/ListPreference;

    iget-object v6, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mPaperSize:Landroid/preference/ListPreference;

    if-nez v6, :cond_0

    const-string v6, "BluetoothBppPrintJobSettings"

    const-string v7, "PaperSize preference is null"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    const-string v6, "number_of_copies_setting"

    invoke-virtual {p0, v6}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v6

    iput-object v6, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mNumberOfCopies:Landroid/preference/Preference;

    iget-object v6, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mNumberOfCopies:Landroid/preference/Preference;

    if-nez v6, :cond_1

    const-string v6, "BluetoothBppPrintJobSettings"

    const-string v7, "NumberOfCopies preference is null"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    const-string v6, "sides_setting"

    invoke-virtual {p0, v6}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v6

    check-cast v6, Landroid/preference/ListPreference;

    iput-object v6, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mSidesSetting:Landroid/preference/ListPreference;

    iget-object v6, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mSidesSetting:Landroid/preference/ListPreference;

    if-nez v6, :cond_3

    const-string v6, "BluetoothBppPrintJobSettings"

    const-string v7, "SideSetting preference is null"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    const-string v6, "pages_per_sheet_setting"

    invoke-virtual {p0, v6}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v6

    check-cast v6, Landroid/preference/ListPreference;

    iput-object v6, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mPagesPerSheet:Landroid/preference/ListPreference;

    iget-object v6, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mPagesPerSheet:Landroid/preference/ListPreference;

    if-nez v6, :cond_4

    const-string v6, "BluetoothBppPrintJobSettings"

    const-string v7, "PagesPerSheet preference is null"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_3
    const-string v6, "orientation_setting"

    invoke-virtual {p0, v6}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v6

    check-cast v6, Landroid/preference/ListPreference;

    iput-object v6, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mOrientation:Landroid/preference/ListPreference;

    iget-object v6, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mOrientation:Landroid/preference/ListPreference;

    if-nez v6, :cond_5

    const-string v6, "BluetoothBppPrintJobSettings"

    const-string v7, "Orientation preference is null"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_4
    const-string v6, "quality_setting"

    invoke-virtual {p0, v6}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v6

    check-cast v6, Landroid/preference/ListPreference;

    iput-object v6, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mQualitySetting:Landroid/preference/ListPreference;

    iget-object v6, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mQualitySetting:Landroid/preference/ListPreference;

    if-nez v6, :cond_6

    const-string v6, "BluetoothBppPrintJobSettings"

    const-string v7, "QualitySetting preference is null"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_5
    const v6, 0x7f080011

    invoke-virtual {p0, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/Button;

    iput-object v6, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mGetDefaultValue:Landroid/widget/Button;

    iget-object v6, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mGetDefaultValue:Landroid/widget/Button;

    if-nez v6, :cond_7

    const-string v6, "BluetoothBppPrintJobSettings"

    const-string v7, "Get Default Value button is null"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_6
    const v6, 0x7f080010

    invoke-virtual {p0, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/Button;

    iput-object v6, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mPrint:Landroid/widget/Button;

    iget-object v6, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mPrint:Landroid/widget/Button;

    if-nez v6, :cond_8

    const-string v6, "BluetoothBppPrintJobSettings"

    const-string v7, "Print button is null"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_7
    iget-object v6, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v7, Landroid/content/IntentFilter;

    const-string v8, "com.mediatek.bluetooth.bppprinting.action.ATTR_UPDATE"

    invoke-direct {v7, v8}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v6, v7}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void

    :cond_0
    iget-object v6, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mPaperSize:Landroid/preference/ListPreference;

    invoke-virtual {v6, v3}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    iget-object v6, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mPaperSize:Landroid/preference/ListPreference;

    invoke-virtual {v6, v3}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_1
    iget v6, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mMaxCopies:I

    if-ne v8, v6, :cond_2

    iget-object v6, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mNumberOfCopies:Landroid/preference/Preference;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/preference/Preference;->setEnabled(Z)V

    :goto_8
    iget-object v6, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mNumberOfCopies:Landroid/preference/Preference;

    const-string v7, "1"

    invoke-virtual {v6, v7}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_2
    iget-object v6, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mNumberOfCopies:Landroid/preference/Preference;

    invoke-virtual {v6, v8}, Landroid/preference/Preference;->setEnabled(Z)V

    goto :goto_8

    :cond_3
    iget-object v6, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mSidesSetting:Landroid/preference/ListPreference;

    invoke-virtual {v6, v5}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    iget-object v6, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mSidesSetting:Landroid/preference/ListPreference;

    invoke-virtual {v6, v5}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :cond_4
    iget-object v6, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mPagesPerSheet:Landroid/preference/ListPreference;

    invoke-virtual {v6, v2}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    iget-object v6, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mPagesPerSheet:Landroid/preference/ListPreference;

    invoke-virtual {v6, v2}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    :cond_5
    iget-object v6, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mOrientation:Landroid/preference/ListPreference;

    invoke-virtual {v6, v1}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    iget-object v6, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mOrientation:Landroid/preference/ListPreference;

    invoke-virtual {v6, v1}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    :cond_6
    iget-object v6, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mQualitySetting:Landroid/preference/ListPreference;

    invoke-virtual {v6, v4}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    iget-object v6, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mQualitySetting:Landroid/preference/ListPreference;

    invoke-virtual {v6, v4}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    :cond_7
    iget-object v6, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mGetDefaultValue:Landroid/widget/Button;

    invoke-virtual {v6, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_6

    :cond_8
    iget-object v6, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mPrint:Landroid/widget/Button;

    invoke-virtual {v6, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_7
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 2
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Lcom/mediatek/bluetooth/bpp/CopiesPickerDialog;

    iget v1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mMaxCopies:I

    invoke-direct {v0, p0, p0, v1}, Lcom/mediatek/bluetooth/bpp/CopiesPickerDialog;-><init>(Landroid/content/Context;Lcom/mediatek/bluetooth/bpp/CopiesPickerDialog$OnCopiesSetListener;I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 2

    const-string v0, "BluetoothBppPrintJobSettings"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    return-void
.end method

.method protected onPause()V
    .locals 4

    const-string v2, "BluetoothBppPrintJobSettings"

    const-string v3, "onPause()"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v2}, Landroid/preference/Preference;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    if-nez v1, :cond_1

    const-string v2, "BluetoothBppPrintJobSettings"

    const-string v3, "share preferences is null"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    iget-boolean v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->retrunFromPrintingDialog:Z

    if-nez v2, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "action"

    const-string v3, "com.mediatek.bluetooth.bppmanager.action.CANCEL"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void

    :cond_1
    invoke-interface {v1, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    goto :goto_0
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 2
    .param p1    # Landroid/preference/PreferenceScreen;
    .param p2    # Landroid/preference/Preference;

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mNumberOfCopies:Landroid/preference/Preference;

    if-ne p2, v0, :cond_0

    invoke-virtual {p0, v1}, Landroid/app/Activity;->showDialog(I)V

    :cond_0
    return v1
.end method

.method public onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 2
    .param p1    # I
    .param p2    # Landroid/app/Dialog;

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    move-object v0, p2

    check-cast v0, Lcom/mediatek/bluetooth/bpp/CopiesPickerDialog;

    iget v1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mCopies:I

    invoke-virtual {v0, v1}, Lcom/mediatek/bluetooth/bpp/CopiesPickerDialog;->updateCopies(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 3

    const-string v1, "BluetoothBppPrintJobSettings"

    const-string v2, "onResume()"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1}, Landroid/preference/Preference;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v1, "BluetoothBppPrintJobSettings"

    const-string v2, "share preferences is null"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    goto :goto_0
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/content/SharedPreferences;
    .param p2    # Ljava/lang/String;

    const-string v0, "BluetoothBppPrintJobSettings"

    const-string v1, "onSharePreferenceChanged......"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mPaperSize:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mPaperSize:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mSidesSetting:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mSidesSetting:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mPagesPerSheet:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mPagesPerSheet:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mOrientation:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mOrientation:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mQualitySetting:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->mQualitySetting:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected onStart()V
    .locals 2

    const-string v0, "BluetoothBppPrintJobSettings"

    const-string v1, "onStart()"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->retrunFromPrintingDialog:Z

    invoke-direct {p0}, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrintJobSettings;->updateAttr()V

    return-void
.end method

.method protected onStop()V
    .locals 2

    const-string v0, "BluetoothBppPrintJobSettings"

    const-string v1, "onStop()"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onStop()V

    return-void
.end method
