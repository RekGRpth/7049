.class Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting$1;
.super Landroid/content/BroadcastReceiver;
.source "BluetoothBppPrinting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;


# direct methods
.method constructor <init>(Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting$1;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v4, -0x1

    const-string v2, "BluetoothBppPrinting"

    const-string v3, "onReceive"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v2, "com.mediatek.bluetooth.bppprinting.action.PRINTING_UPDATE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "com.mediatek.bluetooth.bppprinting.extra.DIALOG_TYPE"

    invoke-virtual {p2, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    if-eq v4, v1, :cond_0

    iget-object v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting$1;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;

    invoke-static {v2, v1}, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->access$002(Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;I)I

    :cond_0
    iget-object v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting$1;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;

    const-string v3, "com.mediatek.bluetooth.bppprinting.extra.PERCENTAGE"

    const/4 v4, 0x0

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    invoke-static {v2, v3}, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->access$102(Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;I)I

    iget-object v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting$1;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;

    const-string v3, "com.mediatek.bluetooth.bppprinting.extra.FILE_NAME"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->access$202(Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;Ljava/lang/String;)Ljava/lang/String;

    iget-object v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting$1;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;

    iget-object v3, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting$1;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;

    invoke-static {v3}, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->access$400(Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->access$302(Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;Ljava/lang/String;)Ljava/lang/String;

    iget-object v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting$1;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;

    const-string v3, "com.mediatek.bluetooth.bppprinting.extra.REASON"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->access$402(Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;Ljava/lang/String;)Ljava/lang/String;

    iget-object v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting$1;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;

    invoke-static {v2}, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->access$400(Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting$1;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;

    const-string v3, "Reason"

    invoke-static {v2, v3}, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->access$402(Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;Ljava/lang/String;)Ljava/lang/String;

    :cond_1
    const/16 v2, 0x63

    iget-object v3, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting$1;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;

    invoke-static {v3}, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->access$100(Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;)I

    move-result v3

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting$1;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;

    invoke-static {v2}, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->access$300(Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting$1;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;

    const v4, 0x7f060045

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting$1;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;

    iget-object v3, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting$1;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;

    invoke-static {v3}, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->access$300(Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->access$402(Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;Ljava/lang/String;)Ljava/lang/String;

    :cond_2
    const-string v2, "BluetoothBppPrinting"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mDialogType:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting$1;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;

    invoke-static {v4}, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->access$000(Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\tmNotificaitonId:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting$1;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;

    invoke-static {v4}, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->access$500(Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\tmPercentage:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting$1;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;

    invoke-static {v4}, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->access$100(Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\tmFileName:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting$1;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;

    invoke-static {v4}, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->access$200(Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\tmReason:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting$1;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;

    invoke-static {v4}, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->access$400(Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting$1;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;

    invoke-static {v2}, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->access$600(Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;)V

    iget-object v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting$1;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;

    invoke-static {v2}, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->access$700(Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;)V

    iget-object v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting$1;->this$0:Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;

    invoke-static {v2}, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->access$800(Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;)V

    :cond_3
    return-void
.end method
