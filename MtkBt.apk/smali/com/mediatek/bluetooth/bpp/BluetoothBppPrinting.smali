.class public Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;
.super Lcom/android/internal/app/AlertActivity;
.source "BluetoothBppPrinting.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# static fields
.field public static final ACTION_PRINTING_UPDATE:Ljava/lang/String; = "com.mediatek.bluetooth.bppprinting.action.PRINTING_UPDATE"

.field public static final DIALOG_PRINT_FAIL:I = 0x4

.field public static final DIALOG_PRINT_PROCESSING:I = 0x2

.field public static final DIALOG_PRINT_PROCESSING_INIT:I = 0x1

.field public static final DIALOG_PRINT_SUCCESS:I = 0x3

.field public static final EXTRA_DIALOG_TYPE:Ljava/lang/String; = "com.mediatek.bluetooth.bppprinting.extra.DIALOG_TYPE"

.field public static final EXTRA_FILE_NAME:Ljava/lang/String; = "com.mediatek.bluetooth.bppprinting.extra.FILE_NAME"

.field public static final EXTRA_NOTIFICATION_ID:Ljava/lang/String; = "com.mediatek.bluetooth.bppprinting.extra.NOTIFICATION_ID"

.field public static final EXTRA_PERCENTAGE:Ljava/lang/String; = "com.mediatek.bluetooth.bppprinting.extra.PERCENTAGE"

.field public static final EXTRA_REASON:Ljava/lang/String; = "com.mediatek.bluetooth.bppprinting.extra.REASON"

.field public static final RESULT_BACK:I = 0x4

.field public static final RESULT_CANCEL:I = 0x3

.field public static final RESULT_HIDE:I = 0x2

.field private static final TAG:Ljava/lang/String; = "BluetoothBppPrinting"


# instance fields
.field private clicked:Z

.field private mDescriptionView:Landroid/widget/TextView;

.field private mDialogType:I

.field private mFileName:Ljava/lang/String;

.field private mNotificationId:I

.field private mOldReason:Ljava/lang/String;

.field private mPara:Lcom/android/internal/app/AlertController$AlertParams;

.field private mPercentText:Landroid/widget/TextView;

.field private mPercentage:I

.field private mReason:Ljava/lang/String;

.field private mReasonView:Landroid/widget/TextView;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mSendingProgress:Landroid/widget/ProgressBar;

.field private mView:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/android/internal/app/AlertActivity;-><init>()V

    iput-object v1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mView:Landroid/view/View;

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mPercentage:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->clicked:Z

    iput-object v1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mFileName:Ljava/lang/String;

    iput-object v1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mReason:Ljava/lang/String;

    iput-object v1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mOldReason:Ljava/lang/String;

    const v0, 0x2625a00

    iput v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mNotificationId:I

    new-instance v0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting$1;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting$1;-><init>(Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;)I
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;

    iget v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mDialogType:I

    return v0
.end method

.method static synthetic access$002(Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;I)I
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mDialogType:I

    return p1
.end method

.method static synthetic access$100(Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;)I
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;

    iget v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mPercentage:I

    return v0
.end method

.method static synthetic access$102(Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;I)I
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mPercentage:I

    return p1
.end method

.method static synthetic access$200(Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mFileName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$202(Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mFileName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mOldReason:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$302(Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mOldReason:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$400(Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mReason:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$402(Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mReason:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$500(Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;)I
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;

    iget v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mNotificationId:I

    return v0
.end method

.method static synthetic access$600(Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->setViewContent()V

    return-void
.end method

.method static synthetic access$700(Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->updateProgressbar()V

    return-void
.end method

.method static synthetic access$800(Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->updateButton()V

    return-void
.end method

.method private createView()Landroid/view/View;
    .locals 3

    const-string v0, "BluetoothBppPrinting"

    const-string v1, "createView"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03000c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mView:Landroid/view/View;

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mView:Landroid/view/View;

    const v1, 0x7f08000d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mSendingProgress:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mView:Landroid/view/View;

    const v1, 0x7f08000c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mPercentText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mView:Landroid/view/View;

    const v1, 0x7f08000b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mDescriptionView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mView:Landroid/view/View;

    const v1, 0x7f080012

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mReasonView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mSendingProgress:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mPercentText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mDescriptionView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mReasonView:Landroid/widget/TextView;

    if-nez v0, :cond_1

    :cond_0
    const-string v0, "BluetoothBppPrinting"

    const-string v1, "visual component is null"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mView:Landroid/view/View;

    return-object v0

    :cond_1
    invoke-direct {p0}, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->setViewContent()V

    goto :goto_0
.end method

.method private setUpDialog()V
    .locals 4

    const v3, 0x7f060039

    const v2, 0x7f060038

    const-string v0, "BluetoothBppPrinting"

    const-string v1, "setUpDialog"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/internal/app/AlertActivity;->mAlertParams:Lcom/android/internal/app/AlertController$AlertParams;

    iput-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    const v1, 0x108009b

    iput v1, v0, Lcom/android/internal/app/AlertController$AlertParams;->mIconId:I

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    const v1, 0x7f060022

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/internal/app/AlertController$AlertParams;->mTitle:Ljava/lang/CharSequence;

    iget v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mDialogType:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mDialogType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    const v1, 0x7f060037

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/internal/app/AlertController$AlertParams;->mPositiveButtonText:Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    iput-object p0, v0, Lcom/android/internal/app/AlertController$AlertParams;->mPositiveButtonListener:Landroid/content/DialogInterface$OnClickListener;

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/internal/app/AlertController$AlertParams;->mNegativeButtonText:Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    iput-object p0, v0, Lcom/android/internal/app/AlertController$AlertParams;->mNegativeButtonListener:Landroid/content/DialogInterface$OnClickListener;

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->createView()Landroid/view/View;

    move-result-object v1

    iput-object v1, v0, Lcom/android/internal/app/AlertController$AlertParams;->mView:Landroid/view/View;

    invoke-virtual {p0}, Lcom/android/internal/app/AlertActivity;->setupAlert()V

    return-void

    :cond_2
    iget v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mDialogType:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/internal/app/AlertController$AlertParams;->mPositiveButtonText:Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    iput-object p0, v0, Lcom/android/internal/app/AlertController$AlertParams;->mPositiveButtonListener:Landroid/content/DialogInterface$OnClickListener;

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/internal/app/AlertController$AlertParams;->mNegativeButtonText:Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    iput-object p0, v0, Lcom/android/internal/app/AlertController$AlertParams;->mNegativeButtonListener:Landroid/content/DialogInterface$OnClickListener;

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mDialogType:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    const v1, 0x1080027

    iput v1, v0, Lcom/android/internal/app/AlertController$AlertParams;->mIconId:I

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/internal/app/AlertController$AlertParams;->mPositiveButtonText:Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    iput-object p0, v0, Lcom/android/internal/app/AlertController$AlertParams;->mPositiveButtonListener:Landroid/content/DialogInterface$OnClickListener;

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/internal/app/AlertController$AlertParams;->mNegativeButtonText:Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    iput-object p0, v0, Lcom/android/internal/app/AlertController$AlertParams;->mNegativeButtonListener:Landroid/content/DialogInterface$OnClickListener;

    goto :goto_0
.end method

.method private setViewContent()V
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/16 v4, 0x8

    const-string v0, "BluetoothBppPrinting"

    const-string v1, "setViewContent"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mDialogType:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mDialogType:I

    if-ne v0, v5, :cond_3

    :cond_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mDescriptionView:Landroid/widget/TextView;

    const v1, 0x7f060034

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mFileName:Ljava/lang/String;

    aput-object v3, v2, v6

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mReasonView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mReason:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mReason:Ljava/lang/String;

    const v1, 0x7f060045

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-ne v0, v5, :cond_2

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mReasonView:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mReasonView:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mDialogType:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mDescriptionView:Landroid/widget/TextView;

    const v1, 0x7f060035

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mFileName:Ljava/lang/String;

    aput-object v3, v2, v6

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mSendingProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mPercentText:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mReasonView:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_4
    iget v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mDialogType:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mDescriptionView:Landroid/widget/TextView;

    const v1, 0x7f060036

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mFileName:Ljava/lang/String;

    aput-object v3, v2, v6

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mSendingProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mPercentText:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mReasonView:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateButton()V
    .locals 9

    const v8, 0x7f060039

    const v7, 0x7f060038

    const v6, 0x7f060037

    const/16 v5, 0x8

    const/4 v4, 0x0

    const-string v2, "BluetoothBppPrinting"

    const-string v3, "updateButton"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/internal/app/AlertActivity;->mAlert:Lcom/android/internal/app/AlertController;

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Lcom/android/internal/app/AlertController;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    iget-object v2, p0, Lcom/android/internal/app/AlertActivity;->mAlert:Lcom/android/internal/app/AlertController;

    const/4 v3, -0x2

    invoke-virtual {v2, v3}, Lcom/android/internal/app/AlertController;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    if-eqz v1, :cond_0

    if-nez v0, :cond_2

    :cond_0
    const-string v2, "BluetoothBppPrinting"

    const-string v3, "get null button"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mDialogType:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_3
    iget v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mDialogType:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_4
    iget v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mDialogType:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_5

    invoke-virtual {p0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_5
    iget v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mDialogType:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/android/internal/app/AlertActivity;->mAlert:Lcom/android/internal/app/AlertController;

    const v3, 0x1080027

    invoke-virtual {v2, v3}, Lcom/android/internal/app/AlertController;->setIcon(I)V

    invoke-virtual {p0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateProgressbar()V
    .locals 3

    const-string v0, "BluetoothBppPrinting"

    const-string v1, "updateProgressbar"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mSendingProgress:Landroid/widget/ProgressBar;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mSendingProgress:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mPercentage:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mPercentText:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mPercentage:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 3

    const-string v1, "BluetoothBppPrinting"

    const-string v2, "onBackPressed"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x1

    iget v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mDialogType:I

    if-ne v1, v2, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "action"

    const-string v2, "com.mediatek.bluetooth.bppmanager.action.CANCEL"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setResult(I)V

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, -0x1

    const-string v1, "BluetoothBppPrinting"

    const-string v2, "onClick"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->clicked:Z

    if-ne p2, v3, :cond_4

    const-string v1, "BluetoothBppPrinting"

    const-string v2, "positive button"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget v1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mDialogType:I

    if-ne v1, v4, :cond_1

    invoke-virtual {p0, v4}, Landroid/app/Activity;->setResult(I)V

    :cond_0
    :goto_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void

    :cond_1
    iget v1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mDialogType:I

    const/4 v2, 0x4

    if-eq v1, v2, :cond_2

    iget v1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mDialogType:I

    if-ne v1, v5, :cond_3

    :cond_2
    const-string v1, "notification"

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    iget v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mNotificationId:I

    invoke-virtual {v1, v2}, Landroid/app/NotificationManager;->cancel(I)V

    invoke-virtual {p0, v3}, Landroid/app/Activity;->setResult(I)V

    goto :goto_0

    :cond_3
    const-string v1, "BluetoothBppPrinting"

    const-string v2, "exception case"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    const/4 v1, -0x2

    if-ne p2, v1, :cond_0

    const-string v1, "BluetoothBppPrinting"

    const-string v2, "negative button"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "notification"

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    iget v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mNotificationId:I

    invoke-virtual {v1, v2}, Landroid/app/NotificationManager;->cancel(I)V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mediatek/bluetooth/bpp/BluetoothBppManager;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "action"

    const-string v2, "com.mediatek.bluetooth.bppmanager.action.CANCEL"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    invoke-virtual {p0, v5}, Landroid/app/Activity;->setResult(I)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const/4 v4, 0x1

    const/4 v5, 0x0

    const-string v2, "BluetoothBppPrinting"

    const-string v3, "OnCreate"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Lcom/android/internal/app/AlertActivity;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v2, "clicked"

    invoke-virtual {p1, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->clicked:Z

    :cond_0
    iget-boolean v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->clicked:Z

    if-nez v2, :cond_3

    const-string v2, "BluetoothBppPrinting"

    const-string v3, "Get attributes from savedInstanceState"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "com.mediatek.bluetooth.bppprinting.extra.DIALOG_TYPE"

    invoke-virtual {p1, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mDialogType:I

    const-string v2, "com.mediatek.bluetooth.bppprinting.extra.PERCENTAGE"

    invoke-virtual {p1, v2, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mPercentage:I

    const-string v2, "com.mediatek.bluetooth.bppprinting.extra.NOTIFICATION_ID"

    iget v3, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mNotificationId:I

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mNotificationId:I

    const-string v2, "com.mediatek.bluetooth.bppprinting.extra.FILE_NAME"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mFileName:Ljava/lang/String;

    const-string v2, "com.mediatek.bluetooth.bppprinting.extra.REASON"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mReason:Ljava/lang/String;

    iget-object v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mFileName:Ljava/lang/String;

    if-nez v2, :cond_1

    const-string v2, "FileName"

    iput-object v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mFileName:Ljava/lang/String;

    :cond_1
    iget-object v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mReason:Ljava/lang/String;

    if-nez v2, :cond_2

    const-string v2, "Reason"

    iput-object v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mReason:Ljava/lang/String;

    :cond_2
    :goto_0
    const-string v2, "BluetoothBppPrinting"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mDialogType:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mDialogType:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\tmNotificaitonId:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mNotificationId:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\tmPercentage:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mPercentage:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\tmFileName:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mFileName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\tmReason:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mReason:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v5, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->clicked:Z

    invoke-direct {p0}, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->setUpDialog()V

    iget-object v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "com.mediatek.bluetooth.bppprinting.action.PRINTING_UPDATE"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2, v3}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void

    :cond_3
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v2, "com.mediatek.bluetooth.bppprinting.extra.DIALOG_TYPE"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mDialogType:I

    const-string v2, "com.mediatek.bluetooth.bppprinting.extra.PERCENTAGE"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mPercentage:I

    const-string v2, "com.mediatek.bluetooth.bppprinting.extra.NOTIFICATION_ID"

    iget v3, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mNotificationId:I

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mNotificationId:I

    const-string v2, "com.mediatek.bluetooth.bppprinting.extra.FILE_NAME"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mFileName:Ljava/lang/String;

    const-string v2, "com.mediatek.bluetooth.bppprinting.extra.REASON"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mReason:Ljava/lang/String;

    iget-object v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mFileName:Ljava/lang/String;

    if-nez v2, :cond_4

    const-string v2, "FileName"

    iput-object v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mFileName:Ljava/lang/String;

    :cond_4
    iget-object v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mReason:Ljava/lang/String;

    if-nez v2, :cond_2

    const-string v2, "Reason"

    iput-object v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mReason:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    const-string v0, "BluetoothBppPrinting"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const-string v0, "BluetoothBppPrinting"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSaveInstanceState, clicked="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->clicked:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "clicked"

    iget-boolean v1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->clicked:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "com.mediatek.bluetooth.bppprinting.extra.DIALOG_TYPE"

    iget v1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mDialogType:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "com.mediatek.bluetooth.bppprinting.extra.PERCENTAGE"

    iget v1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mPercentage:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "com.mediatek.bluetooth.bppprinting.extra.NOTIFICATION_ID"

    iget v1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mNotificationId:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "com.mediatek.bluetooth.bppprinting.extra.FILE_NAME"

    iget-object v1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mFileName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "com.mediatek.bluetooth.bppprinting.extra.REASON"

    iget-object v1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->mReason:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method protected onStart()V
    .locals 2

    const-string v0, "BluetoothBppPrinting"

    const-string v1, "onStart"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    invoke-direct {p0}, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->updateProgressbar()V

    invoke-direct {p0}, Lcom/mediatek/bluetooth/bpp/BluetoothBppPrinting;->updateButton()V

    return-void
.end method

.method protected onStop()V
    .locals 2

    const-string v0, "BluetoothBppPrinting"

    const-string v1, "onStop()"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method
