.class public Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;
.super Ljava/lang/Object;
.source "BluetoothBppServer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/bluetooth/bpp/BluetoothBppServer$BppListener;
    }
.end annotation


# static fields
.field private static final BPP_CANCEL_FAIL:I = 0x10

.field private static final BPP_CANCEL_SUCCESS:I = 0xf

.field private static final BPP_CONNECT_FAIL:I = 0x6

.field private static final BPP_CONNECT_SUCCESS:I = 0x5

.field private static final BPP_DISABLE_FAIL:I = 0x3

.field private static final BPP_DISABLE_SUCCESS:I = 0x2

.field private static final BPP_DISCONNECT_FAIL:I = 0xe

.field private static final BPP_DISCONNECT_SUCCESS:I = 0xd

.field private static final BPP_ENABLE_FAIL:I = 0x1

.field private static final BPP_ENABLE_SUCCESS:I = 0x0

.field private static final BPP_GET_PRINT_ATTR_FAIL:I = 0x8

.field private static final BPP_GET_PRINT_ATTR_SUCCESS:I = 0x7

.field private static final BPP_OBEX_AUTHREQ:I = 0x4

.field private static final BPP_PRINT_COMPLETE_FAIL:I = 0xc

.field private static final BPP_PRINT_COMPLETE_SUCCESS:I = 0xb

.field private static final BPP_PRINT_STATUS:I = 0xa

.field private static final BPP_PROGRESS:I = 0x9

.field private static final MEDIASIZE:[Ljava/lang/String;

.field private static final NO_DEFINITION:Ljava/lang/String; = "nukonwn"

.field private static ORIENTATION:[Ljava/lang/String; = null

.field private static QUALITY:[Ljava/lang/String; = null

.field private static SIDESETTING:[Ljava/lang/String; = null

.field private static final TAG:Ljava/lang/String; = "BluetoothBppServer"

.field private static mCurSentLength:I

.field private static mPreSentLength:I

.field private static mSObjLength:I

.field private static final mimeTable:[Ljava/lang/String;


# instance fields
.field private mCallback:Landroid/os/Handler;

.field private mListener:Lcom/mediatek/bluetooth/bpp/BluetoothBppServer$BppListener;

.field private mNativeData:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "application/vnd.pwg-xhtml-print+xml:0.95"

    aput-object v1, v0, v3

    const-string v1, "application/vnd.pwg-xhtml-print+xml:1.0"

    aput-object v1, v0, v4

    const-string v1, "application/vnd.pwg-multiplexed"

    aput-object v1, v0, v5

    const-string v1, "text/plain"

    aput-object v1, v0, v6

    const-string v1, "text/x-vcard:2.1"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "text/x-vcard:3.0"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "text/x-vcalendar:1.0"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "text/calendar:2.0"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "text/x-vmessage:1.1"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "text/x-vnote:1.1"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "image/jpeg"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "image/gif"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "image/bmp"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "image/vnd.wap.wbmp"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "image/png"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "image/svg+xml"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mimeTable:[Ljava/lang/String;

    const/16 v0, 0x2b

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "A10"

    aput-object v1, v0, v3

    const-string v1, "A9"

    aput-object v1, v0, v4

    const-string v1, "A8"

    aput-object v1, v0, v5

    const-string v1, "A7"

    aput-object v1, v0, v6

    const-string v1, "A6"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "A5"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "A5-extra"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "A4"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "A4-tab"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "A4-extra"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "A3"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "A2"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "A1"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "A0"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "2A0"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "B10"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "B9"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "B8"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "B7"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "B6"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "B6C4"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "B5"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "B5-extra"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "B4"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "B3"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "B2"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "B1"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "B0"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "C10"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "C9"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "C8"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "C7"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "C7C6"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "C6"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "C6C5"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "C5"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "C4"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "C3"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "C2"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "C1"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "C0"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "Photo 4x6"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "Letter 8.5x11"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->MEDIASIZE:[Ljava/lang/String;

    sput v3, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mPreSentLength:I

    sput v3, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mCurSentLength:I

    sput v3, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mSObjLength:I

    const-string v0, "extbpp_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->classInitNative()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/os/Handler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mNativeData:I

    iput-object p2, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mCallback:Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->ORIENTATION:[Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->SIDESETTING:[Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->QUALITY:[Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;)Z
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->startListenNative()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;)Z
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->stopListenNative()Z

    move-result v0

    return v0
.end method

.method private native bppAuthRspNative(Lcom/mediatek/bluetooth/bpp/AuthInfo;)V
.end method

.method private native bppDisableNative()Z
.end method

.method private native bppDisconnectNative()V
.end method

.method private native bppEnableNative()Z
.end method

.method private native bppGetPrinterAttrNative(Ljava/lang/String;I)V
.end method

.method private native bppPrintNative(Ljava/lang/String;Lcom/mediatek/bluetooth/bpp/PrintObject;)V
.end method

.method private static native classInitNative()V
.end method

.method private native cleanupDataNative()V
.end method

.method private native disableServiceNative()V
.end method

.method private native enableServiceNative()Z
.end method

.method private native initializeDataNative()V
.end method

.method private onCallback(III[Ljava/lang/String;)V
    .locals 27
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # [Ljava/lang/String;

    packed-switch p1, :pswitch_data_0

    const-string v24, "BluetoothBppServer"

    const-string v25, "UN-KNOWN EVENT"

    invoke-static/range {v24 .. v25}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const-string v24, "BluetoothBppServer"

    const-string v25, "BPP_ENABLE_SUCCESS"

    invoke-static/range {v24 .. v25}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mCallback:Landroid/os/Handler;

    move-object/from16 v24, v0

    const/16 v25, 0x1

    const/16 v26, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v1, v11, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    :pswitch_1
    const-string v24, "BluetoothBppServer"

    const-string v25, "BPP_ENABLE_FAIL"

    invoke-static/range {v24 .. v25}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v11, -0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mCallback:Landroid/os/Handler;

    move-object/from16 v24, v0

    const/16 v25, 0x1

    const/16 v26, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v1, v11, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    :pswitch_2
    const-string v24, "BluetoothBppServer"

    const-string v25, "BPP_DISABLE_SUCCESS"

    invoke-static/range {v24 .. v25}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mCallback:Landroid/os/Handler;

    move-object/from16 v24, v0

    const/16 v25, 0xb

    const/16 v26, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v1, v11, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    :pswitch_3
    const-string v24, "BluetoothBppServer"

    const-string v25, "BPP_DISABLE_FAIL"

    invoke-static/range {v24 .. v25}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v11, -0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mCallback:Landroid/os/Handler;

    move-object/from16 v24, v0

    const/16 v25, 0xb

    const/16 v26, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v1, v11, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    :pswitch_4
    const-string v24, "BluetoothBppServer"

    const-string v25, "BPP_OBEX_AUTHREQ"

    invoke-static/range {v24 .. v25}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v24, 0x0

    aget-object v24, p4, v24

    const-string v25, "1"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_1

    const-string v24, "BluetoothBppServer"

    const-string v25, "UserID required"

    invoke-static/range {v24 .. v25}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mCallback:Landroid/os/Handler;

    move-object/from16 v24, v0

    const/16 v25, 0x7

    invoke-virtual/range {v24 .. v25}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_5
    const-string v24, "BluetoothBppServer"

    const-string v25, "BPP_CONNECT_SUCCESS"

    invoke-static/range {v24 .. v25}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mCallback:Landroid/os/Handler;

    move-object/from16 v24, v0

    const/16 v25, 0x2

    const/16 v26, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v1, v11, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_6
    const-string v24, "BluetoothBppServer"

    const-string v25, "BPP_CONNECT_FAIL"

    invoke-static/range {v24 .. v25}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v11, -0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mCallback:Landroid/os/Handler;

    move-object/from16 v24, v0

    const/16 v25, 0x2

    const/16 v26, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v1, v11, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_7
    const-string v24, "BluetoothBppServer"

    const-string v25, "BPP_DISCONNECT_SUCCESS"

    invoke-static/range {v24 .. v25}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mCallback:Landroid/os/Handler;

    move-object/from16 v24, v0

    const/16 v25, 0x9

    const/16 v26, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v1, v11, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_8
    const-string v24, "BluetoothBppServer"

    const-string v25, "BPP_DISCONNECT_FAIL"

    invoke-static/range {v24 .. v25}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v11, -0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mCallback:Landroid/os/Handler;

    move-object/from16 v24, v0

    const/16 v25, 0x9

    const/16 v26, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v1, v11, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_9
    const-string v24, "BluetoothBppServer"

    const-string v25, "BPP_CANCEL_SUCCESS"

    invoke-static/range {v24 .. v25}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mCallback:Landroid/os/Handler;

    move-object/from16 v24, v0

    const/16 v25, 0x4

    const/16 v26, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v1, v11, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_a
    const-string v24, "BluetoothBppServer"

    const-string v25, "BPP_CANCEL_FAIL"

    invoke-static/range {v24 .. v25}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v11, -0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mCallback:Landroid/os/Handler;

    move-object/from16 v24, v0

    const/16 v25, 0x4

    const/16 v26, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v1, v11, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_b
    const-string v24, "BluetoothBppServer"

    const-string v25, "BPP_GET_PRINT_ATTR_SUCCESS"

    invoke-static/range {v24 .. v25}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v24, 0x0

    aget-object v24, p4, v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Byte;->parseByte(Ljava/lang/String;)B

    move-result v22

    const/16 v24, 0x1

    aget-object v24, p4, v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Byte;->parseByte(Ljava/lang/String;)B

    move-result v16

    const/16 v24, 0x2

    aget-object v24, p4, v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Byte;->parseByte(Ljava/lang/String;)B

    move-result v20

    const/16 v24, 0x3

    aget-object v24, p4, v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v14

    const/16 v24, 0x4

    aget-object v24, p4, v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    const/16 v24, 0x5

    aget-object v24, p4, v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v15

    const/16 v24, 0x6

    aget-object v24, p4, v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v19

    const-string v24, "BluetoothBppServer"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "sides: "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "\torientations: "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "\tqualities: "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "\tmax_numberup: "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "\tmax_copies: "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "\tmedia_size_number: "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "\tprinter_type: "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-array v8, v15, [Ljava/lang/String;

    const/4 v12, 0x0

    :goto_1
    if-ge v12, v15, :cond_2

    sget-object v24, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->MEDIASIZE:[Ljava/lang/String;

    add-int/lit8 v25, v12, 0x7

    aget-object v25, p4, v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v25

    aget-object v24, v24, v25

    aput-object v24, v8, v12

    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    :cond_2
    sget-object v24, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->SIDESETTING:[Ljava/lang/String;

    move-object/from16 v0, p0

    move/from16 v1, v22

    move-object/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->translateBitMap(B[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    sget-object v24, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->ORIENTATION:[Ljava/lang/String;

    move-object/from16 v0, p0

    move/from16 v1, v16

    move-object/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->translateBitMap(B[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    sget-object v24, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->QUALITY:[Ljava/lang/String;

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->translateBitMap(B[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    const/16 v24, 0x1

    move/from16 v0, v24

    if-ne v14, v0, :cond_3

    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v9, v0, [Ljava/lang/String;

    const/16 v24, 0x0

    const-string v25, "1"

    aput-object v25, v9, v24

    :goto_2
    move/from16 v11, v19

    new-instance v4, Lcom/mediatek/bluetooth/bpp/PrinterAttr;

    invoke-direct/range {v4 .. v10}, Lcom/mediatek/bluetooth/bpp/PrinterAttr;-><init>([Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mCallback:Landroid/os/Handler;

    move-object/from16 v24, v0

    const/16 v25, 0x3

    const/16 v26, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v1, v11, v2, v4}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :cond_3
    const/16 v24, 0x4

    move/from16 v0, v24

    if-ge v14, v0, :cond_4

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v9, v0, [Ljava/lang/String;

    const/16 v24, 0x0

    const-string v25, "1"

    aput-object v25, v9, v24

    const/16 v24, 0x1

    const-string v25, "2"

    aput-object v25, v9, v24

    goto :goto_2

    :cond_4
    const/16 v24, 0x3

    move/from16 v0, v24

    new-array v9, v0, [Ljava/lang/String;

    const/16 v24, 0x0

    const-string v25, "1"

    aput-object v25, v9, v24

    const/16 v24, 0x1

    const-string v25, "2"

    aput-object v25, v9, v24

    const/16 v24, 0x2

    const-string v25, "4"

    aput-object v25, v9, v24

    goto :goto_2

    :pswitch_c
    const-string v24, "BluetoothBppServer"

    const-string v25, "BPP_GET_PRINT_ATTR_FAIL"

    invoke-static/range {v24 .. v25}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v11, -0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mCallback:Landroid/os/Handler;

    move-object/from16 v24, v0

    const/16 v25, 0x3

    const/16 v26, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v1, v11, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_d
    const-string v24, "BluetoothBppServer"

    const-string v25, "BPP_PROGRESS"

    invoke-static/range {v24 .. v25}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v24, 0x0

    aget-object v24, p4, v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v21

    const/16 v24, 0x1

    aget-object v24, p4, v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v23

    const-string v24, "BluetoothBppServer"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "current: "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    sget v26, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mCurSentLength:I

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v24, "BluetoothBppServer"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "present: "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    sget v26, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mPreSentLength:I

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    sput v21, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mCurSentLength:I

    const/16 v24, 0x64

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mCallback:Landroid/os/Handler;

    move-object/from16 v24, v0

    const/16 v25, 0x5

    move-object/from16 v0, v24

    move/from16 v1, v25

    move/from16 v2, v21

    move/from16 v3, v23

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :cond_5
    sget v24, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mCurSentLength:I

    sget v25, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mPreSentLength:I

    sub-int v24, v24, v25

    sget v25, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mSObjLength:I

    div-int/lit8 v25, v25, 0x28

    move/from16 v0, v24

    move/from16 v1, v25

    if-le v0, v1, :cond_0

    sget v24, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mCurSentLength:I

    sput v24, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mPreSentLength:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mCallback:Landroid/os/Handler;

    move-object/from16 v24, v0

    const/16 v25, 0x5

    move-object/from16 v0, v24

    move/from16 v1, v25

    move/from16 v2, v21

    move/from16 v3, v23

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_e
    const-string v24, "BluetoothBppServer"

    const-string v25, "BPP_PRINT_STATUS"

    invoke-static/range {v24 .. v25}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v24, 0x0

    aget-object v24, p4, v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    const/16 v24, 0x1

    aget-object v24, p4, v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v17

    const/16 v24, 0x2

    aget-object v24, p4, v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mCallback:Landroid/os/Handler;

    move-object/from16 v24, v0

    const/16 v25, 0x6

    const/16 v26, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v25

    move/from16 v2, v18

    move/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_f
    const-string v24, "BluetoothBppServer"

    const-string v25, "BPP_PRINT_COMPLETE_SUCCESS"

    invoke-static/range {v24 .. v25}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mCallback:Landroid/os/Handler;

    move-object/from16 v24, v0

    const/16 v25, 0x8

    const/16 v26, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v1, v11, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_10
    const-string v24, "BluetoothBppServer"

    const-string v25, "BPP_PRINT_COMPLETE_FAIL"

    invoke-static/range {v24 .. v25}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v11, -0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mCallback:Landroid/os/Handler;

    move-object/from16 v24, v0

    const/16 v25, 0x8

    const/16 v26, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v1, v11, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method private native startListenNative()Z
.end method

.method private native stopListenNative()Z
.end method

.method private translateBitMap(B[Ljava/lang/String;)[Ljava/lang/String;
    .locals 7
    .param p1    # B
    .param p2    # [Ljava/lang/String;

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    const/16 v4, 0x8

    if-ge v1, v4, :cond_1

    const/16 v4, 0x80

    ushr-int/2addr v4, v1

    int-to-byte v3, v4

    and-int v4, p1, v3

    if-eqz v4, :cond_0

    add-int/lit8 v4, v0, 0x1

    int-to-byte v0, v4

    :cond_0
    add-int/lit8 v4, v1, 0x1

    int-to-byte v1, v4

    goto :goto_0

    :cond_1
    new-array v2, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v0, :cond_2

    aget-object v4, p2, v1

    aput-object v4, v2, v1

    const-string v4, "BluetoothBppServer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "listEntries: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v2, v1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v4, v1, 0x1

    int-to-byte v1, v4

    goto :goto_1

    :cond_2
    return-object v2
.end method


# virtual methods
.method public bppAuthRsp(Lcom/mediatek/bluetooth/bpp/AuthInfo;)V
    .locals 2
    .param p1    # Lcom/mediatek/bluetooth/bpp/AuthInfo;

    const-string v0, "BluetoothBppServer"

    const-string v1, "+bppAuthRsp"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mNativeData:I

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->bppAuthRspNative(Lcom/mediatek/bluetooth/bpp/AuthInfo;)V

    :goto_0
    const-string v0, "BluetoothBppServer"

    const-string v1, "-bppAuthRsp"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    const-string v0, "BluetoothBppServer"

    const-string v1, "mNativeData has been cleaned"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public bppDisable()V
    .locals 2

    const-string v0, "BluetoothBppServer"

    const-string v1, "+bppDisable"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mNativeData:I

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->bppDisableNative()Z

    :goto_0
    const-string v0, "BluetoothBppServer"

    const-string v1, "-bppDisable"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    const-string v0, "BluetoothBppServer"

    const-string v1, "mNativeData has been cleaned"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public bppDisconnect()V
    .locals 2

    const-string v0, "BluetoothBppServer"

    const-string v1, "+bppDisconnect"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mNativeData:I

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->bppDisconnectNative()V

    :goto_0
    const-string v0, "BluetoothBppServer"

    const-string v1, "-bppDisconnect"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    const-string v0, "BluetoothBppServer"

    const-string v1, "mNativeData has been cleaned"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public bppGetPrinterAttr(Ljava/lang/String;I)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const-string v0, "BluetoothBppServer"

    const-string v1, "+bppGetPrinterAttr"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mNativeData:I

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->bppGetPrinterAttrNative(Ljava/lang/String;I)V

    :goto_0
    const-string v0, "BluetoothBppServer"

    const-string v1, "-bppGetPrinterAttr"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    const-string v0, "BluetoothBppServer"

    const-string v1, "mNativeData has been cleaned"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public bppPrint(Ljava/lang/String;Lcom/mediatek/bluetooth/bpp/PrintObject;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/mediatek/bluetooth/bpp/PrintObject;

    const/4 v5, 0x1

    const/4 v4, 0x0

    const-string v0, "BluetoothBppServer"

    const-string v1, "+bppPrint"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput v4, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->nMimeType:I

    const-string v0, "BluetoothBppServer"

    sget-object v1, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mimeTable:[Ljava/lang/String;

    array-length v1, v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    iget v0, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->nMimeType:I

    sget-object v1, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mimeTable:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v0, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->MimeType:Ljava/lang/String;

    sget-object v1, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mimeTable:[Ljava/lang/String;

    iget v2, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->nMimeType:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->nMimeType:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "BluetoothBppServer"

    sget-object v1, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mimeTable:[Ljava/lang/String;

    iget v2, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->nMimeType:I

    add-int/lit8 v2, v2, -0x1

    aget-object v1, v1, v2

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    iget v0, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->nMimeType:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->nMimeType:I

    iget-object v0, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->ObjectSize:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->nObjectSize:I

    sput v4, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mPreSentLength:I

    sput v4, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mCurSentLength:I

    iget v0, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->nObjectSize:I

    sput v0, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mSObjLength:I

    iget-object v0, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->NumberUp:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->nNumberUp:I

    iput v4, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->nSides:I

    :goto_1
    iget-object v0, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->Sides:Ljava/lang/String;

    sget-object v1, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->SIDESETTING:[Ljava/lang/String;

    iget v2, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->nSides:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->nSides:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->nSides:I

    goto :goto_1

    :cond_1
    iget v0, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->nSides:I

    shl-int v0, v5, v0

    iput v0, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->nSides:I

    iput v4, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->nOrient:I

    :goto_2
    iget-object v0, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->Orient:Ljava/lang/String;

    sget-object v1, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->ORIENTATION:[Ljava/lang/String;

    iget v2, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->nOrient:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->nOrient:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->nOrient:I

    goto :goto_2

    :cond_2
    iget v0, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->nOrient:I

    shl-int v0, v5, v0

    iput v0, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->nOrient:I

    iput v4, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->nQuality:I

    :goto_3
    iget-object v0, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->Quality:Ljava/lang/String;

    sget-object v1, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->QUALITY:[Ljava/lang/String;

    iget v2, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->nQuality:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_3

    iget v0, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->nQuality:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->nQuality:I

    goto :goto_3

    :cond_3
    iget v0, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->nQuality:I

    shl-int v0, v5, v0

    iput v0, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->nQuality:I

    iput v4, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->nMediaSize:I

    :goto_4
    iget-object v0, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->MediaSize:Ljava/lang/String;

    sget-object v1, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->MEDIASIZE:[Ljava/lang/String;

    iget v2, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->nMediaSize:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_4

    iget v0, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->nMediaSize:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->nMediaSize:I

    goto :goto_4

    :cond_4
    const-string v0, "BluetoothBppServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " filePath: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->DirName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\tfileName: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->FileName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\tfileSize: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->nObjectSize:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\tmimeType:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->nMimeType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\tsideSetting: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->nSides:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\tsheetSetting: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->nNumberUp:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\torientation: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->nOrient:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\tquality: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->nQuality:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\tmediasize: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Lcom/mediatek/bluetooth/bpp/PrintObject;->nMediaSize:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mNativeData:I

    if-eqz v0, :cond_5

    invoke-direct {p0, p1, p2}, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->bppPrintNative(Ljava/lang/String;Lcom/mediatek/bluetooth/bpp/PrintObject;)V

    :goto_5
    const-string v0, "BluetoothBppServer"

    const-string v1, "-bppPrint"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_5
    const-string v0, "BluetoothBppServer"

    const-string v1, "mNativeData has been cleaned"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5
.end method

.method public disableService()V
    .locals 3

    const-string v1, "BluetoothBppServer"

    const-string v2, "+disableService"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mNativeData:I

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mListener:Lcom/mediatek/bluetooth/bpp/BluetoothBppServer$BppListener;

    invoke-virtual {v1}, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer$BppListener;->shutdown()V

    iget-object v1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mListener:Lcom/mediatek/bluetooth/bpp/BluetoothBppServer$BppListener;

    invoke-virtual {v1}, Ljava/lang/Thread;->join()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mListener:Lcom/mediatek/bluetooth/bpp/BluetoothBppServer$BppListener;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-direct {p0}, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->disableServiceNative()V

    invoke-direct {p0}, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->cleanupDataNative()V

    :goto_1
    const-string v1, "BluetoothBppServer"

    const-string v2, "-disableService"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :catch_0
    move-exception v0

    const-string v1, "BluetoothBppServer"

    const-string v2, "BppServer mListener close error"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    const-string v1, "BluetoothBppServer"

    const-string v2, "mNativeData has been cleaned"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public enable()Z
    .locals 2

    const-string v0, "BluetoothBppServer"

    const-string v1, "+enable"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mNativeData:I

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->initializeDataNative()V

    invoke-direct {p0}, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->enableServiceNative()Z

    new-instance v0, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer$BppListener;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer$BppListener;-><init>(Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mListener:Lcom/mediatek/bluetooth/bpp/BluetoothBppServer$BppListener;

    iget-object v0, p0, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->mListener:Lcom/mediatek/bluetooth/bpp/BluetoothBppServer$BppListener;

    invoke-virtual {v0}, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer$BppListener;->startup()V

    :goto_0
    invoke-direct {p0}, Lcom/mediatek/bluetooth/bpp/BluetoothBppServer;->bppEnableNative()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "BluetoothBppServer"

    const-string v1, "-1 enable"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_1
    return v0

    :cond_0
    const-string v0, "BluetoothBppServer"

    const-string v1, "mNativeData has been initialized"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const-string v0, "BluetoothBppServer"

    const-string v1, "-enable"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    goto :goto_1
.end method

.method public onBppDisable(I)V
    .locals 0
    .param p1    # I

    return-void
.end method
