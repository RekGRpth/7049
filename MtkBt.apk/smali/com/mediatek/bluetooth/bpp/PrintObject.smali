.class Lcom/mediatek/bluetooth/bpp/PrintObject;
.super Ljava/lang/Object;
.source "BluetoothBppServer.java"


# instance fields
.field public Copies:I

.field public DirName:Ljava/lang/String;

.field public DocFmt:Ljava/lang/String;

.field public FileName:Ljava/lang/String;

.field public JobState:I

.field public MediaSize:Ljava/lang/String;

.field public MimeType:Ljava/lang/String;

.field public NumberUp:Ljava/lang/String;

.field public ObjectSize:Ljava/lang/String;

.field public Orient:Ljava/lang/String;

.field public PrintMediaType:I

.field public PrinterState:I

.field public PrinterStateReason:I

.field public Quality:Ljava/lang/String;

.field public Sides:Ljava/lang/String;

.field public bJobBased:Z

.field public nMediaSize:I

.field public nMimeType:I

.field public nNumberUp:I

.field public nObjectSize:I

.field public nOrient:I

.field public nQuality:I

.field public nSides:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Z
    .param p6    # I
    .param p7    # Ljava/lang/String;
    .param p8    # Ljava/lang/String;
    .param p9    # Ljava/lang/String;
    .param p10    # Ljava/lang/String;
    .param p11    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mediatek/bluetooth/bpp/PrintObject;->DirName:Ljava/lang/String;

    iput-object p2, p0, Lcom/mediatek/bluetooth/bpp/PrintObject;->FileName:Ljava/lang/String;

    iput-object p3, p0, Lcom/mediatek/bluetooth/bpp/PrintObject;->MimeType:Ljava/lang/String;

    iput-object p4, p0, Lcom/mediatek/bluetooth/bpp/PrintObject;->ObjectSize:Ljava/lang/String;

    iput-boolean p5, p0, Lcom/mediatek/bluetooth/bpp/PrintObject;->bJobBased:Z

    iput p6, p0, Lcom/mediatek/bluetooth/bpp/PrintObject;->Copies:I

    iput-object p7, p0, Lcom/mediatek/bluetooth/bpp/PrintObject;->NumberUp:Ljava/lang/String;

    iput-object p8, p0, Lcom/mediatek/bluetooth/bpp/PrintObject;->Sides:Ljava/lang/String;

    iput-object p9, p0, Lcom/mediatek/bluetooth/bpp/PrintObject;->Orient:Ljava/lang/String;

    iput-object p10, p0, Lcom/mediatek/bluetooth/bpp/PrintObject;->Quality:Ljava/lang/String;

    iput-object p11, p0, Lcom/mediatek/bluetooth/bpp/PrintObject;->MediaSize:Ljava/lang/String;

    return-void
.end method
