.class public Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;
.super Landroid/app/ListActivity;
.source "BluetoothFtpLocalBrowser.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/SimpleCursorAdapter$ViewBinder;
.implements Lcom/mediatek/bluetooth/ftp/GetLocalContentThread$ResultListener;


# static fields
.field private static final DATA_READY:I = 0x0

.field private static final DEFAULT_ROOT:Ljava/lang/String; = "/mnt"

.field private static final ERROR:I = 0x1

.field private static final KEY_CUR_PATH:Ljava/lang/String; = "current_path"

.field private static final LOCAL_BROWSER_BASE:I = 0x898

.field private static final MENU_BASE:I = 0x8a2

.field private static final MENU_EXIT:I = 0x8a5

.field private static final MENU_GOTO_ROOT:I = 0x8a3

.field private static final MENU_MARK_SEVERAL:I = 0x8a4

.field private static final TAG:Ljava/lang/String; = "BluetoothFtpLocalBrowser"

.field private static mThread:Lcom/mediatek/bluetooth/ftp/GetLocalContentThread;


# instance fields
.field private mCurrentPath:Ljava/lang/String;

.field private mCurrentPathView:Landroid/widget/TextView;

.field private mFilter:Landroid/content/IntentFilter;

.field private mFolderContentCursor:Landroid/database/Cursor;

.field private mHandler:Landroid/os/Handler;

.field private mListAdapter:Landroid/widget/SimpleCursorAdapter;

.field private mProgressDialog:Lcom/mediatek/bluetooth/ftp/ProgressDialog;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mRequestFocusListener:Landroid/view/View$OnClickListener;

.field private mRoot:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mThread:Lcom/mediatek/bluetooth/ftp/GetLocalContentThread;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    iput-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mCurrentPathView:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mProgressDialog:Lcom/mediatek/bluetooth/ftp/ProgressDialog;

    iput-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mFolderContentCursor:Landroid/database/Cursor;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mFilter:Landroid/content/IntentFilter;

    new-instance v0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser$1;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser$1;-><init>(Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser$2;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser$2;-><init>(Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser$3;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser$3;-><init>(Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mRequestFocusListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mCurrentPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/mediatek/bluetooth/ftp/GetLocalContentThread;)Lcom/mediatek/bluetooth/ftp/GetLocalContentThread;
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/ftp/GetLocalContentThread;

    sput-object p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mThread:Lcom/mediatek/bluetooth/ftp/GetLocalContentThread;

    return-object p0
.end method

.method static synthetic access$200(Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->updateUI()V

    return-void
.end method

.method static synthetic access$300(Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->sendErrorAndFinish()V

    return-void
.end method

.method private declared-synchronized dismissProgressDialog()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mProgressDialog:Lcom/mediatek/bluetooth/ftp/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mProgressDialog:Lcom/mediatek/bluetooth/ftp/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mProgressDialog:Lcom/mediatek/bluetooth/ftp/ProgressDialog;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static getDateString(J)Ljava/lang/String;
    .locals 7
    .param p0    # J

    const/4 v6, 0x2

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x5

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v4, v6, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_0
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v4, v6, :cond_1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method private sendErrorAndFinish()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.mediatek.bluetooth.ftp.client.ACTION_ERROR"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method private declared-synchronized showProgressDialog()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mProgressDialog:Lcom/mediatek/bluetooth/ftp/ProgressDialog;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    new-instance v0, Lcom/mediatek/bluetooth/ftp/ProgressDialog;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/ftp/ProgressDialog;-><init>(Landroid/content/Context;)V

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    const v1, 0x7f060065

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mediatek/bluetooth/ftp/ProgressDialog;->setMessage(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/mediatek/bluetooth/ftp/ProgressDialog;->setIndeterminate(Z)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mProgressDialog:Lcom/mediatek/bluetooth/ftp/ProgressDialog;

    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mProgressDialog:Lcom/mediatek/bluetooth/ftp/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private declared-synchronized updateData(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->showProgressDialog()V

    invoke-virtual {p0}, Landroid/app/ListActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    new-instance v0, Lcom/mediatek/bluetooth/ftp/GetLocalContentThread;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p1, v1, p0}, Lcom/mediatek/bluetooth/ftp/GetLocalContentThread;-><init>(Ljava/lang/String;Landroid/content/ContentResolver;Lcom/mediatek/bluetooth/ftp/GetLocalContentThread$ResultListener;)V

    sput-object v0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mThread:Lcom/mediatek/bluetooth/ftp/GetLocalContentThread;

    sget-object v0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mThread:Lcom/mediatek/bluetooth/ftp/GetLocalContentThread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private updateUI()V
    .locals 7

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mCurrentPathView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mCurrentPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mFolderContentCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mFolderContentCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_0
    :try_start_0
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/mediatek/bluetooth/ftp/BluetoothFtpProviderHelper$FolderContent;->LOCAL_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mFolderContentCursor:Landroid/database/Cursor;

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mListAdapter:Landroid/widget/SimpleCursorAdapter;

    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mFolderContentCursor:Landroid/database/Cursor;

    invoke-virtual {v0, v1}, Landroid/widget/CursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mListAdapter:Landroid/widget/SimpleCursorAdapter;

    invoke-virtual {p0, v0}, Landroid/app/ListActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mCurrentPathView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    invoke-direct {p0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->dismissProgressDialog()V

    invoke-virtual {p0}, Landroid/app/ListActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v6

    const-string v0, "BluetoothFtpLocalBrowser"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[BT][FTP] updateUI(), Exception: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->sendErrorAndFinish()V

    goto :goto_0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const-string v0, "BluetoothFtpLocalBrowser"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[BT][FTP] Local browser onActivityResult(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x8a4
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 3

    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mCurrentPath:Ljava/lang/String;

    iget-object v2, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mRoot:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mCurrentPath:Ljava/lang/String;

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mCurrentPath:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mCurrentPath:Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mCurrentPath:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->updateData(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const/4 v3, 0x0

    const/4 v2, 0x3

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f060092

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setTitle(I)V

    const v0, 0x7f03000f

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    const v0, 0x7f080016

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mCurrentPathView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mCurrentPathView:Landroid/widget/TextView;

    if-nez v0, :cond_1

    const-string v0, "BluetoothFtpLocalBrowser"

    const-string v1, "onCreate(): mCurrentPathView is null."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/mediatek/bluetooth/util/SystemUtils;->getMountPointPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mRoot:Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mRoot:Ljava/lang/String;

    if-nez v0, :cond_2

    const-string v0, "/mnt"

    iput-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mRoot:Ljava/lang/String;

    :cond_2
    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mRoot:Ljava/lang/String;

    iput-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mCurrentPath:Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mCurrentPathView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mRequestFocusListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mFilter:Landroid/content/IntentFilter;

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-array v4, v2, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "name"

    aput-object v1, v4, v0

    const/4 v0, 0x1

    const-string v1, "type"

    aput-object v1, v4, v0

    const/4 v0, 0x2

    const-string v1, "size"

    aput-object v1, v4, v0

    new-array v5, v2, [I

    fill-array-data v5, :array_0

    new-instance v0, Landroid/widget/SimpleCursorAdapter;

    const v2, 0x7f03000e

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[I)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mListAdapter:Landroid/widget/SimpleCursorAdapter;

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mListAdapter:Landroid/widget/SimpleCursorAdapter;

    invoke-virtual {v0, p0}, Landroid/widget/SimpleCursorAdapter;->setViewBinder(Landroid/widget/SimpleCursorAdapter$ViewBinder;)V

    invoke-virtual {p0}, Landroid/app/ListActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    if-eqz p1, :cond_4

    const-string v0, "current_path"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mCurrentPath:Ljava/lang/String;

    sget-object v0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mThread:Lcom/mediatek/bluetooth/ftp/GetLocalContentThread;

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->showProgressDialog()V

    sget-object v0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mThread:Lcom/mediatek/bluetooth/ftp/GetLocalContentThread;

    invoke-virtual {v0, p0}, Lcom/mediatek/bluetooth/ftp/GetLocalContentThread;->isDone(Lcom/mediatek/bluetooth/ftp/GetLocalContentThread$ResultListener;)Z

    move-result v0

    if-eqz v0, :cond_0

    sput-object v3, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mThread:Lcom/mediatek/bluetooth/ftp/GetLocalContentThread;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->updateUI()V

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->updateUI()V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mCurrentPath:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->updateData(Ljava/lang/String;)V

    goto/16 :goto_0

    :array_0
    .array-data 4
        0x7f080014
        0x7f080013
        0x7f080015
    .end array-data
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1    # Landroid/view/Menu;

    const/4 v2, 0x0

    const/16 v0, 0x8a3

    const v1, 0x7f060086

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f020018

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    const/16 v0, 0x8a4

    const v1, 0x7f060087

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f020016

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    const/16 v0, 0x8a5

    const v1, 0x7f060088

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f020013

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/ListActivity;->onDestroy()V

    invoke-direct {p0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->dismissProgressDialog()V

    const/16 v0, 0x8a4

    invoke-virtual {p0, v0}, Landroid/app/Activity;->finishActivity(I)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mFolderContentCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mFolderContentCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mFolderContentCursor:Landroid/database/Cursor;

    :cond_0
    sget-object v0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mThread:Lcom/mediatek/bluetooth/ftp/GetLocalContentThread;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mThread:Lcom/mediatek/bluetooth/ftp/GetLocalContentThread;

    invoke-virtual {v0}, Lcom/mediatek/bluetooth/ftp/GetLocalContentThread;->removeListener()V

    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 16
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    invoke-virtual/range {p0 .. p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const/4 v11, 0x0

    const v13, 0x7f080014

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    const v13, 0x7f080015

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v13, 0x7f080013

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    if-eqz v5, :cond_0

    if-eqz v3, :cond_0

    if-nez v10, :cond_1

    :cond_0
    const-string v13, "BluetoothFtpLocalBrowser"

    const-string v14, "[BT][FTP] Can\'t find entry_name, entry_info, or entry_type in the list item."

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Integer;

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v9

    const-wide/16 v7, -0x1

    packed-switch v9, :pswitch_data_0

    const-string v13, "BluetoothFtpLocalBrowser"

    const-string v14, "Unknown Type"

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_0
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mCurrentPath:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mCurrentPath:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mCurrentPath:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->updateData(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    :try_start_0
    new-instance v12, Landroid/content/ContentValues;

    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    sget-object v13, Lcom/mediatek/bluetooth/ftp/BluetoothFtpProviderHelper$TransferringFile;->CONTENT_URI:Landroid/net/Uri;

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-virtual {v6, v13, v14, v15}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    const-string v13, "name"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mCurrentPath:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "/"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v13, "status"

    const/16 v14, 0x14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v13, "direction"

    const/16 v14, 0x1f

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v13, Lcom/mediatek/bluetooth/ftp/BluetoothFtpProviderHelper$TransferringFile;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v6, v13, v12}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    new-instance v13, Landroid/content/Intent;

    const-string v14, "com.mediatek.bluetooth.ftp.client.ACTION_PUSH"

    invoke-direct {v13, v14}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->finish()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v11, v12

    goto/16 :goto_0

    :catch_0
    move-exception v1

    :goto_1
    const-string v13, "BluetoothFtpLocalBrowser"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "[BT][FTP] onItemClick(), Execption: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct/range {p0 .. p0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->sendErrorAndFinish()V

    goto/16 :goto_0

    :catch_1
    move-exception v1

    move-object v11, v12

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_0
    const/4 v1, 0x1

    return v1

    :pswitch_0
    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mCurrentPath:Ljava/lang/String;

    iget-object v2, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mRoot:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f06007f

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mRoot:Ljava/lang/String;

    iput-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mCurrentPath:Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mCurrentPath:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->updateData(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/mediatek/bluetooth/ftp/BluetoothFtpSeveralMarker;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "direction"

    const/16 v2, 0x1f

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "local_path"

    iget-object v2, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mCurrentPath:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/16 v1, 0x8a4

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x8a3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onPause()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    invoke-virtual {p0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->dismissProgressDialog()V

    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "current_path"

    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mCurrentPath:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onThreadResult(I)V
    .locals 2
    .param p1    # I

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public setViewValue(Landroid/view/View;Landroid/database/Cursor;I)Z
    .locals 8
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/database/Cursor;
    .param p3    # I

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    const-string v6, "type"

    invoke-interface {p2, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const-string v6, "modified"

    invoke-interface {p2, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    const/4 v3, -0x1

    const/4 v0, 0x0

    packed-switch v2, :pswitch_data_0

    :goto_0
    const/4 v6, 0x1

    return v6

    :pswitch_0
    check-cast p1, Landroid/widget/TextView;

    invoke-interface {p2, p3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_1
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {p1, v6}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    packed-switch v4, :pswitch_data_1

    :pswitch_2
    check-cast p1, Landroid/widget/ImageView;

    const v6, 0x7f02001d

    invoke-virtual {p1, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :pswitch_3
    check-cast p1, Landroid/widget/ImageView;

    const v6, 0x7f020010

    invoke-virtual {p1, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :pswitch_4
    check-cast p1, Landroid/widget/ImageView;

    const v6, 0x7f02000e

    invoke-virtual {p1, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :pswitch_5
    check-cast p1, Landroid/widget/ImageView;

    const v6, 0x7f020011

    invoke-virtual {p1, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :pswitch_6
    check-cast p1, Landroid/widget/ImageView;

    const v6, 0x7f02001e

    invoke-virtual {p1, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :pswitch_7
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    move v3, p3

    const/16 v6, 0xa

    if-ne v4, v6, :cond_0

    check-cast p1, Lcom/mediatek/bluetooth/ftp/EntryInfoView;

    const-wide/16 v6, -0x1

    invoke-virtual {p1, v6, v7, v0}, Lcom/mediatek/bluetooth/ftp/EntryInfoView;->setEntryInfo(JLjava/lang/String;)V

    goto :goto_0

    :cond_0
    check-cast p1, Lcom/mediatek/bluetooth/ftp/EntryInfoView;

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-virtual {p1, v6, v7, v0}, Lcom/mediatek/bluetooth/ftp/EntryInfoView;->setEntryInfo(JLjava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f080013
        :pswitch_1
        :pswitch_0
        :pswitch_7
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0xa
        :pswitch_3
        :pswitch_2
        :pswitch_5
        :pswitch_4
        :pswitch_6
    .end packed-switch
.end method
