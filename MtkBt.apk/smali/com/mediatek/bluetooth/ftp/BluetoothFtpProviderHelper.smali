.class public Lcom/mediatek/bluetooth/ftp/BluetoothFtpProviderHelper;
.super Ljava/lang/Object;
.source "BluetoothFtpProviderHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/bluetooth/ftp/BluetoothFtpProviderHelper$TransferringFile;,
        Lcom/mediatek/bluetooth/ftp/BluetoothFtpProviderHelper$FolderContent;
    }
.end annotation


# static fields
.field public static final AUTHORITY:Ljava/lang/String; = "com.mediatek.provider.bluetooth.ftp"

.field private static final TAG:Ljava/lang/String; = "BluetoothFtpProviderHelper"

.field public static final UNKNOWN_DATE:Ljava/lang/String; = "unknown"

.field public static final UNKNOWN_SIZE:I = -0x1

.field private static sHSAudio:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sHSImage:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sHSVideo:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpProviderHelper;->sHSAudio:Ljava/util/HashSet;

    sget-object v0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpProviderHelper;->sHSAudio:Ljava/util/HashSet;

    const-string v1, "aac"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpProviderHelper;->sHSAudio:Ljava/util/HashSet;

    const-string v1, "amr"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpProviderHelper;->sHSAudio:Ljava/util/HashSet;

    const-string v1, "mid"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpProviderHelper;->sHSAudio:Ljava/util/HashSet;

    const-string v1, "mp3"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpProviderHelper;->sHSAudio:Ljava/util/HashSet;

    const-string v1, "ogg"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpProviderHelper;->sHSAudio:Ljava/util/HashSet;

    const-string v1, "wav"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpProviderHelper;->sHSAudio:Ljava/util/HashSet;

    const-string v1, "wma"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpProviderHelper;->sHSImage:Ljava/util/HashSet;

    sget-object v0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpProviderHelper;->sHSImage:Ljava/util/HashSet;

    const-string v1, "bmp"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpProviderHelper;->sHSImage:Ljava/util/HashSet;

    const-string v1, "gif"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpProviderHelper;->sHSImage:Ljava/util/HashSet;

    const-string v1, "jpeg"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpProviderHelper;->sHSImage:Ljava/util/HashSet;

    const-string v1, "jpg"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpProviderHelper;->sHSImage:Ljava/util/HashSet;

    const-string v1, "png"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpProviderHelper;->sHSVideo:Ljava/util/HashSet;

    sget-object v0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpProviderHelper;->sHSVideo:Ljava/util/HashSet;

    const-string v1, "3gp"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpProviderHelper;->sHSVideo:Ljava/util/HashSet;

    const-string v1, "avi"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpProviderHelper;->sHSAudio:Ljava/util/HashSet;

    const-string v1, "flv"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpProviderHelper;->sHSVideo:Ljava/util/HashSet;

    const-string v1, "mp4"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpProviderHelper;->sHSVideo:Ljava/util/HashSet;

    const-string v1, "wmv"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getTypeCode(Ljava/lang/String;)I
    .locals 5
    .param p0    # Ljava/lang/String;

    const/16 v2, 0xb

    const-string v3, "."

    invoke-virtual {p0, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    const/4 v3, -0x1

    if-eq v1, v3, :cond_0

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {p0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/mediatek/bluetooth/ftp/BluetoothFtpProviderHelper;->sHSAudio:Ljava/util/HashSet;

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/16 v2, 0xd

    :cond_0
    :goto_0
    return v2

    :cond_1
    sget-object v3, Lcom/mediatek/bluetooth/ftp/BluetoothFtpProviderHelper;->sHSImage:Ljava/util/HashSet;

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v2, 0xc

    goto :goto_0

    :cond_2
    sget-object v3, Lcom/mediatek/bluetooth/ftp/BluetoothFtpProviderHelper;->sHSVideo:Ljava/util/HashSet;

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    const/16 v2, 0xe

    goto :goto_0

    :cond_3
    const/16 v2, 0xb

    goto :goto_0
.end method
