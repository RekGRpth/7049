.class Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings$2;
.super Ljava/lang/Object;
.source "BluetoothFtpServerAdvSettings.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;


# direct methods
.method constructor <init>(Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings$2;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Landroid/os/IBinder;

    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings$2;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;

    invoke-static {p2}, Landroid/bluetooth/IBluetoothFtpServer$Stub;->asInterface(Landroid/os/IBinder;)Landroid/bluetooth/IBluetoothFtpServer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;->access$102(Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;Landroid/bluetooth/IBluetoothFtpServer;)Landroid/bluetooth/IBluetoothFtpServer;

    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings$2;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;

    invoke-static {v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;->access$100(Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;)Landroid/bluetooth/IBluetoothFtpServer;

    move-result-object v1

    if-eqz v1, :cond_1

    :try_start_0
    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings$2;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;

    invoke-static {v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;->access$100(Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;)Landroid/bluetooth/IBluetoothFtpServer;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings$2;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;

    invoke-static {v2}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;->access$200(Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;)Landroid/bluetooth/IBluetoothFtpServerCallback;

    move-result-object v2

    invoke-interface {v1, v2}, Landroid/bluetooth/IBluetoothFtpServer;->registerCallback(Landroid/bluetooth/IBluetoothFtpServerCallback;)V

    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings$2;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;

    invoke-static {v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;->access$100(Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;)Landroid/bluetooth/IBluetoothFtpServer;

    move-result-object v1

    invoke-interface {v1}, Landroid/bluetooth/IBluetoothFtpServer;->getStatus()I

    move-result v1

    const/16 v2, 0x64

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings$2;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;

    invoke-static {v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;->access$300(Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings$2;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;

    invoke-static {v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;->access$300(Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings$2;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;

    invoke-static {v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;->access$100(Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;)Landroid/bluetooth/IBluetoothFtpServer;

    move-result-object v1

    invoke-interface {v1}, Landroid/bluetooth/IBluetoothFtpServer;->getPermission()I

    move-result v0

    const/16 v1, 0x3e9

    if-ne v0, v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings$2;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;

    invoke-static {v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;->access$400(Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;)Landroid/preference/ListPreference;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setValueIndex(I)V

    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings$2;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;

    invoke-static {v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;->access$400(Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;)Landroid/preference/ListPreference;

    move-result-object v1

    const v2, 0x7f0600a3

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(I)V

    :goto_0
    const-string v1, "FtpsAdvancedSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[BT][FTP] Init-value of FTP server permission: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings$2;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;

    invoke-static {v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;->access$400(Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;)Landroid/preference/ListPreference;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setValueIndex(I)V

    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings$2;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;

    invoke-static {v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;->access$400(Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;)Landroid/preference/ListPreference;

    move-result-object v1

    const v2, 0x7f0600a2

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3
    .param p1    # Landroid/content/ComponentName;

    const/4 v2, 0x0

    const-string v0, "FtpsAdvancedSettings"

    const-string v1, "[BT][FTP] Unexpectedly disconnected with BluetoothFtpService"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings$2;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;

    invoke-static {v0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;->access$300(Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings$2;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;

    invoke-static {v0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;->access$500(Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerAdvSettings;)Landroid/preference/PreferenceActivity;

    move-result-object v0

    const-string v1, "FTP Service disconnected unexpectedly."

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method
