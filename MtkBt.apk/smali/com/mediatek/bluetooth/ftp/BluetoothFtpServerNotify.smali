.class public Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;
.super Landroid/app/Activity;
.source "BluetoothFtpServerNotify.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "BluetoothFtpServerNotify"


# instance fields
.field private isDone:Z

.field private mCancelListener:Landroid/content/DialogInterface$OnCancelListener;

.field private mDeviceName:Ljava/lang/String;

.field private mDialog:Landroid/app/AlertDialog;

.field private mFilter:Landroid/content/IntentFilter;

.field private mFtpServerNotifyConn:Landroid/content/ServiceConnection;

.field private mNotifyType:I

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mServerNotify:Lcom/mediatek/bluetooth/ftp/IBluetoothFtpServerNotify;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;->isDone:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;->mServerNotify:Lcom/mediatek/bluetooth/ftp/IBluetoothFtpServerNotify;

    new-instance v0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify$1;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify$1;-><init>(Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;->mFtpServerNotifyConn:Landroid/content/ServiceConnection;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "server_disconnected"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;->mFilter:Landroid/content/IntentFilter;

    new-instance v0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify$2;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify$2;-><init>(Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify$3;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify$3;-><init>(Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;->mCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    return-void
.end method

.method static synthetic access$002(Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;Lcom/mediatek/bluetooth/ftp/IBluetoothFtpServerNotify;)Lcom/mediatek/bluetooth/ftp/IBluetoothFtpServerNotify;
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;
    .param p1    # Lcom/mediatek/bluetooth/ftp/IBluetoothFtpServerNotify;

    iput-object p1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;->mServerNotify:Lcom/mediatek/bluetooth/ftp/IBluetoothFtpServerNotify;

    return-object p1
.end method

.method static synthetic access$100(Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;->forceExit()V

    return-void
.end method

.method private buildDialog(ILjava/lang/String;)Landroid/app/AlertDialog;
    .locals 9
    .param p1    # I
    .param p2    # Ljava/lang/String;

    const/4 v7, 0x1

    const/4 v8, 0x0

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x0

    const v6, 0x5b8d81

    if-ne p1, v6, :cond_0

    const v1, 0x108009b

    const v5, 0x7f060057

    const v6, 0x7f060058

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-array v7, v7, [Ljava/lang/Object;

    aput-object p2, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const v4, 0x7f060059

    const v3, 0x7f06005a

    :goto_0
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v4, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v3, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    iget-object v7, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;->mCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    return-object v6

    :cond_0
    const v6, 0x5b8d82

    if-ne p1, v6, :cond_1

    const v1, 0x108009b

    const v5, 0x7f06005e

    const v6, 0x7f06005f

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-array v7, v7, [Ljava/lang/Object;

    aput-object p2, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const v4, 0x7f060094

    const v3, 0x7f060095

    goto :goto_0

    :cond_1
    const-string v6, "BluetoothFtpServerNotify"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Invalid notification type: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;->forceExit()V

    goto :goto_0
.end method

.method private forceExit()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;->isDone:Z

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method private sendAuthResult(Z)V
    .locals 3
    .param p1    # Z

    :try_start_0
    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;->mServerNotify:Lcom/mediatek/bluetooth/ftp/IBluetoothFtpServerNotify;

    invoke-interface {v1, p1}, Lcom/mediatek/bluetooth/ftp/IBluetoothFtpServerNotify;->authResult(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-direct {p0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;->forceExit()V

    return-void

    :catch_0
    move-exception v0

    const-string v1, "BluetoothFtpServerNotify"

    const-string v2, "authResult() failed."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private sendDisconnect()V
    .locals 3

    :try_start_0
    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;->mServerNotify:Lcom/mediatek/bluetooth/ftp/IBluetoothFtpServerNotify;

    invoke-interface {v1}, Lcom/mediatek/bluetooth/ftp/IBluetoothFtpServerNotify;->disconnect()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-direct {p0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;->forceExit()V

    return-void

    :catch_0
    move-exception v0

    const-string v1, "BluetoothFtpServerNotify"

    const-string v2, "disconnect() failed."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private updateNotification()V
    .locals 3

    :try_start_0
    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;->mServerNotify:Lcom/mediatek/bluetooth/ftp/IBluetoothFtpServerNotify;

    iget v2, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;->mNotifyType:I

    invoke-interface {v1, v2}, Lcom/mediatek/bluetooth/ftp/IBluetoothFtpServerNotify;->updateNotify(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "BluetoothFtpServerNotify"

    const-string v2, "updateNotification() failed."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const v2, 0x5b8d81

    packed-switch p2, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const-string v0, "BluetoothFtpServerNotify"

    const-string v1, "Positive button pressed."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;->mNotifyType:I

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;->sendAuthResult(Z)V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;->sendDisconnect()V

    goto :goto_0

    :pswitch_1
    const-string v0, "BluetoothFtpServerNotify"

    const-string v1, "Negative button pressed."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;->mNotifyType:I

    if-ne v0, v2, :cond_1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;->sendAuthResult(Z)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/4 v2, -0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "notify_type"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;->mNotifyType:I

    const-string v1, "device_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;->mDeviceName:Ljava/lang/String;

    iget v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;->mNotifyType:I

    if-ne v1, v2, :cond_0

    const-string v1, "BluetoothFtpServerNotify"

    const-string v2, "Notification type is not assigned."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;->forceExit()V

    :cond_0
    iget v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;->mNotifyType:I

    iget-object v2, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;->mDeviceName:Ljava/lang/String;

    invoke-direct {p0, v1, v2}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;->buildDialog(ILjava/lang/String;)Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;->mDialog:Landroid/app/AlertDialog;

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/mediatek/bluetooth/ftp/IBluetoothFtpServerNotify;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;->mFtpServerNotifyConn:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3}, Landroid/content/ContextWrapper;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;->mFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v1, v2}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    const-string v0, "BluetoothFtpServerNotify"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;->mFtpServerNotifyConn:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unbindService(Landroid/content/ServiceConnection;)V

    return-void
.end method

.method protected onPause()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    const-string v0, "BluetoothFtpServerNotify"

    const-string v1, "onPause()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    return-void
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const-string v0, "BluetoothFtpServerNotify"

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method protected onStop()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    const-string v0, "BluetoothFtpServerNotify"

    const-string v1, "onStop()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;->isDone:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpServerNotify;->updateNotification()V

    :cond_0
    return-void
.end method
