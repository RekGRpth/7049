.class Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$4;
.super Landroid/bluetooth/IBluetoothFtpCtrl$Stub;
.source "BluetoothFtpService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;


# direct methods
.method constructor <init>(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$4;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-direct {p0}, Landroid/bluetooth/IBluetoothFtpCtrl$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public connect(Landroid/bluetooth/BluetoothDevice;)V
    .locals 10
    .param p1    # Landroid/bluetooth/BluetoothDevice;

    const/16 v9, 0x13ed

    const/16 v5, 0x1389

    const/16 v8, 0xca

    const/4 v7, 0x0

    const/16 v6, 0xc9

    iget-object v3, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$4;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    const-string v4, "Launching FTP Client"

    invoke-static {v3, v4}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$300(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;Ljava/lang/String;)V

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$4;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v3, v9, p1, v6, v8}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$1600(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;ILandroid/bluetooth/BluetoothDevice;II)V

    iget-object v3, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$4;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v3}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$500(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)I

    move-result v3

    if-ne v3, v6, :cond_1

    iget-object v3, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$4;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    const v4, 0x5b8d83

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5, v7}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$1400(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;ILjava/lang/String;Z)Landroid/app/Notification;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$4;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v3}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$1500(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)Landroid/app/NotificationManager;

    move-result-object v3

    const v4, 0x5b8d83

    invoke-virtual {v3, v4, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v3, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$4;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    const-string v4, "/"

    invoke-static {v3, v4}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$3102(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;Ljava/lang/String;)Ljava/lang/String;

    iget-object v3, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$4;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v3, p1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$1802(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;

    iget-object v3, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$4;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-virtual {v3}, Landroid/content/ContextWrapper;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-class v4, Lcom/mediatek/bluetooth/ftp/BluetoothFtpClient;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    const/high16 v4, 0x10000000

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v3, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$4;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v3, v7}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$3202(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;Z)Z

    const/4 v1, 0x1

    iget-object v3, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$4;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-virtual {v3, v0}, Landroid/content/ContextWrapper;->startActivity(Landroid/content/Intent;)V

    :goto_0
    if-nez v1, :cond_0

    iget-object v3, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$4;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v3, v9, p1, v8, v6}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$1600(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;ILandroid/bluetooth/BluetoothDevice;II)V

    :cond_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$4;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v3}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$500(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)I

    move-result v3

    const/16 v4, 0xc8

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$4;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    const-string v4, "FTP client is not available."

    invoke-static {v3, v4}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$000(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$4;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    const v4, 0x7f06006b

    invoke-static {v3, v5, v4}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$800(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;II)V

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$4;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    const v4, 0x7f06009a

    invoke-static {v3, v5, v4}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$800(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;II)V

    goto :goto_0
.end method

.method public disconnect(Landroid/bluetooth/BluetoothDevice;)V
    .locals 2
    .param p1    # Landroid/bluetooth/BluetoothDevice;

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$4;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$3202(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;Z)Z

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$4;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$3300(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)V

    return-void
.end method

.method public getCurrentDevice()Landroid/bluetooth/BluetoothDevice;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$4;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$1800(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    return-object v0
.end method

.method public getState()I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$4;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$500(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)I

    move-result v0

    return v0
.end method
