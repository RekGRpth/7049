.class Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;
.super Lcom/mediatek/bluetooth/ftp/IBluetoothFtpClient$Stub;
.source "BluetoothFtpService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;


# direct methods
.method constructor <init>(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/ftp/IBluetoothFtpClient$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public abort()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$500(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)I

    move-result v1

    const/16 v2, 0xcc

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$500(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)I

    move-result v1

    const/16 v2, 0xcd

    if-ne v1, v2, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    const/16 v2, 0xd1

    invoke-static {v1, v2}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$502(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;I)I

    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    const/16 v2, 0x139e

    invoke-static {v1, v2, v0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$800(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;II)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const-string v1, "BluetoothFtpService"

    const-string v2, "[BT][FTP] Client aborts without trnasferring."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public connect()V
    .locals 3

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$1800(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$500(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)I

    move-result v0

    const/16 v1, 0xc9

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Connect to: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v2}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$1800(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$300(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    const/16 v1, 0xca

    invoke-static {v0, v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$502(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;I)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$1800(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$4100(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    const-string v1, "Invalid connect request: no server device or connected already."

    invoke-static {v0, v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$000(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    const/16 v1, 0x31

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$4200(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;II)I

    goto :goto_0
.end method

.method public createFolder(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$500(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)I

    move-result v0

    const/16 v1, 0xcb

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    const/16 v1, 0xcf

    invoke-static {v0, v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$502(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;I)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$3100(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$4602(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v0, p1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$5000(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "createFolder() in wrong state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v2}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$500(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$4500(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public delete(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$500(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)I

    move-result v0

    const/16 v1, 0xcb

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    const/16 v1, 0xcf

    invoke-static {v0, v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$502(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;I)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v0, p1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$5200(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "delete() in wrong state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v2}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$500(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$4500(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public disconnect()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$3202(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;Z)Z

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$500(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)I

    move-result v0

    const/16 v1, 0xca

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$1500(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)Landroid/app/NotificationManager;

    move-result-object v0

    const v1, 0x5b8d83

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$4300(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$3300(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)V

    goto :goto_0
.end method

.method public getCurrentPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$3100(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLastTransferResult()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public getState()I
    .locals 3

    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$500(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    const/16 v2, 0xcb

    invoke-static {v1, v2}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$502(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;I)I

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    const/16 v2, 0xc9

    invoke-static {v1, v2}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$502(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;I)I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0xce
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public goBackward()V
    .locals 4

    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$500(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)I

    move-result v1

    const/16 v2, 0xcb

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    const/16 v2, 0xcf

    invoke-static {v1, v2}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$502(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;I)I

    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$3100(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    iget-object v2, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$3100(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v1, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-static {v2, v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$4602(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;Ljava/lang/String;)Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "goBackward(): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v3}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$4600(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$300(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$4800(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)V

    :goto_1
    return-void

    :cond_0
    const-string v1, "/"

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "goBackward() in wrong state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v3}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$500(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$4500(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public goForward(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$500(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)I

    move-result v0

    const/16 v1, 0xcb

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    const/16 v1, 0xcf

    invoke-static {v0, v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$502(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;I)I

    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v2}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$3100(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$3100(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p1

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$4602(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "goForward(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v2}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$4600(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$300(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v0, p1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$4700(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "goForward() in wrong state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v2}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$500(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$4500(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public goToRoot()V
    .locals 3

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$500(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)I

    move-result v0

    const/16 v1, 0xcb

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    const/16 v1, 0xcf

    invoke-static {v0, v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$502(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;I)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    const-string v1, "/"

    invoke-static {v0, v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$4602(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "goToRoot(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v2}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$4600(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$300(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$4900(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "goToRoot() in wrong state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v2}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$500(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$4500(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public refresh()V
    .locals 3

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$500(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)I

    move-result v0

    const/16 v1, 0xcb

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$500(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)I

    move-result v0

    const/16 v1, 0xce

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    const/16 v1, 0xcf

    invoke-static {v0, v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$502(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;I)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$4400(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "refresh() in wrong state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v2}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$500(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$4500(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public registerCallback(Lcom/mediatek/bluetooth/ftp/IBluetoothFtpClientCallback;)V
    .locals 1
    .param p1    # Lcom/mediatek/bluetooth/ftp/IBluetoothFtpClientCallback;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$4000(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)Landroid/os/RemoteCallbackList;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    :cond_0
    return-void
.end method

.method public startPull()V
    .locals 3

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$500(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)I

    move-result v0

    const/16 v1, 0xcb

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    const/16 v1, 0xcd

    invoke-static {v0, v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$502(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;I)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    const/16 v1, 0x13a4

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$5100(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;ILandroid/os/Bundle;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "startPull() in wrong state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v2}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$500(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$4500(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public startPush()V
    .locals 3

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$500(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)I

    move-result v0

    const/16 v1, 0xcb

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    const/16 v1, 0xcc

    invoke-static {v0, v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$502(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;I)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    const/16 v1, 0x13a5

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$5100(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;ILandroid/os/Bundle;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "startPush() in wrong state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v2}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$500(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$4500(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public unregisterCallback(Lcom/mediatek/bluetooth/ftp/IBluetoothFtpClientCallback;)V
    .locals 1
    .param p1    # Lcom/mediatek/bluetooth/ftp/IBluetoothFtpClientCallback;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$8;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$4000(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)Landroid/os/RemoteCallbackList;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    :cond_0
    return-void
.end method
