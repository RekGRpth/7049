.class Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$SocketListenerThread;
.super Ljava/lang/Thread;
.source "BluetoothFtpService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SocketListenerThread"
.end annotation


# instance fields
.field private init_ok:Z

.field final synthetic this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;


# direct methods
.method public constructor <init>(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)V
    .locals 2

    iput-object p1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$SocketListenerThread;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    const-string v0, "BtFtpMessageListener"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$SocketListenerThread;->init_ok:Z

    invoke-static {p1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$2800(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$SocketListenerThread;->init_ok:Z

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "After preparing, init_ok: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$SocketListenerThread;->init_ok:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$300(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$SocketListenerThread;->init_ok:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$SocketListenerThread;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$2900(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)Z

    move-result v0

    :cond_0
    iget-object v1, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$SocketListenerThread;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SocketListener exited. job_done: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$300(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;Ljava/lang/String;)V

    return-void
.end method

.method public shutdown()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$SocketListenerThread;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    const-string v1, "Shutdown socketListener."

    invoke-static {v0, v1}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$300(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService$SocketListenerThread;->this$0:Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;->access$3000(Lcom/mediatek/bluetooth/ftp/BluetoothFtpService;)V

    return-void
.end method
