.class Lcom/mediatek/bluetooth/ftp/GetLocalContentThread;
.super Ljava/lang/Thread;
.source "BluetoothFtpLocalBrowser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/bluetooth/ftp/GetLocalContentThread$ResultListener;
    }
.end annotation


# static fields
.field private static final DEFAULT_ROOT:Ljava/lang/String; = "/mnt"

.field protected static final RESULT_FAIL:I = 0x1

.field protected static final RESULT_SUCCEED:I = 0x0

.field private static final SDCARD:Ljava/lang/String; = "sdcard"

.field private static final TAG:Ljava/lang/String; = "GetLocalContentThread"


# instance fields
.field private mListener:Lcom/mediatek/bluetooth/ftp/GetLocalContentThread$ResultListener;

.field private mPath:Ljava/io/File;

.field private mResolver:Landroid/content/ContentResolver;

.field private mRoot:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/content/ContentResolver;Lcom/mediatek/bluetooth/ftp/GetLocalContentThread$ResultListener;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/content/ContentResolver;
    .param p3    # Lcom/mediatek/bluetooth/ftp/GetLocalContentThread$ResultListener;

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput-object v0, p0, Lcom/mediatek/bluetooth/ftp/GetLocalContentThread;->mPath:Ljava/io/File;

    iput-object v0, p0, Lcom/mediatek/bluetooth/ftp/GetLocalContentThread;->mResolver:Landroid/content/ContentResolver;

    iput-object v0, p0, Lcom/mediatek/bluetooth/ftp/GetLocalContentThread;->mListener:Lcom/mediatek/bluetooth/ftp/GetLocalContentThread$ResultListener;

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/ftp/GetLocalContentThread;->mPath:Ljava/io/File;

    iput-object p2, p0, Lcom/mediatek/bluetooth/ftp/GetLocalContentThread;->mResolver:Landroid/content/ContentResolver;

    iput-object p3, p0, Lcom/mediatek/bluetooth/ftp/GetLocalContentThread;->mListener:Lcom/mediatek/bluetooth/ftp/GetLocalContentThread$ResultListener;

    invoke-static {}, Lcom/mediatek/bluetooth/util/SystemUtils;->getMountPointPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/bluetooth/ftp/GetLocalContentThread;->mRoot:Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/GetLocalContentThread;->mRoot:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "/mnt"

    iput-object v0, p0, Lcom/mediatek/bluetooth/ftp/GetLocalContentThread;->mRoot:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method private declared-synchronized postResult(I)V
    .locals 1
    .param p1    # I

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/GetLocalContentThread;->mListener:Lcom/mediatek/bluetooth/ftp/GetLocalContentThread$ResultListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/GetLocalContentThread;->mListener:Lcom/mediatek/bluetooth/ftp/GetLocalContentThread$ResultListener;

    invoke-interface {v0, p1}, Lcom/mediatek/bluetooth/ftp/GetLocalContentThread$ResultListener;->onThreadResult(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public declared-synchronized isDone(Lcom/mediatek/bluetooth/ftp/GetLocalContentThread$ResultListener;)Z
    .locals 2
    .param p1    # Lcom/mediatek/bluetooth/ftp/GetLocalContentThread$ResultListener;

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Thread;->getState()Ljava/lang/Thread$State;

    move-result-object v0

    sget-object v1, Ljava/lang/Thread$State;->TERMINATED:Ljava/lang/Thread$State;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iput-object p1, p0, Lcom/mediatek/bluetooth/ftp/GetLocalContentThread;->mListener:Lcom/mediatek/bluetooth/ftp/GetLocalContentThread$ResultListener;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized removeListener()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/mediatek/bluetooth/ftp/GetLocalContentThread;->mListener:Lcom/mediatek/bluetooth/ftp/GetLocalContentThread$ResultListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public run()V
    .locals 19

    new-instance v14, Landroid/content/ContentValues;

    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    const/4 v5, 0x0

    const/4 v9, 0x0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/mediatek/bluetooth/ftp/GetLocalContentThread;->mPath:Ljava/io/File;

    invoke-virtual {v15}, Ljava/io/File;->isDirectory()Z

    move-result v15

    if-eqz v15, :cond_7

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/mediatek/bluetooth/ftp/GetLocalContentThread;->mResolver:Landroid/content/ContentResolver;

    sget-object v16, Lcom/mediatek/bluetooth/ftp/BluetoothFtpProviderHelper$FolderContent;->LOCAL_URI:Landroid/net/Uri;

    const/16 v17, 0x0

    const/16 v18, 0x0

    invoke-virtual/range {v15 .. v18}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/mediatek/bluetooth/ftp/GetLocalContentThread;->mPath:Ljava/io/File;

    invoke-virtual {v15}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/mediatek/bluetooth/ftp/GetLocalContentThread;->mPath:Ljava/io/File;

    invoke-virtual {v15}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    if-nez v5, :cond_0

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/mediatek/bluetooth/ftp/GetLocalContentThread;->postResult(I)V

    :goto_0
    return-void

    :cond_0
    move-object v1, v5

    array-length v7, v1

    const/4 v6, 0x0

    :goto_1
    if-ge v6, v7, :cond_3

    aget-object v4, v1, v6

    const/4 v13, -0x1

    const-wide/16 v10, 0x0

    invoke-virtual {v14}, Landroid/content/ContentValues;->clear()V

    invoke-virtual {v4}, Ljava/io/File;->isHidden()Z

    move-result v15

    if-eqz v15, :cond_2

    :cond_1
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {v4}, Ljava/io/File;->isFile()Z

    move-result v15

    if-eqz v15, :cond_4

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/mediatek/bluetooth/ftp/GetLocalContentThread;->mRoot:Ljava/lang/String;

    invoke-virtual {v15, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_1

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpProviderHelper;->getTypeCode(Ljava/lang/String;)I

    move-result v13

    invoke-virtual {v4}, Ljava/io/File;->lastModified()J

    move-result-wide v15

    invoke-static/range {v15 .. v16}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->getDateString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v10

    const-string v15, "name"

    invoke-virtual {v14, v15, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "type"

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v15, "modified"

    invoke-virtual {v14, v15, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "size"

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :goto_3
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/mediatek/bluetooth/ftp/GetLocalContentThread;->mResolver:Landroid/content/ContentResolver;

    sget-object v16, Lcom/mediatek/bluetooth/ftp/BluetoothFtpProviderHelper$FolderContent;->LOCAL_URI:Landroid/net/Uri;

    move-object/from16 v0, v16

    invoke-virtual {v15, v0, v14}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v3

    const-string v15, "GetLocalContentThread"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "[BT][FTP] Failed to get local folder content. Exception: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v9, 0x1

    :cond_3
    :goto_4
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/mediatek/bluetooth/ftp/GetLocalContentThread;->postResult(I)V

    goto/16 :goto_0

    :cond_4
    :try_start_1
    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v15

    if-eqz v15, :cond_6

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    const/16 v13, 0xa

    invoke-virtual {v4}, Ljava/io/File;->lastModified()J

    move-result-wide v15

    invoke-static/range {v15 .. v16}, Lcom/mediatek/bluetooth/ftp/BluetoothFtpLocalBrowser;->getDateString(J)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/mediatek/bluetooth/ftp/GetLocalContentThread;->mRoot:Ljava/lang/String;

    invoke-virtual {v15, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_5

    const-string v15, "name"

    invoke-virtual {v14, v15, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "type"

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v15, "modified"

    invoke-virtual {v14, v15, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_5
    if-eqz v8, :cond_1

    const-string v15, "sdcard"

    invoke-virtual {v8, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_1

    const-string v15, "name"

    invoke-virtual {v14, v15, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v15, "type"

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v15, "modified"

    invoke-virtual {v14, v15, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_6
    const-string v15, "GetLocalContentThread"

    const-string v16, "Unknown Type"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_7
    const-string v15, "GetLocalContentThread"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "[BT][FTP] The path is not a directory: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/ftp/GetLocalContentThread;->mPath:Ljava/io/File;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    const/4 v9, 0x1

    goto/16 :goto_4
.end method
