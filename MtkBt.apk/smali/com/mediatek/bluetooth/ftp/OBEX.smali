.class Lcom/mediatek/bluetooth/ftp/OBEX;
.super Ljava/lang/Object;
.source "BluetoothFtpClient.java"


# static fields
.field public static final BAD_GATEWAY:I = 0x52

.field public static final BAD_REQUEST:I = 0x40

.field public static final CONFLICT:I = 0x49

.field public static final FORBIDDEN:I = 0x43

.field public static final GATEWAY_TIMEOUT:I = 0x54

.field public static final GONE:I = 0x4a

.field public static final HTTP_VER_NO_SUPPORT:I = 0x55

.field public static final INTERNAL_SERVER_ERR:I = 0x50

.field public static final LENGTH_REQUIRED:I = 0x4b

.field public static final METHOD_NOT_ALLOWED:I = 0x45

.field public static final NOT_ACCEPTABLE:I = 0x46

.field public static final NOT_FOUND:I = 0x44

.field public static final NOT_IMPLEMENTED:I = 0x51

.field public static final PAYMENT_REQUIRED:I = 0x42

.field public static final PRECONDITION_FAILED:I = 0x4c

.field public static final PROXY_AUTHEN_REQ:I = 0x47

.field public static final REQUEST_TIME_OUT:I = 0x48

.field public static final REQ_ENTITY_TOO_LARGE:I = 0x4d

.field public static final REQ_URL_TOO_LARGE:I = 0x4e

.field public static final SERVICE_UNAVAILABLE:I = 0x53

.field public static final STATUS_FAILED:I = 0x1

.field public static final UNAUTHORIZED:I = 0x41

.field public static final UNSUPPORT_MEDIA_TYPE:I = 0x4f

.field public static final USER_CANCEL:I = 0x70

.field public static final USER_UNKNOW:I = 0x71


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
