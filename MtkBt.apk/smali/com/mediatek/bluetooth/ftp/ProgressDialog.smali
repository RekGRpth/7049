.class public Lcom/mediatek/bluetooth/ftp/ProgressDialog;
.super Landroid/app/AlertDialog;
.source "ProgressDialog.java"


# static fields
.field public static final MODE_BASE:I = 0x0

.field public static final MODE_MULTIPLE:I = 0x4

.field public static final MODE_SIMPLE:I = 0x1

.field public static final MODE_SINGLE_PERCENTAGE:I = 0x3

.field public static final MODE_SINGLE_TRANSFERRED:I = 0x2

.field private static final TAG:Ljava/lang/String; = "ProgressDialog"


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private mIndeterminate:Z

.field private mMax:I

.field private mMessage:Ljava/lang/String;

.field private mMessageView:Landroid/widget/TextView;

.field private mMode:I

.field private mProgress:Landroid/widget/ProgressBar;

.field private mProgressText:Landroid/widget/TextView;

.field private mProgressValue:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    iput v0, p0, Lcom/mediatek/bluetooth/ftp/ProgressDialog;->mMode:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/bluetooth/ftp/ProgressDialog;->mMessage:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;I)V

    const/4 v0, 0x1

    iput v0, p0, Lcom/mediatek/bluetooth/ftp/ProgressDialog;->mMode:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/bluetooth/ftp/ProgressDialog;->mMessage:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/bluetooth/ftp/ProgressDialog;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/ftp/ProgressDialog;

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/ProgressDialog;->mProgress:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/bluetooth/ftp/ProgressDialog;)J
    .locals 2
    .param p0    # Lcom/mediatek/bluetooth/ftp/ProgressDialog;

    iget-wide v0, p0, Lcom/mediatek/bluetooth/ftp/ProgressDialog;->mProgressValue:J

    return-wide v0
.end method

.method static synthetic access$200(Lcom/mediatek/bluetooth/ftp/ProgressDialog;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/ftp/ProgressDialog;

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/ProgressDialog;->mProgressText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/bluetooth/ftp/ProgressDialog;)I
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/ftp/ProgressDialog;

    iget v0, p0, Lcom/mediatek/bluetooth/ftp/ProgressDialog;->mMode:I

    return v0
.end method

.method static synthetic access$400(Lcom/mediatek/bluetooth/ftp/ProgressDialog;)I
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/ftp/ProgressDialog;

    iget v0, p0, Lcom/mediatek/bluetooth/ftp/ProgressDialog;->mMax:I

    return v0
.end method

.method static synthetic access$500(Lcom/mediatek/bluetooth/ftp/ProgressDialog;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/ftp/ProgressDialog;

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/ProgressDialog;->mMessageView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/mediatek/bluetooth/ftp/ProgressDialog;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/ftp/ProgressDialog;

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/ProgressDialog;->mMessage:Ljava/lang/String;

    return-object v0
.end method

.method private updateUI()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/ProgressDialog;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/ProgressDialog;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    const v6, 0x7f080019

    const/high16 v5, 0x7f080000

    const/4 v4, 0x0

    new-instance v2, Lcom/mediatek/bluetooth/ftp/ProgressDialog$1;

    invoke-direct {v2, p0}, Lcom/mediatek/bluetooth/ftp/ProgressDialog$1;-><init>(Lcom/mediatek/bluetooth/ftp/ProgressDialog;)V

    iput-object v2, p0, Lcom/mediatek/bluetooth/ftp/ProgressDialog;->mHandler:Landroid/os/Handler;

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget v2, p0, Lcom/mediatek/bluetooth/ftp/ProgressDialog;->mMode:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    const v2, 0x7f030014

    invoke-virtual {v0, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, p0, Lcom/mediatek/bluetooth/ftp/ProgressDialog;->mProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/mediatek/bluetooth/ftp/ProgressDialog;->mMessageView:Landroid/widget/TextView;

    iput-object v4, p0, Lcom/mediatek/bluetooth/ftp/ProgressDialog;->mProgressText:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    :goto_0
    iget-boolean v2, p0, Lcom/mediatek/bluetooth/ftp/ProgressDialog;->mIndeterminate:Z

    invoke-virtual {p0, v2}, Lcom/mediatek/bluetooth/ftp/ProgressDialog;->setIndeterminate(Z)V

    invoke-direct {p0}, Lcom/mediatek/bluetooth/ftp/ProgressDialog;->updateUI()V

    invoke-super {p0, p1}, Landroid/app/AlertDialog;->onCreate(Landroid/os/Bundle;)V

    return-void

    :cond_0
    const v2, 0x7f030013

    invoke-virtual {v0, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, p0, Lcom/mediatek/bluetooth/ftp/ProgressDialog;->mProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/mediatek/bluetooth/ftp/ProgressDialog;->mMessageView:Landroid/widget/TextView;

    const v2, 0x7f080008

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/mediatek/bluetooth/ftp/ProgressDialog;->mProgressText:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public setIndeterminate(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/ProgressDialog;->mProgress:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/ftp/ProgressDialog;->mProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    :cond_0
    iput-boolean p1, p0, Lcom/mediatek/bluetooth/ftp/ProgressDialog;->mIndeterminate:Z

    return-void
.end method

.method public setMax(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/bluetooth/ftp/ProgressDialog;->mMax:I

    invoke-direct {p0}, Lcom/mediatek/bluetooth/ftp/ProgressDialog;->updateUI()V

    return-void
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/bluetooth/ftp/ProgressDialog;->mMessage:Ljava/lang/String;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/ftp/ProgressDialog;->updateUI()V

    return-void
.end method

.method public setProgress(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/bluetooth/ftp/ProgressDialog;->mProgressValue:J

    invoke-direct {p0}, Lcom/mediatek/bluetooth/ftp/ProgressDialog;->updateUI()V

    return-void
.end method

.method public setProgressMode(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/bluetooth/ftp/ProgressDialog;->mMode:I

    return-void
.end method
