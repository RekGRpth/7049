.class Lcom/mediatek/bluetooth/hid/BluetoothHidActivity$2;
.super Landroid/content/BroadcastReceiver;
.source "BluetoothHidActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/bluetooth/hid/BluetoothHidActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidActivity;


# direct methods
.method constructor <init>(Lcom/mediatek/bluetooth/hid/BluetoothHidActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidActivity$2;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v8, 0x1

    const-string v6, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const-string v6, "android.bluetooth.adapter.extra.STATE"

    const/high16 v7, -0x80000000

    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const-string v6, "[BT][HID][BluetoothHidActivity]"

    const-string v7, "hid activity receiver receives BT OFF intent"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidActivity$2;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidActivity;

    invoke-static {v6}, Lcom/mediatek/bluetooth/hid/BluetoothHidActivity;->access$300(Lcom/mediatek/bluetooth/hid/BluetoothHidActivity;)V

    :try_start_0
    invoke-static {}, Lcom/mediatek/bluetooth/hid/BluetoothHidActivity;->access$200()Z

    move-result v6

    if-ne v6, v8, :cond_1

    const/4 v6, 0x0

    invoke-static {v6}, Lcom/mediatek/bluetooth/hid/BluetoothHidActivity;->access$202(Z)Z

    const-string v6, "[BT][HID][BluetoothHidActivity]"

    const-string v7, "mReceiver,unbindservice"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidActivity$2;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidActivity;

    iget-object v6, v6, Lcom/mediatek/bluetooth/hid/BluetoothHidActivity;->ct:Landroid/content/Context;

    iget-object v7, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidActivity$2;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidActivity;

    invoke-static {v7}, Lcom/mediatek/bluetooth/hid/BluetoothHidActivity;->access$400(Lcom/mediatek/bluetooth/hid/BluetoothHidActivity;)Landroid/content/ServiceConnection;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    invoke-static {}, Lcom/mediatek/bluetooth/hid/BluetoothHidActivity;->access$500()Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidActivity$2;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidActivity;

    invoke-virtual {v6}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :catch_0
    move-exception v2

    const-string v6, "[BT][HID][BluetoothHidActivity]"

    const-string v7, "mReceiver,unbindservice error"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidActivity$2;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidActivity;

    invoke-virtual {v6}, Landroid/app/Activity;->finish()V

    goto :goto_1

    :cond_2
    const-string v6, "com.mediatek.bluetooth.BluetoothHidActivity.ACTION_SUMMARY_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    const-string v6, "[BT][HID][BluetoothHidActivity]"

    const-string v7, "Update summary"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x0

    const-string v6, "com.mediatek.bluetooth.BluetoothHidActivity.extra.EXTRA_DEVICE"

    invoke-virtual {p2, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v6, "com.mediatek.bluetooth.BluetoothHidActivity.extra.EXTRA_SUMMARY"

    const/4 v7, -0x1

    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    const-string v6, "com.mediatek.bluetooth.BluetoothHidActivity.extra.EXTRA_ENABLE"

    invoke-virtual {p2, v6, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    sget-object v6, Lcom/mediatek/bluetooth/hid/BluetoothHidActivity;->mDeviceList:Landroid/preference/PreferenceCategory;

    if-eqz v6, :cond_0

    sget-object v6, Lcom/mediatek/bluetooth/hid/BluetoothHidActivity;->mDeviceList:Landroid/preference/PreferenceCategory;

    invoke-virtual {v6, v0}, Landroid/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v4, v5}, Landroid/preference/Preference;->setSummary(I)V

    invoke-virtual {v4, v3}, Landroid/preference/Preference;->setEnabled(Z)V

    goto :goto_0

    :cond_3
    const-string v6, "com.mediatek.bluetooth.BluetoothHidActivity.ACTION_DEVICE_ADDED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const-string v6, "[BT][HID][BluetoothHidActivity]"

    const-string v7, "New device added"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidActivity$2;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidActivity;

    invoke-static {v6}, Lcom/mediatek/bluetooth/hid/BluetoothHidActivity;->access$100(Lcom/mediatek/bluetooth/hid/BluetoothHidActivity;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0xd
        :pswitch_0
    .end packed-switch
.end method
