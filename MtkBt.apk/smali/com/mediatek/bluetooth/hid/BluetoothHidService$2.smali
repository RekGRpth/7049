.class Lcom/mediatek/bluetooth/hid/BluetoothHidService$2;
.super Lcom/mediatek/bluetooth/hid/IBluetoothHidServerNotify$Stub;
.source "BluetoothHidService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/bluetooth/hid/BluetoothHidService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;


# direct methods
.method constructor <init>(Lcom/mediatek/bluetooth/hid/BluetoothHidService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$2;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/hid/IBluetoothHidServerNotify$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public activateReq()V
    .locals 2

    const-string v0, "[BT][HID][BluetoothHidService]"

    const-string v1, "BluetoothHidServer Activate: "

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$2;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$800(Lcom/mediatek/bluetooth/hid/BluetoothHidService;)V

    return-void
.end method

.method public authorizeReq(Ljava/lang/String;Z)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "[BT][HID][BluetoothHidService]"

    const-string v1, "BluetoothHidServer authorizeReq"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$2;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v0, p1, p2}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$2100(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;Z)V

    return-void
.end method

.method public clearService()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "[BT][HID][BluetoothHidService]"

    const-string v1, "BluetoothHidServer clearService"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$2;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-virtual {v0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->localClearService()V

    return-void
.end method

.method public connectReq(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v0, "[BT][HID][BluetoothHidService]"

    const-string v1, "BluetoothHidServer Connect"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$2;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v0, p1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$600(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;)V

    return-void
.end method

.method public deactivateReq()V
    .locals 2

    const-string v0, "[BT][HID][BluetoothHidService]"

    const-string v1, "BluetoothHidServer DeactivateReq"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$2;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$900(Lcom/mediatek/bluetooth/hid/BluetoothHidService;)V

    return-void
.end method

.method public disconnectReq(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v0, "[BT][HID][BluetoothHidService]"

    const-string v1, "BluetoothHidServer Disconnect"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$2;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v0, p1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$700(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;)V

    return-void
.end method

.method public finishActionReq()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "[BT][HID][BluetoothHidService]"

    const-string v2, "BluetoothHidServer finishActionReq"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.mediatek.bluetooth.hid.finish"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$2;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-virtual {v1, v0}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method public getIdleReq(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v0, "[BT][HID][BluetoothHidService]"

    const-string v1, "BluetoothHidServer getIdle"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$2;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v0, p1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$2000(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;)V

    return-void
.end method

.method public getProtocolReq(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v0, "[BT][HID][BluetoothHidService]"

    const-string v1, "BluetoothHidServer getProtocol"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$2;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v0, p1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$1800(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;)V

    return-void
.end method

.method public getReportReq(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v0, "[BT][HID][BluetoothHidService]"

    const-string v1, "BluetoothHidServer getReport"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$2;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v0, p1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$1600(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;)V

    return-void
.end method

.method public getStateByAddr(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$2;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v0, p1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$500(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public sendReportReq(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v0, "[BT][HID][BluetoothHidService]"

    const-string v1, "BluetoothHidServer sendReport"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$2;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v0, p1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$1400(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;)V

    return-void
.end method

.method public setIdleReq(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v0, "[BT][HID][BluetoothHidService]"

    const-string v1, "BluetoothHidServer setIdle"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$2;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v0, p1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$1900(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;)V

    return-void
.end method

.method public setProtocolReq(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v0, "[BT][HID][BluetoothHidService]"

    const-string v1, "BluetoothHidServer setProtocol"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$2;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v0, p1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$1700(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;)V

    return-void
.end method

.method public setReportReq(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v0, "[BT][HID][BluetoothHidService]"

    const-string v1, "BluetoothHidServer setReport"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$2;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v0, p1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$1500(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;)V

    return-void
.end method

.method public unplugReq(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const-string v0, "[BT][HID][BluetoothHidService]"

    const-string v1, "BluetoothHidServer unplug"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$2;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    const v1, 0x7f0600c6

    const/4 v2, 0x0

    invoke-static {v0, p1, v1, v2}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$1000(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$2;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    const-string v1, "disconnecting"

    iget-object v2, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$2;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v2, p1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$1100(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$1200(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;Landroid/bluetooth/BluetoothDevice;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$2;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v0, p1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$1300(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;)V

    return-void
.end method
