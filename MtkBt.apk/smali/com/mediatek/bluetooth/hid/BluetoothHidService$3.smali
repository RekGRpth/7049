.class Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;
.super Landroid/os/Handler;
.source "BluetoothHidService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/bluetooth/hid/BluetoothHidService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;


# direct methods
.method constructor <init>(Lcom/mediatek/bluetooth/hid/BluetoothHidService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 14
    .param p1    # Landroid/os/Message;

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    const-string v0, "[BT][HID][BluetoothHidService]"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "handleMessage(): "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p1, Landroid/os/Message;->what:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v7

    if-eqz v7, :cond_5

    const-string v0, "device_addr"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :goto_0
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2}, Ljava/lang/String;-><init>()V

    const/4 v11, 0x0

    if-eqz v3, :cond_1

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {}, Lcom/mediatek/bluetooth/hid/BluetoothHidActivity;->getDeviceList()Landroid/preference/PreferenceCategory;

    move-result-object v4

    iput-object v4, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->pc:Landroid/preference/PreferenceCategory;

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v0, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->pc:Landroid/preference/PreferenceCategory;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v4, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v4, v4, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->pc:Landroid/preference/PreferenceCategory;

    invoke-virtual {v4, v3}, Landroid/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    iput-object v4, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mPreference:Landroid/preference/Preference;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v0, v3}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$1100(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v10

    if-eqz v10, :cond_6

    invoke-virtual {v10}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v0, v3}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$2300(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :cond_1
    :goto_1
    iget v0, p1, Landroid/os/Message;->what:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    sparse-switch v0, :sswitch_data_0

    :cond_2
    :goto_2
    const/4 v0, 0x4

    iget v4, p1, Landroid/os/Message;->what:I

    if-eq v0, v4, :cond_3

    const/4 v0, 0x5

    iget v4, p1, Landroid/os/Message;->what:I

    if-eq v0, v4, :cond_3

    const/4 v0, 0x6

    iget v4, p1, Landroid/os/Message;->what:I

    if-eq v0, v4, :cond_3

    const/4 v0, 0x7

    iget v4, p1, Landroid/os/Message;->what:I

    if-eq v0, v4, :cond_3

    const/16 v0, 0xa

    iget v4, p1, Landroid/os/Message;->what:I

    if-eq v0, v4, :cond_3

    const/16 v0, 0xb

    iget v4, p1, Landroid/os/Message;->what:I

    if-ne v0, v4, :cond_4

    :cond_3
    new-instance v9, Landroid/content/Intent;

    const-string v0, "com.mediatek.bluetooth.hid.finish"

    invoke-direct {v9, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-virtual {v0, v9}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    :cond_4
    return-void

    :cond_5
    const/4 v3, 0x0

    goto :goto_0

    :cond_6
    :try_start_1
    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v0, v3}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$2300(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v0, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mPreference:Landroid/preference/Preference;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v0, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mPreference:Landroid/preference/Preference;

    invoke-virtual {v0}, Landroid/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :sswitch_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    const/16 v4, 0x64

    invoke-static {v0, v4}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$2402(Lcom/mediatek/bluetooth/hid/BluetoothHidService;I)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v0, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->update_state_intent:Landroid/content/Intent;

    const-string v4, "android.bluetooth.profilemanager.extra.EXTRA_NEW_STATE"

    const/16 v5, 0xb

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v4, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v4, v4, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->update_state_intent:Landroid/content/Intent;

    invoke-virtual {v0, v4}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    new-instance v6, Landroid/content/Intent;

    const-string v0, "android.bluetooth.input.profile.action.ACTION_BIND_SERVICE"

    invoke-direct {v6, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-virtual {v0, v6}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :catch_0
    move-exception v8

    const-string v0, "[BT][HID][BluetoothHidService]"

    const-string v4, "hid stateMap error"

    invoke-static {v0, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v8}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_2

    :sswitch_1
    :try_start_2
    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v0, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->update_state_intent:Landroid/content/Intent;

    const-string v4, "android.bluetooth.profilemanager.extra.EXTRA_NEW_STATE"

    const/16 v5, 0xe

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v4, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v4, v4, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->update_state_intent:Landroid/content/Intent;

    invoke-virtual {v0, v4}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_2
    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    const/16 v4, 0x68

    invoke-static {v0, v4}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$2402(Lcom/mediatek/bluetooth/hid/BluetoothHidService;I)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v0, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->update_state_intent:Landroid/content/Intent;

    const-string v4, "android.bluetooth.profilemanager.extra.EXTRA_NEW_STATE"

    const/16 v5, 0xd

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v4, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v4, v4, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->update_state_intent:Landroid/content/Intent;

    invoke-virtual {v0, v4}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_3
    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v0, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->update_state_intent:Landroid/content/Intent;

    const-string v4, "android.bluetooth.profilemanager.extra.EXTRA_NEW_STATE"

    const/16 v5, 0xe

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v4, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v4, v4, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->update_state_intent:Landroid/content/Intent;

    invoke-virtual {v0, v4}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_4
    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v0, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->cx:Landroid/content/Context;

    iget-object v4, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    const v5, 0x7f0600bd

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v2, v12, v13

    invoke-virtual {v4, v5, v12}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v0, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    const/16 v4, 0x66

    invoke-static {v0, v4}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$2402(Lcom/mediatek/bluetooth/hid/BluetoothHidService;I)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    const-string v4, "connected"

    iget-object v5, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v5, v3}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$1100(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v5

    invoke-static {v0, v4, v5}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$1200(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;Landroid/bluetooth/BluetoothDevice;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    const-string v4, "connected"

    invoke-static {v0, v3, v4}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$2500(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    const v4, 0x7f0600b2

    const/4 v5, 0x1

    invoke-static {v0, v3, v4, v5}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$1000(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v0, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->notifyMap:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v0, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->notifyMap:Ljava/util/Map;

    invoke-static {}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$2600()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$2608()I

    :cond_7
    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v0, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->notifyMap:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    const-string v4, "connected"

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$2700(Lcom/mediatek/bluetooth/hid/BluetoothHidService;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/app/Notification;

    move-result-object v11

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$2800(Lcom/mediatek/bluetooth/hid/BluetoothHidService;)Landroid/app/NotificationManager;

    move-result-object v0

    invoke-virtual {v0, v1, v11}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$2900(Lcom/mediatek/bluetooth/hid/BluetoothHidService;)Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v0

    if-eqz v0, :cond_2

    :try_start_3
    const-string v0, "[BT][HID][BluetoothHidService]"

    const-string v4, "mConnectTimeout close."

    invoke-static {v0, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$2900(Lcom/mediatek/bluetooth/hid/BluetoothHidService;)Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;->shutdown()V

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$2900(Lcom/mediatek/bluetooth/hid/BluetoothHidService;)Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    const/4 v4, 0x0

    invoke-static {v0, v4}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$2902(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;)Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    const-string v0, "[BT][HID][BluetoothHidService]"

    const-string v4, "mConnectTimeout close OK."

    invoke-static {v0, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_2

    :catch_1
    move-exception v8

    :try_start_4
    const-string v0, "[BT][HID][BluetoothHidService]"

    const-string v4, "mConnectTimeout close error."

    invoke-static {v0, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :sswitch_5
    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v0, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->stateMap:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v0, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->stateMap:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v4, "connecting"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v0, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->stateMap:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v4, "authorize"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_8
    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v0, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->cx:Landroid/content/Context;

    iget-object v4, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    const v5, 0x7f0600be

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v2, v12, v13

    invoke-virtual {v4, v5, v12}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v0, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    const-string v4, "disconnect"

    iget-object v5, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v5, v3}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$1100(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v5

    invoke-static {v0, v4, v5}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$1200(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;Landroid/bluetooth/BluetoothDevice;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    const-string v4, "disconnect"

    invoke-static {v0, v3, v4}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$2500(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    const v4, 0x7f0600b4

    const/4 v5, 0x1

    invoke-static {v0, v3, v4, v5}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$1000(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;IZ)V

    :cond_9
    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$2900(Lcom/mediatek/bluetooth/hid/BluetoothHidService;)Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    move-result-object v0

    if-eqz v0, :cond_a

    :try_start_5
    const-string v0, "[BT][HID][BluetoothHidService]"

    const-string v4, "mConnectTimeout close."

    invoke-static {v0, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$2900(Lcom/mediatek/bluetooth/hid/BluetoothHidService;)Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;->shutdown()V

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$2900(Lcom/mediatek/bluetooth/hid/BluetoothHidService;)Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    const/4 v4, 0x0

    invoke-static {v0, v4}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$2902(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;)Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    const-string v0, "[BT][HID][BluetoothHidService]"

    const-string v4, "mConnectTimeout close OK."

    invoke-static {v0, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    :cond_a
    :goto_3
    :try_start_6
    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v0, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->notifyMap:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v0, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->notifyMap:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    :cond_b
    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$2800(Lcom/mediatek/bluetooth/hid/BluetoothHidService;)Landroid/app/NotificationManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    goto/16 :goto_2

    :catch_2
    move-exception v8

    const-string v0, "[BT][HID][BluetoothHidService]"

    const-string v4, "mConnectTimeout close error."

    invoke-static {v0, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :sswitch_6
    invoke-static {}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$3000()Z

    move-result v0

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v0, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->cx:Landroid/content/Context;

    iget-object v4, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    const v5, 0x7f0600bf

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v2, v12, v13

    invoke-virtual {v4, v5, v12}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v0, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_c
    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    const/16 v4, 0x67

    invoke-static {v0, v4}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$2402(Lcom/mediatek/bluetooth/hid/BluetoothHidService;I)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v0, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->stateMap:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v0, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->stateMap:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v4, "unplug"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    const-string v4, "disconnect"

    iget-object v5, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v5, v3}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$1100(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v5

    invoke-static {v0, v4, v5}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$1200(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;Landroid/bluetooth/BluetoothDevice;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v0, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->stateMap:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v0, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->stateMap:Ljava/util/Map;

    const-string v4, "unplug_disconnect"

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v0, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mPreference:Landroid/preference/Preference;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v0, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->pc:Landroid/preference/PreferenceCategory;

    iget-object v4, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v4, v4, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mPreference:Landroid/preference/Preference;

    invoke-virtual {v0, v4}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    :cond_d
    :goto_4
    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$3100(Lcom/mediatek/bluetooth/hid/BluetoothHidService;)Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    move-result-object v0

    if-eqz v0, :cond_2

    :try_start_7
    const-string v0, "[BT][HID][BluetoothHidService]"

    const-string v4, "mDisconnectTimeout close."

    invoke-static {v0, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$3100(Lcom/mediatek/bluetooth/hid/BluetoothHidService;)Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;->shutdown()V

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$3100(Lcom/mediatek/bluetooth/hid/BluetoothHidService;)Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    const/4 v4, 0x0

    invoke-static {v0, v4}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$3102(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;)Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    const-string v0, "[BT][HID][BluetoothHidService]"

    const-string v4, "mDisconnectTimeout close OK."

    invoke-static {v0, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0

    goto/16 :goto_2

    :catch_3
    move-exception v8

    :try_start_8
    const-string v0, "[BT][HID][BluetoothHidService]"

    const-string v4, "mDisconnectTimeout close error."

    invoke-static {v0, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_e
    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    const-string v4, "disconnect"

    iget-object v5, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v5, v3}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$1100(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v5

    invoke-static {v0, v4, v5}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$1200(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;Landroid/bluetooth/BluetoothDevice;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v0, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->stateMap:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v0, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->stateMap:Ljava/util/Map;

    const-string v4, "disconnect"

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v0, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->notifyMap:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v0, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->notifyMap:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$2800(Lcom/mediatek/bluetooth/hid/BluetoothHidService;)Landroid/app/NotificationManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    :cond_f
    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    const v4, 0x7f0600b4

    const/4 v5, 0x1

    invoke-static {v0, v3, v4, v5}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$1000(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;IZ)V

    goto/16 :goto_4

    :sswitch_7
    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v0, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->stateMap:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v0, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->stateMap:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v4, "disconnecting"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v0, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->stateMap:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v4, "unplug"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    :cond_10
    invoke-static {}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$3000()Z

    move-result v0

    if-nez v0, :cond_11

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v0, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->cx:Landroid/content/Context;

    iget-object v4, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    const v5, 0x7f0600c0

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v2, v12, v13

    invoke-virtual {v4, v5, v12}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v0, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_11
    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    const-string v4, "connected"

    iget-object v5, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v5, v3}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$1100(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v5

    invoke-static {v0, v4, v5}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$1200(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;Landroid/bluetooth/BluetoothDevice;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    const-string v4, "connected"

    invoke-static {v0, v3, v4}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$2500(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    const v4, 0x7f0600b2

    const/4 v5, 0x1

    invoke-static {v0, v3, v4, v5}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$1000(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;IZ)V

    :cond_12
    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$3100(Lcom/mediatek/bluetooth/hid/BluetoothHidService;)Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0

    move-result-object v0

    if-eqz v0, :cond_2

    :try_start_9
    const-string v0, "[BT][HID][BluetoothHidService]"

    const-string v4, "mDisconnectTimeout close."

    invoke-static {v0, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$3100(Lcom/mediatek/bluetooth/hid/BluetoothHidService;)Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;->shutdown()V

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$3100(Lcom/mediatek/bluetooth/hid/BluetoothHidService;)Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    const/4 v4, 0x0

    invoke-static {v0, v4}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$3102(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;)Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    const-string v0, "[BT][HID][BluetoothHidService]"

    const-string v4, "mDisconnectTimeout close OK."

    invoke-static {v0, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catch Ljava/lang/InterruptedException; {:try_start_9 .. :try_end_9} :catch_4
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0

    goto/16 :goto_2

    :catch_4
    move-exception v8

    :try_start_a
    const-string v0, "[BT][HID][BluetoothHidService]"

    const-string v4, "mDisconnectTimeout close error."

    invoke-static {v0, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :sswitch_8
    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v0, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->cx:Landroid/content/Context;

    iget-object v4, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    const v5, 0x7f0600c1

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v2, v12, v13

    invoke-virtual {v4, v5, v12}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v0, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    const/16 v4, 0x67

    invoke-static {v0, v4}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$2402(Lcom/mediatek/bluetooth/hid/BluetoothHidService;I)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    const-string v4, "unplug"

    invoke-static {v0, v3, v4}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$2500(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v0, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->notifyMap:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v0, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->notifyMap:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$2800(Lcom/mediatek/bluetooth/hid/BluetoothHidService;)Landroid/app/NotificationManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    goto/16 :goto_2

    :sswitch_9
    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v0, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->cx:Landroid/content/Context;

    iget-object v4, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    const v5, 0x7f0600c2

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v2, v12, v13

    invoke-virtual {v4, v5, v12}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v0, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_2

    :sswitch_a
    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v0, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->notifyMap:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_13

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v0, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->notifyMap:Ljava/util/Map;

    invoke-static {}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$2600()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$2608()I

    :cond_13
    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    const-string v4, "connecting"

    iget-object v5, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v5, v3}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$1100(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v5

    invoke-static {v0, v4, v5}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$1200(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;Landroid/bluetooth/BluetoothDevice;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    const-string v4, "authorize"

    invoke-static {v0, v3, v4}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$2500(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    const v4, 0x7f0600b3

    const/4 v5, 0x0

    invoke-static {v0, v3, v4, v5}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$1000(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    const/4 v4, 0x1

    invoke-static {v0, v3, v4}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$2100(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;Z)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_0

    goto/16 :goto_2

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0xa -> :sswitch_8
        0xb -> :sswitch_9
        0x1b -> :sswitch_a
    .end sparse-switch
.end method
