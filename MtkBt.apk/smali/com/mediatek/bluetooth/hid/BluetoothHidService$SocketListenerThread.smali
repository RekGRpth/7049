.class Lcom/mediatek/bluetooth/hid/BluetoothHidService$SocketListenerThread;
.super Ljava/lang/Thread;
.source "BluetoothHidService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/bluetooth/hid/BluetoothHidService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SocketListenerThread"
.end annotation


# instance fields
.field public stopped:Z

.field final synthetic this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;


# direct methods
.method private constructor <init>(Lcom/mediatek/bluetooth/hid/BluetoothHidService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$SocketListenerThread;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Lcom/mediatek/bluetooth/hid/BluetoothHidService$1;)V
    .locals 0
    .param p1    # Lcom/mediatek/bluetooth/hid/BluetoothHidService;
    .param p2    # Lcom/mediatek/bluetooth/hid/BluetoothHidService$1;

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService$SocketListenerThread;-><init>(Lcom/mediatek/bluetooth/hid/BluetoothHidService;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$SocketListenerThread;->stopped:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$SocketListenerThread;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$000(Lcom/mediatek/bluetooth/hid/BluetoothHidService;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$SocketListenerThread;->stopped:Z

    goto :goto_0

    :cond_1
    const-string v0, "[BT][HID][BluetoothHidService]"

    const-string v1, "SocketListener stopped."

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public shutdown()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$SocketListenerThread;->stopped:Z

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$SocketListenerThread;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$100(Lcom/mediatek/bluetooth/hid/BluetoothHidService;)V

    return-void
.end method
