.class Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;
.super Ljava/lang/Thread;
.source "BluetoothHidService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/bluetooth/hid/BluetoothHidService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "actionTimeoutThread"
.end annotation


# instance fields
.field public BT_Addr:Ljava/lang/String;

.field public state:Ljava/lang/String;

.field private stoped:Z

.field final synthetic this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;


# direct methods
.method private constructor <init>(Lcom/mediatek/bluetooth/hid/BluetoothHidService;)V
    .locals 1

    iput-object p1, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;->stoped:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Lcom/mediatek/bluetooth/hid/BluetoothHidService$1;)V
    .locals 0
    .param p1    # Lcom/mediatek/bluetooth/hid/BluetoothHidService;
    .param p2    # Lcom/mediatek/bluetooth/hid/BluetoothHidService$1;

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;-><init>(Lcom/mediatek/bluetooth/hid/BluetoothHidService;)V

    return-void
.end method

.method private actionTimeout(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v3, v3, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->stateMap:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "[BT][HID][BluetoothHidService]"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ERROR: stateMap not contain "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-wide/16 v3, 0x64

    :try_start_0
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    add-int/lit8 v0, v0, 0x64

    :cond_2
    iget-object v3, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v3, v3, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->stateMap:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-boolean v3, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;->stoped:Z

    if-nez v3, :cond_3

    const v3, 0xea60

    if-lt v0, v3, :cond_1

    const/4 v2, 0x1

    :cond_3
    if-eqz v2, :cond_0

    const-string v3, "[BT][HID][BluetoothHidService]"

    const-string v4, "Waiting action time-out. Force return."

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "connected"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v3, p1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$200(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    const/4 v4, 0x5

    invoke-static {v3, v4, p1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$300(Lcom/mediatek/bluetooth/hid/BluetoothHidService;ILjava/lang/String;)V

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v3, "[BT][HID][BluetoothHidService]"

    const-string v4, "Waiting for action was interrupted."

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_4
    const-string v3, "disconnect"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    const/4 v4, 0x7

    invoke-static {v3, v4, p1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$300(Lcom/mediatek/bluetooth/hid/BluetoothHidService;ILjava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;->BT_Addr:Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;->state:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;->actionTimeout(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public shutdown()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;->stoped:Z

    return-void
.end method
